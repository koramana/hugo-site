---
title: Polish brioches
date: '2015-11-12'
categories:
- Buns and pastries
- Mina el forn
tags:
- Pastries
- Boulange
- Bakery
- To taste
- Bread maker
- meringues

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioches-polonaises-1.jpg
---
[ ![Polish brioches](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioches-polonaises-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioches-polonaises-1.jpg>)

##  Polish brioches 

Hello everybody, 

You remember the technical test last year of the best patissier, when Mercotte offer candidsats prepare the brioches Polish ... It was not an easy task for those, especially when Mercotte removes some details in the realization of the recipe that makes the recipe a bit difficult. 

I was cracking for the recipe, as is the case of Lunetoiles who finally got to work ... Anyway Lunetoiles took the opportunity to make another brioche recipe (a recipe to come), or she had a lot of pasta to make Polish brioches ... and the result is perfect, and she tells us: 

the dough is too soft, too good. The brioche is soft, it looks like small cushions, and we feel the good taste of grilled almonds, as we feel all the flavors and textures that mix. We have this wonderful taste of the cream, the melting texture of the brioche, and the crunchy side of the meringue ... 

**Polish brioches**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioches-polonaises-2.jpg)

portions:  6  Prep time:  60 mins  cooking:  20 mins  total:  1 hour 20 mins 

**Ingredients** For brioche dough (breads with milk): 

  * 540 g of flour 
  * 85 g caster sugar 
  * 10 g of milk powder (I have not put any) 
  * 8 g of salt 
  * 1 sachet Briochin dehydrated baker's yeast (or 20 g fresh baker's yeast) 
  * 60 g whole liquid cream 30% 
  * 2 eggs at room temperature 
  * 55 g lukewarm milk 
  * 50 g butter cut into pieces 
  * 200 g of tangzhong (I put everything) 

Tangzhong: 
  * 50 g of flour 
  * 125 ml of water 
  * 125 ml of milk 

For gilding: 
  * 1 egg yolk (preserved white which will be used for the meringue) 
  * a little bit of milk 

Confectioner's custard cream: 
  * 250 ml of milk 
  * 1 vanilla pod 
  * 70 g of sugar 
  * 2 egg yolks (preserved whites for meringue) 
  * 20 g of flan powder 
  * 20 g of butter 
  * 30 g whole cream 
  * 100 g candied fruit 

Soaking syrup: 
  * 150 ml of water 
  * 100 g of sugar 
  * 2 bags of vanilla sugar 

Italian Meringue: 
  * 3 egg whites 
  * 180 g of sugar 
  * 60 ml of water 



**Realization steps** Start with the tangzhong: 

  1. In a saucepan, mix with the whisk (manual), the flour with the water and the milk. Heat to a temperature not exceeding 65 ° while mixing. The mixture will thicken and look like a porridge. Once the temperature is reached ... remove from the heat. Book until it warms up. 
  2. In the bowl of your mess, pour 440 g flour, salt, sugar, milk powder. Mix. Then add the liquid cream, the two eggs and the tangzhong. 
  3. Lightly warm your milk and melt your yeast. Pour over the mixture in the bowl and operate your kneading on medium speed. Pour the yeast-milk mixture into a net. Let the robot work until you get a ball of dough. After about 10 minutes knead then add the butter in small pieces. When the butter is well incorporated, add your remaining flour in small amounts. Little by little you will see a ball of dough forming and peeling off the walls of the bowl. The dough must be homogeneous and a little sticky. 
  4. Cover and let stand near a radiator or heat source for one hour, or until the dough doubles in volume. 
  5. At the end of this hour, degas your dough on a work surface and divide it into 8 pieces of 60 - 70 gr each. Work the pieces of dough between your hands to get pretty balls that you place on a baking sheet covered with parchment paper. 
  6. Cover again with a cloth and leave for an hour and a half near a radiator or heat source. (with the rest of brioche dough, I make a loaf of milk in a cake mold) 
  7. min before the end of the rest period, preheat your oven to 170 ° C. 
  8. Brush your buns with an egg yolk mixed with a little milk. 
  9. Bake for about 25 minutes   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioches-polonaises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioches-polonaises.jpg>)

For candied custard custard: 
  1. Heat the milk with the vanilla bean split in half and scraped. 
  2. Mix the egg yolks with the sugar. Whisk. 
  3. Add the flan powder. 
  4. When the milk is boiling, pour over the egg / sugar / flan powder mixture. 
  5. Remove the vanilla pod. Put all in the saucepan over the heat and cook the cream until it thickens. 
  6. Stir in the butter and whisk until well mixed with the cream. 
  7. Pour the cream into a salad bowl and allow to cool. Shoot it in contact with the cream to prevent skin from forming. 
  8. When the cream is cold, whip it with the thick whole cream to make it supple and incorporate the candied fruit. 

For the soaking syrup: 
  1. Pour the water and sugars into a small saucepan and bring to a boil. 
  2. Cool completely. 

For the Italian meringue: 
  1. Boil the water and sugar in a saucepan. 
  2. Check the temperature with a probe. When the sugar reaches 100 ° C, start to turn the whites to full power. 
  3. When the sugar reaches 121 ° C, reduce the speed of the mixer and gradually pour the syrup. Once incorporated, return the mixer to full speed and let it run for 5 minutes. 

Assembly of Polish brioches:   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioche-polonaise-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioche-polonaise-.jpg>)

  1. Cut each buns, in three slices, soak each slice of syrup. 
  2. Then garnish with the candied fruit cream, and reform the brioche. 
  3. Cover Italian meringue buns and sprinkle with chopped almonds, then place on baking sheet covered with baking paper. 
  4. Bake at 240 ° C for 5 minutes. 



Note the Lunetoiles remark:   
In fact for this recipe, I took advantage of the hokkaido milk bread dough, and I took balls of 70 gr to make the Polish buns, and with what remained to me of dough (in all 700 gr) I made the Japanese hokkaido milk bread that I will share with you soon.   
  
Because on all the recipes of Polish buns that are on the net, you will notice that they give a brioche dough recipe and they say, from the outset that there will be too much, you will not use everything to make the brioches   
so here, so I was able to make two recipes today, with the same base 

[ ![polognese buns](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioches-polonaises-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/brioches-polonaises-4.jpg>)
