---
title: recipe Fondue of leeks
date: '2017-12-08'
categories:
- diverse cuisine
tags:
- inputs
- accompaniment
- dishes
- Easy cooking
- cod

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/fondue-de-poirreaux-004.jpg
---
[ ![recipe Fondue of leeks and cod back](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/fondue-de-poirreaux-004.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/fondue-de-poirreaux-002.jpg>)

##  Fondue recipe of leeks and cod back 

Hello everybody, 

We were not big fans of **leek recipes** at home, but since I shared with my children the recipe for [ leek soup ](<https://www.amourdecuisine.fr/article-soupe-aux-poireaux-veloute-de-poireaux-et-parmesan.html> "leek soup / leek soup and parmesan cheese") , and the [ leek pie ](<https://www.amourdecuisine.fr/article-tarte-aux-poireaux.html> "leek pie") Of all the wines I have made to many people, we have started to love this vegetable at home. 

So at dinner yesterday it was **leek fondue with cod back** , the most beautiful thing in the case is that I made my recipe in the assets that made it smelled less at home especially our apartment and very small, and it It's a cold duck, you can not open the window, and I do not have an aspiring host to start. 

Most importantly, this delicious dish or accompaniment of **leek fondue** was unanimous at home, no trace of rest on the plates, I who always grumbled that my children were not great eaters, and they were not open minded to eat all that is on their plate, I must say that I was surprised by their attitudes, I who had to plan to make them eggs on the dish if they did not eat their **leek fondue** !!! the recipe for leek fondue is not only delicious, it is a very simple recipe to make with ingredients within reach of everyone. 

If like me you will use your **Actifry** you have to feed the contents each time with a little water or chicken broth, so that the leeks do not burn and give a bitter taste a **leek fondue** .   


**Leek fondue**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/fondue-de-poirreaux-009.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 6 to 7 leeks 
  * 2 small shallots 
  * 2 tablespoons of butter 
  * 150 ml whole liquid cream 
  * salt and black pepper 
  * 4 Cod steaks 
  * lemon juice 
  * salt and black pepper. 
  * garlic powder 
  * olive oil 



**Realization steps**

  1. Start by marinating the cobblestones with a little salt, black pepper, garlic powder and fillet of lemon juice. leave aside until the moment of cooking 
  2. Peel and wash the shallots and leeks. 
  3. prune the leeks in two lengths, then thinly sliced 
  4. Melt the butter, and sweat the chopped onions with a pinch of salt. Cook over medium heat for 2 to 3 minutes, 
  5. Add leeks and cook again for 10 minutes.   
if you use the active it is necessary to add 2 spoonfuls of water, remove all parts of leeks located on the walls of the assets, so that it does not burn 
  6. Pour in the cream and black pepper. cook for another 5 min. Season the seasoning with salt and pepper to taste. 
  7. Now cook the cobblestones in a little oil, and arrange on the leek fondue to serve.   
I cooked the cobblestones in the assetry also with a small trickle of oil, do not forget to remove the pallet in this case. 



[ ![recipe Fondue of leeks and cod back](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/fondue-de-poirreaux-002.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/fondue-de-poirreaux-002.jpg>)
