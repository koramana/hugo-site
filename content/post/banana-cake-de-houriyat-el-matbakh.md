---
title: banana cake from Houriyat el matbakh
date: '2011-10-06'
categories:
- Algerian cakes- oriental- modern- fete aid
- Gateaux au chocolat
- Algerian dry cakes, petits fours

---
Hello everybody, 

I made this delicious cake, which is very soft, but that was not at all the kind of cake that my husband loves, and my children, they eat but not in a way that will please me .... So it's up to you to guess who's finally eating everything. 

I had to post an article on my day in the forest, but I did not have time to arrange the photos, but I'll give you some pictures: 

we had to spend a sublime day, the last picture, is the photo of a very old boat on the quay of the Avon bristol 

one day I will make lots of photos. 

go for the recipe before I start talking to you about this wonderful walk in the forest. 

  * 260 g flour 
  * 1 teaspoon of baking powder 
  * 120 g of sugar 
  * 4 c. honey 
  * 4 eggs 
  * 1 teaspoon of vanilla 
  * 5 c. vegetable oil 
  * 5 c. tablespoon milk, liquid 
  * A pinch of salt 
  * 3 ripe bananas 



for the glazing: 

  * 150 g caster sugar 
  * c. cocoa powder 
  * 1 egg white + a pinch of salt 
  * 1 tablespoon Nescafe (instant coffee) 
  * 4 c. fresh whipping cream   




  
Sift flour, sugar, salt and baking powder. 

in another container, mix the liquids, add the dry ingredients. mix everything together, and add the crushed bananas.   
Mix and pour into a mold well butter and flour. 

cook in a preheated oven at 180 ° C for 40 to 45 minutes   
during this time prepare the frosting: 

Beat the egg white with a pinch of salt, gradually add the powdered sugar   
Add cocoa and coffee powder dissolved in fresh cream 

  
at the end of the oven let cool a little and unmold, and start to sprinkle with frosting, because the frosting is a bit liquid, you place your cake on a rack, under which you place a plate, and you get all the frosting which falls down, and you water until the cake is well covered. 

the glaze appears a bit sticky, because I put the cake on the microwave and it was covered by a bell, I believe in the heat that escapes the microwave wave melted ice, hihihihi 

thank you very much for your visits. 

bonne journee 
