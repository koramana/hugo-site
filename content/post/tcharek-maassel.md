---
title: Tcharek Maassel
date: '2017-06-19'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- sweet recipes
tags:
- Algerian cakes
- Cakes
- Algeria
- Aid cake
- baklava
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tcharek-maassel-3-683x1024.jpg
---
![Tcharek Maassel](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tcharek-maassel-3-683x1024.jpg)

##  Tcharek maassel 

Hello everybody, 

I remember the first time I realized this cake **Tcharek Maassel** , at my brother's wedding in 2006. It was realized me and my sister-in-law, and I admit that it was a huge success at the time. Since then, I do it every time at the request of my husband, but never had the opportunity to share it on the blog. It's a delicious honey cake, and it's not easy to resist this delight. 

For this Eid el fitr 2017, I made this cake and video for you, you will see how easy this recipe is, I did it in advance for you, and I think I have to do it because with my husband and those friends who come to court, there is almost nothing left for Eid, hihihihi 

{{< youtube PTQGN83CoMg >}} 

**Tcharek Maassel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tcharek-maassel-2-1.jpg)

portions:  30  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** Dough : 

  * 3 measures of flour 
  * 1 measure of butter 
  * 1 whole egg 
  * 1 tablespoon iced sugar 
  * 1 cup of vanilla coffee 
  * 1 cup of yeast 
  * ½ glass of rose water 

The joke: 
  * 2 glasses of almonds 
  * 1 glass of icing sugar 
  * 2 tablespoons of melted margarine 
  * Water With Rose Water 
  * Vanilla (optional) 

decoration: 
  * Egg yolk 
  * milk 
  * crushed almonds 
  * honey 



**Realization steps**

  1. Start by preparing the dough, mixing the ingredients without kneading too much. 
  2. Collect the dough to have a homogeneous ball. 
  3. divide the dough into a ball and let it rest between 20 minutes and 1 hour. 
  4. Mix the ingredients of the stuffing and form small sausages of almost 8 cm 
  5. Spread the dough on a lightly floured work surface, to a height of almost 3 cm 
  6. cut rectangles with a griwech mold   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tcharek-maassel-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tcharek-maassel-2.jpg>)
  7. place in each rectangle a bunch of stuffing in a manner that is diagonal. 
  8. cover the pudding with the dough, and form a small croissant. 
  9. brush your cakes with a beaten egg, or an egg yolk and some milk 
  10. sprinkle with coarsely crushed almond powder 
  11. cook the cakes in a preheated oven at 180 degrees C 
  12. at the end of the cooking, let cool the cakes, before plunging them into honey perfumed with orange blossom water. 



![Tcharek Maassel 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tcharek-maassel-1-1-799x1024.jpg)
