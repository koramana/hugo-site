---
title: Couscous with vegetables / amekfoul, Algerian cuisine
date: '2014-01-12'
categories:
- diverse cuisine
- Cuisine by country
- Unclassified
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/couscous-aux-legumes-amekfoul-013-copie-1.jpg
---
![couscous aux legumes - amekfoul-013-copy-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/couscous-aux-legumes-amekfoul-013-copie-1.jpg)

##  Couscous with vegetables / amekfoul, Algerian cuisine 

Hello everybody, 

If you ask me that it is your favorite recipe, I will say without reflecting "amekfoul" or couscous with vegetables go by steam, hum, this dish is a real delice ... 

A recipe that I learned from my mother, and what's good is that we can do in it all the available vegetables or vegetables we like. 

**Couscous with vegetables / amekfoul, Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Couscous-aux-legumes.jpg)

Recipe type:  Algerian dish  portions:  4  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * [ Couscous steamed ](<https://www.amourdecuisine.fr/article-comment-cuire-le-couscous-111575933.html> "how to cook couscous")
  * 1 glass of peas. 
  * 1 glass of carrots, diced. 
  * 1 onion 
  * 1 sweet pepper. 
  * salt 
  * olive oil. 



**Realization steps**

  1. wash and clean all the vegetables, and cut them in, except the peas of course ... 
  2. place the bottom of a couscoussier with enough water over high heat. 
  3. place all the vegetables with a little salt in the top of the couscoussier. 
  4. steam, covering so vegetables cook quickly. 
  5. when the vegetables are cooked, add the couscous on top, and let the steam escape between the couscous grains. 
  6. pour all in a large bowl. 
  7. add the olive oil according to your taste, between 3 to 4 tablespoons, or according to the quantity of couscous, and mix with a wooden spatula. 
  8. in this way the dish is ready, you can serve it with shallot, or red onion, if you like, otherwise with a nice fresh salad. 



Méthode de préparation: 
