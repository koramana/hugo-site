---
title: Extra soft and easy lemon cake
date: '2013-05-09'
categories:
- diverse cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/Cake-au-citron-2.CR2_thumb.jpg
---
##  Extra soft and easy lemon cake 

Hello everybody, 

a very easy recipe, frankly, I read a lot of recipes, but it's the easiest of them all. 

As you notice I do not put a lot of frosting on my cake, because at home we do not like too much, but otherwise a beautiful layer of icing will only make the cake more appetizing, so do not hesitate to dress this delicious cake in lemon with this nice layer of sweet tart glaze, yum, so I'll give you the recipe, and I'll put my changes:   


**Extra soft and easy lemon cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/Cake-au-citron-2.CR2_thumb.jpg)

Recipe type:  cake  portions:  8  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 4 eggs 
  * 250 gr of caster sugar 
  * 1 pinch of fine salt 
  * 100 gr of liquid cream. 
  * the zest of 2 lemons 
  * 190 gr of flour 
  * 4 gr of baking powder   
I put ½ cup of baking powder 
  * 70 gr cold melted butter. 

icing:   
(For my part I made half of the ingredients, because my husband does not like the frosting) 

  * 40 gr of water 
  * 180 gr sifted icing sugar 
  * 10 gr of lemon juice 



**Realization steps**

  1. Preheat your oven to 170 ° C. 
  2. Melt the butter in a saucepan and set aside. 
  3. Sift the flour with the baking powder and then zest the lemons. 
  4. With the mixer, put together the eggs and the caster sugar until the mixture doubles in volume. 
  5. Then add the salt, liquid cream, lemon zest, sifted flour with yeast and finally cold melted butter by gently mixing with a whisk. 
  6. Butter and flour a cake mold (19cm long, 8 cm wide and 7 cm high) and garnish with the cake dough. 
  7. Bake and cook for about 1 hour (check the cooking by pressing the blade of a knife, it must come out slightly wet) 
  8. Prepare the ice-cream, Mix the icing sugar with the water and the lemon juice with a whisk. 
  9. When the cake is lukewarm, cover it completely with ice and pour it over it. 
  10. Enjoy this tepid or cold cake. 


