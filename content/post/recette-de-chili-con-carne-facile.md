---
title: easy con carne chili recipe
date: '2017-06-28'
categories:
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-021.CR2_thumb.jpg
---
[ ![chili con carne 021.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-021.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-021.CR2_2.jpg>)

##  easy con carne chili recipe 

Hello everybody, 

Oh!! Chili con carne !!! This is what my husband said when he saw his dish, a recipe that he likes chili con carne and I do not do often, because when you say Chile, it's very spicy, spicy to the Mexican in more…. 

Of course, all that is spicy, is a good recipe for my husband, and when it contains minced meat, and beans, what to ask for more? 

It's difficult for me to take these pictures, because he was in a hurry: it has cooled now, quickly, quickly, I have to go out ... 

**Chili con carne, easy and spicy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-022.CR2_thumb.jpg)

Recipe type:  dish  portions:  2  Prep time:  30 mins  cooking:  50 mins  total:  1 hour 20 mins 

**Ingredients**

  * 200 g of red beans 
  * 200 g ground beef 
  * 2 onions 
  * 1 clove of garlic 
  * ½ red pepper (spicy here, if you do not like, just sweet red pepper) 
  * 500 g tomatoes 
  * 2 teaspoons chili powder 
  * ½ teaspoon powdered cumin 
  * oil 
  * salt 



**Realization steps**

  1. soak the beans in cold water for 12 hours. (I do it at night) 
  2. in the morning, drain and put in a casserole. fill the casserole with enough cold water and cook until the beans become tender. 
  3. slice the onions, and place them in a pan. 
  4. cut the red pepper into cubes and chop the garlic.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-011_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-011_thumb1.jpg>)
  5. add the garlic and peppers to the onions, introduce the oil. 
  6. sweat a few minutes. 
  7. Add the minced meat. 
  8. cook well. 
  9. when the beans are cooked, drain and put back in the casserole. 
  10. pour over the minced meat sauce. 
  11. add over the seeded and crushed tomatoes. 
  12. season with spices and salt. 
  13. add a little water, cook for about 30 minutes. Check the cooking 
  14. Serve hot. 



[ ![chile con carne.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne.CR2_2.jpg>)
