---
title: Djerbian rice with chicken or rouz jerbi
date: '2015-12-21'
categories:
- Algerian cuisine
- Tunisian cuisine
- rice
tags:
- Ramadan
- Vegetables
- Steam cooking
- Tomato sauce

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/riz-djerbien-au-poulet.jpg
---
[ ![Djerbian rice with chicken or rouz jerbi](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/riz-djerbien-au-poulet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/riz-djerbien-au-poulet.jpg>)

##  Djerbian rice with chicken or rouz jerbi 

Hello everybody, 

Those who visit my blog often know how much I love Tunisian cuisine. Having lived a beautiful period of my life in a city not too far from Tunisian borders, and also because of my repeated trips to Tunisia, I discovered several sublime recipes as delicious as each other. When you go to Tunisia, I never eat in the restaurants of the hotel, I always prefer going out to eat in the small restaurants of the district, just to taste the real Tunisian cuisine. 

To be honest, I did not have the chance to eat the Djerbian rice in one of these restaurants, but our neighbor Dréan, always shared a dish with us every time she realized rouz jerbi. It was super hot, but I ate all the same, all sweat and tears, hihihihi I liked a lot. 

Fortunately my mother managed to take the recipe from the neighbor, who did not always give the recipe to the letter, hihihihi. In any case, wanting to do it this time my friend gave me a nice tip to have an even more tasty and fragrant rice. Thanks Nadia for the tip, even my mother liked this new recipe. 

[ ![rouz jerbi](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/rouz-jerbi.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/rouz-jerbi.jpg>)   


**Djerbian rice with chicken or rouz jerbi**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/rouz-jerbi-be-jaj-.jpg)

portions:  6  Prep time:  30 mins  cooking:  50 mins  total:  1 hour 20 mins 

**Ingredients**

  * 2 cups of basmati rice 
  * 3 tablespoons of extra virgin olive oil 
  * 2 chicken breasts 
  * 2 medium onions 
  * 2 to 3 cloves of garlic 
  * ½ can of crushed tomatoes or 2 medium tomatoes 
  * 1 tablespoon of tomato paste 
  * 300 g chopped fresh spinach 
  * 1 bunch of chopped flat parsley 
  * 2 carrots cut into small cubes 
  * 1 nice handful of peas 
  * 2 potatoes cut into cubes 
  * 1 glass of chickpeas in a box 
  * 1 teaspoon of cumin 
  * 1 tbsp ground coriander 
  * 1 tablespoon paprika 
  * 1 teaspoon of turmeric 
  * 1 teaspoon coffee 
  * salt pepper 



**Realization steps**

  1. In a large skillet, place chopped onion in hot oil 
  2. leave to brown well until you have a beautiful translucent color 
  3. add the chicken breast cut into cubes of almost 2 to 3 cm 
  4. let it come back a little, add the crushed garlic, the crushed tomato and the tomato paste. 
  5. add the spices, and the chopped parsley and let it cook   
This method of cooking gives a chicken more good and super tasty, better than a chicken just steamed. 
  6. cut the spinach and place in a large terrine 
  7. pour over the chicken and tomato sauce   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/poulet-du-riz-en-sauce1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/poulet-du-riz-en-sauce1.jpg>)
  8. add cubed potatoes, peas, and carrots. 
  9. rinse the rice under abundant water until the water becomes clear 
  10. add the rice on the water other elements, and mix well with a wooden spoon 
  11. place the mixture in the top of the couscoussier, make a small hole inside to allow the steam to escape well.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/rouz-jerbi-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/rouz-jerbi-3.jpg>)
  12. place it under boiling water, cover for a quick cooking. 
  13. When half cooked, put the contents of the couscous in the terrine, mix with the spoon, this allows the uniform cooking of all the ingredients. 
  14. add the chickpeas that are already cooked, mix again, and put back in the top of the couscoussier. 
  15. Serve immediately   
because my husband loves the sauce I always keep a little chicken sauce with tomato sauce, so that he can enjoy it with his rice. 



[ ![rouz jerbi 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/rouz-jerbi-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/rouz-jerbi.jpg>)
