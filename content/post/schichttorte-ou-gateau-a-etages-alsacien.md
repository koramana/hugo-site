---
title: Schichttorte or cake in Alsatian
date: '2015-09-06'
categories:
- cakes and cakes
- sweet recipes
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Schichttorte.jpg
---
[ ![Schichttorte or cake in Alsatian](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Schichttorte.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Schichttorte.jpg>)

##  Schichttorte or Alsatian tiered cake, 

Hello everybody, 

The Schichttorte or cake in Alsatian stages, or the baumkuchen which begins to make its little noise on facebook under the name of cake tree is a super delicious Alsatian cake, whose cooking differs completely from other cakes that can be made. 

We had the same right to make it between mom for the children's school, we had made 12 cakes, and it's true the cake was very nice, I had to eat a spoon, just to know the taste. I had then planned to redo it, but here, there are always plenty of projects to recipes here and there, that I had forgotten this cake. 

Returning in any case to this delight Schichttorte which is cooked under the grill, so the cake baked from above, it pours gradually as a small layer of dough when it is cooked, poured another layer, until 'exhaustion of the dough. 

On facebook it is called the tree cake, because it looks a lot like the multilayer that can be seen in the cut of a tree. Just for information, the cake is cooked like the Chawirma on a horizontal axis: 

[ ![Baumkuchen Schichttort](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Baumkuchen-Schichttort-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Baumkuchen-Schichttort.jpg>) [ ![traditional schichttorte](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/schichttorte-traditionnelle-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/schichttorte-traditionnelle.jpg>)

In any case a big thank you to my dear **Wamani merou** for these beautiful pictures, and for this beautiful achievement ...   


**Schichttorte or cake in Alsatian**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/schichttorte-ou-le-gateau-a-etages-alsacien.jpg)

portions:  8  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 4 eggs 
  * ¾ glass of sugar 
  * ¼ glass of milk 
  * ¾ glass of flour 
  * 1 cup of baking powder 
  * ½ glass of oil 

Icing: 
  * 2 boxes of chocolate 
  * 2 tablespoons of oil 
  * chocolate vermicelli 



**Realization steps**

  1. beat the eggs and sugar until it doubles in size. 
  2. add the milk while whisking 
  3. introduce flour and baking powder 
  4. then add the oil, everything must be well incorporated. 
  5. line a 22 cm diameter pan with a piece of baking paper, and light your oven from above. 
  6. pour 3 tablespoons of dough into the mold, it should be well spread, and cook from above until it turns a beautiful golden color. 
  7. remove from the oven, and pour over this cooked layer 3 more tablespoons of dough, and brown once more. 
  8. continue until the dough is exhausted. 
  9. let cool, then remove the cake from the mold. 
  10. Melt the chocolate in a bain-marie, introduce the oil to incorporate it well 
  11. cover the cake with this icing, and decorate with the chocolate vermicelli 



[ ![Schichttorte or cake in Alsatian](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/schichttorte-gateau-a-etages-alsacien.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/schichttorte-gateau-a-etages-alsacien.jpg>)
