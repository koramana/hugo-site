---
title: Peach and banana smoothie
date: '2015-11-26'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-et-peche.jpg
---
[ ![banana smoothie and peaches](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-et-peche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-et-peche.jpg>)

##  Peach and banana smoothie 

Hello everybody, 

Another smoothie recipe, but this time it's a Lunetoiles recipe she sent me in September, and I did not have time to post. And because she saw me in mood smoothie recently, she reminded me that this recipe warms well in the box of my archives, hihihihih. 

A super smooth smoothie in the good taste of peach and banana, me anyway, I'll be up for a drink like that at breakfast. 

**Peach and banana smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-peches.jpg)

portions:  3  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 250 ml of organic milk (or vegetable milk) 
  * oranges 
  * 1 banana 
  * 3 ripe peaches 



**Realization steps**

  1. Squeeze oranges until you get 250 ml of juice. 
  2. Put the milk cold, the banana cut into slices, the peaches peeled and cut into pieces in the blinder, mix a little, add the orange juice continue mixing, pour into a carafe and serve immediately! 
  3. If the smoothie is too thick just add a little milk before giving a last shot of blinder! 



[ ![banana smoothie and peaches](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-peche-et-banane.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-peche-et-banane.jpg>)
