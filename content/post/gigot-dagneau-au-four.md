---
title: Gigot D'Baked Lamb
date: '2018-03-17'
categories:
- Algerian cuisine
- diverse cuisine
- idea, party recipe, aperitif aperitif
- Dishes and salty recipes
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/aid-el-kebir-gigot-d-agneau-au-four.CR2_2-300x187.jpg
---
##  Gigot D'Baked Lamb 

Hello everybody, 

This recipe for roasted leg of lamb, roasted and grilled at wish is just a delight ... A very easy way but that takes a lot of time when cooking. So plan 3 hours of cooking or more, depending on the size of leg of lamb, to have a tender meat, melting in the mouth, juicy and especially rich in taste. 

Do not let this cooking time push you to not make this recipe, know that the result is really worth it. 

you can also see the recipe of: [ leg of lamb in the pressure cooker ](<https://www.amourdecuisine.fr/article-26570801.html>)   


**Gigot D'Baked Lamb**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/aid-el-kebir-gigot-d-agneau-au-four.CR2_2-300x187.jpg)

Recipe type:  baked dish  portions:  6  Prep time:  20 mins  cooking:  150 mins  total:  2 hours 50 mins 

**Ingredients**

  * 1 leg of lamb of 2 kilos for me (not too big) 
  * 6 to 7 cloves of garlic 
  * some peeled rosemary 
  * olive oil 
  * Butter 
  * salt, pepper and coriander. 
  * water 



**Realization steps**

  1. Preheat the oven to 220 ° C. 
  2. In a baking dish, place your leg of lamb. Make small incisions in the leg. 
  3. insert in garlic cut on two, you can reduce the amount if you do not like garlic, in any case it's just for the flavor, because at the end of cooking garlic will be so melted, that you will not feel it. 
  4. add rosemary, and the pieces of butter in these small incisions. 
  5. season with salt and pepper and add the coriander powder. 
  6. Drizzle with olive oil. 
  7. add 1 half glass of water and put in the oven.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gigot-de-mouton-au-four-aid-el-adha-2012_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gigot-de-mouton-au-four-aid-el-adha-2012_thumb1.jpg>) place a tray on the top floor of the oven, and put in a little water, to have a wet oven. 
  8. Put in the leg. After 15 minutes of cooking, lower the temperature to 180 ° C and cook for 3 hours. (not required because it depends on your oven) 
  9. every 15 minutes, turn the leg of lamb, and sprinkle with the cooking juice, and add 1 half glass if the sauce evaporates 
  10. also the tray in bs with water, do not let it dry, add water as and when. 
  11. if your leg starts to pick up color, you can cover it with aluminum foil for the last 30 minutes of cooking. 



Note knowing that my oven is a fan oven, so if your oven is gas, or the heat comes from below, put the water in a glass of water, instead of pouring it into the tray because it will dry faster.   
if the oven is too hot, you can lower the temperature up to 150 degrees., which I did, I prefer that the meat takes time to cook slowly, that to cook in 30 minutes has a heat too high it will burn, and give a dry and hard meat.   
shame for me it was photos taken at night, and I could not take pictures of the cut, but the leg was tender wish, mellow, delicious .... Your turn to try.   
rosemary is about taste and flavor, you can replace with bay leaves, or any other fine herbs that you like. 

you can also see: 

[ ![Shoulder of Lamb and Baked Potato 016.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/epaule-d-agneau-et-pomme-de-terre-au-four-016.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/article-epaule-d-agneau-rotie-au-four-113882735.html>)

[ epaule d’agneau rotie au four ](<https://www.amourdecuisine.fr/article-epaule-d-agneau-rotie-au-four-113882735.html>)
