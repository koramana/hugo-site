---
title: raisin fritters, Algerian sfenj
date: '2012-09-25'
categories:
- cuisine algerienne
- cuisine diverse
- Cuisine par pays
- recette a la viande rouge ( halal)
- riz

---
hello everyone, from Algerian donuts to raisins, is a recipe that you absolutely must try ... I personally am looking for the first opportunity to realize them ... and this time, I did them for my daughter Ines. Yesterday it was his first day at school, and the custom in Algeria is that we make donuts this day (well it's customary at home, I do not know what others do) so yesterday it was donuts at home. and as Ines is very fond of raisins (like his mum for that matter), so it was & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.7  (  1  ratings)  0 

Hello everybody, 

Algerian donuts with raisins, is a recipe that you must try .... I personally am looking for the first opportunity to realize them ... 

and this time I did it for my daughter Ines. Yesterday it was his first day at school, and the custom in Algeria is that we make donuts this day (well it's customary at home, I do not know what others do) so yesterday it was donuts at home. 

and as Ines likes raisins a lot (like his mom), so it was donuts with raisins. 

and the, my little princess who was the model for mom, of course in England, is the uniform, and what made me happy to see my daughter in school uniform, it's all adorable . 

So we go back to the donuts ... and it was like each time a long story before realizing .... 

Because it's her first day, Ines was in the second group doing the afternoons for a week, then it's going to be the mornings, for another week, and then it's going to be the full day. 

So, the program was to give him a shower, to eat, to prepare the dough for donuts, and the time that the dough is resting, I take my daughter to school which begins at 12:30 ... 

So, everything is well programmed, we lingered a little, and I began to run in all directions .... then we run out, because we'll be right on time .... 

We arrive at school, I start looking for a place for the car, no need for a fine, right? 

Nobody is in front of school, I panic, we are late, I find it difficult to park the car, I go back to school, and I find parents who come to pick up the children who made the morning. 

I look here and there, I do not see the other children, I get to class, and the teacher tells me: 

"But your daughter starts at 1:30 pm !!!!" ...... 

so you imagine the scene ???? .... 

we're back to our donuts now, hihihihihi 

I made donuts with the mixture semolina and flour, a recipe too good that gives donuts very mellow. and you can see my other recipe for [ sfenj ](<https://www.amourdecuisine.fr/article-beignets-khfaf-ou-sfendj-64651536.html>) , too good too 

Ingredients: 

  * 400 g flour 
  * 150 g of semolina. 
  * 1 cup instant yeast 
  * 1/2 teaspoon salt 
  * 1 egg 
  * 1 tablespoon of sugar 
  * lukewarm water 
  * a nice handful of raisins 
  * frying oil 



method of preparation: 

  1. In a large bowl or kneader or bread machine put the flour, semolina, yeast, salt and egg 
  2. add warm water to wet well 
  3. pick up the dough (neither too hard nor too soft) 
  4. leave a few moments 
  5. Then gently wet your dough, working with the palms of your hands until you get a very soft and elastic dough 
  6. cheat your hands in lukewarm water and take the dough with your fingertips to bend it violently in this way you will air your dough well. 
  7. stir in the raisins while kneading. 
  8. Let the dough rest in a large salad bowl. 
  9. let it rise for 1 hour 
  10. Put a deep frying pan on medium heat, 3/4 full of oil, let warm (very important) 
  11. take a bowl filled with water, dip your fingers in it 
  12. take the value of a large egg dough, well stretched between the fingers gently and quickly to give it a shape length or round according to your choice 
  13. Put in the pan, sprinkle with hot oil, brown the first side then turn to brown the 2nd side. 
  14. do the same way until the dough is exhausted 



Note: 

You can put all these ingredients in the bread machine, but be careful, you must monitor the amount of water (add warm water gently) 

you can present your donuts coated with crystallized sugar, or with honey. 

and it's too good to accompany a tea or coffee. 

Generally, when I make a large quantity, I put in the freezer in a bag, and as soon as I want to eat it, I make out half an hour before. 

at the last moment before serving, you can turn them in the microwave for 4 seconds .... it's going to be like this goes out of the fryer. 

thank you for your visits and comments. 

have a nice day. 

et a une prochaine aventure avec “ Amour de Cuisine” 
