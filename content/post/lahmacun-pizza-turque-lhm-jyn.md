---
title: lahmacun - Turkish pizza لحم عجين
date: '2016-07-25'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/lahmacun-052_thumb_11.jpg
---
##  lahmacun - Turkish pizza لحم عجين 

Hello everybody, 

I remember on my last trip to Turkey this delicious pizza, in the large format, that I devoured me and my husband with teeth, a delight that I will never forget, and although it was piquant part report but I did not let my husband eat it alone. 

so today I share with you the lahmacun recipe, or Turkish pizza as I found it on one of my books, I hope you will realize it, succeed it and love it. 

So I share with you the recipe:   


**lahmacun - Turkish pizza لحم عجين**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/lahmacun-052_thumb_11.jpg)

portions:  4  Prep time:  45 mins  cooking:  20 mins  total:  1 hour 5 mins 

**Ingredients** for the dough: 

  * 2¼ teaspoons freeze-dried yeast (instant for me) 
  * 225 ml hot water 
  * 2 tablespoons sweet butter, melted 
  * 2 tablespoons olive oil 
  * 1 teaspoon of salt 
  * 400 g flour or until a dough 

for garnish: 
  * 1 tablespoon olive oil 
  * 1 red onion, finely chopped 
  * 450 g ground meat, lamb or beef, (lamb for me) 
  * 225 g tomato 
  * 4 tablespoons flat parsley, finely chopped 
  * 3 tablespoons pine nuts, grilled 
  * ¼ teaspoon cinnamon powder 
  * ⅛ teaspoon of four-spice powder (if you have not put some ginger and nutmeg) 
  * a nice pinch of clove powder 
  * a pinch of crushed red pepper (optional) 
  * ½ teaspoon salt 
  * ½ teaspoon of freshly ground black pepper 
  * 1 tbsp fresh lemon juice 
  * 110 g melted butter 



**Realization steps** preproduction of the dough: 

  1. In a bowl mix the yeast in the hot water, reserve 10 minutes the yeast must be completely dissolved 
  2. stir in all the melted butter and olive oil 
  3. add the salt, then the flour a little at a time until the dough begins to form 
  4. knead to obtain a flexible and elastic set 
  5. place the dough in a lightly oiled bowl 
  6. roll it so that it is immersed in oil evenly 
  7. cover the bowl with a paper towel and place it in a warm and draft-free place for 1 hour, to allow the dough to rise. it must double in size 

preparation of the filling: 
  1. while double volume dough, prepare the meat filling 
  2. in a large skillet, heat 1 tbsp olive oil. 
  3. Make the onions return 2 minutes; they must be translucent. 
  4. Add the minced meat, stir and continue cooking until the meat is well cooked. 
  5. add tomatoes, tomato paste, parsley, pine nuts or pistachios and spices 
  6. let simmer 10 to 15 minutes, then sprinkle with lemon juice 
  7. preheat the oven to 240 ° C (th 8) 
  8. divide the dough into 16 equal parts the size of an egg. 
  9. spread each portion of dough into a disk 
  10. put them down on an oiled or covered baking sheet and let stand for 10 minutes 
  11. garnish each disc with the meat mixture and sprinkle with melted butter 
  12. bake for 8 to 10 minutes; the edges should be nicely gilded 


