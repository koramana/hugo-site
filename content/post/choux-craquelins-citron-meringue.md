---
title: cabbage crackers lemon meringue
date: '2016-01-07'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Cracker
- Lemon curd
- Cream puffs

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-craquelin-au-citron-meringu%C3%A9-1.jpg
---
[ ![cabbage cracker with lemon meringue 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-craquelin-au-citron-meringu%C3%A9-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-craquelin-au-citron-meringu%C3%A9-1.jpg>)

##  cabbage crackers lemon meringue 

Hello everybody, 

For this recipe, **Food** had prepared a [ custard ](<https://www.amourdecuisine.fr/article-creme-patissiere.html>) that it has flavored with [ lemon curd ](<https://www.amourdecuisine.fr/article-lemon-curd-creme-au-citron-fait-maison.html>) (that is to say, it adds a little lemon curd to the cream to have the lemony taste ... But if you want the real taste you can make the lemon cream like the one I put in my [ lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee.html>) . 

**cabbage crackers lemon meringue**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-craquelin-au-citron-meringu%C3%A9-1-001.jpg)

portions:  30  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** For about 30 cabbages: 

  * 150 ml of water 
  * 60g of butter 
  * 10g of sugar 
  * 2g of salt 
  * 80g of flour 
  * 148g of eggs (about 3 whole eggs but it is important to weigh them) 

Cracker: 
  * 50g of flour 
  * 50g of soft butter 
  * 50g of brown sugar 
  * a pinch of salt 

Cream pastry with vanilla: 
  * 500ml of milk 
  * 1 vanilla pod 
  * 3 eggs 
  * 100g of sugar 
  * 40g cornflour 
  * 25g of butter 
  * two pinches of salt 

to flavor 
  * [ lemon curd ](<https://www.amourdecuisine.fr/article-lemon-curd-creme-au-citron-fait-maison.html>) according to taste 
  * lemon zest 
  * lemon juice 

Meringue: 
  * 2 egg white 
  * 80 gr of sugar 
  * 1 pinch of salt 



**Realization steps** for the pasta 

  1. Boil water, sugar, salt and butter. 
  2. Remove from heat from boiling and add the sifted flour at once and stir very quickly using a spatula. 
  3. Put over medium heat to dry the dough without stirring for 1 to 2 minutes. 
  4. The dough now peels off easily from the sides, let cool or put in your pastry bowl.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/preparation-pate-a-choux1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/preparation-pate-a-choux1.jpg>)
  5. Light the oven at 210 C static heat (no rotating heat). 
  6. When the puff pastry is lukewarm, beat the eggs and add them 3 times, stirring vigorously each time, after incorporation, start again and mix again. You can do this manually or use the flat sheet (k sheet) of your robot. The dough should make a bird's beak at the end, it should not be too liquid, be sure to respect the measures of the eggs. 
  7. Remove the cracker from the chop and cut out some discs * slightly smaller than your cabbages. Using a plain pastry bag, place the 4cm sprouts on a plate and place a cracker disk on each cabbage. 
  8. Tip: I cut the discs using the back of the socket. Otherwise a plug can do the trick.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/pr%C3%A9paration-du-craquelin.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/pr%C3%A9paration-du-craquelin.jpg>)
  9. Bake in static heat at 210C for 10 min then lower the temperature to 180 C (still static) for 20min: the cabbages should be well rounded, a blond caramel color. Cooking is essential for the success of cabbage. 
  10. Important: do not open the oven door while cooking, cabbage will fall back. 

Cream "pastry" to aromatize. 
  1. Heat the milk in a saucepan with the vanilla bean split in half. 
  2. In a bowl, mix eggs, sugar, cornflour, add hot milk, mix, and put back in the pan to thicken until boiling, stirring constantly. Turn off and add the butter. 
  3. Reserve and film in contact with the cream to not have "crust". 
  4. As soon as the cream is warm, add the lemon with lemon curd and lemon zest 
  5. make the meringue with 2 egg whites and 80g of sugar, 
  6. Finally, garnish the cabbage with a thin casing by slightly perforating the underside or cut the top of the cabbage to garnish from above and put the disc over it, 
  7. Start filling the cabbage with lemon cream   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-craquelin-citron-meringu%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-craquelin-citron-meringu%C3%A9.jpg>)
  8. add over a beautiful cloud of meringue   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-craquelin-citron-meringu%C3%A9-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-craquelin-citron-meringu%C3%A9-2.jpg>)
  9. Cover the meringue with the cabbage hat 
  10. use a torch to caramelize the meringue, and enjoy. 


