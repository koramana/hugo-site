---
title: GRIWECH FEUILLETE dessert of Ramadan 2014
date: '2014-06-01'
categories:
- Algerian cakes with honey
- fried Algerian cakes
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/griwech-feuillet%C3%A9.jpg
---
##  ![GRIWECH FEUILLETE dessert of Ramadan 2014](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/griwech-feuillet%C3%A9.jpg)

##  GRIWECH FEUILLETE, dessert of Ramadan 2014 

Hello everybody, 

Thanks to one of my readers Fatiha S. for this recipe of GRIWECH FEUILLETE dessert Ramadan 2014, she shares with us through the lines of my blog. 

These laminated griwech I like a lot, certainly there is a little work of shaping, but the result of griwech melt in the mouth, yum yum. For my part I will try this laminated griwech recipe as soon as possible, especially during Ramadan, because I really like them [ Algerian cakes with honey, ](<https://www.amourdecuisine.fr/gateaux-algeriens-au-miel>) to accompany my evening tea. 

**GRIWECH FEUILLETE**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/griwech-feuillet%C3%A9.jpg)

portions:  30  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * INGREDIENTS 
  * 1 kg of flour, 
  * 1 teaspoon salt 
  * 1 tablespoon of sugar, 
  * 1 sachet of baking powder, 
  * 1 tbsp (shaved) of baker's yeast, 
  * 250g of melted butter, 
  * 1 whole egg, lukewarm water to pick up the dough, 
  * cornflour 
  * 1.5 kg of honey, 
  * sesame seeds. 



**Realization steps**

  1. Put all the solid ingredients (except cornflour and sesame seeds) in a gasaâ (or large salad bowl) and mix. 
  2. Then pour the cooled melted butter and sand longly between the hands. Add the beaten egg, mix; 
  3. and then add warm water gradually. 
  4. Form a ball of dough (a little dry because we will pass it to the dough machine) and well knead like bread. 
  5. Let stand for 2 hours (and ideally 4 hours) in food film, freezer bag, or plastic bag. 
  6. After resting, take the dough and degas it. 
  7. Cut pieces. Take a piece of dough and put the rest in the freezer bag. 
  8. Sprinkle the work surface with cornflour and spread the dough with a rolling pin. 
  9. Then pass it to the machine; first, at number 1. Then fold the dough in three and iron it at number 1 a second time. Then pass it to the number 2, then directly to the number 3 and finally to the number 4. We obtain a fairly fine band of dough. 
  10. Train the GRIWECH with a roulette wheel and place them on a tray. 
  11. Continue the same process for the rest of the dough. 
  12. When all GRIWECH are formed, fry in hot oil (not hot). 
  13. Finish frying all the cakes. 
  14. Warm the honey and dip the griwech in it. Allow to absorb honey for 2-3 minutes. 
  15. Sprinkle with sesame seeds. 


