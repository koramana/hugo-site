---
title: onion galette, chilli and oregano
date: '2014-02-06'
categories:
- Algerian cuisine
- Moroccan cuisine
- Cuisine by country
- Tunisian cuisine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/09/galettes-ciboulettes-7_thumb.jpg
---
[ ![ciboulettes cakes 7](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/galettes-ciboulettes-7_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/07/galettes-ciboulettes-7.jpg>)

Hello everybody, 

I'm still in England for that, if I went on holiday, but I'm super busy, I forget to post recipes, but my dear lunetoiles is always there to remind me, hihihihi, and here is a new recipe that I share with you. 

so here is how Lunetoiles made these delicious cookies: 

I did according to my desires, we can very well replace the oregano with basil or mint, or associated both!   
I give you my recipe, for 2 big cakes or 3 medium ones: 

[ ![onion pancakes](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/galettes-aux-oignons_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/07/galettes-aux-oignons_2.jpg>)   


**onion galette, chilli and oregano**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/galette-ciboulettes-2_thumb.jpg)

Cooked:  Algerian  Recipe type:  Bread  portions:  4  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 2 large bowls of semolina 
  * 1 large bowl of fine semolina 
  * 1 tablespoon of salt 
  * 1 coffee cup filled with olive oil 
  * 3 green onions 
  * 1 tablespoon fresh minced oregano 
  * 1 teaspoon red pepper flakes 
  * Water 



**Realization steps**

  1. pour in a large dish the semolina. Mix with your fingers. 
  2. In a robot, put onions, oregano, chilli, salt and oil, mix. 
  3. Pour on the semolina. Mix well together and sand between the fingers. 
  4. Add water little by little while kneading until you get a ball of dough, which does not stick to the fingers. 
  5. Knead the dough well. 
  6. Divide into two or three equal balls. 
  7. Heat a tagine or crepe maker. 
  8. Spread a ball of dough on a worktop using a roll in a large slab if you like it thin, or otherwise spread it a little less, to have a thick and melting slab. 
  9. Transfer it to your tajine or crepe maker, and prick the whole surface with a fork 
  10. Cook on each side, watching, until the whole surface is golden brown. 
  11. Do the same for the other ball of dough. Keep the patties warm, then put in a cloth to keep them 



[ ![cake chives 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/galette-ciboulettes-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/07/galette-ciboulettes-2.jpg>)

Thanks for your comments and your visits. 

good night. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
