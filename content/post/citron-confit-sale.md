---
title: lemon confit salty
date: '2017-04-12'
categories:
- Moroccan cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/citron-confit-009.CR2_1.jpg
---
![lemon confit salty](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/citron-confit-009.CR2_1.jpg)

##  Salted confit lemon 

Hello everybody, 

Lemon confit salted: Moroccan cuisine is a cuisine rich in flavor, and well varied, and one of the most important ingredients for making delicious Moroccan recipes, is the lemon confit .... 

I had the chance to taste several dishes containing lemon confit, and I tell you, the recipe is totally different if you make it without this lemon ... all that to tell you, how much this ingredient can make the difference taste and flavors of a dish. 

the recipe for candied lemon is very simple and easy, you just need to prepare it well in advance, like all the products you keep. 

You can see on my blog, the recipe of [ Tajine Of Chicken In Candied Lemon ](<https://www.amourdecuisine.fr/article-tajine-de-poulet-au-citron-confit-119652651.html> "Tajine Of Chicken In Candied Lemon") , which is a super delicious dish, full of flavors and aromas 

**lemon confit salty**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/citron-confit-003.CR2_1.jpg)

Recipe type:  preserve  portions:  8  Prep time:  20 mins  cooking:  5 mins  total:  25 mins 

**Ingredients**

  * Lemon 
  * Salt 
  * Boiling water 
  * oil 



**Realization steps**

  1. wash the lemons and drain them. 
  2. with a knife, make incisions in each lemon by cutting it in four without reaching the end. 
  3. fill in the salt incisions. 
  4. Put the lemons in a glass jar 
  5. fill the jar with boiling water and finish with a drizzle of oil 
  6. Close the jar, place it in a place away from light. 
  7. shake every 5 to 6 days 
  8. candied lemons will be ready after almost 1 month 



{{< youtube t7u >}} 
