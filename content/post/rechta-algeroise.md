---
title: Algerian rechta
date: '2011-12-05'
categories:
- Mina el forn
- bread, traditional bread, cake

---
& Nbsp; hello everyone, and here is my delicious homemade rechta with a very good white sauce turnip taste, I do not tell you the delight, after a day of fasting, it is a dish that is only savored. so as I told you yesterday, I woke up early in the morning to prepare it, and my return shopping, I prepare our ftour, and especially the sauce for this very good dish, and I mention, that although it's pasta, but this dish is very light, I do not speak "light" to say & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.23  (  2  ratings)  0 

Hello everybody, 

and here is my delicious [ rechta  homemade ](<https://www.amourdecuisine.fr/article-rechta-fait-maison-pour-achoura-91125814.html>) with a very good white sauce turnip taste, I do not tell you the delight, after a day of fasting, it is a dish that is only savored. 

so as I told you yesterday, I woke up early in the morning to prepare it, and my return shopping, I prepare our ftour, and especially the sauce for this very good dish, and I mention, that although it is pasta, but this dish is very light, I do not speak "light" to say "diet" but light to say that it is tasted more easily than a chekhchoukhat dfar, or even chakhchoukha biskria, finally, remains that is my opinion, despite that I like both dishes. 

ingredients: 

  * chicken legs, or pieces of meat 
  * 1 onion 
  * 3 tablespoons of oil 
  * salt, pepper, 1 stick of cinnamon 
  * precooked soaked chickpeas 
  * 200 g white turnips 
  * 2 small zucchini 
  * 1 kg of homemade rechta 



method of preparation: 

  1. In a couscous maker, prepare the sauce with chopped chicken, grated onion, salt, pepper, cinnamon, and oil. 
  2. Make return 2 to 3 minutes. Add 2 liters of water and the chickpeas. 
  3. As soon as the sauce begins to boil, go to steam (top of the cuckoo) the fresh rechta. 
  4. after 15 min, remove the rechta, pour it in a big salad bowl, sprinkle it with a little sauce that is being cooked 
  5. open between your fingers, and coat with a little oil 
  6. still wear the recha with evaporation. 
  7. When the chicken is cooked, remove it from the sauce. Book. 
  8. Add to the sauce turnips and zucchini cut in half lengthwise. Let it cook 
  9. Serve the drizzle drizzled with sauce, and garnished with vegetables, chick peas and chicken pieces 
  10. present with a little cinnamon powder sprinkled on top 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
