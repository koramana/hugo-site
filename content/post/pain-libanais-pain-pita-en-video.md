---
title: Lebanese bread, pita bread video
date: '2016-09-29'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/Pain-libanais-pain-pita.CR2_2.jpg
---
![Lebanese bread - bread-pita.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/Pain-libanais-pain-pita.CR2_2.jpg)

##  Lebanese bread, pita bread video 

Hello everybody, 

Do you like homemade breads? me too, and I can tell you that Lebanese bread, known as pita bread, and the bread I often make to my children when they want to eat sandwiches. 

You'll tell me why ?, I'll tell you that in addition to being delicious and can be stuffed without problems, it also among the easiest breads to achieve, not even 3 minutes of kneading, 30 minutes of rest , 5 minutes of cooking, and here are your ready buns .... 

**Lebanese bread, pita bread video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-libanais-pain-pita.CR2-0011.jpg)

portions:  6  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 200 gr of white flour 
  * 200 gr of whole wheat flour. 
  * 1.5 tablespoons of salt 
  * 1.5 tablespoons of sugar 
  * 3 bags of instant dry yeast (28 gr) 
  * 5 to 6 tablespoons of olive oil 
  * lukewarm water. 



**Realization steps**

  1. you can see the video it's more detailed 



{{< youtube eNv >}} 

recipe tested and approved by:   
  
<table>  
<tr>  
<td>

[ ![pita breads](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pains-pitas-150x133.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pains-pitas.jpg>)

at Nesrine Sissou 


</td>  
<td>

[ ![pita bread at Naila's](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-pita-chez-Naila-150x84.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-pita-chez-Naila.jpg>)

at Nail 


</td>  
<td>

[ ![pita bread khadija](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-pita-khadidja1-150x113.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-pita-khadidja1.jpg>)

at Khadidja 


</td> </tr>  
<tr>  
<td>

[ ![pita bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-pita-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-pita.jpg>)

at Wamani Merou 


</td>  
<td>


</td>  
<td>


</td> </tr> </table>
