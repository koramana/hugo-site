---
title: melting chicken skewers
date: '2015-07-21'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/brochettes-de-poulet.jpg
---
[ ![chicken skewers](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/brochettes-de-poulet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/brochettes-de-poulet.jpg>)

##  chicken skewers 

Hello everybody, 

I am in full [ diet ](<https://www.amourdecuisine.fr/recettes-speciales-regime-et-ww>) , I did not tell you that, hihihiih, I am a new method of diet: herbalife (even if the site is in English, but the girl who holds it is a French, and she can help you and orient, and even you provide the products you need), with milkshake rich in fiber and protein. 

For a meal, I have to either eat fish, and [ vegetables ](<https://www.amourdecuisine.fr/plats-vegetariens>) , or then chicken breast and vegetables, my method for vegetables, well that's [ soups and velvety soups ](<https://www.amourdecuisine.fr/categorie-10678936.html>) , and the revenue coming from [ soups ](<https://www.amourdecuisine.fr/soupes-et-veloutes>) will please you. so today to change soups, I prepared these [ skewers ](<https://www.amourdecuisine.fr/article-panier-pique-nique-102297629.html>) , yes, there is chicken breast and vegetables, so I did not miss anything, hihihiih 

easy, simple, a knack, and it's done, a pity that I do not have the plancha, or barbecue otherwise these small grills would know even more delicious.   


**chicken skewers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/brochettes-de-poulet1.jpg)

portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * a chicken breast 
  * 2 medium zucchini 
  * 3 tomatoes 
  * black pepper, salt, coriander powder, garlic powder 
  * a drizzle of olive oil 



**Realization steps**

  1. clean the chicken breast and cut it into cubes 
  2. Season with salt of black pepper, coriander and garlic powder, and leave aside 
  3. cut the zucchini into rounds, and season chicken breasts the same way 
  4. cut the tomatoes in quarters, and season as well. 
  5. take the skewers, and alternately prick a slice of tomato, a slice of zucchini, and the cube of chicken breast 
  6. water the skewers with a drizzle of olive oil 
  7. grill each side, and introduce with a fresh salad, for my part it was with the Greek salad 


