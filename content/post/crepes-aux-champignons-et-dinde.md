---
title: mushroom and turkey crepes
date: '2014-08-22'
categories:
- crepes, waffles, fritters

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/crepes-aux-champignons-et-dinde-2_thumb.jpg
---
##  mushroom and turkey crepes 

Hello everybody, 

Want some light and fast meal, here is one, and that it must be good, these pancakes stuffed with mushrooms and turkey that my dear Lunetoiles and once again, to love to share with us. 

This recipe is ideal, especially if you have the rest of a turkey baked ... You can also do it with the rest of chicken, which I myself did once, and frankly it was too good. 

**mushroom and turkey crepes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/crepes-aux-champignons-et-dinde-2_thumb.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 20 g of butter 
  * 300 g fresh mushrooms cut into small pieces 
  * 300 g sliced ​​turkey slices 
  * salt and pepper 
  * 2 tablespoons fresh cream 
  * 2 teaspoons chopped parsley 
  * 6 [ salty pancakes, or natures ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee.html>)
  * 1 egg 



**Realization steps**

  1. In a frying pan over low heat, melt the butter. 
  2. Add the mushrooms and fry for 8 minutes until golden brown and add the turkey pieces. 
  3. Salt, pepper and stir. 
  4. Pour the cream and mix gently for 5 minutes. 
  5. Sprinkle with 1 tsp. chopped parsley, stir and remove from heat. 
  6. Spread a pancake, place in center 1 tbsp. at the coffee of the mixture then fold down the four sides to form a rectangle. 
  7. Arrange in an oiled dish and continue for other pancakes. 
  8. In a small bowl, mix the egg with 1 tbsp minced parsley, salt and pepper. 
  9. Brush the pancakes with. 
  10. Bake in a preheated oven at 180 ° C for 15 minutes until gilding. 
  11. Serve hot. 



et pour plus de recettes aux crepes, cliquez sur la photos 
