---
title: gruyere twists
date: '2011-05-23'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/05/220920701.jpg
---
![twists3](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/220920701.jpg)

it is a recipe for children, delicious crispy with gruyere, so very good for the development of your child, and also presentable in a buffet, because it can be accompanied by salad, mayonnaise, ketchup, try you will love. 

Ingredients: 

  * 85 gr of complete flour 
  * 40 gr of butter or margarine 
  * 55 gr finely grated Gruyère cheese 
  * 1/2 cac of ground mustard seeds (I did not have any) 
  * 1 beaten egg 
  * 1 pinch of salt (if your butter is not salted) 



mix the flour with the butter, to mix well into each other, add the grated gruyere, and the mustard seeds, and wet the dough with enough beaten egg to have a homogeneous and manageable dough. 

spread the dough on a floured surface, brush the surface with the rest of the beaten egg, and cut the dough into small ribbons 

shape your ribbons, and lay them on a baking sheet. 

cook your twists in a preheated oven at 180 degrees C, hanging 12 to 15 minutes, or until you have a nice golden color. 

it's twist are rich in calcium, which is great for a kid. and even you 

bon appétit 
