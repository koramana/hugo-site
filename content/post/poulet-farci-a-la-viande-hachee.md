---
title: stuffed chicken with chopped meat
date: '2012-04-16'
categories:
- cuisine algerienne
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/boudins-de-poulet-farci-017_thumb1.jpg
---
##  stuffed chicken with chopped meat 

Hello everybody, 

When I have a chicken steak at home, this chopped meat chicken recipe is the first thing I think about. 

This stuffed chicken meat dish is not only super easy to make, it's a very delicious dish, to accompany with a good salad, french fries or [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html>) and voila 

you can also find the recipe from [ tandoori chicken ](<https://www.amourdecuisine.fr/article-poulet-tandoori-85541703.html>) , [ Chicken Tikka ](<https://www.amourdecuisine.fr/article-poulet-tikka-99061936.html>) , [ kfc chicken ](<https://www.amourdecuisine.fr/article-poulet-kentucky-103449536.html>) , and full full ...   


**stuffed chicken with chopped meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/boudins-de-poulet-farci-017_thumb1.jpg)

**Ingredients**

  * 4 chicken steaks 
  * 150g minced meat 
  * 3 cloves of garlic 
  * Salt pepper 
  * some branches of parsley 
  * olive oil 
  * 2 hard boiled eggs 
  * 4 slices of cheese special burger, or then gruyère (optional) 
  * coriander powder 



**Realization steps**

  1. grated a clove of garlic, and season your chicken steaks with 
  2. add a little salt and black pepper, and leave aside 
  3. chop the meat with 2 cloves of garlic, and parsley, otherwise if the meat is already chopped, add chopped parsley and crushed garlic 
  4. add a little salt, black pepper and coriander 
  5. knead well, and set aside 
  6. now on your worktop, covered with cling film, place the chicken steaks (with 2 steaks you will make a pudding) 
  7. place the minced meat on it without covering the whole steak. 
  8. place the hard-boiled eggs in 4 
  9. cover with cheese 
  10. close the meat to form a pudding 
  11. cover this meat pudding with the chicken steak 
  12. wrap the chicken pudding in the cling film and roll it tightly 
  13. Cook in boiling water for about 5 minutes on each side. 
  14. drain from this water and gently remove the cling film 
  15. place the chicken sausages in a baking dish 
  16. oil, and place in an oven preheat to 200 degrees C. 
  17. watch the cooking, wipe each time the rod, with a brush, with the oil that escapes into the mold 
  18. when the pudding takes a nice color, take it out of the oven 
  19. cut medium slices with an electric knife if possible 
  20. you can introduce it with a nice fresh salad, and baked apples, or mashed potatoes 
  21. recover the baking sauce from the pudding, adding a little water. 



Thank you for your visit and comments 

bonne journée 
