---
title: Far Breton with prunes
date: '2014-12-25'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/far-breton-028.CR2_thumb1.jpg
---
![far breton 028.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/far-breton-028.CR2_thumb1.jpg)

##  Far Breton with prunes 

Hello everybody, 

Here is a dessert from Brittany, a classic timeless: **Far Breton with prunes** , A recipe that looks a lot like [ clafoutis ](<https://www.amourdecuisine.fr/article-clafoutis-aux-cerises-108444016.html>) , and [ custard pastry ](<https://www.amourdecuisine.fr/article-flan-patissier-100019503.html>) . It must be said that during my research on the net, I did not find at least 2 recipes that are alike ... Then I went to draw in a  Women's magazine  I bought on my first trip to France (the magazine dates from 2001). There was a nice picture of far Breton (shame without the name of the pastry chef) 

The good thing too, I came across dried pitted prunes in Lidl, I did not even know we could find the pitted, they are super tender, and the recipe was a real success with it. 

**Far Breton with prunes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/far-breton-aux-prunes-023.CR2_thumb1.jpg)

portions:  8  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 4 eggs 
  * 120 g sugar 
  * 120 g flour 
  * 500 ml milk 
  * a coffee of vanilla 
  * 60 g of butter 
  * 1 pinch of salt 
  * twenty pitted dried prunes (and it depends on your taste, if you want a lot of prunes in your far or not) 
  * orange juice, or the, (instead of rum, as on my books) 



**Realization steps**

  1. macerate prunes in warm orange juice, or warm tea at least 30 minutes before starting your recipe. 
  2. Preheat your oven to 180 ° C. 
  3. in a bowl, lightly whisk the eggs, with the sugar and the pinch of salt 
  4. gently stir in the flour while mixing. 
  5. add the lukewarm milk, little by little to have a homogeneous mixture. 
  6. stir in lukewarm melted butter. 
  7. in a buttered baking tin, pyrex mold, or gratin mold of 25 out of 30, or two average molds of almost 15 out of 20. place the drained prunes. 
  8. gently pour the egg mixture, without moving the prunes. 
  9. cook for 40 to 45 minutes, or depending on your oven, until you have a nice golden color. 
  10. remove from the oven, allow to cool to room temperature then refrigerate 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
