---
title: mchawek with almonds, Algerian cake without gluten
date: '2014-10-02'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
tags:
- Dry Cake
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/mchewek-gateau-algerien-020_thumb-300x224.jpg
---
##  mchawek with almonds, Algerian cake without gluten 

Hello everybody, 

The **mchawek** or **Mchewek** is a typical pastry of the city of Algiers. Its name means in slang Algiers "full of spines" because of the decoration of the almonds effillees. a gluten-free cake from the family of macaroons, very delicious and well-melted. 

**mchawek with almonds, Algerian cake without gluten**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/mchewek-gateau-algerien-020_thumb-300x224.jpg)

portions:  40  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 3 measures ground ground almonds 
  * 1 measure of fine sugar 
  * Grated zest of 2 lemons 
  * 1 handful of flaked almonds 
  * Bigarreau cherries 
  * 3 to 4 eggs 



**Realization steps**

  1. Mix the finely ground almonds with the sugar and grated rind of the lemon. 
  2. Add the eggs one after the other, kneading until smooth and even. 
  3. Let stand 15 minutes. 
  4. Form the balls the size of a walnut, dip them one by one into the egg white and roll each ball into the flaked almonds to coat them. 
  5. Garnish the middle of each cake with a half cherry bigarreau. 
  6. Arrange the cakes on a buttered baking sheet and bake in a preheated oven at 180 ° C, cook for 10 minutes. 
  7. When the cakes are pink below these are cooked. 
  8. after cooking, the cakes are fragile so allow to cool before unmolding.   
Good tasting. 


