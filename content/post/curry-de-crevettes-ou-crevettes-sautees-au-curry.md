---
title: Shrimp curry or curry shrimps
date: '2014-03-30'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-de-crevettes-007.CR2_.jpg
---
[ ![Shrimp curry 007.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-de-crevettes-007.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-de-crevettes-007.CR2_.jpg>)

Hello everybody, 

Yesterday at noon, I found myself alone at home, after the release of my husband and children, I said to myself I'm just going to make eggs on flat for lunch, but we opened the fridge, I found these beautiful shrimp that made me an eye ... 

I immediately think about a sauteed shrimp curry ... No time to waste, I start to be hungry, my baby is sleeping, so quickly I take my wok and we start:    


**Shrimp curry or curry shrimps**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-de-crevettes-007.CR2_.jpg)

Cooked:  Indian  Recipe type:  Curry  portions:  2  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * 300 g unshelled shrimp 
  * 1 onions 
  * 2 cloves garlic 
  * 1 cm fresh ginger   
I still have grated ginger cube to the freezer 
  * ¼ bunch of fresh coriander. 
  * 1 glass of water 
  * 100 g fresh tomatoes 
  * 1 clove of cardamom 
  * 1 teaspoon of curry powder 
  * 1 tablespoon of curry leaf 
  * 1 pinch of cinnamon 
  * 1 pinch of grated nutmeg 
  * 3 tablespoons fresh cream 
  * 1 tablespoon of coconut powder. 
  * ½ teaspoon of lemon juice 
  * Salt pepper 
  * 3 tablespoons of olive oil or coconut oil 



**Realization steps**

  1. Wash, peel onions and garlic and mix. 
  2. Peel the tomatoes, seed them and cut them in small ones. 
  3. Heat the oil in a wok and brown the shrimp over high heat. 
  4. Pepper them and reserve. 
  5. brown the onion and garlic and add the tomatoes. Salt slightly. Add the cardamom seeds, the two curry, the cinnamon, the nutmeg, the ginger and the fresh cream. 
  6. Add lemon juice and water. Cook uncovered until the sauce is reduced. 
  7. Put the shrimp back in the sauce, let it simmer a little and remove from the heat 
  8. you can serve with rice, fries, or just with [ Naan bread ](<https://www.amourdecuisine.fr/article-les-naans-a-la-coriandre.html> "naans, indian bread") . 



[ ![Shrimp curry 014.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-de-crevettes-014.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-de-crevettes-014.CR2_.jpg>)
