---
title: coral lentil soup with sweet potato
date: '2016-02-12'
categories:
- soups and velvets
tags:
- Healthy cuisine
- Algeria
- Full Dishes
- Easy cooking
- Fast Food
- India
- vegetarian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/soupe-de-lentilles-corail-a-la-patate-douce-1.jpg
---
[ ![Coral lentil soup with sweet potato 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/soupe-de-lentilles-corail-a-la-patate-douce-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/soupe-de-lentilles-corail-a-la-patate-douce-1.jpg>)

##  coral lentil soup with sweet potato 

Hello everybody, 

We love lentils at home, for me it's the ideal dish when I'm short of ideas, when I do not have meat at home, when it's really late and I have to make a dish which is good, delicious and hearty. 

This was the case with this soup with coral lentils and sweet potato. preparing to go to London, a hand to make the [ birthday cake ](<https://www.amourdecuisine.fr/article-gateau-danniversaire-herisson-au-chocolat.html>) of my son, and the cleaning ... It was 11:40 and I had not made food yet, I did not have the meat, because we were going to travel, I did not want to leave meat in the fridge. I look here and there, I had half an onion, half a leek, a sweet potato, a celery stalk ... What else? ah an open bag of coral lentils ... in less than 40 minutes the dish was ready, the kids just had to wait while I took pictures, hahahaha. 

**coral lentil soup with sweet potato**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/soupe-de-lentilles-corail-a-la-patate-douce-2.jpg)

portions:  5  Prep time:  10 mins  cooking:  40 mins  total:  50 mins 

**Ingredients**

  * 1 glass of coral lentils 
  * ½ onion, finely chopped 
  * 2 crushed garlic cloves 
  * 1 celery stalks, finely chopped 
  * ½ leek, finely chopped 
  * 1 sweet potato cube 
  * a little grated ginger 
  * 1 C. ground cumin 
  * ½ c. harissa's coffee 
  * 1 C. paprika 
  * ½ c. black pepper 
  * 1 tablespoon of canned tomato 
  * 2 liters of vegetable broth (if not a cube of broth diluted with boiling water) 
  * salt 
  * 2 tablespoons of olive oil 
  * 1 little plain yoghurt and fresh parsley to decorate. 



**Realization steps**

  1. put the oil in a pot over medium heat. 
  2. Add the onion, leek, celery (all cut into small cubes) and sauté until they become well melted. 
  3. Add lentils, sweet potato, garlic, ginger, cumin, paprika, black pepper and canned tomato and stir to mix evenly. 
  4. Add the vegetable broth and the harissa. 
  5. Bring to a boil, then lower the heat and simmer for about 40 minutes until the lentils are tender. Season with salt and black pepper to taste. 
  6. Serve with a spoonful of plain yogurt and some chopped fresh parsley. 



[ ![coral lentil soup with sweet potato](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/soupe-de-lentilles-corail-a-la-patate-douce.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/soupe-de-lentilles-corail-a-la-patate-douce.jpg>)
