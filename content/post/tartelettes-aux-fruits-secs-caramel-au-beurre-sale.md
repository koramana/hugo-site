---
title: Tartlets with dried fruit and salted butter caramel
date: '2016-08-09'
categories:
- gateaux algeriens au miel
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tartelettes-aux-fruits-secs-caramel-au-beurre-sal%C3%A9-1.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tartelettes-aux-fruits-secs-caramel-au-beurre-sal%C3%A9-1.jpg)

##  Tartlets with dried fruit and salted butter caramel 

Hello everybody, 

Although I'm on vacation and I try to get away as much as possible from the blog and computer, the emails I receive daily and dozens do not leave me indifferent. Many of you asked me for recipes from [ Algerian cakes from Eid 2016 ](<https://www.amourdecuisine.fr/article-gateaux-algeriens-aid-el-fitr-2016.html>) which I promised to share soon. 

In any case, luckily I had signed the pictures of the tarts. Such delicious and crunchy cakes are full of flavor and taste. I often made my tarts with simple homemade caramel, but since I made these tartlets it has become the most requested recipe by my husband, he finds that the taste of [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html>) and a thousand times better and it's less sweet than ordinary caramel. 

The thing that will take the most time is the preparation of tartlets, I realize them a little more in advance, sometimes I do a week in advance and I keep in a box Hermitique and as soon as I want these tarts I just prepare the quantity that we want to taste. Even if I have surprise guests, I immediately grill the dried fruit in the oven, I let cool a little before coating them in salted butter caramel (I always have a jar in reserve) and here it is is ready to serve in all pride 

![tarts with dried fruit salted butter caramel 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tartelettes-aux-fruits-secs-caramel-au-beurre-sal%C3%A9-3.jpg)

**Tartlets with dried fruit and salted butter caramel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tartelettes-aux-fruits-secs-caramel-au-beurre-sal%C3%A9-6.jpg)

portions:  50  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** For tartlets: 

  * 200 g flour 
  * 110 g of butter 
  * 65 gr of icing sugar 
  * 2 egg yolks 
  * 1 packet of vanilla 

for the stuffing: 
  * crushed nuts 
  * grilled pistachios 
  * crushed toasted almonds 
  * grilled hazelnuts crushed. 

decoration: 
  * [ Salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html>)
  * silver beads 



**Realization steps** for the dough: 

  1. beat the butter with the sugar in the ointment; 
  2. add egg yolks, vanilla and pick up with flour (depending on the flour of the times I use + or - of the given quantity 
  3. form a ball without over-kneading, cover with cling film and chill. 
  4. then spread the dough to a thickness of almost 5 mm and fill the tart molds. 
  5. Pierce on the toothpick and bake in a preheated oven at 180 ° C 
  6. let cool a little before unmolding. 
  7. prepare it [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html>)

preparation of the stuffing: 
  1. toast the crushed dried fruits in the oven for a few minutes. 
  2. let cool and mix with salted butter caramel. 
  3. fill the tartlets with a teaspoon of the stuffing. 
  4. decorate with some remaining salted butter caramel and silver pearls. 



![tartlets with dried fruit salted butter caramel 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tartelettes-aux-fruits-secs-caramel-au-beurre-sal%C3%A9-5.jpg)
