---
title: waffles Liégeoises
date: '2016-02-15'
categories:
- crepes, donuts, donuts, sweet waffles
tags:
- Delicacies
- Easy cooking
- Fast Food
- Pastry
- To taste
- Home made
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gaufres-liegeoises-3.jpg
---
![waffles from Liège 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gaufres-liegeoises-3.jpg)

##  waffles Liégeoises 

Hello everybody, 

Well, what do I like about waffles, I like them all hot and crisp, sometimes after cooking I put them in the toaster, just to have more crunch. My son, when he loves them soft, hihihihi, he makes me laugh frankly, but each his taste is not it. 

This morning my children asked me for the Liège waffles, and I was super happy that there was sunshine, despite the fact that it was very cold: 2 degrees C. So I could take pictures to share with you this beautiful greediness. I quickly made the dough, let a little rest and cooking, I gave the children their breakfast, and I started my photo session before the sun disappears, hihihihi. 

**waffles from Liège**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gaufres-liegeoises-2.jpg)

portions:  18  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 375 grams of flour 
  * 135 grams of warm milk 
  * 35 gr of fresh yeast (I put 1 cup of dehydrated yeast instead) 
  * 2 eggs 
  * 1 pinch of salt 
  * 1 sachet of vanilla sugar 
  * 200 gr of butter 
  * 250 gr of pearl sugar (I had not just replaced by 200 gr of sugar) 



**Realization steps**

  1. In a salad bowl, put the milk and butter. 
  2. Heat this liquid until the butter is melted. Book. 
  3. Put the flour, the pinch of salt, the vanilla sugar and the yeast in your robot or in another bowl, mix. 
  4. Add the 2 eggs to the liquid mixture, beat. 
  5. Finally, add the liquid mixture to the flour, 
  6. Let stand 30 minutes 
  7. and take directly from the salad bowl dumplings, 
  8. place them in the waffle iron and cook until you have a nice color 



[ ![waffles from Liège 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gaufres-liegeoises-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gaufres-liegeoises-4.jpg>)
