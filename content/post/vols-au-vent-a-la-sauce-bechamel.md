---
title: Windward flights with bechamel sauce
date: '2013-11-13'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/06/volovent-011_thumb.jpg
---
![volovent 011](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/volovent-011_thumb.jpg)

##  **Windward flights with bechamel sauce**

Hello everybody, 

I always like to present on my table to accompany the main course, appetizers, pizza, or a small entree. 

sometimes I prepare [ tuna cones ](<https://www.amourdecuisine.fr/article-cornets-sales-a-la-creme-au-thon-49023773.html>) , sometimes times [ a quiche ](<https://www.amourdecuisine.fr/article-mini-quiches-aux-carottes-84166550.html>) sometimes times like [ squares with spinach ](<https://www.amourdecuisine.fr/article-carres-epinards-a-la-bechamel-47124888.html>) , [ spinach slippers ](<https://www.amourdecuisine.fr/article-chausson-aux-epinards-73387059.html>) , or then [ tuna pasties ](<https://www.amourdecuisine.fr/article-minis-feuilletes-minis-pasties-thon-fromage-olives-67293676.html>) ... the choice is great. 

this time, for today's lunch and because I had leftovers of yesterday's dishes, I added these delicious flights to the béchamel sauce, and that was super good delicious, in addition very easy to to do, especially if you have the pate to flights to the wind ready, 

**Windward flights with bechamel sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/volovent-017_thumb.jpg)

Recipe type:  entrance, amuses mouths  portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * flying coins in the wind (12 for me) 
  * Bechamel sauce (recipe here) 
  * 200 g fresh or canned mushrooms 
  * The quantity of green olives pitted and cut 
  * 100 g cooked shrimp 
  * 2 tablespoons chopped green onions 
  * the juice of half a lemon 
  * 2 tbsp. chopped parsley 
  * Salt according to taste 
  * Black pepper 
  * a little nutmeg 
  * 200 g grated cheese 
  * 2 tbsp. tablespoon of olive oil 



**Realization steps**

  1. Start by cooking the vol au vent, stuffed with egg yolk, in a hot oven at 170 degrees C. let cool. 
  2. In a frying pan over high heat put the oil and fry the mushrooms until it is a beautiful gilding, Add the lemon juice, parsley, salt and black pepper and set aside. 
  3. on the other hand put the onions and garlic on the stove or you cooked the mushrooms then add the shrimp already cooked too, then add the other ingredients and a little Bechamel sauce, a little cheese and the olives cut in slices. 
  4. Fill the flights with this stuffing, and garnish with a little grated cheese, and put in the oven to brown for 5 min. 



[ ![volovent 017](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/volovent-017_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/06/volovent-017.jpg>)

bonne degustation. 
