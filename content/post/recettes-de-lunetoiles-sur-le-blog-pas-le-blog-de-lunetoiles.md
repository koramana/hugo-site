---
title: recipes of Lunetoiles on the blog / not the blog of Lunetoiles
date: '2013-02-16'
categories:
- juice and cocktail drinks without alcohol
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateaux-moelleux-aux-carambars1.jpg
---
cakes / cakes   
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateaux-moelleux-aux-carambars1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-fondant-aux-carambars-85029025.html>) [ Carambar cake ](<https://www.amourdecuisine.fr/article-gateau-fondant-aux-carambars-85029025.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/mini-mouskoutchou-a-l-orange-13_21.jpg) ](<https://www.amourdecuisine.fr/article-minis-mouskoutchou-a-l-orange-112244829.html>)   
[ Minis mouskoutchous with orange ](<https://www.amourdecuisine.fr/article-minis-mouskoutchou-a-l-orange-112244829.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cake-marbre-a-la-pate-a-tartiner-61.jpg) ](<https://www.amourdecuisine.fr/article-cake-marbre-a-la-pate-a-tartiner-114502897.html>)   
[ Marbled cake with spread ](<https://www.amourdecuisine.fr/article-cake-marbre-a-la-pate-a-tartiner-114502897.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/moelleux-aux-figues-et-amandes-6_211.jpg) ](<https://www.amourdecuisine.fr/article-gateau-moelleux-figues-miel-et-amandes-111273676.html>)   
[ fluffy cake with figs, honey and almonds ](<https://www.amourdecuisine.fr/article-gateau-moelleux-figues-miel-et-amandes-111273676.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a398.idata.over-blog.com/2/42/48/75/API/2013-01/22/) ](<https://www.amourdecuisine.fr/article-cake-aux-pommes-114614250.html>) [ Cake with apples ](<https://www.amourdecuisine.fr/article-cake-aux-pommes-114614250.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/croquemitoufle-aux-bananes-21.jpg) ](<https://www.amourdecuisine.fr/article-croquemitoufle-aux-bananes-85028288.html>)   
[ Cr ](<https://www.amourdecuisine.fr/article-croquemitoufle-aux-bananes-85028288.html>) [ banana shake ](<https://www.amourdecuisine.fr/article-croquemitoufle-aux-bananes-85028288.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crumble-de-fraises_21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crumble-de-fraises_21.jpg>) [ Strawberry Bars / Crumb Strawberry Bars ](<https://www.amourdecuisine.fr/article-barres-aux-fraises---crumble-aux-fraises-103847602.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/200x134/2/42/48/75/API/2012-03/06/) ](<https://www.amourdecuisine.fr/article-gateau-aux-pommes-a-l-ancienne-100844326.html>) [ Old-fashioned apple cake ](<https://www.amourdecuisine.fr/article-gateau-aux-pommes-a-l-ancienne-100844326.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/200x134/2/42/48/75/API/2012-01/04/) ](<https://www.amourdecuisine.fr/article-galette-des-rois-praline-chocolat-114078374.html>) [ Galette des rois praline / chocolate ](<https://www.amourdecuisine.fr/article-galette-des-rois-praline-chocolat-114078374.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/streusel-cacao-et-des-noisettes-caram-lis-es11.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-streusel-cacao-et-noisettes-caramelisees-113196530.html>) [ Chocolate cake, cocoa streusel and caramelized hazelnuts ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-streusel-cacao-et-noisettes-caramelisees-113196530.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/Croquemitoufle-aux-cerises11.jpg) ](<https://www.amourdecuisine.fr/article-croquemitoufle-aux-cerises-62193634.html>) [ Cherry Croquemitoufle ](<https://www.amourdecuisine.fr/article-croquemitoufle-aux-cerises-62193634.html>) 
</td>  
<td>

[ ![lune1](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/lune1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-moelleux-au-citron-et-cranberry-98496922.html>)   
[ Fluffy Lemon and Cranberry Cake ](<https://www.amourdecuisine.fr/article-gateau-moelleux-au-citron-et-cranberry-98496922.html>) 
</td> </tr>  
<tr>  
<td>

[ ![cake with orange 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-a-l-orange-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25345557.html>)   
[ Cake with orange ](<https://www.amourdecuisine.fr/article-25345557.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/financiers-aux-myrtilles-3-11.jpg) ](<https://www.amourdecuisine.fr/article-fnanciers-coeur-aux-myrtilles-71704126.html>) [ Financial heart of blueberries ](<https://www.amourdecuisine.fr/article-fnanciers-coeur-aux-myrtilles-71704126.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://i21.servimg.com/u/f21/13/81/72/14/) ](<https://www.amourdecuisine.fr/article-les-mandises-82111334.html>) [ The Mandises ](<https://www.amourdecuisine.fr/article-les-mandises-82111334.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cake-a-l-orange1.jpg) ](<https://www.amourdecuisine.fr/article-cake-de-pierre-herme-a-l-orange-118221052.html>) [ Cake with Pierre Hermé orange ](<https://www.amourdecuisine.fr/article-cake-de-pierre-herme-a-l-orange-118221052.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/muffins-aux-abricots-et-framboises1.jpg) ](<https://www.amourdecuisine.fr/article-muffins-abricots-framboises-et-streusel-118173498.html>) [ Apricots Raspberry and Streusel Muffins ](<https://www.amourdecuisine.fr/article-muffins-abricots-framboises-et-streusel-118173498.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a395.idata.over-blog.com/555x381/2/42/48/75/API/2013-03/07/) ](<https://www.amourdecuisine.fr/article-financiers-au-chocolat-et-noisettes-recette-pour-gouter-118130892.html>) [ Chocolate and hazelnuts ](<https://www.amourdecuisine.fr/article-financiers-au-chocolat-et-noisettes-recette-pour-gouter-118130892.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/DSC004431.jpg) ](<https://www.amourdecuisine.fr/article-financiers-<br%20/><br%20/><br%20/>%0Aaux-fraises-117844160.html>) [ Strawberry financiers ](<https://www.amourdecuisine.fr/article-financiers-aux-fraises-117844160.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-mousseux-au-chocolat-sans-farine1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-mousseux-au-chocolat-sans-farine-117604127.html>) [ Flourless chocolate cake without flour ](<https://www.amourdecuisine.fr/article-gateau-mousseux-au-chocolat-sans-farine-117604127.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/muffins-chocolat-pralin-1_21.jpg) ](<https://www.amourdecuisine.fr/article-muffins-au-chocolat-praline-115855203.html>) [ Praline chocolate muffins ](<https://www.amourdecuisine.fr/article-muffins-au-chocolat-praline-115855203.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-aux-fruits-rouges-d-ete_21.jpg) ](<https://www.amourdecuisine.fr/article-gateau-aux-fruits-rouges-berry-summer-cake-115847894.html>) [ Red berry cake / Berry summer cake ](<https://www.amourdecuisine.fr/article-gateau-aux-fruits-rouges-berry-summer-cake-115847894.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/Fondant-au-chocolat-et-brisures-de-speculoos1.jpg) ](<https://www.amourdecuisine.fr/article-fondant-au-chocolat-et-brisures-de-speculoos-115568682.html>) [ Chocolate fondant and speculoos chips ](<https://www.amourdecuisine.fr/article-fondant-au-chocolat-et-brisures-de-speculoos-115568682.html>) 
</td>  
<td>


</td> </tr> </table>

the bavarois / entremets   
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/entremet-au-chocolat-au-lait-0011.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/entremet-au-chocolat-au-lait-0011.jpg>)   
[ milk chocolate dessert ](<https://www.amourdecuisine.fr/article-entremet-au-chocolat-au-lait-de-pierre-herme-97530078.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/entremet-Caramel-pommes-nougatine1.jpg) ](<https://www.amourdecuisine.fr/article-entremet-caramel-pommes-nougatine-67449626.html>)   
[ Entremet Caramel apples nougatine ](<https://www.amourdecuisine.fr/article-entremet-caramel-pommes-nougatine-67449626.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/royal-au-chocolat-2_21.jpg) ](<https://www.amourdecuisine.fr/article-royal-au-chocolat-dessert-des-fetes-de-fin-d-annee-113609866.html>)   
[ Royal chocolate ](<https://www.amourdecuisine.fr/article-royal-au-chocolat-dessert-des-fetes-de-fin-d-annee-113609866.html>) 
</td> </tr>  
<tr>  
<td>


</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/royal-au-chocolat-trianon-31.jpg) ](<https://www.amourdecuisine.fr/article-recette-trianon-recette-royal-au-chocolat-113763683.html>)   
[ Royal chocolate / Trianon ](<https://www.amourdecuisine.fr/article-recette-trianon-recette-royal-au-chocolat-113763683.html>) 
</td>  
<td>


</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a397.idata.over-blog.com/2/42/48/75/API/2012-04/26/) ](<https://www.amourdecuisine.fr/article-la-recette-du-fraisier-108418788.html>) [ strawberries tree ](<https://www.amourdecuisine.fr/article-la-recette-du-fraisier-108418788.html>) 
</td>  
<td>


</td>  
<td>


</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/bavarois-fraises-speculoos_21.jpg) ](<https://www.amourdecuisine.fr/article-bavarois-fraises-speculoos-70074240.html>) [ Bavarian speculoos strawberries ](<https://www.amourdecuisine.fr/article-bavarois-fraises-speculoos-70074240.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/bavarois-chocolat-vanille1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-black-and-white-103247493.html>) [ Noir et blanc ](<https://www.amourdecuisine.fr/article-gateau-black-and-white-103247493.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/bavarois-choc-poires111.jpg) ](<https://www.amourdecuisine.fr/article-bavarois-chocolat-poires-71935286.html>) [ Bavarian chocolate / pear ](<https://www.amourdecuisine.fr/article-bavarois-chocolat-poires-71935286.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://i65.servimg.com/u/f65/11/05/97/32/) ](<https://www.amourdecuisine.fr/article-bavarois-poire-vanille-58928958.html>) [ Bavarian pear / vanilla ](<https://www.amourdecuisine.fr/article-bavarois-poire-vanille-58928958.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/charlotte-aux-fraises-2_21.jpg) ](<https://www.amourdecuisine.fr/article-charlotte-aux-fraises-113111657.html>) [ Strawberry Charlotte ](<https://www.amourdecuisine.fr/article-charlotte-aux-fraises-113111657.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/fraisier2a_21.jpg) ](<https://www.amourdecuisine.fr/article-la-fraise-gateau-d-annivers<br%20/><br%20/><br%20/>%0Aaire-98108041.html>) [ The strawberry ](<https://www.amourdecuisine.fr/article-la-fraise-gateau-d-anniversaire-98108041.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-d-anniversaire-Ondul-s-aux-noisettes-1.jpg) ](<https://www.amourdecuisine.fr/article-ondules-noisettes-gateau-d-anniversaire-au-chocolat-103343738.html>) [ Wavy hazelnut ](<https://www.amourdecuisine.fr/article-ondules-noisettes-gateau-d-anniversaire-au-chocolat-103343738.html>) 
</td>  
<td>

[ ![daisy soissons14](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/p-querette-de-soissons14_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-paquerette-de-soissons-93219227.html>)   
[ Paquerette de Soissons ](<https://www.amourdecuisine.fr/article-paquerette-de-soissons-93219227.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tres-leches11.jpg) ](<https://www.amourdecuisine.fr/article-tres-leche-cake-a-la-chantilly-et-fruits-79680907.html>) [ Tres leche cake with whipped cream and fruits ](<https://www.amourdecuisine.fr/article-tres-leche-cake-a-la-chantilly-et-fruits-79680907.html>) 
</td> </tr> </table>

the pies   
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a404.idata.over-blog.com/2/42/48/75/API/2010-07/) ](<https://www.amourdecuisine.fr/article-pear-clafouti---tarte-aux-poires-54405438.html>)   
[ Pear Clafouti - Pear tart ](<https://www.amourdecuisine.fr/article-pear-clafouti---tarte-aux-poires-54405438.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-conversation-7_21.jpg) ](<https://www.amourdecuisine.fr/article-la-tarte-conversation-de-l-emission-le-meilleur-patissier-de-m6-112899949.html>)   
[ pie conversation ](<https://www.amourdecuisine.fr/article-la-tarte-conversation-de-l-emission-le-meilleur-patissier-de-m6-112899949.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://i21.servimg.com/u/f21/13/81/72/14/) ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-peches-85028616.html>)   
[ Almond peach pie ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-peches-85028616.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-nectarine-a-la-creme-11.jpg) ](<https://www.amourdecuisine.fr/article-tarte-aux-nectarines-a-la-creme-88034837.html>)   
[ Nectarine and cream pie ](<https://www.amourdecuisine.fr/article-tarte-aux-nectarines-a-la-creme-88034837.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a136.idata.over-blog.com/2/42/48/75/API/2011-08/) ](<https://www.amourdecuisine.fr/article-tarte-de-linz-au-chocolat-et-a-la-confiture-de-mures-maison-81695831.html>) [ Linz tart with chocolate and blackberry jam ](<https://www.amourdecuisine.fr/article-tarte-de-linz-au-chocolat-et-a-la-confiture-de-mures-maison-81695831.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-amandines-aux-fraises_21.jpg) ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-fraises-73876989.html>) [ Strawberry almond tart ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-fraises-73876989.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://i61.servimg.com/u/f61/13/81/72/14/) ](<https://www.amourdecuisine.fr/article-tarte-aux-fraises-et-a-la-creme-patissiere-58354661.html>) [ Strawberry and custard pie ](<https://www.amourdecuisine.fr/article-tarte-aux-fraises-et-a-la-creme-patissiere-58354661.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a401.idata.over-blog.com/2/42/48/75/API/2012-03/28/) ](<https://www.amourdecuisine.fr/article-tarte-aux-myrtilles-102396921.html>) [ Blueberry shortbread pie ](<https://www.amourdecuisine.fr/article-tarte-aux-myrtilles-102396921.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a398.idata.over-blog.com/2/42/48/75/API/2011-11/29/) ](<https://www.amourdecuisine.fr/article-tarte-normande-97115899.html>) [ Norman apple tart ](<https://www.amourdecuisine.fr/article-tarte-normande-97115899.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-aux-fraise2-a_21.jpg) ](<https://www.amourdecuisine.fr/article-tarte-sablee-aux-fraises-102110510.html>) [ Strawberry shortbread pie ](<https://www.amourdecuisine.fr/article-tarte-sablee-aux-fraises-102110510.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-amandine-poire-framboise-2_41.jpg) ](<https://www.amourdecuisine.fr/article-tarta-amandine-poires-framboises-81218958.html>) [ Raspberry pear amandine tart ](<https://www.amourdecuisine.fr/article-tarta-amandine-poires-framboises-81218958.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-aux-pommes-ancienne16a1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-anciennes-65550114.html>) [ Old apple pie ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-anciennes-65550114.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a398.idata.over-blog.com/2/42/48/75/API/2010-12/) ](<https://www.amourdecuisine.fr/article-paquerette-de-soissons-93219227.html>) [ Daisy of soissons ](<https://www.amourdecuisine.fr/article-paquerette-de-soissons-93219227.html>) 
</td>  
<td>

[ ![White Cheese Pie](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-au-fromage-blanc_thumb.jpg) ](<https://www.amourdecuisine.fr/article-tarte-au-fromage-blanc-kaskueche-90621607.html>) [ White cheese pie - (Käsküeche) ](<https://www.amourdecuisine.fr/article-tarte-au-fromage-blanc-kaskue<br%20/><br%20/><br%20/>%0Ache-90621607.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-abricot-noix-de-coco1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-abricots-noix-de-coco-85028794.html>) [ Apricot / coconut tart ](<https://www.amourdecuisine.fr/article-tarte-abricots-noix-de-coco-85028794.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a136.idata.over-blog.com/2/42/48/75/API/2013-02/08/) ](<https://www.amourdecuisine.fr/article-banoffee-pie-115147754.html>) [ Banoffee Pie ](<https://www.amourdecuisine.fr/article-banoffee-pie-115147754.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-amandine-aux-poires_21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-amandine-aux-poires_21.jpg>) [ Pear Almond Tart ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-amandine-aux-poires_21.jpg>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-a-la-rhubarbe-et-fraises-ultra-facile-copie-11.jpg) ](<https://www.amourdecuisine.fr/article-tarte-a-la-rhubarbe-et-fraises-ultra-facile-118331036.html>)   
[ Rhubarb and strawberry pie ](<https://www.amourdecuisine.fr/article-tarte-a-la-rhubarbe-et-fraises-ultra-facile-118331036.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-aux-fraises-sur-dacquoise-aux-amandes-et-citron1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-a-la-fraise-sur-dacquoise-aux-amandes-et-citron-117671269.html>) [ Strawberry tart on almond and lemon dacquoise ](<https://www.amourdecuisine.fr/article-tarte-a-la-fraise-sur-dacquoise-aux-amandes-et-citron-117671269.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-aux-fraises-Lunetoiles1.jpg) ](<https://www.amourdecuisine.fr/article-pie-aux-fraises-tarte-aux-fraises-confites-117539780.html>) [ Tartlets with candied strawberries ](<https://www.amourdecuisine.fr/article-pie-aux-fraises-tarte-aux-fraises-confites-117539780.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-fine-aux-pommes-41.jpg) ](<https://www.amourdecuisine.fr/article-tarte-fine-aux-pommes-117353071.html>) [ Apple tart ](<https://www.amourdecuisine.fr/article-tarte-fine-aux-pommes-117353071.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-flan-aux-pommes-11.jpg) ](<https://www.amourdecuisine.fr/article-tarte-flan-aux-pommes-117274851.html>) [ Apple pie cake ](<https://www.amourdecuisine.fr/article-tarte-flan-aux-pommes-117274851.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-macaron-fraises1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-fraises-macaron-117152632.html>) [ Macaron strawberry pie ](<https://www.amourdecuisine.fr/article-tarte-fraises-macaron-117152632.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-au-chocolat-et-fraises-1-copie-11.jpg) ](<https://www.amourdecuisine.fr/article-tarte-au-chocolat-et-fraises-115568499.html>) [ Strawberry chocolate pie ](<https://www.amourdecuisine.fr/article-tarte-au-chocolat-et-fraises-115568499.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-creme-brulee-aux-pommes-1-copie-11.jpg) ](<https://www.amourdecuisine.fr/article-tarte-a-la-creme-brulee-aux-pommes-116282929.html>) [ Apple crème brûlée pie ](<https://www.amourdecuisine.fr/article-tarte-a-la-creme-brulee-aux-pommes-116282929.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/linzer-torte-ala-confiture-de-framboise_21.jpg) ](<https://www.amourdecuisine.fr/article-linzer-torte-confiture-de-framboises-115568664.html>) [ Linzer torte / raspberry jam ](<https://www.amourdecuisine.fr/article-linzer-torte-confiture-de-framboises-115568664.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/TARTE-AUX-FRUITS-ROUGES-ET-A-LA-PISTACHE1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-aux-fruits-rouges-et-a-la-pistache-118521937.html>) [ Pie with red fruits and pistachio ](<https://www.amourdecuisine.fr/article-tarte-aux-fruits-rouges-et-a-la-pistache-118521937.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/Tarte-abricot-macaron-2-copie-11.jpg) ](<https://www.amourdecuisine.fr/article-tarte-abricot-macaron-118485294.html>) [ Apricot tart macaroon ](<https://www.amourdecuisine.fr/article-tarte-abricot-macaron-118485294.html>) 
</td>  
<td>


</td>  
<td>


</td> </tr> </table>

oriental pastries / biscuits / petits fours / cookies 

dirty 

= "233" valign = "top"> [ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/adhress-cuisine-algerienne1.jpg) ](<https://www.amourdecuisine.fr/article-adhress-adghes-cuisine-algerienne-118750508.html>)   
[ Adhress, adghes ](<https://www.amourdecuisine.fr/article-adhress-adghes-cuisine-algerienne-118750508.html>)  
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a400.idata.over-blog.com/2/42/48/75/API/2011-10/29/) ](<https://www.amourdecuisine.fr/article-quiche-au-saumon-100566479.html>)   
[ Salmon and spinach pie ](<https://www.amourdecuisine.fr/article-quiche-au-saumon-100566479.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/hasselback-potatoes1.jpg) ](<https://www.amourdecuisine.fr/article-pommes-de-terre-a-la-suedoise-hasselbackpotatis-108541271.html>)   
[ Hasselback potatoes ](<https://www.amourdecuisine.fr/article-pommes-de-terre-a-la-suedoise-hasselbackpotatis-108541271.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a136.idata.over-blog.com/2/42/48/75/API/2011-02/) ](<https://www.amourdecuisine.fr/article-cake-aux-olives-113781840.html>)   
[ Cake feta / olives ](<https://www.amourdecuisine.fr/article-cake-aux-olives-113781840.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-aux-champignons-et-dinde-2_22.jpg) ](<https://www.amourdecuisine.fr/article-crepes-aux-champignons-et-dinde-114963235.html>)   
[ Pancakes with mushrooms and turkey ](<https://www.amourdecuisine.fr/article-crepes-aux-champignons-et-dinde-114963235.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/samossas-de-crepes-panees-3_22.jpg) ](<https://www.amourdecuisine.fr/article-samossas-de-crepes-panes-114970535.html>)   
[ Samosas of breaded pancakes ](<https://www.amourdecuisine.fr/article-samossas-de-crepes-panes-114970535.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-courgette-et-pomme-de-terre-21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-courgette-et-pomme-de-terre-21.jpg>)   
[ Zucchini / potato pie ](<https://www.amourdecuisine.fr/article-tarte-pommes-de-terre-et-courgettes-109654753.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/borek-au-fromage-2_22.jpg) ](<https://www.amourdecuisine.fr/article-borek-au-fromage-borek-bulgare-115093547.html>)   
[ Cheese Borek ](<https://www.amourdecuisine.fr/article-borek-au-fromage-borek-bulgare-115093547.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tajine-de-poulet-aux-olives2.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-poulet-aux-olives-zeste-de-citron-et-tomates-seches-108449919.html>) [ Chicken tajine with olives ](<https://www.amourdecuisine.fr/article-tajine-de-poulet-aux-olives-zeste-de-citron-et-tomates-seches-108449919.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a405.idata.over-blog.com/2/42/48/75/API/2011-10/17/) ](<https://www.amourdecuisine.fr/article-soupe-de-potiron-surprise-86694827.html>) [ Surprise pumpkin soup ](<https://www.amourdecuisine.fr/article-soupe-de-potiron-surprise-86694827.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a397.idata.over-blog.com/2/42/48/75/API/2012-03/04/) ](<https://www.amourdecuisine.fr/article-tarte-de-legumes-bio-100753968.html>) [ Vegetable pie ](<https://www.amourdecuisine.fr/article-tarte-de-legumes-bio-100753968.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a398.idata.over-blog.com/2/42/48/75/API/2012-05/12/) ](<https://www.amourdecuisine.fr/article-terrine-de-courgettes-105037768.html>) [ From Zucchini Terrine ](<https://www.amourdecuisine.fr/article-terrine-de-courgettes-105037768.html>) 
</td>  
<td>

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-de-poivrons-cuisine-algerienne_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-hmiss-salade-de-poivrons-108102530.html>)   
[ Pepper salad ](<https://www.amourdecuisine.fr/article-hmiss-salade-de-poivrons-108102530.html>) 
</td> </tr>  
<tr>  
<td>

[ ![sweet potato gratin](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-patate-douce_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-patate-douce-115328738.html>) [ Sweet potato gratin ](<https://www.amourdecuisine.fr/article-gratin-de-patate-douce-115328738.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/quiche-lorraine-facile-et-rapide-3_22.jpg) ](<https://www.amourdecuisine.fr/article-quiche-lorraine-facile-et-rapide-114926322.html>) [ Quiche lorraine fast easy ](<https://www.amourdecuisine.fr/article-quiche-lorraine-facile-et-rapide-114926322.html>) 
</td>  
<td>

[ ![https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-aux-poireaux-3_21.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-aux-poireaux-3_21.jpg) ](<https://www.amourdecuisine.fr/article-tarte-aux-poireaux-115568724.html>) [ Leek pie ](<https://www.amourdecuisine.fr/article-tarte-aux-poireaux-115568724.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-a-la-tomate1.jpg) ](<https://www.amourdecuisine.fr/article-quiche-au-chevre-tomates-confites-et-basilic-118019414.html>) [ Quiche with goat cheese tomatoes confit and basil ](<https://www.amourdecuisine.fr/article-quiche-au-chevre-tomates-confites-et-basilic-118019414.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/quiche-aux-courgettes-tarte-aux-courgettes1.jpg) ](<https://www.amourdecuisine.fr/article-quiche-fine-aux-courgettes-et-chevre-116346634.html>) [ Quiche with zucchini and goat cheese ](<https://www.amourdecuisine.fr/article-quiche-fine-aux-courgettes-et-chevre-116346634.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/quiche-aux-carottes-gouda-cumin-1_21.jpg) ](<https://www.amourdecuisine.fr/article-quiche-gouda-carottes-cumin-115568629.html>) [ Carrot quiche, gouda and cumin ](<https://www.amourdecuisine.fr/article-quiche-gouda-carottes-cumin-115568629.html>) 
</td> </tr>  
<tr>  
<td>


</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/panna-cotta-salee-au-parmesan1.jpg) ](<https://www.amourdecuisine.fr/article-panna-cotta-au-parmesan-panna-cotta-salee-118373093.html>) [ Panna cotta with Parmesan cheese ](<https://www.amourdecuisine.fr/article-panna-cotta-au-parmesan-panna-cotta-salee-118373093.html>) 
</td>  
<td>


</td> </tr> </table>

dessert   
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a396.idata.over-blog.com/2/42/48/75/API/2013-02/06/) ](<https://www.amourdecuisine.fr/article-clafoutis-aux-abricots-115114731.html>) [ Clafoutis with apricots ](<https://www.amourdecuisine.fr/article-clafoutis-aux-abricots-115114731.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepe-au-caramel-au-beurre-sale-2_22.jpg) ](<https://www.amourdecuisine.fr/article-crepes-au-caramel-au-beurre-sale-114962194.html>) [ Salted butter caramel pancakes ](<https://www.amourdecuisine.fr/article-crepes-au-caramel-au-beurre-sale-114962194.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/Petits-gratins-aux-figues-amandes-et-speculoos-11.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-figues-aux-amandes-111235011.html>) [ Small gratins of figs with almonds and speculoos ](<https://www.amourdecuisine.fr/article-gratin-de-figues-aux-amandes-111235011.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a406.idata.over-blog.com/2/42/48/75/API/2010-07/) ](<https://www.amourdecuisine.fr/article-clafoutis-aux-cerises-108444016.html>) [ Cherry clafoutis ](<https://www.amourdecuisine.fr/article-clafoutis-aux-cerises-108444016.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a404.idata.over-blog.com/2/42/48/75/API/2012-02/15/) ](<https://www.amourdecuisine.fr/article-cupcakes-de-la-saint-valentin-99394195.html>) [ Cupcakes cupid arrow / red velvet ](<https://www.amourdecuisine.fr/article-cupcakes-de-la-saint-valentin-99394195.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a407.idata.over-blog.com/2/42/48/75/API/2013-02/11/) ](<https://www.amourdecuisine.fr/article-tiramisu-aux-speculoos-115260297.html>) [ Tiramisu with speculoos ](<https://www.amourdecuisine.fr/article-tiramisu-aux-speculoos-115260297.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/nougat-glace-2_21.jpg) ](<https://www.amourdecuisine.fr/article-nougat-glace-112683509.html>) [ Iced nougat ](<https://www.amourdecuisine.fr/article-nougat-glace-112683509.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/dsc04217.jpg) ](<https://www.amourdecuisine.fr/article-cupcakes-chocolat-pralinoise-110865577.html>)

[ Chocolate Cupcakes ](<https://www.amourdecuisine.fr/article-cupcakes-chocolat-pralinoise-110865577.html>)


</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a405.idata.over-blog.com/2/42/48/75/API/2011-06/) ](<https://www.amourdecuisine.fr/article-ananas-roti-en-papillotes-dessert-rapide-et-facile-a-la-cannelle-et-anis-etoile-113921962.html>) [ Caramelized pineapple papillotes with spices and vanilla ](<https://www.amourdecuisine.fr/article-ananas-roti-en-papillotes-dessert-rapide-et-facile-a-la-cannelle-et-anis-etoile-113921962.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cheesecake-facile-en-verrines-6_22.jpg) ](<https://www.amourdecuisine.fr/article-cheesecake-facile-en-verrines-cheesecake-a-la-confiture-de-framboise-115297381.html>) [ Cheesecake with raspberry jam in verrines ](<https://www.amourdecuisine.fr/article-cheesecake-facile-en-verrines-cheesecake-a-la-confiture-de-framboise-115297381.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a407.idata.over-blog.com/2/42/48/75/API/2012-11/12/) ](<https://www.amourdecuisine.fr/article-cupcakes-spartacus-112250629.html>) [ Chocolate cupcakes and heart of speculoos ](<https://www.amourdecuisine.fr/article-cupcakes-spartacus-112250629.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cupcake-a-la-fraise_21.jpg) ](<https://www.amourdecuisine.fr/article-cupcakes-aux-fraises-a-la-creme-meringuee-68717764.html>) [ Strawberry cupcakes with meringue butter cream ](<https://www.amourdecuisine.fr/article-cupcakes-aux-fraises-a-la-creme-meringuee-68717764.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/strawberry-cheesecake_21.jpg) ](<https://www.amourdecuisine.fr/article-cheesecake-aux-fraises-strawberry-cheesecake-78292621.html>) [ Strawberry and lemon cheesecake ](<https://www.amourdecuisine.fr/article-cheesecake-aux-fraises-strawberry-cheesecake-78292621.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://i31.servimg.com/u/f31/13/81/72/14/) ](<https://www.amourdecuisine.fr/article-clafoutis-de-provence-aux-abricots-peches-amandes-et-lavande-76664049.html>) [ Clafoutis with apricots peaches almonds and lavender ](<https://www.amourdecuisine.fr/article-clafoutis-de-provence-aux-abricots-peches-amandes-et-lavande-76664049.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cupcakes-ananas-121.jpg) ](<https://www.amourdecuisine.fr/article-cupcakes-ananas-noisettes-fleurs-sechees-d-ananas-70652002.html>) [ Pineapple / hazelnut cupcakes ](<https://www.amourdecuisine.fr/article-cupcakes-ananas-noisettes-fleurs-sechees-d-ananas-70652002.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a400.idata.over-blog.com/500x334/2/42/48/75/Images/) ](<https://www.amourdecuisine.fr/article-calfoutis-au-tofu-soyeux-69506952.html>) [ Clafoutis with silky tofu ](<https://www.amourdecuisine.fr/article-calfoutis-au-tofu-soyeux-69506952.html>) 
</td>  
<td>

[ ![](http<br%20/><br%20/><br%20/>%0A://a142.idata.over-blog.com/555x707/2/42/48/75/API/2013-03/07/cheesecake-express.jpg) ](<https://www.amourdecuisine.fr/article-cheesecake-au-framboise-cheesecake-express-118081229.html>) [ Cheesecake express ](<https://www.amourdecuisine.fr/article-cheesecake-au-framboise-cheesecake-express-118081229.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/creme-brulee-a-la-pate-de-pistache_21.jpg) ](<https://www.amourdecuisine.fr/article-creme-brulee-a-la-pistache-115809743.html>) [ Crème brûlée with pistachio ](<https://www.amourdecuisine.fr/article-creme-brulee-a-la-pistache-115809743.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/moelleux-au-chocolat-et-son-coeur-fondant1.jpg) ](<https://www.amourdecuisine.fr/article-moelleux-coulant-au-chocolat-115568653.html>) [ Fluffy chocolate cake ](<https://www.amourdecuisine.fr/article-moelleux-coulant-au-chocolat-115568653.html>) 
</td>  
<td>


</td>  
<td>


</td> </tr> </table>

jams / spreads / drinks   
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/dulce-de-leche-confiture-de-lait_21.jpg) ](<https://www.amourdecuisine.fr/article-confiture-de-lait-dulce-de-leche-114782422.html>) [ Dulce de leche / milk jam ](<https://www.amourdecuisine.fr/article-confiture-de-lait-dulce-de-leche-114782422.html>) 
</td>  
<td>

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/lait-d-amande-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-lait-d-amande-99661012.html>) [ Almond milk ](<https://www.amourdecuisine.fr/article-lait-d-amande-99661012.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/nutella-maison-facile-001_22.jpg) ](<https://www.amourdecuisine.fr/article-nutella-fait-maison-115425010.html>) [ Home made Nutella ](<https://www.amourdecuisine.fr/article-nutella-fait-maison-115425010.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a392.idata.over-blog.com/2/42/48/75/API/2013-02/06/) ](<https://www.amourdecuisine.fr/article-confiture-d-abricots-115511848.html>) [ Apricot jam ](<https://www.amourdecuisine.fr/article-confiture-d-abricots-115511848.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pate-a-tartiner-a-la-noix-de-coco-1_21.jpg) ](<https://www.amourdecuisine.fr/article-pate-a-tartiner-a-la-noix-de-coco-115568698.html>) [ Coconut spread ](<https://www.amourdecuisine.fr/article-pate-a-tartiner-a-la-noix-de-coco-115568698.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/sirop-de-menthe1.jpg) ](<https://www.amourdecuisine.fr/article-sirop-a-la-menthe-boisson-rafraichissante-118439235.html>) [ Mint sirup ](<https://www.amourdecuisine.fr/article-sirop-a-la-menthe-boisson-rafraichissante-118439235.html>) 
</td> </tr> </table>

bread / brioche / donuts   
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/164946640311.jpg) ](<https://www.amourdecuisine.fr/article-pain-pide-pain-turc-108808832.html>) [ Turkish Pide / Turkish Pide Bread ](<https://www.amourdecuisine.fr/article-pain-pide-pain-turc-108808832.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/galette-ciboulettes-21.jpg) ](<https://www.amourdecuisine.fr/article-galette-aux-oignons-piment-et-origan-78811188.html>) [ Kabyle galette with green onions peppers and oregano ](<https://www.amourdecuisine.fr/article-galette-aux-oignons-piment-et-origan-78811188.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/1101.jpg) ](<https://www.amourdecuisine.fr/article-les-croissants-au-beurre-de-christophe-felder-109831641.html>) [ Butter croissants by Christophe Felder ](<https://www.amourdecuisine.fr/article-les-croissants-au-beurre-de-christophe-felder-109831641.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a407.idata.over-blog.com/2/42/48/75/API/2011-03/) ](<https://www.amourdecuisine.fr/article-pets-de-none-69086545.html>) [ None ](<https://www.amourdecuisine.fr/article-pets-de-none-69086545.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pain-aux-noisettes1.jpg) ](<https://www.amourdecuisine.fr/article-pain-a-l-avoine-pain-riche-en-fibres-116018697.html>) [ Oat bread / high fiber bread ](<https://www.amourdecuisine.fr/article-pain-a-l-avoine-pain-riche-en-fibres-116018697.html>) 
</td>  
<td>


</td> </tr> </table>

Paste or basic preparation   
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pate-a-tarte-sucree-0011.jpg) ](<https://www.amourdecuisine.fr/article-pate-a-tarte-sucree-117841768.html>) [ Sweet pie dough ](<https://www.amourdecuisine.fr/article-pate-a-tarte-sucree-117841768.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/fond-de-tarte-au-cacao1.jpg) ](<https://www.amourdecuisine.fr/article-pate-sablee-au-cacao-fond-de-tarte-au-cacao-117021042.html>) [ Shortbread with cocoa ](<https://www.amourdecuisine.fr/article-pate-sablee-au-cacao-fond-de-tarte-au-cacao-117021042.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://a397.idata.over-blog.com/555x371/2/<br%20/><br%20/><br%20/>%0A42/48/75/API/2013-03/07/) ](<https://www.amourdecuisine.fr/article-pate-brisee-116283085.html>) [ Pastry ](<https://www.amourdecuisine.fr/article-pate-brisee-116283085.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pate-a-crepes-au-nutella-2_22.jpg) ](<https://www.amourdecuisine.fr/article-pate-a-crepe-au-nutella-crepe-au-chocolat-facile-et-rapide-115021705.html>) [ Nutella pancake dough ](<https://www.amourdecuisine.fr/article-pate-a-crepe-au-nutella-crepe-au-chocolat-facile-et-rapide-115021705.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-17.bmp_22.jpg) ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee-114932092.html>) [ Natural pancake dough ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee-114932092.html>) 
</td>  
<td>


</td> </tr> </table>

Ice cream / sherbets / frozen dessert   
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x826/2/42/48/75/API/2013-03/06/) ](<https://www.amourdecuisine.fr/article-creme-glacee-aux-fraises-sans-sorbetiere-118650360.html>) [ Ice   
magic strawberry ](<https://www.amourdecuisine.fr/article-creme-glacee-aux-fraises-sans-sorbetiere-118650360.html>) 
</td>  
<td>


</td>  
<td>


</td> </tr> </table>
