---
title: Couscous with peas
date: '2016-09-15'
categories:
- couscous
- cuisine algerienne
- recettes de feculents
tags:
- Algeria
- dishes
- Vegetarian cuisine
- Easy cooking
- Semolina
- Full Dish
- Steam cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/couscous-au-petit-pois-2.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/couscous-au-petit-pois-2.jpg)

##  Couscous with peas 

Hello everybody, 

Hum ... Couscous with peas steamed and olive oil !!! Among the recipes that marked my childhood, and that I prepare proudly for my children, it is the vegetable couscous with olive oil. 

I remember every time my mother prepared us [ Chicken couscous ](<https://www.amourdecuisine.fr/article-couscous-au-poulet.html>) or couscous with meat and no more sauce, while there is always a little steamed couscous. She prepared us these couscous with olive oil. 

Sometimes it was [ steamed vegetable couscous (amekfoul) ](<https://www.amourdecuisine.fr/article-couscous-aux-legumes-amekfoul-cuisine-algerienne.html>) , sometimes [ couscous with beans (aghmoudh) ](<https://www.amourdecuisine.fr/article-aghmoudh-couscous-aux-feves-et-huile-dolive.html>) and the one I'm going to share with you today **couscous with peas** . 

Personally I do like my mother, every time I have couscous, I realize one of these couscous with vegetables steamed and I enjoy.   


**Couscous with peas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/couscous-au-petit-pois.jpg)

portions:  4  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * steamed couscous 
  * peas (shelled, here frozen) 
  * olive oil 
  * salt 



**Realization steps**

  1. If you have couscous already cooked it is good, if not start by steaming the couscous twice. 
  2. let the couscous rest before steaming for a third time, cook the peas. 
  3. place the peas at the bottom of the couscous top, salt, cover and let cook, 
  4. when the peas are very tender in the mouth, put over the couscous, and cook until the steam starts to escape between the grains of the couscous. 
  5. pour this mixture into a large terrine, mix the peas and couscous well under a generous drizzle of olive oil. 
  6. your dish is ready now, serve immediately. 


