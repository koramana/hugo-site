---
title: Strawberry almond tart
date: '2011-05-15'
categories:
- Algerian cuisine
- cuisine marocaine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/05/tarte-amandine-au-fraises-7_thumb.jpg
---
##  Strawberry almond tart 

Hello everybody, 

here is a sweet recipe, that shares with us our dear Lunetoiles, I hope you are well my dear, I did not hear from you ???? !!! 

so this recipe she has sent me for almost a month, but I had not had time to share it with you, and as they say: it's never too late to do well:   


**Strawberry almond tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/tarte-amandine-au-fraises-7_thumb.jpg)

portions:  8 

**Ingredients** For a pie circle 24 cm in diameter For the shortbread dough: 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

Almond cream: 
  * 12Og of almond powder 
  * 2 eggs 
  * 100g of sugar 
  * 200 ml whole liquid cream 
  * 200 gr of washed strawberries, hulled and cut in half 

Decoration: 
  * Scraped almonds 
  * 2 tablespoons strawberry jam 



**Realization steps** The shortbread dough: 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a paste is removed from the sides of the bowl and is non-sticky. 
  4. Let it rest. 
  5. Spread the dough with the roll between 2 sheets of baking paper. 
  6. Smear a pie circle 24 cm in diameter. 
  7. Stir the batter stock with a fork and chill for the time to prepare the filling. 
  8. The amnadine cream: 
  9. In the bowl of the beater this time equipped with the whisk, beat the eggs with the sugar. 
  10. When the mixture is frothy add the almond powder and the cream. Beat again. 
  11. Arrange the strawberries on the pie shell and cover with almond cream. 
  12. Put in the oven for 30 minutes at 180 ° C (th.6). Then sprinkle with blanched almonds 15 minutes before the end of cooking. 
  13. Take the pie out of the oven. Heat the jam and brush the entire surface of the pie with a brush. 
  14. Let cool. 
  15. Unmould and present on a serving platter. Let stand 1 hour in the fridge before eating. 



thank you my friends for your visits and comments 

thanks also to all those who click on the links just under this article, it helps me a lot. 

bonne journee 
