---
title: the Moorish kefta
date: '2011-11-03'
categories:
- Cupcakes, macaroons, and other pastries
- Gateaux au chocolat

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/mauresque-kefta-gateau-algerien-95_thumb1.jpg
---
##  the Moorish kefta 

###  **Kefta with almonds, kefta bellouz كفته باللوز**

Hello everybody, 

once again my dear lecterices, a recipe that comes directly from the cuisine of Lunetoiles: Moorish or Kefta with almonds revisited. They are good, they are too pretty, nothing more to say, than to try them, besides it is very easy to do, except when you arrive at the coloring, maybe the children will help in that, hihihihih 

**The Moorish** by Saida Benberim from the Special Decoration book 

![Moorish kefta Algerian cake 95](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/mauresque-kefta-gateau-algerien-95_thumb1.jpg)

**the Moorish kefta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/mauresque-kefta-gateau-algerien-29_thumb1.jpg)

**Ingredients** Almond paste : 

  * 1 measure white almond powder 
  * ½ measure of icing sugar 
  * 1 C. dessert vanilla extract 

Syrup: 
  * 250 gr of crystallized sugar 
  * ½ liter of water 

Prank call : 
  * 1 measure of finely ground roasted almonds 
  * ½ measure of sieved icing sugar 
  * 2 tbsp. softened butter 
  * 1 C. teaspoon praline extract 
  * Honey 
  * Yellow dye / banana flavor 
  * Orange dye / peach aroma 
  * Cocoa to get the brown color 
  * Pink dye / strawberry aroma 



**Realization steps**

  1. Boil water and sugar in a saucepan over medium heat until thickened (about 15 minutes after boiling), remove and let cool. 
  2. Mix the almonds, the sifted icing sugar and the vanilla extract, pass them through the fine sieve and collect with the syrup to obtain a smooth and very manageable paste, color it in white and cover with food paper. 
  3. Stain the majority of the almond paste in white, divide the rest into 4 equal balls, color them in yellow, orange, pink and brown. Dilute each color in its aroma, when the cocoa introduce it directly into the marzipan. 
  4. Prepare the stuffing with the ingredients mentioned above, so as to obtain a manageable and homogeneous stuffing. 
  5. Lower the white almond paste with the rolling pin on a work surface sprinkled with icing sugar to a thickness of ½ cm. 
  6. Cut out mosaic shapes using the Moorish mold. 
  7. The same operation is made for stuffing, arrange mosaic shapes on a tray sprinkled with icing sugar and cover with cling film. 
  8. Paste on the stuffing the two forms of Moorish white almond paste one at the top and the other at the bottom. Pass the diluted color in its aroma with a small pastry brush all around the room 



thanks again to Lunetoiles who always shares with us these wonderful recipes, 

and thank you for your comments which encourages him every time 

and thank you to the faithful and the people who subscribe more and more to my newsletter. 

bisous 
