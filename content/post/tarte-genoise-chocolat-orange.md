---
title: orange chocolate sponge cake
date: '2017-02-03'
categories:
- Gateaux au chocolat
- gateaux, et cakes
- recettes sucrees
tags:
- Cakes
- desserts
- Pastry
- To taste
- Easy cooking
- Algerian cakes
- Citrus

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/tarte-genoise-chocolat-orange-1.jpg
---
![orange chocolate genoise pie 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/tarte-genoise-chocolat-orange-1.jpg)

##  orange chocolate sponge cake 

Hello everybody, 

This orange chocolate sponge pie is a recipe that I highly recommend! Since the time I bought my mold for Genoese pie, and I wanted to make recipes with it, but here is where I put it ??? 

Yes when we have only 2 closets in the house, and a kitchen that is 1m by 2.5m, it is not easy to arrange! I had found the idea to buy large plastic box to store under the original beds, but over time, with almost 5 to 6 boxes under each bed, not easy to find what we finally look for! ? 

In any case, by chance, I came across my cake for Genoese cake, and I did not hesitate to make this delicious cake, which finally had a great success, even with children who thought at first that the orange cream was going to be a little acidic to their tastes. 

![orange chocolate genoise pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/tarte-genoise-chocolat-orange-2.jpg)

You can also see all the previous rounds as well as godmothers and star ingredients: 

**orange chocolate sponge cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/tarte-genoise-chocolat-orange.jpg)

portions:  12  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** For the chocolate sponge cake: 

  * 220 gr of flour 
  * 2 eggs 
  * 20 grams of cocoa 
  * 70 ml of milk 
  * 240 gr of sugar (you can go up to 260 gr, I do not like my cakes too sweet) 
  * 100 ml of oil 
  * 1 C. coffee baking powder 
  * 1 C. vanilla coffee 
  * 1 C. of soluble coffee dissolved in 30 ml of water 

for the orange cream: (you can double the quantities for more cream, I was afraid that the children do not like) 
  * 250 ml of pressed orange juice 
  * 50 gr of sugar 
  * (or less if your oranges are sweet) 
  * 2 egg yolks 
  * 15 gr of cornflour 

to soak and garnish 
  * freshly squeezed orange juice. 
  * segments of oranges 
  * chocolate chips 



**Realization steps**

  1. prepare the Genoese cake: 
  2. beat the eggs, add the sugar and vanilla sugar and whip again. 
  3. introduce milk, oil and coffee and whisk again. 
  4. now add sifted flour, sifted cocoa and baking powder. introduce everything well. 
  5. butter and flour a sponge cake pan about 30 cm in diameter. 
  6. pour the mixture and tap a little against the work surface to standardize the surface of the cake 
  7. cook in a preheated oven at 170 ° C between 20 and 25 min. 

for the orange cream: 
  1. beat the egg yolks with the sugar and add the orange juice. 
  2. Heat the mixture in a saucepan while stirring 
  3. add the diluted cornflour in a little orange juice 
  4. thicken over low heat, stirring regularly. 
  5. The cream is ready when it has a nice consistency and slicks the back of your spoon. 

mounting, 
  1. when you take it out of the oven, let the Genoese pie cool a little before unmolding. 
  2. soak the cake with a little orange juice. 
  3. fill the small cavity with the orange cream 
  4. when the cream cools, garnish the pie with segments of orange and chocolate. 



la liste des participantes au jeu: 
