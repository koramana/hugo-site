---
title: hash recipe
date: '2012-11-10'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/hachid-parmentier-2.jpg
---
Hello everybody, 

Here is a well-known dish, the mutton mince, a very rich and delicious dish, add an orange juice, or some fruit, and here you are before a complete meal. 

a recipe that I make quite often in my kitchen, but not post on the blog, so it's never too late to do well.   


**hash recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/hachid-parmentier-2.jpg)

**Ingredients**

  * Ingredients: 
  * 1 kg of potatoes 
  * 400 g ground beef 
  * 1 onion 
  * 1 clove of garlic 
  * ½ bunch of parsley 
  * 1 egg 
  * 100 g of butter 
  * 300 ml of milk 
  * nutmeg 
  * salt 
  * pepper 



**Realization steps**

  1. Peel the potatoes and cook them in English for about 30 minutes. 
  2. Chop the onion and garlic. In a frying pan, sweat the onion with butter. 
  3. Stir until it becomes translucent, add the chopped meat, cook and reserve. 
  4. In a bowl, combine ground meat, chopped parsley, onions, and whole egg. 
  5. Season with salt and pepper and mix well. 
  6. Once the potatoes are cooked, drain and purée them 
  7. Heat the milk over low heat, add the butter in the mashed potato, then finally the hot milk. 
  8. Mix well to obtain a creamy mash. 
  9. Season with salt, pepper and nutmeg. 
  10. Butter a gratin dish. Spread half of the purée on the bottom of the dish, spread over the meat and spread the remaining mashed potatoes, you can use the piping bag to spread this layer. 
  11. Bake in a hot oven at 200 ° C until the surface of the shepherd's pie has a nice golden color. 
  12. take out of the oven ... and serve immediately. 



[ recette a la viande ](<https://www.amourdecuisine.fr/categorie-12359239.html>)
