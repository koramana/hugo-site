---
title: pecan pie
date: '2014-08-06'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-a-la-noix-de-pecan-032.CR2_1.jpg
---
![pie-in-the-nuts-to-pecan-032.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-a-la-noix-de-pecan-032.CR2_1.jpg)

##  pecan pie 

Hello everybody, 

![pie-in-the-nuts-to-pecan-021.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-a-la-noix-de-pecan-021.CR2_1.jpg)

**pecan pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-a-la-noix-de-pecan-042.CR2_1.jpg)

portions:  8  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 1 lowers [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast")
  * 350 pecan nuts 
  * 140 gr of honey 
  * 150 gr of sugar 
  * 3 eggs 
  * 50 gr of butter 
  * 60 ml of maple syrup 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. Pour the pie shell into a mold with a removable base if possible, and cook the dough for 20 min. 
  3. Cut roughly half of the pecans with a knife. 
  4. In a bowl, mix eggs, honey and brown sugar. 
  5. Melt the butter in the microwave and stir in the mixture. 
  6. Add the chopped pecans and the maple syrup. 
  7. pour this mixture onto the precooked pie shell. 
  8. decorate the top of whole pecans, 
  9. bake for 30 to 35 minutes. 
  10. serve and enjoy after cooling the pie. 



![pie-in-the-nuts-to-pecan-copy-2.jpg 048.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-a-la-noix-de-pecan-048.CR2-copie-211.jpg)
