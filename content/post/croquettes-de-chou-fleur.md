---
title: cauliflower croquettes
date: '2017-07-09'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- Algeria
- Easy recipe
- Ramadan
- inputs
- accompaniment
- Galette
- Vegetables

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/croquettes-de-chou-fleur21.jpg
---
![croquettes de cauliflower fleur21](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/croquettes-de-chou-fleur21.jpg)

##  cauliflower croquettes 

Hello everybody, 

It is difficult for me sometimes to make some children eat vegetables, and I am sure that even by you it is the same worry especially when one manages to make eat the cauliflower !!! 

For this time, the cauliflower is well disguised in this nice recipe of kale flower, they are super good, and they ate without question, yeay! A marked point! hahahaha. 

I recommend this recipe of cauliflower croquettes, because if you do not say it's with what, nobody will guess!   


**cauliflower croquettes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/croquettes-de-chou-fleur.jpg)

**Ingredients**

  * 1 medium cauliflower, 
  * 1 big egg, 
  * half a bunch of parsley, 
  * 3 cloves of garlic, 
  * ½ teaspoon of black pepper, 
  * ½ teaspoon of cumin, 
  * ½ teaspoon coriander powder, 
  * salt, 
  * flour, 
  * oil for frying 



**Realization steps**

  1. clean the cauliflower, and cut it into bunches. 
  2. dip them in a pan of cold water, add salt and vinegar (it absorbs a little smell), 
  3. Bake for 8 to 10 minutes over medium heat. 
  4. Drain and let cool. 
  5. chop the parsley, add the garlic crushes 
  6. break an egg and whip, add the spices mentioned. 
  7. add the cauliflower on the egg mixture, and crush it with the potato crush, or by hand, 
  8. form balls or small pudding 
  9. coat the cauliflower balls in flour, 
  10. fry in an oil bath until they turn a beautiful color 


