---
title: covered pizza at the magic dough
date: '2017-08-30'
categories:
- pizzas / quiches / pies and sandwiches
tags:
- Pizza dough
- Algeria
- Cheese
- inputs
- dishes
- Easy cooking
- Bakery

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pizza-couverte-a-la-pate-magique.jpg
---
[ ![covered pizza at the magic dough](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pizza-couverte-a-la-pate-magique.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pizza-couverte-a-la-pate-magique.jpg>)

##  covered pizza at the magic dough 

Hello everybody, 

A recipe quickly made and well done for lunch? Here is one, this covered pizza recipe, whose dough is the magic dough, is the solution for a deliciously good lunch. 

This recipe is that of my friend Pipou jewels, with the ingredients of the magic dough she gives us, we can have a nice covered pizza, and a big pizza that you can decorate according to your choice. 

**covered pizza at the magic dough**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pizza-couverte-2.jpg)

portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * [ magic pasta ](<https://www.amourdecuisine.fr/article-pate-magique-facile-en-10-minutes.html>)

prank call: 
  * 1 kg of fresh tomatoes 
  * 1 onion 
  * 1 clove of garlic 
  * 2 tablespoons of olive oil 
  * salt and black pepper 
  * thyme 
  * 1 tablespoon of sugar 
  * 1 to 2 tablespoons concentrated tomato 

for decoration: 
  * sweet pepper 
  * 1 fresh onion 
  * olives 
  * parsley 
  * tuna 
  * melted cheese 



**Realization steps** prepare the stuffing: 

  1. place the grated tomatoes, onion, garlic and oil in a pan, 
  2. let it come back, and add salt, black pepper and thyme 
  3. then add the concentrated tomato and the sugar (to reduce the acidity) 
  4. let the sauce well reduce to have a nice covered pizza. 
  5. pass the sauce with the strainer (Chinese) so that it is velvety. 
  6. prepare the [ magic pasta ](<https://www.amourdecuisine.fr/article-pate-magique-facile-en-10-minutes.html>)
  7. you can share the resulting dough into three ball. 
  8. spread out one of the balls. 
  9. cover it with the tomato sauce, then decorate with the ingredients of your choice, here: pepper, fresh onion cut into cubes, olives, parsley, tuna and melted cheese 
  10. spread out another ball of dough, and cover the stuffing   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pizza-couverte-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pizza-couverte-4.jpg>)
  11. you can decorate the dough with an egg yolk and milk, and why not seed of nigella. 
  12. cook in a preheated oven at 180 degrees C, until you have a nice golden pizza. 



[ ![covered pizza at the magic dough 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pizza-couverte-a-la-pate-magique-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pizza-couverte-a-la-pate-magique-1.jpg>)
