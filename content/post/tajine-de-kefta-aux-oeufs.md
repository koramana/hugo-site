---
title: tagine of kefta with eggs
date: '2014-07-22'
categories:
- Moroccan cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-de-kefta-aux-oeufs-013.CR2_thumb1.jpg
---
##  tagine of kefta with eggs 

Hello everybody, 

a dish easier, faster, and more delicious than this tajine of kefta with eggs, I do not think to know ... .. 

so generally, when I have chopped meat at home, I use what I need, and the rest I season it and I pelletize in the freezer, and when I'm running ideas for to make food, well, I do this tajine of kefta with eggs, and here we are, we sit down to table after 30 minutes ... 

**tagine of kefta with eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-de-kefta-aux-oeufs-013.CR2_thumb1.jpg)

portions:  2  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 200 gr of minced meat 
  * ½ chopped onion 
  * 1 cloves of garlic mashed 
  * some sprigs of chopped parsley. 
  * 1 cup of cumin powder 
  * 1 cc of paprika 
  * salt and black pepper 
  * 2 to 3 eggs 
  * 2 to 3 fresh tomatoes, or a box of tomato in pieces. 
  * 2 to 3 tablespoons of table oil 



**Realization steps**

  1. in a bowl, mix the chopped meat, crushed garlic, chopped parsley salt and black pepper to prepare meatballs. Book. 
  2. in a frying pan, or if you use a Moroccan tajine, fry the grated onion and clove of garlic in a little oil. 
  3. Add the tomatoes cut into small cubes, add salt, black pepper and a little cumin 
  4. Add the chopped parsley and simmer for about 15 minutes. 
  5. Stir in kefta meatballs. 
  6. when the meatballs are cooked, break the eggs, sprinkle with a touch of salt. 


