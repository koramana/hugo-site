---
title: lemon squares
date: '2014-02-15'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/carres-au-citron-3-1024x685.jpg
---
![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/carres-au-citron-3-1024x685.jpg)

Hello everybody, 

I'm still not back to my kitchen, between breastfeeding baby, change the diaper, make the beautiful sleepless nights, I'm only looking for the moment, or baby sleeps, to put me on my pillow, and what is it? I miss this pillow, lol ... 

So today, and in a moment of rest, before giving the baby, his bath .... yes it's a little late, but it's at the request of the children, who want me to do that in their presence, hihihihih ... 

I find this little moment to post a Lunetoiles recipe, lemon squares, a crispy layer of crumbs on the bottom, covered by a beautiful layer of creamy lemon cream, and all scented ... yum I already salivate .... 

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/carres-au-citron-685x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/carres-au-citron.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

thank you 

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/carres-au-citron-1-768x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/carres-au-citron-1.jpg>)
