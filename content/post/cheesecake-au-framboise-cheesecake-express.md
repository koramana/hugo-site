---
title: Raspberry cheesecake / cheesecake express
date: '2014-08-02'
categories:
- cheesecakes and tiramisus

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/cheesecake-facile-et-rapide1.jpg
---
![cheesecake-and-easy-rapide.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/cheesecake-facile-et-rapide1.jpg)

##  Raspberry cheesecake / cheesecake express 

Hello everybody, 

Who does not like cheesecakes ???? almost everyone loves this delicious cake or dessert with sweet and salty flavors. But sometimes the problem is the cooking time too long, so here is the recipe for an express cheesecake, quick and easy to make ... a raspberry cheesecake, yumi to leave nothing on your plate ... 

**Preparation time : 10 minutes  ** **Cooking time : 30 min  **

**Raspberry cheesecake / cheesecake express**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/cheesecake-facile-et-rapide-208x300.jpg)

portions:  20  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * 450 g of white cheese 
  * 50 g whole liquid cream 
  * 80 g caster sugar 
  * 25 g of cornstarch 
  * 3 eggs 
  * 50 g raspberry or raspberry puree or frozen puree 



**Realization steps**

  1. Preheat the oven to 120 ° C (Th.4). 
  2. In a small bowl, soften the white cheese with a hand whisk, then stir in the cornstarch. 
  3. Mix well to avoid lumps. Add, in order, the liquid cream, sugar and eggs, one by one, whisking well: the mixture should be smooth and homogeneous. 
  4. Pour this preparation into the small ramekins, covered with paper cups, then place on each a line of raspberry coulis, drawing a spiral. 
  5. Bake for about 30 minutes: the cheesecakes must be taken. 
  6. Reserve in the refrigerator at least 2 hours before tasting. 



![cheesecake-express-easy-and-rapide.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/cheesecake-express-facile-et-rapide1.jpg)
