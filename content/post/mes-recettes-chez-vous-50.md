---
title: my recipes at home 50
date: '2015-01-24'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

Thank you my friends again for sharing with me the recipes you made, by insinuating me from my blog. It is a pleasure to read your comments, to receive your emails, and I apologize if I do not answer in time, whether it is to you my friends, or to my blogger friends who are very kind leave some beautiful commentaries, and I can not even give it a visit, I hope I can do it, when I find the right rhythm with Mr. baby running in all directions, lol. 

Follow this page to send me photos of your achievements: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") . **

You can send the photo of your achievement with the link of the recipe you have tried (I am talking about the link from my blog) on ​​this email address: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

Or, join us on the group [ cook with love of cooking ](<https://www.amourdecuisine.fr/article-mes-realisations-chez-vous-49.html>) , sharing is great on this group, and sometimes it's easier to join on. 

If you are a blogger, it will be nice to leave the link to your blog too, and if you are not a blogger, your name will be good enough. Also if you have tested and approved a recipe from my blog, and you have no photos, just leave a comment in the recipe in question. your opinion will surely help people who want to try the recipe. 

[ ![baba has the orange](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/baba-a-lorange.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/baba-a-lorange.jpg>)

[ baba has the orange ](<https://www.amourdecuisine.fr/article-la-recette-du-gateau-le-baba-sans-rhum.html> "the cake recipe the rum-free baba") at sissy chergui 

[ ![chocolate pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tarte-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tarte-au-chocolat.jpg>)

[ chocolate pie ](<https://www.amourdecuisine.fr/article-tarte-au-chocolat-115954210.html> "chocolate pie تورتة الشكولاطة") at sissy chergui 

[ ![ghribia has oil](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/ghribia-a-lhuile.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/ghribia-a-lhuile.jpg>)

[ ghribia ta3 ezzite ](<https://www.amourdecuisine.fr/article-ghribiya-ta3-ezzit-ghribiya-a-l-huile.html> "ghribiya ta3 ezzit, oil ghribiya غريبية بالزيت") at Rachid Dj 

[ ![dali gribia](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gribia-de-dali.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gribia-de-dali.jpg>)

[ ghribia has oil ](<https://www.amourdecuisine.fr/article-ghribiya-algeroise-ghribia-a-l-huile-gateau-algerois.html> "ghribiya algeroise, ghribia oil, cakes algerois") at Dali's 

[ ![donut fricassee](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/beignet-a-fricass%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/beignet-a-fricass%C3%A9.jpg>)

[ Fritters fritters ](<https://www.amourdecuisine.fr/article-fricasse-tunisien.html> "Tunisian Fricassee") at Oréda S 

[ ![basboussa fleurdz](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/basboussa-fleurdz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/basboussa-fleurdz.jpg>)

[ Basboussa with breadcrumbs ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html> "algerian patisserie, Basboussa in video بسبوسة سهلة") at Fleur Dz 

[ ![flan at nestlé fleur dz](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/flan-au-nestl%C3%A9-fleur-dz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/flan-au-nestl%C3%A9-fleur-dz.jpg>)

[ Nestle flan ](<https://www.amourdecuisine.fr/article-flan-au-lait-concentre-nestle-106499948.html> "concentrated milk flan / Nestlé") at Fleur Dz 

[ ![basboussa at rachida](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/basboussa-chez-rachida.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/basboussa-chez-rachida.jpg>)

[ qalb ellouz bel lben ](<https://www.amourdecuisine.fr/article-qalb-el-louz-au-lben-babeurre-recette-inratable.html> "Qalb el louz with lben essential buttermilk recipe") with breadcrumbs at Rachida Dj 

[ ![stuffed tomato oum nour](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tomate-farcie-oum-nour.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tomate-farcie-oum-nour.jpg>)

[ stuffed tomatoes with eggs ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs.html> "stuffed tomatoes") at Oum Nour 

[ ![tagine breaded at oum nour](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tajine-pan%C3%A9-chez-oum-nour.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tajine-pan%C3%A9-chez-oum-nour.jpg>)

[ Paned chicken ](<https://www.amourdecuisine.fr/article-cuisse-de-poulet-panee.html> "paned chicken leg أفخاذ الدجاج بالفرن") chez Oum Nour 
