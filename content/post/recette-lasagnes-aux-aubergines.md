---
title: recipe lasagna with eggplant
date: '2017-08-23'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- recipe for red meat (halal)
- ramadan recipe
tags:
- Easy cooking
- Oven cooking
- Full Dish
- Economy Cuisine
- Mozzarella
- gratins
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/lasagnes-daubergines-3-a.jpg
---
[ ![eggplant lasagna 3 a](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/lasagnes-daubergines-3-a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/lasagnes-daubergines-3-a.jpg>)

##  recipe lasagna with eggplant 

Hello everybody, 

I really like eggplant, and I'm always looking for a recipe to make with eggplant, you can see that this is not the missing ingredient in my blog, Before making this delicious eggplant lasagna recipe . 

I already realized a [ aubergine gratin ](<https://www.amourdecuisine.fr/article-recette-de-gratin-daubergines.html> "aubergine gratin recipe") , [ rolled eggplant with pesto ](<https://www.amourdecuisine.fr/article-des-roules-d-aubergine-au-pesto.html> "rolled eggplant with pesto") , [ aubergines stuffed in the oven ](<https://www.amourdecuisine.fr/article-aubergine-farcie-au-four.html>) , [ aubergines in sauce: Algerian mderbel ](<https://www.amourdecuisine.fr/article-aubergines-en-sauce-mderbel-algerien.html> "aubergines in sauce, Algerian mderbel") , [ eggplant fritters ](<https://www.amourdecuisine.fr/article-beignets-d-aubergines-au-basilic.html> "eggplant donuts with basil") , and the [ charlotte with aubergines ](<https://www.amourdecuisine.fr/article-charlotte-a-laubergine.html> "Charlotte aubergine")

So today, it's the eggplant lasagna recipe I saw on the M2 channel, a recipe from the talented Choumicha, whom I've been for over 13 years now. You have to say something about Choumicha, she developed a lot of her cooking, but she stayed at the same time "Young", I would like to know her secret, you're not of my opinion? lol ...   


**eggplant lasagna recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/lasagnes-daubergines-2.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 3 eggplants 
  * ½ c. salt 
  * 3 c. oil soup 

For the sauce: 
  * [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video")
  * [ bolognaise sauce ](<https://www.amourdecuisine.fr/article-sauce-bolognaise-maison-halal-et-sans-vin.html> "homemade Bolognese sauce / halal and without wine")
  * 4 tablespoons grated mozzarella 



**Realization steps**

  1. Cut the aubergines into thin slices. Sprinkle them with salt and let them drain their water. 
  2. Rinse them in water and dry on paper towels. 
  3. Smear the eggplant slices with oil and cook them in a skillet over medium heat on both sides. 
  4. prepare the Bolognese sauce and bechamel sauce. 
  5. In a lightly buttered baking dish, arrange the eggplant slices and top with Bolognese sauce and grated cheese. make 3 layers of eggplants 
  6. Top the last layer of aubergines with Bolognese sauce and cheese. 
  7. Cook the eggplant lasagna in a preheated oven at 180 ° C for about 30 minutes. 
  8. Serve the hot dish. 



[ ![eggplant lasagna 4](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/lasagnes-daubergines-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/lasagnes-daubergines-4.jpg>)
