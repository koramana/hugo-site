---
title: Tlitli, Algerian recipe
date: '2012-08-27'
categories:
- cuisine algerienne
- Mina el forn
- pain, pain traditionnel, galette
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/tlitli-1_thumb.jpg
---
##  Tlitli, Algerian recipe 

Hello everybody, 

I do not tell you the number of recipes that I prepare, and that I say to myself "not worth publishing" on the pretext that everyone knows the recipe, or each one his way of preparing this recipe, but at the same time, I do not tell you the number of people who come to ask me this same recipe that I go to push back to not publish on the blog. 

this time, almost 20 girls in the space of 3 days have asked me for this recipe, the Tlitli recipe, also known as the languages ​​of birds. so that I try to prepare a traditional dish every week (and I speak of Couscous, chakhchoukhat eddfar, Trida square, rechta ......) this week it was the turn of the Tlitli, a pity I had forgotten to take the pictures of the steps. 

in any case this is not going to be the last time, inshAllah I will try again to make even more detailed photos, but for the moment I pass you the recipe, knowing that it is the recipe as I prepare it myself , and as the members of my family love it:   


**Tlitli, Algerian recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/tlitli-1_thumb.jpg)

portions:  6  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients** for the sauce: 

  * 500g of chicken   
(legs in this recipe) 
  * 1 big onion 
  * 2 cloves garlic 
  * olive oil 
  * a generous handful of chick peas 
  * salt, black pepper, spice mixture "coriander / garlic" 
  * 1 tbsp and a half tomato concentrate 

for the pasta: 
  * 500 gr of tlitli (bird languages) 
  * 1 tablespoon of table oil 
  * 1 tablespoon of butter 

decoration: 
  * 2 to 3 hard boiled eggs 



**Realization steps**

  1. Sauté the chopped chicken pieces with the finely chopped onion oil and crushed garlic in a couscous pot 
  2. add the tomato concentrate 
  3. add salt and spices, and let simmer 
  4. add the chickpeas and cover with 1 and ½ to 2 liters of hot water. 
  5. take the pack of bird tongues, coat it with a case of oil so that it does not stick during the passage to the steam. 
  6. pour them into the top of the couscoussier and let steam for about 15 to 20 minutes, 
  7. pour into a bowl, salt, pepper and sprinkle with a little hot water. 
  8. let this water absorb, then cook again on the steam 
  9. pour in the bowl, sprinkle with 2 or 3 ladles of sauce, allow to absorb and cook again on steam. 
  10. after cooking chicken remove with cooked chickpeas, place in another small pot and reserve sauce. 
  11. Pour the tlitli, in a pot, add the sauce very slowly, ladle by ladle, turning on medium heat so as not to 
  12. do this until the tlitli absorbs the sauce well, and is well tender and cooked, 
  13. add the butter spoon 
  14. rectify the seasoning if necessary 
  15. serve in a deep plate, decorate with meat and chickpeas in the middle, boiled eggs 


