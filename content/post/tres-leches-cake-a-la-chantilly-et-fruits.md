---
title: very leches Cake with whipped cream and fruits
date: '2016-12-18'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/tres-leches2.jpg
---
![very leches Cake with whipped cream and fruits](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/tres-leches2.jpg)

##  very leches Cake with whipped cream and fruits 

Hello everybody, here is a delicious cake that shares with us our dear lunetoiles, the very leche cake with whipped cream and fruits. But what is the lick? the very leches is a cake from Latin America, a spongy cake watered after cooking a delicious syrup composed of three milks. 

**very leches Cake with whipped cream and fruits**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/tres-leches11.jpg)

portions:  12  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients** Cake: 

  * 5 big eggs 
  * 200 gr of sugar 
  * 180 gr of flour 
  * 75 ml of milk 
  * 1/2 teaspoon of pure vanilla extract 
  * 1cc of baking powder 
  * 1 pinch of salt 

Syrup with three milks: 
  * 1 can of sweetened condensed milk 397 gr 
  * 1 can of unsweetened condensed milk 397 gr 
  * 100 gr whole cream (or whole milk) 

Garnish : 
  * 1 cup whipping cream 
  * 2 tbsp. tablespoon icing sugar 
  * 1/2 teaspoon of pure vanilla 



**Realization steps**

  1. Preheat the oven to 170 ° C degrees. Butter generously a baking tin with 24 cm sides. 
  2. In a large bowl of your electric mixer, beat 3/4 cup sugar and 5 egg yolks for about 5 minutes or until yolks turn pale yellow. 
  3. Whisk in the milk yolks, vanilla extract, flour and baking powder and set aside. 
  4. In another clean bowl of your electric mixer, beat the 5 egg whites until soft peaks form. 
  5. Gradually add the remaining sugar (1/4) and continue beating until the whites are shiny and firm. Gently stir the egg white mixture into the egg yolk mixture. Pour the dough into the prepared pan. 
  6. Bake for about 40 to 50 minutes or until a toothpick inserted in the center comes out clean. Remove from oven and let the cake cool completely on a wire rack. 

While the cake is cold, prepare the syrup three milks: 
  1. In a large bowl, mix the sweetened condensed milk, unsweetened condensed milk, and the cream, vanilla extract, whisking until smooth. 
  2. Once the cake has completely cooled, pierce the cake completely with a fork, taking care not to tear the cake. 
  3. Place the cake in the baking pan. Slowly pour the syrup with the three milks on the top of the cake and sprinkle it on all sides, until all the syrup is absorbed. 
  4. Cover and refrigerate until serving time (minimum 12 hours, best to refrigerate for 24 hours) 

When serving, prepare the filling mixture: 
  1. In a small bowl, beat the whipping cream, sugar and vanilla extract until stiff peaks form. 
  2. Spread the whipped cream on top of the cake and serve immediately with fresh fruit, diced! 



![very leches Cake with whipped cream and fruits-2.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/tres-leches-21.jpg) Bonne journee. 
