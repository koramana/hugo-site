---
title: hmiss, pepper salad
date: '2013-09-30'
categories:
- Algerian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/salade-de-poivrons-cuisine-algerienne_thumb.jpg
---
Hello everybody, 

here is an appetizer or salad, that I personally like a lot, when I go to Algeria, I only prepare this salad of peppers, or as it is called the hmiss, or hmiss, especially with the delicious and fragrant Algerian pepper, grilled on the tri-foot, or for people who like to barbecue, drizzled with a nice drizzle of olive oil, and accompanied by a mellow [ matlou3 ](<https://www.amourdecuisine.fr/article-41774164.html>) , or [ Lebanese bread ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html>) , [ pita bread ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html>)

I do not often have pepper salad here in England, because bell pepper for me is just sweet, so it completely changes the taste of hmiss. 

but Lunetoiles doing it yesterday, to love to share with us, his photos and his recipe.   


**hmiss, pepper salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/salade-de-poivrons-cuisine-algerienne_thumb.jpg)

Recipe type:  salad  portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 6 peppers of different colors 
  * a little parsley 
  * 4 cloves of garlic 
  * 3 tablespoons olive oil 
  * 1 tablespoon of vinegar 
  * ½ teaspoon of salt 
  * black pepper 



**Realization steps**

  1. Preheat the oven to 225 ° C. 
  2. Wash the peppers. Cover a baking sheet with aluminum foil and add the peppers. 
  3. Grill about 30 minutes, rotating the peppers so that their skin is black on the whole surface. 
  4. Take the plate out of the oven. Wrap the peppers well with the foil on which they have grilled and let cool. 
  5. As soon as the peppers are cold, remove the blackened skin and empty them. 
  6. Cut the peppers into cubes and transfer to a bowl. 
  7. Sift the juice and pour over the peppers. 
  8. Add olive oil, vinegar, chopped parsley and pressed garlic. 
  9. Season with salt and pepper. Put in the fridge. Serve very fresh. 



I really want to plunge my piece of cake matlou3 in, what is it for you? otherwise, sorry if I did not respond to your comments, or if I did not put them online, sorry if I did not jump on your blog for a while now, I'm in full preparation for my trip to Algeria the end of this week, I can not stop running in all directions for the purchase of children's summer clothes, especially that here it is really cold, so my children have nothing of clothing light for a warm climate. des que j’arrive en Algerie incha Allah, je reprendrai le chemin de vos blogs, je répondrai aux coms, donc attendez moi..!! 
