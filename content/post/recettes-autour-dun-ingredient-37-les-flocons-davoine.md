---
title: 'recipes around an ingredient # 37 oatmeal'
date: '2018-02-18'
categories:
- Coffee love of cooking
tags:
- Pastry
- desserts
- Cakes
- pies
- Shortbread
- pies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg)

source: Pinterest 

Hello everybody, 

For this round she offers us: **oatmeal**

![flakes](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/flocons.jpg)

and that's what she tells us: 

I'm sure you'll have a lot of ideas. 

Be careful the month of February is short, so hurry up to register. 

I will take my computer with me my week of vacation to update the list as and when. 

**_The principle of the game:_ **

2 - Make a new recipe (recipes are not accepted) around the star ingredient ie for this month **oatmeal.**

4 - Publish the article on **March 3 at 8 AM** . 

5 - In the article you must mention: 

\- The initiators of the game **[ Samar ](<https://www.amourdecuisine.fr/article-soupe-aux-pois-cassees-bissara.html>) and [ Soulef ](<https://www.amourdecuisine.fr>) **

\- Add the list of participants with the titles of their recipe 

If despite your registration, you have not managed to make your recipe, contact me before the 3 so that I remove you from the list and that I inform the other participants. 

_New since edition # 12_

A Facebook page has been created so that the participants the URL of their blog and the title of their recipe, and this, to facilitate the task of the godmother and participants. 

_Voici les différents ingrédients et leurs marraines :_
