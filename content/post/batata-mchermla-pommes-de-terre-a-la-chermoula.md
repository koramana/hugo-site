---
title: batata mchermla, potatoes with chermoula
date: '2018-05-13'
categories:
- Algerian cuisine
- Dishes and salty recipes
tags:
- Ramadan 2018
- Vegetarian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/batata-mchermla-pommes-de-terre-%C3%A0-la-chermoula-768x1024.jpg
---
![batata mchermla, potatoes with chermoula](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/batata-mchermla-pommes-de-terre-%C3%A0-la-chermoula-768x1024.jpg)

##  batata mchermla, potatoes with chermoula 

Hello everybody, 

I really like batata mchermla, potatoes to chermoula! I remember when we were little, when my mother did not know what to do to eat, we asked for fries, or then the potato has charmoula, "Batata mchermla". 

This dish goes very well with roast chicken, or merguez, as we were introduced by my mother. 

So we go to the recipe: for 4 people 

**batata mchermla, potatoes with chermoula**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/batata-mchermla-pommes-de-terre-%C3%A0-la-chermoula-2.jpg)

Recipe type:  dish, entree  portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 500 g potatoes 
  * a little coriander 
  * a little flat parsley. 
  * 1 clove of garlic 
  * 2 tbsp. paprika 
  * 2 tbsp. cumin 
  * 2 tbsp. lemon juice 
  * 3 c. tablespoon of olive oil 
  * Salt and pepper from the mill 



**Realization steps**

  1. Peel the potatoes, wash them and dry them. 
  2. Cut them into 2 cm cubes. 
  3. salt and pepper, and pass them by steam. 
  4. Now prepare the chermoula, crush the garlic, and sauté in a pan with a little oil, watch for it does not burn. 
  5. add the chopped parsley and cilantro, then the spices, while remuanat with a wooden spoon. 
  6. stir in the cubes of boiled potatoes, sauté a little 
  7. And here it is ready, adjust the salt and serve with some sprigs of coriander and / or parsley and pepper. 


