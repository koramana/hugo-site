---
title: Bavarian pineapple agar agar
date: '2010-06-24'
categories:
- gateaux algeriens au glaçage
- Algerian cakes- oriental- modern- fete aid
- gâteaux sablés, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/06/115-copie-111.jpg
---
& Nbsp; & Nbsp; Hello, I will take my exam in 3 hours, and I fly to Algeria a little later, I do not tell you my condition, nor the state of the house. I put you a recipe, and I tell you "See you tomorrow" & nbsp; a simple recipe that does not require too much time, apart from the dissolution of agar agar. ingredients: a sponge cake - 2 eggs - 60 gr of sugar - 50 gr of flour - 1/2 of yeast - 10 gr of almond mousse pineapple: - 250 ml of fresh cream mounted in chantilly & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.7  (  1  ratings)  0   
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/06/115-copie-111.jpg) 
</td>  
<td>

Hello, I will take my exam in 3 hours, and I fly to Algeria a little later, I do not tell you my condition, nor the state of the house. 

I put you a recipe, and I tell you "See you tomorrow" 


</td> </tr> </table>

a simple recipe that does not require too much time, apart from the dissolution of agar agar. 

ingredients: 

a sponge cake 

\- 2 eggs 

\- 60 gr of sugar 

\- 50 gr of flour 

\- 1/2 cac yeast 

\- 10 gr of almond 

pineapple mousse: 

\- 250 ml of whipped cream 

\- 60 grams of sugar 

\- 200 grams of pineapple (canned, drained, and mashed) 

\- 1 case of agar agar 

pink wafers, otherwise pink cookies dip in the pineapple juice. 

mirror: 

\- 100 ml pineapple / orange tropicana juice 

\- 1 cup agar agar 

** preparation of the sponge cake:  **

Preheat the oven to 180 °. Separate the whites from the yolks. Beat the egg whites until stiff, adding the pinch of salt. When they are firm, gradually add the sugar and continue whisking. Suddenly, add the egg yolks. Beat again. Add the flour in the rain while continuing to whisk with the mixer. Add the almond. 

Once the dough is homogeneous, pour it into a mold butter and flour or line with baking paper and bake for 20 minutes. the sponge cake will be colored, it is cooked when the edges retract the walls of the mold. remove the cooked layer and sprinkle the sponge cake with an orange juice 

** preparation of the pineapple mousse:  **

put the cold cream very cold in chantilly, with the sugar. 

melt the agar agar so 2 cases of water or fruit syrup, over low heat until it melts completely, add the mashed fruit, always on the heat, until the agar agar mix well with mashed potatoes. 

let cool a little, and then add it gently to the whipped cream, and pour half on your sponge cake, place the pink biscuits and sprinkle them with a little juice of your taste, or then the pink wafers, pour the other half foam and place in cold until it takes. 

** mirror preparation  ** : 

melt the agar agar in the pineapple juice, add a red dye, carry over low heat until the agar agar melts, let cool a little and pour over the mousse. 

bonne dégustation 
