---
title: tbikha, Algerian vegetable gardener
date: '2016-04-04'
categories:
- cuisine algerienne
- Healthy cuisine
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tbikha-alg%C3%A9roise-jardiniere-aux-l%C3%A9gumes-de-printemps.jpg
---
![tbikha, Algerian vegetable gardener](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tbikha-alg%C3%A9roise-jardiniere-aux-l%C3%A9gumes-de-printemps.jpg)

##  tbikha, Algerian vegetable gardener 

Hello everybody, 

Here is a dish that evokes a lot of memories at home, the **tbiykha** or **Tbikha** which is a **planter** rich in spring vegetables, beans, artichokes, and peas. When I was little, I was looking forward to the season of artichokes and beans to taste this dish, it's not like now, we can find these vegetables all year long especially frozen. 

For once, since I was in England, I had the right to taste with 100% fresh products, and I thank my father who sent me artichokes (I would rather say baby artichokes, because they were small, and very melting) and fresh beans from Algeria. When the peas, I found on the supermarket, my daughter was happy to shell them while enjoying a few pieces, because these peas were very sweet. 

[ ![tbikha, Algerian vegetable gardener](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tbikha-alg%C3%A9roise-jardiniere.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tbikha-alg%C3%A9roise-jardiniere.jpg>)   


**tbikha, Algerian vegetable gardener**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tbikha-alg%C3%A9roise-jardiniere.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 500 gr of shelled peas 
  * 500 gr of artichokes 
  * 500 gr whole fresh beans 
  * 2 cloves garlic 
  * 1 hot pepper (optional) 
  * salt and black pepper 
  * paprika 
  * a small handful of rice 
  * 3 tablespoons extra virgin olive oil 
  * 1 tbsp of concentrated tomato 



**Realization steps**

  1. in the mortar, crush the garlic with the hot pepper, salt and black pepper 
  2. Clean the beans, cut them while keeping their skin. 
  3. Rinse the peas and cut the artichokes into four (mine were small, I left whole) 
  4. In a pot with a thick bottom, place the beans, peas, oil, dersa (crushed garlic mixture) 
  5. let it simmer for a few minutes, then add the water (just to cover the vegetables) 
  6. Halfway through cooking, add the artichokes, and the tomato paste, dissolved in a little boiling water. 
  7. Simmer on low heat until cooked, while watching so that it does not dry and burns. 
  8. when all is well cooked, add the rice and coriander and leave on low heat. The sauce will be partially reduced, but not floating. 



![tbikha, Algerian vegetable gardener](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tbikha-alg%C3%A9roise-jardiniere-aux-l%C3%A9gumes-de-printemps-1.jpg)
