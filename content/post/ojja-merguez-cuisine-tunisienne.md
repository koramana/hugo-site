---
title: ojja merguez / Tunisian cuisine
date: '2015-08-26'
categories:
- Tunisian cuisine
tags:
- Full Dish
- Aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/ojja-merguez-004.CR2_11.jpg
---
#  ![ojja merguez / Tunisian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/ojja-merguez-004.CR2_11.jpg)

##  ojja merguez / Tunisian cuisine 

Hello everybody, 

Here is a recipe of Tunisian ojja merguez that rocked my childhood, especially in the hot summer days, when my mother was in the course of ideas .... 

a recipe very easy, very quick to realize, but very rich in taste and flavors ... this vioja in base is a tchakchouka, or chekchouka to the Algerian, but to which one adds merguez 

**ojja merguez / Tunisian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/ojja-merguez-007.CR2_1.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * merguez between 4 and 6 according to your taste 
  * 4 to 5 eggs depending on the number of people 
  * olive oil 
  * 1 medium onion 
  * 2 garlic cloves crushed with garlic press 
  * 2 large sweet peppers 
  * 2 beautiful tomatoes 
  * salt, black pepper, coriander powder 
  * fresh thyme. 
  * 1 teaspoon harissa 



**Realization steps**

  1. clean the onion, and cut it into thin slices. 
  2. Fry the onion and crushed garlic in a pan with a little olive oil until the onions become translucent. 
  3. Add the peppers cut into strips and let them simmer. 
  4. add salt, black pepper and coriander powder. 
  5. add the chopped and degraded tomato and the harissa, and a little water, if the tomato is not too juicy. 
  6. Cover everything and cook for about 10 minutes. 
  7. in another pan, cook the merguez. 
  8. cut the merguez into medium pieces and add to the sauce. 
  9. Make small wells with a spoon in the sauce and break the eggs over and let the eggs cook (do not mix) for a few minutes 
  10. Serve immediately 



![ojja-merguez - 009.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/ojja-merguez-009.CR2_1.jpg)
