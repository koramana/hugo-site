---
title: chtitha sardines
date: '2017-04-06'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sardine-en-sauce-tomate-031_thumb_111.jpg
---
##  chtitha sardines 

Hello everybody, 

I'm in full mhadjeb, so no time to write a new recipe, but I do not leave you today without an article, so here I go back to you a delicious dish of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) . 

it is true that this recipe is very very good, and if you do not like too much sardines fries this recipe is good for you. 

and as here it is a sardine dish with tomato sauce, I pass you, if you like: [ baked sardine ](<https://www.amourdecuisine.fr/article-sardines-au-four-97716578.html>) (a treat this dish)   


**chtitha sardines**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sardine-en-sauce-tomate-031_thumb_111.jpg)

**Ingredients**

  * 500 grs of sardines 
  * 4 cloves of garlic 
  * 1 small onion (very small or a quarter of a large onion) 
  * 4 beautiful tomatoes 
  * ½ tablespoon of tomato paste 
  * 1 bay leaf 
  * 1 little thyme 
  * black pepper 
  * cumin 
  * ginger 
  * salt 
  * 3 tablespoons oil 
  * ½ glass of water 



**Realization steps**

  1. put the oil in a small pot. 
  2. in a pestle crush garlic, onion, cumin, salt and spices, 
  3. fry this mixture in the oil. 
  4. crush and cut tomato, add to this meal and cook a little, add tomato paste, bay leaf and thyme. 
  5. cook a little add ½ glass of water. 
  6. clean the sardines, remove the head and stop it, if you can not stand too, remove the tail. 
  7. and start putting the sardines in the sauce, placing 3 to 5 sardines on top of each other. when the sauce starts to boil. 
  8. cook for a maximum of 15 minutes over low heat. 
  9. and garnish with a little parsley, and why not a lemon juice. 



regalez vous et à bientot, bisous 
