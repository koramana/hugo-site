---
title: cake le castel, castelnaudary
date: '2012-11-18'
categories:
- cuisine algerienne
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/russes1_thumb1.jpg
---
##  cake le castel, castelnaudary 

Hello everybody 

I remember when my father bought a box of cake, he liked to vary and sometimes he only bought 3 pieces of this delight, so I do not tell you the war, this time, I say to my brothers and sisters: "I eat it all alone, na na nanana, hihihihihiihi 

no I'm kidding, in truth I enjoy the occasion of the party at the school of rayan and I prepare this delight 

![russes1](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/russes1_thumb1.jpg)

**cake le castel, castelnaudary**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/russes4_thumb1.jpg)

**Ingredients**

  * 300 gr unhunted almonds 
  * 150 gr of flour 
  * 1 C. yeast 
  * 250 gr of butter 
  * 250 gr of icing sugar 
  * 5 egg whites 

ingredients for butter cream: 
  * 2 eggs 
  * 220 gr icing sugar 
  * 250 gr of butter 



**Realization steps** preparation of the butter cream 

  1. beat the eggs and the icing sugar in a bain-marie for 15min 
  2. then let cool, then stir in the butter and mix with the mixer 
  3. and leave aside 

preparation of the dough: 
  1. make the egg whites rise. 
  2. mix the almond powder that should not be too thin with the sugar yeast, flour and melted butter, 
  3. then incorporate the white in snow gently 
  4. n suites put in a dish (for biscuit roulette), going to the oven, lined with parchment paper and bake at 180 ° 
  5. once cooked let cool cut the cake in 2 spread the butter cream and close with the other half and spread over the butter cream 
  6. toast almonds and put on and sprinkle with icing sugar let rest and sprinkle again 
  7. and cut square or rectangle according to your tastes 



merci a tous et à toutes pour votre visite, et vos encouragement. 
