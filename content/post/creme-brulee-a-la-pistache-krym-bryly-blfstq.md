---
title: crème brûlée with pistachio كريم بريلي بالفستق
date: '2016-10-26'
categories:
- panna cotta, flan, and yoghurt

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-brulee-a-la-pate-de-pistache_thumb1.jpg
---
##  crème brûlée with pistachio 

Hello everybody, 

after the delicious [ creme brulee ](<https://www.amourdecuisine.fr/article-creme-brulee-recette-creme-brulee-115598588.html>) with sweetened condensed milk, here is another delicious crème brûlée with pistachio, 

A recipe that in addition to being good, the color is to sublimate your little family, or adorn the table that you present to your guests. 

a recipe that we share with us Lunetoiles, and that I thank a lot.   


**crème brûlée with pistachio كريم بريلي بالفستق**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-brulee-a-la-pate-de-pistache_thumb1.jpg)

portions:  6 to 8  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients**

  * 200 ml whole liquid cream 
  * 200 ml of whole milk 
  * 80 g caster sugar 
  * 30 g pistachio paste 
  * 7 egg yolks 
  * brown sugar 
  * green dye (optional) 



**Realization steps**

  1. Preheat the oven to 100 ° C (Th.3). 
  2. Put the milk and cream to boil with the pistachio paste. Let cool. 
  3. Blanch the yolks with the sugar, then mix with the milk / cream / pistachio paste mixture. 
  4. If you wish, add a few drops of green dyes. 
  5. Then fill the ramekins. 
  6. Bake in a bain-marie for 45 minutes or until, when you move the ramekins, only the heart shakes a little. 
  7. Let cool, and place at least 2 hours in the refrigerator. 
  8. before serving, sprinkle with brown sugar and caramelise with a torch. 


