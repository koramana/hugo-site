---
title: Mascarpono-Tobleronian Delight
date: '2011-06-09'
categories:
- gateaux algeriens au miel
- Algerian cakes- oriental- modern- fete aid

---
A delight that I prepared this week, and that we really liked, my children and I, is a Bavarian, made of a mascarpone-based mousse (you know how much I love mascarpone I must say that this foam I took Lamu (thank you my dear for this idea), and thank you Agenda, because it's bitch his blog that I recovered this recipe. the second layer in it is composed of two foams, one based on white chocolate, and the other with Toblerone and this recipe I took from Muriel. so the & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

so the marriage of these two beautiful mousses to give a taste really delicious, and it will be a dessert to redo with great pleasure. 

Preheat the oven to 180 °. Separate the whites from the yolks. Beat the egg whites until stiff, adding the pinch of salt. When they are firm, gradually add the sugar and continue whisking. Suddenly, add the egg yolks. Beat again. Add the flour in the rain while continuing to whisk with the mixer. Add the almond. 

Once the dough is homogeneous, pour it into a mold butter and flour or line with baking paper and bake for 20 minutes. the sponge cake will be colored, it is cooked when the edges retract the walls of the mold. remove the cooked layer and sprinkle the sponge cake with an orange juice (you can sprinkle with a syrup that you have already prepared) 

I garnished my sponge cake with some pitted Cherries 

In a bowl, beat the yolks and sugar well, add the cold mascarpone, whisk again. 

Heat the 2 tablespoons of water in a small bowl in the microwave (without boiling!), Dissolve the gelatin, mix until a liquid is obtained (there must be no piece of gelatin). Stir in mascarpone, whisk to homogenize everything. Beat the egg whites in very firm snow. When they are mounted, gradually incorporate the icing sugar while whisking until a smooth, firm and shiny meringue is obtained. Whisk the mascarpone whites very gently with a spatula, lifting the mass well. This step can take a long time because the mascarpone preparation is thick. 

Add the almond powder, and pour on the sponge cake. spawn for at least 1 hour. 

beat the egg white in firm snow, and whip the egg yolk with the sugar. in a saucepan melt the toblerone with half of the butter, and in another saucepan melt the white chocolate with the other half of the butter. 

add half of the egg yolk / sugar mixture and then add half of the egg white just gently to form a mousse. 

do the same thing with white chocolate, ie add the egg yolk / sugar mixture, then add the egg white very slowly. 

take your two chocolate mousses, pour them on the first mascarpone mousse, to form a marbling to your taste, put in the fridge, for 2 hours or more. 

Bonne dégustation 
