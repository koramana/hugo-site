---
title: Black Olive Tapenade
date: '2017-12-21'
categories:
- dips and sauces
- salads, salty verrines
tags:
- Christmas
- Holidays
- Aperitif
- Amuse bouche

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tapenade-020.CR2_.jpg
---
![Black Olive Tapenade](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tapenade-020.CR2_.jpg)

##  Black Olive Tapenade 

Hello everybody, 

tapenade, or black tapenade, is a recipe that was created by chef Meynier, of the Maison Doree restaurant in Marseille, in 1880, which consists of equal parts of black olives and capres, to which are added, fillets anchovies, and tuna matinee. the chef had prepared it to garnish the moities of hard-boiled egg (mimosa egg). 

for my part, I prepared this tapenade of black olives that I like, for another recipe that I will publish later, and we feasted my guests and me. 

It's simple and super easy then to you the recipe for the black olives tapenade: 

![tapenade-026.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tapenade-026.CR2_.jpg)

**Black Olive Tapenade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tapenade-030.CR2_.jpg)

Recipe type:  dip, sauce, filling  portions:  4  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 200 gr of pitted black olives 
  * 6 anchovy fillets in oil 
  * 1 tablespoon of capers 
  * 1 clove of garlic 
  * 3 tablespoons olive oil 
  * 1 teaspoon lemon juice (more or less according to your taste) 



**Realization steps**

  1. deglaze and finely chop the garlic clove. 
  2. in the bowl of a blender, place the anchovy fillets, capers, minced garlic clove, black olives and olive oil and mix well enough. 
  3. Add the lemon juice. 
  4. mix well, and keep in a jar. 



![tapenade-023.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tapenade-023.CR2_.jpg)
