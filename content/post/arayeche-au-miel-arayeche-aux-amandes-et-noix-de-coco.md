---
title: arayeche with honey - almond and coconut arayeche
date: '2015-07-04'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- sweet recipes
tags:
- Algerian cakes
- Cakes
- Algeria
- Aid cake
- baklava
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-miel3_thumb1.jpg
---
##  arayeche with honey - almond and coconut arayeche 

Hello everybody, 

Arayeches are very beautiful Algerian cakes, usually coated with a generous layer of royal glaze, to balance the taste of sugar that is lacking in the dough, usually prepared without sugar. It's a super delicious cake, besides, my daughter likes it a lot, but I do not like all this sugar on top, that's why I prefer this version of arayeches with honey, you can see the version: [ arayeches with honey and almonds ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-gateaux-algeriens.html>) , and I share with you that of a friend and reader: " **leila Saadia** "Who made arayeches with almonds and coconut while being inspired by my recipe: [ arayeche with honey ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-60064269.html>)

Leila Saadia is very talented, and she always likes to put her grain of salt in the recipes, so I'm happy to fill you the leila recipe. 

for more algerian cake recipes visit: 

the category: [ modern Algerian cakes, parties and weddings ](<https://www.amourdecuisine.fr/gateaux-algeriens-orientales-modernes-fete-aid>)

or the index: [ Algerian cakes ](<https://www.amourdecuisine.fr/article-gateau-algerien-et-gateaux-orientals-pour-laid-el-fitr-2014.html>)

**arayeche with honey - almond and coconut arayeche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-miel3_thumb1.jpg)

portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 4 measures of flour (measure is any utensil you can use so that it becomes the measuring tool in all the cake) 
  * ½ measure of melted margarine 
  * Orange Blossom 
  * white and orange dye. 

prank call: 
  * 1measured almonds 
  * 2 measures of ground coconut 
  * 1 measure of sugarsucre 
  * water + a little orange blossom 



**Realization steps**

  1. Pour the flour into a terrine, add the margarine, mix well with the fingertips 
  2. divide the dough into two parts 
  3. for the one you are going to color in white add the dye, then pick up the dough with flower water, add a little water to pick up, continue to knead a few seconds. 
  4. for the other color, take the other half of the shortbread, add the dyes to the orange water until the desired color is obtained, and start to collect the dough with this water, add the water if you need, and knead a few seconds. 
  5. cover your dough and let it sit on the side. 
  6. for stuffing mix almonds, coconut pick up with water and orange blossom. 
  7. lower the dough and pass it to the machine from number 3 to number 7, cut two circles of dough, the first of the dark color with a cookie cutter 8 cm in diameter. 
  8. the second of the white color with a 9 cm diameter cookie cutter. 
  9. then superimpose them, the big one on the small 
  10. take a ball of 25 grams of the almond stuffing, form a star, place it on the circle of the dough, and pick up the sides of the dough to coat the almond stuffing, without covering it completely, press well so that the dough is well stick to the stuffing, and form well the ends of the star. 
  11. decorate with small flowers 
  12. and cook for 10 minutes in a preheated oven at 170 degrees C. 
  13. take it out of the oven, let cool a little, and emerge your cakes in a little hot honey. 
  14. now decorate the small flowers with pearls. 
  15. enjoy. 



merci pour tout vos commentaires qui vont surement faire un grand plaisir a leila Saadia 
