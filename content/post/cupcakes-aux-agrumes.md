---
title: citrus cupcakes
date: '2015-04-05'
categories:
- Cupcakes, macarons, et autres pâtisseries
tags:
- Easy cooking
- desserts
- Pastry
- Algerian cakes
- Inratable cooking
- To taste
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cupcakes-aux-agrumes.jpg
---
[ ![citrus cupcakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cupcakes-aux-agrumes.jpg) ](<https://www.amourdecuisine.fr/article-cupcakes-aux-agrumes.html/cupcakes-aux-agrumes-1>)

##  citrus cupcakes 

Hello everybody, 

Here is a recipe for cupcakes that will not leave you indifferent, they are super well done, plus it was the first test of my friend: A moment of pleasure, these pretty cupcakes citrus, it must be said that the decoration is especially A professional's work !!!. 

These cupcakes are just beautiful and too good, especially the butter cream flavored with citrus, I like this recipe, besides I immediately realize the butter cream to garnish a cupcake that I do for my neighbor ... cream is a real treat, I recommend this recipe. 

**citrus cupcakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cupcakes-aux-agrumes-1.jpg)

portions:  12  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** For the base dough at Cupcakes: 

  * 150 gr sugar 
  * 125 gr of melted butter 
  * 3 eggs 
  * 250 gr of flour 
  * 1 sachet of baking powder 
  * 1 pinch of salt 
  * zest 1 lemon and 4 beautiful mandarins for these citrus cupcakes. 

For the lemon-mandarin butter cream: 
  * 250 gr of butter at room temperature 
  * 400 gr icing sugar 
  * yellow dye 
  * 5 tablespoons of citrus cream made before 

citrus cream: 
  * 1 glass of citrus juice 
  * 4 egg yolks 
  * 150 gr of sugar 
  * 80 gr of butter 



**Realization steps** Preparation of cupcakes: 

  1. Preheat the oven to 180 ° C 
  2. Wash the tangerines well and boil them. 
  3. Meanwhile, beat the butter and sugar until you get an ointment, 
  4. add the eggs one by one, the zest of the lemon. 
  5. Drain the tangerines without squeezing them and cut them in half to make sure there are no pips and mix them together. 
  6. add the compote obtained to the previous mixture then gently incorporate the flour, salt and yeast. 
  7. Fill ⅔ of the paper or silicone molds with the preparation. 
  8. place in preheated oven at 180 degrees C for 20 min. 

Preparation of citrus butter cream: For citrus cream: 
  1. Heat the citrus juice, in the meantime beat the egg yolks with the sugar 
  2. gently pour the citrus juice, put it all in a saucepan over low heat and mix until obtaining a smooth cream 
  3. add the pieces of butter, place it all in the refrigerator for at least 2 hours. 
  4. Beat 250 grams of butter at high speed so that it becomes creamy 
  5. slowly pour in the icing sugar and continue to whisk everything to become sparkling, 
  6. at the end, introduce the 5 tablespoons of citrus lacrème. 
  7. Put the butter cream obtained in a pastry bag and decorate the cupcakes. 



[ ![citrus cupcakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cupcakes-aux-agrumes-3.jpg) ](<https://www.amourdecuisine.fr/article-cupcakes-aux-agrumes.html/cupcakes-aux-agrumes-3>)
