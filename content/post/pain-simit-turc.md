---
title: Turkish simit bread
date: '2017-05-20'
categories:
- Mina el forn
- bread, traditional bread, cake
tags:
- shaping
- Boulange
- Algeria
- Galette
- Picnic
- House Bread
- Bakery

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pain-simit-turc-1.jpg
---
[ ![Turkish simit bread 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pain-simit-turc-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pain-simit-turc-1.jpg>)

##  Turkish simit bread 

Hello everybody, 

When I went to Turkey a few years ago, we had a great time enjoying the Simit breads, so my son, he ate it without moderation, he was only 3 years old at the time, those little half-brioche crowns were his little consolation when he had a hollow, or just to taste it while he was in his stroller, and we were wandering around **Manavgat** »Or« **Side** To go around the market. 

In any case, it was not easy to see this delicious bread in his hands, even we did not even know how these 10 crowns that had been bought by the first merchant disappeared so quickly. 

Anyway, at the sight of these pictures of my friend **Oum Thaziri,** I do not tell you the flash of memory that passed before my eyes, I could stay all night to tell you our little walks, the dishes we tasted, Turkish steam, and massage .... But I do not think you're interested in that, so better that I go to the recipe, hihihih 

Otherwise, I invite you to watch the new video on youtube: 

**Turkish simit bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pain-simit-turc.jpg)

portions:  12  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * 500 gr of flour 
  * 3 tablespoons of oil 
  * 1 tablespoon of sugar 
  * 1 teaspoon of salt 
  * 1 tablespoon instant baker's yeast 
  * 1 glass of warm water (220 ml) 
  * 1 glasses of warm milk 
  * molasses of grapes or date jam (رب التمر) 
  * sesame seeds 



**Realization steps**

  1. sift the flour, add salt, sugar and yeast 
  2. mix, add the oil, and pick up the dough with the mixture of milk and water, until you have a very soft and supple dough. 
  3. Pet the dough very well, until it becomes very tender. 
  4. let the dough rise well, then degas it. 
  5. shape a pudding or baguette, then cut it into equal pieces 
  6. to form well-shaped sausages of almost 22 cm, 
  7. fold in half and roll it up on itself then reattach the ends to form a crown.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pain-simit-turc-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pain-simit-turc-.jpg>)
  8. lighten the molasses grapes or date jam with a little water 
  9. dip the crowns in it, then cover with sesame seeds 
  10. place the wreaths on a tray lined with baking paper. 
  11. let rise just a little and then cook in a preheated oven at 200 degrees C for 15 minutes 



[ ![Turkish simit bread 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pain-simit-turc-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pain-simit-turc-2.jpg>)
