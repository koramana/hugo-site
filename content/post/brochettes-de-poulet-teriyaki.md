---
title: teriyaki chicken skewers
date: '2012-05-27'
categories:
- Algerian cakes- oriental- modern- fete aid
- gâteaux sablés, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/brochettes-de-poulet-a-la-sauce-teriyaki_thumb1.jpg
---
![chicken skewers with teriyaki sauce](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/brochettes-de-poulet-a-la-sauce-teriyaki_thumb1.jpg)

##  teriyaki chicken skewers 

Hello everybody, 

a bright sun these days at home, so not too the desire to turn in the kitchen, so for meal times, it's obviously quick recipes, for which must not stay long in front of the stove ... 

So this time, and with my spices, I left for an Asian adventure to prepare the chicken breast that I bought yesterday, so here are these delicious kebabs chicken teriyaki sauce, for me I choose the orange flavor and lemon.   


**teriyaki chicken skewers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/brochettes-de-poulet-teriyaki_thumb.jpg)

**Ingredients**

  * 2 tablespoons olive oil 
  * 1 tablespoon finely grated ginger 
  * 1 tbsp orange zest 
  * 1 clove of chopped garlic 
  * 10ml of vinegar 
  * 3 tablespoons soy sauce 
  * 2 tablespoons sugar 
  * 2 tablespoons orange marmalade 
  * 1 tbsp freshly squeezed orange juice 
  * 1 tablespoon of lemon juice 
  * 1 tablespoon of honey 
  * 1 pinch of paprika 
  * 1 chicken breast cut into cubes 
  * 2 tablespoons of sesame seeds. 



**Realization steps**

  1. method of preparation: 
  2. prepare the sauce first, place a small saucepan over medium heat and add the olive oil. 
  3. Add ginger, orange peel and garlic, and cook, stirring frequently, for 1-2 minutes, or until fragrant. 
  4. Stir in vinegar, soy sauce, sugar, orange marmalade, orange juice, honey and paprika. Bring to a boil and continue cooking for 3-5 minutes, or until sauce has slightly thickened. 
  5. Set aside half of the sauce to serve with the cooked chicken, the other half is for brushing the kebabs. 
  6. place the chicken cubes in half of the sauce 
  7. Preheat the griddle, oil lightly 
  8. put the chicken cubes in the skewers, then sprinkle with sesame seeds. 
  9. place the skewers strung on the griddle and cook, giving them a quarter turn every 2-3 minutes, and each time using a brush with a little sauce on top, until they are lightly cooked, and the caramelized sauce, 
  10. Transfer the skewers to a dish and let rest for 5 minutes before serving with reserved sauce 


