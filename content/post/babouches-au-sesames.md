---
title: sesame babouches
date: '2016-10-15'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian cakes
- Morocco

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/babouches-au-s%C3%A9sames-1.jpg
---
![slippers au sesames-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/babouches-au-s%C3%A9sames-1.jpg)

##  sesame babouches 

Hello everybody, 

sesame slippers or blighates are a pretty Moroccan pastry that I had the pleasure of tasting several times at my Moroccan friends. 

These sesame slippers by cons are an achievement of our dear Lunetoiles, who prepared them for the feast of Eid el Fitr, but because of my departure in Algeria and return to major work here at home, I have not had the chance to take a tour on the photo album sent by Lunetoiles to choose the best photos. 

That's what Lunetoiles tells us about these delicious sesame slippers: 

> It's a recipe that I've already prepared before we know each other, it's super good, everyone loves it at home, and almost everything else that does not like sesame seeds. These slivers are super crispy and so honeyed that it looks like sesame nougatine. it's a real treat !! you should try them one day 

**sesame babouches**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/babouches-au-s%C3%A9sames.jpg)

**Ingredients**

  * 500 g of golden sesame seeds 
  * 500 g of flour 
  * 300 to 350 ml of cold milk 
  * 1 teaspoon of orange blossom 
  * 2 tbsp. soft butter 
  * 1 pinch of salt 
  * Oil for frying 
  * Honey for coating 
  * Fried almonds for the decor 



**Realization steps**

  1. Mix all the ingredients, the assembly of the flour is done with cold milk 
  2. Knead well and assemble the dough into a homogeneous ball   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/pate-babouches-au-s%C3%A9sames.jpg)
  3. Spread the dough without it being too thick 
  4. Cut out rounds with a small tea glass 
  5. Form the circle into a butterfly by pinching well in the middle. Solder well to prevent it from opening. 
  6. Fry in a bath of hot oil and not smoking   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/faconnage-babouches-au-s%C3%A9sames.jpg)
  7. Diving immediately in honey flavored with orange blossom, it is necessary that the honey be a little lukewarm. 
  8. Drain, then place in a large dish and decorate with preferably small fried almonds 



![slippers au sesames-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/babouches-au-s%C3%A9sames-2.jpg)
