---
title: Algerian cake easy and economical for the aid 2016
date: '2016-07-01'
categories:
- Index

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/whirl_thumb_11.jpg
---
![Algerian cake easy and economical for the aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/whirl_thumb_11.jpg)

##  Algerian cake easy and economical for the aid 2016 

Hello everybody, 

We always try to make the best cakes to present at parties or when we receive guests, but sometimes we look for recipes Algerian easy quick and economical. so here is a little index to help you in your research, a small click on the picture will take you to the cake recipe in question, I hope you will love the ride. 

These cakes are super easy to make, and they are not just cakes for the Eid party. These cakes are ideal for snacking, children love a lot, or for a bounty (lunch box). 

It's always good to have a small list of easy and economical cakes in your little cookbook, right? 

for even more, you can see the indexes of: 

[ Algerian cake ](<https://www.amourdecuisine.fr/gateaux-algeriens-orientales-modernes-fete-aid>)

[ index of Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

**Algerian cake easy and economical for the help**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/whirl_thumb_11.jpg)

portions:  30  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * flour 
  * sugar 
  * chocolate 



**Realization steps**

  1. pastry roller 
  2. entonoire 
  3. sieve 
  4. spatula 


