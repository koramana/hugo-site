---
title: Spanish omelette or zucchini tortilla
date: '2016-09-24'
categories:
- amuse bouche, tapas, mise en bouche
- cuisine diverse
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/OMELETTE-courgette-omelette-espagnole-0261.jpg
---
![Spanish omelette or zucchini tortilla](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/OMELETTE-courgette-omelette-espagnole-0261.jpg)

##  Spanish omelette or zucchini tortilla 

Hello everybody, 

Another easy recipe to make and very delicious, this pretty **Spanish omelette or zucchini tortilla** is a treat! 

I'm not going to tell you who has eaten this zucchini omelet, all by itself, because no one likes zucchini at home, but me, it's my favorite vegetable, I eat it with all the sauces. 

Anyway, I must tell you that I ate this egg cake with all the pleasure of the world ... .. The others are happy with the eggs on the plate .... they do not know what they are missing !!! ??? 

the Spanish omelette or the Spanish tortilla is more consistent than the French omelet, and you can put in an infinity of ingredients: potato (the most famous Spanish omelette), asparagus, peppers, mushrooms ...   


**Spanish omelette or zucchini tortilla**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/OMELETTE-courgette-espagnole-039.jpg)

Recipe type:  entree, dish  portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * a large zucchini cut into cubes 
  * 1 big sliced ​​onion 
  * 4 eggs 
  * 3 tablespoons of olive oil 
  * 2 chopped garlic cloves 
  * 2 tablespoons oregano 
  * salt pepper 



**Realization steps**

  1. Heat the oil in the pan and place the zucchini in cubes. 
  2. Fry while stirring. 
  3. Add salt, pepper, 
  4. add the onion until it becomes a bit translucent 
  5. add the garlic. 
  6. Cover and stew until vegetables are golden brown. 
  7. Beat the omelette eggs in a large salad bowl and pour the vegetables in them. 
  8. mix everything together and put it back in the pan a little oiled 
  9. Cook over medium heat, stirring the pan occasionally to prevent it from sticking. 
  10. when the omelette has taken a little, cross it in a wide, flat plate. 
  11. cook the omelette on the other side. Serve hot. 



{{< youtube iDjwFSy1Fn4 >}} 
