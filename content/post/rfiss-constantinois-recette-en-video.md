---
title: rfiss constantinois recipe video
date: '2017-10-21'
categories:
- Algerian cuisine
- dessert, crumbles and bars
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries
tags:
- Algerian cakes
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/rfiss-constantinois-recette-de-la-cuisine-algerienne-2.jpg
---
##  ![rfiss constantinois recipe video](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/rfiss-constantinois-recette-de-la-cuisine-algerienne-2.jpg)

##  Rfiss constantinois recipe video 

Hello everybody, 

rfiss constantinois recipe video: A delicious dish Constantinois, a sweet recipe that will not leave you indifferent, rfiss constantinois recipe video at your request. This recipe is a family legacy on the father's side. With my grandmother and my aunts at weddings or other ceremonies, we prepared a considerable amount, I helped as best I could, especially on the tasting side lol. 

At home, it is especially in Ramadan, or when I receive friends that I prepare Constantinois rfiss. Like my mother, I always prepare the slab "mbessa" in large quantities when I have time, I reduce it to powder I let cool, then I put in the freezer. Like that, if I have friends who come unexpectedly, I get out of the freezer, a generous steaming, a good dosage of honey, butter and sugar, and here is a small dessert to present to accompany a good mint tea. I confess that my friends ask for it and ask for it again, and they never leave empty-handed with her. 

I share with you today the recipe recipe rfiss Contantinois video of my grandmother, I realized for a Ramadan night with friends, we had a good time, besides I look at the photos, and I remember, my friends were the all the spotlight of the house so that I can make beautiful pictures (not as famous as I hoped) lol, that we had fun this day the , I told them, if there are no beautiful pictures, there is no rfiss, hihihih, They still eat, ok. 

You can see the video recipe of the constantinois rfiss below: 

**rfiss constantinois recipe video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/rfiss-constantinois-recette-de-la-cuisine-algerienne.jpg)

portions:  5-6  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients** for the galette mbessa, kesra mbessa: 

  * 4 measures of medium semolina (240 ml glass) 
  * 240 ml of oil + melted butter mixture 
  * Water + orange blossom water to pick up the dough 
  * ½ teaspoon of salt 

for the passage to steam: 
  * between 500 and 600 ml of milk (or as needed) 
  * 4 to 5 tablespoons of orange blossom water. 

for the rfiss: 
  * Semolina sugar according to taste 
  * Honey according to taste 
  * between 120 and 150 gr of butter (I confess that I let go with the butter, because it gives a good taste to the rfiss) 

To decorate 
  * icing sugar 
  * nuts kernels 



**Realization steps** Prepare the cake: 

  1. In a bowl, sear the semolina and the melted butter and oil mixture. 
  2. add the salt and pick up the dough without kneading it with the mixture of water and orange blossom water. let stand a little and spread the cake in a large pyrex mold, or a tray covered with aluminum foil. 
  3. cut squares in the cake.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/galette-mbessa-pour-rfiss.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/galette-mbessa-pour-rfiss.jpg>)
  4. Cook the cake in a preheated oven at 180 degrees C, until the patty turns a nice golden color 
  5. Let cool a little, and pass the pieces of cake to the mixer to have grains the size of couscous grains.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/mbessa-et-grains-de-rfiss.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/mbessa-et-grains-de-rfiss.jpg>)
  6. sieve for grains of the same size, if you have coarse grains you can always go back to the robot. 
  7. place the rfiss grains in a terrine.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/grains-de-rfiss.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/grains-de-rfiss.jpg>)
  8. Gradually and gently moisten these grains with milk and orange blossom water. (not all the quantity) 
  9. Steam the top of the couscous pot over boiling water for 20 to 25 minutes. 
  10. remove the grains from the rfiss for the trans-pour again in the terrine, and to wet them again with the rest of the milk and water of orange blossom. The mixture should be well moist but not sticky. 
  11. let stand a little, gently sand the grains between your fingers, and replace them in the top of the couscoussier for another steaming of 20 to 25 min. 
  12. When the grains are well cooked and tender in the mouth, put them in the terrine, add the butter in pieces while it's still hot, for it to melt, add the sugar and the honey. 
  13. sand with a wooden spoon, to spread the sugar, butter and honey ... 
  14. add and adjust the sugar and honey according to your taste. 
  15. during the presentation, decorate with walnut kernels and icing sugar. Indulge yourself and add your decorative touch. 



![rfiss constantinois recipe video](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/rfiss-constantinois-recette-de-la-cuisine-algerienne-1.jpg)
