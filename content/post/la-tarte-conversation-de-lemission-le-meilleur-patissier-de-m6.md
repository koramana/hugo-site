---
title: 'the conversation pie: from the show the best pastry chef of M6'
date: '2013-11-03'
categories:
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-conversation-2_thumb.jpg
---
[ ![pie conversation 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-conversation-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-conversation-2_2.jpg>)

Hello everybody, 

I suppose the vast majority of you, saw the show "the best pastry chef" on M6 yesterday, and the candidates had to prepare a pie conversation .... 

Lunetoiles could not resist the charm of this recipe, and here is his participation, hihihihihi, what do you say? 

I can not tell you much, on this pie, nor on the show "the best pastry" of the fact that I do not have French channels at home. 

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-conversation-1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-conversation-1_2.jpg>)

**the conversation pie: from the show the best pastry chef of M6**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-conversation-7_thumb.jpg)

Recipe type:  pie dessert  portions:  8  Prep time:  60 mins  cooking:  30 mins  total:  1 hour 30 mins 

**Ingredients** For quick puff pastry: 

  * 250g of flour 
  * 200 g of butter 
  * 5 g of salt 
  * 125 g of water 

For almond cream: 
  * 75 g of ointment butter 
  * 75 g caster sugar 
  * 75 g of almond powder 
  * 1 egg and a half (75g) 
  * 35 g whole whipping cream 
  * The zest of half an orange (optional) 

For the royal ice cream: 
  * 15 g of flour 
  * 125 g icing sugar 
  * 1 egg white 
  * 1 tbsp. coffee lemon juice 



**Realization steps**

  1. Step 1: the quick puff pastry: 
  2. Mix the butter in small cubes and the flour. Do not over-add the butter to the dough. 
  3. Add salt and cold water. 
  4. Give 5 rounds: Turn (fold three or four dough pieces after lying down). 
  5. Keep cool for 20-30 minutes. 
  6. Step 2: the almond cream 
  7. Whisk and without mixing the machine, the butter, the sugar and the almond powder. 
  8. Add the egg, the cream, then the zest. 
  9. Smooth the mixture. 
  10. Step 3: the sinking 
  11. Lower the dough to 2mm thick. 
  12. Put the dough in the mold (darken) buttered beforehand, leaving it to protrude all around. 
  13. Fill with almond cream. 
  14. Cut out another disc of dough a little smaller than the mold. 
  15. Wet the edges of the dough with a brush. 
  16. Place the puff pastry disk on the almond cream. 
  17. Pass the roller to weld and remove excess dough. 
  18. With the excess dough, prepare strips of finish. 
  19. Keep everything cool for 15 minutes. 
  20. Step 4: The royal ice 
  21. Sift 15g flour with 125g icing sugar. 
  22. Mix quickly with egg white. 
  23. Adjust the lemon. 
  24. Book. 
  25. Step 5: Dressage 
  26. Spread a layer of royal ice with the back of a spoon. 
  27. Arrange in strips loaves of puff pastry dough previously gilded with egg. 
  28. Refrigerate 30 minutes before cooking at 180 ° for 30 minutes. 
  29. Cool with decelerate. 



[ ![pie conversation 5](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-conversation-5_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-conversation-5_2.jpg>)
