---
title: tuna tart
date: '2014-09-16'
categories:
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tarte-thon-et-tomate_thumb.jpg
---
![tuna and tomato pie](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tarte-thon-et-tomate_thumb.jpg)

##  **tuna tart**

Hello everybody, 

an empty recipe fridge this beautiful tuna pie, of course better than: two tomatoes that start losing their beautiful shiny skin, a box of tuna half-finished, a little cream that I must consume before tomorrow. 

and if you like salty pies, quiches and salty cakes you can see: 

the category of [ cake, quiches and pies ](<https://www.amourdecuisine.fr/pizzas-quiches-tartes-salees-et-sandwichs>) . 

**tuna tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/tarte-thon-et-tomate.160x12011.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** the shortbread 

  * 250 grams of flour 
  * 120 grams of butter in pieces 
  * 1 egg yolk 
  * ½ cup of sugar 
  * ½ teaspoon of salt 
  * 50 ml of cold water (+ or - as needed) 

for the stuffing: 
  * ½ tuna can in oil, drained 
  * 3 medium-sized tomatoes 
  * 2 tablespoons mustard 
  * grated cheese 
  * 100 ml of fresh cream 
  * 2 eggs 
  * 1 pinch of nutmeg 
  * thyme 
  * Salt pepper 



**Realization steps**

  1. in the bowl of a blender, put the flour and butter in pieces, mix 
  2. add the egg yolk, sugar, and salt, then mix again 
  3. add the water in small quantities and mix, until the dough forms a ball (go there gently with the water) 
  4. Preheat the oven to 180 ° C. 
  5. Garnish the pie plate with the dough, 
  6. Drain the tuna, and wash the tomatoes then cut into slices, lightly soil let drain to reduce its water. 
  7. Spread pie crust with 2 tablespoons mustard, sprinkle with grated cheese, 
  8. spread the tomatoes and crumbled tuna and cover again with the remaining grated cheese. 
  9. whip cream, eggs, nutmeg, salt and pepper. 
  10. Pour this mixture over the tomatoes, and garnish with a little thyme. 
  11. cook at 200 ° C for 35 min. 



## 

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
