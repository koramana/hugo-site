---
title: Montecao with sesame seeds
date: '2016-09-19'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya
tags:
- Gateau Aid
- Aid cake
- Ramadan
- Ramadan 2016
- Pastry
- delicacies
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/montecaos-aux-grains-de-s%C3%A9sames-1.jpg
---
![montecaos-to-grain-de-sesames-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/montecaos-aux-grains-de-s%C3%A9sames-1.jpg)

##  Montecao with sesame seeds 

Hello everybody, 

These sesame seeds montecao are so good and melting that you will not stop at one piece. Do not say I did not warn you. 

For Eid el adha this year I admit to having a great laziness to make cakes, especially since I just returned from Cleveland where we spent two beautiful days. The room in the hotel had a splendid view of the athlete. While it was cloudy and a little cool, but the kids loved playing kites (me too to be frank) 

![montecaos-to-grain-de-sesames-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/montecaos-aux-grains-de-s%C3%A9sames-2.jpg)

**Montecao with sesame seeds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/montecaos-aux-grains-de-s%C3%A9sames-4.jpg)

portions:  40  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 700 g flour 
  * 125 g of butter 
  * ¼ liter of oil (I used melted smen) 
  * 120 g sifted icing sugar 
  * 100 g toasted sesame seeds 
  * cinnamon 



**Realization steps**

  1. Melt the butter in a saucepan and let it cool. 
  2. In a bowl, toss with a spatula, icing sugar, melted butter and add the oil. 
  3. Then add the grilled sesame seeds over low heat in a nonstick skillet. 
  4. Stir the flour little by little and gather without kneading. 
  5. Take a little dough (the size of a big walnut) and roll into a ball 
  6. Sprinkle the top with cinnamon. 
  7. Place them on an ungreased baking sheet or covered with baking paper. 
  8. Cook over low heat (th 145 ° C) for 15 minutes. 
  9. Wait for the cakes to cool before removing them from the plate, otherwise they will crumble. 



![montecaos-to-grain-de-sesames-3](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/montecaos-aux-grains-de-s%C3%A9sames-3.jpg)
