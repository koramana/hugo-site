---
title: crunchy nougat with sesame
date: '2008-01-08'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/01/t-IMGB51BA729_EF64_492B_912AA27F33BBF30821.jpg
---
I was told that sesame is very good and that it opens the appetite of children, so I prepared these nougats for rayan: 

![imgb51ba729_ef64_492b_912aa27f33bbf3082](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/t-IMGB51BA729_EF64_492B_912AA27F33BBF30821.jpg)

For a good box 

Ingredients: 

  * 400g of sugar 
  * 100g of sesame seed 
  * 1citron 
  * 2 soupspoons of liquid honey 



Put the sugar in a heavy-bottomed saucepan with 1/2 liter of water and the juice of a lemon. 

Bring to a simmer while stirring to activate the dissolution of the sugar, then lower the heat to low and cook for 40 minutes, until obtaining a caramelized syrup thick and golden. 

Toast sesame seeds in a nonstick skillet (or oven). 

Add the honey to the syrup. Brew well. Spread half of the sesame seeds layer on an oiled marble or the oven plate covered with oiled parchment paper. 

Pour caramel, then cover with remaining sesame seeds. 

Let cool completely and cure for at least 6 hours. 

At the end of this time, cut the nougat. 

rayan likes it. 
