---
title: Chorba vermicelli, soup for ramadan
date: '2015-08-26'
categories:
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chorba-vermicelle-005_thumb1.jpg
---
![chorba vermicelle 005](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chorba-vermicelle-005_thumb1.jpg)

##  Chorba vermicelli, soup for ramadan 

Hello everybody, 

today I'm putting you a soup that my father always loves to eat in the holy month of Ramadan, when I was a child, my mother was often doing it, is to be frank, I was scrambling to not eat it Yes, I had my taste of my little age. 

that's why I never post this recipe, because I do not think I could eat it. so I take advantage that I am with my mother and that she prepare this soup for my father to post you the recipe. 

the recipe in Arabic: 

**Chorba vermicelli, soup for ramadan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chorba-vermicelle-007_thumb-300x224.jpg)

portions:  6  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * chicken or meat 
  * 1 onion 
  * ½ grated carrot 
  * ½ grated zucchini 
  * oil 
  * 1 and ½ cases of canned tomato 
  * 1 handful of chickpeas soaked the day before 
  * 1 bunch of coriander 
  * 1 glass of water with vermicelli 
  * salt, coriander powder, black pepper 
  * 1 cinnamon stick 

for meatballs chopped (optional) 
  * 200 grs of chopped meat 
  * some brown coriander 
  * a little onion rapee 
  * a little egg white to pick up 
  * salt, pepper, coriander powder, cinnamon powder (a pinch) 



**Realization steps**

  1. clean and cut the meat into cubes or chicken in pieces, 
  2. put in a pot, and add oil and onion to blender with the branches of coriander, 
  3. add the carrot rapee, and the zucchini rapee 
  4. add well chopped coriander, oil, salt, black pepper, and coriander powder, and cinnamon stick 
  5. let it simmer a little over low heat, add ½ glass of water, and cook a little over low heat. 
  6. after the canned tomato diluted in water, let it come back for a few more minutes, 
  7. add the chickpeas and cover with water, and cook 
  8. prepare the chopped meat, and make dumplings. 
  9. when the meat is well cooked add the chopped meatballs 
  10. then towards the end add the vermicelli, let it cook just a little, and remove from the heat. 



thank you for your visits and comments, 

thank you to all who continue to subscribe to the newsletter 

saha ramdankoum 

bonne journee 
