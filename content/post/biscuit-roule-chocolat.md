---
title: Chocolate rolled biscuit
date: '2015-10-29'
categories:
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Fruits
- desserts
- To taste
- Breakfast
- Ramadan
- Algeria
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-roul%C3%A9-chocolat%C3%A9.jpg
---
[ ![chocolate rolled biscuit](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-roul%C3%A9-chocolat%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-roul%C3%A9-chocolat%C3%A9-1.jpg>)

##  Chocolate rolled biscuit 

Hello everybody, 

I noticed that on my blog, we prefer more sweet recipes than salty recipes, it's still amazing, because perso when I'm on a blog, I prefer to look at salty recipes and dishes ... It gives me an idea for an even more diverse menu of the week. 

In any case, to please you, I share with you the recipe of one of my readers **Wamani Merou,** she already share many recipes on my blog and have had a suck on facebook, like my readers of this blog: the [ creamy chocolate cake with cream ](<https://www.amourdecuisine.fr/article-gateau-moelleux-au-chocolat-la-creme.html>) , try it and tell me your opinion! 

**Chocolate rolled biscuit**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-roul%C3%A9-chocolat%C3%A9-1.jpg)

portions:  12  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 4 eggs (yellow and white separated) 
  * 80 g of sugar 
  * 2 tablespoons hot water 
  * 40 gr of flour 
  * 25 gr of cocoa 
  * 20 gr of sugar for whites in snow 
  * vanilla . 
  * garnish: 
  * whipped cream 
  * some strawberries 
  * cocoa 



**Realization steps**

  1. whip the egg whites with 20 gr of sugar 
  2. work the egg yolks with sugar and 2 spoonfuls of water, 
  3. The mixture will double in volume, add the flour and the cocoa, mix well 
  4. then introduce the whites into snow little by little. 
  5. cover the tray with a baking paper, and bray the brush a little, 
  6. gently pour your mixture, standardize the surface with a spatula. 
  7. put in the oven at 180 degrees C. 
  8. remove the biscuit from the oven and roll gently into a torchan, and let cool. 
  9. roll out the biscuit, garnish with whipped cream and strawberries. 
  10. Roll again without being too tight but still keeping the shape of the roll. 
  11. sprinkle with cocoa and let cool. 



[ ![chocolate rolled biscuit 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-roul%C3%A9-chocolat%C3%A9-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-roul%C3%A9-chocolat%C3%A9-1.jpg>)
