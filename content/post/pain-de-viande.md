---
title: meatloaf
date: '2017-06-11'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
tags:
- Algeria
- inputs
- Healthy cuisine
- Morocco
- Oven cooking
- Easy cooking
- Gratin

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-de-viande.jpg
---
[ ![Meatloaf](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-de-viande.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-de-viande-2.jpg>)

##  meatloaf 

Hello everybody, 

When I have ground meat at home, my husband always asks me to give him a [ dolma ](<https://www.amourdecuisine.fr/article-dolma-batata-pommes-terre-viande-hachee.html>) , or some [ minced meatballs in sauce ](<https://www.amourdecuisine.fr/article-tajine-boulettes-de-viande-hachee-et-olives-kefta-be-zitoune.html>) ... This time, I did not want to ask him what he wants to eat, to be able to make this recipe for meatloaf that I have not done for a few years now. I remember when he bought me minced meat and I forget it in the fridge, I run quickly, the meatloaf, because it is a recipe very very simple and easy to do, just enough to have some bread crumbs at home. 

I hope you will love this meatloaf that can easily accompany a simple salad, a [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html>) , or just with a simple tomato sauce and a good bit of [ homemade bread ](<https://www.amourdecuisine.fr/article-comment-faire-son-pain-maison.html>) . You do not have to use only beef, you can add turkey meat, or chicken ... It'll be even more delicious. 

{{< youtube lLioQFA_Bvo >}} 

[ ![Meatloaf 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-de-viande-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-de-viande-2.jpg>)   


**meatloaf**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-de-viande-1.jpg)

portions:  4  Prep time:  10 mins  cooking:  40 mins  total:  50 mins 

**Ingredients**

  * 600 gr of minced meat 
  * 2 eggs 
  * ½ glass of breadcrumbs (1 glass of 240 ml) 
  * ½ chopped onion 
  * 1 crushed garlic clove 
  * ½ bunch of chopped parsley 
  * 2 tablespoons of milk 
  * salt and black pepper 
  * garnish: 
  * 2 tablespoons of ketchup 
  * 1 tablespoon of mustard 



**Realization steps**

  1. in a large salad bowl, place the minced meat. 
  2. add above all the ingredients mentioned in the first part. 
  3. Mix well to blend well. 
  4. cover a cake tin with aluminum foil (it must be wide enough to cover all the meat. 
  5. shape a large pudding with the meat mixture, and place it in the cake mold 
  6. cover the surface with the ketchup and mustard mixture. 
  7. cover the meatloaf well 
  8. and cook in a preheated oven at 180 degrees for at least 40 minutes. 
  9. open the aluminum foil, and pierce the meatloaf with a knife blade, it must come out well dry. 
  10. otherwise, cook the meatloaf covered now to have a nice gilding. 
  11. if you think there is a lack of garnish, add a little ketchup and mustard, and let it brown. 
  12. remove from the oven, let cool a little, before unmolding 



[ ![Meatloaf 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-de-viande-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-de-viande-3.jpg>)

Thank you for your visits and comments 

A la prochaine recette. 
