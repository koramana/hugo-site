---
title: Banana smoothie and dates
date: '2016-01-09'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-banane-et-dattes-1.jpg
---
![banana smoothie and dates 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-banane-et-dattes-1.jpg)

##  Banana smoothie and dates 

Hello everybody, 

I still have not recovered all the photos that are on the hard drive of my laptop that has made the soul (hihihihih), that's why I share with you the recipes of photos taken with my mobile phone. 

This time, it's a super smooth and super energy smoothie ... I think I'll be running all day without getting tired, hihihih, it's a banana and date smoothie, yum yum, you did not eat need to add honey to the preparation, unless you like sweet. 

I give you the nutritional value of this smoothie, which is total: 568 Kcal for all the preparation, to know if you go to taste it as a dessert (you can serve 3 people with this quantity) but in breakfast as I did me, it will be enough for two people only, so 284 kcal per person. 

**Banana smoothie and dates**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-a-la-banane-et-dattes.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 30 grams of oat bran 
  * 1 banana 
  * 2 tbsp clear honey 
  * 4 dates 
  * 100 ml of green tea 
  * 300 ml natural yogurt (I used actimel 0%)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/ingr%C3%A9dient-du-smoothie.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/ingr%C3%A9dient-du-smoothie.jpg>)



**Realization steps**

  1. prepare green tea in advance and cool. 
  2. pass the sound of avoin mill, or like me in the bowl of the blinder to reduce it to powder   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/20160109_100941.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/20160109_100941.jpg>)
  3. add the rest of the ingredients, and shield well for at least 5 minutes, making poses each time (you do not need to burn your blinder, hihihih) 
  4. enjoy and here is the nutritional value of this smoothie for two people:   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-banane-et-datte-nutrition.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-banane-et-datte-nutrition.bmp.jpg>)



[ ![banana smoothie and dates 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-banane-et-dattes-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-banane-et-dattes-2.jpg>)
