---
title: makrout nekkache
date: '2015-05-10'
categories:
- gateaux algeriens au miel
- gateaux algeriens- orientales- modernes- fete aid
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/makrout-nekkache.jpg
---
[ ![makrout nekkache](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/makrout-nekkache.jpg) ](<https://www.amourdecuisine.fr/article-makrout-nekkache.html/makrout-nekkache>)

##  Makrout nekkache, 

Hello everybody, 

Another makrout recipe, after [ makrout el koucha ](<https://www.amourdecuisine.fr/article-makrout-el-koucha-en-video-gateau-de-semoule-et-dattes.html> "makrout el koucha in video / semolina cake and dates") , [ makrout laassel ](<https://www.amourdecuisine.fr/article-makrout-laassel-aux-amandes.html> "makrout laassel with almonds") , [ makrout rolled ](<https://www.amourdecuisine.fr/article-makrout-laasel-makrout-roule-frit.html> "makrout laasel, makrout rolled fried") , [ makrout sniwa ](<https://www.amourdecuisine.fr/article-makrout-sniwa-ou-makrout-facon-baklawa.html> "makrout sniwa or makrout way baklawa") , and [ makrout Tlemcen ](<https://www.amourdecuisine.fr/article-makrout-de-tlemcen.html> "makrout from Tlemcen") Here we are today with the delicious and beautiful makrut nekkache, or the makrout with the tongs. 

This beautiful makrout nekkache is a traditional recipe, which often finds its place in the tables of Ramadan evenings, or those of the festivals of the aid, and weddings. A makrout all elegant, rich in taste, melting in the mouth is a recipe of my exotic flower friend, whom she generously shares with us. I hope that the recipe will please you, and that you will try it and give me your opinion. 

**makrout nekkache**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/makrout-enekache.jpg)

portions:  40  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients** For the dough: 

  * 3 measures of flour 
  * 1 measure icing sugar 
  * 1 overflowing measure of smen, 
  * a pinch of salt 
  * rose water to pick up the dough 

The joke: 
  * paste of dates (I used el 3ajwa, a special date in Algeria) 
  * cinnamon 
  * cloves 
  * a bit of smen 
  * Decoration: 
  * Honey 



**Realization steps**

  1. Work the date paste with the ingredients previously given until you have a soft ball. Leave aside. 
  2. In a large bowl, pour the flour, salt. Mix. 
  3. Make a fountain and pour the melted clarified butter. Mix well so that the butter is well absorbed by the flour. Let it rest. 
  4. Wet with orange blossom water and water, and mix without working with your fingertips and without kneading. Once you have a compact ball, leave aside, and cover well with a cling film. 
  5. take a good amount of the dough, shape a pudding. With your index finger, make a channel in the center in the direction of the length.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/makrout-ennekache-5.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42002>)
  6. Roll a little roll of date paste and put it in the slot. 
  7. Reassemble the edges of the dough on the dates to cover it all. 
  8. Ride gently again to have a roll of a width of 2 cm in height. 
  9. Cut pieces of almost 8 cm long, draw them with the pliers according to your taste   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/makrout-ennekache-4.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42003>)
  10. place them in a baking tin 
  11. let it dry for at least 1 hour then put in the oven to preheat to 180 ° C for about 15 min 
  12. at the end of the cooking let cool and keep in an airtight container. 
  13. at the moment of serving, dip them in a little warm honey, and have a good appetite. 



[ ![makrout nekkache](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/makrout-nekkache1.jpg) ](<https://www.amourdecuisine.fr/article-makrout-nekkache.html/makrout-nekkache-2>)
