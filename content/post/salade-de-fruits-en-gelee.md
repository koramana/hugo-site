---
title: Fruit salad in jelly
date: '2012-04-15'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/salade-de-fruit-en-gelee-032_thumb1.jpg
---
##  Fruit salad in jelly 

Hello everybody, 

a very good [ fruit salad ](<https://www.amourdecuisine.fr/categorie-12347071.html>) but in jelly, an irresistible taste. a very fresh dessert that my kids prepared yesterday, well they overtook mom in the kitchen, and I swear she's too good a fruit salad like that. 

so that for these children, we used the jelly, buy in the corner store, I used this same jelly for the preparation of [ trifles ](<https://www.amourdecuisine.fr/article-34906604.html>) , a delicious English dessert 

but promised the recipe with gelatin is coming very soon.   


**Fruit salad in jelly**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/salade-de-fruit-en-gelee-032_thumb1.jpg)

**Ingredients** you can put all the fruits that you like 

  * 3 kiwis 
  * a groin of 10 strawberries 
  * 50 grams of grapes 
  * 1 can of sliced ​​pineapple 
  * 1 can of jelly (mango flavor) 
  * 1 tablespoon of sugar 
  * 300 ml of boiling water 
  * 100 ml pineapple juice 



**Realization steps**

  1. wash the fruits 
  2. peel the kiwis and cut them into 
  3. cut all the fruits into 
  4. place them in the bottoms of small ramekins (without their water) 
  5. boil the water, add the pineapple juice, and the sugar 
  6. pour the powder of the jelly into this water, stir well 
  7. pour over the fruit and place in the fridge so that it takes well 
  8. at the moment of serving turn the ramekins or the pots and enjoy. 



![Fruit salad in jelly](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/salade-de-fruit-en-gelee_thumb1.jpg)

I must thank my children for this very delicious recipe 

Thank you for your visit 

bonne journée. 
