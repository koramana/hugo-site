---
title: 'fattouche: Lebanese salad with toast'
date: '2016-08-13'
categories:
- appetizer, tapas, appetizer
- salades, verrines salees
tags:
- inputs
- Vegetables
- Vegetarian cuisine
- Easy cooking
- Algeria
- accompaniment
- Detox

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fattouche-libanais-1.CR2_.jpg
---
[ ![Fattouche Lebanese salad with toast](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fattouche-libanais-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-fattouche-salade-libanaise-au-pain-grille.html/fattouche-libanais-1-cr2>)

##  Fattouche: Lebanese salad with toast 

Hello everybody, 

Here is a salad that I often prepared during Ramadan, it is a Lebanese salad known as fattouche or fattouch, a salad with toast, rich in green elements and full of colors and taste. The flavor does not stop there, because the mint and salmon-flavored vinaigrette sauce gives another dimension to the salad, one more taste, one more perfume. 

Do not worry if you do not have all the ingredients, because everything is allowed in this salad, On the book of Anahid, we can put in this salad: red pepper, green pepper, pomegranate molasses. In sites here and there, there were other ingredients, there were more missing ... I did not come across two recipes that looked alike. So you have to play with your ingredients, let yourself go to have a salad as rich in ingredients as possible. 

[ ![Fattouche Lebanese salad with toast](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Fattouche-salade-libanaise-au-pain-grill%C3%A9.jpg) ](<https://www.amourdecuisine.fr/article-fattouche-salade-libanaise-au-pain-grille.html/fattouche-salade-libanaise-au-pain-grille>)   


**fattouche: Lebanese salad with toast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fattouche-libanais-001.jpg)

portions:  6  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 1 arabic bread 
  * Lettuce washed and chopped 
  * 1 boot of mache (purslane if possible) 
  * ½ bunch of parsley 
  * ½ bouquet of mint 
  * 7 to 8 radishes cut in slices 
  * 1 cucumber peeled and sliced 
  * 4 small shallots washed and sliced 
  * 6 to 7 cherry tomatoes cut in half, otherwise leave the whole 

The mint dressing vinaigrette: 
  * 2 to 3 cloves of garlic 
  * ½ glass of lemon juice 
  * ⅔ glass of extra virgin olive oil 
  * ½ cup of paprika 
  * 1 tablespoon sumac 
  * 1 tablespoon of dried mint. 
  * salt according to taste 



**Realization steps** prepare the pieces of bread: 

  1. open the pita breads 
  2. using a clean kitchen scissors, cut 2 cm squares almost 
  3. brush them with a little olive oil   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pain-arabe.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pain-arabe.jpg>)
  4. place them on a baking tray, and cook until they turn a nice golden color.   
You can fry the bread squares in an oil bath. 

Prepare the vinaigrette sauce: 
  1. In a mortar, crush garlic with salt and paprika 
  2. when the mixture becomes a nice dough, add above all other ingredients   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Sauce-a-la-menthe.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Sauce-a-la-menthe.jpg>)
  3. keep the vanilla in a small jar. 
  4. Prepare the salad: 
  5. place all the ingredients of the salad in a large salad bowl.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fattouch-ou-fattouche-libanais.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fattouch-ou-fattouche-libanais.jpg>)
  6. mix everything well by hand 
  7. add the vinaigrette just before serving. 



[ ![Fattouche Lebanese salad with toast](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fattouche-libanais-008.jpg) ](<https://www.amourdecuisine.fr/article-fattouche-salade-libanaise-au-pain-grille.html/fattouche-libanais-008>)
