---
title: dziriettes
date: '2012-04-13'
categories:
- amuse bouche, tapas, mise en bouche
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/dziriettes-en-fleur-1.jpg
---
Rosales, Dziriettes el warda, دزيريات الوردة Hello everyone, once again, I'll be back for you to post one of the recipes of my cakes that I prepared during the festival of Aid, this time these Dziriettes in flower shape, a very nice Algerian cake. the first time I saw this recipe, it was one of my readers Lila who had sent me the photos, so I absolutely wanted to try the recipe. for even more recipes of Algerian cakes, the index is super well done, a click on the photo of the choice, takes you to the recipe directly. so quickly follow me & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.48  (  2  ratings)  0 

the Rosaces, Dziriettes el warda, دزيريات الوردة 

Hello everyone, 

again, I'm coming back for you post one of the recipes from [ my cakes ](<https://www.amourdecuisine.fr/article-gateaux-algeriens-pour-l-aid-2010-56806564.html>) that I prepared during the festival of the Aid, this time these Dziriettes in the shape of flowers, a very pretty [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) . 

the first time I saw this recipe, it was one of my readers Lila who had sent me the photos, so I absolutely wanted to try the recipe. 

for even more recipes from [ Algerian cakes, the index ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) is super well done, a click on the photo of the choice, takes you to the recipe directly. 

so quickly follow me in my kitchen: 

  * 3 measures of flour 
  * 1/2 measure of melted margarine 
  * 1 pinch of salt 
  * 1/2 C.a vanilla extract coffee 
  * 3 C.a orange blossom water soup 
  * water needed 



for the stuffing: 

  * 6 measures of finely ground almonds 
  * 2 sugar measures 
  * 1 C.a coffee full of lemon zest 
  * 2 to 3 eggs by size (or until wet stuffing) 
  * 1 teaspoon of vanilla extract 



####  work sifted flour with melted margarine salt vanilla extract .mow with orange blossom water and necessary water.    
work with the apple of your hands until you have a smooth, firm dough. 

![dziriettes-in-flower-jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/dziriettes-en-fleur-1.jpg)

####  cover with foil and leave to rest.    
during this time prepare the stuffing with the indicated ingredients.    
knead with your hand for 10 to 15 minutes to obtain a fairly soft and well-aerated composition 

####  take the dough on a floured surface and using a rolling pin spread it out very thin and then go to the machine n 1 then n 3 and then n 5 and cut out discs 9 cm in diameter using a cookie cutter. 

put the stuffing in and take a bottle cap tilt and try to make traces to give the shape of a flower. 

continue until exhaustion of the dough, decorate with an almond 

####  place in a dish and bake in preheated oven for 15 to 20 minutes.    
after cooking demold and soak them in the honey for 10 minutes then put in boxes. 

in any case this cake is super delicious .... 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
