---
title: black and white cake
date: '2012-04-11'
categories:
- cuisine diverse
- Indian cuisine
- Cuisine par pays
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/bavarois-chocolat-vanille.jpg
---
##  black and white cake 

Hello everybody 

A [ Bavarian ](<https://www.amourdecuisine.fr/categorie-12125855.html>) super delicious vanilla and chocolate that shares with us lunetoiles. we will call it black and white cake, because it is a black and white cake with the black or rather chocolate layer of the chocolate cake, covered with a generous layer of Bavarian mousse with vanilla. 

You want to do this little delight, you can follow the recipe of the black and white cake of Lunetoiles that she shares with us: 

**black and white cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/bavarois-chocolat-vanille.jpg)

portions:  8  Prep time:  1 min  cooking:  40 mins  total:  41 mins 

**Ingredients** For the chocolate mellow: 

  * 200 gr of dark chocolate 
  * 150 gr of butter 
  * 200 gr of sugar 
  * 1 tbsp. tablespoon icing sugar 
  * 2 tbsp. flour 
  * 4 eggs 

For the vanilla mousse: 
  * 250 ml of milk 
  * 60 gr of sugar 
  * 3 egg yolks 
  * 2 vanilla pods 
  * 150 gr of 20% cottage cheese 
  * 4 sheets of gelatin 
  * 200 ml whipping cream (whole) 

For the decor: 
  * 150 gr of dark chocolate 
  * 2 tbsp. tablespoon icing sugar 
  * Pearl pearls white sugar (I did not put) 



**Realization steps**

  1. Prepare the mellow: melt the dark chocolate with the butter in a bain-marie (or microwave oven three times 30 seconds). Add sugar, icing sugar, eggs and flour. 
  2. Pour this mixture into a 24 cm diameter buttered (or silicone) mold. 
  3. Put in the oven at 180 ° C (th.6) for 30 minutes. At the end of the oven, wait 5 minutes then turn out on a rack. Let cool. 
  4. Prepare the vanilla mousse: boil the milk with vanilla beans split in half and scrape the seeds. Beat the egg yolks with the sugar until blanching. Remove the vanilla pods and pour the warm milk over the egg yolks, off the heat. 
  5. Put back on low heat and allow to thicken without boiling, stirring constantly. The cream should coat the wooden spoon. Add the softened gelatin in cold water and allow to cool. 
  6. Rinse the vanilla pods and keep them for the decoration. Add the cottage cheese and then gently add the whipped cream. 
  7. Assembly: Place the chocolate cake on the serving platter with a dessert ring (24 cm in diameter) dressed with rhodoïd strips. Pour vanilla mousse and refrigerate for 4 hours. 
  8. Remove the circle and rhodoid strips. Sprinkle with icing sugar. 
  9. Prepare the decor: Melt the chocolate in a bain-marie to reach a temperature between 50 and 55 ° C, then lower the temperature to 29 ° C in a bath of water and ice cubes. Finally, raise the temperature to 33 ° C in a bain-marie. 
  10. Spread the chocolate on a strip of rhodoïd, previously cut to the right size, so that it fits perfectly around the edge of the cake. (I cut the rhodoid band, to obtain triangles) 
  11. Wait for the chocolate to begin to freeze and place the rhodoïd strip gently on the edge of the cake. 
  12. Let it cure completely before delicately detaching the rhodoid strip. 
  13. To accelerate the hardening of chocolate, you can place a few minutes, the cake in the refrigerator. 
  14. Decorate with pearly sugar pearls and vanilla pods, and place in the fridge until ready to serve. 



**Prepare the mellow: melt the dark chocolate with the butter in a bain-marie (or microwave oven three times 30 seconds). Add sugar, icing sugar, eggs and flour.  
Pour this mixture into a 24 cm diameter buttered (or silicone) mold. **

Put in the oven at 180 ° C (th.6) for 30 minutes. At the end of the oven, wait 5 minutes then turn out on a rack. Let cool. 

Prepare the vanilla mousse: boil the milk with vanilla beans split in half and scrape the seeds. Beat the egg yolks with the sugar until blanching. Remove the vanilla pods and pour the warm milk over the egg yolks, off the heat. 

Put back on low heat and allow to thicken without boiling, stirring constantly. The cream should coat the wooden spoon. Add the softened gelatin in cold water and allow to cool. 

Rinse the vanilla pods and keep them for the decoration. Add the cottage cheese and then gently add the whipped cream. 

Assembly: Place the chocolate cake on the serving platter with a dessert ring (24 cm in diameter) dressed with rhodoïd strips. Pour vanilla mousse and refrigerate for 4 hours. 

Remove the circle and rhodoid strips. Sprinkle with icing sugar. 

Prepare the decor: Melt the chocolate in a bain-marie to reach a temperature between 50 and 55 ° C, then lower the temperature to 29 ° C in a bath of water and ice cubes. Finally, raise the temperature to 33 ° C in a bain-marie.   
Spread the chocolate on a strip of rhodoïd, previously cut to the right size, so that it marries perfectly around the cake. (I cut the rhodoid band, to obtain triangles)   
Wait for the chocolate to begin to freeze and place the rhodoïd strip gently on the edge of the cake. 

Let it cure completely before delicately detaching the rhodoid strip.   
To accelerate the hardening of chocolate, you can place a few minutes, the cake in the refrigerator. 

Decorate with pearly sugar pearls and vanilla pods, and place in the fridge until ready to serve. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
