---
title: quiche with chicken and tomatoes
date: '2015-04-19'
categories:
- pizzas / quiches / tartes salees et sandwichs
tags:
- Easy recipe
- Fast Recipe
- inputs
- Accessible Kitchen
- pies
- Pastry
- Cheese

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/quiche-au-poulet-et-tomate-4.jpg
---
[ ![quiche with chicken and tomato 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/quiche-au-poulet-et-tomate-4.jpg) ](<https://www.amourdecuisine.fr/article-quiche-au-poulet-et-tomates.html/quiche-au-poulet-et-tomate-4>)

##  Chicken and tomato quiche 

Hello everybody, 

When we do not want to be too much in the kitchen, I do not know why we have this reflection to make a quiche, on the pretext that it's easy? Certainly the quiche and easy to make if it is made from puff pastry, but if it will be home-made dough, it does not become an easy recipe? or what do you say? 

Personally, I prefer quiches based puff pastry or broken pastry because I find that the texture of the dough makes even better taste and texture side. This is also the case for my friend **Flower Dz,** who shares with us today this beautiful chicken and tomato quiche, and who has prepared a delicious dough with milk ... I hope that the realization will please you.   


**quiche with chicken and tomatoes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/quiche-au-poulet-et-tomate-3.jpg)

Recipe type:  quiche  portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** For the dough: 

  * 250g of flour. 
  * 125 g of cold butter diced. 
  * Two pinches of salt. 
  * A C. coffee full of fine herbs (chervil, savory, basil). 
  * Half a c. coffee turmeric. 
  * Milk to collect the dough around 5 tbsp. soup. 

Garnish: 
  * 3 beaten eggs slightly (remove a small amount of egg white for the pie shell) 
  * 150 ml warm milk. 
  * 5 to 6 beautiful small tomatoes of different colors (red and yellow for me 
  * 2 tbsp. coffee herbal mixture. 
  * 150 g grated cheese (ricotta, mozzarella, feta and cheddar mix) + 30g cheddar cheese for the bottom of the pie 
  * Spices of your choice (peeled haunch and pepper for me) + a pinch of salt. 
  * A half white chicken cooked and crumble. 
  * 1 C. coffee and half of cornflour or flour. 



**Realization steps**

  1. In a food processor bowl, mix the flour and butter, then add the remaining ingredients and collect with the milk. 
  2. Form a ball of dough and put it in the fridge for 10 minutes. 
  3. Take out your dough and lower it on a floured worktop, and shape your removable base cake pie (my mold is 20cm x 23). 
  4. Place the dough in the freezer for ten minutes while preparing the filling.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/quiche-de-poulet-et-tomate.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41609>)

Garnish: 
  1. In a large bowl mix milk and eggs spices, salt and herbs with half a spoon of cornflour. 
  2. Prepare your egg white and tomatoes. 
  3. take out your pie crust from the freezer and brush it with the egg white and drop 
  4. the 30 gr of cheese. 
  5. Pour the mixture of milk and eggs and drop tomatoes then leave the cheese crumbs. 
  6. Bake in a preheat oven at 200 degrees until the filling is firm and the dough turns a beautiful golden color. 



[ ![quiche with chicken and tomato 5](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/quiche-au-poulet-et-tomate-5.jpg) ](<https://www.amourdecuisine.fr/article-quiche-au-poulet-et-tomates.html/quiche-au-poulet-et-tomate-5>)
