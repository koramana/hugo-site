---
title: Pumpkin jam
date: '2013-11-14'
categories:
- Cupcakes, macaroons, and other pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/confiture-de-citrouille-131.CR2_-1024x682.jpg
---
[ ![pumpkin jam 131.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/confiture-de-citrouille-131.CR2_-1024x682.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/confiture-de-citrouille-131.CR2_.jpg>)

##  Pumpkin jam 

Hello everybody, 

Here is a super delicious pumpkin jam, pumpkin jam, or pumpkin jam that I made with my friend Sally's garden pumpkin. Sally is a new reader of my blog, and when I started chatting with her, I knew she did not live far from home. 

So with the pumpkin of her garden and that she sowed herself, I made you this pretty jam, an organic jam and 100% organic.   


**Pumpkin jam / pumpkin jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/confiture-de-citrouille-124.CR2_-1024x676.jpg)

Recipe type:  jam, canned  portions:  8  Prep time:  24 hours  cooking:  15 mins  total:  24 hours 15 mins 

**Ingredients**

  * Pumpkin peeled and cut into small pieces (1.1 kg) 
  * 900 gr of sugar 
  * 1 cinnamon stick 
  * star anise (optional) 
  * 1 tablespoon of butter (tablespoon) 
  * the juice of a lemon 



**Realization steps**

  1. In a large bowl, combine pumpkin, sugar and lemon. Cover the bowl and leave one night in the refrigerator. 
  2. The next day, place the mixture in a deep bottom-heavy saucepan, and cook on medium heat. 
  3. Stir gently until the sugar begins to dissolve. 
  4. Cook over high heat for 2-4 minutes, then lower the heat (very low heat). 
  5. If you want to flavor jam, it's time to add them. 
  6. Cover the pan and continue cooking for another 15-20 minutes, being careful to stir from time to time. 
  7. Remove, let cool and enjoy immediately. 
  8. Otherwise put in jars immediately. 



Note It must be taken into account, that the pumpkin does not keep long, so this jam will not be kept for more than a month. 
