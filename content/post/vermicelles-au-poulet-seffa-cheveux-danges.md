---
title: Vermicelli with chicken, seffa hair of angels
date: '2015-10-15'
categories:
- Moroccan cuisine
tags:
- tajine
- Oriental cuisine
- Morocco
- Algeria
- dishes
- Algerian dish
- sauces

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet-11.jpg
---
[ ![Chicken Seffa 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet-11.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet-11.jpg>)

##  Vermicelli with chicken, seffa hair of angels 

Hello everybody, 

Today, I share with you the recipe of one of my readers she posted by following the link: Submit your recipe. 

This seffa medfouna, or vermicelli with chicken is a realization of my friend Samia Z, a Moroccan recipe based on vermicelli (hair of angels) steamed and presented with a unctuous reduced chicken sauce, garnished in the end by tapered almonds and a little icing sugar. 

I do not forget the opportunity to wish a happy new year hijri (Hijrah) 1437, to the entire Muslim community ... I wish you all peace, happiness and serenity. 

[ ![Muharram](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/moharrem.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/moharrem.jpg>)

**Vermicelli with chicken, seffa hair of angels**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 kg of good quality vermicelli 
  * A chicken of 1.5 kg 
  * 250 g onions cut into very small cubes (chopped with a knife) 
  * A bunch of coriander 
  * 2 cinnamon sticks 
  * Salt, pepper, ginger, saffron, and turmeric 
  * 5 to 6 tablespoons of olive oil + a little butter 
  * A little butter for vermicelli 



**Realization steps**

  1. Melt the butter with a little oil in a cooking pot. 
  2. add the chopped onion and sauté for at least 5 min over low heat until the onion becomes translucent. 
  3. add the cinnamon sticks, and let it simmer. 
  4. add chopped coriander 
  5. Cut the chicken in half and put it in the pot 
  6. add salt and spices and let roast well without burning 
  7. add enough water just to cover the chicken. cook, and reduce until you have a small sauce well reduced. 
  8. remove the chicken, and brown it just a little in the oven, then peel it and keep aside. 

For the preparation of the vermicelli: 
  1. In the bottom of a couscoussier, place a little water to boil 
  2. oil the vermicelli with 2 to 3 tablespoons of table oil 
  3. place it in the top of the couscoussier and let steam for 150 minutes take the top of the couscoussier as it is and place it on a basin 
  4. wet the vermicelli with the boiling water (which will drain in the bowl) 
  5. then pour the vermicelli into a large bowl, add a little salt and let the water absorb. for a few minutes 
  6. steam again, and repeat with boiling water on the bowl, until vermicelli is cooked through. 
  7. Remove the vermicelli from the steam, add the butter and allow to absorb   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet-5-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet-5-001.jpg>)
  8. during the presentation, decorate the serving dish with a little vermicelli, 
  9. add over a little crumbled chicken and a little sauce 
  10. put over another layer of vermicelli   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet-5.jpg>)
  11. decorate with the remaining crumbled chicken, the sauce, decorate with flaked almonds and a little icing sugar. 



[ ![Chicken seffa](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/seffa-au-poulet1.jpg>)
