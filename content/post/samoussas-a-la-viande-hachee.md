---
title: samosas has chopped meat
date: '2011-11-11'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/samoussa-a-la-viande-hachee-1_thumb1.jpg
---
##  samosas has chopped meat 

Since Ramadan, I had not done samosas, bourak or brig, and last week my husband bought bricks, and asked me to make minced meat samosas. 

I did not want to make potato boureks, and since I had ground meat at home, I thought about samoussa that I had to eat at school, it was an Indian recipe, my friend passed it to me, they were spicy, but too good.   


**samosas has chopped meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/samoussa-a-la-viande-hachee-1_thumb1.jpg)

portions:  6 

**Ingredients**

  * 6 sheets of brick 
  * 200 gr of minced meat 
  * ¼ onion 
  * ¼ carrots cut into 
  * medium sized potato 
  * a small handful of peas 
  * a little parsley 
  * ½ teaspoon of curry 
  * ¼ cup of aniseed coffee 
  * salt. pepper 
  * oil for frying 



**Realization steps**

  1. in a frying pan, brown the chopped onions until it becomes translucent 
  2. add the diced potato, peas, and carrots 
  3. cook well, then add meat and spices 
  4. cover and cook 
  5. Take your bricks leaves and cut them in 2. 
  6. Stuff them with the meat and close them carefully (solder with egg white) 
  7. Heat a little oil in a pan 
  8. bring back your samosas. When they are well gilded on all sides. 
  9. serve them with a nice green salad 


