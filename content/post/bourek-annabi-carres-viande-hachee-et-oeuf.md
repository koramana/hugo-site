---
title: bourek Annabi, square minced meat and egg
date: '2017-05-27'
categories:
- Bourek, brick, samoussa, slippers
tags:
- Algeria
- Bricks
- samossa
- Ramadan
- briouat
- accompaniment
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Bourek-annabi.jpg
---
[ ![bourek Annabi, square minced meat and egg](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Bourek-annabi.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Bourek-annabi.jpg>)

##  bourek Annabi, square minced meat and egg 

Hello everybody, 

The bourek annabi was my first culinary discovery when I was little, hihihihih. It must be said that in this period of my life barely 8 years I think and it was the first time that I tried to make my first day of fasting during the month of Ramadan, and to give me a gift, my mother prepared bourek annabi, i remember she was cooking it and she said to me: i am very proud of you, there are only a few minutes left and you are going to eat this squares with ground meat and egg, i was there, a to look at her, to fill the big leaves of bricks all the beautiful colors of these ingredients made me forget that I was hungry, I even had the right to put the olives, a lot of olives in my bricks to me. 

Since that day, the bourek annabi was my most favorite recipe, I liked a lot. But since I'm here in Bristol, I do not find the sheets of bricks very wide, and the ones I make home [ homemade dyouls ](<https://www.amourdecuisine.fr/article-dyouls-ou-feuilles-de-brick-fait-maison.html>) Are too small. 

Anyway, but if I do every time I go to London and I do my stock of dyouls, I never find the chance to take pictures. My children and my husband are just like me, they like to taste the bourek annabi while it's hot, and its egg yolk all flowing like that of a soft-boiled egg. Even this time, I could not make photos as I wanted, because they were hungry, besides they had eaten the most beautiful pieces, and I was there to take pictures of my little brig I did not want to take a lot of pictures, because I wanted to eat it hot, because impossible to heat the bourek annabi, we risk overcooking the egg, and the tasting is not going to be the same. 

**bourek Annabi, square minced meat and egg**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Bourek-annabi-1.jpg)

portions:  6  Prep time:  15 mins  cooking:  8 mins  total:  23 mins 

**Ingredients**

  * 6 sheets of brick 
  * 2 cubed potatoes cooked in salted water and crushed with a fork 
  * 6 eggs 
  * 200 gr of minced meat 
  * 6 cheese portion (cheese triangles) 
  * 1 small onion cut into small cube 
  * chopped parsley 
  * Hrissa 
  * green olives cut into slices 
  * salt and black pepper 
  * cooking oil 



**Realization steps**

  1. cook the minced meat in a pan with a little oil, salt, pepper, and water. 
  2. Cover and cook until the sauce is completely reduced. 
  3. preheat the oil in a shallow pan. 
  4. in wide dish, place the pastry sheet. 
  5. put some crushed potato, cheese pieces all around, 
  6. Add a little onion, minced meat, parsley and olives. 
  7. Make a small well in the center, and break the egg 
  8. add on top of pepper and salt from the mill   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/farce-du-bourek-annabi.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/farce-du-bourek-annabi.jpg>)
  9. fold the sheet of brick in the shape of an envelope, place the square gently in the hot oil, and brown both sides. 
  10. Control the cooking to have bricks well golden, but try to have a yolk well flowing. 



[ ![Bourek annabi 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Bourek-annabi-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Bourek-annabi-2.jpg>)
