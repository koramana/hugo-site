---
title: 2015 new year cakes and end of year festivities
date: '2012-12-15'
categories:
- Algerian cuisine
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-frangipane-au-poire_31.jpg
---
Hello everybody, 

at the request of many of my readers, I am giving you a small index of cake recipes that you can create for New Year's Eve and New Year's Eve parties. 

this index is under construction, there are still many new recipes that I have not put on it. 

in the meantime make a jump in my category cakes and sweet recipes in the menu at the top of my blog. 

Bavarian cheesecake and moss 

Log   
  
<table>  
<tr>  
<td>

[ ![pear frangipane pie](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-frangipane-au-poire_31.jpg) ](<https://www.amourdecuisine.fr/article-tarte-bourdaloue-97257496.html>) 
</td>  
<td>

[ ![Normandy Pie](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-normande_31.jpg) ](<https://www.amourdecuisine.fr/article-tarte-normande-97115899.html>) 
</td>  
<td>

[ ![flan pie](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-flan_31.jpg) ](<https://www.amourdecuisine.fr/article-tarte-au-flan-64077148.html>) 
</td>  
<td>

[ ![tarte tatin with quince](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-tatin-au-coing_31.jpg) ](<https://www.amourdecuisine.fr/article-tarte-tatin-aux-coings-63684300.html>) 
</td> </tr> </table>

Pie 

Cake   
  
<table>  
<tr>  
<td>

[ ![coffee cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cake-au-cafe_31.jpg) ](<https://www.amourdecuisine.fr/article-33054404.html>) 
</td>  
<td>

[ ![carrot cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/carrot-cake_31.jpg) ](<https://www.amourdecuisine.fr/article-38217007.html>) 
</td>  
<td>

[ ![three quarter chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/trois-quart-au-chocolat_31.jpg) ](<https://www.amourdecuisine.fr/article-53471408.html>) 
</td> </tr> </table>

Cake   
  
<table>  
<tr>  
<td>

[ ![brownies](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/brownies_31.jpg) ](<https://www.amourdecuisine.fr/article-brownies-aux-cacahuetes-97765323.html>) 
</td>  
<td>

[ ![gift to the sugar dough](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cadeau-a-la-pate-a-sucre_31.jpg) ](<https://www.amourdecuisine.fr/article-37650983.html>) 
</td>  
<td>

[ ![daisy of soissons](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/paquerette-de-soissons_31.jpg) ](<https://www.amourdecuisine.fr/article-paquerette-de-soissons-93219227.html>) 
</td>  
<td>

[ ![gluten free cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateau-sans-gluten_31.jpg) ](<https://www.amourdecuisine.fr/article-gateau-sans-gluten-aux-oranges-et-amandes-91051849.html>) 
</td> </tr> </table>

muffins and cupcakes   
  
<table>  
<tr>  
<td>

[ ![speculoos muffins](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/muffins-au-speculoos_31.jpg) ](<https://www.amourdecuisine.fr/article-muffins-aux-speculoos-et-chocolat-noir-91366942.html>) 
</td> </tr> </table>

AT   
  
<table>  
<tr>  
<td>

![amandine 640](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/amandine-640_thumb1.jpg)

####  [ Amandines with puff pastry ](<https://www.amourdecuisine.fr/article-38772190.html>)


</td> </tr> </table>

B 

C 

D   
  
<table>  
<tr>  
<td>

![101_0483](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/101_0483_thumb.jpg)

####  [ Algerian flag ](<https://www.amourdecuisine.fr/article-42436429.html>)

![dora ines 033](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/dora-ines-033_thumb.jpg)

####  Dora [ Ines birthday cake ](<https://www.amourdecuisine.fr/article-37896589.html>)


</td>  
<td>

![AVB2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bav2_thumb.jpg)

####  [ Mascarpono-Tobleronian Delight ](<https://www.amourdecuisine.fr/article-26736625.html>)


</td> </tr> </table>

E 

F   
  
<table>  
<tr>  
<td>

![fondant2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/fondant2_thumb.jpg)

####  [ chocolate fondant ](<https://www.amourdecuisine.fr/article-gateau-fondant-au-chocolat-42706545.html>)

![pic 003](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/pic-003_thumb.jpg)

####  [ Erynian flan ](<https://www.amourdecuisine.fr/article-31934619.html>)


</td>  
<td>

![birth](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/birth_thumb.jpg)

####  [ pink forest ](<https://www.amourdecuisine.fr/article-28913717.html>)

####  [ my birthday cake… ](<https://www.amourdecuisine.fr/article-28913717.html>)

![Picture_688](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/31405480.jpg)

####  [ flowers ... Ines birthday cake ](<https://www.amourdecuisine.fr/article-25345347.html>)


</td> </tr> </table>

G 

H 

I 

J   
  
<table>  
<tr>  
<td>

![jardin2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/jardin2_thumb.jpg)

####  [ flower garden .... my birthday cake ](<https://www.amourdecuisine.fr/article-la-recette-du-jardin-fleuri-mon-gateau-d-anniversaire-46564330.html>)


</td> </tr> </table>

K 

The 

M 

NOT 

O   
  
<table>  
<tr>  
<td>

![oranais 005](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/oranais-005_thumb.jpg)

####  [ oranais / glasses with apricots ](<https://www.amourdecuisine.fr/article-les-oranais-lunettes-44602543.html>)


</td> </tr> </table>

P   
  
<table>  
<tr>  
<td>

![S7302195](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/21455840.jpg)

####  [ for the best and the worst cake of our wedding anniversary ](<https://www.amourdecuisine.fr/article-25345366.html>)


</td> </tr> </table>

Q 

R   
  
<table>  
<tr>  
<td>

![roll 018](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/roll-018_thumb1.jpg) [ rolled with jam ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture-a-minuit-45608781.html>) 
</td> </tr> </table>

S   
  
<table>  
<tr>  
<td>

![sponge cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/sponge-cake_thumb_1.jpg)

####  [ Squishy Yogurt Cake ](<https://www.amourdecuisine.fr/article-44979357.html>)

![rouler3](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/rouler3_thumb.jpg)

####  [ Swiss rolls marble with lemon cream, Fatafeat Houriat el matbakh ](<https://www.amourdecuisine.fr/article-37115347.html>)


</td>  
<td>

![SAV7](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/sav7_thumb.jpg)

####  [ A savarin with Nawel ](<https://www.amourdecuisine.fr/article-26438036.html>)


</td> </tr> </table>

T 
