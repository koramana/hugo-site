---
title: tartlets with orange
date: '2017-01-27'
categories:
- dessert, crumbles et barres
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-lorange-011.CR2_-806x1024.jpg
---
[ ![tartlets with orange](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-lorange-011.CR2_-806x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-lorange-011.CR2_.jpg>)

##  tartlets with orange 

Hello everybody, 

In this season, we enjoy very sweet and juicy oranges to make delights starting with these tartlets with orange, yum yum what they are easy to do! 

You can also do some [ fruit paste with orange ](<https://www.amourdecuisine.fr/article-pate-de-fruit-a-lorange.html> "fruit paste with orange") , of the [ jam with orange ](<https://www.amourdecuisine.fr/article-confiture-d-oranges.html> "orange jam, easy and fast") , a [ orange marmalade ](<https://www.amourdecuisine.fr/article-marmelade-d-oranges-non-amere-117420888.html> "non-bitter orange marmalade") , a [ orange cake ](<https://www.amourdecuisine.fr/article-cake-a-l-orange-moelleux-facile.html> "cake with orange, easy fluffy") , a [ muskoutchou with orange ](<https://www.amourdecuisine.fr/article-minis-mouskoutchou-a-l-orange.html> "mini mushroom with orange") , or then a [ fruit salad with oranges ](<https://www.amourdecuisine.fr/article-salade-d-agrumes-salade-d-oranges-pamplemousse-mandarine.html> "citrus salad, mandarin orange grapefruit salad") .... only choice. 

This recipe for tartlets with orange is the easiest version, to taste this dessert, you can of course decorate your pie with a meringue, candied or caramelized oranges ... as you can enjoy them natures, as I did at the request of my husband ...   


**Orange tart / Individual version: tartlet**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-lorange-23-11-2013-1024x674.jpg)

Recipe type:  dessert pie  portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** for the pasta: 

  * [ sweet dough ](<https://www.amourdecuisine.fr/article-pate-a-tarte-sucree.html> "Sweet pie dough")   
or 
  * [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast") . 

for the cream: 
  * 250 ml of pressed orange juice 
  * 50 gr of sugar   
(or less if your oranges are sweet) 
  * 2 egg yolks 
  * 15 gr of cornflour 



**Realization steps**

  1. beat the egg yolks with the sugar and add the orange juice. 
  2. Heat the mixture while stirring 
  3. add the maïzena deluee in a little orange juice 
  4. thicken over low heat, stirring regularly. 
  5. The cream is ready when it has a nice consistency and slicks the back of your spoon. 
  6. Pour the cream on the cooked pie shell. Let cool 



[ ![tartlets with orange](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tartelette-a-lorange.CR2_-762x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tartelette-a-lorange.CR2_.jpg>)
