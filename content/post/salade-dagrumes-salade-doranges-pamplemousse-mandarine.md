---
title: citrus salad, mandarin orange grapefruit salad
date: '2016-09-20'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-d-agrumes-salade-de-fruits-orange-pamplemousse-m1.jpg
---
[ ![citrus salad, fruit salad, orange tangerine grapefruit.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-d-agrumes-salade-de-fruits-orange-pamplemousse-m1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-d-agrumes-salade-de-fruits-orange-pamplemousse-man.jpg>)

##  citrus salad, mandarin orange grapefruit salad 

Hello everybody, 

An ultra-vitamin salad, a salad that will make you grow wings, especially in the cold winter that does not want to leave us! I share with you my delicious **citrus salad, mandarin orange grapefruit salad** . 

A citrus salad, rich in taste, a little tart, citrus that gives a lot of vibrant colors, to garnish your party table, and finish the evening in style, and above all, to consume without moderation. 

this salad with oranges, grapefruit and tangerines, scented with cinnamon, and enhanced in color by the addition of pistachio chips, and dried cranberries. 

[ ![winter fruit salad 039.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-aux-fruits-d-hiver-039.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-aux-fruits-d-hiver-039.CR2_2.jpg>)   


**citrus salad, mandarin orange grapefruit salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-d-agrumes-salade-de-fruits-orange-pamplemousse-m1.jpg)

Recipe type:  salad, dessert  portions:  4  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 2 oranges 
  * 2 grapefruits 
  * 4 mandarins 
  * sugars according to taste 
  * ¼ teaspoon of cinnamon powder. 
  * 1 handful of cranberries (optional, raisins will replace them well) 
  * bursts of pisatches, or any other dry fruit 
  * cinnamon sticks for garnish. 



**Realization steps**

  1. Peel oranges, grapefruit and tangerines raw. 
  2. Detach the quarters, sliding a knife between the flesh and the skin. 
  3. Put the quarters in a salad bowl. without taking the membranes, (work over a salad bowl to recover the juice)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-d-agrumes-057_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-d-agrumes-057_thumb.jpg>)
  4. Press the citrus scraps between your hands to get the juice from the salad bowl. 
  5. add sugar and cinnamon 
  6. add the cranberries, and let macerate all in the fridge for 3 hours minimum. 
  7. just before serving, add the grilled pistachio chips and enjoy. 



[ ![citrus salad 042.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-d-agrumes-042.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-d-agrumes-042.CR2_2.jpg>)

[ ![fruit salad in jelly 035](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/salade-de-fruit-en-gelee-035_thumb.jpg) ](<https://www.amourdecuisine.fr/article-salade-de-fruits-en-gelee-97871255.html>)

[ salade de fruit en gelee ](<https://www.amourdecuisine.fr/article-salade-de-fruits-en-gelee-97871255.html>)
