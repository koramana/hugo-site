---
title: bricks with minced meat and potatoes
date: '2017-06-08'
categories:
- Algerian cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
tags:
- Ramadan 2016
- Ramadan 2017
- Algeria
- Ramadan
- Morocco
- inputs
- Hot Entry

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/06/bricks-%C3%A0-la-viande-hach%C3%A9e-et-pommes-de-terre-679x1024.jpg
---
![bricks with minced meat and potatoes](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/bricks-%C3%A0-la-viande-hach%C3%A9e-et-pommes-de-terre-679x1024.jpg)

##  bricks with minced meat and potatoes 

Hello everybody, 

These bricks with ground meat and potatoes are the favorites of my husband during Ramadan, finally with the boureks with shrimps and potatoes too. Personally I prefer tuna bricks. 

And what do you like to do most for Ramadan parties? At home even if there are sweet tricks on the table, my husband and son prefer what I present as salty recipes! 

So too much about these small tastings, we go to the preparation of these bricks to minced meat and potato. If you do not like to put the potato in (I put to minimize the absorption of fat during the frying), you can see the recipe of [ boureks or bricks with minced meat ](<https://www.amourdecuisine.fr/article-bricks-ou-bourek-a-la-viande-hachee.html>) . 

{{< youtube Mu_6je_ls8s >}} 

![bricks with minced meat and potatoes 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/bricks-%C3%A0-la-viande-hach%C3%A9e-et-pommes-de-terre-1.jpg)
