---
title: Tunisian tajine with meatballs bnadèk
date: '2017-08-01'
categories:
- diverse cuisine
- Tunisian cuisine
- ramadan recipe
tags:
- Ramadan 2016
- Ramadan
- Algeria
- Tunisia
- dishes
- kofta
- omelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tajine-tunisien-aux-boulettes-de-viande-bnad%C3%A8k-2.jpg
---
![Tunisian tajine with meatballs bnadèk 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tajine-tunisien-aux-boulettes-de-viande-bnad%C3%A8k-2.jpg)

##  Tunisian tajine with meatballs bnadèk 

Hello everybody, 

Here is a Tunisian tagine that I like a lot, not only is it super easy to make this Tunisian tagine meatballs or bnadèk, he too too good. Besides, when I'm super tired or do not know what to do for my children, or even during Ramadan, when I do not know what to do as a dish to accompany a [ chorba ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) or a [ harira ](<https://www.amourdecuisine.fr/article-harira-soupe-marocaine-du-ramadan-2014.html>) , I'm doing this tagine. 

![Tunisian tajine with meatballs bnadèk](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tajine-tunisien-aux-boulettes-de-viande-bnad%C3%A8k.jpg)

**Tunisian tajine with meatballs bnadèk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tajine-tunisien-aux-boulettes-de-viande-bnad%C3%A8k-1.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 300 gr of minced meat 
  * 2 teaspoon generous mint dried minced 
  * 2 crushed garlic cloves 
  * 2 teaspoons of tabil (or coriander seeds in powder form) 
  * salt 
  * a drizzle of olive oil 
  * 8 eggs 
  * a very good handful of grated cheese 
  * 1 potato cut into cubes, then fried. 
  * 2 tablespoons breadcrumbs 



**Realization steps**

  1. in a container, place the minced meat, add mint, tabil, garlic and salt, mix well; 
  2. form small, medium-sized meatballs 
  3. in a done everything, heat the oil fillet and brown the meatballs, stirring the pot from time to time without using the spoon to not damage the "bnadék", 
  4. as soon as they are golden, wet with a large glass of water, cover and cook over medium heat. 
  5. leave some of the meatball broth to add to the tajine later. 
  6. As soon as the meatballs are cooked and the sauce reaches the desired consistency, remove from the heat, collect the meatballs in a bowl, and let everything cool. 
  7. Once the meatballs have cooled down and their sauce, add the cheese and bread crumbs, the fried potato cubes, 
  8. beat the eggs with the omelette sauce and mix everything. 
  9. heat the oven to 180 ° C, put the tajine in a buttered anti-adhesive mold, bake about twenty minutes until the tajine is golden, 



![Tunisian tajine with meatballs bnadèk 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tajine-tunisien-aux-boulettes-de-viande-bnad%C3%A8k-3.jpg)
