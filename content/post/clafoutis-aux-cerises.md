---
title: Cherry clafoutis
date: '2012-03-01'
categories:
- Cuisine détox
- Healthy cuisine
- recipes at least 200 Kcal

---
Hello everybody, 

a light, fluffy dessert, a must-have classic that Lunetoiles has generously shared with us. 

I know it's not yet the cherry period, but here in England I sometimes come across cheap cherries, and as I do not find them as good as those in the summer, I prefer to cover them with a good custard, as for this clafoutis. 

now I give you his recipe: 

Ingredients for 6 parts: 

  * 800 gr of pitted cherries 
  * 6 eggs 
  * 100 gr of flour 
  * 90 gr of sugar 
  * 25 cl of fermented milk 
  * 1 pinch of salt 
  * icing sugar 



pie pan about 26 cm   
Preheat the oven to 180 C   
In a salad bowl pour the flour, break the eggs on the flour, add a pinch of salt and a quarter of the fermented milk.   
Beat vigorously until smooth, homogeneous, and without lumps.   
Add the sugar, the rest of the milk while continuing to beat. Then pour the cherries into the dough.   
Butter the dish, pour the paste in it. Bake for 35 minutes, watching, the end of cooking.   
When the clafoutis is cooked is golden brown, remove from oven, and let cool.   
Sprinkle with icing sugar before serving. 

thank you to you my dear, and thank you for your comments, sorry if I have not yet validate a lot of comment, I'm really too taken since I'm in Algeria 

bisous a tous 
