---
title: my achievements at home 42
date: '2013-12-31'
categories:
- Chocolate cake
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

here is another article to close the year 2013, that of my recipes at home, and thank you very much my dear readers who have tried to contact me by email, to send me the pictures of the recipes they have tried in following the method on my blog. 

I put you, a system much faster so that you can send me your photos, just go on this page: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll like ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link of your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)

for the beginning we have: 

[ ![bratel and rakhssiss ros](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/ros-bratel-et-rakhssiss.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/ros-bratel-et-rakhssiss.jpg>)

[ Bratel ros ](<https://www.amourdecuisine.fr/article-ros-bratel-feves-en-sauce.html> "ros bratel - beans in sauce") at Malika a reader 

[ kesra rekhssis ](<https://www.amourdecuisine.fr/article-kesra-rakhsiss-galette-rakhsiss.html> "kesra rakhsiss - galette rakhsiss") at Malika a reader 

![06082013410-fa22322debcc0c315e6087fbc105f7ec10534fd2](https://www.amourdecuisine.fr/wp-content/gallery/1/06082013410-fa22322debcc0c315e6087fbc105f7ec10534fd2.jpg)

the Algerian cake [ the esses ](<https://www.amourdecuisine.fr/article-les-esses-gateaux-aux-amandes.html> "Esses, almond cakes, Algerian cake 2013") with a reader (she did not mention her name) 

[ ![mamyrosa-3](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mamyrosa-3-1024x768.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mamyrosa-3.jpg>)

I do not recognize this recipe, the person did not mention the name of the Mamyrosa recipe, 

if she can leave a comment, it will be nice 

[ ![mamyrosa-2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mamyrosa-2-1024x768.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mamyrosa-2.jpg>)

yet another Mamyrosa recipe, 

thank you Mamyrosa, if it's a recipe you want to propose on the blog, just drop the recipe by following this link: 

[ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

Bisous 
