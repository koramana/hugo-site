---
title: the brookies
date: '2018-01-14'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- cakes and cakes
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/brookies-3.jpg
---
[ ![brookies 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/brookies-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/brookies-3.jpg>)

##  the brookies 

Hello everybody, 

Have you heard of Brookies? I'm sure some of you already know ... 

The brookies are cakes consisting of two layers, a layer of brownies, and a layer of cookies .... the brownies you know, it's a cake mellow chocolate .... everyone loves, big and small, despite the fact that brownies are the enemy number of thin waist hihihihihi .... but still it's a delight to taste from time to time ... 

I'm going to be frank with you, me when a thing like that pinches my heart, I do not refrain, because as say the English "life is too short, better not to private your self" ... life is very short , better not to deprive oneself ... 

Not everyone thinks like me, but sometimes I think it's cruel to look at it like this, and have eyes that come out of their sockets ... then say, no I want to stay in my Jean, hihihihi ... a time is not everyday ... hihihih 

So that was the story of the first layer, and the second layer is a giant cookie, crusty-soft, yum yum ... 

So why do I tell you this story, and why do I say do not deprive yourself ???? simply, because it's not me who made these brookies .... sniff, sniff, it's Lunetoiles, sniff, sniff ... and I'm just like you go to look at these pictures behind my screen, and drool like a baby in full dentition .... I do not even have chocolate at home to make this recipe ... sniff, sniff sniff   


**the brookies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/brookies.jpg)

Recipe type:  dessert cake  portions:  36  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** For the Brownie part: 

  * 75 gr of flour 
  * 125 gr of sugar 
  * 75 gr of half-salted butter 
  * 125 gr of chocolate 
  * 2 eggs 
  * 65 g of pecan nuts, hazelnuts or crushed almonds. 

For the cookie part: 
  * 165 grams of flour 
  * 1 teaspoon of baking powder 
  * 135 g of brown sugar 
  * 120 gr of half-salted butter 
  * 1 egg 
  * 110 gr of dark chocolate chopped into pieces 



**Realization steps**

  1. Preheat the oven to 180 ° C. 

Prepare the brownie: 
  1. melt chocolate and butter in a bain-marie. 
  2. Out of the heat, smooth the mixture and add the sugar. 
  3. Whip briskly, and add the eggs. 
  4. Finally, add the flour and the dried fruits. 
  5. Pour into a 23 cm square baking dish. 

Prepare the cookie dough: 
  1. Mix the butter and brown sugar in a salad bowl. 
  2. Whisk well to get a creamy texture, then add the egg. 
  3. Stir in flour, yeast and chocolate chips. 
  4. Mix everything quickly without overworking the dough. 
  5. Sprinkle on the brownie dough, like a crumble. 
  6. Bake for about twenty minutes (18 everything in mine). 
  7. Let cool completely before detailing in small squares. 



[ ![brookies 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/brookies-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/brookies-1.jpg>)
