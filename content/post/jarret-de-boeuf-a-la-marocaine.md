---
title: Moroccan beef shank
date: '2014-02-02'
categories:
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/jarret-de-boeuf-a-la-marocaine_4643647-M.jpg
---
[ ![Moroccan beef shank](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/jarret-de-boeuf-a-la-marocaine_4643647-M.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/jarret-de-boeuf-a-la-marocaine_4643647-M.jpg>)

##  Moroccan beef shank 

Hello everybody, 

here is this delicious **Moroccan beef shank** , that one of my readers Kaikoux likes to share with us, thank you very much my dear ... this dish looks delicious, and the meat melting ... 

this Moroccan beef shank is proposed by Kaikoux who liked sharing the recipe of his dish that she presented with semolina [ Steamed couscous ](<https://www.amourdecuisine.fr/article-comment-cuire-le-couscous-111575933.html> "how to cook couscous") ... 

to try as soon as possible, it is promised my dear. 

**Moroccan beef shank** Servings: for 4 people 

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/jarret-de-boeuf-a-la-marocaine_4643647-M-300x224.jpg)

portions:  4  Prep time:  30 mins  cooking:  4 hours  total:  4 hours 30 mins 

**Ingredients**

  * 1 kg of beef shank 
  * 1 minced onion 
  * 1 clove garlic minced 
  * 1 tablespoon of honey 
  * 1 teaspoon of cumin 
  * 1 teaspoon Ras El Hanout 
  * 1 teaspoon grated nutmeg 
  * ½ teaspoon of Espelette pepper 
  * 1.5 liters of beef stock *   
read the preparation below 
  * 1 branch of fresh thyme 
  * 15 pitted dates 
  * 6 potatoes 
  * Olive oil 
  * Salt pepper 



**Realization steps**

  1. Remove the meat 1 hour before starting to cook. 
  2. Preheat the oven to 150 ° C. 
  3. In a cast iron casserole heat the olive oil. 
  4. Brown the knuckle on all sides and set aside. 
  5. Degrease the casserole with absorbent paper. 
  6. Heat a little olive oil in the cast iron pan. 
  7. Add the garlic and onion and brown over low heat for a few minutes. 
  8. Add honey, spices and mix. 
  9. Simmer 2 minutes. 
  10. Peel and cut in length the potatoes. 
  11. Add meat, thyme, potatoes and dates. 
  12. Pour the beef stock, salt, pepper, stir and bring to a boil. 
  13. Cover the casserole and put it in the oven. 
  14. Cook for 4 hours without stirring. 
  15. Remove the meat, potatoes and dates and put them in a serving dish. 
  16. Bring the juice to a boil and reduce for 10 minutes. 
  17. Serve the meat with the potatoes and dates and pour the juice 
  18. using a ladle. 
  19. Serve this dish of couscous semolina. 

for the veal stock *: 
  1. dilute 2 tablespoons of beef stock for 1.5 liters of boiling water.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/jarret-de-boeuf-a-la-marocaine_4643647-M-300x224.jpg)


