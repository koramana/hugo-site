---
title: my recipes at home 17
date: '2012-03-31'
categories:
- Brioches et viennoiseries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg
---
![2011-09-04 for the moment tout1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg)

Hello everybody, 

once again you have been many and many to try my recipes, and it is a great honor for me, and every time this article is ready to be published, I receive other emails essays from my readers .... thank you very much for everything, and if you also try one of my recipes, send me the photo and or link of the recipe (if you have a blog) 

here is my email and thanks for everything: 

[ amour2cuisine@gmail.com ](<mailto:amour2cuisine@gmail.com>)

now we will go to your delicious recipes: 

![lentille.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/lentille1.jpg)

[ lentil soup ](<https://www.amourdecuisine.fr/article-soupe-de-lentilles-58156074.html>) in [ my culinary inspirations ](<http://mesinspirationsculinaires.over-blog.com>)

a very delicious **soup** for lovers of the **vegetarian cuisine** , and for those who love **light dishes** but very rich. 

![bourack-fromage1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/bourack-fromage11.jpg)

the **Bourak** is not in the menu of [ ramadan recipes ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>) only, it can always very well accompany your [ light soups ](<https://www.amourdecuisine.fr/categorie-10678936.html>) , or even be an entry that savorfully accompanies your [ fresh salad ](<https://www.amourdecuisine.fr/categorie-10678933.html>) in the hot days ahead. 

![February-2144.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/fevrier-21441.jpg)

a sublime starter, and I confess that for me these little ones with a salad are just a complete dish. 

![IMGP1031.JPG](http://img.over-blog.com/500x375/5/63/13/08/IMGP1031.JPG)

[ pineapple gaspacho ](<https://www.amourdecuisine.fr/article-gaspacho-d-ananas-99863999.html>) in [ Stephlabrodeuse ](<http://lesgourmandisesetcreationdestephlabrodeuse.over-blog.com>)

a cool drink, which my children keep claiming since I drip them. 

![https://www.amourdecuisine.fr/wp-content/uploads/2012/03/6835056692_b75728ef21_b1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/6835056692_b75728ef21_b1.jpg)

[ chocolate cake and buttermilk ](<https://www.amourdecuisine.fr/article-recette-gateau-au-chocolat-et-buttermilk-babeurre-49605226.html>) , at [ on the side of val ](<http://ducotedechezval.over-blog.com>)

a very delicious and light cake, which really makes its success with every test. 

[ ![foam ramekin](http://amour-de-cuisine.com/wp-content/uploads/2012/03/ramequin-de-mousse1.jpg) ](<http://amour-de-cuisine.com/wp-content/uploads/2012/03/ramequin-de-mousse.jpg>)

[ Salted butter caramel mousse ](<https://www.amourdecuisine.fr/article-mousse-caramel-au-beurre-sale-101394974.html>) in [ Nadjet's sweets ](<http://douceursdenadjet.over-blog.com>)

if you like the [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale-100020029.html>) Here is a delicious mousse that you can prepare with this delight. 

  
![Sabou](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/Sabou1.jpg)

[ rice balls with tuna ](<https://www.amourdecuisine.fr/article-boulettes-de-riz-au-thon-97137268.html>) , at [ Matina ](<http://horizon-culinaire.over-blog.com>)

a very delicious starter, and amuse bouche that will always be welcome on your tables, and that will always please the people who tasted them. 

what she thinks about it: 

Thanks again for the recipe because it was very good. 10/10   
I just added green olives at the request of my husband. 

[ Barbie cake ](<https://www.amourdecuisine.fr/article-princesse-barbie-gateau-d-anniversaire-86764345.html>) at Amel D (a faithful reader) 

Amel just took the idea and she worked hard on this sublime cake: 

for the cake it is composed of two cakes: the dress is a cake with yoghurt and cocoa stuffed with cream chantilly nature and chocolate. The base is composed of two intercalated genoises, one with cocoa and the other with coconut and fodder and the same as for the dress. The dress is made of sugar paste that I made myself with mashmallow she was super good, and the sponge cake is covered with whipped cream (my sinful cute I confess) white and colored according to the request of my daughter ...... .finally I thank you for your idea and all your recipes I tested a lot but c so good that it finishes before. 

So thanks to all of you who have shared their essays with me, and I hope I have not forgotten any of your recipes, if so, please contact me, and it is with great pleasure that I read your emails 

have a good day. 

follow me on: 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et cochez les deux cases. 
