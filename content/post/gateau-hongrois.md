---
title: Hungarian cake
date: '2015-06-23'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
tags:
- Algeria
- Aid
- Oriental pastry
- Algerian cakes
- Cookies
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/G%C3%82TEAU-HONGROIS.jpg
---
[ ![HUNGARIAN CAKE](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/G%C3%82TEAU-HONGROIS.jpg) ](<https://www.amourdecuisine.fr/article-gateau-hongrois.html/gateau-hongrois>)

**Hungarian cake**

Hello everybody, 

What do I like about these delicious cakes, which are shaped at once in a baking tin, cooked, cut and you get a good amount of cakes, ready to eat. 

This is the case of this cake called: **Hungarian cake** , not only that it is very delicious, this cake is also very presentable, and that you can decorate according to your taste, you do not have to keep this chocolate decoration. This recipe is the realization of one of my friends and reader Alae M Bahri, and whom I thank very much for this very generous sharing. 

If you like this kind of cakes, then I recommend the following recipes that follow the same principle: 

**Hungarian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/G%C3%82TEAU-HONGROIS.jpg)

portions:  30  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 400 g margarine 
  * 7 tbsp sugar 
  * 3 egg yolks 
  * 1 tablespoon dried yeast (baker's yeast) 
  * 4 bags of chemical yeast 
  * vanilla 
  * lemon zest - 
  * 2 tablespoons cocoa 
  * flour. 
  * jam 
  * Turkish halwa 

decoration: 
  * 3 dark chocolate bars 
  * White chocolate. 



**Realization steps**

  1. beat the margarine and sugar to have a creamy mixture. 
  2. introduce the egg yolks as you go. 
  3. then add instant yeast, vanilla, and lemon zest. 
  4. add the baking powder mix with a little flour. 
  5. you can divide the dough in two at this point. 
  6. add a portion of the cocoa, to pick it up with flour to have a malleable and mole dough, 
  7. pick up the other dough with flour only. 
  8. divide each dough into two equal parts   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/G%C3%82TEAU-HONGROIS-2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42994>)
  9. spread the cocoa-free dough on a baking pan of almost 35 cm by 28 cm 
  10. spread over a layer of jam 
  11. spread the layer of dough with cocoa, on a sheet of baking paper or food film, to facilitate its transfer to the mold, it must have the same size of the mold, to cover the previous layer well   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/gateau-hongrois-5.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42995>)
  12. cover again with a layer of jam, as you can add Turkish halwa crumbs   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/gateau-hangrois-6.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42996>)
  13. cover the cake with a layer of cocoa-free dough   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/gateau-hangrois-7.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42997>)
  14. You can stop at three layers of dough, or you continue to have 4 layers of dough. 
  15. Bake in a preheated oven at 180 degrees C until the cake is completely cooked. 
  16. melt the dark chocolate in a bain marie 
  17. spread it on the cake, and decorate with white chocolate 
  18. let cool before cutting into small squares. 



[ ![HUNGARIAN CAKE 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/G%C3%82TEAU-HONGROIS-4.jpg) ](<https://www.amourdecuisine.fr/article-gateau-hongrois.html/gateau-hongrois-4>)

If you do not want to put dark chocolate to decorate the cake, you can add the cocoa layer and after cooking decorate with icing sugar. 

[ ![cake hangrois 8](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/gateau-hangrois-8.jpg) ](<https://www.amourdecuisine.fr/article-gateau-hongrois.html/gateau-hangrois-8>)
