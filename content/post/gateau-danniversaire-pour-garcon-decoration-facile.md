---
title: birthday cake for boy easy decoration
date: '2017-02-09'
categories:
- gateaux d'anniversaires, de fetes, celebrations
- recettes sucrees
tags:
- Pastry
- To taste
- Easy cooking
- Algerian cakes
- Chocolate
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gateau-danniversaire-pour-gar%C3%A7on-decoration-facile-3.jpg
---
![birthday cake for boy easy decoration 3](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gateau-danniversaire-pour-gar%C3%A7on-decoration-facile-3.jpg)

##  birthday cake for boy easy decoration 

Hello everybody, 

Yesterday was the birthday of my youngest, already 3 years old! I prepared for this occasion a birthday cake for boy with easy decoration and within reach of everyone. 

We do not have to waste so much money on a nice cake! Basically, I chose a yogurt cake that my kids love, decorated with a thin layer of cream cheese, and garnished with liquorice candy, mint chocolate sticks, M & ms and here we go. is played. 

I admit that I did not really break my head to make this cake, because firstly, it's a little party that we just do between us 5. Secondly, it's a cake that we will taste for the tasting and breakfast, so I did not want too much cream on it (I used my kids not to eat too much sweet and creamy cakes), and thirdly, it was a day to run in all directions, so I only had 2 hours to make the cake! 

You can see the video of the realization: 

**birthday cake for boy easy decoration**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gateau-danniversaire-pour-gar%C3%A7on-decoration-facile-1.jpg)

portions:  12  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** for yoghurt cake:   
I use the yoghurt pot to measure the ingredients: 

  * 2 jars of yoghurt 
  * 3 jars of sugar 
  * 1 jar of butter 
  * ½ pot of table oil 
  * 6 pots of flour 
  * 4 eggs 
  * 2 bags of baking powder 
  * 1 cup of vanilla coffee 

for the cream cheese: 
  * 280 gr of pheladelphia cheese 
  * 120 gr of liquid cream 
  * 4 tablespoons of sugar (you can add more, at home they do not like when it's too sweet) 

for decoration: 
  * licorice 
  * chocolate sticks 
  * M  & ms 
  * chocolate balls 
  * train-shaped toys 
  * chocolate 



**Realization steps** preparation of cake with yoghurt: 

  1. preheat the oven to 170 ° C 
  2. beat the eggs and sugar until blanching 
  3. add yogurt, melted butter and oil and whisk again. 
  4. stir in sifted flour and baking powder, mix well. 
  5. butter and flour your mold (mine is a mold with a chimney of almost 23 cm) 
  6. pour the device in it. 
  7. cook for 40 minutes, or depending on your oven, let cool completely before unmolding. 

preparation of the cream cheese: 
  1. prepare the filling by whisking phéladelphia cheese, liquid cream and sugar. 
  2. cool until filling. 

mounting: 
  1. cut the yoghurt cake in half, decorate with a thin layer of cream cheese   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gateau-danniversaire-pour-gar%C3%A7on-decoration-facile-2.jpg)
  2. cover with the second layer of cake, and cover the cake with the remaining cream. 
  3. then decorate with licorice and chocolate sticks to make the railroad 
  4. continue decorating the cake according to your taste. 



![birthday cake for boy easy decoration](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gateau-danniversaire-pour-gar%C3%A7on-decoration-facile.jpg)
