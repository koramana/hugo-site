---
title: new year's log
date: '2013-01-13'
categories:
- Bourek, brick, samoussa, slippers
- diverse cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/buche-au-chocolat.CR2-001_thumb4.jpg
---
![chocolate eve](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/buche-au-chocolat.CR2-001_thumb4.jpg)

Hello everybody, 

it's the countdown, there are only a few days left, or we will say 1 day and a half, and the majority is in search of the recipe of the log that he will savor with all the people he loves , or that she likes, waiting for the new lights of the New Year. 

so for your pleasure, and to help you, I'm doing a selection of logs, as beautiful as each other .... very easy to make logs, Bavarian logs, genoise-based logs, you have the choice ....   
we start with the bases of logs:   
[ ![biscuit rolls 002](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/biscuit-roule-002_thumb4.jpg) ](<https://www.amourdecuisine.fr/article-biscuit-roule-pour-buche-113713295.html>)

a [ Genoese ](<https://www.amourdecuisine.fr/article-biscuit-roule-pour-buche-113713295.html>) very easy to achieve, in addition with a [ video ](<https://www.amourdecuisine.fr/article-biscuit-roule-pour-buche-113713295.html>) very well exliquee ... it is super soft as base of new year's log. 

[ ![biscuit rolls with chocolate mirror.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/biscuit-roule-au-miroir-au-chocolat.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-buche-du-reveillon-2012-2013-en-video-etape-par-etape-113780010.html>)

[ buche with chocolate mirror ](<https://www.amourdecuisine.fr/article-buche-du-reveillon-2012-2013-en-video-etape-par-etape-113780010.html>) , explained with video 

[ ![chocolate mousse cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateaun-dacquoise-mousse-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateaun-dacquoise-mousse-au-chocolat.jpg>)

[ Trianon chocolate buche ](<https://www.amourdecuisine.fr/article-buche-fa-on-trianon-89548428.html>)

I hope you like this selection ... .. 

and you have this article if you do not want to make logs, but rather a cake: 

**new year's log**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateaun-dacquoise-mousse-au-chocolat.jpg)

portions:  10  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * flour 
  * egg 
  * sugar 
  * chocolate ganache 



**Realization steps**

  1. cook the base of the cake 


