---
title: Clafoutis with zucchini
date: '2015-06-30'
categories:
- Algerian cuisine
- diverse cuisine
- pizzas / quiches / pies and sandwiches
tags:
- ramadan 2015
- Ramadan
- Ramadhan
- Minced meat
- Express cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/clafoutis-de-courgettes1.jpg
---
[ ![zucchini clafoutis](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/clafoutis-de-courgettes1.jpg) ](<https://www.amourdecuisine.fr/article-clafoutis-aux-courgettes.html/clafoutis-de-courgettes-2>)

##  Clafoutis with zucchini 

Hello everybody, 

I always told you how much I like zucchini recipes, and when one of my friends on facebook shared this recipe, I did not hesitate to make this zucchini clafoutis which was super delicious. But here I did the clafoutis at the last moment with the Iftar, suddenly I could not take the photos, because I wanted to present the little dish very warm. I told myself that it will remain after Iftar to take some pictures, well, there was nothing left ... 

This zucchini clafoutis was so good that even my kids ate it and spooned it, hihihih. And because this recipe is delicious, I share it with you, using the photos of my girlfriend **Wamani M** who share it on facebook.   


**Clafoutis with zucchini**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/clafoutis-aux-courgettes1.jpg)

portions:  6  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 3 medium zucchini 
  * 2 tablespoons of butter 
  * 2 gr of minced meat (+ / - according to taste) 
  * salt and black pepper 
  * 200 ml of liquid cream 
  * 200 ml of milk 
  * 3 eggs 
  * 1 C. flour 
  * 30 gr of grated cheddar cheese 
  * 1 tablespoon grated mozzarella 
  * turmeric, salt, black pepper 



**Realization steps**

  1. clean and cut the zucchini into small ones. 
  2. fry zucchini in a pan with butter, salt and black pepper so that it is tender 
  3. remove the zucchini, and place the meat in the same pan, to cook with a little salt and black pepper too. 
  4. mix the cooked meat with the zucchini, and leave aside. 
  5. In a bowl put milk, cream, eggs, flour, grated cheddar, a case of grated mozzarella, turmeric, salt and black pepper. mix well The whole thing. 
  6. In a mold put the mixture zucchini - meat then pour the creamy mixture over 
  7. top with both cheeses and bake in preheated oven at 180 degrees C for about 20 minutes, or until clafoutis is cooked through. 



[ ![clafoutis with zucchini](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/clafoutis-aux-courgettes1.jpg) ](<https://www.amourdecuisine.fr/article-clafoutis-aux-courgettes.html/clafoutis-de-courgettes-2>)
