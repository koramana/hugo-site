---
title: minced meat tagine with olives
date: '2016-12-19'
categories:
- cuisine algerienne
- Dishes and salty recipes
tags:
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/viande-hachee-aux-olives.CR2_thumb1.jpg
---
##  minced meat tagine with olives 

Hello everybody, 

Here's one  recipe for minced meat  in white sauce, which I often realize when my husband is not there, or while something else to eat, because my husband as I always have you, is not a big fan of dishes in the white sauce ... a recipe that my mother often made us, this delicious  olive tajine with chopped meat  , But without the mushrooms ... 

The recipe in Arabic: 

**minced meat tagine with olives**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/viande-hachee-aux-olives.CR2_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients** for meatballs chopped: 

  * 350 gr of minced meat of lamb. 
  * a slice of bread, or 20 gr of bread crumbs. 
  * 2 tablespoons of cheese. 
  * chopped parsley. 
  * 1 clove of garlic. 
  * salt, black pepper and a pinch of cumin 

for the sauce: 
  * 1 onion 
  * 200 gr mushrooms (boxed for me) 
  * 300 gr of green olives. 
  * chopped parsley 
  * 1 glass of water. 
  * salt, black pepper. 
  * yellow food color, or Saffron 
  * 1 tablespoon of flour 



**Realization steps**

  1. in a salad bowl, place the crumbs in milk and allow to absorb a little. 
  2. in another bowl, mix the minced meat and spices, the chopped parsley, and add the crumb. 
  3. form minced meatballs. and fry in a little oil, just to give a nice color to the minced meatballs. 
  4. blanch the pitted olives, in water with two slices of lemon, to reduce the amount of salt, the lemon will keep the green color of the olives. 
  5. change the water, and boil a second time. 
  6. in a bowl, sauté the grated onion, in a little oil, add the mushrooms, cut into pieces, and sauté 5 minutes while watching. 
  7. add the minced meatballs, the olives, and the glass of water. 
  8. let it boil, add the spices (pay attention to the salt) 
  9. take a small amount of the sauce, dilute the flour in it, and put it back in the sauce. 
  10. let the sauce thicken a little, and remove your tajine from the heat. 
  11. present the dish decorated with chopped parsley, and slices of lemon. 


