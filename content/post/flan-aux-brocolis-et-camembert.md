---
title: broccoli and camembert flan
date: '2016-03-22'
categories:
- appetizer, tapas, appetizer
- Healthy cuisine
tags:
- Easy cooking
- eggs
- Curry
- Healthy cuisine
- Vegetarian cuisine
- accompaniment
- Flan Of Vegetables

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/flan-aux-brocolis-et-camembert_.jpg
---
[ ![flan with broccoli and camembert_](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/flan-aux-brocolis-et-camembert_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/flan-aux-brocolis-et-camembert_.jpg>)

##  broccoli and camembert flan 

Hello everybody, 

When I realized last time my recipe for [ salmon teriyaki with vegetables ](<https://www.amourdecuisine.fr/article-saumon-teriyaki-aux-legumes.html>) , A few small broccoli flowers had remained in the fridge, and I did not know what to do with, I started looking on the net, an easy and simple recipe to taste this vegetable that I like to cook and cook for my children and that miraculously, my eldest loves it a lot, even if I just steam it with a little salt. 

[ ![flan with broccoli and camembert 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/flan-aux-brocolis-et-camembert-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/flan-aux-brocolis-et-camembert-1.jpg>)   


**broccoli and camembert flan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/flan-aux-brocolis-et-camembert-3.jpg)

portions:  2  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 4 eggs 
  * 6 tbsp of 4% cream 
  * 200 gr of broccoli 
  * 60 gr of Camembert cheese 
  * 20 gr of Gruyère 
  * Salt, pepper and curry 



**Realization steps**

  1. Cook broccoli in salted water or steam. 
  2. Preheat the oven to 180 ° C. 
  3. In a bowl, beat the eggs and cream together. Stir in Gruyère cheese. Add salt and pepper. Add 1 tsp curry and mix well. 
  4. When broccoli is cooked, drain well. Arrange them in an oven-proof ramekin and pour the egg mixture over them. 
  5. Arrange 3 slices of camembert on the mixture and bake for 20 minutes. 



[ ![flan with broccoli and camembert 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/flan-aux-brocolis-et-camembert-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/flan-aux-brocolis-et-camembert-2.jpg>)
