---
title: chocolate cupcakes stuffed with nuts
date: '2017-02-13'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- Gateaux au chocolat
- gâteaux sablés, ghribiya
tags:
- Algerian cakes
- delicacies
- Shortbread
- Cookies
- biscuits
- Vanilla
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/petits-g%C3%A2teaux-au-chocolat-fourr%C3%A9s-aux-noix-2.jpg
---
[ ![chocolate cupcakes stuffed with nuts](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/petits-g%C3%A2teaux-au-chocolat-fourr%C3%A9s-aux-noix-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/petits-g%C3%A2teaux-au-chocolat-fourr%C3%A9s-aux-noix-2.jpg>)

##  chocolate cupcakes stuffed with nuts 

Hello everybody, 

Do you miss Lunetoiles' recipes? hihihihi, they are still there, but wait wisely on my email box, it's just me who can not find the rhythm to post the recipes. I hope to be able to organize and share all the delights of my friend Lunetoiles as soon as possible. 

The recipe in Arabic: 

**chocolate cupcakes stuffed with nuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/petits-g%C3%A2teaux-au-chocolat-fourr%C3%A9s-aux-noix.jpg)

portions:  50  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** (I divided the recipe by two and got about 50 pieces) 

  * 500 g softened butter 
  * 1 glass of icing sugar 
  * 3 eggs 
  * 1 sachet of baking powder 
  * 1 sachet of vanilla sugar 
  * 4 to 5 tablespoons cocoa (I have not put any) 
  * 1 vanilla pod 
  * 1 glass of cornflour 
  * flour needed 

PRANK CALL: 
  * 3 small measures of mixing nuts and almonds 
  * ½ measure of crystallized sugar 
  * Orange tree Flower water 
  * Apricot jam 

DECORATION: 
  * 500 gr of good quality dark chocolate 
  * 3 tbsp. sunflower oil 
  * walnut kernel 



**Realization steps** Prepare the shortcrust pastry: 

  1. Put the icing sugar with the vanilla pod in a small robot, I do it in an electric mill, to spray the vanilla pod powder. 
  2. Mix the soft butter and the icing sugar that has just been mixed with the vanilla, with the palm of your hand, add the vanilla sugar and the eggs one by one, sift the cocoa (I did not put some) and the yeast then incorporate them into the mixture. 
  3. Add the cornstarch while mixing and last the flour, you will get a smooth paste to spread it. 
  4. In the meantime, prepare the stuffing by mixing all the ingredients and gently moisten with the orange blossom water.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/farce-des-gateaux.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/farce-des-gateaux.jpg>)
  5. Spread the dough to a thickness of ½ cm, cut discs with a tea glass (do not use cookie cutters). 
  6. Take a disc of dough and with a brush brush a little apricot jam and put a little stuffing then put another disc on it, weld well and always cut with the tea glass to have curved cakes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/faconnage-des-gateaux-aux-noix.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/faconnage-des-gateaux-aux-noix.jpg>)
  7. Arrange them in a baking sheet lined with baking paper. 
  8. Preheat the oven to 160 ° C and cook for 20 minutes. 
  9. After 20 minutes, take them out and allow them to cool well. 
  10. In a bain-marie melt the chocolate, add the sunflower oil to have a beautiful shine, 
  11. glaze your cakes and decorate with the walnut kernels (for me half a kernel of nuts since the cake is small),   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dans-le-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dans-le-chocolat.jpg>)
  12. let dry and decorate in small boxes. 



[ ![chocolate cupcakes stuffed with nuts](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/petits-g%C3%A2teaux-au-chocolat-fourr%C3%A9s-aux-noix1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/petits-g%C3%A2teaux-au-chocolat-fourr%C3%A9s-aux-noix1.jpg>)
