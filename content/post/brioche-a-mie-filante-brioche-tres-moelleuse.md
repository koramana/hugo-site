---
title: Spinach brioche - very soft brioche
date: '2010-11-06'
categories:
- salades, verrines salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/11/brioche8_thumb.jpg
---
[ ![brioche8](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/brioche8_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2010/11/brioche8_2.jpg>)

Louiza has always made very beautiful brioche, and here is the proof, a super smoothy crumb, as we really like, without delay I pass you his recipe: 

ingredients:    


**Spinach brioche - very soft brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/brioche14_thumb.jpg)

Recipe type:  Brioche  portions:  16  Prep time:  45 mins  cooking:  40 mins  total:  1 hour 25 mins 

**Ingredients**

  * 2 eggs 
  * 1 egg yolk for gilding 
  * ½ l of milk 
  * 1 large glass of sugar, not too full (250 ml glass) 
  * ½ large glass of oil 
  * 1.5 tablespoons baker's yeast 
  * lemon zest 
  * about 1 kg of flour 
  * 2 teaspoons of vanilla sugar 



**Realization steps**

  1. warm up the milk. beat the eggs with the sugar and the vanilla sugar. 
  2. add zest, milk, oil, and yeast finish with flour, until a soft dough is sticky, but without kneading. 
  3. let it rise, degass and form brioches according to your inspiration, 
  4. Let your imagination be free. 
  5. brown with egg yolk, let it rise again and leather in the oven. 



so look at my [ brioches and pastries ](<https://www.amourdecuisine.fr/brioches-et-viennoiseries>)

[ ![Brioche](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/brioche_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2010/11/brioche_2.jpg>)

Bonne dégustation 
