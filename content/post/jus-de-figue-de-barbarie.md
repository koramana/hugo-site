---
title: barbaric fig juice
date: '2014-09-12'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/jus-de-figue-de-barbarie1.jpg
---
[ ![fig juice from barbarism1](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/jus-de-figue-de-barbarie1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/jus-de-figue-de-barbarie1.jpg>)

##  Prickly pear juice 

Hello everybody, 

How to make fig juice from barbarism? Do you think this is the question we ask ourselves when we see figs of barbarism? I doubt it, my question to me, and how to clean all these figs of barbarism without being bitten, lol ... 

In any case, when I am in Algeria, it is not a 6 pieces of prickly pie that I face, but rather a large bucket of water, as they sell here, sometimes my husband brings back 2 bucket of water, hihihihi, although it is a beautiful chore, but it remains a delice to eat figs of barbarism, and one can not stop at one piece, yum yum. 

A little documentation on this delicious fruit led me to this small result: 

The prickly pear is a fleshy and juicy fruit, rich in vitamins A, B, C. It contains minerals, such as potassium, calcium, magnesium, phosphorus, iron and copper. It has been used for centuries by traditional Mexican medicine to treat diabetes,  this fruit also helps fight against obesity ... 

Well, eat in, and enjoy yourself ... 

For my part, I allowed myself to make a good juice of fig of barbarism, I'm delighted, because the taste is really a test.   


**barbaric fig juice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/jus-de-figue-de-barbarie3.jpg)

portions:  1  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 prickly pears 
  * 1 lemon 
  * ½ c. powdered ginger (or grated) 
  * 1 C. honey 



**Realization steps**

  1. Remove the skin from the prickly pears. 
  2. Clean the lemon. 
  3. Pass the fruits in the centrifuge (if not like me to the blinder then to the Chinese) 
  4. pour the juice obtained in a glass. 
  5. Add honey, ginger, 
  6. stir well and taste 



[ ![fig juice from barbarism2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/jus-de-figue-de-barbarie2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/jus-de-figue-de-barbarie2.jpg>)
