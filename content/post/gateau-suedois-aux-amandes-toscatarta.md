---
title: Swedish cake with almonds, Toscatårta
date: '2015-09-11'
categories:
- cakes and cakes
- sweet recipes
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/gateau-suedois-aux-amandes-1.jpg
---
[ ![Swedish cake with almonds 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/gateau-suedois-aux-amandes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/gateau-suedois-aux-amandes-1.jpg>)

##  Swedish cake with almonds, Toscatårta 

Hello everybody, 

Here is a delicious Swedish cake with almonds, Toscatårta that will make your happiness, so that it is easy, and will make the happiness of your small tribune because it is too good. The Swedish almond cake, the toscatarta or the tosca cake is a simple four-quarter cake made with a crunchy layer of almonds sliced ​​in butter. 

This decoration is well known among Swedes, they use it in several other pastries ... yum yum what it must be good. 

Just for info, we can also name this cake: toscaKaka, but whatever his appointment, this cake will make many happy snack time ... so ready for the recipe shared with us Lunetoiles, a recipe she shared with me for more than 5 months, but I did not have time to post ... Thank you very much my dear. 

more 

[ ![Swedish cake with almonds 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/gateau-suedois-aux-amandes-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/gateau-suedois-aux-amandes-2.jpg>)   


**Swedish cake with almonds, Toscatårta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/gateau-suedois-aux-amandes-Toscat%C3%A5rta.jpg)

portions:  10  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients** Cake: 

  * 3 large eggs 
  * ¾ cup (150g) granulated sugar 
  * 2 teaspoons vanilla extract powder 
  * 1 cup + 1 tablespoon (150g) of flour 
  * 1 + ½ teaspoons of baking powder 
  * pinch of salt 
  * 3 tablespoons whole milk 
  * ⅓ cup (75g) unsalted butter, melted 
  * 50 gr of almond powders (not included in the original recipe, it's an addition on my part) 

Garnish: 
  * 50g of unsalted butter 
  * 100g of flaked almonds 
  * ¼ cup (50 g) sugar 
  * 2 teaspoons of flour 
  * 3 tablespoons thick cream 
  * 1 teaspoon vanilla extract powder 



**Realization steps**

  1. Preheat oven to 180 ° C / 350 ° F. 
  2. Butter and flour a 23 cm hinge pan, or wrap it with parchment paper, you might as well use a cake tin. 
  3. In the large bowl of an electric mixer, whisk together eggs and sugar for 5-7 minutes, or until thick and frothy. Continue beating with vanilla powder. Sift flour, baking powder and salt. 
  4. Pour the milk and melted butter into the mixture, then gently stir in the flour mixture and almond powder if you are using and mixing everything together. 
  5. Transfer the dough into the pan and bake for 30-35 minutes, or until a toothpick inserted in the center of the cake comes out wet but not completely clean, because the cake will still cook another 10 minutes of cooking . 
  6. Increase the fire to 200 ° C / 400 ° F. 
  7. Filling: Just before the 30-35 minutes are up, make the filling. Place the butter in a saucepan over medium heat. When it has melted, add the remaining ingredients and bring to a boil, allow the mixture to bubble for 1 minute. 
  8. After the cake has been baked for the first 30 to 35 minutes, remove from the oven and pour the almond mixture evenly over the top. Place it in the oven again and bake for about 10-15 minutes or until the filling is golden brown. 
  9. Cool in the mold for 10 minutes. With a small, sharp knife, loosen around the edges and carefully remove the sides of the pan before placing the cake (still on the base of the pan) on a rack and let cool completely. 



Note Note from Lunetoiles:   
-) I made the above recipe using a round cake pan of 22 cm with a removable bottom   
_personal tips_ :   
-) you can add a good big tablespoon of honey in the almond filling, the cake will be even more tasty   
-) I added 50 gr of almond powder in the cake dough 

[ ![Swedish cake with almonds](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/gateau-suedois-aux-amandes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/gateau-suedois-aux-amandes.jpg>)
