---
title: apple pie with mascarpone
date: '2016-06-30'
categories:
- recettes sucrees
- pies and tarts
tags:
- desserts
- To taste
- Algerian cakes
- Cakes
- Pastry
- Shortbread
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tarte-aux-pommes-au-mascarpone-3.jpg
---
![apple pie with mascarpone 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tarte-aux-pommes-au-mascarpone-3.jpg)

###  apple pie with mascarpone 

Hello everybody, 

Today I share with you a pie that Lunetoiles shared with me a few moments ago (in March to be precise): A beautiful apple pie with mascarpone. Frankly I love mascarpone, so any recipe that contains mascarpone can only be a treat for me ... I'm sure you're of my opinion. I had already made a recipe for [ apple cake with mascarpone ](<https://www.amourdecuisine.fr/article-genoise-mascarpone-pommes-caramelisees-67729142.html>) This is a totally different version of the recipe that Lunetoiles shares with us, but take a look at it, it will just as well please you this recipe. 

We liked the pie so much that I did not even have the chance to take pictures. I will try to redo the recipe as soon as I have time, to share it with you. In any case it must be said that apple pies are always the most delicious in my opinion, for example you can try the [ apple pie with butter ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-au-beurre-67056341.html>) , who always makes his success with my little family, and my guests. 

You can also do [ apple pie with brown cream, ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-a-la-creme-de-marron.html>) then this pie is a pure delight, besides I finished my supply of chestnut this year after having made this pie several times ... A treat. 

If you like apple pie I share with you the apple pie recipes I have on my blog: [ apple tart ](<https://www.amourdecuisine.fr/article-tarte-fine-aux-pommes-117353071.html>) , [ Apple pie cake ](<https://www.amourdecuisine.fr/article-tarte-flan-aux-pommes.html>) , [ Apple crumble pie ](<https://www.amourdecuisine.fr/article-tarte-crumble-aux-pommes.html>) , [ apple crème brûlée pie ](<https://www.amourdecuisine.fr/article-tarte-a-la-creme-brulee-aux-pommes.html>) , [ Norman apple pie ](<https://www.amourdecuisine.fr/article-tarte-normande-aux-pommes.html>) and the [ old apple pie ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-anciennes.html>) ... 

And it's not over, there is as I told you a nice collection of recipes of apple pies on the blog, just do a search in the small window in the right bar of the blog ... 

I spoke too much this time, because it's my love for apple pies that inspired me, hihihih and we continue to have this delicious recipe for Lunetoiles mascarpone apple pie. 

more 

![apple pie with mascarpone](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tarte-aux-pommes-au-mascarpone.jpg)

**apple pie with mascarpone**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tarte-aux-pommes-au-mascarpone-1.jpg)

portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** For the shortbread dough: 

  * 150 g flour 
  * 1 pinch of baking powder 
  * 50 g of sugar 
  * 75 g of soft butter 
  * 1 egg yolk 
  * 1 tablespoon of water 

Garnish : 
  * 4 apples 
  * 200 gr of mascarpone 
  * 3 eggs 
  * 35 gr of sugar 
  * 35 gr of brown sugar 
  * 1 sachet of vanilla sugar 

For the top of the pie: 
  * 25 gr of unsalted butter 
  * 25 gr of sugar 



**Realization steps** Prepare the shortcrust pastry: 

  1. To the Robot, put all the ingredients in the bowl, then knead until you have a ball of dough. 
  2. In the hand, put the flour, the yeast and the sugar in a salad bowl. Add the soft butter in the center, and rub in the hands until you have a consistency of sand. Then add the egg yolk and water without much work. 
  3. The dough must be flexible. Form a ball. 
  4. Put the dough in a cool food film for 1 hour so that it is easier to work. 

Garnish : 
  1. Preheat your oven to 170 ° C. 
  2. In a bowl beat the eggs with the sugars and mascarpone. 
  3. Spread the dough on a floured work surface or on a parchment paper and darken a pie plate. 
  4. Prick with a fork. 
  5. Pour the cream on the dough. 
  6. Arrange the peeled and thinly sliced ​​apples, overlapping and forming a rosette. 
  7. Cook for about 40 minutes. 
  8. minutes before the end of the cooking time, brush the pie with a brush, a mixture of melted butter and sugar. 
  9. And continue cooking until the apple slices are golden brown. 



![apple pie with mascarpone 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/tarte-aux-pommes-au-mascarpone-2.jpg)
