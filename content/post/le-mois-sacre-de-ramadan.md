---
title: The holy month of Ramadan
date: '2008-08-30'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/08/294986361.jpg
---
![greenbism](https://www.amourdecuisine.fr/wp-content/uploads/2008/08/29498306.png)

![card_ram02_c](https://www.amourdecuisine.fr/wp-content/uploads/2008/08/294986361.jpg)

* * *

**The month of Ramadhan**

* * *

![20980_1218657112](https://www.amourdecuisine.fr/wp-content/uploads/2008/08/294987321.jpg)

In his infinite wisdom, ALLAH has prescribed to His creatures the unalterable rules that assure them of a dignified life in this world and bliss in the Hereafter. Among these rules is the fast that he ordained to successive communities of believers. Far from being reduced to food abstinence, fasting requires the man to mobilize his whole being. The external observation of the rules of fasting must be accompanied by a mastery of the senses and more particularly of the language.   
The spiritual exercise represented by fasting must demonstrate to man his capacity to deprive himself for a time of what seemed to him indispensable. It must reveal to him that in this domain, as in many others, wanting is power, provided that the intention is firm and that the goal sought is the approval of ALLAH.   
The purpose of such an act of worship has been clearly defined as the search for the state of God's reverential fear (godliness) criterion of superiority of one individual over another. This quality is only measured by the works that witness it. The school of fasting is unparalleled and must lead to silence in everyone the tendency to domination of others, ostentation, the fear of other than God and all the insidious forms of the call of the devil, the only real enemy of the human race. 

* * *

**The initiation of the fast of Ramadan**

* * *

The fast of the month of Ramadan is the fourth of the five fundamental bases on which Islam is built. The obligation to fast was established for the Muslims, in the second year of the Hegira, by the revelation of this verse from the Koran: "O the believers! You have been prescribed as-Siyam [fasting] as prescribed to those before you, so will you attain piety "(Surah 2, verse 183). 

* * *

**The merits of Ramadan**

* * *

The Prophet (SAW) said:   
"Ramadan has come to you! It is a month of blessing. Allah wraps you with peace and brings down mercy. He discharges faults and answers requests. Allah looks on you to rival with ardor for this purpose and he boasts of you with His angels. Show Allah the best of yourselves, for whoever is deprived of the mercy of Allah, Mighty and Majestic is very unhappy! " (Ibn Majah)   
"It is the month of patience, and the reward of patience is Paradise. This is the month of giving. It is a month in which the resources of the believer increase. A month whose beginning is mercy, whose midst is forgiveness and the end of the enfranchisement of the fire of Hell. " (Bayhaqee)   
"When the first night of Ramadan arrives, Allah commands his Paradise:" Prepare and beautify yourself for My servants who will soon come to My home and My generosity to rest from the sorrows of the world! " (Bayhaqee)   
"He who fasteth the month of Ramadan, knowing and observing the rules of fasting, expiates his past." (Bukhari)   
"If the servants knew what is the value of the month of Ramadan, they would like the whole year to be Ramadan." (Bayhaqee) 

**Good works during Ramadan**

* * *

**The charity**

The Prophet (SAW) says:   
"The best charity is that accomplished during Ramadan" (Tirmidy)   
"Who gives food or drink to someone who is fasting, of a lawfully acquired good, the angels do not cease praying for him during Ramadan. The Archangel Gabriel prays for him the night of Destiny "(Bukhari) 

**The Tarawih prayer**

The Prophet (SAW) says, "Whoever gets up to pray during the nights of Ramadan, with faith and relying on the divine reward, God forgives his past sins." (Boukhari  & Moslim) 

**Reading the Qur'an**

The Prophet (SAW) redoubled the recitation of the Quran during the month of Ramadan. Gabriel came down to recite with him. (Bukhari).   
The Prophet (SAW) says, "Fasting and Ramadan prayer will intercede for man on the day of the resurrection. Fasting will say, "Lord! I stopped him from drinking and eating during the day. The Qur'an will say, "Lord! I prevented him from sleeping at night "" Accept our intersession for him! "". (Ahmed  & Nassai) 

**The spiritual retreat (I'tikaf)**

It consists of keeping the mosque in a spirit of devotion to please God. The Prophet (SAW) retired the last decade of Ramadan and kept practicing it until his death. He says, "The mosque is the refuge of every godly man. God has promised him who is retiring to grant him serenity and mercy, to make him cross the Sirat [bridge over Hell] to send him to His grace in Paradise "(Tirmidy). 

**Recommended practices of Ramadan**

* * *

**Break the fast as soon as the sun goes down**

The Prophet (SAW) says: "One keeps going on the right path as long as one hastens to break the fast". (Boukhari  & Moslim) 

**Invoking God at the very moment of breaking the fast**

The Prophet (SAW) says: "The fasting's request is not rejected when his fast is broken" (Ibn Majah)   
The Prophet (SAW) made the following invocation: "In the name of Allah! Oh my God! I fasted for you and broke with what you gave me! [ _Bismillah! Allahoumma laka submits wa wa ala rizqika aftartou!_ ] ". (Abu Daoud) 

**Take the Sahour and delay it**

Take a last meal "Sahour" at the end of the night, at the approach of dawn but without feeding beyond the Fajre. The Prophet (SAW) says: "The Sahur is all blessing; do not leave it. Take at least a sip of water because Allah sends His mercy and the angels ask for forgiveness for the one who makes this meal ". (Ahmed) 

![2808907021_3e9810cd90_t](https://www.amourdecuisine.fr/wp-content/uploads/2008/08/294986641.jpg)

* * *
