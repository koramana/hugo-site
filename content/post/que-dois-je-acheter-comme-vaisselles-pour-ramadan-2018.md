---
title: What should I buy as dishes for Ramadan 2018
date: '2018-05-11'
categories:
- tips and tricks

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/05/vaisselles.jpg
---
![What should I buy as dishes for Ramadan 2018](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/vaisselles.jpg)

Hello everybody, 

##  What should I buy as dishes for Ramadan 2018 

This is a question that is repeated each time the arrival of Ramadan. Many of my readers often ask me what they need to buy as necessary dishes that will be useful during Ramadan. 

I know women, who every Ramadan are to buy new dishes, a list as long as the list of the school supply! To be honest, I do not do that, and if necessary, I buy only the strict necessary. But today, I'm going to put myself in the shoes of someone who is doing his Ramadan for the first time, who does not know what he needs or does not know. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-3-260x300.jpg) For example, you can always opt for full table services, those services that often contain: bowls for [ chorba ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) and soups, plates or dishes of medium size, and dessert plates. 

Purchasing a service like this is more convenient, besides you are sure to present at the end a nice table. 

Do not forget the cutlery (spoons, forks, knives, teaspoons). Again, it's good to buy a set, like that, even if you have guests, your table will be uniform and more enjoyable. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/eau-parfume-aux-fraises-et-rose-1-190x300.jpg)

Then you will need a nice water service. 

There are people who just buy just sets of glasses. 

But perso I prefer water services with a pitcher or a large cup. Especially during Ramadan, we tend to prepare plenty of drinks and homemade juice, like [ Cherbet. ](<https://www.amourdecuisine.fr/article-cherbet-citron-limonade-algerienne-ramadan.html>)

In my opinion there is no more pleasant than putting this pretty pitcher in the middle of the table, filled with ice cube and refreshing drink, or just scented water, or mineral water. 

The most important thing to garnish Ramadan is quoted above! Each one has its own means, and each one has its own taste, but you can offer yourself a nice dish on a small budget. 

A perfect table is not complete without a beautiful tablecloth, a set of table and dishcloths, think about it! Especially table sets that can protect your tablecloth from large stains and soiling. 

For tea towels, there are of course washable and reusable kitchen towels, but for my par during Ramadan, I use most of the paper towels, it is more convenient, there are all the colors and the sizes, and in addition it avoids us to wash the tea towels every day. It's more hygienic from my point of view. 

Oh I forgot to mention the dishes of services for the [ salads ](<https://www.amourdecuisine.fr/article-salade-froide-aux-lentilles-verts.html>) , and the [ boureks ](<https://www.amourdecuisine.fr/article-bourek-annabi-carres-viande-hachee-et-oeuf.html>) Although there are people who like to present in individual dishes, but I think it will load the table too much. and also a tureen or a pretty pot to present the hot dishes and eat directly at the table. 

Je pense que j’ai a peu prêt lister ici ce dont vous aurez le plus besoin pour présenter une table bien garnie. Si vous pensez que j’ai oublié quelque chose, mentionnez le en commentaire et Saha ramdankoum. Bon Ramadan! 
