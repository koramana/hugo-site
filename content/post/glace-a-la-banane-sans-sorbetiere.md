---
title: banana ice cream without sorbetiere
date: '2013-09-15'
categories:
- appetizer, tapas, appetizer
- dips and sauces
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-a-la-banane_thumb.jpg
---
[ ![banana ice cream without sorbetiere](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-a-la-banane_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-a-la-banane.jpg>)

##  banana ice cream without sorbetiere 

Hello everybody, 

here is the easiest ice cream in the world, no need for an ice cream maker, no need to make the aisles back to the freezer to mix each time, no need to make a custard, no need for dairy products. 

an ice cream has to fall with only two ingredients, yes, this ice cream with banana and peanut butter, only needs two ingredients: banana and peanut butter (recipe coming soon). 

when I saw this recipe on an English blog, a while ago, I did not believe my eyes, I told myself 2 ingredients only .... I was just waiting for summer to come and do my little tests ... and I must admit, I was surprised, because this ice cream is beautiful, melting, and very rich in taste, especially if you like these two ingredients In addition, I was able to vary and replace peanut butter with other ingredients, the result is just "a delight". 

then wait for the forthcoming ice cream recipes. 

**banana ice cream without sorbetiere**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-banane-beurre-de-cacahuetes_thumb.jpg)

portions:  3  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 4 bananas 
  * 2 tablespoons of peanut butter   
yes nothing but these two ingredients. 



**Realization steps**

  1. peel the bananas, cut them in, and place them in the freezer for one night 
  2. in the bowl of a blinder (the capacity of mine is 300 W) place the pieces of banana 
  3. shield with the help of a spatula to replace the remaining pieces in the walls of the bowl. 
  4. continue to pulse and shield, and you will see this pretty creamy texture, that will take these frozen pieces of banana in pieces. 
  5. add the two scoops of peanut butter, and still just stir to mix.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-facile_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-facile_thumb.jpg>)
  6. here is the ice cream is ready to be served immediately, or you can put it in a box with lid that you put back in the freezer .... just think about taking it out 5 min before serving. 



[ ![ice cream with banana and peanut butter](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-a-la-banane-et-beurre-de-cacahuete_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-a-la-banane-et-beurre-de-cacahuete.jpg>)
