---
title: Eggplant rolls with minced meat
date: '2017-06-05'
categories:
- diverse cuisine
- Cuisine by country
- Dishes and salty recipes
- recipe for red meat (halal)
tags:
- Algeria
- Ramadan
- Easy cooking
- Ramadan 2016
- Ramadan 2017
- stuffed
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/06/roul%C3%A9s-daubergines-%C3%A0-la-viande-hach%C3%A9e-2-683x1024.jpg
---
![rolled eggplant with minced meat 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/roul%C3%A9s-daubergines-%C3%A0-la-viande-hach%C3%A9e-2-683x1024.jpg)

##  Eggplant rolls with minced meat 

Hello everybody, 

If there's one ingredient we all love at home, it's eggplant, so I'm always looking for eggplant recipes. When it comes to aubergine rolls with minced meat, it's a delicious dish of Turkish origin that I discovered a few years ago, and that I like to prepare especially during Ramadan. 

The recipe is very simple, but you will need some time to make the dish, so take a little bit of the lead you want to make. 

Eggplants are cooked in a frying pan, this will give you a lot of fat, but in fact it is the only time we will put oil in the dish, so in the end we have a perfect dish, that you will really love. 

**Eggplant rolls with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/Roul%C3%A9s-daubergines-%C3%A0-la-viande-hach%C3%A9e-1.jpg)

**Ingredients** For the meat stuffing: 

  * 450 gr of minced meat (beef) 
  * 1 grated onion 
  * 2 cloves garlic grated 
  * 2-3 tbsp. tablespoon breadcrumbs 
  * 1 bunch of chopped parsley 
  * salt and black pepper 

for the dish: 
  * 2 large aubergines, otherwise 3 medium 
  * 2 fresh tomatoes that stand well 
  * 1 green pepper 
  * 2 potatoes cut in half-round, cooked half-steamed with a little salt 
  * 2 tbsp. tomato paste 
  * boiling water 



**Realization steps**

  1. wash the eggplants and cut them in long strips and almost 3mm thick 
  2. salt and let them drain from their water in a Chinese 
  3. prepare the minced meat by mixing all the ingredients, and mix well. 
  4. take the eggplants, pat each piece on paper towels, to reduce its water 
  5. fry in a hot oil on both sides and drain on paper towels. 
  6. take the minced meat and form sausages almost 3 cm thick and the width of the aubergine strips. 
  7. roll each pudding in the eggplant to cover the meat, 
  8. prick with a toothpick so that it does not unfold, and garnish with a slice of salted tomato, and a piece of pepper 
  9. place the aubergine rolls as you go in a gratin dish 
  10. separate the rows of eggplants with half a slice of potatoes 
  11. dilute the tomato paste in the boiling water, and pour over the eggplant rolls to cover them well (for even cooking of the minced meat) 
  12. cover the baking tin with aluminum foil and bake in a pre-heat oven at 180 ° C for 20 to 30 minutes. 



![rolled eggplant with minced meat 3](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/roul%C3%A9s-daubergines-%C3%A0-la-viande-hach%C3%A9e-3.jpg)
