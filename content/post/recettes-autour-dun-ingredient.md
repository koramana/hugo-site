---
title: 'recipes around an ingredient:'
date: '2012-09-23'
categories:
- diverse cuisine
- Cuisine by country
- recettes aux poissons et fruits de mer

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/autour-d-un-ingredient-copie-11.jpg
---
![round-of-a-ingredient-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/autour-d-un-ingredient-copie-11.jpg)

Hello everybody, 

this morning during a walk with an English blogger friend, I was surprised to see an article that attracted me a lot. 

the article was titled: Sundy Supper "Apple" .... so it was so we can say (it's not a translation to the letter) "the Sunday recipe, around the apple. 

they were more than 70 bloggers who contacted each other on twitter each week, to choose the ingredient of the week, and each will realize the recipe of their choice with this ingredient, and the recipe will be put online on Sunday. 

in the article below, we could see all the recipes of all the other bloggers "the link of the blog and the title of the recipe" it was magnificent this sharing .... 

I love this spirit of community between English bloggers, and I love that it is present between us bloggers from the French community. 

We make the recipe in the week, the articles will be ready on Saturday to put the list of blogs with the title of each recipe. and the article will be posted on Sunday. 

you can pass the information around you to other bloggers who are your friends and are not in this community, they will be able to reach us as soon as possible. 

it's a game to share the links between us bloggers, so it'll be good for all of us. 

si vous ne pouvez pas acceder au forum alors que vous etes une blogueuse, il se peut que c’est par ce que vous n’etes pas membre de la communaute, vous pouvez toujours nous joindre dessus, merci 
