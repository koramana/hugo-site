---
title: raspberry pears amandine pie
date: '2016-09-04'
categories:
- cakes and cakes
tags:
- Cakes
- desserts
- To taste
- The start of the school year
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/tarte-amandine-poire-framboise-2_thumb_1.jpg
---
##  raspberry pears amandine pie 

Hello everybody, 

Here is a super delicious recipe that sent me Lunetoiles yesterday afternoon, and as I read the recipe, I immediately went to do it, the only difference, I did not have Raspberries on hand, I made pears, and I made a layer of jam on the pie shell 

What I can tell you, I had to prepare the pie at 18h, she was ready for the milk after the ftour, well I did not have the chance to take pictures, so it is absolutely necessary, tested and approved, I assure you. 

thank you very much my dear lunetoiles for this wonderful recipe, and also thanks for the photos 

**raspberry pears amandine pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/tarte-amandine-poire-framboise-2_thumb_1.jpg)

portions:  8 

**Ingredients** For the shortbread dough: 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

Almond cream: 
  * 12Og of almond powder 
  * 2 eggs 
  * 100g of sugar 
  * 200 ml whole liquid cream 
  * 3 pears 
  * 125 gr raspberries 

finish: 
  * flaked almonds 
  * 2 tbsp. apricot jam 



**Realization steps** The shortbread dough: 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a paste is removed from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Darken a pie circle 28-30 cm in diameter. 
  6. Stir the batter stock with a fork and chill for the time to prepare the filling. 

The almond cream: 
  1. In the bowl of the beater this time equipped with the whisk, beat the eggs with the sugar. 
  2. When the mixture is frothy add the almond powder and the cream. 
  3. Beat again. Place half-pears before peeling, emptying and slicing in slices on the pie shell and then sprinkle with fresh raspberries evenly, 
  4. cover with the almond cream. 
  5. Sprinkle with blanched almonds. 
  6. Put in the oven for 30 minutes at 180 ° C (th.6). 
  7. Let cool. When the tart is warm, heat the jam in the microwave and brush the jam pie with a brush. 
  8. Circle and present on a serving platter. 



Thanks to Lunetoiles for this delicacy we all love at home 

bonne journee et saha ramdankoum 
