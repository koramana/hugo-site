---
title: Clementine Curd
date: '2016-02-15'
categories:
- jams and spreads
tags:
- creams
- Based
- desserts
- Lemon curd
- Spread
- pancakes
- Toasts

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/clementine-curd-010_thumb1.jpg
---
![clementine curd 010](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/clementine-curd-010_thumb1.jpg)

##  Clementine Curd 

Hello everybody, 

it's almost the end of the season of these delicious citrus fruits, pretty clementines, and I must tell you that I planned to make this easy and very creamy cream for 3 months, but each time we bought clementine, I found only the skin in the corners of the house, with my two little mice that used it at will, so now with the clementines that become more and more acidic, I finally realized this delicious recipe . 

So here is a way to taste this delicious fruit while savoring it as a cream. to use to garnish these cakes, to fill these funds with pies, or to stuff in donuts or to spread its delicious [ pancakes ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee.html>) .   


**Clementine Curd**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/clementine-curd-015_thumb1.jpg)

portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 100 ml pressed juice of 4 to 5 clementines. 
  * 140 g of sugar 
  * 2 eggs 
  * 65 g soft butter in small cubes 
  * 1 pinch of salt 



**Realization steps**

  1. peel the clementines then mix them to the blinder. 
  2. pass them in Chinese to have only juice 100 ml, if possible. 
  3. put the juice back in the bowl of the robt, add the sugar and the eggs and mix again 
  4. put now in a saucepan over low heat and add the butter. 
  5. stir constantly until the butter melts. 
  6. Increase the heat and cook without stopping mixing until you get a cream of thick consistency. 
  7. the cream curd is ready when it laps the back of a spoon. 
  8. pour the cream into a container that you have previously sterilized. 



good tasting, and the next recipe 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
