---
title: nutella mousse without egg
date: '2017-10-19'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/MOUSSE-AU-NUTELLA7.jpg
---
[ ![NUTELLA7 FOAM](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/MOUSSE-AU-NUTELLA7.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/MOUSSE-AU-NUTELLA7.jpg>)

##  nutella mousse without egg 

Hello everybody, 

A big craving for chocolate mousse that I had this morning, I look in my fridge and I do not have an egg, then I remember this recipe that I saw it did a little time on a English blogs, and I was just looking for an opportunity to make it happen. 

A very easy recipe this nutella mousse without egg, you will need only two ingredients, a good whip, and in 20 minutes, your mousse all chocolate, and in your verrines ready to serve. You can also see on the blog, [ a duo of chocolate mousse ](<https://www.amourdecuisine.fr/article-duo-de-mousse-au-chocolat.html> "Chocolate mousse duo") , and many other recipes from [ foam ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=mousse&sa=Rechercher&siteurl=www.amourdecuisine.fr%2F&ref=&ss=437j75839j6>)

You can see here the new video on my youtube channel: 

**nutella mousse without egg**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/MOUSSE-AU-NUTELLA-0005.jpg)

Recipe type:  Dessert  portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 250 ml whole cold liquid cream 
  * 3 to 4 tablespoons of Nutella, or more depending on your taste. 



**Realization steps**

  1. hours before making this mousse, place the cream in the freezer 
  2. Put your cream in Chantilly, 
  3. add the Nutella one spoon at a time while continuing to whisk   
you will have a cream smooth and creamy. 
  4. place this mousse in a salad bowl and cover it with a film of food. 
  5. keep in the fridge until you serve, or you can fill your verrines with a pastry bag. 



[ ![Foam AU NUTELLA 0003](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Mousse-AU-NUTELLA-0003.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Mousse-AU-NUTELLA-0003.jpg>)
