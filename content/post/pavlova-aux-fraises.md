---
title: strawberry pavlova
date: '2017-12-31'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- desserts
- To taste
- delicacies
- meringues
- Chantilly
- Easy recipe
- Fast Recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-10.jpg
---
[ ![strawberry pavlova 10](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-10.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-10.jpg>)

##  strawberry pavlova 

Hello everybody, 

Yum yum! A **strawberry pavlova** !!! I told you that the **Pavlova** is the **dessert** number one in my children. When I give them the last pieces, they tell me how will be the next? Frankly it makes me happy to make pavlovas, because it's super easy to make and to decorate made according to your taste, whether it is fruit or side quoted cream, the most important is to keep the balance of sugar, do not overdo it, even in the meringue, reduce the sugar compared to the usual, the cream does not have to be too sweet either , and here is a dessert that you will enjoy without having too much remorse, plus it is rich in protein! 

This time, this **strawberry pavlova** was so simple. In addition I made my whipped cream with cream 30%, but I made sure that the cream is very cool cold, beater feet well cold, the bowl too, and I whipped and mounted whipped cream in an ice-cold bath. and I managed to have a nice whipped cream. 

This strawberry pavlova was ready at 5pm, and my kids really wanted to taste it, so I took some pictures in the light of the chandelier in my living room, was a bit strong as light, and it does not give this pavlova strawberry its true value !! 

and here is the video recipe: 

{{< youtube ZTDijmLZBLY >}} 

[ ![strawberry pavlova 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-4.jpg>)   


**strawberry pavlova**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-7.jpg)

portions:  10  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients** for the meringue: 

  * 3 egg whites 
  * 150 gr of sugar 
  * 2 teaspoons of Maïzena 

for garnish : 
  * 200 ml of fresh cream whipped cream with 2 tablespoons of sugar 
  * strawberries 
  * mint leaves. 



**Realization steps**

  1. Preparation of the meringue: 
  2. Preheat the oven to 130 ° C. 
  3. Using a drummer, mount the whites to very firm snow starting at medium speed then gradually increasing. 
  4. Gradually add the sugar to the spoon while the egg whites rise. Then add the cornstarch and mix again. 
  5. At this point you can use the pastry bag to form the meringue disc, I did not do it, I rather chose to make a rectangular shape that I decorated with a fork.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/meringue-de-pavlova.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/meringue-de-pavlova.jpg>)
  6. Bake for 1 cooking at 130 ° C. (If you double the amount, increase the cooking time) 
  7. Remove from oven and let cool before taking off. 
  8. before serving the dessert (at least 1 hour before), put the cold cream very cold in chantilly with 2 tablespoons of sugar 
  9. Spread the whipped cream on the meringue which must be cold. 
  10. decorate with well washed strawberries. add some mint leaves for the touch of color. 
  11. Return to the fridge until serving 



[ ![strawberry pavlova 7](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-7.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-7.jpg>)

[ ![strawberry pavlova 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pavlova-aux-fraises-1.jpg>)
