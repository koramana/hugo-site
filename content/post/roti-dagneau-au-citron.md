---
title: roasted lamb with lemon
date: '2015-09-23'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/viande-rotie-au-citron-1_thumb1.jpg
---
##  roasted lamb with lemon 

Hello everybody, 

A way of cooking leg of lamb meat very quickly, and have a tender meat wish, 

in addition a cooking that will not take more than 30 minutes. 

I strongly recommend this method for having too good a meat. 

you can also see my recipes from: 

[ leg of lamb roasted in the oven ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-au-four-aid-el-adha-2012-111759550.html>)

[ Roasted lamb shoulder ](<https://www.amourdecuisine.fr/article-epaule-d-agneau-rotie-au-four-113882735.html>)

[ leg of lamb in the east ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-a-l-orientale.html>)   


**roasted lamb with lemon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/viande-rotie-au-citron-1_thumb1.jpg)

portions:  4  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * 4 to 5 pieces of medium sized leg of meat cut 
  * the juice of a lemon (more or less, according to your taste for lemon) 
  * salt 
  * black pepper 
  * ¼ cup of cardamom coffee powder 
  * ¼ cup of cinnamon 
  * 2 cloves garlic crushed 
  * ½ teaspoon coriander powder 
  * 4 tablespoons of oil 



**Realization steps**

  1. marinate the meat in the sauce prepared with the ingredients at the top 
  2. leave almost 1 hour 
  3. in a pot, over low heat, put the oil 
  4. put on the meat and cover 
  5. try each time to turn the meat, at the beginning the meat will reject its water, always leave covered, after, this sauce will be reduced, and the meat will begin to roast, 
  6. watch for a nice color. 



then it's an easy recipe day, I've presented it for my kids with fries, and for my husband and I a [ spicy cream with peanut butter ](<https://www.amourdecuisine.fr/article-creme-piquante-au-beurre-de-cacahuete-89052515.html>) . 
