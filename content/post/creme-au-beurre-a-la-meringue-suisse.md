---
title: butter cream with Swiss meringue
date: '2012-03-11'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-au-beurre-a-la-meringue-suisse3_thumb2-300x286.jpg
---
##  butter cream with Swiss meringue 

Hello everybody, 

this cream butter with Swiss meringue is a very good and smooth cream, ideal for icing cakes or cupcakes, I really like to do because it was really very easy to achieve. 

when you make a certain weight of egg white, you double your weight in sugar, and triple its weight in butter, that's great. 

so for my part, I went out my egg whites freezer, because yes, if you do not know, when in a recipe you only use the egg yolk and you said: 

#  what to do with egg whites? 

do not discard them, put them in a bag in the freezer, the ideal is to place an egg white per small sachet, like that, if in a recipe you need a number of egg white, you remove the number of bags, do not defrost your egg whites in the microwave, or in hot water. Or take them out a little early from the freezer, if you remember, or place the bags tightly closed, without water at normal temperature, it will really help in defrosting. 

then we come back to cream, so like me, I take what I have as egg whites, I weigh their weight, then I multiply by two, and it will be the weight of sugar. 

I multiply the weight of the egg whites by three, and it will be the weight of the butter. 

remove the butter a little before the refrigerator so that it is soft.   


**butter cream with Swiss meringue**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-au-beurre-a-la-meringue-suisse3_thumb2-300x286.jpg)

**Ingredients**

  * 130 gr of egg white 
  * 260 gr of sugar 
  * 390 gr of butter 
  * vanilla 
  * dye (here pink gel for me) 



**Realization steps**

  1. method of preparation: 
  2. mix the sugar with the egg white 
  3. place it in a bain-marie, and mix, not whip. mix until the sugar grains are completely dissolved in the eouf white, you can check this by touching the mixture and between your fingers, you must not feel the sugar grain anymore. 
  4. when the mixture is well viscous it is ready, remove it from the bain marie 
  5. whip the good to have a very beautiful meringue, the ideal is to work with your trouble. 
  6. Now change the squeegee of your whip, and start beating the meringue mixture and add the butter slowly. 
  7. at some point, the mixture will attach to the whip squeegee, do not worry, you do not miss your cream, keep whipping while adding the butter, and you'll have a nice butter cream . 
  8. now add the vanilla, and the dye. 
  9. your cream is ready, it is smooth, you can decorate your cupcakes and your cakes. 


