---
title: cheesecake mascarpone / speculoos, is this Friday the 13th?
date: '2010-02-17'
categories:
- recette a la viande rouge ( halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/02/cheesecake-019_thumb1.jpg
---
the video is here you will tell you what is this title ???? Well I think yesterday was Friday the 13th when I was doing this cheesecake, so I tell you the story, I had to make my cheescake with the soft cheese (fresh cheese) but we do not like the sweet / salty a the house, so I thought the mascarpone will do the trick, the quantity was not enough, I said I'll add some fresh cream mounted whipped cream, but the cream has sworn not to become a ... whipped. not serious. I mix my fresh cream liquid & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![cheesecake 019](https://www.amourdecuisine.fr/wp-content/uploads/2010/02/cheesecake-019_thumb1.jpg)

you are going to tell you what is this title ???? Well I think yesterday was Friday the 13th when I was doing this cheesecake, so I tell you the story, I had to make my cheescake with the soft cheese (fresh cheese) but we do not like the sweet / salty a the house, so I thought the mascarpone will do the trick, the quantity was not enough, I said I'll add some fresh cream mounted whipped cream, but the cream has sworn not to become a ... whipped. not serious. 

I mix my liquid cream with the mascarpone, I start adding the eggs one by one and I put 3 instead of 2 (I do not know why?), and I had a very sparkling mixture, and I wanted to do a marbled cheesecake, but adding chocolate, the chocolate mixture becomes thicker and it starts at the base, so too bad for the marbled. 

I put my cake to cook it, but the removable base mold is doing now, and all the fat starts to come out if and by, so what I'm presenting to you is this really a cheesecake? Or was yesterday yesterday Friday 13th? 

the recipe I had to do: 

for the base: 

  * 150 gr of biscuits (speeculoos for me) 
  * 75gr of butter 



for the cream: 

  * 150 gr of dark chocolate 
  * 500 gr of fresh cheese 
  * 150 grams of sugar (better to put less, the English like sugar, 120 will be super good) 
  * 5 ml of vanilla extract 
  * 2 eggs 



crush the cookies, and add the melted butter to have a homogeneous mixture, heap well this mixture in a baking mold. 

melt the chocolate and let cool. 

whisk the fresh cheese, add the sugar, while mixing then add the eggs one by one, add the vanilla extract, and pour half of this mixture on the basis of speeculoos. 

add the remaining half of the chocolate, and mix well again, and add this on the first mixture, trying to make mottling. 

and place in a preheated oven between 40 to 50 minutes, or until the cheesecake is cooked 

faut quand même avoué que c’était super délicieux. 
