---
title: spilled cake with caramelized apples
date: '2017-03-21'
categories:
- gateaux, et cakes
- recettes sucrees
tags:
- Fluffy cake
- Cake
- Algerian cakes
- To taste
- Soft
- desserts
- fondant

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/gateau-renvers%C3%A9-aux-pommes-caram%C3%A8lis%C3%A9es-683x1024.jpg
---
![rolled cake with caramelized apples](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/gateau-renvers%C3%A9-aux-pommes-caram%C3%A8lis%C3%A9es-683x1024.jpg)

##  spilled cake with caramelized apples 

Hello everybody, 

This inverted cake with caramelized apples is the number one recipe at home and the favorite among my children. It's a super-sweet cake, super-melt in the mouth, and with those nice caramelized apples on top, you're not going to stop at one. 

  
So today you will know how to make a caramelized apple spilled cake very easily? This caramelized apple cake is so simple and easy that you can do it every day! 

**spilled cake with caramelized apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/gateau-renvers%C3%A9-aux-pommes-caram%C3%A8lis%C3%A9es-1.jpg)

**Ingredients** For the cake: 

  * 4 eggs 
  * 200 gr of flour 
  * 150 gr of sugar 
  * 120 gr of butter 
  * 1 C. coffee baking powder 
  * vanilla, 
  * 3 well chopped apples 

For garnish and caramel: 
  * apples 
  * 100 gr of sugar 
  * 30 ml of water 
  * ½ lemon juice 



**Realization steps**

  1. Preheat the oven to 180 °. 
  2. In a saucepan, put the sugar with the water and the lemon juice, cook for 15 minutes until you get a caramel. 
  3. In a slightly buttered pan, line the bottom of the pan with the liquid caramel. 
  4. Put the pieces of apples to form a rosette 
  5. whip butter, sugar and vanilla for a nice cream 
  6. add the eggs one by one while whisking. 
  7. add flour and sifted yeast, then chopped apples 
  8. Pour the mixture on apples and caramel. 
  9. Bake for 40 minutes. 
  10. Take the cake out of the oven, let cool on a cooling rack, a moment. 
  11. Then unmould on a serving platter. 
  12. Let cool before eating. 



![spilled cake with caramelized apples 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/gateau-renvers%C3%A9-aux-pommes-caram%C3%A8lis%C3%A9es-2-770x1024.jpg)
