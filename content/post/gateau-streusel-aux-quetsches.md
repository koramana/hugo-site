---
title: streusel cake with quetsches
date: '2017-08-23'
categories:
- cakes and cakes
- sweet recipes
tags:
- To taste
- Algerian cakes
- Easy cooking
- Pastry
- Inratable cooking
- desserts
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/gateau-streuse-aux-quetsches-3.jpg
---
![cake-streuse-to-plum-3](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/gateau-streuse-aux-quetsches-3.jpg)

##  streusel cake with quetsches 

Hello everybody, 

I'm going to galley for almost 3 weeks with all the work and all the dust, hihihihi so my friends do not expect too much new recipes in the next few days. But I will still try to update my blog and post all these recipes waiting in the archives of my laptop. 

So this streusel cake with quetsches is just the ideal if you are in a hurry to make a delicious cake for the snack of your children. Even hot out of the oven, accompany a piece of this streusel cakes to the quetsches of a scoop of vanilla ice cream, certainly make a lot of fun for children. This was the case with mine who devoured this little delight in the blink of an eye. 

**streusel cake with quetsches**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/gateau-steusel-aux-quetsches.jpg)

portions:  6  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients** I divided the quantities on two to have a small cake: Dry ingredients 

  * 2 cups of flour (cup = 240 ml) 
  * 2 tbsp. coffee baking powder 
  * 1 C. coffee baking soda 
  * ½ c. cinnamon 
  * ¼ c. salt 

Liquid ingredients 
  * ½ cup of sugar (sugar is not liquid but mixes with liquids) 
  * 2 large eggs 
  * 2 tbsp. melted butter 
  * Cup of buttermilk 
  * ⅓ cup of yoghurt 

Streusel garnish: 
  * 3 c. flour 
  * 3 c. chopped almonds 
  * ¼ cup brown sugar 
  * 1 C. cinnamon 
  * 2 tbsp. cold butter 
  * 1 and ½ cup pitted quetsches and cut in half 



**Realization steps**

  1. Preheat the oven to 180 ° C. and lightly butter a dish of 25 x 30 cm 

start by preparing the crumble (streusel): 
  1. mix the flour, chopped almonds, sugar and cinnamon in a small bowl. 
  2. Add the cold butter and crush in your fingers until you have the crumbles (lumps) that form.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/gateau-streusel-aux-quetsches-5.jpg)
  3. In a large bowl, mix dry ingredients. 
  4. In another bowl, beat sugar, eggs and butter together until frothy and light in color. 
  5. add buttermilk and yoghurt. 
  6. Add the liquid ingredients to the dry ingredients and mix until it becomes homogeneous. 
  7. Pour the dough into the prepared pan. 
  8. prick the quetsches evenly in the dough. 
  9. garnish with crumble and bake for 45-50 minutes or until an inserted toothpick comes out clean. 



![cake-streuse-to-plum-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/gateau-streuse-aux-quetsches-1.jpg)
