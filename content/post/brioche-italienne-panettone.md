---
title: Panettone Italian brioche
date: '2017-11-19'
categories:
- bakery
- Buns and pastries
tags:
- Boulange
- To taste
- Breakfast
- Ramadan
- Algeria
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/11/panettone_.jpg
---
![Panettone Italian brioche](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/panettone_.jpg)

Panettone Italian brioche 

Hello everybody, 

At the end of the panettone sales season at home, my children continue to claim it, I confess that I too much prefer it to accompany my coffee latte, and that's where comes this recipe that I have kept and have been making for almost 4 years, having made a good amount of this brioche for the party at my son's school in a workshop, the panettones were so good and beautifully successful that I kept the recipe. 

**Panettone Italian brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/panettone-brioche-italienne-de-no%C3%ABl.jpg)

**Ingredients**

  * 3 cups flour (250 ml cup) more to work the dough. 
  * 3 eggs 
  * 60 ml of lukewarm water 
  * 60 ml warm milk 
  * 1 C. baking yeast 
  * 75 gr of butter at room temperature 
  * 125 gr of sugar 
  * Zest of 1 Orange 
  * Zest of 1 lemon 
  * 1 tsp vanilla extract 
  * 1 C. salt 
  * ½ cup raisins 
  * ¼ cup candied orange peel and lemon peel 
  * for gilding: 
  * 1 egg yolk 
  * milk 
  * vanilla sugar   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/panettone-brioche-italienne-de-no%C3%ABl-1.jpg)



**Realization steps**

  1. In a small bowl, mix the water and the milk, sprinkle in the baking yeast and let stand for 5 minutes. 
  2. add 4 tablespoons flour and 1 tablespoon sugar to the yeast mixture, mix to combine and cover with a cling film. Let stand in a warm place and let rise until doubled in size, about 30 minutes. 
  3. In the bowl of a mess, whisk together the eggs, sugar, vanilla, lemon peel and orange zest, add the yeast mixture and whisk until combined. 
  4. Using the kneading hook, turn on the kneader, and add the flour and salt, knead at medium speed. 
  5. Add the butter and mix until well incorporated. 
  6. add the raisins and candied peel and mix for a few more seconds. 
  7. Oil a large bowl. 
  8. pour the dough on a well-floured surface and work it for a minute or until it picks up, place it in the oiled bowl and brush the top with a little more vegetable oil to avoid formation of a crust. cover well with a film of food and let rise away from drafts in a warm place for 3 to 4 hours or until the volume is triple. 
  9. cover a panettone pan or cake pan with 20 cm of baking paper and set aside. 
  10. once the dough is lifted, just degass it a little bit of a little movement on a floured space, to pick it up and place it in the panettone pan 
  11. brush the top with a little melted butter, cover with plastic wrap again and let it rise again for 2 to 3 hours or until tripled again. 
  12. Preheat the oven to 180 ° C, brush the surface of the panettone with a little milk + egg yolk and vanilla, and bake in the preheated oven by lowering the temperature to 160 ° C for about 45 minutes or until a skewer inserted in the center comes out clean. 
  13. Let cool for about 20 minutes and serve! 


