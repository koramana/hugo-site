---
title: cream with cauliflower
date: '2013-10-22'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/soupe-de-chou-fleur_thumb.jpg
---
##  cream with cauliflower 

Hello everybody, 

not easy to give the cauliflower to children, but this cream with cauliflower, well, they will never realize that there are cauliflower in it, and it's really too good, I can only you recommend this delicious recipe, which I made over a month ago, and I promised you to post it, but not have time at all. 

then to your aprons:   


**cream with cauliflower**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/soupe-de-chou-fleur_thumb.jpg)

Recipe type:  soup, dish  portions:  2  Prep time:  10 mins  cooking:  40 mins  total:  50 mins 

**Ingredients**

  * 1 cauliflower 
  * 2 potatoes 
  * 750 ml of water 
  * 200 gr of chicken breast 
  * salt, pepper, cumin powder, and coriander powder. 
  * 50 g of butter 
  * 2 tablespoons fresh cream 



**Realization steps**

  1. Detail the cauliflower. 
  2. Put in a saucepan, cut cauliflower ("flowers"), potatoes, chicken breast, water, spices and salt. 
  3. Cook until everything is tender. 
  4. Mix everything with the butter and cream. 
  5. Season with salt and pepper if necessary. 



[ ![cream-of-cauliflower fleur_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/creme-de-chou-fleur_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/creme-de-chou-fleur_thumb1.jpg>)
