---
title: homemade chestnut cream recipe
date: '2015-11-13'
categories:
- jams and spreads
- basic pastry recipes
tags:
- desserts
- creams
- jam
- To taste
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/creme-de-marron-.jpg
---
##  [ ![Chestnut cream](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/creme-de-marron-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/creme-de-marron-.jpg>)

##  homemade chestnut cream recipe 

Hello everybody 

yum yum what do I like homemade chestnut cream? This year I made a nice reservation, because I want to make many recipes based homemade chestnut cream, such as the [ muffins with chestnuts ](<https://www.amourdecuisine.fr/article-28328374.html>) , the [ apple pie with brown cream ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-a-la-creme-de-marron.html>) , the [ cabbage with chestnut cream ](<https://www.amourdecuisine.fr/article-choux-a-la-creme-de-marrons.html>) or again [ pannacotta with chestnut cream ](<https://www.amourdecuisine.fr/article-panna-cotta-a-la-creme-de-marron.html>) . 

I also made shortbreads with chestnut cream a recipe that I will share with you very soon: 

![shortbreads with brown cream Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/sabl%C3%A9s-%C3%A0-la-creme-de-marron-Aid-2016-200x300.jpg)

Here in England, I do not find the cream of chestnuts to trade, so I am obliged to make it home ... But the only good thing is that I do not prepare the cream from chestnuts or chestnuts, because here and by magic, in the trade we find the mashed chestnuts, that is to say the cooked chestnuts, and without the skin, then reduced in puree, so half of the work, or rather the most difficult part in the preparation of the chestnut cream is already made ..., hihihih 

## 

I'm sure there will be a lot of girls here in England who are going to be happy, so we go to the recipe:   


**homemade chestnut cream recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/creme-de-marron-2.jpg)

portions:  8  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 400 grams of pureed chestnuts 
  * 315 sugar gre 
  * water 
  * 1 vanilla pod 



**Realization steps**

  1. Put the sugar in a large pot and dilute with a little water to obtain a thick syrup of sugar. 
  2. Cook on high heat until you reach a large bowl (120 degrees C) 
  3. Add the chestnut puree. Stir quickly. Continue to cook on high heat, the cream of brown will thicken and darken after about 15 minutes. 
  4. To test the thickness of the chestnut cream, it is possible to put a ladle in a plate or dish and turn it on its side. If it stays in place, it is ready. 
  5. Stop the fire, then fill the glass jars initially prepared. Fill them to the brim, close them immediately and fill them upside down. This avoids any risk of mold. 
  6. Keep in a cupboard and go out for any occasion. Serve with white cheese or cream, spread on a slice of bread for breakfast 



for those who do not have mashed chestnut it is necessary to put the chestnuts in a big pressure cooker. 

Fill it with water up to the chestnuts above (without covering them). Put the vanilla pod, then close the casserole. Put it on high heat and then heat softly when the casserole begins to whistle. Cooking takes about 10 to 15 minutes. 

Using a blender, purée the chestnuts. It is then necessary to weigh this purée to determine the exact weight of sugar which one will need, knowing that one needs 750 g of sugar per 1 kg of mashed chestnuts (3/4 of the weight of puree). 

[ ![Chestnut cream](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/creme-de-marron-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/creme-de-marron-1.jpg>)

en tout cas je vous le dit, ne perdez pas l’occasion de goûter a ça. 
