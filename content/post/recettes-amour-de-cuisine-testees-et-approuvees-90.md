---
title: kitchen love recipes tested and approved 90
date: '2016-01-18'
categories:
- the tests of my readers
tags:
- desserts
- Cakes
- Algeria
- Morocco
- dishes
- tajine
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg>)

Hello everybody, 

Once again thank you for all those who have honored me, trust my recipes and shared the pictures of their achievements with me. 

Do not worry if the cutter photo recipe is not on this article, it will be as soon as possible in the next. 

If you are not a member of this group, join me on it, otherwise if you do not have a facebook account, keep sending me the pictures on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

[ ![bun with garlic-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/brioche-a-lail-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/brioche-a-lail-001.jpg>)

[ ![brioche oum hamza-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/brioche-oum-hamza-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/brioche-oum-hamza-001.jpg>)

[ Very soft brioche ](<https://www.amourdecuisine.fr/article-37067174.html>) at Oumou hamza 

[ ![donuts-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/donuts-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/donuts-001.jpg>)

[ Homemade donuts ](<https://www.amourdecuisine.fr/article-donuts-faciles-et-delicieux.html>) at Oum Ritej 

[ ![rolled biscuit Rinales-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/biscuit-roul%C3%A9-Rinales-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/biscuit-roul%C3%A9-Rinales-001.jpg>)

[ Chocolate rolled biscuit ](<https://www.amourdecuisine.fr/article-biscuit-roule-chocolat.html>) at Rinales Selanir 

[ ![fluffy with lemon-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/moelleux-au-citron-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/moelleux-au-citron-001.jpg>)

[ sparkling with lemon ](<https://www.amourdecuisine.fr/article-moelleux-au-citron.html>) at Tissem Cham 

[ ![donut samia-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/donut-samia-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/donut-samia-001.jpg>)

[ Homemade donut ](<https://www.amourdecuisine.fr/article-donuts-maison-faciles-et-delicieux.html>) at Samia Z 

[ ![matlou3 radhia-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/matlou3-radhia-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/matlou3-radhia-001.jpg>)

[ Matlou3 el koucha ](<https://www.amourdecuisine.fr/article-khobz-matloua-fel-koucha-matlou3-au-four.html>) at Radhia chenouf 

[ ![tajine of cauliflower flower sihem-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/tajine-de-fleur-de-chou-fleur-sihem-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/tajine-de-fleur-de-chou-fleur-sihem-001.jpg>)

[ tajine with cauliflower and minced meatballs ](<https://www.amourdecuisine.fr/article-tajine-de-chou-fleur-et-boulettes-de-viande-hachee.html>) at Sihem Foukroun 

[ ![msemen Marisa-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/msemen-Marisa-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/msemen-Marisa-001.jpg>)

[ msemen ](<https://www.amourdecuisine.fr/article-msemen-ou-crepes-feuilletees.html>) at Marisa Sonia 

[ ![basquaise chicken-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/poulet-basquaise-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/poulet-basquaise-001.jpg>)

[ baked chicken drumsticks ](<https://www.amourdecuisine.fr/article-pilons-de-poulet-au-four.html>) , at Marisa Sonia's place 

[ ![rolled aryouma mess.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/roul%C3%A9-aryouma-mess.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/roul%C3%A9-aryouma-mess.bmp.jpg>)

[ Rolled potato ](<https://www.amourdecuisine.fr/article-roule-de-pomme-de-terre-a-la-viande-hachee.html>) at Aryouma mess 

[ ![mille feuille.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/mille-feuille.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/mille-feuille.bmp.jpg>)

[ miles ](<https://www.amourdecuisine.fr/article-millefeuilles-fait-maison-mille-feuilles.html>) at Naima Z. 

[ ![carrots charmola-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/carottes-charmola-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/carottes-charmola-001.jpg>)

[ carrot salad with the charmoula ](<https://www.amourdecuisine.fr/article-salade-de-carottes-a-la-chermoula.html>) chez Oum younes London 
