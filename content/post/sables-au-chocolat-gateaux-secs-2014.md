---
title: Shortbread cookies, dry cakes 2014
date: '2014-02-23'
categories:
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/cho-four-002_thumb.jpg
---
#  chocolate shortbread cookies. 

[ ![cho-oven-002](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/cho-four-002_thumb.jpg) ](<https://www.amourdecuisine.fr/article-35955000.html>)

Hello everybody, 

Here is a recipe that I can realize the closed eyes, knowing that it is a success suddenly, these cakes Chocolate Shortbread, are a delice, fondant, with the intense tastes, taste of the chocolate shortbread cake, with the endless fondant of cream ... and the beautiful layer of chocolate ............... hum 

to accompany a good bowl of hot chocolate, for the fans of the sweet taste, or of an English tea ... and we forget everything that's going on around us. 

**Shortbread cookies, dry cakes 2014**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/cho-four-002_thumb.jpg)

Recipe type:  dessert  portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 1 egg 
  * 400 gr of softened butter 
  * 200 gr of icing sugar 
  * 1 sachet of baking powder 
  * 12 tablespoons of cocoa   
(if your cocoa is bitter like mine, reduce the dose a little) 
  * flour   
(everything depends on your flour, but it will be almost between 400 gr and 500 gr) 

Cream: 
  * 120 gr of softened butter 
  * 80 gr of icing sugar 
  * 2 bags of vanilla 
  * ½ glass of milk 
  * 2 teaspoons of milk powder. 

Icing: 
  * 4 cases of cocoa 
  * 3 cases of icing sugar 
  * 50 grams of butter 
  * 1 chocolate bar (100 gr to 150) 
  * 1 little water 



**Realization steps**

  1. In a bowl, combine the butter and sugar until a creamy melange is obtained. 
  2. Add the egg, stir well, then add the cocoa and finally the flour mixed with the yeast. 
  3. mix well to have a firm and smooth dough. 
  4. Using a rolling pin, lower the dough to a thickness of 4 mm, on a floured work surface. 
  5. cut circles of 5 cm diameters with a punch piece or a tea glass. 
  6. put them on a tray butter, and bake for 10 to 15 minutes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/S7302352_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/S7302352_thumb.jpg>)

Preparation of the cream: 
  1. Mix the softened butter with the sugar, add the vanilla and the milk little by little, then at the end the milk powder. 
  2. after cooling the cakes, spread the cream on one side, and gather it with another piece of the cake. 

Preparation of the icing: 
  1. in a saucepan, with a bain-marie, mix the cocoa, the icing sugar and the butter., stir until the butter is well melted, slowly add the chocolate in pieces and the water to have a rather liquid glazing but not too much flowing. 
  2. For my part, I prepared a small white ice cream with icing sugar and water, and I put this mixture in a paper bag that I shaped myself, and every time I tropm a cake in the chocolate icing, I trace two lines of white glagaceous and I do as I please. 


