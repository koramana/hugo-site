---
title: Stuffed aubergines with charmoula
date: '2012-05-18'
categories:
- cuisine algerienne
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/aubergine-farci-karima12.jpg
---
#  ![eggplant-stuffed-karima.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/aubergine-farci-karima12.jpg)

##  Stuffed aubergines with charmoula 

Hello everybody, 

here is a very delicious entry recipe that I find from my archives, because it is really worth seeing, I hope you will like it. it is a recipe from one of my readers, that I myself have tried several times, and each time, a delight. 

![Stuffed eggplant karima1](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/aubergine-farci-karima111.jpg)

**Stuffed aubergines with charmoula**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/aubergine-farci-karima-300x225.jpg)

**Ingredients**

  * 4 to 5 medium sized eggplants 
  * 5 cloves of garlic 
  * 2 tomatoes 
  * a few sprigs of coriander 
  * salt and black pepper 
  * oil for frying 



**Realization steps**

  1. clean the eggplant, and grill until it is soft inside 
  2. clean the aubergine from the grated skin 
  3. cut in half without separating the two parts 
  4. make incisions with the knife 
  5. crush garlic, cut tomatoes and coriander 
  6. season with salt 
  7. season the eggplants with a little salt, fill with stuffing, and close the eggplant 
  8. pass in a little flour 
  9. and cook in an oil bath 
  10. let drain well on absorbent paper 
  11. present with a good tomato sauce 



thank you for your visits and comments 

Have a good evening 

#  ![stuffed aubergine karima2](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/aubergine-farci-karima2_thumb11.jpg)
