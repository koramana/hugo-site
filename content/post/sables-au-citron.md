---
title: shortbread with lemon
date: '2017-06-18'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
tags:
- Algerian cakes
- Algeria
- Cakes
- Cookies
- Aid
- biscuits
- Little butter

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sables-au-citron-1.jpg
---
[ ![sands with lemon 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sables-au-citron-1.jpg) ](<https://www.amourdecuisine.fr/article-sables-au-citron.html/sables-au-citron-1>)

##  Shortbread with lemon 

Hello everybody, 

Always in the sanded mood, and this time, I share with you the recipe of shortbread with lemon. This is a recipe from one of my readers waiting to be validated on my blog for a while already. 

The recipe is very different from mine, the fact that in the recipe of my shortbread, I do not put a whole egg, I will still try this version, to see the difference. 

These shortbread with lemon **Tim de Bouj** all beautiful are melting and crunchy with this beautiful layer of icing taste very tart, which is worth a try ... So you want the recipe?  Shortbread    


**shortbread with lemon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sables-au-citron-2.jpg)

portions:  30  Prep time:  15 mins  cooking:  12 mins  total:  27 mins 

**Ingredients** ingredients: 

  * 250 gr of butter. 
  * 2 yolks and 1 whole egg. 
  * 100 of cornflour. 
  * 150 gr of icing sugar. 
  * 1 sachet (10 g) of baking powder. 
  * zest of a lemon. 
  * flour as needed. 

Icing: 
  * juice of a lemon. 
  * Yellow color. 
  * icing sugar. 



**Realization steps** Preparation: 

  1. in a bowl work butter and icing sugar, add eggs and lemon zest and work again. 
  2. add cornstarch, baking powder and pick up with flour until you obtain a smooth and manageable dough. 
  3. roll out the dough with a roll on a floured board and cut out the desired patterns. 
  4. bake the cake in a 180 oven • 
  5. after cooking let the cake cool and prepare the frosting. Mix the icing sugar with the lemon juice and the yellow color. 
  6. glaze the cake and leave in a ventilated place to dry. 
  7. decorate to choose. 



[ ![lemon sands](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sables-au-citron.jpg) ](<https://www.amourdecuisine.fr/article-sables-au-citron.html/sables-au-citron-3>)
