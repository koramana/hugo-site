---
title: pumpkin sweet pie pumpkin pie
date: '2017-10-24'
categories:
- sweet recipes
- pies and tarts
tags:
- Cakes
- desserts
- Algerian cakes
- To taste
- Pastry
- Shortbread
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-sucree-au-potiron-1.jpg
---
[ ![pumpkin sweet pie pumpkin pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-sucree-au-potiron-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-sucree-au-potiron-1.jpg>)

##  pumpkin sweet pie pumpkin pie 

Hello everybody, 

It surprised me that yesterday posting photos of my sweet pumpkin pie on my Facebook group, many of the girls were surprised that the pumpkin could be the basis of a dessert recipe. 

And yet sweet pie with pumpkin pie is a classic of English and American cuisine, and in America you can even find pumpkin puree in a box, for a quick preparation of this dessert. 

For my part I had to prepare pumpkin puree at home, it's easy to do, but you must be sure to have a puree not too liquid, to have a pie of a perfect texture. 

**pumpkin sweet pie pumpkin pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-sucr%C3%A9e-au-potiron.CR2_.jpg)

portions:  12  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>)
  * 425 gr of pumpkin puree 
  * 495 g sweetened condensed milk 
  * 2 eggs 
  * 1 tablespoon of mixed spice (nutmeg, cinnamon, clove powder, ginger, mix on equal) 



**Realization steps**

  1. prepare the shortbread, let it rest, 
  2. go for a 22 cm pie pan with this shortbread, or use a mold of your choice. 
  3. place in the fridge. 

prepare the pumpkin puree: 
  1. cut the pumpkin on two, remove the seeds and the fibers. 
  2. place the two halves in a mold lined with cling film, the face of the cut down. 
  3. cook until the pumpkin becomes tender. 
  4. with a spoon, remove all the skin, let drain a little of its name, before mashing, let cool. 

prepare the filling of the pie: 
  1. when the pumpkin puree is cold, weigh 425 gr 
  2. in a bowl beat the pumpkin puree, the milk, the 2 eggs and the spices. The mixture must be homogeneous. 
  3. pour this mixture on the pie shell. 
  4. and baked in a preheated oven at 180 degrees C for at least 50 minutes. 



Note I cover the edges of the pie (pasta) with aluminum foil, so it does not burn too much during cooking. 

[ ![sweet pumpkin pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-sucree-au-potiron-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-sucree-au-potiron-2.jpg>)
