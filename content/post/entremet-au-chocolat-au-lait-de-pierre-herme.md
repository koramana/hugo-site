---
title: Pierre Hermé milk chocolate dessert
date: '2017-11-18'
categories:
- dessert, crumbles and bars
tags:
- Cakes
- desserts
- Pastry
- pralines
- mosses
- dacquoise
- fondant

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/entremet-au-chocolat-au-lait-001_thumb1.jpg
---
##  Pierre Hermé milk chocolate dessert 

Hello everybody, 

Here is a chocolate dessert to melt, we can only succumb to this crunchy fondness, sweet and tasty, as if you already have it in your mouth, and me with you, because this delight is not my achievement , or the realization of a friend of the corner, this dessert comes to me in downloadable photos from my dear Lunetoiles, it is a beautiful torture for me, to look at the photos without touching it .... 

If you like chocolate pudding, why not watch this video: 

**Pierre Hermé milk chocolate dessert**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/entremet-au-chocolat-au-lait-001_thumb1.jpg)

**Ingredients** For 12 pieces (size: 6 x 6 cm squares) For the milk chocolate ganache (to prepare the day before) 

  * 200g of whipping cream 
  * 250g dessert milk chocolate 

For milk chocolate whipped cream (to prepare the day before) 
  * 210g of dessert milk chocolate 
  * 300g of whipping cream 
  * For the milk chocolate leaves: 
  * 450g dessert milk chocolate 
  * Glitter or star food (optional decoration) 

For hazelnut dacquoise: 
  * 75g icing sugar 
  * 65g of hazelnut powder 
  * 75g of egg white (2 to 3 eggs depending on the size) 
  * 25g caster sugar 
  * 65g of hazelnuts 
  * Preheat the oven to 170 ° C. 

For the praline laminated: 
  * 30g of butter 
  * 75g dessert milk chocolate 
  * 150g praline paste 
  * 150g of hazelnut puree (in organic stores) 
  * 150g crumbled gavottes 



**Realization steps** Preparation of the milk chocolate ganache (to be made the day before) 

  1. Heat the cream in a microwavable bowl, 2 times 30 seconds, then add the chocolate, previously broken in pieces. 
  2. heat the cream, when the cream shivers, add the chocolate, 
  3. stir well with a whisk until the chocolate is completely melted and the mixture is well blended. 
  4. Wait for it to cool down and cool 

Preparation of milk chocolate whipped cream 
  1. Heat the cream in a microwave-safe bowl, 2 times 30 seconds, then add the chocolate previously broken in pieces. Or melt in a saucepan, 
  2. heat the cream, when the cream shivers, add the chocolate, 
  3. stir well with a whisk until the chocolate is completely melted and the mixture is well blended. Wait until it cools. Reserve cool. 

Preparation of milk chocolate squares: 
  1. In a bain-marie over low heat, melt the milk chocolate, previously broken in pieces, in a suitable bowl. 
  2. Pour the chocolate on a 40x40 cm plate, previously lined with guitar paper or rhodoïd leaves. 
  3. Spread the chocolate evenly with a spatula. 
  4. Let the chocolate cool. The chocolate will freeze while cooling. 
  5. As soon as the chocolate begins to freeze it trace and cut as you go with a knife of square 6 x 6 centimeters using a ruler. 
  6. Then let it harden completely overnight (or until final assembly) in a very cool place. 

Preparation of dacquoise with hazelnuts: 
  1. Preheat the oven to 170 ° C. 
  2. Roast the hazelnuts by putting them on a baking sheet lined with baking paper, for 15 minutes. Then roughly crushed. 
  3. Using your blender, mix the icing sugar and the hazelnut powder together until you obtain a very fine powder.While this time, mount the egg whites in snow, and when they start to be firm, it ie to leave traces and to form peaks. 
  4. Add the powdered sugar little by little to obtain a soft and shiny meringue. 
  5. Gently stir in the icing sugar / hazelnut powder with the meringue using a spatula. 
  6. Place a frame 27 x 27 centimeters (or if you do not have one, draw with a ruler and a pencil, a rectangle of 27cm x 27cm) on a sheet of parchment paper 
  7. Spread out and smooth the surface and sprinkle with roasted and coarsely chopped hazelnuts. 
  8. Bake for 30 minutes still at 170 ° C. 

Preparation of the laminated praline 
  1. Melt the chocolate previously broken in pieces and the butter, in a salad bowl, in a bain-marie. 
  2. Mix well with a whisk. 
  3. Add praline and hazelnut puree. Mix well, when the mixture is very supple and homogeneous, remove from the water bath. 
  4. Stir in crumbled gavottes. 

Mounting: 
  1. Place the dacquoise on a dish, spread over the laminated praline, using the back of a tablespoon previously dipped in water, evenly equalizing the surface, must be flat. 
  2. Let it harden in the fridge. 
  3. Once frozen, cut 6 x 6 cm squares. You must get 12 or 16 squares. 
  4. Put in a piping bag, a fluted socket, garnish the bag with the milk chocolate ganache and place on each small square 4 rosettes of ganaches that must cover the entire surface of the square. 
  5. Place a sheet of milk chocolate on top. Reserve cool. 
  6. Beat the milk chocolate whipped cream with an electric mixer for a few minutes, until the whipped cream is well mounted. 
  7. Put in a piping bag a fluted socket, and garnish the bag with the whipped cream and place on each small entremet 4 rosette of whipped cream that must cover the entire surface of the chocolate sheet. 
  8. Place a sheet of milk chocolate on top. Store in a cool place. 


