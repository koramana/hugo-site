---
title: chocolate truffles and biscuits
date: '2011-11-24'
categories:
- Chocolate cake
- birthday cake, party, celebrations
- recettes sucrees

---
& Nbsp; hello everyone, yesterday, after taking my daughter to school, and go home, a friend calls me and said she will come to see me, I was tired, and more with this flu that does not I do not want to leave because of cold unbearable, I was lazy, to put in the stove, to prepare any cake, as I was ashamed to put business cookies to accompany the coffee. I started to turn here and look at what I had in my closet, I wanted to prepare something, simple, good and presentable, & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 0.85  (  3  ratings)  0 

Hello everybody, 

yesterday, after taking my daughter to school, and going home, a friend calls me and tells me that she will come to see me, I was tired, and more with this flu that does not want to leave me Because of the unbearable cold, I was too lazy to put in the oven to prepare any cake, as I was ashamed to put commercial biscuits to accompany the coffee. 

I started to turn here and look at what I had in my closet, I wanted to prepare something, simple, good and presentable, hence the idea of ​​this delicious cake without cooking, which reminds me of truffles, I had read the recipe in a magazine, when I was doing my shopping, I vaguely remembered the quantities of ingredients, but I remembered many ingredients, and by chance, I had the necessary at home, so with a speed turbo, I prepared these little delicacies, in 15 min, they were already in the fridge, to keep fit. 

Although I made the ingredients at random, but I still measure it, for you give the exact recipe. 

Ingredients: 

  * 325 gr biscuits (digestive, small lu, or bimo) 
  * 125 gr of butter 
  * 200 gr of condensed milk (Nestlé) 
  * 2 tablespoons of cocoa 
  * 80 gr of almond powder 
  * Chocolate vermicelli 



method of preparation: 

  1. place the cookies in a bag and crush them with a baking roll 
  2. Melt the butter, and pour on the biscuits 
  3. add the condensed milk 
  4. mix well by hand 
  5. add the ground almonds and the cocoa 
  6. form small balls 
  7. place the chocolate vermicelli in a bag 
  8. place your balls in the bag (not all the amount of a blow) so that the chocolate vermicelli adheres well to the balls 
  9. place in the fridge 



enjoy this little greed with hot tea, or coffee, a real treat, besides the children of their return from school, we ate a good amount, I had to hide that because I do not like not too much sweets for kids 

thank you for your visits and your comments, excuse me if I do not respond immediately to the comment, or if I do not validate right away, all your beautiful, it's just for lack of time, I usually used to make that, but with the flu, it is hardly that I exceed 22:00 

bonne journée, et on se retrouvera InchAllah avec une nouvelle recette. 
