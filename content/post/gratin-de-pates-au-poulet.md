---
title: chicken pasta gratin
date: '2017-07-16'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
tags:
- Full Dish
- Having dinner
- tomatoes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/08/gratin-de-pates-au-poulet-2.jpg
---
[ ![chicken pasta gratin 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/gratin-de-pates-au-poulet-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/gratin-de-pates-au-poulet-2.jpg>)

##  Pasta gratin with chicken 

Hello everybody, 

At home, we love pasta, besides, they are present every week on our table ... And not to eat the same thing every time, I vary the method of preparation, or the type of pasta: minestrones, pasta a vice, ravioli, spaghetti ... 

For this time, it's a gratin of pasta with chicken, a delicious dish, that I do every time, because we like a lot at home, it takes a little preparation time, but it's a dish that is worth the stroke. 

I did not think to post this recipe before, but on the occasion of my participation in the game between blogger: [ recipes around an ingredient # 9: the tomato ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-9-la-tomate.html>) , and because I'm on vacation and I do not have time to refill a tomato recipe, I thought this gratin will do the trick. 

[ ![gratin pasta with chicken 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/gratin-de-pates-au-poulet-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/gratin-de-pates-au-poulet-1.jpg>)   


**chicken pasta gratin**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/gratin-de-pates-au-poulet-4.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** for pasta: 

  * 500 gr of pasta (minestrones here) 
  * boiling water 
  * salt 
  * 2 tablespoons of table oil 

for the chicken: 
  * chicken pieces according to the number of people 
  * 2 cloves garlic 
  * salt and black pepper 
  * 1 bay leaf 
  * thyme 
  * 2 tablespoons of oil 

for the tomato sauce: 
  * 1 and a half pounds of fresh tomato 
  * ½ onion 
  * 2 garlic cloves 
  * 1 bay leaf 
  * thyme 
  * salt and black pepper 
  * 2 tablespoons of olive oil 
  * 1 tablespoon of canned tomato 

to brown: 
  * Gruyère cheese according to taste 



**Realization steps** Cooking chicken: 

  1. In a pot, fry the garlic in a little oil, add the chicken, salt, black pepper, bay leaf, and thyme, cover the chicken with the water, and cook. 
  2. when the chicken is tender, drain it from its broth, and keep the broth 

preparation of the tomato sauce: 
  1. beat the tomato, 
  2. fry the chopped onion and crushed garlic in a little oil. 
  3. when the onion is tender, add the bay leaf, thyme, and tomato rappé 
  4. add the salt, black pepper, and canned tomato, cook until the oil rises to the surface, and the sauce is well reduced 
  5. cook the pasta in boiling salted water to which you will add 2 tablespoons of oil. Do not over cook the pasta should remain, because it will bake again. 

dish preparation: 
  1. in a large pot, mix the pasta with the tomato sauce, add the chicken broth very slowly, do not add a lot of broth, just so that the pasta is well coated in the sauce. 
  2. add the grated Gruyere, and mix again. 
  3. pour the pasta into a buttered pyrex mold, cover the surface with a little gruyère, or according to your taste. 
  4. bake for almost 20 minutes. 
  5. you can fry the chicken pieces in a little oil so that they are well roasted 
  6. enjoy while it's still hot, this dish is a delight. 



[ ![chicken pasta gratin 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/gratin-de-pates-au-poulet-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/gratin-de-pates-au-poulet-3.jpg>)
