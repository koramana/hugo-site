---
title: turkey stuffed in the oven
date: '2016-12-22'
categories:
- diverse cuisine
- idea, party recipe, aperitif aperitif
- Plats et recettes salees
- recettes a la viande de poulet ( halal)
tags:
- Poultry
- Vegetables
- Roast
- Potato
- Roast turkey
- Mustard
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dinde-farcie-au-four-1.jpg
---
[ ![stuffed turkey 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dinde-farcie-au-four-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dinde-farcie-au-four-1.jpg>)

##  turkey stuffed in the oven 

Hello everybody, 

I've always liked making turkey in the oven, but in Bristol where I live I never find halal turkey, not even turkey cutlets that I really like to eat just grilled ... The one and only time I do not I found a halal turkey it was last year for Christmas, but the turkey was huge, a turkey of almost 10 kg .... 

I was going back and forth to the butcher's shop, I bought it or not, but in the end, I just forgot about the idea, not only would it take me a long time to cook, but also who would eat all this turkey Frankly, it was going to be a big waste ... me who does not tolerate waste, hihihih. 

![turkey stuffed in the oven](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dinde-farcie-au-four.jpg)

**turkey stuffed in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dinde-farcie-au-four-2.jpg)

portions:  8  Prep time:  15 mins  cooking:  210 mins  total:  3 hours 45 mins 

**Ingredients**

  * 1 turkey of almost 5 kg 
  * salt, black pepper, cumin 
  * 3 to 4 cloves of garlic 
  * 1 to 2 tablespoons mustard. 

for the stuffing: 
  * 2 medium onions 
  * the offal of the turkey 
  * Oyster mushrooms if possible   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/champignons.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/champignons.jpg>)
  * 1 tablespoon of olive oil 
  * 1 tablespoon of butter 

the vegetables: 
  * 250 gr of brussels sprouts 
  * 250 gr of small potatoes 
  * 250 gr of carrots 
  * 250 gr of parsnips. 
  * garlic, salt, black pepper, mixed herbs 
  * olive oil. 



**Realization steps**

  1. wash the turkey very well, empty the contents 
  2. wash outside and inside with vinegar and lemon water 
  3. do not dry turkey from inside and outside with paper towel. Then wipe it again with lemon, also inside and outside 
  4. take a mixture of black pepper, cumin and salt, and rub them all over the turkey, also try to rub it between the skin and the pulpit of the turkey. 
  5. take the cloves of garlic, cut them into strips and place them in the belly of the turkey. 
  6. now cover the turkey with a generous layer of mustard, and leave aside the time to prepare the stuffing. 
  7. Cut the onion into strips and fry in a little olive oil and butter and a little lonely. 
  8. add mushrooms 
  9. add black pepper, herbs provinces. when this mixture is well cooked, put it aside 
  10. take the giblets you keep from the turkey, wash them well, and let them drain from their water. 
  11. chop it in the meat grinder, and then chop the onion and mushroom mixture 
  12. mix everything together and stuff your turkey with it.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/mariner-et-farcir-une-dinde-au-four.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/mariner-et-farcir-une-dinde-au-four.jpg>)
  13. place the turkey on a rack place in a baking tray.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dinde-au-four.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dinde-au-four.jpg>)
  14. add a few pieces of butter over the turkey, and place in a preheated oven at 240 degrees C 
  15. Cook at this temperature for 30 minutes, then turn the oven down to 180 degrees C. and let the turkey cook slowly. 
  16. cooking the turkey took the equivalent of 3:15 to 3:30 minutes, the cooking time will depend on the size of the turkey and your oven. 

vegetable preparation: 
  1. cook each vegetable separately in gently salted water for almost 5 minutes no more. 
  2. season these vegetables either separately or by mixing them, with grated garlic, salt, black pepper, mixed herbs, olive oil and just a little bit of the cooking water,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/legumes-pour-accompagner-une-dinde-au-four.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/legumes-pour-accompagner-une-dinde-au-four.jpg>)
  3. then sauté these vegetables in a pan with a little butter. 
  4. and just before the end of cooking the turkey, place these vegetables in the tray that is already filled with all the turkey juice. mix them and let them cook for almost 10 minutes under the turkey and in its juice. 



[ ![stuffed turkey 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dinde-farcie-au-four-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dinde-farcie-au-four-4.jpg>)
