---
title: Coconut macarons
date: '2015-09-18'
categories:
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/macarons-a-la-noix-de-coco-gateau-algerien-0451.jpg
---
![Coconut macarons](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/macarons-a-la-noix-de-coco-gateau-algerien-0451.jpg)

##  Coconut macarons 

Hello everybody, 

here are old-fashioned coconut macaroons that I prepared yesterday night, because a friend, call me late, to say: "I'll come see you tomorrow", and as I I do not like to introduce commercial tricks to my guests, that's what I've concocted in a hurry, very late at night, after putting the children to bed. 

I do not like the result because my macaroons should be a little clearer, I left too much in my opinion, so I had to camouflage it all with chocolate, hihihihi 

it must be said that the taste is there, these macaroons are super super fondant, and pleasantly tasty .... 

so, when I went to see in my closet, I had nothing, yes because with the new year, and the holidays here in England, besides sales, I did not go out to do my shopping, I do not like too much jostling, and standing in line, to pay 2 or 3 things ... 

so I look at the cupboard, only sugar, chocolate and coconut .... 

in the fridge, no eggs, no mascarpone, no fresh cream for a quick bavarois .... 

but I have egg whites in the freezer, which I keep every time I use egg yolks to decorate my buns or my bread .... 

I remove the egg whites from the freezer ... 

I prepare my ingredients, and the stove ... .. finally my little electric oven ... hihihihih   


**Coconut macarons**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/macarons-a-la-noix-de-coco-gateau-algerien-039.CR2_thumb1.jpg)

portions:  30  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 3 egg whites 
  * 1 teaspoon of vanilla extract 
  * 1 glass of sugar (my glass measures 220 ml) 
  * 3 glasses of grated coconut (unsweetened, otherwise reduce sugar) 
  * dark chocolate for decoration 



**Realization steps**

  1. Mix the egg whites, vanilla, sugar and coconut in a bowl. 
  2. form balls, which you would place on a baking pan covered with paper (dough is a bit sticky, for my part I cooked on a special carpet macaron, and I try to make circles of the same size) 
  3. Bake in a preheated oven at 180 ° C, until lightly browned, about 10-15 minutes. (Do not like me, do not answer the comments without putting the oven timer). 
  4. Cool on a pastry rack. 
  5. Meanwhile, melt the chocolate in a bain-marie and dip the macaroons in the chocolate. 
  6. then with a pastry bag make strokes to decorate the macaroons. 



thank you for your visit and comments 

recipe tested and approved by:   
  
<table>  
<tr>  
<td>

[ ![coconut macaroon at lili](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/macaron-a-la-noix-de-coco-chez-lili-113x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/macaron-a-la-noix-de-coco-chez-lili.jpg>)

at Délice de Lili 


</td>  
<td>

[ ![coconut macaroons at Lunetoiles](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/macarons-a-la-noix-de-coco-chez-Lunetoiles-100x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/macarons-a-la-noix-de-coco-chez-Lunetoiles.jpg>)

at Lunetoiles 


</td>  
<td>


</td> </tr> </table>
