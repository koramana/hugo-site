---
title: recipe for orange pie
date: '2018-02-04'
categories:
- sweet recipes
- pies and tarts
tags:
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/02/recette-de-la-tarte-%C3%A0-lorange-2.jpg
---
![recipe for orange pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/recette-de-la-tarte-%C3%A0-lorange-2.jpg)

##  recipe for orange pie 

Hello everybody, 

A seasonal pie, or what's left of the orange season, an orange pie that I made a while ago and that I did not have time to share with you ... This Orange pie recipe I adopted it for years, the first time I had shared with you the version [ orange tartlets ](<https://www.amourdecuisine.fr/article-tartelettes-a-l-orange.html>) super easy without decoration. 

This time I decorated my orange pie with some rosettes of orange mousse (orange whipped cream) I admit that I regretted not having made a large amount of foam, because the latter was so good, that when I put them in the cool and I came back, some of my musketeers passed and he left nothing! 

**recipe for orange pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/recette-de-la-tarte-%C3%A0-lorange.jpg)

**Ingredients**

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 
  * 500 ml of pressed orange juice 
  * 100 gr of sugar (or less if your oranges are sweet) 
  * 4 egg yolks 
  * 30 gr of cornflour 

whipped cream with orange cream: 
  * 3 c. orange cream 
  * 150 ml of liquid cream 
  * 1 C. sugar (if you like sugar, add to taste) 



**Realization steps** prepare the shortbread 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a dough is detached from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Darken a pie circle 25 cm in diameter. 
  6. Stitch the bottom of the dough with a fork (if your mold is not perforated) 
  7. cover with a baking sheet, and fill with dry vegetables (for me it was glass tablets for vase ornament) and put in the oven 20 min at 180 ° C, then remove the legumes and the leaf and continue the cooking 10 minutes. 

for orange cream: 
  1. beat the egg yolks with the sugar and add the orange juice. 
  2. Heat the mixture while stirring 
  3. add the maïzena deluee in a little orange juice 
  4. thicken over low heat, stirring regularly. 
  5. The cream is ready when it has a nice consistency and slicks the back of your spoon. 
  6. Pour the cream on the cooked pie shell. Let cool 
  7. remove 3 tablespoons of this cream to make mousse or chantily with orange cream 

whipped cream or orange mousse: 
  1. add the whipped cream 
  2. add the sugar and go up again 
  3. introduce the orange cream without breaking the whipped cream to have a nice mousse. 
  4. decorate your pie according to your taste and place in the fridge until tasting. 



![recipe for the orange pie 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/recette-de-la-tarte-%C3%A0-lorange-1.jpg)
