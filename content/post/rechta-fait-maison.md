---
title: rechta - homemade
date: '2014-09-20'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- Dishes and salty recipes
- recipe for red meat (halal)
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/rechta-008_thumb.jpg
---
[ ![rechta - homemade](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/rechta-008_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/rechta-008.jpg>)

##  rechta - homemade 

Hello everybody, 

You have a desire to make a good [ rechta ](<https://www.amourdecuisine.fr/article-rechta-algeroise.html>) , this delicious Algerian dish, rich and fragrant, a dish in white sauce very famous especially in the region of Algiers and its surroundings. 

At home, we rarely do this dish, because at home it's the dishes that my father likes that take place, and my father likes it, [ chakhchoukha edfar ](<https://www.amourdecuisine.fr/article-chekhchoukha-constantinoise-chakhchoukha-de-constantine.html> "Constantinese Chekhchukhah, Constantine's Chakhchukha") or  [ chekhch ](<https://www.amourdecuisine.fr/article-chakhchoukha-de-biskra-charchoura-biskria-112155334.html> "Biskra chakhchoukha, Charchura Biskria") [ oukhat biskra ](<https://www.amourdecuisine.fr/article-chakhchoukha-de-biskra-charchoura-biskria-112155334.html> "Biskra chakhchoukha, Charchura Biskria") ... 

So if you want to eat the recipe today think about preparing the dough the day before .... 

**rechta - homemade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/rechta-008_thumb.jpg)

Recipe type:  Algerian cuisine  portions:  4  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * 450 gr of fine semolina 
  * 150 gr sifted flour 
  * 1 tablespoon of table oil 
  * Water at room temperature 
  * 1 teaspoon of salt 



**Realization steps**

  1. In a large salad bowl, put the semolina salt and flour 
  2. add the oil, amalgam 
  3. spray gently with water until you have a very firm dough, 
  4. knead to homogenize everything 
  5. form a pudding and wrap with a film of food. 
  6. put in the fridge all night. 
  7. The next day take out the dough and leave at room temperature. 
  8. cut out pieces, roll down just a little bit so that you can machine them again. 
  9. if your dough is too soft do not hesitate to sprinkle with flour. 
  10. go first to n ° 1 then n ° 2 and so on go back last time to n ° 7 
  11. you are going to get a very fine dough strip that you go to put on a clean cloth to let dry a little (just a little, if it dries a lot, you may not be able to pass in the compartment spaguitti) 
  12. continue until you have used up all your dough   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/2011-12-04-rechta_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/2011-12-04-rechta_thumb.jpg>)
  13. take the strips of dough and pass them in your machine to have fine spaghetti. 
  14. coat the Rechta with 2 tablespoons of oil to open them, put them in the top of the couscous for 15 min 
  15. to the escape of steam. spill your pasta in a large salad bowl, sprinkle it a little bit with a glass of water, let it absorb oil and put one last time on the top of the couscoussier. 
  16. like that the dough is ready, and the recipe dish rechta and next [ this link ](<https://www.amourdecuisine.fr/article-rechta-algeroise-cuisine-algerienne.html> "Algerian rechta, Algerian cuisine") . 



[ ![rechta 007](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/rechta-007_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/rechta-007.jpg>)

and for other recipes of traditional Algerian dishes I pass you my favorites: 

![tlitli constantinois](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-constantinois-043.CR2_thumb.jpg) **[ tlitli Constantinois, or tlitli in white sauce ](<https://www.amourdecuisine.fr/article-tlitli-constantinois-pour-l-annee-hijir-1434-112473751.html>) **

**[ Chicken couscous ](<https://www.amourdecuisine.fr/article-couscous-au.html> "couscous with chicken, Algerian cuisine") ** ![https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-611_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-611_thumb.jpg) couscous very repute, and always present in the majority of families, light, rich in vegetables, and especially very tasty **[ Chakhchukhat eddfar ](<https://www.amourdecuisine.fr/article-chekhchoukha-constantinoise-chakhchoukha-de-constantine.html> "Constantinese Chekhchukhah, Constantine's Chakhchukha") ** ![https://www.amourdecuisine.fr/wp-content/uploads/2012/11/rus-007_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/rus-007_thumb.jpg) one of the most famous dishes, especially in eastern Algeria, in any case I like a lot. **[ Trida ](<https://www.amourdecuisine.fr/article-trida.html> "trida") ** [ ![trida for moharem 011](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/trida-pour-moharem-011_thumb_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/trida-pour-moharem-011_1.jpg>) a very good other Algerian dish, rather to prepare in the Algiers region, a delice, I present you too good rechta: **[ RECHTA ALGEROISE ](<https://www.amourdecuisine.fr/article-rechta-algeroise-cuisine-algerienne.html> "Algerian rechta, Algerian cuisine") ** ![rechta at navis 006](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/rechta-au-navis-006_thumb.jpg) still a wealth of Algerian heritage, this delicious dish: **[ Couscous beautiful osbane كسكسي بالعصبان » ](<https://www.amourdecuisine.fr/article-couscous-bel-osbane.html> "Couscous beautiful osbane كسكسي بالعصبان") ** ![couscous beautiful bakbouka](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-bel-bakbouka_thumb.jpg) and also another dish, and the list is always long, the I pass you the: [ Tlitli ](<https://www.amourdecuisine.fr/article-tlitli-constantinois-pour-l-annee-hijir-1434.html> "Tlitli constantinois, for the year hijir 1434") ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-1_thumb.jpg)

another   
delicious dishes, the **[ Chakhchukha, or mfermsa ](<https://www.amourdecuisine.fr/article-chakhchoukha-de-biskra-charchoura-biskria-112155334.html> "Biskra chakhchoukha, Charchura Biskria") **

![biskra chakhchoukha, trida, mfermsa, charchoura](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chakhchoukha-de-biskra-trida-mfermsa-charchoura_thumb.jpg)

**[ Couscous with meat ](<https://www.amourdecuisine.fr/article-25345493.html> "Couscous with meat") **

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/25340955.jpg)

[ how to cook couscous with steam ](<https://www.amourdecuisine.fr/article-comment-cuire-le-couscous-111575933.html> "how to cook couscous")

![cooking steamed couscous 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cuisson-du-couscous-a-la-vapeur-3_thumb.jpg)

you also have: 

[ ![couscous-Kabyle-a-la-cornille](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/couscous-kabyle-a-la-cornille-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/couscous-kabyle-a-la-cornille.jpg>)

**[ Kabyle couscous has dried meat ](<https://www.amourdecuisine.fr/article-couscous-kabyle-a-la-viande-sechee-couscous-au-guedid.html> "Kabyle couscous with dried meat: couscous with guedid") **

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
