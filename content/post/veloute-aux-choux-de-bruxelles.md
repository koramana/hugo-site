---
title: Velouté with Brussels sprouts
date: '2015-12-27'
categories:
- soups and velvets
tags:
- Lentil soup
- lentils
- Soup
- Healthy cuisine
- Algeria
- Easy cooking
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/velout%C3%A9-au-choux-de-bruxelles-001.jpg
---
[ ![velouté with Brussels sprouts-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/velout%C3%A9-au-choux-de-bruxelles-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/velout%C3%A9-au-choux-de-bruxelles-001.jpg>)

##  Velouté with Brussels sprouts 

Hello everybody, 

In winter for me, there is no more delicious than velvety smooth, tasty, warming. I make almost every day a velvety, and when you make a type of recipe daily, it takes suddenly, try to change, and vary ... 

**Velouté with Brussels sprouts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/velout%C3%A9-de-choux-de-bruxelles-1.jpg)

portions:  3  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 500 g of Brussels sprouts, 
  * 1 medium potato, 
  * 50 g shallot, 
  * 40 g of green lentils, 
  * 1 tablespoon of olive oil, 
  * 1 teaspoon of curry, 
  * salt and pepper. 

decoration: 
  * crushed nuts 
  * black pepper from the mill 
  * 1 pinch of paprika 



**Realization steps**

  1. Remove the base and damaged leaves of Brussels sprouts. Cut in half the larger ones and wash them. Book. 
  2. Peel the shallots and the potato. Mince the shallots and sauté in a casserole in olive oil. 
  3. Pass the potato under water and cut into slices. 
  4. When the shallot is colored, add the curry then the Brussels sprouts, the potato, the lentils. Cover with water and simmer for about 30 minutes. 
  5. Mix everything in a bowl. Salt and pepper. 
  6. Serve hot! 



[ ![velouté or Brussels sprouts soup](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/velout%C3%A9-ou-soupe-aux-choux-de-bruxelles.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/velout%C3%A9-ou-soupe-aux-choux-de-bruxelles.jpg>)
