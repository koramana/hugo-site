---
title: Pineapple froth
date: '2012-10-12'
categories:
- gateaux, et cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/mousse-a-l-Ananas_thumb1.jpg
---
![Pineapple froth](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/mousse-a-l-Ananas_thumb1.jpg)

##  Pineapple froth 

Hello everybody, 

Which is what does not like the taste and the perfume of the very fresh pineapples, hum in any case I like very much, but pity that for this recipe of mousse with the pineapple I used pineapple in box. 

This mousse is just a killer, very rich in taste, and a very airy and easy to present, next time I will present in verrines. 

**Pineapple froth**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/mousse-a-l-anans_thumb1.jpg)

portions:  6  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * a box of pineapple 
  * a dozen boudoirs 
  * 2 egg white 
  * 4 tablespoons of sugar (or a little more according to your taste) 
  * 200 ml of fresh cream 
  * a gelatin sheet of 8 gr 
  * 150 grams of white chocolate   

  * for decoration, a square of dark chocolate, and you can decorate according to your taste. 



**Realization steps**

  1. method of preparation: 
  2. cut the gelatin into pieces, and let it swell in a little water 
  3. whip the cream in chantilly with 2 tablespoons of sugar, and place it in a cool place 
  4. turn the egg white into meringue with the rest of the sugar is set aside 
  5. cover a cake mold with food paper, to arrange the halves of pineapple slices according to your taste. 
  6. crush the rest of the pineapple into a chunk 
  7. Melt the gelatine in a little pineapple juice (pineapple juice canned) over low heat 
  8. add the pineapple pieces 
  9. add the white chocolate in pieces, and mix well to melt 
  10. let cool, and mix with the whipped cream gently without breaking it 
  11. incorporate the egg white into snow as gently without breaking it 
  12. pour one-third of this mixture over the pineapple to make a first layer 
  13. dip the boudoirs in the pineapple juice, and cover well the first layer of foam 
  14. pour the second third of foam on the boudoirs 
  15. dive the rest of the boudoirs into the pineapple juice again and cover the mousse 
  16. pour the rest of the mousse to cover the whole mold, and chill 
  17. let rest at least 4 hours, but this dessert is even better the next day 



thanks for your visit and comments 

thank you to all who continue to subscribe to my newsletter 

and if you like mosses I share with you: 

[ chocolate mousse ](<https://www.amourdecuisine.fr/article-duo-de-mousse-au-chocolat-97088624.html>)

[ strawberry mousse ](<https://www.amourdecuisine.fr/article-mousse-aux-fraises-98790659.html>)

[ caramel mousse with salted butter ](<https://www.amourdecuisine.fr/article-mousse-caramel-au-beurre-sale-101394974.html>) é 
