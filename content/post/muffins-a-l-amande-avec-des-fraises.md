---
title: Almond muffins with strawberries
date: '2015-05-21'
categories:
- Cupcakes, macarons, et autres pâtisseries
- dessert, crumbles et barres
- gateaux, et cakes
- recettes sucrees
tags:
- Easy cooking
- desserts
- Pastry
- Algerian cakes
- Inratable cooking
- Fast Food
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Muffins-a-l-amande-avec-des-fraises-1.jpg
---
[ ![Almond muffins with strawberries](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Muffins-a-l-amande-avec-des-fraises-1.jpg) ](<https://www.amourdecuisine.fr/article-muffins-a-l-amande-avec-des-fraises.html/muffins-a-l-amande-avec-des-fraises>)

##  Almond muffins with strawberries 

Hello everybody, 

We always enjoy the strawberry season to make delicacies and delicacies, and today it's a recipe for  muffins  Almond with strawberries that Lunetoiles shares with us. I must admit that this recipe is on the archives of my email since April 2014, hihihihi. I do not know how I zapped this delight. 

Muffins all soft and melting in the mouth, filled with fresh strawberries. So it is assured, with this recipe, it is bursts of fresh strawberries that you will smell in the mouth, as will say Cyril Lygnac: I like the true taste of the fruits in the cakes .... It will be well served with these muffins, will not it? 

**Almond muffins with strawberries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Muffins-a-l-amande-avec-des-fraises-2.jpg)

portions:  12  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 150 g butter, melted and cooled 
  * 6 eggs 
  * 1 cup caster sugar (200 g) 
  * 1 cup and 2 tablespoons of cake flour (170g) 
  * 1 teaspoon of almond extract 
  * 12 strawberries washed and hulled 
  * almonds, to sprinkle (I did not have any) 
  * icing sugar, for the decoration 



**Realization steps**

  1. All ingredients must be at room temperature. 
  2. Place the eggs with the powdered sugar in a mixing bowl, beat on medium speed until a soft mousse is obtained. 
  3. Sift the flour, and gently add it to the previous mixture with a spatula. 
  4. Pour butter, almond extract and mix again. 
  5. Prepare a mold for 12 muffins rubbed with butter, sprinkle with flour, or garnished with muffin boxes. 
  6. Put the dough in the molds, fill in the ¾. 
  7. In the middle put a strawberry. Sprinkle with roasted almonds. 
  8. Bake at 170 ° C for about 20 - 25 minutes or longer. 
  9. Remove from oven, let cool on a rack, sprinkle with icing sugar. 



[ ![Almond muffins with strawberries07 * -](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Muffins-a-l-amande-avec-des-fraises-1279x1706.jpg) ](<https://www.amourdecuisine.fr/article-muffins-a-l-amande-avec-des-fraises.html/muffins-a-l-amande-avec-des-fraises07>)
