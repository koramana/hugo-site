---
title: appetizer with smoked salmon and bricks leaves
date: '2016-08-28'
categories:
- amuse bouche, tapas, mise en bouche
- idea, party recipe, aperitif aperitif
- Dishes and salty recipes
- fish and seafood recipes
tags:
- Cocktail dinner
- Tapas
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-de-saumon-fume.CR2_thumb1.jpg
---
##  appetizer with smoked salmon and bricks leaves 

Hello everybody, 

Here are tapas that I often prepare when I have some leaves of bricks and a few pieces of smoked salmon in the fridge. These amuses mouths are to fall .... already the taste of smoked salmon dill flavored, with a touch of lemon and yoghurt is just too good, adding to that the crunchy leaves of grilled bricks in the oven, I do not tell you more ... 

The most difficult step is to cut the leaves of bricks, and form triangles and cook for 10 minutes in the oven. The rest is child's play. 

{{< youtube CXbAAcnAGWE >}} 

**appetizer with smoked salmon and bricks leaves**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-de-saumon-fume.CR2_thumb1.jpg)

Recipe type:  appetizer, aperitif  portions:  4  Prep time:  5 mins  cooking:  15 mins  total:  20 mins 

**Ingredients**

  * leaves of bricks spotted on 4 
  * smoked salmon (depending on the number of people) 
  * some dill stalk 
  * 2 small shallots 
  * ½ tablespoon of lemon juice (or according to your taste and the number of people) 
  * 1 tablespoon natural yoghurt 
  * black pepper, salt 
  * some basil leaves and lemon peel for decoration (optional) 



**Realization steps**

  1. method of preparation: 
  2. cut the leaves of bricks on 4 
  3. fold them diagonally on two and then on two to have small triangles 
  4. brush with a little oil 
  5. and bake for 10 minutes or until you get a golden color. 
  6. when cooking triangles of bricks, chop the smoked salmon. 
  7. add chopped dill and shallot over it. 
  8. season with a little lemon juice and natural yogurt, you can taste and season to taste. 
  9. add a little salt. 
  10. now the triangles of bricks are cooked and cooled well, decorate them with this topping of smoked salmon. 
  11. decorate with a little black pepper from the mill, and a touch of basil and lemon zest. 

and enjoy me this delight ... .. 
