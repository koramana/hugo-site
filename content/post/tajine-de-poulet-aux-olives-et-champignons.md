---
title: chicken tagine with olives and mushrooms
date: '2017-10-03'
categories:
- Algerian cuisine
- Dishes and salty recipes
tags:
- Full Dish
- Healthy cuisine
- Stew
- Morocco

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-de-poulet-aux-olives-et-champignons-3.jpg
---
![chicken tajine with olives and mushrooms 3](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-de-poulet-aux-olives-et-champignons-3.jpg)

##  chicken tagine with olives and mushrooms 

Hello everybody, 

Here is a chicken dish or tajine with olives and mushrooms that I often do for my children. A recipe that I find super easy to make and delicious, hum! 

To be honest, I never thought about posting this recipe, but a friend who came for an unexpected visit and had the right to eat this simple family dish, and since then, she has not stopped asking for the recipe, and because she is one of my readers, I share my recipe with her and with you. 

so I'll give you the recipe immediately, especially to you **Estera, enjoy!**

Each version to prepare this family dish, besides I share with you the video of my friend Lynda Akdader on youtube: 

**chicken tagine with olives and mushrooms**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-de-poulet-aux-olives-et-champignons.jpg)

**Ingredients**

  * 1 kg of pitted olives 
  * 4 to 5 chicken legs (depending on the number of people) 
  * 1 C. tomato paste 
  * 2 carrots 
  * 1 onion 
  * 1 clove of garlic 
  * some parsley leaves 
  * a box of mushrooms 
  * 1 bay leaf 
  * 3 tablespoons of olive oil 
  * salt, black pepper, paprika, garlic / coriander powder 



**Realization steps**

  1. my olives are in jars and they are too soft, so I did not bleach to reduce the salt, I just put them in the water, and I keep changing the water to minimize the salt. 
  2. in a pot, put the oil, and the mushrooms, made to return well in the oil 
  3. add onion, crushed garlic, and chicken pieces, sauté a little. 
  4. add the sliced ​​carrots and chopped parsley. 
  5. season with your spices add the bay leaf. 
  6. cover everything with water and cook. 
  7. When the carrots are tender, add the olives, cook for 5 to 10 minutes. 
  8. and remove fire 
  9. serve with a delicious homemade bread 



![chicken tagine with olives and mushrooms 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-de-poulet-aux-Olives-et-champignons-2.jpg)
