---
title: baklawa rolls - Baklava rolls
date: '2011-12-07'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/baklawa-rolls_1.jpg
---
##  baklawa rolls - Baklava rolls 

Hello everybody, 

a little late, but is better late than never, and I start with the lunetoiles recipes of the cakes of the aid, which are written, and ready to post, not like my recipes, I must write everything, and explain, and frankly with this time of greyness, and this cold duck, I have the fleeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeme, hihihiih 

**Ingredients (for about 60 baklava):**

  * a packet of filo leaf, 
  * 200 g of almonds or pistachios crushed finely (but not reduced to powder), 
  * 125 g of melted butter, 



_the sirup :_

  * 435 ml of water, 
  * 310 g of sugar, 
  * 5 ml of lemon juice. 



method: 

  1. Coarsely grind the almonds 
  2. spread the filo leaves on the worktop, 
  3. brush the first sheet of melted butter and sprinkle with dried fruit all over the surface of the sheet, 
  4. put at the end of the sheet a stick about 1cm in diameter and wrap the sheet on the stick, put aside 
  5. Take a second sheet, brush it with melted butter, take the roll of filo paste on the stick that you have prepared and arrange it on the second sheet and roll it up a second time, 
  6. tighten on the stick to have a wrinkled effect and remove the, 
  7. arrange in a rectangular dish, 
  8. keep going until the filo dough is exhausted 
  9. arrange the crumpled puddings, squeezing them together, 
  10. gently pour some melted butter on top, bake for about 25 minutes at 180 ° 
  11. after cooking, pour lemon flavored honey that you have prepared in advance, let cool before cutting and arrange your baklava in small boxes 



**baklawa rolls - Baklava rolls**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/baklawa-rolls_1.jpg)

portions:  60  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients**

  * a packet of filo leaf, 
  * 200 g of almonds or pistachios crushed finely (but not reduced to powder), 
  * 125 g of melted butter, 

the sirup : 
  * 435 ml of water, 
  * 310 g of sugar, 
  * 5 ml of lemon juice. 



**Realization steps**

  1. Coarsely grind the almonds 
  2. spread the filo leaves on the worktop, 
  3. brush the first sheet of melted butter and sprinkle with dried fruit all over the surface of the sheet, 
  4. put at the end of the sheet a stick about 1cm in diameter and wrap the sheet on the stick, put aside 
  5. Take a second sheet, brush it with melted butter, take the roll of filo paste on the stick that you have prepared and arrange it on the second sheet and roll it up a second time, 
  6. tighten on the stick to have a wrinkled effect and remove the, 
  7. arrange in a rectangular dish, 
  8. keep going until the filo dough is exhausted 
  9. arrange the crumpled puddings, squeezing them together, 
  10. gently pour some melted butter on top, bake for about 25 minutes at 180 ° 
  11. after cooking, pour lemon flavored honey that you have prepared in advance, let cool before cutting and arrange your baklava in small boxes 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
