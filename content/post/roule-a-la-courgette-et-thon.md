---
title: Zucchini and tuna roll
date: '2015-03-11'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- idea, party recipe, aperitif aperitif
tags:
- Aperitif
- inputs
- Algeria
- Vegetarian cuisine
- Easy cooking
- Economy Cuisine
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/roul%C3%A9-a-la-courgette.jpg
---
[ ![roulade with zucchini and tuna](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/roul%C3%A9-a-la-courgette.jpg) ](<https://www.amourdecuisine.fr/article-roule-la-courgette-et-thon.html/roule-a-la-courgette>)

##  Rolled with zucchini and tuna, 

Hello everybody, 

A delicious entry to lovers of zucchini ... A roll of zucchini and tuna, super delicious, and above all very easy to do. 

The principle is super simple, the recipe is identical to that of [ rolled to the potato ](<https://www.amourdecuisine.fr/article-roule-pommes-de-terre-et-patate-douce.html> "Rolled potatoes and sweet potato") . In any case, personally this style of recipes I like a lot, because we do not need to have to follow the recipe to the letter. Just understand the principle of the recipe, and then use the ingredients that you like. 

This zucchini and tuna roll recipe is a recipe from my dear friend Wamani Merou, I hope you enjoy it. 

**Zucchini and tuna roll**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/roul%C3%A9-a-la-courgette.jpg)

portions:  4-6  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 3 to 4 beautiful zucchini grated 
  * 1 onion grated 
  * 1 shredded shallot 
  * 3 to 4 eggs 
  * chopped parsley 
  * 2 tablespoons fresh cream 
  * 3 tablespoons of milk 
  * salt, black pepper, saffron or turmeric 
  * tuna oil as needed 
  * cheese for sandwiche 
  * some cherry tomatoes (optional) 



**Realization steps**

  1. grated zucchini cleaned 
  2. finely chop onions, shallot and parsley 
  3. mix all these ingredients, with the whipped eggs, the cream, and the milk 
  4. season with salt, black pepper, turmeric 
  5. spread the mixture evenly, in a baking tin lined with a sheet of buttered baking paper 
  6. cook in a preheated oven at 190 degrees C for almost 30 minutes 
  7. remove the tray from the oven, place the special sandwich forages to cover the surface 
  8. garnish the surface of drained tuna with oil, and cherry tomatoes cut in half 
  9. immediately roll with the baking paper 
  10. then put back in the oven so that it takes a nice color for 5 to 7 minutes 
  11. let cool a little before cutting and tasting 


