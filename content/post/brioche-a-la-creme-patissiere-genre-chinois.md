---
title: Chinese pastry cream cake
date: '2009-04-26'
categories:
- Algerian cuisine
- Moroccan cuisine
- Tunisian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/04/243202381.jpg
---
I still made the cake of Dani, which has very good memories at home, but this time I made it kind of Chinese, so stuffed with pastry cream. the ingredients and the preparation of the dough to this brioche are here for the pastry cream: 200 ml of milk 50 gr of sugar 2 egg yolks 20 grs of maize lemon peel In a bowl, mix the eggs, the sugar and the lemon zest. Add the cornstarch and mix well. Boil the milk and pour over the egg / sugar / cornflour mixture. Then put back on fire & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 1.9  (  1  ratings)  0 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/04/243202381.jpg)

I still made the cake of Dani, which has very good memories at home, but this time I made it kind of Chinese, so stuffed with pastry cream. 

for pastry cream: 

  * 200 ml of milk 
  * 50 gr of sugar 
  * 2 egg yolks 
  * 20 grs of cornflour 
  * lemon zest 



In a bowl, mix eggs, sugar and lemon zest. Add the cornstarch and mix well. Boil the milk and pour over the egg / sugar / cornflour mixture. Then put back on low heat and allow to thicken 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/04/243202501.jpg) ![](https://www.amourdecuisine.fr/wp-content/uploads/2009/04/243202691.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/04/243202881.jpg)

after the beep sound of my map, I pulled my dough, I degassed a little, and then I spread my dough in a rectangle, I spread on it the pastry cream, on which I sprinkled a little raisin 

we roll the dough to form a pudding, and cut sections, when placed in a mold covered with parchment paper, here you let your imagination play, depending on the shape of your mold 

we cover our molds with a cloth, and place in a place protected from the current of air, and leave for a rise of 45 minutes to 1 hour. 

decorate your bun, before baking it, with an egg yolk mixed with milk. 

place your mold in a cold oven (so you do not heat up the front part), and cook for 30 minutes, or according to your oven. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/04/243203151.jpg)

next time, I will put less milk 200 ml, because in the presence of custard, my dough was difficult to handle. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/04/243203221.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/04/243203371.jpg)

bon Appétit. 
