---
title: Flan pumpkin pudding and soy milk
date: '2017-10-03'
categories:
- panna cotta, flan, and yoghurt
- sweet recipes
tags:
- desserts
- Easy cooking
- creams
- To taste
- Fast Recipe
- Kitchen without egg
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/10/flan-pudding-au-potiron-et-lait-de-soja-1.jpg
---
![pumpkin pudding custard and soy milk 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/flan-pudding-au-potiron-et-lait-de-soja-1.jpg)

##  Flan pumpkin pudding and soy milk 

Hello everybody, 

I will certainly have fun by going to look at the list of players in the game (at the bottom of this article) at least so I'll have ideas for next soy recipes. 

So with this recipe I participate in our game, [ recipes around an ingredient # 32 soy ](<https://www.amourdecuisine.fr/article-recette-autour-dun-ingredient-32-soja.html>) Aurélie is godmother and I thanked her a lot for the beautiful work she did. You must now know this game that was born more than 2 years ago now, a game that started me and Samar, and here are all the previous godmothers and their star ingredients: 

Now we're ready for this simple and easy-to-follow pumpkin and soymilk pudding custard recipe, I'll attach the video of the recipe as soon as it goes online, knowing that I've made two versions of this recipe pumpkin pudding flan and soy milk, one with gelatin, and another with commercial flan. both versions were just too good, yumiiii! 

{{< youtube 4nWGFmxbrz4 >}} 

**_The next godmother_ ** Congratulations to you and thank you all again for your superb participation! 

**Flan pumpkin pudding and soy milk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/flan-pudding-au-potiron-et-lait-de-soja-2.jpg)

**Ingredients** gelatin version: 

  * 150g of pumpkin purée 
  * 500 liters of unsweetened soy milk (store bought) 
  * 80 g of sugar 
  * 2 tbsp. Agar agar or 12 g of gelatin in foil 

version on the commercial flank: 
  * 1 tin of vanilla flavored flan for ½ liter of milk 
  * 500 ml of soy milk 
  * 2 tbsp. sugar (as needed depending on the quality of your custard) 
  * 150 gr of pumpkin puree. 



**Realization steps** gelatin version: 

  1. steam the pumpkin until it becomes tender 
  2. place the gelatin in the cold water so that it swells well. 
  3. puree with 150 ml unsweetened soy milk. 
  4. bring the rest of the soymilk and sugar to the boil over medium heat. 
  5. add the swollen and drained gelatin. 
  6. Keep stirring so it does not burn. 
  7. Add the pumpkin mixture to the warm soy milk and stir well. 
  8. remove from the fire fill the verrines. Let cool at room temperature then cool for at least 4 hours.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/pudding-.jpg)

Blank version: 
  1. steam the pumpkin until it becomes tender 
  2. place the gelatin in the cold water so that it swells well. 
  3. puree with 150 ml unsweetened soy milk. 
  4. bring the rest of the soymilk and sugar to the boil over medium heat. 
  5. add the vanilla flan powder 
  6. Keep stirring so it does not burn. 
  7. Add the pumpkin mixture to the warm soy milk and stir well. 
  8. remove from the fire fill the verrines. Let cool at room temperature then cool for at least 2 hours. 



![pumpkin pudding pudding and soymilk](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/flan-pudding-au-potiron-et-lait-de-soja.jpg)

and here is the list of participants in this round: 

2\. Soulef of the blog [ Kitchen love ](<https://www.amourdecuisine.fr/>) avec Flan pudding au potiron et lait de soja 
