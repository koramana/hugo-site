---
title: Salted butter caramel yoghurt
date: '2014-11-11'
categories:
- panna cotta, flan, and yoghurt
- sweet verrines
tags:
- Easy cooking
- Fast Food
- Healthy cuisine
- Kitchen without egg
- Cakes
- verrines
- Sweet Verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/yaourt-au-caramel-au-beurre-sal%C3%A9-1.jpg
---
[ ![yogurt with salted butter caramel 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/yaourt-au-caramel-au-beurre-sal%C3%A9-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/yaourt-au-caramel-au-beurre-sal%C3%A9-1.jpg>)

##  Salted butter caramel yoghurt 

Hello everybody, 

Homemade yogurt, we like it a lot. But I'll admit, sometimes I'm lazy to pull my ice cream machine from the bottom of the cupboard, ah by what you really have to dig to find it, lol ... 

I know there are many other ways to prepare homemade yoghurts, such as in the bain-marie, or in the oven, or even in front of a heat source, for a night ... but I prefer to make my yogurt in my little one yogurt that I have for 7 years now .... It's just a quesiton of habit. 

Today, it is a salted butter caramel yogurt that I will share with you. Yeah, I have a beautiful jar of [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html> "salted butter caramel") that I prepared the last time when I did the [ apple pie ](<https://www.amourdecuisine.fr/article-apple-pie-dautomne-au-caramel-au-beurre-sale.html> "Apple pie with salted butter caramel") , and I have to exploit it, I do not want to find the empty pot, because we would just lick it all !!! 

The recipe is really a delight, it is very easy to achieve. Otherwise for the net of [ salted butter caramel sauce ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html> "salted butter caramel") it is optional, because it must be said that yoghurt and just good, no need for a plus from above, but if it is salted butter caramel, hihihihi 

The recipe is taken from the cookbook "Yogurt cookbook", from which I had already realized the [ yoghurt with spicy clementines ](<https://www.amourdecuisine.fr/article-yaourt-aux-clementines-epicees.html> "spicy clementine yoghurt")   


**Salted butter caramel yoghurt**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/yaourt-au-caramel-au-beurre-sal%C3%A9-2.jpg)

portions:  6  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 1 whole yogurt 
  * 1 liter of whole milk 
  * 230 gr of salted butter caramel   
you can adjust the amount of salted butter caramel to your taste. 
  * 3 tablespoons milk powder   
for more smoothness, because the addition of salted butter caramel can change the texture of yogurt a little. 



**Realization steps**

  1. beat the yogurt with a whisk. 
  2. add the milk and salted butter caramel, whip a little, 
  3. introduce the milk powder whisk again and fill your jars with yoghurt. 
  4. place in the yogurt maker for at least 10 hours. 
  5. At the end of the process, place the yogurt jars in the refrigerator for at least 4 hours and enjoy. 



[ ![salted butter caramel yoghurt 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/yaourt-au-caramel-au-beurre-sal%C3%A9-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/yaourt-au-caramel-au-beurre-sal%C3%A9-3.jpg>)
