---
title: spinach squares with béchamel
date: '2017-08-17'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- inputs
- Algeria
- Easy recipe
- Ramadan
- Healthy cuisine
- Pizza
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/04/car-epin_thumb1.jpg
---
##  Spinach squares with bechamel sauce 

Hello everybody, 

I have to tell you that my husband likes to eat a lot [ chorba ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) . I do it every week !!! and he still likes that the chorba is accompanied by a good parallel entry of the [ matloue ](<https://www.amourdecuisine.fr/article-matlouh-pain-arabe.html>) who must accompany the chorba, sometimes when I have time, I make him [ boureks ](<https://www.amourdecuisine.fr/article-boureks-aux-crevettes-et-pommes-de-terre.html>) . But sometimes I'm so overwhelmed that I like to make a quick and easy entry to accompany her soup. 

I also took the opportunity to do some things with spinach that my husband eats hard times. But with this recipe of spinach and bechamel squares, this entry disappeared quickly as if by a miracle. 

If you have ideas for quick and easy entries that you can make in the blink of an eye, like those spinach squares with béchamel, let me know, because I really need it, especially by these heat, I do not really want to cook! 

**spinach squares with béchamel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/car-epin_thumb1.jpg)

**Ingredients**

  * the puff pastry 
  * a little boot of spinach 
  * a handful of mushrooms 
  * Bechamel 
  * garlic 
  * salt pepper 
  * Gruyere 

Bechamel sauce: 
  * 2 tbsp. butter 
  * 2 tbsp. flour 
  * 400 ml of Milk if not mixed with 200 ml of milk + 200 fresh cream 
  * Little spoon of salt 
  * a little chopped garlic 
  * a little nutmeg 
  * Black pepper to taste 



wash the spinach, cut it and put it in an oiled pan right away, and let it cook. 

then fry the cut mushrooms, mix with the spinach and add the bechamel sauce. 

cut the puff pastry into squares, prick with a fork, pour over the spinach mixture, and cook in a preheated oven 

![spinach squares with béchamel 2](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/car-epinard_thumb1.jpg)

a la sortie du four, râpez dessus un peu de gruyère 
