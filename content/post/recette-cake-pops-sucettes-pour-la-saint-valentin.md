---
title: recipe cake pops / lollipops for valentine's day
date: '2013-01-29'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/valentin-cake-pops-049_thumb1.jpg
---
![Valentine cake pops 049](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/valentin-cake-pops-049_thumb1.jpg)

Hello, 

very nice sweets, very easy to do, and above all too good, and good presentable to impress people around you, especially if it is for a small confectionery to offer as a gift for Valentine's Day. 

these lollipops coated with white chocolate colored according to your taste and your desire, to shape in heart, in ball or any other form which you would like, are sublime. 

so without delay I give you the little recipe: 

  * 400 g of [ brownie ](<https://www.amourdecuisine.fr/article-brownies-aux-cacahuetes-97765323.html>)
  * 120 g mascarpone 
  * 80 g of Nutella 
  * 200 g white chocolate color 
  * 1 tablespoon of oil. 
  * pearls, sugar hearts, or candies to decorate. 



method of preparation: 

  1. Prepare the cream: Mix the mascarpone with nutella 
  2. crumble the brownies and mix well with the cream, 
  3. form a large ball, flatten slightly. 
  4. Wrap in food film, put in the refrigerator for 15 minutes. 
  5. remove the dough from the fridge and spread it 
  6. form hearts with cookie cutters, or form balls. 
  7. Place them on a baking sheet lined with parchment paper and let cool in the freezer for 15 minutes. 
  8. Melt the chocolate with oil in a bain-marie 
  9. color as you like especially if you have special chocolate dyes 
  10. stuff a special succine stick into your little cake 
  11. and coat the sweets in the chocolate 
  12. decorate to choose, and leave to freeze. 



this mignardise was inspired in large part by: the delights of Algiers 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

thank you. 

[ ![panacotta has the rose 026](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/panacotta-a-la-rose-026_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-panna-cotta-a-la-rose-99132888.html>)

#  [ panacotta has the rose ](<https://www.amourdecuisine.fr/article-panna-cotta-a-la-rose-99132888.html>) , pour un menu saint valentin 
