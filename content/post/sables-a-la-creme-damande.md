---
title: Shortbread with almond cream
date: '2015-05-17'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya
tags:
- Algerian cakes
- Algeria
- Cakes
- Cookies
- Aid
- biscuits
- Little butter

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-a-la-creme-damande-.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-a-la-creme-damande-.jpg) ](<https://www.amourdecuisine.fr/article-sables-a-la-creme-damande.html/sony-dsc-289>)

##  Shortbread with almond cream 

Hello everybody, 

Here is a wonderful shortbread recipe with almond cream that is really worth trying. A recipe born of a little conversation between me and my dear Lunetoiles, when we talked about pretty [ sandblasted ettaj, or sandblasted crown ](<https://www.amourdecuisine.fr/article-sables-ettaj-sables-la-couronne-au-chocolat.html> "Shortbread ettaj - shortbread chocolate crown") that I realized recently, because Lunetoiles had the same mussels. 

And here is this little confection of Lunetoiles, who preferred to put my recipe for [ shortbread ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-fraises.html> "sands with strawberry jam") and my recipe [ almond cream ](<https://www.amourdecuisine.fr/article-gateau-frangipane-aux-abricots.html> "Frangipane cake with apricots") has the honor. I'm very proud of that, and most importantly, I'm very proud of this recipe she has beautifully achieve ...  Shortbread with almond cream 

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sables-a-la-creme-damande-001.jpg) ](<https://www.amourdecuisine.fr/article-sables-a-la-creme-damande.html/sony-dsc-290>)   


**Shortbread with almond cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sables-a-la-creme-damande.jpg)

portions:  30  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** For shortbread: 

  * 125 grams of butter (at room temperature) 
  * 1 packet of vanilla sugar 
  * 125 grams of icing sugar 
  * 1 egg yolks 
  * ½ sachets of baking powder 
  * 250 grams of flour 

For cakes with almond cream: 
  * 125 gr of butter at room temperature 
  * 125 gr of sugar 
  * ½ cuil. a coffee of almond extract 
  * 100 gr of flour 
  * 2 eggs 
  * 125 gr of almonds in white powder 
  * 1 C. a clear coffee of baking powder 

Assembly: 
  * 1 jar of strawberry jam 
  * 1 bowl of chopped and toasted almonds 



**Realization steps** Preparation of shortbread: 

  1. Mix the ointment butter with the icing sugar and the vanilla sugar packet. 
  2. Add the egg yolks and mix well. 
  3. Then the baking powder and the flour. 
  4. Wrap the dough in stretch film and let stand for 30 minutes before starting to spread the dough in small amounts at a time. 
  5. Cut out the shapes with a serrated round pieces, about 4 cm in diameter (it must be slightly larger than the almond cake) 
  6. And put to cook at 160 ° C for 12 to 15 minutes to watch the cooking (it is necessary that the shortbreads are not too gilded) 

Preparation of cakes with almond cream: 
  1. Preheat the oven to 180 ° C 
  2. In a salad bowl, whip the butter and sugar until you have a nice creamy consistency. 
  3. Add the eggs one by one, whisking well between each addition. 
  4. Then stir in the almond extract. 
  5. In another container, mix the flour, almonds and baking powder, then add this mixture gently to the butter cream. 
  6. Pour this mixture into a pastry bag. 
  7. Fill the cavities of the impression mold with the aid of the piping bag, do not fill to the brim as they will swell during cooking. 
  8. Cook for 15 minutes at 180 ° C. 
  9. Unmold on a cooling rack, wait several minutes before removing them from the mold cavity. 
  10. Repeat the process until the dough is gone.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-a-la-creme-damande.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42124>)

To assemble the cakes: 
  1. Heat some strawberry jam in a saucepan and let it cool down. 
  2. Take a round shortbread garnish with strawberry jam, place over a cake with almond cream. Then pass around the toasted almonds and then fill the cake cavity with strawberry jam. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sables-a-la-creme-damande1.jpg) ](<https://www.amourdecuisine.fr/article-sables-a-la-creme-damande.html/sony-dsc-291>)
