---
title: Indian rice cream, Phirni
date: '2014-08-10'
categories:
- panna cotta, flan, and yoghurt
- ramadan recipe
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/riz-indien-048_thumb1.jpg
---
##  Indian rice cream, Phirni 

Hello everybody, 

a [ Indian recipe ](<https://www.amourdecuisine.fr/article-plats-et-recettes-indiennes.html>) is always a pleasure to taste, and when it's a dessert, it's probably even better, so my story with this dessert is that I've been trying to make it for a while, but what's wrong? I do not like it when I miss some ingredients, every time something is missing, but finally yesterday, I had gathered all my ingredients and quickly to my pan 

in any case this [ Dessert ](<https://www.amourdecuisine.fr/categorie-recettes-sucrees/dessert-crumbles-et-barres>) Indian is a real treat, whose taste is enhanced with the addition of freshly ground green cardamom seed powder, and a little nutmeg. an intense taste with each spoon, I'm not used to publishing recipes with ingredients that are not available to everyone, but this time, I was tempted, I hope you could test it. 

**Indian rice cream, Phirni**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/riz-indien-048_thumb1.jpg)

portions:  6  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * Some saffron filaments 
  * 2 C a cup of hot milk 
  * 40 gr of butter 
  * 55 gr of powdered rice 
  * 25 gr of flaked almonds 
  * 25 grams of raisins 
  * 600 ml of milk 
  * 450 ml of evaporated milk 
  * 55 gr of crystallized sugar 
  * 12 pieces of dried apricots 
  * 1 cup freshly ground green cardamom grain 
  * ½ teaspoon grated nutmeg 
  * 2 tablespoons of flower water 

to decorate: 
  * 25 gr of nuts 
  * 15 grams of pistachios 
  * 2 pieces of dried apricots 



**Realization steps**

  1. place the saffron filaments with the hot milk and let it massage. 
  2. place the butter in a heavy saucepan, and put on a low heat, add the powdered rice, almonds and grapes, and cook for 2 minutes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2010-10-24-riz-indien_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2010-10-24-riz-indien_thumb1.jpg>)
  3. add the milk, increase the temperature and cook while stirring the mixture with a wooden spoon, when the mixture begins to bubble, reduce the fire. 
  4. cook for 10 to 12 minutes, stirring constantly 
  5. add the evaporated milk, the sugar the apricots cut into pieces, and cook everything until the mixture becomes a little thick. 
  6. add saffron and milk, cardamom powder, nutmeg and rose water. 
  7. mix everything over low heat, and voila the rice is ready   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2010-10-24-riz-indien1_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2010-10-24-riz-indien1_thumb1.jpg>)
  8. fill your verrines with this delicious mixture, and reserve until it is completely cool. 
  9. at least 2 hours in the fridge, before serving, decorate your verrines, with walnuts that you have made in a little butter, do the same things with the pistachio. 
  10. add the apricot in pieces and enjoy. 



I hope you will like this recipe, 

Thank you for your feedback 

thank you for subscribing to my newsletter if you have not done so yet. 

merci pour vôtre visites et a la prochaine recette. 
