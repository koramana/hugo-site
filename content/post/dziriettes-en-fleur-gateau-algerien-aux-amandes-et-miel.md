---
title: Dziriettes in bloom - algerian cake with almonds and honey
date: '2012-01-04'
categories:
- Non classé
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/dziriettes-en-fleurs_thumb1.jpg
---
##  Dziriettes in bloom - algerian cake with almonds and honey 

###  the Rosaces, Dziriettes el warda, دزيريات الوردة 

Hello everybody, 

Again, I'm coming back for you post one of the recipes from [ my cakes ](<https://www.amourdecuisine.fr/article-gateaux-algeriens-pour-l-aid-2010-56806564.html>) that I prepared during Aid Day. 

This time, I share with you Dziriettes in bloom - algerian cake with almonds and honey, it's Dziriettes in the shape of flowers, the first time I saw this recipe, it was one of my readers Lila who m had to send the pictures, so I absolutely wanted to try the recipe. 

![dziriettes in bloom](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/dziriettes-en-fleurs_thumb1.jpg)

so quickly I pass you the recipe:   


**Dziriettes in bloom - algerian cake with almonds and honey**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/dziriettes-en-fleur-006_thumb1.jpg)

**Ingredients**

  * 3 measures of flour 
  * ½ measure of melted margarine 
  * 1 pinch of salt 
  * ½ C.a vanilla extract coffee 
  * 3 C.a orange blossom water soup 
  * water needed 

for the stuffing: 
  * 6 measures of finely ground almonds 
  * 2 sugar measures 
  * 1 C.a coffee full of lemon zest 
  * 2 to 3 eggs by size (or until wet stuffing) 
  * 1 teaspoon of vanilla extract 



**Realization steps**

  1. work sifted flour with melted margarine salt vanilla extract .mow with orange blossom water and necessary water. 
  2. work with the apple of your hands until you have a smooth, firm dough.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/dziriettes-en-fleur-1.jpg)
  3. cover with foil and leave to rest. 
  4. during this time prepare the stuffing with the indicated ingredients. 
  5. knead with your hand for 10 to 15 minutes to obtain a fairly soft and well-aerated composition 
  6. take the dough on a floured surface and using a rolling pin spread it out very thin and then go to the machine n 1 then n 3 and then n 5 and cut out discs 9 cm in diameter using a cookie cutter. 
  7. place your dough in special boxes 
  8. put the stuffing in and take a bottle cap tilt and try to make traces to give the shape of a flower. 
  9. continue until exhaustion of the dough, decorate with an almond 
  10. place in a dish and bake in preheated oven for 15 to 20 minutes. 
  11. after cooking demold and soak them in the honey for 10 minutes then put in boxes. 



en tout cas ce gâteau est super délicieux…. 
