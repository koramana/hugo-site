---
title: salmon tartare
date: '2012-03-01'
categories:
- dessert, crumbles and bars
- gateaux, et cakes
- sweet recipes

---
Hello everybody, 

to present in a buffet as an appetizer, or even to taste as an entree, if you like tartare. So here is a very good recipe to taste, these delicious pancakes stuffed with a very good salmon tartare, dill flavored, presented in cornets, just a delight 

For pancakes: 

  * 250 g buckwheat flour 
  * 2 eggs 
  * 2 tablespoons of oil 
  * 400 ml of water 
  * 1 pinch of salt. 



for the tartar: 

  * smoked salmon fillets 
  * the juice of half a lemon 
  * olive oil 
  * sprigs of dill 
  * salt and black pepper 



method of preparation: 

in a bowl, form a well with the dry elements, break the eggs in the center and mix vigorously with the whisk by adding the milk gradually. Let the dough rest for 1 hour at room temperature. 

Heat a skillet or crepe maker and lightly oiled. Pour a ladle of dough by spreading and cook each pancake on both sides. Return as soon as the coloring. 

cut the salmon fillets into pieces, add the lemon, and the olive oil 

add chopped dill, season with salt and pepper to taste. marinate in a cool place for 15 minutes 

cut each pancake in half, and form cornets, fill them with the salmon tartare, and prick with a toothpick so that it does not open. 

good tasting, and the next recipe 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

hello everyone, to present in a buffet appetizer, or even to taste as input, if you like tartare. So here is a very good recipe to taste, these delicious pancakes stuffed with a very good salmon tartare, perfumed with dill, presented in cornets, just a delight For pancakes: 250 g of buckwheat flour 2 eggs 2 tbsp oil 400 ml of water 1 pinch of salt. for tartare: smoked salmon fillets juice of half a lemon olive oil sprigs of dill salt and black pepper preparation method: in & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 
