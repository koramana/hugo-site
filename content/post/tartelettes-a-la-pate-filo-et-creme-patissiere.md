---
title: tartlets with filo paste and pastry cream
date: '2015-11-19'
categories:
- sweet recipes
- pies and tarts
tags:
- Cakes
- desserts
- Algerian cakes
- To taste
- Pastry
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tartelettes-pate-a-filo-et-creme-patissiere-1.jpg
---
[ ![Filo pasta tartlets and pastry cream 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tartelettes-pate-a-filo-et-creme-patissiere-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tartelettes-pate-a-filo-et-creme-patissiere-1.jpg>)

##  tartlets with filo paste and pastry cream 

Hello everybody, 

We all like to taste a taste made in two or three movements ... Me, I often look for recipes like this, especially with my baby who now likes to play and go out, Sometimes it's me and him out, more than we have to House. So more time to stay two hours in the kitchen to make a cake ... and especially not pies that require several stages of realization ... 

Today it is a nice recipe to taste simple and fast, for buffet, for children. I find my friend's idea **Wamani M** really great, and I liked this choice to make the pasta dough paste filo a great idea, but attention to consume very quickly, to keep the crispness of the dough a philo, because otherwise it will absorb the moisture that can be released by the pastry cream. But if you have to prepare a large quantity, and in advance, why not brush with a thin layer of white chocolate with small pie, your pies will be even more crisp, and you do not risk have a bad surprise when serving your little treats. 

The recipe in Arabic: 

**tartlets with filo paste and pastry cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tartelettes-pate-a-filo-et-creme-patissiere-1.jpg)

portions:  20  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * pate a filo 

Custard 
  * 70 g brown sugar 
  * 15 g of maizena 
  * vanilla 
  * 2 egg yolks 
  * 250 ml of milk 

for decoration: 
  * Nutella 
  * chocolate buttons. 



**Realization steps** Start by preparing the pastry cream: 

  1. boil the milk 
  2. beat the egg yolks, sugar and vanilla until you see a nice whitish creamy mixture. 
  3. add over the broth and beat again. 
  4. pour everything into a saucepan rinsed with cold water (it will prevent the cream does not stick) and put on low heat while stirring, until thickening of the cream. 

preparation of tartlets: 
  1. for cooking tartlets I used these small molds of almost 5 cm in diameter   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/moule-a-filo.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/moule-a-filo.jpg>)
  2. take 5 sheets of filo paste, and cut slices of almost 7 to 8 cm depending on the depth of your molds 
  3. butter gently with a little melted butter, and press the leaves into the mold. 
  4. cook in a preheated oven at 180 degrees C for almost 15 minutes (see less depending on your oven) 
  5. let the pie shell cool, and just before serving, garnish with the pastry cream place in a pastry bag 
  6. place a chocolate button, and garnish with a little nutella. 



[ ![Filo and pastry pie tartlets](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tartelettes-pate-a-filo-et-creme-patissiere.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tartelettes-pate-a-filo-et-creme-patissiere.jpg>)
