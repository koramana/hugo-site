---
title: my recipes at home 6
date: '2011-01-21'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/IMGP6957_thumb1.jpg
---
![](https://www.amourdecuisine.fr//import/http://2.bp.blogspot.com/_at4PqZ8QJS8/TQllgLsGHJI/AAAAAAAABcA/60JhumgUzLM/s400/)

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/IMGP6957_thumb1.jpg)

![](https://www.amourdecuisine.fr//import/http://img690.imageshack.us/img690/9985/)

![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/500x374/2/78/30/18/Sal-s/)

![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/500x374/3/20/46/47//)

![Ball-of-berlin-2.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/boule-de-berlin-21.jpg)

[ qalb elouz ](<https://www.amourdecuisine.fr/article-35768313.html>) at hajar (a reader) 

![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/4/15/29/42/Images-pour-articles/)

![Photo - 9-.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/photo-9-1.jpg)

[ arayeches with honey ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-60064269.html>) with Sabrina (a reader) 

[ vanilla chocolate muffins ](<https://www.amourdecuisine.fr/article-muffins-vanille-chocolat-61854062.html>) at Meriem (a reader, next time it will be even better, is not it) 

a nice selection of my recipes at Amira (a reader) 

so in detail, there was: 

[ the baqlawa ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-aux-amandes-et-cacahuettes-61018034.html>)

[ Two-colored croquet ](<https://www.amourdecuisine.fr/article-croquets-bicolores-a-la-confiture-et-aux-noix-48772644.html>)

[ Makrout ](<https://www.amourdecuisine.fr/article-26001222.html>)

[ chocolate washers ](<https://www.amourdecuisine.fr/article-35955000.html>)

[ khobz eddar ](<https://www.amourdecuisine.fr/article-25345314.html>)

[ Braided bun ](<https://www.amourdecuisine.fr/article-25922246.html>) at Nancy 

[ Frangipane cake ](<https://www.amourdecuisine.fr/article-gateau-frangipane-aux-abricots-62539467.h<br%20/>%0Atml>) with apricots at blue butterfly 

and you too, if you have tried one of my recipes, do not forget to send me either the link, if you are a blogger, or the picture on this email: 

vosessais@gmail.com 

thank you for all your comments that I'm confirming slowly, and thank you for wanting to subscribe to the article publication notification, if you want to be aware of any new update 

or then add my blog to your [ google reader ](<https://www.amourdecuisine.fr/article-37693519.html>)
