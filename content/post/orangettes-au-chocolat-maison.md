---
title: homemade chocolate orangettes
date: '2014-12-24'
categories:
- dessert, crumbles and bars
- sweet recipes
tags:
- desserts
- Fruits
- Confectionery
- Easy recipe
- delicacies
- Gourmet Gifts
- Orange

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/orangettes-au-chocolat-1.CR2_.jpg
---
[ ![chocolate orangettes, gourmet basket](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/orangettes-au-chocolat-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/orangettes-au-chocolat-1.CR2_.jpg>)

Hello everybody, 

I imagined my children were eating such and such greed, but I never thought my children would eat chocolate orangettes !!! It happened at the school party, when one of the mothers had prepared chocolate orangettes, and many other sweets, like [ Bounty House ](<https://www.amourdecuisine.fr/article-bounty-maison.html> "Bounty House") , or even [ Raffaello House ](<https://www.amourdecuisine.fr/article-raffaello-fait-maison-recette-facile.html> "homemade raffaello easy recipe") In any case, her table was full of chocolate delicacies, that the children were around her like bees, lol. 

In any case, back home, my children were asking me these chocolate orangettes, so I had to buy untreated organic oranges, to make them this little delight, and it is hardly that I could you make these pictures, because they waited for the result impatiently, in addition, they ate the most beautiful ... So the photos is not the top, lol. 

**chocolate orangettes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/orangettes-au-chocolat-3.CR2_.jpg)

portions:  6  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * Peels of 2 oranges 
  * 200 gr of sugar 
  * 200 ml of water 
  * 200 to 300 gr of dark chocolate 65% 



**Realization steps**

  1. peel the oranges, and cut the skin into a stick   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/ecorces-dorange-confites-au-chocolat-261x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/ecorces-dorange-confites-au-chocolat-261x300.jpg>)
  2. blanch the peels in boiling water for 3 minutes 
  3. drain from this water, rinse with cold water 
  4. return to bleach again in boiling water for another 3 minutes 
  5. drain and rinse with cold water, if the peels are still bitter, repeat the operation a third time. 
  6. Prepare the syrup by combining the water and sugar in a saucepan. 
  7. Bring the syrup to a boil, place the peels in it and let it simmer for 50 minutes. check the cooking anyway so that it does not burn 
  8. Once the peels are well preserved, remove them from the pan and place on a rack and let cool.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/ecorces-dorange-confites-290x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/ecorces-dorange-confites-290x300.jpg>)
  9. Melt the chocolate on a bain-marie. 
  10. Dip the candied orange peel into the chocolate, remove quickly, and let cool on a sheet of parchment paper. 
  11. Keep the orange peel in an airtight container. 



[ ![chocolate orangettes, gourmet basket](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/orangettes-au-chocolat-panier-gourmand.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/orangettes-au-chocolat-panier-gourmand.jpg>)
