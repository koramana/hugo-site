---
title: Algerian cakes for the aid 2010
date: '2011-10-31'
categories:
- boissons jus et cocktail sans alcool
- cooking fat-burning, slimming recipes
- Cuisine détox
- Cuisine saine
- recipes at least 200 Kcal

---
video detailed here salam alikoum saha aidkoum everyone, we thank the good god who met us again, to celebrate this great opportunity. I want to thank you, you were many to come on my blog, I even exceed 14,000 visitors, in a day, I would never think to have so many visitors, and thank you very much, I hope you have find what you like on my blog and if you have tried one of my recipes, do not forget to send me your photos, I would be happy to publish this on my blog. therefore, & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.8  (  1  ratings)  0 

Salam alikoum 

saha aidkoum everyone, we thank the good god who met us once again, to celebrate this great occasion. 

I want to thank you, you were many to come on my blog, I even exceed 14,000 visitors, in a day, I would never think to have so many visitors, and thank you very much, I hope you have find what you like on my blog 

and if you have tried one of my recipes, do not forget to send me your photos, I would be very happy to publish this on my blog. 

So, here is my little tray of cakes that I made for this festival of Aid 2010 

you can see kefta (recipe to come), qnidlette (recipe to come), [ Skikrates ](<https://www.amourdecuisine.fr/article-25345462.html>) , [ bniouen ](<https://www.amourdecuisine.fr/article-25345483.html>) , gribiya (recipe to come)   


Mkheddete laaroussa (recipe to come) 

[ Griwech ](<https://www.amourdecuisine.fr/article-25345475.html>)   


[ Makrout ](<https://www.amourdecuisine.fr/article-26001222.html>) , coconut cnidlette, [ two-colored croquet ](<https://www.amourdecuisine.fr/article-croquets-bicolores-a-la-confiture-et-aux-noix-48772644.html>) , [ grape croquet ](<https://www.amourdecuisine.fr/article-25647952.html>)

saha aidkoum to all, and I will be happy to publish your cake trays, on my blog 

as here I share with you the cakes of the 2010 aid of Lunetoiles 

![](http://lh3.ggpht.com/_iU3nptplfb4/TIgFCGeCKnI/AAAAAAAAE3w/j2DnYngW-uQ/s720/DSC05084.JPG)

![](http://lh6.ggpht.com/_iU3nptplfb4/TIgEzd78MXI/AAAAAAAAE2o/ywAfq1DzN7E/s720/DSC05067.JPG)

![](http://lh6.ggpht.com/_iU3nptplfb4/TIgEjinvGQI/AAAAAAAAE1c/ZGAurNDzOCs/s720/DSC05048.JPG)

![](http://lh6.ggpht.com/_iU3nptplfb4/TIgD_wGS6YI/AAAAAAAAEy8/bEW2Lo3CgXA/s720/DSC05006.JPG)

thanks to all of you for sharing, saha aidkoum again, all the recipes will be coming very soon, I'm still waiting for the 1500 subscriber to the newsletter, especially since there is only about 30 people left 

bisous 
