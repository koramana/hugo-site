---
title: Mureque cake without cooking with coconut
date: '2016-06-13'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mauresque-030_thumb1.jpg
---
![Moorish 030](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mauresque-030_thumb1.jpg)

##  Mureque cake without cooking with coconut 

Hello everybody, 

a magnificent [ Algerian cake without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) , very beautiful, and super delicious, you will find also on my blog, the [ Moorish ](<https://www.amourdecuisine.fr/article-les-mauresques-kefta-57994918.html>) and the [ Moorish nutella ](<https://www.amourdecuisine.fr/article-mauresque-au-nutella-gateau-algerien-sans-cuisson-93226591.html>) , two recipes of my friends: Lunetoiles and Leila. 

This time, it's my turn to make this delight, especially now that I have the cookie cutter, and I prepared the recipe in the same way as the Moorish Nutella, but this time with coconut Powdered and it was sublime, and too good, it's a mixture of coconut and almonds.   


**Moorish with coconut - Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mauresque-040-300x225.jpg)

**Ingredients**

  * 1 measure of ground almond 
  * 1 measure of coconut powder (so reduce the coconut still a little to the blinder) 
  * 1 measure icing sugar 
  * white dye 
  * syrup (250 gr of sugar + ½ liter of water) 

for the stuffing: 
  * 1 pack of biscuits small butters 
  * 1 big handful of coconut 
  * Nutella 



**Realization steps**

  1. mix ground almonds, coconut and sugar 
  2. add the white dye 
  3. pick up with the syrup 
  4. put the side dough 
  5. prepare the stuffing by mixing the crushed biscuits and the coconut (not powdered here) 
  6. pick it up with Nutella 
  7. keep a ball of almond paste and a ball of Nutella stuffing for decoration 
  8. spread the almond paste and walnuts and using a cookie cutter shape your cakes 
  9. do the same thing with Nutella stuffing, 
  10. take the cakes, spread a little Nutella, then place the Nutella stuffing on top and cover with another almond paste covered with a little Nutella (so that it sticks well) 
  11. using a flower-shaped cookie cutter, shape a flower with the Nutella pasta, and a small ball with the almond paste and walnuts, or follow your intuition. 
  12. stick them with Nutella on your Moorish. 



do not forget that you will find on my blog, a nice selection of: 

[ Algerian cake ](<https://www.amourdecuisine.fr/article-category/gateaux-algeriens-orientales-modernes-fete-aid>) which I categorized for you in: 

[ Algerian honey cake ](<https://www.amourdecuisine.fr/gateaux-algeriens-au-miel>)

[ Algerian cake with icing ](<https://www.amourdecuisine.fr/gateaux-algeriens-au-glacage>)

[ fried Algerian cake ](<https://www.amourdecuisine.fr/gateaux-algeriens-frits>)

[ sand cake ](<https://www.amourdecuisine.fr/gateaux-sables-ghribia>)

[ ghribiya, ghoriba ](<https://www.amourdecuisine.fr/gateaux-sables-ghribia>)

[ dry Algerian cake and petits fours ](<https://www.amourdecuisine.fr/gateaux-secs-algeriens-petits-fours>)

[ Algerian cake without cooking. ](<https://www.amourdecuisine.fr/gateaux-algeriens-sans-cuisson>)

and to help you even more, I pass you [ the index of Algerian cakes ](<https://www.amourdecuisine.fr/article-gateau-algerien-et-gateaux-orientals-pour-laid-el-fitr-2014.html>) which I update as regularly as possible. 

merci. 
