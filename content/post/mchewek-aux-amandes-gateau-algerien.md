---
title: mchewek with almonds, Algerian cake
date: '2012-08-07'
categories:
- les essais de mes lecteurs

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/mchewek-gateau-algerien-020.jpg
---
##  mchewek with almonds, Algerian cake 

Hello everybody, 

The **Mchewek** or **Mchewek** is a typical pastry of the city of Algiers. Its name means in Algiers slang "full of quills" because of the decoration of flaked almonds. A gluten-free cake from the macaroons family, very delicious and well-melted. 

**mchewek with almonds, Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/mchewek-gateau-algerien-020.jpg)

**Ingredients**

  * 3 measures ground ground almonds 
  * 1 measure of fine sugar 
  * Grated zest of 2 lemons 
  * 1 handful of flaked almonds 
  * Bigarreau cherries 
  * 3 to 4 eggs 



**Realization steps**

  1. Preparation: 
  2. Mix the finely ground almonds with the sugar and grated rind of the lemon. 
  3. Add the eggs one after the other, kneading until smooth and even. 
  4. Let stand 15 minutes. 
  5. Form the balls the size of a walnut, dip them one by one into the egg white and roll each ball into the flaked almonds to coat them. 
  6. Garnish the middle of each cake with a half cherry bigarreau. 
  7. Arrange the cakes on a buttered baking sheet and bake in a preheated oven at 180 ° C, cook for 10 minutes. 
  8. When the cakes are pink below these are cooked. 
  9. after cooking, the cakes are fragile so allow to cool well before removing from the tray. 


