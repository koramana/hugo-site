---
title: homemade donuts recipe
date: '2014-09-11'
categories:
- crepes, donuts, donuts, sweet waffles

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/donut-1-b_thumb.jpg
---
[ ![easy homemade donut](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/donut-1-b_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/donut-1-b_2.jpg>)

##  homemade donuts recipe 

Hello everybody, 

and here are the [ donuts ](<https://www.amourdecuisine.fr/categorie-11700496.html>) that I let well inflate yesterday when I went to the emergency room so that the doctor sees the beautiful eye, very bluish of my poor little girl, god thank you it is a blue, and that his eye was not touched , but what can children do, just to put us in all our states ??? 

in any case, back home, she played with her sister and her brother, and I immediately attacked my donuts, which had quadrupled volume. so I pass you this recipe that I found on my forum favorite "kitchen tested" because usually when I want to try a recipe for the first time, and I can not find what I want on my books or my friends blogs, I head to this forum, because as the title suggests, it's recipes tested, so there is no doubt in the success of the recipe, and must admit, no one do not like to miss a recipe ... 

so here is the recipe, that the girls on the forum had to try and try again, and I was seduced by the recipe, was even one of the girls who put a video, that I will pass you. a delicious pastry that I share with you, with great pleasure, put remains that I love more the [ sfenj ](<https://www.amourdecuisine.fr/article-sfenj-patisserie-algerien-103295136.html>) , this delicious [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>)   


**homemade donuts recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/donut-2a_thumb.jpg)

Recipe type:  donuts  portions:  6  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * 2 eggs 
  * 1 sachet of dehydrated yeast 
  * 1 C. liquid vanilla 
  * 60 g caster sugar 
  * 1 pinch of salt 
  * 60 g of butter 
  * 250 g of milk (it is good gr and not ml) 
  * more or less 500g of flour (so depending on the absorption of the flour) 



**Realization steps**

  1. Melt the butter in the milk in the saucepan or in the microwave. 
  2. Let cool. 
  3. then pour into a salad bowl 
  4. Add vanilla, sugar, eggs, pinch of salt and yeast, 
  5. mix with a hand whisk. 
  6. Add the flour little by little, with the whisk at the beginning and by hand or with the mixer with the special kneading whips (my drummer does not have a large capacity, so I finish by hand) 
  7. The dough should be beautiful, flexible and not sticky. 
  8. Let the rest 1h / 1h30 covered with film in the oven door close 
  9. After the exposure time, the dough will double in size. degas it and spread it on a floured worktop about 1 cm to 1.5 cm thick. 
  10. Take a Donut special cutter (this is the gift I received yesterday from Souhila), or use a large glass of almost 10 cm in diameter and cut circles in the dough, in these circles cut from small circles in the middle 
  11. Place these donuts on a well floured and floured top. 
  12. Cover and let stand for another 30 minutes under a cloth or food film 
  13. heat the oil, and fry the donuts on each side, it cooks quickly 
  14. you can prepare a good ice cream, something I could not do, I was well over the dinner, and an eye on my daughter, because I have to watch her 
  15. for children I presented these donuts with caramel cream, and they have devores ca. 



[ ![donuts 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/donuts-3_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/donuts-3_2.jpg>)

recipe tested and approved, and I urge you 

merci pour vos visites bonne journée 
