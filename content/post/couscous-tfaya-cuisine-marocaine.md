---
title: Tfaya Couscous - Moroccan cuisine
date: '2018-01-18'
categories:
- couscous
- Algerian cuisine
- Moroccan cuisine
- recipes of feculents
tags:
- Onions
- Algeria
- dishes
- Chicken
- Couscous with vegetables
- Ramadan
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/couscous-tfaya-marocain-4.jpg
---
[ ![Tfaya Couscous - Moroccan cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/couscous-tfaya-marocain-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/couscous-tfaya-marocain-1.jpg>)

##  Tfaya Couscous - Moroccan cuisine 

Hello everybody, 

![Moroccan couscous tfaya 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/couscous-tfaya-marocain-2.jpg)

Do not forget my blogger friends, if you have not done it yet, it's time to join the game: 

[ recipes around an ingredient # 36 raisins ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient36-raisins-secs.html>)

![raisins recipes around an ingredient 36](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/raisins-secs-recettes-autour-dun-ingr%C3%A9dient-36-300x200.jpg)

**Tfaya Couscous - Moroccan cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/couscous-tfaya-marocain-3.jpg)

portions:  5 to 6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * [ Steamed couscous ](<https://www.amourdecuisine.fr/article-comment-cuire-le-couscous-111575933.html> "how to cook couscous")
  * 1 chicken cut into pieces 
  * 1 chopped onion 
  * 1 smen coffee 
  * ¼ cup of black pepper 
  * salt 
  * ½ teaspoon of turmeric 
  * 3 to 4 tablespoons of extra virgin olive oil 
  * some saffron pistils 
  * water 
  * 1 handful of precooked chickpeas 

for the tfaya 
  * 4 big onions 
  * 1 nice handful of raisins (I put the mixture of white grapes and black grapes) 
  * 1 teaspoon cinnamon 
  * ¼ teaspoon of black pepper 
  * ½ teaspoon of turmeric 
  * 1 smen coffee 
  * 2 to 3 tablespoons of sugar 
  * 3 tablespoons of oil 
  * a little salt 
  * a cinnamon stick 
  * some saffron pistils 
  * 1 handful of roasted almonds 



**Realization steps** Prepare the couscous sauce: 

  1. In the bottom of the couscoussier, place the chicken in pieces, 
  2. Add chopped onion, oil, spices and salt 
  3. cover with water and let the chicken cook. 
  4. at the same time you can [ cook the couscous with steam ](<https://www.amourdecuisine.fr/article-comment-cuire-le-couscous-111575933.html> "how to cook couscous")

Prepare the tfaya: 
  1. Peel the onions and cut into slices. 
  2. place them in a pot over medium heat. 
  3. Add the smen, the oil, the cinnamon stick, salt, pepper, turmeric and saffron. 
  4. stir and cook, stirring occasionally so that the onions do not burn 
  5. When the onions are tender and cooked, add the raisins and cinnamon powder. 
  6. Then add the caster sugar and cook until the sauce is reduced 

To serve 
  1. Fill a serving dish with buttered couscous 
  2. Make a hollow in the center, place the chicken, sprinkle the sauce couscous and cover generously tfaya. 
  3. decorate with toasted almonds 


