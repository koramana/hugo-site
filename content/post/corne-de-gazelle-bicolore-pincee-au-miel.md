---
title: Two-tone gazelle horn pinched with honey
date: '2011-10-31'
categories:
- gateaux, et cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/corne-loghzale-mankouche-1_thumb1.jpg
---
##  Two-tone gazelle horn pinched with honey 

###  قرن لغزال ملون منقوش بالعسل 

b = Hello everyone, 

Here is a recipe Horn Hornet two-tone pinch honey, one of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) that I like a lot : **Gazelle horn** . 

A cake that my mother has always made us **Gazelle horn** almonds, but I especially loved those prepared by my aunt **Gazelle horn** Ice. 

Today I move to the modernized version of this [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , Two-tone gazelle horn pinch honey, a beautiful makeover for a delicious **Algerian cake** . Model pull from the book: **Oriental cakes** , step not step. 

![Two-tone gazelle horn pinched with honey](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/corne-loghzale-mankouche-1_thumb1.jpg)

**Two-tone gazelle horn pinched with honey**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/garn-loghzale-benekache-maasel_1.jpg)

**Ingredients** dough: 

  * 250 gr of flour 
  * 75 gr softened margarine 
  * 1 pinch of salt 
  * 1 C. white dye coffee 
  * 1 C. a green dye coffee 
  * flower water 
  * water 

the joke: 
  * 250 gr of almonds 
  * 125 gr of sugar 
  * 1 C. a vanilla coffee 
  * 1 egg 

garnish: 
  * honey 
  * crushed pistachios. 



**Realization steps**

  1. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  2. Mix well with your fingertips 
  3. divide the dough into two parts 
  4. for the one you are going to color in white add the dye, then pick up the dough with flower water, add a little water to pick up, continue to knead a few seconds. 
  5. for the other color, take the other half of the shortbread, add the dyes to the orange water until the desired color is obtained, and start to collect the dough with this water, add the water if you need, and knead a few seconds. 
  6. cover your dough and let it sit on the side. 
  7. for stuffing mix almonds and dry ingredients, collect with egg and set aside. 
  8. spread the two pasta, pass the colored pasta in the pasta machine, side tagliatelles, place the colored strips on the white paste, trying to keep the tape distance between each strip.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/corne-de-gazelle-bicolore-au-miel-au-nekache_thumb1.jpg)
  9. iron the white dough with the colored strips in the dough machine, to stick them well, pass them to the number 6. 
  10. cut circles with the 8 cm diameter cookie cutter. 
  11. make cornets with the almond stuffing. 
  12. place them on the dough circles. roll up and give the shape of a horn, pinched with pliers. 
  13. bake for 10 minutes at 170 degrees, and after cooling soak gazelles horns in honey. 
  14. and cover the top of the cone with crushed pistachios. 


