---
title: sands with strawberry jam
date: '2017-03-17'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/sables-a-la-confiture-gateaux-de-laid-2014.CR2_.jpg
---
![sands with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/sables-a-la-confiture-gateaux-de-laid-2014.CR2_.jpg)

##  Sands with strawberry jam 

Hello everybody, 

sands with strawberry jam: I love shortbread with strawberry jam, and I never miss opportunity to realize them, especially that my shortbread are super melting in the mouth, super delicious, not too sweet, so we can eat 3 or 4 pieces without having nausea, hihihihi. 

This time I made my sands at the strawberry jam at the [ homemade strawberry jam ](<https://www.amourdecuisine.fr/article-confiture-de-fraises-express-au-micro-onde.html> "strawberry jam «express, in the microwave»") , a jam recipe well express in the microwave, so enjoy strawberries as long as it's always the season. 

![sands with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/sabl%C3%A9s-a-la-confiture-Aid-2016.jpg)

**sands with strawberry jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/sabl%C3%A9s-a-la-confiture-Aid-2016.jpg)

portions:  40  Prep time:  20 mins  cooking:  12 mins  total:  32 mins 

**Ingredients**

  * 250 gr of butter at room temperature (no margarine, this is the cause that missed this recipe to many people) 
  * 100 gr of maizena 
  * 125 gr of icing sugar 
  * 2 egg yolks 
  * vanilla 
  * 1 sachet of baking powder 
  * flour as needed 



**Realization steps**

  1. whip the butter with the sugar and the vanilla sugar sachets to have a nice cream 
  2. add the egg yolks and mix well (I say 2 egg yolks in my recipe, but you see 4, because I doubled the quantities) 
  3. introduce the cornstarch, then a little yeast mixed with flour 
  4. pick up the dough with dough, adding the flour in a small amount to make a dough well malleable that picks up well. 
  5. wrap in stretch film and allow to rest for 30 min before starting to spread the dough in a small quantity on a lightly floured work surface. 
  6. cut out shapes according to your mold. 
  7. cook at 180 ° C (shortbreads should not be too browned) while monitoring for 10 to 12 minutes. 
  8. once all the shortbreads are cooked, assemble with jam (I use the strawberry jam) and I dip the edges in crushed peanuts 



preparation: 

1-beat the butter with the sugar and the vanilla sugar sachets to have a nice cream 

![sands with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/203654401.jpg) ![sands with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/203655091.jpg)   
2 - add the yolks and mix well (I say 2 egg yolks in my recipe, but you see 4, because I doubled the quantities) 

![sands with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/203655221.jpg)

3- introduce the maïzena, then a little yeast mixed with flour 

4- Pick up the dough with dough, adding the flour in small amount to have a dough well malleable that picks up well. 

![sands with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/203655331.jpg)   
5- wrap in stretch film allow to rest for 30 min before starting to spread the dough in a small amount, on a lightly floured worktop. 

![sands with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/203655451.jpg)   
6- cut shapes according to your mold. 

![sands with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/203656251.jpg)

7- cook at 180 ° C (the shortbread should not be too brown) while monitoring between 10 to 12 minutes.   
8-Once all the shortbread cooked, assemble with jam (I use the strawberry jam) and I plunge the edges into crushed peanuts. 

[ ![sands with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sabl%C3%A9s-a-la-confiture-de-fraises.bmp.jpg) ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-fraise-2.html/sables-a-la-confiture-de-fraises-bmp>)

they are exceptionally melting I promise you 

ps : les sablés ne doivent pas être trop épais avant cuisson vue qu’il y a de la levure 
