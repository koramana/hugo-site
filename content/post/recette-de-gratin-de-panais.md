---
title: recipe of parsnip gratin
date: '2013-11-11'
categories:
- bakery
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-panais-057.CR2_thumb.jpg
---
<table>  
<tr>  
<td>

![parsley gratin 057.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-panais-057.CR2_thumb.jpg) 
</td>  
<td>

![parsley gratin 045.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-panais-045.CR2_thumb.jpg) 
</td> </tr> </table>

##  recipe of parsnip gratin 

Hello everybody, 

after the recipe for [ parsnip puree ](<https://www.amourdecuisine.fr/article-puree-de-panais.html> "From mashed parsnip") Here I am with another parsnip recipe, and this time it's a delicious gratin parsnip, and it was a real treat, I really recommend this gratin. 

maybe you are not many to like this vegetable, but I like a lot, in fact it depends on the recipe and cooking, in any case, for me, this vegetable is likely to be on my list of shopping every time. 

especially to prepare this gratin once again, because, the captivating smells that emanated from the oven, and the taste with each bite, frankly a true delight. 

**recipe of parsnip gratin**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-panais-050.CR2_thumb.jpg)

Recipe type:  dish  portions:  4  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients**

  * 400 gr of parsnips. 
  * 1 onion. 
  * 400 ml whipping cream 
  * Butter for greasing the mold 
  * ¼ cup of nutmeg 
  * ¼ cup of black pepper 
  * salt, thyme 
  * 1 tablespoon of flour 
  * grated cheese. 



**Realization steps**

  1. Preheat oven to 200 ° C 
  2. peel the parsnips. and cut them into thin slices, using a mandolin, if possible, 
  3. Mince the onion, and grate the cheese. 
  4. Grease a baking dish generously with butter. 
  5. place the mixture of parsnip, grated cheese, and onion, reserve the beautiful slices of parsnip, to garnish the mold. 
  6. by riding the pucks. 
  7. In a saucepan, heat the crème fraîche add salt, nutmeg, thyme, black pepper, and flour 
  8. when the mixture comes to a simmering, pour it over the parsnips, 
  9. Sprinkle top with cheese and some thyme leaves and a pinch of nutmeg. 
  10. Cover with aluminum foil and cook for 50 to 60 minutes in the oven (depending on the fineness of the parsnips, otherwise you will need more time) 
  11. Remove the aluminum foil, pierce with a fork, parsnip should be al dente 
  12. cook another 15 minutes uncovered, or until it becomes golden brown. 
  13. It is important to leave at room temperature for 15-20 minutes before serving, so that the gratin is well taken. 



{{< youtube 4gcuyE9TR4c >}} 

[ ![gratin of parsnips 053.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-panais-053.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-panais-053.CR2_2.jpg>)
