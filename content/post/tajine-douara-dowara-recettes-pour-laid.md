---
title: tajine douara, dowara, recipes for help
date: '2012-10-22'
categories:
- gateaux d'anniversaires, de fetes, celebrations
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tripes-aux-legumes-2-_thumb.jpg
---
##  tajine douara, dowara, recipes for help 

###  Bakbouka, douara in sauce, tajine dowara 

Hello everyone, 

tajine douara, dowara, recipes for help, a recipe that comes directly from Algeria, from my mother's kitchen, hummmmmmmm 

by this cold, I will love that my mother does not send me the photos, but rather the dish as it is ... sniff sniff. 

I really like this dish of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) tasty, but here they do not sell the tripe complete with the intestines, and in this dish, I really like the guts ... 

finally, I share with you the photos and the recipe of my mother: 

**bakbouka - Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tripes-aux-legumes-2-_thumb.jpg)

Recipe type:  hand  portions:  4  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * 1 kg of tripe (fat-double, intestines, belly ...) 
  * 1 handful of chickpeas soaked the day before 
  * 4 or 5 carrots 
  * 3 zucchini 
  * 2 tablespoons of oil 
  * 1 tbsp canned tomato soup 
  * 1 onion 
  * 2 cloves garlic 
  * a chopped coriander boot 
  * paprika, black pepper, salt 



**Realization steps**

  1. Clean the tripe, cut into small pieces (about 2 to 3 cm around) 
  2. put them in a done everything, or in a casserole of preferable. 
  3. Add the oil and chopped onion in a thin piece and crushed garlic. 
  4. return to a slow fire. 
  5. Add black pepper, paprika and salt, mix, and leave a little on low heat. 
  6. then cover with water, close the casserole and let it cook. 
  7. in the meantime, scrape and cut the carrots out of two, as well as the zucchini. 
  8. half way through the tripe, immerse the vegetables and the chickpeas, add the tomato concentrate and let it cook over a low heat, while checking the water during the cooking, until the grout is smooth. decorate with coriander. 
  9. and enjoy hot, you can throw a pepper to enhance the taste. 



Thank you for your feedback 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
