---
title: chicken breasts stuffed with Choumicha mushrooms
date: '2011-10-20'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/blanc-de-poulet-champignon-de-choumicha_thumb1.jpg
---
![chicken breasts stuffed with Choumicha mushrooms](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/blanc-de-poulet-champignon-de-choumicha_thumb1.jpg)

##  chicken breasts stuffed with Choumicha mushrooms 

Hello everybody, 

I try with my children to find just a few minutes to watch TV or a program that I like, but impossible, no luck with their endless programs, frankly .... I remember the time or in the TV program, we were entitled to only 1 hour of cartoons, and our parents must be in a good mood, to turn on the TV, when it is 17:00, now the TV for children, it's 24h / 24h ....... 

yes, then this time, I do not know how, I was able to zap on 2M, the Moroccan channel, to be able to watch the beautiful choumicha cook, and by chance, she presented this day a beautiful recipe .... 

chicken breast stuffed in a very nice way, and with a very tasty stuffing 

and at the first opportunity I tried his recipe, which was a real treat, huuuuuuuuuuuuuuum 

the only problem, I made this delight in the evening for me and my husband, and in my kitchen I have a bad light, so the photos are not as successful as the recipe.   


**chicken breasts stuffed with Choumicha mushrooms**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/blanc-de-poulet-aux-champignons_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 4 chicken breasts 
  * half an onion 
  * 1 clove of garlic 
  * some sprig of parsley 
  * some sprigs of coriander 
  * 8 small mushrooms (fresh or in box) 
  * 5 tablespoons of bread crumbs 
  * Hard mozarilla (not the one in the water, or replace with a cheese that will melt when cooked) 
  * salt, black pepper, olive oil. 



**Realization steps**

  1. in the bowl of the blinder, put the cut mushrooms, garlic, onion, parsley, cilantro, half of the mozzarilla, bread crumbs (breadcrumbs are added to absorb the water released by the onion and mushrooms) 
  2. reduce everything, and season with salt and black pepper 
  3. take the chicken breasts, and with a sharp knife, make a cast in the middle, and then try to open both sides, to have the shape on the picture above 
  4. season lightly with salt and black pepper 
  5. put some olive oil on it with a brush, or by hand 
  6. garnish stuffing, without overfilling 
  7. decorate with the remaining mozarilla 
  8. and bake in the oven, until chicken is cooked 
  9. you can recover the sauce in the baking pan, and sprinkle the chicken breasts with, during the presentation 



la recette etait trop bonne, a faire absoluemnt, lors d’un buffet, ou quand on invite des amis. 
