---
title: Artichokes Stuffed with Shrimp and Aioli Sauce
date: '2014-03-11'
categories:
- Algerian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/artichaut-farcis-et-sauce-aioli-021.CR2_.jpg
---
![artichoke-stuffed-and-sauce-aioli-021.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/artichaut-farcis-et-sauce-aioli-021.CR2_.jpg)

Hello everybody, 

it's the season of artichokes, and we always try to make beautiful recipes with this plant. 

If I'm my husband, it's going to be always in salad, with a good acid vinaigrette, but I like discovering and testing new recipes and here it is ...    


**Artichokes Stuffed with Shrimp and Aioli Sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/artichauts-farcis-aux-crevettes.CR2_.jpg)

Recipe type:  salad  portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 2 medium artichokes 
  * salt 
  * lemon 
  * water for cooking 

prank call: 
  * 1 glass of frozen shrimp 
  * ¼ cup of grated Parmesan cheese 
  * ½ glass of bread crumbs 
  * 1 clove of garlic 
  * 2 tablespoons chopped parsley 
  * 2 tablespoons of olive oil 
  * 1 pinch of black pepper 

Aioli sauce: 
  * 2 egg yolks 
  * 1 cup of oil 
  * 2 cloves of garlic, chopped 
  * ¼ teaspoon of lemon zest 
  * 2 tablespoons lemon juice 
  * salt 



**Realization steps**

  1. clean the artichokes well under the abundant water 
  2. cut each artichoke on two with a knife, and try with a little lemon juice, so that it does not blacken. 
  3. remove the thin leaves just in the heart of the artichokes, as well as the hays. lemon again 
  4. cook the artichokes, in boiling salted water, with a drizzle of lemon juice. 
  5. prepare the stuffing: fry the shrimps in a little oil 
  6. keep some pieces for decoration. 
  7. in the bowl of a blinder, place the shrimp, parmesan, bread crumbs, parsley and garlic clove 
  8. mix to have a homogeneous mixture, keep. 
  9. when the artichokes are cooked, drain them, and let them cool well. 
  10. prepare the aioli: place the egg yolks in a small bowl, 
  11. using an electric whisk, whip the egg yolks, 
  12. Add the oil gradually while whisking. 
  13. add grated garlic, lemon peel and lime juice. Refrigerate. 
  14. garnish the artichokes with the prawn stuffing. 
  15. garnish with unmixed shrimp, grated parmesan and black pepper. 
  16. pour a drizzle of oil over 
  17. grill in the oven until golden brown. 
  18. serve the warm artichokes with the aioli sauce. 



![artichoke-stuffed-023.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/artichaut-farci-023.CR2_.jpg)
