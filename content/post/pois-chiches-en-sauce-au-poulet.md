---
title: chickpeas in chicken sauce
date: '2010-12-11'
categories:
- couscous
- Algerian cuisine
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/12/pois-chiches-en-sauce-au-poulet-2.jpg
---
![chickpeas in chicken sauce](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/pois-chiches-en-sauce-au-poulet-2.jpg)

##  chickpeas in chicken sauce 

شطيطحة حمص بالكمون 

Hello everyone, 

At the request of my friend Hannah, who has eaten at my home recipe for chickpeas in sauce and who kept claiming the recipe that I never posted on my blog, because everyone has his method to make this dish, and everyone the little family secrets with. 

but Hannah is an Englishman, and does not know this well-known dish, beyond England, hihihihihi. Usually, this dish is usually prepared without meat. In Constantine in the small Arab restaurants, in the small alleyways (the zinqa) one smells the smell of this magnificent dish, that the cooks cook the day before on fire very very soft, in a white sauce flavored with Cumin and seasoned with salt and during the presentation, the cook adds to the side, a small tomato sauce well raised to the taste of chilli, known as the doubara (very well known especially in Biskra). 

So we mix this sauce directly to the chickpea dish and enjoy ... .. I had a lot of luck to accompany my father, and eat with him this delicious dish in these small restaurants, long live the traditional cuisine! 

But here I present another way to make chickpeas in sauce. Of course always remains that every person has his own recipe of this delicious dish.   


**chickpeas in chicken sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/pois-chiches-en-sauce-au-poulet-11.jpg)

Recipe type:  Algerian cuisine  portions:  4  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * 500 grams of chickpeas deceived the day before 
  * 3 to 4 pieces of chicken 
  * 1 cup of cumin 
  * 2 cloves garlic 
  * 1 tablespoon of tomato concentrate,   
(or according to your taste) 
  * 3 tablespoons of olive oil 
  * salt according to taste 
  * ½ onion for decoration   
(optional and according to taste) 



**Realization steps**

  1. in a pot, fry chicken pieces, crushed garlic with cumin and salt, and concentrate tomato, turn with wooden spoon, so tomato does not burn, then cover with water . 
  2. place the chickpeas in a slow cooker with a little salt, and enough water to cover the chickpeas, close the casserole and cook. 
  3. when the chickpeas are very tender, add them to the sauce with their water (especially that it is not a lot of water, if the chicken is already tender, remove it from the sauce, and leave well the sauce of chickpeas take. 
  4. towards the end of the cooking put the chicken back, so that it tastes like chickpeas 
  5. before serving, cut the onion into small squares and decorate, sprinkle the dish, with ½ spoon of olive oil   
(I use the olive oil that I bring with me every year of Kabylie, it has a wonderful taste) 
  6. sprinkle with a little powdered cumin and enjoy. 



![chickpea-in-sauce-the-chicken-1](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/pois-chiches-en-sauce-au-poulet-11.jpg)

this dish I had it accompany the famous [ Focaccia with rosemary and onion ](<https://www.amourdecuisine.fr/article-focaccia-au-romarin-et-oignon-62123009.html>)

![focaccia-004 thumb](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/focaccia-004_thumb.jpg)
