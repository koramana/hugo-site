---
title: crepe cake with applesauce
date: '2012-02-05'
categories:
- Buns and pastries
- Mina el forn

---
hello everyone, a very good crepe cake full of fluffy, easy to make, melting in the mouth, and rich in flavor, that I prepared for my diet Caoch linda, moreover if you want to contact her for the diet, I'm giving you her email (linda@my-herbalife-health.com) so Linda did not come out empty-handed from home, she took a nice little share with her, and it really made me pleasure. So we go to the recipe: ingredients: 8 to 10 crepes caramel applesauce caramel ingredients: 1 pinch of salt 40g sugar & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

a very good crepe cake full of mellowness, easy to make, melting in the mouth, and rich in flavor, that I prepared for my diet Caoch linda, besides if you want to contact her for the diet, I pass you his email (linda@my-herbalife-health.com) 

so Linda did not come out empty-handed from home, she took a nice little part with her, and it really made me happy. 

so we go to the recipe: 

ingredients: 

  * 8 to 10 [ Crepes ](<https://www.amourdecuisine.fr/article-crepes-aromatisees-a-l-orange-98544972.html>)
  * [ applesauce ](<https://www.amourdecuisine.fr/article-compote-de-pomme-98542533.html>)
  * caramel 



caramel ingredients: 

  * 1 pinch of salt 
  * 40g of sugar 
  * 1 C. water 
  * 30g of half-salted butter 
  * 50 gr of fresh cream. 



method of preparation: 

  1. Line the inside of a buttered baking pan with about three to four pancakes. 
  2. Pour a few spoonfuls of compote and then arrange a pancake and repeat this operation until the compote has run out. 
  3. Put your cake in the oven for 5-8 minutes. 
  4. Meanwhile, prepare your caramel. 
  5. Put in a small saucepan salt, sugar and water. 
  6. Heat until you get a blond toffee. 
  7. Add the cream, being careful not to burn yourself. 
  8. Heat by mixing vigorously with a whisk to obtain a creamy sauce. 
  9. Stir in the half-salted butter. 
  10. Unmould your cake and serve it with the caramel 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
