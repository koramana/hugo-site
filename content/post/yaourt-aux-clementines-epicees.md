---
title: spicy clementine yoghurt
date: '2016-12-04'
categories:
- dessert, crumbles and bars
- panna cotta, flan, et yaourt

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-005.CR2_-1024x1024.jpg
---
[ ![clementine yogurt with spice 005.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-005.CR2_-1024x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-005.CR2_.jpg>)

##  spicy clementine yoghurt 

Hello everybody, 

It's the season of pretty clementines, and I really enjoy this fresh fruit, juicy and refreshing ... and before yesterday, I was looking at this book of yogurt recipes that had been dragging for a while in my little library (I have so many books that sometimes I'm surprised to find a new one, that I have not seen for a long time, and that I forgot when I bought it) 

in any case, in this book, there was the recipe of this spicy clementine yogurt, and I admit you quickly, quickly made, I did not hang around, especially since I always have these spices at home, for make the gingerbread that I like a lot. 

**Spicy clementines yoghurt**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-010.CR2_-1024x1021.jpg)

Recipe type:  Dessert  portions:  6  Prep time:  20 mins  cooking:  600 mins  total:  10 hours 20 mins 

**Ingredients**

  * 5 to 6 clementines according to the size 
  * 8 teaspoon powdered brown sugar 
  * 1 teaspoon of four spices (cinnamon, ginger, nutmeg, clove) 
  * 1 whole yoghurt (not whole 0%, it will not succeed yoghurt) 
  * 1 liter of whole milk 
  * 2 tablespoons of sugar   
In the book they did not even add sugar, but I did not want to have a surprise with an inedible yogurt. 



**Realization steps**

  1. Peel the clementines, and put the quarters in a frying pan with the sugar and the four spices. 
  2. cook for about 5 minutes. 
  3. Leave the quarters at the bottom of the pots and reserve the cool. 
  4. beat the yogurt with a whisk. 
  5. add the milk and sugar, whip a little, and fill your jars with yoghurt. 
  6. place in the yogurt maker for 10 hours. 
  7. put in the refrigerator for at least 4 hours and enjoy. 



Spiced Clementines yogurt / yoghurt 

Ingredients 

  * 5 to 6 clementines 
  * 8 teaspoon of brown sugar 
  * 1 teaspoon allspice (cinnamon, ginger, nutmeg, cloves) 
  * 1 natural yogurt (not 0%) 
  * 1 liter milk 
  * 2 sugar tablespoons   
In the book where I got the recipe, they did not put sugar. 



[ ![spicy clementine yogurt 010.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-010.CR2_-1024x1021.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-010.CR2_.jpg>)

preparation 

  1. Peel the clementines and put the quarters in a pan with the sugar and spices. 
  2. cook for about 5 minutes. 
  3. spread on the bottom of yogurt pots and let it cool in the fridge. 
  4. mean while, beat yogurt. 
  5. add milk and sugar, beat again, and fill your yogurt pot. 
  6. place in the yogurt maker for 10 hours. 
  7. refrigerate for at least 4 hours and enjoy. 



[ ![spicy clementine yogurt 012.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-012.CR2_-731x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-012.CR2_.jpg>)

I hope this recipe will please you, and if you have tried one of my recipes, send me the picture here: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

[ ![yoghurt with clementine spice 026.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-026.CR2_-756x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-026.CR2_.jpg>)
