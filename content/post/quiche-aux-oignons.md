---
title: onion quiche
date: '2015-09-27'
categories:
- pizzas / quiches / pies and sandwiches
tags:
- Salty pie
- Vegetables
- inputs
- Algeria
- la France
- Easy cooking
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/quiche-aux-oignons-1.jpg
---
[ ![onion quiche](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/quiche-aux-oignons-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/quiche-aux-oignons-1.jpg>)

##  onion quiche, 

Hello everybody, 

a quiche easy to make and well perfumed to the taste of the onion, do you like? I'm not going to say no to taste a piece, and savor all these perfumes ... 

Thank you Lunetoiles again for this delicious onion quiche recipe ... If you try it, do not forget to share your opinion, or join us on my facebook group: 

**onion quiche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/quiche-aux-oignons-2.jpg)

portions:  6  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** for a 28 cm mold Homemade salted dough (broken): 

  * 200 gr of flour 
  * 90 gr of very cold butter cut into pieces 
  * 1 whole egg 
  * 20 ml of cold water 
  * 1 pinch of salt 

Quiche garniture: 
  * 3 very very large onions (900 gr) 
  * 500 ml of milk 
  * 1 to 2 spoons of thick cream 
  * 3 tablespoons flour 
  * 1 egg 
  * 100 gr grated emmental + to sprinkle 
  * 25 gr of butter 
  * olive oil 
  * salt pepper 



**Realization steps**

  1. Butter and flour a tart pan 28 cm in diameter. 

For the broken dough: 
  1. In the food processor equipped with steel blades, put in order the flour, the cold butter cut into pieces, a pinch of salt. 
  2. Start the robot, and pulse until you get a fine breadcrumbs, then add the water and the egg. Dough until a ball of dough forms. 
  3. Take the broken ball of dough, wrap it in plastic wrap and cool for at least 1 hour. 
  4. After one hour of rest, take the dough out of the refrigerator, and start by flouring a worktop, flour the ball of dough and flatten the ball of dough by tapping it with the flat of the end of the roll. 
  5. Roll out the dough to make a larger disk than your pie pan, and roll the dough into the pie pan, prick with a fork and reserve in the fridge. 
  6. Preheat the oven to 200 ° C (th.6 / 7) 

Prepare the filling: 
  1. Peel, wash and cut the onions into thin slices, put them in a pan to cook with olive oil and butter, stirring very often. 
  2. Let them cook until they melt and take a nice yellow, golden color. 
  3. Or, first cook them in the microwave in a tupperware for about 10 minutes, then finish cooking in the pan with a spoonful of olive oil and a little butter, leaving them to color while stirring. 
  4. Add the spoons of flour with onions while stirring, then the milk, stirring constantly, then the cream and cheese still stirring. 
  5. Salt, pepper, and mix well. 
  6. In a small bowl, beat the egg with a fork and add to the filling. 
  7. Take out the pie pan from the refrigerator. 
  8. Spread the onion filling over the dough. 
  9. Sprinkle with a little grated emmental and bake at 200 ° C for about 30 minutes. 



[ ![onion quiche](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/quiche-aux-oignons-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/quiche-aux-oignons-2.jpg>) [ ![quiche with onions 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/quiches-aux-oignons.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/quiches-aux-oignons.jpg>)
