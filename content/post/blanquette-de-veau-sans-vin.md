---
title: veal stew without wine
date: '2016-11-29'
categories:
- diverse cuisine
- Plats et recettes salees
- recipe for red meat (halal)
- riz
tags:
- dishes
- Meat
- Mushrooms
- creams
- Algeria
- Ramadan
- la France

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/blanquette-de-veau.jpg
---
![veal stew without wine](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/blanquette-de-veau.jpg)

##  veal stew without wine 

Hello everybody, 

The veal blanquette is a great classic of French cuisine, I was lucky enough to often enjoy this delicious dish very tasty prepared by the care of my aunt who succeeded wonderfully. 

I was very happy that my husband had the same weakness as me for this recipe, he who does not like at all dishes based on white sauce, like the blanquette of veal because his mother often prepared this dish accompanied by [ Baked Potato ](<https://www.amourdecuisine.fr/article-pomme-de-terre-au-four-et-rillettes-de-thon.html>) . I especially thank our butcher who brought veal specially at my request, because among butchers in Pakistan we never find beef or veal, as Pakistanis are influenced by the Indian tradition where the cow is sacred. 

The veal blanquette without wine as I realized consumes a lot of time during its realization, but frankly this dish is worth all this trouble (and all the dishes that will fill the sink during the preparation, hihihih), it is necessary just do it a little bit in advance. 

**veal stew**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/blanquette-de-veau-1.jpg)

portions:  4  Prep time:  30 mins  cooking:  60 mins  total:  1 hour 30 mins 

**Ingredients**

  * 500 g of veal 
  * 60 g half-salted butter 
  * 2 small shallots 
  * 3 bay leaves 
  * 2 branches of thyme 
  * 1 leek 
  * 1 celery stalk 
  * 2 medium carrots 
  * 1 crushed garlic clove 
  * 6 mushrooms of Paris 
  * 1 C. lemon juice 
  * 2 tbsp. flour 
  * 20 cl whole cream 
  * 2 egg yolks 
  * salt, pepper, nutmeg   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/ingr%C3%A9dients-de-la-blanquette-au-veau.jpg)



**Realization steps**

  1. Place 20 gr of butter (about 60) in a casserole on a strong enough fire. 
  2. when it melts completely, add the veal pieces. 
  3. Salt and pepper generously and color the meat on all sides 
  4. Wash the celery cut, the leek cut in two and then sliced, the carrots cut into slices, and the shallots cut on 2. 
  5. drain and add to the meat in the pot. 
  6. crush the clove of garlic with a knife (put pressure on it without cutting it) 
  7. Add the bay leaves and thyme branches, cover generously with water to cover the meat and vegetables, cover and cook until the meat becomes tender. 
  8. You can add a little water if necessary.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/bouillon-pour-blanquette-de-veau.jpg)
  9. in the meantime, wash the mushrooms well and cut them into slices (remove one part of the foot) 
  10. brown in 20 grams of butter and a drizzle of lemon juice   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/cuisson-des-champignons.jpg)
  11. Returning to the meat stock, if it is tender, put everything in Chinese. 
  12. remove the meat and carrots and keep them aside.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/bouillon-de-veau.jpg)
  13. prepare a white roux with the remaining butter and flour over low heat in a saucepan and melt the butter. 
  14. Add the flour and cook a little. 
  15. Now add the veal broth that you just recovered while mixing, add in small amounts, until you have a nice sauce of almost 1 liter. ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/roux-blanc-pour-blaquette-de-veau.jpg)
  16. Put the meat, carrots and mushrooms in this sauce, season with salt, black pepper, nutmeg and let go a little.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/etape-finale-blanquette-de-veau.jpg)
  17. In a bowl, put the two egg yolks, add the cream. Mix well and pour into the hot sauce. 
  18. remove from heat and enjoy with good rice or potato in the oven. 



![veal stew without wine 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/blanquette-de-veau-2.jpg)
