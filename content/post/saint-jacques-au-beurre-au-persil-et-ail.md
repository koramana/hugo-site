---
title: Scallops with parsley butter and garlic
date: '2012-11-16'
categories:
- gateaux algeriens au glaçage
- gateaux algeriens- orientales- modernes- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/noix-de-saint-jacques-016_thumb.jpg
---
![scallops with garlic butter](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/noix-de-saint-jacques-016_thumb.jpg)

Hello everybody, 

Here is a recipe for small appetizers mixing the sophistication of the scalloped walnuts on a bed of parsley cream in crispy bites, which I really liked, this is my first recipe for Saint Jacques nuts , and it was too good, especially with this cream of parsley, I tasted in the company of my two friends this recipe with a good spinach soup. 

so without further ado, I give you this delicious recipe: 

**Ingredients:**

  * 6 scallops (frozen for me) 
  * 1 shallot, 
  * 1/2 cloves of garlic, 
  * 2 tbsp. chopped parsley 
  * 30 g of butter, 
  * 2 servings of cheese 
  * 1 to 2 butter, 
  * Salt, 
  * Mill pepper, 
  * 6 flights to the wind, 



method of preparation: 

  1. Finely chop the shallots, garlic and parsley in the bowl of a mini-blinder 
  2. mix with butter and cheese and season to taste with salt and pepper. 
  3. In a frying pan, heat a knob of butter and fry the scallops for 1 minute on each side until they are cooked. 
  4. remove the nuts and set aside on a hot plate, 
  5. Garnish the bites with the parsley cream. and place the nut on top 
  6. Heat the bites in medium oven 175 ° C, about 10 minutes. 
  7. hot. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
