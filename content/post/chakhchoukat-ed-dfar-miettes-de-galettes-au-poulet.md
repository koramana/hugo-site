---
title: chakhchoukat ed dfar-Chicken crumbs with chicken
date: '2010-02-25'
categories:
- dessert, crumbles et barres
- panna cotta, flan, et yaourt
- recette de ramadan
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chekhchoukha-de-constantine.jpg
---
[ ![chekhchoukha of constantine](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chekhchoukha-de-constantine.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chekhchoukha-de-constantine.jpg>)

##  chakhchoukat ed dfar ... ..Chicken pancakes 

Hello everybody 

Here is one of the delights of the cuisine of eastern Algeria, **chakhchukhat dfar** , Constantinese chakhchukha or crumbs galettes in sauce with chicken, or even meat ... This is one of the dishes that is sometimes presented at parties, or as a special dinner for guests. 

You can see here as [ prepare chekhchukhat dfar ](<https://www.amourdecuisine.fr/article-preparation-de-chekhchoukhat-dfar.html>) and when you have the necessary amount to prepare the dish, you can attack the recipe! 

[ ![Preparation of chekhchoukhat dfar](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Pr%C3%A9paration-de-chekhchoukhat-dfar.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Pr%C3%A9paration-de-chekhchoukhat-dfar.jpg>)

then without delay the recipe ... 

firstly, we need crumbs of pancakes ............... the pancakes is easy to do, but the crumbs, takes time, hihihihihihihihi, in our habits, we prepare it well in advance, why ? 

because it takes time, because first of all we must prepare thin semolina cakes, more salt and water, which we knead a little, until we obtain a malleable paste, having the consistency bread dough. 

divide this paste into a ball, which is rolled flat, into a very thin slab (less than one centimeter thick) 

the two sides of the pancakes are cooked on a slightly oiled sheet, or on a tadjine. 

crumble the patties with the fingertips to very small pieces, as in the photo 

you're going to tell me, where are the detailed pictures Soulef, I'm spilling you out, I do not have any, because that's my mother's chakhchoukha, that I bring back some ready corn, because we can prepare these crumbs well in advance, which is well dried in the open air, until hardened, and stored in airtight boxes. 

we go to the sauce and it's ingredients are: 

  * chicken (depending on the number of people) 
  * 2 cases of tomato concentrate (or one if you are not too numerous and you will do a small amount) 
  * salt 
  * paprika 
  * black pepper 
  * 1 onion 
  * 2 cloves garlic 
  * 1 case of butter 
  * oil 
  * 1 nice handful of chickpeas 



in a couscoussier (it is preferable), put oil, onion and garlic past the blinder, the tomato paste, the chicken pieces, the salt and the rest of the spices, let simmer a little, then add water, I will say a lot of water. do not forget if the chickpeas are very tender, put them just at the end of the cooking, otherwise you have to put them now. 

boil, put the crumbs of patties in a bowl, and sprinkle with a little of your sauce, let a little imbibe, then put it in the top of the couscoussier, and cook it with steam. 

remove after steam escaping, and sprinkle ink once with a little sauce (which is always entails to cook) 

let still imbibe, and put back in the top of the couscoussier. 

repeat until the crumbs are tender. 

if necessary add water to your sauce, and adjust salt and tomato. 

at the end and just before serving, put the crumbs of cakes in a pot, sprinkle with more sauce (not too much) 

remove from heat and put in a serving dish, garnish with chickpeas, chicken pieces, and a little butter (what do I like the part where there is butter, huuuuuuuuuum) 

[ ![chakhchoukha ed dfar](https://www.amourdecuisine.fr/wp-content/uploads/2010/02/chakhchoukha-ed-dfar.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2010/02/chakhchoukha-ed-dfar.jpg>)   


**chakhchoukat ed dfar-Chicken crumbs with chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/02/chakhchoukha-ed-dfar.jpg)

**Ingredients**

  * chicken red sauce 
  * crumbs of chekhchoukha ed dfar 


