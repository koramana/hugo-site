---
title: Strawberry mousse
date: '2013-02-14'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mousse-aux-fraises_21.jpg
---
![strawberry mousse 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mousse-aux-fraises_21.jpg)

Hello everybody, 

a very good mousse with creamy strawberry, frothy, light, and super melting in the mouth, with this layer of jelly, and these strawberry rings that decorate it, this fresh strawberry mousse and easy to make, is a real pleasure .   
so without delay we go to the recipe: 

  * 400 gr of strawberries] 
  * 120 gr of icing sugar 
  * 2 sheets of gelatin 
  * 50 cl of very cold liquid cream 
  * 1 cup of vanilla coffee 
  * 1 box of strawberry jelly 
  * 5 strawberries cut in slices 



method of preparation: 

  1. Rinse and cut strawberries 
  2. Mix with half of the icing sugar and vanilla 
  3. Soften the gelatin in a bowl of cold water for 10 minutes and wring it in your hands 
  4. Put it in a saucepan with 2 spoonfuls of strawberry purée 
  5. Melt over low heat and add this mixture to the remaining mashed potatoes. 
  6. Whip the cream very cold in chantilly with the remaining sugar and stir in the strawberry purée, mix well together 
  7. Put in 6 small verrines or ramekins 
  8. prepare the jelly as indicated on the jelly box 
  9. decorate your verrines with strawberries cut in a slice, pour over the jelly 
  10. and put them in the refrigerator 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
