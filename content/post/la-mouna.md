---
title: The Mouna
date: '2018-01-06'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/brioche-la-mouna-030.CR2_thumb.jpg
---
##  The Mouna 

Hello everybody, 

the result is sublime, except that I started the brioche too late and between the times of emergence and rest, I was too lazy to braid, especially since it was already 21 pm, so I went to do these snails, hihihiihi .... 

But frankly the kids really like these forms the ... 

**The Mouna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/brioche-la-mouna-030.CR2_thumb.jpg)

Recipe type:  brioche and pastries  portions:  8  Prep time:  45 mins  cooking:  20 mins  total:  1 hour 5 mins 

**Ingredients**

  * 250 ml warm milk 
  * 100 gr of sugar 
  * ¼ teaspoon of salt 
  * 50 ml of oil 
  * 2 eggs 
  * zest of 1 lemon 
  * 500 gr of flour more or less, until you have a nice dough 
  * 1 sachet of instant baker's yeast 
  * 1 teaspoon of butter 
  * decoration: 
  * 1 egg yolk 
  * 1 teaspoon of milk 
  * Pearl sugar. 



**Realization steps**

  1. In a salad bowl, mix the warm milk, yeast, oil, sugar, zest and eggs with the mixer. 
  2. Mix well until the mixture becomes foamy. 
  3. Cover it with a cloth and let it double in a draft-free place (about 1 hour). 
  4. Then add the flour little by little, then knead the good until the resulting dough becomes smooth and elastic (do not need to put the 500 gr of flour) 
  5. Add the knob of butter while continuing to knead. Again, cover the dough and let doubling or even triple for 2 to 3 hours.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/la-mouna-051.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/la-mouna-051.CR2_thumb.jpg>)
  6. Degas the dough then shape 6 balls. 
  7. Divide each of them into 3 branches that you will braid. So you will have 6 braids. 
  8. Take the first and turn there on itself to get a snail and drop it in the center of the mold. Do the same for the 5 others by spacing them all around the one already deposited. (That's the djouza method) when I just made pudding and turned them on themselves to have a snail, but I made 7 snails, one in the middle, and six around. 
  9. Let it swell for 1H30. 
  10. Preheat your oven th. 150 °. 
  11. Before cooking, brush with egg yolk mixed with milk and a spoon of sugar. Decorate with pearl sugar. 



Note You can make 2 buns with these ingredients.   
The most important to succeed this brioche is the rest time. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/la-mouna-lamona.-brioche-algeroise-034.CR2_thumb.jpg)
