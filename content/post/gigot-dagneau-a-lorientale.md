---
title: Leg of lamb in the east
date: '2014-11-27'
categories:
- Algerian cuisine
- diverse cuisine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-roti-a-lorientale-001.jpg
---
[ ![leg of lamb roasted in eastern-001](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-roti-a-lorientale-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-roti-a-lorientale-001.jpg>)

##  **Gigot recipe of oriental lamb**

Hello everybody, 

So here is a recipe for **leg of lamb in the east** you're really going to like it, it's a recipe from my friend Sherine, and I tell you frankly, if you do not eat leg of lamb with oriental cooked in this way, you never eat so of **roast leg** . 

I've already posted recipes I made with my friend Sherine, as [ the rice has the corte or mouloukhiya ](<https://www.amourdecuisine.fr/article-riz-a-la-corete-mouloukhiya.html> "Rice with corn, Mouloukhiya") , [ cucumber and carrot yoghurt salad ](<https://www.amourdecuisine.fr/article-salade-de-concombre-carotte-au-yaourt.html>) , or [ the cornets with taboulé ](<https://www.amourdecuisine.fr/article-amuses-bouches-cornets-au-taboule.html> "amuse bouches, cornets with tabouleh") . 

This time, Sherine used the form [ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.") , to publish his recipe at home, with these pictures taken using his phone. 

If like her, you have recipes that you can share with us, do not hesitate to do like Sherine. 

You can also see my way of cooking a [ leg of lamb roasted in the oven ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-au-four-aid-el-adha-2012-111759550.html> "Gigot D'Baked Lamb")

**Leg of lamb in the east**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-roti-a-lorientale-001.jpg)

portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 2 to 3 leg of lamb, otherwise 1 large cut into 3 pieces 
  * 2 chopped onions 
  * 4 to 5 clove garlic crushed 

Spices in grains: 
  * 1 teaspoon coriander beans 
  * 1 cup of cumin in grain 
  * 1 teaspoon of black peppercorns 
  * 1 cup of cardamom seed 
  * 1 cinnamon stick 
  * 4 cm fresh ginger 

for the yoghurt filling: 
  * 3 tablespoons of plain yoghurt 
  * 1 tablespoon of white vinegar 
  * 1 little salt (just a little) 
  * 1 teaspoon coriander powder 
  * ½ tsp of black pepper 
  * ½ cup of cardamom powder 
  * ¼ cup cinnamon powder 
  * 1 tablespoon of lemon juice 
  * 2 cloves garlic crushed 



**Realization steps**

  1. In a large pot, place the leg of lamb. 
  2. Cover the legs with enough water 
  3. add over chopped onions, and crushed garlic 
  4. add spices: cinnamon, cardamom, black peppercorns, ginger, cumin-grain, coriander-grain, and salt   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-a-lorientales-7.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-a-lorientales-7.jpg>)
  5. cook until the meat is tender 
  6. After cooking, remove the leg of lamb from the sauce, and place on a flat plate so that it cools quickly. 
  7. Prepare the yoghurt filling, mixing all the ingredients. 
  8. place cooked leg quarters in a baking dish 
  9. cover the pieces of leg of lamb with the yoghurt filling, leave 15 minutes   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-a-lorientale-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-a-lorientale-3.jpg>)
  10. then put in the oven, to grill the top of pieces of legs. 
  11. Serve at the end of the oven, with fries or salad. You can serve it with a delicious [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html> "mashed potatoes")



[ ![leg of lamb roasted in the east](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-roti-a-lorientale.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gigot-dagneau-roti-a-lorientale.jpg>)
