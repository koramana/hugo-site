---
title: Cooking couscous with steam
date: '2016-03-19'
categories:
- couscous
- Algerian cuisine
- Dishes and salty recipes
- ramadan recipe
- recipes of feculents
tags:
- Oriental cuisine
- Algeria
- dishes
- Algerian dish
- Ramadan 2016
- Healthy cuisine
- Morocco

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Couscous-a-la-vapeur.jpg
---
[ ![Couscous steamed](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Couscous-a-la-vapeur.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Couscous-a-la-vapeur.jpg>)

##  Cooking couscous with steam 

Hello everybody, 

cooking couscous with steam couscous is a very delicious dish, but before making the sauce that accompanies it, we must start by cooking the couscous grains. 

Over time, one can become professional in cooking steamed couscous, but the beginnings are not easy at all, I do not tell you how it was my couscous the first time I prepared it, and yet There was my mother who was watching me. 

the video: 

First of all, you must be well equipped, because when cooking couscous with steam, you will need a couscous maker, and a "gues3a" or large bowl, which will help you to work the couscous beans without putting the vast majority in the work table. 

you can use this couscous to prepare: 

the [ Chicken couscous ](<https://www.amourdecuisine.fr/article-couscous-au-poulet-103194885.html>) . 

the [ couscous with meat ](<https://www.amourdecuisine.fr/article-25345493.html>) . 

the [ couscous aghmoudh ](<https://www.amourdecuisine.fr/article-25345506.html>) ( or [ couscous with beans ](<https://www.amourdecuisine.fr/article-25345506.html>) ). 

the [ raisins with raisins, or raisins with raisins ](<https://www.amourdecuisine.fr/article-couscous-sucre-seffa-mesfouf.html>) . 

the [ couscous salad with Surimi ](<https://www.amourdecuisine.fr/article-salade-de-couscous-au-surimi-98901237.html>) . 

the [ Cantonese couscous ](<https://www.amourdecuisine.fr/article-couscous-cantonais-couscous-au-wok-85211638.html>) . 

[ couscous bel osbane ](<https://www.amourdecuisine.fr/article-couscous-bel-osbane-96835375.html>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

**Cooking couscous with steam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Couscous-a-la-vapeur-3.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * couscous 
  * salt 
  * water 



**Realization steps**

  1. first, sprinkle the couscous beans well with water, add the salt, and leave to dry for almost 15 min.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cuisson-du-couscous-a-la-vapeur-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cuisson-du-couscous-a-la-vapeur-1_thumb1.jpg>)
  2. When the couscous beans are dry, roll them between your hands wet with oil, roll well until all the grains are separated. 
  3. place the bottom of the couscoussier with enough water on the fire, when the water begins to boil, place the top of the couscoussier, over, and celelle well to not let the vapor escaped from the opening between the bottom Couscoussier and the top of couscoussier.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/comment-cuire-les-grain-de-couscous-a-la-vapeur_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/comment-cuire-les-grain-de-couscous-a-la-vapeur_thumb1.jpg>)
  4. Pour the couscous into the top of the couscous, until the steam appears, let steam for 15 min. 
  5. Put the couscous seeds back in the bowl, add a glass of water. gently, stirring with your hand, or if you can not stand the heat with a wooden spoon. 
  6. Let stand 15 minutes. 
  7. Bake again 20 to 25 min. 
  8. the grains are now softer, when you remove this time from the top of the couscoussier, add a little butter, incorporate it well with couscous, and place in a large salad bowl and cover, until serving with the sauce that you have prepare.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cuisson-du-couscous-a-la-vapeur-3_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cuisson-du-couscous-a-la-vapeur-3_thumb1.jpg>)



![Couscous steamed 3](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Couscous-a-la-vapeur-3.jpg)
