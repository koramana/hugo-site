---
title: gingerbread with dried fruits
date: '2016-12-11'
categories:
- dessert, crumbles and bars
tags:
- Cakes
- desserts
- To taste
- Kitchen without egg
- Kitchen without Milk
- Gingerbread
- Honey

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-aux-fruits-confits.jpg
---
[ ![gingerbread with candied fruits](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-aux-fruits-confits.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-aux-fruits-confits.jpg>)

##  gingerbread with dried fruits 

Hello everybody, 

I admit that I often visit the blog of my friend Isabelle, but I had not spotted this recipe! luckily Lunetoiles realized it and I will not delay to do it, because I really like gingerbread, and according to Lunetoiles and comments on Isca's blog, the recipe is sublime, moreover Lunetoiles again redone and with other dried fruits. And you! will you try it? 

[ ![candied fruit bread with candied fruit 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-qux-fruits-confits-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-qux-fruits-confits-3.jpg>)   


**gingerbread with dried fruits**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-qux-fruits-confits-2.jpg)

portions:  12  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients** For a 24 cm mold 

  * 250g of flour 
  * 300 g of liquid honey (chestnut honey for me) 
  * 15 cl of water 
  * 1 tbsp. curved green anise soup 
  * ½ sachet of baking powder 
  * 1 tbsp. coffee bicarbonate 
  * 100 g raisins 
  * 75 g pistachios 
  * 50 g dried cranberry 
  * a handful of crushed pistachios 



**Realization steps**

  1. Heat the honey with 15 cl of water and let it warm. 
  2. Mix sifted flour with yeast, baking soda and green anise spoon, which corresponds to 15 g. 
  3. In a bowl, pour the honey little by little on the flour while mixing carefully with the manual whisk to avoid lumps. 
  4. Stir in the dried fruits and raisins. 
  5. Pour the dough into a mold lined with a strip of parchment paper. 
  6. Leave to firm for 1 hour before cooking and sprinkle a few grains of sugar. 
  7. Bake in preheated oven at 160 ° C for about 1 hour. Test the cooking with a knife. 
  8. Halfway through cooking, add some crushed pistachios on top. Wait 5 minutes before unmolding.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices.jpg>)
  9. Wait for it to cool completely before removing it from a rack. As soon as it is cold enough to wrap in a cellophane film and leave it well for 2 days. 
  10. Then after 2 days, you can slice it and keep it for a long time and keep it moist, wrap it well in the cellophane film.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-qux-fruits-confits-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-qux-fruits-confits-4.jpg>)



[ ![gingerbread with candied fruits 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-qux-fruits-confits-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-depices-qux-fruits-confits-1.jpg>)
