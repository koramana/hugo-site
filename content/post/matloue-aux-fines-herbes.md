---
title: matloue with herbs
date: '2015-07-15'
categories:
- Algerian cuisine
- Mina el forn
- bread, traditional bread, cake
tags:
- '2015'
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-aux-fines-herbes-3.jpg
---
[ ![matluck with herbs 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-aux-fines-herbes-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-aux-herbes-1.jpg>)

##  Matloue with herbs 

Hello everybody, 

It must be said that homemade bread during Ramadan is a must on the table. It is always appreciated to have [ khobz dar ](<https://www.amourdecuisine.fr/article-pain-maison-facile-khobz-eddar-aux-3-grains.html>) freshly baked, or [ matloue ](<https://www.amourdecuisine.fr/article-matloue-matlou3-matlou-pain-maison-au-tadjine.html>) very hot whose smell embals. However, we women, we like to get out of the crushing routine of always doing the same thing, that's why, we always look for a new recipe for bread or matlou, which will stand out from the one of the day before. 

This time, it's the cuisine of our dear **Oume Tarek** that we come back with a wonderful recipe matloue fine herbs, a matlou full of flavors, and different flavors, then ready for the recipe? 

Do not forget too, like Oume Tarek, you can share your recipes by following this link: [ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette>) . 

**matloue with herbs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-aux-fines-herbes-001.jpg)

portions:  4  Prep time:  45 mins  cooking:  15 mins  total:  1 hour 

**Ingredients**

  * 3 glasses of semolina 
  * 3 glasses of water 
  * ½ tablespoon of salt 
  * 1 tablespoon of sugar 
  * 1 and ½ tablespoon instant yeast 
  * 2 tablespoons of olive oil 
  * 1 tablespoon of chopped parsley 
  * 1 tablespoon chopped fresh mint 
  * 1 cup fresh chopped oregano 
  * 1 tablespoon chopped dill 
  * of flour 



**Realization steps**

  1. Start by grinding the semolina with salt, sugar and yeast and collect with water. 
  2. knead until you have a fluid dough. 
  3. let the dough rest and double the volume 
  4. take it back, and introduce the flour until you have a paste that picks up the well 
  5. Introduce while grinding chopped herbs and olive oil. 
  6. when everything is homogeneous, turn the dough into 4 balls.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-aux-herbes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-aux-herbes-1.jpg>)
  7. let it rise a little, then shape your patties 
  8. let the cakes rise well before cooking on a hot tadjine. 



[ ![matloue with herbs](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-aux-fines-herbes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-aux-fines-herbes.jpg>)
