---
title: 'pina colada: cold drink without alcohol'
date: '2014-06-24'
categories:
- juice and cocktail drinks without alcohol
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Pina-colada-boisson-fraiche-sans-alcool.CR2_.jpg
---
[ ![Pina colada cold drink without alcohol.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Pina-colada-boisson-fraiche-sans-alcool.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Pina-colada-boisson-fraiche-sans-alcool.CR2_.jpg>)

[ ![Pina colada without alcohol.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Pina-colada-sans-alcool.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Pina-colada-sans-alcool.CR2_.jpg>)

##  pina colada: cold drink without alcohol 

Hello everybody, 

I always love to dine at the pina colada, but as this refreshing drink is made with alcohol, I forget the idea of ​​enjoying this refreshing drink, believing that the absence of any of these important ingredients, will change the taste of this drink ... 

Anyway, if in this infernal heat, you're looking for juice and refreshing cocktails, I recommend this table on pinterest: 

**pina colada: cold drink without alcohol**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Pina-colada-sans-alcool.CR2_.jpg)

portions:  2  Prep time:  10 mins  total:  10 mins 

**Ingredients** for 2 small glasses of 180 ml each 

  * 150 ml pineapple juice 
  * 60 ml of orange juice 
  * 90 ml of coconut milk 
  * 2 slices of pineapple cut in small dice. 
  * some peelings of coconut 
  * ice cubes 



**Realization steps**

  1. In a blender place the diced pineapple. 
  2. Pour the pineapple juice (100% natural for me), add the orange juice and the coconut milk 
  3. Pour the ice cubes. 
  4. Operate the blender (do not forget to put the lid loll) 
  5. Serve with a slice of pineapple and coconut peel 



[ ![pina colada without alcohol of Samar.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/pina-colada-sans-alcool-de-Samar.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/pina-colada-sans-alcool-de-Samar.CR2_.jpg>)

Pina Colada 
