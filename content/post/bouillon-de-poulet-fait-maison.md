---
title: Homemade chicken broth
date: '2015-12-29'
categories:
- chicken meat recipes (halal)
- soups and velvets
tags:
- Poultry
- Vegetables
- Based
- Soup
- carrots
- Vegetable soup
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/bouillon-de-poulet-fait-maison-1.jpg
---
[ ![homemade chicken broth 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/bouillon-de-poulet-fait-maison-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/bouillon-de-poulet-fait-maison-1.jpg>)

##  Homemade chicken broth 

Hello everybody, 

In my recipes, I often use chicken broth, which people prefer to replace with chicken broth in cubes. But frankly, when I start reading all the ingredients in this little cube, I'm a little outdated it's E ... I do not know what, and C ... blah, products that we will never reveal their natures . 

So, I prefer myself made my chicken broth, which does not require a lot of ingredients, and in the end you have a broth very good, and especially you know the composition. 

I made you a little video to see how easy it is to make broth, and the most astute in the story is that you do not have to use a whole chicken, or pieces of chicken, even the bones of a chicken will do the trick. As on this picture ... 

[ ![chicken bone for a broth.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/os-de-poulet-pour-un-bouillon.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/os-de-poulet-pour-un-bouillon.bmp.jpg>)   


**Homemade chicken broth**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/bouillon-de-poulet-fait-maison.jpg)

portions:  2  Prep time:  5 mins  cooking:  60 mins  total:  1 hour 5 mins 

**Ingredients**

  * 2 l of water 
  * 1 whole chicken, pieces or chicken bones 
  * 1 onion 
  * 2 carrots 
  * 1 teaspoon of black peppercorns 
  * 2 bay leaves 
  * 1 little salt 
  * 1 small bunch of parsley 
  * 1 small bunch of coriander 



**Realization steps**

  1. place the ingredients in the pot 
  2. cover well with water 
  3. and boil for at least 1 hour on medium heat 
  4. remove all the ingredients 
  5. pass the broth to the Chinese 
  6. reserve the cooled broth in jars for immediate use (you can preserve it for two days, otherwise place in sachets in the freezer for later use. 



{{< youtube t7ApKaea2RI >}} 
