---
title: eggs with milk
date: '2017-01-12'
categories:
- dessert, crumbles et barres
- panna cotta, flan, et yaourt
- recette de ramadan
- recettes sucrees
tags:
- desserts
- Ramadan
- Ramadan 2017
- Easy cooking
- Algeria
- Algerian cakes
- Caramel

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/oeufs-au-lait-2.jpg
---
![eggs with milk 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/oeufs-au-lait-2.jpg)

##  eggs with milk 

Hello everybody, 

My favorite dessert, homemade milk eggs ... A dessert that I often prepared with my mother when I was young, especially during Ramadan. 

This egg custard, or egg au lait is super very tasty, very unctuous, easy to make, and especially no need to stand in front of the stove and mix constantly so that it does not stick to the bottom. In addition to the bonus, I put you a video of the preparation of this dessert without reading too much ... 

then to you the recipe for this inratable dessert, timeless and indestrônable. 

**eggs with milk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/oeufs-au-lait-3.jpg)

portions:  3  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 2 egg yolks 
  * 1 whole egg 
  * 250 ml of milk 
  * 25 gr of powdered sugar 
  * 1 vanilla pod 

for caramel: 
  * 50 gr of sugar 
  * the juice of a lemon 



**Realization steps**

  1. Turn on the oven at 170 ° C 
  2. Pour the milk into a saucepan. 
  3. base the vanilla pod in half lengthwise. With a sharp knife, scrape the small seeds and put them in the milk. Add the remaining vanilla pod. Heat on low heat, remove from heat just before boiling. 
  4. Meanwhile, place the egg yolks and the egg in a large bowl, add the sugar, whip vigorously for 2 to 3 minutes until the mixture is frothy. 
  5. Remove the pieces of vanilla bean from the milk. Pour a small amount of milk over the eggs, stirring quickly to avoid cooking. Continue to pour the milk gradually without stopping. 
  6. When all the milk is incorporated turn another few seconds. 
  7. Put the sugar over medium heat just to give it a slightly golden color 
  8. add the lemon juice, mix a little and fill the glasses 
  9. pour the liquid device into the verrines, and place the verrines in a baking tin. 
  10. add a little warm water in the pan for a bain-marie cooking. 
  11. cook between 25 and 30 minutes (or depending on your oven) 
  12. At the end of cooking the texture of the custard is well fixed. to check the cooking, plant a knife in the middle, the blade must come out without trace of liquid. 
  13. let the verrines cool well before placing them in a cool place. 
  14. Take out just before serving. 



![eggs with milk 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/oeufs-au-lait-1.jpg)

Source: livre « Nature, simple, sain et bon » publié aux Éditions Alain Ducasse 
