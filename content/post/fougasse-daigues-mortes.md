---
title: fougasse of Aigues-mortes
date: '2018-03-09'
categories:
- Buns and pastries
tags:
- buns
- Boulange
- Bakery
- Pastries
- Milk bread
- To taste
- Breakfast

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte-1.jpg
---
[ ![fougasse of Aigues-mortes 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte-1.jpg>)

##  fougasse of Aigues-mortes 

Hello everybody, 

I have not shared with you a recipe for breads or buns for a while now, and yet at home, I do it every day. Sometimes it is not easy to take pictures, especially if you are a little late and it is already time to sit down to eat. 

Today is a baking recipe from the kitchen of **Lunetoiles** . She realized to share with us this brioche called: Aigues-mortes fougasse. When I received Lunetoiles' email and read his title I thought it was a fish foccacia recipe, well, I was completely next to the plate, hihihihi. 

The fougasse Aigues-mortes is a brioche from the Gard region in France, it is the region where lives Lunetoiles, so it is a recipe of a French commune of this region named Aigues-mortes. This recipe Lunetoiles found it on the forum **lil happiness pastry chef.**

[ ![fougasse of Aigues-mortes 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte-3.jpg>)   


**fougasse of Aigues-mortes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte-5.jpg)

portions:  8  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** for a round fougasse 

  * 250g of flour 
  * ½ c. dehydrated baker's yeast (briochin brand alsa) 
  * ½ cuil. salt 
  * 4 c. melted butter 
  * 4 c. tablespoons sugar 
  * 2 tbsp. orange blossom water 
  * 125 ml warm milk 
  * 1 egg 
  * 75 g of tangzhong (it's an addition that is mine) 

Garnish : 
  * 30 gr of sugar 
  * 30 gr of soft butter 
  * Orange tree Flower water 

For the tangzhong: 
  * 62 ml of milk 
  * 62 ml of water 
  * 25 gr of flour 



**Realization steps** Prepare the tangzhong in advance so that it is time to warm up: 

  1. in a small saucepan heat all ingredients (62 g milk + 62 g water and 25 g flour) in a saucepan over low heat, stirring constantly until it begins to thicken. When when you stir, the whisk leaves traces in the mixture, it's ready. Get out of the fire. 
  2. Let cool. 

For the fougasse dough: 
  1. Put all the ingredients in the bowl of your knead (yeast, flour, tangzhong, melted butter, egg, sugar, salt, warm milk, orange blossom water) in the bowl then rotate with a hook. 
  2. Let knead well for 10 minutes until you get a well elastic ball that adheres to the hook. 
  3. Remove the dough from the bowl of the kneader, cover the dough with a cloth or a plastic wrap and let it grow in a warm place or near a heating between 1h30 and 3h (or until the double dough volume) 
  4. Cut out a sheet of parchment paper the size of your round cake pan. 
  5. Degas the dough and spread the dough by hand in the round pan. 
  6. Using the handle of a wooden spoon, make holes in the dough everywhere. 
  7. Let stand for a good hour, always covered with a damp cloth or baking paper. 
  8. Cut the 30 g butter into small pieces and push them with your thumb all over the holes that were made in the dough. 
  9. Sprinkle the dough with some orange blossom water plugs evenly. 
  10. Sprinkle with 30 g sugar. 
  11. Bake at 180 ° C for about 20 minutes depending on your oven. 
  12. The top must be golden.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte-4.jpg>)
  13. once it is cooked and comes out of the oven 
  14. Normally it is necessary to spray it with orange blossom water between 2 or 3 spoons according to taste 
  15. with a vaporizer evenly and some even sprinkle with powdered sugar right after. She will not be too sweet so do not worry, the cake will be just right. 
  16. Enjoy tepid or cold with coffee, afternoon tea or breakfast 



[ ![fougasse of Aigues-mortes](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fougasse-dAigues-morte.jpg>)
