---
title: aubergine stuffed in the oven
date: '2014-09-15'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/karniyarik-aubergines-farcies_thumb1.jpg
---
![karniyarik, stuffed aubergines](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/karniyarik-aubergines-farcies_thumb1.jpg)

##  aubergine stuffed in the oven 

Hello everybody, 

How do you like to eat your eggplants? you love them in salad, as in the recipe of [ baba ghanouj ](<https://www.amourdecuisine.fr/article-baba-ghanouj.html> "baba ghanouj") , you like them gratin, like this recipe [ aubergine gratin ](<https://www.amourdecuisine.fr/article-recette-de-gratin-daubergines.html> "aubergine gratin recipe") who is a real delice? maybe you like them as a starter? I do not tell you how much these [ rolled eggplants ](<https://www.amourdecuisine.fr/article-amuses-bouches-roules-aux-aubergines.html> "appetizer / aubergine rolls") are delicious and melting, or these [ eggplant fritters ](<https://www.amourdecuisine.fr/article-beignets-d-aubergines-au-basilic.html> "eggplant donuts with basil") … a delight. 

This time, I share with you another version of aubergine gratin, or rather stuffed aubergine, this super delicious Turkish recipe, known as ** karnıyarık  ** which simply means ** Stuffed eggplant  ** has minced meat. 

The method of preparation of this recipe is very different from what I used to do, which made me want to try it, and I do not tell you what it was good, the kids and I, we really like it. 

**aubergine stuffed in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/aubergines-farcies_thumb1.jpg)

portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 4 medium eggplants 
  * 4 peeled tomatoes 
  * 200 gr of minced meat (I did it at random, I do not remember exactly how much to stuff the 4 aubergines) 
  * ½ onion 
  * 2 cloves garlic 
  * 3 tablespoons of oil 
  * some sprig of chopped parsley 
  * 1 tablespoon of tomato paste 
  * salt, black pepper, and a pinch of coriander powder. 



**Realization steps**

  1. wash and cut the eggplant ends. 
  2. Peel the skin in strips once in two, and make a split in the middle. 
  3. sprinkle with salt, and let drain in a colander, for almost 30 min, they will disgorge a little water. 
  4. wipe with paper towel, and put some oil around. 
  5. cover with foil, in foil. 
  6. bake for 30 minutes at 180 ° C. 
  7. prepare the stuffing, in a pan put some oil and cook the onion cut until it becomes translucent. 
  8. add the minced meat, cook well then add the chopped parsley, salt and pepper to taste. (pay attention to salt) 
  9. In the meantime, prepare a tomato sauce, cook the tomato cut into cubes, in a little oil, add the crushed garlic, salt and pepper, and let cook until reduced water. 
  10. In a baking dish put the tomato sauce. then place the eggplants and stuff them with the minced meat. 
  11. bake for 15 minutes in preheated oven at 180 ° C 



![karnıyarık](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/karniyarik_thumb1.jpg)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
