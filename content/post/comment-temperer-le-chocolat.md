---
title: How to temper the chocolate
date: '2015-12-09'
categories:
- recettes patissieres de base
tags:
- Confectionery
- Gourmet Gifts
- Delicacies
- delicacies
- Easy recipe
- Without cooking
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-temp%C3%A9r%C3%A9.jpg
---
[ ![tempered chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-temp%C3%A9r%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-temp%C3%A9r%C3%A9.jpg>)

Hello everybody, 

To have a chocolate with a shiny and crunchy appearance, it is very important to respect the tempering of the chocolate when it melts, because an untempered chocolate is immediately less pretty, bland despite it still goes to be delicious.   
In fact everything is played on the crystallization of cocoa butter contained in chocolate to give it a stable shape and to be able to work it more easily. For this, it must be made liquid, cool and reheat to reach its ideal temperature of work. The secret is to properly monitor the temperature. A probe thermometer is therefore very important for this operation. 

1 / For dark chocolate.   
Place the chocolate in a bain-marie and melt while stirring until it reaches a temperature between 50/55 ° C. Remove from heat and let cool while stirring regularly until it reaches 27/28 ° C. Once the chocolate has cooled, return it to the water bath for a few minutes until it is at its crystallization temperature and therefore working, 30/32 ° C. 

2 / For milk chocolate.   
Place the chocolate in a bain-marie and melt while stirring until it reaches a temperature between 45/50 ° C. Remove from heat and let cool while stirring regularly until it reaches 26/28 ° C. Once the chocolate has cooled, return it to the water bath for a few minutes until it is at its crystallization temperature and therefore working, 29/30 ° C. 

3 / For white chocolate.   
Place the chocolate in a bain-marie and melt while stirring until it reaches a temperature of 40 ° C. Remove from heat and let cool while stirring regularly until it reaches 26/27 ° C. Once the chocolate has cooled, return it to the water bath for a few minutes until it is at its crystallization temperature and therefore working, 27/29 ° C.   


**How to temper the chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-temp%C3%A9r%C3%A9.jpg)

Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * Dark chocolate, or white or milk chocolate 
  * probe thermometer. 



**Realization steps**

  1. / For dark chocolate. 
  2. Place the chocolate in a bain-marie and melt while stirring until it reaches a temperature between 50/55 ° C. Remove from heat and let cool while stirring regularly until it reaches 27/28 ° C. Once the chocolate has cooled, return it to the water bath for a few minutes until it is at its crystallization temperature and therefore working, 30/32 ° C. 
  3. / For milk chocolate. 
  4. Place the chocolate in a bain-marie and melt while stirring until it reaches a temperature between 45/50 ° C. Remove from heat and let cool while stirring regularly until it reaches 26/28 ° C. Once the chocolate has cooled, return it to the water bath for a few minutes until it is at its crystallization temperature and therefore working, 29/30 ° C. 
  5. / For white chocolate. 
  6. Place the chocolate in a bain-marie and melt while stirring until it reaches a temperature of 40 ° C. Remove from heat and let cool while stirring regularly until it reaches 26/27 ° C. Once the chocolate has cooled, return it to the water bath for a few minutes until it is at its crystallization temperature and therefore working, 27/29 ° C. 


