---
title: Stuffed sardines
date: '2014-09-04'
categories:
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/sardines-farcies-.CR2_1.jpg
---
![sardine-stuffed-.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/sardines-farcies-.CR2_1.jpg)

##  Stuffed sardines 

Hello everybody, 

Many do not like to prepare or fry sardines, to avoid these strong smells in the house, especially when it is cold, and that we do not really want to open the window .... 

I think sometimes like that, but when I remember that summer taste in the mouth, and that tickling smell that awakens all the senses, when we pass the chermoula mortar, to stuff these sardines. 

When one remembers the crispy taste of the skin delicately softened by a few drops of freshly squeezed lemon, which precedes the mellifluous infinity of the sardine and finally this tangy flavor, piquant of the stuffing ... we forget everything, and we run quickly on the first fishmonger, to buy these beautiful fresh sardines, and hop to the kitchen ...   


**Stuffed sardines**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/sardines-farcies-006.CR2_.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 500 gr of sardines 
  * 1 medium onion 
  * 3 shallots 
  * 5 cloves of garlic 
  * salt, black pepper and cumin 
  * 2 cm fresh ginger, otherwise ½ teaspoon powdered ginger 
  * 1 tablespoon of tomato paste 
  * 1 cup of harissa coffee (more or less according to your taste) 

for frying: 
  * of flour 
  * oil. 



**Realization steps**

  1. method of preparation: 
  2. Prepare the sardines, empty, remove the head, the central ridge and the tail of the sardines. 
  3. Wash them and dry them with paper towels. Arrange them flat on a tray, skin side underneath, salt them very lightly and reserve them in the refrigerator. 
  4. Prepare the chermoula by crushing the rest of the ingredients in a mortar. 
  5. Take the sardines out of the fridge. pair up trying to collect the sardines two by two. 
  6. Spread the stuffing on one of the two sardines, and cover each stuffed sardine with the second sardine, as if to make a sandwich. 
  7. In a large skillet, heat oil for frying. 
  8. Pour the flour on a sheet of aluminum foil. 
  9. Flour each stuffed sardine on both sides with care. Tap to remove excess flour. 
  10. Fry stuffed sardines in oil 2 minutes per side. 
  11. enjoy with a fresh salad and a lemon fillet. 



![sardine-stuffed-to-the-chermoula.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/sardines-farcies-a-la-chermoula.jpg)
