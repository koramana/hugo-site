---
title: pineapple mango and almond smoothies
date: '2018-05-07'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-mangue-ananas-et-amandes.CR2_.jpg
---
[ ![pineapple and almond mango smoothies.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-mangue-ananas-et-amandes.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-mangue-ananas-et-amandes.CR2_.jpg>)

##  Pineapple mango and almond smoothies 

Hello everybody, 

What do I like mango, especially when it is tender, juicy and sweet wish. Sometimes I buy green mangoes to let them rest and mature at home, already my daughter when she sees them in the fruit basket, she turns like a vulture around, I'm asked to give her some ... 

When it's too green, I hide them from his sight so that it ripens, and I forget them ... As is the case for these two mangoes that I completely forget, and the day I found them, they were so old that I could not cut them. 

The only solution is to make a smoothie, I especially benefit because the baby sleeps, and I can take the photos without problem. Because yes, the difficulty is not in the preparation of the smoothie, but rather when I bring out my camera, and I start to prepare the landscape, to make a beautiful photo, it takes me easily 30 minutes to prepare the landscape, find the right light, and then try to make beautiful pictures ... Finally I'm not going to say that I make beautiful pictures, lol ... especially since the arrival of the baby, I do not have too much mind to work properly behind my camera ... 

So, come back to this pineapple and almond mango smoothie, perfumed with a fillet of caramel syrup, that I recently bought in Lidl, but that you can easily replace with honey, this smoothie is a real moment of pleasure especially if you do not like me, and you plan to do it at least 3 hours before, and have time to put the fruit a little in the freezer, to have a super refreshing smoothie. 

On the blog you can find even more [ smoothie recipes ](<https://www.amourdecuisine.fr/article-tag/recettes-de-smoothies>) [ ](<https://www.amourdecuisine.fr/article-tag/recettes-de-smoothies>)   


**Pineapple mango and almond smoothies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-mangue-ananas-et-amandes-1.jpg)

portions:  2  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 fresh mangoes cut in pieces 
  * ½ tin of pineapple pieces 
  * 1 handful of uncut almonds 
  * 4 tablespoons natural yoghurt 
  * 4 ice cubes 
  * 1 tablespoon of caramel syrup or honey 



**Realization steps**

  1. Prepare the fruits in advance and place them in the freezer for at least 2 hours before preparing the smoothie. 
  2. place your ingredients in the blonder or smoothie machine, depending on their hardness,   
that is to say if it is in a blinder, first put honey, yoghurt, mango, pineapple, ice cubes and towards the end almonds. If it is in the smoothie machine, which reverses when you place it in the appliance, start with the almonds, and the other ingredients, in the opposite order. 
  3. turn on the unit for at least 2 minutes. 
  4. Serve quickly, as long as it is cool. 



[ ![pineapple mango and almond smoothies.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-mangue-ananas-et-amandes.CR2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-mangue-ananas-et-amandes.CR2-001.jpg>)

I hope you'll be lucky to try this delicious pineapple and almond mango smoothie. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
