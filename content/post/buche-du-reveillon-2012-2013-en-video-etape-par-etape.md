---
title: Christmas Eve 2012/2013, in step by step video
date: '2012-12-26'
categories:
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/biscuit-roule-buche-de-noel-au-chocolat.CR2_thumb4.jpg
---
Hello everybody, 

here is a Christmas bauble, with a chocolate icing (a chocolate mirror) that I made to order for a friend .... 

so sorry if I do not have a cup of the log, my friend promised me to take pictures, but the fact that it will be cut after the party, I do not think she will remember (kiss Emelie ... .. it does not matter) 

so I come back to this log, I made you a video still live, during the preparation of the biscuit rolled, during the preparation of the cream with mascarpone and caramelized apples, during the preparation of the chocolate icing and during the decoration… 

I apologize for the quality of the video, I learn, plus I do not have a camcorder of good quality ... 

but it's really sublime to make videos, I start to like the thing, despite that it takes the lead for the staging (hihihihih) and the final editing 

**Christmas Eve 2012/2013, in step by step video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/biscuit-roule-buche-de-noel-au-chocolat.CR2_thumb4.jpg)

Recipe type:  dessert, Christmas log  portions:  8  Prep time:  60 mins  cooking:  35 mins  total:  1 hour 35 mins 

**Ingredients** for the rolled biscuit 

  * 5 eggs (for a tray of 30 cm by 40cm) 
  * 140 grams of sugar 
  * 150 grams of flour 
  * 1 cup of vanilla coffee 
  * 1 cup of baking powder 
  * 1 pinch of salt 
  * 2 drops of vinegar (to avoid the smell of eggs) 

mascarpone cream: 
  * 250 gr of mascarpone 
  * 50 gr of sugar + 4 tablespoons to caramelise the apples 
  * 2 medium sized apples 
  * 1 handful of tapered almonds 
  * 2 tablespoons fresh cream 

for chocolate icing: 
  * 103 g of sugar 
  * 37 g of water 
  * 35 g of unsweetened cocoa powder 
  * 71 g of liquid cream (35% of MG) 
  * 4 g of gelatin in foil   
I made this icing twice to have a great mirror on the roll. 

final decoration: 
  * dark chocolate 
  * shiny food gold. 



**Realization steps** preparation of rolled biscuit: 

  1. cover the tray with a baking paper, and butter it a little, and place it on the side (you have to prepare everything in advance). 
  2. preheat the oven to 180 degrees C 
  3. start by beating the eggs and the foam sugar, to have a good foamy mixture, if you use the whip of a kneader, it will give a beautiful foam, otherwise if you use an electric whip like mine, tilt the whip as in the photo to aerate the mixture well, do it for 8 to 10 min, until the egg sugar mixture, triple volume. 
  4. add vanilla and vinegar. 
  5. stir in the flour and baking powder mixture, gently spoon per spoon, without breaking the foamy egg mixture. 
  6. mix with a spatula while aerating the mixture. 
  7. pour the mixture gently on the tray 
  8. bake for 10 to 15 minutes 
  9. remove from the oven and allow to cool completely (to have a good roll do not allow to cook too much) 
  10. prepare a piece of aluminum foil, very wide, and after total cooling of the cake roll it in aluminum. and leave out.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/biscuit-roule-002_thumb2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/biscuit-roule-002_thumb2.jpg>)

preparation of the cream mascarpone / caramelized apples: 
  1. wash and peel the apples 
  2. remove the seeds and cut the apples into small cubes. 
  3. in a skillet, caramelize 4 tablespoons sugar until it becomes a golden liquid (do not let it burn) 
  4. add the apple cubes, turn so that the apples are well caramelized. 
  5. add the cream and mix ... let it cool down. 
  6. beat the mascarpone with the sugar, until the sugar melts completely and it gives a nice cream. 
  7. introduce the caramelized apple mixture, mix. 
  8. roll out the rolled biscuit, garnish with mascarpone cream, roll again the cake, do not press too hard not to let out all the cream. place in the fridge.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/buche-au-chocolat.CR2_thumb2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/buche-au-chocolat.CR2_thumb2.jpg>)

preparation of the chocolate mirror: 
  1. dip the gelatin in cold water. 
  2. Bring the water and sugar to the boil and leave the surroundings. 3 minutes. 
  3. In another saucepan, heat the cream. 
  4. Remove the syrup from the heat and add the cocoa powder and mix. 
  5. Pour the cream before boiling and mix again. 
  6. Add the gelatin softened and well wrung out. 

garnish: 
  1. remove the roll of fresh, place it on a support (for my part I put the roll on a sheet of polystyrene, and I then place on 2 glasses of tea) 
  2. place all this in a tray topping with baking paper (to catch the flowing icing, and re-decorate the cake again with it) 
  3. repeat the process until you have a uniform and smooth seal. (I had to prepare the icing twice, to cover the whole log well) 
  4. now melt the chocolate in a bain-marie, spread it over a piece of baking paper and let it set.   
if you have shiny food, color with a brush the chocolate, and garnish the log according to your taste: 



{{< youtube HVOT_gZiZog >}} 
