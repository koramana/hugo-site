---
title: awama or lokmat el kadi
date: '2017-05-07'
categories:
- Algerian cakes with honey
tags:
- Algerian cakes
- Algeria
- Algerian patisserie
- delicacies
- Aid
- Ramadan 2017
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/awama-lokmet-el-kadi.jpg
---
![awama lokmet el kadi](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/awama-lokmet-el-kadi.jpg)

##  Awama or lokmat el kadi 

Hello everybody, 

Here is a delicious oriental pastry which graciously finds its place in our table of Ramadan, Awama or lokmat el kadi. Small balls of well-round dough fried in an oil bath and passed with honey to accompany a good glass of tea flavored with mint ... I'm already hungry, and I wait for the time of the ftour to taste them, yum yum. 

Today's recipe is the recipe of one of my readers and friend of Tema's kitchen ... who has already published on my blog the [ shortbread with lemon ](<https://www.amourdecuisine.fr/article-sables-au-citron.html>) , the [ rolled with lemon ](<https://www.amourdecuisine.fr/article-biscuit-roule-au-citron.html>) and the [ tartlets with cream djouzia ](<https://www.amourdecuisine.fr/article-tartelettes-a-la-creme-djouzia-djawzia.html>) . 

**awama or lokmat el kadi**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/awama-lokmat-el-kadi.jpg)

portions:  30  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 1 glass and a half flour. 
  * 1 tablespoon cornflour. 
  * 1 tablespoon of oil. 
  * 1 tablespoon of sugar. 
  * 1 tablespoon of baker's yeast. 
  * pinch of salt. 
  * 1 package of vanilla. 
  * 1 glass of warm water. 
  * cooking oil 
  * Syrup 



**Realization steps**

  1. in the bowl of the mixer or mixer, or simply in a container pour the ingredients and mix everything. 
  2. cover the mixture and let rise 1h. 
  3. put the oil in a pan and heat it. 
  4. put in a small cup a little oil and put in a teaspoon. 
  5. degas the dough and grease the hand with a little oil. take a little of the dough. Press on the dough 
  6. a small ball will come out between the thumb and the index finger. grab it with the teaspoon already soaked in oil and dip the ball in hot oil. 
  7. -Fill the pan with the balls and once the kimps are golden brown remove them and immerse them immediately in honey 
  8. -drain limests and decorate with pistachio or sesame seeds ... 



[ ![awamatte loqmat el kadi](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/awamatte-loqmat-el-kadi-.jpg) ](<https://www.amourdecuisine.fr/article-awama-ou-lokmat-el-kadi.html/awamatte-loqmat-el-kadi>)
