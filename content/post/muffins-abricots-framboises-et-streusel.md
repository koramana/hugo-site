---
title: raspberry and streusel apricot muffins
date: '2013-05-31'
categories:
- cheesecakes and tiramisus
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/muffins-aux-abricots-et-framboises1.jpg
---
![muffin-with-apricot-and-framboises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/muffins-aux-abricots-et-framboises1.jpg)

## 

Hello everybody, 

Another recipe to taste it, especially that now the days are prolonged, and that the children or even the gourmands like me, would like to put under the tooth a pleasant sweetness, like these muffins which burst in mouth all the perfumes that one can like. 

These muffins with apricots, raspberries and streusel are a revisited version of my [ blueberry muffins ](<https://www.amourdecuisine.fr/article-muffins-aux-myrtilles-recette-de-muffins-extra-moelleux-115984796.html>) , that Lunetoiles realized, and that she was very successful. 

**raspberry and streusel apricot muffins**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/Muffins-framboise-abricots-streusel1.jpg)

portions:  12  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients** For the crumble: 

  * 90 gr of flour 
  * 2 tablespoons brown sugar 
  * 2 tablespoons sugar 
  * 4 tablespoons of almond puree 
  * vanilla extract 
  * a pinch of salt 

for muffins: 
  * 250 gr of flour 
  * 2 tablespoons of baking powder 
  * 1 cup of baking soda 
  * 1 pinch of salt 
  * 2 eggs 
  * 120 ml of buttermilk 
  * 100 gr of brown sugar 
  * 1 teaspoon of vanilla extract 
  * the zest of 1/2 lemon 
  * 100 gr of oil 
  * 6 dried apricots, 
  * 60 gr frozen raspberries 

For the deco: 
  * Oat flakes. 



**Realization steps**

  1. dip the apricots in water at least 24 hours in advance 
  2. prepare the streusel, mix all the ingredients with the fork, and leave to rest on the side. add the oat flakes. 
  3. preheat the oven to 200 degrees C, and place the trays in the muffin cups 
  4. in a bowl, sift flour, baking powder, baking soda and salt. 
  5. in a bowl, whisk the egg, add the two sugar and mix. 
  6. incorporate buttermilk, lemon zest, and vanilla extract. 
  7. then add the oil. 
  8. introduce the flour mixture 3 times into the liquid mixture. 
  9. add the drained and cut apricots, as well as the frozen raspberries, just to disperse them in the muffin dough. 
  10. fill the boxes. 
  11. sprinkle the streusel generously on top. 
  12. bake for 10 minutes at 200 degrees C, lower the temperature to 180 degrees, and cook another 15 minutes, you can check the cooking with a toothpick, if you think you need 5 minutes more, do it. 


