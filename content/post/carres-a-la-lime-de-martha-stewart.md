---
title: squares with Martha Stewart's lime
date: '2017-02-28'
categories:
- dessert, crumbles and bars
- sweet recipes
tags:
- Easy cooking
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/carr%C3%A9s-%C3%A0-la-lime-de-Martha-Stewart-1.jpg
---
![squares with the lime of Martha Stewart 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/carr%C3%A9s-%C3%A0-la-lime-de-Martha-Stewart-1.jpg)

##  squares with Martha Stewart's lime 

Hello everybody, 

This Lunetoiles Martha Stewart Lime Squares Recipe is on my email for over a year and a half! When Lunetoiles had passed me the album of these pretty squares with the file of Martha Stewart, I thought that it is exactly the same recipe that I had on my blog. 

Yes, because on my blog I already have two recipes of lemon squares: the recipe of my [ lemon squares ](<https://www.amourdecuisine.fr/article-carres-citronnes.html>) , and another Lunetoiles recipe [ the lemon squares ](<https://www.amourdecuisine.fr/article-carres-au-citron.html>) . But when Lunetoiles told me yesterday that I forgot to post this recipe, I was going to tell him that I already had two recipes on the blog, but reading the recipe, I found that it is very different, especially with the presence of sweetened condensed milk ... By reading the ingredients, I really want to make this recipe. 

**squares with Martha Stewart's lime**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/carr%C3%A9s-%C3%A0-la-lime-de-Martha-Stewart.jpg)

portions:  16  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients** For the sanded base: 

  * 200 gr small butter finely crushed breadcrumbs (or other biscuits) 
  * ⅓ cup of powdered sugar 
  * 50 gr of melted butter 

Lemon filling: 
  * 3 large egg yolks 
  * The grated rind of 2 lemons + lime 
  * ⅓ cup fresh lime juice (80 ml) 
  * ⅓ cup fresh lemon juice (80 ml) 
  * 1 can of sweetened condensed milk (397 gr) 

Decor : 
  * ½ lemon, finely sliced ​​in half-moons (optional) 
  * !WHIPPED CREAM : 
  * 60 ml whole cream with 30% fat 
  * 1 - 2 tablespoons icing sugar 



**Realization steps** LEMON LIME BARS 

  1. The day before: Place the bowl of the electric mixer and the whisk in the freezer, to make the whipped cream more easily. 
  2. Heat the oven to 175 degrees Celcius. 
  3. Jack a 20 cm square baking pan of parchment paper leaving excess leaf hanging over two opposite sides for easy removal. 
  4. In a small bowl, mix together the breadcrumbs of biscuits, sugar and butter. 
  5. Press evenly into the bottom of the prepared baking pan. 
  6. Note: Use the back of a measuring cup to press firmly. 
  7. Bake for 10 minutes or until light brown and golden brown. 
  8. Remove from oven and cool on a wire rack. 
  9. Leave the oven on. 
  10. In a medium bowl with electric mixer, whisk egg yolks, lemon zest at high speed until thick, about 5 minutes. 
  11. Reduce the speed to medium. Add the condensed milk in a slow and steady stream, continue to mix. 
  12. Increase the speed to high and mix until thick, about 3 minutes. Reduce speed. Add both lemon and lime juice and continue mixing until just everything is amalgamated. 
  13. Spread the filling evenly over the crust. 
  14. Bake for 10-15 minutes. Cool completely on a wire rack. Refrigerate at least 4 hours or overnight. 
  15. Cut into squares. 

WHIPPED CREAM 
  1. In a large cooled bowl (I placed the mixer bowl and the whisk in the freezer the day before), beat the whole cream and 1 tablespoon of icing sugar together until soft peaks form. Taste, add an extra tablespoon of icing sugar if necessary and continue beating until stiff peaks form. 
  2. Place a spoonful of whipped cream on each bar or fill a piping bag with a fluted socket. 
  3. Decorate with lemon peel, and half-moons of lemon on the whipped cream. 



![squares with lime from Martha Stewart 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/carr%C3%A9s-%C3%A0-la-lime-de-Martha-Stewart-2.jpg)
