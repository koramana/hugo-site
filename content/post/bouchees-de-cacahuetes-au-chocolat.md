---
title: chocolate peanut bites
date: '2012-12-08'
categories:
- fried Algerian cakes
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/Zellij-mignardise-2012_thumb.jpg
---
##  chocolate peanut bites 

Hello everybody, 

when my friend Lunetoiles gave me her wonderful Rkhama recipe ( [ cake without cooking ](<https://www.amourdecuisine.fr/gateaux-algeriens-sans-cuisson>) ) I published the past week, I was waiting for the first opportunity to make this pretty confectionery, it must be said that the ingredients are at the door of everyone, so just find the time and opportunity to do it. 

so before yesterday, my friends who were looking for an opportunity to come take the tea at home (is not it girls ???? ... .. they know each other, and I hear them laugh in their corners .... that you are !!!!) So, for them, the fact that I passed my exam (in addition it is only the beginning ... .. I wonder what will wait for me when I will have the license , maybe it's going to be a dinner ...... I have to prepare, hihihihiih ....) I have to celebrate this success with them, it's okay, because when we are surrounded and we share a joy it is already a beautiful thing. 

so for tea, I opt for the easy of the easy, this mignardise:  nougat with peanuts and sesame seeds, covered with a beautiful layer of white marble chocolate  ............... .hum, and not that, I had to drip the thousand leaves, this sublime French pastry that they never had the chance to drip. 

#  [ thousand leaves in cafe ](<https://www.amourdecuisine.fr/article-41158909.html> "Thousand leafs with fondant in cafe")

.................. .. hummmmmmmmmmmmm .... recipe already on the blog, 

the girls really love these two delights. 

for the Nougat recipe, I did not change the recipe except to put white chocolate as it was indicated on the original recipe. 

the result was sublime, so if you like this pretty confectionery to you to see the recipe:   


**chocolate peanut bites**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/Zellij-mignardise-2012_thumb.jpg)

Recipe type:  desset, cake without cooking  portions:  30  Prep time:  20 mins  cooking:  2 mins  total:  22 mins 

**Ingredients**

  * 1 kg roasted and mixed peanuts 
  * 1 bowl of grilled sesame seeds 
  * 1 teaspoon cinnamon 
  * 400 g of white chocolate (dark chocolate here) 
  * 400 g of milk chocolate 
  * honey (to pick up the peanut paste) 



**Realization steps**

  1. Melt dark chocolate in a bain-marie Mix peanuts, sesame, cinnamon, and honey until you can form a ball, 
  2. cover a worktop with a sheet of food film, 
  3. put your ball on it, then cover it with another sheet of food film, 
  4. spread out with a baking roll, then put this dough in a mold (a tray, a fried noodle, depending on the width and length obtained) 
  5. remove the first sheet of the cling film, and apply the first layer of melted chocolate 
  6. To go faster, let this chocolate layer in the refrigerator, 
  7. during this time, melt the milk chocolate in a bain marie, and a little dark chocolate aside, and fill a cone for decoration 
  8. cover the other side with milk chocolate, and start decorating 
  9. With dark chocolate, draw parallel lines on the layer of milk chocolate, 
  10. then with the help of a knife draw lines perpendicular to the first lines drawn, to make mottles 
  11. let take, then cut the cupcakes according to the desired size and shape 



{{< youtube DK06r16 >}} 

merci mes amies pour la visites, passez une belle journee et a la prochaine recette 
