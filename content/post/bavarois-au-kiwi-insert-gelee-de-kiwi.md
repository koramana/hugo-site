---
title: kiwi bavarian insert kiwi jelly
date: '2016-03-04'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- sweet recipes
tags:
- Dessert
- Algerian cakes
- To taste
- Cakes
- desserts
- Pastry
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bavarois-au-kiwi-insert-gel%C3%A9e-de-kiwi-.jpg
---
[ ![kiwi bavarian insert kiwi jelly](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bavarois-au-kiwi-insert-gel%C3%A9e-de-kiwi-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bavarois-au-kiwi-insert-gel%C3%A9e-de-kiwi-.jpg>)

##  kiwi bavarian insert kiwi jelly 

Hello everybody, 

Today, she shares with us her kiwi Bavarian: A kiwi bavarois with her frozen kiwi insert .... So going for the recipe?   


**kiwi bavarian insert kiwi jelly**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bavarois-kiwi-insert-kiwi-2.jpg)

portions:  9  Prep time:  60 mins  cooking:  40 mins  total:  1 hour 40 mins 

**Ingredients** jelly kiwi insert: 

  * 100 gr of kiwi 
  * 100 gr of crystallized sugar 
  * 4 gr of gelatin 

kiwi bavarian 
  * 100 gr of kiwi puree 
  * 100 gr of milk 
  * 2 egg yolks 
  * 20 gr of sugar 
  * 4 gr of gelatin 
  * 100 gr of whipping cream 

for the biscuit 
  * 4 egg 
  * 125 gr of sugar 
  * 125 gr of flour 

mirror glaze 
  * 75 gr of water 
  * 11 g of gelatin 
  * 150 gr of sugar 
  * 150 grams of glucose 
  * 150 gr white chocolate 
  * 100 g sweetened condensed milk 
  * green dye   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/la-coupe.jpeg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/la-coupe.jpeg>)



**Realization steps** prepare the mirror glaze: 

  1. soak the gelatin in cold water 15 min 
  2. in a heated saucepan water and sugar + glucose, until boiling 
  3. remove from the heat and pour over the condensed milk and white chocolate 
  4. add the gelatin and the dye, mix everything and reserve the cool (prepared the day before) 

prepare the frozen kiwi insert: 
  1. let the gelatin soak for 15 minutes in cold water 
  2. peel the kiwis and cut them into pieces. 
  3. mix with the sugar, and put on low heat then remove and let warm 
  4. add the spun gelatin, mix well with the mixture 
  5. pour the mixture into a small cavity mold and freezer for a whole night.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/insert-kiwi.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/insert-kiwi.jpg>)

Prepare the biscuit: 
  1. beat the eggs and sugar until you have a nice mousse. 
  2. Introduce the flour gently with a spatula, 
  3. pour the mixture into a 28 x 38 cm mold, lined with parchment paper. 
  4. cook in a preheated oven at 180 ° C for 10 to 12 minutes (or depending on your oven) 
  5. remove from oven, let cool, and cut pucks that are going to be the size of your Bavarian mussels. 

prepare the kiwi bavarois: 
  1. soak the gelatin 15 min in cold water 
  2. heat the kiwi puree and the milk, 
  3. blanch the yolks with the sugar and pour them in a saucepan 
  4. add the kiwi and milk mixture, and put back on medium heat while mixing constantly. 
  5. remove from the heat and add the dehydrated gelatine, add the mixture to the mixture and leave to cool, turn the cream into whipped cream and add to the kiwi mixture after mixing a little to relax it.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/montage-du-bavarois.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/montage-du-bavarois.jpg>)

Mounting: 
  1. pour the Bavarian mousse into almost ⅔ cups. 
  2. place the insert, then cover with bavarois and put in the freezer for at least 6 h (all night here) 
  3. remove from the freezer and place the biscuit over it. remove the mosses.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/finition-kiwi.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/finition-kiwi.jpg>)
  4. heat the icing a little at 35 ° C and cover the bavarois 



[ ![Bavarian kiwi and kiwi insert](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bavarois-au-kiwi-et-son-insert-kiwi.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bavarois-au-kiwi-et-son-insert-kiwi.jpg>)
