---
title: Cake without cooking, express
date: '2012-05-13'
categories:
- gateaux, et cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/gateau-algerien-gateau-sans-cuisson.jpg
---
##  Cake without cooking, express 

Hello everybody, 

a cake that I add to my selection of [ cakes without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) , a cake, too good, very easy to do, very presentable, and above all, ultra .. ultra fast, yes, it's done in the blink of an eye, must say that my daughter loves to help me cover these cupcakes with jam and coconut. 

ingredients: 

  * boudoirs, or cat-tongue cake. 
  * apricot jam, or any other 
  * coconut 
  * flower water 



it's easy to make this cake. take the boudoirs and cut them in half. 

pass the jam in the microwave, or over low heat, if it is thick. 

add the water of flower, if you judge that it is too thick, but if it is a jam that flows, you do not need it. 

coat the cakes in jam, then cover with coconut. 

put it in paper boxes, and here it is. 

**Cake without cooking, express**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/gateau-algerien-gateau-sans-cuisson.jpg)

portions:  30  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * boudoirs, or cat-tongue cake. 
  * apricot jam, or any other 
  * coconut 
  * flower water 



**Realization steps**

  1. it's easy to make this cake. take the boudoirs and cut them in half. 
  2. pass the jam in the microwave, or over low heat, if it is thick. 
  3. add the water of flower, if you judge that it is too thick, but if it is a jam that flows, you do not need it. 
  4. coat the cakes in jam, then cover with coconut. 
  5. put it in paper boxes, and here it is. 


