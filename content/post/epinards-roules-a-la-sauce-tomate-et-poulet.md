---
title: spinach rolled with tomato sauce and chicken
date: '2011-04-18'
categories:
- Chocolate cake
- gateaux, et cakes
- recettes sucrees

---
hello everyone, when the idea of ​​this recipe has grown in my head, I did not know that imane had made it. finally, mine had a little difference, the chicken that accompanies it .... so first I put, the legs of my chicken to cook, in water to which I add, oil , salt, crushed garlic, a bay leaf, and black pepper. cook on low heat. & Nbsp; cook the spinach now, I do the method that showed me khalti kheira, that is to say, take the spinach well washed, cut in small quantity, and & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

finally, mine had a small difference, the chicken that accompanies it .... 

so first I put, the legs of my chicken to cook, in water to which I add, oil, salt, crushed garlic, a bay leaf, and black pepper . cook on low heat. 

cook the spinach now, I do the method that showed me khalti kheira, that is to say, take the spinach well washed, cut in small amount, and put to cook in an oiled pan with lid, so the moment that you cut a quantity, the other one is cooking, and you put the new quantity cut on the bake and that way you save a huge amount of time, because the baking will be finished right after the exhaustion of your spinach. 

after spinach cooling, you mix them with 5 to 6 beaten eggs, and season with salt, you can add crushed garlic. 

pour your mixture into a mold lined with sulphured paper, and place in your oven. 

if your dough swells, gently pierce it with a fork. 

in the meantime, prepare a nice tomato sauce, with 4 to 5 tomatoes, or as I did, with 1 can and a half, crushed tomato and cut. 

at the exit of your spinach from the oven, cover them with the tomato sauce. 

and with the chicken you crumbled. 

roll gently, because the tomato sauce may overflow. 

garnish your ride to your taste. 

serve well hot 

bon appétit 
