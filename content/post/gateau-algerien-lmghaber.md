---
title: Algerian cake, lmghaber
date: '2012-05-05'
categories:
- dessert, crumbles et barres
- recettes sucrees
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mghaber-007_thumb-300x224.jpg
---
##  Algerian cake, lmghaber 

Hello everybody, 

here's one **[ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) ** very delicious, ** [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) ** that you can present for all occasions, so it's easy to do more beautiful, and too good. 

a recipe from a book of Algerian cakes, I'm not very sure of the source, but one of my readers told me once that it's  Samira's book  , knowing that I had found the recipe on a forum. 

so I will not make you wait, and here is the recipe. 

**Algerian cake, lmghaber**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mghaber-007_thumb-300x224.jpg)

**Ingredients** (for my part I did half of the ingredients) 

  * flour 
  * 500 gr of margarine 
  * 1 glass and a half icing sugar 
  * 2 eggs 
  * 1 pinch of baking powder 
  * Orange tree Flower water 

the joke: 
  * 1 package of biscuits 
  * 1 handful of ground peanuts 
  * Turkish halwa 
  * jam   
you can do without it and stuff it with Turkish Halwa only 



**Realization steps**

  1. In a container, put margarine ointment, icing sugar, eggs and yeast pinch. 
  2. mix everything very well to have a nice cream. 
  3. add the flour, so that you can pound the dough, then wet with a little bit of orange blossom water, to obtain a soft dough. 
  4. Formet then small balls of 3 cm in diameter, let stand, in the meantime, prepare the stuffing. 
  5. In a container, put the ground biscuits, the cachuete handle, and the Turkish halibut, pick it all up with jam, until you get a soft stuffing. 
  6. Lower the balls of dough with the palms of the hands, then put some stuffing in the middle   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mghaber-003_thumb1.jpg) ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mghaber-002_thumb1.jpg)
  7. go up the edges and close. 
  8. Using a nakache (a special plier, used for kaak nakache), pinch the entire length and the entire surface of the cakes, going from the bottom to the top.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mghaber-001_thumb1.jpg)
  9. must say that I'm not too good with nakache, hihihihihi 
  10. Arrange the cakes on a baking dish sprinkle with flour, once cooked, sprinkle with the icing sugar. 



vous pouvez egalement garnir chaque gateau avec une fleur et des feuilles en pate d’amande, moi je n’en avais pas. 
