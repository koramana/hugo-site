---
title: praline chocolate muffins
date: '2013-11-19'
categories:
- juice and cocktail drinks without alcohol
- cooking fat-burning, slimming recipes
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/muffins-chocolat-pralin-1_thumb.jpg
---
Hello everybody, 

what do I like muffins, and these muffins, huuuuuuuuuuuuum, they are sublime and tasty, it's praline chocolate muffins ... 

a very nice achievement from my dear friend Lunetoiles, and it gives me great pleasure to put it on my blog. 

for the recipe of the **[ praline paste ](<https://www.amourdecuisine.fr/article-praline-maison-89554344.html>) **   


**praline chocolate muffins**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/muffins-chocolat-pralin-1_thumb.jpg)

portions:  12  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 50 g of butter 
  * 50 g dark chocolate 
  * 100 g of sugar 
  * 2 spoon with praline paste soup: 
  * 100 ml semi-skimmed milk 
  * 200 g flour 
  * 2 teaspoons baking powder 
  * 2 eggs 
  * 60 g chopped dark chocolate 



**Realization steps**

  1. method of preparation: 
  2. Preheat the oven to 180 ° C. 
  3. In a bowl, beat the eggs with the sugar. 
  4. Add flour, yeast and dilute with milk. 
  5. Melt the chocolate, butter and praline paste in the microwave and add to the mixture. 
  6. Stir in the chopped chocolate and mix well until the mixture is well blended. 
  7. Fill the cavities with a muffin tin previously filled with paper boxes and fill until ¾. 
  8. Put in the oven about 15 to 20 minutes. 


