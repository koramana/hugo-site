---
title: Pie with red fruits and pistachio
date: '2016-07-11'
categories:
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/TARTE-AUX-FRUITS-ROUGES-ET-A-LA-PISTACHE11.jpg
---
![PIE-IN-FRUIT-RED-AND-A-LA-PISTACHE.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/TARTE-AUX-FRUITS-ROUGES-ET-A-LA-PISTACHE11.jpg)

##  Pie with red fruits and pistachio 

Hello everybody, 

Is it already at home? It's not yet the case here in England, it's raining cats, sometimes it's a little cool, not to mention the fog, we do not feel at all here summer !! In any case at Lunetoiles, I think it's already summer watching this beautiful pie with red fruits and pistachios .... I would like to use it ... 

The shortbread dough: 

  * 240g of flour, 
  * 150g of butter, 
  * 90g of sugar, 
  * 30g of almond powder, 
  * 1 sachet of vanilla sugar, 
  * 1 egg yolk, 
  * 1 pinch of salt 



Cream of almonds and pistachios: 

  * 200g of frozen red fruits, 
  * 60g of almond powder, 
  * 60g of pistachio powder, 
  * 2 eggs, 100g of sugar, 
  * 20 cl whole cream 



The ganache: 

  * 150g of white chocolate, 
  * 2 tablespoons pistachio paste, 
  * 200g whole liquid cream 



Decoration: 

  * 200 g of fresh red fruits, 
  * 1 tbsp chopped fresh pistachios 



####  ![PIE-IN-FRUIT-RED-AND-A-LA-PISTACHE-2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/TARTE-AUX-FRUITS-ROUGES-ET-A-LA-PISTACHE-211.jpg)

####  Preparation: 

The day before prepare the ganache: 

  1. Melt the white chocolate with 5 ml of cream in the microwave 2 times 30 seconds. 
  2. Add the pistachio paste and the remaining cream. 
  3. Mix well, put in the fridge all night. 



Prepare the shortcrust pastry: 

  1. Preheat your oven to 180 °. 
  2. In the mixer bowl or beater, beat the flour, sugar, vanilla sugar and salt. 
  3. Add the diced butter and mix with the whisk K until you get a big shortbread. 
  4. Add the egg yolk and knead until a smooth, non-sticky paste is obtained. Let it rest. 
  5. Spread the dough and garnish a pie circle about 24 cm buttered. 
  6. Stir the dough with a fork and let cool enough to prepare the filling. 



Prepare the almond cream and pistachios: 

  1. Beat using the same robot, this time equipped with the whisk, the eggs, and the sugar, until the mixture is foamy. 
  2. Then add the almond and pistachio powder and finally the cream. Beat well. 
  3. Take the dough out of the fridge, garnish with red berries and pour the cream on it. 
  4. Bake for about 30 minutes and let cool. 



The ganache: 

  1. Mount the ganache with an electric mixer, then fill a pastry bag and form rolls spaced half a centimeter. 
  2. Decorate with fresh fruit. 



**Pie with red fruits and pistachio**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/TARTE-AUX-FRUITS-ROUGES-ET-A-LA-PISTACHE.jpg)

portions:  8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** The shortbread dough: 

  * 240g of flour, 
  * 150g of butter, 
  * 90g of sugar, 
  * 30g of almond powder, 
  * 1 sachet of vanilla sugar, 
  * 1 egg yolk, 
  * 1 pinch of salt 

Cream of almonds and pistachios: 
  * 200g of frozen red fruits, 
  * 60g of almond powder, 
  * 60g of pistachio powder, 
  * 2 eggs, 100g of sugar, 
  * 20 cl whole cream 

The ganache: 
  * 150g of white chocolate, 
  * 2 tablespoons pistachio paste, 
  * 200g whole liquid cream 

Decoration: 
  * 200 g of fresh red fruits, 
  * 1 tbsp chopped fresh pistachios 



**Realization steps** The day before prepare the ganache: 

  1. Melt the white chocolate with 5 ml of cream in the microwave 2 times 30 seconds. 
  2. Add the pistachio paste and the remaining cream. 
  3. Mix well, put in the fridge all night. 

Prepare the shortcrust pastry: 
  1. Preheat your oven to 180 °. 
  2. In the mixer bowl or beater, beat the flour, sugar, vanilla sugar and salt. 
  3. Add the diced butter and mix with the whisk K until you get a big shortbread. 
  4. Add the egg yolk and knead until a smooth, non-sticky paste is obtained. Let it rest. 
  5. Spread the dough and garnish a pie circle about 24 cm buttered. 
  6. Stir the dough with a fork and let cool enough to prepare the filling. 
  7. Prepare the almond cream and pistachios: 
  8. Beat using the same robot, this time equipped with the whisk, the eggs, and the sugar, until the mixture is foamy. 
  9. Then add the almond and pistachio powder and finally the cream. Beat well. 
  10. Take the dough out of the fridge, garnish with red berries and pour the cream on it. 
  11. Bake for about 30 minutes and let cool. 

The ganache: 
  1. Mount the ganache with an electric mixer, then fill a pastry bag and form rolls spaced half a centimeter. 
  2. Decorate with fresh fruit. 


