---
title: algerian cake el mechkouk
date: '2012-06-08'
categories:
- pizzas / quiches / tartes salees et sandwichs

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateaux-algeriens-lemchakek_thumb1.jpg
---
![algerian cake lemchakek](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateaux-algeriens-lemchakek_thumb1.jpg)

##  algerian cake el mechkouk 

Hello , 

Another **Algerian cake** , a delight based on almonds, this is the simple version of Algerian cakes " [ mchawek ](<https://www.amourdecuisine.fr/article-25345484.html>) "Except that instead of being wrapped in flaked almonds, these cakes are coated, in icing sugar, so less work, with the same delicious taste. 

A simple Algerian cake, also at the door of the average budget, because we can replace almonds with peanuts and it gives a cake ... I do not tell you ... 

Just for info and for people who do not understand Arabic: el mechkouk, means cracked cakes. I will not be too late to talk to you about all [ delicious Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) , but looking at this link, you will find wonders.   


**algerian cake el mechkouk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateau-algerien-el-machkouk_2.jpg)

portions:  50 

**Ingredients** the ingredients for almost 50 pieces: 

  * 500g ground almonds 
  * 300 g sifted icing sugar 
  * 3 to 4 egg whites (depending on the size) 
  * red and blue dye 
  * strawberry aroma 
  * pistachio aroma 
  * icing sugar to coat 
  * whole almonds for decoration 



**Realization steps**

  1. In a large bowl, mix almonds and sifted icing sugar. 
  2. Wet with egg whites until you get a fairly soft dough, one egg white at a time, and do not put too much, otherwise your cakes will not be well cracks 
  3. cut the dough in 2 
  4. If you choose the pink color, put the red dye with the aroma corresponding to the color: strawberry 
  5. blue dye (it gives green mchekaks) pistachio aroma 
  6. Make balls the size of an egg yolk, 
  7. roll them in sifted icing sugar, 
  8. decorate each ball with a whole almond. 
  9. Arrange the cakes on a plate covered with baking or floured paper 
  10. Make leather in a preheated oven at 180 ° C for about 15 minutes, place the plate in the middle of the oven. 
  11. Put cooled cakes in small boxes. 



![algerian cake el mechkouk](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateaux-algeriens-el-machkouk-11.jpg)

Bonne journée. 
