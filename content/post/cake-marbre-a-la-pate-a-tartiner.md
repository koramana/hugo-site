---
title: Marbled cake with spread
date: '2013-01-18'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-marbre-a-la-pate-a-tartiner-61.jpg
---
![marble-cake-a-la-pate-a-spread-6.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-marbre-a-la-pate-a-tartiner-61.jpg)

##  Marbled cake with spread 

Hello everybody, 

a recipe that shares with us today Lunetoiles, in fact it's his period "cakes" and it contaminated me, just today, I realize 2 cakes, so his pictures make me want . 

so today, I share with you its first cake, a cake marble and its marbare is with the spread, Nutella ... hum, I imagine the taste, yumi ...   


**Marbled cake with spread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-marbre-a-la-pate-a-tartiner-31.jpg)

**Ingredients**

  * 250g of flour 
  * 150 g of sugar 
  * 125 g of butter 
  * 1 natural yoghurt 
  * 1 sachet of baking powder 
  * 1 sachet of vanilla sugar 
  * 2 big eggs 
  * 1 pinch of salt 
  * 125 gr chocolate / hazelnut spread 



**Realization steps**

  1. Preheat the oven to 180 ° C (th.6). Butter a cake tin or lined with parchment paper. 
  2. In a terrine, break the eggs; add the sugar and beat well together. Add 1 pinch of salt, then gradually pour the flour, yeast and yoghurt. 
  3. When the mixture is homogeneous, always beat for just melted butter. Divide the dough into two parts. Perfume one with vanilla sugar and the other with the spread. 
  4. Put a layer of vanilla dough, a layer of dough in the spread and so on. 
  5. Bake at 180 ° C (th.6), for 45 minutes to 1 hour. 
  6. Check the cooking with the tip of a knife, it must come out dry and clean. 
  7. At the exit of the oven, turn out, remove the paper if necessary and let cool on a grid. 


