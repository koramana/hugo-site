---
title: Hazelnut and caramel chocolate bars
date: '2014-05-12'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-barres-chocolat-noisette-et-caramel-.1.jpg
---
[ ![Hazelnut and caramel chocolate bars](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-barres-chocolat-noisette-et-caramel-.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-barres-chocolat-noisette-et-caramel-.1.jpg>)

##  Hazelnut and caramel chocolate bars 

Hello everybody, 

I really like these small delights in square or rectangular shapes, crispy and melting in the mouth, it's heartland turtle bar-bars chocolate hazelnut and caramel, yes the name in English is a little weird, lol, so it's bars are a realization of Lunetoiles, which she found on an English blog ... 

I try to document this recipe to find the origin of the name, but I do not find great things, we will say that it is bars based on crumble with oat flakes, garnished with a beautiful layer of hazelnuts, chocolate and caramel melting brown sugar ... not that, we finish the filling with the rest of the crumble. 

It must be super delicious is not it, now the form, it's up to you to see how to present these rich and crispy cakes.   


**Hazelnut and caramel chocolate bars**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars.1.jpg)

portions:  25  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** for the crust and crumble garnish: 

  * 1 + ½ cups flour (220 g) 
  * 1 pinch of salt 
  * ½ teaspoon ground cinnamon 
  * ¾ teaspoon of baking soda 
  * 1 cup dark brown sugar (200 g) 
  * 1 + 3/4 cups oatmeal (175 g) 
  * 1 cup unsalted butter, melted (230 g) 

for the medium 
  * 1 cup pecans or grilled hazelnuts, roughly chopped (170 g) 
  * 1 + ½ cup chocolate chips (250 g) 

for the caramel filling 
  * 2 tablespoons thick cream 
  * ½ cup tight dark brown sugar (100 g) 
  * 1 pinch of salt 
  * 10 tablespoons unsalted butter - cut into cubes (140 gr) 



**Realization steps**

  1. Preheat the oven to 175 ° C. 
  2. Butter a mold of 23x33 cm, dress the mold with parchment paper (leave a slight surplus on the edges). 
  3. In a bowl, mix together the flour, salt, cinnamon and baking soda. 
  4. Add the brown sugar and use your hand to work and mix with the other ingredients 
  5. Add the oatmeal and mix to combine. 
  6. Make a well in the center of the dry ingredients and pour in the melted butter. 
  7. Use a large rubber spatula or wooden spoon to stir until the mixture is wet and combined. 
  8. Spread about ⅔ of the mixture in the prepared mold.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-preparation-300x201.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-preparation-300x201.jpg>)
  9. Press in an even layer. 
  10. Bake for 8-10 minutes, or until the edges begin to brown. 
  11. Remove the mold from the oven and place on a cooling rack.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-preparation-1-300x201.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-preparation-1-300x201.jpg>)
  12. After about 10 minutes of cooling time, sprinkle the crust with pecans (or hazelnuts) and chocolate chips. {Note: Do not worry if the crust is still warm.}   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-preparation-3-300x201.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-preparation-3-300x201.jpg>)

Prepare the filling: 
  1. Place the brown sugar, salt, and butter cubes in a medium-sized saucepan. 
  2. Heat over medium-high heat until the butter is melted. 
  3. Bring the mixture to a slow boil and boil for about 1 minute, stirring constantly (at this point, the caramel would have darkened slightly). 
  4. Remove the pan from the heat and carefully add the cream. 
  5. Pour the hot caramel directly on the hazelnut / chocolate layer.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-PREPARATION-4-300x201.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars-PREPARATION-4-300x201.jpg>)
  6. Use a spatula to spread the caramel. 
  7. Sprinkle the remaining oatmeal mixture over the caramel layer. 
  8. Bake for 10-15 minutes, or until top is lightly browned. 
  9. Remove from the oven and place on a rack. 
  10. Let cool completely before cutting. 
  11. When you are ready to cut the bars, use the parchment paper and gently lift the cake from the mold. 
  12. Cut into bars and serve. 
  13. Enjoy! 



[ ![Hazelnut and caramel chocolate bars](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/heartland-turtle-bars.1.jpg>)
