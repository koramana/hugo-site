---
title: stuffed tomatoes
date: '2013-11-16'
categories:
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/nids-de-tomates-aux-oeufs-038_thumb.jpg
---
Hello everybody, 

the **stuffed tomatoes** : here is a very beautiful [ Entrance ](<https://www.amourdecuisine.fr/recettes-salees/amuse-bouche-tapas-mise-en-bouche>) you can even bring with us on a picnic, or just enjoy with a [ salad ](<https://www.amourdecuisine.fr/salades-verrines-salees>) for a light meal, these delicious crunchy tomatoes, closing in their hearts an egg, topped with a nice layer of thick cream and cheese. 

**stuffed tomatoes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/nids-de-tomates-aux-oeufs-038_thumb.jpg)

Recipe type:  tapas, amuses mouths  portions:  3  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 3 large tomatoes 
  * 4 medium-sized eggs 
  * 60 ml thick cream 
  * 114 gr grated cheese 
  * salt and black pepper. 



**Realization steps**

  1. wash the tomatoes, and remove a top of each tomato. empty the heart with a teaspoon. 
  2. put the tomatoes upside down on a piece of absorbent sopalain paper, and let them drain for at least 15 minutes. 
  3. salt and pepper the tomatoes, place them in an ovenproof baking tin, pressing them tightly against each other.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/2012-03-13-nids-de-tomates-aux-oeufs_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/2012-03-13-nids-de-tomates-aux-oeufs_thumb.jpg>)
  4. break in the heart of each tomato an egg, 
  5. cover it with a spoonful of heavy cream. 
  6. decorate with grated cheese. 
  7. cook in a preheated oven at 180 degrees, for 15 to 20 minutes, or until you feel that the egg white is cooked through. 



{{< youtube dWP2x >}} 

you can see: 

[ ![tapas with avocado cream 023.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tapas-a-la-creme-d-avocat-023.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/article-tapas-a-la-creme-d-avocat-tomate-cerises-farcies-113829637.html>)

[ tomatoes stuffed with avocado cream ](<https://www.amourdecuisine.fr/article-tapas-a-la-creme-d-avocat-tomate-cerises-farcies.html>)

thank you for your visits. 

passez une belle journée. 
