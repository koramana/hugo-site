---
title: bread dripping with cheese and mushroom
date: '2018-03-08'
categories:
- appetizer, tapas, appetizer
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pain-degoulinant-au-fromage-et-champignon-1.CR2_.jpg
---
![dripping bread with cheese and mushroom 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pain-degoulinant-au-fromage-et-champignon-1.CR2_.jpg)

##  bread dripping with cheese and mushroom 

Hello everybody, 

I'm coming back to this cheese-dripping bread recipe, but this time it's not just cheese in there, but rather cheese and sautéed mushrooms. 

I liked this recipe for a while when I saw it on American blogs, but I did not have the chance to realize it. 

Then when Lunetoiles has to share with us his recipe for [ dripping bread with two cheeses ](<https://www.amourdecuisine.fr/article-pain-degoulinant-aux-deux-fromages.html> "Bread dripping with both cheeses") This recipe only made me want to make this easy recipe.   


**bread dripping with cheese and mushroom**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pain-degoulinant-au-fromage-et-champignon.jpg)

Recipe type:  bread  portions:  4  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients** For mushrooms 

  * a nice handful of sliced ​​mushrooms 
  * 1 tablespoon of butter 
  * 1 tablespoon chopped fresh thyme 

For the bread 
  * 1 loaf of un-sliced ​​bread. 
  * a nice amount of mozzarella cheese rappé 
  * a nice amount of cheddar cheese rappé 
  * ½ glass of butter, melted 
  * ½ glass of green onion cut into small cubes 
  * a few sprigs of parsley 
  * 2 teaspoons of poppy seeds 



**Realization steps** For mushrooms 

  1. Heat a medium skillet over medium heat. Add the butter. Once the butter is melted, add the mushrooms. Bake 4-5 minutes until it turns a nice color. Add the thyme and cook another 2-3 minutes. 
  2. Set mushrooms aside and let cool. 

For the bread 
  1. Preheat the oven to 170 degrees. 
  2. Cut the bread in length and width without cutting through the lower crust.   
It can be a little difficult to go in the second direction I used scissors to cut the crust without ruining it, then I made the cuts with the knife. 
  3. Place the bread on a sheet of aluminum foil. 
  4. Insert slices of cheese between cuts. 
  5. Pour the mushrooms between the cuts.   
Use your fingers to push the mushrooms down into the bread. 
  6. Mix butter, onion, and poppy seeds. Pour on the bread. Wrap in aluminum foil; place on a baking sheet. 
  7. Bake at 350 degrees for 15 minutes. 
  8. remove foil and bake 10 minutes longer, or until cheese is melted 



[ ![cheese dripping bread and mushroom 3.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pain-degoulinant-au-fromage-et-champignon-3.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pain-degoulinant-au-fromage-et-champignon-3.CR2_.jpg>)
