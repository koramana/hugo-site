---
title: minced meat pie
date: '2016-05-12'
categories:
- pizzas / quiches / tartes salees et sandwichs

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-%C3%A0-la-viande-hach%C3%A9e.jpg
---
[ ![minced meat pie](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-%C3%A0-la-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-%C3%A0-la-viande-hach%C3%A9e.jpg>)

##  minced meat pie 

Hello everybody, 

The salty pie for me is an appetizer I can not help but this mince pie is just pure delicacy, very easy to prepare and especially good. I confess that when I make this pie with minced meat, I easily eat half of this pie with a few Kalamata olives that I really like, I do not eat anything, but I really like this salty pie. the melting and crunchy crust, 

and with this very greedy stuffing of minced meat well perfumed with parsley and caraway (I like cumin very much with minced meat, it gives it even more flavor), and especially to fall on this super melting piece of cheese ... Frankly this minced meat pie is a must try. 

Feel free to pass me your photos if you tried one of my recipes .... 

![minced meat pie 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-%C3%A0-la-viande-hach%C3%A9e-3.jpg)

**minced meat pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-%C3%A0-la-viande-hach%C3%A9e-1.jpg)

portions:  6  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * [ pasta for salty pie ](<https://www.amourdecuisine.fr/article-pate-a-tarte-salee.html>)
  * 3 cheese triangle in portion. 
  * 1 onion. 
  * 300 g ground meat. 
  * 1 glass of milk. 
  * 3 eggs. 
  * 1 tablespoon of flour. 
  * salt and pepper. 
  * ¼ teaspoon of cumin 
  * chopped parsley 
  * 1 little nutmeg 
  * grated cheese. 



**Realization steps**

  1. Prepare the pie dough, go into a pie pan and refrigerate for 30 minutes. 

prepare the minced meat stuffing: 
  1. Cook the chopped onion in a little oil, add the minced meat, salt and pepper. 
  2. add the caraway, and the nutmeg touch. 
  3. add a little water and let the meat cook well. 
  4. Toward the end of cooking, add a little chopped parsley and make sure the sauce is well reduced. 
  5. In a bowl mix the eggs with the milk, flour, salt and pepper and leave the mixture aside. 
  6. remove the pie plate from the fresh, poke the surface of the dough with a fork and cover the dough with a piece of baking paper, 
  7. pour dry beans on the paper, and cook the dough for about 15 minutes. 
  8. After 15 minutes of cooking, remove the dough, remove the baking paper and spread the piece of cheese on the dough. 
  9. add the cooked minced meat mixture and pour over the liquid mixture to cover well. 
  10. garnish the top with grated cheese. 
  11. Bake the pie in the preheated oven at 180 ° C, to have a beautiful golden color. 



[ ![minced meat pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-%C3%A0-la-viande-hach%C3%A9e-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-%C3%A0-la-viande-hach%C3%A9e-2.jpg>)
