---
title: I also made the flan erynien
date: '2009-05-27'
categories:
- ice cream and sorbet
- basic pastry recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/05/pic-002_thumb1.jpg
---
a real delight, that sublime custard, which I had prepared after louiza spoke to me of her sublime taste. the recipe is here. to do again without any hesitation. 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![pic 002](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/pic-002_thumb1.jpg) ![pic 003](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/pic-003_thumb1.jpg)

a real delight, that sublime custard, which I had prepared after louiza spoke to me of her sublime taste. 

the recipe is [ right here ](<https://www.amourdecuisine.fr/article-30828812.html>) . 

![pic 004](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/pic-004_thumb1.jpg) ![pic 005](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/pic-005_thumb1.jpg)

![pic 006](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/pic-006_thumb1.jpg) ![pic 007](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/pic-007_thumb1.jpg)

a refaire sans aucune hesitation. 
