---
title: Perfect Kumquat jam
date: '2016-06-26'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/confiture-de-kumquat-2-731x1024.jpg
---
![kumquat jam 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/confiture-de-kumquat-2-731x1024.jpg)

##  Perfect Kumquat jam 

Hello everybody, 

Every summer when I go to Algeria, I find that my mother has prepared me and kept two large jars of kumquat jam that I like very much because I never find it on the market here in Bristol. 

Well yesterday, I went out with my husband to do the shopping in order to make some Algerian cakes, for Eid, and we went to the Indian store of the district, I like to do my little shopping at home because I know that I will find all the ingredients I need. From one department to another, what do I find among the fruits on display? a big cashew of Kumquat .... my eyes are well out of their places, hihihihi, without hesitation and despite the price, I still bought a nice amount .... 

As soon as I got home, I immediately started making my Kumquat jam as perfect as my mother prepares it. However, my mother still likes to flavor her jams with a cinnamon stick, while I prefer to put the star anise, or the badienne its taste and fragrance have everything to please me. 

![kumquat](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/kumquat-683x1024.jpg)

**Perfect Kumquat jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/confiture-de-kumquat-1.jpg)

portions:  2  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 500 kumquats 
  * 350 g of crystallized sugar 
  * 1 slice of lemon 
  * 2 star anis 
  * water 



**Realization steps**

  1. Wash the kumquats, drain them, and blanch them for 10 minutes in a pan of boiling water. Drain and rinse to cool under cold water. Cut them in 2 and remove the seeds. 
  2. Put kumquats, lemon slices, sugar and star anise in a large saucepan. 
  3. Cover with water and macerate overnight, 
  4. The next day, take the kumquats with a slotted spoon and place them in a large bowl 
  5. place the contents of the pan on medium heat and bring to a boil 
  6. let boil for 8 to 10 minutes then put back the kumquats and let simmer gently for about 30 minutes.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/pr%C3%A9paration-de-la-confiture-de-Kumquat.jpg)
  7. check the cooking of the jam, placing a few drops of the cooking syrup on a cold plate: if they are frozen, the jam is ready. 
  8. place it directly in clean sterilized jars. 



![kumquat jam](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/confiture-de-kumquat-676x1024.jpg)
