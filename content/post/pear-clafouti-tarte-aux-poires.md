---
title: Pear Clafouti - Pear tart
date: '2014-04-16'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg
---
[ ![pear pie](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/image_thumb4.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/image_25.png>)

Hello everybody, 

I thank lunetoiles who frankly did a wonderful job, and I leave you with the recipe for this pear pie in melting well in the mouth, and crispy at a time ... it must be a delight. 

**Pear Clafouti - Pear tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/image_thumb_110.png)

Recipe type:  Dessert  portions:  8  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * A [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast")

Syrup: 
  * 80 cl of water (40 cl of boiling water, 40 cl of cold water) 
  * 300g of sugar 
  * vanilla pod 
  * juice of a small lemon and its zest 
  * 6-7 small pears 

Cream: 
  * ⅔ cup of whole cream 
  * 4 eggs 
  * 4 tablespoons flour (or cornflour) 
  * 4cs of sugar 
  * 1 cup pear syrup 



**Realization steps**

  1. Bring the water and sugar to a simmer in a large pan with a thick bottom. 
  2. Add the zest and the juice of the lemon, as well as the vanilla. 
  3. Peel the pears carefully, taking care to keep the tail. Dip them in the syrup. 
  4. Bring to a boil. Bake 12 to 15 minutes, until the pears are tender. 
  5. Then let cool in the syrup 30 minutes. 
  6. Pre-cook the pie shell in an oven heated to 175 ° C. 
  7. The filling: Mix the flour or cornflour and the sugar in a bowl, and whisk the syrup little by little. Add whisking eggs and cream. 
  8. Place the poached pears on the bottom of the pie, and pour the cream around. 
  9. Bake for about 30 minutes at 175-180 ° C. 
  10. Let cool before topping with apricot jam. Put at least 1 hour before tasting. 



[ ![picture](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/image_thumb_23.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/image_63.png>)

thank you very much to lunetoiles and if you like lunetoile, you want to share your recipes on my blog, or then you have to try my recipes, 

I invite you to send me your essays by clicking on this link 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll like ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link of your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)
