---
title: stuffed bread recipe
date: '2014-03-26'
categories:
- couscous
- Algerian cuisine
- Dishes and salty recipes
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/chaussons-a-la-viande-hachee_thumb.jpg
---
[ ![chopped meat slippers](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/chaussons-a-la-viande-hachee_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/01/chaussons-a-la-viande-hachee.jpg>)

Hello everybody, 

here are very good rolls, or slippers, or even name buns that I often prepare, to give them a shape even more beautiful, I like to make lattices, 

they are also pretty round shape, but with this shape, I think we can stuff them more, something that my husband likes very much. 

I already did this form when I prepared my [ stuffed buns with eggplant ](<https://www.amourdecuisine.fr/article-chaussons-aux-aubergines-et-autres-farces-55865874.html>) , A recipe too good and that has made its success. 

so quickly I pass you this easy recipe, for the dough, I'm always faithful to the dough a [ pizza ](<https://www.amourdecuisine.fr/article-pizza-aux-poivrons-87367397.html>) of Kaouther, and for which I have become an addict, and that I advise everyone.   


**stuffed bread recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/buns-a-la-viande-hachee_thumb-300x238.jpg)

portions:  12  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients** for the dough: 

  * 3 glasses of flour (200 ml glass) 
  * 1 tablespoon instant yeast 
  * 1 tablespoon of sugar 
  * 1 teaspoon of salt 
  * 3 tablespoons milk powder 
  * 4 tablespoons of olive oil or even table 
  * 1 cup of baking powder 
  * water as needed 

for the stuffing: 
  * 250 g minced mutton 
  * 1 onion 
  * Olive oil 
  * 2 cloves garlic 
  * 1 bunch of parsley 
  * 1 tbsp tomato concentrate diluted in a little water 
  * Salt, pepper, coriander powder, cumin (1/4 cup of coffee) 

For gilding: 
  * 1 egg yolk 
  * 1 tablespoon of milk 
  * seed of nigella 



**Realization steps**

  1. Mix all the dough ingredients and add enough lukewarm water until soft, soft and easy to work. 
  2. Knead a few minutes. 
  3. Form the dough into a ball and let it rise under a cloth, protected from drafts until it has doubled in volume. 
  4. In a skillet, pour a splash of olive oil. Add chopped onion, grated garlic and chopped parsley. 
  5. Let it come back until the onion is translucent. 
  6. Add the minced meat, salt and pepper and add the spices 
  7. Simmer on low heat. 
  8. At the end of cooking, add the diluted tomato and simmer for a few minutes. 
  9. Once the dough has doubled in volume, degas it and divide it into 10 to 12 balls. 
  10. Preheat the oven to 180 ° 
  11. Spread the dough on a floured work surface. 
  12. Put a line of stuffing in the middle 
  13. with a roulette wheel, draw lines in one end 
  14. cover the stuffing with the end not drawn, then fold the other side over. 
  15. put your buns on a baking flour plate and let them double in size. 
  16. When it has risen well, brush with a mixture of milk and egg yolk and sprinkle with seeds of nigella. 
  17. Bake for 20 to 25 min (depending on the oven) (and especially not going to respond to comments like me, I almost burn my buns, hihihihi) 
  18. enjoy with a very good salad 



[ ![buns with chopped meat](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/petits-pains-a-la-viande-hachee_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/01/petits-pains-a-la-viande-hachee_2.jpg>)

thank you for your visits 

bonne journée 
