---
title: easy and fast genoise
date: '2012-03-12'
categories:
- fried Algerian cakes
- Algerian cakes- oriental- modern- fete aid

---
Hello everybody, 

the genoise is a basic cake, in several recipes of party cakes, birthday cakes, or cakes for 4 hours. 

so here is my basic recipe, to prepare a well ventilated genoise. 

  * 90g flour 
  * 90g caster sugar 
  * 3 eggs 
  * 1/3 sachet baking powder 
  * 1 pinch of salt 
  * pink dye 



the sirup: 

  * 100 ml of water 
  * 100 gr of sugar 



method of preparation: 

  * Separate the egg whites from the yolks. 
  * Blanch the yolks with the sugar (I make it to the robot). Add the flour. 
  * Mount the whites until stiff and incorporate them into the machine twice. 
  * cook at 160 ° C for about 20 minutes. 
  * Prepare the syrup by making water and sugar 
  * sprinkle the sponge cake with your prepared syrup    



