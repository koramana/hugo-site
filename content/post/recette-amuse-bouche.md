---
title: amuse bouche recipe
date: '2013-12-10'
categories:
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/fleurs-a-la-creme-d-artichaut_thumb.jpg
---
##  amuse bouche recipe 

Hello everybody, 

You want to impress your guests with an appetizer recipe different from what you're used to concocting or a small take-out for your [ picnics ](<https://www.amourdecuisine.fr/article-panier-pique-nique-102297629.html>) ??? ... 

Here is a nice appetizer recipe, from fried roses to **bricks leaves** or won your Chinese wraps stuffed with a cream of artichokes, really a crunchy delight and melting in the mouth. 

I will see this recipe among the [ ramadan recipes more ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>)   


**amuse bouche recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/fleurs-a-la-creme-d-artichaut_thumb.jpg)

Recipe type:  aperitif, appetizer  portions:  6  Prep time:  15 mins  cooking:  5 mins  total:  20 mins 

**Ingredients**

  * 3 leaves of bricks, or 12 won your Chinese wraps 
  * ½ jar of heart of grilled artichokes marinated in oils 
  * 3 cloves of garlic 
  * 20 gr of parmesan 
  * 2 portions of cheese 
  * black pepper and salt. 
  * egg white for decoration 
  * 2 pieces of 8.5 cm and 4 cm 
  * oil for frying. 



**Realization steps**

  1. place cloves of garlic in aluminum foil, with a little oil, close and grill in the oven for 10 min. 
  2. in the bowl of the blinder, put the hearts of artichokes, grilled garlic after removing the skin, 
  3. add the parmesan, the cheese, the pepper, and mix to have a cream.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/fleurs-farcies-a-la-creme-d-artichauts_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/fleurs-farcies-a-la-creme-d-artichauts_thumb.jpg>)
  4. cut 6 large circles with the 8.5 cm cookie cutter, and between 30 and 36 small circles with the 3.5 cm cookie cutter. 
  5. place a 2 cm diameter nut in the big circle 
  6. brush the edges with egg white, and close it, 
  7. brush again with egg white, and start to place the petals 
  8. for each rose, you can stick between 5 and 6 small circles. 
  9. cook in a well preheated oil bath. 
  10. present as an aperitif with a fresh salad. 



[ ![flower-stuffed-fries-a-la-creme-artichauts_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/fleur-frites-farcies-a-la-creme-artichauts_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/fleur-frites-farcies-a-la-creme-artichauts_thumb1.jpg>)

follow me on: 

Here is a nice entry list for you: 

![appetizers au-smoke salmon](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/amuses-bouches-au-saumon-fume-150x150.jpg)

###  [ appetizers with smoked salmon ](<https://www.amourdecuisine.fr/article-amuses-bouches-au-saumon-fume.html> "Permalinks to appetizers with smoked salmon")

![Tartar-DE-SALMON-FUMEE_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/TARTARE-DE-SAUMON-FUMEE_thumb1-150x150.jpg) ![tapas-of-salmon-fume.CR2_thumb2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-de-saumon-fume.CR2_thumb2-150x150.jpg)

####  [ appetizer with smoked salmon and bricks leaves ](<https://www.amourdecuisine.fr/article-amuse-bouche-au-saumon-fume-et-feuilles-de-bricks.html> "Permalinks to amuse bouche with smoked salmon and bricks leaves")

![Salmon-001](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/saumon-001-150x150.jpg) ![artichoke-goat-088_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/artichauts-chevres-088_thumb1-150x150.jpg)

###  [ Golden goat on artichoke background / amuses bouches ](<https://www.amourdecuisine.fr/article-chevre-dore-sur-fond-d-artichaut.html> "Permalinks to Goat golden on artichoke background / amuses bouches")

![Recipe hasselback potatoes, Swedish-style potatoes 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/DSC07786-101x150.jpg)

###  [ recipe hasselback potatoes, Swedish-style potatoes ](<https://www.amourdecuisine.fr/article-pommes-de-terre-a-la-suedoise-hasselbackpotatis.html> "Permalinks to recipe hasselback potatoes, Swedish-style potatoes")

![lahmacun-052_thumb_11](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/lahmacun-052_thumb_11-150x150.jpg)

###  [ lahmacun - Turkish pizza لحم عجين ](<https://www.amourdecuisine.fr/article-lahmacun-pizza-turque.html> "Permalinks to lahmacun - Turkish pizza لحم عجين")

![cheese slippers 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chaussons-au-fromage-1-122x150.jpg)

###  [ cheese slippers ](<https://www.amourdecuisine.fr/article-chaussons-au-fromage.html> "Permalinks to cheese slippers")

you will find more recipes on the category: [ amuse bouche, tapas, mise en bouche ](<https://www.amourdecuisine.fr/recettes-salees/amuse-bouche-tapas-mise-en-bouche>)
