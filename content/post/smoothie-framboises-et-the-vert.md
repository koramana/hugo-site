---
title: raspberry smoothie and green tea
date: '2016-01-20'
categories:
- juice and cocktail drinks without alcohol
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-framboises-et-th%C3%A9-vert.jpg
---
[ ![raspberry smoothie and green tea](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-framboises-et-th%C3%A9-vert.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-framboises-et-th%C3%A9-vert.jpg>)

##  raspberry smoothie and green tea 

Hello everybody, 

I waited to recu the photos of my recipes that were on the hard drive of my late laptop, but voila, until today, no news of the technician who takes care of it. It's still intriguing, I do not know if he poured hot tea on my hard drive and he does not want to tell me, or he recycled it forgetting that he had to recover the data above???? One thing is sure, I curse a little the day I decided to do chemistry instead of computer science, hihihihihih yet I was too strong in this area. 

In any case, waiting to hear from this computer specialist, I am obliged to post the photos I took with my samsung, photos that I took to share on facebook ... And I start with this delicious raspberry smoothie and green tea. 

What I like in this kind of smoothie is that I have a combination of fruit and green tea, you surely know the benefits of green tea, you will not regret at all to consume it, so in taking my bowl of smoothie, I have my right dose of fruit and green tea.   


**raspberry smoothie and green tea**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-framboise-et-th%C3%A9-vert.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 1 cup of green tea prepared in advance and well chilled 
  * 100 ml natural yoghurt (0% if possible) 
  * 1 glass of frozen raspberries 
  * 1 large banana cut in slices and frozen 
  * 1 C. honey (perso I did not put, but there are people who like the sweet taste 
  * ½ teaspoon of vanilla extract 
  * ½ cup ice cubes 



**Realization steps**

  1. place all the ingredients in the bowl to be blended 
  2. mix until you have a frothy and creamy drink. 
  3. enjoy immediately 



[ ![raspberry smoothie and green tea](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smothie-de-framboises-et-th%C3%A9-vert.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smothie-de-framboises-et-th%C3%A9-vert.jpg>)
