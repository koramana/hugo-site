---
title: almond ghriba / easy almond dried cake
date: '2016-06-29'
categories:
- Algerian cakes- oriental- modern- fete aid
- gâteaux sablés, ghribiya
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/ghribia-aux-amandes-ghoriba-ghriba.CR2_1.jpg
---
![ghribia aux almonds - ghoriba - ghriba.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/ghribia-aux-amandes-ghoriba-ghriba.CR2_1.jpg)

##  almond ghriba / easy almond dried cake 

Hello everybody, 

I love ghribia, or ghriba, or ghoriba and each time, I discover another flavor that gives another dimension to this super fondant cake. 

A perfect result, a very melting ghribia, a delight, I changed the recipe a bit, and it was a delight with pronounced aromas of almond extract.   


**almond ghriba / easy almond dried cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/ghribia-aux-amandes-gateau-sec-aux-amandes-faciles.CR2_1.jpg)

Recipe type:  shortbread, Algerian cake, ghoriba  portions:  10  Prep time:  25 mins  cooking:  15 mins  total:  40 mins 

**Ingredients**

  * 150 gr butter or softened margarine 
  * 4 tablespoons of table oil 
  * 60 gr of icing sugar 
  * ½ teaspoon of almond extract 
  * 100 gr ground almonds 
  * 230 to 270 gr of flour 
  * Some whole almonds. 



**Realization steps**

  1. whip the butter, oil and sugar until you have a cream, it can last 20 minutes, be passionate, it's the secret of the success of a ghribia (I used the electric whip for this operation) . 
  2. Introduce the almond extract and continue to whip. 
  3. using a spatula, introduce the ground almonds, and collect with flour, until you have a malleable paste. 
  4. Shape the balls of the size of a walnut, and go into a well-floured maamoul mold so that the ghribia piece is easily detached. 
  5. place the ghriba in a baking tray, not buttered. 
  6. garnish if you want each piece with an almond. 
  7. cook in an oven preheated to 160 degrees C, between 10 and 15 minutes. watch the cooking is very delicate cakes. At the exit of the oven, let cool well before unmolding the ghribiya coins. 



![cake-algerien.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-algerien.CR2_1.jpg)
