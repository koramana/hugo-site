---
title: shortbread biscuits with caramelized sunflower seeds
date: '2014-05-16'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/biscuits-sabl%C3%A9-aux-graines-de-tournesol-caram%C3%A9lis%C3%A9es-2.jpg
---
![shortbread biscuits with caramelized sunflower seeds](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/biscuits-sabl%C3%A9-aux-graines-de-tournesol-caram%C3%A9lis%C3%A9es-2.jpg)

##  shortbread biscuits with caramelized sunflower seeds 

Hello everybody, 

Here are delicious biscuits sanded with caramelized sunflower seeds well-filled with a beautiful layer of caramelized sunflower seeds, a recipe super easy and not very expensive, of [ cookies ](<https://www.amourdecuisine.fr/article-les-gateaux-secs-algeriens-pour-aid-el-fitr-2012.html> "Algerian dry cakes for Eid el Fitr 2012") prepared by our dear Lunetoiles. 

I received the recipe only yesterday, and I loved so much these butter cookies that I wanted to post them right away for you, although there are other recipes waiting, and some recipes to me that I I realized quite recently. 

This recipe will be ideal for the taste of children, in addition to rich in fiber with this layer of sunflower seeds, I already see varieties of this recipe, replacing the sunflower seeds, with dried fruits, such as almonds, hazelnuts, nuts, or even the mixture. 

You can also garnish with caramelized pumpkin seeds, yumi yumi   


**shortbread biscuits with caramelized sunflower seeds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/biscuits-sabl%C3%A9-aux-graines-de-tournesol-caram%C3%A9lis%C3%A9es-1.jpg)

portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 300 g flour (type 45) 
  * 200 g of good quality butter 
  * 100 g of sugar 
  * 1 big egg 
  * 1 tablespoon of cold water (optional) 
  * caramelised sunflower seeds: 
  * 250 g sunflower seeds (shelled) 
  * 50 g of honey 
  * 40 g of sugar 
  * 40 g cream at 30% fat 
  * 40 g butter 



**Realization steps** Preparation of cookies 

  1. In a robot, equipped with the blade "K" mix the flour with the sugar, add the butter and mix until you get a texture well sanded 
  2. Add the egg and mix again until the dough forms. 
  3. If the dough does not sit, add a tablespoon of cold water. 
  4. Spread the dough on a lightly floured table, about 4 mm thick, and cut with a round dice with a diameter of about 8 cm. 
  5. Place them on a baking sheet with baking paper, and put in the preheated oven at 180 degrees C and bake for 8 to 10 minutes. 
  6. Repeat the same operation until the dough is used up. 
  7. Let the biscuits cool on a wire rack. 
  8. Meanwhile prepare the sunflower seeds. 

Prepare the caramelized sunflower seeds: 
  1. In a saucepan with a thick bottom put the honey, sugar, butter and cream. Heat to boiling, then cook, stirring constantly for 3 to 4 minutes. 
  2. Pour the sunflower seeds, still stirring, cook for a minute. 
  3. For each biscuit, put a tablespoon of sunflower and re-insert them in the oven for 7-8 minutes. 
  4. and enjoy 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/biscuits-sabl%C3%A9-aux-graines-de-tournesol-caram%C3%A9lis%C3%A9es.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/biscuits-sabl%C3%A9-aux-graines-de-tournesol-caram%C3%A9lis%C3%A9es.jpg>)
