---
title: pissaladiere with red onions
date: '2014-12-22'
categories:
- appetizer, tapas, appetizer
- pizzas / quiches / pies and sandwiches
tags:
- Onion tart
- pies
- la France
- Nice
- Amuse bouche
- Aperitif
- Salty pie

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pissaladiere-1.jpg
---
[ ![pissaladiere 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pissaladiere-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pissaladiere-1.jpg>)

##  Pissaladière with red onions 

Hello everybody, 

I am always in my humor "Summer" wanting to make recipes that recall the summer, just to forget the cold, and the clouds all gray that hides the beautiful sun. 

Today is the pissaladiere that I will share with you. You tell me the pissaladiere is with yellow onions, and I'm with you on that, but I chose to make a pissaladiere with red onions, because at this time of year, I find yellow onions of very bad qualities on the stalks, and I do not want to spoil the taste of my pissaladiere, because I have to realize it with "the" premoral ingredient, lol ... 

So we come back to this delicious pissaladière that did not last long, immediately realized, immediately devoré. 

![pissaladiere 006](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pissaladiere-006.jpg)

For the base, I opted to make my recipe fetish my [ magic pasta ](<https://www.amourdecuisine.fr/article-pate-magique.html> "Magic paste") because I do not want to have surprises after cooking ... And especially at home, everyone prefers the pizza with my magic dough ... 

You can also realize your favorite dough, which sings you the most in the ear (I would say rather to the stomach, lol ...) 

And for cooking onions, I followed the trick that passed me Lunetoiles last time during the realization of the recipe [ Algerian Coca ](<https://www.amourdecuisine.fr/article-coca-chaussons-cuisine-algerienne.html> "coca: slippers of Algerian cuisine") , after having finely cut the onions, I placed them in a salad bowl going to the microwave, I made over a drizzle of oil, I covered them with food film, and hop for 3 minutes at the micro- wave. 

Caramelizing then onions with a little oil in the pan was a breeze, I was not allowed to burn onions, and the taste was super-melting and onions well scented.   


**pissaladiere with red onions**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pissaladiere-2.jpg)

portions:  4 to 6  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients** for the pasta:   
1 cup = 250 ml 

  * 2 cups of flour 
  * 3 tablespoons milk powder 
  * 30 g of soft butter 
  * 1 tablespoon of sugar 
  * About 180 ml of warm water (depending on the absorption of the flour) 
  * 1 cup of baker's yeast. 
  * ½ teaspoon of salt   
with these ingredients you will have pissaladiere squares of 26 cm each side 

for garnish: 
  * 4 red onions 
  * 3 tablespoons of extra virgin olive oil 
  * 1 pinch of salt 
  * 1 dozen black olives 
  * 2 cloves garlic 
  * 1 bay leaf 
  * 2 branches of thyme 
  * 2 small boxes of anchovies. 



**Realization steps** preparation of the dough: 

  1. Pour the flour into a container. 
  2. Add salt, sugar and milk powder, mix. 
  3. Add the yeast, mix again. 
  4. Pour warm water and knead to obtain a smooth paste. 
  5. Add the soft butter and knead again until the butter is well incorporated into the dough. 
  6. Cover with a clean, damp cloth until the dough doubles in volume. 

preparation of the filling: 
  1. During the rest period of the dough, peel the onions and cut them into thin slices. 
  2. put in a bowl with a drizzle of oil and go to the microwave for 2 minutes maximum. 
  3. remove the microwave, place the onions in a pan with the remaining amount of olive oil, add the garlic and bay leaf. 
  4. Cook over medium-low heat until softened for 5 minutes. 
  5. Add some thyme leaves and remove from heat. 

Preparation of the pissaladiere: 
  1. take the dough on a floured plan and degas it with your hands to release the air incorporated. 
  2. Form a square 8mm thick and place it on a well-oiled plate. 
  3. Spread the onion stuffing and garnish with anchovy fillets to make a grill and place an olive cut in half in the center. 
  4. Preheat the oven to 180 ° C. Brush the pissaladière with olive oil and bake in the bottom of the oven. Watch for the cooking. 
  5. Serve warm or at room temperature. 



Note trick:   
If you do not like the smell of onions remaining in your hands, rub a little toothpaste in your hands, wash and look at the miracle ... You're sure you'll go out and nobody will smell the onion in your hands. [ ![pissaladiere 042](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pissaladiere-042.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pissaladiere-042.jpg>)
