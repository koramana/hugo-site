---
title: strawberry streusel bars
date: '2014-05-15'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/bars-streusel-aux-fraises.jpg
---
[ ![strawberry streusel bars](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/bars-streusel-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/bars-streusel-aux-fraises.jpg>)

##  Streusel strawberry bars 

Hello everybody, 

I fell in love with these streusel Strawberry Strawberries Lunetoiles, but I lingered to publish the recipe, because frankly the list of recipes Lunetoiles becomes longer and longer, and I'm really sorry that I do not I do not share his recipes with you as much as possible, but I try to do my best ... 

So today is the recipe for these streusel bars fresh, well melting. You will also find on the blog, the recipe of [ strawberry bars ](<https://www.amourdecuisine.fr/article-barres-aux-fraises-crumble-aux-fraises.html> "Strawberry Bars - Strawberry Crumble") , another lunetoiles recipe she shared with us a while ago. 

This recipe is not as complicated as you think, because the base of the bars and the streusel is the same preparation, and the strawberry stuffing is very simple to realize, you can also use any other fruits of seasons instead strawberries, and this recipe will be present on your tables to taste throughout the year ... 

[ ![strawberry streusel bars](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/bars-streusel-aux-fraises-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/bars-streusel-aux-fraises-2.jpg>)

We go then for this recipe and these ingredients, and if you have ideas of pranks, keep me informed of the result ...   


**strawberry streusel bars**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/bars-streusel-aux-fraises-1.jpg)

portions:  30  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients** for the base and the streusel: 

  * 230 g + 5 tablespoons unsalted butter, cut into pieces 
  * 150 gr of powdered sugar 
  * 40 gr of icing sugar 
  * ½ teaspoon of salt 
  * 2 egg yolks, slightly beaten 
  * ½ teaspoon of almond extract 
  * 350 g + 3 tablespoons all-purpose flour 

for the strawberry filling: 
  * 525 gr strawberries 
  * 135 g caster sugar or more if necessary 
  * 60 ml fresh lemon juice 
  * 3 teaspoons of cornstarch 
  * 2 tablespoons water (Optional) 



**Realization steps**

  1. Butter a 22x33 cm pan (I use a slightly larger pan) or garnish with greaseproof paper, leaving a slight surplus along the edges. 

Prepare the filling: 
  1. In a large saucepan, mix strawberry pieces, sugar, lemon juice, cornstarch and water (if necessary). 
  2. Mix well. 
  3. Boil the water. 
  4. Once it begins to boil, reduce the heat to medium low and cook, stirring occasionally, until the liquid turns into a thick syrup (about 8-10 minutes). 
  5. Remove the pan from the heat and allow to cool to room temperature. {Note:. It will continue to thicken as it cools down} 
  6. Let take, and cool about 40 minutes. 

Prepare the dough: 
  1. In a large saucepan, melt the butter over low heat. Increase the heat to medium and, stirring occasionally, cook the butter until it is golden and develops a nutty aroma. Depending on the heat of the fire, it may take 5-8 minutes.   
During this process, the butter will first become sparkling before changing color, making it difficult to see what is happening. Just be sure to take a look from time to time so that the butter does not burn. 
  2. Once it is beautiful and fragrant, remove the pan from the heat and pour the butter into a heatproof bowl. Let cool to room temperature. 
  3. In a large bowl, mix cooled hazelnut butter, 150 g of caster sugar and salt. 
  4. Stir until well blended. 
  5. Add egg yolks and almond extract, beat until smooth. 
  6. Using a large spatula or wooden spoon, add the flour until you have a firm dough. 
  7. Transfer about 2-thirds of dough into the pan lined with parchment paper and press down evenly on the bottom. 
  8. Refrigerate the dough for about 40 minutes. 

Steusel: 
  1. Add the icing sugar to the remaining dough, and use the fingertips, work the sugar into the dough until it becomes friable and sand. 
  2. Cover and set aside. 
  3. Place an oven rack on the second bottom slot. 
  4. Preheat the oven to 160 ° C. 
  5. Remove the dough from the refrigerator after the dough rested for 40 minutes and prick the dough several times with a fork.   
{Note: Resting the dough and pricking it will prevent it from blowing too much in the oven while cooking, do not omit these steps.} 
  6. Place the pan on the bottom rack of the oven and bake for about 15 minutes, until the crust begins to set. 
  7. Do not worry if the center always looks pasty, it is best not to overcook at this point. 
  8. Remove the partially cooked dough from the oven and place it on a cooling rack. 
  9. Increase the oven to 175 ° C. 
  10. Spread the strawberry mixture over the partially cooked crust. 
  11. Sprinkle the remaining streusel on the fruits. 
  12. Cook 25-30 minutes, or until golden. 
  13. Remove from the oven and place on a cooling rack. 
  14. let cool for at least 3 hours before removing the mold bars (use excess paper to remove from the mold). 
  15. Cut into squares - the size you prefer. 
  16. Enjoy! 



[ ![strawberry streusel bars](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/bars-streusel-aux-fraises-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/bars-streusel-aux-fraises-3.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
