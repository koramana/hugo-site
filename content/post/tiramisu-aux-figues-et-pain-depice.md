---
title: tiramisu with figs and gingerbread
date: '2012-10-09'
categories:
- boissons jus et cocktail sans alcool
- Cuisine détox
- Cuisine saine

---
Hello everybody, 

tiramisu is one of the desserts I love most, and that I can vary in any sauce so much I like mascarpone and the taste of coffee. 

this time I paired with this delicious Italian cheese, the fig I love and the gingerbread, so I can not tell you how much I like. 

so you imagine the result with all these tastes. 

a slaughter ... I assure you. 

so we start with the ingredients: 

  * 400 grs of mascarpone 
  * 4 eggs 
  * 150 grams of sugar 
  * 12 slices of gingerbread 
  * 1 big cup of black coffee 
  * Figs according to your taste. 



method of preparation: 

  1. clean the figs, wipe them, and cut them into cubes, keep some figs for the deco. 
  2. beat the sugar with the egg yolks, until the mixture whitens. 
  3. add the mascarpone and whip 
  4. turn the egg whites into firm snow. 
  5. gently stir in the previous mixture. 


  1. remove crusts of gingerbread cut in medium size. 
  2. soak the pieces of black coffee gingerbread. 
  3. put a layer of gingerbread in your verrines 
  4. cover with a layer of the mascarpone mixture 
  5. place a layer of figs in a cube. 
  6. cover again with the mascarpone preparation. 
  7. decorate with figs according to your taste. 
  8. reserve in the fridge for 4 hours before eating, 



and I know who will not resist ... 

I hope you like this recipe like me. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
