---
title: Oranais / glasses with apricots
date: '2011-06-29'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cheesecakes et tiramisus
- dessert, crumbles and bars
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/06/millesf-010_thumb.jpg
---
I really like this delicacy, and more recently, we saw it everywhere on blogs, and as I'm working in the house, an easy delight as it is prepared in minutes, it is surely welcome . the only problem for me was the apricots, I had to prepare the dough, the pastry cream, when I open the box to garnish my cakes, I was surprised by the size of the apricots. they were very small, they look like dried apricots and put in the syrup, too late it was already 22h, and I was not going to get out my ... 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

I really like this delicacy, and more recently, we saw it everywhere on blogs, and as I'm working in the house, an easy delight as it is prepared in minutes, it is surely welcome . 

the only problem for me was the apricots, I had to prepare the dough, the pastry cream, when I open the box to garnish my cakes, I was surprised by the size of the apricots. 

they were very small, they look like dried apricots and put in the syrup, too late it was already 22h, and I was not going to get out my husband for a box of apricot. 

Ingredients: 

\- puff pastry 

\- pastry cream (recipe  [ right here  ](<https://www.amourdecuisine.fr/article-25345351.html>) ) 

\- apricots (not as small as mine it will be ugly) 

simple is easy, cut the puff pastry square. garnish with a nice spoon of custard, and with two slices of apricots. 

bring back the two free ends to cross on one another, brush with egg white so that it does not open. 

then garnish with egg yolk + vanilla to avoid the bad smell of eggs, and sprinkle with sugar. 

place in a tray covered with baking paper. 

bake in a preheated oven for 15 to 20 minutes, depending on your oven. 

and treat yourself, 

* * *

for other Western pastry recipes, click on the photo of your choice: 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/millesf-010_thumb.jpg) ](<https://www.amourdecuisine.fr/article-41158909.html>) [ ![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/545x430/2/42/48/75/API/2009-11/) ](<https://www.amourdecuisine.fr/article-39444151.html>) [ ![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x416/2/42/48/75/Images/) ](<https://www.amourdecuisine.fr/article-53001970.html>)
