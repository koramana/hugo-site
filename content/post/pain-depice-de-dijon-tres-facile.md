---
title: very easy Dijon gingerbread
date: '2012-12-05'
categories:
- jams and spreads
- dessert, crumbles and bars
- sweet recipes
tags:
- Gingerbread
- Christmas
- Bread
- Spice
- Celebration meal
- Cakes
- Honey
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/pepi1_thumb1.jpg
---
##  very easy Dijon gingerbread 

Hello everybody, 

The recipe is simple, the ingredients are within reach of everyone, and the result is impeccable. 

**very easy Dijon gingerbread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/pepi1_thumb1.jpg)

portions:  12  Prep time:  10 mins  cooking:  45 mins  total:  55 mins 

**Ingredients**

  * 250 gr of complete wheat flour 
  * 150 gr of powdered sugar 
  * 4 C a Honeysuckle 
  * 1 C a Soup bicarbonate of baking soda 
  * 1 C a Domed Soup of Spice Mix for Gingerbread 
  * 250 ml of milk 



**Realization steps**

  1. Preheat the oven to 175 ° C. 
  2. Mix the spices and the sugar. 
  3. Add the honey. Add the boiling milk and mix well. 
  4. Stir in flour and baking soda without lumps. 
  5. Pour into a cake tin and bake for 45 minutes. 
  6. Demould after cooling the gingerbread. 



frankly try it you are not going to be disappointed. 

merci 
