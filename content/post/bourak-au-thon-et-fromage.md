---
title: Bourak with tuna and cheese
date: '2014-04-03'
categories:
- Bourek, brick, samoussa, slippers

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/bourak-au-thon-et-fromage_thumb.jpg
---
[ ![bourek-au-thon-and-cheese thumb](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/bourak-au-thon-et-fromage_thumb.jpg) ](<https://www.amourdecuisine.fr/article-bourak-au-thon-et-fromage.html/bourak-au-thon-et-fromage_thumb>)

**B** we have everyone, 

Here is a recipe that will embellish your tables of ramada, these bouras with tuna and cheese are a delicacy crispy and melting at once, something that will revive your flavors and tastes .... This is a recipe that comes from a reader very passionate about cooking Nacy ... thank you to her. 

and if you also have recipes that you want to share with everyone, click on this link: 

[ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

**Bourak with tuna and cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/bourak-au-thon-et-fromage_thumb-300x224.jpg)

**Ingredients**

  * six leaves of leaves of bricks 
  * half of a grated onion 
  * two hard-boiled eggs cut into small pieces 
  * 3 tablespoons chopped parsley 
  * a small box of tuna 
  * a fillet of lemon juice, 
  * salt pepper 
  * 50 g grated cheese to mix with the stuffing or by default a slice of melted cheese on each sheet of dioul 
  * oil for frying 



**Realization steps**

  1. mix all the ingredients of the stuffing 
  2. season with lemon salt and pepper 
  3. garnish the leaves with diouls and form cigars 
  4. fry in a hot oil bath 
  5. and that's ready 



[ ![bourek-au-thon-and-cheese thumb](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/bourak-au-thon-et-fromage_thumb.jpg) ](<https://www.amourdecuisine.fr/article-bourak-au-thon-et-fromage.html/bourak-au-thon-et-fromage_thumb>)

merci a Nacy pour cette recettes 
