---
title: Algerian donut, Sfenj
date: '2012-06-03'
categories:
- cuisine diverse
- Healthy cuisine
- recettes aux poissons et fruits de mer
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignet-khefaf-067-2_thumb1.jpg
---
##  Algerian donut, Sfenj 

Hello everybody, 

Algerian donut, Sfenj, a delight of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , done in different ways and according to the tradition of each family, 

Usually, I make my semolina-based donuts only, I really like the crispness that gives to the outer layer covering the mellow sfenj. But in this recipe, I added a little flour for an even finer and airy effect.   


**Algerian donut, Sfenj**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignet-khefaf-067-2_thumb1.jpg)

**Ingredients**

  * 375 g of flour 
  * 180 g medium or fine semolina 
  * Instant yeast coffee 
  * ½ teaspoon salt 
  * 1 egg 
  * lukewarm water 
  * 1 tablespoon of sugar 
  * frying oil 



**Realization steps**

  1. In a large bowl or kneader or bread machine put the flour, semolina, yeast, salt and egg 
  2. add warm water to wet well 
  3. pick up the dough (neither too hard nor too soft) 
  4. leave a few moments 
  5. Then gently wet your dough, working with the palms of your hands until you get a very soft and elastic dough 
  6. cheat your hands in lukewarm water and take the dough with your fingertips to bend it violently in this way you will air your dough well. 
  7. Let the dough rest in a large pot 
  8. let it rise for 1 hour 
  9. Put a deep pan on the fire filled with ¾ oil, let warm (very important) 
  10. take a bowl filled with water, dip your fingers in it 
  11. take the value of a large egg dough stretched between the fingers gently and quickly to give it a length or round shape according to your choice 
  12. Put in the pan, sprinkle with hot oil, brown the first side then turn to brown the 2nd side. 
  13. do the same way until the dough is exhausted 



Note: 

You can put all these ingredients in the bread machine, but be careful, you must monitor the amount of water (add warm water gently) 

# 

Merci pour votre visite. 
