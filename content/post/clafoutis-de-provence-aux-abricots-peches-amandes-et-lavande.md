---
title: Clafoutis de Provence with apricots, peaches, almonds and lavender
date: '2016-07-16'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/06/dsc046101-600x401.jpg
---
**Clafoutis de Provence with apricots, peaches, almonds and lavender**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/dsc046101-600x401.jpg)

portions:  6  Prep time:  15 mins  cooking:  60 mins  total:  1 hour 15 mins 

**Ingredients**

  * 5 eggs 
  * 40cl of skimmed milk 
  * 15 cl of fermented milk 
  * 120 gr Bourbon vanilla flavored sugar 
  * 180 gr of flour 
  * 6 peaches 
  * a dozen small ripe apricotsAlmonds 
  * Lavender 
  * Icing sugar 



**Realization steps**

  1. Preheat the oven to 165 ° C, ventilated heat. 
  2. Butter and sprinkle with sugar a rectangular mold. 
  3. In a salad bowl, mix the flour with the sugar. Stir in the skim milk and fermented milk, mix a little, and add the eggs. 
  4. Beat until you have a dough of a consistency quite liquid and without lumps. 
  5. Arnir 
  6. Wash apricots and peaches. Peel the peaches and cut them in quarters. 
  7. Cut the apricots in half, and remove the stones. 
  8. Store the apricots and peach quarters in the dish. 
  9. Pour the dough, sprinkle with whole almonds and lavender. 
  10. Cook for 1 hour at 165 ° C. 
  11. At the exit of the oven, allow to cool. 
  12. Book fresh. Before tasting, sprinkle with icing sugar. 


