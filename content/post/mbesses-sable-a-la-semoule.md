---
title: Mbesses shortbread with semolina
date: '2015-03-02'
categories:
- Algerian cuisine
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya
tags:
- Algerian cakes
- desserts
- Confectionery
- Semolina Cake
- Algeria
- Pastry
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/mbesses.jpg
---
[ ![mbesse sablé with semolina](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/mbesses.jpg) ](<https://www.amourdecuisine.fr/article-mbesses-sable-la-semoule.html/mbesses>)

##  Mbesses shortbread with semolina 

Hello everybody, 

Mbesses shortbread is a delicious Algerian cake known as mtheqba in areas where Mtakba or Mbessa. 

The semolina shortbread is very similar to [ mbardja, or bradj ](<https://www.amourdecuisine.fr/article-bradj-losanges-de-semoule-aux-dattes.html> "Bradj semolina lozenges with dates") but mbesses or mtheqba is not stuffed with date paste. 

The naming of the mbesses comes from the amount of fat that this cake contains so it is so sanded, that's why we can consider this cake as semolina shortbread crispy fondants. 

Now the cooking of these Mbesses shortbread semolina is your choice, you can cook them with split tajine or pancake like bradjs, or fry them in a bath of hot oil ... And this is the method that adopted Nawel Zellouf for the preparation of these Mbesses shortbread with semolina. 

Thank you Nawel Zellouf for sharing.   


**Mbesses shortbread with semolina**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/mbesses.jpg)

portions:  30  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 2 bowls of semolina. 
  * ½ bowl of butter or smen 
  * Water to pick up the mixture 
  * Salt 



**Realization steps**

  1. In a gas3a (large terrine) mix semolina, salt and butter 
  2. scrub well between hands to have a well blasted mixture   
me personally I put more than half of the bowl of butter because my semolina was not well sanded 
  3. let stand for 5 min then collect the dough with a little water. 
  4. lower the dough with a rolling pin and cut as you like with a knife to make small squares, otherwise use a cookie cutter. 
  5. you have the choice to fry these cakes, or cook them on a Tajine. 
  6. After cooking, cheat the cake in honey. 


