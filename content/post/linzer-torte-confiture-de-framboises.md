---
title: linzer torte / raspberry jam
date: '2014-12-22'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/linzer-torte-ala-confiture-de-framboise_thumb1.jpg
---
##  linzer torte / raspberry jam 

Hello everybody, 

Yet another Linzertorte, or Linz pie this time, is a Linzer torte with raspberry jam ... a pie that will really make you happy. 

a tart very easy to prepare, the most difficult will be to prepare the dough and decorate, but with Lunetoiles, we feel that this pie is a breeze, so it does it perfectly. 

you can see his first [ linzer chocolate torte ](<https://www.amourdecuisine.fr/article-tarte-de-linz-au-chocolat-et-a-la-confiture-de-mures-maison-81695831.html>) . 

**linzer torte / raspberry jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/linzer-torte-ala-confiture-de-framboise_thumb1.jpg)

portions:  8  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 130 g of butter, at room temperature 
  * 130 g caster sugar 
  * the zest of 1 lemon 
  * 1 teaspoon of vanilla 
  * 1 large egg, at room temperature 
  * 130 gr of white almond powder 
  * 190 gr of flour 
  * 1 teaspoon baking powder 
  * 500 raspberry jam 
  * garnish (1 egg + 1 tablespoon milk) 



**Realization steps**

  1. whip in cream, together the butter and sugar in a mixer. Add lemon zest and vanilla. 
  2. Add the egg and beat until smooth. 
  3. Sift together almond powder, flour and baking powder. 
  4. Add to the butter mixture in three times. Mix until the dough is standing. 
  5. Flatten the dough into a small rectangle and wrap it in plastic. Let cool in the refrigerator for 15 minutes. 
  6. Once the dough has cooled slightly, cut ⅓ of the dough and place in the refrigerator. Take the remaining et and roll on a floured table. 
  7. Spread the dough evenly on the table and transfer to the pie pan with a removable bottom. 
  8. Spread the raspberry jam on the pie shell. 
  9. Take the remaining de of dough from the refrigerator. Spread on a floured table in a rectangle. Using a roulette, cut strips of the desired width. 
  10. Place the strips over the raspberry jam. 
  11. Place the pie in the freezer for 15-20 minutes to cool the dough. (The pie can also be made a few days in advance and stored in the refrigerator before cooking). 
  12. Preheat the oven to 180 degrees C. Prepare the egg gilding. Using a pastry brush, brush a thin, even layer over the pie. 
  13. Bake for 35-40 minutes. Check the cooking every 15-20 minutes to make sure it does not burn. If it browns too quickly, cover the dough with aluminum foil. 
  14. Let the pie cool completely before removing the pie shell. 



you can also see: 

[ the chocolate linz pie / jam of blackberries: ](<https://www.amourdecuisine.fr/article-tarte-de-linz-au-chocolat-et-a-la-confiture-de-mures-maison-81695831.html>)

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/linz-chocolat-111.jpg) ](<https://www.amourdecuisine.fr/article-tarte-de-linz-au-chocolat-et-a-la-confiture-de-mures-maison-81695831.html>)
