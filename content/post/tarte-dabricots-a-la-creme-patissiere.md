---
title: apricot tart with cream pastry
date: '2015-08-18'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/08/tarte-aux-abricots-et-creme-patissiere-1-685x1024.jpg
---
![apricot pie with cream](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/tarte-aux-abricots-et-creme-patissiere-1-685x1024.jpg)

##  apricot tart with cream pastry 

Hello everybody, 

Yes it is the period of apricots, but you do not have to wait for apricots season to make this recipe, this apricot pie with cream pastry is a recipe made with dried apricots. 

This recipe is a lunetoiles, and it is on my email for over a year already, I had already published a recipe for apricot pie but has the cream frangipane: 

[ custard tart with almond cream ](<https://www.amourdecuisine.fr/article-tarte-aux-abricots-et-creme-d-amande.html> "apricot tart and almond cream")

so follow me for this recipe 

**apricot tart with cream pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/tarte-aux-abricots-a-la-creme-patissiere-1024x685.jpg)

portions:  8  Prep time:  15 mins  cooking:  50 mins  total:  1 hour 5 mins 

**Ingredients** For sweet pie dough: 

  * 250g of flour 
  * 90 g of sugar 
  * 100 g of butter 
  * 4 egg yolks 

For pastry cream: 
  * 5 egg yolks 
  * 150 g caster sugar 
  * 1 sachet of vanilla sugar 
  * 75 flour 
  * 75 cl of milk 

For garnish : 
  * about twenty dried apricots (previously soaked in water to rehydrate them for 24 hours and well drained) 



**Realization steps** Prepare the pie dough: 

  1. Put the flour and sifted icing sugar in a bowl. Add the ointment butter and squeeze it. 
  2. When there are no more large pieces of butter, stir in the egg yolks. Keep cold for 30 minutes. Lower to 3 mm thick on a floured work surface and darken a circle 28 cm in diameter. Pass around the edge with the roller to drop the excess dough. 

Prepare the pastry cream: 
  1. Put the milk to heat in a saucepan until boiling. 
  2. Beat the egg yolks with the sugar and vanilla sugar, until the mixture whitens. Then add the flour and beat again. 
  3. When the milk is boiling, pour it over the egg / sugar / flour mixture, and never stop whipping. Beat until smooth and smooth. 
  4. Pour everything back into the saucepan and put back on the heat. Reporter has boiling without ever stop whipping. When the cream has thickened (count 1 minutes from boiling, remove from heat, place in a bowl, and let cool.) Cover the cream with cling film to prevent skin from forming. 
  5. When the cream has cooled, spread it over the bottom of dough, then place the apricots in the cream. 
  6. Then bake for 10 minutes at 210 ° C, then lower the oven temperature to 160 ° C and continue cooking another 50 minutes. 
  7. Serve cold or at room temperature. 



[ ![apricot pie with cream](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/tarte-aux-abricots-et-creme-patissiere-2-666x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/tarte-aux-abricots-et-creme-patissiere-2.jpg>)
