---
title: Muffins with pralinoise
date: '2014-02-25'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/muffins-%C3%A0-la-Pralinoise-711x1024.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/muffins-%C3%A0-la-Pralinoise-711x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/muffins-%C3%A0-la-Pralinoise.jpg>)

##  Muffins with pralinoise 

Hello everybody, 

I'm still not back to my kitchen after my delivery. you know that a baby is a big responsibility, and I'm sorry if I still have not responded to your comments .... 

I enjoy these moments with my baby, irreplaceable and irretrievable moments ... 

Yesterday was the first night that my little angel let me sleep for two hours in a row, lol ... And I write you a recipe that Lunetoiles because baby sleeps after a nice shower .... 

Lunetoiles shares with us these little muffins to the pralinoise ... what it has appeared mellow in the photo ... and what I want one or two muffins with a good bowl of coffee at milk…. yum yum.   


**Muffins with pralinoise**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/muffins-%C3%A0-la-Pralinoise-2-685x1024.jpg)

Recipe type:  Dessert  portions:  12  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 200 g of chocolate pralinoise 
  * 220 g flour 
  * 100 g caster sugar 
  * 1 sachet of baking powder 
  * 2 pinches of salt 
  * 2 whole eggs 
  * 130 g of walnut kernels 
  * 150 ml of milk 
  * 100 ml of oil 
  * 1 tbsp. coffee vanilla extract 



**Realization steps**

  1. Preheat oven to 200 ° C (thermostat7 / 8) 
  2. Mix the flour, yeast, sugar and salt. 
  3. In another bowl, combine the milk, eggs and oil. 
  4. Mix the 2 preparations to obtain a homogeneous paste. 
  5. Add the crushed nuts. 
  6. Melt the chocolate pralinoise in the microwave in two passes of thirty seconds, and add it to the dough. 
  7. Fill the prepared muffin cups with paper boxes and bake for about 15 to 20 minutes. 
  8. Let stand for 5 minutes before unmolding. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/muffins-%C3%A0-la-Pralinoise-001-1024x685.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/muffins-%C3%A0-la-Pralinoise-001.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
