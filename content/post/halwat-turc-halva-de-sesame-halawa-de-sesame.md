---
title: Turkish halwat (sesame halva, sesame halawa)
date: '2008-02-08'
categories:
- basic pastry recipes
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219074061.jpg
---
![S7302518](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219074061.jpg)

Turkish halwat, halwat ettork حلوة الترك 

my friend Imane asked me to translate a recipe from English to French, and my surprise was the recipe for the Turkish halwa. 

who is what does not like this little fondant delicacy in the mouth ????? 

I would never say no to such a flavor, and to be able to make it home, would be a beautiful thing. 

so I started in the experience, with my ingredients: 

  * 200 gr of sugar 

  * 100 ml of water 

  * juice of 1/2 lemon 

  * Aroma to your taste 

  * 340 gr of tahini (sesame puree) 




![halwa](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/218266591.jpg)

In truth to proceed one must have a sugar thermometer, I did not have any, because the temperature of sugar affects the final aspect of your halva. 

first boil the mixture of sugar, water and lemon with the aroma of your choice at 115 Degrees C 

![halwa1](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/218284561.jpg)

put the side mix to cool a little and make your tahini heat up to 50 degrees C, 

add the tahini to the above mixture, mix well by whisking at first, and then knead with a good spoon when the dough begins to harden. 

![halwa2](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/218292081.jpg)

the more you work the dough while it is hot the better your halva will be. 

![halwa3](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/218293581.jpg)

put the halva in an oiled cake box or a mold with a removable base, after cool, cover your halva, and place it in the refrigerator for 36 hours 

![halwa4](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/218295111.jpg)

I want to say that my halva was very good, with rather the texture of a carambar or a little caramel 

so rather elastic than a paste that crumbles to the touch 

![S7302517](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219074361.jpg)

it may be because of the absence of the sugar thermometer 

![S7302519](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219075111.jpg)

if you try the recipe, and you succeed, tell me. 

your comment I ferrons enormously pleasure. 
