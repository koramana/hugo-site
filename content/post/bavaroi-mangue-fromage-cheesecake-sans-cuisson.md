---
title: Bavarian mango / cheese (cheesecake without cooking)
date: '2010-03-31'
categories:
- Chhiwate Choumicha
- cuisine algerienne
- cuisine diverse
- ramadan recipe
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/03/bavarois-mangue2.jpg
---
Bavarian mango hello everyone, here is a delicious Bavarian mango, a delicacy with each mouth, melting and unctuous at the same time, with this unique taste of mango. the recipe is very simple is super good delicious: for the base: 50 grs of coconut 150 grs of biscuits 70 grs of melted butter grilling the coconut, just a few minutes because it burns quickly. in a blinder, mix the biscuits with the coconut, to have a fine mixture, then add the melted butter and to mix again. pour this & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

#  Bavarian At The Mango 

![Bavarian-mangue.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/bavarois-mangue2.jpg)

Hello everybody, 

here is a delicious Bavarian mango, a delicacy with each mouth, melting and unctuous at the same time, with this unique taste of mango. 

the recipe is very simple is super good delicious: 

for the base: 

  * 50 grams of coconut 
  * 150 grams of biscuits 
  * 70 grs of melted butter 



grill the coconut, just a few minutes because it burns quickly. 

in a blender, mix the biscuits with the coconut, to have a fine mixture, then add the melted butter and to mix again. 

pour this mixture into a mold with removable base, or then in a pastry circle place on a plate, and heap well with the back of a spoon, place in the refrigerator. during the preparation of the cheese mousse. 

Ingredients of the cheese mousse: 

  *     * 250 ml thick cream 
  * 200 grams of unsalted cheese 
  * 75 grams of sugar 
  * 6 grams of powdered gelatin or foil. 
  * mango or peach cut into pieces 
  * the juice of a mango or peach + 2 grams of gelatin for the mirror 



whip the cream with 30 grams of sugar to make a nice mousse. 

whip the cheese with the remaining sugar, add the mango cut in pieces or the peaches (according to your taste) 

Melt the gelatin in a little water on a bain-marie, add the melted gelatine to the cheese mixture, and slowly add the whipped cream without breaking it, pour this mixture on the biscuit base, and return to the fridge. 

Now prepare the mango mirror, placing the mango juice with a little gelatin in a water bath, until the mixture is completely dissolved. 

and pour it gently on your cheese-cake. 

leave it cool, and enjoy 

![Bavarian-mangue1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/bavarois-mangue11.jpg)
