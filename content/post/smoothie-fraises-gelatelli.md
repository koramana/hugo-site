---
title: strawberry smoothie gelatelli
date: '2014-06-22'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-et-gelatelli-1.CR2_.jpg
---
##  [ ![smoothie strawberries and gelatelli 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-et-gelatelli-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-et-gelatelli-1.CR2_.jpg>) strawberry smoothie gelatelli 

Hello everybody, 

A smoothie with strawberries and gelatelli that I forgot to post, lol ... maybe there are still some strawberries on the market (in England there's always some), so do not miss this smoothie, because I promise it's a real treat ... 

I often make this smoothie with canned ice cream, but this time I preferred to make it with ice cream cone, called Gelatelli ... 2 small cornets that were in the fridge for a while now, and that the kids did not want to eat, so solution a smoothie ... 

in addition the wafers of the cone gives a special taste to gelatelli strawberry smoothie, especially if you do not mix the mixture too much, and that there are some crisp pieces of cornets .... 

that's why, you have to choose very crispy cornets, and you have to present the smoothie right away. 

**strawberry smoothie gelatelli**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-et-gelatelli-2.CR2_.jpg)

portions:  2 - 3  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 2 bananas 
  * 2 gelatelli (ice cream cone with strawberries)   
[ ![gelatelli ice cream with strawberry](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/gelatelli-fraises.bmp-300x201.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/gelatelli-fraises.bmp.jpg>)
  * 2 jars of natural yoghurt (300 ml) 
  * 2 handful of strawberries 



**Realization steps**

  1. first we start with the strawberries: wash and squirt the strawberries 
  2. crush them with a fork 
  3. fill with this puree the bottom of 3 medium glasses, and place them in the fridge 
  4. in the bowl of a blinder, place the cut cream cones (the cream and the cornet) 
  5. add the sliced ​​bananas, and the yogurts 
  6. operate the blinder to have a smooth mixture. 
  7. fill the verrines delicately, and serve immediately. 



##  [ ![strawberry smoothie and gelatelli.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-et-gelatelli.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-et-gelatelli.CR2_.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

thank you 

E-mail * 
