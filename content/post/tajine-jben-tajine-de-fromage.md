---
title: Tajine jben Tajine Cheese
date: '2018-04-18'
categories:
- Tunisian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/tajine-jben-ou-tajine-au-fromage.CR2_.jpg
---
![tajine jben or tajine with cheese.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/tajine-jben-ou-tajine-au-fromage.CR2_.jpg)

#  Tajine jben Tajine Cheese 

Hello everybody, 

Here is a delicious Tunisian tajine, very melting and very rich in taste, to chew up your taste buds, to eat without moderation, lol ... It is promised on my part that it is a recipe that you will succeed closing eyes. 

This tajine Tunisian jben, or tagine cheese is a very easy entry to make, it differs a lot from tajine jben Algerian, that I will share with you very soon. 

This recipe is ideal to be presented on your tables of the month of Ramadan, and it accompanies super nice soup, and a very fresh salad. 

**Tajine jben Tajine Cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/tajine-jben-tunisien.CR2_.jpg)

portions:  8 

**Ingredients**

  * 3 chicken legs 
  * 1 onion 
  * 2 heads of garlic 
  * ¼ teaspoon of paprika 
  * ½ teaspoon coriander powder 
  * ¼ teaspoon of turmeric 
  * Salt pepper 
  * ½ bunch of parsley 
  * 150 gr grated gruyere 
  * 2 medium sized potatoes. 
  * 5 to 6 eggs (depending on the size) 
  * 1 glass of water 
  * olive oil 



**Realization steps**

  1. In a pot, put the chicken with all the spices, the oil and the glass of water. Cook until reduced of broth. 
  2. When the chicken is cooked, crumble it into small pieces. 
  3. In a skillet, put the diced onion, crushed garlic and sauté with a little olive oil. 
  4. Add the crumbled chicken, 2 or 3 tablespoons of the cooking broth and cook until the sauce is completely reduced. 
  5. Add chopped parsley and coriander and let cool. 
  6. Fry the potatoes and drain them on paper towels. 
  7. then mix the grated cheese, potatoes and chicken. Correct the seasoning if necessary, 
  8. break the eggs one by one. Mix well. Pour this mixture into either a cake pan, shortbread pan or buttered muffin molds. 
  9. Preheat the oven to 180 degrees C and cook for about 35 minutes. 
  10. Remove from oven and allow to cool before unmolding. 



[ ![tajine jben or tajine djben.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/tajine-jben-ou-tajine-djben.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/tajine-jben-ou-tajine-djben.CR2_.jpg>)
