---
title: Pear chocolate fondant
date: '2015-12-03'
categories:
- dessert, crumbles and bars
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/fondant-chocolat-et-poires-1.jpg
---
[ ![Chocolate fondant pears 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/fondant-chocolat-et-poires-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/fondant-chocolat-et-poires-1.jpg>)

##  Pear chocolate fondant 

Hello everybody, 

I had to prepare a recipe based on pears, besides I had found sublime recipes, but here for two weeks, in our buildings is the work of piping, and even my cook is in the living room. Fortunately, my cook is electric, at 17h when the plumbers go out I gently branch the stove to do the dinner. Although I make cakes and desserts, to be honest, I can not get out my camera and do all the staging to take pictures. Moreover for a moment it is with my samsung galaxy that I make the photos, especially those of my smoothies. 

In any case, not to miss this edition, I publish a recipe for Lunetoiles based on pears, a delicious melting chocolate pears, and here is what she tells us about this recipe: 

here is a  melting  the  chocolate  and to  pears  in the form of a cake, the peculiarity is that it is with  pears  whole, so it's beautiful visually. He is very good, not very sweet, and he is well  melting  at heart, with the  perry  It's a delight. It's hard to settle for just one piece. 

[ ![Fondant chocolate pears 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/fondant-poires-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/fondant-poires-chocolat.jpg>)   


**Pear chocolate fondant**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/fondant-chocolat-poires-3.jpg)

portions:  12  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients**

  * 200 gr of dark chocolate dessert 
  * 100 gr of ground almonds 
  * 60 gr of cornflour 
  * 60 gr of sugar 
  * 4 eggs 
  * 120 gr of butter 
  * 1 sachet of baking powder 
  * 4 small seasonal pears 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. Melt the butter with the dark chocolate in a bain-marie. 
  3. In a bowl whisk the eggs with the sugar. 
  4. Add the almond powder, cornflour and baking powder and mix well. 
  5. Stir in the melted chocolate with the butter. 
  6. Pour the dough into a cake mold lined with baking paper (to facilitate the demolding). 
  7. Arrange the small peeled pears whole. 
  8. Bake for 35 minutes at 180 ° C. 
  9. Cool completely on a wire rack, then cool for several hours before unmolding and cutting parts. 



[ ![Fondant chocolate pears 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Fondant-chocolat-poires.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/fondant-poires-chocolat.jpg>)

En tout cas, voila la liste des particpantes a cette ronde: 
