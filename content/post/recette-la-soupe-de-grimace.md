---
title: recipe the Grimace Soup
date: '2016-07-15'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/soupe-de-grimace.jpg
---
![grimace soup](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/soupe-de-grimace.jpg)

##  recipe the Grimace Soup 

Soup recipe with a grimace: 

100gr of our recipes   
100gr of our photos   
200 gr of our time   
300 gr of our passion   
1 pinch of know-how   
1 spoon of our desires 

Simmer with our love   
Reduce our efforts to nothing   
Stir the knife in the wound from time to time   
Crush our pride and serve cold.   
Enjoy with a big glass of bitterness. 

Dear readers, 

Today a very funny recipe ....   
That of the sadness and anger of some 262 culinary blogs listed to date.   
Yes it's huge ... The term industrial was used.   
A hosting site, aspires, PIRATE, without our knowledge the content of our blogs.   
Besides reader, please, by pity even, look at the top of the page in the URL bar ...   
If it contains Recette.land or Cuisine.land (because I speak of them well ...) is that you are not on the blog ...   
And that's the problem. As in addition to copy our recipes, they take the design, you think to be on the blog ...   
And so you do not come to visit us ... But we do all our blogs by passion ...   
Time, personal investment, rigor are irrelevant.   
But if we do not even have the pleasure of your comments and our discussions, what's the point?   
We do not want to see our blogs go out ...   
Reader if you want to help us, because I'm sure the situation affects you, share, publish, tweet, instagram the logo ...   
And put it on the #solidshareblogs ... 

![thief](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/voleur.jpg)

Small legal reminder ...   
THE THEFT OF IP AND COPYRIGHT IS PUNI BY LAW !!!!!!!!!!   
Copyright infringements and their penalties   
Any copyright infringement that constitutes an infringement is performed by:   
violation of the moral rights of the author (eg infringement of the right of disclosure or paternity of the author, infringement of the right to respect for the work);   
the violation of these economic rights (reproduction and / or complete or partial representation of the work without authorization of the author).   
These offenses give rise to criminal sanctions (article L. 335-2 ICC: 3 years imprisonment, 300,000 euros fine and, if necessary, confiscation of proceeds from the offense or counterfeit objects). They also give rise to civil penalties, such as the payment of damages to the author for damages.   
The legislator has also provided for specific sanctions:   
firstly in case of circumvention of technical protection measures (article L. 335-3-1 and L.335-3-2 ICC: from 750 euros to 30 000 euros fine and up to 6 months of imprisonment).   
on the other hand, in the case of infringements relating to peer-to-peer software publishers allowing unauthorized exchanges of protected works (Article L. 335-2-1 ICC: penal sanctions up to 300 000 euros fine and 3 years imprisonment).   
In addition, reflections are currently underway for the development of specific sanctions and adapted to Internet counter-factors.   
NOBODY IS SUPPOSED TO IGNORE THE LAW !!!!!   
We can not say that we did not know when in its Terms and Conditions they dare warn Internet users not to steal because, steal is bad!   
Here is their CGV:   
Intellectual property   
The presentation and each of the elements making up the appearance on Recette.land were created by Recette.land and are protected by the laws in force on the intellectual property and can not be copied, reproduced, modified, reissued, loaded, denatured, transmitted or distributed in any manner whatsoever, in any medium whatsoever, in part or in whole, without the written permission of Recette.land   
Illustrative recipe photos are the property of their respective authors.   
The user who adds a recipe with a representation picture accepts Recette.land's use of these recipes to promote their recipes.   
Personal data   
No personal information of the Recette.land Site User is published without the knowledge of the user, exchanged, transferred, assigned or sold on any support to third parties.   
In accordance with the provisions of Article 38 et seq. Of Law 78-17 of 6 January 1978 relating to data, files and freedoms, any User has a right of access, rectification and opposition to personal data. concerning him, by making his written and signed request, accompanied by a copy of the identity document with signature of the holder of the document, specifying the address to which the answer must be sent. 

Under the terms of Article L335-2 of the French Intellectual Property Code, infringement of an exclusive right to intellectual property, be it literary or artistic property (copyright or related rights), constitutes an infringement ) or industrial property (eg patent, trademark, design or model). 

For example, the exchange of files protected by copyright (music, films not in the public domain) is considered counterfeit. 

Counterfeiting may involve: 

the civil liability of its author under Article 1382 of the Civil Code, which may lead him to pay damages, calculated according to the harm suffered by the victim;   
its criminal liability, the penalties incurred up to 5 years in prison and 500 000 € fine according to the new law adopted on October 29, 2007.   
The author of the counterfeit does not need to be aware of the seriousness of his action to be found responsible. 

Parliament has adopted1 a draft law to combat counterfeiting which transposes a directive of 29 April 2004 on the enforcement of intellectual property rights. This text introduces a specialization of the courts of first instance competent to know actions in matters of intellectual property. The list of the TGI concerned will be indicated by a decree in Council of State. Another measure in this text allows judges to award damages in a lump sum that can not be less than the sums that would have been received by the owner of the rights if the infringer had asked for authorization. 

Copyright [edit | change the code] 

Seller of pirated albums [ref. necessary].   
Article L335-3 of the Code of Intellectual Property states that it is also an offense of counterfeiting any reproduction, representation or dissemination, by any means whatsoever, of a work of the mind in violation of the rights of the author, as defined and regulated by law. 

In other words, any use of a copyrighted work that is not expressly authorized by its author or his rights holders is prohibited. 

The only exceptions are those provided by Article L122-5 of the same Code, which provides in particular: 

"When the work has been divulged, the author can not prohibit: 

Private and free performances performed exclusively in a family circle;   
Copies or reproductions strictly reserved for the private use of the copier and not intended for collective use, with the exception of copies of works of art intended to be used for purposes identical to those for which the original work was created and copies of software other than the backup copy established under the conditions provided for in II of Article L. 122-6-1 as well as copies or reproductions of an electronic database;   
Provided that the name of the author and the source are clearly indicated:   
Analyzes and short quotations justified by the critical, controversial, educational, scientific or informational nature of the work in which they are incorporated;   
Press reviews;   
The dissemination, even in full, by means of the press or television broadcasting, as news information, of speeches intended for the public pronounced in the political, administrative, judicial or academic assemblies, as well as in public meetings of order political and official ceremonies;   
Reproductions, in whole or in part, of graphic or plastic works of art intended to appear in the catalog of a judicial sale [...];   
Representing or reproducing excerpts of works, subject to works designed for educational purposes and musical scores, for the sole purpose of illustration in the context of teaching and research [...];   
Parody, pastiche and caricature, considering the laws of the genre;   
Les actes nécessaires à l’accès au contenu d’une base de données électronique pour les besoins et dans les limites de l’utilisation prévue par contrat […]. » 
