---
title: strawberry meringue tartlets
date: '2015-05-02'
categories:
- gateaux, et cakes
- recettes sucrees
- tartes et tartelettes
tags:
- Algerian cakes
- Strawberry tart
- pies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tartelettes-meringu%C3%A9es-aux-fraises.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tartelettes-meringu%C3%A9es-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/article-tartelettes-meringuees-aux-fraises.html/sony-dsc-285>)

#  Meringue tart with strawberries 

Hello everybody, 

Enjoying strawberries as long as it's the season, and this time, I share with you these delicious strawberry meringue tartlets. An achievement of our dear Lunetoiles who is always present with these beautiful wonders. 

These strawberry meringue pies are just to die for. A nice melting dough all melted, covered with a nice scented scented cream, finally decorated with the crunchy layer of meringue and fresh strawberries. 

[ ![strawberry meringue tartlets 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tartelettes-meringuees-aux-fraises-1.jpg) ](<https://www.amourdecuisine.fr/article-tartelettes-meringuees-aux-fraises.html/tartelettes-meringuees-aux-fraises-1>)   


**strawberry meringue tartlets**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tartelettes-meringuees-aux-fraises-3.jpg)

portions:  6  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** Dough: 

  * 100 g of butter 
  * 175 g flour 
  * 40 g of sugar 
  * 30 g of cocoa 
  * 2 pinches of salt 
  * 2 egg yolks 
  * 1 tbsp. water coffee 

Cream: 
  * 1 egg yolk 
  * 1 tbsp. cornflour 
  * 15 cl thick cream 
  * 40 g icing sugar 
  * 100 g of almond powder 
  * 5 cl of milk 

The meringue: 
  * 3 egg whites 
  * 1 pinch of salt 
  * 140 g of sugar 

\+ 400 g strawberries 

**Realization steps** dough: 

  1. Cut the butter into cubes. 
  2. Mix the flour, sugar and cocoa. 
  3. Add the butter and salt and mix with your fingertips to obtain a sandy mixture. 
  4. Pour the egg yolks and the water in the middle. Mix, make a ball and let stand 40 minutes. 
  5. Spread the dough, squeeze the buttered molds and refrigerate for 20 minutes. 

cream: 
  1. Beat the egg yolk lightly with the crème fraiche. 
  2. Add cornstarch, icing sugar and almond powder. 
  3. Mix with the milk and pour over the dough. 
  4. Bake in the preheated oven at 180 ° C for 20 minutes. 

meringue and decoration: 
  1. Whisk 3 egg whites with salt. Pour the sugar in the rain while continuing to whisk until a smooth and shiny mixture is obtained. 
  2. Cover the tartlets with meringue and sprinkle with a little icing sugar. Brown for 15 minutes in the oven at 190 ° / 200 ° C. Remove the tarts, let cool before unmolding and prick 400 g strawberries in the meringue. 



[ ![strawberry meringue tartlets 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tartelettes-meringuees-aux-fraises-2.jpg) ](<https://www.amourdecuisine.fr/article-tartelettes-meringuees-aux-fraises.html/tartelettes-meringuees-aux-fraises-2>)
