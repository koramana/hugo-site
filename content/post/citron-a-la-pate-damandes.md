---
title: lemon with almond paste
date: '2016-09-13'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid
- recettes sucrees
tags:
- Ramadan
- Algerian cakes
- Algerian patisserie
- Oriental pastry
- Cakes of Aid
- Almond Cakes
- Without cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Citron-a-la-pate-damande-1.jpg
---
![lemon-a-la-pate-almond-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Citron-a-la-pate-damande-1.jpg)

##  lemon with almond paste 

Hello everybody, 

Among the Algerian cakes that I liked to see my mother present on the Eid party table when I was little, it was the fruit with almond paste. My mother did wonders, especially the almond-based cherries not only were they too beautiful, they were also too good. I also liked lemons especially when the taste and scent of lemon are present. 

For this Eid el adha, it was my daughter who asked me for lemons, because she tasted it well this summer during weddings that we attended. I hope the recipe will please you. 

![lemon-a-la-pate-almond-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Citron-a-la-pate-damande-2.jpg)

**lemon with almond paste**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Citron-a-la-pate-damande.jpg)

portions:  40  Prep time:  20 mins  cooking:  5 mins  total:  25 mins 

**Ingredients** For the syrup: 

  * 300 ml of water 
  * 125 gr of caster sugar 
  * zest of a lemon 

almond paste: 
  * 500 gr of almond powder 
  * 200 gr of icing sugar 
  * 1 knob of butter 
  * aroma of lemon 

For the stuffing: 
  * 200 gr of reduced pie cake (here speculoos and small butter) 
  * 50 gr of Turkish halwa 
  * 2 tablespoons apricot jam 
  * 1 C. cocoa coffee 
  * 1 little butter to pick up the mixture 
  * yellow dye. 

For the decoration: 
  * Green almond paste 



**Realization steps**

  1. prepare the syrup in advance for it to cool, place the mixture water, sugar and lemon peel to boil, count 5 minutes after boiling and remove from heat (after cooling to Chinese to remove the zest) 
  2. pass the almond and the sugar to the blinder to have a fine mixture. 
  3. place this mixture in a salad bowl, add the butter nut, the aroma of lemon and the syrup little by little until obtaining a firm paste which picks up well. 
  4. add the colorant slowly to get the right color. 

prepare the stuffing: 
  1. mix the powder cake, Turkish halwa, jam, cocoa and butter to obtain a homogeneous paste. 
  2. Form balls of almost 20 gr and set aside. 
  3. take the almond paste and form larger balls (almost 35 to 40 gr) 
  4. Flatten the ball of marzipan in the palm of your hand, put the ball of stuffing in the middle and close the whole thing. 
  5. try to give the shape of a lemon to your cake. 
  6. stick a sheet of green almond paste to the bulging side of the lemons. 
  7. present boxes, and enjoy with a good tea. 



![lemon-a-la-almond-paste-3](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Citron-a-la-pate-damande-3.jpg)
