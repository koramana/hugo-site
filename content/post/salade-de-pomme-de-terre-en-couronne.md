---
title: Potato salad in a crown
date: '2015-05-15'
categories:
- amuse bouche, tapas, mise en bouche
- ramadan recipe
- recipes of feculents
- salades, verrines salees
tags:
- Ramadan
- inputs
- Cheese
- Summer
- Ramadan 2015
- Algeria
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/salade-de-pomme-de-terre-en-couronne-11.jpg
---
[ ![potato salad in a crown 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/salade-de-pomme-de-terre-en-couronne-11.jpg) ](<https://www.amourdecuisine.fr/article-salade-de-pomme-de-terre-en-couronne.html/salade-de-pomme-de-terre-en-couronne-1-2>)

##  Potato salad in a crown 

Hello everybody, 

We always tend to want to eat [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html> "mashed potatoes") very hot accompanied by [ roast chicken ](<https://www.amourdecuisine.fr/article-poulet-roti-au-four.html> "Oven-roasted chicken") , or [ leg of lamb roti ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-au-four-aid-el-adha-2012-111759550.html> "Gigot D'Baked Lamb") but what about a mashed cold potato? As an appetizer with a light salad. 

this recipe of potato salad in the crown is really a good idea, which will be ideal in a buffet and even during Ramadan, when we are fed up with making [ boureks ](<https://www.amourdecuisine.fr/article-bourek-aux-pommes-de-terre-et-fromage-entree-pour-ramadan.html> "bourek with potatoes and cheese / entree for ramadan") . 

It's a recipe from a reader and friend: **Amira Ch** who shares it with us, I hope that it will please you, and that you will realize it. 

**Potato salad in a crown**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/salade-de-pomme-de-terre-en-couronne1.jpg)

portions:  10  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 2 to 3 medium potatoes 
  * 1 handful of peas 
  * 1 carrots medium 
  * 3 to 4 triangles of cheese, or according to taste 
  * 3 hard boiled eggs 



**Realization steps**

  1. it is ideal to cook the potato cleaned and cut in small steamed with a little salt. 
  2. Separately, cook peas and salted carrots lightly with steam. 
  3. crushed the potato with the cheese. 
  4. stir in peas and carrots to mix 
  5. wrap a savarin mold with food film. 
  6. fill it with this puree, delicately. 
  7. place the hard-boiled eggs in quarters on top, 
  8. cover with cling film, and gently tamp. 
  9. refrigerate until serving 
  10. unmold the potato wreath delicately, and garnish as you please, or present with a fresh salad 



[ ![potato salad in a crown](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/salade-de-pomme-de-terre-en-couronne1.jpg) ](<https://www.amourdecuisine.fr/article-salade-de-pomme-de-terre-en-couronne.html/salade-de-pomme-de-terre-en-couronne-2>)
