---
title: chicken with chicken, Nwasser bidjej, Tunisian cuisine
date: '2013-03-09'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- dips and sauces
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/trida-pour-moharem-011_111.jpg
---
![Trida, Mkertfa, Nouasser](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/trida-pour-moharem-011_111.jpg)

##  chicken with chicken, Nwasser bidjej, Tunisian cuisine 

Hello everybody, 

When one of my readers came to kindly ask me the recipe for chicken noodles, Nwasser bidjej, Tunisian cuisine 

And frankly it is surprising how much the Maghreb cuisine is similar, because in fact the nouasser resemble much the trida of eastern Algeria ... even the method of preparation of the dough. So, for this reader, and for many of you who love Tunisian cuisine, I pass you the recipe of the trida, version "Nouasser" ...   


**chicken with chicken, Nwasser bidjej, Tunisian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/trida-pour-moharem-011_1.jpg)

**Ingredients**

  * square pasta, Nouasser pasta, or pasta trida 
  * chicken (thighs) 
  * onion 
  * 3 cases of olive oil 
  * 1 case and ½ canned tomato 
  * 1 handful of chickpeas 
  * 2 cloves garlic 
  * 1 tablespoon harissa 
  * 1 spicy, for those who like the dish even more raised. 
  * salt, pepper, paprika, ras el hanout 



**Realization steps**

  1. let's start by preparing the sauce, mixing the onion and the garlic, put in the bottom of a couscous pot, adding the oil, the chicken, the canned tomato and the spices. let it simmer a little, and then cover well with water. and add the chickpeas. 

in the meantime, we go to our trida,   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215438671-300x227.jpg)

  1. we put the quantity we want to prepare in a terrine, we add a case of oil, we rub in hands. during this time the sauce should start boiling.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215438911.jpg)
  2. so we put our edges in the top of a couscoussier, and place it on the bottom of our couscoussier. 
  3. when the steam begins to escape between the edges, take the couscous and pour into a bowl and sprinkle with the sauce, and try to get the sauce in and try to separate the small squares from other.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215439301.jpg)
  4. and then put the couscoussier again on the top and redo this action 2 to 3 times until the edges become soft. 
  5. we remove our edges and add a little butter. 
  6. before serving, put our edges in a thick pot, and sprinkle with sauce and allow to absorb well over low heat.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215440001.jpg)
  7. serve well garnished with eggs, chickpeas and chicken. 



Enjoy your meal 

suivez moi sur: 
