---
title: beet salad
date: '2014-04-11'
categories:
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tartare-de-betterave-au-chevre_thumb.jpg
---
Hello everybody, 

Here is a nice and fresh appetizer, an even better salad if it is prepared in advance, a new way to savor the beetroot accompany with the goat cheese well perfumed with a sweet touch of crushed pepper. 

This beet salad can be prepared in advance and it can be kept refrigerated for almost 4 days. 

**beet salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tartare-de-betterave-au-chevre_thumb.jpg)

Recipe type:  salad  portions:  4  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 300 g of red beets (raw to cook or already cooked) 
  * 1 log of dry goat's cheese 
  * 4 to 5 tablespoons mayonnaise 
  * 1 chopped shallot 
  * 2 tablespoons chopped capers 
  * a few sprigs of chopped coriander 
  * some flat parsley leaves 
  * red pepper powder 
  * olive oil 
  * 1 tablespoon lemon juice 
  * fine salt 
  * ground pepper 



**Realization steps**

  1. Cut the goat cheese into thin slices. 
  2. Place in a deep plate and sprinkle with olive oil and pepper. 
  3. Let it macerate at room temperature for the rest of the preparation. 
  4. Peel the cooked beets and chop them finely. 
  5. Put in a salad bowl the chopped beets, the chopped coriander, the shallot. 
  6. add the chopped capers, mayonnaise, lemon juice, remaining salt, pepper. 
  7. Mix gently and adjust the seasoning if necessary. 
  8. Put in the center of a plate the ¼ of the preparation. 
  9. Arrange the goat cheese slices around the sides and drizzle with the rest of the olive oil. 
  10. Decorate with the flat parsley leaves. 


