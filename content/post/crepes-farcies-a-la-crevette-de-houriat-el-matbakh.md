---
title: Stuffed pancakes with Houriat el matbakh shrimp
date: '2010-07-01'
categories:
- dessert, crumbles et barres
- ramadan recipe
- recettes sucrees
- verrines sucrees

---
yet another recipe from Houriat el matbakh, I did not resist when one of my readers "Ratiba" told me about it, and told me that she had tried it, and that it was a delight, so this morning, and although it was a busy day for me, I said, I'll do it insha'Allah, and voila, must say that I'm really well feasted, and it was super delicious, I go to the recipe: 

for the crepe dough: (must say that I made half of the ingredients, and I had a good amount of pancakes, already half is in the freezer) 

  * 400 g flour 
  * 3 eggs 
  * 50 g of melted butter 
  * 1 liter of milk 
  * A pinch of salt 
  * 1 tablespoon of sugar 



Mix the dry ingredients, then add the milk, eggs and melted butter and mix well with a whisk if you have one 

Let the dough rest a little while, heat a frying pan and rub with a little butter, pour the amount of a small ladle and spread well cook on one side, then on the other side too. and let it cool a little 

the joke: 

  * 300 g cooked shrimp 
  * 250 g mushrooms 
  * 2 cloves garlic 
  * 2 tbsp. chopped parsley 
  * 1 C. soup of lemon juice 
  * Black pepper to taste 
  * ½ teaspoon of salt 
  * a little nutmeg 
  * 100 g of pitted olives 
  * A sliced ​​onion 
  * Half a red pepper grilled and cut into pieces 
  * a quantity of bechamel sauce (recipe below) 
  * 2 tbsp. butter 



In a frying pan over high heat put the butter and fry the mushrooms until it is a beautiful gilding, add the lemon juice, parsley, salt and black pepper and set aside. 

on the other side put some butter and fry the onions and garlic, then add the already cooked shrimp too, then add the other ingredients and a little Bechamel sauce. 

Bechamel sauce: 

  * 2 tbsp. butter 
  * 2 tbsp. flour 
  * Milk or 400 ml (200 ml of milk + 200 fresh cream (not for me)) 
  * Little spoon of salt 
  * a little chopped garlic 
  * a little nutmeg 
  * Black pepper to taste 
  * 200g of Emmantal cheese (for me it was a bit of Gruyère cheese) 
  * 50 g of parmesan cheese 



In a saucepan over low heat, put the butter, then add the flour and mix well until it turns a nice golden color, then add the milk and mix without stopping. Add salt, black pepper, nutmeg and crushed garlic. do not stop stirring with a spoon until a mixture becomes thick. 

Stuff each pancake with a large spoonful of stuffing and place in a well-buttered pan, cover with a little béchamel sauce and sprinkle with grated cheese then put in a hot oven to grill for 15 to 20 minutes. minutes 

servir bien chaud avec une belle salade. 
