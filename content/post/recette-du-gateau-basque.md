---
title: Basque cake recipe
date: '2018-01-12'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Galette
- Shortbread
- Algerian cakes
- Pastry
- desserts
- To taste
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/01/recette-du-gateau-basque-696x1024.jpg
---
![Basque cake recipe](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/recette-du-gateau-basque-696x1024.jpg)

##  Basque cake recipe 

Hello everybody, 

Among my favorite desserts recipes I confess that I really like the Basque cake, or as it is sometimes called pastry pie. I prefer it by far [ yarrow ](<https://www.amourdecuisine.fr/article-millefeuilles-fait-maison-mille-feuilles.html>) because I think the Basque cake, which also contains a very good layer of custard cream, and much more greedy with these magnificent layers of shortbread. Of course, everyone likes it and everyone loves it, but I love this delicacy. 

In any case the Basque cake recipe, this classic cake that comes from the Basque country is a recipe of simplicity and easy, two versions are well known Basque cake with custard that I share with you today, or therefore the [ Basque cake with black cherries ](<https://www.amourdecuisine.fr/article-gateau-basque-la-confiture-de-cerises-noires.html>) Lunetoiles shared with us a moment ago. 

**Basque cake recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/recette-du-gateau-basque-1.jpg)

**Ingredients** for the shortbread paste: 

  * 300g of flour 
  * 1 whole egg 
  * 3 egg yolks 
  * 200g of butter 
  * 180g of powdered sugar 
  * 1 teaspoon yeast 
  * 1 zest of lemon 

for pastry cream: 
  * 500 ml of milk 
  * 100 gr of sugar 
  * 1 vanilla pod 
  * lime zest 
  * 4 egg yolks 
  * 60 gr of flour 



**Realization steps** Preparation of the dough: 

  1. place all the ingredients in a large salad bowl, and collect the dough by sanding it. 
  2. form two patas of dough (one larger than the other) 
  3. spread the two pasta in discs of almost 5 mm and place it in the cool so that you can then darken the mold. 

preparation of the pastry cream: 
  1. whisk egg yolks, flour, sugar and lemon zest 
  2. Pour the warm vanilla flavored milk over, mix at the same time, and put back on medium heat while stirring constantly with a wooden spoon until the mixture thickens (about 4-5 minutes). 
  3. Allow to cool, covering with a film of food, so that it forms a thick film. 

mounting: 
  1. drive the largest disc into a nearly 22 cm miss. 
  2. pour in the pastry cream. 
  3. adjust the sides of the dough so that they are at the level of the pastry cream or fold gently on the pastry cream. 
  4. place the second disc to cover the pastry cream. 
  5. Dune with egg yolk and draw streaks with a fork. 
  6. Bake at 150 ° C for about an hour until cake is golden and let cool. 



![Basque cake recipe 2](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/recette-du-gateau-basque-2-706x1024.jpg)
