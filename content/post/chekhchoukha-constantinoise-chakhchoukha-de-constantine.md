---
title: Constantinese Chekhchukhah, Constantine's Chakhchukha
date: '2016-04-18'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chekhchoukha-dfar-1a.jpg
---
[ ![chekhchoukha dfar 1a](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chekhchoukha-dfar-1a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chekhchoukha-dfar-1a.jpg>)

##  Constantinese Chekhchukhah, Constantine's Chakhchukha 

Hello everybody, 

Even if we will publish every day a recipe of Algerian cuisine, we can never present this cuisine in its true value. Algerian cuisine is so vast and rich especially if you start to get closer to regional recipes, and this dish **Constantinese Sheikhchukha, Constantine's Chakhchukha** is proof of it. This dish comes from the cuisine of eastern Algeria prepared from [ Semolina cakes crumbled into small squares ](<https://www.amourdecuisine.fr/article-preparation-de-chekhchoukhat-dfar.html>) . This is the hardest part, prepare the chekhchoukha squares, if you do not know where to buy it. 

This friendly dish whose sauce can be based on chicken, or lamb meat is one of the dishes that are sometimes presented at parties (imperatively with red meat), and even for a special dinner that we present to the guests, to show the great generosity of the host of the house who took a lot of love and time to prepare this dish very generous in taste and flavors. 

Well, having not missed the family tradition, I prepared this good dish for the mouloud, it really pleased my Measter and children.   


**Constantinese Chekhchukhah, Constantine's Chakhchukha**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chekhchoukha-de-constantine-1.jpg)

Recipe type:  Algerian cuisine  portions:  6  Prep time:  40 mins  cooking:  45 mins  total:  1 hour 25 mins 

**Ingredients**

  * pieces of lamb meat (part of the hock, leg of lamb, or colier) 
  * 2 tablespoons tomato paste (or one if you are not too many and you will do a small amount) 
  * salt 
  * paprika 
  * black pepper 
  * 1 onion 
  * 2 cloves garlic 
  * 1 tablespoon of butter 
  * 3 tablespoons extra virgin olive oil 
  * 1 nice handful of chickpeas (I do a lot, we love at home) 
  * 1 kg of [ chekhchoukhat dfar ](<https://www.amourdecuisine.fr/article-preparation-de-chekhchoukhat-dfar.html>)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chekhchoukha-et-cake-au-mascarpone-014-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chekhchoukha-et-cake-au-mascarpone-014-1.jpg>)



**Realization steps**

  1. in the bottom of a couscoussier (it is preferable), put oil, onion and garlic past the blinder, the tomato paste, the pieces of meat, the salt and the rest of the spices, 
  2. let it simmer a little, then add the water, I will say a lot of water. Do not forget if the chickpeas are tender and precooked, put them just at the end of cooking, otherwise if they are soaked the day before, you have to put them now. 
  3. In a boil, put the crumbs of galettes in a terrine, and sprinkle with a little of your sauce, let a little imbibe, then put in the top of the couscoussier, and cook them with the steam. 
  4. remove after steam escaping, and sprinkle again with a little sauce (which is always entails cooking) 
  5. let still imbibe, and put back in the top of the couscoussier. 
  6. repeat until the crumbs are tender. 
  7. if necessary add water to your sauce, and adjust salt and tomato. 
  8. at the end and just before serving, put the crumbs of cakes in a pot, sprinkle with more sauce (not too much) 
  9. remove from heat and put in a serving dish, garnish with chickpeas, pieces of meat and a little butter (what I like the part where there is butter, huuuuuuuuuum) 



![chekhchoukha of constantine](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chekhchoukha-de-constantine.jpg)

c’etait pas facile de taper toute cette recette, ça demande trop d’ explication, enfin, si un point m’ échappe, ou un truc que vous n’avez pas compris, laissez un commentaire, j’ essayerai de mieux expliquer!!! 
