---
title: Mkhabez, Algerian cakes with almonds
date: '2017-06-14'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/mkhabez-el-louz-3.jpg
---
![Mkhabez, Algerian cakes with almonds 2017](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/mkhabez-el-louz-3.jpg)

##  Mekhabez almond cakes 

Hello everybody, 

The Mkhabez, Algerian cakes with almonds is one of my favorite cakes, this delicious cake with almonds flavored with lemon super melting in the mouth, especially if you prepare a good frosting and super perfumed icing with orange blossom water . My daughter inherited this love for this cake from me, and now and at her request I must realize either this **cake with almond sauce** , or then [ **arayeches** ](<https://www.amourdecuisine.fr/article-arayeches-gateaux-algeriens.html>) if not [ iced charek ](<https://www.amourdecuisine.fr/article-tcharek-glace-corne-de-gazelle-algerien-au-glacage.html>) . 

In any case, this Algerian cupcake full of almonds with delicate taste of orange blossom water, covered with a thin layer of lemon icing has everything to please you, so if you do not know this cake, it's time to try it. 

![Mkhabez, Algerian cakes with almonds 2017](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/mkhabez-el-louz.jpg)

**Mekhabez almond cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/mkhabez-el-louz-2.jpg)

portions:  40  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 400 gr of ground almonds 
  * 200 gr of icing sugar 
  * the zest of 1 lemon 
  * 1 egg + 2 egg yolk 
  * vanilla 

icing: 
  * 3 egg whites 
  * 3 c. lemon juice 
  * 2 tbsp. orange blossom water 
  * 3 c. tablespoon of milk 
  * 2 tbsp. oil soup 
  * icing sugar as needed. 

decoration: 
  * sugarpaste 
  * shiny food 



**Realization steps**

  1. Mix the almonds, the icing sugar, the lemon zest, the vanilla extract and pick up with the eggs to obtain a manageable and firm paste. 
  2. On a floured work surface, lower the dough to a thickness of 1.5 cm and cut circles of 4 cm in diameter using a cutter. 
  3. Put them on a greased and floured plate, cook for 15 to 20 minutes at 160 ° C 
  4. Let cool on a baking rack. 

Preparation of the icing: 
  1. Prepare the icing with the given ingredients, 
  2. add the sugar in small quantities by mixing without whipping to avoid the formation of air bubbles. 
  3. Check the icing by trying on a cake, otherwise add even more sugar, if it's flowing, or a little milk if it's too thick. 
  4. Ice the cakes and let them dry completely, remove the excess frosting that would have sank. 
  5. Change the silvery shiny silver on the surface with a clean sponge. 
  6. garnish the mkhabez according to your taste, here I realized flower with sugar dough in two colors. 



![mkhabez el louz, Algerian cake with almonds](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/mkhabez-el-louz-1.jpg)
