---
title: braided brioche with hazelnuts and marzipan
date: '2017-11-09'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/brioche-Tressee-aux-noisettes-et-%C3%A0-la-p%C3%A2te-damandes-001.jpg
---
![Hazelnut and almond paste braid](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/brioche-Tressee-aux-noisettes-et-%C3%A0-la-p%C3%A2te-damandes-001.jpg)

##  Braided brioche with hazelnuts and almond paste 

Hello everybody, 

Here is a delicious braided bun with hazelnuts and almond paste that shares with us Lunetoiles. She is not used to making brioches, for fear of missing them, but personally I find this brioche very successful, a spinning and airy crumb, in addition to a farce allies, duet of hazelnuts and dough. 'almond. I am tempted to taste this delight. A thin slice to accompany a good green tea will be welcome. When is he for you? 

If you like stuffed buns, why not try to make [ Buchty ](<https://www.amourdecuisine.fr/article-buchty-brioche-facile-a-mie-filante.html> "Buchty, easy brioche") , or then a [ Chinese ](<https://www.amourdecuisine.fr/article-le-chinois.html> "the Chinese") . And if you like natural buns, the [ Mouna ](<https://www.amourdecuisine.fr/article-la-mouna.html> "The Mouna") , or this [ home bun ](<https://www.amourdecuisine.fr/article-brioche-fait-maison.html> "Home bun") are a treat. 

And if you tell yourself, we are only two people, a brioche is a lot for us, why not try this [ brioche pudding ](<https://www.amourdecuisine.fr/article-pudding-brioche-banane-nutella.html> "Brioche pudding with banana and nutella") or so this [ brioche lost ](<https://www.amourdecuisine.fr/article-brioche-perdue.html> "brioche lost") you will be very useful. 

you can have a look at my video of brioche express: 

**braided brioche with hazelnuts and marzipan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/brioche-Tressee-aux-noisettes-et-%C3%A0-la-p%C3%A2te-damandes.jpg)

portions:  8  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients** brioche dough: 

  * 400 g flour 
  * 25 g fresh yeast 
  * 130 g of milk 
  * 60 g of butter 
  * 40 g of sugar 
  * ½ teaspoon salt 
  * 2 eggs 

For garnish : 
  * 100 g of hazelnuts in powder 
  * 50 gr of sugar 
  * 40 gr breadcrumbs 
  * 1 pinch of cinnamon 
  * 125 ml of milk 
  * 30 gr of almond paste 
  * 1 drop of aroma of bitter almonds 

Finishing, frosting: 
  * 100 gr of icing sugar 
  * Lemon juice 



**Realization steps**

  1. Emit the yeast, and pour a little warm milk (1 to 2 tablespoons) to dilute it with a teaspoon of powdered sugar. 
  2. Mix and allow to swell for 15 minutes. 
  3. Pour the sifted flour into the bowl of the robot with all the other ingredients of the dough. 
  4. Knead for 5/7 minutes to obtain a homogeneous ball of dough. 
  5. Let stand for 1 hour, the bowl covered with a dry cloth or plastic film. 
  6. Your dough should be placed in a cool room away from drafts. 
  7. Prepare the filling, mix the hazelnuts with the powdered sugar, breadcrumbs and cinnamon. 
  8. Boil the milk with almond paste and bitter almond aroma in a saucepan. 
  9. Add to the hazelnut mixture. 
  10. Lower a rectangle of dough on a floured work surface and spread over the hazelnut filling. 
  11. Roll up the dough to form a roll. 
  12. Cut the roll in the middle with a knife and intertwine the two strips of dough to form a braid. 
  13. Place on a baking sheet lined with parchment paper and let stand for another 30 minutes. 
  14. Then bake the braid in the preheated oven at 190 ° C for about 35 minutes. 
  15. Let cool. 
  16. Dilute the icing sugar with lemon juice and water and brush the braiding with lemon frosting. 



[ ![Hazelnut and almond paste braid](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/brioche-tressee-aux-noisettes-et-%C3%A0-la-p%C3%A2te-damandes-002.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/brioche-tressee-aux-noisettes-et-%C3%A0-la-p%C3%A2te-damandes-002.jpg>)
