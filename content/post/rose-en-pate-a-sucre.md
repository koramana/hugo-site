---
title: rose in sugar paste
date: '2012-05-02'
categories:
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/decoration-du-gateau_thumb1.jpg
---
##  rose in sugar paste 

**[ birthday cake decoration ](<https://www.amourdecuisine.fr/article-bavaroise-fraise-104360210.html>) **

Hello everybody, 

here is a tutorial to show you how to prepare a **rose in sugar paste** , or a **rose in almond paste** , according to your taste, ideal for decorating your cakes, and especially the [ birthday cakes ](<https://www.amourdecuisine.fr/categorie-11700499.html>) , so here is a little tutorial for you: 

in this tutorial it's with the sugar pie, 

you can use the color of your choice, I have colored shrimp a little dark and the other a little lighter. 

spread the sugar dough over a surface sprinkled with sugar, until you get a thin layer. 

cut circles, according to your taste for the size of the flower, I made circles of 4 cm and others of 3 cm. 

shape a cone of almost 2 cm and a half high, and start sticking the pastry circles one by one, use a little water at the base of each circle, so that it sticks, insert the circles, to have a beautiful flower, let the flower dry a little, on a paper that you have placed in a glass to not spoil the shape of your flower, after a few minutes, return to your flower, and deform a little petals, to give the pace of a real flower. 

![decoration of the cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/decoration-du-gateau_thumb1.jpg)

**rose in sugar paste**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/INES-birthday-093_thumb.jpg)

**Ingredients**

  * in this tutorial it's with the sugar pie, 
  * you can use the color of your choice, I have colored shrimp a little dark and the other a little lighter. 



**Realization steps**

  1. spread the sugar dough over a surface sprinkled with sugar, until you get a thin layer. 
  2. cut circles, according to your taste for the size of the flower, I made circles of 4 cm and others of 3 cm. 
  3. shape a cone of almost 2 cm and a half high, and start sticking the pastry circles one by one, use a little water at the base of each circle, so that it sticks, insert the circles, to have a beautiful flower, let the flower dry a little, on a paper that you have placed in a glass to not spoil the shape of your flower, after a few minutes, return to your flower, and deform a little petals, to give the pace of a real flower. 


