---
title: choumicha chicken meatballs gratin
date: '2014-03-23'
categories:
- juice and cocktail drinks without alcohol
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-au-boules-de-poulet-choumicha_thumb.jpg
---
[ ![gratin with chicken balls, choumicha](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-au-boules-de-poulet-choumicha_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-au-boules-de-poulet-choumicha_2.jpg>)

Hello everybody 

I do not propose a lot of **gratins** on my blog, because my children did not eat them, but there, I try to introduce them more flavors. 

This recipe is a recipe for the sublime [ Choumicha ](<https://www.amourdecuisine.fr/categorie-12244163.html>) , I will not tell you since when I'm a fan of his [ Moroccan cuisine ](<https://www.amourdecuisine.fr/categorie-12359409.html>) and global, its recipes always make you want. 

So when I wanted to do this gratin at [ chicken ](<https://www.amourdecuisine.fr/categorie-12364208.html>) , I remembered this recipe, which I noted when she went on the Moroccan channel 2M. 

In any case, the recipe was too good, and the children were happy to see that 3 meatballs on their plates, one of them told me, I'll finish it in 2 bites, hihihihiih 

so I deliver my friends the recipe:    


**choumicha chicken meatballs gratin**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-au-boules-de-poulet-choumicha_2-300x225.jpg)

Cooked:  French  Recipe type:  Hand flat  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 250 grams of chopped white chicken 
  * 125 grs potato cooked and mashed 
  * 1 chopped onion 
  * 2 tablespoons of oil 
  * 2 cm fresh ginger rapee (I did not have any) 
  * ½ teaspoon of salt 
  * 1 cup of turmeric coffee (I forgot to put, hihihihi) 
  * 2 tablespoons chopped coriander 
  * 20 gr of flour 
  * 4 tablespoons of oil 

to brown: 
  * 400 ml of bechamel 
  * 50 gr grated cheese 
  * 2 sliced ​​onions 
  * 2 tablespoons of oil 
  * 1 pinch of salt 



**Realization steps**

  1. in a frying pan brown the chopped onion in the oil 
  2. in a container, mix the chopped chicken breast with mashed potato, golden onion, coriander and spices 
  3. make medium sized meatballs and put them in the flour 
  4. in an anti-adhesive frying pan, heat the oil and brown the meatballs, then set aside 
  5. in a pot over low heat, cook covered sliced ​​and salted onions in oil until golden brown 
  6. mix them to obtain a smooth puree and incorporate it in the béchamel with the cheese. 
  7. arrange the meatballs in a gratin butter dish, then top them with béchamel sauce with onion 
  8. brown in preheated oven at 180 degrees C for 25 min. 
  9. serve the gratin immediately 



[ ![2011-09-26 gratin with chicken balls](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/2011-09-26-gratin-au-boules-de-poulet_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/2011-09-26-gratin-au-boules-de-poulet_2.jpg>)

if you have tried one of my recipes, send me the picture here: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

and if you have a recipe for you that you want to publish on my blog, you can put it online here: 

[ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
