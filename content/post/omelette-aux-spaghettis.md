---
title: spaghetti omelette
date: '2012-06-11'
categories:
- Cupcakes, macaroons, and other pastries
- recettes sucrees

---
hello everyone, a Spaghetti Omelette is a recipe from a faithful reader Yousra, who likes to participate in the contest "express cooking", and this recipe tempts me, because I never think to realize it. so you get this recipe: ingredients: boiled spaghetti 1 handful of boiled peas tomatoes diced potato boiled and diced (optional) 1 or 2 cloves of garlic rapped parsley 3 to 4 eggs green olives pitted salt, pepper black, grated or portioned paprika 1 tbsp oil Preparation: In a frying pan, sauté the garlic, tomato, & greek; 

##  Overview of tests 

####  please vote 

**User Rating:** 0.85  (  1  ratings)  0 

Hello everybody, 

a spaghetti omelette is a recipe from a faithful reader Yousra, who likes to participate in [ "express kitchen" contest ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>) , and this recipe tempts me well, because I never think to realize it. 

so to you this recipe: 

**ingredients:**

  * boiled spaghetti 
  * 1 handful of boiled peas 
  * diced tomatoes 
  * Potato boiled and diced (optional) 
  * 1 or 2 cloves of garlic rapped 
  * parsley 
  * 3 to 4 eggs 
  * pitted green olives 
  * salt, black pepper, paprika 
  * rectified or portioned cheese 
  * 1 tablespoon of oil 



Preparation: 

  1. In a skillet, fry the garlic, tomato, peas, potatoes, olives and oil for 2 to 3 minutes. 
  2. add spaghetti for 1 min and remove from heat. 
  3. In a bowl, beat the eggs, chopped parsley, salt, black pepper, paprika and cheese, pour this mixture over the spaghetti and cook like an omelette on both sides. 
  4. I put the mixture in small round molds on the pan. 
  5. You can season with any sauce, for me a mixture of mayonnaise, ketchup, melted cheese 



Bon appétit 
