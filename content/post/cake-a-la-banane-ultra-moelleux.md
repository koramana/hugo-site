---
title: ultra moist banana cake
date: '2015-01-07'
categories:
- cakes and cakes
- sweet recipes
tags:
- Buttermilk

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-a-la-banane-ultra-moelleux-3.jpg
---
[ ![ultra moist cake with banana](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-a-la-banane-ultra-moelleux-3.jpg) ](<https://www.amourdecuisine.fr/article-cake-a-la-banane-ultra-moelleux.html/cake-a-la-banane-ultra-moelleux-3>)

##  ultra moist banana cake 

Hello everybody, 

It's the best banana cake recipe I've made to date. I almost always make banana bread, as my children call it, because I always have ripe bananas at home, and the easiest solution for you is a banana cake. 

And on the net, it's not the banana cake recipes that are missing. Even on my blog, I already have two banana cake recipes, the [ banana cake from el matbakh ](<https://www.amourdecuisine.fr/article-cake-a-la-banane-fatafeat-tv.html> "banana cake - fatafeat TV") , and another recipe from [ banana cake ](<https://www.amourdecuisine.fr/article-banana-bread-cake-banane.html> "Banana bread-Cake with banana") from Lunetoiles. 

This recipe I really wanted to share with you, because it is the best recipe, it is "the recipe" cake banana, a light cake, airy, fluffy, super perfumed and above all too good. 

[ ![ultra moist cake with banana](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-a-la-banane-ultra-moelleux-4.jpg) ](<https://www.amourdecuisine.fr/article-cake-a-la-banane-ultra-moelleux.html/cake-a-la-banane-ultra-moelleux-4>)   


**ultra moist banana cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-a-la-banane-ultra-moelleux-1.jpg)

portions:  12  Prep time:  15 mins  cooking:  50 mins  total:  1 hour 5 mins 

**Ingredients**

  * 1 egg 
  * 220 gr of powdered sugar 
  * 115 gr of butter at room temperature 
  * 1 cup of vanilla coffee 
  * 120 ml of buttermilk 
  * 2 ripe bananas, pureed 
  * 280 grams of flour 
  * ½ teaspoon of salt 
  * ½ cup of baking soda 
  * 1 cup of baking powder 
  * 1 teaspoon cinnamon 



**Realization steps**

  1. In a bowl of the robot equipped with the foil palette, whisk together the sugar, egg and vanilla until you have a creamy mixture. 
  2. Add the butter in a small piece and beat until the mixture is well incorporated. 
  3. Introduce the buttermilk and bananas while mixing. 
  4. In another bowl, mix flour, salt, baking soda, baking powder and cinnamon. 
  5. Gently add the dry ingredients into the liquid mixture 
  6. Shirt a cake tin with baking paper (my mold is 26 cm x 12 cm) otherwise use two small molds 
  7. pour the paste in and arrange the surface with a spatula. 
  8. cook in a preheated oven at 180 degrees, for 45 to 50 minutes, or depending on the capacity of your oven 



[ ![ultra moist cake with banana](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-a-la-banane-ultra-moelleux-2.jpg) ](<https://www.amourdecuisine.fr/article-cake-a-la-banane-ultra-moelleux.html/cake-a-la-banane-ultra-moelleux-2>)
