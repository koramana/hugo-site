---
title: Dolma vegetables stuffed with meat minced in the oven
date: '2017-11-16'
categories:
- Algerian cuisine
- recipe for red meat (halal)
tags:
- Full Dish
- Ramadan 2015
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/dolma-aux-legumes-2.jpg
---
![Dolma vegetables stuffed with meat minced in the oven](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/dolma-aux-legumes-2.jpg)

##  Dolma vegetables stuffed with meat minced in the oven 

Hello everybody, 

Dolma vegetables stuffed with grilled meat or dolma bel khodra is a very simple recipe, which takes a little time to prepare, but it must be said that it is worth it because this dish or tajine is just too good. 

I publish this recipe at the request of **a** of my readers, who really wants to prepare this dish Dolma stuffed vegetables with baked meat for the family of his fiancée, I hope you will impress them. 

I'm posting my new photos, though this time I did not put stuffed tomatoes or onions. Now, vegetables, it's everyone's taste, you can change, or remove one vegetable, double another. You can see from elsewhere [ zucchini dolma ](<https://www.amourdecuisine.fr/article-dolma-courgettes-courgettes-a-la-viande-hachee.html>) we like a lot at home, as you can see [ a dolma of potatoes with white sauce ](<https://www.amourdecuisine.fr/article-dolma-batata-pommes-terre-viande-hachee.html>) which is a real delight. 

Personally, I love zucchini, but not my husband and children, my husband loves potatoes, but not children, my son does not like minced meat, and my daughter does not like green stuff which float (parsley) in your opinion, how should I do? hihihihihi 

![Dolma vegetables stuffed with meat minced in the oven](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/dolma-aux-legumes-1.jpg)

Before turning to the recipe, I'll give you the new video on my youtube channel of dolma with various vegetables to the Iraqi, a delight: 

**Dolma, vegetables stuffed with meat baked in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/dolma-aux-legumes-2.jpg)

portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 500 to 700 gr of minced meat 
  * 5 medium potatoes 
  * 3 peppers 
  * 2 medium tomatoes 
  * 3 onions (2 for stuffing, 1 for sauce0 
  * 3 small zucchini 
  * 1 handful of chickpeas deceived the day before 
  * 2 to 3 tbsp. oil soup 
  * 1 tablespoon tomato concentrate 
  * salt, black pepper, paprika, 
  * 1 bunch of parsley 
  * ½ teaspoon cinnamon 
  * crumb of bread deceived in milk 
  * 1 egg 
  * 1 clove of garlic 



**Realization steps**

  1. wash the zucchini, tomato and potatoes, peppers, and onions well 
  2. empty them using a knife or the tail of a teaspoon. 
  3. in a container put the minced meat, the clove of crushed garlic, the crumb of bread deceived in milk, parsley, salt 
  4. pick it up with the egg. 
  5. Fill the zucchini, tomato and potatoes with this mixture. 
  6. with the remaining stuffing, prepare small balls of minced meat 
  7. arrange these vegetables in a high baking tin. 
  8. in a pot, brown the grated onion in the oil 
  9. add the tomato, let it simmer a little, 
  10. season with salt, and other spices 
  11. when the onion is very tender, 
  12. add almost 3 glasses of water, and the chickpeas 
  13. when the sauce starts to boil, add the minced meat balls 
  14. extinguish the fire, water the mold already filled with vegetables stuffed with half of the sauce 
  15. and put the dish well covered with aluminum foil in preheated oven 
  16. cook, then remove the foil, to give a little color to the vegetables, 
  17. if the vegetables absorb the sauce, add a little more 
  18. just before serving, garnish with a little sauce, chickpeas and chopped meatballs. 
  19. sprinkle with chopped parsley 



![Dolma vegetables stuffed with meat minced in the oven](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/dolma-aux-legumes_.jpg)
