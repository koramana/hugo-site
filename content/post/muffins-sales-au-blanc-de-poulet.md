---
title: Salted muffins with chicken breast
date: '2015-06-22'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- diverse cuisine
tags:
- Ramadan 2015
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/Muffins-sal%C3%A9s-au-blanc-de-poulet1.jpg
---
[ ![Salted muffins with chicken breast](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/Muffins-sal%C3%A9s-au-blanc-de-poulet1.jpg) ](<https://www.amourdecuisine.fr/article-muffins-sales-au-blanc-de-poulet.html/muffins-sales-au-blanc-de-poulet-2>)

##  Salted muffins with chicken breast 

Hello everybody, 

a salty recipe, more precisely "salty muffins" do you like? I like it a lot, because the salty muffins go on like .. uh ... pass like sweet muffins, hahahaha ... They never go out of style, besides for my part, it's one of the best recipes that we can prepare for a picnic or beach trip. In addition to being good, salty muffins are super easy to make. 

The proof is in the recipe for these chicken breast muffins, a recipe from one of my readers. **AS Sou** And members of the group: cook with love of cooking, who share this delicious recipe with us, then to your pan ... uh, no need to pan in this recipe, then to your muffin pan ... **.**   


**Salted muffins with chicken breast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/Muffins-sal%C3%A9s-au-blanc-de-poulet-2.jpg)

portions:  12  Prep time:  10 mins  cooking:  40 mins  total:  50 mins 

**Ingredients**

  * 1 chicken breast without bone; 
  * 1 glass of flour (100g); 
  * Grated cheese ; 
  * 1 glass of chopped parsley; 
  * 1 glass of milk (20 cl); 
  * 2 whole eggs; 
  * Black pepper ; 
  * 1 teaspoon baking powder (about 5 g); 
  * 1 pinch of salt (max 3g). 



**Realization steps**

  1. Cook the chicken breast in the water (or steam) with a pinch of salt, ginger, black pepper and oil for half an hour. 
  2. Cut into small pieces. Mix with cheese and parsley. 
  3. Meanwhile, sift the flour and yeast. Then add the milk and stir to avoid lumps. 
  4. Then add the beaten whole eggs, salt and black pepper. 
  5. Butter the muffin mussels. Place the pieces of chicken breast, and pour the mixture over them.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/Muffins-sal%C3%A9s-au-blanc-de-poulet-3.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42978>)
  6. Bake at 180 ° C for 40 to 45 minutes. 



Note I added green pepper cut in small pieces to the mixture to give even more flavor [ ![Salted muffins with chicken breast 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/Muffins-sal%C3%A9s-au-blanc-de-poulet-4.jpg) ](<https://www.amourdecuisine.fr/article-muffins-sales-au-blanc-de-poulet.html/muffins-sales-au-blanc-de-poulet-4>)
