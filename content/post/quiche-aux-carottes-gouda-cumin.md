---
title: carrot quiche Gouda cumin
date: '2013-03-03'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

---
Hello everybody, 

for lovers of quiches, here is a delicious quiche with carrots, gouda, scented with cumin, huuum that it must be delicious. 

you can see the version of [ carrot quiche flavored with fennel seeds ](<https://www.amourdecuisine.fr/article-mini-quiches-aux-carottes-100689898.html>) . 

this quiche recipe is a recipe that Lunetoiles has generously shared with us, I take the share from above, and the rest, only to people who leave comments, hihihihihi. 

Ingredients: 

  * 1 flaky pastry from the trade for me pure butter 
  * 4 large grated carrots 
  * 2 onions chiselled 
  * 2 tablespoons of oil 
  * 1 tbsp cumin seeds 
  * 250 ml of fresh cream 
  * 3 eggs 
  * 200 gr of cumin gouda if possible if not nature 
  * 1 small bunch of chopped coriander 
  * 2 tablespoons Dijon mustard 
  * salt 
  * mill pepper 



method of preparation: 

  1. Put the oil in a skillet and fry the cumin seeds for 2 minutes to give off their aromas 
  2. then add the onions until they are translucent. 
  3. Off the heat, add the grated carrots, chopped coriander, cheese cut into small cubes and mix everything together. 
  4. In a small bowl, or belly, beat the eggs, 
  5. add the cream and a small pinch of salt and pepper. 
  6. Spread the dough in a pie pan, making sure to leave the paper for cooking. 
  7. Stitch the dough several times with fork, 
  8. spread the mustard over the entire surface of the dough. 
  9. Garnish with the carrot mixture and pour over the cream and egg mixture. 
  10. Cook in a preheated oven at 180 ° C for about 30 minutes until the quiche is golden brown. 


