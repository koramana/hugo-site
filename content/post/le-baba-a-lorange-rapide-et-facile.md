---
title: the baba has orange, fast and easy
date: '2012-03-02'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/baba1-0011_31.jpg
---
[ ![the baba has orange, fast and easy](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/baba1-0011_31.jpg) ](<https://www.amourdecuisine.fr/article-42502008.html>)

##  the baba has orange, fast and easy 

Hello everybody, 

I remember the time when I ate this orange baba in Algerian pastries, a real fondness on the palate, 

and with this delicious taste of orange syrup, so before yesterday I was on the net and I find the recipe of houriat el matbakh, you know how much I like his recipes, besides being a good chef Cook, she is very friendly and in addition she gives all the tricks possible to make the recipe a success. 

so I will give you all the possible details as she explained   


**the baba has orange, fast and easy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/baba1-003_11.jpg)

Recipe type:  dessert, french pastry  portions:  6  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients**

  * 225 gr of flour 
  * 25 gr of sugar 
  * 5g of salt 
  * 12 grams of instant yeast 
  * 2 eggs 
  * 100 ml of water 
  * 50 gr of butter at room temperature 

orange syrup: 
  * 500 ml of water 
  * 500 gr of sugar 
  * orange dune juice or a lemon 



**Realization steps** prepare the syrup: 

  1. mix the water and sugar, put on the heat until boiling, then add the juice and remove from heat. 

Prepare the pasta a baba: 
  1. mix the dry ingredients together.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/baba1-008-1_3.jpg)
  2. add the eggs, then the water, and knead, you can use the kneader or the bread machine, or even knead by hand, as I did myself.   
so you have to raise the dough so that the dough is well ventilated. 
  3. stir in the butter and continue to mix in the same way 
  4. put in savarin molds, or a well-buttered baba, 
  5. place in a slightly hot oven (not light) or you have placed a pot filled with hot water. as these cakes usually lie to rise in a damp place. 
  6. after emergence, bake in an oven heated to 180 degrees. 
  7. let cool, and sprinkle with syrup, and water it as much as you can, if your baba are hot the syrup should be a little warm not hot, if the babas are cold, the syrup should be hot. 
  8. water the first time, let it absorbed a little, then water another time, let it absorb too, then water a 3rd time, and a 4th time. 



[ ![the baba has orange, fast and easy](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/baba-019-1_31.jpg) ](<https://www.amourdecuisine.fr/article-42502008.html>)

garnir les babas selon votre gout 
