---
title: borek cheese / Bulgarian borek
date: '2013-02-06'
categories:
- jams and spreads

---
Hello everybody, 

here is a very delicious salty pie recipe, known as: Bulgarian Borek, no relation with [ bourek cheese ](<https://www.amourdecuisine.fr/article-55595087.html>) because it's BORNK are made with a dough called yufka dough, which is a flaky pastry with flour, it looks a bit like filo dough, but a little thicker than filo dough. 

it's a dough that you can find in oriental grocery stores. 

Note :   
Yufka paste has the property of not absorbing oil during cooking. 

so today Lunetoiles likes to share with us this Borek cheese recipe, or Bulgarian Borek. 

Ingredients: 

  * a pack of yufka dough 
  * 200 gr grated cheese (mixture of mozzarella and emmental) 
  * 500 ml of yoghurt 
  * 8 eggs 
  * 400 gr of feta 
  * 100 gr of melted butter (or replace with 3 to 4 tablespoons of olive oil) 
  * 100 ml of sparkling water 
  * 100 ml of milk 
  * 1 large pinch of baking powder 
  * grated cheese to sprinkle 
  * no salt, because the cheese is already salty 



method of preparation: 

  1. Preheat the oven to 200 ° C. 
  2. Prepare a rectangular dish by lining it with parchment paper. In a bowl mix yogurt, milk, baking soda and 7 eggs (keep 1 for last), beat everything. 
  3. Then add the grated cheese, the crumbled feta cheese and add the melted butter. Mix well. 
  4. Place a first sheet of dough in the dish fold the sides of the sheet in the middle to have the flat cover the surface of the mixture and reproduce the operation until the dough is used up. Finish with the egg / cheese mixture. 
  5. Pre-cut the börek square. In a small bowl mix the remaining egg and the sparkling water, pour over the top of the börek, sprinkle with grated cheese and bake for 30 to 40 minutes in a preheated oven at 200 °. 
  6. Once the börek is golden, moisten it with a little water and cover it for about 15 minutes with a damp cloth. Serve hot or warm with as accompaniment or as a starter. 


