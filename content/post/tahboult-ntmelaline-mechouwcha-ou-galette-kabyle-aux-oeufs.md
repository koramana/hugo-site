---
title: Tahboult n'tmelaline, mechouwcha or Kabyle galette with eggs
date: '2018-01-23'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries
tags:
- Cakes
- Algerian cakes
- Cake
- To taste
- Algeria
- Ramadan 2017
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tahboult-n%E2%80%99tmelaline-mechouwcha-ou-galette-kabyle-aux-oeufs-2.jpg
---
![Tahboult n'tmelaline, mechouwcha or Kabyle cake with eggs 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tahboult-n%E2%80%99tmelaline-mechouwcha-ou-galette-kabyle-aux-oeufs-2.jpg)

##  Tahboult n'tmelaline, mechouwcha or Kabyle galette with eggs 

Hello everybody, 

I invite you today to enjoy this **Tahboult n'tmelaline, mechouwcha or Kabyle galette with eggs** , A snack super easy to prepare. 

the **tahboult n'tmelaline** , or the mchewcha is a small Kabyle delicacy, and as well done in other regions of Algeria. It is a cake with very light eggs and especially the flour version, a well airy cake and all honeyed, huuuuuuuuuum! 

in addition to all this the mchewcha is a snack very fast to realize, and you can see the video, to help you, if it is the first time that you will do the mchewcha! 

**Tahboult n'tmelaline, mechouwcha or Kabyle galette with eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tahboult-n%E2%80%99tmelaline-mechouwcha-ou-galette-kabyle-aux-oeufs-1.jpg)

Recipe type:  cake, Algerian pastry  portions:  8  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 8 eggs 
  * 8 tablespoons flour well rounded 
  * 3 tablespoons fine sugar 
  * ¼ teaspoon of salt 
  * ½ teaspoon vanilla powder 
  * 1 sachet of baking powder 
  * 1 glass of oil for frying 
  * liquid honey 



**Realization steps**

  1. Beat the eggs with the whip, add the sugar, vanilla powder, salt, flour and baking powder. 
  2. Stir well until the mixture becomes homogeneous. (you can use the whisk for a well sparkling mixture) 
  3. In a heavy-bottomed pan, heat the oil and pour the dough, 
  4. lower the heat and let it cook slowly. 
  5. invert the cake on the other side to cook the other side 
  6. After cooking, put it on a paper towel to eliminate excess fat. 
  7. Heat the liquid honey and water the tahboult still hot. 



![Tahboult n'tmelaline, mechouwcha or Kabyle cake with eggs 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tahboult-n%E2%80%99tmelaline-mechouwcha-ou-galette-kabyle-aux-oeufs-3.jpg)

Bonne dégustation. 
