---
title: lemon cherbet, Algerian lemonade from Ramadan
date: '2018-05-20'
categories:
- juice and cocktail drinks without alcohol
- ramadan recipe
tags:
- Ramadan 2018
- Easy cooking
- Ramadan
- Juice
- Drinks
- Summer kitchen
- cocktails

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cherbet-au-citron-limonade-algerienne-du-ramadan-3.jpg
---
[ ![lemon cherry, Algerian lemonade from Ramadan 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cherbet-au-citron-limonade-algerienne-du-ramadan-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cherbet-au-citron-limonade-algerienne-du-ramadan-3.jpg>)

##  lemon cherbet, Algerian lemonade from Ramadan 

Hello everybody, 

Among the most important recipes that must be imperatively present on **the Algerian table during Ramadan** , there is the **lemon cherbet** , or **cherbate el karess** . A refreshing drink and energizing, which will help ended the evening of Ramadan in style. 

When one of my readers came to ask me the recipe of this lemon cherry, or Algerian lemonade Ramadan, I was super surprised and surprised not to find the recipe on my blog, yet I remember having posted at all beginning of my blog !!! Maybe I deleted the recipe without paying attention. 

In any case, I took advantage that my husband invites his friends home for dinner this weekend, and I have prepared a large amount of this **cherbet el karess.** Besides, one of his friends from Boufarik was so surprised by the recipe that reminded him of the good **cherbet el karess of boufarik** That he did not leave empty-handed, and he asked my husband if I could prepare a bottle for him every day of Ramadan, hihihihih, that does not bother me frankly because I do it every day, it's too good a drink to taste imperatively. 

more 

[ ![cherbet or Algerian lemonade for ramadan](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cherbet-ou-citronade-alg%C3%A9rienne-pour-ramadan.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cherbet-ou-citronade-alg%C3%A9rienne-pour-ramadan.jpg>)   


**lemon cherbet, Algerian lemonade from Ramadan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cherbet-au-citron-limonade-algerienne-du-ramadan.jpg)

portions:  6  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 5 large unprocessed lemons well juicy 
  * 5 c. tablespoon of sugar (or according to your taste) 
  * 2 tbsp. orange blossom water (I really like his taste, you can put less, if you do not like it too much) 
  * 4 tablespoons of milk 
  * 1.5 L of boiling water 



**Realization steps**

  1. Cut a lemon into slices. 
  2. boil half a liter of water. 
  3. place the lemon slices in a heatproof pitcher, and pour over the boiling water. let it infuse well. 
  4. when this lemon infusion is cold, squeeze the remaining lemons to extract the juice and add to the brew. 
  5. Add sugar, orange blossom water and milk. 
  6. add the remaining liter of water and stir to dilute the sugar well 
  7. rectify and adjust the taste of your drink by adding sugar if needed. 



[ ![cherbet, Algerian drink 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cherbet-boisson-alg%C3%A9rienne-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cherbet-boisson-alg%C3%A9rienne-1.jpg>)
