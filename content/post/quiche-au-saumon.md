---
title: salmon quiche
date: '2014-03-04'
categories:
- juice and cocktail drinks without alcohol
- ramadan recipe
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/Tarte-saumon-pinards4_thumb.jpg
---
#  salmon quiche. 

[ ![salmon quiche](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/Tarte-saumon-pinards4_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/Tarte-saumon-pinards4.jpg>)

Hello everybody, 

I love salmon, and spinach nothing to say, we must thank the popeye the sailor for that, and this recipe is very rich, it is a sharing of my friend Lunetoiles, thank you very much my dear for all your recipes that you send me regularly, you are very very nice. 

so you want just like me the recipe for this pie:   


**salmon quiche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/Tarte-saumon-pinards2_thumb.jpg)

portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** all the ingredients were organic 

  * 300 g frozen spinach in branches 
  * 500 g frozen salmon in a net 
  * 1 puff pastry 
  * 40 cl of semi-thick cream 
  * 10 cl of milk 
  * 3 eggs 
  * salt 
  * pepper 



**Realization steps**

  1. Preheat the oven to 200 ° C (thermostat 7). 
  2. Garnish the dough in a pie plate and poke the bottom with a toothpick. 
  3. Reserve in the fridge while waiting to prepare the filling. 
  4. Cook the spinach with a little salt and a spoon. of water. 
  5. When cooked, drain well. Let cool. 
  6. Cook the salmon in boiling water for 11 minutes. 
  7. Crush with a fork into small pieces and let cool. 
  8. Beat the eggs with salt and pepper with the cream and add the milk. Mix well 
  9. Add spinach and salmon. Mix everything well. 
  10. Pour over the puff pastry. Bake 45 to 50 minutes at 200 ° C (thermostat 7). 



[ ![Salmon spinach pie1](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/Tarte-saumon-pinards1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/Tarte-saumon-pinards1.jpg>)

merci ma chere Lunetoiles pour cette delicieuse recette. 
