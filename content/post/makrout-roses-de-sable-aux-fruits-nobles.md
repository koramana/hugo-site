---
title: makrout sand roses with noble fruits
date: '2015-07-16'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- sweet recipes
tags:
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-rose-de-sable-4.jpg
---
[ ![pink sand makrout 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-rose-de-sable-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-rose-de-sable-4.jpg>)

##  makrout sand roses with noble fruits 

Hello everybody, 

Another makrout recipe you will tell me !!! I'll tell you you're in the presence of the kings of cakes, an inexpensive cake compared to some cakes with honey like the [ baklawa ](<https://www.amourdecuisine.fr/article-baklawa-constantinoise.html>) ... However this cake does not stop being revisited again and again and again by the gifted of the kitchen. 

Today it's with **Oume Tarek** we will see this pretty version of makrout, but she preferred a stuffing with noble fruits, such as walnuts, hazelnuts and almonds, but still that the stuffing is a choice of each here **Oume tarek** used nuts in the stuffing and even in the dough. His review of our beautiful and delicious makrout is not only on the side of the stuffing, but also on the side of his look, using silicone molds that gives it the appearance of sand roses ... The result in any case is bluffing ... So with the ingredients and the details, all you have left is the act ...   


**makrout sand roses with noble fruits**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-rose-de-sable-6.jpg)

portions:  30  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** Dough 

  * 2 measures of medium semolina 
  * 1 measure of mixture: ½ semolina fat + ½ flour 
  * 1 pinch of salt 
  * 1 measure of melted butter 
  * 1 pinch of cinnamon 
  * ½ cup of baking soda 
  * vanilla 
  * ½ glass of finely ground walnuts 
  * water + orange blossom water 

the joke 
  * 3 measures of crushed walnuts 
  * 1 measure of caster sugar 
  * Orange tree Flower water 



**Realization steps** Start by preparing the dough: 

  1. mix semolina, flour, butter and salt 
  2. sand in your hands until the fat is fully absorbed 
  3. add cinnamon, baking soda and vanilla 
  4. grind the walnut until it becomes a dough 
  5. introduce the nut with the semolina mixture 
  6. then wet the dough to pick it up with the mixture of water and orange blossom water. 
  7. it is best to let the dough sit overnight in the fridge. 
  8. prepare the stuffing by mixing the ingredients. 
  9. the next day, push the dough into the silicone molds 
  10. fill with stuffing, and cover again with dough:   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-rose-de-sable-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-rose-de-sable-5.jpg>)
  11. bake in a preheated oven at 180 degrees, it is well monitored as the sides take a color, turn off the fire from below, and turn on the one from above, to cook the cakes from above. 
  12. at the exit of the oven, let cool your cakes before they turn out, because they are well sanded and may crumble. 
  13. dip your cakes in honey, and decorate with shiny golden food to give them the effect of bright sand roses. 



[ ![pink sand makrout 6-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-rose-de-sable-6-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-rose-de-sable-6-001.jpg>)
