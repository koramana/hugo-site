---
title: Cream of carrot soup with coriander
date: '2017-11-28'
categories:
- Algerian cuisine
- Cuisine by country
- salty recipes
- soups and velvets
tags:
- Soup
- Vegetarian cuisine
- Healthy cuisine
- inputs
- Carrot soup
- Easy recipe
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/11/velout%C3%A9-de-carottes-%C3%A0-la-coriandre-2.jpg
---
![carrot soup with coriander 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/velout%C3%A9-de-carottes-%C3%A0-la-coriandre-2.jpg)

##  Cream of carrot soup with coriander 

Hello everybody, 

In this cold, there is no better than a velvety, and this carrot soup with coriander made our happiness this afternoon. After running in all directions my husband and I all morning, this soup was comforting for us. 

I had prepared the soup quickly before going out without mixing. When I returned, I boiled for a few more minutes and then mixed and added the cream. We had a great time with this creamy carrot and coriander soup, very unctuous, and especially warm with this cold. 

You can see the recipe in video, it's a simple that you can make this soup with your children! 

**Cream of carrot soup with coriander**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/velout%C3%A9-de-carottes-%C3%A0-la-coriandre.jpg)

**Ingredients**

  * 250 g carrots cut into medium pieces 
  * 1 onion 
  * 1 potato 
  * ½ bunch of fresh coriander 
  * ½ c. powdered cumin 
  * ½ c. black pepper 
  * Salt (towards the end if needed) 
  * ½ glass of liquid cream 
  * 1 liter of [ chicken broth ](<https://www.amourdecuisine.fr/article-bouillon-de-poulet-fait-maison.html>)
  * 3 c. extra virgin olive oil. 



**Realization steps**

  1. start with the onions cut roughly, in the oil until you have a translucent color. 
  2. add the carrots cut and the potato. 
  3. then add the chopped coriander, chicken broth, black pepper, powdered cumin and cook. 
  4. When carrots are cooked, mash. 
  5. add the liquid cream, let it boil a little and remove from the heat. 
  6. enjoy hot! 



![carrot soup with coriander 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/velout%C3%A9-de-carottes-%C3%A0-la-coriandre-1.jpg)
