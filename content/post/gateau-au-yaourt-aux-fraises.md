---
title: Strawberry yoghurt cake
date: '2017-03-28'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/G%C3%A2teau-au-yaourt-aux-fraises-3.jpg
---
##  [ ![strawberry yogurt cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/G%C3%A2teau-au-yaourt-aux-fraises-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/G%C3%A2teau-au-yaourt-aux-fraises-3.jpg>)

##  [ Strawberry yoghurt cake  ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/G%C3%A2teau-au-yaourt-aux-fraises-21.jpg>)

Hello everybody, 

A delicious strawberry yogurt cake made by our dear Lunetoiles, the texture of the cake is superb, and with these strawberries that decorate the cake, it gives only a desire to prepare a good bowl of milk to taste this delight. 

The recipes that Lunetoiles to achieve in this strawberry season are waiting to be published very soon, I want to publish them one after the other for you to enjoy this season, but it's just the lack of time that stops me, I can not even publish my recipes and videos that I realized, but I will try to organize and do my best.   
[ ![Strawberry yoghurt cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/G%C3%A2teau-au-yaourt-aux-fraises-1.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/G%C3%A2teau-au-yaourt-aux-fraises-1.1.jpg>)   


**Strawberry yoghurt cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/G%C3%A2teau-au-yaourt-aux-fraises-4.jpg)

Recipe type:  Dessert  portions:  8  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * 500 g strawberries 
  * 250g of flour 
  * 2 large eggs 
  * 6 tablespoons of sugar 
  * 1.5 teaspoons of baking powder 
  * 125 g of butter or vegetable butter 
  * 300 g Greek yogurt 
  * icing sugar to sprinkle 
  * flaked almonds 



**Realization steps**

  1. All ingredients must be at room temperature. 
  2. Mix the butter with the sugar, then add one egg after the other. 
  3. Sift the flour with the baking powder. 
  4. Add the flour to the egg / butter / sugar mixture alternately with the yoghurt and mix very gently, preferably with a fork. 
  5. When all ingredients are well mixed, take a square pan measuring 21 cm x 26 cm or round with a diameter of about 20/22 cm 
  6. Grease it and sprinkle with flaked almonds around the mold. 
  7. Pour the dough, which should be very dense, and smooth. 
  8. Arrange strawberries that have been cleaned and cut in half. 
  9. Bake for about 50 minutes at 160 º C (use a wooden pick to check the cooking, it must come out dry). 
  10. Cool the strawberry cake to room temperature. 



[ ![Strawberry yoghurt cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/G%C3%A2teau-au-yaourt-aux-fraises-21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/G%C3%A2teau-au-yaourt-aux-fraises-21.jpg>)
