---
title: homemade hamburger bread
date: '2016-10-20'
categories:
- Mina el forn
- pain, pain traditionnel, galette

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/sauce-pour-burger-1.jpg
---
##  homemade hamburger bread 

Hello everybody, 

yesterday the children had a great desire to **burgers** , and I remembered this recipe for **homemade buns or burger bread** I had seen in one of my books, which I was anxious to realize, because pictures of the book, we could see **shooting** , very airy, **super light buns** . 

And this was the result, and it must be said that before realizing [ burgers ](<https://www.amourdecuisine.fr/article-hamburgers-fait-maison.html> "homemade burgers") more than half of the buns disappeared ... the children kept asking for more. Try the recipe, and you'll understand why these buns disappear in no time. 

in the book, the steps for the realization of this **homemade hamburger bread** are in "cup", so cup, and I used, the "cups special measures" that I have, knowing that 

1 cup = 240 ml 

the ingredients for 16 **hamburger buns** small (for children) 

if you want in bigger you can realize 10 buns to have beautiful **burgers** American sizes, hihihihi .... 

Do not forget to make a [ burgers sauce ](<https://www.amourdecuisine.fr/article-sauce-pour-burger-maison-la-meilleure.html>) it is to fall 

![homemade burger sauce](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/sauce-pour-burger-1.jpg)

**homemade hamburger bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pain-pour-hamburgers.CR2_thumb.jpg)

Recipe type:  bread  portions:  16  Prep time:  45 mins  cooking:  20 mins  total:  1 hour 5 mins 

**Ingredients**

  * 3½ cups all-purpose flour 
  * 1 sachet of instant baker's yeast 
  * ¼ cup of potato starch (bag of starch to prepare mashed potatoes) 
  * 3 tablespoons milk powder 
  * 1 tablespoons sugar 
  * 1½ teaspoons of salt 
  * 4 tablespoons of soft butter 
  * ⅔ cup warm water (I was not satisfied with the texture of the pasta I had to add more water) 
  * ½ cup warm milk 



**Realization steps**

  1. mix all the ingredients to have a dough that picks up 
  2. knead the dough in your hands for almost 15 minutes to have a very smooth dough (it may stick, so use the butter gently, to knead and detach from your hands.) If you do it at the knead, knead for 7 minutes , and if you made it to the bread machine, do the pate program, and go do something else, hihihihi) 
  3. Continuing with the people who are working on the hands, or the kneader, gather the dough into a ball and put it in a buttered container, and let rise until it doubles in volume almost between 60 and 90 minutes. 
  4. Once the dough is lifted, degas it gently. and turn it into 16 pieces, to make 16 buns. 
  5. butter lightly with mussels of 23 cm each, and place the buns in according to your taste, I like that the buns stick to each other during the lifting and cooking, but if you want separate buns, plan larger mussels.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pain-pour-hamburgers-buns-super-facile-et-leger_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pain-pour-hamburgers-buns-super-facile-et-leger_thumb1.jpg>)
  6. Cover the mussels, and let the buns rise until they are stacked against each other and swollen enough, about 60 to 90 minutes. 
  7. preheat the oven to 180 ° C. 
  8. cook the buns for 15 to 20 minutes (or depending on your oven), until the buns turn a nice golden color. 
  9. Remove the loaves from the oven and immediately brush with melted butter. You will need 1 to 2 tablespoons of melted butter, it will give a shine, and a final texture to the buns, to fall. 



and for the recipe [ homemade burgers ](<https://www.amourdecuisine.fr/article-hamburgers-fait-maison-112343938.html>)

[ ![homemade burger 022.](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/hamburger-maison-022._thumb1.jpg) ](<https://www.amourdecuisine.fr/article-hamburgers-fait-maison-112343938.html>)
