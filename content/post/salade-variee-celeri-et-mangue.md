---
title: Varied salad Celery and mango
date: '2015-12-01'
categories:
- salads, salty verrines
tags:
- Easy cooking
- Holidays
- Cucumber
- tomatoes
- Ramadan
- inputs
- accompaniment

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-de-celeri-2.jpg
---
[ ![Celeri salad 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-de-celeri-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-de-celeri-2.jpg>)

##  Varied salad Celery and mango 

Hello everybody, 

A varied salad super rich in color and taste, a salad with celeri, mango, tomato, avocado and so on ... For this varied salad, give free rein to your emagination. This is a salad of my friend and reader for more than 5 years, Fleur Dz who tells us that in this salad, it does not stop only on the mangoes to give sweetness to this sweet salad, sometimes it puts even raisins or dry cranberries. 

**Varied salad Celery and mango**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-de-celeri.jpg)

portions:  4  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 1 mango 
  * 3 to 4 celery stalks 
  * ½ cucumber 
  * 2 to 3 seedless tomatoes 
  * 1 lawyer 
  * 1 apple 
  * ½ red pepper 
  * ½ yellow pepper 
  * the vinaigrette: 
  * 2 tablespoons of vinegar 
  * 6 tablespoons extra virgin olive oil 
  * 1 cup dry basil. 
  * mayonnaise 
  * salt 



**Realization steps**

  1. prepare the vinaigrette and set aside. 
  2. peel and seed the apple 
  3. cut the mango into pieces 
  4. cut the avocado, and the rest of the ingredients into pieces. 
  5. mix everything and sprinkle with vinaigrette. 
  6. stir again, and place in a bowl. 
  7. you can sprinkle some dry basil on top. 



![Celeri salad 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-de-celeri-1.jpg)
