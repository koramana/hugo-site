---
title: cream of rice with roasted figs
date: '2016-09-06'
categories:
- dessert, crumbles and bars
- rice
tags:
- To taste
- The start of the school year
- Nuts
- Algeria
- desserts
- Ramadan
- Milk rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/creme-de-riz-aux-figues-roties-2.jpg
---
![rice cream with roasted figs 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/creme-de-riz-aux-figues-roties-2.jpg)

##  cream of rice with roasted figs 

Hello everybody, 

This cream of rice with roasted figs is a super delicious dessert and super rich in taste. I had these few figs that I really wanted to eat for my children, and as I personally like roasted figs, I wanted to prepare this little dessert to taste it, but in my opinion the roasted figs alone were not sufficient. 

I began to reflect on one thing with which I present my figs, and I remembered that my children really like the "rice pudding" that they take a few times in the school canteen, so I told myself I will make them the "home made" version of a cream of rice well scented with vanilla that they will not forget to forget. 

The result, I succumbed before my children for this delight, hihihihih, and I began to tell myself if one of them leaves his share, I'm not going to force him (of course for me to eat) to my disappointment, they had left nothing, hihihihih 

![rice cream with roasted figs 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/creme-de-riz-aux-figues-roties-1.jpg)

**cream of rice with roasted figs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/creme-de-riz-aux-figues-roties.jpg)

portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients** for roasted figs: 

  * 4 fresh figs, very ripe. 
  * 4 c. butter 
  * 1 little honey 
  * cinnamon powder 

for the cream of rice: 
  * 150 g of rice 
  * 800 ml of milk 
  * 70 g caster sugar 
  * 50 of liquid cream 
  * 1 vanilla pod 
  * decoration: 
  * nuts in pieces. 



**Realization steps** preparation of roasted figs: 

  1. preheat oven in grill position 
  2. rinse the figs and dry them. 
  3. remove the peduncle and melt them in four with a sharp knife 
  4. press a little down to give a nice shape to the figs 
  5. place a little honey and 1 tbsp in the heart of each fig 
  6. sprinkle with a little cinnamon and powder and place in a baking tin   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/figues-roties.jpg)
  7. cook for 8 to 10 minutes 

preparation of the cream of rice: 
  1. Pour the milk, cream, sugar and vanilla bean melted in a saucepan. Wear to shudder. 
  2. remove the pod, rub the seeds and leave on medium heat 
  3. when the milk is hot, pour the rice and mix. 
  4. Cook for 30 minutes, stirring regularly. 
  5. when the rice is well cooked, present in bowls 
  6. place a rotten fig in each bowl, place half a walnut in each fig 
  7. sprinkle some figs with cooking juices, and sprinkle a few pieces of walnuts. 
  8. Enjoy tepid, it's a treat. 



![rice cream with roasted figs 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/creme-de-riz-aux-figues-roties-3.jpg)
