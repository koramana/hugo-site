---
title: lham hlou bet tmar / tajine hlou with stuffed dates
date: '2013-07-11'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-hlou-bet-tmar-aux-dattes-farcies-recette-de-ramada1.jpg
---
![tajine-hlou-bet-Tmara - in the dates-stuffed - Recipe of ramada.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-hlou-bet-tmar-aux-dattes-farcies-recette-de-ramada1.jpg)

##  lham hlou bet tmar / tajine hlou with stuffed dates 

Hello everybody, 

Here is the tajine hlou that I prepared for the first day of Ramadan, and that I wanted to share with you. The recipe for this tajine hlou, or lham hlou with stuffed dates, this salty sweet tajine is a delight with every bite, the stuffed dates have a taste to sublimate the taste buds. 

A recipe that will fill your Ramadan tables well, or even for special occasions, or when you have guests.   


**lham hlou bet tmar / tajine hlou with stuffed dates**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-lham-hlou-aux-dattes-farcies.CR2_.jpg)

Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients**

  * 500 g lamb meat 
  * 2 tablespoons of butter 
  * 1 cinnamon stick 
  * 1 teaspoon cinnamon powder 
  * 3 star anise, or badiane 
  * 1 beautiful clip of saffron filament 
  * 1 small pinch of salt 
  * 2 tablespoons caster sugar 
  * 1 teaspoon of rose water (or else orange blossom water) 
  * Tapered almonds 

to stuff the dates: 
  * 30 well-kept dates (not too soft) 
  * 250 g ground almonds and ground very finely 
  * 1 tablespoon icing sugar 
  * 1 teaspoon cinnamon powder 
  * 1 teaspoon of rose water (or orange blossom water) 
  * 1 egg white 



**Realization steps**

  1. in a large pot, melt the butter, brown the meat with the spices, and the pinch of salt, 
  2. sprinkle with water to cover the meat, cover and cook. 
  3. When the meat is cooked, add the sugar. 
  4. cook for a few minutes, add the rose water, and remove the meat. 

stuff the dates: 
  1. make a cast with a knife, remove the core and reserve. 
  2. Prepare the stuffing by mixing the almonds, sugar, cinnamon, rose water, and pick up with the egg white, 
  3. stuff the dates with this mixture, and if there is some stuffing left, you can make rhombus and fry in the oil, to present them with the tajine. 
  4. Place the dates gently in the sauce, let them cook in almost 5 to 6 minutes, 
  5. just the time to give the egg white in the stuffing time to cook. 
  6. The sauce normally be well reduced now, the dish is ready. 
  7. Put the meat back in the sauce and at the time of serving decorate with a little flaked almonds. 



![tajine de-stuffed dates - Recipe for ramadan.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-de-dattes-farcies-recette-pour-ramadan.CR2_1.jpg)
