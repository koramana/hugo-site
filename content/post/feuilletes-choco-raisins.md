---
title: Choco / raisin puff pastries
date: '2007-12-17'
categories:
- diverse cuisine
- dessert, crumbles and bars
- sweet recipes

---
** ingredients  **

_Square puff pastry blocks_

Almond cream : 

**_125g of soft butter  
125g of sugar   
2 eggs   
2s of flour   
\+ or - 150g ground almonds   
a few drops of bitter almond extract _ **

Raisins, chocolate chips   
Eggs for gilding   
Ice sugar for the decor 

** Preparation  **

Prepare the almond cream by creaming the butter with the sugar ... then add the eggs 1 to 1 then the flour ... end with the almond powder and the bitter almond extract ... you must obtain a cream that is not too flowing ... 

Take a block of puff pastry, cut it in the middle vertically ... spread the first rectangle of dough in length and cut it in the middle horizontally ... spread a little more the two strips of pasta ... 

![](https://www.amourdecuisine.fr//import/http://images.imagehotel.net/)

Put one on a baking sheet, brush all around beaten egg, spread a layer of almond cream (not too much otherwise, it will come out cooking) ... sprinkle with raisins or chocolate chips ... 

![](https://www.amourdecuisine.fr//import/http://images.imagehotel.net/)   
![](https://www.amourdecuisine.fr//import/http://images.imagehotel.net/)

Take the other dough strip, fold in length and cut every 1 cm approx. without going to the end with a wheel or a sharp knife ... 

![](https://www.amourdecuisine.fr//import/http://images.imagehotel.net/)

![](https://www.amourdecuisine.fr//import/http://images.imagehotel.net/)

![](https://www.amourdecuisine.fr//import/http://images.imagehotel.net/)

Lay it on the almond cream, weld the edges well ... 

![](https://www.amourdecuisine.fr//import/http://images.imagehotel.net/)

Make cuts all around with the back of a knife ... 

![](https://www.amourdecuisine.fr//import/http://images.imagehotel.net/)

Brown with beaten egg and bake at 180 ° C until nicely browned ... 

Proceed in the same way with the rest of the dough ... 

Cool, sprinkle with icing sugar and cut into strips ... 

![](https://www.amourdecuisine.fr//import/http://i103.photobucket.com/albums/m156/soulefl/)

So, there are some that I made with little chocolates, and others without, because my husband asked them simple without anything other than the stuffing of almonds.   
It's a delight 
