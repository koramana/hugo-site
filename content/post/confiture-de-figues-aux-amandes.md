---
title: fig jam with almonds
date: '2012-10-10'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-de-figues-aux-mandes-038.CR2_thumb-300x278.jpg
---
I filled my basket of figs, I go home, and I think my husband has to buy, because he knows how much I like this fruit. 

recipes with this delicious fruit, I did, and I will publish it very quickly. and the first of course and what I like most to fight the winter cold, it's a good jam, and in the cupboard, jam I have, hihihihi a big stock, but not  Fig jam, so I go to work. 

**fig jam with almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-de-figues-aux-mandes-038.CR2_thumb-300x278.jpg)

**Ingredients**

  * 1 kg of figs, 
  * 550 gr of powdered sugar (my fig was very sweet, otherwise you can go up to 800 gr according to your figs) 
  * the juice of 1 lemon, 
  * 1 cinnamon stick (optional) 
  * 1 handful of roasted almonds (optional too, it's just for a little crunchier in the recipe) 



**Realization steps**

  1. In a large bowl, pour the previously washed figs, hulled and cut in half, the sugar, the freshly squeezed lemon juice and the cinnamon stick. 
  2. Mix everything gently, cover and leave between 8 to 10 hours. 
  3. pour the fig mixture into a large pot. 
  4. Bring to a boil, stirring regularly. 
  5. In the first broth, lower the temperature over medium heat, and cook for 45 to 50 minutes while crushing the fig pieces while cooking. 
  6. As the jam gets a semi-thick consistency and freezes when you pour a drop on the worktop. She is ready. 
  7. add the crushed almonds and mix 
  8. fill the previously sterilized pots and return them after having closed them well, until total cooling. 



![jam of figs 014.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-de-figues-014.CR2_thumb1.jpg)
