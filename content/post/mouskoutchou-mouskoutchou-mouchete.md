---
title: Mouskoutchou, spotted mouskoutchou
date: '2011-10-31'
categories:
- Healthy cuisine
- Cupcakes, macarons, et autres pâtisseries
- dessert, crumbles and bars
- recettes sucrees

---
Hello everybody, 

As you already know, the mouskoutchou is the favorite cake of my husband, you can find on my blog "The" [ mouskoutchou ](<https://www.amourdecuisine.fr/article-28557136.html>) , the [ chocolate mouskoutchou ](<https://www.amourdecuisine.fr/article-38356047.html>) , sometimes I do it with the taste of fruits, like here a [ muskoutchou with orange ](<https://www.amourdecuisine.fr/article-cake-a-l-orange-et-noix-de-coco-76562605.html>) . 

this time, always remaining in the recipe of the nature mouskoutchou, ie only to the taste of vanilla, I present you the mouskoutchou speckled, the only problem, when I wanted to do it this time, I did not have much vermicelli chocolate, but as I do my shopping, I redo it, and I will put you new pictures. 

ingredients: 

  * 5 eggs 
  * 1 glass of sugar (I use a 180 ml glass as a measure) 
  * 1 coffee vanilla sugar 
  * 3 tablespoons of oil 
  * 4 tablespoons of milk 
  * 1 teaspoon of baking powder 
  * 2 glasses of flour 
  * 2 to 3 tablespoons of chocolate vermicelli 



method of preparation: 

  1. preheat the oven to 180 degrees C 
  2. in a large salad bowl, beat the eggs (I do not separate here0 
  3. whip until it makes a nice mousse 
  4. add the sugar gradually, and the vanilla, while whisking, and try to ventilate your foam 
  5. add the oil and whip always 
  6. introduce the milk, and whip without stopping 
  7. now sift the flour, and using a spatula, introduce the yeast flour mixture gradually, until it becomes a homogeneous mixture. 
  8. butter and flour a kouglouf mold, or a savarin mold 
  9. return to your mixture, add the vermicelli chocolatee and mix 
  10. fill the mold, and bake for 40 to 45 minutes depending on the oven 
  11. bake remove from the oven, just leave a little and turn out the cake 



a delicious cake, easy to make, knowing that it is optional the addition of vermicelli. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
