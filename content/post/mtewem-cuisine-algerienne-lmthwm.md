---
title: mtewem Algerian cuisine المثوم
date: '2013-04-06'
categories:
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/mtewem-020_thumb1.jpg
---
##  mtewem, Algerian cuisine المثوم 

Hello everybody, 

before yesterday I received a comment asking me the recipe of the **mtewem Algerian cuisine** , it's a [ Algerian dish ](<https://www.amourdecuisine.fr/categorie-12135732.html>) , meat-based and **minced meat** season with garlic, whose name mtewem, because "garlic" in Arabic is "toum" so I said to the girl is on the category [ red meat dish ](<https://www.amourdecuisine.fr/categorie-10678928.html>) , she tells me I can not find, 

I go looking, and to my surprise I had never post this dish on my blog, while I do it very often because my husband loves a lot, and even children eat meat in this way (luckily for me). 

and as I had **minced meat** at home, I told myself I'm preparing it and I'm posting it this time.   


**mtewem, Algerian cuisine المثوم**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/mtewem-020_thumb1.jpg)

portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 300g of meat in pieces (mutton for me here) 
  * 250g minced meat 
  * 1 handful of chickpeas dipped the day before 
  * 3 tablespoons of table oil (I cook with extra virgin olive oil) 
  * 5 cloves of garlic 
  * 1 small onion 
  * salt, black pepper, cumin, red pepper 
  * ½ teaspoon of concentrated tomato 



**Realization steps**

  1. method of preparation: 
  2. In a pot over low heat, sauté the pieces of meat in the oil with chopped onion 
  3. add 2 cloves garlic crushed, cumin and other spices 
  4. add the chickpea grip and wet with the water until the meat completely covers and cook 
  5. Now prepare the meatballs, in a salad bowl, put the minced meat, add the crushed garlic, salt and spices. form balls 3 cm in diameter. 
  6. Once the meat is cooked, immerse the meatballs in the sauce, 
  7. simmer a few minutes, to have a reduced sauce 



I hope Rihem that you will try the recipe, and thank you for drawing my attention to the fact that the recipe was not on my blog. 

المثوم هو طبق من الاطباق الجزائرية المعمولة بالحم المفروم و الحمص 

المقادير: 

(غ 300 قطع من لحم (خروف او عجل- 

200 غ 200 لحم مفروم- 

حفنة من الحمص المنقوع في الماء ليلة من قبل- 

\- 3 ملاعق كبيرة زيت- 

ثوم حبات 5 - 

فلفل اسود- 

فلفل احمر- 

ملعقة صغيرة كمون 

* ملعقة ملح 

* 1 بيضة 

* 1 لتر ماء دافىء 

طريقة التحظير 

1- في قدر ضعي اللحم و الزيت و اتركي اللحم يتحمص على نار هادئة, بينما تحضري (الدرسة) التي هي عبارة عن خليط التوابل كالأتي: 

اهرسي الثوم, الملح, الكمون, الفلفل الأسود, الفلفل الأحمرو اخلطي الكل مع بعض جيدا. ثم قسمي الدرسة إلى قسمين. حسن العالم في اللحم الذي يحمص في القدر بعدها اضيفي الحمص و الماء الساخن و اغلقي القدر و اتركيه يطهى لمدة 25 الى 30 دقيقة. 

2- في حين حضري اللحم المفروم في إناء و اضيفي عليه الدرسة و البيضة لتساعد على تماسل اللحم المفروم! 

اخلطي الكل مع بعض جيداثم شكلي كريات من اللحم المفروم و اتركيها على حدى. 

في هذا الوقت تفقدى اللحم الموجود في القدر ,,,,,,,, فإذا كان ناضج ضعي الكريات في المرق و اتركيها بضع دقائق حتى يتخثر المرق و شهية طيبة 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
