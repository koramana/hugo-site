---
title: pate of speculoos, homemade recipe
date: '2014-03-20'
categories:
- Algerian cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pate-de-speculoos-021_thumb1.jpg
---
[ ![pate-de-spiced-021_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pate-de-speculoos-021_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pate-de-speculoos-021_thumb1.jpg>)

Hello everybody, 

here is a delicious spread, the dough of speculoos, a paste that is not found here in England. 

fortunately, there are speculoos known here under the name of Lotus, or "caramelised biscuits", and so when we have the recipe for the speculoos paste, we can take the opportunity to achieve it. 

**pate of speculoos, homemade recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pate-de-speculoos-021_thumb1.jpg)

Recipe type:  spread  portions:  8  Prep time:  20 mins  cooking:  1 min  total:  21 mins 

**Ingredients**

  * 125gr of butter (you can reduce the butter so that it is not too liquid) 
  * ¾ a box of sweetened condensed milk 
  * 200g of speculoos 
  * 1 sheet of gelatin (it serves as an emulsifier) 
  * 2 teaspoons of lemon juice 
  * 1 tablespoon of neutral oil 



**Realization steps**

  1. soak the gelatin sheet in cold water. 
  2. melt the butter in the microwave 
  3. add the sweetened milk and crumbled biscuits, the oil and mix 
  4. heat the lemon juice and add the gelatin, 
  5. pour everything into the dish and continue mixing until the preparation is smooth. 



[ ![pate-de-spiced-010.CR2_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pate-de-speculoos-010.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pate-de-speculoos-010.CR2_thumb1.jpg>)
