---
title: baked potato, batata kbab
date: '2018-04-11'
categories:
- Algerian cuisine
- Dishes and salty recipes
tags:
- Full Dish
- Ramadan 2015
- Ramadan
- Ramadhan
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/batata-kbab-1.jpg
---
[ ![baked potato, batata kbab](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/batata-kbab-1.jpg) ](<https://www.amourdecuisine.fr/article-pomme-de-terre-au-four-batata-kbab.html/batata-kbab-1>)

##  baked potato, batata kbab 

Hello everybody, 

Here is a delicious dish of **baked potato, batata kbab** which will find its place on **the table of ramadan 2015** . A dish well appreciated, super easy to do, and too good. A dish has three cooking course, but do not worry it's a breeze ... 

The preparation of this dish begins with making the white chicken sauce with chickpeas, at the same time, we pass to fry the potatoes cut into pieces medium wide. Finally, place the potato frit in a baking pyrex dish, sprinkle with half of the sauce, garnish the dish with the chicken pieces, and bake for a nice color. golden. 

Voila for people who appreciate video recipes: 

{{< youtube rKoH16OvWn8 >}} 

  
You can, however, during the presentation sprinkle the dish with a little sauce, or degiste it as it is. This delicious baked potato dish "batata kbab" is a wonderful accompaniment to a beautiful [ matlou, matlou3 ](<https://www.amourdecuisine.fr/article-matloue-matlou3-matlou-pain-maison-au-tadjine.html>)

[ ![batata kbab 3](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/batata-kbab-3.jpg) ](<https://www.amourdecuisine.fr/article-pomme-de-terre-au-four-batata-kbab.html/batata-kbab-3>)   


**baked potato, batata kbab**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/batata-kbab-3.jpg)

portions:  4-5  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * chicken pieces 
  * potatoes 
  * 1 onion 
  * 1 clove 
  * 1 handful of chickpeas, or more according to your taste 
  * parsley 
  * oil 
  * salt and black pepper 



**Realization steps**

  1. clean the chicken, or the pieces of chicken, and put them in a pot, add the oil, finely chopped onion, salt, black pepper and crushed garlic, cook over low heat, add a little parsley, cover the whole with about 1 liter of water, and cook. 
  2. now, peel and clean the potato, cut it in length, a little wider than the normal fries, salt it a little bit. and go to the frying. 
  3. when the chicken is cooked, take it out of the sauce, take an oven-baking tin, place in the frits, sprinkle with half of the sauce, cover the pieces of chicken, and place in a hot oven, to give a beautiful color. 
  4. for people who love eggs, you can beat an egg and pour it on the surface, otherwise, it's like my husband, leave like that. 
  5. before serving, sprinkle each dish with a little sauce, and garnish with chickpeas and parsley. 
  6. you can garnish the dish with onions cut in small cubes if you like. 



[ ![batata kbab 4](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/batata-kbab-4.jpg) ](<https://www.amourdecuisine.fr/article-pomme-de-terre-au-four-batata-kbab.html/batata-kbab-4>)
