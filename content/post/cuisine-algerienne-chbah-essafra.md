---
title: Algerian cuisine Chbah essafra
date: '2017-05-09'
categories:
- Algerian cuisine
- Dishes and salty recipes
tags:
- Ramadan
- Ramadan 2017
- Algeria
- Tunisia
- dishes
- Full Dish
- tajine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra3.jpg
---
[ ![chbah essafra3](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra3.jpg>)

##  Algerian cuisine Chbah essafra 

Hello everybody, 

Algerian cuisine Chbah essafra, or essofra chbah, a [ Algerian dish ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , who usually prepares in Eastern cities **Algerian** , which is presented as an entry into **wedding parties** , or for other celebrations, or also during the month of Ramadan, because it is one of the [ ramadan recipes ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>) more **sacred** . 

a delicious dish **sweet savory flavors,** with delicious rhombuses **almonds** that garnishes it, a recipe that I usually prepare when I receive prompts, and how much it makes its success. 

it is still far from **Ramadan** if you want to prepare it, do not look for a **special occasion** it's a delicious dish that will really please everyone. 

**Algerian cuisine, Chbah essafra**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra1.jpg)

portions:  6  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients** for the tajine hlou: 

  * mutton 
  * dried prunes 
  * dried apricots 
  * some dried raisins 
  * 2 green apples (optional) 
  * ½ onion 
  * a little butter or smen (I prefer butter) 
  * 3 tablespoon of crystallized sugar (or according to your taste, if you like more) 
  * 2 cups of orange blossom water 
  * 1 pinch or 1 cinnamon stick 
  * 1 little saffron or dye 
  * 3 star anise 
  * 1 pinch of salt 

Essafra: 
  * 3 measures of almonds (270 grs for me) 
  * 1 measure of sugar (90 grs) 
  * egg yolk (2 for me) 
  * 1a 2 tablespoons of orange blossom water 



**Realization steps** first, prepare the dish: 

  1. arrange all the cut meat, butter or smen, finely chopped onion, dye, cinnamon and salt and star anise 
  2. cover with about ½ liter of water and cook. 
  3. in the meantime, pass the prunes, the apricots in a steamer and place the raisins in the orange blossom water to inflate, 
  4. then drain them, peel the apples and cut them in quarter, or so in large strips 
  5. dip them in the sauce, add the sugar and orange blossom water, 
  6. cook on low heat, 
  7. then add the prunes and apricots steamed 
  8. cook for just a few minutes and remove from heat. 

now the almond paste: 
  1. mix the blanched and ground almonds with the sugar 
  2. sprinkle with 2 tablespoons of orange blossom water 
  3. separate the egg yolks from the whites 
  4. slowly add the almond yellows while mixing at the same time until you have a firm dough 
  5. flour a plate on which you will work the almond paste 
  6. shape a roll with this paste 
  7. cut into slices of 1cm to 1cm and a half thick 
  8. place these washers flat and cut out triangles, or form squares 
  9. pass these pieces in the white of beaten eggs a little without it being necessary to mount it in snow   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/2011-08-01-chbah-essofra_thumb-300x243.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/2011-08-01-chbah-essofra_thumb.jpg>)
  10. fry in a fairly hot fry and then reduce the heat 
  11. remove and drain the almond paste 
  12. return to the egg white a second time 
  13. put them back in the frying a second time too 
  14. remove as soon as they have taken a beautiful golden color 
  15. now, put your pieces of almond paste, in the sauce, give a single boiling and remove them, during the presentation, garnish your dish, and sprinkle sauce 



**[ ![chbah essafra2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra2.jpg>) **
