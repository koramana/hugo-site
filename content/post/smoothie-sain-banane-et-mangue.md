---
title: healthy banana and mango smoothie
date: '2015-09-20'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-sain-banane-mangue-1.jpg
---
[ ![healthy smoothie banana mango 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-sain-banane-mangue-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-sain-banane-mangue-1.jpg>)

##  healthy banana and mango smoothie 

Hello everybody, 

A cool drink is always welcome, be it in summer, in autumn or in winter ... And when it's a homemade drink, healthy and full of rich and healthy foods, we will never say no. Moreover at home and for children a smoothie is a must they like to take everyday when they come back from school, I know I do not often post their smoothies, simply, by what in their smoothies, they put everything they have in their heads, everyone they like, his smoothie maker ... 

For this time, I share with you this delicious delicious and healthy banana and mango Lunetoiles smoothie, I hope you will like it, in any case, the smoothies are always good, try the ... 

The recipe in Arabic: 

**healthy banana and mango smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-sain-banane-mangue-2.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 2 frozen banana 
  * 1 fresh mango cut into pieces 
  * 1 natural yoghurt 
  * 280 ml mango juice (or a mixture of half orange juice / half mango juice) 



**Realization steps**

  1. Put banana, mango, yogurt and juice in a blinder; 
  2. Mix until smooth. 



[ ![healthy smoothie banana mango](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-sain-banane-mangue.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-sain-banane-mangue.jpg>)
