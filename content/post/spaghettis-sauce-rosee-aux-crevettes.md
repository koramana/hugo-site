---
title: Spaghetti with rosée sauce and shrimps
date: '2015-01-28'
categories:
- diverse cuisine
tags:
- Pasta
- Unique dish
- Algeria
- Easy cooking
- Express cuisine
- Morocco
- Sea food

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/spaghettis.jpg
---
[ ![spaghetti with pink sauce](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/spaghettis.jpg) ](<https://www.amourdecuisine.fr/article-spaghettis-sauce-rosee-aux-crevettes.html/spaghettis>)

**Spaghetti with rosée sauce and shrimps**

Hello everybody, 

Today is a recipe from a reader that I will share with you. Fleur Dz a passionate about cooking, who is used to sharing with me many of her achievements from my blog. And today, it is we who will benefit from his expertise. 

Fleur Sharing with us a delicious dish: Spaghetti sauce Rosée shrimp (the title I like) so we go to the tasting, but before that, here is what she told us: 

__ Pasta is the source of slow sugars and carbohydrates, which helps us to find a strong source of energy, especially by this cold. They have the property of being satiating. And who says pasta says "spaghetti" and "macaroni". 

I propose a 100% Quebec recipe, this is the first dish that my husband has prepared for me the only dish that can do hihihi. Really it is a delight that is prepared very quickly and with few ingredients. 

**Spaghetti with rosée sauce and shrimps**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/spaghettis.jpg)

portions:  4-5  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * a package of spaghetti 
  * A green pepper 
  * 3 tablespoons parsley 
  * 3 tablespoons of vegetable oil 
  * 1 carrot cut into cubes 
  * 1 celery stalk, diced 
  * a box of tomatoes for dough or some fresh tomatoes 
  * a package of shrimp 500 g. 
  * -a 60ml glass of liquid cream 
  * -basilic for decoration, a mint leaf 
  * -sel and pepper according to taste 
  * -A nut of butter 



**Realization steps**

  1. In a large saucepan of salted boiling water, cook the pasta until al denté, drain and add the butter 
  2. In a big pole made pepper come back with parsley a few minutes 
  3. Add the carrots and celery, mix well and season with salt and pepper 
  4. Stir in the tomatoes or your special tomato sauce. 
  5. let simmer for a few minutes then add your shrimp and cook slowly (your sauce must be very consistent). 
  6. To the desired consistency, add your cream and remove from heat 
  7. Serve your dish decorated with basil 


