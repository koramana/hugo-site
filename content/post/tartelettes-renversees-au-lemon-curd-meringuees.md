---
title: tartlets spilled with lemon curd meringue
date: '2016-02-04'
categories:
- sweet recipes
- pies and tarts
tags:
- Lemon meringue pie
- Lemon pie
- pies
- Algerian cakes
- Genoese
- Pastry
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelettes-renversees-lemon-curd-meringuee-1.jpg
---
[ ![overturned tartlets lemon curd meringue 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelettes-renversees-lemon-curd-meringuee-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelettes-renversees-lemon-curd-meringuee-1.jpg>)

##  tartlets spilled with lemon curd meringue 

Hello everybody, 

Here is a delicious recipe for Lunetoiles, these pretty mini pieces mounted way tarry lemon meringue that she called: tartlets reversed 2 stages with lemon curd and meringue ... And here is what she tells us about the recipe: 

I had bought mini tarts, a sponge cake to make tarts faster, (you can always [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-a-l-ananas.html>) House). This is good for those times when you want to taste a sweet treasure or take a gourmet dessert quickly. I'm honestly not disappointed, I took 2 boxes and I already planned to garnish them with lemon cream for [ the lemon pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee.html>) , And then put meringue on top of it. 

Then I had the idea to make a meringue decor as for the Mont Blanc, except that I did not have the socket mont blanc! 

So, I had recovered a toothpick cap, which did very well! 

As a result, they are very good, well tart, and not too sweet.   


**overturned tarts with lemon curd meringue**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelettes-renversees-lemon-curd-meringu%C3%A9e-2.jpg)

portions:  6  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 2 boxes of 6 tartlets   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/genoise-lidl.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/genoise-lidl.jpg>)

Filling lemon cream (lemon curd): 
  * 430 ml of water 
  * 100 g of sugar 
  * 2 egg yolks 
  * 1 whole egg 
  * 45 g cornflour 
  * zest of 2 lemon 
  * 35 g of butter 
  * 120 ml of lemon juice 

Swiss Meringue: 
  * The 2 egg whites 
  * 100 g of sugar 



**Realization steps** lemon cream Filling / lemon curd: 

  1. In a saucepan put half the sugar (50 gr) in the water and bring to a boil. 
  2. In a bowl mix the egg yolks and the whole egg and the remaining sugar (50 gr). 
  3. Add the zest of lemon, then the starch of maize (corn) and mix well until smooth. 
  4. Pour the hot water into the egg mixture while whisking constantly! stirring with a whisk. 
  5. Pour back the mixture into the pan and simmer while whisking constantly! 
  6. When the mixture is bubbling and thickened, add lemon juice, mix well and bring to a boil again, stirring, remove from heat. 
  7. Cool to 40 ° C. 
  8. Add the butter and mix until the butter is completely dissolved and mixed with cream. 
  9. Cover with plastic wrap. The film should directly touch the cream to prevent the formation of a skin on the surface of the cream.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/creme-au-citron.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/creme-au-citron.jpg>)

Swiss Meringue: 
  1. Whisk the 2 egg whites and the sugar, using an electric mixer in a large bowl placed over a saucepan of boiling water (bain marie) to bring the egg-sugar mixture to 50 ° C , whisking constantly. 
  2. When the meringue reaches the temperature of 50 ° C (after a few minutes) 
  3. Remove the meringue from the water bath and whisk at high speed until stiff peaks and until the meringue is cooled to room temperature.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/meringue.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/meringue.jpg>)

Assembly: 
  1. Put the 6 mini bottom for tarts on a serving platter. 
  2. Garnish with lemon cream (lemon curd) using a bag with a socket. 
  3. Cover each piece with the other tartlets, so you have two floors 
  4. Top the bottoms generously with the lemon curd. 
  5. Decorate the top with the meringue that you will place in a socket pocket with the socket at Mont Blanc and if you do not have one, take a cap of a toothpick box that's what I did . (see photos)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/assemblage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/assemblage.jpg>)
  6. Burn the meringue with a kitchen torch. 
  7. You can also decorate with candied lemon chips. 
  8. Place the pie in the refrigerator a few hours in advance before tasting. 
  9. Enjoy! 



[ ![tartlets spilled with lemon curd meringue](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelettes-renvers%C3%A9es-au-lemon-curd-meringu%C3%A9es1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelettes-renvers%C3%A9es-au-lemon-curd-meringu%C3%A9es1.jpg>)
