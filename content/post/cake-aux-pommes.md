---
title: Cake with apples
date: '2014-01-16'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/cake-aux-pommes-4_thumb2.jpg
---
Hello everybody, 

I continue with the recipes of cakes, and this time, it is an apple cake .... you smell good ... 

you can imagine the sweetness of this cake with the juice that escapes apples when cooking ... 

hum ... just like me, you want to taste this recipe of lunetoiles, so why not try it? 

**Cake with apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/cake-aux-pommes-4_thumb2.jpg)

Recipe type:  cake dessert  portions:  12  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients**

  * 250g of flour 
  * 150 g of sugar 
  * 125 g of butter 
  * ½ glass of milk 
  * 1 sachet of baking powder 
  * 1 sachet of vanilla sugar 
  * 2 big eggs 
  * 1 pinch of salt 
  * 2 to 3 apples (depending on the size) 
  * 125 gr of ground almonds 



**Realization steps**

  1. Preheat the oven to 180 ° C (th.6). Butter a cake tin or lined with parchment paper. 
  2. In a terrine, break the eggs; add the sugars and beat well together. Add 1 pinch of salt, then gradually pour in the flour and milk. 
  3. When the mixture is homogeneous, always beat for just melted butter. Add the yeast and the almond powder. 
  4. Peel and cut 1 to 2 apples in cubes. And cut the other apples into thick slices. 
  5. Add the apples in cubes to the dough. 
  6. Mix with a spatula and pour into the mold, place on top of the apple slices by pressing them slightly. 
  7. Bake at 180 ° C (th.6), for 1 hour to 1:15. 
  8. Check the cooking with the tip of a knife, it must come out dry and clean. 
  9. At the exit of the oven, turn out, remove the paper if necessary and let cool on a rack. 



Apple cake 

Ingredients 

  * 250 g flour 
  * Sugar 150 g 
  * 125 g of butter 
  * ½ cup of milk 
  * 1 teaspoon baking powder 
  * 1 teaspoon of vanilla extract 
  * 2 large eggs 
  * 1 pinch of salt 
  * 2-3 apples peeled, cored and diced 
  * 125 g almons powder. 

preparation 
  1. Preheat the oven to 350 degrees F (175 degrees C, gas mark 6). Butter has loaf tin or lined it with parchment paper. 
  2. In a bowl, beat eggs, and sugar very well. Add 1 pinch of salt, then slowly add to the flour and add the milk. 
  3. When the mixture is well blended, for in the melted butter. 
  4. fold in the baking powder and almonds. 
  5. Peel and slice 1-2 apples into cubes. And cut the other apples into thick slices. 
  6. Add the diced apples to the batter. 
  7. Mix using a spatula and for the prepared pan. 
  8. Bake for an hour to an hour and 15 min. 
  9. test for doneness using the tip of a knife or skewer, it should come out clean and dry. 
  10. after removing from the oven, remove the cake from the tin and let it cool on a rack. 


