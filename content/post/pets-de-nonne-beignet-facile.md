---
title: nun farts, easy donut
date: '2013-02-15'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

---
Hello everybody, 

the little story: 

At the abbey of Baume-les-Dames, an inexperienced novice named Agnes, would inadvertently drop a spoonful of cabbage paste in the frying ... it gave the farts-de-nonne! 

all this to share with you these pretty donuts, our sublime "Lunetoiles" 

Ingredients (for 30 donuts around) 

  * 1 C. orange blossom 
  * 1 C. liquid vanilla extract 
  * 3 eggs 
  * 60 gr of butter 
  * 2 tbsp. tablespoons sugar 
  * 125 gr of flour 
  * 1/2 cuil. salt 
  * oil for frying 
  * icing sugar (decor) 



The choux pastry: 

  1. In a saucepan, pour 25 cl of water, 1 tbsp. vanilla, 1 tbsp. orange blossom. 
  2. Add 60 gr of butter in parcels, 2 tbsp. tablespoons sugar and 1/2 tsp. salt coffee. 
  3. Boil the water. 
  4. Simmer for about 5 minutes, until the butter has melted completely. 
  5. Out of the heat, sieve 125 gr of flour using a fine strainer 
  6. pour it all at once in the saucepan. 
  7. Cook, stirring vigorously with a wooden spoon, until the dough forms a homogeneous ball from the bottom and the wall of the pan. 
  8. Out of the fire, stir one by one 3 eggs. 



The frying: 

  1. Heat the oil in a fryer. 
  2. When they reach 180 ° C, form a dozen small puffs with two spoons and slide them into the boiling oil bath. 
  3. Let them cook for 4 to 5 minutes on each side. 
  4. The donuts turn themselves after a few minutes of cooking. Let them fry until well puffed and evenly golden. 
  5. Drain and place on paper towels. 
  6. continue until exhaustion of the dough 
  7. Drain and place on paper towels. 



Finishing : 

Assemble all the donuts on a large dish. Sprinkle them with icing sugar before serving hot. 

You can sprinkle the donuts of brown sugar, vanilla sugar or colored sugar, flavored with the most unusual scents: lavender, rose, geranium or violet. 

thanks to Lunetoiles for sharing 

thank you for your reading 

and do not forget, if you have tried one of my recipes, send me the picture on this email: 

[ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

do not forget to subscribe to the newsletter, to be aware of any new publication 

merci 
