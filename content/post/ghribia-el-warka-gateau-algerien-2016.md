---
title: Ghribia el warka Algerian cake 2016
date: '2016-05-17'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
tags:
- Ramadan
- Algeria
- Oriental pastry
- Ramadan 2016
- Eid
- Dry Cake
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/ghribia-el-warka-gateau-alg%C3%A9rien-2016.jpg
---
[ ![ghribia el warka algerian cake 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/ghribia-el-warka-gateau-alg%C3%A9rien-2016.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/ghribia-el-warka-gateau-alg%C3%A9rien-2016.jpg>)

##  Ghribia el warka Algerian cake 2016 

Hello everybody, 

Here is a delicious cake of Constantine cuisine: ghribia el warka, or ghribiyat el warqa. One of my readers last came to ask me the recipe, and I admit that although I have eaten the recipe several times by visiting a cousin of mine, who will never give the recipe when asked, under pretext : yes I will write it to you, or else: yes I send it to you by email, or again: oh you know me I make the recipe at random, so I can not give it to you correctly. 

So, I went back to my closest Constantinian friends and thank you very much, **Pipo jewelry** , gave me the recipe right away not even 30 seconds, and my friend **Wamani Merou** to make the recipe at the first opportunity, made the pictures and share them with ... Thank you my friends you are super nice. 

So ghribia el warka what is it? it's actually a super fondant stuffing like ghribia but more precisely a good praline peanut not too liquid, mixed up to have a paste that picks up, then roll in a baqlawa dough .... Yum Yum. 

**Wamani Merou** gave me a little trick to have a stuffing too sweet and that picks up great without cramming your mill or armor, add peanut butter to pick up the stuffing. The result is just perfect. I will try both versions: the recipe that Pipo jewelry passed me, and Wamani Merou's recipe with peanut butter. This delicious cake deserves to take the name of the Algerian cake for the year 2016, because it is a recipe that was shelved and that will surely make a nice buzz this year, because the ingredients must be within reach of everything the world! So we go to this beautiful recipe of: **Ghribia el warka Algerian cake 2016**

The recipe in Arabic: 

**Ghribia el warka Algerian cake 2016**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/ghribiat-el-warka-ghribiyate-el-warqa-gateau-algerien-2016-001.jpg)

portions:  40  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** for the pasta: 

  * 5 measures of flour 
  * 1 measure of melted butter 
  * rose water + water 
  * 1 pinch of salt 
  * for the stuffing: 
  * 3 measures of roasted and ground peanuts 
  * 1 measure of crystallized sugar   
If the stuffing does not pick up add the peanut butter. 

decoration: 
  * 1 egg 
  * 1 little roasted peanuts 



**Realization steps** to prepare the dough: 

  1. place the sifted flour and salt in a large bowl, 
  2. make a fountain in the middle and add the melted butter (it should not be hot) 
  3. sand at your fingertips and pick up the dough gradually with the rose water and water to have a paste that picks up. 
  4. Cover the dough in a ball and let stand. 

prepare the stuffing: 
  1. mix the ground peanuts with the sugar, in a good blinder until having a paste that picks up (if you mix more you risk to have a spread, pay attention) 
  2. pick up the peanut paste and form rolls of almost 3 cm in diameter. 
  3. Take the dough again, and finely spread it on a thickness of 1mm 
  4. place the stuffing pudding on the dough and roll the dough over the pudding twice and cut the excess dough, you can stick the end of the dough with a little egg white. 
  5. brush the top of the egg yolk pudding and sprinkle with chopped peanuts 
  6. cut diamonds like when making makrouts   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/fa%C3%A7onnage-de-ghribia-el-warka.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/fa%C3%A7onnage-de-ghribia-el-warka.jpg>)
  7. place the ghribia lozenges in a baking dish.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cuisson-de-ghribia-el-warqa.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/cuisson-de-ghribia-el-warqa.jpg>)
  8. Bake in a preheated oven at 180 ° C for 15 minutes, or until the ghribia turns a beautiful golden color. 
  9. Let cool before eating if you can resist! 



[ ![ghribia el warka algerian cake 2016 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/ghribia-el-warka-gateau-alg%C3%A9rien-2016-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/ghribia-el-warka-gateau-alg%C3%A9rien-2016-1.jpg>)
