---
title: onion pie
date: '2013-07-20'
categories:
- appetizer, tapas, appetizer
- idea, party recipe, aperitif aperitif
- salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tarte-tatin-aux-oignons1.jpg
---
Hello everyone, here are pretty tartlets with onions, all cute and all tasty. She is very beautiful this version of onion pie, made in small bites as made Lunetoiles .... Preparation time: 10 min Cooking time: 35 min Ingredients: 4 tablespoons olive oil 12 small sprigs of thyme 24 olives cut in half 12 cherry tomatoes 1 medium onion (try to find the little ones that will enter in the bottom of the muffin cups, cut into thin circles) 1 roll of puff pastry 1/2 cup of feta cheese, goat cheese or & gly; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.65  (  1  ratings)  0 

![tarte Tatin aux-oignons.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tarte-tatin-aux-oignons1.jpg)

Hello everybody, 

here are pretty tartlets with onions, all cute and all tasty. She is very beautiful this version of onion pie, made in small bites as made Lunetoiles .... 

**Preparation time : 10 minutes  ** **Cooking time : 35 min  **

ingredients: 

  * 4 tablespoons olive oil 
  * 12 small sprigs of thyme 
  * 24 olives cut in half 
  * 12 elongated cherry tomatoes 
  * 1 medium onion (try to find the little ones that will go into the bottom of the muffin cups, cut into thin circles) 
  * 1 roll of puff pastry 
  * 1/2 cup feta, goat cheese or ricotta 
  * Salt and pepper to taste 



![tarte aux oignons.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tarte-aux-oignons1.jpg)

Method of preparation: 

  1. Preheat the oven to 200 ° C 
  2. Spread olive oil, thyme, olives and tomatoes in the bottom of 12 muffin cups. 
  3. Cover with a slice of onion. 
  4. Cut out circles of the puff pastry (the circles should be slightly larger than the top of the muffin pan cavities, as they will shrink in the oven) and place them on the onions. 
  5. Gently squeeze the dough on the onions for a good fit. 
  6. Bake for about 20 to 25 minutes or until golden brown and well puffed. 
  7. Let cool for a moment before turning on a platter. 
  8. Add the cheese, salt and pepper to taste and serve immediately. 



![tart-with-onion-and-olives.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tarte-aux-oignons-et-olives1.jpg)
