---
title: Khobz eddar Constantinois butter bread homemade Eid
date: '2018-03-20'
categories:
- Mina el forn
- bread, traditional bread, cake
tags:
- Bread
- Ramadan 2016
- Ramadan
- Breakfast

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/04/khobz-dar-au-beurre.jpg
---
[ ![Khobz eddar Constantinois butter bread homemade Eid](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/khobz-dar-au-beurre.jpg) ](<https://www.amourdecuisine.fr/article-25345448.html/khobz-dar-au-beurre>)

##  Khobz eddar Constantinois butter bread homemade Eid 

Hello everybody, 

I'm putting you a super old recipe that dates on my blog since April 2010, I just changed the old photos of this khobz eddar butter, or homemade bread of Ramadan, because I love to make this bread during Ramadan. 

The photos are taken with my Iphone 5C, hihihih, during Ramadan, I really lazy out my camera, or do a montage to make a beautiful picture, hihihih. Rest that this delicious [ bread ](<https://www.amourdecuisine.fr/categorie-10678924.html>) homemade, which I also do with oil " [ khobz eddar has oil ](<https://www.amourdecuisine.fr/article-pain-maison-facile-khobz-eddar-aux-3-grains.html>) "Always lets me ask myself this question: is it a bread or a brioche? so much that it is light, very sweet on the palate, mellow, fragrant ... anything that is "yumi" can be the description of this bread. 

the video recipe is here: 

**Khobz eddar Constantinois butter bread homemade Ramadan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/khobz-dar-inratable.jpg)

portions:  8  Prep time:  45 mins  cooking:  20 mins  total:  1 hour 5 mins 

**Ingredients**

  * 2 tablespoons baking yeast 
  * 400 gr of semolina (or mixture of equal quantity flour / semolina) 
  * 40 gr of butter (when I'm not on a diet, I can go up to 80 gr of butter, hihihih) 
  * 1 egg 
  * 1 tablespoon of sugar 
  * 1 teaspoon of salt 
  * 3 tablespoons milk powder 
  * 1 teaspoon of yeast (optional) 
  * 1 tablespoon of orange blossom water (optional) 
  * 220 ml of warm water (for the water must see according to the degree of absorption of your semolina, you can go much more than that sometimes I arrive at almost 280 ml of water to have a very very sticky dough , the final result is bleffant) 
  * egg yolk for gilding + a little milk 
  * nigele grains for decoration (you can even add nigele seeds to the dough) 



**Realization steps** For this recipe, I put the ingredients as they are in order in my bread machine, and put my program on pizza dough 

  1. if you do it by hand, you mix your ingredients as if to make your normal bread, but without adding the butter 
  2. in the middle of kneading, when the dough sticks to the hands, brush a little butter on the work surface, and knead until all the butter is absorbed by the dough 
  3. repeat the process until the butter is used up. 
  4. knead well together, and let stand for 40 min or until the dough doubles in volume, 
  5. and then degas out your dough, and let it inflate again 
  6. when the dough doubles volume, transfer it into the oiling tray going to the oven (I put some cooking paper to avoid putting the oil in the tray). 
  7. let your bread rest in the tray, then shape your cake.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/khobz-dar.jpg) ](<https://www.amourdecuisine.fr/article-25345448.html/khobz-dar>)
  8. let it rest and inflate well, brush the surface well with the egg yolk and milk mixture and garnish to your liking.   
I like the taste of Nigel, I have garnished the surface with these pretty black grains. 
  9. bake a preheated oven at 180 degrees for 20 min (or depending on your oven) 



[ ![Khobz eddar Constantinois butter bread homemade Eid](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/khobz-dar-au-beurre-001.jpg) ](<https://www.amourdecuisine.fr/article-25345448.html/khobz-dar-au-beurre-001>)
