---
title: mint chocolate cream after eight
date: '2015-04-30'
categories:
- panna cotta, flan, et yaourt
- recette de ramadan
- recettes sucrees
tags:
- Ramadan
- Ramadan 2015
- verrines
- Holidays
- Dessert
- Easy cooking
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/creme-a-lafter-eight-chocolat-a-la-menthe.CR2_.jpg
---
[ ![mint chocolate cream after eight](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/creme-a-lafter-eight-chocolat-a-la-menthe.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-creme-chocolat-a-la-menthe-after-eight.html/creme-a-lafter-eight-chocolat-a-la-menthe-cr2>)

##  mint chocolate cream after eight 

Hello everybody, 

I am a big fan of chocolate, and especially dark chocolate, but if there is chocolate **after eight** at home, it's damned for me, because I do not stop at a square, it's 2, 3 or even 4 ... yes, I'm like that with the **after eight.** It must also be said that after a nice dose of After eight, I do not linger to sleep like a baby ... But since my birth, I do not take this chocolate for fear of not waking up if the baby crying. Do you think I'm making it worse? try !!! eat 3 or 4 squares and already if you have the chance to finish reading this recipe, so you have strong nerves, hihihihi and you must do medicine, so you're sure to do your sleepless nights, hihihihiih ... 

Otherwise for this cream of mint chocolate, his story begins with this box of After eight which has been in the closet for 2 months already, and I dared not open. Then when I recently realized the [ toffy caramel flan ](<https://www.amourdecuisine.fr/article-flan-maison-au-caramel-ou-caprices.html> "Homemade caramel flan or whims") I thought why not copper the same principle with After eight, and make a delicious chocolate cream with a wonderful mint scent? 

The result is just beautiful, a delight, a fragrance, a sweetness, an irresistible taste of this cream of mint chocolate, **after eight** . 

**mint chocolate cream after eight**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/creme-au-chocolat-et-menthe-after-eight.CR2_.jpg)

portions:  4  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 500 ml of milk 
  * 80 gr of After eight 
  * 2 egg yolks 
  * 2 tablespoons of cornflower soup 
  * Decoration: 
  * fresh cream whipped cream 
  * mint 
  * after eight 



**Realization steps**

  1. take the equivalent of 2 tablespoons of milk 
  2. in a microwave-safe bowl, place remaining milk and After-eight Chocolate Squares 
  3. remove from the microwave, stir to melt the chocolate 
  4. transfer the mixture to a heavy-bottomed saucepan 
  5. in a bowl, mix remaining milk, cornflour and egg yolks until a homogeneous mixture. 
  6. add this mixture to the chocolate milk, place the pan over low heat 
  7. stir with a spatula until the mixture thickens. 
  8. pour immediately into small ramekins, let cool a little and chill until serving time. 
  9. Decorate with whipped cream and mint, or according to your taste. 



[ ![mint chocolate cream after eight](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/creme-de-chocolat-a-la-menthe-after-eight-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-creme-chocolat-a-la-menthe-after-eight.html/creme-de-chocolat-a-la-menthe-after-eight-1-cr2>)
