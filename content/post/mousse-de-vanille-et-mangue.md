---
title: vanilla mousse and mango
date: '2016-12-16'
categories:
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mousse-vanille-mousse-mangue_thumb.jpg
---
##  vanilla mousse and mango 

Hello everybody, 

That's what I prepared as a dessert for my friend's son's birthday party, delicious and pretty sweet verrines, with vanilla and mango mousse, and I assure you that the guests ate two to three verrines, it must be said that it was very small verrines, hihihihi, 

Well yes, for a party where there are 6 adults and almost 20 children, I could not use big glasses, plus super nice with children in disposable tea glasses. So without delay, I give you the recipe:   


**vanilla mousse and mango**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mousse-vanille-mousse-mangue_thumb.jpg)

portions:  6  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** for vanilla mousse: 

  * 100g of mascarpone 
  * 100 ml of milk 
  * 200 ml whole cream 
  * 60 gr of sugar 
  * a sachet of vanilla sugar (but preferably a vanilla pod) 
  * 8 g of gelatin 

for mango mousse: 
  * 2 mangoes cut in pieces 
  * icing sugar according to taste (because it depends on the mango) 
  * 300 ml of fresh cream 
  * 6 g of gelatin 

deco: 
  * 1 mango for decoration, including small cubes to put between the two layers, and a beautiful half circle to decorate the top of the verrines 



**Realization steps** vanilla mousse 

  1. soften the gelatin in cold water 
  2. whip the cream and the whipped cream, add the mascarpone slowly, continue to whisk. 
  3. heat the milk and vanilla. Add the softened gelatin stir until the gelatin is dissolved. 
  4. Remove from heat and let cool. 
  5. Gently add the warm milk to the whipped cream, mix 
  6. fill the verrines that you have placed on a plane tilting to have this effect. 
  7. let this foam in a cool place   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2012-03-14-mousse-vanille-mangue_thumb1-300x224.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2012-03-14-mousse-vanille-mangue_thumb1-300x224.jpg>)

the mango mousse: 
  1. soften gelatin in cold water 
  2. Put the mango pieces, the icing sugar, the softened gelatine and that you dilute on a low heat in a little cream in the bowl of a blinder 
  3. mix for 1 minute or until it becomes a light mousse. 
  4. Add the cream and mix. 
  5. take the verrines and decorate the vanilla mousse with cubes of mango 
  6. using a pastry bag fill the verrines with the mango mousse 
  7. straighten them, and decorate them with a half circle of mango, and caramel decorations 
  8. put off 



I also prepared a decoration with caramel, but I did not do too well, I have to train for next time, hihihihi 

suivez moi sur: 
