---
title: Pastry
date: '2013-08-22'
categories:
- Chocolate cake
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/pate-sablee-copie-12.jpg
---
![pate-sablee-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/pate-sablee-copie-12.jpg)

Hello everybody, 

dough is broken, is a basic recipe used in pies and tarts. a recipe very easy to prepare ... 

I thank Lunetoiles for the detailed photos that allowed me to make a small photomontage video.   


**Pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/pate-sablee-11.jpg)

Recipe type:  basic recipe  portions:  8  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 190 gr of flour 
  * 1 pinch of salt 
  * 90 g of butter 
  * 45 ml of ice water (about 3 tablespoons) or 90 grams of yoghurt 



**Realization steps** In the robot: 

  1. With the help of a robot equipped with the drummer K, 
  2. mix the flour and the salt. Add the butter and mix for a few seconds at a time until the butter has the size of coarse sand. 
  3. Add water or yogurt and mix again until dough forms. 
  4. Add water if needed. Remove the dough from the robot and lower on a floured surface. 

By hand : 
  1. Stir the butter into the flour while sanding in your fingers to obtain a sandy texture. 
  2. Add water or yogurt by lifting and turning the mixture with a wooden spoon. 
  3. Add cold water if necessary, one spoonful at a time, just enough for the mixture to hold when pressed between fingers. 
  4. Lower on a floured surface. 
  5. Bake the dough in an oven at 180 ° C for 15 minutes using either cooking weights or dry beans or flour in a special clear cooking film. 
  6. Then remove the cooking weights or dry beans, or the flour with the paper and continue cooking for another 15 to 20 minutes. 


