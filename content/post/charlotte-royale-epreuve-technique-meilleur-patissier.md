---
title: royal charlotte technical event best pastry chef
date: '2014-11-25'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cakes and cakes
- sweet recipes
tags:
- desserts
- New Year

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-008.jpg
---
[ ![royal charlotte 008](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-008.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-008.jpg>)

##  royal charlotte technical event best pastry chef 

Hello everybody, 

No, I do not participate in the challenges imposed by **Mercotte** during **the technical test of the best pastry chef,** and this **Royal Charlotte** , is not the recipe given (that will be given) by Mercotte. I propose my **royal charlotte** Following several comments received yesterday by my readers, when I had to re-post [ strawberry charlotte ](<https://www.amourdecuisine.fr/article-charlotte-aux-fraises.html> "recipe Strawberry Charlotte") from Lunetoiles and that I had to mention the name of the **royal charlotte** . 

At the request of these girls, I went dragging to my kitchen after a hard day (Monday is the day of cleaning at home, lol, even do my laundry is Monday, besides there are three baskets Linen waiting to be ironed and put in the wardrobes, I think this is the most frustrating in the business !!!) 

So to go back to the **royal charlotte** I made the biscuit rolled yesterday after dinner. For my part I cheated and I used the jam of the trade, and not homemade. This morning, when I woke up, I put the gelatin in cold water. I cut my swiss rolls, I did the editing, I put in the fridge time to prepare the device **Bavarian** . 

I did not do it **vanilla / raspberry bavarois** because I was too lazy to prepare a custard, but if you want, here's how [ Bavarian with vanilla ](<https://www.amourdecuisine.fr/article-black-and-white-bavarois-noir-et-blanc.html> "black and white cake - bavarois black and white") I just made a strawberry mousse, prepared a frozen strawberry compote, in which I dissolved gelatin, and I finally mixed with whipped cream ... And here is a royal charlotte express! lol. 

**A small remark** : never use cream whipped cream, because many people ask me if they can put this whipped cream in the Bavarian, look, I used it just to decorate the royal charlotte, and not even 10 minutes it had already flow 

**royal charlotte technical event best pastry chef**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-013.jpg)

portions:  8  Prep time:  60 mins  cooking:  20 mins  total:  1 hour 20 mins 

**Ingredients** for the rolled 

  * 6 eggs 
  * 150 grams of sugar 
  * 150 grams of flour 
  * 1 cup of vanilla coffee 
  * 1 cup of baking powder 
  * 1 pinch of salt 
  * 2 drops of vinegar (to avoid the smell of eggs) 
  * for decoration: 
  * 250 ml of liquid cream, whipped cream with 4 to 5 tablespoons of sugar 
  * Strawberry jam 
  * some strawberries 
  * white tablecloth 

for the strawberry bavarois: 
  * 250 grs of frozen strawberries 
  * 2 to 3 tablespoons of sugar 
  * 8 grams of gelatin 
  * what's left of the whipped cream ingredients from above 
  * some strawberries cut into pieces, 



**Realization steps** prepare the biscuit rolls: 

  1. Start by beating the eggs and the foam sugar, to have a good foamy mixture, if you use the whip of a kneader, it will give a beautiful foam, otherwise if you use a whip like mine, tilt the whip to aerate the mixture well, make it for 8 to 10 min, until the egg sugar mixture, triple volume. 
  2. add vanilla and vinegar, 
  3. incorporate the mixture flour and baking powder, gently spoon per spoon, without breaking the foamy mixture of eggs, 
  4. mix with a spatula while aerating the mixture. 
  5. cover the tray with a baking paper, and oil a little, and pour gently your mixture. 
  6. bake in a hot oven, 180 degrees for 10 minutes, or depending on the capacity of your oven. 
  7. remove from the oven and let cool completely (to have a good roll does not allow to cook too much) 
  8. prepare a piece of aluminum foil, very wide, and after total cooling of the cake roll it in aluminum. and leave out. 
  9. Put the liquid cream in whipped cream with a little sugar until you have a whipped cream. 
  10. roll out the rolled biscuit, spread on top a thin layer of whipped cream, and spread over a thin layer of strawberry jam.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale.jpg>)
  11. roll the rolled biscuit again and place in a cool place so that it holds up well. 
  12. After at least 1 hour of rest, take the biscuit rolled and cut the fine washer by almost 1cm or a half, I do not use a knife, but rather a special cooking wire that I pass under the roll, I chose the dimension of my choice, I cross the wire and I squeeze it to cut.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-1.jpg>)
  13. cover the bottom of a large pyrex salad bowl with cling film. 
  14. place the slices of biscuits cut in the mold to cover the whole salad bowl.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-4.jpg>)
  15. Spread a layer of whipped cream to cover the cookies, this will help you to avoid the Bavarian flowing between the holes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-6.jpg>)
  16. put back in the fridge, and start by preparing the bavarois. 
  17. place the gelatine in cold water so that it swells   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/ramolir-la-gelatine.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/ramolir-la-gelatine.jpg>)
  18. place the strawberries in a saucepan with the sugar 
  19. let melt and crush well with a fork, or blender. 
  20. add gelatine and stir over medium heat until gelatin melts completely, let cool. 
  21. Take what is left of the whipped cream, pour over the fruit compote a little cooled (warm to the touch)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-7.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-7.jpg>)
  22. stir gently with the spatula to introduce the compote into the mousse. 
  23. Add the strawberry pieces.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-8.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-8.jpg>)
  24. cover with remaining biscuit rings and refrigerate for at least 4 hours.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-10.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-10.jpg>)
  25. After the Bavarian has taken, turn the royal charlotte on a wide plate, cover it with white topping if you like, and serve. 



[ ![royal charlotte recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-023.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-023.jpg>)

With the rest of the mousse and cake I realized these strawberry trifles: (recipe coming soon) 

[ ![strawberry trifles](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trifles-aux-fraises-031.jpg) ](<https://www.amourdecuisine.fr/article-trifles-aux-fraises.html/trifles-aux-fraises-031>)
