---
title: kaak- ka3k - dry cake
date: '2015-07-24'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
tags:
- To taste
- Algerian patisserie

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kaak-1.jpg
---
[ ![kaak 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kaak-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kaak-1.jpg>)

##  kaak, ka3k - dry cake 

Hello everybody, 

Here is a delicious dry cake known mostly in western Algeria, kaak, or ka3k is a dry cake that is prepared like a brioche but the result is rather a dry cake. Still, this Kaak is a delicious dry cake very fragrant. 

It's a recipe from my friend and reader: **Fardadou G,** who learned this recipe from the family of her husband from Mostaganem. The kaak is a must in the festivals of the Aid in the western regions of Algeria, as it is also often present for the snack, it is really worthwhile to plunge a piece of this delight, in his glass of tea, or coffee with milk, to increase even more its flavor and to give life to those perfumes of dry grains which it hides under this golden crust. 

**kaak, ka3k - dry cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kaak-1.jpg)

portions:  30  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 1 kg of flour 
  * 250 gr of icing sugar 
  * 1 tablespoon of vanilla 
  * 1 tablespoon chnane powder (melilot)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/chnan.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/chnan.jpg>)
  * 1 tablespoon of powdered anise seeds 
  * 1 packet of baking powder 
  * 1 tablespoon instant baker's yeast 
  * 1 egg 
  * ¼ liter of oil 
  * 1 tablespoon of orange blossom water 
  * water 



**Realization steps** Better that the grains are complete, and that you crush them when using them, to have kaaks well perfumed. 

  1. In a terrine mix the dry ingredients: flour, sugar vanilla, powdered grains, baking powder and baker's yeast. 
  2. pick up the dough with lightly whipped egg, oil, orange blossom water, and water slowly to get a homogeneous dough. 
  3. knead the dough well until it becomes smooth, if you have a petrin, it will be even better to use it. 
  4. let the dough rest at least 45 minutes (in winter let rest even more) 
  5. degas the dough and cut dough pieces the size of a clementine, 
  6. make small sausages, and with the help of a knife make cuts all along the rolls and close them to form rings   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kaak-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kaak-5.jpg>)
  7. on them even to make rings, arrange them as and when on the dishes in the oven. 
  8. Cover with a clean cloth and leave to rest according to the temperature of the room. 
  9. Brush the surface of the kaak with egg yolk and bake at 180 ° C in the previously preheated oven 
  10. note: 
  11. We must keep the kaak in a hermetic box. The best thing is to freeze them and take them out as they go. 



[ ![kaak, or ka3k dry cake from mostaganem](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kaak-ou-ka3k-gateau-sec-de-mostaghanem.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kaak-ou-ka3k-gateau-sec-de-mostaghanem.jpg>)
