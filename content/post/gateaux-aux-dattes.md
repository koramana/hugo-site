---
title: cakes with dates
date: '2012-11-21'
categories:
- dessert, crumbles et barres
- verrines sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makrout-aux-dattes_thumb.jpg
---
![cake with dates](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makrout-aux-dattes_thumb.jpg)

##  cakes with dates 

Hello everybody, 

We love all **cakes with dates** , this fruit fleshy and very energitic. The dates are very rich in sugars (glucose, fructose and sucrose), as they contain vitamins such as: B2, B3, B5 and B6, and a small amount of vitamin C as well as mineral salts (potassium and calcium). 

dates are also rich in chromium (passing the craving for sugar), as well as in dietary fiber, the _American Cancer Society_ recommend that you consume 20-35 grams of dietary fiber a day. 

so now I'm going to give you some recipes I made with this   
delicious fruit 

[ ![Makrout rolled](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/120x90-cz5t-ffffff11.jpg) ](<https://www.amourdecuisine.fr/article-makrout-roule-102845781.html>)

####  [ Makrout rolled ](<https://www.amourdecuisine.fr/article-makrout-roule-102845781.html>)

Makrout rolled - Makrout aux **dates** , fried, Algerian pastry hello everyone, here is a delicious pastry ... 

[ ![Baked Makrout](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/120x90-cz5t-ffffff21.jpg) ](<https://www.amourdecuisine.fr/article-makrout-au-four-108967959.html>)

####  [ Baked Makrout ](<https://www.amourdecuisine.fr/article-makrout-au-four-108967959.html>)

Water for stuffing: 500 gr of dough **dates** 1/2 cac of cinnamon 1/2 cac ... 

[ ![Maamoul](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/120x90-cz5t-ffffff31.jpg) ](<https://www.amourdecuisine.fr/article-maamoul-112293141.html>)

####  [ Maamoul ](<https://www.amourdecuisine.fr/article-maamoul-112293141.html>)

Meanwhile prepare your stuffing: knead the dough **dates** after cleaning it with any pips, with the other ingredients ... 

[ ![Rfiss, or semolina cakes grilled with date paste Algerian cakes](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/120x90-cz5t-ffffff41.jpg) ](<https://www.amourdecuisine.fr/article-25345471.html>)

####  [ Rfiss, or semolina cakes grilled with date paste Algerian cakes ](<https://www.amourdecuisine.fr/article-25345471.html>)

Rfiss, or semolina cakes toasted dough **dates** Algerian cakes - this little Algerian delight has several nominations: Rfiss, Rfiss tounsi, Tamina, and full, ... 

[ ![Bradj - semolina cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/120x90-cz5t-ffffff51.jpg) ](<https://www.amourdecuisine.fr/article-bradj-gateau-de-semoule-100622694.html>)

####  [ Bradj - semolina cake ](<https://www.amourdecuisine.fr/article-bradj-gateau-de-semoule-100622694.html>)

other nominations, but they are beautiful sanded patties, and filled with pate **dates** bien aromée de poudre de cannelle, et poudre de clou de girofle, un délice, je ne… 
