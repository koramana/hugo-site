---
title: minestrone soup
date: '2016-01-29'
categories:
- soups and velvets
tags:
- Defi soups and velvets
- Vegetable soup
- Vegetables
- Spices
- Vegetarian cuisine
- Hot Entry
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-2.jpg
---
[ ![minestrone soup 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-2.jpg>)

##  minestrone soup 

Hello everybody, 

Last time I had a great desire to make [ berkoukes ](<https://www.amourdecuisine.fr/article-berkoukes-bercouces.html>) , or the Algerian lead soup. It must be said that in winter, this soup is very warm especially if it is very pungent. What I like with the lead soup is that we can put all the vegetables we love in it. But here I am looking for the ingredients, impossible to find the bag of pellets, yet I'm sure it's not just one bag that I have. 

I lost 30 minutes to search, then I gave up, and decided that my bercoukes became a minéstrone soup. In any case in the end, I enjoyed it too much because my soup was super good. 

[ ![minestrone soup 1a](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-1a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-1a.jpg>)   


**minestrone soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-1.jpg)

portions:  6  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients**

  * 3 c. tablespoon of olive oil 
  * 1 chopped onion 
  * 2 cloves garlic 
  * 1 zucchini cut into small cubes 
  * 1 celery stalk cut into small cubes 
  * 1 carrot cut into small cubes 
  * 1 diced potato 
  * 1 pougnée of peas. 
  * 3 peeled and crushed tomatoes 
  * ½ tablespoon of tomato paste. 
  * 150 g of white beans soaked overnight in the water or as for me in box 
  * 1 liter of chicken broth 
  * 1 glass of pasta 
  * salt pepper 
  * 2 tablespoons Parmesan (freshly grated for topping) 
  * dried basil powder. 



**Realization steps**

  1. Heat the olive oil in a heavy-bottomed saucepan, add the onions and cook over low heat for 5 minutes, stirring occasionally until tender. 
  2. Add crushed garlic, zucchini, celery, carrots, tomatoes and peas. 
  3. Cover and cook for about 10 minutes, stirring occasionally. 
  4. Drain the dry beans, rinse and add to the contents of the pan. 
  5. Add the broth, cover and let simmer, add the diced potatoes. 
  6. When the vegetables are tender, add the pasta in the pan, cook uncovered for 8 to 10 minutes until the pasta is toothed. Salt, pepper to taste. 
  7. Serve with grated parmesan cheese and a little basil on top 



[ ![easy minestrone soup.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-facile.CR2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-facile.CR2-001.jpg>)
