---
title: kalb el louz / ramadan dessert / قلب اللوز
date: '2013-07-07'
categories:
- Chocolate cake
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/kalb-el-louz.CR2_.jpg
---
[ ![kalb el louz](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/kalb-el-louz.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/kalb-el-louz.CR2_.jpg>)

##  kalb el louz / ramadan dessert / قلب اللوز 

Hello everybody, 

I bring you this delicious recipe at the request of my readers, who can not find it ... 

Kalb el louz, or qalb elouz قلب اللوز, is the favorite recipe of Algerian families during the holy month of Ramadan, and although it winds up everywhere this month, but Algerian women love to prepare it at home. 

I have to tell you that this cake is a little capricious ... yes it's a cake that can succeed today, and miss tomorrow .... I do not know why, but it's like that, since the years that I realize this same recipe, and that people try it, one person tells me to have managed it without problem, and another will tell me total mishandling .... 

so everyone's lucky, hihihihihihi ... .. 

for me it's the real thing, it's the best recipe I've made. it is the qalb el louz, or kalb elouz, who has always tasted our Ramadan table.   


**kalb el louz / ramadan dessert / قلب اللوز**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-el-louz-kalb-elouz.jpg)

portions:  20  Prep time:  60 mins  cooking:  25 mins  total:  1 hour 25 mins 

**Ingredients**

  * 500g of semolina 
  * 250g of sugar (next time I will reduce the quantity, because it was a bit sweet for me) 
  * 125g of melted butter 
  * 125 ml of liquid: 65 ml of orange blossom water and 60 ml of water 

for the syrup: 
  * 1 liter of water 
  * 500 g of sugar 
  * juice of lemon 
  * a glass of orange blossom water 



**Realization steps** the day before: 

  1. mix semolina and sugar 
  2. add the melted butter, and mix with the fingertips, especially do not knead 
  3. finally wet with half of the liquid (reserved the rest for the next day) and work as when you wet the couscous. 
  4. Cover and chill all night. (important step) 

the same day: 
  1. Leave the bowl at room temperature for about 30 minutes, gently shred the semolina grains, and wet with the remaining liquid, which is kept. 
  2. butter a baking dish, the dimensions of the one I used are 33 * 23 cm (13 * 9 inches) 
  3. prepare the stuffing: 1 measure almonds for ¼ cup sugar, 2 tablespoons melted butter, and 2 teaspoons cinnamon. this step is optional, for my part, I did not poke my qalb el louz. 
  4. Arrange small piles of dough with your fingertips in the mold, when it is uniform, spread the stuffing on top and then cover the rest of the dough in the same way. if you do the unfilled version, take larger piles from the beginning and do only one layer 



[ ![qalb el louz](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-el-louz.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-el-louz.CR2_.jpg>)

[ ![Recently Updated](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/Recently-Updated_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35768313.html>)

always use your fingertips to flatten so that it is homogeneous, then cut portions with a large knife making the knife stand out each time, do not slide it. 

Brush with melted butter (100g) (here a little accident with the butter, I try to spread more evenly when it began to cool, we see the traces of the brush, but it does not matter!)   
Decorate each serving with an almond   
Cook at 200 C for 1 hour, or until it is golden brown. 

During cooking prepare the syrup by adding orange blossom water at the end   
Water at the oven exit, using a ladle. you may not use all your syrup, because it depends on the quality of the semolina, but it is better to have more than not enough. you can check with the tip of a knife if it is soaked or not ... .. 

let cool then cut out .... 

[ ![qalb elouz or kalb el louz](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-elouz-ou-kalb-el-louz-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-elouz-ou-kalb-el-louz-.jpg>)

and good tasting 

thanks for your visit and comments: 

thank you for continuing to subscribe to the newsletter: 

do not forget by subscribing to tick one or both boxes at least if you want your registration not to be deleted. 

have a good day 

yet another recipe of qalb el louz: 

![qalb elouz 020](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/qalb-elouz-020_thumb1.jpg)

[ qalb el louz, ou chamia ](<https://www.amourdecuisine.fr/article-qalb-el-louz-ou-chamia-86960968.html>)
