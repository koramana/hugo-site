---
title: olive bread
date: '2017-11-06'
categories:
- bakery
- Mina el forn
- bread, traditional bread, cake
tags:
- Boulange
- Ramadan
- Ramadan 2017
- Algeria
- Easy cooking
- Sandwich
- Accessible Kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/pains-aux-olives-4_.jpg
---
![breads with olives 4_](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/pains-aux-olives-4_.jpg)

##  Olive bread 

Hello everybody, 

Hot in front !!! of the **rolls with olives** , just out of the oven, and barely cooled, I could only save 5 loaves for photos, hihihihi .... It must be said that at home we are in love with small homemade facts made by the care of "mom! 

And I can not resist without passing my hand to the dough, I find it a lot of pleasure to see this beautiful dough well kneaded, take volume, and then take the form of breads, cakes or buns that are our happiness, and that accompany any recipe to perfection. 

This recipe I made the first time in 2011 made the happiness of many people around me, repeated here and there on blogs and youtubes channels (thank you to those who cited the source ... But not everything the world has the courage to do it, which is a pity!) 

I redid the recipe of these olives bread video, just to share it with you, with new photos, do not forget to jump on my youtube channel to subscribe, it will make me very happy: (for the video, I divided the ingredients on 2 to have 6 rolls) 

**olive bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/pains-aux-olives_.jpg)

Recipe type:  bread  portions:  12  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * 450 gr of flour 
  * 150 gr of semolina 
  * 1 teaspoon of salt 
  * 1 tablespoon yeast baker 
  * 2 tablespoons milk powder 
  * 1 tablespoon of sugar 
  * 4 tablespoons of table oil or extra virgin olive oil 
  * a nice handful of pitted green olives 
  * warm water (between 280 and 300 ml) everything depends on dry ingredients. 
  * 1 tablespoon of black seed 
  * 1 egg yolk for decoration 
  * 1 tablespoon of milk for decoration 



**Realization steps**

  1. Mix the flour, semolina and salt. 
  2. Stir in the oil and sand well between your hands. 
  3. Add yeast, black seed, milk powder, and sugar. 
  4. add warm water and knead to have a smooth and malleable paste. 
  5. place in a large salad bowl a little oiled and let double volume. 
  6. Degas a little and add the olives (I had to cut the olives in two, because my children do not like large pieces, I left only a few whole pieces). 
  7. cut the dough into 12 equal pieces. 
  8. work on a surface a little floured to form your rolls 
  9. let shelter from drafts. 
  10. when the breads swell well, turn the oven on at 180 degrees C 
  11. brush the bread with the egg yolk and lai mixture, 
  12. make nicks with a sharp knife. 
  13. Put in leather for 20 minutes or according to your oven. 



![olive breads 5_](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/pains-aux-olives-5_.jpg)
