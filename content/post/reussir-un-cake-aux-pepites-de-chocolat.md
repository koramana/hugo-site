---
title: make a chocolate chip cake
date: '2016-04-01'
categories:
- Chocolate cake
- gateaux, et cakes
tags:
- To taste
- Algerian cakes
- Easy cooking
- desserts
- Pastry
- Fluffy cake
- Chocolate cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/cake-aux-p%C3%A9pites-de-chocolat-2.jpg
---
[ ![chocolate chip cake 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/cake-aux-p%C3%A9pites-de-chocolat-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/cake-aux-p%C3%A9pites-de-chocolat-2.jpg>)

##  make a chocolate chip cake 

Hello everybody, 

The children are on vacation and my daughter wanted to make a cake because she likes a lot (not that she too I like grapes, it's my favorite snack, hihihih), but I had more grapes, and Luckily there were chocolate chips, so our raisin cake quickly became a chocolate chip cake. 

In any case, my daughter was entitled to her first pastry lesson, and if it is your first cake, or you have a problem with your cakes, like many of my readers who come to ask me often why their cake is missed, poorly cooked, very thick crust, compact interior and pasty, follow these little tricks to have a cake well done. 

The base of the cake is always the same, you then vary to make a chocolate cake, a raisin cake, candied fruit cake, or any other cake of your choice. The most important thing is to respect these very important points: 

**First point:**

It is very important that when making a cake or cake, that all the ingredients are prepared in advance, and that the eggs and butter are at room temperature, do not be stupid to remove at the last minute butter and eggs from the fridge. 

**Second point:**

Sift the flour. There are some people who think that you have to sift the flour just to remove the impurities, but that's a good point, but we sift the flour to air it, and it is not compact and gives a cake and a well-aired cake. 

**3rd point:**

when we say butter ointment, it means butter ointment, hihihihi so bring out the butter well before, and if it is too cold and you are in a hurry, spend a few seconds in the microwave, just so that it is a little soft (do not melt) 

**4th point:**

A tip I had seen on a blog a long time ago, I do not remember what was the blog, but I admit that this trick helped me to have a better cake, and a finer crust , use icing sugar instead of crystallized sugar, because the fine sugar dissolves more easily in the butter to have a beautiful cream, so the sugar does not separate during cooking to rise to the surface and brown more quickly when cooking, it is sometimes because of this that the cake cooks faster on the surface while it is still not cooked inside. 

Personally, if I find a recipe that I like and that uses icing sugar, I reduce the amount given in fine sugar and I work with, the result is still nickel. 

[ ![chocolate chip cake 6](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/cake-aux-p%C3%A9pites-de-chocolat-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/cake-aux-p%C3%A9pites-de-chocolat-6.jpg>)

**5th point:**

When you make the butter and sugar cream, and you start adding the eggs one by one, sometimes the mixture separates, my solution, after the introduction of the eggs, I give a blow with the foot mixer, the mixture becomes super creamy and that's where I add the flour. If the melanhe is separated and you begin to introduce you may miss everything and after cooking you will have a cake too fat. 

**6th point:**

When you arrive at the time of adding the flour, do not mix too much: add the flour, introduce it with the spatula, when it is incorporated it is necessary to stop stirring. 

**7th point:**

Pour the dough in the mold and equalize the surface using a wet spatula, on one side you will have a smooth surface, and on the other side this little moisture will protect the surface from cooking too quickly and brown. 

**8 th point:**

for an even cooking place the cake mold on a baking tray, you will see, your cake will be just sublime. 

In any case, my daughter has followed these details, and here is the result, the only small problem, we had only three eggs, while my mold is ideal for 4 eggs, so the cake was not too high but it was super good, super mellow, just perfect baking.   


**make a chocolate chip cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/cake-aux-p%C3%A9pites-de-chocolat-5.jpg)

portions:  12  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 4 eggs or 220 gr at room temperature   
I prefer to weigh the eggs and in relation to their weight I make the rule of three to have the right measures of the other ingredients. 
  * 170 gr of ointment butter 
  * 170 gr icing sugar 
  * 240 gr of flour 
  * 1 teaspoon of baking powder 
  * vanilla extract 
  * 100 gr of chocolate chips 



**Realization steps**

  1. Preheat the oven to 150 ° C by placing the rack in the middle. 
  2. blanch the ointment butter and the icing sugar with the vanilla. Whip for at least 5 minutes to obtain a whitish cream. 
  3. Add the eggs one by one while whipping to make sure the egg is well incorporated. 
  4. sift the flour and yeast, add the chocolate chips in this mixture. 
  5. Introduce everything gently into the creamy base using a spatula, mix everything without doing too much. 
  6. pour this mixture into the mold previously buttered and floured, or lined with parchment paper. 
  7. Smooth the top of the cake with a wet spatula, place the pan on two baking sheets, and bake for about 45 to 50 minutes. 
  8. check the baking of the cake with a cooked knife tip should come out well dry. 
  9. remove from the oven and let cool on a rack before unmolding 



[ ![chocolate chip cake 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/cake-aux-p%C3%A9pites-de-chocolat-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/cake-aux-p%C3%A9pites-de-chocolat-4.jpg>)
