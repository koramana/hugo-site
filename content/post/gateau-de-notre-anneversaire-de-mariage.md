---
title: cake of our wedding anniversary
date: '2008-01-27'
categories:
- cheesecakes and tiramisus
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214558401.jpg
---
& Nbsp; & Nbsp; & Nbsp; yes it's already 4 years of marriage, time flies quickly ......... is not it god thank you I fell on a nice man malgres the fat to be away from the family but alhamdou lil Allah I do not feel loneliness ddonc up, a patit cake that I made for this occasion bearing the initials of our names, S & amp; R and 4 Hearts, which expresses 4 years of simple wedding decoration made at the end of the night because we are preparing for the return of my parents in Algeria which coincides with the date & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![bonj4](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214557981.gif)

![S7302195](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214558401.jpg)

yes it's already 4 years of marriage, 

time flies ......... is not it? 

thank god I fell on a nice man 

despite being away from family 

but alhamdu lil Allah 

I do not feel loneliness 

on the top, a patit cake that I made for this occasion 

bearing the initials of our names, S  & R 

and 4 Hearts, which expresses 4 years of marriage 

simple decoration 

done at the end of the night 

because we are preparing for the return of my parents in Algeria 

which coincides with the anniversary of our marriage. 

so I made a simple cake to the genoise, as it is very light 

I made the cream I made in the chocolate cake 

and another cream based double cream, also called thick cream. 

![S7302192](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214564971.jpg)

![rose8](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214558141.jpg)

Here is the detail: 

for the genoise: 

  * 120g flour 
  * 120g caster sugar 
  * 4 eggs 
  * 1/2 sachet baking powder 
  * 1 pinch of salt 



Beat the eggs with the caster sugar at maximum speed, add the flour to which the baking powder and salt have been added. 

Butter and flour a mold, pour the mixture and put in the oven 15min at 180 ° 

![S7302154](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214603231.jpg)

Cut in half horizontally. 

** Syrup:  **

  * 600ml water 
  * 300g caster sugar 
  * 1 sachet vanilla sugar 



Prepare a syrup with the three ingredients and sponge the two circles of sponge very well, until the syrup flows by the edges. 

![S7302157](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214603641.jpg)

chocolate filling: 

  * 75 gr of butter 
  * 250 ml of evaporated milk 
  * 50 gr of crystallized sugar 
  * 150 gr of chocolate 



![S7302158](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214604631.jpg)

now prepare the cream, putting in a saucepan, the butter, the evaporating milk, the sugar and the chocolate, bring to the heat and mix until all the ingredients are melange well, remove from the heat, let cool, and then beat the mixture well until it becomes thick and creamy. 

![S7302159](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214604921.jpg)

my cream was a little liquid because at the last minute I ran out of butter, and I used instead of margarine, I will never do it again. 

I put half of the cream on the first part of the cake 

and I covered it from the second part 

cream filling: 

  * 250 gr of double cream 
  * icing sugar according to taste 



![S7302162](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214606011.jpg)

beat the cream until the whip starts to leave traces 

add the icing sugar according to your taste 

![S7302160](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214606551.jpg)

put this cream in the pastry bag and garnish the cake according to taste 

![S7302174](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214607171.jpg)

![S7302189](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214607441.jpg)

![S7302191](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214607981.jpg)

the cake is very delicious 

there is not one left 

![S7302193](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214608381.jpg)

![S7302195](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/214558401.jpg)

with all my love 
