---
title: chocolate cream and strawberry easy
date: '2017-03-24'
categories:
- dessert, crumbles and bars
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/mousses-au-chocolat-aux-fraises.jpg
---
[ ![chocolate mousse with strawberries](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/mousses-au-chocolat-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/article-creme-chocolat-fraise-facile.html/mousses-au-chocolat-aux-fraises>)

##  chocolate cream and strawberry easy 

Hello everybody, 

This recipe is a recipe from one of my Fleur DZ readers that she posted by following the link [ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.") . 

My recipe is very easy and at the same time a real delight. A simple chocolate cream and strawberry, an easy recipe and creamy cream that comes from where, I do not know? I can not remember because I have been making this cream for a long time, especially when I have nothing for my children.  chocolate cream and strawberry easy 

Simple ingredients and present at all times, without delay I give you my recipe.  chocolate cream and strawberry easy 

**_ Ingredients for 4 ramekins  _ **   


**chocolate cream and strawberry easy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/mousse-de-chocolat-aux-fraises.jpg)

portions:  4 

**Ingredients**

  * 250 ml of pureed red fruits (for me strawberries but raspberries is better). 
  * 70 ml of sugar. 
  * A dark chocolate bar with 50% cocoa or less than that. 
  * 02 Egg yolks. 



**Realization steps**

  1. Place your chocolate break in pieces in a bowl and set aside. 
  2. In a small saucepan heat the fruit puree with half of the sugar until boiling. 
  3. In another bowl and with a whisk mix the egg yolks with the remaining sugar until blanching. 
  4. Gradually pour hot mashed potatoes over the egg mixture and cook for one minute, stirring constantly. After passing your preparation sieve pouring on the chocolate. 
  5. Let stand for a minute then using a whisk to mix gently to have a cream homogeneous. and pour it in your ramekins. 
  6. In the end refrigerated 4 hours and good tasting. 



[ ![chocolate strawberry mousse](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/mousse-aux-fraises-chocolat.jpg) ](<https://www.amourdecuisine.fr/article-creme-chocolat-fraise-facile.html/mousse-aux-fraises-chocolat>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
