---
title: Rhubarb and ginger jam
date: '2017-05-10'
categories:
- jams and spreads
- sweet recipes
tags:
- To taste
- Easy cooking
- Breakfast
- Compote
- Fast Food
- Algerian cakes
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/confiture-de-rhubarbe-et-gingembre.jpg
---
![rhubarb and ginger jam](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/confiture-de-rhubarbe-et-gingembre.jpg)

##  Rhubarb and ginger jam 

Hello everybody, 

I can not tell you the intense taste and the pep that ginger brings to this jam! this is the first time I put in a jam, and waw! I do not tell you the result. Yesterday already half of a jar is gone. 

In any case, I had planned this recipe rhubarb and ginger jam for the game, but this jam is so good that I wanted to share it with you soon. 

**Rhubarb and ginger jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/confiture-de-rhubarbe-et-gingembre-1.jpg)

**Ingredients**

  * 1 kg of rhubarb 
  * 900 gr of sugar for jam 
  * zest and juice of a lemon 
  * zest and juice of an orange 
  * 2 cm fresh ginger 



**Realization steps**

  1. Wash the rhubarb under water and slice into 2 cm pieces. 
  2. Place in a large, heavy-bottomed saucepan and add sugar, lemon zest and juice, orange zest and juice and chopped ginger. 
  3. Mix well, cover the pot and leave on the side for about 2 hours to allow the sugar to dissolve in the rhubarb juice. Stir the mixture occasionally to speed up this process. 
  4. Now place the pot on a medium fire. Stir until sugar is completely dissolved and bring to a boil. Continue cooking at a fairly fast pace until the rhubarb is really tender and the jam begins to set. 
  5. Remove the pan from the heat and leave on one side for 2-3 minutes before pouring into sterilized jars. 
  6. Close immediately and place the pots upside down until cool 



![rhubarb and ginger jam 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/confiture-de-rhubarbe-et-gingembre-2.jpg)
