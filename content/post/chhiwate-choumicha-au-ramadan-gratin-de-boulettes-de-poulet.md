---
title: choumwate choumicha with ramadan, chicken dumplings gratin
date: '2012-07-28'
categories:
- Brioches et viennoiseries
- Mina el forn

---
& Nbsp; hello everyone I do not offer a lot of gratins on my blog, because my children did not eat them, but the, I try to introduce them more flavors. this recipe is a recipe for the sublime Choumicha, I will not tell you since when I'm a fan of its Moroccan and world cuisine, its recipes always give you desire. so when I wanted to make this gratin with chicken, I remembered this recipe, which I noted when she went on the Moroccan channel 2M anyway, the recipe was too good, and the children were happy to see that 3 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.1  (  1  ratings)  0 

Hello everybody 

I do not propose a lot of **gratins** on my blog, because my children did not eat them, but there, I try to introduce them more flavors. 

this recipe is a recipe for the sublime [ Choumicha ](<https://www.amourdecuisine.fr/categorie-12244163.html>) , I will not tell you since when I'm a fan of his [ Moroccan cuisine ](<https://www.amourdecuisine.fr/categorie-12359409.html>) and global, its recipes always make you want. 

so when I wanted to do this gratin at [ chicken ](<https://www.amourdecuisine.fr/categorie-12364208.html>) , I remembered this recipe, which I noted when she went on the Moroccan channel 2M 

in any case, the recipe was too good, and the children were happy to see that 3 meatballs on their plates, one of them told me, I'll finish it in 2 bites, hihihihiih 

so I deliver my friends the recipe: 

  * 250 grams of chopped white chicken 
  * 125 grs potato cooked and mashed 
  * 1 chopped onion 
  * 2 tablespoons of oil 
  * 2 cm fresh ginger rapee (I did not have any) 
  * 1/2 teaspoon of salt 
  * 1 cup of turmeric coffee (I forgot to put, hihihihi) 
  * 2 tablespoons chopped coriander 
  * 20 gr of flour 
  * 4 tablespoons of oil 



to brown: 

  * 400 ml of [ Bechamel ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile-99197863.html>)
  * 50 gr grated cheese 
  * 2 sliced ​​onions 
  * 2 tablespoons of oil 
  * 1 pinch of salt 



method of preparation: 

  1. in a frying pan brown the chopped onion in the oil 
  2. in a container, mix the chopped chicken breast with mashed potato, golden onion, coriander and spices 
  3. make medium sized meatballs and put them in the flour 
  4. in an anti-adhesive frying pan, heat the oil and brown the meatballs, then set aside 
  5. in a pot over low heat, cook covered sliced ​​and salted onions in oil until golden brown 
  6. mix them to obtain a smooth puree and incorporate it in the béchamel with the cheese. 
  7. arrange the meatballs in a gratin butter dish, then top them with béchamel sauce with onion 
  8. brown in preheated oven at 180 degrees C for 25 min. 
  9. serve the gratin immediately 



thank you for your visits. 

have a good day 

cochez l’une des deux cases pour que votre inscription soit valide 
