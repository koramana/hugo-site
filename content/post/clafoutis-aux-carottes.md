---
title: Clafoutis with carrots
date: '2012-07-16'
categories:
- cuisine algerienne
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/clafoutis-aux-carottes-049_thumb1.jpg
---
##  Clafoutis with carrots 

Hello everybody, 

A light starter Clafoutis with melting carrots and rich in taste, full of delicious melting and crunchy carrots at the same time, swallowed up in a beautiful, well-scented layer 

A recipe that will only please you. 

**Clafoutis with carrots**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/clafoutis-aux-carottes-049_thumb1.jpg)

**Ingredients**

  * 4 carrots tops 
  * 1 teaspoon liquid honey 
  * 2 tablespoons chopped fresh coriander 
  * 1 cup of cumin 
  * 1 spring onion 
  * 2 eggs + 1 yellow 
  * 100 ml of milk 
  * 150 ml of cream 
  * 35 g cornflour 
  * 30 g of butter + 1 walnut 
  * Salt pepper 



**Realization steps**

  1. clean the carrots and bring them back. 
  2. Finely chop the spring onion. 
  3. fry the onion in the melted butter, add the grated carrots and the honey, 
  4. cook for a few minutes. Let cool. 
  5. Add coriander, cumin, and mix. 
  6. Beat the eggs with the yolk, then add the cornstarch and mix well. 
  7. finally add the milk and the cream. then season with salt and pepper 
  8. pour the mixture into small ramekins or mini cocottes. 
  9. cook for 15 to 20 minutes in an oven preheated to 160 ° C (theory 5). 


