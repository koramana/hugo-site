---
title: chorba beida
date: '2017-05-31'
categories:
- Algerian cuisine
- ramadan recipe
- chicken meat recipes (halal)
tags:
- ramadan 2017
- Healthy cuisine
- Easy cooking
- Algeria
- Vegetables
- Ramadan
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/chorba-beida-2.jpg
---
![chorba beida 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/chorba-beida-2.jpg)

##  chorba beida 

Hello everybody, 

Here is a soup that I like to prepare during Ramadan especially, chorba beida, or white soup. A soup that I like a lot, it changes well of the [ chorba Frik ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) which sometimes and despite all this delight, I find it still a little heavy. 

The chorba beida is despite all the simplicity in its preparation, remains a very tasty dish, with this little tangy taste of lemon that is added towards the end, in the small binder of the sauce, and the infinite sweetness of the vermicelli so melting . 

You can see how to prepare this white soup or white chorba video on my youtube channel: 

**chorba beida**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/chorba-beida-3.jpg)

Recipe type:  Hand  portions:  1  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * pieces of chickens 
  * handful of chickpeas dipped the day before 
  * 1 onion 
  * 1 tablespoon butter or smen 
  * 2 tablespoons of oil 
  * 1 pinch of black pepper 
  * 1 pinch of cinnamon 
  * 1 glass of vermicelli 
  * 1 egg yolk 
  * 1 bunch of parsley 
  * 1 lemon juice 
  * salt 
  * minced meatballs (optional) 



**Realization steps**

  1. clean the pieces of chicken, put them in a pot, fry on low heat (without browning) with grated onion, oil, butter, pepper, cinnamon and salt. 
  2. add the amount of water needed, boil the chickpeas, and the minced meatballs, cover and let cook. 
  3. pour the vermicelli into the stock, let it cook, stirring occasionally. 
  4. Take a ladle of sauce, pour it into a deep plate, Spread the egg yolk in this sauce, add the parsley, and the lemon juice, mix well. 
  5. slowly pour everything into the pot while stirring. 
  6. Remove immediately from the fire and serve. 



![chorba beida 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/chorba-beida-1.jpg)

Enjoy your meal. 

and for more chorbas, click on the links at the bottom:   
  
<table>  
<tr>  
<td>

![Algerian chorba with bird tongues 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-4-150x150.jpg) [ **chorba bird languages** ](<https://www.amourdecuisine.fr/article-chorba-algerienne-aux-langues-doiseaux.html>) 
</td>  
<td>

**![chorba-frik-2-006.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-2-006.CR2_1-150x150.jpg) [ chorba frik - Jari bel frik - Crisp green wheat soup ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) ** 
</td>  
<td>

**![hrira with vegetables](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/hrira-aux-legumes-150x100.jpg) ** [ **HARIRA, FRESH, DRIED VEGETABLE SOUP WITH CHICKEN** ](<https://www.amourdecuisine.fr/article-hrira-aux-legumes.html>) 
</td> </tr>  
<tr>  
<td>

**![chorba vermicelli 007](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-vermicelle-007_thumb.jpg) ** [ **Chorba vermicelli** ](<https://www.amourdecuisine.fr/article-chorba-vermicelle-soupe-a-la-vermicelle-81037155.html>) 
</td>  
<td>

**![shrimp soup 014a](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-de-crevettes-014a_thumb.jpg) ** [ **Shrimp soup** ](<https://www.amourdecuisine.fr/article-soupe-de-crevette-85354072.html>) 
</td>  
<td>

[ **![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/image_thumb_121.png) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/image_261.png>) [ **Cream of peas** ](<https://www.amourdecuisine.fr/article-41284308.html>) 
</td> </tr>  
<tr>  
<td>

**![lentil soup 036](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-de-lentilles-036_thumb.jpg) ** [ **LENTIL SOUP** ](<https://www.amourdecuisine.fr/article-soupe-de-lentilles-58156074.html>) 
</td>  
<td>

**![velvet of feves 015 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-de-feves-015-1_thumb.jpg) ** [ **VEGETABLE OF FÈVES** ](<https://www.amourdecuisine.fr/article-veloute-de-feves-64719071.html>) 
</td>  
<td>

**![grain velvet 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-de-grain-1_thumb.jpg) **

##  [ VEGETABLE DRY VEGETABLES ](<https://www.amourdecuisine.fr/article-veloute-de-legumes-secs-64240957.html>)


</td> </tr>  
<tr>  
<td>

**![Veloute de Courgettes-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/Veloute-de-Courgettes-1.jpg) ** [ **CREAM OF ZUCCHINI** ](<https://www.amourdecuisine.fr/article-veloute-de-courgettes-61481531.html>) 
</td>  
<td>

**![pumpkin soup 018](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-de-potiron-018_thumb.jpg) ** [ **PUMPKIN SOUP** ](<https://www.amourdecuisine.fr/article-soupe-de-potiron-58516712.html>) 
</td>  
<td>

**![sweet potato velvet 020](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/veloute-patate-douce-020_thumb.jpg) ** [ and = "_blank"> **Veloute with sweet potato and squash** ](<https://www.amourdecuisine.fr/article-veloute-automnal-a-la-patate-douce-et-la-courge-88167689.html>) 
</td> </tr>  
<tr>  
<td>

**![pumpkin soup](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-de-potiron_thumb.jpg) ** [ **Pumpkin soup** ](<https://www.amourdecuisine.fr/article-soupe-de-potiron-surprise-86694827.html>) 
</td>  
<td>

**![sweet potato soup and pears 005](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-patate-douce-et-poires-005_thumb.jpg) ** [ **velvety sweet potato and pear** ](<https://www.amourdecuisine.fr/article-veloute-de-patates-douces-et-de-poires-100286555.html>) 
</td>  
<td>

**![spinach of spinach 011](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-d-epinards-011_thumb.jpg) ** [ **Cream of spinach** ](<https://www.amourdecuisine.fr/article-creme-d-epinard-97641704.html>) 
</td> </tr>  
<tr>  
<td>

**![Bissara](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bissara_thumb.jpg) ** [ **Bissara, broken peas velvety** ](<https://www.amourdecuisine.fr/article-soupe-au-pois-casses-bissara-97334307.html>) 
</td>  
<td>

**![cauliflower cream](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/creme-de-chou-fleur_thumb.jpg) ** [ **cream with cauliflower** ](<https://www.amourdecuisine.fr/article-creme-au-chou-fleur-100720359.html>) 
</td>  
<td>

**![carrot velout 140](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-de-carotte-140_thumb.jpg) ** [ **Veloute of carrots with coconut milk** ](<https://www.amourdecuisine.fr/article-veloute-de-carotte-au-lait-de-coco-maison-81947971.html>) 
</td> </tr> </table>
