---
title: Barbie Princess Birthday Cake
date: '2012-04-13'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-princesse-barbie-001.jpg
---
##  Barbie Princess Birthday Cake 

Hello everybody, 

Here's the barbie princess [ birthday cake ](<https://www.amourdecuisine.fr/categorie-11700499.html>) of my little Ines doll. So she asked me for a princess this time, and I went to do haute couture this time .... what do you say, from my first princess dress to the sugar pie ??? 

I lingered to post you the recipe, but here I will pass it to you and try my best for the detailed, hihihiih 

Princess Barbie recipe, birthday cake is very very easy, the only difficulty for this cake is cooking, because I made a big cake that I cooked in a mold. **pyrex** So it took a long time for regular cooking.   


**Barbie Princess Birthday Cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-princesse-barbie-001.jpg)

**Ingredients**

  * 2 jars of natural yoghurt 
  * 6 eggs 
  * 3 jars of sugar (when I say jar, I use the jar of yogurt as a measure) 
  * 1 C. a vanilla coffee 
  * 5 pots of flour 
  * 2 tbsp. tablespoon of baking powder 
  * 1 pot of oil 

the butter cream: 
  * 4 egg yolks 
  * 140 grams of sugar 
  * 30 ml of water 
  * 200 grams of butter 
  * vanilla 

decoration: 
  * sugarpaste 
  * pink and purple dye 
  * silver beads 
  * butterflies in sugar 



**Realization steps**

  1. preheat the oven to 170 degrees C 
  2. Separate the egg whites from the yolks 
  3. whip the whites and leave them blank 
  4. whip the egg yolks, stir in the sugar slowly 
  5. add the yogurt, then the oil, and the vanilla 
  6. sift the flour with the yeast 
  7. stir in flour mixture with egg yolk mixture 
  8. introduce the egg white to this mixture very slowly 
  9. pour the mixture into a floured and oiled pyrex mold 
  10. put in the preheated oven, cover the pyrex mold with the lid of a pot if possible 
  11. cook the first 30 minutes at 170 degrees 
  12. cook for another 30 minutes at 160 degrees 
  13. and cook for the last 30 minutes at 150 degrees 
  14. check the cooking with the blade of a knife, if you think that the cake needs more minutes, do not hesitate to let it cook again (me with my oven, I remove the lid after 1 hour of cooking , and the total time of cooking in my oven was 1h and 50 minutes) 
  15. remove after cooking, turn out and let the cake cool   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-barbie-gateau-yaourt-creme-au-beurre_thumb1.jpg)

preparation of the butter cream: 
  1. in a saucepan place the wet sugar with the water and cook until 117 degrees (if you do not have a sugar thermometer, let the syrup cook until you start to see the big bubbles 
  2. beat the egg yolks, and gradually add the still hot syrup, in net on 
  3. whisk at medium speed to avoid hot syrup spraying 
  4. continue to whisk at high speed until the mixture has cooled down 
  5. add the butter in pieces slowly to this mixture, and the mixer should be at medium speed.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-barbie-020_thumb1.jpg)

cake mounting: 
  1. cut the cake into three layers 
  2. decorate each layer with butter cream 
  3. then cover all the cake with the butter cream 
  4. color the butter cream according to your taste and dress your princess 
  5. for my part I used the circle cutters, and cut each ring in half to have the lace bottom 
  6. I covered the cake with a layer of sugar paste 
  7. I still cut circles, knowing that we stick the pieces of the sugar paste with a little water 
  8. using a pliers, trace the join and I always finish the deco with the pieces in a circle 



this and the other barbie I made this Friday, for my girlfriend's daughter: 

bonne journee 
