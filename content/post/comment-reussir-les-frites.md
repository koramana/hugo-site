---
title: How to make french fries?
date: '2016-12-29'
categories:
- cuisine diverse
- Cuisine by country
- tips and tricks

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-3.jpg
---
![lham mhamer, roast meat tagine 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-3.jpg)

##  How to make fries and fries? 

Hello everybody, 

My story with the frying started when I arrived here in England, with a mid-range electric stove, not to mention low-end (and not hurt my husband who bought this electric stove to please me).   


But in reality, and for me, who is used to cooking on gas in my native country, I was very annoyed with this cook. The disasters with were endless. It was just necessary that I absent myself for a short time so that my dish burns, or that my fries stick to the bottom ... The worst was with the fries, the oil did not heat well, and the fries which touched the bottom of the pan burned easily without mentioning fries that were too oiled for my taste. It was the ordeal. 

So now that you are equipped with the good fryer just like me, we go to work to have perfectly successful fries!    


**How to make french fries?**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-roti-et-sa-croute-au-fromage-2.jpg)

**Ingredients**

  * 1 kg of potatoes for french fries 
  * 3.5 l of frying oil (the capacity of my fryer) 
  * 1 tablespoon of salt 



**Realization steps**

  1. wash, peel and cut potatoes 
  2. place as you go in the water to remove the excess starch. 
  3. place the fries on a clean cloth and dry them well with water. 
  4. Heat the oil until it reaches 160 ° C.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/friteuse.jpg)
  5. place the fries in the fryer basket and dip them in the cooking oil to whiten them for almost 5 minutes. 
  6. Drain on paper towels, let cool a little, continue with the rest of the potato. 
  7. replace the blanched potato in the basket and dip back into the frying oil heated this time to 180 ° C to finish the cooking and give a nice color   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/les-frites.jpg)
  8. Drain again, salt and serve immediately. 



![roasted chicken and its cheese crust 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-roti-et-sa-croute-au-fromage-2.jpg)
