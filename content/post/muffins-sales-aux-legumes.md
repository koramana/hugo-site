---
title: salted muffins with vegetables
date: '2017-02-11'
categories:
- amuse bouche, tapas, mise en bouche
- idee, recette de fete, aperitif apero dinatoire
- recette de ramadan
- recettes salées
tags:
- Economy Cuisine
- Salty cake
- Aperitif
- Accessible Kitchen
- Ramadan
- Algeria
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/muffins-sal%C3%A9s-aux-l%C3%A9gumes.jpg
---
![salted muffins with vegetables](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/muffins-sal%C3%A9s-aux-l%C3%A9gumes.jpg)

##  salted muffins with vegetables 

Hello everybody, 

These salty muffins with vegetables are so good that you will not stop at one piece! I often prepare these salty muffins when we have an outing, when we hit the road, or just to accompany a good fresh salad and soup or [ chorba ](<https://www.amourdecuisine.fr/article-chorba-algeroise.html>) . 

In addition to being super good, these salty muffins with vegetables are ready quickly in the blink of an eye ... And you can put in everything you love, it's just a delight. You can also see the recipe [ salted muffins with chicken breast ](<https://www.amourdecuisine.fr/article-muffins-sales-au-blanc-de-poulet.html>) . 

{{< youtube Y4tbCwOPub4 >}} 

**salted muffins with vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/muffins-sal%C3%A9s-aux-l%C3%A9gumes-1.jpg)

portions:  12  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 250 gr Flour 
  * 1 and ½ tsp. coffee baking powder 
  * 1 C. baking soda 
  * soda 
  * Salt, paprika, mixed herbs 
  * 2 eggs 
  * 150 ml of milk 
  * 75 ml of table oil 
  * 1 handful of mozzarella 
  * 1 handful of cheddar 
  * 1 cup steamed salted vegetables: carrots, peas, corn, green beans 



**Realization steps**

  1. Preheat the oven to 170 ° C 
  2. Line a muffin tin with muffin trays. 
  3. sift flour, baking powder and baking soda, add salt, paprika and mixed herbs. 
  4. in a large bowl whisk the eggs and oil until the mixture becomes foamy. 
  5. introduce the milk and continue to whisk. 
  6. introduce the flour mixture without much whipping 
  7. add the vegetables and the two cheeses, mix gently so as not to liquefy your appliance. 
  8. fill the boxes at ⅔ with a spoon. 
  9. Bake for 15 minutes at 170 ° C or until a toothpick comes out clean by inserting it into the muffin. 
  10. remove the muffin pan from the oven and let it cool on a rack for 5 to 6 minutes before serving. 



![salted muffins with vegetables 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/muffins-sal%C3%A9s-aux-l%C3%A9gumes-2.jpg)
