---
title: fluffy Berlin balls
date: '2017-03-07'
categories:
- boulange
- crepes, beignets, donuts, gauffres sucrees
tags:
- Boulange
- To taste
- fritters
- Carnival
- Pastry
- Cakes
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/boules-de-berlin-moelleux-et-fondant-1.jpg
---
![fluffy and melting berlin balls 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/boules-de-berlin-moelleux-et-fondant-1.jpg)

##  fluffy Berlin balls 

Hello everybody, 

Surely you already taste or try the recipe of Berlin balls donuts fluffy! No, you do not know, I tell you you missed a lot, and I urge you to quickly make this recipe of donuts balls of Berlin fluffy and fondant wish. 

I tried this recipe of Berlin balls, or German donuts a long time ago [ right here ](<https://www.amourdecuisine.fr/article-27186708.html>) , and today I share with you the recipe of Mina Minnano, it's always good to change and try new recipes. 

Here is a video, and I hope you enjoy it. 

**fluffy Berlin balls**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/boules-de-berlin-moelleux-et-fondant-2.jpg)

**Ingredients** for leaven: 

  * 2 tbsp. baking yeast 
  * 2 tbsp. tablespoons sugar 
  * warm milk 
  * 70 gr of butter 

for the pasta: 
  * 400 gr of flour 
  * ½ c. salt 
  * 1 C. coffee baking powder 
  * 2 eggs 
  * 100 to 150 ml warm milk 

For the pastry cream: 
  * 200 ml of milk 
  * 2 egg yolks 
  * 50 gr of sugar 
  * 20 gr of cornflour 
  * vanilla 



**Realization steps** prepare the pastry cream: 

  1. Boil the milk with a little sugar. 
  2. In a bowl, beat eggs, sugar and vanilla to blanch. 
  3. Add the maizena, mix 
  4. pour some milk on top while stirring with the whisk vigorously. 
  5. put everything back in the saucepan, and cook on the fire while whisking, until splicing the thickened cream, remove from heat and let cool in salad bowl covered with food film. 
  6. Prepare the dough: 
  7. Mix all the ingredients of the dough with the leaven (leave the butter at the end) 
  8. work the dough in the robot, MAP (bread machine) or by hand: it must have the consistency of a soft bread dough that does not stick to the fingers 
  9. introduce the pomade butter in a small amount 
  10. Let the dough rest about 1 hour, or until doubled in size. 
  11. Spread the dough on your worktop to a thickness of about 1.5 cm and cut out discs about 6 to 8 cm in diameter using a punch or a glass. 
  12. Leave to rest again. Fry on medium heat (be careful if the fire is too soft they will be raw inside and if it is too strong they will burn, I heat the oil of the pan over high heat and then I lower the temperature, the ideal is to fry them (with the temperature recommended for donuts on some electric fryers) and start with donuts not too big until you master the cooking 
  13. stuff the donuts with the pastry cream, and sprinkle with icing sugar from above. 



![fluffy and melting berlin balls](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/boules-de-berlin-moelleux-et-fondant.jpg)
