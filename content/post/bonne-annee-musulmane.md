---
title: Happy New Year Muslim
date: '2009-12-18'
categories:
- Algerian cakes with honey
- fried Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/12/chekhchoukha-1.jpg
---
On the occasion of the Muslim New Year, I wish to extend my best wishes to all Muslims all over the world, prosperity and happiness. 

![](https://www.amourdecuisine.fr//import/http://under20.maktoob.com/CardPath/)

**_Origin of the Hegira date:_ **

> The well-guided Caliph 'Umar ibn Al-Khattab (may Allah be pleased with him) was the first to start dating the lunar months from the emigration of the Prophet (pbuh) from Mecca to Medina, the Hegira constituting - according to this process - the first day of the Hegira calendar. It was in 622 AD AD 

> It was customary to call the Hegira date by the Arabic letter ![](https://www.amourdecuisine.fr//import/http://prayer.al-islam.com/images/) as a sign of abbreviation or by (A.H.) in the Latin languages, abbreviation of Anno Hegirae meaning (after the Hegira). The first Muharram of year 1 of the Hegira corresponded to July 16 of the year 622 AD. AD 

> The Hebrew (Islamic) year includes 12 lunar months: Muharram - Safar - Rabî` Al-'Awwal - Rabî` Ath-Thânî - Jumâda Al-'Awwal - Jumâda Ath-Thânî - Rajab - Cha`bân - Ramadan - Chawwâl - Dhul-Qa`da - Dhul-hijja) 

> Among the Islamic occasions of the Hegira year, we find: the Hebrew New Year (first Muharram) - the commemoration of the night journey and the ascent of the Prophet (the 27th of Rajab) - the beginning of the fasting of Ramadan and the night of al-qadr coinciding with one of the last ten days of this month - the feast of breaking the fast (first day of chawwâl) - the feast of sacrifice (10 dhul-hijja) - the season of the pilgrimage (from eight to thirteen dhul-hijja). 

and on this beautiful occasion, I prepared for my husband and we of course a very good chekhchouka: 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/chekhchoukha-1.jpg)

the recipe is [ right here ](<https://www.amourdecuisine.fr/article-26090486.html>)

a true delight 

Encore une fois bonne Annee 
