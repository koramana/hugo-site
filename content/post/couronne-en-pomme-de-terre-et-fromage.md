---
title: potato and cheese wreath
date: '2011-08-29'
categories:
- the tests of my readers

---
if you like this recipe vote 43 here is hello everyone, here is another participation in the contest "Ramadan entrance", it is one of my reader that I thank very much, thank you "Elihalger" so do not forget you, today is the last time, so at midnight, I will close the access to the comments page. so if you want to participate, you still have time. and for the girls who send me their entries, and I have not yet validated on the recap article, it's just for lack of time, but I'm going to do it for that. 

##  Overview of tests 

####  please vote 

**User Rating:** 4.65  (  1  ratings)  0 

if you like this recipe vote 43 a [ right here ](<https://www.amourdecuisine.fr/article-concours-les-entrees-ramadanesques-le-vote-83357324.html>)

Hello everybody, 

here is another participation in the contest "Ramadan entrance", it is one of my reader that I thank very much, thank you "Elihalger" 

So do not forget you're tired, today is the last time, so at midnight, I'll close the access to the comments page. 

so if you want to participate, you still have time. and for the girls who send me their entries, and I have not yet validated on the recap article, it's just for lack of time, but I will do it as soon as possible, insha'Allah. 

For the pasta: 

  * 250g of flour , 
  * 150 gr butter very very cold cuts in small cubes 
  * 1/2 teaspoon of salt 
  * \+ or - 150 ml of ice water 



for the farce 

  * 1/2 onion 
  * a potato 
  * 1/2 carrot 
  * 1/4 pepper (for me can orange and yellow) 
  * salt and pepper 
  * 4 olives 
  * 3 portions of laughing cow 
  * grated cheese 
  * egg 



preperation: 

  1. Mix flour and salt in the food processor for a second or two, add the cold butter and pulse until you have a good dough (you can use two knives in 'X'). 
  2. Avoid using your hands so far as this will heat up the mixture and make the dough harder and less flaky. Add water 3 tablespoons at the beginning and pulverize without too much work then add by spoon until the paste begins to form in a pile and to take off from the edges of the bowl. 
  3. Cover the dough with Food Film and refrigerate for at least 1 hour. 
  4. Meanwhile, prepare the stuffing by cutting the first 6 ingredients into small cubes and making them cuires (half box), 
  5. add the olives and put off the heat. 
  6. Once cooled, add the cheese and the egg. 



For assembly: 

  1. Preheat the oven to 200 degrees. 
  2. Flour the work table and spread the dough in disc and cut into 8 triangles. 
  3. Place the wide part of the triangles on the board and the end of the triangles out of the board, 
  4. put the stuffing, fold the dough and decorate according to taste. 
  5. Bake 40 to 45 minutes. 



AND GOOD TASTING !! 

thank you my dear for the recipe 

thanks to you my readers, for your comments and visits 

bonne journee 
