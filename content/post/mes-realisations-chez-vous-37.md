---
title: my achievements at home 37
date: '2013-10-26'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- recipes of feculents
- salty recipes
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg)

Hello everybody, 

I am very late to publish this article, because I had a lot of messages about your tests, and it makes me very happy. 

But, because I was not a good student, and I did not do the job at the right time, the recipes accumulated, and I apologize in advance if there is going to be a gap, and if you do not find your recipe, do not hesitate to contact me on my email: [ your "gmail" gmail "point" com ](<mailto:vosessais@gmail.com>)

**just a clarification** : 

**this email,** [ **yours @robasque gmail dot com** ](<mailto:vosessais@gmail.com>) **is not a contact email, if you have questions to ask me about a recipe, make the comment, because this email, I only consult when I have time to write the article (so do not me do not blaze if you have a question that I did not answer): my achievements at home, and sometimes I find urgent questions that are more than 1 month old ....**

**sorry, I have the comments to answer your questions, and I have this email for the achievements, and I have another email to contact the author, but it's mostly for commercials, hihihihih**

**I can not receive everything in the same email, it will be overflowing.**

**so, and I'll say it again, if you have an urgent question about a recipe, make it a comment, it will allow me to respond faster, as it allows other visitors to enjoy the answer, and not to rest the question again and again.**

**thank you.**

![cake-magique.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/gateau-magique.jpg)

[ Magic cake ](<https://www.amourdecuisine.fr/article-gateau-magique-au-chocolat-et-creme-caramel-107207656.html>) at Shoukran 

![picture-89-.jpeg](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/image-89-.jpeg)

![05-3511.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/05-3511.jpg)

[ chocolate slices ](<https://www.amourdecuisine.fr/article-35955000.html>) at Sonia's 

![080820133868.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/080820133868.jpg)

[ Nakkash ](<https://www.amourdecuisine.fr/article-kaak-nakache-gateaux-algeriens-103646923.html>) , [ Makrout ](<https://www.amourdecuisine.fr/article-makrout-el-koucha-en-video-gateau-de-semoule-et-dattes-119361264.html>) , [ ghribiya ](<https://www.amourdecuisine.fr/article-ghribiya-algeroise-ghribia-a-l-huile-gateau-algerois-108952737.html>) , [ maamoul ](<https://www.amourdecuisine.fr/article-maamoul-112293141.html>) , [ tcharek ](<https://www.amourdecuisine.fr/article-tcharak-msakar-103050711.html>) at Maryouma 

![photo.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/photo.jpg)

[ lentil soup ](<https://www.amourdecuisine.fr/article-soupe-de-lentilles-58156074.html>) at Oxana K 

![maamoul figs1](http://sweetcookfreetime.files.wordpress.com/2013/08/maamoul-figues1.jpg?w=812)

![05 0032](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/05-0032.jpg)

[ bread stuffed with meat ](<https://www.amourdecuisine.fr/article-recette-de-pains-farcis-117017598.html>) at Hayet M 

![003.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/003.jpg)

[ Mint sirup ](<https://www.amourdecuisine.fr/article-sirop-a-la-menthe-boisson-rafraichissante-118439235.html>) at Ginette L 

![21072013-017-.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/21072013-017-.jpg)

[ Khobz dar without fluffing ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) chez Mina B 
