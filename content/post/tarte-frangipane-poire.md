---
title: pear frangipane tart
date: '2011-01-10'
categories:
- gateaux, et cakes
- recettes sucrees

---
these days I am with the recipes of midnight, frankly I have more time between taking care of my children and the house, I am leading to abandon the blog, hihihihihihi, and as my husband always likes a delight to accompany his coffee, so I had to do an easy thing. at first, I wanted to make the dried fruit pie of djouza, that I never drip, but I do not know why, the frangipane pie has imposed itself, maybe it is because of the box of pear which had been wisely waiting at the back of the closet for quite some time now ???? & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

these days I am with the recipes of midnight, frankly I have more time between taking care of my children and the house, I am leading to abandon the blog, hihihihihihi, and as my husband always likes a delight to accompany his coffee, so I had to do an easy thing. 

for the base: 

the shortbread   
\- 200 grams of flour   
\- 100 gr of butter   
\- 75 gr of sugar   
\- 1 egg 

For frangipane cream   
\- 100 gr of butter   
\- 90 gr of sugar   
\- 1 tablespoon of cornflour   
\- 2 eggs   
\- 125 g ground almonds 

1 can of pear in its juice 

for the topping (mine) 

\- 2 tablespoons apricot jam 

\- 1 cup of acacia honey (I will have to use my honey anyway, hihihihi) 

\- 1 teaspoon of orange blossom water 

Preparation of the dough   
In a bowl, cut the butter into small pieces. Add the flour, sugar and a pinch of salt. Crush with your fingertips to get a sanding. Add the egg and gather quickly and form a ball. Cover with a film and let cool for 20 min. 

Preparation of frangipane   
Mix the softened butter with the sugar. Add the eggs then the almonds and the flour. Stir well. 

Lower the sweet dough and prick with a fork ( _I spread it directly on the mold with removable bottom_ ). Garnish the bottom of the pie with the frangipane cream. Place the sliced ​​pears and cook in a preheated oven. 170 ° approximately 30 to 40 min. When the top is golden. Remove and let cool. 

prepare the topping by passing the ingredients over low heat just so that the mixture is a little liquid and you can easily brush the pie with. 

en tout cas c’est un réel délice, qu’on a bien apprécié 
