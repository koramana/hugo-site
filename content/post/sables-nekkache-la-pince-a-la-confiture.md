---
title: shortbreads nekkache - the clip has jam
date: '2015-07-13'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- sweet recipes
tags:
- Algeria
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9s-a-la-pince-7.jpg
---
[ ![shortbread 7](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9s-a-la-pince-7.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9s-a-la-pince-6.jpg>)

##  shortbreads nekkache - the clip has jam 

Hello everybody, 

Here is a very good idea to make shortbread of a different shape and which is very close to the shape of shortbread in the mold, as the [ sandblasted ettaje (the crown) ](<https://www.amourdecuisine.fr/article-sables-ettaj-sables-la-couronne-au-chocolat.html>) where the [ sesame seed shortbread ](<https://www.amourdecuisine.fr/article-sables-aux-grains-de-sesames.html>) . So for people who can not get these mussels, this model is just right for you. 

A realization of a gifted woman in the kitchen, **Sadia Tizi** who shared wonderful recipes during the month of Ramadan, achievements to fall ... And here is one of these recipes, a nakkache shortbread cake, the tongs and jam. I hope that the idea of ​​this cake will please you, so have your ingredients ... 

**shortbreads nekkache - the clip has jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9s-a-la-pince.jpg)

portions:  30  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 500 gr of margarine 
  * 2 complete eggs and 3 yolks 
  * a glass of cornflour 
  * a pinch of salt 
  * vanilla 
  * the lemon peel 
  * flour 
  * 3 spoon of jelly (in the dough) 
  * Decoration: 
  * Jelly 
  * jam + a little lemon juice and a little vanilla 



**Realization steps**

  1. Mix the margarine with the sugar until you have a nice cream 
  2. add jelly and eggs one by one 
  3. stir in salt, maizena, vanilla and lemon peel 
  4. and finally, produce the baking powder and flour. 
  5. to make the cake, you can see this picture, and to stick the pudding to the circle of the shortbread, use egg white:   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/les-gateaux.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/les-gateaux.jpg>)
  6. bake shortbreads in a preheated oven at 180 degrees C to have golden cakes. 
  7. after cooling, brush the shortbreads with a little jelly using a brush. 
  8. fill the small wells with the mixture: jam + lemon juice + vanilla, for an incomparable taste. 



[ ![shortbread with pliers 6](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9s-a-la-pince-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9s-a-la-pince-6.jpg>)
