---
title: Strawberry and lemon sorbet
date: '2015-05-23'
categories:
- crème glacée, et sorbet
tags:
- Ramadan 2015
- ice cream
- Summer
- Easy recipe
- Algeria
- Without ice cream maker
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sorbet-de-fraises-et-citron-1.jpg
---
[ ![strawberry and lemon sorbet 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sorbet-de-fraises-et-citron-1.jpg) ](<https://www.amourdecuisine.fr/article-sorbet-de-fraises-et-citron.html/sorbet-de-fraises-et-citron-1>)

##  **Strawberry and lemon sorbet**

Hello everybody, 

Although the cool days are back, we still try to prepare for the summer season, and the holy month of **Ramadan 2015.** A refreshing dessert is always welcome, whether it is during sunny afternoons and hot, or the evenings of Ramadan, which may well be short here at home in England, and not to refresh that with cold water, a sorbet like that is much appreciated. 

Besides, I can not tell you how much I realized last year during Ramadan, my husband and I were eating all the time, a shame that I could not take pictures, because the sorbet was always ready at night, and it was impossible to make a photo. I hope that I will be able to organize this year, and be able to take pictures, as long as it is day. 

For this sherbet from my friend of the page: A moment of pleasure, you can use a sorbetiere, as you can do without, as is the case for all ice creams. Just do not forget that some things in the fridge that must be stirred from time to time. 

[ ![strawberry and lemon sorbet 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sorbet-de-fraises-et-citron-2.jpg) ](<https://www.amourdecuisine.fr/article-sorbet-de-fraises-et-citron.html/sorbet-de-fraises-et-citron-2>)

Today it's a strawberry and lemon sorbet that my friend shares with us, and for the lemon, in her recipe, she uses [ concentrated lemon juice ](<https://www.amourdecuisine.fr/article-concentre-de-jus-de-citron-fait-maison.html> "Concentrated homemade lemon juice") , then jump on the link, to see this delicious recipe. 

**Strawberry and lemon sorbet**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sorbet-de-fraises-et-citron.jpg)

portions:  10  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * ½ L mixed strawberries 
  * 150 ml freshly squeezed lemon juice 
  * 200 ml of syrup (sugar + water in equal quantity, put on the fire until it becomes a little honeyed) 



**Realization steps**

  1. the recipe is easy to do, just mix these 3 ingredients, put them in the freezer for a moment. 
  2. or use the ice cream maker to spin the mixture to get your sherbet quickly, or remove the mixture from the freezer every 30 minutes, and whip it a little. 
  3. you can put the ready sorbet in a cake mold near the end, and at the time of the presentation, enjoy it with whipped cream. 



[ ![strawberry and lemon sorbet 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sorbet-de-fraises-et-citron-3.jpg) ](<https://www.amourdecuisine.fr/article-sorbet-de-fraises-et-citron.html/sorbet-de-fraises-et-citron-3>)
