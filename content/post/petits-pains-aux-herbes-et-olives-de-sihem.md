---
title: herbs and olives rolls from Sihem
date: '2010-10-07'
categories:
- cuisine algerienne
- Mina el forn
- pain, pain traditionnel, galette

---
a delicious bread, which I wanted to try for a while, and as accustomed to my home, today is a chorba for dinner, so accompany it with this bread, would only appreciate. the recipe I found at Sihem, I made very small changes, but otherwise I keep the delicacy of his recipe: 500 gr of flour 6 ca soup of olive oil a coffee ca of fine salt 2 Sugar soup (1 tablespoon for me) a soup of yeast a ca plain yeast soup & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

a delicious bread, which I wanted to try for a while, and as accustomed to my home, today is a [ chorba ](<https://www.amourdecuisine.fr/article-25555887.html>) for dinner, so accompany it with this bread, would only appreciate well. 

  * 500 gr of flour 
  * 6 tablespoons of olive oil 
  * a coffee of fine salt 
  * 2 tablespoons of sugar (1 tablespoon for me) 
  * a soup of baking powder 
  * a very plain soup of baker's yeast 
  * a soup of dried thyme (I did not have any, put oregano) 
  * a soup of herbes de provence 
  * 2 garlic cloves, finely chopped (not grated) 
  * 2 tablespoons of milk powder that I added. 
  * 2 tablespoons grated Parmesan cheese 
  * a coffee of paprika 
  * lukewarm water 
  * some black and green olives cut into small pieces 
  * and some olives to garnish 



mix flour, salt, baking powder, yeast, sugar, olive oil and squeeze well between hands   
add the water little by little and collect the paste which must be slightly sticky to the fingers   
cover and let double the volume   
degas the dough and knead well by hand for 10 minutes adding the garlic, the olives cuts, the herbs   
divide the dough into 9 small balls of milk and sprinkle with herbs, parsley and paprika   
decorate with an olive in the center   
let doubling once again the volume 

put in the oven at 200 degrees for half an hour 

un delicieux pain, au gout bien different, et surtout bien moelleux et doux. 
