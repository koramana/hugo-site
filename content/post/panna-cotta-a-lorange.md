---
title: panna cotta with orange
date: '2016-06-20'
categories:
- panna cotta, flan, and yoghurt
tags:
- Ramadan 2016
- Ramadan
- verrines
- Agar Agar
- Custard

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/panna-cotta-a-lorange.jpg
---
![panna cotta with orange](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/panna-cotta-a-lorange.jpg)

##  panna cotta with orange 

Hello everybody, 

Here is a recipe that I realized just before losing my old computer and all its contents. These photos are the only two that I could get, one that I shared on Facebook and the other that I took with my mobile phone just by chance. 

I admit that I was disappointed when I lost all the data and photos of my recipes with my computer. In addition I have the habit that every time I transfer my photos from my device to my computer, I erased the whole of the map mimoire ... A beautiful stupidity that I advise you not to do. Because of that I lost more than 30 recipes that I was waiting to publish with you on my blog, but here it is! 

In any case this orange panna cotta is just too good, I can do it again and make other new photos, but when we break the head to do the staging to bring out the best of a recipe, I confess that it is not easy to redo all this after ... In any case, I hope that one day I can redo them, hihihihi 

**panna cotta with orange**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/panna-cotta-a-lorange.jpg)

portions:  4  Prep time:  10 mins  cooking:  5 mins  total:  15 mins 

**Ingredients**

  * 600 ml whipping cream 
  * 320 ml of milk 
  * 100 g of sugar 
  * 2g of agar-agar (or 2 sheets of gelatin) 
  * 1 teaspoon powdered vanilla 
  * the zest of an untreated half orange 

for orange jelly: 
  * 150 ml of freshly squeezed orange juice 
  * ½ teaspoon agar agar 
  * sugar according to taste 
  * decoration: 
  * pieces of orange cut raw. 



**Realization steps**

  1. In a saucepan, mix the milk cream, sugar and vanilla, orange peel 
  2. Heat 2 min to infuse, if you like orange zest go to the next step, otherwise pass the solution to the Chinese. 
  3. add the agar-agar mixed with a little powdered sugar. Boil 1 min. 
  4. Pour into cups and let cool to room temperature then put in the fridge. 

prepare the orange jelly: 
  1. In a saucepan, pour the orange juice and a little sugar to have a good taste not too sweet. Heat on low heat. 
  2. Add the agar agar, let it boil for 1 minute and remove from the heat 
  3. spread on the surface of the panna cotta cups. 
  4. add pieces of orange and put back in the fridge. 



![panna cotta with orange 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/panna-cotta-a-lorange-2.jpg)
