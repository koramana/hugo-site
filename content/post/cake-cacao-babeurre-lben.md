---
title: cocoa cake Buttermilk, lben
date: '2016-09-27'
categories:
- cakes and cakes
tags:
- To taste
- Cakes
- Algerian cakes
- Easy cooking
- Pastry
- Inratable cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/12/cake-cacao-babeurre-004_thumb1.jpg
---
##  cocoa cake Buttermilk, lben 

Hello everybody, 

cocoa cake Buttermilk, lben: Here is a wonderful recipe for a really tasty cake, a cocoa and buttermilk cake (lben, lair ribot) The color and the taste are just right with all this cocoa in the recipe, the result a Cocoa buttermilk cake just perfect. 

For my part I love recipes to taste dark chocolate, moreover I tend to eat dark chocolate more than milk chocolate. In addition this recipe contains buttermilk, an ingredient that gives a lot of cake cake, but I advise you a little trick, prepare this cake today, to enjoy it the next day, huuuuuuuuuuuuuuuuum it really melts in the mouth. 

**cocoa cake Buttermilk, lben**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/cake-cacao-babeurre-004_thumb1.jpg)

**Ingredients**

  * 125 gr of butter at room temperature 
  * 300 gr of sugar 
  * 1 egg 
  * 240 ml of buttermilk 
  * 1 cup of vanilla coffee 
  * 225 gr of flour 
  * 85 g of bitter black cocoa 
  * ¼ cup of baking soda 
  * ½ teaspoon of baking powder 
  * ¼ teaspoon of salt 



**Realization steps**

  1. preheat the oven to 160 degrees C 
  2. butter and flour a cake mold 22cm X 12cm X 7 cm 
  3. in a bowl, whip the butter until it becomes creamy 
  4. add the sugar slowly while continuing to mix 
  5. add the egg, then the buttermilk and vanilla 
  6. keep on beating 
  7. sift the flour, the cocoa into another container 
  8. incorporate salt, baking powder, and baking soda 
  9. add this mixture gently to the butter and buttermilk mixture 
  10. mix with a spatula, just to incorporate the ingredients 
  11. pour this mixture into your cake mold 
  12. cook between 50 to 60 min, or until the tip of a knife comes out dry if you are baking the cake with 
  13. let cool 10 min before unmolding on a rack 
  14. enjoy it if you are patient the next day 



merci tout le monde, pour toutes vos visites, et bonne fin de journée 
