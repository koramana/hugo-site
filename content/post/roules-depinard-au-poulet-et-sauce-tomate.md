---
title: spinach rolls with chicken and tomato sauce
date: '2012-04-18'
categories:
- boissons jus et cocktail sans alcool
- Healthy cuisine

---
hello everyone, a very good dish, that you can introduce as input, this roll of spinach with chicken and tomato sauce. a recipe very easy to make, and super delicious the ingredients: 4 chicken legs a spinach boot salt and black pepper 4 cloves of garlic 1 bay leaf 5 to 6 eggs 4 to 5 fresh tomatoes & nbsp; method of preparation: place chicken legs in a pot with oil, salt, crushed garlic, bay leaf, and black pepper. cover with a little water and cook on low heat. clean the & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

a very good dish, which you can present in [ Entrance ](<https://www.amourdecuisine.fr/categorie-10678929.html>) , this spinach roll with chicken and tomato sauce. 

a recipe very easy to make, and super delicious 

Ingredients: 

  * 4 chicken legs 
  * a spinach bunch 
  * salt and black pepper 
  * 4 cloves of garlic 
  * 1 bay leaf 
  * 5 to 6 eggs 
  * 4 to 5 fresh tomatoes 



method of preparation: 

  1. place the chicken legs in a pot with the oil, salt, crushed garlic, bay leaf, and black pepper. cover with a little water and cook on low heat. 
  2. clean the spinach and cut them, place the spinach as you cut in an oiled skillet with a lid and let time cut another part, continue until the spinach is exhausted, and what is in the stove is already cooked. 
  3. after cooling spinach, mix with beaten eggs, and season with salt, you can add crushed garlic. 
  4. pour your mixture into a mold lined with sulphured paper, and bake for 15 min. 
  5. if the dough swells, prick it gently with a fork. 
  6. Meanwhile, prepare a nice tomato sauce, and fry the tomatoes cut in pieces with a little oil, salt, black pepper and crushed garlic, until you have a juicy sauce. 
  7. at the end of the oven, cover the spinach cake with the tomato sauce. 
  8. add over a few pieces of chicken that you have crumbled. 
  9. roll it gently, as the tomato sauce may overflow. 
  10. garnish your roll according to your taste. and serve hot 



Enjoy your meal 

for others [ chicken recipes ](<https://www.amourdecuisine.fr/categorie-12364208.html>) , I invite you to have the recipe of [ tandoori chicken ](<https://www.amourdecuisine.fr/article-poulet-tandoori-85541703.html>) , [ chicken stuffed pancakes ](<https://www.amourdecuisine.fr/article-crepes-farcies-au-poulet-55641173.html>) …. 
