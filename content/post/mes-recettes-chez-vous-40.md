---
title: my recipes at home 40
date: '2013-12-17'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- Mina el forn
- bread, traditional bread, cake
- pizzas / quiches / pies and sandwiches
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

Thank you to my dear readers who have tried to contact me by email, to send me the pictures of the recipes they have tried following the method on my blog. 

I put you, a system much faster so that you can send me your photos, just go on this page: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll like ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link of your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)

we start with: 

![dsc00036-af60e33befd99d948d95e84d393e913571a9f30d](https://www.amourdecuisine.fr/wp-content/gallery/1/dsc00036-af60e33befd99d948d95e84d393e913571a9f30d.jpg)

[ lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee-114713045.html> "lemon meringue pie") in a reader who forgot to mention her name 

![DSC01725.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/DSC01725.jpg)

![cake-tiramisu-chicory-13e3544372b4e1a7d2f83424afa7fbc76a29bf93](https://www.amourdecuisine.fr/wp-content/gallery/1/gateau-tiramisu-chicore-13e3544372b4e1a7d2f83424afa7fbc76a29bf93.jpg)

[ chicory tiramisu cake ](<https://www.amourdecuisine.fr/article-tiramisu-aux-speculoos-et-chicoree.html> "Tiramisu with speculoos and chicoree") at karimouch 

![Revenue-020-b00e8a11dc5642d54a5402f056af856bee06d62b](https://www.amourdecuisine.fr/wp-content/gallery/1/recettes-020-b00e8a11dc5642d54a5402f056af856bee06d62b.jpg)

[ shortbread with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-d-abricots.html> "shortbread with jam") at fleurdz 

![revenue-025-0d1a492bb31698a59563eed01481313cefca9e61](https://www.amourdecuisine.fr/wp-content/gallery/1/recettes-025-0d1a492bb31698a59563eed01481313cefca9e61.jpg)

[ Tuna quiche ](<https://www.amourdecuisine.fr/article-quiche-au-thon-et-capres.html> "tuna and capres quiche") , at fleurdz 

![20131211_170740-336f079674b0aefe87ff0502b255327ffa253a8d](https://www.amourdecuisine.fr/wp-content/gallery/1/20131211_170740-336f079674b0aefe87ff0502b255327ffa253a8d.jpg)

When in this picture, she comes from someone who forgot to mention the name of the recipe and her name to it. 

So please do not forget, it's easy to put the photo of your realization on this page: 

[ my achievements at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") , 

but I can not guess who you are, nor what is it like? 

bisous 
