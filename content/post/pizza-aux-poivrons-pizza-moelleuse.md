---
title: pepper pizza / fluffy pizza
date: '2014-06-12'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/pizza-au-poivron-griller-010_thumb1.jpg
---
##  pepper pizza / fluffy pizza 

Hello everybody, 

the pizza is always present in the house, my husband and my children like it a lot, but the problem, neither my children nor my husband love the topping, just the tomato sauce for the children, the black olives, anchovies and spicy for my husband . 

this time I made a pizza just for myself, and I had fun putting the ingredients that I love, and especially I love mozzarella walnut, hihihiihih of course if my husband sees that, I do not do not tell, hihihihi, I take advantage of his departure to France to do that.   


**pepper pizza / fluffy pizza**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/pizza-au-poivron-griller-010_thumb1.jpg)

portions:  6  Prep time:  45 mins  cooking:  20 mins  total:  1 hour 5 mins 

**Ingredients** for 3 medium sized pizzas [ the magic dough ](<https://www.amourdecuisine.fr/article-pate-magique.html> "Magic paste") : 

  * 3 glasses of flour (200 ml glass) 
  * 1 tablespoon instant yeast 
  * 1 tablespoon of sugar 
  * 1 teaspoon of salt 
  * 3 tablespoons milk powder 
  * 4 tablespoons of olive oil or even table 
  * 1 cup of baking powder 
  * water as needed 

garnish: 
  * 4 to 5 fresh tomatoes 
  * 1 grilled pepper 
  * 2 cloves garlic 
  * salt, black pepper, thyme 
  * olive oil 
  * mushrooms 
  * green olives 
  * Mozzarella 



**Realization steps**

  1. Mix all these ingredients and add enough lukewarm water until soft, soft and easy to work. 
  2. Knead a few minutes. 
  3. Form the dough into a ball and let it rise under cover, protected from drafts until it has doubled in volume. 
  4. grill the pepper, and clean the 
  5. prepare the tomato sauce 
  6. cut the dough into 3 balls of the same size 
  7. spread the dough 
  8. garnish with the tomato sauce 
  9. then garnish with pepper toasted, mushrooms, olives and finally mozzarilla 
  10. cook in a preheated oven 



je vais être franche, j’ai manger vraiment sans moderation, et c’etait trop trop bon, hihihihi 
