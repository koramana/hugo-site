---
title: Orange and apricot cheesecake
date: '2011-12-28'
categories:
- couscous
- cuisine algerienne
- Healthy cuisine
- recipes of feculents

---
here is a very very delicious cheesecake that I found on the magazine of one of our supermarkets, the recipe was made from lemon and raisins, but I chose the taste of orange and dried apricots, because my husband does not like grapes. so right now I pass you this very very good recipe: for the base: 50gr of sugar 50 gr of butter 50 gr of flour 1 ca coffee of baking powder 1 eggs for cream cheese: 450 gr of unsalted fresh cheese ( diet) 225 gr of sugar 40 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

here is a very very delicious cheesecake that I found on the magazine of one of our supermarkets, 

the recipe was made from lemon and raisins, but I opt for the taste of orange and dried apricots because my husband does not like grapes. 

so right now I pass you this very very good recipe: 

for the base: 

  * 50gr of sugar 
  * 50 gr of butter 
  * 50 gr of flour 
  * 1 cup of baking powder 
  * 1 eggs 



for the cream cheese: 

  * 450 g unsalted fresh cheese (diet) 
  * 225 gr of sugar 
  * 40 gr of flour 
  * 4 eggs 
  * zest of an orange 
  * 3 tablespoons of orange juice 
  * 150 ml of fresh cream 
  * 75 grams of dried apricots 



preparing the dough 

  1. preheat the oven to 170 degrees C 
  2. butter a 20.5 cm removable base pan, or cover with baking paper 
  3. Whisk 50 gr of butter with 50 gr of sugar, until it becomes a creamy mixture 
  4. add the egg and whip 
  5. add the flour and the baking powder, knead to have a homogeneous mixture 
  6. pour the mixture into the mold and arrange it well with the back of the spoon 



preparation of the cream of cheese 

  1. Separate the yolks from the whites of the eggs 
  2. in the bowl of the mixer, knead the fresh cheese, 
  3. add the egg yolks one to one 
  4. then the sugar while kneading 
  5. pour this mixture into a container, stir in the flour 
  6. then add the orange peel and the orange juice 
  7. now add the fresh cream 
  8. whip the egg white into the snow 
  9. gently add it to the cheese mixture, to have an airy texture 
  10. pour the mixture over the already prepared dough 
  11. sprinkle over apricots cut in cubes 
  12. bake in preheated oven for 1 hour, or until cake is holding well if touched with fingertip 
  13. cover the cake during cooking if it burns quickly 
  14. turn off the oven and let the cheese cake in between 2 and 3 hours 
  15. remove the cheese-cake from the oven and allow to cool before unmolding 
  16. garnish with a little icing sugar and orange 



Thanks for your visit and your comments 

e kindly sign up for the newsletter (articles and newsletter alert) to be up to date with any new article on my blog 

bonne journee 
