---
title: pastilla with fish and seafood
date: '2017-05-13'
categories:
- Moroccan cuisine
- rice
tags:
- Algeria
- Morocco
- Healthy cuisine
- inputs
- Ramadan 2016
- accompaniment
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pastilla-aux-fruits-de-mer.jpg
---
[ ![pastilla with fish and seafood](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pastilla-aux-fruits-de-mer.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pastilla-aux-fruits-de-mer.jpg>)

##  pastilla with fish and seafood 

Hello everybody, 

Despite the fact that the pastilla is a recipe that we often eat at home, I was surprised to see that I never posted the fish and seafood pastilla on my blog !! 

For my pastilla with fish and seafood, all my ingredients were frozen, I'm talking about white fish, and seafood, so I removed it at night from the freezer, I prepared my charmoula, I marinated each mixture separately with chermola, placed in a hermetic box, and in the fridge for cooking each ingredient. In the morning, my ingredients were half thawed and super scented. It only remained to cook each ingredient separately and make my delicious pastilla with fish and seafood. 

We had a good time that day. My kids found that the pastilla was even better than the previous times, and that's good for me because I do not like to preheat the pastillas, and we ate the pastilla with fish and seafood at the last crumb. 

**pastilla with fish and seafood**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pastilla-au-poisson-et-fruits-de-mer.jpg)

portions:  6  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients**

  * 250 g shrimp 
  * 250 g of fish fillet (cod, shark, whiting, monkfish ...) 
  * 150 g of rice vermicelli 
  * 25 g black mushroom 
  * Juice of a lemon 
  * 3 tablespoons olive oil 
  * 2 to 3 cloves of garlic 
  * 1 tablespoon paprika 
  * 1 teaspoon of cumin, 
  * salt and black pepper 
  * 1 teaspoon of hot pepper (I did not put away because of children) 
  * 1 small bunch of parsley 
  * 1 small bunch of coriander 
  * 1 package of bricks 
  * 30 g of butter 
  * 1 egg yolk 
  * 1 tbsp soy sauce 
  * 2 tablespoons tomato paste 
  * Some green olives pitted in slices. 
  * some shrimp full to decorate. 



**Realization steps** Prepare the charmoula: 

  1. Slice parsley and coriander and mix in the blinder bowl with garlic, spices, olive oil, lemon juice and soy sauce. 
  2. Season the fish and seafood separately from half of the charmoula sauce and reserve the other half for the rice vermicelli and sauce. 
  3. fry the pieces of fish in a little oil in a pan. 
  4. do the same thing with seafood 
  5. Drain the juices and reserve. 
  6. Dip the rice vermicelli into boiling water for a few minutes, drain and cut with scissors. 
  7. fry the mushrooms in the fish and seafood cooking juices 
  8. add the vermicelli then the rest of the charmoula. 
  9. season to taste, add the tomato paste, stir for a few minutes over medium heat so that the vermicelli and mushrooms are imbued with different flavors of spices and fish. 
  10. Allow the various fillings to cool completely. Mix shrimp, squid, vermicelli and green olives.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/farce-de-pastilla.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/farce-de-pastilla.jpg>)
  11. Make the pastilla by placing the intercalated sheets to cover the surface of the mold (you can see the video)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/faconnage-pastilla.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/faconnage-pastilla.jpg>)
  12. pour the vermicelli mixture in it, decorate with the pieces of fish and close the buttered pastilla leaves with egg yolks to stick the leaves and wrap the stuffing well. 
  13. Prick the pastilla with a fork to allow steam to escape during cooking. 
  14. Brush the pastilla generously with melted butter to get the right side crispy and color cooking. 
  15. Cook in a hot oven at 180 ° C, until the pastilla turns a beautiful golden color. 
  16. Serve the hot pastilla decorated with sautéed shrimp, or according to your taste 



[ ![pastilla with fish and seafood 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pastilla-au-poisson-et-fruits-de-mer-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pastilla-au-poisson-et-fruits-de-mer-1.jpg>)
