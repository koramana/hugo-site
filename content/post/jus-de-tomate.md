---
title: Tomato juice
date: '2017-12-16'
categories:
- juice and cocktail drinks without alcohol
tags:
- Parsley
- Celery
- Vegetables
- Drinks
- Juice
- Easy cooking
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/Jus-de-tomate-1.jpg
---
[ ![tomato juice](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/Jus-de-tomate-1.jpg) ](<https://www.amourdecuisine.fr/article-jus-de-tomate.html/jus-de-tomate-1>)

##  Tomato juice 

Hello everybody, 

The tomato I like a lot, it never misses at home, and personnellemnt I eat everyday since my youngest age, I remember that when I was hungry, I took a piece of bread, I place tomatoes in pieces, a little salt just to raise the taste of the tomato, and a drizzle of olive oil, and here is a good sandwich to stall me, until the time of the meal. 

With age to eat a sandwich like that is not always beneficial, especially if you want to avoid weight gain, and knowing the virtues of the tomato, it's not a fruit-legume which I would not do without. My solution and make a tomato juice super moisturizing and refreshing, rich in fiber, vitamin C, Magnesium, Calcium and so on. 

**Tomato juice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/Jus-de-tomate.CR2_.jpg)

portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 4 ripe tomatoes 
  * 4 large celery stalks 
  * a few sprigs of parsley 
  * the juice of 1 lemon 
  * Salt and freshly ground pepper 



**Realization steps**

  1. after cleaning the vegetables, cut them into pieces. 
  2. Squeeze the lemon. 
  3. In the bowl of the blinder, place the pieces of tomatoes, parsley, celery, lemon juice, add a pinch of salt. 
  4. mix everything to have a homogeneous mixture. 
  5. pass the preparation in a Chinese, keep the juice obtained. 
  6. Serve the juice with a pinch of black pepper, and decorate the glasses according to your taste. 



[ ![tomato juice](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/jus-de-tomates.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-jus-de-tomate.html/jus-de-tomates-cr2>)
