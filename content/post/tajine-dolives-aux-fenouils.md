---
title: olive tajine with fennel
date: '2013-12-17'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/zitoun-004-1024x768-A.jpg
---
[ ![zitoun-004-1024x768 A](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/zitoun-004-1024x768-A.jpg) ](<https://www.amourdecuisine.fr/article-tajine-dolives-aux-fenouils.html/zitoun-004-1024x768-a>)

##  olive tajine with fennel 

Hello soulef, and hello girls, 

I find it a great pleasure to post my recipes to you, Soulef, and I really like this option that you put in our favor. 

Today I come to share with you my tajine of olives with fennel, a recipe that I often realize, and that we like a lot at home. 

it is a very easy dish, and very rich in taste, you will see.   


**tajine zitoune bel besbes, tajine with olives and fennel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/zitoun-004-1024x768-A.jpg)

Recipe type:  dish tagine  portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * chicken pieces (depending on the number of people) 
  * 500 gr of pitted green olives 
  * 2 bulbs of fennel 
  * 1 onion 
  * 1 cinnamon stick 
  * salt and black pepper 
  * olive oil 
  * 1 tablespoon of flour. 



**Realization steps**

  1. boil the green olives several times in water with a slice of lemon for desalinated. 
  2. clean the fennels and cut them in length, and place them in hot water, just so that they lose some of their bitterness. 
  3. in a large saucepan, fry the onion cut into small, add the chicken pieces. 
  4. add cinnamon, salt and black pepper, then cover with water and cook. 
  5. halfway through cooking, add olives and pieces of fennel, and reduce the sauce. 
  6. towards the end, take a little sauce, add the flour, stir well, if you have lumps, pass the Chinese sauce. 
  7. add this sauce to the pot, and let the sauce of your dish reduce and take. 
  8. you can before serving, pass the pieces of chicken cooked in the fryer to have a beautiful golden color. 



[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tajine-zitoune-bel-besbes-1024x768.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tajine-zitoune-bel-besbes.jpg>)
