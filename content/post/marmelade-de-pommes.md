---
title: apple marmalade
date: '2015-09-15'
categories:
- jams and spreads
tags:
- Toasts

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-pommes-2.jpg
---
[ ![apple marmalade 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-pommes-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-pommes-2.jpg>)

##  Apple marmalade, 

Hello everybody, 

After the [ peach marmalade ](<https://www.amourdecuisine.fr/article-marmelade-de-peches.html>) , here we are with the apple marmalade, a recipe that prepares for us Lunetoiles ... The photos are enticing, and I have only one desire, taste a little! 

Lunetoiles asked me to post this recipe as soon as possible, because it will send me soon a recipe based on this marmalade of apples, so we will wait for the surprise recipe soon, ok! 

**apple marmalade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-pommes-1.jpg)

portions:  4  Prep time:  15 mins  cooking:  60 mins  total:  1 hour 15 mins 

**Ingredients**

  * For 1kg of boiled apples: 
  * 750 g of sugar 
  * 1 stick of vanilla 
  * glass jars and lid previously washed and dried 



**Realization steps**

  1. Choose healthy, ripe apples. 
  2. Wash them and wipe them thoroughly, cut them up and cut them in half and then in four. 
  3. Weigh them and put them on the fire in a large saucepan with ½ glass of water per kilogram of fruit. 
  4. Cook them by mixing frequently with a wooden spoon, until all the water has evaporated. 
  5. Then pass them to the vegetable mill to obtain a smooth puree or using a plunging blender. 
  6. Then weigh the obtained apple puree and calculate the amount of sugar needed (750 g of sugar per kilo of apple puree). 
  7. Then put the apples, sugar, and split vanilla on the fire.Remuse. 
  8. Boil the mixture very gently for about 1 hour. 
  9. Check the cooking by doing the test on a plate: by dropping a drop of marmalade on an inclined cold plate, it must freeze immediately. 
  10. Then remove from the fire. 
  11. Immediately put in clean pots and dried. 
  12. close them well. 



[ ![apple marmalade 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-pommes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-pommes.jpg>)
