---
title: Al-Aqsa Mosque
date: '2009-01-11'
categories:
- dessert, crumbles and bars
- sweet recipes

---
It was only with the arrival of Islam that the mosque esplanade was again used for religious buildings. 

« Gloire et Pureté à Celui qui de nuit, fit voyager Son serviteur [Mohammed], de la Mosquée Al-Haram à la Mosquée Al-Aqsa dont Nous avons béni l’alentours, afin de lui faire voir certaines de Nos merveilles. C’est Lui, vraiment, qui est l’Audient, le Clairvoyant. » (Coran 17;1) 
