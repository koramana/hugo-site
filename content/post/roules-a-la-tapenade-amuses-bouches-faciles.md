---
title: tapenade rolls, easy mouths
date: '2017-12-21'
categories:
- Chhiwate Choumicha
tags:
- Christmas
- Holidays

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/roule-a-la-tapenade-037.CR2_1.jpg
---
![tapenade rolls](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/roule-a-la-tapenade-037.CR2_1.jpg)

##  tapenade rolls, easy mouths 

Hello everybody, 

here are some tapenade rolls that I often do when I prepare for the [ homemade tapenade ](<https://www.amourdecuisine.fr/article-tapenade.html>) , I must imperatively realize these amusing mouths easy because my husband likes very much. 

It's super crisp, and too good, especially when you like the tapenade. A recipe very very easy, inexpensive, and especially delicious.   


**tapenade rolls, easy mouths**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/roule-a-la-tapenade-039.CR2_1.jpg)

portions:  6  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * a roll of puff pastry. 
  * [ tapenade ](<https://www.amourdecuisine.fr/article-tapenade.html> "tapenade")
  * 1 egg white. 



**Realization steps**

  1. unroll the puff pastry. 
  2. garnish with the tapenade, and leave a cm on the widest side. 
  3. brush this small side with egg white. 
  4. roll the dough sheet again, and place it in the freezer for almost 30 minutes 
  5. when you remove the pudding from the freezer, preheat the oven to 160 degrees C 
  6. cut with a thick knife, (not with the electric knife, it will not cut well) slices of a cm almost (but not smaller than 1 cm) 
  7. if you are afraid that the rolls will take place during cooking, you can prick them with a toothpick 
  8. place them as you go on a baking tray, space them just a little. 
  9. cook in the oven, until the center of the roll gets a nice golden color. 
  10. remove them, let cool a little before removing them from the hotplate, because when they are hot, they can deform quickly, 



![Roll-a-la-tapenade-043.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/roule-a-la-tapenade-043.CR2_11.jpg)
