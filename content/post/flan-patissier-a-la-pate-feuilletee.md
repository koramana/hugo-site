---
title: pastry custard with puff pastry
date: '2012-03-20'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/flanlouiza5_thumb1.jpg
---
##  pastry custard with puff pastry 

so I copy you paste of the recipe: 

**Mold 22-24 cm in diameter with high edges (5 cm):**   


**pastry custard with puff pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/flanlouiza5_thumb1.jpg)

portions:  8  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * puff pastry 
  * 1 L of semi-skimmed milk 
  * 3 eggs 
  * 160 g of sugar 
  * 100 g cornflour 
  * 2 bags of vanilla sugar 
  * 3 teaspoons of vanilla extract 
  * ½ teaspoon vanilla powder (or a vanilla pod) 



**Realization steps**

  1. With an electric whisk, beat the eggs with cornstarch, vanilla extract and 125 ml of milk (taken from the liter of milk of the recipe) in a bowl. 
  2. Pour the remaining milk into a saucepan, with the vanilla sugar, sugar, vanilla powder (or the vanilla pod split in half lengthwise and scraped). 
  3. Bring to a boil. Pour boiling vanilla sweet milk over egg mixture while beating (whisk). 
  4. Pour everything back into the saucepan, put back on very low heat and stir with a wooden spatula. 
  5. To simmer a few seconds, no more, it is necessary that the preparation is slightly thickened. 
  6. Pour the mixture on the dough (removing the vanilla pod if you put one) in the mold and bake for 40 minutes at 180 ° C. 
  7. Cover with aluminum if necessary the last 5 minutes. 
  8. Allow to cool completely before unmolding and cutting. Keep refrigerated. 


