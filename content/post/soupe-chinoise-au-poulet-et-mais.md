---
title: Chinese soup with chicken and corn
date: '2017-10-30'
categories:
- diverse cuisine
- Cuisine by country
- Healthy cuisine
- recipes at minus 300 Kcal
- soups and velvets
tags:
- Soup
- inputs
- Holidays
- Christmas
- Healthy cuisine
- Dietetic cooking
- Diet

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/10/soupe-chinoise-au-poulet-et-ma%C3%AFs-1.jpg
---
![Chinese soup with chicken and corn 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/soupe-chinoise-au-poulet-et-ma%C3%AFs-1.jpg)

##  Chinese soup with chicken and corn 

Hello everybody, 

For a quick recipe, I confirm that this Chinese soup with chicken and corn is ready in the blink of an eye! Not only this soup is very easy to make, you can make with the rest of chicken too, it is low calorie too, so ideal for dieting. 

J’avais envie de cette soupe depuis un moment, et quand je faisais le montage de la vidéo réalisée par mon amie Lynda Akdader, je n’avais qu’une envie de la joindre pour déguster un bol avec elle. Et matin, la premiere chose que j’ai faite est de préparer ma belle et bien bonne soupe chinoise, surtout que j’avais tout les ingrédients sous la main. Bien sur ma recette est un peu différente de la sienne, mais le principe est le même 😉 

You can see Lynda's video here: 

**Chinese soup with chicken and corn**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/soupe-chinoise-au-poulet-et-ma%C3%AFs.jpg)

**Ingredients**

  * 750 ml of [ chicken broth ](<https://www.amourdecuisine.fr/article-bouillon-de-poulet-fait-maison.html>) (otherwise a diluted bouillon cube in 750 ml) 
  * 450 gr of canned corn 
  * 1 C. Soybean 
  * 1 C. coffee ginger minced or finely chopped 
  * 1 garlic clove minced or finely chopped 
  * 1 teaspoon cornflour 
  * 1 egg beaten 
  * 1 cup cooked chicken 
  * Salt and white pepper, to taste 
  * 3 c. chopped shallots / shallots 



**Realization steps**

  1. Put chicken broth, puréed corn, soy sauce, ginger, garlic and corn spoon diluted in water in a saucepan over high heat. 
  2. Bring to a boil, then reduce heat to medium and stir occasionally. Bake 5 minutes or until slightly thickened. 
  3. Add the chicken already cooked, season with white pepper 
  4. Adjust the seasoning with salt, extinguish the heat and slowly stir in the beaten egg. It also thickens the soup. 
  5. serve garnished with shallots. 



![Chinese soup with chicken and corn 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/soupe-chinoise-au-poulet-et-ma%C3%AFs-2.jpg)
