---
title: Kabyle galette stuffed with chakchouka
date: '2018-03-07'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- Mina el forn
- bread, traditional bread, cake
- pizzas / quiches / pies and sandwiches
- ramadan recipe
tags:
- Algeria
- Bread
- Boulange
- Bakery
- slippers
- inputs
- salted

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-012.jpg
---
[ ![Kabyle galette stuffed with chakchouka](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-012.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-012.jpg>)

##  Kabyle galette stuffed with chakchouka 

Hello everybody, 

The **Kabyle galette stuffed with chakchouka** is a culinary heritage of Kabylie, which I had the chance to learn at a young age, I remember that I was barely 10 years old, maybe less, when I started kneading to help my mother. She gave me small quantities of dough which I rolled in a small "Djefna en terre" - terrine, which had been offered to me by the aunt of her mother, who herself made earthenware and baskets of straw. 

I remember when we went to visit her, she was preparing this kabyle galette stuffed with the chakchouka (onions) very pungent, she cooked on a special trick filled with "f'ham" -charbon and wood, I feel now even smell that soaked the house at the time, the sublime smell of olive oil drained by the hands of my aunt, mixed with the semolina, which cooked gently on this tadjine in the ground, the heat that there cleared and warmed the whole place ... A lot of memories jostling in my head at the same time, and that I want to tell you, but I think that rather wanted the recipe. 

The recipe of this kabyle cake stuffed with chakchouka is super easy to make, and if you have olive oil from the wheat, it's even better, because with the extra virgin olive oil, pn n ' does not have the same smell or the same taste. In any case, personally I never come back from my holidays in Algeria without a large bottle of 2 liters of freshly wrung olive oil, whether it is the land of my aunt's husband in Bouira "Ait laaziz", or from that of the mother's aunt to Tizi Ouzou "Batrouna, Ain Mezyab". 

And nobody touches this oil, I keep it especially for a good [ hmiss ](<https://www.amourdecuisine.fr/article-hmiss-salade-de-poivrons.html> "hmiss, pepper salad") , a delicious [ couscous with vegetables ](<https://www.amourdecuisine.fr/article-couscous-aux-legumes-amekfoul-cuisine-algerienne.html> "Couscous with vegetables / amekfoul, Algerian cuisine") , or [ couscous with beans »aghmoudh» ](<https://www.amourdecuisine.fr/article-25345506.html> "Aghmoudh ... .. Couscous with beans in olive oil") or to make this Kabyle galette .... I like when I use it, feel that strong smell that tells a whole story. 

**Kabyle galette stuffed with chakchouka**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-a-la-chakchouka-1.jpg)

portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 500g of fine or medium semolina. 
  * 120 ml of olive oil 
  * 1 teaspoon of salt 
  * ½ teaspoon instant baker's yeast 
  * ½ teaspoon of baking powder (optional) 
  * Some water 

for the chakchouka: 
  * 2 big onions 
  * 1 crushed garlic clove 
  * 2 to 3 fresh tomatoes 
  * salt 
  * 2 tablespoons extra virgin olive oil 
  * 1 tablespoon of dried mint (here I use fresh, because my children do not like the strong taste of dry mint, I'm crazy about it) 



**Realization steps** Prepare the chakchouka: 

  1. fry the thinly sliced ​​onion in a little oil, add crushed garlic halfway through cooking 
  2. Then introduce the tomato cut into small cubes 
  3. and let it cook, season according to your taste. 

prepare the pasta: 
  1. Prepare pasta by sifting semolina, salt, yeast and oil 
  2. Pick up with water without kneading just to curl up 
  3. Divide the dough into 2 equal balls then each two-part ball in one is larger than the other 
  4. Spread the large ball to a thickness of ½ cm. spread over the cooled chakchouka stuffing.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-a-la-chakchouka-4-267x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-a-la-chakchouka-4-267x300.jpg>)
  5. spread the second ball in a smaller diameter, and cover the stuffing on top.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-256x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-256x300.jpg>)
  6. Fold the sides of the big dough on the dough from above to weld it well.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-262x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-262x300.jpg>)
  7. Prick with a fork 
  8. Cook on a crepiere or tajine slit on both sides until you have a nice color. 



![Kabyle galette stuffed with chakchouka](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-a-la-chakchouka-001.jpg)
