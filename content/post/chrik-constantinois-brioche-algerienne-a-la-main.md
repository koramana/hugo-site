---
title: Chrik Constantinois, Algerian brioche in hand
date: '2015-06-30'
categories:
- Buns and pastries
- Mina el forn
tags:
- ramadan 2015
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/chrik-constantinois-brioche-algerienne-fait-a-la-main-011.jpg
---
##  Chrik Constantinois, Algerian brioche in hand 

Hello everybody, 

The chrik is a fluffy Constantinian cake with a very tight yarn, that my mother made us all the time and hand please, when we were little, and that I found taste to cook the buns, it is on the first brioche that I found myself going to realize to make taste this delight to my children, and to tell them my little gourmet stories. 

And I'm super happy that they enjoyed it, especially since it was not too sweet. 

**Chrik Constantinois, Algerian brioche in hand**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/chrik-constantinois-brioche-algerienne-fait-a-la-main-011.jpg)

Recipe type:  chrik constantinois, Algerian brioche in hand  portions:  6  Prep time:  45 mins  cooking:  20 mins  total:  1 hour 5 mins 

**Ingredients**

  * 3 beaten eggs 
  * 100 ml of water 
  * 3 tablespoons milk powder 
  * 40 ml of orange blossom water 
  * 50 gr of sugar 
  * ½ teaspoon of salt 
  * \+ or - 450 gr of flour 
  * 100 gr of soft butter in pieces 
  * 1 tablespoon of dehydrated yeast 
  * ½ teaspoon of baking powder 

for decoration: 
  * 1 egg yolk 
  * 1 tablespoon of milk 
  * crystallized sugar, or then grains of sesame. 



**Realization steps**

  1. sift the flour. 
  2. pour in the center the sugar, and the beaten eggs. 
  3. sand well between your hands. 
  4. add the two yeasts, milk powder, and collect with warm water. 
  5. then add the orange blossom water, and knead. 
  6. cut the butter into pieces, and crush in small amounts on the work surface, and knead the dough, until the butter is completely absorbed. 
  7. do so until all the butter is used up. 
  8. if the dough near the end sticks too much to your hand, you can add a tablespoon of flour until you have a dough that is very malleable and does not stick too much. 
  9. place the dough now in a floured salad bowl, and put either in a hot oven, or in a place sheltered from drafts but at medium temperature (I put in front of the radiator) 
  10. let the dough rise until it doubles in size. 
  11. degas the dough, and form dumplings (I weigh pellets of less than 70 gr each) 
  12. place your spaced meatballs in a floured tin or covered with baking paper. 
  13. cover with a cloth, and let doubling in volume (the time will depend on the temperature of the room or so the season) for me it is between 1:30 and 2 hours, sometimes 3 hours, the more your dough swells, better will be the result, do not be in a hurry to bake your buns. 
  14. decorate your buns with the eggs and milk mixture, then sprinkle with sugar or, according to your taste, sesame seeds. 
  15. cook in a preheated oven at 180 degrees between 15 and 20 minutes, or depending on your oven, mine will cook in not even 15 minutes. because the balls are very very light, they will cook very quickly. 
  16. let the chrik dumplings cool on a rack, and enjoy with a good tea 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
