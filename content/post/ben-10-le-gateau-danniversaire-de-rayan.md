---
title: Ben 10, Rayan's birthday cake
date: '2010-07-30'
categories:
- amuse bouche, tapas, mise en bouche
- crepes, gauffres, beignets salees
- diverse cuisine
- idee, recette de fete, aperitif apero dinatoire
- pizzas / quiches / pies and sandwiches

---
hello everyone, first of all thank you all in place of rayan your comments were beautiful, he is happy to know that there are people who said "happy birthday" famous my little picasso, hihihihihih 

in any case, yesterday between power failure, no connection and all that can disturb, I could not post the recipe, I will try, and I hope that the recipe will pass. 

Here is the cup of the cake, and I can assure you that it was beautiful, delicious, melting, and especially too beautiful in the eyes of my son with his little drawing of Ben 10. 

at the base of the cake, I prepare a cake with yogurt, very very fondant. then I cut in two I stuffed with a cream muslin and pears in syrup, in piece, I garnished with a royal glaze, and decorate with the drawing of Ben 10 made with chocolate. 

so I go to the ingredients: 

yogurt cake: (in a mold of 23 cm diameter) 

  * 2 jars of natural yoghurt 
  * 6 eggs 
  * 3 jars of sugar (when I say jar, I use the jar of yogurt as a measure) 
  * 1 C. a vanilla coffee 
  * 6 pots of flour 
  * 2 tbsp. tablespoon of baking powder 
  * 1 pot of oil 



Mix eggs, sugar, yoghurt, add oil and vanilla. Mix flour and baking powder, add to the mixture,   
Butter the mold, pour the dough and cook for 45 minutes to 1 hour at 180 °. Check the cooking with a needle or knife.   
Let cool on rack 

Chiffon cream: 

  * 250 ml of milk 
  * 57 g of sugar 
  * 1 packet of vanilla sugar 
  * 2 egg yolks 
  * 30 g cornflour 
  * 100 g soft butter or margarine 



better start with the cream muslin, to give it time to rest and cool   
Remove the butter from the refrigerator so that it is at room temperature.   
Mix the egg yolks, cornflour, and sugars in a saucepan and whisk well.   
Pour the hot milk over, mix at the same time, and put back on medium heat while stirring constantly with a wooden spoon until the mixture thickens (about 4-5 minutes).   
Out of the fire add half of the butter.   
Let cool, add the other half of the butter in the cooled cream and whisk well with an electric mixer if possible until a light and foamy mass that must be almost white. 

cover a cake disc with the muslin cream, and decorate with pears cut into pieces (or other fruit of your choice) 

cover with the other disc and make parreil, covering the cake with cream muslin, and fishing. 

for my cake I made half of these ingredients. 

  * 500g icing sugar 
  * 2white eggs and a half 
  * juice of half a lemon 
  * food coloring (I put some blue) 



Work 3/4 of the sugar with the whites and the lemon juice for 15 minutes (by hand) 8 minutes (in the blender) 

When it's homogeneous, clear, white and light add the rest of the sugar and whisk another 2 minutes 

if it's not firm enough add a little icing sugar 

try not to whip in the blender so that there are as few air pockets as possible 

once ready, cover with damp cloth so that it does not dry out 

Beat vigorously before using 

It can be kept in the fridge for one week provided it is covered with 2 plastic films: the first directly against the surface: to avoid a crust 

Finally can make it thicker or thinner as needed. 

cover all the cake and garnish the sides with grilled and coarsely crushed peanuts. 

now for drawing Ben 10, I use white and black chocolate to garnish and fill the drawing: 

ingredients: 

2 squares of dark chocolate 

4 squares of white chocolate 

green food coloring 

light gray food coloring 

dark gray food coloring (I did not have both gray colors, I mixed a little red, green and blue dye to have gray) 

start by drawing the contours of the drawing with melted dark chocolate and place in a small bag, you cut with scissors to have a tiny hole, 

the rest is a breeze, made color by color, letting dry after each coloring. 

at the end invert the drawing on the cake before the royal glaze dries to adhere well. 

and here we are, our cake Ben 10. 

merci pour vos commentaires, et merci de vous inscrire a la newsletter pour etre a jour avec chaque nouvelle publication. 
