---
title: Halwat tabaa, dry Algerian cake
date: '2015-03-09'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- Gateaux Secs algeriens, petits fours
tags:
- To taste
- Cookies
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/halwat-tabaa-11.jpg
---
#  ![halwat tabaa, dry Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/halwat-tabaa-11.jpg)

##  Halwat tabaa, dry Algerian cake 

Hello everybody, 

I make you a confession ... !!! it's been almost 10 years since I ate this delicious cake, the famous **Halwat tabaa.**

I remember that my mother always made this cake for us, and I confess that I was the mouse of the house, where she hid the cake, I found it again, and of course, I used it at will, oh It must be said that I had my little punishment, when my mother opened the box or she kept the cake, to find two cupcakes, the only ones that escaped me. 

So today, I promised myself to taste these little delights to my children, and I was very happy that my little loulous liked this little greed (they inherited well from their mother, hihihihi). 

My mother made them with peanuts, almonds, sesame, and many others ... .. for my part I made the recipe based grilled sesame seeds crispy, a real pleasure. 

  
![halwat tabaa](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/sables-aux-grains-de-sesames_thumb_12.jpg)

**Halwat tabaa, dry Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/halwat-tabaa-bel-djeldjlane1.jpg)

portions:  40  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 500 grs of flour (+ or -) 
  * 3 eggs 
  * 125 ml of oil 
  * 130 gr of sugar 
  * 1 sachet of baking powder 
  * 1 good handful of grilled sesame seeds 
  * 1 cup of vanilla coffee 
  * 1 egg yolk to brown 



**Realization steps**

  1. in a bowl, pour oil, eggs, sugar, vanilla and beat well to have a nice mousse. 
  2. add the sesame seeds, add the mixture flour and baking powder gradually, by amalgamating with the hands until obtaining a smooth and homogeneous paste. 
  3. take a piece of dough, spread it on a baking roll about 1/2 cm thick. 
  4. Cut out the pasta dough of your choice, or failing with a glass. 
  5. Repeat the operation until all the dough has been used up. 
  6. Oil and flour a baking sheet. 
  7. arrange the shortbreads by spacing them on the plate, brown them with egg yolk 
  8. bake and cook in a preheated oven at 180 degrees C for 15 to 20 minutes. 



[ ![halwat tabaa dry Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/halwat-tabaa_thumb2.jpg) ](<https://www.amourdecuisine.fr/article-halwat-tabaa-bel-jeljlane-sables-aux-grains-de-sesames.html/halwat-tabaa_thumb-2>)

just for info: 

You can do without some sesame seeds, and nothing will change in the recipe. 

thank you for all your comments. 

For more Algerian cakes recipes RDV on the [ category Algerian cakes ](<https://www.amourdecuisine.fr/gateaux-algeriens-orientales-modernes-fete-aid>) , or then the page of [ index des Gateaux algeriens ](<https://www.amourdecuisine.fr/article-gateau-algerien-et-gateaux-orientals-pour-laid-el-fitr-2014.html>)
