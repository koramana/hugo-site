---
title: Eggplant caviar
date: '2015-06-14'
categories:
- vegetarian dishes
tags:
- Vegetarian cuisine
- Healthy cuisine
- Easy cooking
- inputs
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/caviar-d-aubergine_thumb.jpg
---
#  [ ![eggplant caviar](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/caviar-d-aubergine_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/caviar-d-aubergine.jpg>)

##  Eggplant caviar 

Hello everybody, 

My mother always prepared this recipe, but I did not know it was called: Baba ghenouj, or eggplant caviar, for us it's a grilled aubergine salad, which we enjoyed with good [ matlou3 ](<https://www.amourdecuisine.fr/article-matloue-matlou3-matlou-pain-maison-au-tadjine.html> "Matlouh, matlou3, matlouh - home-made tajine bread") so in my head it's a recipe from [ Algerian cuisine ](<https://www.amourdecuisine.fr/cuisine-algerienne>) .... 

Is the difference in this touch of sesame cream ??? or in some recipes adding natural yoghurt .... I will try this soon, because I like eggplant in all these states. 

**Eggplant caviar**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/baba-ghanouj_thumb.jpg)

portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 2 medium eggplants 
  * 2 tablespoons sesame cream (tahina) 
  * ½ glass of lemon juice 
  * 2 cloves garlic 
  * Salt and pepper 
  * olive oil 



**Realization steps**

  1. Wash the eggplants. grill them either in the oven or on the grill; 
  2. when they have become tender, take out of the oven and put them in a bag to sweat well. 
  3. remove the skin. 
  4. crush the aubergines with a fork. 
  5. add the sesame cream and the rest of the ingredients. 
  6. mix with a fork. 
  7. put in the serving dish, and decorate with olive oil. 



[ ![baba ghanouj](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/baba-ghanouj_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/baba-ghanouj.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
