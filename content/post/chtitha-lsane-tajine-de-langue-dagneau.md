---
title: chtitha lsane, lamb tongue tajine
date: '2017-09-02'
categories:
- Algerian cuisine
tags:
- Algeria
- dishes
- Meat
- Ramadan 2016
- Ramadan
- Morocco
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chtitha-lsane-tajine-langue-dagneau-3.jpg
---
[ ![chtitha lsane, tajine tongue of lamb 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chtitha-lsane-tajine-langue-dagneau-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chtitha-lsane-tajine-langue-dagneau-1.jpg>)

##  chtitha lsane, lamb tongue tajine 

Hello everybody, 

Among the lamb-based recipes I like most is chtitha lasne, or lamb tongue tajine. Maybe it's not everyone's taste, but frankly I really like the texture of this part of meat. 

When I go to the butcher's shop in our neighborhood and I find the lamb tongue I do not buy a single plate (a plate usually contains 7 small languages), I buy a lot because I do not I do not find it all the time, and so I cook it as soon as I want to eat it. 

My favorite lamb tongue recipe is **chtitha lsane** , a recipe with reduced sauce, spicy, well perfumed and with the unique taste of chickpea, this recipe is just sublime.   


**chtitha lsane, lamb tongue tajine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chtitha-lsane-tajine-langue-dagneau-4.jpg)

portions:  6  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients**

  * 5 languages ​​of Lamb 
  * 2 cloves of garlic disguised 
  * 3 c. oil soup 
  * salt 
  * pepper 
  * 1 hot pepper (optional) 
  * 1 pinch of ground cumin 
  * ½ c. coffee mixture of garlic powder and coriander 
  * ½ c. paprika 
  * ½ c. tomato paste 
  * ½ can of chickpeas drained and rinsed 
  * water as needed and cooking the dish 



**Realization steps**

  1. Start by washing and rinsing the lamb tongues, then place them in salt water for at least 1 hour. 
  2. change the water and bring the tongues to a boil for 5 minutes at the end of the bleaching five (at this point you can peel them, I keep the skin, because they are super mowed and the skin is super melting, but generally I removed). 
  3. In a heavy-bottomed pot, add oil, squeezed garlic, spices and salt. 
  4. Then add the lamb tongues cut in medium slices and brown them. 
  5. Add the tomato paste and water, and cook until the meat becomes tender. 
  6. Add the chickpeas and let the sauce thicken. 
  7. Serve hot with a good homemade bread. 



[ ![chtitha lsane, tajine lamb tongue 6](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chtitha-lsane-tajine-langue-dagneau-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chtitha-lsane-tajine-langue-dagneau-6.jpg>)
