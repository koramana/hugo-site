---
title: focaccia with parmesan and rosemary
date: '2013-05-15'
categories:
- Algerian cuisine
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/focaccia-parmesan-et-romarin-031.CR2_1.jpg
---
Hello everyone, I like the focaccias well scented and well inflated, although after the degassing of the last lift, the shaping of bread, we do not think that after a rest of 15 minutes and cooking, she will be well inflated our focaccia. So yesterday to accompany a delicious chorba frik, I made this extra airy focaccia with parmesan and rosemary ... hum, I do not tell you, those beautiful smells that embalmed the house during the cooking .... to you intoxicating ... you can also see another delicious focaccia: Focaccia with rosemary and onion ingredients: & nbsp; 1 glass & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.6  (  1  ratings)  0 

![focaccia-parmesan-and-rosemary-031.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/focaccia-parmesan-et-romarin-031.CR2_1.jpg)

Hello everybody, 

I really like the focaccias well scented and well inflated, although after degassing the last lift, shaping the bread, we do not think that after a rest of 15 minutes and cooking, it will be well inflated our focaccia . 

So yesterday to accompany a delicious [ chorba Frik ](<https://www.amourdecuisine.fr/article-chorba-111535303.html>) , I made this extra airy focaccia with parmesan and rosemary ... hum, I do not tell you, those beautiful smells that embalmed the house when cooking .... to get drunk ... 

you can also see another delicious focaccia: [ Focaccia with rosemary and onion ](<https://www.amourdecuisine.fr/article-focaccia-au-romarin-et-oignon-62123009.html>)

![focaccia-parmesan-and-rosemary-020.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/focaccia-parmesan-et-romarin-020.CR2_1.jpg)

Ingredients: 

  * 1 glass of (230 ml) water 
  * 1 tablespoon olive oil 
  * 3 glasses (not complete everything depends on the absorption) of flour. 
  * 1 cup of salt 
  * 1 tablespoon of sugar, 
  * 1 tablespoon of dehydrated yeast (1 pack of 7 gr) 
  * 1 tablespoon fresh rosemary, 
  * 1/4 glass of grated parmesan cheese, 
  * Extra olive oil, to garnish the focaccia 
  * Coarse salt, to sprinkle 



![focaccia parmesan au-and-romarin.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/focaccia-au-parmesan-et-romarin1.jpg)

method of preparation: 

  1. in the mixer bowl of your robot, place a glass of flour, salt, sugar, oil and yeast. 
  2. add the glass of warm water and mix well to have a very very liquid paste. 
  3. add the rest of the flour in a small amount while mixing, until the dough comes off the sides of the bowl. 
  4. remove the dough from the bowl, knead just a little to mix the flour that was not well introduced to the dough. 
  5. place the dough in an oiled bowl and let stand for 1 hour, until the dough doubles in volume. 
  6. after the resting time, release the dough well, and place it in a baking sheet, oiled or even lined with baking paper. 
  7. flatten the dough for a good 1 cm round loaf, or an oval loaf, depending on your taste. 
  8. cover and let rest for 15 minutes 
  9. preheat the oven to 180 degrees C 
  10. take the dough again, and with your fingers, prick the dough. 
  11. spread the oil over it with a brush, sprinkle the rosemary over it, and the grated parmesan cheese. make 2 to 3 turn at the salt mill to roughly cover the whole surface. 
  12. cook in the oven for 20 to 25 minutes. 


