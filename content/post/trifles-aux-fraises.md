---
title: Trifles with strawberries
date: '2015-03-21'
categories:
- dessert, crumbles et barres
- verrines sucrees
tags:
- biscuits
- desserts
- verrines
- Chantilly
- creams
- Compote
- Bavarian

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trifles-aux-fraises-003.jpg
---
[ ![strawberry trifles](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trifles-aux-fraises-003.jpg) ](<https://www.amourdecuisine.fr/article-trifles-aux-fraises.html/trifles-aux-fraises-003>)

##  Trifles with strawberries 

Hello everybody, 

You remember the program The Best Pastry Chef, and the period when they realized the [ royal charlotte ](<https://www.amourdecuisine.fr/article-charlotte-royale-epreuve-technique-meilleur-patissier.html> "royal charlotte technical event best pastry chef") during the technical event. I realized mine using a Bavarian strawberry as a base. 

After the assembly of the royal charlotte, I still had a little Bavarian moss and rolled biscuit, which allowed me to make these strawberry trifles. and I assure you these strawberry trifles did not last long, as they were incredibly sweet. and an irresistible taste. 

**Trifles with strawberries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trifles-aux-fraises-027.jpg)

portions:  6-8  Prep time:  20 mins  cooking:  5 mins  total:  25 mins 

**Ingredients**

  * [ rolled jam cookie ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture.html> "rolled jam cookie") (not all the cake but some pieces) 

for the strawberry bavarois: 
  * 180 grs of frozen strawberries 
  * 2 to 3 tablespoons of sugar 
  * 4 g of gelatin 
  * 100 ml whipped cream whipped cream 
  * some strawberries 



**Realization steps** prepare strawberry bavarois 

  1. place the gelatine in cold water so that it swells 
  2. place the strawberries in a saucepan with the sugar 
  3. let melt and crush well with a fork, or blender. take the ⅓ of this strawberry compote 
  4. add the remaining gelatine and stir over medium heat until the gelatin melts completely, let cool. 
  5. Prepare the whipped cream, pour over the fruit compote a little cooled (warm to the touch) 
  6. stir gently with the spatula to introduce the compote into the mousse. 

Assembly of the verrines: 
  1. place 1 tablespoon of strawberries in pieces at the bottom of the verrines 
  2. place the Bravoise mousse in a piping bag 
  3. place a quantity of the mousse on the strawberries 
  4. place the equivalent of a tablespoon of cracked biscuit 
  5. pour over the equivalent of a tablespoon of strawberry compote 
  6. continue editing in the same way, until you fill the glasses 
  7. Decorate with a strawberry and a little whipped cream, and a nice fillet of strawberry compote on top 
  8. Refrigerate at least 2 hours. 



[ ![strawberry trifles](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trifles-aux-fraises-031.jpg) ](<https://www.amourdecuisine.fr/article-trifles-aux-fraises.html/trifles-aux-fraises-031>)
