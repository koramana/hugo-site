---
title: shrimp soup
date: '2009-05-11'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/05/206270091.jpg
---
It's a very delicious soup, its taste is unique, I like it, to be honest I have to write this recipe for the second time, because my beautiful daughter has touch the keyboard, and oops all the recipe has disappeared. 

then, the ingredients: 

  * shrimp (I buy the frozen) 
  * an onion 
  * a clove of garlic 
  * 2 bay leaves 
  * fresh coriander chopped 
  * thyme 
  * 1 case of tomato puree 
  * oil 
  * salt, black pepper, paprika 
  * rice 



in a pot, the onion and garlic are browned in blender, oil, 

![S7301810](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/206270091.jpg)

and we add the preserved tomato. 

![S7301811](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/206270931.jpg)

then add the shrimp, and the condiments, let it simmer a little 

![S7301815](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/2062720211.jpg)

and then add laurel, coriander, thyme and cover with water, and let it cook 

![S7301816](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/206273611.jpg)

At the end of cooking if you are the type who does not like a light soup, you can add a handful of rice, allow time to cook, and remove from the heat. 

![S7301823](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/206271391.jpg)

bonne appetit 
