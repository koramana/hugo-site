---
title: Thank you from a blogger
date: '2009-03-18'
categories:
- Buns and pastries
- Mina el forn

---
hello to our faithful readers (on behalf of all the bloggers touched by this discontent) 

I assure you that I was really moved yesterday by reading all the comments here and there, this really makes you want to continue. 

I had messages that said: do not give too much importance to the comments ... .. to tell the truth, if I do not give importance to the comments, I do not give importance to the people who leave them ......... .. finally, we always like to be rewarded on any work we do, when we cook, we like to be told: ah it's delicious .... 

when you have a good grade on exams, we like to be congratulated, look around you, there are nobel prizes on anything, there are always rewards on any success, on competitions of games, there are even contest on whoever eats the most, on anything, and these people provide a huge effort, for a reward that can be times a setting ... .. with a small certificate on ... .. 

So, we are human beings, and this is in our nature ...... ..for me, a comment of a person, is not going to be the comment of everyone, I like that you come to tell me: me I do not eat that for all the gold in the world, what is this recipe, I prefer that, has an invisible passage ... .. 

it makes me so happy that someone comes to say to me: oh here I did your recipe, and people found it delicious, or so, but I think she misses it, or next time I would do that, you see, it will make me happy, it will encourage me even more .... 

and it's not because you see 30 comments on the subject that you find that yours is not worth it ......... no sorry, every person has its place, do you like someone to eat for you? sleep in your place? going for a trip to your place? watch a movie for you? certainly not??!! and even the comments, do not let others leave them in your place. 

no one replaces another ............. I know that I write anything, under emotion, I want to say everything, but I do not find what to say. 

Finally, I had a problem yesterday, because I lost some recipes, I think my son deleted them, fortunately I made the backup of my blog, I will post them as and when, until that I reproach myself with this moral fatigue. for a fresh return on my blog, by then, I'm waiting for your comments. 

et personne ne remplace personne 
