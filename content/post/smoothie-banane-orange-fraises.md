---
title: Strawberry orange banana smoothie
date: '2016-05-04'
categories:
- juice and cocktail drinks without alcohol
- recipes at least 200 Kcal
tags:
- Juice
- Fast Food
- Healthy cuisine
- Drinks
- Summer kitchen
- Ramadan 2016
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-banane-orange-fraises-2.jpg
---
[ ![orange banana smoothie strawberries 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-banane-orange-fraises-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-banane-orange-fraises-2.jpg>)

##  Strawberry orange banana smoothie 

Hello everybody, 

I could not miss you the recipe for this delicious Strawberry Orange Banana Smoothie. I tasted this smoothie in a Lebanese restaurant that I often visit my family and me. This restaurant has a bar dedicated to smoothies, a menu of more than 40 recipes, the simplest recipes based on a single fruit or as they call them: **Solo smoothies** well-composed smoothies with 4 ingredients. 

My husband does not like all the combinations or saying it's one of the least sweet smoothies. For me, I'll be happy to go every day, and enjoy each time a smoothie menu, to savor everything. So I said for my husband, the nude smoothie kisses (hihihih the name is really funny: fucked naked, I wonder why this name?) Is the only smoothie that my husband likes in the whole list, amazing anyway, but recipe that this smoothie made from banana, orange and strawberries is just delicious. 

Although this combination is very easy, but I had to try to reproduce the recipe several times to reach a satisfactory result, I am 90% ready of the original recipe according to my husband, hihihihih. 

**Strawberry orange banana smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-banane-orange-fraises-4.jpg)

portions:  4  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 2 bananas without skin in slices 
  * 1 orange without skin in cubes 
  * 100 grs of frozen or fresh strawberries 
  * ½ glass of ice cubes 



**Realization steps**

  1. place all the ingredients in the blender bowl 
  2. mix until you have a homogeneous liquid 
  3. switch to Chinese to have a super smooth smoothie to sip 
  4. Consume immediately, I never prepare my smoothies in advance. 



[ ![orange banana smoothie strawberries 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-banane-orange-fraises-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-banane-orange-fraises-1.jpg>)
