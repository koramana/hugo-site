---
title: Osbane - osbana - Stuffed belly
date: '2012-04-30'
categories:
- gateaux algeriens au miel
- gateaux algeriens- orientales- modernes- fete aid
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/Couscous-bel-osbane-001_thumb.jpg
---
##  Osbane - osbana - Stuffed belly 

Hello everybody, 

I lingered to post the recipe of [ couscous with stuffed belly ](<https://www.amourdecuisine.fr/article-couscous-bel-osbane-96835375.html>) , [ couscous bel osbane ](<https://www.amourdecuisine.fr/article-couscous-bel-osbane-96835375.html>) because I wanted to show you how to prepare Osbane - osbana - stuffed pans. the problem is that it was my mother who prepared the stuffed stomach of the sheep, and the photos were at her place, so I was waiting for her to give me the pictures, and that's it. 

I know that there will be people who will not like these photos, because not everyone likes tripe, but for my part, it is a delight that I eat only once a year, when I leave in Algeria, so sorry my friends, I love madly. 

So here are the cleansed ingredients, and I'll give you the recipe:   


**Osbane - osbana - Stuffed belly**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/Couscous-bel-osbane-001_thumb.jpg)

**Ingredients**

  * sheep's guts with lungs and intestines 
  * 1 handful of chickpeas soaked the day before 
  * 1 tablespoon tomato concentrate 
  * 1 onion 
  * 2 cloves garlic. 
  * 1 bunch of parsley 
  * 1 bunch of coriander 
  * 1 tablespoon of paprika 
  * ½ tsp of black pepper 
  * 1 tablespoon of cumin 
  * salt 



**Realization steps**

  1. Clean the guts   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tripes-de-mouton_thumb1.jpg)
  2. cut the belly into 5 or 6 large pieces, so that each piece folds in 2 will form a pocket of about 10 cm of dimension 
  3. then cut off the falls, lungs, and intestines. 
  4. put these in a terrine 
  5. add the onion and grated garlic, parsley and chopped coriander, chickpeas, paprika, black pepper, cumin, salt, and canned tomato. 
  6. mix everything well   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/3osbana_thumb1.jpg)
  7. sew the pockets (the rough side inside) with a thick needle and a solid thread, leaving an opening to introduce the stuffing 
  8. Gently fill these stuffing pockets without overloading them 
  9. then finish sewing the opening 
  10. prick with a fork. 



Note you can do like my mother, freeze these osbanes until a moment and you will want, or then prepare them the next day, in sauce (recipe to come) or with couscous like the recipe of the [ couscous bel osban ](<https://www.amourdecuisine.fr/article-couscous-bel-osbane-96835375.html>)   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-bel-3osbane_thumb1.jpg)

method of preparation: 

merci pour vos visites et bonne journee 
