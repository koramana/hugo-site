---
title: chicken cakes stuffed with cheese
date: '2012-06-16'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/face-005_thumb1.jpg
---
![chicken patties stuffed with cheese 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/face-005_thumb1.jpg)

##  chicken cakes stuffed with cheese 

Hello everybody, 

these chicken patties stuffed with cheese are too good, my husband liked it so much that he reclaims this recipe often. 

Do not hesitate to make this delight because frankly it is a different way to taste the chicken breast in addition to being a recipe that is done in a jiffy. 

Do not forget if you have tried one of my recipes to share the photos with me on this email: 

![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg)

**chicken cakes stuffed with cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/face-009.jpg)

**Ingredients**

  * 500 gr of chopped white chicken 
  * salt and black pepper 
  * cheese according to your taste 

for breading: 
  * an egg 
  * flour 
  * breadcrumbs   
frying oil 



**Realization steps**

  1. mix the chicken breast, salt and pepper 
  2. take a small amount of meat flatten it on a dish 
  3. floured to work better, put on the 3 cheeses, 
  4. close the meat, and flatten to have a nice puck. 
  5. pass them to the flour, egg and breadcrumbs 
  6. once you have finished all the meat, heat the oil and fry over medium heat 



dégustez chaud. 
