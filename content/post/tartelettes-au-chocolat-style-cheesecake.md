---
title: cheesecake style chocolate tartlets
date: '2014-12-19'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cheesecake-au-chocolat-96_thumb1.jpg
---
##  cheesecake style chocolate tartlets 

Hello everybody, 

Dessesans **oeufrts** chocolate, I do not like too much, because when I'm home, I will not stop to taste a spoon after another without getting tired .... and I do not tell you the result on the scale after !!! 

These chocolate tartlets are a very easy dessert to make, a **Cheesecake without cooking** with a beautiful **chocolate mousse** who garnished, and with each spoon tasted, we have these delicious pieces of twix, well melting in the mouth ... .. 

so…. you ... how many spoons do you think you stop? 

**chocolate tart / dessert easy and fast for the end of the year**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cheesecake-au-chocolat-96_thumb1.jpg)

Recipe type:  dessert, pie and tartlets  portions:  6  Prep time:  60 mins  cooking:  15 mins  total:  1 hour 15 mins 

**Ingredients** for the base: 

  * 100 grams of biscuits (speculoos for me) 
  * 2 twix 
  * 50 g of melted butter 

For the cheese mousse: 
  * 220 g unsalted cheese (Philadelphia) 
  * 50 grams of sugar 
  * 1 teaspoon of vanilla 
  * 250 ml cold cream very cold 
  * 4 fingers of twix 
  * 1for decoration: 
  * 100 g of dark chocolate 
  * 250 ml of fresh cream 



**Realization steps** the base 

  1. In the bowl of the blinder, crush the biscuits and and finely twix. 
  2. Add the melted butter and mix. 
  3. garnish the tart molds with this mixture, covering the base and sides well. 
  4. put in the fridge time to prepare the layer of cheese. 

the forming machine 
  1. whip the cheese, sugar and vanilla. 
  2. beat the cream until stiff. 
  3. Gently stir the whipped cream into the cheese mixture, one third each time, until smooth. 
  4. introduce the pieces of crushed twix 
  5. Fill the pie pan on a base. 
  6. smooth with a spatula and return to the fridge.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cheesecake-sans-cuisson-au-chocolat-119.CR2_thumb3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cheesecake-sans-cuisson-au-chocolat-119.CR2_thumb3.jpg>)

preparation of the filling: 
  1. warm half of the cream in a small saucepan to a boil, 
  2. remove from heat and pour over crushed chocolate immediately. 
  3. Stir well until chocolate melts completely. 
  4. the mixture is going to be at room temperature, whisk the other half of the cream, until the mixture gets the texture of a chocolate mousse 
  5. put this foam in a pastry bag with a circular opening without decoration (1 cm smooth socket), and put in the fridge for 5 minutes. 
  6. take the sleeve pocket, and make a line of balls of the same size. 
  7. with the back of a teaspoon press each circle while sweeping down, 
  8. wipe each time the spoon to do the same with the adjacent ball, 
  9. repeat until the cake is well covered 
  10. put back in the fridge for 30 to 1 hour and enjoy. 



{{< youtube pcOStKvsJD8 >}} 
