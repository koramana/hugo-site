---
title: shrimp sautéed with coconut milk
date: '2013-12-14'
categories:
- appetizer, tapas, appetizer
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/riz-et-crevette-009.CR2_.jpg
---
[ ![rice and shrimp 009.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/riz-et-crevette-009.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/riz-et-crevette-009.CR2_.jpg>)

Hello everybody, 

A delicious shrimp sauce, or prawns sautéed in coconut milk that I realized a while ago, I made it on video, but I never post it (or does it I have the head here ??? 

Well it's never too late, and this recipe is a delight that I recommend you do, because it is a taste to be absolutely tested. 

I tell you something too, it's a little dish that will really make you forget this cold winter ... it's a dish from Madagascar, which will add a lot of sun to your table. 

a sauce to serve with rice, or on toast. 

you can still see the video of this recipe: 

**shrimp sautéed with coconut milk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/riz-et-crevette-012.CR2_.jpg)

Recipe type:  dish  portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients** For 2 people: 

  * 250 grams of pre-cooked shrimp 
  * 200 ml of coconut milk 
  * 1.5 tbsp butter 
  * 1 medium onion, cut into 
  * 2 crushed garlic 
  * 1 tbsp grated fresh ginger 
  * 1 tablespoon of lemon juice 
  * 1 tablespoon of tomato paste 
  * Salt 
  * Black pepper 



**Realization steps**

  1. place the butter in a wok 
  2. add cubed onion, crushed garlic and grated ginger. 
  3. cook until it becomes translucent. 
  4. when the mixture is well cooked, add the shrimp. 
  5. Season with salt and black pepper. 
  6. Add the lemon juice, and the tomato paste. 
  7. mix and add over the coconut milk. 
  8. let everything cook well until the sauce becomes well reduced. 



[ ![rice and shrimp 010.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/riz-et-crevette-010.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/riz-et-crevette-010.CR2_.jpg>)
