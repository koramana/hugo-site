---
title: chocolate muffins, an inescapable recipe
date: '2013-01-26'
categories:
- Algerian cuisine
- diverse cuisine
- Dishes and salty recipes
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316889161.jpg
---
![chocolate muffin](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316889161.jpg) ![chocolate muffin](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316891381.jpg)

##  chocolate muffins, an inescapable recipe 

Hello everybody, 

What do you think of some **chocolate muffins** , an inescapable recipe for having muffins that are soft, well inflated, and super-melting in the mouth ???? 

Well here is my secret recipe, my favorite recipe that I pass absolutely to anyone who tasted my muffins and loved them .... It's always good to make a recipe and be successful every time, right? 

![chocolate muffin](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316890761.jpg)

**chocolate muffins, an inescapable recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/31689138.jpg)

**Ingredients**

  * 250 grams of flour 
  * 150 grams of sugar 
  * 2 tablespoons of cocoa 
  * 2 tablespoons of baking powder 
  * ½ cup of Bicarbonate 
  * ½ cup of vanilla coffee 
  * 1 pinch of salt 
  * 100 gr with 125 gr of crumbled chocolate 
  * 250 ml of milk 
  * 2 eggs 
  * 80 ml of oil 



**Realization steps**

  1. mix the dry ingredients in a container   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316890861.jpg)
  2. mix the liquids in another,   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316891031.jpg)
  3. you do not need a drummer, a fork will do the trick, just a little lash, to incorporate the liquids together. 
  4. add the liquid mixture to the dry ingredients mixture, just to incorporate everything, (do not mix, whip or use your mixer).   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316891121.jpg)
  5. fill the cassettes au high.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316891261.jpg)
  6. cook in the preheated oven at 200 degrees C for 5 min, then lower the temperature to 180 degrees C, for 15 minutes cooking (do not take this as a guide, check muffin baking with a toothpick) 



![chocolate muffin](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316891311.jpg) ![chocolate muffin](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/316891411.jpg)

source de la recette: Houriat el matbakh, Fatafeat TV 
