---
title: granola smoothie
date: '2015-12-14'
categories:
- juice and cocktail drinks without alcohol
- Cuisine saine
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-au-granola-1.jpg
---
[ ![granola smoothie 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-au-granola-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-au-granola-1.jpg>)

##  granola smoothie 

Hello everybody, 

I like to eat a good granola in the morning with milk ... But I never do it home, even though [ homemade granola ](<https://www.amourdecuisine.fr/article-granola-maison-extra-croustillant.html>) can be super rich in ingredients that you can choose yourself thoroughly, and make your granola, the most beneficial possible. 

But as I told you, I prefer a granola bought, hihihih because I find a brand that does not contain too much sugar at all, so for me it's much better than the one I could do home, and that risk to be a sweeter ... 

Today, with my granola, I made myself a super delicious smoothie ... yum yum what is super good.   


**granola smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-au-granola-2.jpg)

portions:  2  Prep time:  3 mins  total:  3 mins 

**Ingredients**

  * 1 glass of [ almond milk ](<https://www.amourdecuisine.fr/article-lait-d-amande-maison.html>)
  * 2 mature banana 
  * 1 glass of [ granola ](<https://www.amourdecuisine.fr/article-granola-maison-extra-croustillant.html>)
  * 1 glass of crushed ice cubes 



**Realization steps**

  1. place the granola in the bowl of the blender, and mix to reduce to powder 
  2. add the other ingredients, and mix again to have a smooth smoothie, 
  3. Deduce right away, while it is very fresh. 



[ ![granola smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-au-granola.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-au-granola.jpg>)
