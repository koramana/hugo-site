---
title: English-style slippers, (the coka)
date: '2007-12-16'
categories:
- Algerian cuisine
- Moroccan cuisine
- Tunisian cuisine
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200944681.jpg
---
![bonj20](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200991071.gif)

it's a very simple recipe, the coka as it was called here in Algeria, usually it was made in the shape of a crescent, but I made them in the shape of a big carree, you know for what ??? 

simply because I do not want to waste the puff pastry: 

![heart_84](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200991651.gif)

Ingredients: 

  * 1 beautiful onion 
  * 2 beautiful red tomato (or like me, a box of tomato cut) 
  * a head of garlic 
  * tuna (we like it at home) 
  * salt, pepper, coriander powder 
  * (you can add to your taste, olives, meat chop al aplce time .. each his taste) 



cut the onions in strips and cook in the tuna oil. 

![S7301612](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200944681.jpg)

add the tomatoes and cover so that it thighs well. season with salt, pepper, coriander powder, and garlic crush 

![S7301613](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200970401.jpg)

leave everything to simmer, 

Remove from heat and let cool. 

![S7301614](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200971171.jpg)

Now take the dough feuillete and do it at your leisure: 

![S7301616](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200987581.jpg)

wipe the edges with a beaten egg, so that it sticks well after, and put the stuffing already prepare 

![S7301617](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200988601.jpg)

once again, whit the top of your cokas well and place them in a baking tray, 

cook in a hot oven, and bake out 

![S7301619](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/2009896511.jpg)

enjoy yourself, and have a good appetite. 

![bye2](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200990871.jpg)
