---
title: chocolate ganache
date: '2016-08-24'
categories:
- basic pastry recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/ganache-au-chocolat.CR2_thumb1.jpg
---
##  chocolate ganache 

Hello everybody, 

here is my **chocolate ganache** ! My favorite recipe that I often make to decorate birthday cakes, my verrines, or just to make a delicious [ Biscuit Roll ](<https://www.amourdecuisine.fr/article-genoise-facile-pour-biscuit-roule.html>) , but I do not know why I never think to put the recipe of my chocolate ganache on my blog ??? Well, it's never too late, is it? 

You can also see the ganache [ with white chocolate ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat-blanc.html>) , which I often realize to garnish [ macarons ](<https://www.amourdecuisine.fr/article-macarons-a-la-ganache-au-chocolat-blanc.html>) . It's always successful all the time. 

**chocolate ganache**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/ganache-au-chocolat.CR2_thumb1.jpg)

Recipe type:  basic pastry recipe  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 250 gr of dark chocolate, 
  * 250 ml of liquid cream, 
  * 70 g of butter. 



**Realization steps**

  1. Roughly chop the chocolate and melt it in a bain-marie pan. 
  2. Meanwhile, in another saucepan, bring the liquid cream to a boil. 
  3. Pour the cream in three times over the melted chocolate. 
  4. Using a spatula, gently mix in small circles, starting from the center of the container to obtain a homogeneous mixture. 
  5. Let the mixture cool for 10 minutes, then add the cut butter. 
  6. Mix until your ganache is smooth and shiny. 
  7. Cover your ganache with cling film and let it rest in the refrigerator 15 minutes before use. 



{{< youtube cRWo_rWj >}} 
