---
title: Hazelnut and honey financiers
date: '2015-02-17'
categories:
- Cupcakes, macarons, et autres pâtisseries
- dessert, crumbles and bars
- gateaux, et cakes
- recettes sucrees
tags:
- Cakes
- desserts
- Pastry
- Algerian cakes
- To taste
- delicacies
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Financiers-aux-noisettes-et-miel-1.jpg
---
[ ![Financial hazelnut and honey 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Financiers-aux-noisettes-et-miel-1.jpg) ](<https://www.amourdecuisine.fr/article-financiers-aux-noisettes-et-miel.html/financiers-aux-noisettes-et-miel-1>)

##  Hazelnut and honey financiers 

Hello everybody, 

A delicious recipe of financiers with hazelnuts that shares with us Lunetoiles. I especially like the form that Lunetoiles has given them, to get out of the routine of the rectangular financiers, or to believe that they have to have financial mussels to make these little treats. 

Before you pass the recipe for these little fondant cakes, I pass you other recipes of financiers: 

[ financial cake with praline ](<https://www.amourdecuisine.fr/article-cake-financier-au-praline.html> "financial cake with praline") , [ with myertilles ](<https://www.amourdecuisine.fr/article-financiers-aux-myrtilles.html> "Blueberry financiers") , [ chocolate and hazelnuts ](<https://www.amourdecuisine.fr/article-financiers-au-chocolat-et-noisettes-recette-pour-gouter.html> "Chocolate and hazelnut financiers / recipe to taste") , [ marbled with strawberries ](<https://www.amourdecuisine.fr/article-financiers-marbre-aux-fraises.html> "Financial marbled strawberry")

and even more about [ financial ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=financier&sa=Rechercher>)

[ ![Hazelnut and honey financiers 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Financiers-aux-noisettes-et-miel-2.jpg) ](<https://www.amourdecuisine.fr/article-financiers-aux-noisettes-et-miel.html/financiers-aux-noisettes-et-miel-2>)   


**Hazelnut and honey financiers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Financiers-aux-noisettes-et-miel-3.jpg)

portions:  12  Prep time:  10 mins  cooking:  13 mins  total:  23 mins 

**Ingredients**

  * 150 g icing sugar 
  * 50 g of hazelnut powder 
  * 5 egg whites 
  * 2 grams of yeast 
  * 80 g of butter 
  * 60 gr of flour 
  * 50 g of hazelnuts 
  * 3 tablespoons of honey + honey for watering 



**Realization steps**

  1. Grill the hazelnuts in the oven at 160 ° C, watching the cooking well. 
  2. To make sure they are cooked, break a hazelnut: it must be slightly colored inside. 
  3. Melt the butter gently until you get a nutty butter. 
  4. Mix the icing sugar, the hazelnut powder, the flour and the yeast in a salad bowl. 
  5. Add the egg whites, then continue mixing by incorporating the nutty butter. 
  6. Then add 3 tablespoons of honey. 
  7. Pour into financial molds, otherwise use a muffin mold, butter well the cavities of the mold. 
  8. Crush the hazelnuts and spread them over the financiers. 
  9. Bake for 13 minutes at 190 ° C. 
  10. At the end of the oven, wait a little before unmolding, and sprinkle them with honey. 



[ ![Hazelnut and honey financiers](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Financiers-aux-noisettes-et-miel.jpg) ](<https://www.amourdecuisine.fr/article-financiers-aux-noisettes-et-miel.html/financiers-aux-noisettes-et-miel>)
