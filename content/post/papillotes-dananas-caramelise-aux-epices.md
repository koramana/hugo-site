---
title: Caramelized pineapple papillotes with spices
date: '2012-05-29'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours

---
**Hello everybody,**

**here is a delicious dessert that Lunetoiles likes to share with us.**

**I'm not out of my house yet, so apologize for putting my new recipes online**

**Ingredients for 6 people:**

1 big pineapple 

125 g caster sugar 

25 g of half-salted butter 

200 ml hot whole liquid cream 

1 cinnamon stick 

1 vanilla bean Bourbon 

2 stars of badiane 

**ingredients:**

Preheat the oven to 180 ° C. 

Remove the plumet and the base of the pineapple, then cut in 6 equal parts in the direction of the height. Detach the pulp pieces of bark and cut into pieces. 

Heat a large pan with the caster sugar. Cook and melt the sugar until it turns amber. Add the butter half salt, mix, then incorporate the hot liquid cream being careful to projections. Add cinnamon stick, cracked vanilla pod and star anise stars. To mix everything. 

Place the pineapple pieces in the pan, set the heat to a minimum and cook the pineapple pieces for about 10 minutes, turning them regularly. 

Cut out 6 rectangles of parchment paper. Place in the center of each piece of pineapple and pour over a little caramel spice. Close each candy-shaped foil or chaff with food thread and bake for 30 minutes. 

Thanks for your comments and your visits 

thank you for subscribing to my newsletter, if you do not want to miss all my new publications 
