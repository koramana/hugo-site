---
title: Smoked Salmon Tagliatelli
date: '2014-11-05'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tagliatelle-au-saumon-fume_thumb.jpg
---
[ ![Smoked Salmon Tagliatelli](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tagliatelle-au-saumon-fume_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tagliatelle-au-saumon-fume.jpg>)

##  **Smoked Salmon Tagliatelli**

Hello everybody, 

a dish very easy to achieve, especially on these days of heat ... a delicious recipe from Aina Sam, this tagliatelle recipe with smoked salmon (  pasta with smoked salmon  ). 

**Aina Sam's remark** :   
it is one of my first recipes that I prepared, I hope you will try in any case my husband asks me regularly even if it is not a fan of pasta. So you too have the time to participate in the contest, the recap will be online very soon, so quickly, quickly ...   


**Smoked Salmon Tagliatelli**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tagliatelle-au-saumon-fume_thumb.jpg)

Recipe type:  dish  portions:  2  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 400g of tagliatelle 
  * 200g of smoked salmon 
  * 2 shallots 
  * a few drops of lemon (I used the little yellow bottle) 
  * 25 cl of fresh cream 
  * 10 g of butter 
  * dried dill 
  * Cayenne pepper 
  * salt pepper 



**Realization steps**

  1. prepare the pasta as you usually do. 
  2. cut the shallots finely, then in a saucepan with the butter brown the shallots, 
  3. Add the lemon juice 
  4. tear / cut the salmon in small strips (I took out the salmon 30 min before the preparation is those he recommended on the package) and add it. 
  5. as soon as the salmon has taken another color, add the cream, pepper and pepper, cook 1 to 2 minutes 
  6. in a plate arrange the tagliatelle previously drained, (do not add fat) pour the sauce and sprinkle with dill 
  7. here it is ready, enjoy. 


