---
title: easy express brioche
date: '2016-12-20'
categories:
- boulange
- Brioches et viennoiseries
tags:
- To taste
- Boulange
- Orange
- Easy cooking
- Breakfast
- Pastry
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/12/brioche-express-facile-3.jpg
---
![brioche-express-easy-3](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/brioche-express-facile-3.jpg)

##  easy express brioche 

Hello everybody, 

Ah this brioche express easy, she made my happiness to taste the children during these first days of vacation! My kids love buns, spread with a little bit of [ Homemade Nutella ](<https://www.amourdecuisine.fr/article-nutella-fait-maison.html>) or from [ pasta with speculoos ](<https://www.amourdecuisine.fr/article-la-pate-a-speculoos-maison-hommage-a-micky-63142494.html>) for them it's the best taste! 

and frankly making a brioche every two days is a lot of work, hihihihi. But before yesterday when I was on my Facebook cooking group, I saw one of the girls "amoula amoula" post an express brioche recipe, I admit I was super seduced by the idea, and I had the Great desire to put myself ... 

I have prepared all my ingredients, but in her recipe, she uses oil, frankly I prefer butter buns with good taste! so I changed the butter while decreasing the amount (it was 100 ml of oil). His recipe contained only 1 tablespoon of sugar, in my opinion it's really little, so I added 50 gr, and the result was just sublime. 

The brioche is ready in me for an hour .... In any case, if you are not in a hurry, and you leave the brioche pushed a little before cooking, the result will be better. 

**easy express brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/brioche-express-facile-1.jpg)

portions:  12  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** for leaven: 

  * 1 glass of warm water (1 glass of 220 ml) 
  * 1 glass of flour 
  * 1 C. tablespoons sugar 
  * 1 tablespoon instant baking yeast 

for the brioche: 
  * the zest of an orange 
  * ¼ c. salt 
  * 50 gr of sugar 
  * 1 C. tablespoon milk powder 
  * 70 gr of melted butter 
  * 1 glass of flour 

to decorate: 
  * 1 yellow of water diluted in a little milk 
  * 1 C. vanilla coffee 
  * pearl sugar. 



**Realization steps** prepare the leaven: 

  1. in a large salad bowl, place the sugar, the baker's yeast and the flour 
  2. add warm water while stirring with a hand whisk. 
  3. cover the bowl with cling film and leave to rise for 10 minutes minimum. 

preparation of the brioche: (you can use the kneader at this stage) 
  1. add the zest of lemon, the sugar and the milk powder, stir to homogenize well. 
  2. introduce the half glass of flour. then add the salt. 
  3. now introducing the flour and butter by interposing and whipping. The dough is sticky. let it rest a little, then shape the brioche. 
  4. cut the dough in 8 parts, shape dumplings and put them in a cake mold. 
  5. let rise 20 minutes minimum (depending on the temperature around the brioche, the more it rises, and the better the result) 
  6. brush with the egg yolk mixture, milk and vanilla. 
  7. decorate with pearl sugar and bake in a preheated oven at 180 ° C for 20 minutes. 



![brioche-express-easy](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/brioche-express-facile.jpg)
