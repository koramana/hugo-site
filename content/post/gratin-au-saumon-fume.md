---
title: gratin with smoked salmon
date: '2015-07-04'
categories:
- Chhiwate Choumicha
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
tags:
- ramadan 2015
- Ramadan
- Ramadhan
- Express cuisine
- Tunisia

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gratin-au-saumon-1-001.jpg
---
[ ![salmon gratin 1-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gratin-au-saumon-1-001.jpg) ](<https://www.amourdecuisine.fr/article-gratin-au-saumon-fume.html/gratin-au-saumon-1-001>)

##  gratin with smoked salmon 

Hello everybody, 

Do you like gratins? And you like smoked salmon? So here is a delicious grain with smoked salmon that will please you ... A recipe super easy to make, but especially very delicious. 

It must be said that in the month of Ramadan, gratins are very popular, because it is rich and hearty dishes, which help you much to resist the long day of fasting. This recipe **Flower Dz** will please you and will be well appreciated by small and big, in addition it is very rich in taste, and flavors and food. But do not think that this recipe is just to realize during Ramadan, no ... It will find its place in our summer tables with just a fresh salad .... I'm already salivating, so have your friends for a delicious recipe. 

**gratin with smoked salmon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gratin-au-saumon-1-001.jpg)

portions:  6-8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 3 to 4 peppers of different colors grilled (green, red, yellow for me), 
  * A strong pepper (optional). 
  * Two small fried potatoes (do not over fries). 
  * 7 tablespoons finely chopped parsley. 
  * 200 g to 300 g smoked salmon cut into small pieces. 
  * 200 g grated cheese for stuffing and 200 g to garnish 
  * ½ teaspoon baking yeast. 
  * 4 eggs 
  * salt and pepper to taste and ½ teaspoon turmeric. 



**Realization steps**

  1. mix all your ingredients 
  2. Butter a baking tin and pour the gratin mixture into it 
  3. cover the surface generously with the cheese and bake at 180 degrees C for almost 30 minutes 



[ ![salmon gratin](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gratin-au-saumon.jpg) ](<https://www.amourdecuisine.fr/article-gratin-au-saumon-fume.html/gratin-au-saumon>)
