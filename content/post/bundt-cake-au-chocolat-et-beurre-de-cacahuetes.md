---
title: Bundt cake with chocolate and peanut butter
date: '2015-02-09'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- To taste
- Delicacies
- Cakes
- Algerian cakes
- desserts
- Fluffy cake
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/bundt-cake-au-chocolat-et-beurre-de-cacahuete.jpg
---
[ ![bundt cake with chocolate and peanut butter](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/bundt-cake-au-chocolat-et-beurre-de-cacahuete.jpg) ](<https://www.amourdecuisine.fr/article-bundt-cake-au-chocolat-et-beurre-de-cacahuetes.html/sony-dsc-269>)

##  Bundt cake with chocolate and peanut butter 

Hello everybody, 

A beautiful cake, this bundt cake with chocolate and peanut butter, I love the texture and the marriage of color, and not talking about both tastes: chocolate and peanut butter, a cake that I urge you to achieve. 

Thanks to Lunetoiles for this delicious recipe that will make you happy, accompanied by a good latte, or [ hot chocolate ](<https://www.amourdecuisine.fr/article-chocolat-chaud-au-nutella-hot-chocolat.html> "Nutella hot chocolate / hot chocolate") to taste it. 

This recipe reminds me in any case [ chocolate and orange cake ](<https://www.amourdecuisine.fr/article-cake-marbre-chocolat-orange.html>) , if you are tempted by the recipe. Otherwise if you just like the intense taste of chocolate, why not try my recipe for [ chocolate cake ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-facile.html> "chocolate cake") . 

[ ![bundt cake with chocolate and peanut butter 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Bundt-cake-chocolat-et-beurre-de-cacahuetes-2.jpg) ](<https://www.amourdecuisine.fr/article-bundt-cake-au-chocolat-et-beurre-de-cacahuetes.html/sony-dsc-271>)   


**Bundt cake with chocolate and peanut butter**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Bundt-cake-au-chocolat-et-beurre-de-cacahuetes-1.jpg)

portions:  8  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients** chocolate cake dough: 

  * 115 g dark chocolate, coarsely chopped 
  * 3 c. instant espresso powder + 1 cup hot water (or 1 cup of coffee) 
  * 2 tablespoons unsweetened cocoa powder 
  * 18 cl of milk 
  * 240 g of sweet butter, at room temperature 
  * 250 g caster sugar 
  * 3 large eggs 
  * 360 g flour 
  * 1 teaspoon of baking soda 
  * 1 pinch of sea salt 

Pastry cake with peanut butter: 
  * 60 g unsalted butter, at room temperature 
  * ¼ cup of creamy peanut butter 
  * 2 tbsp. tablespoon caster sugar 
  * 1 big egg, beaten 
  * 90 g of flour 
  * 60 ml of milk 
  * 1 pinch of sea salt 

Chocolate Icing: 
  * 115 g dark chocolate, coarsely chopped 
  * 60 g unsalted butter 
  * 1 tbsp. tablespoon of glucose 

Peanut butter icing: 
  * 2 tablespoons creamy peanut butter 
  * 2 - 3 tablespoons milk 
  * about 150 g icing sugar, sieved 
  * 1 pinch of salt 



**Realization steps** Chocolate cake: 

  1. Preheat oven to 180 degrees . generously butter a fireplace mold. 
  2. In a microwave-safe bowl, turn the dark chocolate down for 30 seconds and wait 30 seconds apart. Stir often. Repeat until the chocolate is melted. Put aside. 
  3. In a glass measuring cup, mix instant espresso powder and cocoa powder. Add enough hot water until the liquid mixture measures 1 cup. Stir until the powders are dissolved. Add the milk and let cool. 
  4. In the bowl of a stand mixer with a spatula, cream butter and sugar until light and fluffy. Add the eggs one at a time, making sure the egg is incorporated before adding the next. Rinse the sides of the bowl if necessary. 
  5. Add the melted chocolate slightly chilled. Continue mixing at low to medium speed until smooth. 
  6. In a medium bowl, combine the flour, baking soda and salt. With the mixer on low speed, add ⅓ of the milk mixture, then half of the flour mixture. Repeat, ending with the milk mixture. Mix until smooth. 

Peanut butter cake dough: 
  1. In a medium bowl, cream butter and peanut butter until smooth. Add the sugar and mix until smooth. Add the beaten egg. Mix until smooth. 
  2. Stir in flour and salt. Add the milk and mix. Mix until the flour is incorporated. 

Cake assembly: 
  1. Pour half of the chocolate paste into the savarin dish previously buttered. Distribute the peanut butter paste on top of the chocolate layer. With a knife, spread the peanut butter paste. Add the rest of the chocolate dough to cover the peanut butter cake dough. 
  2. Bake for 50 to 60 minutes, until a toothpick inserted into the cake comes out clean. Let cool in the mold for a few minutes before unmolding. Cool to room temperature on the rack. 

Chocolate Icing: 
  1. Melt the chocolate and butter together until smooth. 
  2. Incorporate glucose. Pour on the cooled bundt cake. 

icing peanut butter: 
  1. Mix together the sifted icing sugar, peanut butter, and salt. Add the milk and mix until smooth. Add extra milk if it is too dry. 
  2. If the icing is too liquid, add the extra sieved icing sugar. 
  3. Sprinkle with peanut butter icing on the iced chocolate cake. 



[ ![bundt cake with chocolate and peanut butter 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/bundt-cake-chocolat-et-beurre-de-cacahuetes-3-001.jpg) ](<https://www.amourdecuisine.fr/article-bundt-cake-au-chocolat-et-beurre-de-cacahuetes.html/sony-dsc-272>)
