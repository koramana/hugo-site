---
title: Easy strawberry and lemon cheesecake
date: '2017-02-24'
categories:
- cheesecakes et tiramisus
tags:
- Strawberry tart
- Pastry
- Cakes
- desserts
- Sweet pie
- Algerian cakes
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/strawberry-cheesecake_thumb1.jpg
---
##  Easy strawberry and lemon cheesecake 

Hello everybody, 

the **cheesecakes** become more and more a **[ dessert ](<https://www.amourdecuisine.fr/categorie-recettes-sucrees/dessert-crumbles-et-barres>) ** popular and very popular, there are cheesecakes without cooking, and cheesecakes "cooked" and here is the recipe for a tasty cheesecake, a recipe simple and easy, but a fondant to infinity. 

thanks to **Lunetoiles** who likes to share the **pics** and the recipe [ **delicious cheesecake** ](<https://www.amourdecuisine.fr/categorie-recettes-sucrees/cheesecakes-et-tiramisus>) with us, and here is his recipe:   


**Easy strawberry and lemon cheesecake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/strawberry-cheesecake_thumb1.jpg)

portions:  8  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients** Cookie pie background: 

  * 300 gr Small butter reduced to powder 
  * 90 gr of melted butter 

Cheesecake appliance: 
  * 1 can of sweetened condensed milk 
  * 400 gr of cream cheese (Philadelphia or other ...) (I used Ricotta) 
  * 2 tbsp. tablespoons sugar 
  * 175 ml of lemon juice (I put 100 ml) 
  * 3 Yellows and 2 eggs 
  * 1 tbsp. lemon zest 

topping: 
  * 2 to 3 cu. jelly strawberries 
  * strawberries 
  * white icing or melted white chocolate for decoration (optional) 



**Realization steps**

  1. Preheat the oven to 160 ° C. 
  2. Reduce small butter powder 
  3. mix them with the melted butter and garnish the bottom of a pan (26 cm) by going up generously on the edges 
  4. cool until use. 

Prepare the device to cheesecake: 
  1. mix all the ingredients with a whisk and pour the mixture over the pie shell. 
  2. Bake at 160 ° C for about 45 minutes, 
  3. remove the still shaking pie from the oven, so that it keeps its fudge as much as possible, 
  4. let it warm up. 

The topping: 
  1. heat 2 to 3 large spoons of strawberry jelly in a pan, coat the top of the pie, then decorate with halves of strawberries with white frosting. 



Thank you for your visits and your comments that I have not validated yet, just because I am super busy, I will try to do my best to validate and respond to your comments. 

bonne journée. 
