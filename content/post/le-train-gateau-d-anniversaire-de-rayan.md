---
title: the train, Rayan's birthday cake
date: '2009-08-13'
categories:
- pizzas / quiches / pies and sandwiches

---
the train, Rayan's birthday cake hello my dear friends, I have been absent very recently, but I hope to come back very regularly, especially with the approach of the holy month of Ramadan, full of recipes, and full ideas, and also do not forget my blog is yours, if you have beautiful recipes ramadan special, or if you have recipes, I would be happy to share it with others, just contact me on my email with the recipe, and one or a few photos, and it will be published immediately on & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.8  (  1  ratings)  0 

##  the train, Rayan's birthday cake 

Hi my dear friends, I have been absent a lot recently, but I hope to come back very regularly, especially with the approach of the holy month of Ramadan, with lots of recipes, lots of ideas, and also do not forget my blog is yours, if you have nice recipes Ramadan, or if you have recipes, I would be happy to share it with others, just contact me on my email with the recipe, and one or a few photos, and it will be published right away on the blog, thank you for all your sharing. 

now I go to the recipe for the cake that I had prepared for my son's birthday: 

the choice was not difficult, my son loves a cartoon here in England Thomas de thank Engine, so why not do that to him, 

here is the famous thomas, so a test of sculpture, hihihihihi rather successful. 

an easy cake, which is based on a cake that I already made, recipe [ right here ](<https://www.amourdecuisine.fr/article-33054404.html>)

but still, I rewrite it to you: 

3 eggs   
\- 200g of muscovado sugar (simple for me)   
\- 120g of soft butter   
\- 200g of flour   
\- 3 tablespoons of strong coffee of good quality (3 cases of hot water in which I dissolve 3 cac of nescafe)   
\- 3 tablespoons cocoa powder   
\- A pinch of salt   
\- A pinch of vanilla powder or a bag of vanilla sugar   
\- Two teaspoons, shaved, baking powder 

**Preparation:**

Preheat the oven to medium temperature. 

Start by whipping the sugar and butter with electric whisk, adding the coffee, and vanilla, continue beating for two minutes then add the eggs one by one while continuing to beat. 

In a bowl, mix together, flour, salt, yeast and cocoa powder. Add little by little to the first mixture and stir with a wooden spatula until a homogeneous mixture is obtained. Butter a cake mold, sprinkle with cocoa, shake the mold to remove the extra and pour the dough. Bake for 15 to 20 minutes, check the cooking with a knife, take out and let cool before unmolding 

cut into 4 equal parts, and shape one of it in the shape of a locomotive. 

icing: 

I doubled the proportion: 

180gr of chocolate   
100g of cream   
2 tablespoons of neutral oil 

melt chocolate with bain marie cream   
add the spoon of oil to make it shine, spread it on the cake with a spatula and pour it on the edges 

garnish the cake as you please, and amaze your children, 

bisous 
