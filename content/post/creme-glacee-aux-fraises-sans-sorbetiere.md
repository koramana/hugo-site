---
title: strawberry ice cream without sorbetiere
date: '2015-04-15'
categories:
- crème glacée, et sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/DSC05928.jpg
---
![DSC05928.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/DSC05928.jpg)

##  strawberry ice cream without sorbetiere 

Hello everybody, 

the sun begins to point these rays of good morning, and the summer tends these arms to remove this scarf around our necks which accompanied us during all this period of cold and almost interminable rain .... 

And to welcome the summer, there is no better than a beautiful homemade ice cream, so imagine an ice cream without ice cream maker, and full of fruity flavors ... "it sounds good, is not it? " 

I share with you today this sublime strawberry ice cream, without sorbetry and extra fast, which comes directly from the kitchen of Lunetoiles .... so serve before it melts ...   


**strawberry ice cream without sorbetiere**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/DSC05930.jpg)

portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients** (provided you have frozen fruit in the freezer ...) 

  * 500 g frozen strawberries by yourself 
  * 12 cl of vanilla soy milk or 1 soya yogurt, or 2 Swiss puffs, or 1 cow or sheep yogurt, or 200 g of well-drained cottage cheese 
  * 6 to 8 tablespoons of agave syrup (you can replace with honey or maple syrup) 
  * 1 tablespoon vanilla extract or powder 



**Realization steps**

  1. Method of preparation: 
  2. Put everything in the bowl of a mixer. 
  3. Start pulse mode with brief pulses, then mix frankly until you have a perfectly creamy consistency.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/creme-glacee-a-la-fraise-sans-sorbitiere-1--300x216.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/creme-glacee-a-la-fraise-sans-sorbitiere-1-.jpg>)
  4. Serve immediately or put in the freezer in a plastic tray but not more than 4 hours otherwise the ice loses its softness. 
  5. If you need to freeze the ice longer, take it out 20 to 30 minutes before eating. 


