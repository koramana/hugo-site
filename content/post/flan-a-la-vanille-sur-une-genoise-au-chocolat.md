---
title: vanilla flan on a chocolate sponge cake
date: '2015-12-03'
categories:
- dessert, crumbles and bars
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/flan-a-la-vanille-sur-une-genoise-au-chocolat.jpg
---
[ ![vanilla flan on a chocolate genoise](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/flan-a-la-vanille-sur-une-genoise-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/flan-a-la-vanille-sur-une-genoise-au-chocolat.jpg>)

##  vanilla flan on a chocolate sponge cake 

Hello everybody, 

When I saw these photos of my friend Samia Z, I thought she realized the cake on [ magic chocolate cake and caramel cream ](<https://www.amourdecuisine.fr/article-gateau-magique-au-chocolat-creme-caramel.html>) but Samia explained to me that she made a chocolate sponge cake, and that before she was completely cooked, she poured it over to the custard and handed over to cook again ... 

A beautiful cake that must be super delicious, and I really like the idea that it can be presented underneath ... A two-sided cake, hihihihi. 

[ ![vanilla flan on chocolate genoise](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/flan-vanill%C3%A9-sur-genoise-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/flan-vanill%C3%A9-sur-genoise-au-chocolat.jpg>)   


**vanilla flan on a chocolate sponge cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/flan-a-la-vanille-sur-genoise-au-chocolat.jpg)

portions:  10  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** sponge cake for a 24 cm mold: 

  * 4 eggs 
  * 120 g of sugar 
  * 90 g of flour 
  * 10 to 15 g of cocoa powder 
  * a pinch of himself and a yeast pinch 

Custard pastry: 
  * 500g of semi-skimmed milk 
  * 1 vanilla pods 
  * 1 combavas (replace with 1 lemons) 
  * 120 gr of cream 
  * 120 gr of sugar 
  * 100 gr of egg yolks 
  * 50 gr of cornstarch 
  * 60 gr of orange blossom 



**Realization steps**

  1. in the bowl of the robot, beat the eggs and sugar until it doubles in volume 
  2. sift the flour and cocoa and add salt and yeast using a spatula. 
  3. line up your mold with well-buttered baking paper 
  4. pour the dough into the pan and bake in a preheated oven at 180 degrees C. 
  5. do not let the sponge cake bake too much, because you have to pour the flan over and put it back in the oven. 

while the sponge cake is in the oven, prepare the flan: 
  1. Take the lemon peel 
  2. Boil milk and cream with vanilla and lemon peel, in a saucepan 
  3. Let infuse 15 min 
  4. Whisk together sugar, egg yolks and cornstarch 
  5. Add the infused milk to the mixture and mix well. 
  6. Put back in the saucepan and bring everything to a boil without stopping mixing until the cream thickens. 
  7. when your sponge cake is cooked (not too much) pour your Flan on it and put it all in the oven over medium heat, until the flan is cooked. 
  8. turn on the oven rack and let some color 
  9. remove the cake from the oven and allow to cool before removing from the oven 



[ ![vanilla flan and chocolate sponge cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/flan-vanill%C3%A9-et-sa-g%C3%A9noise-chocolat%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/flan-vanill%C3%A9-sur-genoise-au-chocolat.jpg>)
