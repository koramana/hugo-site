---
title: a super delicious Savarin
date: '2011-06-28'
categories:
- dessert, crumbles et barres
- Gateaux au chocolat
- cakes and cakes
- recettes sucrees

---
it's the first time that I make a savarin, and I try for a first the recipe of my friendNawel, verdict ..................... a delight to redo absolutely. and so we pass the recipe Nawel 2 eggs 250 gr flour 1 tablespoon baker's yeast 50 gr butter 2 pinches salt 50 gr sugar 25 ml milk & nbsp; Syrup: 350 ml of orange juice 3 tablespoons of orange blossom water 350 ml of water 180 gr of sugar Preparation: In a bowl, put the flour, sugar, salt, and yeast. 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

and so we pass Nawel's recipe 

  * 2 eggs 
  * 250 gr of flour 
  * 1 tablespoon baker's yeast 
  * 50 gr of butter 
  * 2 pinches of salt 
  * 50 gr of sugar 
  * 25 ml of milk 



Syrup: 

  * 350 ml orange juice 
  * 3 tablespoons of orange blossom water 
  * 350 ml of water 
  * 180 gr of sugar 



preparation: 

  * In a salad bowl, put the flour, sugar, salt, and yeast you have stirred in warm milk add the eggs. 
  * pick up the dough to make it homogeneous 
  * Add the melted butter the dough, cover the salad bowl with a clean cloth and leave to rest for 2 hours so that the dough ferments and rises. 
  * 2 hours later, butter a mold or small mussels put savarin dough in it. 
  * Let rise another hour. 
  * Preheat oven to 180 ° C, thermostat 6 and cook 
  * While savarin is cooling, prepare the syrup. 
  * In a saucepan, pour water, sugar and orange juice + orange blossom water bring to a boil and cook just until the sugar is dissolved. 
  * pour the syrup with a spoon on the savarin to soak it. 
  * before tasting, put the savarin on a dish and you decorate to your taste. 



a delice I recommend it. 

thank you for your visits and comments 

bonne journee 
