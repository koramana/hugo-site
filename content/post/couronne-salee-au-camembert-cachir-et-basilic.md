---
title: Crown salted with Camembert, cachir and basil
date: '2015-02-21'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- Aperitif
- Amuse bouche
- Cocktail dinner
- inputs
- Algeria
- Salty cake
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/cake-sale-038b_thumb1.jpg
---
##  Crown salted with Camembert, cachir and basil 

Hello everybody, 

I bring you this recipe that dates from 2011, a salty cake that I converted into a salty crown for lack of adequate size mold. 

What's great with the salty cakes is that once you have the base of the cake, you can put everything you want in it, it's like the [ olives cake ](<https://www.amourdecuisine.fr/article-cake-aux-olives.html> "olives cake") which I urge you, or the [ pepper cake ](<https://www.amourdecuisine.fr/article-cake-aux-poivrons-et-camembert.html> "Pepper and camembert cake") . 

for this salty ring with camembert, kosher and basil, no need to take out your drummer, or your mixer, a wooden spoon will do the trick (well I was pressed me so I did it with a spoon, but with a manual whip, surely it will be even more airy), anyway, it was super light. 

**Crown salted with Camembert, cachir and basil**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/cake-sale-038b_thumb1.jpg)

portions:  6  Prep time:  10 mins  cooking:  45 mins  total:  55 mins 

**Ingredients**

  * 3 eggs 
  * 200 g flour 
  * 1 sachet of baking powder 
  * 80 g grated Gruyère cheese 
  * 100 ml of table oil 
  * 100 ml of milk 
  * 90 gr of Cachir 
  * 1 tablespoon of basil 
  * 50 gr of camembert 
  * 80 gr of pitted green olives 



**Realization steps**

  1. Method of preparation: 
  2. Preheat your oven to 180 ° C. 
  3. In a bowl, pour the flour and yeast. 
  4. Dig a hole in the middle. break the eggs and start mixing slowly. 
  5. add milk and oil gradually. 
  6. Then add the grated Gruyere cheese, the cube-cut hides, the olives, the basil. 
  7. Mix and add the cut camembert in large pieces. 
  8. Pour everything into a buttered and floured cake mold, or lined with baking paper 
  9. place in the oven for 45 to 50 minutes. 



this cake was too good, it must absolutely drip to judge. 

thank you for all your visits. 

thank you for all your comments 

bonne journée 
