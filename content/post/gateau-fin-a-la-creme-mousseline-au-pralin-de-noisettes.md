---
title: fine cream cake with hazelnut praline muslin
date: '2014-07-30'
categories:
- birthday cake, party, celebrations

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/biscuit-fin-a-la-creme-mousseline-a-la-pralinoise-300x225.jpg
---
fine cream cake with hazelnut praline muslin 

Hello everybody, 

here is the birthday cake I made for my little prince, a cake made from 3 layers of fine biscuit (a kind of very fine sponge cake), covered with a very delicious layer of [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline-108846274.html>) scented [ praline with hazelnuts ](<https://www.amourdecuisine.fr/article-pralin-aux-noisettes-108845174.html>) and decorate to please children, with a drawing of Sonic, made with the "windows color" method 

at first, I was going to ask her to make me the cake of Rayan, but it's Ramadan, and also, she was well overloaded, I had fun making this cake for my son, I did not finish only at 3 o'clock in the morning ... .. 

so we start with the base of the cake:   


**thin cream cake with hazelnut praline**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/biscuit-fin-a-la-creme-mousseline-a-la-pralinoise-300x225.jpg)

portions:  20  Prep time:  60 mins  cooking:  60 mins  total:  2 hours 

**Ingredients** fine cake: for each biscuit (23 X 32 cm) 

  * 2 eggs 
  * 60 gr of sugar 
  * 60 gr of flour 
  * 1 pinch of baking powder   
I made 3 cookies, so I used 6 eggs in total 

syrup: 
  * 200 gr of sugar 
  * 300 ml of water 
  * vanilla 



**Realization steps** first prepare the syrup, 

  1. mix all the ingredients, 
  2. and place on medium heat, at the first boil extinguish the fire, and let cool. 

Prepare the cake 
  1. beat the eggs thoroughly with the sugar until the mixture is white and very airy. 
  2. add the flour and baking powder, cook in a hot oven, for 15 to 20 minutes (depending on the capacity of your oven) 



for decoration: 

  * [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline-108846274.html>)
  * [ praline with hazelnuts ](<https://www.amourdecuisine.fr/article-pralin-aux-noisettes-108845174.html>)



method, 

  1. add the hazelnut praline to the muslin cream while whisking. 
  2. place the first layer of fine biscuit, and soak it a little (not too much) with the prepared syrup. 
  3. pour over one-third of the hazelnut praline muslin cream, and cover the cake well. 
  4. place the second layer of biscuit on top, soak generously with the syrup, and cover with another third of the muslin cream with hazelnut praline. 
  5. place the last layer of the cake, soak it once more generously with the syrup, and cover the whole cake with the rest of the muslin cream. 
  6. place in the fridge. 



preparation of Sonic's drawing: 

  * 1 tablet of 200 gr of white chocolate 
  * dyes: blue, yellow, and chocolate special red 
  * 2 dark chocolate squares 



method of realization: 

  1. print the drawing you want to make 
  2. place the sheet in a clear plastic pouch. 
  3. melt the dark chocolate, and place it in a disposable pastry bag, or a cone that you have made with the baking paper, and draw the outline of the drawing. 
  4. let the chocolate harden 
  5. Melt the white chocolate in small quantities, and color it according to your needs, and place it in a disposable pastry bag, and fill in the coloring space. 
  6. let all the chocolate harden, then reverse the design on the cake, and remove the plastic pouch 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
