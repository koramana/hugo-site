---
title: trida
date: '2012-12-01'
categories:
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/trida-pour-moharem-011_1.jpg
---
Hello everybody, 

a [ Algerian recipe ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , of the **cooked** from the east **Algerian** , also known as: **Mkartfa** , usually cooked in white sauce. 

it is a kind of very fine dough cut into small squares, which are prepared well in advance, and dried like couscous. 

the **pics** are old, as I redo the recipe, I will share them with you.   


**trida**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/trida-pour-moharem-011_1.jpg)

Recipe type:  Algerian dish  portions:  4  Prep time:  40 mins  cooking:  45 mins  total:  1 hour 25 mins 

**Ingredients**

  * square pasta 
  * chicken (thighs) 
  * onion 
  * 3 cases of olive oil 
  * 1 case and ½ canned tomato 
  * 1 handle of chickpea 
  * 2 cloves garlic 
  * salt, pepper, paprika, ras el hanout 



**Realization steps**

  1. let's start by preparing the sauce, mixing the onion and the garlic, put in the bottom of a couscous pot, adding the oil, the chicken, the canned tomato and the spices, 
  2. let it simmer a little, and then cover well with the water. and add the chickpeas. 
  3. in the meantime, we go to our trida,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215438671.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215438671.jpg>)
  4. we put the quantity we want to prepare in a terrine, we add a case of oil, we rub in hands. during this time the sauce should start boiling.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215438911.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215438911.jpg>)
  5. so we put our edges in the top of a couscoussier, and place it on the bottom of our couscoussier. 
  6. when the steam begins to escape between the edges, take the couscous and pour into a bowl and sprinkle with the sauce, and try to get the sauce in and try to separate the small squares from other.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215439301.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215439301.jpg>)
  7. and then put the couscoussier again on the top and redo this action 2 to 3 times until the edges become soft. 
  8. we remove our edges and add a little butter. 
  9. before serving, put our edges in a thick pot, and sprinkle with sauce and allow to absorb well over low heat.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215439301.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215439301.jpg>)
  10. serve well garnished with eggs, chickpeas and chicken. 



Bon appetit 
