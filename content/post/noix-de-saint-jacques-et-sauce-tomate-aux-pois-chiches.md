---
title: Scallops and tomato sauce with chick peas
date: '2015-12-17'
categories:
- diverse cuisine
- Dishes and salty recipes
tags:
- Sea food
- dishes
- Easy cooking
- accompaniment
- inputs
- Hot Entry
- Shellfish

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/noix-de-saint-jacques-sur-sauce-tomate-aux-poischiches.jpg
---
[ ![scallop nuts on tomato sauce with pea](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/noix-de-saint-jacques-sur-sauce-tomate-aux-poischiches.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/noix-de-saint-jacques-et-sauce-tomate-aux-poischiches-1.jpg>)

##  Scallops and tomato sauce with chick peas 

Hello everybody, 

Last week, I was doing my homework quietly while my husband took the kid out with him, the TV was shut down and the computer, something I like to do to work quickly and efficiently. But by dusting the TV, it turned on my favorite tv channel, foodnetwork ... I was going to turn it off, the dish presented has caught my attention ... A chickpea tomato sauce presented with lots of seafood grilled on the barbecue. 

**Scallops and tomato sauce with chick peas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/noix-de-saint-jacques-et-sauce-tomate-aux-poischiches-1.jpg)

portions:  4  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients**

  * 3 c. olive oil (1 tablespoon for onions and 2 to sauté scallop nuts) 
  * 1 medium onion, chopped 
  * 1 C. chili (red pepper, yes it's a recipe from Latin America, so expect it to be super hot) 
  * 1 tablespoon of tomato paste. 
  * 1 clove garlic minced 
  * ½ cup of dry white wine (I did not put any) 
  * 1 can of precooked chickpeas drained 
  * 500 gr tomatoes peeled and cut into small cubes 
  * Salt and freshly ground black pepper 
  * 12 giant scallops (St. Jacques nuts) 
  * thyme. 



**Realization steps**

  1. In a saucepan, heat 1 tablespoon olive oil over medium heat. 
  2. Add the onion and cook, stirring, until the onion caramelises slightly, about 12 minutes. 
  3. Add chili, tomato paste and garlic and cook, stirring for 2 minutes. 
  4. Add the wine (I just put the water and it's too good with) and cook, stirring until the sauce is reduced, 8 - 10 minutes. 
  5. Stir in the chickpeas, tomatoes, and ½ cup of water and bring to a boil. 
  6. Reduce the heat and allow to continue cooking, stirring occasionally. 
  7. Meanwhile, in a large skillet, heat remaining 2 tablespoons remaining olive oil on high heat until the stove starts to smoke. 
  8. Season the scallops with salt and pepper, then place in the pan and cook, turning once, until just cooked, about 1 minute per side. 
  9. Transfer the scallops to a plate and serve with the tomato and chickpeas sauce 



[ ![scallop nuts on tomato sauce with chick peas.](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/noix-de-saint-Jacques-sur-sauce-tomate-aux-poischiches..jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/noix-de-saint-jacques-et-sauce-tomate-aux-poischiches-1.jpg>)
