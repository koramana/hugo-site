---
title: halwat ezhar algerian cake of the feast of the aid
date: '2014-08-06'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/08/gateau-algerien-halwat-ezahar.CR2_.jpg
---
[ ![algerian cake halwat ezahar.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/gateau-algerien-halwat-ezahar.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/gateau-algerien-halwat-ezahar.CR2_.jpg>)

##  halwat ezhar algerian cake of the feast of the aid 

Hello everybody, 

Halwat ezhar, or lucky cake, I do not know why the creator of this Algerian cake with honey, give it that name? but it remains that this cake is just a delight, it is very beautiful, super easy to achieve, and especially very rich in taste. 

I saw this cake on the TV channel, Samira TV, but sorry if I did not remember the name of the pastry chef who made it. In any case, as soon as the recipe is on TV, I immediately write the ingredients, and I remember in mind the method of making the cake, which is super easy, and I just waited opportunity to make this Algerian cake. 

You can make a version with peanuts for a taste even more pronounced and tasty (for my part I really like peanut-based cakes), as you can make a mixture of dried fruits, it's only going to be better. more   


**halwat ezhar algerian cake of the feast of the aid**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/halwat-ezhar-gateau-algerien-de-laid-el-fitr.CR2_.jpg)

portions:  20  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** dough: 

  * 4 measure of flour, 
  * a measured smen melted and cooled, 
  * 1 coffee vanilla, 
  * water and water of orange blossom 

the joke: 
  * 3 measures of almonds with the skin 
  * 1 measure of sugar 
  * 1 teaspoon of almond extract 
  * eggs 

decoration: 
  * honey 
  * silver beads 



**Realization steps** prepare the stuffing: 

  1. mix almonds, sugar and almond extract 
  2. beat an egg and pick up the dough, if necessary add another egg, very slowly, until you have a stuffing that picks up. 

prepare the pasta: 
  1. sand the flour with the smen melted and cooled, in your hands 
  2. add the vanilla, then pick up the dough with a mixture of water and orange blossom water 
  3. prepare the cake: 
  4. spread the dough with a baking roll, or the dough machine to have a wide rectangle is fine. 
  5. form a pudding with the stuffing the size of the dough in length, and almost 1cm and a half thick. 
  6. roll the dough on the stuffing to cover it well, roll it in two. 
  7. paste the end with beaten egg white 
  8. using a square shortbread pan of about 10 cm on the side, trace cakes.   
it is necessary to see the video, otherwise it is necessary to put the mold on the roll so that the angles of the mold are the part which cuts the piece of cake.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/zhar.bmp-300x203.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/zhar.bmp-300x203.jpg>)
  9. take the small cut pudding, and cut in half width on both sides without getting in the middle of the cake   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/zhar1.bmp-300x184.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/zhar1.bmp-300x184.jpg>)
  10. join the two opposites, sticking them with egg white   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/zhar3.bmp-300x185.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/zhar3.bmp-300x185.jpg>)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/zhar3.bmp-001-300x214.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/zhar3.bmp-001-300x214.jpg>)
  11. towards the end, decorate the cakes of a flower cut from the remaining dough. 
  12. let it dry all night in the open air 
  13. cook in a preheated oven at 180 degrees C for about 20 minutes, or until the cake is cooked. 
  14. from the oven, dip the cakes in honey perfumed with orange blossom water, and decorate the center of the flower with a silver pearl. 



{{< youtube d96faJpx7Tw >}} 

[ ![halwat ezhar.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/halwat-ezhar.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/halwat-ezhar.CR2_.jpg>)
