---
title: Baked snapper, baked goldfish recipe
date: '2018-04-16'
categories:
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/01/pic-038_thumb1.jpg
---
![Baked snapper, baked goldfish recipe](https://www.amourdecuisine.fr/wp-content/uploads/2010/01/pic-038_thumb1.jpg)

#  Baked snapper, baked goldfish recipe 

Hello everybody, 

I bring you a recipe from 2008! ah the archives! Baked snapper, baked goldfish recipe I dig up this recipe from the archives of my blog, because the first time I did it, I did not know the name of this fish in French, after research I knew that it's called snapper, which I know here in England as Sniper fish (but if you think it's not the right name, correct me !!!) 

my husband bought me this "sniper fish" snapper, frozen, the day before I cleaned it well, and well marinated, with: 

![Baked snapper, baked goldfish recipe](https://www.amourdecuisine.fr/wp-content/uploads/2010/01/pic-039_thumb1.jpg) ![Baked snapper, baked goldfish recipe](https://www.amourdecuisine.fr/wp-content/uploads/2010/01/pic-035_thumb1.jpg)

**Baked snapper, baked goldfish recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/01/pic-038_thumb.jpg)

**Ingredients**

  * salt 
  * black pepper 
  * of salmon 
  * ginger 
  * Garlic 
  * thyme 



**Realization steps**

  1. so let everything marinate at least 4 hours (for me it was a night in the fridge) 
  2. take a baking dish, 
  3. place potatoes peeled, rinsed and cut into slices (3 potatoes for me) 
  4. slice over a long cut onion, crush over 2 garlic cloves, salt, pepper and spice according to your taste, add bay leaves, and a little thyme, beware of salt. 
  5. put on the fish (2 fish for me). 
  6. pour over the marinade. 
  7. cover with aluminum foil, and place in a hot oven, until cooking fish, remove the aluminum foil, and let brown a little. 



![Baked snapper, baked goldfish recipe](https://www.amourdecuisine.fr/wp-content/uploads/2010/01/pic-039_thumb_11.jpg) ![Baked snapper, baked goldfish recipe](https://www.amourdecuisine.fr/wp-content/uploads/2010/01/pic-037_thumb1.jpg)

bonne dégustation. 
