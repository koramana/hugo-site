---
title: meringue nests with mascarpone and strawberries
date: '2016-03-20'
categories:
- dessert, crumbles and bars
- basic pastry recipes
- sweet recipes
tags:
- Pastry
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/nids-de-meringue-a-la-creme-aux-fraises-3.jpg
---
[ ![meringue nests with mascarpone and strawberries 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/nids-de-meringue-a-la-creme-aux-fraises-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/nids-de-meringue-a-la-creme-aux-fraises-3.jpg>)

##  meringue nests with mascarpone and strawberries 

Hello everybody, 

Do you remember the story of my neighbor who asked me to make little cakes for Valentine's Day so that she could give them to her children? I had made him [ thousand strawberry leaves ](<https://www.amourdecuisine.fr/article-mille-feuilles-aux-fraises-et-mascarpone.html>) with the roll of puff pastry, and I made them those pretty little meringue nests that I burst with a mascarpone cream and strawberries on top for a little color. 

I completely forgot this recipe in the archives of my pc, it was only yesterday that a friend to whom I had shown the photos, came to ask me the recipe. She wants to make this recipe for the party at her children's school. It's nest of meringues are crisp and melting at will. Mascarpone cream and whole strawberries add an endless flavor. 

For my children had loved these little delights I had made for the neighbor, they had asked me. It was sublime to play with the colors and creams to fill the nests, we loved it.   


**meringue nests with mascarpone and strawberries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/nids-de-meringue-a-la-creme-aux-fraises-1.jpg)

portions:  20  Prep time:  30 mins  cooking:  120 mins  total:  2 hours 30 mins 

**Ingredients** for meringue nests 

  * 3 big egg whites 
  * 1 packet of strawberry jelly (powdered) 
  * ¼ glass of powdered sugar (glass = 240 ml) 
  * 1 C. powdered vanilla 
  * 2 gr of dark chocolate, finely chopped 
  * mascarpone cream: 
  * ½ cup of liquid cream 
  * ¼ cup of mascarpone 
  * ¼ cup of strawberry jam 
  * ½ teaspoon of vanilla extract 

decoration: 
  * 10 fresh strawberries, to garnish 
  * 10 springs of fresh mint, to garnish (optional) 



**Realization steps** To prepare the meringue nests: 

  1. Preheat the oven to 200 ° C. Line 2 baking sheets with baking paper. 
  2. Place the egg whites in the mixer bowl, beat on medium speed until frothy. 
  3. Pour the strawberry jelly powder and strawberry sugar over the egg whites while whisking but at low speed.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/nids-de-meringue.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/nids-de-meringue.jpg>)
  4. add the vanilla powder and continue beating until the egg whites are shiny, about 5 minutes. 
  5. place the meringue in a pastry bag with a star shaped casing and shape the nest of meringue. Start from the middle of a tight circle, make 3 turns to form the base. Then continue to poach on the perimeter of the base for 2 full circles to form the edges of the cup.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/fa%C3%A7onnage-meringue.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/fa%C3%A7onnage-meringue.jpg>)
  6. lower the oven temperature to 100 ° C and cook the meringues until they are firm, about 2 hours. Then turn off the oven and leave them there overnight with the oven door closed. The heat will continue to dry the meringue. 

To prepare the toppings with strawberry cream: 
  1. Place the cream, mascarpone cheese, strawberry jam and vanilla extract in the mixer bowl. whip until light and fluffy. Transfer in a sleeve pocket. 
  2. Refrigerate until use. 
  3. To assemble: 
  4. In a microwavable heatproof bowl, place ¾ chopped chocolate and melt on high heat. Stir the chocolate every 20 seconds apart until the chocolate is melted. 
  5. add the remaining ¼ chocolate and stir until completely melted. let cool before using. 
  6. Pour a teaspoon of melted chocolate into the meringue nest. With the back of a teaspoon, gently spread the chocolate in the meringue nest for a thin even layer. Set aside until the chocolate is firm.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/creme-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/creme-aux-fraises.jpg>)
  7. When serving, place a generous amount of strawberry cream in the meringue nest. 
  8. Cut the strawberry in half and garnish the top with the strawberry cream. Add some fresh mint leaves for a splash of color 



[ ![meringue nests with mascarpone and strawberries 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/nids-de-meringue-a-la-creme-aux-fraises-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/nids-de-meringue-a-la-creme-aux-fraises-4.jpg>)
