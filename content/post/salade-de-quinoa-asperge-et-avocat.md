---
title: asparagus and avocado quinoa salad
date: '2013-12-23'
categories:
- Buns and pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/salade-de-quoina-avocat-asperge.CR2_.jpg
---
[ ![asparagus salad of quoina avocado.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/salade-de-quoina-avocat-asperge.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/salade-de-quoina-avocat-asperge.CR2_.jpg>)

Hello everybody, 

Back with another quino recipe. Yes, we can always associate pleasure with what is benign for health, right? This quinoa-based salad, asparagus, avocado and a little peas is just a delight, with the crunch of quinoa and asparagus, the fondant of avocado and peas, and the slightly acidic taste of the vinaigrette-based lemon juice freshly squeezed, huuum, who wants to join me at the table !!! ...    


**asparagus and avocado quinoa salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/salade-quinoa-avocat-et-asperges.jpg)

Recipe type:  salad entrance  portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 1 glass of quinoa 
  * 2 glasses of water 
  * 7 stalks of asparagus 
  * 1 lawyer 
  * ½ glass of peas 
  * some fresh basil leaves. 
  * salt and black pepper 
  * squeezed lemon juice (quantity according to taste) 
  * olive oil 
  * 1 clove of garlic 



**Realization steps**

  1. start by boiling the quinoas with the water a little salty, at first boil the mixture well, then reduce the fire, and let well inflate and cook the quinoas, stirring a few times. 
  2. cook until the quinoas absorb all the water. 
  3. clean the asparagus, you can peel them if they are a little hard, and cut them into small sticks of almost 2 cm long. 
  4. fry in a skillet a little oiled. 
  5. add over the peas, let the whole thing chisel, and set aside. 
  6. in a salad bowl, place the quinoas well cooked and cooled. 
  7. top with asparagus and peas. 
  8. add the chopped basil and avocado cut into small cubes. 
  9. Prepare the viengrette by whisking together the lemon juice, salt, black pepper, olive oil and crushed garlic. 
  10. for a fine touch you can add a little honey ... 
  11. mix this vanilla with quinoa, asparagus, avocado and peas. 
  12. let it all macerate together a good thirty minutes cool. 



[ ![avocado salad with quinoa and asparagus](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/salade-davocat-quinoa-et-asperge.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/salade-davocat-quinoa-et-asperge.jpg>)

You can present this salad in verrines, I do not tell you how much it will please your guests. 

for my part I presented it with grilled shrimp skewers with sesame seeds, we enjoyed a lot at home. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
