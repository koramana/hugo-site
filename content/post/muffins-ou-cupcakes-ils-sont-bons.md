---
title: Muffins or cupcakes ....... they are good!!!
date: '2010-02-05'
categories:
- Algerian cuisine
- ramadan recipe

---
so the recipe should be muffins, but for my kids it became cupcakes 

a real delight, that the children devoured. 

For 16 to 18 muffins: 

\- 280g of flour 

\- 1 packet of dry yeast 

\- 100 g of sugar 

\- 1 pinch of salt 

\- 25 cl of milk 

\- 2 eggs 

\- 75g of melted butter 

\- 50g grated coconut 

\- frozen fruits 

  
In a salad bowl, mix all the dry ingredients, flour, yeast, sugar, salt, coconut. In another bowl, mix the eggs with the milk and the melted butter. Assemble the 2 preparations and put in the muffin cups. Bake at 180 ° C for about 15 minutes.   
For the icing, it was with white chocolate   
you can put them in an airtight box (I did not have the chance to do it). 
