---
title: Financial blueberry heart
date: '2013-09-10'
categories:
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/financiers-myrtilles_thumb_11.jpg
---
Hello everybody, 

Today this beautiful recipe comes from Lunetoiles, you know, she loves cooking, and I do not tell you the stock of her recipes that I have in my email, super super talented. 

This recipe for blueberry financiers, Lunetoiles did it by putting a little jam in the center, and without throwing it, at the exit of the oven, she had hearts in these financiers, hihihihih, so do not tell me I tried the recipe, and I could not have the hearts ...   


**Financial blueberry heart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/financiers-myrtilles_thumb_11.jpg)

Recipe type:  cupcakes, taste  portions:  12  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients**

  * 70ml of oil or 70g of butter 
  * 50g of white almond powder (without skin) 
  * 85g icing sugar 
  * 25g of flour 
  * 2 egg whites 
  * bilberry jelly 



**Realization steps**

  1. In a salad bowl, mix the almond powder with the icing sugar and the flour. 
  2. Pour the whites then the melted and slightly cooled butter, stirring constantly while turning gently until a homogeneous mixture is obtained. 
  3. Fill the muffin cups or special muffin cups to ⅔ of the height and 
  4. add a teaspoon of blueberry jelly to each one. 
  5. Bake in a preheated oven at 150 ° C for 25 minutes. 
  6. Check the cooking by planting a toothpick in a cake, if it comes out clean and dry is that the cakes are cooked. 
  7. Let cool before unmolding. 



thank you for your visits and comments 

bonne journee, meme si le mauvais temps est de retour 
