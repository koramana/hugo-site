---
title: Monkfish tajine with chermoula
date: '2016-08-12'
categories:
- Algerian cuisine
- Moroccan cuisine
- recette de ramadan
- fish and seafood recipes
tags:
- Candied lemons
- Algeria
- Full Dish
- dishes
- Easy cooking
- Healthy cuisine
- Morocco

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tajine-de-lotte-a-la-chermoula.jpg
---
[ ![tajine of monkfish has the charmoula](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tajine-de-lotte-a-la-chermoula.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-lotte-la-chermoula.html/tajine-de-lotte-a-la-chermoula>)

##  Monkfish tajine with chermoula 

Hello everybody, 

**Monkfish tajine with chermoula** Like you, I'm a big fan of tajines, and today it's a tajine monkfish chermoula that I share with you. It is a tagine great tasty and we will not stop at a single bite, believe me! 

This **Moroccan tajine from monkfish to chermoula** is a recipe from my friend **Nawel** who lives in Ireland, she shares nicely with us today this delicious recipe, and this beautiful photo that makes salivate well. 

If you like fish recipes, you can take a look at the category: [ recipe based on fish and seafood. ](<https://www.amourdecuisine.fr/recettes-aux-poissons-et-fruits-de-mer>)

If you, too, love tajines, do not forget to share your recipes with us. Thank you.   


**Monkfish tajine with chermoula**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tajine-de-lotte-a-la-chermoula.jpg)

portions:  4-5  Prep time:  30 mins  cooking:  60 mins  total:  1 hour 30 mins 

**Ingredients**

  * 700 gr of monkfish cut into cubes 
  * 4 tomatoes grated, 
  * 4 carrots, 
  * 4 potatoes, 
  * 1 red pepper, 1 yellow, 1 orange, 
  * fresh lemon (but candied lemon is even better) 
  * kalamata olives (purple olives) for decoration 

for chermoula we will need: 
  * 3 cloves garlic grated 
  * A bunch of chopped parsley 
  * 1 tablespoon of tomato paste 
  * Black pepper, cumin, ginger, turmeric, salt 
  * a fish-flavored knorr cube, 
  * a little olive oil, 
  * lemon juice 



**Realization steps**

  1. Start by preparing the chermoula 
  2. put the chopped monkfish pieces in for 1 hour or more. 
  3. You can cook in a tajine, or a pot with a thick bottom. 
  4. put some olive oil in the bottom of the tajine, 
  5. add the grated tomatoes, over the carrots cut into sticks and the potato cut into slices, between each layer put a drizzle of olive oil 
  6. now prepare the pieces of monkfish 
  7. garnish with peppers cut into strips, lemon slice and then olives 
  8. finally add the rest of the diluted chermoula with a soup of water and cook slowly. 


