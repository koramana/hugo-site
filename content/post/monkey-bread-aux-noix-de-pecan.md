---
title: Monkey bread with pecan nuts
date: '2015-01-19'
categories:
- Buns and pastries
- Mina el forn
tags:
- Boulange
- buns
- Cinnamon
- shaping
- Bread
- Pastries
- Picnic

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/monkey-bread-2-a.jpg
---
[ ![monkey bread with pecans](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/monkey-bread-2-a.jpg) ](<https://www.amourdecuisine.fr/article-monkey-bread-aux-noix-de-pecan.html/sony-dsc-259>)

##  Monkey bread with pecan nuts 

Hello everybody, 

I'm in brioche mode these days, and it's the monkey bread: A pecan monkey bread. A super delicious brioche, different in presentation of other buns, but it remains a small enigma side appointment ... Monkey bread which means monkey bread .... 

what is good with this brioche is that you do not need to cut with a knife, so it is ideal for picnic outings ... everyone takes a small ball that is easily detached from the rest of the brioche, a little ball a bit sticky because of caramel poured over, and super scented with cinnamon ... 

Having to eat this monkey bread several times, I assure you, it's a brioche that is worth seeing in the kitchen to realize it ... In any case, we thank Lunetoiles for this beautiful recipe, and these pretty pictures, and we go in the planet of the monkeys to taste this delight, hihihihih 

**Monkey bread with pecan nuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/monkey-bread-1a.jpg)

portions:  8-10  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients** for the dough: 

  * 180g warm milk 
  * 40g of melted butter but not hot! just lukewarm 
  * 30g of sugar 
  * 375g of flour 
  * 1 teaspoon of salt 
  * 10g baker's yeast or 1 sachet dry yeast such as Briochin 
  * 1 beaten egg 

for the syrup: 
  * 160g of brown sugar 
  * 60g of butter 
  * 40g of liquid cream at 30% fat 
  * 100g of pecans or other nuts 

for coating the balls: 
  * 250g of brown sugar 
  * 1 teaspoon cinnamon 
  * 150g of melted butter 



**Realization steps**

  1. Heat the milk. In a cup put 3 tablespoons warm milk and dilute the yeast with 1 teaspoon of sugar, and let stand 15 minutes. 
  2. In the bowl of your robot, put the flour, salt and sugar. 
  3. Mix. Add the liquids (beaten egg / butter / milk / yeast). 
  4. Knead until smooth and homogeneous (about 10 minutes at medium speed) 
  5. Let rise for 1h30 in a warm place away from drafts lou until the dough has doubled in volume. 
  6. Spend this time, prepare the syrup. 
  7. In a saucepan, place all the ingredients of the syrup (without the nuts) and bring to a boil over a low heat. 
  8. Pour half of this syrup in a mold and half of the nuts (put pretty good whole, because when you unmould they will be at the top of the brioche, the rest of the nuts chop them coarsely). 
  9. Take the dough and divide into small balls of 20 to 22 g each. 
  10. Pass them in the melted butter and coat them in the cinnamon sugar brown sugar. 
  11. When half the balls are in place in the mold, heat the remaining syrup to liquefy and pour on the balls. Sprinkle with remaining nuts. 
  12. Finish with the remaining balls. Repeat the operation. 
  13. Allow to double in volume for about 30 minutes to 1 hour. 
  14. Meanwhile, preheat the oven to 180 ° C. 
  15. When the time of the second lift has elapsed, bake for about 30 minutes. 
  16. On leaving the oven, wait 5 minutes before turning out. 



[ ![monkey bread with pecans](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/monkey-bread-a.jpg) ](<https://www.amourdecuisine.fr/article-monkey-bread-aux-noix-de-pecan.html/sony-dsc-260>)
