---
title: khobz with onions, onion bread
date: '2013-07-01'
categories:
- diverse cuisine
- Cuisine by country
- Healthy cuisine
- recipes at minus 300 Kcal
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/focaccia-004_thumb-300x224.jpg
---
##  khobz with onions, onion bread 

Hello everyone, 

You want a bread recipe rich in flavor, easy to make, here is a well flavored Focaccia Rosemary and onion. 

Imagine that to do it you only need your blinder ???? 

Beautiful, no bread machine, no mess, or even in the hand, in 5 minutes my pasta already rested and I do not tell you how this beautiful Italian bread well watered rosemary flavored olive oil and onion is a delice, huuuuuuuuuuuum, you will tell me, and I do not tell you the pretty crumb and the beautiful taste. 

so without delay I pass you the ingredients (I put half):   


**khobz with onions, onion bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/focaccia-004_thumb-300x224.jpg)

portions:  6  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients**

  * 1 sachet of instant yeast (I do not know and despite that I put everything in half, but I use a bag of instant yeast) 
  * 400 ml warm water 
  * 1 C. a sugar coffee 
  * 2 and a half c. a coffee of salt 
  * 60 ml of olive oil 
  * 700 gr of flour (more at least if the flour absorbs well or not) 

for garnish: 
  * 2 tbsp. soup of olive oil 
  * ¼ onion cut into strips 
  * a little fresh rosemary (or dry, which I put) 



**Realization steps**

  1. in the bowl of the blender, place the water sugar and yeast, let act 5 minutes at the max, add oil, salt, and ¾ of the amount of the flour, and shield gently, until that the dough is formed (not having to put all the flour, add the flour as needed, until you get a nice dough, not too sticky.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/focaccia-romarin-oignon-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/focaccia-romarin-oignon-1_thumb1.jpg>)
  2. place your dough in a well-oiled mold, and spread out your dough evenly. 
  3. Cover the dough with a clean cloth and place it in a hot but off oven, until it doubles in volume (in the video the girl says 1h to 1h30, for me and despite the cold in 20 min, already it does well to swell, hihihiih I do not know is it yeast, or the oven that was hot, hihihiih) 
  4. remove your tray, turn on the oven 180 degrees time to garnish the Focaccia 



with the tip of your finger, leave prints on your bread. 

in a bowl, mix the olive oil, rosemary and onion, then with a brush and without breaking the dough that has risen, try to brush the entire surface of your bread. 

then decorate with the rosemary soaked in oil, as well as onion, sprinkle after the focaccai with a little bit of coarse salt. (I had only a fine salt, so I sprinkled the surface with a pinch of salt. 

and cook for 20 to 25 minutes, or depending on your oven. 

enjoy this marvel, I swear it is soft, very tender, with a crispy layer, and especially the taste of flavored olive oil, I do not tell you, a real delight 

try this recipe and tell me your opinion. 

this recipe is published in the community: and if I petris ... 

if you like breads and all that follows, and you have a blog, do not forget to join this community. 

thank you for your comments, and thank you to all those who have tried my recipes and send photos, or even the link of my recipes tested, 

thank you to all those who subscribe to my RSS feeds, or to my newsletter (do not forget if you uncheck the two boxes during your registration, it means that you will not receive any alert when I post an article, or I send a newsletter, so you have to check both boxes) 

you may like:   
  
<table>  
<tr>  
<td>

![http://amour-de-cuisine.com/wp-content/uploads/2012/12/pain-herbe-014_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/pain-herbe-014_thumb11.jpg)

[ bread rolls with herbs and olives ](<https://www.amourdecuisine.fr/article-petits-pains-aux-herbes-et-olives-de-sihem-49247320.html>)


</td>  
<td>

![khobz dar, home-made homemade bread 003](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/khobz-dar-pain-maison-sans-petrissage-003_thumb1.jpg)

[ Khobz ed dar without fluffing ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>)


</td>  
<td>

![garlic brioche 013](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/brioche-a-l-ail-013_thumb.jpg) [ garlic brioche ](<https://www.amourdecuisine.fr/article-brioche-a-l-ail-90935841.html>) 
</td> </tr> </table>
