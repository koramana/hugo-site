---
title: small mugcake steamed
date: '2018-04-01'
categories:
- Cupcakes, macaroons, and other pastries
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/01/petits-mugcake-cuit-a-la-vapeur-2.jpg
---
![grandchildren mugcake cooked-in-the-vapor-2](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/petits-mugcake-cuit-a-la-vapeur-2.jpg)

##  small mugcake steamed 

Hello everybody, 

Today I share with you the recipe for these little mugcakes steamed, yes, you do not need your oven to achieve these small delight. 

It is a recipe shared with us my dear Lunetoiles, from the book of Marie Chioca: cakes and sweets low GI. 

It's small steamed mugcake, in a Dutch oven (for me a cast iron casserole), but we could quite cook them with couscous too. They are low (glycemic index), so healthy, good for diabetics etc ... moreover they are gluten free and lactose free. super is not it? 

Words from author marie chioca: 

> A simple and really delicious recipe for lovers of Italian-inspired coffee desserts. 
> 
> These mugcakes are cooked in the pan, certainly for a super soft texture, but also for you to present this dessert _c_ _appuccino_ in pretty coffee cups that would not go in the oven. 

![grandchildren mugcake cooked-in-the-vapor-3](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/petits-mugcake-cuit-a-la-vapeur-3.jpg)

you can see another version of this cake without eggs in video. .. and do not forget to subscribe to the YouTube channel: 

{{< youtube A3ROoMrL2aA >}} 

**small mugcake steamed**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/petits-mugcake-cuit-a-la-vapeur-1.jpg)

portions:  6  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** For mugcakes: 

  * 100 gr of hulled barley flour 
  * ½ bag of baking powder (5 gr) 
  * 1 pinch of salt 
  * 25 gr of cocoa 
  * 1 egg 
  * 10 cl of agave syrup 
  * 5 cl of strong espresso 
  * 5 cl of neutral oil (grape seed, sunflower ...) 

For the syrup: 
  * 6 tbsp. coffee strong 
  * 6 tbsp. coffee with agave syrup 
  * 1 tbsp. liquid vanilla extract 

For the cream: 
  * 2 tbsp. espresso very strong, and cold 
  * 1 tbsp. liquid vanilla extract 
  * 2 tbsp. tablespoon of coconut sugar 
  * 20 cl of 90% coconut cream (brand kara) cold, in the fridge since the day before 
  * cocoa to sprinkle 



**Realization steps**

  1. Place 6 coffee cups in a Dutch oven (cast iron casserole dish) and pour enough water into the bottom of the pot so that the level is about one third of the height of the cups. 
  2. Remove the cups, place a lid on the pot and bring the water to a boil. 
  3. Meanwhile, mix flour, baking powder, salt and cocoa. 
  4. Make a well. Pour in the egg, agave syrup, coffee and oil. 
  5. Mix with a whisk, then pour into the cups, without exceeding two-thirds of their height (otherwise the dough will overflow while cooking). 
  6. As soon as the water boils in the pan, place the cups, cover and cook for 20 minutes. 
  7. During all cooking especially do not open the lid. 
  8. After 20 minutes, check the cooking by pricking a mugcakes, then remove from the pot. 
  9. Mix the syrup ingredients, soak the mugcakes with a pipette or syringe and let them cool completely. 
  10. Mix 2 tbsp. cold coffee, liquid vanilla spoon and coconut sugar. 
  11. Whip the cream of coconut in whipped cream and as soon as it is thick, pour the cold sweetened coffee in a net without stopping whipping. 
  12. Dress each mugcake well chilled with a volute of coffee cream, then sprinkle with cocoa. Serve immediately, or chill until tasting. 



Note Note from author marie chioca:   
Wait until the mugcakes are well chilled before crowned with cream: this will prevent the cream from melting. ![grandchildren mugcake cooked-to-the-steam](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/petits-mugcake-cuit-a-la-vapeur.jpg)
