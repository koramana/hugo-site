---
title: algerian cake 2013 / el ftimates
date: '2013-10-10'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/gateau-algerien-2013-el-ftimates_thumb.jpg
---
[ ![cake-Algerian-2013-el-ftimates_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/gateau-algerien-2013-el-ftimates_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/gateau-algerien-2013-el-ftimates_thumb.jpg>)

Hello everybody, 

and I start the ball of Algerian cakes 2013, with this nice Algerian cake with beautiful colors, that I did not have time to publish .... and as I did not want the year to end without giving you the recipe, well here it is with a little video ... 

{{< youtube snzrBla06C8 >}} 

**algerian cake 2013 / el ftimates**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/el-ftimates-gateaux-algeriens-2013_thumb.jpg)

Recipe type:  Algerian cake  portions:  50  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for the pasta: 

  * 250 gr of flour 
  * 75 gr of margarine 
  * 1 pinch of salt 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of white dye 
  * 1 cup of dye (color of your choice, here orange and white) 
  * Orange blossom water + water 

the joke: 
  * 3 measures of ground almonds (I use a 200 ml bowl) 
  * 1 measure of crystallized sugar 
  * zest of 1 lemon 
  * 1 cup of vanilla coffee 
  * egg as needed 

decoration: 
  * honey 
  * silver beads 
  * egg white 
  * shiny food 



**Realization steps**

  1. of the dough: 
  2. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  3. Mix well with your fingertips 
  4. take a little part of this mixture 
  5. in a little flower water mix, dye them to have the color of your choice 
  6. start picking up the dough (let a little, to color it in white) with this water, 
  7. add water if you need, and knead a few seconds. 
  8. turn into a ball and let it rest. 
  9. add white dye to a little flower water, and collect the rest of the flour with, to have the white dough. 
  10. stuffing: 
  11. mix the almond and the sugar, 
  12. add the vanilla, and the lemon zest 
  13. stir an egg, and add it gently to the almonds 
  14. pick up the mixture, if necessary add in small quantity, another egg (no need to add everything) 
  15. method of preparation: 
  16. Lower the two pasta and switch to the machine from No. 1 to 5 ° then cut two circles of different colors, 
  17. the white make a circle of 12 cm of diameter and the orange of 10 cm of diameter. 
  18. Overlay the two circles (the little one on the big one) 
  19. press a little so that it sticks, and turn the dough so that the orange circle goes down. 
  20. Cut the edges of the dough with a knife to form a serrated end. 
  21. Coat the stuffing with egg white and place in the middle the stuffing of almonds 
  22. lift the ends in such a way that one corner is on the almond ball and another is facing outward. 
  23. you do not have to cover all the surface of the almond stuffing. 
  24. now spread the dough of two colors or just one of them to form small flowers 
  25. make a hole in the cake and insert two or three flowers 
  26. let rest overnight 
  27. the next day bake in a preheated oven at 150 degrees for 15 minutes 
  28. after cooking let cool, and dip the cakes in hot honey 
  29. decorate with pearls and shiny food 
  30. present in boxes, and good tasting 


