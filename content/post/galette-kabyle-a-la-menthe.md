---
title: Kabyle galette with mint
date: '2016-08-18'
categories:
- cuisine algerienne
- Mina el forn
- pain, pain traditionnel, galette
tags:
- Ramadan 2016
- Ramadan
- Easy cooking
- Healthy cuisine
- Ramadan 2015
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/galette-kabyle-%C3%A0-la-menthe-1.jpg
---
![Kabyle galette with mint 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/galette-kabyle-%C3%A0-la-menthe-1.jpg)

##  Kabyle galette with mint 

Hello everybody, 

Here is a recipe that has rocked my childhood, a recipe from Kabylie: Kabyle galette with mint. A legacy of family cooking that I am proud of and that I can not live without. 

It makes me happy that this kabyle mint galette makes its success at the loan of my children, because I do not want this recipe is lost with time. My recipe today is exactly as Jedda (my mother's grandmother) has always prepared, and all those who followed, I hope you will like it. 

**Kabyle galette with mint**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/galette-kabyle-%C3%A0-la-menthe-5.jpg)

portions:  20  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 1 nice bunch of fresh mint 
  * 1 medium onion (here 2 small) 
  * 4 cloves of garlic 
  * ½ glass of olive oil (120 ml) 
  * salt 
  * 2 large bowls of semolina (a 400ml bowl) 



**Realization steps**

  1. remove the mint (to use only the leaves, the stems may be a little hard in the cake) 
  2. immerse them in cool water, to get rid of all dust and dirt) 
  3. you can put the mint leaves in a blender to mix well, or use a large mortar 
  4. add salt, garlic and onion and crush well   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/galette-kabyle-preparation.jpg)
  5. in a bowl, sand the semolina with the olive oil. 
  6. add the mint mixture, work well at all. 
  7. normally you will not need to add water to the dough because the water released by the onion and mint should be enough to form the cake. 
  8. If necessary, wet gently with a little water to have a paste that picks up well and you can spread. 
  9. shape your cakes: the cake must be 1.2 cm thick,   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/galette-kabyle-cuisson.jpg)
  10. cut in rhombuses or in quarter, and cook in a good cast iron tajine (I use my electric pancake maker) 
  11. enjoy the pieces of mint patties by dipping the pieces in olive oil it's just a delight. 



![Kabyle galette with mint 4](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/galette-kabyle-%C3%A0-la-menthe-4.jpg)

merci pour vos commentaires, et vos visites, bonne journée 
