---
title: chorba mkatfa, jari mqatfa
date: '2013-12-26'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Soup
- Velvety
- Algeria
- Healthy cuisine
- Ramadan
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mkartfa-032_thumb.jpg
---
##  chorba mkatfa, jari mqatfa 

Hello everybody, 

I dig up this recipe of traditional Algerian dish to warm up well, a semolina dish named 

**Mkertfa, mquertfa, or M'kertfa, mguetaa, mketaa**

my mother has done this recipe just last week, and she made me new pictures, and they are sublime, I already want to plunge my spoon in this beautiful plate filled with this hot mkertfa. 

it is a traditional Algerian dish, very well known in the east of Algeria, especially in Constantine, it is based pasta worked a little cut and then cooked in a sauce. 

**chorba mkatfa, jari mqatfa**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mkartfa-032_thumb.jpg)

Recipe type:  Algerian cuisine, dish  portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** for the dough: 

  * semolina (1 small bowl) 
  * salt 
  * water 

for the sauce 
  * 1 onion 
  * 2 to 3 tablespoons tomato paste 
  * garlic 
  * a handful of dry beans (I was frozen) 
  * a lens handle 
  * a little oil 
  * salt, black pepper, paprika 



**Realization steps**

  1. boil lentils and beans 
  2. in a pot, simmer the oil, garlic, onion, and canned tomato 
  3. add the bean broth and lentils 
  4. if you like the spice you can add a spice 
  5. cover with water 
  6. now prepare the dough: wet the semolina and salt mixture with a little water, just to form a ball of dough, 
  7. then roll this pasta just a little, and let it rest. 
  8. Spread the dough well with corn starch so that the dough does not stick 
  9. shape a roll by rolling the dough on itself 
  10. with a knife cut thin slices. 
  11. When the sauce is boiling well, slices of m'kartfa are sieved through the sieve to remove excess cornstarch. 
  12. and put in the pot, just the time to cook, and put out the fire 
  13. serving hot, and I do not think there will be any left for people who are not at home. 



bon appétit 
