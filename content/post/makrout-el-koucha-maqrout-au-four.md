---
title: makrout el koucha maqrout in the oven
date: '2012-04-13'
categories:
- Algerian cakes- oriental- modern- fete aid
- gâteaux sablés, ghribiya
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makrout3_1.jpg
---
##  makrout el koucha maqrout in the oven 

Hello everybody, 

A sublime [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) Makrout el Koucha or Baked Makrout is generally more prepared in the eastern regions of Algeria. In the Algerian center we tend to like the [ fried makrout ](<https://www.amourdecuisine.fr/article-makrout-frit-makrout-el-maqla-ou-el-makla-72046810.html>) , or then the almond makrout. 

Me and the fact that I grew up in eastern Algeria, it is rather this recipe makrout el koucha or makrout oven I prefer the most. I also like the [ makrout rolled ](<https://www.amourdecuisine.fr/article-makrout-roule-102845781.html>) but it is a pity that my husband does not have a great fondness for the latter. So Makrout el koucha remains the kings makrouts at home. 

{{< youtube A9sujhy7JK4 >}} 

**makrout el koucha - Baked makrout**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makrout3_1.jpg)

**Ingredients** for a small tray of 40 x 50 cm 

  * 1200 gr of coarse semolina (not too much fat ok) 
  * 400 gr of ghee (smen, or clarified butter) 
  * 1 little salt 
  * Orange tree Flower water 
  * Water 
  * for the stuffing: 
  * 500 gr of date paste 
  * ½ teaspoons cinnamon 
  * ½ teaspoon of ground cloves 
  * 2 tablespoons of butter 
  * 2 cafe jam (optional, I had put it, because I had a paste of date of poor quality, and so I was afraid that the stuffing becomes hard cooking, I added the fig jam, which will make the dough soft) 
  * Orange tree Flower water. 



**Realization steps**

  1. the recipe is [ right here ](<https://www.amourdecuisine.fr/article-makrout-el-koucha-en-video-gateau-de-semoule-et-dattes.html>) on my blog 



![makrout el koucha - Baked maqrout](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makrout3_thumb_11.jpg)
