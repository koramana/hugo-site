---
title: Amandines with puff pastry
date: '2011-01-10'
categories:
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pliage11.jpg
---
English a recipe that I really lingered to publish, because I had to redo it to take more photos, but, I was running out of time, or rather, I did other things to my children and my husband. I'll try to put you the recipe, and I hope you'll excuse me for the delay & nbsp; for the ingredients, you will need: 50 grs of butter at room temperature 50 gr of crystal sugar 1 egg 1 teaspoon of vanilla extract 50 grs of ground almonds 25 grs of flour 50 grs & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 1.07  (  3  ratings)  0 

a recipe that I really lingered to publish, because I had to redo it to take more photos, but, I was short of time, or rather, I did other things to my children and my husband. 

I'll try to put you the recipe, and I hope you'll excuse me for the delay 

for the ingredients, you will need: 

50g of butter at room temperature 

50 gr of crystallized sugar 

1 egg 

1 teaspoon of vanilla extract 

50 grams of ground almonds 

25 grams of flour 

50 grs of almonds 

of the puff pastry 

prepare the almond cream, mixing the egg and the butter in a blender, add the sugar, the almond powder then the flour, add the vanilla extract, and set aside. 

take the puff pastry, cut it into squares, cut the sides of each square, drawing one "L" on one side, and another on the opposite side. 

![pliage.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pliage11.jpg)

take both sides and fold them, taking the right side on the left side, and the opposite for the opposite side. 

fill the hollow that you have formed with the almond cream. 

brush the sides with an egg yolk, and garnish with the slivered almonds. 

![amandine-641.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/amandine-64111.jpg)

Cook in a preheated oven at 180 degrees for 15 to 20 minutes 

at the end of the oven, you can brush your small tarts with apricot jam. 

merci pour tout vos commentaires. 
