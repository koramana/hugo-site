---
title: tajine minced meatballs and olives / kefta be zitoune
date: '2017-08-11'
categories:
- Moroccan cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/kefta-bi-zitoune.CR2_1.jpg
---
[ ![tajine minced meatballs and olives / kefta be zitoune](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/kefta-bi-zitoune.CR2_1.jpg) ](<https://www.amourdecuisine.fr/article-tajine-boulettes-de-viande-hachee-et-olives-kefta-be-zitoune.html>)

##  tajine minced meatballs and olives / kefta be zitoune 

Hello everybody, 

a delicious tajine with minced meatballs and olives that can be enjoyed with a home-baked bread, or 

to serve with a dish of rice, or a pasta with butter .... 

since I made this recipe at the beginning of my blog, a recipe that I saw on TV I adopted it, and I do not lie to you, when I have minced meat it's this kefta be zitoune that I think immediately. 

**tajine minced meatballs and olives / kefta be zitoune**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tajine-de-boulettes-de-viande-hachee-et-olives.CR2_1.jpg)

portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients** for the minced meatballs: 

  * 500 gr of minced meat 
  * 50 gr bread crumbs dipped in milk (which will give the tender side to minced meat) 
  * 2 crushed garlic cloves 
  * salt, black pepper. 
  * 1 pinch of cumin 

for the tajine: 
  * 1 onion 
  * 1 can of tomato in pieces or 4 to 5 fresh tomatoes 
  * 2 crushed cloves of garlic 
  * ½ teaspoon powdered cumin 
  * ½ teaspoon Ras el Hanout 
  * 1 teaspoon of paprika 
  * olive oil 
  * a nice handful of pitted green olives 
  * lemon confit 
  * salt pepper 



**Realization steps**

  1. start with the meatballs, mix the minced meat, the garlic, the crumb of bread drained of its milk, the spices, and the caraway. 
  2. form medium meatballs and leave aside. 
  3. cut the diced onions and brown them in a little olive oil 
  4. add tomatoes in pieces, crushed garlic and spices. 
  5. Let it cook and halfway through cooking, add the meatballs, 
  6. cover everything and let it simmer over medium heat 
  7. then add the boiled green olives in fresh water to desalt them a little. 
  8. simmer and add the lemon pieces just before reducing the sauce. 



[ ![tajine minced meatballs and olives kefta be zitoune 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/kefta-be-zitoune-1.CR2_1.jpg) ](<https://www.amourdecuisine.fr/article-tajine-boulettes-de-viande-hachee-et-olives-kefta-be-zitoune.html>)
