---
title: Hot sauce with yoghurt
date: '2013-12-22'
categories:
- Moroccan cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sauce-piquante-au-yaourt-1.CR2_.jpg
---
[ ![spicy sauce with yoghurt 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sauce-piquante-au-yaourt-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sauce-piquante-au-yaourt-1.CR2_.jpg>)

##  Hot sauce with yoghurt 

Hello everybody, 

Here is a hot sauce with yoghurt to fall, for lovers of spice, or even people who do not like spicy like me ... 

I always admit it, that I can not tolerate the pungency, but this sauce, I fell under its charm, when I had tasted it, during our English lessons that we had at the school of my children . It was every Monday, we were more than 16 women to gather to study together and each week, one of us had to prepare a dish, a small entrance, and a cupcake to take with tea, we will say a small buffet, and there was this recipe that made one of my friends of Somalian origin, she presented with salty donuts, or then samboussas ... And as I told you, despite that I ate with hhhhhhhhhhhhhh, haaaaaaaaaaaaaaaaa, and ssssssssssssswwww, I could not help but submerge my donut or my samboussa in this sauce that was swimming my mouth in a saliva bath, so it was spicy, but delicious ... 

Impossible to resist. Today I share with you this dip recipe, or spicy yogurt sauce that I presented to accompany chicken nuggets or [ chicken nuggets ](<https://www.amourdecuisine.fr/article-nugget-de-poulet-a-la-noix-de-coco.html> "Chicken nugget with coconut") yum yum, to fall ...   


**Hot sauce with yoghurt**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sauce-piquante-au-yaourt.CR2_.jpg)

Recipe type:  dip sauce  portions:  4  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 1 natural yoghurt   
the quantity will depend on according to your love for the spice, you can do more, you can do less. 
  * 1 clove of garlic 
  * ¼ fresh tomato 
  * 1 spicy pepper   
you can reduce the dose, I am small peppers almost 1 finger length, and I do not put any, hihihihi 
  * some sprig of parsley 
  * salt and black pepper 



**Realization steps**

  1. in the mixer bowl place the pepper cut, garlic, tomato, parsley. 
  2. mix everything until you have a very homogeneous mixture. 
  3. place it in a salad bowl, and add the yogurt on top, until you have the dosage that suits you. 
  4. Season with salt and black pepper to taste. 



[ ![spicy sauce with yoghurt.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sauce-piquante-au-yaourt.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sauce-piquante-au-yaourt.CR2_.jpg>)
