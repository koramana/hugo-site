---
title: Thimchouecht / Taknift or Kabyle omelette
date: '2014-12-10'
categories:
- Algerian cuisine
- Algerian cakes with honey
- fried Algerian cakes
- Algerian pastries
tags:
- Algerian cakes
- Algeria
- desserts
- Cakes
- Pastry
- Delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Thimchouecht-Taknift-Omelette-Kabyle-3.jpg
---
![Thimchouecht Taknift Omelette Kabyle](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Thimchouecht-Taknift-Omelette-Kabyle-3.jpg)

##  Thimchouecht / Taknift or Omelette Kabyle 

Hello everybody, 

Here is a recipe for a homemade semolina cake that is usually prepared for births or weddings. it looks like an omelette but it's a real cake with semolina and honey cooked in the pan. 

Thimchouecht, or [ mchewcha ](<https://www.amourdecuisine.fr/article-tahboult-mchewcha-galette-aux-oeufs.html> "tahboult - mchewcha-galette with eggs") , is a delicious cake all soft and unctuous, rich in egg, which is prepared for the woman who gave birth, it helps him to regain his strength, as it helps in breastfeeding, it must be said that it The Algerian woman is well spoiled with all the delights that are prepared for her, and it must be said that each region has its delight, and the way it takes care of this mother. 

This omelet recipe Kabyle omelet slightly differs from my recipe for [ egg pancake ](<https://www.amourdecuisine.fr/article-tahboult-mchewcha-galette-aux-oeufs.html> "tahboult - mchewcha-galette with eggs") but it remains the same principle of preparation. 

[ ![Thimchouecht Taknift Omelette Kabyle](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Thimchouecht-Taknift-Omelette-Kabyle-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Thimchouecht-Taknift-Omelette-Kabyle-1.jpg>)   


**Thimchouecht / Taknift (Omelette Kabyle)**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Thimchouecht-Taknift-Omelette-Kabyle-2-300x256.jpg)

**Ingredients**

  * 125 g of fine semolina 
  * 1 glass of milk 
  * 5 eggs. 
  * 4 tablespoons of neutral oil 
  * 100 g of sugar (you can decrease and put only 50 g) 
  * 1 sachet of baking powder 
  * 1 pinch of salt 
  * 1 bowl of neutral oil for cooking 
  * honey 



**Realization steps**

  1. Preparation: 
  2. Mix well the semolina with the 4 c. oil, add the eggs one by one and mix well. 
  3. Add cold milk, salt and yeast. 
  4. Mix the dough for a few minutes. 
  5. Heat half of the bowl of oil in a skillet not too wide then pour the dough. 
  6. Cook on a very low heat and put on a lid. 
  7. During cooking, spread oil on the surface of the cake. 
  8. Using a plate, drain the oil and return to the other side. 
  9. Put the oil back, while cooking, cut the omelette 
  10. triangles. 
  11. Add the remaining oil to brown well on all sides. 
  12. Remove from heat and sprinkle generously with melted honey. 



[ ![Thimchouecht Taknift Omelette Kabyle](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Thimchouecht-Taknift-Omelette-Kabyle.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Thimchouecht-Taknift-Omelette-Kabyle.jpg>)
