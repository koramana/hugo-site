---
title: cocoa sand pie / cocoa pie
date: '2014-03-25'
categories:
- salads, salty verrines
tags:
- Based
- pies
- Pastry
- Pie background
- Fast Food
- Easy cooking
- Pasta

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/fond-de-tarte-au-cacao1.jpg
---
[ ![bottom-of-the-pie-cacao1](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/fond-de-tarte-au-cacao1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/fond-de-tarte-au-cacao1.jpg>)

##  Cocoa pie with cocoa pie 

Hello everybody, 

I share with you today, a basic pastry recipe, that lunetoiles has prepared for one of these delicious pastries, a cocoa pie background, which has the texture of a beautiful cocoa shortbread dough. 

You can see on the blog, full of basic recipes, like the [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast") , the broken dough, the [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-inratable.html> "unbreakable genesis") ... These recipes will help you a lot to achieve the simplest dessert, has a complicated entry. 

**cocoa sand pie / cocoa pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/fond-de-tarte-au-cacao-290x195.jpg)

portions:  8  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 125 g flour 
  * 30 g of cocoa 
  * 3 tablespoons of sugar 
  * 120 g unsalted butter 
  * 3 tablespoons concentrated orange juice 



**Realization steps**

  1. In a food processor, gently mix the flour, cocoa, sugar and butter until the mixture is just lumpy. 
  2. Add enough orange juice to keep the mixture together. (I did not need to put the orange juice) 
  3. Remove from the robot and form a ball. 
  4. Flatten slightly, wrap with plastic wrap and refrigerate for 30 minutes. 
  5. Remove the dough from the refrigerator and lower on a floured board to 5 mm thick. Place in a 23 cm pie pan with a removable bottom. Sting the bottom. 
  6. Place a sheet of parchment or aluminum foil over the dough and cover with dried beans or rice. 
  7. Bake at 350 F (180 ° C) for 10 minutes. 
  8. Remove the beans and cook another 10 to 15 minutes or until the crust is cooked through. Let cool. 



method of preparation: 

[ ![pate-sablee au cocoa](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/pate-sablee-au-cacao.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/pate-sablee-au-cacao.jpg>)
