---
title: easy recipe of the floating island with video
date: '2017-07-04'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/ile-flotante_.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/ile-flotante_.jpg)

##  easy recipe of the floating island with video 

Hello everybody 

Here is the favorite dessert of my children all covered with a beautiful layer of caramel, my children really like, and me it suits me because it is a recipe super easy to achieve. 

The floating island is a super easy dessert to make. Just prepare a delicious custard, beat the whites in snow and cook for a few seconds in the microwave and voila it's done! 

**floating island recipe / with video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/ile-flottante_.jpg)

portions:  4  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 400 ml of milk. 
  * 3 eggs. 
  * 90g of sugar. 
  * 1 vanilla pod 

for caramel: 
  * 60g of sugar. 
  * 4 c. soup of water. 



**Realization steps** preparation of the English cream: 

  1. in a saucepan, put the milk to heat, 
  2. beat the egg yolks with 60g of sugar until blanching. 
  3. pour the hot milk little by little over, mixing 
  4. put the mixture back in the saucepan and cook on a low heat, stirring 
  5. put in a salad bowl, and let cool. 
  6. WHITES IN SNOW: 
  7. turn the whites into firm snow with the remaining sugar (30g) 
  8. cook in the microwave, 
  9. butter ramekin and sprinkle with sugar then fill 
  10. cook 20 seconds in the microwave, 
  11. pour the custard into cups and put the egg whites to boil and drizzle with warm caramel. 



![floating island with video](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/ile-flottante-1.jpg)
