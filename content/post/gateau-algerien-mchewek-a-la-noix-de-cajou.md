---
title: Algerian cake Mchewek with cashew nuts
date: '2014-07-22'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mchewek-a-la-noix-de-cajou-gateaux-algeriens_thumb1.jpg
---
![mchewek with cashew nuts, Algerian cakes](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mchewek-a-la-noix-de-cajou-gateaux-algeriens_thumb1.jpg)

##  Algerian cake Mchewek with cashew nuts 

Hello everybody, 

These mchawek has cashew nuts, I realized them during the past aid kbir, and I completely forgot to post, so I was overwhelmed. 

Last time and for the engagement of her sister, Lunetoiles asked me for the recipe, which I completely forgot about, but Lunetoiles finally found the recipe, then realized it, and here she shares it with us 

Some pictures are mine, and others are hers. 

**Algerian cake Mchewek with cashew nuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mchewek-noix-de-cajou-300x225.jpg)

portions:  40  Prep time:  10 mins  cooking:  5 mins  total:  15 mins 

**Ingredients**

  * 500 gr of finely ground almonds 
  * 300 gr sifted icing sugar 
  * 3 to 4 egg white 
  * 1 C. coffee vanilla extract 
  * 250 gr of cashew nuts 



**Realization steps**

  1. In a bowl, combine almonds, sugar and vanilla extract. 
  2. Wet with the egg whites until a firm dough is obtained. 
  3. Spread the dough into small balls the size of a walnut that you must pass in the egg white, then place half cashew nuts. 
  4. Garnish the center of the mécheweks with half a candied cherry. 
  5. Place the mécheweks in a buttered and floured plate, or covered with baking paper. 
  6. Bake in preheated oven halfway up th. (4-5) 
  7. for 10 to 15 minutes. Place them in boxes. 


