---
title: friable apple pie and almond cream
date: '2016-12-24'
categories:
- dessert, crumbles et barres
- gateaux, et cakes
- recettes sucrees
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-friable-aux-pommes-2.jpg
---
[ ![friable apple pie and almond cream 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-friable-aux-pommes-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06073.jpg>)

##  friable apple pie and almond cream 

Hello everybody, 

Back to fruit recipes of all seasons the apple, this tasty fruit, juicy, crisp and especially rich in vitamins and minerals. On my blog, you'll find plenty of apple recipes: [ apple pie with butter, ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-au-beurre-67056341.html> "Butter apple pie") [ apple tart ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-raffinee.html> "refined apple pie") , [ apple creme pie with apple ](<https://www.amourdecuisine.fr/article-tarte-a-la-creme-brulee-aux-pommes.html> "apple crème brûlée pie") , [ Moist appelcake ](<https://www.amourdecuisine.fr/article-moelleux-aux-pommes-117713740.html> "Soft apple cake") , [ Apple pie cake ](<https://www.amourdecuisine.fr/article-tarte-flan-aux-pommes.html> "Apple pie cake") , and still plenty of [ apple recipes ](<https://www.amourdecuisine.fr/article-tag/recettes-aux-pommes>)

We come back to today's recipe, this friable apple pie and almond cream, a realization of Lunetoiles, which makes me personally an eye, I really want to dive my fork, hum 

![friable apple pie and almond cream 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-friable-aux-pommes-et-creme-damande.jpg)

**friable apple pie and almond cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-friable-pommes-et-creme-damandes-1.jpg)

portions:  16  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients** for the broken dough: 

  * 150 g flour 
  * 50 g icing sugar 
  * 1 pinch of salt 
  * 1 big pinch of cinnamon in option (if you wish it) 
  * 100 g butter cut into cubes 
  * 1 egg yolk 
  * 1 teaspoon thick cream 

Almond paste : 
  * 175 g ground almond powder 
  * 175 g icing sugar 
  * The juice of half a lemon 
  * 1 egg white 

the cream with almond paste: 
  * 625 ml of milk 
  * 2 tablespoons of sugar with real vanilla (vanilla sugar) 
  * 40 g cornflour 
  * 175 g marzipan 

the crumble: 
  * 80 g flour 
  * 60 g of diced butter 
  * 2 tablespoons brown sugar 
  * large pinch of cinnamon optional (if you wish) 
  * 50 g grated almond paste, grated on a large-mesh grater 

garnish: 
  * 400 g apples - weight after peeling and cavity removed 
  * juice of half a lemon 
  * 75 g grated almond paste, grated on a large-mesh grater 



**Realization steps** Prepare the cake: 

  1. line a 24x24cm baking pan with parchment paper. 
  2. In a robot equipped with the leaf (K ​​beater), mix flour, sugar, cinnamon. 
  3. Add the butter cut into pieces. Beat quickly on medium speed until mixture is very fine. 
  4. Add the egg yolk and cream, mix for a while, until you have the dough. 
  5. Then flour the dough and spread it between two pieces of parchment paper and place it in the mold thoroughly (it must fit perfectly the size of the mold). 
  6. Cool the dough 1 hour in the refrigerator. 
  7. Stir the dough with a fork and bake for about 20 minutes at 175 ° C it should turn a golden brown color. Let cool.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06070.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06070.jpg>)

Prepare the almond paste: 
  1. In a large bowl, mix the almond powder with the icing sugar. 
  2. Make a well in the center and pour in the lemon juice and a little egg white. 
  3. Work the mixture by hand until it comes off the walls of the bowl walls. 
  4. Bind with enough of the remaining egg white to obtain a firm dough. 
  5. On a marble plate sprinkled with icing sugar, gently knead this dough until it is homogeneous. Do not work it too much because it would become oily. 

Prepare the cream with almond paste: 
  1. Put 500 ml of milk to the boil with the sugar and almond paste. 
  2. The rest of the milk pour in a bowl and add the cornstarch and mix with a whisk to dissolve the cornstarch. 
  3. When the marzipan is melted and the milk is hot, slowly pour the milk that has been mixed with the cornstarch, stir until the cream thickens clearly. 
  4. Remove from heat, cover with clingfilm and let cool.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06073.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06073.jpg>)

Prepare the apples and the crumble. 
  1. Cut the apples into thick cubes, put in cold water mixed with lemon juice. 
  2. In the bowl of the robot, always equipped with the sheet (K beater) mix the butter, the flour and the sugar. 
  3. Add marzipan and mix on medium speed until crumble. 

Montage: 
  1. Sprinkle evenly over the dough that is pre-cooked and golden brown, 75 g grated almond paste.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06075.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06075.jpg>)
  2. Put the apples tidy (at this stage if you wish you can skip the apples in a pan with a little butter for 3 to 4 minutes.)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06076.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06076.jpg>)
  3. Cover the apples with the marzipan cream   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06078.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06078.jpg>)
  4. and finish by sprinkling with crumble.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-friable-aux-pommes-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-friable-aux-pommes-4.jpg>)
  5. Bake in preheated oven at 175 ° C for 40 minutes until golden brown. 
  6. Let cool completely and place a few hours in the refrigerator before cutting.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06087.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/DSC06087.jpg>)
  7. Keep refrigerated. 



[ ![friable apple pie and almond cream 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-friable-aux-pommes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-friable-aux-pommes.jpg>)
