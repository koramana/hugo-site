---
title: Rosette has the mercotte orange
date: '2017-12-13'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Rosace-a-lorange-de-mercotte-1.jpg
---
##  ![rosette-a-lorange de Mercotte-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Rosace-a-lorange-de-mercotte-1.jpg)

##  Rosette has the mercotte orange 

Hello everybody, 

I'm sure you've all seen the cake Rosace a mercotte orange that was the theme of the technical event in season 5 of the best pastry chef in France. 

There was some damage, but frankly to be in front of a recipe that has never been seen or heard ... and start reading between the lines to understand how the cake should be! for me it could be a big carnage .... hihihihih. Hats off to the participants who still, and despite the lack of information, they manage to put a cake a final on the presentation table. 

Lunetoiles went to the stove too to make this beautiful rosette has mercotte orange, frankly, the result is super beautiful, is not it? 

![rosette-a-lorange de Mercotte-4](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Rosace-a-lorange-de-mercotte-4.jpg)

**Rosette has the mercotte orange**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Rosace-a-lorange-de-mercotte-3.jpg)

portions:  8  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** The Genoese: 

  * 147g of sugar, 
  * 3.5g vanilla sugar, 
  * 245g of eggs, 
  * 78g of butter, 
  * 122 g flour. 

Candied oranges: 
  * 6 to 8 beautiful untreated oranges, 
  * 400g of sugar. 

The diplomat cream: 
  * 490g whole milk, 
  * 1 clove and ½ vanilla, 
  * 148g of egg yolks, 
  * 148g caster sugar, 
  * 20g of flour, 
  * 20g of starch, 
  * 7g of gelatin powder and 42g of water, 
  * 148g of whipping cream 35% fat. 



**Realization steps** ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Rosace-a-lorange-de-mercotte-5.jpg) prepare the sponge cake: 

  1. Preheat the oven to 200 ° C rotating heat. 
  2. Butter and flour the mold, keep it cool. 
  3. In a bain-marie in a large salad bowl, whisk the sugar, vanilla sugar and eggs until the mixture is triple the volume and make the ribbon. 
  4. Remove it from the heat, pour it into the robot's tank and whisk until cool. Meanwhile melt the butter in a small bowl and add a little sponge cake, mix well. 
  5. When the main unit is cooled, gradually sift over the flour by incorporating it carefully and delicately with the spatula. 
  6. At the end, stir in the butter. 
  7. Fill the mold and bake for about 20 minutes until the sponge cake is sufficiently cooked. 

prepare the candied oranges: 
  1. Slice oranges into thin, even slices. 
  2. Put them in a large saucepan with the sugar. 
  3. Bring to a simmer on high heat and then confit slowly over low heat and covered for about 30 minutes. 
  4. Gently drain the slices, recover the syrup to soak the sponge cake. Let cool. 

Prepare the diplomat cream: 
  1. Start by making custard cream. Bring the milk to a boil in a large saucepan with vanilla cracked and scraped. 
  2. Rehydrate the gelatin in its water and put it in the refrigerator. 
  3. In a bowl, combine the egg yolks and sugar, then add the sifted flour and starch, smooth the mixture well. 
  4. Add while stirring constantly half of the hot milk. Put everything back on the heat, bring to a boil and cook while winnowing for about 3min. 
  5. Remove the vanilla beans and add the gelatin off the heat. Let cool. 

Mounting : 
  1. moisten the mold slightly and film it completely. 
  2. Line the bottom with the finest and slenderest slices of oranges starting at the center and overlapping slightly to obtain a beautiful, regular rosette. Reserve cool. Cut the sponge cake into two layers, soak generously with a brush with the reserved orange syrup. 
  3. Finely chop the remaining oranges and add them to the pastry cream. Fit the crème fraîche without trying to be too firm, it must remain frothy, then incorporate gently to the pastry with orange. 
  4. Fill the pan halfway up with half the cream, add, crust down, half a sponge cake and repeat the process. 
  5. Carefully pack everything and set aside for a minimum of two hours. 
  6. Unmould turn upside down on the serving platter. 
  7. Keep cool until ready to serve and enjoy within 48 hours. 



![rosette-a-lorange de mercotte_](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Rosace-a-lorange-de-mercotte_.jpg)
