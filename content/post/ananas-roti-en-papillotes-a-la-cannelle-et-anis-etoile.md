---
title: pineapple roasted in cinnamon wrappers and star anise
date: '2013-12-07'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/papillotte-ananas8_thumb.jpg
---
##  pineapple roasted in cinnamon wrappers and star anise 

Hello everybody, 

Have you ever tasted pineapple? this pineapple recipe made of cinnamon and star anise is an easy and quick dessert to try quickly ... it's just too good, especially if you enjoy it hot with a nice scoop of ice cream of your taste. This delicious is a realization of our dear Lunetoiles who liked to share it with us. 

**pineapple roti en papillotes, quick and easy dessert with cinnamon and star anise**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/papillotte-ananas8_thumb.jpg)

Recipe type:  dessert  portions:  6  Prep time:  5 mins  cooking:  30 mins  total:  35 mins 

**Ingredients**

  * 1 big pineapple 
  * 125 g caster sugar 
  * 25 g of half-salted butter 
  * 200 ml hot whole liquid cream 
  * 1 cinnamon stick 
  * 1 vanilla bean Bourbon 
  * 2 stars of badiane 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. Remove the plumet and the base of the pineapple, then cut in 6 equal parts in the direction of the height. Detach the pulp pieces of bark and cut into pieces. 
  3. Heat a large pan with the caster sugar. Cook and melt the sugar until it turns amber. Add the butter half salt, mix, then incorporate the hot liquid cream being careful to projections. Add cinnamon stick, cracked vanilla pod and star anise stars. To mix everything. 
  4. Place the pineapple pieces in the pan, set the heat to a minimum and cook the pineapple pieces for about 10 minutes, turning them regularly. 
  5. Cut out 6 rectangles of parchment paper. Place in the center of each piece of pineapple and pour over a little caramel spice. Close each candy-shaped foil or chaff with food thread and bake for 30 minutes. 



Thanks for your comments and your visits 

merci de vouloir vous abonnez a ma newsletter, si vous ne voulez pas rater toutes mes nouvelles publications 
