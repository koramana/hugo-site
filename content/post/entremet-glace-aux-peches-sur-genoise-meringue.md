---
title: frozen dessert with peaches on genoise meringue
date: '2016-02-07'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles and bars
- sweet recipes
tags:
- desserts
- To taste
- Breakfast
- Ramadan
- Algeria
- Algerian cakes
- delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/entremet-glac%C3%A9-aux-peches-sur-genoise-meringu%C3%A9-2.jpg
---
[ ![frozen dessert with peaches on genoise meringue 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/entremet-glac%C3%A9-aux-peches-sur-genoise-meringu%C3%A9-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/entremet-glac%C3%A9-aux-peches-sur-genoise-meringu%C3%A9-2.jpg>)

##  frozen dessert with peaches on genoise meringue 

Hello everybody, 

[ ![snow Queen](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/reine-des-neiges.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/reine-des-neiges.jpg>)

For today, Mina offers us an ice cream mousse dessert on a Genoese biscuit and a beautiful meringue. A dessert that will not leave you indifferent.   


**frozen dessert with peaches on genoise meringue**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/entremet-glac%C3%A9-aux-peches-sur-genoise-meringu%C3%A9-3-a.jpg)

portions:  10  Prep time:  60 mins  cooking:  20 mins  total:  1 hour 20 mins 

**Ingredients** for the biscuit: 

  * 2 eggs 
  * 7 tablespoons sugar 
  * 7 tablespoons milk 
  * 7 tablespoons of oil 
  * 7 tablespoons flour 
  * 2 packets of yeast 
  * 1 package of vanilla 

for the peach moss: 
  * 350 gr of peaches in a box 
  * 4 sheets of gelatin (8 gr) 
  * 90 gr of icing sugar 
  * 250 gr of whipped cream 

the mirror: 
  * 75 ml of mineral water 
  * 11 g of gelatin 
  * 150 gr of sugar 
  * 150 grams of glucose 
  * 150 gr of white chocolate 
  * 100 gr of condensed milk 
  * 5 gr of orange dye, or close to the color of the peaches 
  * 2 g of titanium oxide or white dye powder. 

Meringue: 
  * egg white 
  * 50 gr of sugar 



**Realization steps** prepare the biscuit: 

  1. whisk eggs, sugar and vanilla. 
  2. add milk and oil. 
  3. mix the flour and the baking powder, and introduce it delicately to the liquid mixture. 
  4. pour this mixture into a buttered and floured sponge cake mold 
  5. cook in an oven preheated to 180 degrees C until the biscuit turns a beautiful golden color. 
  6. remove and let cool on a rack 

Prepare the moss for fishing: 
  1. soak the gelatin in cold water. 
  2. mash the peaches (without the juice you need to keep), add the sugar and mix again 
  3. Introduce the whipped cream gently to this puree without breaking the whipped cream. 
  4. now that the gelatin is very soft, wring out the super good, place it in a little of the juice of peaches. 
  5. and heat it in the microwave until the gelatin melts completely. 
  6. pour it in on the peach moss, and mix gently with the splatula to introduce it. 
  7. pour the mousse into a round pan and let it set. to make sure it's okay you can put it in the freezer. 

the mirror: 
  1. Bring the water, sugar and glucose to a boil. 
  2. Add the milk and chocolate. 
  3. Remove from heat immediately, wait until the chocolate is melted and add the softened gelatin. 
  4. Add the colorant until you get the color you want. 
  5. Allow to thicken and cool to room temperature 
  6. Take the log out of the freezer, unmold and cover ... and let it defrost in the fridge. 
  7. decorate with colored white chocolate circles with a brush. 

mounting: 
  1. remove the foam from the freezer, and pour it on a rack 
  2. place the rack on a tray to recover the frosting. 
  3. pour the frosting on the foam to cover it well, if it is necessary to repeat the operation, do it. 
  4. gently lift the mousse with the peaches and place it on the biscuit. 
  5. put the whites in snow, then introduce the sugar 
  6. put the meringue around the foam, and burn to give a nice look. 
  7. decorate according to your taste. 
  8. it is better to serve immediately, after putting the meringue ... 



[ ![frozen dessert with peaches on genoise meringue1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/entremet-glac%C3%A9-aux-peches-sur-genoise-meringu%C3%A91.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/entremet-glac%C3%A9-aux-peches-sur-genoise-meringu%C3%A91.jpg>)
