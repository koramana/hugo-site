---
title: karantika
date: '2012-06-02'
categories:
- crepes, waffles, fritters
- Algerian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/garantita1.jpg
---
#  **[ ![garantita1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/garantita1.jpg) ](<https://www.amourdecuisine.fr/article-recettes-speciales-ramadan-2012.html/garantita1-3>) **

karantika 

Hello everybody, 

today my brothers have asked me to guarantee it, and I will be frank, I have never eaten, nor ever made 

besides my sister had done it before yesterday, so I was very tempted to try it, and I was not on it, neither me nor those who eat it, it really made me happy 

so I'll give you this delicious Algerian recipe 

**karantika**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/garantita-300x225.jpg)

portions:  8  Prep time:  10 mins  cooking:  60 mins  total:  1 hour 10 mins  **Ingredients** for a pyrex mold of 22 cm by 33 cm 

  * 2 glasses (220ml) of chickpea flour 
  * 1 glass of milk (optional, you can replaced by water) 
  * 3 glasses and a half of water 
  * 2 eggs 
  * ¼ glasses of oil 
  * 1 cup of coffee and a half of salt (or according to taste) 
  * 1 teaspoon of cumin (I let myself go in the amount of cumin because at home they like) 
  * black pepper 



![garantita1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/garantita13.jpg)

method of preparation: 

  1. whisk eggs with milk (if not a glass of water), salt, caraway, black pepper and oil 
  2. pour this mixture gently over the chickpea flour, and dissolve the lumps well 
  3. pour over the remaining water, you will have a very liquid mixture (we make you) 
  4. oil the mold, and pour over the mixture 
  5. place in a preheated oven at 180 ° C 
  6. cook between 1 and 1 and a quarter, depending on the oven 



[ ![garantita2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/garantita2.jpg) ](<https://www.amourdecuisine.fr/article-garantita-karantika.html/garantita2>)
