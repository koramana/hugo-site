---
title: Homemade Nutella
date: '2016-07-12'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/nutella-pate-a-tartiner-fait-maison_thumb.jpg
---
##  Homemade Nutella 

Hello everybody, 

I know it's cruel to show pictures of this delicious spread, which flows in all directions, without being able to put your finger in it, to lick a tip, just under the pretext of cleaning these fingers, or just want to taste to say: huuuuuuuuum it's super good is not it ????? 

So here is homemade Nutella that Lunetoiles has prepared. She even made pancakes with this spread, [ nutella pancakes ](<https://www.amourdecuisine.fr/article-pate-a-crepe-au-nutella-crepe-au-chocolat-facile-et-rapide-115021705.html>) . 

And what Lunetoiles says to you: 

This Nutella has a good taste of praline, and it really tastes of Nutella, plus, homemade, without palm oil, and it's too easy .... So we go for the recipe. 

[ ![nutella, homemade spread](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/nutella-pate-a-tartiner-fait-maison_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/nutella-pate-a-tartiner-fait-maison_2.jpg>)

**Homemade Nutella**   


**Homemade Nutella**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/nutella-maison-facile-001_thumb.jpg)

Recipe type:  spread  portions:  10  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients** for 2 jars of 250 ml 

  * 350 g whole hazelnuts 
  * 150 g brown sugar 
  * 30 g unsweetened cocoa powder 
  * 22 ml to 45 ml of rapeseed oil 



**Realization steps**

  1. Roast whole hazelnuts in preheated oven at 180 ° C for 10 to 15 minutes. 
  2. Place the hazelnuts just out of the oven into the robot bowl with a metal blade. 
  3. Mix until the hazelnuts begin to clump. It takes a while (about 5 minutes) so be patient! 
  4. Add the brown sugar and cocoa powder and mix for another 3 minutes, until a mixture becomes dark and the ingredients are well incorporated. 
  5. Now, slowly add the rapeseed oil, little by little until you get a good consitance (22 ml can largely be enough). To you to see, it must not be liquid! 
  6. Store in sealed glass or plastic jars in the refrigerator for 4-6 weeks 



[ ![homemade nutella](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/nutella-fait-maison_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/nutella-fait-maison_2.jpg>)

and here are some crepes that you can make with this nutella: [ nutella crepes ](<https://www.amourdecuisine.fr/article-pate-a-crepe-au-nutella-crepe-au-chocolat-facile-et-rapide-115021705.html>)

[ ![chocolate crepe paste](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pate-a-crepes-au-chocolat_thumb.jpg) ](<https://www.amourdecuisine.fr/article-pate-a-crepe-au-nutella-crepe-au-chocolat-facile-et-rapide-115021705.html>)
