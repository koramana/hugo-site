---
title: Tiramisu
date: '2007-11-08'
categories:
- recipes for babies 4 to 6 months
- recipes for children and babies
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/11/188834531.jpg
---
& Nbsp; - 3 large eggs - 100 g of sugar - 1 sachet of vanilla sugar - 250 g of mascarpone - 24 biscuits in a spoon - 1/2 liter of unsweetened black coffee - 30 g of bitter cocoa powder & nbsp; & Nbsp; To put in the refrigerator, for more than 4 hours, the more you leave and the more it is good, me personally, I do it at 17:00 to consume it the next morning morning, and it is a delight ......... ... yami yami. & Nbsp; Preparation: Separate the whites from the yolks. Mix the yolks + sugar + vanilla sugar. Add the mascarpone with a whisk. & Hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

\- 3 big eggs   
\- 100 g of sugar   
\- 1 sachet of vanilla sugar   
\- 250 g of mascarpone   
\- 24 biscuits with spoon   
\- 1/2 liter of unsweetened black coffee   
\- 30 g bitter cocoa powder   


To put in the refrigerator, for more than 4 hours, the more you leave and the more it is good, me personally, I do it at 17:00 to consume it the next morning morning, and it is a delight ............ yami yami. 

![1899_tiramisu440](https://www.amourdecuisine.fr/wp-content/uploads/2007/11/188834531.jpg)

Preparation: 

Separate the whites from the yolks. Mix the yolks + sugar + vanilla sugar. Add the mascarpone to the whisk. 

Beat the egg whites and gently stir in the spatula with the previous mixture. 

Prepare black coffee. 

Wet the biscuits in the coffee. 

Tapisser le fond du moule avec les biscuits. Recouvrir d’une couche de crème, oeuf, sucre, mascarpone. Alterner biscuits et crème. Terminer par une couche de crème. Saupoudrer de cacao. 
