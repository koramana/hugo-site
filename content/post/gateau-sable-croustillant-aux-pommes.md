---
title: crispy apple cake with apples
date: '2018-01-16'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/g%C3%A2teau-sable-croustillant-aux-pommes.jpg
---
[ ![crispy apple cake with apples](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/g%C3%A2teau-sable-croustillant-aux-pommes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/g%C3%A2teau-sable-croustillant-aux-pommes.jpg>)

##  crispy apple cake with apples 

Hello everybody, 

You remember the recipe that Lunetoiles shared a moment ago: [ crispy strawberry cake with strawberries ](<https://www.amourdecuisine.fr/article-gateau-sable-croustillant-aux-fraises.html>) ? This recipe has had a great success with my readers and at home. I even prepared it twice when I was having tea with my friends, and I did not leave the party without leaving the recipe. 

[ ![crispy apple cake with apples 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/g%C3%A2teau-sable-croustillant-aux-pommes-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/g%C3%A2teau-sable-croustillant-aux-pommes-4.jpg>)   


**crispy apple cake with apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/g%C3%A2teau-sable-croustillant-aux-pommes-2.jpg)

portions:  8  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients** for a round mold 20 cm in diameter, (mine is 22 cm) 

  * 175 g caster sugar (I put less this time about 80 gr and I used brown sugar) 
  * the zest of 1 lemon (I did not put this time) 
  * 2 medium eggs at room temperature 
  * 150 g sifted flour 
  * ½ cuil. coffee baking powder 
  * 50 g cold butter 
  * 2 big apples 
  * icing sugar for decoration 



**Realization steps**

  1. Preparation: 
  2. Preheat the oven to 180 ° C. 
  3. Butter and flour a missed mold of 20 or 22 cm in diameter. 
  4. Peel the apples and remove the core, then cut the apples into thick slices. 
  5. Sauté the apples in a pan with a little butter for 4 to 5 minutes (or just in a saucepan with a little water, just to make them a bit melty). 
  6. In a bowl mix together the zest of the grated lemon and the sugar until the sugar becomes moist and granular. 
  7. Reserve at least 10 minutes for the sugar to soak up the perfume. 
  8. After 10 minutes, add the eggs in the lemon sugar and beat with the electric mixer for a good 5 minutes, the device will be foamy and pale, this step is important for the mellowness of the cake. 
  9. Sift together the yeast and the flour then add them by mixing gently. But do not overwork the dough. 
  10. Pour the mixture into the mold. 
  11. Divide the apple pieces over the entire surface. Cut the butter into thin slices and arrange them all over the cake. 
  12. Bake halfway up (overlapping if possible 2 baking sheets one over the other) about 25 min depending on the ovens and size of the mold. 
  13. Let stand a few minutes in the mold, then unmold on a rack to let cool completely. 



[ ![crispy sand cake with apples 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/g%C3%A2teau-sable-croustillant-aux-pommes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/g%C3%A2teau-sable-croustillant-aux-pommes-1.jpg>)
