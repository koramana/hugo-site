---
title: Nut brownies
date: '2015-05-20'
categories:
- dessert, crumbles et barres
- gateaux, et cakes
- recettes sucrees
tags:
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/brownies-aux-noix-1.jpg
---
[ ![Nut brownies](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/brownies-aux-noix-1.jpg) ](<https://www.amourdecuisine.fr/article-brownies-aux-noix.html/brownies-aux-noix-1>)

##  Nut brownies 

Hello everybody, 

I come back to sweet delights, I noticed that sweet publications attract more views than salty recipes, it is very surprising, to say that people prefer to eat more sweet than salty !!! ??? Personally when I'm on a new site or a forum, I look at the sweet recipes, while I read the salty recipes ... Because I know that I prefer to make a new dish to enjoy with family. 

In any case, today is a brownie cake of my friend Wamani, a brownie with nuts, super bpn and melting in the mouth. And if you like brownies, I have a nice little selection on the blog, you just have to do the little search in the window that appears right at the bottom of this article, you will have all the brownies recipes 

**Nut brownies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/brownies-aux-noix.jpg)

portions:  12  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 200 gr of dark chocolate 
  * 180 gr of butter 
  * 3 eggs 
  * 125 gr of flour 
  * 180 gr of brown sugar 
  * pinch of salt 
  * 80 gr of nuts 
  * 50 gr of chocolate powder 
  * vanilla 



**Realization steps**

  1. beat the eggs and the sugar to the robot 
  2. in a salad bowl, mix the dry products: flour, nuts, salt, vanilla, chocolate powder 
  3. melt butter and chocolate in a bain-marie 
  4. pour this warm mixture over the egg and sugar mixture 
  5. introduce the mixture of dry products three times over, and incorporate well to have a homogeneous product. 
  6. Pour into a buttered and floured mold 
  7. cook in a preheated oven at 180 degrees C for 30 to 35 minutes 



[ ![Nut brownies](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/brownies-aux-noix-2.jpg) ](<https://www.amourdecuisine.fr/article-brownies-aux-noix.html/brownies-aux-noix-2>)
