---
title: Cherry Croquemitoufle
date: '2010-12-02'
categories:
- confitures et pate a tartiner

---
hello everyone, this article I wrote yesterday at night, this morning I went to the hospital, I am very sick, I cough like an empty can, hihihihi ah this change and this cold touch to l 'Bone, hihihihi and since I'm too cold, I do not tell you how I am. so for that, and as I do not have time, and tomorrow I know that I will not be free. I post you this recipe that Luntoiles to share with me, thank you my dear, you are a love so recipe taken from the site: the surfer For 6 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everyone, this article I wrote yesterday at night, this morning I went to the hospital, I am very sick, I cough like an empty can, hihihihi 

oh this change and cold touch to the bone, hihihihi and as I'm too cold, I do not tell you how I am. 

so for that, and as I do not have time, and tomorrow I know that I will not be free. 

I post you this recipe that Luntoiles to share with me, thank you my dear, you are a love 

**For 6 persons :**

  * 350 g cherries in syrup 
  * 1 jar of plain yoghurt 
  * 3 pots of flour 
  * 2 jars of sugar 
  * 1 sachet of vanilla sugar 
  * 3 eggs 
  * 1/2 pot of sunflower oil 
  * 1 packet of dry yeast 



For the "crumble" filling: 

  * 100 g of butter 
  * 100g of flour 
  * 100 g of sugar 
  * 80 g of almond powder 



Preheat the oven to 180 ° C.    
In a bowl pour the pot of yogurt, the 3 pots of flour, the 2 pots of sugar, the vanilla sugar, the 3 whole eggs, the 1/2 pot of oil. Mix until smooth and homogeneous. Add the yeast packet and mix again.    
Pour this mixture into a buttered pan about 22 cm (high edge). Drain the cherries carefully in the syrup and spread them over the whole surface of the dough by pressing them slightly.    
Prepare the crumble machine:    
In a bowl, pour 100 gr of flour, 100 gr of soft butter, 100 gr of sugar and 80 gr of almond powder. Work quickly with your fingertips until you obtain a granular and very friable mixture.    
Cover the top of the cake completely with this mixture by rubbing it between your hands in order to bunch grains larger or smaller for a "Crumble" effect.    
Bake the cake for about 45 minutes. It must be well cooked inside and the top (crumble) must be barely colored. Let cool before serving.    
This cake is also delicious with other fruits, fresh and pitted or syrup well drained (apricots, peaches, pineapple, etc.). 

thank you very much lunetoiles for this delicious recipe 

et bisous a tout le monde 
