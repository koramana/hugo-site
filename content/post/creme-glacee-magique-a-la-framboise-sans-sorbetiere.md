---
title: magic ice cream with raspberry without sorbetiere
date: '2017-08-13'
categories:
- juice and cocktail drinks without alcohol
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/cr%C3%A8me-glac%C3%A9e-magique-%C3%A0-la-framboise-1.jpg
---
[ ![magic ice cream with raspberry](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/cr%C3%A8me-glac%C3%A9e-magique-%C3%A0-la-framboise-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/cr%C3%A8me-glac%C3%A9e-magique-%C3%A0-la-framboise-1.jpg>)

##  magic ice cream with raspberry without sorbetiere 

Hello everybody, 

in these hot weather, there is no better than an ice cream, is not it? when it's an ice cream without ice cream, we're never going to say no, and when it's without egg cream, we'll say: quick recipe, lol. 

So here is a magic ice cream recipe with raspberry without ice cream maker that Lunetoiles, yumi yumi, has prepared for us, give me 5 minutes, and prepare your ice cream cups, because we will feast. 

**magic ice cream with raspberry without sorbetiere**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/cr%C3%A8me-glac%C3%A9e-magique-%C3%A0-la-framboise-.jpg)

portions:  2 to 3  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 300 g frozen raspberries 
  * 1 soya yoghurt, or 2 Swiss puffs, or 1 yogurt of cow or sheep, or 200g of cottage cheese in well-drained faisselle, or vanilla soymilk 
  * 6 to 8 tablespoons Agave syrup or maple syrup or honey 



**Realization steps**

  1. Put everything in the bowl of a mixer. 
  2. Start pulse mode with brief pulses, then mix frankly until you have a perfectly creamy consistency. 
  3. Serve immediately or return to the freezer but not more than 4 hours otherwise the ice loses its softness. 
  4. If you need to freeze the ice longer, take it out 20 minutes before eating it. 



[ ![magic ice cream with raspberry](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/cr%C3%A8me-glac%C3%A9e-magique-%C3%A0-la-framboise-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/cr%C3%A8me-glac%C3%A9e-magique-%C3%A0-la-framboise-001.jpg>)
