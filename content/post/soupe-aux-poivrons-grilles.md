---
title: grilled pepper soup
date: '2013-02-06'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

---
Hello everybody, 

a super delicious soup with grilled peppers, and when I said veloute hmiss is because with the first spoon that I drip I found this delicious taste of hmiss, huuuuuuum, what is it it was good, it was eat me and my husband while savoring, the only comment from my husband was: "next time, you spice in" ... it's not something that will please me because I do not like spicy ... I have to find a solution a ca ??? !!! 

Ingredients: 

  * 3 red peppers. 
  * 1/2 cup tomatoes cut into pieces 
  * 1 clove of garlic 
  * 1 tablespoon of olive oil 
  * Salt and pepper to taste 
  * croutons for decoration. (optional) 



you can use a knorr cube (boiling chicken) but for my part i like the neutral flavor of grilled peppers in this soup. 

method of preparation: 

  1. Preheat the oven to 160 ° C. 
  2. Line a baking sheet with foil, arrange peppers and grill for about 45 minutes or until very tender, turning regularly so they are evenly grilled. 
  3. Remove from oven and let cool completely. 
  4. Remove skin and seeds and cut peppers roughly. 
  5. in a bowl, add peppers, crushed tomatoes, and salt to taste, garlic. add a little water otherwise the knorr cube dilute in a little water. 
  6. Mix with the blender foot, until smooth, you can add the water until you have the consistency that suits you. 
  7. add the oil and bring the soup to a boil for 5 minutes, and here it is ready. 


