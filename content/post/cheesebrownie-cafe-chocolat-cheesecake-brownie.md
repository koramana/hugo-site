---
title: cheesebrownie cafe chocolate-cheesecake brownie
date: '2015-07-20'
categories:
- cheesecakes and tiramisus
- dessert, crumbles and bars
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Cheesebrownie-caf%C3%A9-chocolat-.jpg
---
[ ![cheesebrownie cafe chocolate-cheesecake brownie](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Cheesebrownie-caf%C3%A9-chocolat-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Cheesebrownie-caf%C3%A9-chocolat-.jpg>)

Hello everybody, 

yet another brownie recipe you'll tell me, a cheesecake brownie ??? Yes yes my friends, you must absolutely taste this delight, and you will see that you will become an "addict", lol. 

This time the recipe for this [ brownie cheesecake ](<https://www.amourdecuisine.fr/article-brownie-cheesecake-recette-facile-delicieuse.html> "Brownie cheesecake easy and delicious recipe") does not come from my kitchen, but rather from the kitchen of Lunetoiles, she made this chocolate chocolate cheesebrownie, or cheesecake brownie.She did some great pictures, that I did not even know which ones to choose for this article. 

This fondant cake in which this mixture of intense tastes, like those of coffee and those of chocolate .. adding to all that, the slightly salty sweetness of the fondant forming .... and the incomparable taste of mascarpone ... 

in any case for more recipes from [ Brownie ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=brownie&sa=Rechercher&siteurl=www.amourdecuisine.fr%2F&ref=&ss=2880j1956714j7>) , visit the link 

Yes, yes I know, you are just as impatient to discover this recipe, so to our ingredients:   


**cheesebrownie cafe chocolate-cheesecake brownie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/mi-brownie-mi-cheesecake-caf%C3%A9-chocolat-.jpg)

portions:  16  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients** For a 25 x 25 cm mold For the device cheesecake: 

  * 70 g of mascarpone 
  * 120 g of cream cheese (or fresh square) 
  * 115 g thick cream 
  * 35 g caster sugar 
  * 1 egg 
  * 30 g of flour 
  * 2.5 cl of espresso coffee 

For the brownie maker: 
  * 250 g butter at room temperature (I put 200 g) 
  * 250 g dark chocolate (I put 200 g) 
  * 150 g flour 
  * 6 eggs (I put 5 eggs) 
  * 340 g of sugar (I put 200 g) 
  * 1 C. natural vanilla extract 
  * 1 pinch of salt 



**Realization steps**

  1. Preheat the oven to 160 ° C (Th 5-6) 
  2. The device has cheesecake 
  3. In a cul-de-poule, mix the mascarpone with the cream cheese, the sugar, the coffee, the cream and finally the sifted flour. 
  4. The device must be smooth and homogeneous. 

The brownie machine 
  1. Melt the butter, cut into small pieces, with the coarsely chopped chocolate in a pothole on a bain-marie. 
  2. Mix and let cool. 
  3. In another container, beat the eggs with the sugar and vanilla extract until the mixture is smooth and light, without letting the cream whiten. 
  4. Stir in the melted chocolate / butter. 
  5. Mix and add the sifted flour and salt using a spatula. 
  6. In the buttered mold, pour the brownie dough and then the cheesecake machine. 
  7. Draw scrolls with a wooden spike mixing the two preparations. 
  8. Bake for about 45 minutes. 
  9. This cake should not be overcooked to keep it soft. 
  10. Let it cool down before unmolding. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/brownie-cheesecake-caf%C3%A9-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/brownie-cheesecake-caf%C3%A9-chocolat.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
