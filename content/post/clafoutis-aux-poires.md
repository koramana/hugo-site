---
title: pear clafoutis
date: '2018-01-21'
categories:
- cakes and cakes
tags:
- Ramadan 2018
- Pastry
- Shortbread
- desserts
- Algerian cakes
- Cakes
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg
---
##  pear clafoutis 

Hello everybody, 

i thank lunetoiles who frankly did a wonderful job, and i leave you with the recipe:   


**pear clafoutis**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/image_thumb_110.png)

**Ingredients**

  * [ A short dough ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>)
  * Syrup: 
  * 80 cl of water (40 cl of boiling water, 40 cl of cold water) 
  * 300g of sugar 
  * vanilla pod 
  * juice of a small lemon and its zest 
  * 6-7 small pears 

Cream: 
  * ⅔ cup of whole cream 
  * 4 eggs 
  * 4 tablespoons flour (or cornflour) 
  * 4cs of sugar 
  * 1 cup pear syrup 



**Realization steps** Poached pears: 

  1. Bring the water and sugar to a simmer in a large pan with a thick bottom. 
  2. Add the zest and the juice of the lemon, as well as the vanilla. 
  3. Peel the pears carefully, taking care to keep the tail. Dip them in the syrup. 
  4. Bring to a boil. Bake 12 to 15 minutes, until the pears are tender. 
  5. Then let cool in the syrup 30 minutes. 
  6. Pre-cook the pie shell in an oven heated to 175 ° C. 
  7. The filling: Mix the flour or cornflour and the sugar in a bowl, and whisk the syrup little by little. Add whisking eggs and cream. 
  8. Place the poached pears on the bottom of the pie, and pour the cream around. 
  9. Bake for about 30 minutes at 175-180 ° C. 
  10. Let cool before topping with apricot jam. Put at least 1 hour before tasting. 



thank you very much to lunetoiles and if you like lunetoile, you want to share your recipes on my blog, or then you have to try my recipes, I invite you to send me the recipe and the picture on the email 

![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg)

merci beaucoup pour vos commentaires. 
