---
title: Macaron strawberry pie
date: '2013-04-17'
categories:
- appetizer, tapas, appetizer
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-macaron-fraises1.jpg
---
hello everyone, another strawberry recipe that Lunetoiles shares with us, and it is frankly greedy ... imagine fleshy and juicy strawberries, decorate with a thin layer of jam, placed on a crisp bed and sweet macaroon. the delight does not stop there, this macaronée layer, covers a very melting layer of pastry cream .... so you imagine all these textures that burst in the mouth with each spoon ... without delay, we go to the recipe. the ingredients: For a 23 cm pie mold or pie or a rectangle pie mold the dough & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.85  (  1  ratings)  0 

![strawberry macaroon pie](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-macaron-fraises1.jpg)

Hello everybody, 

yet another strawberry recipe that Lunetoiles shares with us, and it is frankly greedy ... 

imagine fleshy and juicy strawberries, decorate with a thin layer of jam, placed on a crisp and soft macaroon bed. 

the delight does not stop there, this macaronée layer, covers a very melting layer of pastry cream .... so you imagine all these textures that burst in the mouth with each spoon ... 

without delay, we go to the recipe. 

![strawberry tart macaron-001](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-fraises-macaron-0011.jpg)

the ingredients: For a 23 cm pie shell or pie or a rectangle pie pan 

sweet pie dough: 

  * 180 g flour 
  * 70 g icing sugar 
  * 80 g of ointment butter 
  * 3 egg yolks 



custard : 

  * 3 egg yolks 
  * 60 g caster sugar 
  * 1 sachet of vanilla sugar 
  * 25 g flour 
  * 200 cl of milk 



Macaroon cover: 

  * 3 egg whites 
  * 125 g icing sugar 
  * 25 g of almond powder 



400 to 500 gr of washed strawberries, hulled and cut in half 

2 to 3 tablespoons. strawberry jam 

![macaron strawberry pie](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-fraises-macaron1.jpg)

method of preparation: 

  1. Prepare the pie dough: Put the flour and sifted icing sugar in a bowl. 
  2. Add the ointment butter and squeeze it. When there are no more large pieces of butter, stir in the egg yolks. 
  3. Reserve cold for 30 minutes. Lower to 3 mm thick on a floured work surface and drive a 23 cm diameter circle or a rectangle pie plate. 
  4. Pass around the edge with the roller to drop the excess dough. Stitch the bottom of many fork stings and slide the pie pan into the refrigerator while preparing the filling. 
  5. Prepare the custard: Put the milk to heat in a saucepan until boiling. 
  6. Beat the egg yolks with the sugar and vanilla sugar, until the mixture whitens. Then add the flour and beat again. 
  7. At the first boil of the milk, pour it on the egg / sugar / flour mixture, without ever stop whipping. Beat until smooth and smooth. 
  8. Pour everything back into the saucepan, and put back on the heat. Return to the boil without ever stop whipping. When the cream has thickened, remove from heat, place in a bowl, and let cool. Cover the cream with cling film to prevent skin from forming. 
  9. For the macaroon machine: Whip the egg whites until stiff, add the icing sugar halfway. 
  10. Carefully add the almond powder. 
  11. When the custard has cooled, spread it over the bottom of the dough. 
  12. Then spread over the pastry cream, the macaroon cover. 
  13. Bake for 10 minutes at 180 ° C, then lower the oven temperature to 160 ° C and continue cooking for 50 minutes. 
  14. Let cool completely, before placing the half strawberries on the pie and topping with the help of a strawberry jam brush heated but lukewarm. 
  15. Cool for at least 1 hour before serving. 



![strawberry pie macaron-copy-1](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-aux-fraises-macaron-copie-11.jpg)
