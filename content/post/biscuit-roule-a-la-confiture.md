---
title: rolled jam cookie
date: '2014-12-25'
categories:
- cakes and cakes
- basic pastry recipes
- sweet recipes
tags:
- Based
- Soft
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/roll-021_thumb1.jpg
---
##  Rolled jam cookie 

Hello everybody, 

biscuit rolled with jam: When I last posted the article about chocolate sausage, and that I talked about having 5 logs for school, I had a lot of questions on the basis of my logs. It's pretty simple, easy is delicious, I realize a **rolled jam cookie** , or the chocolate ganache, or the butter cream, and here is the base of a log that I can garnish to my taste after. 

By what I did 5 logs, I went to simplicity, 2 logs **rolled jam cookie** packed at the [ white chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat-blanc.html> "white chocolate ganache") . 2 filled and garnished logs [ chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat.html> "chocolate ganache") , and a log garnished and stuffed with [ butter cream ](<https://www.amourdecuisine.fr/article-la-creme-au-beurre-facile-112774625.html> "easy butter cream") . Unfortunately I did not have time to take photos, because I had my lunch boxes filled, and direct to the school to prepare the stalls, and wait until the end of the school to do the charity sale. 

Otherwise for the girls who are waiting for my log based on this biscuit rolled with jam, I tell you, do not wait my dear, lol, I do not make logs, because at home my children prefer cookies and small ovens, and my husband, he likes cakes without cream, [ mouskoutchou ](<https://www.amourdecuisine.fr/article-le-mouskoutchou-au-chocolat.html> "chocolate mouskoutchou") , or one **rolled jam biscuit** nature without garniture ... I will not make a log to eat alone! lol.   


**rolled jam cookie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/roll-021_thumb1.jpg)

portions:  12  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 4 eggs 
  * 120gr of sugar 
  * 100gr of flour 
  * vanilla 
  * a half c a c of baking powder 
  * jam I used apricot jam 



**Realization steps**

  1. we start by blanching the eggs with the sugar until the mixture is creamy, then add the sifted flour mix with yeast and vanilla (if you use vanilla sugar you put it with sugar and eggs) me I used the extract, 
  2. mix gently until the mixture is homogeneous. 



garnish a baking dish with lightly greased baking paper. pour the mixture and put in the preheated oven at 180 degrees and cook for 10 to 15 minutes.   
remove the biscuit from the oven and let cool.   
unmold on aluminum foil then roll with aluminum foil at the same time, leave for a few minutes and then unroll the cake. now he and close to be garnished. 

so spread the jam over the whole surface. roll the biscuit on itself, then roll it in the aluminum foil and put it in the refrigerator at least half an hour to keep it in shape. 

faites sortir du réfrigérateur enlevez le papier aluminium bien sure moi je l’ai saupoudre du sucre glace comme vous pouvez le glacer avec du chocolat, a la crème au beurre ……. 
