---
title: gingerbread, as well as a game
date: '2009-02-12'
categories:
- birthday cake, party, celebrations
- sweet recipes

---
as I'm a gingerbread addict, and as we do not find that here in England, I'm looking for the ideal recipe that really gives the real gingerbread, that my dad will buy us often, when we was small. so, that one, was not really as I wanted it, already it is with my old oven that I cooked that, so ... I have to retry the recipe in the new oven to give you my verdict ok . in any case it was delicious but not the same texture. 200g honey ingredients & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

as I'm a gingerbread addict, and as we do not find that here in England, I'm looking for the ideal recipe that really gives the real gingerbread, that my dad will buy us often, when we was small. 

so, that one, was not really as I wanted it, already it is with my old oven that I cooked that, so ... I have to retry the recipe in the new oven to give you my verdict ok. 

in any case it was delicious but not the same texture. 

Ingredients 

  * 200g of honey 
  * 20g of butter 
  * 1 tsp of spice bread 
  * 1 cc of anise seeds 
  * 200 ml of milk 
  * 250g of whole wheat flour 
  * 1 packet of dry yeast 



In a saucepan melt the honey and butter over very low heat.   
In a salad bowl, mix the flour with the yeast and add the spices. Stir in gradually, mixing milk and honey. The paste obtained must be unctuous.   
Butter a cake tin and pour in the mixture.   
Bake for 40 minutes cooking at 180 ° C. 

and as said in the title I run a game. 

if you have a gingerbread recipe 

that you are doing very well 

please leave me a comment with a link to the recipe. 

or even if you do not have a blog, leave me a comment, I will contact you. 

the game will end on March 10, 2009 

it's going to be my birthday, so the winner will have a nice gift just like me. 

bisous 
