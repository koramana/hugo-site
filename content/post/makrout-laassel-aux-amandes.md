---
title: makrout laassel with almonds
date: '2017-06-15'
categories:
- Algerian cakes with honey
- fried Algerian cakes
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/makrout-laassel-1-683x1024.jpg
---
![makrout laassel 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/makrout-laassel-1-683x1024.jpg)

##  makrout laassel with almonds 

Hello everybody, 

The makrouts laassel bel louz, or makrout lassel with almonds is a cake that I personally like a lot, but it's not what my husband or children like, they prefer [ makrout el koucha ](<https://www.amourdecuisine.fr/article-makrout-el-koucha-en-video-gateau-de-semoule-et-dattes.html> "makrout el koucha in video / semolina cake and dates") . 

So this time for Aid el adha, we had a little discussion on this subject, makrout laassel or makrout el koucha ??? Surely the result was for the majority who voted makrout el koucha. But who is cooking in the end? lol 

Anyway, I was not the authoritarian person at home, but we made compromises, everyone will have the cake he loves for this holiday of Aid and a single cake, not a list if we do not will never end. Four cakes in the end, yes the baby has not yet had the right to vote, lol ... that I will divulge very soon, in my next tickets. 

{{< youtube B4pBhRgR12Q >}} 

Today, it starts with my cake to me, my cute peach, I was still reasonable, preparing only 30 pieces, lol because in the end, I will eat this cake all alone, something that will not be difficult because, this cake is not going, suddenly long fire. 

![makrout laassel](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/makrout-laassel.jpg)

**makrout laassel with almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/makrout-laassel-3.jpg)

portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for the pasta: 

  * 3 measures overflowing with medium semolina 
  * 1 measure smen minus 1 finger, and fill the rest with the table oil 
  * a pinch of salt 
  * a mixture of water and orange blossom water (almost a measure) 
  * lemon peel finely scraped   
(optional, but it gives a good taste) 
  * for a guaranteed makrout success, I add an egg white + 2 tbsp. flour 

for the stuffing: 
  * 3 small measures of ground almonds 
  * 1 measure of crystallized sugar 
  * ½ c. cinnamon 
  * a few tablespoons of orange blossom water to pick up the stuffing 

decoration: 
  * honey 

frying: 
  * table oil 



**Realization steps** Prepare the stuffing: 

  1. pick up almonds, sugar, cinnamon with orange blossom water to obtain a soft and malleable stuffing. Book. 

Prepare the dough: 
  1. Pour in a large bowl the semolina, the salt. Mix. 
  2. Make a fountain and add over the measure of warm fat. sand well between your hands so that the fat is completely absorbed by the semolina. 
  3. Let stand at least an hour. sand from time to time 
  4. After this time, add the egg white and flour, sprinkle the mixture of semolina and fat with the mixture water and water of orange blossom just at the end of the fingers without kneading it. the dough should pick up but not pasty. 
  5. let the dough rest a little for abosrb 
  6. take a good amount of the dough, shape a pudding. With your index finger, make a slit in the center in the direction of the length. 
  7. Roll a little pudding of almond stuffing and place it in the slot. 
  8. Raise the edges of the semolina paste with a brush with your thumb and forefinger to cover the almond stuffing. 
  9. Ride gently again to have a roll of a width of 3 cm in height. 
  10. cook these lozenges in a very hot oil, but over medium heat. The makrouts must have a golden hue and well cooked inside 
  11. at the end of cooking, soak the almond makrouts in hot honey   
In any case, one of the two must be very hot, either the makrouts or the honey. 



![makrout laassel 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/makrout-laassel-2.jpg)
