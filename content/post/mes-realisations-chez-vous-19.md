---
title: my achievements at home 19
date: '2012-06-06'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/Nids-d-artichauts-DSC_8211_167191.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

you have been many to realize my recipes, and it makes me great pleasure ... 

thank you to all those who continue to leave their comments in comments after their essays, and thank you to those who continue to send photos of their achievements to this email: 

vosessais@amourdecuisine.fr 

on the [ griweches ](<https://www.amourdecuisine.fr/article-25345475.html>) : 

Hello Soulef, 

I found your recipe of the griwiches more than a year ago, since it does not leave me, besides I have never missed it, I even passed it to my little sister and my girlfriends , everyone did it right the first time, you're really the best. 

Thank you for everything, and good luck. 

Fairouz, a faithful reader for a long time. 

Sure [ the basboussa ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html>) : 

I just made your recipe and my children loved it but in your syrup, I replaced the sugar  with very perfumed honey.it is super and very mellow.thank you for all your recipes and has good soon. kisses. 

on the [ home express bread ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) : 

Hello Cooking love, thank you for this recipe, this bread is very light.Je tried it, but it is not as good as yours.Une recipe I intend to adopt Merciiiiiiiii. 

Sousou Meslem 

[ ![Nests of artichokes DSC 8211 16719](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/Nids-d-artichauts-DSC_8211_167191.jpg) ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>)

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/272522251.jpg)

![](https://www.amourdecuisine.fr//import/http://4.bp.blogspot.com/-5AqVoMM71fc/T47QAGweVjI/AAAAAAAAAVM/mwX24jtJ9uI/s320/)

![DSCF3208.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/DSCF32081.jpg)

![011-copy-5.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/011-copie-51.jpg)

[ meskoutchou or meskouta ](<https://www.amourdecuisine.fr/article-mouskoutchou-meskouta-105833476.html>) at Siham (a reader) 

good evening, congratulations for this delicious Meskoutcha, my boy and my husband have admired, finally I found a Meskoutcha very tender   
YAATIK EL SAHA continues to amaze us with your good recipes    
good luck dear Soulef   
Sissi 

[ Khobz ed dar without fluffing ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) at Rocha 

Salam Soulef,   
Here is the photo of my homemade bread tested from your recipe, a marvel.    
Good continuation.    
Rocha 

[ Brioche ](<https://www.amourdecuisine.fr/article-37067174.html>) at houria sana 

[ croissants briochees ](<https://www.amourdecuisine.fr/article-35847545.html>) at houria sana 

[ Panna cotta ](<https://www.amourdecuisine.fr/article-panna-cotta-vanille-et-mousse-fraises-mascarpone-101606363.html>) of Leila at Houria sana 

[ apple pie with butter ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-au-beurre-67056341.html>) , at Houria Sana 

[ Pineapple kiwi verrines ](<https://www.amourdecuisine.fr/article%0A-verrines-sucrees-kiwis-mascarpone-64934521.html>) at Fifi Nadia 

salam my sister   
I am a very big fan of your blog I thank you for your sharing   
machallah 3lik   
j’ai testé tes verrines kiwi ananas et c’est un veritable coup de coeur , j’en fais souvent mnt 🙂   
so here is my little contribution    
salamou3alaykoum 

[ Khobz eddar without fluffing ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) at Source of Hope 

[ Barbie birthday cake ](<http://webmail.amourdecuisine.fr/reademail.php?id=62&folder=Inbox&cache=DUB115-W7BF6043EACFC812AEF74FBB050@phx.gbl>) at Source of Hope 

[ dziriettes ](<https://www.amourdecuisine.fr/article-dziriette-en-fleur-gateau-algerien-aux-amandes-et-miel-58328618.html>) at houria sana 

[ dziriettes ](<https://www.amourdecuisine.fr/article-dziriette-en-fleur-gateau-algerien-aux-amandes-et-miel-58328618.html>) at houria sana 

[ Mkhabez ](<https://www.amourdecuisine.fr/article-lamkhabez-el-matnawaa-57770658.html>) at houria sana 

[ lmkhabez ](<https://www.amourdecuisine.fr/article-lamkhabez-el-matnawaa-57770658.html>) at houria sana 

[ triangular skandraniette ](<https://www.amourdecuisine.fr/article-skandraniette-en-triangle-aux-noisettes-gateau-algerien-2012-96576788.html>) at houria sana 

[ carrot velvety ](<https://www.amourdecuisine.fr/article-veloute-de-carotte-au-lait-de-coco-maison-81947971.html>) at malia minette 

[ shrimp soup ](<https://www.amourdecuisine.fr/article-soupe-de-crevette-85354072.html>) s at malia minette 

[ Nests of tomatoes ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs-101484090.html>) at beatrice 

Hello,    
Here is a photo of my first try, cherry tomato and quail egg ...    
Thanks for the recipe …    
Beatrice 

here for this time, I lingered to put this article online, thank you to all the people who have tried my recipes, and continue to send me the pictures on this email: [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)
