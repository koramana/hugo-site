---
title: mirabelle and ricotta crostini
date: '2017-07-20'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/crostinis-aux-mirabelles-et-ricotta-004.CR2_thumb1.jpg
---
##  mirabelle and ricotta crostini 

Hello everybody, 

to snack this afternoon, me and the kids, we opt for those delicious crostini whose base for me was slices of [ olive bread ](<https://www.amourdecuisine.fr/article-pains-aux-olives.html>) , topped with a smooth layer of ricotta cheese, and adorned with slices of mirabelle plums baked with honey and olive oil, and perfumed with rosemary. 

a recipe that I love to prepare with children, and that we enjoy .... a real delight that I advise you. 

**mirabelle and ricotta crostini**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/crostinis-aux-mirabelles-et-ricotta-004.CR2_thumb1.jpg)

Recipe type:  appetizer, aperitif  portions:  6  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 4 plums or plums. 
  * 1 tablespoon and a half of honey 
  * 1 tablespoon extra-virgin olive oil + 1 tablespoon for toasting slices of bread. 
  * 1 branch of fresh rosemary 
  * salt 
  * ½ cup of ricotta 
  * 6 slices of olive bread (a choice for me, normal slice of baguette will do the trick) 
  * the zest of 1/2 lemon. 



**Realization steps**

  1. Preheat the oven to 200 degrees. 
  2. Cut the plums in half and extract the stones. 
  3. Place the cut side of the plums on a plate. 
  4. In a small bowl, mix honey and olive oil until smooth. 
  5. Pour this mixture over the plums. garnish with rosemary and a few pinches of salt. 
  6. place them in the oven and cook until they are slightly tender and release a little juice, about 15 minutes. 
  7. Meanwhile, brush a plate with olive oil and place slices of bread on top 
  8. top with a little more oil and grill in the oven. and turn on the other side 
  9. let the plums cool a bit and cut into slices. 
  10. evenly garnish each slice of bread with a tablespoon of ricotta cheese. 
  11. decorate with a few slices of mirabelle plums and lemon zest. 


