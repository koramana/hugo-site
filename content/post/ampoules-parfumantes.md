---
title: Perfume ampoules
date: '2007-12-18'
categories:
- the tests of my readers

---
****

**![bonj5](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201443171.gif) **

**Using a cloth, apply perfume extract to the top of the ampoules.**

**When you light the bulb, the smell will diffuse into the room.**

**Lavender extract is good.**

**And for an intimate atmosphere, use your scent.**

![bye1](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201443591.gif)

** **
