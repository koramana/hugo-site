---
title: soups, chorba and hrira ramadan menu 2018
date: '2018-05-09'
categories:
- Algerian cuisine
- ramadan recipe
tags:
- Ramadan 2018
- Healthy cuisine
- Easy cooking
- Algeria
- Vegetables
- Ramadan
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-4.jpg
---
![algerian chorba 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-4.jpg)

##  soups, chorba and hrira ramadan menu 2018 

Hello my friends, 

We will soon welcome the holy month of **Ramadan** and for this sublime occasion, I wish you all to spend a serene month, may Allah accept our fast, and strengthen our faith, and make us and our descendants of pious Muslims. A special thought for the sick, the homeless, and young singles, who spend Ramadan alone, far from their families ... I hope for my part can help you preparing this index which includes many soups and soups, which are still on the menu of the day, there is no more good than a good **chorba** , A voucher **jari** or a good **hrira** to start his Iftar ... 

You can also find other indexes (which I will update as and when) 

  * [ bricks and boureks ](<https://www.amourdecuisine.fr/article-recettes-speciales-ramadan-les-boureks-et-bricks-108003546.html>)
  * [ entrees ](<https://www.amourdecuisine.fr/article-bricks-bourek-entrees-recette-d-accompagnement-ramadan-2013-118781731.html>) and [ salads ](<https://www.amourdecuisine.fr/article-menu-du-ramadan-les-salades-107046301.html>)
  * [ main dishes ](<https://www.amourdecuisine.fr/article-recettes-ramadan-2018-plats-pour-ramadan-2018.html>)
  * [ breads and buns ](<https://www.amourdecuisine.fr/article-index-des-pains-galettes-crepes-viennoiseries-55212113.html>)
  * [ desserts ](<https://www.amourdecuisine.fr/article-menu-du-ramadan-les-desserts-107046748.html>)



**[ Chorbas and soups: ](<https://www.amourdecuisine.fr/soupes-et-veloutes> "chorba, soups and velvets") **  
  
<table>  
<tr>  
<td>

**[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-bida-1_thumb1-272x125.jpg) ](<https://www.amourdecuisine.fr/article-chorba-beida.html>) ** **[ c ](<https://www.amourdecuisine.fr/article-chorba-beida.html>) ** [ horba bayda, soup ](<https://www.amourdecuisine.fr/article-chorba-beida.html>) [ White ](<https://www.amourdecuisine.fr/article-chorba-beida.html>) [ Hey ](<https://www.amourdecuisine.fr/article-chorba-beida.html>) [ with vermicelli and chicken «شربة بيضاء ](<https://www.amourdecuisine.fr/article-chorba-beida.html>) 
</td>  
<td>

**[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-2-006.CR2_1-272x125.jpg) ](<https://www.amourdecuisine.fr/article-chorba-soupe-au-ble-vert-concasse-du-ramadan.html/chorba-frik-2-006-cr2_1>) ** [ chorba frik - Jari bel frik - Crisp green wheat soup ](<https://www.amourdecuisine.fr/article-chorba-soupe-au-ble-vert-concasse-du-ramadan.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/hrira-aux-legumes-272x125.jpg) [ vegetable hrira ](<https://www.amourdecuisine.fr/article-hrira-aux-legumes.html>) 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-5-272x125.jpg) [ algerian chorba ](<https://www.amourdecuisine.fr/article-chorba-algeroise.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/hrira-oranaise-272x125.jpg) [ Oran hrira ](<https://www.amourdecuisine.fr/article-hrira-oranaise.html>) 
</td>  
<td>

**[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-vermicelle-007_thumb-272x125.jpg) ](<https://www.amourdecuisine.fr/article-chorba-vermicelle-soupe-a-la-vermicelle.html>) ** [ Chorba vermicelli ](<https://www.amourdecuisine.fr/article-chorba-vermicelle-soupe-a-la-vermicelle.html>) 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-4-272x125.jpg) [ chorba bird languages ](<https://www.amourdecuisine.fr/article-chorba-algerienne-aux-langues-doiseaux.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/harira-soupe-marocaine2-272x125.jpg) [ Moroccan Harira ](<https://www.amourdecuisine.fr/article-harira-soupe-marocaine-du-ramadan-2014.html>) 
</td>  
<td>

![Cream Of Mushroom](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/velout%C3%A9-de-champignons-150x150.jpg) [ Cream Of Mushroom ](<https://www.amourdecuisine.fr/article-veloute-de-champignons.html>) 
</td> </tr>  
<tr>  
<td>

**[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/soupe-de-poisson-nawel-272x125.jpg) ](<https://www.amourdecuisine.fr/article-soupe-de-poisson-a-langlaise.html/soupe-de-poisson-nawel>) ** [ English-style fish soup ](<https://www.amourdecuisine.fr/article-soupe-de-poisson-a-langlaise.html>) 
</td>  
<td>

**[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/soupe-de-laitue-2-272x125.jpg) ](<https://www.amourdecuisine.fr/article-soupe-la-laitue-et-petits-pois.html/soupe-de-laitue-2>) ** [ lettuce soup ](<https://www.amourdecuisine.fr/article-soupe-la-laitue-et-petits-pois.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-de-crevettes-014a_thumb-272x125.jpg) ** [ Shrimp soup ](<https://www.amourdecuisine.fr/article-soupe-de-crevette-85354072.html>) 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/soupe-de-lentilles-corail-a-la-patate-douce-1-272x125.jpg) [ coral lentil soup ](<https://www.amourdecuisine.fr/article-soupe-de-lentilles-corail-a-la-patate-douce.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-1-272x125.jpg) [ minestrone soup ](<https://www.amourdecuisine.fr/article-soupe-minestrone.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/soupe1_thumb1-272x125.jpg) ** [ Cream of peas ](<https://www.amourdecuisine.fr/article-veloute-de-petits-pois.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Creme-de-citrouille-rotie-272x125.jpg) ](<https://www.amourdecuisine.fr/article-creme-de-citrouille-rotie.html/creme-de-citrouille-rotie>) [ rotie pumpkin cream ](<https://www.amourdecuisine.fr/article-creme-veloutee-de-citrouille-rotie-soupe-de-potiron.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/soupe-de-poulet-038.CR2_-272x125.jpg) ](<https://www.amourdecuisine.fr/article-soupe-veloutee-de-poulet.html/soupe-de-poulet-038-cr2>) [ chicken soup ](<https://www.amourdecuisine.fr/article-soupe-veloutee-de-poulet.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/06/mkartfa-038a_thumb1-272x125.jpg) [ chorba mkartfa ](<https://www.amourdecuisine.fr/article-chorba-mkatfa-jari-mqatfa.html>) 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-a-loignon-21-272x125.jpg) [ onion soup ](<https://www.amourdecuisine.fr/article-la-soupe-a-l-oignon.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-cremeuse-au-chou-vert-272x125.jpg) [ green cabbage soup ](<https://www.amourdecuisine.fr/article-veloute-cremeux-au-chou-vert.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-de-carottes-aux-noix-de-saint-jacques-272x125.jpg) [ Carrot cream-soup ](<https://www.amourdecuisine.fr/article-veloute-de-carottes-aux-noix-de-saint-jaques.html>) 
</td> </tr>  
<tr>  
<td>

![soup-de-chestnut-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/soupe-de-chataigne-2-150x150.jpg) [ chestnut soup ](<https://www.amourdecuisine.fr/article-soupe-de-chataigne.html>) 
</td>  
<td>

![Coral lentil soup with sweet potato 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/soupe-de-lentilles-corail-a-la-patate-douce-1-150x104.jpg) [ coral lentil soup with sweet potato ](<https://www.amourdecuisine.fr/article-soupe-de-lentilles-corail-a-la-patate-douce.html>) 
</td>  
<td>

![easy minestrone soup.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-minestrone-facile.CR2-001-150x111.jpg) [ Minestrone soup ](<https://www.amourdecuisine.fr/article-soupe-minestrone.html>) 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/soupe-de-tomates-019_21-272x125.jpg) [ tomato soup ](<https://www.amourdecuisine.fr/article-soupe-de-tomate-parfaite-et-veloutee-114752334.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-au-poireaux-celeri_thumb-272x125.jpg) [ leek soup and celery ](<https://www.amourdecuisine.fr/article-soupe-aux-poireaux-et-celeri-facile-et-rapide.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/veloute-de-poireaux-soupe-aux-poireaux-026.CR2_-272x125.jpg) [ velvety leek soup ](<https://www.amourdecuisine.fr/article-soupe-aux-poireaux-veloute-de-poireaux-et-parmesan.html>) 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/velout%C3%A9-de-choux-de-bruxelles-1-272x125.jpg) [ velouté with Brussels sprouts ](<https://www.amourdecuisine.fr/article-veloute-aux-choux-de-bruxelles.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-de-navet-grill%C3%A9-1-272x125.jpg) [ grilled turnip soup ](<https://www.amourdecuisine.fr/article-soupe-de-navet-grille.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/bisque-de-potiron-aux-noix-de-saint-jacques-272x125.jpg) [ pumpkin bisque with scallop walnuts ](<https://www.amourdecuisine.fr/article-bisque-de-potiron-aux-noix-de-saint-jacques.html>) 
</td> </tr>  
<tr>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-de-lentilles-036_thumb-272x125.jpg) ** [ LENTIL SOUP ](<https://www.amourdecuisine.fr/article-soupe-de-lentilles.html>) 
</td>  
<td>

**[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-de-feves-015-1_thumb-272x125.jpg) ](<https://www.amourdecuisine.fr/article-veloute-de-feves.html>) ** [ VEGETABLE OF FÈVES ](<https://www.amourdecuisine.fr/article-veloute-de-feves.html>) 
</td>  
<td>

**[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-de-grain-1_thumb-272x125.jpg) ](<https://www.amourdecuisine.fr/article-veloute-de-legumes-secs.html>) ** [ VEGETABLE DRY VEGETABLES ](<https://www.amourdecuisine.fr/article-veloute-de-legumes-secs.html>) 
</td> </tr>  
<tr>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/Veloute-de-Courgettes-1-272x125.jpg) ** [ CREAM OF ZUCCHINI ](<https://www.amourdecuisine.fr/article-veloute-de-courgettes-61481531.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-de-potiron-018_thumb-272x125.jpg) ** [ PUMPKIN SOUP ](<https://www.amourdecuisine.fr/article-soupe-de-potiron-58516712.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/veloute-patate-douce-020_thumb-272x125.jpg) ** [ Veloute with sweet potato and squash ](<https://www.amourdecuisine.fr/article-veloute-automnal-a-la-patate-douce-et-la-courge-88167689.html>) 
</td> </tr>  
<tr>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-de-potiron_thumb-272x125.jpg) ** **[ Pumpkin soup ](<https://www.amourdecuisine.fr/article-soupe-de-potiron-surprise-86694827.html>) ** 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-patate-douce-et-poires-005_thumb-272x125.jpg) ** [ velvety sweet potato and pear ](<https://www.amourdecuisine.fr/article-veloute-de-patates-douces-et-de-poires-100286555.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-d-epinards-011_thumb-272x125.jpg) ** [ Cream of spinach ](<https://www.amourdecuisine.fr/article-creme-d-epinard-97641704.html>) 
</td> </tr>  
<tr>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bissara_thumb-272x125.jpg) ** [ Bissara, broken peas velvety ](<https://www.amourdecuisine.fr/article-soupe-au-pois-casses-bissara-97334307.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/soupe-de-chou-fleur_thumb-272x125.jpg) ** [ cream with cauliflower ](<https://www.amourdecuisine.fr/article-creme-au-chou-fleur-100720359.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/2011-08-20-veloute-de-carotte_2-272x125.jpg) ** [ Veloute of carrots with coconut milk ](<https://www.amourdecuisine.fr/article-veloute-de-carotte-au-lait-de-coco-maison-81947971.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/soupe-de-poivrons-grilles.CR2-001_thumb-272x125.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/soupe-de-poivrons-grilles.CR2-001_2.jpg>) [ grilled pepper soup ](<https://www.amourdecuisine.fr/article-soupe-de-poivrons-grilles-ou-veloute-de-hmiss-112983330.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/soupe-au-fenouil.CR2-copie-1-272x125.jpg) [ fennel soup recipe, easy velvety ](<https://www.amourdecuisine.fr/article-recette-soupe-au-fenouil-veloute-facile-114482601.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mkartfa-032_thumb-272x125.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mkartfa-032.jpg>) [ chorba mkatfa, jari mqatfa ](<https://www.amourdecuisine.fr/article-chorba-mkatfa-jari-mqatfa-113031029.html>) 
</td> </tr>  
<tr>  
<td>

![carrot soup with coriander](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/velout%C3%A9-de-carottes-%C3%A0-la-coriandre-150x150.jpg) [ carrot soup with coriander ](<https://www.amourdecuisine.fr/article-veloute-de-carottes-a-coriandre.html>) 
</td>  
<td>

![Chinese soup with chicken and corn](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/soupe-chinoise-au-poulet-et-ma%C3%AFs-150x150.jpg) [ chinese soup ](<https://www.amourdecuisine.fr/article-soupe-chinoise-poulet.html>) 
</td>  
<td>

![soup or velvety peas](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/soupe-ou-velout%C3%A9-de-petits-pois-150x150.jpg) [ velvety soup of peas ](<https://www.amourdecuisine.fr/article-soupe-veloute-de-petits-pois.html>) 
</td> </tr> </table>

**soups, chorba and hrira ramadan menu 2018**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-vermicelle-007_thumb11.jpg)

**Ingredients**

  * hrira 
  * chorba 
  * jari frik 
  * chorba beida 
  * chorba vermicelli 



**Realization steps**

  1. In a pot, sauté the cut meat in small pieces, with grated onion, zucchini, potato and carrot diced in oil and smen. 
  2. Add the chopped coriander (leave a little for the presentation) and the paprika 
  3. add the mint and the pasta to the blinder and the little cinnamon stick. 
  4. add the mixed tomato, salt and black pepper. 
  5. Cover with water (at least 1 liter of boiling water) and cook. 


