---
title: cakes thousand leaves jam and peanuts
date: '2015-07-14'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
- sweet recipes
tags:
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-milles-feuilles-confiture-et-cacahuetes-4.CR2-001.jpg
---
[ ![cakes mille feuilles jam and peanuts 4.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-milles-feuilles-confiture-et-cacahuetes-4.CR2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-milles-feuilles-confiture-et-cacahuetes-4.CR2-001.jpg>)

##  cakes thousand leaves jam and peanuts 

Hello everybody, 

here is a delicious cake very rich in taste, I assure you, between the taste of apricot jam, the taste of peanuts well roasted and crispy, and between the taste of the cake that resembles the taste of gingerbread. This recipe is a recipe that my beautiful sister passed to me almost 7 years ago, I realized it by making a final layer of chocolate. But this time for lack of chocolate, I opted for this filling jam and roasted peanuts, the taste of the cake is even better. 

For a better result, prepare this cake in advance, and let it rest, so that the cake becomes very soft, and that it absorbs the jam well. 

Another very important thing  : 

I always cooked the dough in a casserole, but many people found that the dough becomes hard and not easy to spread, so I redid the recipe, but cooking in the bain-marie ... we do not risk not having a hard dough, and the result is impeccable. 

[ ![cakes thousand leaves jam and peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-milles-feuilles-confiture-et-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-milles-feuilles-confiture-et-cacahuetes.jpg>)   


**cakes thousand leaves jam and peanuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-milles-feuilles-confiture-et-cacahuetes-2.jpg)

portions:  45  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 125 gr of butter 
  * 16 tablespoons of milk 
  * 16 tablespoons of sugar 
  * 8 tablespoons of honey 
  * 1 cup of baking soda 
  * 1 packet of baking powder 
  * flour 

decoration: 
  * apricot jam 
  * crushed roasted peanuts. 



**Realization steps**

  1. take the butter, sugar, milk, honey and baking soda and place them in a salad bowl on a bain-marie over low heat, 
  2. stir with a wooden spoon while mixing, until the sugar melts, and the mixture gets a color of a beautiful light brown caramel, it should not be a hard caramel nor too thick.   




[ ![multi layer cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateau-multi-couche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateau-multi-couche.jpg>)

devise the dough on three equal parts, 

[ ![the layers](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/les-couche-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/les-couche-.jpg>)

Roll out the first ball and put it in a flour-baking tin, prick the dough well with the fork, and bake in a preheated oven at 180 degrees C. 

[ ![layer](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/couche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/couche.jpg>)

do the same thing with the second and third ball. 

when the cakes are cooked, place the first layer, and cover it well with jam, place the second layer and cover the jam, then the 3rd layer. and let it rest. 

cut the cake into squares of almost 3 by 3 cm 

[ ![the cup](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/la-coupe.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/la-coupe.jpg>)

dip the cakes in the warm jam and then in the peanuts 

[ ![peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cacahuetes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cacahuetes.jpg>)

then decorate with melted dark chocolate. I apologize for the quality of the photos, but it's screenshots of my videos that I made but I could not edit because of lack of time, I hope to publish it for you shortly 

thank you 

[ ![cakes thousand leaves jam and peanuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-milles-feuilles-confiture-et-cacahuetes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-milles-feuilles-confiture-et-cacahuetes-1.jpg>)
