---
title: chicken quesadillas
date: '2016-02-01'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- crepes, waffles, fritters
- diverse cuisine
tags:
- peppers
- inputs
- aperitif
- Mexico
- sauces
- Summer
- tortillas

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/quesadillas-au-poulet-3.jpg
---
![chicken quesadillas 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/quesadillas-au-poulet-3.jpg)

##  chicken quesadillas 

Hello everybody, 

At home we love everything that is based on [ tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html>) , moreover every week, if I do not make wraps, I make some [ tortillas gratins ](<https://www.amourdecuisine.fr/article-gratin-de-tortillas-a-la-bolognaise.html>) , sometimes [ quick pizzas ](<https://www.amourdecuisine.fr/article-pizza-tortillas-aux-champignons.html>) , or mini entries like these [ burritos ](<https://www.amourdecuisine.fr/article-paniers-de-burritos-au-poulet.html>) . 

So I always have tortillas at home, and to be frank, I do not like the trade, especially here in England, everything must contain sugar which I do not see the need in a recipe like tortillas for example, and since my husband does not like salty sweet recipes, he prefers that I make tortillas at home, to avoid this taste of sugar more ... Yes another chore for me, but I organize myself to do once a month, I make a good amount that I keep in the freezer and voila ... 

For today, and when I say today I'm talking about Monday, which is a busy day for me, between laundry and cleaning, eating is the last of my worries. But here is my epoxy call me to tell me I bought some chicken breast, can you do some things with us? I chose to make quesadillas, but to be faster, you have to bring a nice battery of stoves, not a step of two, sometimes 3 ... The I speak when we do not have time when there is only 20 minutes to eat, you have to do it, you have to take out all the dishes, hihihihih. 

I would have liked my husband to offer me a nice drum set of de Buyer iron stoves, but here I have all the stoves of the world, but none of them looks like the other, besides you will see all the colors of stoves that I have earlier on the preparation photos. Why I needed all these stoves, because I had to cook the chicken pieces on one side, in another to sweat the onion, then remove to cook the peppers, and I had to have a bigger one to cook my quesadillas because I had the intelligence to prepare my tortillas in my crepe maker Electric Krampouz, so they were very big that my stoves. 

In any case, after running in all directions, my quesadillas were ready in 25 minutes, but of course I do not tell you the state of the kitchen after. I did not want to think about it, my chicken quesadillas were so good, and it was fast. 

**chicken quesadillas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/quesadillas-au-poulet-1.jpg)

portions:  6  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 6 [ flour tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html>)
  * 2 chicken breasts 
  * 1 red pepper 
  * 1 green pepper 
  * 1 yellow pepper 
  * 1 onion 
  * 1 clove of garlic 
  * 2 tablespoons of butter 
  * 2 tablespoons extra virgin olive oil 
  * salt and black pepper, 
  * turmeric curry 
  * grated cheese (cheddar in my recipe) 



**Realization steps**

  1. Clean and cut the onion into strips. 
  2. Cut the peppers into strips too. 
  3. Clean and cut chicken breasts into cubes and place in a bowl. 
  4. season with salt and black pepper, turmeric and curry 
  5. In a large skillet, heat 1 tablespoon of olive oil and add the chicken cubes. 
  6. Cook the chicken for 5 to 10 minutes until it is fully cooked and no longer pink, let it take a nice golden color. 
  7. Remove the chicken from the pan, and clean the pan well. 
  8. Add another tablespoon of olive oil to the pan and heat. cook the peppers,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poivrons.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poivrons.jpg>)
  9. when it's done, add them to the chicken. 
  10. cook now the onion strips, when it is translucent, add the crushed garlic, when it is well cooked, add the cooked chicken and the peppers, cook for a minute.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/farce-de-tortillas.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/farce-de-tortillas.jpg>)
  11. In a clean pan, put some butter and melt it. place the tortilla on the melted butter, and slip with your hands, for an even cooking 
  12. Add a little chicken and onion mixture on top of the tortilla, and a nice amount of cheese, depending on your taste. 
  13. Add another tortilla on top and press well so that it sticks a little.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/2016-02-01-tortilla-au-poulet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/2016-02-01-tortilla-au-poulet.jpg>)
  14. Now, using a wide, flat plate, turn the patty to cook on the other side until it is golden brown. 
  15. Remove from pan and cut into quarters. 
  16. Repeat the same with the remaining tortillas to have 3 beautiful patties. 



[ ![chicken quesadillas 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/quesadillas-au-poulet-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/quesadillas-au-poulet-2.jpg>)
