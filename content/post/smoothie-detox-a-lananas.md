---
title: detox smoothie with pineapple
date: '2015-12-27'
categories:
- juice and cocktail drinks without alcohol
- Detox kitchen
- Healthy cuisine
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-detox-a-lananas.jpg
---
[ ![detox smoothie with pineapple](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-detox-a-lananas.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-detox-a-lananas.jpg>)

##  detox smoothie with pineapple 

Hello everybody, 

A detox smoothie is always welcome, is not it? you always need to take care of yourself, and take care while savoring a delicious smoothie, for me it's all the happiness of the world, hihihiih. 

This pineapple and green tea detox smoothie recipe is super easy and delicious, you just have to make your green tea in advance to enjoy a fresh smoothie. For my part, I prepare the green tea at night, and I put in the fridge, I also cut the pineapple slices, like that in the morning, I can prepare my smoothie in less than a minute.   


**detox smoothie with pineapple**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-detox-a-lananas-1.jpg)

portions:  2  Prep time:  2 mins  total:  2 mins 

**Ingredients**

  * 300 ml of green tea 
  * 300 gr of pineapple pieces 
  * 2 limes 
  * 1 C. tablespoon oat flakes 
  * 1 tablespoon of flax seeds 



**Realization steps**

  1. If possible, prepare green tea at night. 
  2. place the oat bran and flax seeds in the blinder bowl and reduce to powder 
  3. then add pineapple and green tea, and mix again, until it becomes smooth. 



[ ![pineapple detox smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-detox-dananas.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-detox-dananas.jpg>)
