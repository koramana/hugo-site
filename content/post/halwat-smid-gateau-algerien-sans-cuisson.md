---
title: halwat smid Algerian cake without cooking
date: '2017-11-30'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid
tags:
- Oriental pastries
- Algeria
- delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau-algerien-facile-halwat-smid.jpg
---
[ ![halwat smid Algerian cake without cooking](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau-algerien-facile-halwat-smid.jpg) ](<https://www.amourdecuisine.fr/article-halwat-smid-gateau-algerien-sans-cuisson.html/gateau-algerien-facile-halwat-smid>)

##  halwat smid Algerian cake without cooking 

Hello everybody, 

Here is an Algerian cake without cooking, easy to make and super delicious, which will not leave you indifferent: halwat smid Algerian cakes without cooking, or smida, or **taminat el qol** . 

This cake halwat smid Algerian cake without cooking is a family recipe, I was lucky to be the first to share on the blogosphere in 2010 (I do not usually sell myself for family recipes which I share with great pleasure), but I am disappointed when I see that this recipe was taken literally by some people who do not want to mention the source that is my blog. 

This halwat smid Algerian cake recipe without cooking was passed on to me from word of mouth, from my step sister, and my step sister gave me the ingredients using measures, and by my care I converted the measurements into gram, then give back to Caesar, which belongs to Caesar. 

**halwat smid Algerian cakes without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau-algerien-sans-cuisson-halwat-smid.jpg)

**Ingredients**

  * fine semolina delicately grilled (expect almost 600 gr) 
  * 200 gr of butter (remove from the fridge) 
  * 100 gr iced sugar 
  * 150 gr of honey (if you like the sweet taste, add a little more) 
  * 2 to 3 tablespoons of orange blossom water (according to your taste) 
  * sugar paste, almond paste and silver beads for decoration. 



**Realization steps**

  1. grill the semolina, just for it to be cooked, but not so that it becomes tanned (a tanned semolina, I do not know which beach?), let cool well   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/230190541.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/230190541.jpg>)
  2. in a bowl, mix butter and icing sugar by hand, 
  3. then add the honey, and continue mixing until your mixture doubles in volume and becomes more sparkling   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/230190561.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/230190561.jpg>)
  4. add the orange blossom water, then the semolina gradually until you have a good paste. 
  5. so this dough should not be too hard 
  6. it is still sticky, remember that it hardens after cooling, so the more the dough is soft the more the cake is melting. so do not be embarrassed if the dough still sticks to your hand. 
  7. then pour this dough on a tray, and shape a square or a rectangle 
  8. let it take a little (almost 30 minutes) 
  9. then cut shapes according to your taste. 
  10. also garnish to your liking, cinnamon buffs can garnish them with cinnamon   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/230190801.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/230190801.jpg>)
  11. As you can add dye, preferable to add before adding the creamy semolina.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/235379611.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/235379611.jpg>)
  12. put in the fridge (15 to 30 min) and your cake is ready to serve 



Note _For people who are afraid that semolina is not cooked well, an easy solution: pass the semolina steamed, separate the good, and then grilled, in this way, you are sure that your semolina is well cooked._ [ ![halwat smid Algerian cake without cooking](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/halwat-smid-taminet-el-qol.jpg) ](<https://www.amourdecuisine.fr/article-halwat-smid-gateau-algerien-sans-cuisson.html/halwat-smid-taminet-el-qol>)
