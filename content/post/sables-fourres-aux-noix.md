---
title: Shortbread filled with nuts
date: '2013-12-13'
categories:
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sables-fourees-aux-noix.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sables-fourees-aux-noix.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sables-fourees-aux-noix.jpg>)

##  Shortbread filled with nuts 

Hello everybody, 

Does it tell you a recipe of Shortbread filled with nuts? Here is one that comes from our dear Lunetoiles, I have the plain of these recipes and I have not yet had time to put you online ... 

I will try to do it as soon as possible, and I do not tell you you are going to regale you. 

Today, as we are always between autumn and winter, the leaves continue to fall (I know it has snowed in a few places) but still this recipe is seasonal, I know that many people who celebrate Christmas , and the end-of-year parties, will find this recipe, just a delight. 

So I pass you this recipe that I am sure you like, and remember, if you have tried one of my recipes send me the picture by going to this page: [ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") do not forget to mention your name, and the name of the recipe, so that I can recognize it. 

**Shortbread filled with nuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sables-fourres-aux-noix-1.jpg)

Recipe type:  Shortbread cookies  portions:  20  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** For garnish: 

  * 120 g of pecan 
  * 40 g maple syrup 

For the sweet dough: 
  * 240 g flour 
  * 120 g of soft butter or half-salt at room temperature 
  * 80 g icing sugar 
  * 40 g ground almonds 
  * 1 medium egg 

For gilding: 
  * 2 egg yolks 



**Realization steps** Prepare the sweet dough: 

  1. Cut the butter into small cubes. 
  2. Put the flour, ground almonds, icing sugar and butter in a bowl, then mix with your fingertips until you get some sort of coarse sand. 
  3. Stir in the egg and knead quickly to obtain a ball of dough. 
  4. Flatten them 2 cm thick, wrap with a cling film and let rest at least 2 hours in the refrigerator. 
  5. Preheat the oven to 180 ° C (th.6). 
  6. Cover a baking sheet with baking paper. 
  7. Prepare a template: Draw a maple leaf on card stock and cut it out. 

Prepare the filling: 
  1. Mix the nuts roughly, add the maple syrup and mix well. 
  2. Flour the worktop and lower the dough to 3 mm thick, turning it regularly to prevent it from deforming. 
  3. Using the template, cut the shortbread with a knife. 
  4. Arrange half of the shortbread on the baking sheet and place 1c. coffee topping in the middle of each of them. Brush the edge with egg yolk and a brush, then put another shortbread on top and lightly press all the edge. 
  5. Brush the whole surface with egg yolk, and bake for about fifteen minutes: the shortbreads should be lightly browned. 



Note _Source:_   
Dessert book happy days of guillemette auboyer and Eve-marie briolat [ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sables-fourres-aux-noix-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/sables-fourres-aux-noix-2.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
