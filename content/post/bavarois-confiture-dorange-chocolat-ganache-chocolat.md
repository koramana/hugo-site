---
title: bavarois orange jam / chocolate / chocolate ganache
date: '2011-12-16'
categories:
- amuse bouche, tapas, mise en bouche
- Bourek, brick, samoussa, chaussons
- Algerian cuisine

---
hello everyone, without pc always, I put you back one of my old recipes ....... As promised, here is the Bavarian recipe that I prepared for my beautiful daughter, on the occasion of her birthday, certainly it lacks decoration, because my flu kept me far from the kitchen, and also I had no head to think about a beautiful decoration. despite all the cake was a real success, a real delight, a wonderful marriage of flavors, I will not hold you too much, I pass you the recipe. so for the assembly of this cake or rather I will say & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

without pc always, I put you back one of my old recipes ....... 

As promised, here is the Bavarian recipe that I prepared for my beautiful daughter, on the occasion of her birthday, certainly it lacks decoration, because my flu kept me far from the kitchen, and also I did not have the head to think about a beautiful decoration. 

despite all the cake was a real success, a real delight, a wonderful marriage of flavors, I will not hold you too much, I pass you the recipe. 

so for the assembly of this cake or rather I will say this dessert, I prepare: 

\- a base without cooking, based on speculoos 

\- a mousse to the taste of the orange 

\- a mousse to the taste of dark chocolate 

\- a chocolate ganache 

we do not make the recipe is really easy. 

speculoos base 

  * 200 gr of biscuits speculoos 
  * 50 gr toasted almonds 
  * 80 gr of melted butter 
  * 50 grams of dark chocolate 
  * 20 gr of candied fruit to the taste of the orange 



mousse to the taste of the orange  : 

  * 200 ml thick cream 
  * 200 gr of orange jam, or even orange marmalade 
  * 100 ml unsweetened orange juice 
  * 2 oranges 
  * 8 grams of gelatin 



dark chocolate mousse: 

  * 300 ml thick cream 
  * 2 egg yolks 
  * 300 ml of milk 
  * 120 gr of sugar 
  * 1 cup of vanilla coffee 
  * 2 tablespoon cornflour 
  * 8 grams of gelatin in foil 
  * 120 grams of dark chocolate 



dark chocolate ganache: 

  * 250 g dark chocolate 
  * 250 ml of fresh cream 
  * 70 g of butter 
  * 4 g of gelatin 



preparation of the speculoos base: 

  1. grind the speculoos to the blender 
  2. grind the almonds 
  3. cut the chocolate into a small piece 
  4. cut the candied fruit to the taste of orange in small piece too 
  5. melt the butter over low heat 
  6. mix all the ingredients 
  7. tamp the mixture with a wooden spoon in a square mold of 20 cm on the side, or a circular mold of 23 cm in diameter 
  8. place in the fridge 



preparation of the orange mousse 

  1. place the gelatin in a little water so that it swells 
  2. diluted orange jam in orange juice 
  3. peel and cut the oranges into small cubes 
  4. pass the mixture of orange jam / orange juice / orange in cube, on low heat for almost 10 minutes. 
  5. squeeze the gelatin, and place it in this hot mixture, and dilute it well over a low heat 
  6. let this mixture warm up. 
  7. put the thick cream in whipped cream 
  8. add the orange mixture to the whipped cream very slowly, to have a well-aerated foam 
  9. for this device based on speculoos 



Preparation of the dark chocolate mousse: 

  1. place the gelatin in cold water so that it swells 
  2. separate the whites from the egg yolks (keep the egg whites in the freezer for next use) 
  3. beat the egg yolks with the sugar, add the cornflour and vanilla 
  4. boil the milk 
  5. pour the hot milk over the egg mixture while whisking 
  6. put the mixture back on medium heat and mix until you see a creamy pastry cream 
  7. add the dark chocolate in piece to this cream while mixing 
  8. squeeze the gelatin and add it to this mixture until it is completely dissolved 
  9. let cool 
  10. whip custard cream into whipped cream 
  11. add the cream pastry cream to the whipped cream, very slowly to have a well-aerated foam 
  12. pour this mousse on the chocolate mousse that will have taken 



Preparation of the dark chocolate ganache 

  1. place the gelatin in the cold water so that it swells 
  2. melt the chocolate in a bain-marie 
  3. heat the cream on a low heat 
  4. add the gelatin until it melts completely 
  5. add one-third of the crème fraiche to the melted chocolate, and mix until the mixture is well homogeneous 
  6. add another one third, and mix until the mixture is homogeneous 
  7. then add the remaining third and mix 
  8. stir in the butter until you have a shiny mixture 
  9. pour this mixture on the dark chocolate mousse 
  10. leave in the fridge for at least 2 hours 



the recipe was beautiful, the taste was very present, well there is no piece left, so your utensils, especially that it is without cooking, and that's why I opt for a delicious Bavarian 

thank you for your comments, and thank you for wanting to subscribe to the newsletter, if you want to be up to date with any new publication, be aware that if you uncheck the article box, you may not have an alert when you publish a article 

merci pour votre visite 
