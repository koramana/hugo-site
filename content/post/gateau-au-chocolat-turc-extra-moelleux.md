---
title: extra sweet Turkish chocolate cake
date: '2017-11-04'
categories:
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- To taste
- Breakfast
- Accessible Kitchen
- Easy recipe
- Pastry
- desserts
- creams

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/11/gateau-extra-moelleux-au-chocolat-recette-turque-1.jpg
---
![extra fluffy chocolate cake, Turkish recipe 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/gateau-extra-moelleux-au-chocolat-recette-turque-1.jpg)

##  extra sweet Turkish chocolate cake 

Hello everybody, 

You think I was going to stop there? If lynda just lived by, I'd have knocked on the door to get my share, but she's in London, and me Bristol, we're more than 180 km away from each other! So solution, make this cake extra chocolate mellow, and enjoy without mercy, Hihhihihih, and I confess, we feasted at home. 

This cake is a killer, it is so good, full of different textures, bright flavors, ouch, ouch ... I did not stop at a piece of salt, and then I come to blame every morning my balance, and swear she no longer measure correctly !!! ???? Go this song and we take it seriously! 

Otherwise, if you're tempted to make this cake, I'll give you Lynda Akdader's video: 

**extra sweet Turkish chocolate cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/gateau-extra-moelleux-au-chocolat-recette-turque_.jpg)

**Ingredients** Chocolate biscuit: 

  * 2 eggs 
  * 1 glass of flour (a glass of 200 ml) 
  * 1 glass of sugar 
  * ½ glass of milk 
  * ½ glass of oil 
  * 1 C. yeast 
  * 1 C. vanilla coffee 
  * 3 c. cocoa 

Chocolate syrup: 
  * 1 glass of water 
  * 1 glass of sugar 
  * 4 c. cocoa 

Cream 
  * 4 c. flour 
  * 3 c. cornflour 
  * 3 glass of milk 
  * 1 C. vanilla coffee 
  * 200 ml liquid cream 
  * 6 c. tablespoons sugar 



**Realization steps** Preparation of the chocolate biscuit: 

  1. Preheat your oven to 170 ° C 
  2. In a bowl, beat eggs, sugar and vanilla sugar. 
  3. Add the oil and the milk and whip again, 
  4. then introduce the sifted flour, cocoa and yeast, mix. 
  5. Pour the mixture into a buttered pan about 22 x 22 cm. 
  6. Bake for 30 minutes at 180 ° C or until the point of a knife comes out dry when inserted inside. 

prepare the syrup: 
  1. boil the water and sugar. after boiling count for 10 minutes and remove from heat 
  2. add the sifted cocoa and stir well to have a nice chocolate syrup, switch to Chinese to avoid lumps. 

prepare the cream: 
  1. in a saucepan, put the milk, flour, cornflour, sugar, vanilla and liquid cream 
  2. stir well to dissolve everything and place the saucepan on medium heat while stirring, to have a smooth and consistent cream like cream pastry. 
  3. let cool. 

mounting: 
  1. remove the cake from the oven, leave in the mold and sprinkle the well with all the prepared chocolate syrup. 
  2. let it absorb. 
  3. take the cream and mix well with a blender foot. then pour it on the cake. you can decorate the top with cocoa. 
  4. leave a cool night, and enjoy! 


