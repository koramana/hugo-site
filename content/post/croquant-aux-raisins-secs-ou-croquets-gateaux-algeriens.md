---
title: crunchy raisins or croquet cakes Algerian
date: '2017-06-18'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquet-gateau-algerien_thumb1.jpg
---
![crunchy Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquet-gateau-algerien_thumb1.jpg)

##  crunchy raisins or croquet cakes Algerian 

Hello everybody, 

as planned, I will post the recipe of my crunchy raisins or croquets, a [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) I like a lot, good crunch [ Algerian biscuits ](<https://www.amourdecuisine.fr/categorie-11814728.html>) and I have to tell you, my crunch has always been delicious, everyone loves it. 

and every time I do them, I'm in such a hurry that I never take pictures. 

I have not forgotten this time, and as always, there are so many photos, I do not know which to choose, and therefore, I post you, 90% ......... ..hum, sorry, I mean 99% of photos taken, hihihihihihihi. 

look here for more [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) . 

and here for more [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) . 

and by the one index of [ Algerian cakes in photos ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)

to go, we go to the recipe:   


**crunchy raisins, croquet Algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/croquant-aux-raisins-secs.jpg)

portions:  50  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 6 eggs 
  * zest of a lemon 
  * 1 cup of vanilla coffee 
  * 300 grs of crystallized sugar (I do not like them too much sugar, I put 280 gr) 
  * 500 ml of table oil 
  * 2 tablespoons of baking powder 
  * 1 nice handful of raisins, which you let swell in a little water (you can put everything you like, almonds, peanuts ...) 
  * flour (allow at least 1 kg, depending on your flour) 

for gilding: 
  * 1 egg 
  * 1 cup of milk 
  * 1 cup of coffee (or a little instant coffee) 
  * 1 pinch of sugar 
  * 1 pinch of vanilla 



**Realization steps**

  1. in a large salad bowl, beat the eggs well, add the vanilla and vanilla sugar, 
  2. slowly add the sugar, then the oil, in a net. 
  3. continue to beat. 
  4. then add a little flour, then the baking powder, then add the raisins, and then continue to add your flour, until you obtain the soft and malleable paste,   
do not put too much flour so that you will not have too much crunch. 
  5. even if to form the chopsticks, you find that the dough sticks to the hand, flour your hands for work, or your hands must be well oiled. 
  6. in a baking tray, place your chopsticks, spacing them well from each other. as in the photos above.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/croquant_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/croquant_thumb.jpg>)
  7. garnish your baguettes according to your taste, then brush with the egg mixture   
(You can put only one egg for gilding, but I add vanilla, to no longer smell the eggs, and the other ingredients are to have a nice crunchy color) 
  8. the choice of the garnish after depends on your taste: sugar, coconut, ground almonds, peanuts, vermicelli in chocolate or in color ...... the list is very long. 
  9. cook your crisp in a pre-heated oven at 180 degrees C. 
  10. dice the release of your cake from the oven, 
  11. cut the cakes, lozenges according to the size you like, for me it is between 1.5 and 2 cm wide. 
  12. to have more crispy, you can put your crunchy, on the side, in a tray and put in the oven, for a small gilding from above and from below. 
  13. allow to cool well, then put in a hermetic box, can be preserved a long duration, and even can be frozen, for use in case of unforeseen visits. 


