---
title: Berkoukes of Laghouat (Mardoud)
date: '2014-12-31'
categories:
- Algerian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-00256-1024x768.jpg
---
[ ![mardoud de laghouat](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-00256-1024x768.jpg) ](<https://www.amourdecuisine.fr/article-berkoukes-laghouat-mardoud.html/img-20140201-00256>)

##  Berkoukes of Laghouat (Mardoud) 

Hello everybody, 

It's a cold duck at home, and these holidays I did all the hot food you can eat to warm up, [ Cassoulet with white beans ](<https://www.amourdecuisine.fr/article-cassoulet-d-haricots-blanc-aux-pieds-de-mouton-loubia-bel-ker3ine.html> "White bean cassoulet with lamb's feet, loubia bel ker3ine") , [ Tlitli ](<https://www.amourdecuisine.fr/article-tlitli-recette-de-cuisine-algerienne.html> "Tlitli, Algerian recipe") , [ chakhchoukha ](<https://www.amourdecuisine.fr/article-chekhchoukha-constantinoise-chakhchoukha-de-constantine.html> "Constantinese Chekhchukhah, Constantine's Chakhchukha") , [ Berkoukes ](<https://www.amourdecuisine.fr/article-berkoukes.html> "Berkoukes بركوكس, burrows") , [ Chicken couscous ](<https://www.amourdecuisine.fr/article-couscous-au.html> "couscous with chicken, Algerian cuisine") ... In any case the children loved all these traditional dishes of Algerian cuisine, it changed them from the lunch box they took with them every day to school. 

Personally I would not be happy to eat every day at noon a cold meal. But what can we do, if the dishes in the canteen is not really joy, and children are not allowed to return at noon at home to eat a good meal, then the holidays We are gorging ourselves, and the menu is on demand, everyone asks what he wants to eat tomorrow at night, and we make an agreement on the lunch. 

In any case I stop with the stories of my lunches, and we return to this recipe Berkoukes de Laghout (Mardoud), a recipe that comes from a friend and reader Suha Bou, and here is what it tells about the recipe: 

__ Here is a traditional recipe from Laghouat called Mardoud or berkoukes de laghouat. It is a Berkoukes Roulait to which is added a plant named "Dwa al khala" a plant of the Algerian desert, which gives the green color to berkoukes (lead pate). Qaund has the recipe, put in a spice mix named chibt elchikh 

I do not know you much about herbs anymore, not Suha, and if you know them, I'll be happy to know more. 

**Berkoukes of Laghouat (Mardoud)**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/mardoud-de-laghouat-1024x680.jpg)

portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 500 g lamb meat 
  * 3 tablespoons of oil 
  * 4 cloves of garlic 
  * 1 tablespoon Ras El hanout   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-00246.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-00246.jpg>)   
(mix of spices and in Laghouat we add a grass called Chibt Alchikh)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-00258-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-00258-300x225.jpg>)
  * 1 tablespoon of red pepper 
  * 1 teaspoon of salt 
  * a handful of chickpeas 
  * a handful of dried beans 
  * a handful of dried apricots (known as Fermesse in regions) 
  * fresh coriander 
  * 3 tbsp concentrated tomato soup 
  * ½ cup of Dwa elkhla coffee is a wild spice   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-002451.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-002451.jpg>)
  * 2 green peppers, 
  * 2 potatoes 
  * 1 big glass of berkoukes (mardoud)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-00248-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/IMG-20140201-00248-300x225.jpg>)
  * some water 



**Realization steps**

  1. place in the pot meat, oil, garlic, pulses, spices, 
  2. let it simmer a little and then cover with water, until the meat becomes very tender. 
  3. add the concentrated tomato, when you are sure that the meat is cooked. 
  4. Add the potatoes cut in 4, apricots, berkoukes, chilli and finally fresh coriander and Dwa el khla. 
  5. Enjoy your meal. 



[ ![mardoud de laghouat](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/mardoud-de-laghouat-1024x680.jpg) ](<https://www.amourdecuisine.fr/article-berkoukes-laghouat-mardoud.html/mardoud-de-laghouat>)
