---
title: indian sauce chicken
date: '2012-03-01'
categories:
- panna cotta, flan, et yaourt

---
hello everyone, here is a delicious dish improvised for today's lunch, mashed potatoes, my husband loves a lot, but not the kids, so I enjoy that they have a little bit of yesterday's dinner, I wanted to accompany the puree with a good chicken, and a very good sauce, and that's when I thought of an Indian chicken marinade, with tandoori massala, and a little natural yoghurt, from which was born the name of the Indian sauce. so without delay I'll give you the delicious recipe. for chicken with Indian sauce: 2 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

here is a delicious improvised dish for today's breakfast, mashed potatoes, my husband loves a lot, but not the children, so I enjoy that they have a little bit of yesterday's dinner, so I wanted to accompany the mashed with a good chicken, and a very good sauce, and that's when I thought of an Indian chicken marinade, with tandoori massala, and a little natural yoghurt, where was born the name of the Indian sauce. 

so without delay I'll give you the delicious recipe. 

for chicken with Indian sauce: 

  * 2 whole chicken legs 
  * 2 cloves garlic 
  * 1 cm fresh ginger 
  * 1 cup of tandoori massala 
  * 3 tablespoons of natural yoghurt 
  * 1 teaspoon of chili powder (hot pepper) 
  * 1/2 teaspoon Coriander / Garlic powder 
  * 1/2 teaspoon salt (add more if needed) 
  * 1/2 glass of water 
  * 2 tablespoons of olive oil 



I presented this chicken with a puree: 

for mashed potatoes: (for 2 people) 

  * 5 medium potatoes 
  * 2 cloves garlic 
  * 1/2 teaspoons of black pepper 
  * salt 
  * 1 tablespoon of butter 
  * 2 portions of fresh cheese 
  * a bit of milk 



Preparation of chicken with Indian sauce: 

  1. in a pyrex mold with its lid if possible, put the chicken pieces 
  2. add yogurt, spices, finely chopped ginger, finely chopped garlic, oil, 
  3. mix a little, cover with water, 
  4. cover and place in a preheated oven 
  5. when cooking the chicken, remove the lid and let the chicken take the golden color 
  6. remove the chicken and let the sauce reduce 



preparation of the puree: 

  1. clean, peel and cut the potatoes in cubes 
  2. place the water in salt water 
  3. add the cloves of garlic cut in half and the black pepper 
  4. let it cook 
  5. towards the end of cooking the potato, remove the excess of the cooking water, and set aside 
  6. using an electric mixer, beat while crushing the mashed potato 
  7. add cheese and butter, whip again 
  8. add 1 tablespoon of milk and a tablespoon of cooking water, until you have a very light but not flowing puree 
  9. you can do without milk, and use only the cooking water 
  10. serve your puree with Indian sauce. 



in any case, the dish was too good, my husband liked it. 

thank you for your comments, and your visits, thank you to all those who continue to subscribe to the newsletter (article publication alert and / or Newsletter) 

bonne journée 
