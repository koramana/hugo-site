---
title: red berry cake / berry summer cake
date: '2013-04-02'
categories:
- Bourek, brick, samoussa, slippers
- diverse cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
- ramadan recipe

---
Hello everybody, 

It's not summer yet, but nothing prevents us from tasting this delicious cake just to give a little ray to the table of the snack, or to taste between friends ... 

This is a nice recipe that shares Lunetoiles with us ... what I wanted to put my little fork in this little piece she offers us. 

For 8 people 

for a tart pan with a removable bottom 

ingredients 

  * 85 g butter at room temperature, a little more to butter a pie pan 
  * 190 g of flour 
  * 2 teaspoons of baking powder 
  * 1 pinch of salt 
  * 175 g + 25 g caster sugar 
  * 1 big egg 
  * 120 ml of milk 
  * 1 tbsp. coffee vanilla extract 
  * 400 g fresh strawberries, hulled and cut in half 
  * red fruits to choose from (pitted cherries cut in half, currants, blackberries, raspberries, blueberries ...) 



Preparation 

  1. Preheat the oven to 180 ° C. Butter and flour a pie pan with a bottom of 28 cm diameter. 
  2. Sift the flour, yeast and salt together and mix in a medium bowl. 
  3. Beat the soft butter and 175 g sugar in the bowl of an electric mixer on medium-high speed until light and frothy, about 2 minutes. 
  4. Reduce the speed to medium-low and mix the egg, milk and vanilla. 
  5. Reduce the blender speed to low and gradually add the flour mixture, stir until the dough is smooth and even. 
  6. Transfer the dough into the buttered pie pan. Arrange the strawberries on top of the dough, cut side down. Add the other berries of your choice. 
  7. Sprinkle with remaining 25 g sugar. 
  8. Bake for 10 minutes. Reduce the oven temperature to 150 ° C. and continue cooking until the cake is golden and firm to the touch, about 50 minutes or until a toothpick inserted in the center of the cake comes out clean. 
  9. Cool in the pan on a rack. enjoy. 


