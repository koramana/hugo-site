---
title: Truffles with coconut
date: '2017-12-12'
categories:
- dessert, crumbles and bars
- Algerian cakes without cooking
- idea, party recipe, aperitif aperitif
- Not rated @en

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/truffes-a-la-noix-de-coco-001.CR2_.jpg
---
[ ![truffles with coconut 001.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/truffes-a-la-noix-de-coco-001.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/truffes-a-la-noix-de-coco-001.CR2_.jpg>)

##  Truffles with coconut 

Hello everybody, 

Before you post the recipe for these delicious coconut truffles, which I did almost a week ago, and that I did not have the chance to post, because I was not at home, and I did not take my laptop with me. 

And finally back home, after a great adventure in London, yes, yes just in London not too far from home, but we were stuck laba, despite us because of climatic conditions and trains canceled each time. 

The first day was sublime, although it was raining afloat, but we had a good time: all day at London's Westminster aquarium, which is not too far from the famous clock: Big Bang, House of Parliament, London Eye ... and Mrs. Tussauds Museum. 

In any case, we did not visit everything, because we must say that the prices are not given all the same, but we can do one thing each visit, it's better, more London is not so far from home. 

So a walk to the aquarium was breathtaking, I took lots of photos, but I do not want to load the article with so many images, if you want, I can put that on my google more. 

**Truffles with coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/truffes-a-la-noix-de-coco-011.CR2_.jpg)

Recipe type:  desserts, truffles  portions:  20  Prep time:  10 mins  cooking:  5 mins  total:  15 mins 

**Ingredients**

  * 200 g sweetened condensed milk 
  * 15 g of butter 
  * 120 g of coconut powder 



**Realization steps**

  1. In a saucepan, mix the butter and the condensed milk. Heat over low heat for about 20 minutes, until the mixture comes off the wall. 
  2. Out of the fire add 100g of coconut. Mix well and let cool. 
  3. Shape the truffles and roll them in the remaining coconut. 



Note Truffles can be kept for 3-4 days.  Coconut Truffles  Ingredients 

  * 200 g sweetened condensed milk 
  * 15 g of butter 
  * 120 g coconut powder 

preparation 
  1. In a saucepan, combines the butter and condensed milk. Heat over low heat for about twenty minutes, until the mixture does not stick any more to the saucepan. 
  2. Off the heat, add 100g of coconut. Mix well and let cool. 
  3. Shape truffles and roll in the rest of coconut. 



Note: 

You can keep it in the fridge for 3 to 4 weeks. 

[ ![truffles with coconut 011.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/truffes-a-la-noix-de-coco-011.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/truffes-a-la-noix-de-coco-011.CR2_.jpg>)
