---
title: Dani's Chinese brioche
date: '2010-03-17'
categories:
- dessert, crumbles and bars
- gateaux, et cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/03/218546381.jpg
---
![mabrioche2](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/218546381.jpg)

##  Dani's Chinese brioche 

I can look from one site to another to find a good brioche, but there is always something missing. 

Come on, I'll give you the ingredients: 

  * 2 tablespoons baking yeast 
  * 400 gr of flour 
  * 50 gr of sugar (I put 70 gr, but next time I would go up to 85 gr), Finally the origin of the recipe must put pearl sugar as a garnish, maybe it's for that there is 50gr of sugar only, I did not have pearl sugar 
  * 1 cup of salt 
  * 70 gr of butter in pieces 
  * 1 and 1/2 cups of vanilla 
  * 1 beaten egg 
  * 210 gr of warm milk 



we put the ingredients in order in the map, and we run the program dough, and we go for 2:20 of kneading, lifting, degassing and still lifted. 

as it is possible to make the recipe to the kneader or then to the hand, if you want, always, one goes through a good kneading, a lifting of at least 45 min, a degassing while kneading, and another lifting of 45 minutes to 1 hour. 

![mabrioche](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/218538071.jpg)

after the sound of my map, I pulled my dough, I degassed a little, and then I spread my dough in a rectangle, on which I sprinkled a little raisin (it's in the original recipe, we can sprinkle with pearl sugar, or chocolate chips) 

the dough is rolled to form a pudding, and cut sections (as is done for Chinese) when placed in a mold covered with parchment paper, here you let your imagination play, depending on the shape of your mold. 

for my part, I made two flowers, in 2 molds of the same size. 

we cover our molds with a cloth, and place in a place protected from the current of air, and leave for a rise of 45 minutes to 1 hour. 

be careful, the duration of the lifting should not exceed 1 hour, because your dough is risking too much, and then breaks after, and so you will miss your brioche. 

decorate your bun, before baking it, with an egg yolk mixed with milk. 

place your mold, in a cold oven (so you do not heat the front part), and cook for 20 min (for my part I arrived at 30 min of cooking, because my brioche did not take a color of the top ) 

![mabrioche1](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/218546161.jpg)

therefore the cooking time depends on your oven. 

![mabrioche3](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/218547011.jpg)

and for the first time, I had a brioche, soft to the touch, very good, and especially a spinning to dream. 

![mabrioche4](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/218548901.jpg)

you can judge for yourself. 

![mabrioche5](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/218549251.jpg)

I can not resist, I stitch the first piece before cooling the brioche. 

![mabrioche6](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/218549781.jpg)

I ate the first petal, then the second ...... .. 

![mabrioche7](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/218552361.jpg)

and then another with milk ............ 

the rest was my husband's share. 

have a good meal. 

et Merci beaucoup a Dani pour cette Brioche 
