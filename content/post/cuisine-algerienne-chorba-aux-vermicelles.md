---
title: Algerian cuisine - vermicelli chorba
date: '2012-04-10'
categories:
- Coffee love of cooking

---
hello everyone, a dish of Algerian cuisine, a warm soup that my father always likes to eat in the holy month of Ramadan, when I was a child, my mother often did, and to be frank, I was doing feet and hands not to eat it, yes, I had my taste of my little age. that's why I never post this recipe, because I do not think I could eat it. then I take advantage that I am with my mother and that she prepares this soup for my father to post it to you 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

a dish of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , a **warm soup** that my father still loves to eat in the holy month of **Ramadan** When I was a child, my mother often did it, and to be frank, I used to go out of my way to avoid eating it, yes, I had my taste for my little age. 

that's why I never post this recipe, because I do not think I could eat it. so I take advantage that I am with my mother and that she prepares this **soup** for my father to post it to you **recipe** . 

ingredients: 

  * chicken or meat 
  * 1 onion 
  * 1/2 grated carrot 
  * 1/2 grated zucchini 
  * oil 
  * 1 and 1/2 cases of canned tomato 
  * 1 handful of chickpeas soaked the day before 
  * 1 bunch of coriander 
  * 1 glass of water with vermicelli 
  * salt, coriander powder, black pepper 
  * 1 cinnamon stick 



for minced meatballs (optional) 

  * 200 grams of minced meat 
  * some brown coriander 
  * a little grated onion 
  * a little egg white to pick up 
  * salt, pepper, coriander powder, cinnamon powder (a pinch) 


  1. clean and cut the meat into cubes or chicken in pieces, 
  2. put in a pot, and add the oil and onion to blend with the coriander branches, 
  3. grated carrot and grated zucchini are added 
  4. add well-chopped coriander, oil, salt, black pepper, and coriander powder, and cinnamon stick 
  5. simmer a little over low heat, add 1/2 glass of water, and cook a little over low heat. 
  6. add after tomato canned diluted in water, let it come back a few minutes, 
  7. add the chickpeas and cover with water, and cook 
  8. prepare the minced meat, and make dumplings. 
  9. when the meat is well cooked, add the minced meatballs 
  10. then at the end add the vermicelli, let it cook just a little, and remove from the heat. 



thank you for your visits and comments, 

thank you to all who continue to subscribe to the newsletter 

bonne journee 
