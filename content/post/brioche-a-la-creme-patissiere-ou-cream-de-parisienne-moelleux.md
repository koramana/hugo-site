---
title: brioche with cream pastry or cream of parisienne fluffy
date: '2017-09-22'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/parisienne-028_thumb1.jpg
---
**brioche with cream pastry or cream of parisienne fluffy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/parisienne-028_thumb1.jpg)

portions:  16  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * 500 gr of flour 
  * 70 gr of powdered sugar 
  * 80 gr of melted butter 
  * 10 gr of salt 
  * 10 gr of baking powder 
  * 2 sachets of instant baker's yeast 
  * a coffee of vanilla extract 
  * 2 eggs 
  * \+ or - 150 ml of milk 

for pastry cream: 
  * 250 ml whole milk 
  * 1 egg 
  * 50 gr of sugar 
  * a coffee of vanilla 
  * 35 gr gr of cornflour 
  * 2 tablespoons of butter   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/2010-06-08-parisienne_thumb1.jpg)



**Realization steps** for pastry cream 

  1. mix all the ingredients together before measuring over a heat you can add a little food coloring to your cream to have a nice color 
  2. after mixing very well put on fire and stir occasionally to thicken cool and set aside for later   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/2010-06-08-parisienne1_thumb1.jpg)

for the dough 
  1. mix flour, sugar, salt, baker's yeast, baking powder, vanilla extract, 
  2. add eggs and melted butter, sand well 
  3. add warm milk to collect the dough 
  4. knead by hand 15 mn and the robot 8 mn 
  5. put the dough in a salad bowl and cover with cling film and allow to double the volume 
  6. degas the dough and let the volume double a second time 
  7. make 18 small dumplings with your dough then flatten them with a rolling pin 
  8. cut the bottom side into 6 strips with a roulette   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/2010-06-08-parisienne2_thumb1.jpg)
  9. put a teaspoon of custard on the upper part, start rolling the dough from the top, ie from the side of the cream to the small strips 
  10. transfer to a plate and let the volume double a third time 
  11. brush with a lightly beaten whole egg and bake at 200 degrees and cook 15 to 20 minutes 



bonne dégustation 
