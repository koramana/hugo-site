---
title: Strawberry Charlotte
date: '2016-11-12'
categories:
- bavarois, mousses, charlottes, agar agar recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/charlotte-aux-fraises-1_thumb1.jpg
---
##  Strawberry Charlotte 

Hello everybody, 

Here is **the strawberry charlotte recipe** that lunetoiles had prepared last year, when she was watching the show " **best pastry chef** "At the time if you remember there was the recipe for the [ pie conversation ](<https://www.amourdecuisine.fr/article-la-tarte-conversation-de-l-emission-le-meilleur-patissier-de-m6.html> "the conversation pie: from the show the best pastry chef of M6") , I remember this recipe had made a big buzz ... I know that this year too, there have been some great recipes from the oubliettes drawer by **Mercotte** ... 

I admit to having been well tempted to realize all these delights, but frankly, after a little thought, I say to myself: "and who will eat all that? "I'm not like those candidates who make the recipe, take a small slice just to see the taste, and come out of the kitchen while building, to come back the next day and find it" nickel "to know where the cakes go after ... Personally, I could not make a cake, to throw his half in the trash ... So I watch the show comfort on my "sofa", I capture interesting information, and here is a beautiful evening that ends without stress ... 

We go back to the strawberry charlotte recipe, and I know that Mercotte will offer the recipe for [ **the royal charlotte** ](<https://www.amourdecuisine.fr/article-charlotte-royale-epreuve-technique-meilleur-patissier.html> "royal charlotte technical event best pastry chef") as they call it here in England, they are going to have jobs for the candidates ... to prepare the biscuit rolled, then to make Bavarian ... 

The strawberry charlotte recipe that lunetoiles proposes to you **Pink biscuits from Reims** that you can simply replace boudoirs as I do, because we do not have these cookies at home.   


**recipe Strawberry Charlotte**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/charlotte-aux-fraises-1_thumb1.jpg)

portions:  10-12  Prep time:  40 mins  total:  40 mins 

**Ingredients**

  * A packet of pink biscuits from Reims 

For the gelled: 
  * 300 ml strawberry coulis 
  * ½ teaspoon of agar agar or 2 sheets of gelatin 

For lemon mousse: 
  * 1 egg 
  * the juice of 1/2 lemon + the zest 
  * 30g of sugar 
  * 20g of butter 
  * ½ cup of agar agar or 2 sheets of gelatin 
  * 180 g whole liquid cream 

For strawberry mousse: 
  * 300g strawberries 
  * 2 egg whites 
  * 120 g of sugar 
  * 40ml of water 
  * 300 g of whipping cream 
  * 1 small cc of agar agar or 4 sheets of gelatin 
  * Strawberries, for decoration 
  * white chocolate. 



**Realization steps**

  1. Pour the strawberry coulis into a saucepan and bring to a boil. 
  2. Add the agar agar or softened gelatin in cold water. Pour into a mold 18 cm in diameter. 
  3. In a cul-de-poule, mix the lemon juice, its zest, the egg, the sugar and the butter 
  4. thicken everything in a bain-marie. 
  5. Add softened gelatin in cold water or diluted agar in 2 tbsp. water and bring to a boil. 
  6. Beat the whipped cream and add it to the warmed lemon custard. 
  7. Pour over the gummy strawberries (well taken). Place in the freezer. 
  8. Puree the strawberries. Bring to a boil and add agar agar or softened gelatin. 
  9. Pour the sugar and water into a saucepan and bring to a boil until 120 ° C. 
  10. Meanwhile, turn the egg whites into snow. Pour the boiling sugar syrup over the egg whites while continuing to whisk to a minimum (avoid the whips so that there is no splashing). 
  11. Continue beating at high speed until cool. 
  12. Add this meringue to the strawberry puree. Add the whipped cream and add to the mixture. 
  13. In a circle of 26cm in diameter, place the biscuits all around and in the bottom (dip those of the bottom in a little red fruit juice). 
  14. I filled the small spaces with pieces and sprinkled with crumbs of biscuits. 
  15. Place the gummy disc and lemon mousse in the center. 
  16. Pour the strawberry mousse and let stand overnight in the refrigerator. 
  17. The next day turn out and decorate with fresh fruit 


