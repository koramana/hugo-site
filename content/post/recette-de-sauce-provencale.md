---
title: Provencal sauce recipe
date: '2014-09-20'
categories:
- dips and sauces

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/riz-sauce-provencale1.jpg
---
![Provencal-sauce](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/riz-sauce-provencale1.jpg)

##  Provencal sauce recipe 

Hello everybody, 

here is a very simple recipe, [ vegetarian recipe ](<https://www.amourdecuisine.fr/categorie-12348553.html>) , that I prepare, when I have a little bit of **cooked rice** , and that I do not want to throw. An express recipe super easy to achieve, a bluffing result. 

You can taste this **simple Provencal sauce recipe** and delicious with a piece of bread, and it's just a treat, then the ingredients for you, for this simple realization:   


**Provencal sauce recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/riz-sauce-provencale-300x225.jpg)

portions:  4  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * [ cooked rice ](<https://www.amourdecuisine.fr/article-riz-a-la-corete-mouloukhiya.html> "Rice with corn, Mouloukhiya")
  * tomatoes peeled and seeded 
  * 1 red and / or green pepper 
  * 1 onion 
  * 1 tbsp with olive oil 
  * 1 tbsp tomato paste 
  * basil 
  * parsley 
  * 1 handful of pitted olives 
  * ½ tablespoon garlic 
  * salt pepper 



**Realization steps**

  1. In a frying pan, sauté the chopped onion, red pepper and green pepper. in olive oil. 
  2. add the tomatoes, the tomato paste diluted in a little water, the garlic, a little basil and parsley, the olives pitted, cut into slices, 
  3. add salt and pepper. 
  4. Simmer on low heat. 
  5. after reducing the sauce, remove from heat. 
  6. Arrange the rice on a plate and pour the provancal sauce over it. 



![rice-sauce-provencal-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/riz-sauce-provencale-11.jpg)

thank you for your comments and visits 

bonne journee 
