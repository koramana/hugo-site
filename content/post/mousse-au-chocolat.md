---
title: chocolate mousse
date: '2014-07-09'
categories:
- dessert, crumbles and bars
- ramadan recipe
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/mousse-double-chocolat-003_thumb.jpg
---
##  chocolate mousse 

Hello everybody, 

I've been preparing for some time now, this delicious dessert, which is a duet of chocolate mousse, a delight that still gives me the desire to plunge a large spoon in it. 

This chocolate mousse is easy to make, and very beautiful to present, especially in this period, during Ramadan evenings, certainly the evenings are very short, but these verrines are light, and pass easily ... 

**chocolate mousse**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/mousse-double-chocolat-003_thumb.jpg)

portions:  4 to 6  Prep time:  20 mins  cooking:  5 mins  total:  25 mins 

**Ingredients** white chocolate mousse layer: 

  * 100 g of white chocolate 
  * 300 ml liquid whole cream 

dark chocolate mousse layer: 
  * 3 eggs + 3 egg whites 
  * 200 g dark chocolate with 70% cocoa 
  * 70 g of butter 
  * 60 g caster sugar 



**Realization steps** first layer: 

  1. Grate the white chocolate into fine chips. 
  2. assemble the chips in a bowl. and put in a bain-marie, Mix remove from the heat as soon as the chocolate is melted. 
  3. pour 100 ml liquid cream into a small saucepan and bring to a boil. Let cool, out of the fire. Add the melted chocolate cream in a thin stream while stirring briskly for 3 minutes. 
  4. Pour in the remaining 200 ml liquid cream. With an electric mixer, work until it is firm. (You can place a bowl for 10 minutes in the freezer before whipping the cream). 
  5. Pour two large spoonfuls of whipped cream into the chocolate and mix quickly with a wooden spoon to obtain a soft chocolate cream. 
  6. Finally, add the remaining whipped cream by gently lifting it. 
  7. Place the mousse in 6 verrines in the refrigerator and let it cool for about 1 hour before serving. 
  8. second layer: 
  9. separate the whites from the yolks of the 3 eggs 
  10. melt the chocolate in a bain-marie 
  11. add the butter in small pieces, mix 
  12. add the egg yolks and whisk to obtain a smooth mixture 
  13. in a salad bowl, whisk the 6 egg whites with sugar 
  14. incorporate them in the first preparation, gently so as not to drop the foam 
  15. pour over the white foam chocolate mousse. 
  16. garnish with dark chocolate grated from above 
  17. Reserve at least 2 hours in the refrigerator 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
