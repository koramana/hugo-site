---
title: galette des kings pralin hazelnuts
date: '2016-01-20'
categories:
- cakes and cakes
- sweet recipes
tags:
- frangipane
- Puff pastry
- desserts
- To taste
- Bakery
- Pastry
- la France

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/galette-des-rois-praline-de-noisettes.jpg
---
[ ![galette des kings hazelnut praline](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/galette-des-rois-praline-de-noisettes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/galette-des-rois-praline-de-noisettes.jpg>)

##  galette des kings pralin hazelnuts 

Hello everybody, 

This year, I did not make slab kings, because my children are not too fans, my husband finds it is always fat, and I am fed up with having to eat everything not to throw .... As a friend once said: we are afraid to throw in the trash, but we become the trash, eating everything that others do not like ... What is right on this point. 

Today it is Nour-riture who shares with us the recipe of her galette des rois, visit her facebook page, she has many delicious recipes. 

And here's what she tells us: I made the "traditional" recipe for puff pastry but I heard a lot of good about the puff pastry "inverted" that I will realize for next time. 

**galette des kings pralin hazelnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/galette-des-rois-praline-de-noisettes.jpg)

Prep time:  60 mins  total:  1 hour 

**Ingredients** For 900g of dough is a large cake or two small: 

  * 400g of flour 
  * 20cl of water 
  * 250g + 50g of butter (good quality AOC or butter "tourage") 
  * 10g of salt 

for the hazelnut cream: 
  * 100g roasted almond powder 
  * 70g of praline (or roasted and crushed hazelnuts) 
  * 1 tablespoon of praline (in paste) 
  * 80g of butter 
  * 80g of sugar 
  * 2 eggs 



**Realization steps** The puff pastry : Make the tempera: the dough. 

  1. In a salad bowl, put the flour, dig a well, pour 20cl of cold water and dissolve the salt. 
  2. Mix by gradually adding 50g of ointment butter until a smooth paste is obtained. 
  3. Cover with film paper and cool for the night.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/pate-feuillet%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/pate-feuillet%C3%A9e.jpg>)
  4. The next day, flourish your work plan with flour, and spread the tempera in a rectangle (a little longer than wide) 
  5. Place in the center 250g butter in strips (or a plate of butter spread between two sheets of parchment paper or food film). The butter should have about the same temperature as your dough. Cover the butter with both edges of the dough, close so that the butter can not come out. 
  6. Blossom the work surface and gently lower the dough with a roll vertically - > spread it in front of you regularly. Count about 2mm thick. 
  7. Fold in 3: first the top part and then the bottom part. Turn the dough a quarter of a turn then lower again (a quarter turn: the opening of the dough is now on your right and will always be on this side so as not to lose you) and lower again and then fold in the same way in 3. This is a simple turn x 2. 
  8. Put it in the fridge for an hour, without forgetting to cover it on contact with film paper. 
  9. Pull out the dough, flush the worktop, place the side of the opening to your right and spread out in front of you, fold back into 3, turn a quarter turn, and start again (this is a simple trick x 2). 
  10. Cover and chill 1 hour. 
  11. For the last lap, we can do a "double" lap: this is the last step. Lower in front of you, of course opening to the right, then fold the top part to the pâ of the dough then fold the bottom part to ⅓, the two ends are in contact without overlapping. Finally fold the whole one on the other like to close a book.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/pate-feuillet%C3%A9e-feuilletage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/pate-feuillet%C3%A9e-feuilletage.jpg>)
  12. Your dough is ready, let it rest for 30 minutes in the fridge before lowering it for our cake. 

cream : 
  1. Mix the sugar and soft butter, add the eggs one by one then add the almonds, hazelnuts and finally the praline. Put the cream in a piping bag and let cool.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Pictures1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Pictures1.jpg>)

Production : 
  1. Roll out the puff pastry, cut out two large circles. 
  2. Put the cream in the hazelnut, leave 1 to 2cm of space with the edge. You can optionally cut a 1-centimeter band on the falls and place it around the slab. 
  3. put some egg yolk on the edges and put the second disc of dough to cover everything. Melt both pasta with your fingers and turn the dough over a sheet of baking paper.   
I forgot to do it and it is true that there are fingerprints so do not forget the next time! 
  4. Brush the dough with an egg yolk with 1 case of water. 
  5. Using a knife (the side that does not cut) gently draw on the slab without perforating it, and streaking the edges ("Chiquetter" the edges) 
  6. Drill a "chimney" toothpick (5/6 holes) so that the steam can escape. 
  7. Put in the freezer 20-25 min (or a few hours in the fridge), then bake in a preheated oven at 170 C for 60min.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/galette-des-rois-fourrage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/galette-des-rois-fourrage.jpg>)
  8. Check that the underside is well cooked, that's how you can judge the cooking. 
  9. Cover with a brush the cake with a little syrup (50g of water / 25g of sugar brought to boiling then cooled) or a case of diluted honey with 1 case of hot water. 
  10. Eating it warm is how I prefer it! 



[ ![galette des rois hazelnut praliné](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/galette-des-rois-pralin%C3%A9-de-noisettes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/galette-des-rois-pralin%C3%A9-de-noisettes.jpg>)
