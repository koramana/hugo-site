---
title: 'boussou la tmessou: melting lozenges with sesame'
date: '2014-07-15'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/losanges-fondants-aux-s%C3%A9sames-boussou-la-tmessou.jpg
---
[   
![rhombus melting at sesames](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/losanges-fondants-aux-s%C3%A9sames-boussou-la-tmessou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/losanges-fondants-aux-s%C3%A9sames-boussou-la-tmessou.jpg>)

##  boussou la tmessou: melting lozenges with sesame 

Hello everybody, 

Another Algerian cake recipe of l'aid el fitr 2013 that I could not post you is the boussou the tmessou, or sesame melting lozenges. 

Boussou la tmoussou is an Algerian cake fondant wish, hence its name which means in French: "kiss it and do not touch it", lol, it's so melting even to the touch, it must be really good to be able to coat with icing sugar without crumbling ... 

I remember when I made this cake melting the first time, I had to coat it immediately out of the oven in icing sugar, so that it sticks, but I do not remember how much piece I had broken , just by lifting it from the cooking tray, fortunately we had already invented at the time the bniouen, lol because all futrecycler ... 

This time in any case, the cake comes from Lunetoiles, and it has done very well,    


**boussou la tmessou: melting lozenges with sesame**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/DSC08130.jpg)

portions:  50  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * A bowl of grilled sesame seeds 
  * 500g of soft butter very slightly soft (especially not melted!) 
  * 1 very clean cc of baking powder 
  * 2 bags of vanilla sugar 
  * Flour in sufficient quantity 
  * icing sugar 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. Sulphurize hotplates 
  3. In the bowl of a robot, mounted with the utensil "foil" or in a container if you make the recipe by hand, thoroughly mix the barely soft butter, thick ointment texture (especially not melted!) With the seeds of sesame and vanilla sugar. 
  4. Work everything until the butter becomes soft and supple, a little whitish.   
We should not see traces or small pieces of butter anymore. 
  5. Then add the yeast, being careful not to put more than the amount indicated on the recipe is 1 cc well shaved. 
  6. Then add the flour gradually while mixing until a paste that rolls into a ball, a little soft but not too much. 
  7. It should be barely sticky, almost not. 
  8. Detail the dough into several small dough pieces, easier to work. 
  9. On a floured work surface or on a sheet of silicone type ROUL'PAT (Demarle) very lightly floured, roll a regular pudding paste not too big and detail lozenges small formats (very important) as if we wanted to make mini makrouts. 
  10. Smooth the tips of the lozenges slightly so that they are more rounded. 
  11. Place the diamonds spaced apart on the previously prepared baking sheet. 
  12. Bake the cookies just a few minutes until they are just slightly pink. 
  13. They should not especially color or brown under penalty of losing their side friable and sandy that one seeks in these cupcakes. 
  14. Once cooked, let them warm a few minutes on the baking sheet without touching them so as not to break them because they are fragile at the exit of the oven. 
  15. In a dish, place a thick layer of icing sugar and roll the cooked lozenges, making sure to adhere the icing sugar all over, compressing gently with the palm of your hand. 
  16. To do so with all the biscuits then repeat the operation once or twice until the cookies are well coated with a "shell" of icing sugar. 



[ ![melting lozenges with sesame boussou the tmessou](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/losanges-fondants-aux-s%C3%A9sames.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/losanges-fondants-aux-s%C3%A9sames.jpg>)
