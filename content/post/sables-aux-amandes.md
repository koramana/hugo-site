---
title: Shortbread with almonds
date: '2017-11-21'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/sables-aux-amandes-032.CR2_1.jpg
---
![shortbread with almonds 032.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/sables-aux-amandes-032.CR2_1.jpg)

##  Shortbread with almonds 

Hello everybody, 

Small fondant biscuits such as shortbread almonds, are always present at home, especially to accompany a good tea while watching TV. For me it is with "Gray anatomy" that I can taste a plate, without realizing .... 

These shortbreads with almonds are too crunchy-melting and super delicious, I hope that the recipe will please you and do not forget to share the photos of your achievements with me! 

You can see another version of shortbread with almond jam on this video, it's a pure delight. 

{{< youtube fdvvKqR >}} 

**Shortbread with almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/sables-aux-amandes-032.CR2_1.jpg)

portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 90 gr of icing sugar 
  * 1 egg 
  * 100 gr of butter 
  * 220 gr of flour 
  * 60 gr of ground almonds 
  * 1 pinch of salt 
  * flaked almonds 
  * a white or an egg yolk 



**Realization steps**

  1. Whip butter, sugar and egg for a cream, 
  2. Add the ground almonds and collect with flour until you have a malleable paste. 
  3. Flatten the dough to 4 to 5 mm thick 
  4. use a cookie cutter to cut small biscuits and place them in a baking tray, not buttered. 
  5. whitewash the cakes with the white or the yolk (I had only one egg white, but with an egg yolk the result is better) 
  6. garnish the cakes with almonds. 
  7. cook in an oven preheated to 160 degrees C, between 10 and 15 minutes. watch the cooking is very delicate cakes. At the end of the oven, allow to cool before unmolding the almond sands. 



![almond sands 034.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/sables-aux-amandes-034.CR2_1.jpg)
