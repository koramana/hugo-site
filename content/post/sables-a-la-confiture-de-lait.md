---
title: sands with milk jam
date: '2017-06-16'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/sables-a-la-confiture-de-lait-1_2-300x247.jpg
---
##  sands with milk jam 

Hello everybody, 

Here is a recipe for super shortbread cookies, a recipe that can serve as a base for this cake, and you can present them in different ways. 

Today I opt for these biscuits with milk jam, yum yum, so I do not stick the pieces in two, in any case these cookies do not fizzle at home ... 

As I do, already there is the neighbor who comes to take his part, as she feels the invading smells of shortbread just out of the oven. 

you can see another version in the silicone molds: 

**sands with milk jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/sables-a-la-confiture-de-lait-1_2-300x247.jpg)

portions:  40  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 250 gr of butter at room temperature 
  * 500 gr of flour 
  * 120 gr of icing sugar 
  * 2 egg yolk 
  * vanilla 
  * ½ teaspoon of baking powder 
  * milk jam 
  * coconut 



**Realization steps**

  1. beat the butter and sugar until the mixture is creamy. 
  2. add the yolks, continue to beat. 
  3. stir in the flour, baking powder and vanilla extract until the dough is malleable. 
  4. spread the dough on a floured work surface, with a thickness of 3 mm 
  5. cut with a cookie cutter of your taste, mine I bought it for almost 1 year and never use, 
  6. place your cakes delicately on a floured baking sheet 
  7. cook 10 to 15 minutes at 180 degrees in my oven, cooking was super fast, 7 minutes was more than enough 
  8. Take out of the oven and let cool down 
  9. for jam of milk, it is enough to cook a box of concentrated sugar milk, closed in a pressure cooker containing almost half its volume of water, without the box of milk floating, cook for 10 to 15 min, and here is the confitire of milk. 
  10. when the small sands have cooled well, decorate with the milk jam and sprinkle with coconut 



bonne journee 
