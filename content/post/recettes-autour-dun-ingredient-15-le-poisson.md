---
title: 'Recipes around an ingredient # 15 the fish'
date: '2016-02-09'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
[ ![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg>)

source: Pinterest 

Hello everybody, 

And here is Patricia's theme: For this 15th edition, I propose you to cook **the fish, in all its states** : raw, poached, smoked, roasted, meatballs, soup ... and all that will inspire you. Let me travel through your recipes! To participate, I let you read the principles of the game below. And I hope many of you will join this 15th edition! [ ![PhotoGrid_1455054449979](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/PhotoGrid_1455054449979-1024x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/PhotoGrid_1455054449979.jpg>)

Thank you girls, and here is the rule of our round: 

_**_Principle of the game_ ** : _

  1. _Choose a seasonal ingredient and accessible to all over the world_
  2. _The godmother will publish this article on her blog between the 4th and 7th of the month by citing the links of the initiators of this game._
  3. _Announce the ingredient that will be the star of the current month._
  4. _Bloggers who want to participate, will contact directly the animator of the game so that she can establish the list of the participants in the game._
  5. _On the 1st of the following month (at the latest), the participants in the game will have to send the titles of their recipes and the link of their blog to the godmother._
  6. _The sponsor will publish the final list of participating blogs and their recipes, which she will send by mail to all participants._
  7. _All the recipes must be put on line the 3rd of the month, with at the bottom the list sent by the godmother of the game._
  8. _The sponsor will then choose among the participants, a blogger who will take over and announce the new ingredient for the following month._



__ _**_New since edition # 12 ..._ ** _

_A Facebook page has been created so that participants can submit the url of their blog and the title of their recipe, to simplify the task of the godmother and participants.  
As soon as your recipe is ready, tell us its title here:   
_

_**_How to participate :_ ** _ _**Previous rounds:** _ ********************************** So you like the subject? At fishing my beautiful (sorry I think we had a man anyway the previous round) And here are the people who want to participate in this round: 5- Soulef of the blog [ Kitchen love ](<https://www.amourdecuisine.fr/>) : with _**salmon ravioli with rosé sauce** _ 9- Schou Shanna without blog 23- Babies Cheris Vansteene **sans blog**
