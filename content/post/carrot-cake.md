---
title: Carrot cake
date: '2007-12-07'
categories:
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/12/198245031.jpg
---
for the recipe en français click here this recipe I pulled it from a forum that I esteem a lot: cuisinetestee, you have to try it. the cake is super light and in addition very good. yes I do not hesitate to give you the recipe: 175g flour 120g sugar 140g grated carrots (about 2 beautiful carrots) 175ml of oil 100g of grapes 3 eggs the zest of an orange 1càc shaved cinnamon 1 / 2càc shaved nutmeg 1càc 1 cc baking soda a pinch of salt preheat the oven to 170 ° C buttered and floured your mold. In a salad bowl mix, sugar, oil and eggs, then & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

this recipe I pulled from a forum that I esteem a lot: cuisinetestee, you have to try it. the cake is super light and in addition very good. 

yes I do not hesitate to give you the recipe: 

  * 175g flour 
  * 120g of sugar 
  * 140g grated carrots (about 2 beautiful carrots) 
  * 175ml of oil 
  * 100g of grapes 
  * 3 eggs 
  * the zest of an orange 
  * 1cake cinnamon 
  * 1 / 2caceless nutmeg 
  * 1c with baking powder 
  * 1cc bicarbonate 
  * a pinch of salt 



![Photo_0065](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/198245031.jpg)

preheat the oven to 170 ° C   
buttered and floured your mold.   
In a mixing bowl, sugar, oil and eggs, then add the shredded carrots and grapes, the grapes and the orange zest. 

![Photo_0067](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/198245201.jpg)

In another bowl mix remaining dry ingredients (flour, yeast, bicarbonate, salt and spices). 

Add the dry ingredients to the first mixture, pour the mixture into the mold and cook in the oven for about 40 minutes (depending on the oven). 

![Photo_0068](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/198245431.jpg)

leave tiedir then demolish 

![Photo_0069](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/198245571.jpg)

I assure you it is really good, and very light. 

![Photo_0070](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/198392451.jpg)
