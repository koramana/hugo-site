---
title: shortbread millionaire recipe
date: '2016-11-11'
categories:
- gateaux algeriens au miel
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/shortbread-millionnaire-7_thumb.jpg
---
[ ![shortbread millionaire recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/shortbread-millionnaire-7_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/shortbread-millionnaire-7_2.jpg>)

##  shortbread millionaire recipe 

Hello everybody, 

One for me, one for you ... one for me and one for you ... 

And I'm sure we're not going to stop at this account, as we say the good accounts make good friends ... and for these shortbreads millionaires, we'll do the accounts, because it's Lunetoiles who has everything eat ... .niff sniff. 

for my part, I wanted to make this recipe for a while now, but because my children do not like too much sweets ... or we will say it another way, I do not let them take a lot of sweets, I know that I will end up eating all alone. 

Of course, no need to say more, you know the consequences, so it's better to abstain, and I thank Lunetoiles who saved me from this slaughter, hihihiihih, she eats, and I publish, ca va va ?? ?? 

![shortbread millionaire recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/shortbread-millionnaire-2_thumb.jpg)

**shortbread millionaire recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/shortbread-millionnaire-5_thumb_1.jpg)

Recipe type:  cake, dessert  portions:  60  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for the shortbread: 

  * 100 g of ramoli butter, at room temperature 
  * 50 g of sugar 
  * 100 g of whole rice flour 
  * 100 g of maizena 

For the caramel: 
  * 100 g of butter 
  * 50 g brown sugar 
  * 397 g sweetened condensed milk 

For chocolates: 
  * 200 g of dark chocolate 
  * 100 g of white chocolate 



**Realization steps** for the shortbread: 

  1. Preheat the oven to 200 ° C 
  2. Beat the butter and sugar in a salad bowl until you have a cream. 
  3. Stir in both flour. Mix. 
  4. Spread this dough in a mold of 24cm x 24cm previously buttered or lined with parchment paper. 
  5. Cook, until the top is golden, in the oven at 200 ° C for 10 to 12 minutes. 

for caramel: 
  1. While cooking the shortbread, prepare the caramel: 
  2. In a saucepan, put all the ingredients. 
  3. Heat over low heat and when the sugar is dissolved, cook for 5 minutes without stirring. The mixture begins to take on a dark color. Get out of the fire. 
  4. Remove the pan from the oven, pour the caramel over the biscuit and let cool completely.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/shortbread-millionnaire-4_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/shortbread-millionnaire-4_thumb.jpg>)

for chocolates: 
  1. Melt white chocolate and dark chocolate in a bain-marie separately. 
  2. When the caramel is firm, cover with chocolate alternating spoonfuls of white chocolate and spoonfuls of dark chocolate. 
  3. Tap the mold for the two chocolate to mingle. Use the tip of a knife to make mottling. 
  4. Let cool until the top is firm, but not in the refrigerator. 
  5. When chocolate starts to freeze, cut out some parts. 



[ ![shortbread millionaire 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/shortbread-millionnaire-3_thumb_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/shortbread-millionnaire-3_4.jpg>)

Source: livre edition marabout : la patisserie facile 
