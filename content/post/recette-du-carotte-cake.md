---
title: carrot cake recipe
date: '2017-01-15'
categories:
- gateaux, et cakes
tags:
- To taste
- Algerian cakes
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/recette-carrot-cake-2.jpg
---
![Carrot cake recipe 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/recette-carrot-cake-2.jpg)

##  carrot cake recipe 

Hello everybody, 

Here is the best Carrot cake recipe you can make. It is a delight that my children often ask me to taste it. Frankly, I must admit that it is a very economical recipe, if you do not put nuts in it, plus it's a super easy recipe to make. 

Carrot cake is a super sweet cake, it is just to die for, and frankly, you will never imagine that there are carrots in it, if we do not tell you! You really have to test it to know what is good about this cake, and you can see this video of the realization of the carrot cake recipe: 

**carrot cake recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/recette-carrot-cake-3.jpg)

portions:  12  Prep time:  30 mins  cooking:  70 mins  total:  1 hour 40 mins 

**Ingredients**

  * 4 eggs 
  * 400 gr of powdered sugar 
  * 400 ml of table oil 
  * 3 c. coffee baking powder 
  * 400 gr grated carrots 
  * 400 gr of flour 
  * 100 gr of grapes (optional) 
  * 100 gr of crushed walnuts (optional). 
  * ½ c. ground coffee cinnamon powder 
  * ½ teaspoon of spices for gingerbread. 

for cream cheese (on the video the cream is for 2 layers only) 
  * 570 gr iced sugar 
  * 450 gr of philadelphia cheese 
  * 120 gr of butter at room temperature. 
  * 1 C. coffee extract of vanilla. 



**Realization steps**

  1. preheat the oven to 170 ° C 
  2. butter and flour a mold 22 cm in diameter 
  3. In a bowl mix eggs, oil, sugar and vanilla extract, 
  4. In another bowl mix the dry ingredients flour, baking powder, and spices. 
  5. Add the dry ingredients to the first mixture 
  6. add carrots, raisins and nuts. 
  7. pour the mixture into the pan and bake in the oven at 170 degrees, between 70 and 80 minutes (depending on the oven). 
  8. let cool down and then unmould 

preparation of the cream cheese: 
  1. whip the cheese a little to give it a creamy appearance (the cheese should be at room temperature) 
  2. add the sugar, the butter at room temperature, the vanilla extract. 
  3. whip until you have a creamy cream. Place in the fridge. 
  4. Mounting: 
  5. when the cake has cooled, cut it in 3 layers, and garnish each layer with one third of the cheese cream. 
  6. garnish according to your taste. 



![Carrot cake recipe 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/recette-carrot-cake-1.jpg) ,  ,  ,  , 
