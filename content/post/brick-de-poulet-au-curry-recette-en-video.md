---
title: Chicken Brick curry / recipe video
date: '2013-06-28'
categories:
- Algerian cuisine
- dessert, crumbles and bars
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/bourek-de-poulet-au-curry.CR2_1.jpg
---
![Chicken Brick curry / recipe video](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/bourek-de-poulet-au-curry.CR2_1.jpg)

##  Chicken Brick curry / recipe video 

Hello everybody, 

For lunch, my husband asked me for a light meal, something easy and fast, because we had to go out together to make some small purchases .... 

The first recipe that came to mind was bricks, and it's chicken bricks with curry are a delight that I had once realized, at random, that my husband loved a lot, and that I did not have the chance to redo ... 

it's really delicious bricks, the taste is unique ... 

You will see the video, the recipe is super easy and very fast, I did not even prepare the ingredients in advance, except for the chicken, remaining from the meal the day before. 

![brig-of-the-chicken-curry - entry-to-Ramadan-2013.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/brick-de-poulet-au-curry-entree-pour-ramadan-2013.CR2_1.jpg)

**Chicken Brick curry / recipe video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/brick-de-poulet-au-curry-recette-de-ramadan-2013.CR2_11.jpg)

portions:  2  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 8 leaves of bricks 
  * pre-cooked chicken pieces 
  * 1 glass of frozen peas (a glass of 220 ml) 
  * 1 medium onion 
  * some parsley leaves 
  * 100 ml of fresh cream 
  * salt, black pepper, curry 
  * 1 tablespoon of olive oil 
  * Oil for frying. 



**Realization steps**

  1. if you do not have any chicken already cooked, boil a piece of chicken with a little salt, black pepper, and curry, you can add some oil, onion, and a lauriet leaf to give more flavors. 
  2. cut the onion into small pieces, and sauté in a little oil. 
  3. when the onion becomes translucent, add over the peas, and cover. 
  4. then add the chick pieces, the chopped parsley, the salt, the black pepper, and the curry. 
  5. combine well this mixture, and add over cream. 
  6. fill the pastry sheets with this mixture, and fry in a hot oil bath. 
  7. drain well on paper sopalin before serving warm. 



Method of preparation: 

{{< youtube s78dS_HDcuQ >}} 
