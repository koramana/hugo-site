---
title: chicken tagine with Jerusalem artichokes, batata terfas white sauce
date: '2017-12-09'
categories:
- Algerian cuisine
- Cuisine by country
tags:
- Easy recipe
- Algeria
- dishes
- Full Dish
- Ramadan
- Ramadan 2017
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/12/tajine-de-poulet-aux-topinambours-1-779x1024.jpg
---
![chicken tagine with Jerusalem artichokes, batata terfas white sauce](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/tajine-de-poulet-aux-topinambours-1-779x1024.jpg)

##  chicken tagine with Jerusalem artichokes, batata terfas white sauce 

Hello everybody, 

If I tell you that I have not eaten this chicken tagine with Jerusalem artichokes for over 13 years! My God, since I've been here in England, I've been deprived of many recipes that have rocked my childhood. 

Among the ingredients that I do not find easily here in England, the Jerusalem artichoke! Already, I tried to find the name in English: Jerusalem artichoke ??? So between us we had to dig our heads to give this name ... Anyway, find the name in English, did not really help me, because I searched everywhere to find Jerusalem artichoke, and hardly that I found online, and now can not find any more, yet it's the season. 

In any case, I was lucky that the friend of my husband brought some grams with him from Algeria, and not knowing how to prepare, he asks my husband that I make him a dish with, and that well, because I prepared the recipe as he liked it by following the video of my friend Lynda Akdader: 

**chicken tagine with Jerusalem artichokes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/tajine-de-poulet-aux-topinambours-2.jpg)

**Ingredients**

  * 1 kg of Jerusalem artichoke 
  * 3 c. tablespoon of olive oil 
  * 500 g chicken drumsticks 
  * 1 onion rappé 
  * 1 handful of precooked chickpeas 
  * ½ teaspoon of paprika 
  * ½ c. cinnamon 
  * Salt, black pepper 
  * 1 liter of boiling water 
  * To bind the sauce: 
  * 1 egg yolk 
  * Chopped parsley 
  * Juice of half a lemon 



**Realization steps**

  1. fry the chicken chicken drumsticks in the oil to give them a golden color 
  2. add the chopped onion and sauté until it becomes translucent. 
  3. season with black pepper, paprika and a little salt. 
  4. cover with boiling water. 
  5. clean and peel the Jerusalem artichokes, when the chicken and half-cooked, introduce the Jerusalem artichokes in cubes 
  6. when it's cooked, introduce the chickpeas and prepare the binder, mix the egg yolk, parsley, lemon juice and a little sauce that you have collected at the front. 
  7. add the binder, let it come back for 5 minutes and remove from the heat. 


