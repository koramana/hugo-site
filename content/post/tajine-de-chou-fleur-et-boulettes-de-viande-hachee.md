---
title: tajine with cauliflower and minced meatballs
date: '2017-03-15'
categories:
- Algerian cuisine
- houriyat el matbakh- fatafeat tv
- recette de ramadan
tags:
- Full Dish
- Ramadan 2015
- Ramadan
- Algeria
- dishes
- Chicken
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-de-chou-fleur-et-boulette-de-viande-hach%C3%A9e-1.jpg
---
[ ![tajine with cauliflower and minced meatballs](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-de-chou-fleur-et-boulette-de-viande-hach%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-chou-fleur-et-boulettes-de-viande-hachee.html/tajine-de-chou-fleur-et-boulette-de-viande-hachee-1>)

##  tajine with cauliflower and minced meatballs 

Hello everybody, 

Here is a very good dish for fans and fans of cauliflower ... For dinner today, we feasted on tasting this tajine of cauliflower and meatballs with a little chicken and white pea sauce chick ... 

In any case, if you are looking for good recipes I advise you to take a look at this index: [ recipes and dishes from ramadan 2015 ](<https://www.amourdecuisine.fr/article-plats-du-ramadan-2015.html>)   


**tajine with cauliflower and minced meatballs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-de-chou-fleur-et-boulette-de-viande-hachee.jpg)

Cooked:  Algerian  Recipe type:  dish  portions:  4-5  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** White sauce: 

  * pieces of chicken depending on the number of people (but it is optional, because there are meatballs minced) 
  * 1 medium onion 
  * 1 crushed garlic clove 
  * a quarter of a bunch of chopped parsley, 
  * salt, black pepper, ginger, 1 pinch of cinnamon, coriander powder. 
  * table oil 
  * chickpeas in a box. 

minced meatballs: 
  * 200 to 300 gr of minced meat 
  * 1 crushed garlic clove 
  * chopped parsley 
  * 2 tablespoons of rice cooked in a little salt water 
  * salt, black pepper, cumin 
  * 1 egg yolk 

cauliflower roses: 
  * 1 medium cauliflower 
  * 50 gr of grated cheese 
  * parsley 
  * 1 clove of garlic 
  * 2 eggs 
  * salt and black pepper 



**Realization steps** preparation of cauliflower roses: 

  1. cut small flowers of the cauliflower, 
  2. steam them, 
  3. crush them roughly with a fork, 
  4. whisk the eggs, stir in the grated cheese, parsley, garlic, salt and pepper, 
  5. Stir in the cauliflower 
  6. fill the mold cavity with this mixture (you can butter and flour your mold) 
  7. cook for 30 minutes in a preheated oven at 180 ° C 

for the white sauce: 
  1. fry the chopped onion in a little oil, 
  2. add the garlic, the chicken, and season with ginger, a little coriander powder, a pinch of cinnamon, salt and pepper 
  3. let it simmer, and cover with water, and cook. 

prepare the minced meatballs 
  1. mix the meat, crushed garlic, chopped parsley, salt, black pepper, add rice and egg yolk, and gently knead 
  2. form medium balls, and fry them in a little oil, just to give a nice color. 
  3. Once the chicken is cooked, remove the sauce, put in the meatballs and chickpeas. 
  4. let reduce the sauce just a little, and here is your dish and ready. 
  5. present by decorating the sauce with the cauliflower roses. 



[ ![tajine with cauliflower and minced meatballs](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-de-chou-fleur-et-boulettes-de-viande-hach%C3%A9e-2.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-chou-fleur-et-boulettes-de-viande-hachee.html/tajine-de-chou-fleur-et-boulettes-de-viande-hachee-2>)
