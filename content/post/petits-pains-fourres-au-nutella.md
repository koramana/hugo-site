---
title: rolls filled with nutella
date: '2015-02-12'
categories:
- Buns and pastries
- Mina el forn
tags:
- To taste
- Pastry
- Delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/11/2010-11-10-parisienne2_thumb.jpg
---
[ ![2010-11-10 parisian2](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/2010-11-10-parisienne2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2010/11/2010-11-10-parisienne2_2.jpg>)

##  rolls filled with nutella 

Hello everybody, 

I told you that I will not stop making brioche rolls, this time I made rolls filled with nutella with a beautiful trellis-shaped filling. 

It's a recipe similar to Parisian cream, or buns filled with cream pastry, but here in an easier and faster version, the rolls are filled with Nutella, you can see the recipe of [ homemade nutella ](<https://www.amourdecuisine.fr/article-nutella-fait-maison.html> "Homemade Nutella") . Now for the fodder, let your imagination, seasonal jam, [ pate a speculoos ](<https://www.amourdecuisine.fr/article-la-pate-a-speculoos-maison-hommage-a-micky-63142494.html> "pate of speculoos, homemade recipe") , or why not cream praline ... I already fund just thinking about this idea. 

Without further ado I give you the recipe:   


**rolls filled with nutella**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/parisienne-024_thumb.jpg)

Recipe type:  bakery  portions:  12  Prep time:  45 mins  cooking:  20 mins  total:  1 hour 5 mins 

**Ingredients**

  * 180 ml of milk 
  * 50 g of soft butter   
(The next I will add a little more) 
  * 3 eggs 
  * 5 g of salt 
  * 60 g caster sugar 
  * 500 g of flour 
  * zest of a lemon 
  * 1 tablespoon dried yeast 
  * 2 tablespoons vanilla powder 
  * Nutella 



**Realization steps** for the pasta: 

  1. Put all the ingredients in the MAP, run the dough program and let it rise. 
  2. at the end of the program, still degass your dough, turn it into a ball of 70 gr each, 
  3. spread out each ball in a small rectangle, pass the roulette lattice on one side, and fill the other side Nutella as you see in the photo   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/2010-11-10-parisienne_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2010/11/2010-11-10-parisienne_thumb.jpg>)
  4. cover the stuffing with the non-lattice edge and fold over the lattice edge, place on a baking sheet covered with baking paper. 
  5. do so until exhaustion of the dough. 
  6. make the gilding with the yellow mixture of egg and vanilla. 
  7. let it rise. 
  8. preheat the oven to 170 degrees C, and cook your Parisian 
  9. tepid tepid, I always like to put the rest in the freezer, so that we have a desire at home we just thaw and enjoy.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/2010-11-10-parisienne3_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2010/11/2010-11-10-parisienne3_thumb.jpg>)



Note the crumb was beautiful, these little Parisian was a real delight 

thank you to all those who continue to subscribe to the newsletter of my blog 

bonne soirée 
