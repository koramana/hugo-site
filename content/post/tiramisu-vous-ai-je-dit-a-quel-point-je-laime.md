---
title: Tiramisu……. Did I tell you how much I love him?
date: '2010-01-31'
categories:
- amuse bouche, tapas, mise en bouche
- crepes, gauffres, beignets salees
- diverse cuisine
- idee, recette de fete, aperitif apero dinatoire
- Dishes and salty recipes

---
you like the recipe, you want to share it with your friends on facebook, click on the small blue F at the bottom of the article I like the tiramisu, it happens to me to do it twice a month (do not ask not too much about my weight, I can not answer you, hihihihi) I always made the recipe with eggs in it, recipe here but this time I was tempted by the recipe I found at linda, a very delicious recipe, that I love me and the children then, I give you the recipe of linda: ingredients: - biscuits & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

you like the recipe, you want to share it with your friends on facebook, click on the small blue F at the bottom of the article 

I really like tiramisu, it happens to me twice a month (do not ask too much about my weight, I can not answer you, hihihihi) 

I always made the recipe with eggs inside, recipe  [ right here  ](<https://www.amourdecuisine.fr/article-25345349-6.html#comment41385870>)

so, I give you the recipe for linda: 

ingredients:   
\- biscuits with spoons (for me a box of 36).   
-250g of mascarpone.   
-2 tablespoons milk.   
-50cl of whole liquid cream very cold.   
-200g caster sugar.   
-20 cl of warm coffee.   
-2c cocoa soup 

preparation:   
\- Add liquid cream to chantilly by adding 150g of sugar, mix the milk with mascarpone by adding the 50g of sugar, add the whipped cream to the mascarpone in three times without whipping this appliance in the fridge. 

prepare the coffee take a large plate or dish as you wish or even verrines as I did on the pictures, so dip the biscuits with spoons one by one by placing them side by side. 

put a layer of cream on the biscuits done so three times and decorate the top with a piping bag (optional) sprinkle cocoa with a sieve. cool for one night to have a good performance of the cream but if you are in a hurry put it in the freezer for 3 hours. 

Bonne dégustation 
