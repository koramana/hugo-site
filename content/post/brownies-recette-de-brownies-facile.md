---
title: brownies, easy brownies recipe
date: '2013-02-28'
categories:
- Moroccan cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/brownies-aux-cacahuetes.jpg
---
[ ![peanut brownies](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/brownies-aux-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/brownies-aux-cacahuetes.jpg>)

Hello everybody, 

want chocolate, here is a recipe too good, super easy, and above all ... chocolate, hihihihi. in truth I prepare these brownies for another recipe, but I think I have to redo it because the children have nothing to leave, so it's delicious, mellow, and chocolateeeeeeeeeeeeeeeeeeeeeeee. 

so let's go for this delicious recipe: 

  * 3 eggs 
  * 125 g caster sugar 
  * 60 g flour 
  * 250 g of dark chocolate pastry 
  * 150 g of butter 
  * 100 g of peanuts 



[ ![2012-01-23 Brownie aux cacahuetes1_2](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/2012-01-23-brownies-aux-cacahuetes1_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/01/2012-01-23-brownies-aux-cacahuetes1_2.jpg>)

the method: 

  * preheat the oven to 180 ° C 
  * whisk the eggs with the sugar and 
  * incorporate the flour 
  * Melt the chocolate with the butter in a bain-marie, while mixing. 
  * add the melted chocolate to the preparation then the dried fruits (I preferred not to mix the dried fruits but to sprinkle them on the surface of the cake) 
  * pour the mixture into the buttered and floured mold or lined with baking paper. 
  * put in the oven for 25 to 30 minutes 
  * let cool before turning out 



[ ![peanut brownie](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/brownie-aux-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/brownie-aux-cacahuetes.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
