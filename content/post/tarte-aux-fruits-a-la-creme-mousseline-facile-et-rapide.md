---
title: fruit tart with easy and quick muslin cream
date: '2014-07-11'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/pizza-et-tarte-aux-fruits-013_thumb.jpg
---
![fruit tart with easy and quick muslin cream](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/pizza-et-tarte-aux-fruits-013_thumb.jpg)

##  fruit tart with easy and quick muslin cream 

Hello everybody, 

Well taken today, that's why I dig up this recipe for fruit pie cream muslin archives of my blog (the proof on the photos, the address of my old blog, hihihihi) 

before yesterday I made a delicious [ yarrow ](<https://www.amourdecuisine.fr/article-millefeuilles-fait-maison-mille-feuilles-114765900.html>) , and as I had a little bit of [ custard ](<https://www.amourdecuisine.fr/article-creme-patissiere-108845862.html>) , that I could easily taste with the spoon, but ... I said to myself I'll make another recipe with ... No better than a fruit pie, and as there was not enough cream anyway, a passage to the [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline-108846274.html>) will do the trick .. 

In the end, here is a very good fruit pie with easy and quick chiffon cream, well garnished with a very creamy layer of cream muslin.   


**fruit tart with muslin cream / easy and fast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pizza-et-tarte-aux-fruits-009_thumb1.jpg)

portions:  8  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast")

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 
  * boxed fruits (or fresh even better) what I had on hand (canned pineapple, fresh strawberries, fresh kiwis, peaches in box, apricot in box) 
  * [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline.html> "muslin cream \(ideal for strawberry\)")
  * for the muslin cream: 

the cream muslin: 
  * 250 gr of [ custard ](<https://www.amourdecuisine.fr/article-creme-patissiere.html> "custard")
  * 60 gr of butter (I have not put too much butter, usually it is with 120 gr anyway) 



**Realization steps**

  1. cook the dough, and at the end let cool a little, before decorating it at your leisure 



bisous 
