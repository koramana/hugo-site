---
title: verrine cheesecake with raspberry jam
date: '2016-10-01'
categories:
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cheesecake-facile-en-verrines_thumb1.jpg
---
![verrine cheesecake with raspberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cheesecake-facile-en-verrines_thumb1.jpg)

##  verrine cheesecake with raspberry jam 

Hello everybody, 

Verrine cheesecake with raspberry jam: pretty colors are not these verrines, well you figure it's a cheesecake without cooking, has raspberry jam, huuuuuuum 

yes, all good tasty, these desserts known here in England, or even in America, under the name of trifles (pronounced trayfole, hihihihi) ... 

what is good in trifles, and that there are no recipes to follow, or method to follow, let your imagination, place all that pleases you in your verrines, and voila sublime trifles. 

so, these great raspberry-free cheesecake, is a Lunetoiles recipe, so many of you ask me, it's where Lunetoiles's blog is, who Lunetoiles ???? 

Lunetoiles is a longtime friend (at least since the beginning of my blog) and who finds a lot of fun to make cakes, really exciting desserts .. 

Besides, she always amazed me, and I am always surprised by opening an email from home, with a beautiful photo album, I do not even know what to choose, or what recipe you put before another recipe…. 

Lunetoiles does not have a blog, she likes to post on my blog, for my great happiness, you can say that Lunetoiles is my teammate on this blog, I will very soon, you make an article gathering all these recipes found on my blog ... 

so we go back to his cheesecakes? 

**verrine cheesecake with raspberry jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cheesecake-facile-en-verrines-6_21.jpg)

portions:  4 

**Ingredients**

  * A mixture of crushed cookies 
  * 1 packet of Philadelphia cheese or another cheese 
  * 200 ml sweetened condensed milk (1/2 box) 
  * ½ tablespoon lemon juice 
  * 2-3 tbsp. butter 
  * raspberry jam 



**Realization steps**

  1. Reduce the powdered biscuits, melt the butter and mix to have a good crumble. 
  2. Beat the condensed milk with the philadelphia cheese for 2-3 minutes, then add the lemon juice. 
  3. Place the equivalent of 1 tablespoon of raspberry jam in the bottom of the verrines. 
  4. Then pour over the biscuit / butter crumble. 
  5. add the cheese filling. 
  6. Repeat the process and finish with jam. 
  7. Keep it in the fridge and serve chilled with fresh chocolate chips and raspberries on top. 


