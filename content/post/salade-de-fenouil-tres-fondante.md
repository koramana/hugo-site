---
title: fennel salad very melting
date: '2013-12-12'
categories:
- panna cotta, flan, and yoghurt
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/besbes-004a-1024x768.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/besbes-004a-1024x768.jpg)

Hello, 

I'm Malika, I'm always in my kitchen going to do something for my little family, I get a lot of your blog Soulef, besides I made almost 50% of your recipes. 

Today and for the first time, I will share on the internet, one of my favorite recipes of the season, at home, we love the fennel, so I share with you this fennel salad, very easy.    


**fennel salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/besbes-004a-1024x768.jpg)

Recipe type:  salad  portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 2 fennels 
  * salt 
  * black pepper 
  * 1 clove garlic crushed 
  * olive oil 
  * vinegar. 



**Realization steps**

  1. clean the fennels, and cut them in length. 
  2. season with a little salt, and steam until the fennel is tender in the mouth. 
  3. prepare a dressing with oil, salt (not too much) black pepper, and vinegar. 
  4. add the garlic in very small pieces. 
  5. remove the fennel from the heat, let drain a little of its water. 
  6. put it in the sauce, and let marinate well. 
  7. serve fresh, with olives, tomatoes, eggs, or according to your taste. 



![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/salad-008-1024x768.jpg)

I thank Malika, who has used the system: propine your recipe. I hope she will tell us more about her experience, was it easy, to put the recipe online with this system: 

[ ![suggest your recipe.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/proposez-votre-recette.bmp-300x87.jpg) ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

j’espere que vous aussi, vous allez adopter ce systeme, et que vous allez essayer cette experience de mettre un recette propre a vous, avec vos photos, je vous dis merci, et a une prochaine recette. 
