---
title: shortbread with jam and almond filling
date: '2014-04-13'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/sabl%C3%A9s-a-la-confiture-et-garniture-aux-amandes-.jpg
---
![shortbread with jam and almond filling](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/sabl%C3%A9s-a-la-confiture-et-garniture-aux-amandes-.jpg)

##  shortbread with jam and almond filling 

Hello everybody, 

yum yum for those yummy shortbread jams and almond filling I'm going to share with you today. 

This recipe that Lunetoiles had passed me a long time ago, and that she had made on the occasion of the feast of the past Eid. 

These shortbreads are super melting, garnished with this layer of almonds, a layer of macaroons very easy to make, and that we decorate at the end with a spoon of jam.   


**shortbread with jam and almond filling**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/sable-a-la-confiture-et-aux-amandes.CR2_.jpg)

Cooked:  Algerian  Recipe type:  dessert  portions:  50  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** For the shortbread dough: 

  * 350 g flour. 
  * 150 g softened butter 
  * 2 egg yolks 
  * 1 teaspoon of vanilla extract 
  * 1 pinch of salt 
  * 100 g icing sugar 

For the almond filling: 
  * 200 g of almond powder 
  * 250 g icing sugar 
  * 2 egg whites 
  * jam of your choice 



**Realization steps**

  1. Work the butter and sugar in the ointment, add the vanilla and the pinch of salt. 
  2. Add the egg yolks and the flour and then pick up the dough and reserve it under a cling film. 

Prepare the second dough: 
  1. Mix the almond powder and icing sugar in a food processor and strain them through the sieve. 
  2. Add the egg white one by one gradually mixing with the spatula, you must get a soft dough that does not run. 
  3. Preheat the oven to 170 ° C 
  4. Take the shortcrust pastry and spread it on a floured plan to a thickness of 3 mm. 
  5. Detail patties with a 5cm round cookie cutter and place on a baking sheet covered with baking paper.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/gateau-sabl%C3%A9s-aux-amandes.jpg)
  6. Decorate the edge each cake (thus to form a circle) of the almond filling using a piping bag with a fairly large fluted socket. 
  7. Bake for about 20 minutes. 
  8. Let your shortbreads cool and garnish the jam medium. 



merci 
