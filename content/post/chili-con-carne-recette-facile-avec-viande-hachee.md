---
title: chili con carne easy recipe with minced meat
date: '2014-03-15'
categories:
- Algerian cuisine
- Cuisine by country
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-022.CR2_thumb1.jpg
---
[ ![chili con carne easy recipe with chopped meat 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-022.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-022.CR2_thumb1.jpg>)

##  chili con carne easy recipe with minced meat 

Hello everybody, 

chili con carne easy recipe with minced meat: here is a recipe very much appreciated by my husband, chili con carne this easy recipe with minced meat, well everything that is spicy, is a good recipe for my husband, and when it contains some minced meat, and dried beans, what more do you want ??? !!! 

and it is hardly possible that I could take these photos, because it was the one to press to me: it is cold now, quickly, quickly, I must go out ... 

in addition I only prepare this dish for him, because I do not like spiciness ... and if I tell him I do it without spice and I add it at the last minute, well, he does not like not that, hihihihi 

**chili con carne easy recipe with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/chili-con-carne-011_thumb1.jpg)

Recipe type:  dish  portions:  4  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients**

  * 200 g of red beans 
  * 200 g ground beef 
  * 2 onions 
  * 1 clove of garlic 
  * ½ red pepper (spicy here, if you do not like, just sweet red pepper) 
  * 500 g tomatoes 
  * 2 teaspoons chili powder 
  * ½ teaspoon powdered cumin 
  * oil 
  * salt 



**Realization steps**

  1. soak the beans in cold water for 12 hours. (I do it at night) 
  2. in the morning, drain and put in a casserole. fill the casserole with enough cold water and cook until the beans become tender. 
  3. slice the onions, and place them in a pan. 
  4. cut the red pepper into cubes and chop the garlic. 
  5. add the garlic and peppers to the onions, introduce the oil. 
  6. sweat a few minutes. 
  7. Add the minced meat. 
  8. cook well. 
  9. when the beans are cooked, drain and put back in the casserole. 
  10. pour over the minced meat sauce. 
  11. add over the seeded and crushed tomatoes. 
  12. season with spices and salt. 
  13. add a little water, cook for about 30 minutes. Check the cooking 
  14. Serve hot. 



#  chile con carne / an easy recipe 

Ingredients 

  * 200 g red kidney beans 
  * 200 g of minced beef 
  * 2 medium onions, chopped 
  * 1 garlic clove 
  * ½ red pepper 
  * 500 g of tomatoes 
  * 2 teaspoons hot chili powder 
  * ½ teaspoon ground cumin 
  * oil 
  * salt 

preparation 
  1. soak the beans in cold water for 12 hours at least. 
  2. In the morning, drain the beans and place them in a saucepan. fill the pot with enough water and cook until the beans get tender. 
  3. Place a large pan-type pan on a medium high heat, add oil and the chopped onion. 
  4. Halve the red pepper, remove the stalks and seeds and roughly chop. 
  5. add the chopped garlic and peppers to the onion. 
  6. Stir times to be sweet and lightly colored. 
  7. Add the minced beef, breaking any larger chunks up with a wooden spoon. 
  8. when the beans are well cooked, for over the sauce of minced meat. 
  9. add now the chopped tomatoes, 
  10. season with a good pinch of salt and pepper. Bring to the boil and turn the heat down to a simmer with a lid slightly askew. 
  11. add a little water and cook for about 30 minutes. 
  12. Serve hot. 



You have enjoyed this recipe of chili con carne easy recipe with minced meat, and you realize it, do not forget to send me the photo by following this link: [ my recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") . 
