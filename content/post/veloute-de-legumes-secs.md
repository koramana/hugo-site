---
title: Cream of dried vegetables
date: '2011-01-05'
categories:
- Café amour de cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/veloute-de-legumes-secs-2_2.jpg
---
##  Cream of dried vegetables 

Hello everybody, 

I love velvety soups and creamy soups, and when it's cold or fasting, I love to break the fast with a comforting soup. 

This is the case for this soup of dried vegetables that I realized to enjoy after a long cold day of fasting. As each time these velvety stall me super well in addition to being too good. then to you the recipe of this velvety rich in taste and super smooth, a recipe simple and fast that here:   


**Cream of dried vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/veloute-de-legumes-secs-2_2.jpg)

**Ingredients**

  * 1 medium potato 
  * 1 medium carrot 
  * 1 glass of water of pulses (in truth half grains, half vegetables: pearl barley, yellow split peas, green split peas, wrinkled peas (after Google translation, I do not even know what is it?), Red lentils ) 
  * 1 small onion 
  * 2 cloves garlic 
  * 2 tablespoons of olive oil 
  * 1 cube of chicken broth 
  * 1 liter and a half of water 
  * salt and black pepper 
  * 1 cup of cumin 
  * 1 tablespoon of fresh cream 



**Realization steps**

  1. in a pot, sauté crushed garlic and onion in oil. 
  2. add the chopped vegetables and the pulses, the salt and the black pepper, 
  3. stir with a wooden spoon for 5 minutes over low heat. 
  4. add the knorr cube, and the water 
  5. cook on low heat for 45 minutes. 
  6. at the end of cooking, pass the mixture to the mixer. 
  7. before serving, add the spoon of fresh cream and sprinkle with caraway and enjoy 



thank you my dear friends for your visits and for all your comments 

thank you also for your subscriptions 

a la prochaine recette. 
