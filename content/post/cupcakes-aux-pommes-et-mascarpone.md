---
title: Apple and mascarpone cupcakes
date: '2016-02-20'
categories:
- Cupcakes, macaroons, and other pastries
tags:
- Cakes
- desserts
- Pastry
- To taste
- Easy cooking
- Algerian cakes
- Fluffy cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-1.jpg
---
[ ![apple cupcakes and mascarpone 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-1.jpg>)

##  Apple and mascarpone cupcakes 

Hello everybody, 

Since the time I did not make cupcakes, while my children claimed them every time! I woke up early in the morning after taking the children to school, and back home, I attacked the recipe for cupcakes with the knife, or rather the electric weapon, hihihihi since I Used my fix, which gave me a super nice and creamy dough. 

Once the cupcakes in the oven, I prepared the stuffing with apples, it's the same that I used in my [ For Apple Pie ](<https://www.amourdecuisine.fr/article-apple-pie-dautomne-au-caramel-au-beurre-sale.html>) . After cooking the cupcakes and their cooling I removed some of the heart cupcakes to put the stuffing in. 

Now remained to make a pretty decoration .... I opened my fridge to make butter cream, but the amount of butter I had been insufficient to cover all my pretty cupcakes! I look again and I see the box of mascarpone, finally what was left after doing my [ Tiramisu macaroons ](<https://www.amourdecuisine.fr/article-macarons-tiramisu.html>) . 

A little improvisation that finally gave me a cupcakes recipe just sublime, by the way my last to eat the one I cut to make my photos in the blink of an eye! amazing for such a small mouth, hihihihihih. 

[ ![apple cupcakes and mascarpone 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-4.jpg>)

**For 12 cupcakes:**   
150 gr sugar   
125 gr of ointment butter   
3 eggs   
250 gr of flour   
1 sachet of baking powder   
1 pinch of salt   
**for the apple stuffing:**   
the juice of 1 lemon   
3 apples   
40 gr of sugar   
1/4 teaspoons ground cinnamon   
1 Pinch of freshly grated nutmeg   
1 tablespoon of all-purpose flour   
**butter cream and mascarpone express**   
160 gr of ointment butter   
140 gr of mascarpone   
450 gr of sugar   
1 cup of vanilla coffee   
2 tablespoons of milk   
**for decoration** :   
very thin slices of apples   
a little cinnamon powder 

[ ![apple cupcakes and mascarpone 6](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-6.jpg>)

**Prepare the cupcakes:**   
Preheat the oven to 180 ° C   
beat the butter and sugar until you get an ointment,   
add the eggs one by one, vanilla.   
gently incorporate the flour, salt and yeast.   
Fill 2/3 of the paper or silicone molds with the preparation. 

[ ![cupcakes](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-.jpg>)   
place in preheated oven at 180 ° C for 20 min.   
remove from the oven and let cool on a wire rack.   
**prepare the stuffing:**   
in a ceramic bowl, place the lemon juice.   
peel, epipine and slice apples with a knife or fine mandolin.   
Place the apple slices in the lemon juice and sprinkle with 2 tablespoons of sugar. Let it soften slightly and release some of their juice, 20 to 30 minutes.   
saute in a saucepan so that the apples become tender.   
add the sugar, spices and flour, and mix well.   
let it come back until you have a creamy and thick mixture., let cool.   
**prepare the cream** :   
beat the butter until it becomes creamy, add the mascarpone and the milk.   
stir in the sugar while whisking (I did not put all the sugar, I do not like a butter cream too sweet.   
!mounting:   
empty the heart of cupcakes with a small knife or spoon, there is even a special gadget to do that.   
fill the hollow with the apple stuffing (if you have stuffing keep it in the fridge in a heritic jar) 

[ ![filling](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/remplissage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/remplissage.jpg>)   
place the butter cream in a pastry bag, and garnish the cupcakes, to have a nice cone, start at the middle of the cupcake, circle from inside to outside, then return to the center while going up.   
garnish the cupcakes with a thin slice of apple and sprinkle with cinnamon. 

[ ![apple cupcakes and mascarpone 9](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-9.jpg>)   


**Apple and mascarpone cupcakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cupcakes-aux-pommes-et-mascarpone-1.jpg)

portions:  12  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** For 12 cupcakes: 

  * 150 gr sugar 
  * 125 gr of ointment butter 
  * 3 eggs 
  * 250 gr of flour 
  * 1 sachet of baking powder 
  * 1 pinch of salt 

for the apple stuffing: 
  * the juice of 1 lemon 
  * 3 apples 
  * 40 gr of sugar 
  * ¼ cup ground cinnamon 
  * 1 Pinch of freshly grated nutmeg 
  * 1 tablespoon of all-purpose flour 

butter cream and mascarpone express 
  * 160 gr of ointment butter 
  * 140 gr of mascarpone 
  * 450 gr of sugar 
  * 1 cup of vanilla coffee 
  * 2 tablespoons of milk 

for decoration: 
  * very thin slices of apples 
  * a little cinnamon in 



**Realization steps** Prepare the cupcakes: 

  1. Preheat the oven to 180 ° C 
  2. beat the butter and sugar until you get an ointment, 
  3. add the eggs one by one, vanilla. 
  4. gently incorporate the flour, salt and yeast. 
  5. Fill ⅔ of the paper or silicone molds with the preparation. 
  6. place in preheated oven at 180 ° C for 20 min. 
  7. remove from the oven and let cool on a wire rack. 

prepare the stuffing: 
  1. in a ceramic bowl, place the lemon juice. 
  2. peel, epipine and slice apples with a knife or fine mandolin. 
  3. Place the apple slices in the lemon juice and sprinkle with 2 tablespoons of sugar. Let it soften slightly and release some of their juice, 20 to 30 minutes. 
  4. saute in a saucepan so that the apples become tender. 
  5. add the sugar, spices and flour, and mix well. 
  6. let it come back until you have a creamy and thick mixture., let cool. 
  7. prepare the cream: 
  8. beat the butter until it becomes creamy, add the mascarpone and the milk. 
  9. stir in the sugar while whisking (I did not put all the sugar, I do not like a butter cream too sweet. 

mounting: 
  1. empty the heart of cupcakes with a small knife or spoon, there is even a special gadget to do that. 
  2. fill the hollow with the apple stuffing (if you have stuffing keep it in the fridge in a heritic jar) 

filling 
  1. place the butter cream in a pastry bag, and garnish the cupcakes, to have a nice cone, start at the middle of the cupcake, circle from inside to outside, then return to the center while going up. 
  2. garnish the cupcakes with a thin slice of apple and sprinkle with cinnamon. 


