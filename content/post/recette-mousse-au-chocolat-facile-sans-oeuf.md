---
title: easy chocolate mousse recipe without egg
date: '2017-12-17'
categories:
- dessert, crumbles and bars
- sweet verrines
tags:
- verrines
- desserts
- Express cuisine
- Easy cooking
- Cake
- Algerian cakes
- Kitchen without egg

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-mousse-au-chocolat-facile-3.jpg
---
[ ![easy chocolate mousse recipe without egg 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-mousse-au-chocolat-facile-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-mousse-au-chocolat-facile-3.jpg>)

##  easy chocolate mousse recipe without egg 

Hello everybody, 

These delicious and beautiful verinnes of chocolate mousse are a recipe of Lunetoiles which was classified in the file oubliette in my mailbox. the recipe was there since May 2013, and if Lunetoiles did not remind me, you would never have had the easy recipe of this chocolate mousse. 

A recipe she found on a video youtube, and she prepared to share with me and you. 

Personally I am addictive chocolate chocoholic, and I prefer there is no chocolate at home, because I will risk to commute to the place where the chocolate is, until there is no crumb left! 

You can find on the blog, the [ duo of chocolate mousse ](<https://www.amourdecuisine.fr/article-duo-de-mousse-au-chocolat.html> "Chocolate mousse duo") , a [ Nutella mousse without egg ](<https://www.amourdecuisine.fr/article-mousse-au-nutella-oeuf.html> "nutella mousse without egg") or for what not a [ chocolate panna cotta ](<https://www.amourdecuisine.fr/article-panna-cotta-au-chocolat.html> "chocolate panna cotta") ? and you, how do you like chocolate? 

[ ![easy chocolate mousse recipe without egg 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-mousse-au-chocolat-facile-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-mousse-au-chocolat-facile-1.jpg>)   


**easy chocolate mousse recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-mousse-au-chocolat-facile.jpg)

portions:  6  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 1 liter of liquid cream 
  * 250 grams of chocolate 
  * Strawberries in sufficient quantities 
  * Caramel syrup or maple syrup 



**Realization steps**

  1. Bring the cream to the boil and pour over the chocolate and stir until the chocolate is completely melted. 
  2. Let macerate twelve hours in the refrigerator. 
  3. Whip the chocolate cream to the desired consistency. 
  4. Arrange the chocolate mousse in a verrine with the cut strawberries. 
  5. Sweeten the chocolate mousse to taste with either maple syrup or caramel syrup or icing sugar. 



[ ![easy chocolate mousse recipe without egg 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-mousse-au-chocolat-facile-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-mousse-au-chocolat-facile-2.jpg>)
