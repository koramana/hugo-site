---
title: Zebra cake
date: '2015-08-07'
categories:
- cakes and cakes
- ramadan recipe
- sweet recipes
tags:
- Fluffy cake
- Soft
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-zebr%C3%A9-2.CR2_.jpg
---
[ ![Zebra cake 2.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-zebr%C3%A9-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-zebr%C3%A9-2.CR2_.jpg>)

##  Zebra cake or zebra cake 

Hello everybody, 

I'm not disappointed to have made the recipe, because it is super good this zebra cake ...   


**Zebra cake or zebra cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-zebr%C3%A9.CR2_.jpg)

portions:  12  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * 6 eggs 
  * 340 g of sugar 
  * 120 ml whole liquid cream. 
  * 330 g flour 
  * 2 tablespoons of baking powder 
  * 175 g melted butter 
  * 1 pinch of salt 
  * 2 tbsp. tablespoons unsweetened cocoa 



**Realization steps**

  1. Butter or oil a round mold 25 cm in diameter. 
  2. Preheat your oven to 165 ° C. 
  3. In a large bowl, mix eggs and sugar with a whisk. 
  4. Add the cream, mix, 
  5. take 2 tablespoons flour and leave aside 
  6. add flour and yeast and mix until smooth. 
  7. Add the melted butter and warm, and mix again. 
  8. Pour half of the dough into another bowl, add the sifted cocoa and mix. 
  9. add the 2 tablespoons of flour to the other half of the dough 
  10. Pour a tablespoon of white paste into the mold. 
  11. Add over a tablespoon of cocoa paste. 
  12. do so simulatnément until exhaustion of the two pasta 
  13. Bake for 40 to 50 minutes. check the cooking with a knife blade inserted in the center of the cake, it must come out dry. 
  14. Let cool for 10 minutes then turn out on a rack. 



[ ![Zebra cake 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-zebr%C3%A9-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-zebr%C3%A9-1.CR2_.jpg>)

Merci pour vos commentaires et vos visites 
