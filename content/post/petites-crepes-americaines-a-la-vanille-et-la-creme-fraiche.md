---
title: Small american crepes with vanilla and fresh cream
date: '2008-02-05'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cheesecakes and tiramisus
- dessert, crumbles and bars
- birthday cake, party, celebrations
- basic pastry recipes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/02/217980001.jpg
---
it's pancake today. so my husband asked me for small pancakes, I look everywhere, and I find a nice recipe at Celine, who was super nice ....., thank you Celine. The ingredients are as follows: 250g of flour 5g of baking powder (a good teaspoon) 2 CS of sugar 1 pinch of salt 2 eggs 1 CS of liquid vanilla 20 cl of whole milk or fermented milk Lben 20 cl of fresh liquid cream so according to Celine, it's almost 15 pancakes, so, I dived the quantities of ingredients on 2. Mix & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![pancake1](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/217980001.jpg)

it's pancake today. 

without delay, here are the ingredients: 

  * **_250g of flour_ **
  * **_5g of baking powder (a good teaspoon well rounded)_ **
  * **_2 tablespoons sugar_ **
  * **_1 pinch of salt_ **
  * **_2 eggs_ **
  * **_1 CS of liquid vanilla_ **
  * **_20 cl of whole milk or fermented milk Lben_ **
  * **_20 cl of fresh cream_ **



So, according to Celine, it's almost 15 pancakes, so I divulged the amount of ingredients out of 2. 

![pancake2](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/217982981.jpg)

![pancake3](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/217983401.jpg)

![pancake4](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/217983561.jpg)

Mix all ingredients in the blender or manual whisk making sure to stir well to avoid lumps. 

Let the dough rest for 30 minutes. 

Heat a skillet, oil the surface then, once the pan is hot, make small pancakes by taking a few tablespoons of dough. They will not have to be big but have a pancake size, no more. Cook the pancakes on both sides.   
Continue until the dough is drained. 

![pancake5](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/217984231.jpg)

![pancake6](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/217984421.jpg)

serve hot, and I could have my share, 

finally god for the regime for a few seconds 

I did as for the Arabic pancakes, 

a little melted butter, to which honey is added 

a filet of this delicious mix on these pretty pancakes 

huuuuuuuuuuuuuuuuuuuum 

it was delicious 

of course, the absence of my husband, because for him I am in a very strict regime 

Finally, it's not every day the candlemas 

is not it???? 

![pancake7](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/217984681.jpg)

Bon Appétit 
