---
title: homemade pizza dough, easy recipe
date: '2016-09-29'
categories:
- basic pastry recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/pizza-012_thumb.jpg
---
##  homemade pizza dough, easy recipe 

Hello everybody, 

you want a homemade pizza dough, an easy and successful recipe? I deliver you my recipe, and I promise you that to try it is to adopt it. 

the  homemade pizza  is always appreciated by me, because everyone loves its filling, "without cheese for me" will say one! "Without olives for me" will say another !! "With spice for me" will say another !!! then not better than  the homemade pizza, and which says homemade pizza, says unbreakable homemade pizza pasta. 

With this  pizza pie, you have a very smooth and crunchy pizza at the same time  a happiness in the plate.   


**homemade pizza dough, easy recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/pizza-012_thumb.jpg)

**Ingredients**

  * 3 glasses of flour (220 ml each) 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 tablespoon instant baker's yeast 
  * 1 cup of baking powder 
  * 3 tablespoons of oil 
  * some water 



**Realization steps** I make my pizza dough in the bread machine, but you can easily make this dough by hand, and with the robot. 

  1. put the ingredients in the order recommended for the bread machine. add the water according to the flour and its absorption. run the program only paste, program number 8 for this device,   
![](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/2010-07-11-pizza_thumb1.jpg)
  2. At the end of the program, remove the dough from the machine, place it in an oil tray, flatten the dough and let it rise again. 
  3. garnish the dough according to your taste 



bonne dégustation, car je vous jure c’était un vrai délice, en tout cas mon père a vraiment aimer 
