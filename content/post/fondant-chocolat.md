---
title: Chocolate cake
date: '2012-02-19'
categories:
- gateaux algeriens au miel
- basic pastry recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/02/fondant2_thumb1.jpg
---
& Nbsp; & Nbsp; since I saw this cake at louiza I was only looking for the opportunity to do it, and in this intolerable cold, there is chocolate to cheer up, I enjoyed it with my children, and I confess, they did not wait for it cool, no question ... .. so without delay I pass you, the delicious recipe & nbsp; & Nbsp; For 8 people 4 eggs 200 g 60% chocolate 2 tbsp. milk soup 10m ml liquid cream 1 tbsp flour soup 50g almond 125g sugar & nbsp; Preheat the & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

[ ![fondant2](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/fondant2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-fondant-au-chocolat-42706545.html>)

so, without much delay I pass you, the delicious recipe 

[ ![flowing1 006-1](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/coulant1-006-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-fondant-au-chocolat-42706545.html>)

For 8 people 

  * **4 eggs**
  * **200 g of 60% chocolate**
  * **2 tbsp. soup of milk**
  * **10m ml of liquid cream**
  * **1 tablespoon of flour**
  * **50g of almond**
  * **125g of sugar**



[ ![flowing1 004-1](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/coulant1-004-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-fondant-au-chocolat-42706545.html>)

Preheat the oven to 180 °   
Melt chocolate with cream and milk   
Stir in the yolks one by one, then the flour and ground almonds 

Beat the whites in firm snow with the caster sugar   
Incorporate them gently to the previous preparation   
Pour into a 24 cm diameter mold   
Cook for 30-35 minutes (or depending on the oven)   
At the end of the oven unmold on a rack and let cool   
Serve warm! 

[ ![flowing1 011-1](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/coulant1-011-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-fondant-au-chocolat-42706545.html>)

A delight that I urge you, 

[ ![fondant2](https://www.amourdecuisine.fr//import/http://img.over-blog.com/550x412/2/42/48/75/API/2010-01/) ](<https://www.amourdecuisine.fr/article-gateau-fondant-au-chocolat-42706545.html>)

Hello everybody, 

so, without much delay I pass you, the delicious recipe 

[ ![flowing1 006-1](https://www.amourdecuisine.fr//import/http://img.over-blog.com/550x371/2/42/48/75/API/2010-01/) ](<https://www.amourdecuisine.fr/article-gateau-fondant-au-chocolat-42706545.html>)

For 8 people 

  * ** 4 eggs  **
  * ** 200 g of 60% chocolate  **
  * ** 2 tbsp. soup of milk  **
  * ** 100 ml of liquid cream  **
  * ** 1 tablespoon of flour  **
  * ** 50g of almond  **
  * ** 125g of sugar  **



[ ![flowing1 004-1](https://www.amourdecuisine.fr//import/http://img.over-blog.com/550x412/2/42/48/75/API/2010-01/) ](<https://www.amourdecuisine.fr/article-gateau-fondant-au-chocolat-42706545.html>)

Preheat the oven to 180 °   
Melt chocolate with cream and milk   
Stir in the yolks one by one, then the flour and ground almonds 

Beat the whites in firm snow with the caster sugar   
Incorporate them gently to the previous preparation   
Pour into a 24 cm diameter mold   
Cook for 30-35 minutes (or depending on the oven)   
At the end of the oven unmold on a rack and let cool   
Serve warm! 

[ ![flowing1 011-1](https://www.amourdecuisine.fr//import/http://img.over-blog.com/550x412/2/42/48/75/API/2010-01/) ](<https://www.amourdecuisine.fr/article-gateau-fondant-au-chocolat-42706545.html>)

Un délice que je vous conseille vivement, 
