---
title: Homemade donuts easy and delicious
date: '2016-01-17'
categories:
- crepes, beignets, donuts, gauffres sucrees
tags:
- fritters
- Bugnes
- Carnival
- To taste
- Cakes
- desserts
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/Donuts-fait-maison-2.CR2_.jpg
---
[ ![Homemade Donuts 2.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/Donuts-fait-maison-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/Donuts-fait-maison-2.CR2_.jpg>)

##  Homemade donuts easy and delicious 

Hello everybody, 

Yesterday the night just after dinner, my children ask me to prepare some donuts for breakfast. I was a little upset, because it was very late still for donuts, but after I said to myself, go it's not every day they ask for donuts, and I confess I have makes a tiny amount. I'm hardly out with 12 donuts. 

I left the dough lifted in a preheated oven so as not to stay at very late hours. I made a little icing sugar and water, but it did not dry well, hihihihih. 

This morning they were happy to take their [ hot chocolate ](<https://www.amourdecuisine.fr/article-chocolat-chaud-au-nutella-hot-chocolat.html>) with donuts. Of course they chose the most beautiful ones to eat them ... So sorry for the photos, hihihihihih I had all the donuts with the frosting frosting. 

[ ![homemade donuts 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/donuts-faits-maison-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/donuts-faits-maison-3.jpg>)

I pass you this recipe that I found on my forum favorite "tested kitchen", because usually, when I want to try a recipe for the first time, and I can not find what I want on my books or my friends blogs, I head to this forum, because as its title indicates, it's recipes tested, so there is no doubt in the success of the recipe, and must admit, nobody like to miss a recipe ... 

so here is the recipe, that the girls on the forum had to try and try again, and I was seduced by the recipe, was even one of the girls who put a video, that I will pass you. 

a delicious pastry that I share with you, with great pleasure, put remains that I love more the [ sfenj ](<https://www.amourdecuisine.fr/article-sfenj-patisserie-algerien-103295136.html>) , this delicious [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>)

recipe in Arabic: 

more [ ![Homemade donuts.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/Donuts-faits-maison.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/Donuts-faits-maison.CR2_.jpg>)

**Homemade donuts easy and delicious**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/donuts-fait-maison5.CR2_.jpg)

Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 2 eggs 
  * 1 sachet of dehydrated yeast 
  * 1 C. liquid vanilla 
  * 60 g caster sugar 
  * 1 pinch of salt 
  * 60 g of butter 
  * 250 g of milk (it is good gr and not ml) 
  * more or less 500g of flour (so depending on the absorption of the flour)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/donuts-3_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/donuts-3_thumb.jpg>)



**Realization steps**

  1. Melt the butter in the milk in the saucepan or in the microwave. 
  2. Let cool. 
  3. then pour into a salad bowl 
  4. Add vanilla, sugar, eggs, pinch of salt and yeast, 
  5. mix with a hand whisk. 
  6. Add the flour little by little, with the whisk at the beginning and by hand or with the mixer with the special kneading whips (my drummer does not have a large capacity, so I finish by hand) 
  7. The dough should be beautiful, flexible and not sticky. 
  8. Let the rest 1h / 1h30 covered with film in the oven door close 
  9. After the exposure time, the dough will double in size. degas it and spread it on a floured worktop about 1 cm to 1.5 cm thick. 
  10. Take a Donut special cutter (this is the gift I received yesterday from Souhila), or use a large glass of almost 10 cm in diameter and cut circles in the dough, in these circles cut from small circles in the middle 
  11. Place these donuts on a well floured and floured top. 
  12. Cover and let stand for another 30 minutes under a cloth or food film 
  13. heat the oil, and fry the donuts on each side, it cooks quickly 
  14. you can prepare a good ice cream, something I could not do, I was well over the dinner, and an eye on my daughter, because I have to watch her 
  15. for children I presented these donuts with caramel cream, and they have devores ca. 



[ ![homemade donuts easy and delicious.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/donuts-maison-faciles-et-d%C3%A9licieux.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/donuts-maison-faciles-et-d%C3%A9licieux.CR2_.jpg>)

recipe tested and approved, and I urge you 

thank you for your visits 

bonne journée 
