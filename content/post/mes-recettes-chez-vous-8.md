---
title: My recipes at home 8
date: '2011-03-26'
categories:
- Mina el forn
- pain, pain traditionnel, galette

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/03/sag1.jpg
---
![http://img.over-blog.com/500x374/4/02/55/30/trsse-018.JPG](http://img.over-blog.com/500x374/4/02/55/30/trsse-018.JPG)

[ The two-tone braid ](<https://www.amourdecuisine.fr/article-la-tresse-61395654.html>) in [ Oum Yasmine ](<http://tiabdialy.com.over-blog.com>)

![sag.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/sag1.jpg)

[ Flan with coconut milk ](<https://www.amourdecuisine.fr/article-flan-au-lait-de-noix-de-coco-58241026.html>) in [ Sagwest ](<http://sagweste.over-blog.com/>) e 

![https://www.amourdecuisine.fr/wp-content/uploads/2011/03/SDC133811.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/SDC133811.jpg)

[ Charlotte Pineapple Mascarpone ](<https://www.amourdecuisine.fr/article-charlotte-ananas-mascarpone-65141836.html>) in [ Drissia ](<http://chehiwatdrissia.over-blog.fr/>)

![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/500x374/4/02/55/30/1ER-DOSSIER/)

[ mascarpone kiwi verrines ](<https://www.amourdecuisine.fr/article-verrines-sucrees-kiwis-mascarpone-64934521.html>) in [ Umm manou tunisienne ](<http://tiabdialy.com.over-blog.com>)

![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/600x600/3/52/23/76/basboussa-a-la-chapelure/)

![cake-reverse-abricot.JPG](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/Images/)

[ apricot apricot reverse cakes ](<https://www.amourdecuisine.fr/article-gateau-renverse-abricot-amandes-acacia-45768674.html>) at Samia (a faithful reader) 

![flash-Mc-Queen gateau.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/Images/)

[ Birthday cake dora ](<https://www.amourdecuisine.fr/article-37896589.html>) , become a Flash Mc Queen birthday cake at Mounira Mia (a faithful reader) 

![foccacia.JPG](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/Images/)

[ Focaccia rosemary / onion ](<https://www.amourdecuisine.fr/article-focaccia-au-romarin-et-oignon-62123009.html>) at Nacy's (faithful reader) 

![madeleine-a-la-hull-to-chocolat.JPG](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x224/2/42/48/75/Images/)

[ madeleine with chocolate shell ](<https://www.amourdecuisine.fr/article-les-madeleines-a-la-coque-en-chocolat-63266835.html>) in blue butterfly (faithful reader) 

![http://img.over-blog.com/225x300/4/08/04/65/sables-sesame.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/225x300/4/08/04/65/)

(but with non-refined sugar, and whole flour) 

and if you also try one of my recipes, do not forget to send me your photos on my email: 

vosessais@gmail.com 

thank you for subscribing to my newsletter if you have not done so yet: 

![newsletter](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x367/2/42/48/75/mhalbi/)

have a good day 
