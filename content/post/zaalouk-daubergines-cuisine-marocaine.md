---
title: Zaalouk aubergines, Moroccan cuisine
date: '2013-02-25'
categories:
- juice and cocktail drinks without alcohol
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/zaalouk-d-aubergines_2-300x241.jpg
---
##  Zaalouk aubergines, Moroccan cuisine 

Hello everybody, 

I love eggplants, and this time I prepared a delicious aubergine puree, better known as Zaalouk aubergines ...... it is a delicious recipe of Moroccan cuisine, a cuisine that I appreciate very much. 

I prepared this eggplant zaalouk recipe in a slightly different way from the known one, it is an express method, in 20 minutes the zaalouk was ready to taste with a good [ matlou3 ](<https://www.amourdecuisine.fr/article-matloue-matlou3-matlou-pain-maison-au-tadjine.html> "Matlouh, matlou3, matlouh - home-made tajine bread") :   


**Zaalouk aubergines, Moroccan cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/zaalouk-d-aubergines_2-300x241.jpg)

portions:  4  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 1 eggplant (large) 
  * 1 medium zucchini 
  * 1 half red pepper 
  * 1 half yellow pepper (you can replace both with a green pepper) 
  * 2 medium tomatoes 
  * 2 cloves garlic 
  * salt, black pepper, cumin and paprika 
  * olive oil 



**Realization steps**

  1. clean and peel the zucchini and eggplant 
  2. seed the peppers. 
  3. cut everything into pieces, and put in a pressure cooker. 
  4. add half a glass of water, garlic, and spices. 
  5. close the casserole and cook 15 minutes 
  6. open the pressure cooker, add the olive oil, and with a crushed potato, mash the whole 
  7. cook until the water is completely evaporated, 
  8. adjust the salt. 


