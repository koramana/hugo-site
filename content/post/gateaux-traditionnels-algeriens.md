---
title: traditional Algerian cakes
date: '2012-04-08'
categories:
- cakes and cakes
tags:
- Algerian cakes
- Cakes
- Algeria
- Aid cake
- baklava
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/aftir3-1024x755.jpg
---
[ ![aftir3](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/aftir3-1024x755.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/aftir3.jpg>)

##  traditional Algerian cakes 

Hello my friends 

the **Algerian traditional cakes** have always been a delight to savor, a pleasure to present to these guests, or a nice way to express their joy, when presented at **wedding parties** , or **ugly** , or other celebrations. since time, our grandmothers knew in their diversified ways in the presentation of these pretty **sweets** . But in this last period, Algerian women have been able to give a new dimension to these Algerian cakes, a good move of modernization, until conquering the biggest pastries in the world, either on the taste side or on the presentation side. 

here is the detailed list of some of these **Algerian traditional cakes** , that I had the chance to prepare myself, or while Lunetoiles liked to share with you through my blog, I try to categorize them to make the task easy for you. 

## 

**Algerian cakes coated in honey** :   
  
<table>  
<tr>  
<td>

![gateauxpour-the-Aid-el-Fitr-2012-the-esses_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-gateauxpour-Aid-el-fitr-2012-les-esses_thumb1-150x150.jpg) [ Esses, almond cakes ](<https://www.amourdecuisine.fr/article-les-esses-gateaux-aux-amandes-gateau-algerien-2015.html>) 
</td>  
<td>

![cake-Algerian-2013-el-ftimates_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/gateau-algerien-2013-el-ftimates_thumb1-150x150.jpg) [ el ftimates ](<https://www.amourdecuisine.fr/article-gateau-algerien-2013-el-ftimates.html>) 
</td>  
<td>

![butterflies knots, modern Algerian cake 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/papillons-noeuds_31.jpg) [ knot butterflies ](<https://www.amourdecuisine.fr/article-gateau-algerien-noeuds-papillons.html>) 
</td> </tr>  
<tr>  
<td>

![Algerian-the-cake-Aid-2012-the-pyramides_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-gateaux-algeriens-Aid-2012-les-pyramides_thumb1-150x150.jpg) [ the pyramids with almonds ](<https://www.amourdecuisine.fr/article-les-pyramides-aux-amandes-gateaux-algeriens-2015.html>) 
</td>  
<td>

![mchekla with leaves, Algerian cake 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/mchekla-aux-feuilles_31.jpg) [ mchekla with leaves ](<https://www.amourdecuisine.fr/article-gateau-algerien-mchekla-aux-feuilles.html>) 
</td>  
<td>

![dziriette in bloom, Algerian cake 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/dziriette-en-fleur_311.jpg) [ dziriettes in bloom ](<https://www.amourdecuisine.fr/article-gateau-algerien-dziriettes-aux-amandes.html>) 
</td> </tr>  
<tr>  
<td>

![skandraniette cake modern 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/skandraniette-gateau-moderne_511.jpg) [ skandraniettes ](<https://www.amourdecuisine.fr/article-skandraniette-gateaux-algeriens.html>) 
</td>  
<td>

![arayech with honey cake modern 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/arayeche-au-miel-gateau-moderne_31.jpg) [ Arayeche with honey ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-gateaux-algeriens.html>) 
</td>  
<td>

![lmkhidettes, mkhedette laaroussa](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/lmkhidettes-mkhedette-laaroussa_31.jpg) [ lmkhidatte ](<https://www.amourdecuisine.fr/article-lemkhidette-gateau-algerien-pour-l-aid-2012.html>) 
</td> </tr>  
<tr>  
<td>

_![slilates algerian cake aid el fitr 2014](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/slilates-gateau-algerien-aid-el-fitr-2014-150x150.jpg) _ _[ slilates ](<https://www.amourdecuisine.fr/article-slilates-gateau-algerien-2014-en-video.html>) _ 
</td>  
<td>

![arayeche gateau Algerian](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/arayeche-gateau-algerien_31.jpg) [ arayeche with coconut ](<https://www.amourdecuisine.fr/article-arayeche-au-miel-arayeche-aux-amandes-et-noix-de-coco.html>) 
</td>  
<td>

![gazelle horn with tongs, honey nekkache](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/corne-de-gazelle-a-la-pince-au-nekkache-au-miel_31.jpg) [ gazelle horn with nekkache ](<https://www.amourdecuisine.fr/article-corn-de-gazelle-au-nekkache-a-la-pince.html>) 
</td> </tr>  
<tr>  
<td>

_![Algerian cake 2015](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/griwech-el-warka-004-109x150.jpg) _ [ griwech el warka ](<https://www.amourdecuisine.fr/article-griwech-el-werka.html>) 
</td>  
<td>

![Griwech](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/griwech-150x150.jpg) [ laminated griwech ](<https://www.amourdecuisine.fr/article-griwech-feuillete-dessert-du-ramadan-2014.html>) 
</td>  
<td>

![mqerqchate el warda.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mqerqchate-el-warda.CR2_-150x150.jpg) [ mguergchates el warda crispy atria ](<https://www.amourdecuisine.fr/article-oreillettes-algeriennes-croustillantes-mguergchates-el-warda-en-video.html>) 
</td> </tr>  
<tr>  
<td>

_![Griwech](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/griwech_31.jpg) _ [ Griwech ](<https://www.amourdecuisine.fr/article-griwech-gateau-algerien-en-video.html>) 
</td>  
<td>

![crispy donuts](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2_-150x150.jpg) [ donuts with donut iron ](<https://www.amourdecuisine.fr/article-beignets-au-fer-a-beignet-gateau-algerien.html>) 
</td>  
<td>

![two-tone gazelle horn pinches honey](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/corne-de-gazelle-bicolore-pincee-au-miel_31.jpg) [ Two-tone gazelle horn ](<https://www.amourdecuisine.fr/article-corne-de-gazelle-bicolore-au-nakache.html>) 
</td> </tr>  
<tr>  
<td>

[ ![baklava-to-11-almond](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/baklawa-aux-amandes-11-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/baklawa-aux-amandes-11.jpg>)   
[ algerian baklawa ](<https://www.amourdecuisine.fr/article-bakalwa-aux-amandes.html>) 
</td>  
<td>

![baklawa cake of the aid 2014 042.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/baklawa-gateaux-de-laid-2014-042.CR2_1-150x91.jpg) [ baklawa constantinoise ](<https://www.amourdecuisine.fr/article-baklawa-constantinoise.html>) 
</td>  
<td>

![baklawa has the filo dough](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/baklawa-a-la-pate-a-filo_31.jpg) [ baklawa has filo pate ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-a-la-pate-filo-pour-la-fete-d-aid.html>) 
</td> </tr>  
<tr>  
<td>

![makrout el koucha at the nakache cake of the aid 2014 021.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/makrout-el-koucha-au-nakache-gateaux-de-laid-2014-021.CR2_-150x101.jpg) [ makrout el koucha ](<https://www.amourdecuisine.fr/article-makrout-el-koucha-en-video-gateau-de-semoule-et-dattes.html>) 
</td>  
<td>

![chebakia-Moroccan](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/chebakia-marocaine-150x150.jpg) [ Moroccan chebakia ](<https://www.amourdecuisine.fr/article-chebakia-marocaine-mkherka.html>) 
</td>  
<td>

![Mchewek-to-grain-of-sesames.CR2_2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Mchewek-aux-grains-de-sesames.CR2_2-150x150.jpg) [ mchewek with sesame seeds ](<https://www.amourdecuisine.fr/article-mchewek-aux-grains-de-sesame-gateau-algerien-economique.html>) 
</td> </tr>  
<tr>  
<td>

[ ![hnif-1_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/hnif-1_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/11/hnif-1_thumb1.jpg>) [ hnifiettes ](<https://www.amourdecuisine.fr/article-hnifiettes-gateau-algerien.html>) 
</td>  
<td>

![Image \(26\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-26-_31.jpg) [ Skandraniettes with hazelnuts ](<https://www.amourdecuisine.fr/article-skandraniette-en-triangle-aux-noisettes-gateau-algerien-2012.html>) 
</td>  
<td>

![edges in bloom](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/carres-en-fleur_31.jpg) [ dziriettes with coconut ](<https://www.amourdecuisine.fr/article-dziriette-en-fleur-gateau-algerien-aux-amandes-et-miel.html>) 
</td> </tr>  
<tr>  
<td>

![makrout enekache](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/makrout-enekache-150x100.jpg) [ makrout nekkache ](<https://www.amourdecuisine.fr/article-makrout-nekkache.html>) 
</td>  
<td>

![makrout sniwa, makrout way baklawa](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa-3-150x150.jpg) [ makrout sniwa ](<https://www.amourdecuisine.fr/article-makrout-sniwa-ou-makrout-facon-baklawa.html>) 
</td>  
<td>

![makrout almond almonds almond lazar 2015](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/makrout-lassel-makrout-aux-amandes.CR2_-150x100.jpg) [ makrout laassel ](<https://www.amourdecuisine.fr/article-makrout-laassel-aux-amandes.html>) 
</td> </tr>  
<tr>  
<td>

![makrout-to-dattes_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makrout-aux-dattes_thumb-150x150.jpg) [ makrout rolled ](<https://www.amourdecuisine.fr/article-makrout-laasel-makrout-roule-frit.html>) 
</td>  
<td>

![makrout Tlemcen](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-de-tlemcen-aux-oeufs-1-150x113.jpg) [ makrout tlemcen ](<https://www.amourdecuisine.fr/article-makrout-de-tlemcen.html>) 
</td>  
<td>

![balah el sham honey oriental pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balah-el-sham-patisserie-orientale-au-miel.CR2_-150x105.jpg) [ balah sham ](<https://www.amourdecuisine.fr/article-balah-el-sham-patisserie-orientale-au-miel.html>) 
</td> </tr>  
<tr>  
<td>

![cigars with peanuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cigares-aux-cacahuetes-1-98x150.jpg) [ cigars with peanuts ](<https://www.amourdecuisine.fr/article-cigares-aux-cacahuetes.html>) 
</td>  
<td>

![crispy nutlets](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/oreilletes-croustillantes-150x150.jpg) [ mguergchates or honey atria ](<https://www.amourdecuisine.fr/article-oreillettes-algeriennes-croustillantes-mguergchates-el-warda-en-video.html>) 
</td>  
<td>

![Khochkhach, Khechkhach, auricles](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/khochkhach-khechkhach-les-oreillettes-150x150.jpg) [ khechkhache ](<https://www.amourdecuisine.fr/article-khechkhach-ou-khochkhach-oreillettes-gateau-algerien.html>) 
</td> </tr>  
<tr>  
<td>

[ ![awama lokmat el kadi](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/awama-lokmat-el-kadi-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/06/awama-lokmat-el-kadi.jpg>) [ Awama, Lokmet el kadi ](<https://www.amourdecuisine.fr/article-awama-ou-lokmat-el-kadi.html>) 
</td>  
<td>

![samsa-028_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/samsa-028_thumb1-150x150.jpg) [ samsa laedjine ](<https://www.amourdecuisine.fr/article-samsa-ou-triangles-farcis-damandes.html>) 
</td>  
<td>

![almond knots](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-noeuds-aux-amandes_31.jpg) [ almond knots ](<https://www.amourdecuisine.fr/article-gateaux-algeriens-les-noeuds-aux-amandes.html>) 
</td> </tr>  
<tr>  
<td>

![the eyes](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-oeillees_31.jpg) [ the pillows ](<https://www.amourdecuisine.fr/article-27828394.html>) 
</td>  
<td>

![two-tone cornets with almonds and walnuts.aspx](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/cornets-bicolores-aux-amandes-et-noix.aspx_61.jpg) [ Two-tone cornets with almonds ](<https://www.amourdecuisine.fr/article-cornets-aux-amandes-et-noix-gateaux-algeriens.html>) 
</td>  
<td>

![Image \(15\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-15-_31.jpg) [ the braid ](<https://www.amourdecuisine.fr/article-la-tresse.html>) 
</td> </tr>  
<tr>  
<td>

![ktayefs with almonds 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/ktayefs-aux-amandes-2-150x118.jpg) [ ktaives with almonds ](<https://www.amourdecuisine.fr/article-ktayefs-ou-cheveux-d-ange-aux-amandes.html>) 
</td>  
<td>

![konafa_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/konafa_thumb-150x150.jpg) [ mini kounafa with cream ](<https://www.amourdecuisine.fr/article-mini-konafa-ktayefs-a-la-creme.html>) 
</td>  
<td>

![finger de-kounafa](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/doigts-de-kounafa-150x150.jpg) [ konafa fingers with peanuts and raisins ](<https://www.amourdecuisine.fr/article-doigts-de-kounafa-aux-raisins-ktaif-konafa-kadaif.html>) 
</td> </tr>  
<tr>  
<td>

![Image \(27\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-27-_31.jpg) [ baklawa rolls ](<https://www.amourdecuisine.fr/article-baklawa-rolls-baklava-rolls.html>) 
</td>  
<td>

![Image \(25\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-25-_31.jpg) [ baklawa with walnuts ](<https://www.amourdecuisine.fr/article-baklawa-aux-noix-nobles.html>) 
</td>  
<td>

[ ![Image \(7\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-7-_31.jpg) ](<https://www.amourdecuisine.fr/article-36727369.html>) [ chamsiya ](<https://www.amourdecuisine.fr/article-36727369.html>) 
</td> </tr>  
<tr>  
<td>

[ ![ktaif_3](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ktaif_3-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ktaif_3.jpg>) [ pistachio ktaives ](<https://www.amourdecuisine.fr/article-ktaifs-aux-pistaches-qtayefs-gateau-algerien-aux-cheveux-dange.html>) 
</td>  
<td>

[ ![skandraniette chahda](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/skandraniette-chahda-150x100.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/04/skandraniette-chahda.jpg>) [ skandraniette chahda ](<https://www.amourdecuisine.fr/article-skandraniette-chahda-gateau-algerien-2015.html>) 
</td>  
<td>

[ ![halwat ezhar, algerian cake from l'aid el fitr.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/halwat-ezhar-gateau-algerien-de-laid-el-fitr.CR2_-150x108.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/halwat-ezhar-gateau-algerien-de-laid-el-fitr.CR2_.jpg>) [ halwat ezhar ](<https://www.amourdecuisine.fr/article-halwat-ezhar-gateau-algerien-fete-laid.html>) 
</td> </tr> </table>

**Algerian cakes with glaze:**  
  
<table>  
<tr>  
<td>

[ ![Algerian cake bracelets 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-bracelets-gateau-algeriens_311.jpg) ](<https://www.amourdecuisine.fr/article-la-recette-des-bracelets-gateau-algerien.html>) [ the bells ](<https://www.amourdecuisine.fr/article-la-recette-des-bracelets-gateau-algerien.html>) 
</td>  
<td>

[ ![mkhabez, Algerian cake 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/mkhabez_61.jpg) ](<https://www.amourdecuisine.fr/article-25345466.html>) 
</td>  
<td>

[ ![mkhabez a coconut, Algerian cake 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/mkhabez-a-la-noix-de-coco_311.jpg) ](<https://www.amourdecuisine.fr/article-mkhabez-a-la-noix-de-coco-gateau-algerien.html>) 
</td>  
<td>

[ ![arayech oriental pastry 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-28-_31.jpg) ](<https://www.amourdecuisine.fr/article-arayeche-larayeche-aux-pistaches-gateau-algerien.html>) 
</td> </tr>  
<tr>  
<td>

[ ![mashed with walnuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mkhebez-aux-noix-1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mkhebez-aux-noix-1.jpg>) [ Munch with walnuts ](<https://www.amourdecuisine.fr/article-mkhabez-aux-noix-gateau-algerien-en-video.html> "Mekhabez with nuts, Algerian cake video") 
</td>  
<td>

[ ![ftirates algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/ftirates-gateau-algerien_31.jpg) ](<https://www.amourdecuisine.fr/article-ftirates-gateaux-algeriens-au-glacage.html>) 
</td>  
<td>

[ ![arayeche algerian cakes aid el fitr 2014.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/arayeche-gateaux-algeriens-aid-el-fitr-2014.CR2_-150x104.jpg) ](<https://www.amourdecuisine.fr/article-arayeche-ou-larayeche.html/arayeche-gateaux-algeriens-aid-el-fitr-2014-cr2-2>)

[ Arayches Glacees ](<https://www.amourdecuisine.fr/article-arayeche-ou-larayeche.html>)


</td>  
<td>

[ ![Algerian-the-cake-Aid-el-Fitr-2012-Sbaa el aroussa_thum1](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/les-gateaux-algeriens-Aid-el-fitr-2012-Sbaa-el-aroussa_thum1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/les-gateaux-algeriens-Aid-el-fitr-2012-Sbaa-el-aroussa_thum1.jpg>) [ sbaa laaroussa in the glaze ](<https://www.amourdecuisine.fr/article-les-doigts-de-la-mariee-glaces-gateau-algerien.html>) 
</td> </tr>  
<tr>  
<td>

[ ![try with hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/mkhabez-aux-noisettes_31.jpg) ](<https://www.amourdecuisine.fr/article-mkhabez-aux-noisettes-gateau-algerien.html>) 
</td>  
<td>



[ ![tcharek ice cream cake of the aid 2014 .CR2](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/tcharek-glace-gateaux-de-laid-2014-.CR2_-150x108.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/tcharek-glace-gateaux-de-laid-2014-.CR2_.jpg>) [ iced charek ](<https://www.amourdecuisine.fr/article-tcharek-glace-ou-corne-de-gazelle-algerien-au-glacage.html>)


</td>  
<td>

[ ![kaikaâtes with flan, Samira.CR2 Economic Cakes](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/kaika%C3%A2tes-au-flan-G%C3%A2teaux-Economiques-Samira.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/kaika%C3%A2tes-au-flan-G%C3%A2teaux-Economiques-Samira.CR2_.jpg>) [ k3ik3ates on the flan ](<https://www.amourdecuisine.fr/article-gateaux-economique-samira-kaikaates-au-flan.html> "Samira economic cakes, kaikaates with flan") 
</td>  
<td>

[ ![lemon sands 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sables-au-citron-2-113x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sables-au-citron-2.jpg>) [ shortbread with lemon ](<https://www.amourdecuisine.fr/article-sables-au-citron.html>) 
</td> </tr>  
<tr>  
<td>

[ ![lemon rings with thin almonds 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/anneaux-citronn%C3%A9s-aux-amandes-effil%C3%A9s-7-150x132.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/anneaux-citronn%C3%A9s-aux-amandes-effil%C3%A9s-7.jpg>)

[ lemon rings ](<https://www.amourdecuisine.fr/article-anneaux-citronnes-aux-amandes-effiles.html>)


</td>  
<td>



[ ![Image \(3\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-3-_31.jpg) ](<https://www.amourdecuisine.fr/article-25345466.html>)


</td>  
<td>

[ ![cakes, dry-mkhabez au flan_31](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/gateaux-secs-mkhabez-au-flan_31-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/gateaux-secs-mkhabez-au-flan_31.jpg>)

[ dress in the blank ](<https://www.amourdecuisine.fr/article-mkhabez-au-flan-gateau-au-flan.html>)


</td>  
<td>

[ ![cake lamingtons](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-lamington-1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-lamington-1.jpg>) [ cakes lamingtons ](<https://www.amourdecuisine.fr/article-gateaux-lamingtons-roses.html>) 
</td> </tr>  
<tr>  
<td>



[ ![calissons with gingerbread](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/calissons-au-pain-depices-150x108.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/calissons-au-pain-depices.jpg>)

[ calissons with gingerbread ](<https://www.amourdecuisine.fr/article-calisson-pain-d-epices-maison.html>)


</td>  
<td>


</td>  
<td>


</td>  
<td>


</td> </tr> </table>

dry cakes, ghrebia (ghribiya) and shortbread: 

**Algerian cakes without cooking**  
  
<table>  
<tr>  
<td>

_[ ![Algerian-the-cake-Aid-el-Fitr-2012-balls-of-neige_thum](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-gateaux-algeriens-Aid-el-fitr-2012-boules-de-neige_thum-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-gateaux-algeriens-Aid-el-fitr-2012-boules-de-neige_thum.jpg>) [ snowballs with coconut ](<https://www.amourdecuisine.fr/article-gateaux-algeriens-a-la-noix-de-coco-aid-el-kebir-2012-111495668.html>) _ 
</td>  
<td>

_[ ![algerian cake 2013, bniouen](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-34-_31.jpg) ](<https://www.amourdecuisine.fr/article-gateau-algerien-sans-cuisson-bniouen-farci-101810986.html> "algerian cake, stuffed bniouen") _ 
</td>  
<td>

[ _![cake without cooking 2013, the pyramids](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/gateau-sans-cuisson-les-pyramides_31.jpg) _ ](<https://www.amourdecuisine.fr/article-gateau-sans-cuisson-les-pyramides.html>) 
</td>  
<td>

[ _![Moorish, almond kefta](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-mauresques-kefta-aux-amandes_31.jpg) _ ](<https://www.amourdecuisine.fr/article-les-mauresques-kefta.html>) 
</td> </tr>  
<tr>  
<td>

[ _![chocolate truffles, Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-truffes-au-chocolat_31.jpg) _ ](<https://www.amourdecuisine.fr/article-truffes-au-chocolat-et-biscuits.html>) 
</td>  
<td>

_[ ![cake-baking-without-fingers-at-chocolat_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateaux-sans-cuisson-doigts-au-chocolat_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateaux-sans-cuisson-doigts-au-chocolat_thumb1.jpg>) [ chocolate fingers ](<https://www.amourdecuisine.fr/article-gateau-sans-cuisson-doigts-au-chocolat.html>) _ 
</td>  
<td>

_[ ![bniouen cake without cooking](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/bniouen-gateau-sans-cuisson_31.jpg) ](<https://www.amourdecuisine.fr/article-bniouen-gateau-sans-cuisson-algerien.html>) _ 
</td>  
<td>

_[ ![Image \(21\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-21-_31.jpg) ](<https://www.amourdecuisine.fr/article-mauresque-a-la-noix-de-coco-gateau-algerien-101325541.html>) _ 
</td> </tr>  
<tr>  
<td>

_[ ![balls with coconut and orange jam](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/boules-a-la-noix-de-coco-et-confiture-d-orange_31.jpg) ](<https://www.amourdecuisine.fr/article-boules-a-la-noix-de-coco-et-confiture-d-orange-53050859.html>) _ 
</td>  
<td>

_[ ![Moorish nutella](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/mauresque-au-nutella_31.jpg) ](<https://www.amourdecuisine.fr/article-mauresques-au-nutella-de-leila.html>) _ 
</td>  
<td>

_[ ![harissa el louz, harissat el louz.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/harissa-el-louz-harissat-el-louz.CR2_-150x100.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/harissa-el-louz-harissat-el-louz.CR2_.jpg>) _ _[ harissat el louz ](<https://www.amourdecuisine.fr/article-harissa-aux-amandes-harissat-el-louz.html>) _ 
</td>  
<td>

_[ ![kefta with hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/kefta-aux-noisettes_31.jpg) ](<https://www.amourdecuisine.fr/article-kefta-aux-noisettes-59679046.html>) _ 
</td> </tr>  
<tr>  
<td>

[ ![IMG_0082](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/IMG_00821-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/IMG_00821.jpg>) [ the candles ](<https://www.amourdecuisine.fr/article-les-bougies-gateaux-algeriens.html>) 
</td>  
<td>

_[ ![cake-in-cooking-with-almonds](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateaux-sans-cuisson-aux-amandes-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateaux-sans-cuisson-aux-amandes.jpg>) [ cakes without cooking with almonds ](<https://www.amourdecuisine.fr/article-carres-aux-amandes-sans-cuisson.html>) _ 
</td>  
<td>

[ _![skikrate with coconut](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/skikrate-a-la-noix-de-coco_31.jpg) _ ](<https://www.amourdecuisine.fr/article-25345462.html>) 
</td>  
<td>

[ ![halwat smid](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau-algerien-facile-halwat-smid-150x103.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau-algerien-facile-halwat-smid.jpg>) [ halwat smid ](<https://www.amourdecuisine.fr/article-halwat-smid-gateau-algerien-sans-cuisson.html>) 
</td> </tr>  
<tr>  
<td>

[ ![FIRSTS](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/rfiss_31.jpg) ](<https://www.amourdecuisine.fr/article-rfiss-gateau-algerien.html>) 
</td>  
<td>

_[ ![zri zri, or dessert to the klila](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/zri-zri-1-150x113.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/zri-zri-1.jpg>) _ [ Zri zri ](<https://www.amourdecuisine.fr/article-zri-zri-ou-dessert-la-klila-fromage-sec.html>) 
</td>  
<td>

[ ![Image \(36\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-36-_31.jpg) ](<https://www.amourdecuisine.fr/article-rkhama-zellige-nougat-aux-cacahuetes-et-sesames-grillees.html>) 
</td>  
<td>

[ ![Image \(35\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-35-_31.jpg) ](<https://www.amourdecuisine.fr/article-nougat-au-chocolat-blanc-gateau-sans-cuisson.html>) 
</td> </tr>  
<tr>  
<td>

[ ![Image \(37\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/Image-37-_31.jpg) ](<https://www.amourdecuisine.fr/article-gateaux-sans-cuisson-aux-cereals.html>) 
</td>  
<td>

[ ![homemade raffaello easy recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/raffaello-facon-maison-130x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/raffaello-facon-maison.jpg>) [ Raffaello House ](<https://www.amourdecuisine.fr/article-raffaello-fait-maison-recette-facile.html>) 
</td>  
<td>

[ ![truffles with coconut 011.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/truffes-a-la-noix-de-coco-011.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/truffes-a-la-noix-de-coco-011.CR2_.jpg>) [ truffles with coconut ](<https://www.amourdecuisine.fr/article-truffes-a-la-noix-de-coco.html>) 
</td>  
<td>

[ ![Truffle-dates-and-chocolat_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/truffes-dattes-et-chocolat_thumb-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/truffes-dattes-et-chocolat_thumb.jpg>) [ truffles dates and chocolate ](<https://www.amourdecuisine.fr/article-truffes-chocolat-dattes.html>) 
</td> </tr> </table>

**traditional Algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/aftir3-1024x755.jpg)

portions:  40  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * almonds 
  * sugar 
  * flour 
  * egg 



**Realization steps**

  1. baked 
  2. without cooking 
  3. fried 


