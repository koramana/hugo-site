---
title: chocolate cupcakes, pecan topping / coconut
date: '2013-12-26'
categories:
- dessert, crumbles and bars
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/cupcakes-au-chocolat-pecan-et-noix-de-coco.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/cupcakes-au-chocolat-pecan-et-noix-de-coco.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/cupcakes-au-chocolat-pecan-et-noix-de-coco.jpg>)

##  chocolate cupcakes, pecan topping / coconut 

Hello everybody, 

Who wants some of these pretty cupcakes ???? chocolate cupcakes, garnished with a beautiful and delicious ball of pecans, and coconut, decorated with a beautiful layer of chocolate icing, huuuuuuuuuuum, me anyway I want one or two, especially now to drip it ... 

It's a Lunetoiles recipe, and it must be a delight, then to you the recipe 

**chocolate cupcakes, pecan topping / coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/cupcakes-au-chocolat-topping-pecan-et-noix-de-coco.jpg)

Recipe type:  cupcakes, dessert, taste  portions:  12  Prep time:  40 mins  cooking:  18 mins  total:  58 mins 

**Ingredients** chocolate cupcakes 

  * 1 cup flour (cup of 240 ml) 
  * ½ cup light brown sugar. 
  * ⅓ cup of powdered sugar 
  * ⅓ cup of cocoa powder 
  * 1 teaspoon baking powder 
  * ½ teaspoon of baking soda 
  * 1 pinch of salt 
  * ⅓ cup of vegetable oil 
  * ⅓ cup of milk 
  * 1 big egg 
  * 1 teaspoon of vanilla extract 
  * ⅓ cup hot water 
  * ¼ cup sour cream (whole cream to which you add a few drops of lemon juice) 

Coconut and Pecan Topping 
  * ½ cup unsweetened condensed milk 
  * 2 large egg yolks 
  * ½ cup of light brown sugar packed 
  * ¼ cup salted butter, cut into pieces 
  * ½ teaspoon of vanilla extract 
  * 1 cup sweetened grated coconut 
  * ¾ cup chopped pecans 

Chocolate icing 
  * 150 gr of semi-sweet chocolate, chopped 
  * 6 tablespoons salted butter, softened 
  * 85 gr softened cream cheese (philadelphia) 
  * 1 + ¼ cup caster sugar 
  * 2 + ½ cuil. cocoa 
  * 2 - 4 tablespoons thick cream as needed 



**Realization steps** For cupcakes: 

  1. Preheat the oven to 175 degrees. In a large bowl mix the flour, brown sugar, granulated sugar, cocoa powder, baking powder, baking soda and salt for 30 seconds. 
  2. Add vegetable oil, milk, egg and vanilla extract and beat with an electric mixer on low speed for 2 minutes. Pour in hot water and mix well. Add the sour cream and mix until well blended. 
  3. Fill the doubl-lined paper muffin cups and bake in preheated oven for 16 to 19 minutes, until a toothpick inserted in the center of the cake comes out clean (or with a few damp crumbs). 
  4. Remove from the oven and let cool in the muffin cups for a few minutes, then transfer to a rack and allow to cool completely. 
  5. Once cooled, garnish each cupcake with 1 tablespoon of coconut / pecan garnish (form a mound in the center), then conduct the chocolate icing around cupcakes. 
  6. Store in an airtight container.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/DSC09695.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/DSC09695.jpg>)

For the coconut pecan Topping: 
  1. In a medium saucepan, combine the condensed milk, egg yolks, brown sugar and butter. Cook the mixture over medium heat, stirring constantly, until thickened, about 10 - 15 minutes (mixture should almost come to a boil and when you run a rubber spatula along the bottom, you should see the bottom of the pan for a moment). Remove from heat and strain through a mixing bowl. Stir in vanilla, then mix in grated coconut and chopped pecans and let cool to room temperature. 

For chocolate icing: 
  1. Place the semi-sweet chocolate in a microwaveable bowl, then microwave in 50% power in 20-second intervals, stirring after each interval until melted and smooth, set aside and let cool. a little. 
  2. In the bowl of an electric mixer equipped with the paddle connection (an electric hand mixer would work fine too), whisk together butter and cream cheese at medium-high speed until smooth and fluffy. 
  3. Mix in the cocoa powder at low speed, then mix in the powdered sugar. With the beater running, slowly then in the melted chocolate and mix until homogeneous. 
  4. Add the cream 1 teaspoon at a time, as needed to obtain the desired consistency. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/cupcakes-au-chocolat-topping-pecan-noix-de-coco.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/cupcakes-au-chocolat-topping-pecan-noix-de-coco.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
