---
title: rfiss grilled semolina cakes with date paste
date: '2011-10-31'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/rfiss-037_thumb1.jpg
---
![rfiss 037](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/rfiss-037_thumb1.jpg)

##  rfiss grilled semolina cakes with date paste 

the rfiss this little Algerian delicacy to several nominations: Rfiss, Rfiss tounsi, Tamina, and full, many others (if you name it otherwise say the me) 

Frankly, I've always made this recipe, but since you are numerous to ask me the detailed recipe, here are some measures that can help you to make rfiss semolina cakes grilled with date paste.   


**rfiss grilled semolina cakes with date paste**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/rfiss-038_thumb.jpg)

**Ingredients**

  * 300 gr of medium semolina 
  * 350 gr of date paste 
  * 100 to 150 gr of soft butter 



**Realization steps**

  1. grill the semolina, according to your taste, put in a terrine, butter and date paste, add the semolina all hot on it, and try to mix all together. 
  2. you can start using a spatula, until you can stand the heat, and then knead with your hands, the date paste should be well incorporated in the semolina. 
  3. then shape to your taste. 
  4. for me, I made small sausages, which I then decorated with a mold in Makrout, and cut in rhombus, to make my cupcakes 
  5. simple recipe, and any easy 
  6. good tasting. 



bonne journee 
