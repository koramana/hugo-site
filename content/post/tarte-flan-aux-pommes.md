---
title: Apple pie cake
date: '2018-02-27'
categories:
- dessert, crumbles and bars
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tarte-flan-aux-pommes-2.jpg
---
![Apple pie cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tarte-flan-aux-pommes-2.jpg)

##  Apple pie cake 

Hello everybody, 

a pie with flan and apples .... when I read this message from Lunetoiles who had this title, I was just waiting to read the ingredients .... 

and oh ... what I melted looking at the pictures ... .. so if like me, this recipe tempts you, I pass you the ingredients, and the method of preparation:   


**Apple pie cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tarte-flan-aux-pommes-1.jpg)

Recipe type:  pie, apple dessert  portions:  8  Prep time:  30 mins  cooking:  60 mins  total:  1 hour 30 mins 

**Ingredients** Sweet pie dough : 

  * 250g of flour 
  * 90 g icing sugar 
  * 100 g of butter 
  * 4 egg yolks 

For the blank device: 
  * 50 cl of whole milk 
  * 25 cl of cream 
  * 125 g of sugar 
  * 6 egg yolks 
  * 50 g of maizena 

Syrup: 
  * 50 g of sugar 
  * 50 g of water 

4 apples, peeled, sliced 

**Realization steps** For the pastry: 

  1. Put the flour and sifted icing sugar in a bowl. Add the ointment butter and squeeze it. 
  2. When there are no more large pieces of butter, stir in the egg yolks. Keep cold for 30 minutes. Reduce to 3 mm thick on a floured work surface and darken a pie pan 28 cm in diameter. Pass around the edge with the roller to drop the excess dough. Prick the dough with a fork. 
  3. Refrigerate time to prepare the filling. 

For the blank device: 
  1. Bring the milk and cream to a boil. 
  2. In a bowl, mix the egg yolks and the sugar, then the cornflour. 
  3. Pour the boiling milk over the previous mixture while whisking. 
  4. Pour everything back into the saucepan and heat again. 
  5. Mix until the cream thickens, count about 30 seconds. after resumption of boiling. 
  6. Pour the cream into the bowl and film in contact with the cream, to prevent skin from forming. Let cool. 
  7. Preheat the oven to 180 ° C 
  8. Once the cream has cooled, pour it into the bottom of the dough and spread well uniformly. 
  9. Garnish with apple slices, forming a rosette. 
  10. Put in the oven for 30 minutes, then lower the oven temperature to 160 ° C and continue cooking for 30 minutes. 

Syrup: 
  1. Mix 50 g of sugar with 50 ml of water and boil. 
  2. Brush the syrup pie with the brush at the oven exit and put it back in the oven for a few seconds. 
  3. Remove the pie from the oven and allow to cool. 
  4. Place the pie for several hours in the refrigerator before serving 



![Apple pie cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tarte-flan-aux-pommes.jpg)
