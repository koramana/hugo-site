---
title: gratin of cauliflower with cream of spinach
date: '2013-02-20'
categories:
- Algerian cuisine
- diverse cuisine
- ramadan recipe
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-chou-fleur-aux-epinards_211.jpg
---
Hello everybody, 

a super delicious au gratin of cauliflower garnished with a beautiful layer of bechamel cream flavored with spinach, a cream that I have already realized to garnish a potato gratin, that I love, then I tried the recipe with cauliflower, and the recipe was a real delight: 

ingredients: 

  * an average cauliflower 
  * 250 gr of spinach 
  * sauce [ Bechamel ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile-99197863.html>)
  * 1/2 onion 
  * grated cheese. 
  * 1 tablespoon of oil 
  * salt and black pepper. 



method of preparation: 

![gratin-of-cauliflower-with-spinach 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-chou-fleur-aux-epinards_211.jpg)

  1. Cut the core of the cauliflower. 
  2. Separate it into bouquets and cook in salted boiling water 
  3. grate the onion, and sauté in oil until you have a nice translucent color. book 
  4. Wash and cut the spinach, and cook in slices in a pan with a little oil. 
  5. add the onions. 
  6. and add this mixture to the sauce [ Bechamel. ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile-99197863.html>)
  7. in a buttered baking tin, place the cauliflower flower 
  8. cover with a nice layer of cream of spinach (bechamel + spinach) 
  9. then cover with grated cheese. 
  10. bake in a preheated oven at 180 degrees. for 15 minutes. 
  11. enjoy with a fresh salad. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
