---
title: I participate in the competition of sunny verrines
date: '2011-07-06'
categories:
- recettes patissieres de base

---
![http://amour-de-cuisine.com/wp-content/uploads/2011/07/Photo1.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/480x480/3/52/23/76/)

Hello everybody, 

only yesterday, I began to read and validate the comments left by my readers and my friends, and among them, there was a kind invitation from my friend Linda, to participate in his game: 

luck has it that even so, I can participate within 24 hours of the closing of the participations. 

a participation, just for the sake of sharing with my friends and with you, this beautiful little experience, and besides, I like competitions, not to win, but in order to find a whole pile of recipes in the same place. 

this time, it is with our Linda that all these pretty glasses will come together, and here are my 2 recipes, which I will share with pleasure: 

##  [ VERRINES HOUMOUS TOMATOES CUCUMBER CARROTS ](<https://www.amourdecuisine.fr/article-verrines-homous-tomates-concombre-carottes-74047006.html> "Verrines hummus tomatoes cucumber carrots")
