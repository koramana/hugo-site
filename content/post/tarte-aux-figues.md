---
title: fig tart
date: '2013-10-05'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-aux-figues-008a_thumb.jpg
---
##  fig tart 

Hello everybody, 

A delicious fig tart, that I do and redo when it is the season of figs, an amandine pie with figs is no more good. Da'illeurs I really like the pies amandines you will find something for everyone on my blog. 

[ almonds in the pastry ](<https://www.amourdecuisine.fr/article-38772190.html>) , [ strawberry amandines pie ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-fraises-73876989.html>) , [ Pear Almond Tart ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-poires-67143347.html>) , [ raspberry pears amandine pie ](<https://www.amourdecuisine.fr/article-tarta-amandine-poires-framboises-81218958.html>) , [ pear frangipane tart ](<https://www.amourdecuisine.fr/article-tarte-frangipane-poire-45915918.html>) , [ rhubarb / frangipane pie ](<https://www.amourdecuisine.fr/article-31499515.html>) , and [ frangipane pie and pastry cream ](<https://www.amourdecuisine.fr/article-26492437.html>)

For the basic recipe, I adopted and it will be forever, hihihihi the bottom of my pies, because it is a sublime paste, the dough of lunetoiles, it must be tried, especially if you have a blinder, the dough is only likely to be better, your scale and shield .... and here we go:   


**fig tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-aux-figues-008a_thumb.jpg)

Recipe type:  pie and tartlet, dessert  portions:  8  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients** Sanded almond paste: 

  * 180 g flour 
  * 1 pinch of salt 
  * 20 g of almond powder 
  * 50 g of sugar 
  * 1 sachet vanilla sugar 
  * 80 g of butter 
  * 1 egg 
  * 1 tsp. liquid vanilla 

Almond cream: 
  * 120 g of almond powder 
  * 2 eggs 
  * 100 g of sugar 
  * 200 ml whole liquid cream 
  * 4 to 5 figs cut in quarters or slices. according to your taste. 



**Realization steps** The shortbread dough: 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a paste is removed from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Darken a pie circle 28-30 cm in diameter. 
  6. Stir the batter stock with a fork and chill for the time to prepare the filling.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-frangipane-aux-figues1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-frangipane-aux-figues1_thumb.jpg>)

The amandine cream: 
  1. In the bowl of the beater this time equipped with the whisk, beat the eggs with the sugar. 
  2. When the mixture is frothy add the almond powder and the cream. 
  3. garnish the pie shell with the almond cream. 
  4. decorate with the pieces of figs 
  5. Put in the oven for 30 minutes at 180 ° C (th.6). 
  6. Let cool. When the tart is warm, heat the jam in the microwave and brush the jam pie with a brush. 
  7. unmould and present on a serving platter. 



thank you for your visits and comments 

bonne journée 
