---
title: Kefta has coconut
date: '2013-06-08'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikrate-006_211.jpg
---
Hello everyone, An Algerian cake, all beautiful, all good, and super easy to realize known under the name of Kefta has the coconut. Personally, I really like to make this cake, because it is of incomparable ease, the trick with it is not to abuse with the amount of fat to put in, some people think that 1 ca soup of butter is not many, but in fact, when shaping, the coconut tends to reject its oil .... Preparation time: 20 min Cooking time: 00 min Ingredients: 3 measures & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.85  (  1  ratings)  0 

[ ![https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikrate-006_211.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikrate-006_211.jpg) ](<https://www.amourdecuisine.fr/article-25345462.html>) Hello everyone, An Algerian cake, all beautiful, all good, and super easy to realize known under the name of Kefta has the coconut. Personally, I really like to make this cake, because it is of incomparable ease, the trick with it is not to abuse with the amount of fat to put in, some people think that 1 ca soup of butter is not many, but in fact, when shaping, the coconut tends to reject its oil .... [ ![https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikrate-007_21.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikrate-007_21.jpg) ](<https://www.amourdecuisine.fr/article-25345462.html>) **Preparation time : 20 min  ** **Cooking time : 00 min  **

Ingredients: 

  * 3 measures of coconut 
  * 1 measure icing sugar. 
  * 1 measure of milk powder. 
  * 1 tablespoon melted butter 
  * dyes according to your taste 
  * orange blossom water to water. 



for the stuffing: 

  * dry cakes 
  * jam. 



filling: crystallized sugar [ ![skikratecor](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikratecor_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25345462.html>) Method of preparation: 

  1. prepare the stuffing: crush the cookies, mix with jam to have a malleable paste. 
  2. form rolls 1 cm thick. 
  3. now mix the coconut, the icing sugar, the milk powder, 
  4. add the spoonful of butter, and wet with orange blossom water, it is a paste easy to make and well shaped, 
  5. at this point you can divide your dough and add different colors, 
  6. form sausages of almost 2 cm, make a slit in it and place the small biscuit pudding and jam. 
  7. close the coconut rolls, and roll them in crystallized sugar 
  8. cut diamonds. 


