---
title: spaghetti with bolognese sauce
date: '2017-11-17'
categories:
- diverse cuisine
- Cuisine by country
tags:
- dishes
- Italy
- Algeria
- Full Dish
- Ramadan 2017
- Easy cooking
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/11/spaguettis-a-la-sauce-bolognaise-1.jpg
---
![spaghetti with Bolognese sauce 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/spaguettis-a-la-sauce-bolognaise-1.jpg)

##  spaghetti with bolognese sauce 

Hello everybody, 

If there is a recipe that I like to make when I'm lazy and I do not really want to go to the kitchen, it's the spaghetti sauce Bolognese. in addition I know that no one at home will claim or who will not finish his plate. This is the dish that is unanimously at home! 

**spaghetti with bolognese sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/spaguettis-a-la-sauce-bolognaise-3.jpg)

**Ingredients**

  * spaghetti 
  * 1 tablespoon of butter 
  * 2 tbsp. extra virgin olive oil 
  * 1 onion, finely chopped 
  * 2 crushed garlic cloves 
  * a finely chopped celery stalks 
  * 1 carrot, finely chopped 
  * 500 gr chopped veal 
  * 1 glass of water 
  * a little freshly ground nutmeg 
  * 1 can of tomatoes in pieces, otherwise 4 to 5 fresh tomatoes 
  * salt, pepper, basil, oregano, and thyme 



**Realization steps**

  1. Boil the spaghetti in salted water with 2 tablespoons of oil 
  2. In a heavy-bottomed pan, melt the butter and add the oil over medium heat. 
  3. Fry the onion until it becomes translucent, 
  4. add crushed garlic, grated carrot and chopped celery, cook for about 4 minutes. 
  5. Add ground meat, salt, and a little black pepper. 
  6. cook until the meat changes color. 
  7. Add water and nutmeg and simmer 
  8. add the remaining spices and herbs, and cook until the water is completely evaporated. 
  9. Add the tomatoes now and mix well. 
  10. Reduce the heat and simmer very slowly uncovered, stirring occasionally. 
  11. Rectify the seasoning. 
  12. Drain spaghetti after cooking, and wash with plenty of cold water to stop cooking 
  13. put the spaghetti back in the pan with the butter and fry. Season with a little black pepper. 
  14. present your spaghetti dish garnished with Bolognese sauce. you can add cheese of your choice. This dish is a delight. 



![spaghetti with bolognese sauce 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/spaguettis-a-la-sauce-bolognaise-2.jpg)
