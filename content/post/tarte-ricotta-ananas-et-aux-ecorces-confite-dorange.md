---
title: pineapple ricotta pie with candied orange peel
date: '2013-08-26'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/ricota14_2_thumb1.jpg
---
I bring you this delicious recipe that I made a long time ago. 

so before the recipe, I tell you my story with this cake, that I had prepared a week ago, but that I had not yet post: 

**Me** in the kitchen: Rayan, what are you doing? I do not like this silence .......... 

**Him** in the living room: Nooooooooooooooooooo (he likes to say that my son) 

**Me** : I'm coming 

**Him:** Noooooooooooooo rayan photographer 

Me (very concerned about the kitchen): it's good you want to become a photographer, you'll take pictures of mom? 

Him: Winkiwinki (his teletubbies) Thomas (a train he loves a lot and who goes on TV) 

I finish what I have on hand, and I go back to the salon, and rayan gave me the back, I did not pay attention, then he said: 

Kesserta (so be careful, my son speaks Arabic, French and English, with his childish language) and he means I broke it. 

I come to see and it was my camera in his hands, I do not panic, because he used to touch it, so yesterday I just put the pictures on the pc, and I find that it is missing, eh ben: "kesserta" by rayan that was it, 

so the result, a cake without cooking ......... .euh I mean, today I post a recipe for a cake before it is baked .......... 

Well, the photos after cooking ............... ..Kesserta as said rayan. 

yes, sorry for that, but the cake was very good, and I have to wait for another opportunity to do it, because it was for a small invitation, at home, and since my husband does not eat the cheese cakes, I'm looking for always opportunities to do something.   


**pineapple ricotta pie with candied orange peel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/ricota14_2_thumb1.jpg)

Recipe type:  dessert cake  portions:  8  Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients**

  * 250 gr of ricotta 
  * 3 eggs 
  * 1 bulging case of flour 
  * 50 gr of powdered sugar 
  * 2 cases of candied orange peel 
  * pineapple slices in syrup 
  * Optional icing sugar 



**Realization steps**

  1. Break 2 eggs separating the whites from the yolks. 
  2. In a deep dish, crush the ricotta with a fork, add the two yolks and the whole egg, mix well. 
  3. Add flour by sieving, sugar, candied orange peel. 
  4. Beat the egg whites with a pinch of salt and gently add to the ricotta mixture. 
  5. Pour the dough into a missed mold previously lined with a sheet of parchment paper, mine was silicone, so no need for that. 
  6. Add the pineapple slices on top and sprinkle with powdered sugar. 
  7. Bake 35 min and let cool. 
  8. Sprinkle with icing sugar and enjoy. 



as you must never precede things ............ 

I apologize to my little photographer .......... 

yes, I found the pictures of my cake. 

yes, here is the famous cake that supposedly suddenly disappeared from my camera, but in truth ...... 

when I took a picture of the cake preparation, I had to remove the memory card from my camera, to put the pictures taken on the pc. 

then after cooking, I took my camera, and took the rest of the photos, and after, without paying attention, I took the memory card, I put them back, and so the photos were on the camera, but not in the memory card, and it's only now, that I've noticed that. 

and so excuse my little elf, which I like very much, even if it makes a lot of Kesserta .... 

et voila les photos a tous ceux qui étaient déçus, en l’ absence des photos après cuisson 
