---
title: Algerian sfenj
date: '2012-12-11'
categories:
- appetizer, tapas, appetizer
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/khffaf-beignet-0321.jpg
---
![khffaf-donut-032.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/khffaf-beignet-0321.jpg)

##  Algerian sfenj 

Hello everybody 

Whenever I say I have to lose weight, the temptation comes to me, and I find myself making a recipe that only makes things worse, feasting around and filling my stomach ...... my god, finally I do not dramatize things, 

This time, it is with these beautiful Algerian donuts: sfenj Algerian, I do not prepare this delight often, I made them for a special occasion, generally, in family traditions, when a child goes to school, for the first time, we make him donuts or the Algerian sfenj, but will you guess who took the way to school ???? Well yes this sfenj Algerian I did it for such an occasion ... So, I give you time to guess Ok! 

In any case, I do not do this recipe every time in the same way for example you can see the recipe [ Algerian sfenj ](<https://www.amourdecuisine.fr/article-beignets-khfaf-ou-sfendj-64651536.html>) flour, or why not the american version, hihihihi: the famous [ donuts ](<https://www.amourdecuisine.fr/article-donuts-faciles-et-delicieux-68158130.html>) , Without forgetting the [ donuts with cream pastry ](<https://www.amourdecuisine.fr/article-27186708.html>) , and you can have more choices on the category [ donuts ](<https://www.amourdecuisine.fr/categorie-11700496.html>)   


**Algerian sfenj**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/khffaf-beignet-0321.jpg)

portions:  10  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 500 gr of semolina 
  * lukewarm water 
  * 1 teaspoons instant yeast 
  * warm milk 
  * 1 teaspoon salt (see less, you can test after it is good dough or needs more salt) 



**Realization steps**

  1. mix semolina with salt and lukewarm water, knead to a handy dough and let stand. 



![Algerian donut](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/212177221.jpg) ![Algerian donut 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/212177481.jpg)

go back to our pasta and add water as you go along. 

![Algerian donut 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/212177771.jpg) ![Algerian donut 3](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/212178201.jpg)

Add diluted yeast in a little water to your dough, 

And knead then add the milk so the dough must be very soft and easy to handle 

![Algerian donut 4](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/212178351.jpg)

let the dough rise, and then heat a bath of oil. 

lightly wet your fingers, and take a small ball of dough to shape it in a way to have a circle, and dip it in the oil. 

![Algerian donut 5](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/212179091.jpg) ![Algerian donut 7](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/212179371.jpg)

fry on both sides, drain your khefaf. 

To serve, you can cover them with icing or crystallized sugar, or even put them in honey, you can even consume them as simple, 

![khffaf-donut-029.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/khffaf-beignet-029.jpg)

bonne dégustation 
