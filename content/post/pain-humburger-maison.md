---
title: homemade hamburger bread
date: '2014-04-09'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pain-pour-hamburgers-buns-tres-legers-021_thumb-300x166.jpg
---
#  homemade hamburger bun. 

Hello everybody, 

Yesterday the children had a great desire for burgers, and I remembered this recipe for light buns that I had seen in one of my books, and which I was eager to realize, because pictures, we He was able to see a half-run, very airy, super light buns. 

and this was the result, and it must be said that before making hamburgers, more than half of the buns, have disappeared ... the children kept asking for more, try the recipe, and you will understand why these buns disappear in no time. 

in the book, the measurements are in "cup", so cup, and I used, the "cups special measures" that I have, knowing that 

1 cup = 240 ml 

ingredients for 16 small humbergers (for children) 

if you want bigger you can make 10 buns to have nice American burgers, hihihihi ....   


**homemade hamburger bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pain-pour-hamburgers-buns-tres-legers-021_thumb-300x166.jpg)

portions:  12  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients**

  * 3½ cups all-purpose flour   
1 cup = 240 ml 
  * 1 sachet of instant baker's yeast 
  * ¼ cup of potato starch (bag of starch to prepare mashed potatoes) 
  * 3 tablespoons milk powder 
  * 1 tablespoons sugar 
  * 1½ teaspoons of salt 
  * 4 tablespoons of soft butter 
  * ⅔ cup warm water (I was not satisfied with the texture of the pasta I had to add more water) 
  * ½ cup warm milk 



**Realization steps**

  1. mix all the ingredients to have a dough that picks up 
  2. knead the dough in your hands for almost 15 minutes to get a smooth dough   
(It may stick, so use the butter gently, knead it and remove it from your hands, if you do it at the kneader, knead for 7 minutes, and if you make it to the bread machine, make the program dough , and go do something else, hihihihi)   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pain-pour-hamburgers-buns-super-facile-et-leger_thumb1.jpg)
  3. Continuing with the people who are working on the hands, or the kneader, gather the dough into a ball and put it in a buttered container, and let rise until it doubles in volume almost between 60 and 90 minutes. 
  4. Once the dough is lifted, degas it gently. and turn it into 16 pieces, to make 16 buns. 
  5. butter lightly with mussels of 23 cm each, and place the buns in according to your taste, I like that the buns stick to each other during the lifting and cooking, but if you want separate buns, plan larger mussels. 



  1. Cover the mussels, and let the buns rise until they are stacked against each other and swollen enough, about 60 to 90 minutes. 
  2. preheat the oven to 180 ° C. 
  3. cook the buns for 15 to 20 minutes (or depending on your oven), until the buns turn a nice golden color. 
  4. Remove the loaves from the oven and immediately brush with melted butter. You will need 1 to 2 tablespoons of melted butter, it will give a shine, and a final texture to the buns, to fall. 



and for the recipe [ homemade burgers ](<https://www.amourdecuisine.fr/article-hamburgers-fait-maison-112343938.html>)

[ ![homemade burger 022.](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/hamburger-maison-022._thumb.jpg) ](<https://www.amourdecuisine.fr/article-hamburgers-fait-maison-112343938.html>)
