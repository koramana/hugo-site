---
title: Crumble verrines with star anise pears
date: '2011-07-22'
categories:
- the tests of my readers

---
Hello everybody, 

here is a dessert that has just been enjoyed with much pleasure, between a fondant and crunchy taste, its verrines are very easy to prepare, but maybe you must plan a night of pear marinade. 

so it was during my walk on a site that I found a video Pascale, or she presented this delight. 

she had natural pears that she had marinated in lemon juice, but I had canned pears, as I prepare almost every time the Bavarian, I always plan fruits in boxes, for cravings surprises, hihihiihih 

so I pass you the recipe: 

for 6 verrines: 

  * 2 boxes of pears in syrup (225 gr each) 
  * 6 grains of star anise 
  * 2 tablespoons fresh cream 
  * 100 gr of flour 
  * 100 gr of sugar 
  * 100 gr of almond powder 
  * 100 gr of butter 



method of preparation 

  1. at night, open the pears, add the star anise seeds, and let marinz 
  2. the same day, pour all the dry ingredients in a salad bowl. 
  3. Add the butter cut in small pieces. 
  4. Work the dough with your fingertips until all the butter is incorporated. 
  5. Sand the mixture with your fingers, you must get some kind of coarse sand. 
  6. Preheat the oven to 180 ° C 
  7. Spread the crumble dough on a baking sheet 
  8. Bake for 15 minutes, until the dough is golden brown. 
  9. Heat a skillet over high heat, 
  10. add the diced pears and their juice (not all the juice) 
  11. add the fresh cream 
  12. Brown for 3 minutes. Book. 
  13. Take the crumble dough out of the oven. 
  14. Spread the pears in the glasses, making sure to put a star anise star in each glass 
  15. Divide the crumble dough over the pears 
  16. serve immediately. 



a real delight that we love, and that will not fail to be the dessert that I will present if you come home, hihihihiih 

Thank you for your visit 

Thank you for your feedback 

have a good day 
