---
title: gingerbread with candied orange / lemon peel
date: '2011-01-18'
categories:
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pep_thumb1.jpg
---
[ ![pep](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pep_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25885107.html>) [ ![PEP1](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pep1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25885107.html>)

here is delicious mini gingerbread, which I liked to share with my children, a treat 

recipe: 

  * 145 grams of flour (normal for me, I had the complete or other) 
  * 3/4 c. yeast 
  * 1/2 c. coffee of the spice special gingerbread (cinnamon, anise ginger ...) 
  * 45 g brown sugar (brown sugar) 
  * 1/2 glass of milk 
  * 1 case and a half of honey 
  * 1 egg 
  * orange / lemon peel 
  * 1/2 c.a baking coffee 



[ ![pep6](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pep6_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25885107.html>) [ ![Pep5](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pep5_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25885107.html>)

[ ![pep4](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pep4_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25885107.html>) [ ![PEP3](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pep3_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25885107.html>)

mix all the ingredients, put in the imprints of your mold (I had 6 rolls) 

or in a small cake tin and bake in a preheat oven at 180 degrees C 

turn the baking with the knife blade, or as I do, with a toothpick 

[ ![pep2](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pep2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25885107.html>) [ ![pep7](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/pep7_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25885107.html>)

en tout cas moi j’ai bien aimer la recette, et je suis partante pour un autre essai 
