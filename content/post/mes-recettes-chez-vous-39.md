---
title: my recipes at home 39
date: '2013-12-11'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

Thanks to my dear readers who have tried to contact me by email, to send me the pictures of the recipes you have to try on my blog. 

I put you, a system much faster so that you can send me your photos, just go on this page: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll like ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link of your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)

the link will always be visible to you, it is in the menu, at the top of the page .... 

so, now we start the parade, recipes that you have tested from my blog? 

![footed-020-c46a16f1d9042af929e93badda2052f35b25df68](https://www.amourdecuisine.fr/wp-content/gallery/1/kobez-020-c46a16f1d9042af929e93badda2052f35b25df68.jpg)

[ bread without filling ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html> "khobz dar without food / homemade bread") at Fleurdz (a reader) 

![VLUU L100, M100 / Samsung L100, M100](https://www.amourdecuisine.fr/wp-content/gallery/1/001-6ad3b3704d0b9650a9099cd9a7b1399ab917edd9.jpg)

[ Semolina bread ](<https://www.amourdecuisine.fr/article-pain-de-semoule.html> "semolina bread") to a reader (who does not mention her name) 

![Epinay-sur-Seine from 20,131,202 to 03,322](http://lesdelicesdenounou.files.wordpress.com/2013/12/epinay-sur-seine-20131202-03322.jpg?w=490&h=367)

![<KENOX S730 / Samsung S730>](https://www.amourdecuisine.fr/wp-content/gallery/1/braj-005-7da2c722ffd6a9ce7e0e3151ccaec521b8799e21.jpg)

[ Bradj, Algerian semolina cake ](<https://www.amourdecuisine.fr/article-bradj-gateau-de-semoule.html> "Bradj - semolina cake براج بالتمر") at Malika, a reader. 

![<KENOX S730 / Samsung S730>](https://www.amourdecuisine.fr/wp-content/gallery/1/besbes-004-2276ee68c367af2eec9a1ac4dd6083ee1db94314.jpg)

[ tajine with fennel, tajine and besbes ](<https://www.amourdecuisine.fr/article-tajine-el-besbes-au-four-gratin-de-fenouil.html> "tajine el besbes in the oven, gratin of fennel") at Malika, a reader 

![<KENOX S730 / Samsung S730>](https://www.amourdecuisine.fr/wp-content/gallery/1/besbes-002-7525133c3a661851e01a0843ad4a475fa41053fe.jpg)

[ Zaalouk ](<https://www.amourdecuisine.fr/article-zaalouk-d-aubergines.html> "Zaalouk aubergines, Moroccan cuisine") , at Malika, a reader 

![<KENOX S730 / Samsung S730>](https://www.amourdecuisine.fr/wp-content/gallery/1/besbes-001-2ffe6afc8d34dd4c1153034899ed961d5699cf2c.jpg)

[ Matlou3 ](<https://www.amourdecuisine.fr/article-25345316.html> "Matlouh, matlou3, matlouh - home-made tajine bread") at Malika 

the word of Malika: 

thank you Soulef, for the page [ my recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") , I think it's easier in this way, to post photos directly to you ... Your recipes inspire me a lot, and I must admit, that before sleep, I always go to your blog, I choose the menu of the next day In the morning, I take the children to school, buy the necessary ingredients, and go to the kitchen. 

you are the best cookbook I have, your recipes are not only very easy and not complicated, they are also very delicious. 

Thank you 

Malika 
