---
title: Black Forest Smoothie
date: '2017-08-12'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-foret-noire.jpg
---
[ ![black forest smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-foret-noire.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-foret-noire.jpg>)

##  Black Forest Smoothie 

Hello everybody, 

Here is a beautiful smoothie recipe, a black forest smoothie, Frankly a delight that will make you dream with every sip. It's too good, super strong in taste ... Personally I really like the strong taste of cocoa, and when my smoothies contain cocoa, I can eat 2 to 3 large glasses and get rid of the meal completely, hihihihi . 

**Black Forest Smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-foret-noire-21.jpg)

portions:  2  Prep time:  3 mins  total:  3 mins 

**Ingredients**

  * 1 glass of frozen cherries 
  * 2 to 3 dates without seeds cut into slices 
  * ¼ glass of oat bran 
  * 1 tablespoon of Chia seeds 
  * 2 tablespoons bitter cocoa (you can put less) 
  * 1 glass of [ almond milk ](<https://www.amourdecuisine.fr/article-lait-d-amande-maison.html>) unsweetened 
  * ¼ cup of vanilla sugar 
  * ½ bananas 
  * 1 cup of honey 



**Realization steps**

  1. start by mixing the oat bran to have a fine powder 
  2. add the remaining ingredients to have a smooth smoothie 
  3. consume immediately well fresh 



[ ![black forest smoothie-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-foret-noire-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-foret-noire-001.jpg>)
