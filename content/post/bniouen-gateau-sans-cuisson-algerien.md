---
title: bniouen cake without cooking Algerien
date: '2011-12-19'
categories:
- Cuisine saine
- dessert, crumbles and bars
- Gateaux au chocolat
- gateaux, et cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/l-binouen-0101.jpg
---
![bniouen cake without cooking Algerien](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/l-binouen-0101.jpg)

##  bniouen cake without cooking Algerien 

Hello everybody, 

A delicious Algerian cake that I appreciate a lot, because it is a cake without cooking, beautiful is the recipes that must be done, if you do not need to be a lot of time in the kitchen or control the cooking in the oven. 

![Picture-1715.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/Picture-17151.jpg)

Ingredients: 

  * 2 boxes of biscuits (in England it's tea biscuit, in France it's LU cakes ???) 
  * softened butter 
  * honey according to taste 
  * Turkish halwat (I have to look for his name in French, but it's a kind of sugar sesame paste, I like that) 
  * cocoa 
  * a box of dark chocolate 
  * crushed peanuts or almonds or nuts, pistachios (all this is optional) for me pistachios 



We start by passing the biscuits to the blender. 

and add after the cocoa, and the Turkish halwat, mix everything well, then add honey, crushed pistachios and soft and softened butter, mix well, add honey according to taste, and butter as the dough becomes manageable, be careful not to put too much. 

take the mixture in a tray, which can be put in the refrigerator, and spread our dough, by hand until having a height which goes from 3 cm to 4 cm. 

We place the chocolate in a bain marie, to melt it, and we cover our cake, you can decorate the top of the chocolate with coconut, or peanuts, or other. 

we place in the fridge. 

after the dough becomes stronger, we cut into lozenges, and we place our little delights in boxes.   


**bniouen - cakes without cooking Algerian**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/lbniouen-4.jpg)

**Ingredients**

  * 2 boxes of biscuits (in England it's tea biscuit, in France it's LU cakes ???) 
  * softened butter 
  * honey according to taste 
  * Turkish halwat (I have to look for his name in French, but it's a kind of sugar sesame paste, I like that) 
  * cocoa 
  * a box of dark chocolate 
  * crushed peanuts or almonds or nuts, pistachios (all this is optional) for me pistachios 



**Realization steps**

  1. so we start by passing the biscuits to the blinder. 
  2. and add after the cocoa, and the Turkish halwat, mix everything well, then add honey, crushed pistachios and soft and softened butter, mix well, add honey according to taste, and butter as the dough becomes manageable, be careful not to put too much.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/bniouen-331.jpg)
  3. take the mixture in a tray, which can be put in the refrigerator, and spread our dough, by hand until having a height which goes from 3 cm to 4 cm. 
  4. We place the chocolate in a bain marie, to melt it, and we cover our cake, you can decorate the top of the chocolate with coconut, or peanuts, or other. 
  5. we place in the fridge. 
  6. after the dough becomes stronger, we cut into lozenges, and we place our little delights in boxes. 



![lbniouen-4.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/lbniouen-41.jpg)

bon appetit 
