---
title: panna cotta with mascarpone
date: '2016-09-02'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/panna-cotta-a-la-rose-1_thumb1.jpg
---
##  Pannacotta with mascarpone 

Hello everybody, 

The panna cotta is one of my favorite desserts, I really like its texture in the mouth, frankly it must be tasted to feel this sweetness and how this dessert melts in the mouth. For a variation, I prepared this delicious panna cotta with mascarpone and rose water. We loved it at home and especially the decor that my daughter absolutely loved. 

So it's not going to be that to seduce your spouse for a one-on-one dinner, even the kids will love each other a lot. 

You can see my others [ panna cotta recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=panna+cotta&sa=Rechercher>)   


**panna cotta with mascarpone**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/panna-cotta-a-la-rose-1_thumb1.jpg)

**Ingredients**

  * 250 g of mascarpone 
  * 150 ml of fresh cream 
  * 120 ml of milk 
  * 100 ml of water 
  * 3 tablespoons caster sugar 
  * 3 teaspoons of gelatin or 8 grams of gelatin in foil 
  * ½ teaspoon of rose water 
  * A drop of pink dye 



**Realization steps**

  1. method of preparation: 
  2. Dissolve the gelatin in boiling water, if it is powdered, or put it in cold water to swell if it's leaves. 
  3. mix the mascarpone, cream, milk and water together until the mixture becomes homogeneous. 
  4. place in a heavy saucepan and stir until simmering. 
  5. add the sugar 
  6. remove from the heat and add the gelatin, and mix until it melts 
  7. add the pink dye and the rose water. 
  8. Let stand a little to cool then pour into bowls or glasses. 



si vous voulez décorez avec une coulis de fraises gélifié, juste préparez un coulis de fraises, et ajoutez un peu de gélatine dedans 
