---
title: arayeche, pistachio larch with Algerian cake
date: '2014-07-21'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/arayeche-au-pistache-041a_thumb1.jpg
---
##  arayeche, pistachio larch with Algerian cake 

Hello everybody, 

it is the feast of the aid el kebir which is approaching with great speed, and although it is the feast of the sheep, as it is said, some families try to prepare cakes for this magnificent occasion. 

Algerian cakes are always present on these occasions, and for my part, I did not want to do big things, just dry cakes that my husband likes, but my daughter asked me for arayeches, she calls them Purple Cake, or the purple cakes, because during the celebration of the aid after Ramadan, I made them in purple. 

and I will be very frank again, well she has to eat all the pretty pieces, and she says: my cakes, so no one touches, the I did you photos, and she did not like when I chew in one of the pieces, hihihiihih 

so as I already have arayeches on my blog: the [ arayeches with almonds ](<https://www.amourdecuisine.fr/article-25345469.html>) , the [ arayeches with honey ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-60064269.html>) , the [ arayeches with coconut ](<https://www.amourdecuisine.fr/article-arayeche-au-miel-amande-et-noix-de-coco-61897731.html>) this time it is the pistachio arayeches, and the taste was sublime.   


**arayeche, pistachio larch with Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/arayeche-au-pistache-041a_thumb1.jpg)

Recipe type:  Algerian cake  portions:  30  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients** For the dough: 

  * 3 measures of flour 
  * 1 measure of cooled melted butter 
  * Vanilla extract 
  * 1 pinch of salt 
  * Rose water. 

For the stuffing: 
  * 3 measures of pistachios 
  * 1 measure of powdered sugar 
  * 1 good c to c pistachio aroma 
  * Rose water 

For frosting; 
  * 2 egg whites (more or less depending on the amount of cakes) 
  * 2 tablespoons lemon juice 
  * 2 tablespoons rose water 
  * Icing sugar 
  * Green food coloring 



**Realization steps**

  1. Prepare the dough with the given ingredients, 
  2. pick up with pink water and let stand a few minutes. 
  3. In the meantime, prepare the stuffing by mixing all the ingredients. 
  4. prepare balls of around 30 gr 
  5. and lower each ball to have a small circle, 
  6. put the stuffing in the shape of a "Y". 
  7. fold over all 3 sides, 
  8. put the cake on the palm of your hand and with the other hand, give it the shape of a star with curved angles, as if it floated in the water (a beautiful starfish). 
  9. Bake at 180 ° C and allow to cool completely before icing. 
  10. Prepare the icing with the ingredients mentioned above, it must not be flowing or too thick, 
  11. always test on a cake and grind by adding icing sugar until you get the desired texture. 
  12. decorate according to your taste, for my part, I really love the decoration of Linda, and it was an opportunity to do it, plus the work was easy, and I find his idea sublime, I like the form zebra or tiger. 



good party of help in advance my friends 

Saha Syamkum 

haj makboul a nos freres, peres, meres soeurs, et incha Allah la prochaine fois ca va etre notre tour pour faire la meque 
