---
title: Castels, Algerian cakes
date: '2015-09-30'
categories:
- Algerian cakes- oriental- modern- fete aid
tags:
- Cookies
- biscuits

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/les-castels-gateau-algerien.jpg
---
#  [ ![the castels, Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/les-castels-gateau-algerien.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/les-castels-gateau-algerien.jpg>)

[ ![castel, algerian cake 2012](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/castel-001_1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-36097103.html>)

##  Castels, Algerian cakes 

Hello everybody, 

here is a delicious Algerian cake, which I made with my mother when I was young, very melting in the mouth, not too sweet (unless you add a little more sugar, hihihih). 

Every time I make these castels, to change the old photos and put the news, we eat them so fast, that I never take them in pictures. This time, ditto, same story, my friend visits me, I open the box to put on the plate, and I find myself with the last 4 pieces ... my only solution and immediately take pictures with my iphone, suddenly, it's not the top, hihihihi 

No problem, this cake is a delight, try it and tell me your opinion. 

**Castels, Algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/castels-gateaux-algeriens-.jpg)

portions:  30  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 300 gr roasted and coarsely crushed peanuts. 
  * 150 gr of flour 
  * 1 C. yeast 
  * 250 gr of butter 
  * 250 gr of icing sugar 
  * 5 egg whites 

decoration: 
  * egg yolks 
  * vanilla sugar 
  * icing sugar (until obtaining the good consistency of icing) 



**Realization steps**

  1. in a bowl, put the peanuts, flour, sugar, vanilla, yeast, and melted butter, mix the goods with a wooden spoon. 
  2. Add the egg whites beaten to snow.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/qmirate-011_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/12/qmirate-011_thumb1.jpg>)
  3. butter a baking dish 35 x 30 cm, and spread the dough well. 
  4. put in the oven and cook on low heat, check the cooking with the tip of a knife. 
  5. icing: 
  6. put the egg yolks on a plate, add the vanilla, work well with a spatula. 
  7. add the sugar gradually until the mixture thickens and becomes clear. 
  8. cover your cake with this frosting, wait for one night until the frosting hardens. 
  9. cut diamonds or squares with a very fine knife, so as not to make too much cracking with icing. 



[ ![castels Algerian cake 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/castels-gateau-algerien-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/castels-gateau-algerien-1.jpg>)

j’ai ajouter un colorant a mon glaçage. bonne dégustation 
