---
title: tajine el foul, tajine with beans
date: '2015-05-10'
categories:
- cuisine algerienne
- recette de ramadan
tags:
- Algeria
- Vegetables
- Healthy cuisine
- Easy cooking
- Full Dish
- Ramadan 2015
- sauces

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tajine-el-foul1.jpg
---
[ ![tajine el foul](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tajine-el-foul1.jpg) ](<https://www.amourdecuisine.fr/article-tajine-el-foul-tajine-aux-feves.html/tajine-el-foul-2>)

##  tajine el foul, tajine with beans 

Hello everybody, 

Here is a delicious bean dish shared with us my friend Nawel Zallouf. Personally I love beans, but I never find good quality here in Bristol, and every time I go to Algeria it is no longer the bean season, it disappoints me so much. 

Nevertheless, we enjoy this time with this delicious dish based on beans, a tajine beans or tajine el foul, all good and all delicious, as she cooks my friend Oran Nawel Zallouf. 

**tajine el foul, tajine with beans**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tajine-el-foul1.jpg)

portions:  4-5  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 1 kg of shelled beans 
  * 1 onion 
  * 500 gr of lamb 
  * 2 tablespoons of oil 
  * salt, black pepper, ginger, cinnamon, some cubèbe (kebaba) 
  * some saffron pestils 
  * 1 small bunch of coriander 
  * water 
  * 1 to 2 potatoes 



**Realization steps**

  1. In a pot, brown the chopped onion in a little oil until it becomes translucent, 
  2. add the pieces of lamb and let it come back well 
  3. Season with salt, pepper, ginger, cinnamon, Cubebe and saffron 
  4. Then add the chopped coriander and cover with water 
  5. cook until the meat becomes tender 
  6. Add the potato peeled and quartered and the beans. 
  7. let it cook again, and remove from heat as soon as it is ready. 


