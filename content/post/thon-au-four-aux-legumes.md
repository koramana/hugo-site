---
title: Baked tuna with vegetables
date: '2015-07-23'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
tags:
- Fish
- tomatoes
- Baked fish
- Oven cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/poisson-au-fout.jpg
---
![baked fish](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/poisson-au-fout.jpg)

##  Baked tuna with vegetables 

Hello everybody, 

In summer, it is always difficult to reflect on a recipe that we can eat quickly, which is good, light, and especially especially, which does not force us to stand for hours in the kitchen. 

Here is a very delicious little dish that we prepare in the blink of an eye, and hop ... in the oven. It's a recipe from a reader of the blog, **Kawthar Dz,** slices of tuna in the oven on a bed of seasonal vegetables that you can change according to your taste and your means. 

**Baked tuna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/poisson-au-fout.jpg)

portions:  2  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 2 to 3 medium potatoes 
  * 2 medium onions 
  * 2 bay leaves 
  * 2 to 3 fresh tomatoes 
  * salt and black pepper 
  * thyme 
  * garlic crushed 
  * chopped parsley 

Marinade 
  * two beautiful slices of tuna 
  * olive oil 
  * cumin, salt, crushed garlic 
  * lemon juice 



**Realization steps**

  1. Marinate the fish in a little olive oil, cumin, salt, and lemon juice 
  2. in a baking dish, put a layer of potato in a thin slice 
  3. a layer onions cut into slice rings 
  4. add 2 bay leaves and sliced ​​tomatoes 
  5. salt and pepper and add thyme over 
  6. place the slices of tuna on top 
  7. add water to the rest of the marinade and sprinkle the contents of the tray 
  8. add some slice of lemon 
  9. decorate with finely chopped parsley and cook in a hot oven at 200 degrees C until the fish and potato are tender. 


