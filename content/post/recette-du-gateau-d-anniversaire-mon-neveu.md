---
title: my nephew's birthday cake recipe
date: '2009-09-07'
categories:
- the tests of my readers

---
with a little delay I post you the recipe that my step sister for her son, when are first birthday. So the ingredients: for the heart-shaped mold: - 4 eggs - 125 g sugar - 125 g pastry flour. - 1/2 sachet of baking powder for the rectangular mold: - 8 eggs - 250 g sugar - 250 g pastry flour. - 1 sachet of baking powder for chocolate pastry cream: 3 egg yolks - 90 g sugar - 20 g cornflour - 30 g 

##  Overview of tests 

####  please vote 

**User Rating:** 2.7  (  1  ratings)  0 

with a little delay I post you the recipe that my step sister for her son, when are first birthday. 

so the ingredients: 

for the heart-shaped mold: 

\- 4 eggs   
\- 125 g of sugar   
\- 125 g of pastry flour.   
\- 1/2 teaspoon baking powder 

for the mold in rectangular shape: 

\- 8 eggs 

\- 250 g of sugar 

\- 250 g of pastry flour. 

\- 1 sachet of baking powder 

for the chocolate pastry cream: 

3 egg yolks   
\- 90 g of sugar   
\- 20 g cornflour   
\- 30 g of flour   
\- 1/2 liter of milk   
\- 1 pinch of salt   
\- 50 g chocolate pieces 

preparation of the sponge cake: 

Mix the yeast with the flour. Separate the whites from the egg yolks, and whip the whites with a pinch of salt.   
When they are firm, add the sugar and beat again. Lower the speed of the robot and add the 4 yolks at once, then immediately the flour and the yeast in rain. 

pour into the mold, smooth the surface and bake immediately for 20 minutes. 

Turn out warm and wait for the cake to cool to cut in half and garnish. 

then soak the cake with a syrup, so that it is very soft. 

Preparation of the chocolate pastry cream:   
Boil the milk. Add the chocolate and mix everything.   
Mix sugar and egg yolk. When this mixture has blanched, add cornstarch, flour and salt.   
Dissolve with the milk, put back on low heat, then continue stirring for 2 minutes until the cream laps the spoon.   
Remove from heat and put in a bowl, rinsed in cold water. 

garnish the center of the cake with the pastry cream, then decorate with melted chocolate in a bain-marie. and garnish according to your taste. 
