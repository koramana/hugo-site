---
title: coffee crème brûlée
date: '2017-12-03'
categories:
- panna cotta, flan, and yoghurt
- sweet recipes
tags:
- desserts
- flan
- Ramadan
- verrines
- Christmas
- Easy cooking
- Christmas dessert

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/12/cr%C3%A8me-brul%C3%A9e-au-caf%C3%A9-2-683x1024.jpg
---
![creme brulee with coffee 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/cr%C3%A8me-brul%C3%A9e-au-caf%C3%A9-2-683x1024.jpg)

##  coffee crème brûlée 

Hello everybody, 

I wanted at all costs to make a salty recipe with evaporated milk, but every time I offered a recipe to my children, they refused! Sometimes it's frustrating, but I was not going to make a recipe to eat it alone. 

My solution suddenly, a recipe they would eat without hesitation, and as I told you, my children love the blanks, especially when it does not smell eggs, and with the taste of coffee, it is sure that this coffee crème brûlée was going to be the big success. 

This was the case, moreover I had to redo the recipe for times, because the time I put in the fridge, the ramekins disappeared one after the other! I do not regret of course, because it is the most important for me, that I make a recipe that my children will appreciate! 

{{< youtube 4YCcxZm3af8 >}} 

Going back to our game, here is the list of all previous rounds, and at the bottom of this article you will find a variety of blogger recipes and blogger based on condensed milk: 

**coffee crème brûlée**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/cr%C3%A8me-brul%C3%A9e-au-caf%C3%A9-4.jpg)

portions:  4 

**Ingredients**

  * 300 ml of liquid cream 
  * 100 ml semi-skimmed milk 
  * 2 tbsp. Nescafé coffee machine 
  * 4 large egg yolks 
  * 6 c. tablespoon Sweetened condensed milk 

To caramelize 
  * Brown sugar 



**Realization steps**

  1. Preheat the oven to 150 ° C, 
  2. Place the cream, milk and coffee powder in a saucepan and slowly bring to a simmer. 
  3. Meanwhile, in a large bowl, combine egg yolks and sweetened condensed milk. 
  4. Remove the coffee cream from the heat and add while whisking in the egg yolk mixture. Pour into six small ramekins, or 4 ramekins special crème brûlée 
  5. Gently place the ramekins in a deep baking dish and fill it with enough boiling water to reach two-thirds of the ramekins. 
  6. Lower the oven temperature to 120 ° C and bake for 40 to 45 minutes or until oven is thoroughly cooked. 
  7. Remove from the oven and place the ramekins on a rack to allow them to cool. Refrigerate! 
  8. To caramelize the crème brûlée, preheat the grill to its highest setting. Sprinkle a thin layer of powdered sugar on each dish, making sure the cream is covered. Place under the rack for about 5-6 minutes until the sugar is melted and golden. Alternatively, use the kitchen torch. 
  9. let cool slightly before serving. 



![coffee creme brulee 3](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/cr%C3%A8me-brul%C3%A9e-au-caf%C3%A9-3-683x1024.jpg)

and here is the list of all the participants: 

1- Soulef  of the blog [ Love of cooking  ](<http://amourdecuisine.fr/>) avec Crème brulée au café 
