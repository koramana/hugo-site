---
title: 'Recipe around an ingredient # 38 the lenses'
date: '2018-03-19'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg)

source: Pinterest 

##  Recipes around an ingredient # 38 

Hello my friends, 

Here we are in round # 38 of our game: recipes around an ingredient, a game that I launched myself and Samar 3 years ago, and that continues and make its way and we continue to make beautiful round , and especially to explore a lot of ingredients, sometimes forgotten ingredients, sometimes ingredients that we do not dare to use often in our kitchen ... With this game, we have to explore and make many recipes with different ingredients: 

Previous rounds: 

![logo-recipe-around-dun-ingrédient38](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/logo-recette-autour-dun-ingr%C3%A9dient38.jpg)

####  _**What is the game Recipe around an ingredient:** _

####  _**How to participate :** _

2 - Make a new recipe (recipes are not accepted) around the star ingredient ie for this month **Lenses.**

3- Address me by 1 April 2018 at the latest the title of your recipe. 

5 - Publish the article on **April 3 at 8 AM** . 

6 - In the article you must: 

  * mention the initiators of the game **Samar and Soulef**
  * add the list of participants with the titles of their recipe 



_If despite your registration, you have not managed to make your recipe, contact me before the 3 so that I remove you from the list and that I inform the other participants. Recipe around an ingredient remains a game so no stress!_

_New since edition # 12_

Une page Facebook a été créée afin que les participantes l’URL de leur blog ainsi que le titre de leur recette, et ceci, afin de faciliter la tâche de la marraine et des participantes. 
