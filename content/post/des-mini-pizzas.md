---
title: mini pizzas
date: '2009-06-24'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/06/258852181.jpg
---
& Nbsp; that's the little pizzas I've prepared for Rayan, who sometimes calls me a pizza at the last minute, I've prepared a nice quantity that I freeze afterwards. so that's all ready at his request. for the dough: 7g yeast baker 1cc sugar 250ml lukewarm water 350g flour 1cc salt 1 case olive oil. prepare your dough by mixing all the ingredients, beat well for 4 to 5 minutes, until you have a nice dough. let it rise for at least 30 minutes, until it doubles & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![mini_pizza2](https://www.amourdecuisine.fr/wp-content/uploads/2009/06/258852181.jpg)

that's the little pizzas I've prepared for Rayan, who sometimes calls me a pizza at the last minute, I've prepared a nice quantity that I freeze afterwards. so that's all ready at his request. 

![mini_pizza](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/258849621.jpg)

for the dough: 

  * 7 grams of baker's yeast 
  * 1 sugar cube 
  * 250 ml warm water 
  * 350 gr of flour 
  * 1 cup of salt 
  * 1 case of olive oil. 



![mini_pizza1](https://www.amourdecuisine.fr/wp-content/uploads/2009/06/258850411.jpg)

prepare your dough by mixing all the ingredients, beat well for 4 to 5 minutes, until you have a nice dough. let it rise for at least 30 minutes, until it doubles in volume. 

you can like me after, devise the dough to have mini pizzas or so prepare a big pizza. 

![mini_pizza3](https://www.amourdecuisine.fr/wp-content/uploads/2009/06/258850961.jpg)

for the filling, you make according to your taste. 

![mini_pizza](https://www.amourdecuisine.fr/wp-content/uploads/2009/06/258851291.jpg)

in any case, the dough was very very good. 

![mini_pizza2](https://www.amourdecuisine.fr/wp-content/uploads/2009/06/258852181.jpg)

bon Appétit. 
