---
title: ghrif, or Corsa, or so a thousand holes crepe arabic
date: '2008-01-04'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/01/baghrir-001.jpg
---
![baghrir-001.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/baghrir-001.jpg)

##  ghrif, or Corsa, or so a thousand holes crepe arabic 

Hello everybody, 

so it's a kind of crepes that we do a lot in the Algerian territory. a semolina delice, which after cooking, is made to drizzle with butter and sugar, huuuuuuuuuuuum 

Ingredients:   


**ghrif, or Corsa, or then a thousand holes (crepe arabic)**

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/baghrir-001.jpg)

portions:  8  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * ½ kg of normal semolina 
  * 2 cup instant yeast 
  * a pinch of salt 
  * 1 to 2 eggs well beaten 
  * oil 
  * warm water 
  * 1 glass of warm milk 



**Realization steps**

  1. in a bowl, mix the semolina salt, with a little oil, and this to remove the elasticity of the semolina. 
  2. add instant yeast, and water and avoid forming a handy dough, try adding water in a way to have a liquid dough, add milk, mix well with one hand, and let stand.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/20664779.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2008/01/20664779.jpg>)
  3. leave the dough side, and from time to time mix with a ladle.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/20664923.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2008/01/20664923.jpg>)
  4. after raising the dough, beat the eggs and add them, stir again. and let it rest. 
  5. when the surface of the dough becomes full of bubbles which shows that it has risen well, start cooking. on a soft fire, in a clay tadjine, or a crepe stove, pour the equivalant of a ladle trying to form a beautiful circle.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/20665228.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2008/01/20665228.jpg>)
  6. to serve, drizzle the ghrif with melted butter, and sprinkle with sscre crystallises or honey. 



![baghrir-010.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/baghrir-010.jpg)

merci pour vos visites 
