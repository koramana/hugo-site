---
title: kitchen love recipes tested and approved 70
date: '2015-05-23'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-58.html/dyouls-de-kari-ben>)

Hello everybody, 

I come back to this article dedicated to my readers. An article where I give the floor to the culinary photography of the faithful to my blog. But not that, my readers share through these pretty little photos made by love, their joys to see a very successful dish, or a very delicious cake after trying a recipe from my blog. 

Thank you, my friends for your fidelity, thank you to those who invited me to come to their home, to meet a group of my readers. I was invited to Paris by Soumia, Houyam, Hind, Ratiba, Nathalie .... And the list is long. In Lyon by Kahina, Hafidha, Khoula, Hayette, Samiha .... In Toulouse by Nadia, Wided, Warda, Kelthome, Oum Foued .... Not to mention all my friends on both sides of Algeria. Mias damage, impossible for me and at this moment to move here or because of my baby, as it is impossible for me too to travel with him ... But in any case Your invitations make me very warm in the heart ... and it's as if I came ... 

Let's go back to your recipes from my blog, do not forget to send me the pictures on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

[ ![fruit salad with jelly](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/salade-de-fruit-a-la-gelee.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/salade-de-fruit-a-la-gelee>)

[ Fruit salad in jelly ](<https://www.amourdecuisine.fr/article-salade-de-fruits-en-gelee-2.html> "fruit salad in jelly") at Malika Bouaskeur 

[ ![lemon tart](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tarte-au-citron-mariem.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/tarte-au-citron-mariem>)

[ Lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee.html> "lemon meringue pie") from Mariem Ma 

[ ![eggplant papetons](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/papetons-daubergine.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/papetons-daubergine>)

[ Eggplant pie ](<https://www.amourdecuisine.fr/article-papetons-d-aubergine.html> "Eggplant pie") at Rachida Rach 

[ ![strawberry pie my daughter](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tarte-au-fraises-ma-fille.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/tarte-au-fraises-ma-fille>)

[ Strawberry tart ](<https://www.amourdecuisine.fr/article-tarte-aux-fraises-et-a-la-creme-patissiere.html> "Strawberry and custard pie") at my daughter's house my life. 

[ ![chicken nuggets](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/croquettes-de-poulet.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/croquettes-de-poulet-2>)

[ Chicken nuggets ](<https://www.amourdecuisine.fr/article-croquettes-de-poulet.html> "Chicken nuggets") at Selma Maram 

[ ![pan pizza](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/pizza-a-la-poele.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/pizza-a-la-poele>)

[ pan pizza ](<https://www.amourdecuisine.fr/article-37300577.html> "pizza at the stove, or pizza express") at Cré puscule 

[ ![rolled with lemon](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/roul%C3%A9-au-citron.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/roule-au-citron-2>)

[ rolled with lemon ](<https://www.amourdecuisine.fr/article-biscuit-roule-au-citron.html> "biscuit rolled with lemon") at my daughter's house my life 

[ ![Cheesecake without cooking](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/cheesecake-sans-cuisson1.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/cheesecake-sans-cuisson-2>)

[ Cheesecake without cooking ](<https://www.amourdecuisine.fr/article-cheesecake-sans-cuisson-au-chocolat.html> "cheesecake without chocolate baking") at Oum Ali 

[ ![cake without express cooking](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateau-sans-cuisson-express.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/gateau-sans-cuisson-express-2>)

[ cakes without express cooking ](<https://www.amourdecuisine.fr/article-gateau-sans-cuisson-express.html> "Cake without cooking, express") at Malika Bouaskeur 

[ ![eggplant maklouba](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/maklouba-daubergines.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/maklouba-daubergines>)

[ aubergine maklouba ](<https://www.amourdecuisine.fr/article-maklouba-aux-aubergines-et-poulet.html> "Eggplant and chicken maklouba") (but with zucchini) at Arwa Naili 

[ ![mushroom tajine with chicken roll](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-champignon-au-roul%C3%A9-de-poulet.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-70.html/tajine-de-champignon-au-roule-de-poulet>)

[ mushroom tajine with rolled chicken with minced meat ](<https://www.amourdecuisine.fr/article-tajine-de-champignon-roule-de-poulet-a-la-viande.html> "mushroom tajine-rolled chicken with meat") chez Ml Wassila 
