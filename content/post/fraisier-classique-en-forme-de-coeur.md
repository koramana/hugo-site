---
title: classic strawberry heart shaped
date: '2017-04-10'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles and bars
- sweet recipes
tags:
- Dessert
- strawberries
- Pastry
- desserts
- Birthday cake
- Genoese
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-fraisier-classique-en-forme-de-coeur.jpg
---
[ ![classic strawberry recipe in heart shape](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-fraisier-classique-en-forme-de-coeur.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-fraisier-classique-en-forme-de-coeur.jpg>)

##  classic strawberry heart shaped 

Hello everybody, 

**classic strawberry heart shaped**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-fraisier-classique-en-forme-de-coeur-1.jpg)

portions:  10  Prep time:  60 mins  cooking:  30 mins  total:  1 hour 30 mins 

**Ingredients** For the Genoise 

  * 1 vanilla pod 
  * 120 g of sugar 
  * 100g of flour 
  * 50 g of butter 
  * 4 eggs 

For syrup and garnish 
  * 250 à 500 g de fraises selon votre envie de fraises 🙂 
  * 150 g of sugar 
  * 20 cl of water 
  * Vanilla 

For the cream 
  * 20 cl whole cream 
  * 25 cl of semi skimmed milk 
  * 2 egg yolks 
  * 40 g of maizéna 
  * 50 g of sugar 
  * 1 g of agar agar or 2 sheets of gelatin 
  * 1 vanilla pod or 1 packet of vanilla sugar 



**Realization steps** For the syrup: 

  1. heat the water, sugar and split vanilla bean. 
  2. Bring to the boil 2 to 3 minutes and let infuse. 

For the muslin cream: 
  1. heat the milk with split vanilla bean, vanilla extract or vanilla sugar. 
  2. Whip the egg yolks with the sugar, add the maizéna gradually. 
  3. Add the agar agar in the milk and let it boil for 1 minute 
  4. Pour the boiling milk on at once, mix and put back on medium heat, whip constantly, the custard will thicken 
  5. return the cream to a bowl and cover with food film and let cool. 
  6. whip the liquid cream well, add the whipped cream gently, and put in a piping bag in the fridge. 

Prepare the sponge cake: 
  1. Separate the egg whites from the yolks and beat the firm whites 
  2. Add one-third of the sugar to the whites when they are firm, and beat another 1 minute. 
  3. Whip the yolks with the remaining sugar and vanilla 
  4. Add the melted butter, then half of the sifted flour 
  5. To relax the dough, add 2 tablespoons of egg white, then add the remaining flour. 
  6. Finally, add the egg whites delicately by lifting the dough so as not to break the air bubbles! 
  7. Pour the dough into a fairly large plate lined with baking paper. 
  8. Bake in a hot oven for 10 minutes at 190 ° C. 

strawberry mounting: 
  1. Here I used a heart shaped pastry circle, cut out two hearts in the sponge cake. 
  2. Arrange the first sponge cake in the circle, soak well with syrup 
  3. Cut the strawberries in half in the direction of the height and arrange the ½ strawberries face cut out, 
  4. Fill the bottom of the cake with the prepared diplomat cream, start filling the space in front of the strawberries, so that it does not move. 
  5. If you have strawberries, you can put some pieces in the cream   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fraisier-classique-en-forme-de-coeur.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/fraisier-classique-en-forme-de-coeur.jpg>)
  6. Cover with the second heart of sponge cake, soak this layer with syrup. 
  7. decorate with spread almond paste or roll, 
  8. Place in the fridge between 2 hours and one night before unmolding and serving 



[ ![classic strawberry recipe in heart shape 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-fraisier-classique-en-forme-de-coeur-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-fraisier-classique-en-forme-de-coeur-1.jpg>)
