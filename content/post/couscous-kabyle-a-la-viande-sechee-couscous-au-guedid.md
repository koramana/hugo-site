---
title: 'Kabyle couscous with dried meat: couscous with guedid'
date: '2014-08-24'
categories:
- couscous
- Algerian cuisine
- Dishes and salty recipes
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/couscous-kabyle-a-la-viande-sechee1.jpg
---
![couscous-Kabyle-to-the-meat-sechee.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/couscous-kabyle-a-la-viande-sechee1.jpg)

##  Kabyle couscous with dried meat: couscous with guedid 

Hello everybody, 

Here is a Kabyle couscous with dried meat: couscous guedid that comes directly from Algeria, it's the couscous of my mother, she realized yesterday, and sent me the photos, and as it is a couscous that I like a lot. 

I like to share with you the recipe for this couscous Kabyle with dried meat, or as it is called "guedid" or "queddid". I say to my dear mother: thank you so much for these sublime photos, and I will not leave the comment for this recipe until I go to Algeria incha allah, and take my share of this couscous with the dried meat, which I have not dripped for more than 7 years, because quite simply, I have never prepared dried meat ...  ![Kabyle couscous with cornille](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/couscous-kabyle-a-la-cornille.jpg)

![Couscous au-Kabyle quedid.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/couscous-kabyle-au-quedid.jpg)

pour d’autres recettes de couscous: 
