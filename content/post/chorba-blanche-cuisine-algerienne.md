---
title: white chorba, Algerian cuisine
date: '2012-07-19'
categories:
- cuisine diverse
- Dishes and salty recipes
- recipe for red meat (halal)
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/chorba-bida-2_thumb1.jpg
---
[ ![chorba bida 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/chorba-bida-2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35553378.html>)

##  white chorba, Algerian cuisine 

Hello everybody, 

Here is another [ chorba ](<https://www.amourdecuisine.fr/article-25345575.html>) that I like well prepared when it is cold, it is a chorba of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , made of **vermicelli** , very delicious, especially with the touch of lemon juice that is added when serving. 

the recipe in Arabic: 

**white chorba, Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/chorba-bida-2_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * pieces of chickens 
  * handful of chickpeas dipped the day before 
  * 1 onion 
  * 1 tablespoon butter or smen 
  * 2 tablespoons of oil 
  * 1 pinch of black pepper 
  * 1 pinch of cinnamon 
  * 1 glass of vermicelli 
  * 1 egg yolk 
  * 1 bunch of parsley 
  * 1 lemon juice 
  * salt 



**Realization steps**

  1. clean the pieces of chicken, put them in a pot, fry on low heat (without browning) with grated onion, oil, butter, pepper, cinnamon and salt. 
  2. add the amount of water needed, boil dip the chickpeas, cover and let cook. 
  3. pour the vermicelli into the stock, let it cook, stirring occasionally. 
  4. Take a ladle of sauce, pour it into a deep plate, Spread the egg yolk in this sauce, add the parsley, and the lemon juice, mix well. 
  5. slowly pour everything into the pot while stirring. 
  6. Remove immediately from the fire and serve. 



[ ![chorba bida](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/chorba-bida_thumb_31.jpg) ](<https://www.amourdecuisine.fr/article-35553378.html>)

Enjoy your meal 

et pour plus de chorbas, cliquez sur les liens en bas: 
