---
title: my achievements at home 32
date: '2013-01-13'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/IMGP02711.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

you are from days to days more to visit my blog, to leave me comments even if it's just to say thank you .... hihihihi (sorry if I do not validate them right away, because I'm taken, but I answer as soon as possible). 

and you are more and more people trying my recipes, and it makes me very happy. 

please continue to send me your test photos on my email: [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)

**just a clarification** : 

this email, [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>) is not a contact email, if you have questions to ask me about a recipe, do it in comments, because this email, I only consult when I have time to write the article: my achievements at you, and sometimes I find urgent questions that are more than 1 month old .... 

sorry, I have the comments to answer your questions, and I have this email for the achievements, and I have another email to contact the author, but it's mostly for commercials, hihihihih 

I can not receive everything in the same email, it will be overflowing. 

so, and I'll say it again, if you have an urgent question about a recipe, make it a comment, it will allow me to respond faster, as it allows other visitors to enjoy the answer, and not to rest the question again and again. 

thank you. 

otherwise I share with you today the pictures of your achievements: 

soups, breads, cakes, cookies, desserts, salads, appetizers, brioches ... 

I'm so glad you found my blog is rich in recipes, thanks again. 

so for photos of your essays send me on this email: [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)

![IMGP0271.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/IMGP02711.jpg)

![DSC_0372](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/825659091.jpg)

###  [ dry cakes with chocolate and praline ](<https://www.amourdecuisine.fr/article-gateau-sec-petits-gateaux-chocolat-praline-113476687.html>) in [ Crocraquant ](<http://crocraquant.canalblog.com>)

the mona at S. Serine (a faithful reader) 

![avocado tuna clementines \(1\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/avocat-thon-clementines-1-1.jpg)

[ Milk soup ](<https://www.amourdecuisine.fr/article-soupe-au-lait-vermicelles-au-lait-113495672.html>) at Zina (a faithful reader) 

[ olive bread ](<https://www.amourdecuisine.fr/article-pain-aux-olives-107156686.html>) at Zina (a reader) 

[ makrout rolled ](<https://www.amourdecuisine.fr/article-makrout-roule-102845781.html>) fried at Zina's 

[ Minced meat rolls ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee-96050112.html>) at Zina (a faithful reader). 

[ donuts with raisins ](<https://www.amourdecuisine.fr/article-beignets-aux-raisins-secs-sfenj-algerien-110528237.html>) in [ Lylliratatouille ](<http://lylyratatouille.over-blog.com/article-beignets-aux-raisins-secs-113871243.html>)

[ Karentika, or guaranteed ](<https://www.amourdecuisine.fr/article-garantita-ou-karantika-94535875.html>) at Ingrid (a reader) 

[ tagine of kefta with eggs ](<https://www.amourdecuisine.fr/article-tajine-de-kefta-aux-oeufs-112750436.html>) at Nabu N (a faithful reader) 

[ Chicken nuggets ](<https://www.amourdecuisine.fr/article-croquettes-de-poulet-107257102.html>) at Souhila Z (a faithful reader) 

[ chocolate slices ](<https://www.amourdecuisine.fr/article-gateaux-secs-rondelles-au-chocolat-106682511.html>) chez bachiera B. 
