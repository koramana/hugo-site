---
title: Crunchy peanuts
date: '2012-04-12'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/224968061.jpg
---
hello everyone, here are delicious dry cakes, crisp and fondant wish. this recipe is similar to the one I saw in bigmumy so I had no almonds, I made it with a little peanuts, and I swear, a pure delight. and if like me you like Algerian cakes, I will prepare a long list of ingredients: 3 eggs 155 gr of sugar 180 ml of oil 1 sachet of baking powder 1 sachet of vanilla sugar Salt 1 glass of ground peanuts (roughly) Lemon rind of flour according to & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.65  (  1  ratings)  0 

_![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/224968061.jpg) _

_ Hello everyone,  _

_ Ingredients:  _

  * _ 3 eggs  _
  * _ 155 gr of sugar  _
  * _ 180 ml of oil  _
  * _ 1 sachet of baking powder  _
  * _ 1 sachet of vanilla sugar  _
  * _ Salt  _
  * _ 1 glass of ground peanuts (roughly)  _
  * _ Lemon zest  _
  * _ flour according to its absorbency  _
  * _ 1 egg yolk for gilding mixed with soluble coffee  _



With a beater, mix the sugar and the eggs. Add the oil, salt and vanilla. Continue beating and add the ground almonds (I brown them in the oven before you smell them well), the lemon peel and the baking powder. At this stage, I mix the dough by hand by gradually incorporating the flour until obtaining a soft and malleable dough. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/224967841.jpg)

On a work surface sprinkled with flour, lower the dough to a thickness of 1 cm. Cut the patties with a serrated punch (for my part I also made flowers and half moons). Arrange the pancakes in a dish covered with parchment paper. Brush the surface of the patties with the beaten egg yolk. With the back of a fork draw lines in one direction then in another so that they cross each other. 

Preheat the oven at 175 ° C for about 30 mm until the surface is golden brown. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/224967891.jpg) ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/224967971.jpg)

un délice bien croquant. 
