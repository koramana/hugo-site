---
title: mincemeat pie, English tartlets with apples and grapes
date: '2018-02-03'
categories:
- dessert, crumbles and bars
- sweet recipes
- pies and tarts
tags:
- Pastry
- desserts
- Cakes
- Apple pie
- pies
- Shortbread
- pies

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/02/mincemeat-pie-tartelettes-anglaises-aux-pommes-et-raisins-2.jpg
---
![mincemeat pie, English tartlets with apples and grapes 2](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/mincemeat-pie-tartelettes-anglaises-aux-pommes-et-raisins-2.jpg)

##  mincemeat pie, English tartlets with apples and grapes 

Hello everybody, 

You know mincemeat pies? it's pies or for well explained the name it's mini tartlets covered and stuffed with a mixture called mincemeat, which is a mixture of dried fruit varieties: raisins, cranberries, prunes, apples and greasy. Originally the fat used in this recipe was of animal origin, there was even meat in it, that's why we got this appointment: mincemeat. 

With time the recipe has changed a lot, and the meat has disappeared in the composition of the stuffing. Other fruits have been added to the stuffing, and mincemeat pie are nowadays the favorite cake of the English, especially during the Christmas holidays. 

So today with these tartlets with apples and raisins, I participate in our round "recipes around an ingredient 36 # raisins, which I was godmother, hihihih 

{{< youtube 7upumQxdGSs >}} 

I made you a nice video of the mincemeat pie recipe, so visit my youtube channel and thank you for your encouragement, and before going to the recipe voila, the godmothers and ingredients of all our previous rounds: 

**mincemeat pie, English tartlets with apples and grapes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/mincemeat-pie-tartelettes-anglaises-aux-pommes-et-raisins-3.jpg)

portions:  12/15 

**Ingredients** for the farce 

  * 4 tablespoons frozen butter and rappé 
  * 240 ml of lemon juice 
  * 2 big apples, peeled, stuffed seeded and cut into small pieces, I prefer acidic apples 
  * 250 grs of raisins, cramberies, dried apricots. 
  * 120 grs of candied citrus zest 
  * 80 gr of brown sugar 
  * 2 tbsp. honey 
  * ½ c. ground cinnamon 
  * ½ c. ground nutmeg 
  * ¼ c. ground ginger 
  * ¼ c. ground cloves 

for the shortbread paste: 
  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 
  * (cold water if needed, between 1 teaspoon and 2 ... no more) 



**Realization steps** We begin to prepare the stuffing, knowing that the stuffing must be prepared at least 3 days before. 

  1. freeze the butter until it becomes solid, then stir it. 
  2. mix all the ingredients of the stuffing and place them in jars and let macerate well for at least 3 days. 

prepare the shortbread 
  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and the almond powder and mix well 
  4. remove the dough from the bowl, and pick it up in a ball (it is better to divide the dough on two) 
  5. Let it rest. Spread the dough with the baking roll on a floured space. 
  6. Cut circles of almost 9 cm or depending on the size of the cupcake molds you want to darken. 
  7. push the dough and adjust it to the surface of the cavities. 
  8. fill them with mincemeat stuffing, 
  9. cut shapes like stars, hearts or firs of almost 5 cm, and cover the stuffing with. 
  10. brush the top with a mixture of egg yolk and milk. 
  11. bake in a preheated oven at 180 ° C between 12 and 15 minutes. 
  12. remove the apple and raisin tartlets from the oven when they have a nice color 
  13. let cool before unmolding, because the caramel cooking may be hot. 



![mincemeat pie, English tartlets with apples and raisins 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/mincemeat-pie-tartelettes-anglaises-aux-pommes-et-raisins-secs-1.jpg)

Et voila la liste des participantes: 
