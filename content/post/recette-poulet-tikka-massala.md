---
title: recipe Chicken tikka massala
date: '2016-03-05'
categories:
- Indian cuisine
- Cuisine by country
- Dishes and salty recipes
- chicken meat recipes (halal)
- rice
tags:
- Healthy cuisine
- Full Dishes
- Easy cooking
- Fast Food
- India
- Curry
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/poulet-tikka-massala-2.jpg
---
![recipe Chicken tikka massala](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/poulet-tikka-massala-2.jpg)

##  recipe Chicken tikka massala 

Hello everybody, 

Here is a recipe that I love at an unimaginable point, and I would not think that one day I will realize the recipe for chicken tika masala, simply because every time we go out with the family to the restaurant, and that 'we choose to eat Indian, I ask chicken tika masala ... But what is good this dish. 

Immediately, I asked my husband to buy the ingredients that I missed and I started to concoct this dish so tasty and so good. I do not regret it at all because we feasted. 

You can see another version of this recipe, some [ chicken skewers tikka massala ](<https://www.amourdecuisine.fr/article-brochettes-de-poulet-tikka-massala.html>)   


**Chicken Tikka Massala**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/poulet-tikka-massala-1.jpg)

portions:  6  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 1 chicken cut into pieces 
  * 200 ml Greek yoghurt 
  * 1 tablespoon of tomato paste 
  * 2 or 3 fresh tomatoes otherwise a box of crushed tomatoes 
  * 1 onion 
  * 1 clove of garlic 
  * 2 tablespoons flaked almonds 
  * olive oil 
  * fresh coriander 
  * 1 tablespoon of green curry 
  * 1 tablespoon of cumin powder 
  * 2 tablespoons powdered sugar 
  * salt 



**Realization steps**

  1. In a salad bowl, mix 100 ml Greek yogurt with half the spices, tomato paste and 1 tsp sugar. 
  2. Coat chicken pieces with this mixture and let macerate 
  3. In a saucepan over medium heat, brown the peeled onion and slice it in small dice with a drizzle of olive oil and the remaining sugar for 10 minutes. 
  4. Add the chopped garlic clove, chicken and marinade. 
  5. Hold on medium heat for 20 minutes, turning chicken pieces regularly. 
  6. Stir in almonds, crushed tomatoes, remaining spices, salt and a small glass of water if necessary. Cover and let it simmer again for 20 minutes. 
  7. At the end of this time, add the second yoghurt to bind the sauce, a good amount of coriander and adjust the salt seasoning if necessary. 
  8. Leave on low heat for 5 to 10 minutes. 
  9. Serve chicken Tikka massala at the end of cooking with saffron pilaf rice for example 



[ ![recipe Chicken tikka massala](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/poulet-tikka-massala-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/poulet-tikka-massala-3.jpg>)
