---
title: Strawberry Cheesecake Smoothie
date: '2016-01-27'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-cheesecake-aux-fraises-002.jpg
---
[ ![Strawberry Cheesecake Smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-cheesecake-aux-fraises-002.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-cheesecake-aux-fraises-002.jpg>)

##  Strawberry Cheesecake Smoothie 

Hello everybody, 

Strawberry cheesecake smoothie, another touch to change the smoothies, a smoothie with the flavors of a fruity cheesecake. I enjoy the frozen strawberries I bought during Christmas at a price of dream, besides I filled a whole drawer of the freezer with nothing but frozen fruit, and I'm not the only one to feast with. 

Today, a box of cheese or the rest of a box, with which I had to make a cheesecake, and which arrives at its expiry date was found to swim in the bowl of my blinder with the rest of the ingredients and I tell you the truth is too good. Just for my ingredients, everything was 0% fat, for a light smoothie. 

**Strawberry Cheesecake Smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-cheesecake-aux-fraises-014c.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 100 gr of creamy cheese like Phéladelphia 
  * 200 gr natural yoghurt 
  * 2 tablespoons caster sugar 
  * 2 biscuits speculoos 
  * 240 gr frozen strawberries 

Decoration: 
  * whipped cream 
  * powdered speculoos 



**Realization steps**

  1. place all the ingredients in the bowl of a blender, and mix to obtain a homogeneous and creamy mixture. 
  2. Serve immediately, garnished with a touch of whipped cream and powdered speculoos 



[ ![Strawberry Cheesecake Smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-cheesecake-aux-fraises-016b.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-cheesecake-aux-fraises-016b.jpg>)
