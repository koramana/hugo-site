---
title: hazelnut triangle skandraniette
date: '2012-01-08'
categories:
- Gateaux au chocolat
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/skandraniette-gateau-algerien-2012.jpg
---
##  **hazelnut triangle skandraniette**

Hello everybody, 

I lingered to post you the recipes of [ Algerian cakes ](<https://www.amourdecuisine.fr/article-bonne-fete-d-aid-kbir-et-mes-gateaux-algeriens-88105516.html>) that I did for aid el kbir, but you know that I was in full review for my exam, and also I prepared to go to Algeria, today, I resume my photo file of modern cakes at honey, and I hope you will like this very pretty sweetie 

the principle of this Algerian cake and the same as that of [ skandraniette ](<https://www.amourdecuisine.fr/article-skandraniette-gateaux-algeriens-beau-et-bon-95854677.html>) already on the blog, but this time I cut triangles instead of lozenges, and the decoration will be a matter of hand play, and imagination, so you can develop an even more beautiful decoration.   


**hazelnut triangle skandraniette**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/skandraniette-gateau-algerien-2012.jpg)

**Ingredients** so we go to the dough ingredients: 

  * 250 gr of flour 
  * 75 gr of margarine 
  * 1 pinch of salt 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of white dye 
  * 1 teaspoon of dye (color of your choice,) 
  * Orange blossom water + water 

the joke: 
  * 2 measures of hazelnuts powder (I use a bowl of 200 ml) 
  * 1 measure of almond powder. 
  * 1 measure of crystallized sugar 
  * 1 cup of vanilla coffee 
  * egg as needed 

decoration: 
  * honey 
  * silver beads 
  * egg white 
  * shiny food 



**Realization steps** of the dough: 

  1. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  2. Mix well with your fingertips 
  3. take a little part of this mixture 
  4. in a little flower water mix, dye them to have the color of your choice 
  5. start picking up the dough with this water, 
  6. add water if you need, and knead a few seconds. 
  7. cover your dough and let it rest on the side. 
  8. Now take the rest of the mixture and collect it in a ball with a little water of flower and water. 
  9. knead a little 
  10. turn into a ball and let it rest. 

stuffing: 
  1. mix the almond, the hazelnuts and the sugar, 
  2. add vanilla 
  3. stir an egg, and add it gently to almonds and hazelnuts. 
  4. pick up the mixture, if necessary add in small quantity, another egg (no need to add everything) 

method of preparation: 
  1. take the dough, spread it very thinly using a baking roll (like me) or the dough machine 
  2. shape with the stuffing of almonds and hazelnuts a sausage 3 cm wide, 2 cm high, and its languor, according to the length of the dough you have spread 
  3. roll the dough around the pudding, a turn and a half, and stick with the egg white 
  4. with a knife cut triangles of the same size. 

for leaves 
  1. spread out both pasta 
  2. cut sheets with a leaf pad (according to your taste but you have to use different size stamps to have a small leaf of one color, and a little bigger of another color 
  3. put a little egg white on the big leaf. 
  4. stick the little leaf on it, and pinch the wide end, 
  5. stick this set of leaves on the large extrimite of the cake with a little egg white 
  6. redo it to have another sheet.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/gateau-algerien-2012_thumb1.jpg)

for the flowers: 
  1. according to the desired colors, spread out the dough finely 
  2. using a 4.5 cm diameter round dies (or 5) cut your circles 
  3. take each circle, fold it in half, then in four 
  4. give a little volume to your flower with your fingers 
  5. in the side of the cake make a hole, with the tip of a brush 
  6. insert the center of the flower in 
  7. you can put two to three flowers according to your choice. 
  8. let the cakes rest for a whole night 
  9. the next day bake in a medium-temperature oven 
  10. after cooking let cool, and dip the cakes in hot honey 
  11. decorate with pearls and shiny food 
  12. present in boxes, and good tasting   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/skandraniette-au-noisette-gateaux-algeriens-modernes-2012_t1.jpg)



you can for a start see this video: 

thank you for your visits 

bonne journée 
