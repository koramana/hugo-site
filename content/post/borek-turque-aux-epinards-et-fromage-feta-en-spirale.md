---
title: turkish borek with spinach and feta spiral cheese
date: '2017-06-09'
categories:
- Bourek, brick, samoussa, slippers
- diverse cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
- ramadan recipe
tags:
- Ramadan 2016
- Ramadan 2017
- Algeria
- Ramadan
- Morocco
- inputs
- Hot Entry

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/06/borek-turque-aux-%C3%A9pinards-et-f%C3%A9ta-1-647x1024.jpg
---
![Turkish boronk with spinach and feta 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/borek-turque-aux-%C3%A9pinards-et-f%C3%A9ta-1-647x1024.jpg)

##  turkish borek with spinach and feta spiral cheese 

Hello everybody, 

After the homemade filo dough, here is the Turkish borek spinach and feta cheese spiral, a delight to try absolutely. 

a Turkish recipe super easy to make, if you use filo dough trade. Personally I made them with the [ homemade filo pate ](<https://www.amourdecuisine.fr/article-pate-filo-maison-pate-phyllo.html>) freshly prepared. The result top top top. I gave the spiral shape to my b0rek, but you can still do it by placing 5 to 6 layers of filo dough separated by the cream eggs + yourt, put the stuffing, and repeat again 5 to 6 layers of filo paste separated by the same cream. 

**turkish borek with spinach and feta spiral cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/borek-turque-aux-%C3%A9pinards-et-f%C3%A9ta-2.jpg)

**Ingredients** according to the size of your mold: 

  * 9 to 12 leaves of filo 
  * 400 gr of spinach 
  * 200 gr of feta (or less depending on your taste) 
  * 1 chopped onion 
  * salt, black pepper, Espelette pepper powder 
  * olive oil 

For the cream: 
  * 2 eggs 
  * 2 tablespoons natural yoghurt 
  * 2 tbsp. oil soup   

  * if necessary add the same quantity, if not half) 



**Realization steps**

  1. start by making the spinach stuffing, fry the onions in the oil 
  2. add the chopped spinach, and the spices and salt. 
  3. let it cook 
  4. on cooling, add the feta cheese in crumble. 
  5. now take 2 sheets of filo, together, brush the last sheet with the cream, then add over a 3 rd sheet of filo. 
  6. place the stuffing on one end, and roll to enclose the stuffing, 
  7. form a spiral and place it in your mold. 
  8. continue until you fill the mold. 
  9. pour the remaining cream on your hairspring, it must be well covered and the cream must penetrate between the strands. 
  10. garnish with seeds of nigella, and bake at 180 ° C between 30 and 40 minutes (if necessary lower the temperature for a good cooking inside. 



![Turkish boronk with spinach and feta cheese](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/borek-turque-aux-%C3%A9pinards-et-f%C3%A9ta.jpg)
