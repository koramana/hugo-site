---
title: Red fruit jam
date: '2010-10-14'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

---
Hello everybody, 

today I am posting a jam recipe that I have prepared this week, because when I returned from Algeria, I found in my freezer two boxes of frozen fruit that I had bought and forgotten. 

having no idea, what to do with, I told myself a good jam, will liquidate me this stock. 

here are the two boxes in question. 

so I pass you the recipe: 

here is the recipe: 

\- 800 grams of fruit 

\- 450 grs of jam special sugar 

\- 1 cinnamon sticks 

\- 1 star anise seed 

me here I use special sugar jam, but if you do not have this sugar, you can add lemon juice, or so an apple 

so place your ingredients in a thick bottom pot, or in a special jam bowl, leave overnight. 

the next day, leave leather on a gentle fire 

try the drop on a plate, if the drop freezes so the jam is ready. 

I split the jam in half, I left mine in pieces, because I like jam containing pieces, and I ground the other part because my husband loves it as a spread . 

place immediately in sterilized jars, and turn the jars upside down overnight. 

enjoy after, with a delicious tea. 

today is the funeral of Micky, that god has his soul, and that she rests in peace, she was a woman who loved sweetness very much. so in tribute to her 

kisses. 
