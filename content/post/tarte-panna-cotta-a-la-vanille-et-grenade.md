---
title: panna cotta pie with vanilla and pomegranate
date: '2016-02-03'
categories:
- sweet recipes
- rice
- pies and tarts
tags:
- Based
- Pastry
- desserts
- To taste
- Algerian cakes
- Shortbread
- Shortbread

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tarte-panna-cotta-vanille-et-grenade-4.jpg
---
[ ![vanilla and pomegranate panna cotta pie 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tarte-panna-cotta-vanille-et-grenade-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tarte-panna-cotta-vanille-et-grenade-4.jpg>)

##  panna cotta pie with vanilla and pomegranate 

Hello everybody, 

I admit that the theme was both easy and difficult at the same time. Easy because vanilla can be easily integrated into any recipe. difficult because we want to release traditional recipes to introduce readers and participants a new achievement. 

I wanted to make a salty vanilla flavored recipe, but I was very afraid of my husband's reaction, so I totally changed. I thought a lot about a recipe that was out of the ordinary, and it was not easy. I chose in the end to make the favorite dessert of my children, the panna cotta ... But I preferred to make pie-shaped this ... The D-day, I started by making the dough, in my head, all was going to be a breeze, in less than an hour my pie was erected and ready to make the supermodel in front of the camera ... But here, I do not know what I could do as a mistake, but each time when cooking the edges of my pie bottom fell, I realized the dough 3 times ... I was really dropped, but phew, the 3 rd try is passed as a breeze ???? I confess I did not understand anything. But in any case everything, after all these failures I found myself preparing my pie at 15:00 and take photos at 16:30 under the fear of cutting, my panna cotta flow, because I did not give him the to take, but fortunately, everything went well, except that I had a problem with the light of day that began to disappear at this time, hihihihih. 

**panna cotta pie with vanilla and pomegranate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tarte-panna-cotta-vanille-et-grenade-2.jpg)

portions:  8  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** the shortbread dough: 

  * 250 gr of flour 
  * 125 gr of butter 
  * 1 tablespoon icing sugar 
  * 1 egg yolk 
  * 2 or 3 tablespoons liquid cream 

For the panna cotta garnish: 
  * 300 ml of whipping cream 
  * 160 ml of milk 
  * 50 g of sugar 
  * 1 gr of agar-agar (or 1 gelatin sheet) 
  * 1 vanilla pod. 

pomegranate syrup: 
  * 400 ml pomegranate syrup (trade for me) 
  * 4 tablespoons water 
  * ½ teaspoon agar agar 
  * fresh pomegranate seeds. 



**Realization steps** Start by preparing the pie dough. 

  1. In a bowl, pour the flour. Add the icing sugar and mix. 
  2. Add the soft butter (almost melted), mix, 
  3. add the egg yolk and mix by sanding the dough with your fingers. 
  4. Finish by adding the cream and mix the dough so as to obtain a beautiful smooth and homogeneous ball. Shoot and book cool. 
  5. Take the dough out of the fridge. Spread it on a baking sheet, flouring lightly. The darker in the mold with the sulfide, which will facilitate the demolding. Pre-cook for a few minutes at 180 ° C. (I had prepared and cooked my dough at night) 
  6. Once the bottom is precooked, remove it. and let cool on a rack. 

for panna cotta: 
  1. In a saucepan, mix the milk, cream, sugar and vanilla bean melted in half. 
  2. Heat for 2 minutes, remove the vanilla pod, scratch the inside with the tip of a knife, and return the grated seeds to the milk. 
  3. add the agar-agar mixed with a little powdered sugar. Boil 1 min. 
  4. pour the panna cotta on the well-cooled bottom of the pie and let it rest. 

pomegranate syrup gels: 
  1. I did not want to have a layer of jelly on my panna cotta pie, but rather a thick syrup, which flows like chocolate, so I lightened the pomegranate syrup a little with a little water. 
  2. leave on low heat for two minutes just to have a small broth, and add the agar agar, leave 1 minute. 
  3. pour a little bit in the middle of your pie and garnish with some fresh pomegranate seeds. 



[ ![vanilla and pomegranate panna cotta pie 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tarte-panna-cotta-vanille-et-grenade-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tarte-panna-cotta-vanille-et-grenade-3.jpg>)

and here is the list of participants with the titles of their recipes: 

__ 11- _Soulef of[ Love of cooking ](<https://www.amourdecuisine.fr/>) , With its vanilla and pomegranate panna cotta tart _

__ 14- _Selma (non blogueuse )_
