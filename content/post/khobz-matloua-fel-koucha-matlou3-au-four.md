---
title: khobz matloua fel koucha-matlou3 in the oven
date: '2015-11-20'
categories:
- Algerian cuisine
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-el-koucha.CR2_.jpg
---
[ ![oven matlou3, matlou3 el koucha](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-el-koucha.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-el-koucha.CR2_.jpg>)

##  khobz matloua fel koucha: baked matlou3 

Hello everybody, 

I really like matloua bread, or [ Matlou ](<https://www.amourdecuisine.fr/article-matloue-matlou3-matlou-pain-maison-au-tadjine.html>) 3 as we like to write sometimes, or even matlou .... because it is an Arabic name, which has no synonym in French, apart from the fact that it can be called homemade bread ... 

This time I left for matloua baked bread, yum yum ... and do not bother to stay for hours (finally not exaggerating, lol) ... Most importantly, as the cake swells, and takes the color of at the top, do not turn so that it takes the color from below, if you go to wait for the slab from below, you will miss your bread and the crust may be very hard. 

An important point too, do not hesitate to spread the patties as finely as possible, the more they are fine, and more, they will swell in the oven, and you will have a bread super airy. Thanks to Kiko Benakano for all the advice she gave me to succeed my matlou3 el bed. 

[ ![matlou3 el koucha 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-el-koucha-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-el-koucha-1.CR2_.jpg>)

so without delay, I pass you my secret recipe, lol ... and I hope you will succeed. 

**khobz matloua fel koucha-matlou3 in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-el-koucha-matlou3-au-four.jpg)

portions:  6  Prep time:  60 mins  cooking:  10 mins  total:  1 hour 10 mins 

**Ingredients**

  * 500 gr of semolina 
  * 250 gr of flour 
  * 3 tablespoons milk powder 
  * 1 tablespoon of sugar 
  * 1 cup of coffee and ½ of salt 
  * 1 tablespoon instant yeast 
  * 1 cup of baking powder 
  * 3 tablespoons of oil 
  * 1 tablespoon and a half of white vinegar 
  * up to 450 ml of warm water 



**Realization steps**

  1. Put all the ingredients in the robot bowl and a small amount of water and start kneading for 15 min to 20 until you have a soft and elastic dough. 
  2. add the water gradually while pterissant, the paste will become gradually sticky.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-1.jpg>)
  3. add the oil and still oil, to incorporate it well into the dough. (especially if you knead a hand, so that the dough will not stick to your hands) 
  4. Cover the dough with a cloth and let rise for about 1 hour, in a draft-free place, until the dough doubles in volume. 
  5. Take again the dough and degas the right one separating it from the walls of the bowl, your hands must be well oiled. 
  6. form 5 to 6 balls. and place them on a clean cloth generously covered with a mixture of flour and semolina. 
  7. Let stand for almost 10 minutes, so that the flour is relaxing and allows you to make your patties. 
  8. form slabs as thin as possible, and do not forget to flour the cloth you are working on.   
for my 6 patties, I use 6 clean tea towels, j'etale each galette on a cloth on which I would sprinkle the mixture flour and semolina ... like that during cooking, it will be easy to carry each cake. 
  9. let it rise another 30 minutes, 
  10. preheat the oven to 260 degrees C. 
  11. and place a large tray in it, place it on the middle shelf of your oven. 
  12. I turn on my electric oven from above and below with the fan at the same time. 
  13. make individual cooking of each cake, transfer it and place it delicately, in the tray which is very hot in the oven. 
  14. close the oven and leave inflated, as soon as the cake is well inflated, and if your oven is electric like mine, go to the grill mode, if your oven is a gas, normally it is lit from above, as soon as the galette take the color from above turn it, so that it takes the color on the other side. 
  15. cooking will only last 7 to 8 minutes almost for each cake. So to have a nice cooking, watch the cake, as it takes the color from above, turn it, and let the other side gild. 
  16. Continue this way until the pancakes are exhausted. 



[ ![matlou3 el koucha](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-el-koucha.CR2_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/matlou3-el-koucha.CR2_1.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
