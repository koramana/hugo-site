---
title: chicken and potato chekchouka
date: '2012-11-04'
categories:
- amuse bouche, tapas, mise en bouche
- cuisine algerienne
- cuisine diverse
- pizzas / quiches / tartes salees et sandwichs
- recette a la viande rouge ( halal)
- recette de ramadan

---
Hello, 

here is a recipe for me, one of my readers, Lila. for 4 people   
ingredients: 1 chicken cut into pieces, 2 tablespoons tomato paste, 1 onion, 1/2 liter of water, 1 teaspoon of parika, 1 teaspoon coriander powder, 4 or 5 potatoes quartered, 50 grams of black olives, olive oil, sunflower oil for frying, 3 teaspoon of turmeric, salt, pepper, 1 pepper   
that's it for the ingredients   
now the recipe:   
In a saucepan, add some olive oil, let it warm slightly, add the chopped onion until it is transparent.Add then the cut chicken and brown well on all sides, then add the tomato paste, the salt, coriander, paprika and water.Cover and let simmer.   
Next, take the potatoes you have cut into quarters, put them in a salad bowl, put a spoon and half a shave of turmeric, salt and pepper on the potatoes and mix well to infuse the potatoes, and add water to the potatoes, remove the potatoes, wipe them with a clean cloth and keep aside the spicy water of the potatoes, put a spoon and a half cup of turmeric, salt, new pepper on the potatoes and mix well to impregnate them again,   
Ensuite revenon à la cuisson du plat,au bout de 30 minutes de cuisson ,ajoutez l’eau épicée que vous avez mit de côté,laissez cuire puit quand le poulet est cuit et l’eau un peu diminué,ajoutez le poivron coupé en lanière et les olives noires et quand le poivron est cuit ajoutez les pommes de terres que vous aurez cuit dans une friture comme des frite et arrêter tout de suite la cuisson. 
