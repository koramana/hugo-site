---
title: Jelly Apples
date: '2017-09-08'
categories:
- jams and spreads
- basic pastry recipes
- sweet recipes
tags:
- pies
- Easy cooking
- Breakfast
- jam
- Algeria
- Fruits
- Based

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/gel%C3%A9e-de-pommes-2.jpg
---
![Jelly-of-apples-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/gel%C3%A9e-de-pommes-2.jpg)

##  Jelly Apples 

Hello everybody, 

Do you usually garnish your fruit pies with a jelly? did you try to garnish them with apple jelly? In addition to being super easy to make, the apple jelly is also good, it also gives a lot of shine and a better final look. 

Not to lie to you, usually to garnish my pies I bought a jelly of trade super easy to prepare for a fast use, but with the time I found that it is still a little expensive if one counts for uses frequent. 

That's where I turned to making homemade apple jelly, not only is it easy to make, it's also handy when you need jelly. Simply heat 2 tablespoons in the microwave, and that's ready!   


**apple jelly**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/gel%C3%A9e-de-pommes-1.jpg)

Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients**

  * 1 kg of organic apples with the skin 
  * 1 lemon 
  * water 
  * sugar 



**Realization steps**

  1. Wash the apples and lemon carefully. 
  2. cut in quarters keeping skin and seeds. 
  3. place in a saucepan with 300 ml of water and cook on very low heat and covered. 
  4. Stir from time to time to make sure it does not stick to the bottom. the apples will start to melt in a little compote. 
  5. after almost 1 hour of cooking over very low heat, pour the contents of the pan in a clean cloth on a colander, and recover the juice in a salad bowl. After 30 minutes, extract the maximum juice by pressing. 
  6. weigh the recovered juice, and weigh the same weight of sugar 
  7. place in a saucepan and cook on low heat, foaming if necessary. 
  8. the liquid will soon be thicker and take a nice amber color. Take the drop test to stop cooking to the desired consistency: place a drop of preparation on a plate previously cooled in the freezer. The drop, cooled instantly, gives the indication of the final texture of the preparation. When this drop has the desired consistency, stop cooking. 
  9. Pour into jam jars previously cleaned and sterilized. Return the pots until cool. 


