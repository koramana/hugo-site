---
title: mascarpone cake
date: '2016-04-04'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-au-mascarpone-1.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-au-mascarpone-1.jpg)

##  mascarpone cake 

Hello everybody, 

Making a tiramisu is not the ideal solution in this case, because tiramisu is always better after a night in the fridge **at least** . As a result, mascarpone cake is the ideal solution to enjoy it once ready and just after cooling. 

Virginia's recipe is now printed in my head because I often make this delicious cake. I even realize it several times with some [ homemade mascarpone ](<https://www.amourdecuisine.fr/article-mascarpone-fait-maison-comment-faire.html>) (I do that when I leave Algeria, because it is not easy to find there. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/le-cake-au-mascarpone.jpg)

**mascarpone cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/le-cake-au-mascarpone-2.jpg)

portions:  12  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients**

  * 300 gr of flour 
  * 200 gr iced sugar 
  * 250 gr of mascarpone (at room temperature) 
  * 4 eggs (at room temperature) 
  * 100 ml of milk (at room temperature) 
  * 2 teaspoons vanilla powder 
  * 1 sachet of baking powder (1 teaspoon) 



**Realization steps**

  1. Preheat the oven to 150 ° C. 
  2. Butter and flour a cake mold, or just wrap with baking paper 
  3. In a bowl, beat the mascarpone with an electric mixer a few seconds, just to relax it and make it less compact. 
  4. Add sugar and vanilla and beat again, about 1 minute. 
  5. Add the eggs one by one by whisking the mixture about 1 minute after each addition, always using the electric mixer (this is why the cake gets all its softness). 
  6. In alternation add the sifted flour mixed with the baking powder and the milk. Introduce the flour with a spatula   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/le-cake-au-mascarpone-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/le-cake-au-mascarpone-3.jpg>)
  7. pour the mixture into the mold, place the mold on two plates and place in the oven. 
  8. cook for 45 to 50 minutes (check the cooking with the tip of the knife which must come out dry) 



![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/le-cake-au-mascarpone-1.jpg)
