---
title: mhalbi / cream with rice dessert
date: '2012-07-12'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mhalbi-0182.jpg
---
& Nbsp; hello everyone, you really like the title is the m'halbi of my mother, well I take advantage of my presence with my mother to ask the delights with which I grew up, and yesterday she did her delicious mhlabi perfumed with orange blossom. a delicious dessert of Algerian cuisine. in the genre, you can see the recipe for lamhalabia, and Indian rice cream I do not take long to pass the ingredients: 1 liter of milk 5 ca soups of rice reduced to fine powder 1 ca maizina soup 3 ca Soup & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.63  (  3  ratings)  0 

![https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mhalbi-0182.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mhalbi-0182.jpg)

Hello everybody, 

you really like the title is the **I halbi** from my mother, yes, I take advantage of my presence with my mother to ask for the delights with which I grew up, and yesterday she made us her delicious mhlabi perfumed with orange blossom. a delicious [ dessert ](<https://www.amourdecuisine.fr/categorie-12347071.html>) of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) . in the genre, you can see the recipe for the [ mhalabia ](<https://www.amourdecuisine.fr/article-m-halabiya-tricolore-citron-et-fruits-81792006.html>) , and of [ Indian rice cream ](<https://www.amourdecuisine.fr/article-creme-de-riz-indienne-59532236.html>)

![https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mhalbi_21.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mhalbi_21.jpg)

I do not delay in passing you the ingredients: 

  * 1 liter of milk 
  * 5 tablespoons of rice reduced to a fine powder 
  * 1 tablespoon of maizina 
  * 3 tablespoons of orange blossom water 
  * 5 tablespoons of sugar (or according to your taste) 
  * cinnamon for decoration. 



method of preparation: 

  1. take half a glass of milk from the liter, and make the 
  2. maizina in it. 
  3. mix the sugar and the rice with the milk 
  4. and start stirring over low heat 
  5. When the mixture begins to bubble, add the maizina to the remaining milk and orange blossom water. 
  6. continue stirring, when the mixture is well chopped (not too much anyway) pour in ramekins or in dishes for dessert. 
  7. decorate with cinnamon and let cool. 



![https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mhalbi-0232.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mhalbi-0232.jpg)

degustez le bien frais, huuuuuuuuuuuuuum un delice. 
