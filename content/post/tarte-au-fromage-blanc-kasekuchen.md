---
title: White cheese pie - (Käsekuchen)
date: '2015-03-21'
categories:
- cheesecakes et tiramisus

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-au-fromage-blanc-2_thumb1.jpg
---
##  White cheese pie - (Käsekuchen) 

Hello everybody, **White cheese pie - (Käsekuchen)** Another delight from Lunetoiles, who shares passionately with us this sublime recipe of " **White Cheese Pie** ", A recipe that I will place in the category: **cheesecake** . 

we see very well the super sweet pie, shame is not a photo of a small part, but I think she did not have the chance to make a photo, so much that it does not nothing was left of it. 

I hope you liked the recipe just like me. 

and if you like cheesecakes, I recommend these breathtaking recipes: 

[ Strawberry and lemon cheesecake ](<https://www.amourdecuisine.fr/article-cheesecake-aux-fraises-strawberry-cheesecake-78292621.html>)

[ orange and apricot cheesecake ](<https://www.amourdecuisine.fr/article-cheesecake-aux-l-orange-et-abricots-63914634.html>)

[ chocolate mascarpone cheesecake ](<https://www.amourdecuisine.fr/article-cheesecake-mascarpone-chocolat-91867400.html>)

[ cheesecake with strawberry coulis ](<https://www.amourdecuisine.fr/article-cheesecake-au-coulis-de-fraise-78936873.html>)

[ cheesecake white chocolate almond ](<https://www.amourdecuisine.fr/article-cheesecake-chocolat-blanc-amandes-46357282.html>)

**White cheese pie - (Käsekuchen)**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-au-fromage-blanc-2_thumb1.jpg)

Recipe type:  dessert  portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** For the dough: 

  * 250g of flour 
  * 125 g of butter 
  * 40 g of sugar 
  * 1 sachet of vanilla sugar 
  * 50 ml of milk 
  * salt 

For the white cheese: 
  * 500 g of cottage cheese 
  * 100 ml of fresh cream 
  * 4 eggs 
  * 40 g of Maïzena 
  * 115 g of sugar 
  * 1 sachet of vanilla sugar 

Decor: 
  * icing sugar 



**Realization steps** Prepare the dough: 

  1. Put 250 g flour with 125 g diced butter in a bowl, and sand the mixture between the hands. 
  2. Add 40 g sugar, the vanilla sugar packet and the pinch of salt, 
  3. mix, then pour slowly while mixing, the milk. 
  4. Work the dough, roll it into a ball, and let it cool for 30 minutes. 
  5. Spread out the dough, and garnish a 25 cm diameter hinged mold (high-edge, miss-mold type), line with parchment paper. Prick the bottom of the dough with a fork and reserve in a cool place. 
  6. Preheat the oven to 200 ° C. 

Prepare the cottage cheese filling: 
  1. In a salad bowl, mix the fromage blanc, the crème fraîche, the egg yolks with a whisk. 
  2. Then add cornstarch and sugars. 
  3. Beat the egg whites until very stiff with a pinch of salt, and add to the cottage cheese mixture with a spatula, gently lifting the mixture, so as not to break, the egg whites. 
  4. Pour the mixture on the dough. 
  5. Bake, and cook 40 minutes. 
  6. At the end of the oven, wait 5 minutes. Then turn the mold on a rack. And let cool completely, the pie, on the grid, upside down. 
  7. When the pie is cold. Return to a serving dish and sprinkle with icing sugar. 


