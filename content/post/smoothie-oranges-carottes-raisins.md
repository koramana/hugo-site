---
title: Oranges-carrots-raisins smoothie
date: '2015-11-17'
categories:
- boissons jus et cocktail sans alcool
- Healthy cuisine
tags:
- Organic cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-orange-carotte-et-raisins.jpg
---
[ ![orange smoothie, carrot and grapes](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-orange-carotte-et-raisins.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-orange-carotte-et-raisins.jpg>)

##  Oranges-carrots-raisins smoothie 

Hello everybody, 

When I woke up this morning, prepared this simple smoothie, and took these two unique photos with my Samsung Galaxy S4, I did not think for two seconds that I was going to publish the recipe and these photos on my blog. But here I am, as always, post the pictures on my facebook cooking group, and here everyone asks me the recipe .... Hihihih, fortunately I remember very well what I put in the bowl of my blinder ... 

So here are my friends exactly the recipe, as I prepared: 

**Oranges-carrots-raisins smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-orange-carotte-et-raisins.jpg)

Recipe type:  smoothies  portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 oranges peeled and cut into cubes 
  * 4 clean carrots cut into small cubes 
  * 4 generous handfuls of fresh black grapes (seedless) 
  * 1 banana and a half (you can cut the bananas into slices and put in the freezer to have a nice smoothie) 
  * 4 bottles of Actimel (100 gr bottle, otherwise 2 to 3 yogurts of your taste) 



**Realization steps**

  1. place all the ingredients in the blinder bowl. 
  2. turn it on for at least 4 minutes, depending on the capacity of your blinder.   
you can go up to 4 yogurts in your smoothie, it is according to your choice for a light smoothie, or super velvety. 
  3. pass the juice in a large-hole Chinese (just to remove the unmashed carrot pieces and the skin of the grapes).   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/reste-de-smoothie.jpe) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/reste-de-smoothie.jpe>)
  4. Serve immediately, and treat yourself. 



[ ![smoothie health and happiness](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-sant%C3%A9-et-bonheur.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-sant%C3%A9-et-bonheur.jpg>)
