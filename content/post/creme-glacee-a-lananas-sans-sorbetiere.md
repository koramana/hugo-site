---
title: pineapple ice cream without sorbetiere
date: '2014-07-29'
categories:
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-glacee-a-l-ananas-sans-sorbetiere1.jpg
---
![ice cream with pineapple without sorbetiere](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-glacee-a-l-ananas-sans-sorbetiere1.jpg)

##  pineapple ice cream without sorbetiere 

Hello everybody, 

Here is a perfect dessert for those days or even hot evenings, prepared by the care of my dear Lunetoiles, an ice cream with pineapple without sorbetiere .... very easy to make, no need to make a custard, no need to wait 30 minutes in front of the ice cream maker .... 

**pineapple ice cream without sorbetiere**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-glacee-a-l-ananas-sans-sorbetiere-191x300.jpg)

portions:  2-3  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 450 g frozen pineapple by yourself (cut your pineapple into pieces and place in the freezer a few hours before or the best, the old) 
  * 20 cl of vanilla soy milk, or milk (or 1 soya yogurt, or 2 Swiss puffs, or 1 yogurt of cow or sheep, or 200g of cottage cheese in well-drained faisselle 
  * 1 tbsp liquid or powdered vanilla extract 
  * 10 tablespoons agave syrup (you can replace with honey or maple syrup) 



**Realization steps**

  1. Put everything in the bowl of a mixer. 
  2. Start in pulse mode with brief pulses, then mix frankly until you get a perfectly creamy consistency. 
  3. Serve immediately or put in the freezer in a plastic tray but not more than 4 hours otherwise the ice loses its softness. 
  4. If you need to freeze the ice longer, take it out 20 minutes before eating it. 



![ice cream without pineapple ice cream maker, lunetoi recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-glacee-sans-sorbetiere-a-l-ananas-recette-de-lunetoi1.jpg)
