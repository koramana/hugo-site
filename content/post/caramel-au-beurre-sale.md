---
title: salted butter caramel
date: '2014-09-19'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/caramel-au-beurre-sale-024_thumb1.jpg
---
##  salted butter caramel 

Hello everybody 

Salted butter caramel to spread a crotch, or embellish your pies, or just a filet on your desserts, huuuuuuuuuuuuuum, 

We succumb to the charm of this little sweet salt in the mouth, a simple and easy recipe, 

I used this caramel for the realization of these delicious [ salted butter caramel mousse ](<https://www.amourdecuisine.fr/article-mousse-caramel-au-beurre-sale-101394974.html>) , and it was a killing, as I prepared a [ apple pie, apple pie ](<https://www.amourdecuisine.fr/article-apple-pie-dautomne-au-caramel-au-beurre-sale.html>) with this salted butter caramel, yum yum to die for!   


**salted butter caramel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/caramel-au-beurre-sale-024_thumb1.jpg)

portions:  8  Prep time:  5 mins  cooking:  15 mins  total:  20 mins 

**Ingredients**

  * 240 grs of crystallized sugar 
  * 200 ml of milk (or 150 ml of fresh cream) 
  * 70 grams of salted butter 



**Realization steps**

  1. Heat the sugar alone in a stainless steel saucepan over medium heat. Cook without stirring until it melts and caramelizes. 
  2. Gradually add the butter and then the milk or cream while mixing vigorously with a whisk to avoid lumps. 
  3. When the preparation is homogeneous, continue cooking for 5 minutes and remove from heat. The caramel is still liquid but its texture had to change and become thicker. If it is sufficiently cooked, it will harden on cooling. 


