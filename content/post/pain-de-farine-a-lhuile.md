---
title: flour bread with oil
date: '2015-06-25'
categories:
- Mina el forn
- bread, traditional bread, cake
tags:
- '2015'
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/pain-de-farine-a-lhuile-2.jpg
---
[ ![flour bread with oil 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/pain-de-farine-a-lhuile-2.jpg) ](<https://www.amourdecuisine.fr/article-pain-de-farine-a-lhuile.html/pain-de-farine-a-lhuile-2>)

##  flour bread with oil 

Hello everybody, 

For lovers of homemade breads, and for those who prefer flour buns, more than semolina breads, here is a nice recipe for flour bread with oil. A recipe that you can do by hand as my friend did **My passion My cooking** , or use the mess, if you are the kind that does not support the dough that sticks to the hand, because it will stick well, hihihihih. 

If you like homemade breads, why not take a look at these recipes: [ Semolina bread ](<https://www.amourdecuisine.fr/article-pain-de-semoule.html>) (recipe tested and approved several times by my readers), [ bread without filling ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) (khobz dar without kneading, tested and approved by many readers of this blog), [ khobz dar ](<https://www.amourdecuisine.fr/article-khobz-dar-pain-maison.html>) and still full of recipe on the [ category breads and cakes ](<https://www.amourdecuisine.fr/pain-pain-traditionnel-galette>)

**flour bread with oil**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/pain-de-farine-a-lhuile-3.jpg)

portions:  8  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 1 kg of flour 
  * 4 c. Oil cup 
  * 1 cup of sugar 
  * 1 tablespoon of salt 
  * 1 tablespoon of baking yeast 
  * 1 tablespoon of seed of your choice (for me anise and black seed) 
  * 1 glass of warm milk and water 



**Realization steps**

  1. mix all the ingredients (you must have a paste that sticks) 
  2. let rest a moment 
  3. wet your hands with a little warm water and pick up the dough every 5 minutes (repeat the operation 2 or 3 times)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/aftir1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=43055>)
  4. let it swell well then wet your hands with a little oil and divide your dough into 4 balls put in oiled trays, 
  5. brush the patties with egg roll and let them rest and re-inflate 
  6. cook in a preheated oven at 180 degrees C for 15 minutes 



[ ![flour bread with oil 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/pain-de-farine-a-lhuile-4.jpg) ](<https://www.amourdecuisine.fr/article-pain-de-farine-a-lhuile.html/pain-de-farine-a-lhuile-4>)
