---
title: Raita with cucumber and mint video
date: '2013-09-12'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/raita-de-concombre-et-menthe2.jpg
---
![raita-de-cucumber-and-menthe.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/raita-de-concombre-et-menthe2.jpg)

Hello everybody, 

Here is a very refreshing Indian salad, which accompanies deliciously spicy Indian dishes, such as [ tandoori chicken ](<https://www.amourdecuisine.fr/article-poulet-tandoori-109596880.html>) . 

Mint cucumber raita was my choice for this time, knowing that you can vary the ingredients according to your taste. 

**Raita with cucumber and mint video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/raita-de-concombre1.jpg)

Recipe type:  dip sauce  portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 1 Greek yoghurt 
  * 1 clove of garlic 
  * ½ bunch of fresh mint 
  * 1 cucumber 
  * salt, caraway powder 



**Realization steps**

  1. Wash and peel the cucumber. 
  2. Cut the cucumber in half lengthwise, empty it from its seeds and grate it in the big-hole rape. 
  3. let drain for at least 30 minutes. 
  4. thin and mint the mint leaves and add them. 
  5. add over the juice of half a lemon, the pinch of cumin, the garlic crushed and mix. 
  6. add the yoghurt, mix gently, then season with salt according to your taste. 


