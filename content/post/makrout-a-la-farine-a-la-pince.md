---
title: flour makrout with tongs
date: '2016-07-03'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
tags:
- Ramadan
- Algeria
- Algerian cakes
- Cakes
- delicacies
- Ramadan 2016
- Eid

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/makrout-%C3%A0-la-farine-a-la-pince-1.jpg
---
![flour makrout with tongs 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/makrout-%C3%A0-la-farine-a-la-pince-1.jpg)

##  flour makrout with tongs 

Hello everybody, 

I really like the flour makrout, my mother did this often when we were small .... She had two recipes of this flour makrout, one she baked, and another she was frying. 

I confess that with this makrout, you will not stop at a room. It's just too good too fond in the mouth. When I saw the photos of Samia L on our Facebook group, I immediately had this flash memory of my mother's makrout that I had not eaten for a long time. And I thank you very much my dear **Samia L** who made these photos for the blog, with his mobile phone. 

**flour makrout with tongs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/makrout-%C3%A0-la-farine-a-la-pince.jpg)

portions:  40  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 450 gr of flour 
  * 100 gr of melted butter 
  * 1 pinch of salt 
  * 180 ml of orange blossom water 

prank call: 
  * 1 kg of well-cleaned date paste 
  * 50 gr lightly grilled sesame seeds 
  * 2 tablespoons butter 
  * ¼ teaspoon cinnamon 
  * 1 to 2 tablespoons orange blossom water. 



**Realization steps** Prepare the dough: 

  1. work the flour, the pinch of salt and the melted butter cooled. 
  2. Collect the dough with orange blossom water and divide into 3 balls 
  3. let stand at least 2 hours (if not more to be able to pinch easily) 

prepare the stuffing: 
  1. knead the date paste with a little butter, cinnamon and sesame seeds 
  2. add the orange blossom water and knead again. 
  3. form rolls of almost 3 cm in diameter and cover with cling film. 

preparation of makrouts: 
  1. take the dough of flour, and spread it in a thin layer. 
  2. shape a rectangle of the length of the pudding of dates. 
  3. place the pasta pudding on the side of the rectangle, and roll the dough on it to cover it while squeezing. 
  4. Make 2 rounds of dough and cut with a knife. Roll a little so that the side of the cut dough is underneath. 
  5. cut diamonds almost 4 cm long, and pinch to decorate the surface. 
  6. cook in an oven preheated to 180 ° C to have a nice golden color 
  7. as soon as the oven cakes come out, plunge them into the [ honey syrup ](<https://www.amourdecuisine.fr/article-miel-maison-facile.html>) , and leave in syrup for at least 20 minutes.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/makrout-a-la-farine-a-la-pince.jpg)
  8. then remove makrut that will absorb the syrup well, let a little drip and place them in a hermetic box. 
  9. present in boxes with a good mint tea 


