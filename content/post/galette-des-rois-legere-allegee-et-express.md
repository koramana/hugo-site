---
title: slice of the Kings Light and Express
date: '2018-01-09'
categories:
- bakery
- Buns and pastries
- sweet recipes
- pies and tarts
tags:
- Algerian cakes
- Cakes
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/01/galette-des-rois-l%C3%A9gere-all%C3%A9g%C3%A9e-et-express-b-717x1024.jpg
---
![slice of kings light and light b](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/galette-des-rois-l%C3%A9gere-all%C3%A9g%C3%A9e-et-express-b-717x1024.jpg)

##  slice of the Kings Light and Express 

Hello everybody, 

So for this light, lean and express kibble recipe, I'll tell you the little story: 

One of my resolutions of the year 2018 was to try to lose weight, hihihihi, so there I do not tell you how many times I made this resolution! But here is the beginning of my blog, well it's the only resolution that I can not hold until the end! So, I said, let's go for less butter, less sugar, less fat, less of that and that ... Do the walk, go out and do not stay shut in front of the computer to answer every comment and every request !!! 

But here it is, impossible! Especially this beginning of the year, I'm going to visit my dear blogger friends, and with all the recipes of the kings galettes here and there! I'm just trying to hold on so I do not make one. I even put back my old recipe that dates from 2012 I think, just to resist and not to make this recipe that I really like, so here I do not tell you how much I like this recipe! Then my children came to ask me to make them an apple pie ( [ For Apple Pie ](<https://www.amourdecuisine.fr/article-apple-pie-dautomne-au-caramel-au-beurre-sale.html>) ) 

I embarked on the preparation of a [ express puff pastry ](<https://www.amourdecuisine.fr/article-pate-feuilletee-express.html>) that I realize since a moment, since I discovered it in 2010. The dough made quickly, I turn to prepare the farce, and I find myself with only three apples, at the beginning I had the desire to make mini apple pie, then I said to myself, go I'll make a mix cake of kings / call pie !!! I'm going to enjoy myself while making my children happy. But I also wanted my resolution, so I thought I'm going to make my cream of almonds without custard, then I remembered a vegan friend, who always told me that in these cakes, she replaced the eggs by applesauce. (I do not know if you're still here, hihihihi the story is to sleep) 

So I said to myself again, I'm going to do without eggs, and instead of preparing my stuffing by placing a generous layer of almond cream, which I would cover, or applesauce in pieces, or just apples! then another layer of dough, no, I made the short cut, and I just replaced the eggs with the applesauce in my almond cream, and that's it. 

The result: the first apple pie-galette of the kings, was to begin before cooling and even before I unsheathed my camera to make photos .... Yes. 

And the second, that I post you today, the photos were taken very late at night, I do not know by what force or what energy because my son wanted to taste it for dessert, it was just not to do it again 3 rd time ... because in 2 times, I ate easily the 1/3 of these apples pie galette kings .... hun, seriously! diet! resolution???? It is true that this slice of light and light lighter kings is still less caloric, but the rule says: eat less, not eat every little piece that remains in the plate of his children! 

**slice of the Kings Light and Express**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/galette-des-rois-l%C3%A9gere-all%C3%A9g%C3%A9e-et-express-a.jpg)

**Ingredients** 100 g butter in pieces well frozen 

  * 200 gr of flour 
  * 200 gr of swiss cubs 
  * salt 

for applesauce: 
  * 2 to 3 apples 
  * a little water 
  * butter (optional) 
  * sugar (optional) 

for the almond cream: 
  * 60 g of sugar 
  * 100 g of almond powder 
  * 50 g of butter 
  * 160 gr of applesauce 
  * aroma of bitter almond 
  * 1 egg for gilding 



**Realization steps** Prepare the puff pastry 

  1. Put it all in your robot 
  2. operate for 10 seconds at maximum speed 
  3. You get a lumpy dough 
  4. Simply agglomerate into a ball and put 1 hour filmed in the fridge, but you can give a few laps (bends) before putting it in the fridge, I gave 3 turns of folding. 

prepare the applesauce 
  1. Peel the apples and seed them. 
  2. Cut them into cubes. Put them in a saucepan over low heat adding water 
  3. cover and cook on fire for about 15 minutes, stirring regularly with a wooden spoon. 
  4. Finally, add the sugar if necessary and the butter as needed and pass the compote to the mill. 

prepare the almond cream: 
  1. Place the butter in pieces in a bowl with the sugar, and mix 
  2. add the applesauce, and mix again 
  3. introduce the almond powder and homogenize it all 

mounting: 
  1. spread the puff pastry to almost 5 mm, and form two circles of almost the same size (the disc which will be as covered, is better that it is a little bigger) 
  2. Pass the egg yolk with a brush all around the sides of the base rectangle (2 cm) 
  3. Garnish the almond cream center with the pastry bag or spatula 
  4. Lay the second disc of the puff pastry and solder with your hands to prevent the escape of the almond cream. 
  5. Chipping the edges with fingertip pressure 
  6. Drill with the tip of the knife in the center to make a chimney. 
  7. To brown with a beaten egg and to create the decoration in spike or in spiral with the point of the knife. 
  8. bake at 150 ° C for 20 minutes, or according to your oven. 


