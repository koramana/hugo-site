---
title: slippers with leeks and salmon
date: '2015-03-29'
categories:
- Bourek, brick, samoussa, chaussons
- houriyat el matbakh- fatafeat tv
- idee, recette de fete, aperitif apero dinatoire
tags:
- Amuse bouche
- inputs
- Aperitif
- Appetizers
- Algeria
- Fish
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/chaussons-aux-poireaux-et-au-saumon-1.jpg
---
[ ![slippers with leeks and salmon](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/chaussons-aux-poireaux-et-au-saumon-1.jpg) ](<https://www.amourdecuisine.fr/article-chaussons-aux-poireaux-et-au-saumon.html/sony-dsc-281>)

Hello everybody, 

You are ready for a nice starter, delicious slippers with leeks and salmon, it must be said that the leeks and the salmon give an association to fall, perso when I make a [ leek fondue ](<https://www.amourdecuisine.fr/article-fondue-de-poireaux.html> "recipe Fondue of leeks") I often accompany it with lightly grilled salmon, and it's just to eat without moderation. 

When Lunetoiles gave me the pictures of this recipe and the recipe, I did not hesitate a second to make these leeks and salmon slippers, and I promise you that it's a delight, except that I did not I did not have the time to take pictures, especially since I prepared them for dinner, so I'm very happy to share with you the photos and the Lunetoiles recipe. 

[ ![slippers with leeks and salmon](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/chaussons-aux-poireaux-et-au-saumon-2.jpg) ](<https://www.amourdecuisine.fr/article-chaussons-aux-poireaux-et-au-saumon.html/sony-dsc-282>)   


**slippers with leeks and salmon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/chaussons-aux-poireaux-et-au-saumon-3.jpg)

portions:  12  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients** for the dough: 

  * 250g of flour 
  * ½ glass of water 
  * ½ glass of olive oil 
  * 1 pinch of salt 
  * an egg yolk 

For the stuffing: 
  * 3 leeks 
  * 2 salmon fillets 
  * whole liquid cream 
  * salt and black pepper 
  * olive oil 

For gilding: 
  * an egg 



**Realization steps** Prepare the dough: put the flour in a bowl, make a fountain, add the yolk and oil by sanding with your hands, add salt and water, until you obtain a paste. 

  1. Wrap with a cling film and let the dough rest in a cool place for 1 hour. 

Meanwhile prepare the stuffing: 
  1. Cook the two fish fillets and set aside. 
  2. Wash the leeks well and cut them into slices. 
  3. Put them in a pan with the olive oil, salt and pepper and cook on low heat until they are melted and cooked. 
  4. Add liquid cream, just to make a sauce, but not too much! 
  5. Let reduce, over low heat. 
  6. Once the stuffing has thickened enough, place this stuffing in a salad bowl. 
  7. Crush the salmon fillets with a fork and transfer to the salad bowl with the leeks. 
  8. Mix and let cool! 

To form our slippers: Divide the dough into several pieces. 
  1. Spread each piece of dough on a floured work surface, cut with a round cookie cutter of 12 cm diameter, and put in the center of each round stuffing and fold into slippers. 
  2. Weld each slipper with a fork, pressing on the edges 
  3. Place the slippers on the baking sheet lined with parchment paper. 
  4. Brush with an egg brush and bake in a preheated oven at 170 ° C for 20 to 25 minutes. 



[ ![slippers with leeks and salmon3](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/chaussons-aux-poireaux-et-au-saumon.jpg) ](<https://www.amourdecuisine.fr/article-chaussons-aux-poireaux-et-au-saumon.html/sony-dsc-284>)
