---
title: Roasted chicken with lemon and honey
date: '2011-07-05'
categories:
- cuisine algerienne
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/Concours-ramadan-chez-Soulef_thumb11.jpg
---
hello everyone, here is a little recipe that has done well around the blogosphere, because it seemed strange, with the presence of honey, but ultimately that did not change anything to the taste of the chicken, but rather, we get a flavor of more . in any case, I had resisted too much to make this recipe, but when I saw it at Chris's, I could not help but make it, and frankly, I was not disappointed at all, nor even my husband. the ingredients without delay are: chicken leg 4 tomatoes (canned tomatoes, pieces for me) 2 pods & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

here is a little recipe that has been well rounded the blogosphere, because it seemed strange, with the presence of honey, but ultimately that did not change anything to the taste of the chicken, but rather, we get a flavor more. 

the ingredients without further ado are: 

  * chicken leg 
  * 4 tomatoes (canned tomatoes, pieces for me) 
  * 2 chopped garlic cloves 
  * 2 lemons 
  * 80g of black olives (I did not have any) 
  * 4 bay leaf 
  * 1 tablespoon liquid honey 
  * 5cl of olive oil 
  * salt and black pepper 



in a bowl, mix the honey, the juice of 1 lemon, the olive oil, the thyme leaves and the minced garlic cloves, salt and pepper   
turn the chicken pieces in the preparation   
place the chicken pieces in a roasting dish, drizzle with marinade, add 1 sliced ​​lemon, black olives, bay leaves and tomatoes cut in 4   
slide the dish in a preheated 180 ° oven and cook for 30 minutes, until the chicken is golden brown 

a delight to your turn to try 

and do not forget to prepare your recipes for the contest: 

[ ![Ramadan competition at Soulef](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/Concours-ramadan-chez-Soulef_thumb11.jpg) ](<https://www.amourdecuisine.fr/article-concours-entrees-ramadanesques-78386425.html>)

[ Ramadanque Entrance Contest ](<https://www.amourdecuisine.fr/article-concours-entrees-ramadanesques-78386425.html>)

qui va se lancer a partir du 10 Juillet, donc soyez nombreux a y participer 
