---
title: tagine with spinach
date: '2016-08-06'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tajine-aux-epinards-025_thumb2.jpg
---
##  tagine with spinach 

Hello everybody, 

here is a recipe for **spinach tajine** I invite you to make a recipe quickly made and that can accommodate all the sauces, I never make the same recipe, everything depends on what I have in my fridge, sometimes I do this tagine completely vegetarian, sometimes with chicken, sometimes with meat, and this time with minced meat. 

you can see even more recipes from the [ Algerian cuisine ](<https://www.amourdecuisine.fr/cuisine-algerienne>)

so, to our aprons !!! ... 

**tagine with spinach**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tajine-aux-epinards-025_thumb2.jpg)

Recipe type:  dish, baking dish  portions:  4  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 500 gr spinach (frozen for me) 
  * 150 gr of minced meat. 
  * 4 eggs 
  * ½ can of tomato in piece or 2 fresh tomatoes 
  * 100 gr grated cheese. 
  * 3 cloves of garlic 
  * salt and black pepper 
  * ¼ cup of cumin 
  * 1/4 cup cilantro. 
  * 2 to 3 tablespoons of olive oil 



**Realization steps**

  1. take the spinach out of the freezer 1 hour before 
  2. fry the meat in a little oil, season with a little salt and black pepper, do not over-salt. 
  3. preserve the minced meat, and in the same pan, fry the spinach in a little oil between 5 and 10 minutes, stirring so that it does not burn. 
  4. add the crushed garlic and sauté again 
  5. add the chopped tomato, and let it come back. 
  6. at this point, season with a little salt, and black pepper, beware of salt, spinach contains minerals and therefore salt, and remember the minced meat is salty too. 
  7. remove the spinach from the heat, mix with the minced meat. 
  8. add the grated cheese, and the spices 
  9. gently whisk the eggs, just to scramble them, and introduce it to the mixture. 
  10. put everything in a small mold, I use a silicone kouglouph mold 
  11. bake in preheated oven at 180 degrees, for 15 to 20 minutes 
  12. remove from oven, turn out and garnish with some grated cheese. 


