---
title: Chocolate cake
date: '2017-07-14'
categories:
- cakes and cakes
tags:
- Algerian cakes
- Pastry
- To taste
- desserts
- Cakes
- Cake
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/Gateau-fondant-au-chocolat.jpg
---
![Chocolate cake](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/Gateau-fondant-au-chocolat.jpg)

##  Chocolate cake 

Hello everybody, 

Here we are with a delicious dessert, the chocolate fondant. Personally it is my most appreciated taste. 

today, it is Lunetoiles who shares with us his chocolate fondant recipe. 

And if you like varieties of this good dessert, you can see the [ chocolate fondant with chestnuts ](<https://www.amourdecuisine.fr/article-fondant-au-chocolat-et-aux-marrons.html>) , [ chocolate and peanut butter fondant ](<https://www.amourdecuisine.fr/article-fondant-au-chocolat-au-beurre-cacahuetes.html>) , and [ chocolate fondant and speculoos ](<https://www.amourdecuisine.fr/article-fondant-au-chocolat-et-speculoos.html>) . 

Thanks to Lunetoiles for this recipe, and do not forget my friends, if you try one of the recipes from my blog to share them with me on my email: 

![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg)

**Chocolate cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/Gateau-fondant-au-chocolat-1.jpg)

**Ingredients**

  * 200 g chocolate dessert 
  * 150 g of sugar 
  * 150 g of butter 
  * 50 g of flour 
  * 3 eggs 



**Realization steps**

  1. Melt the chocolate in a bain marie. 
  2. In a salad bowl, mix the melted butter and sugar, then mix the eggs and flour alternately, mixing well between each addition. 
  3. Then add the chocolate, you have to get a smooth, lump-free liquid paste. 
  4. Pour your preparation in a mold of your choice, put in the fridge for 1 hour. 
  5. After one hour, preheat the oven to 150 ° C. 
  6. Bake at 150 degrees for 20 min no more! 



![Chocolate cake](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/Gateau-fondant-au-chocolat-2.jpg) **Source:** **pâtissiers**
