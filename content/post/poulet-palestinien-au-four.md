---
title: Baked Palestinian Chicken
date: '2011-02-12'
categories:
- diverse cuisine
- Cuisine by country
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/poulet-palestinien-51.jpg
---
![Palestinian chicken 5](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/poulet-palestinien-51.jpg)

![Chicken-palestinien1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/poulet-palestinien11.jpg)

Hello everybody, 

here is a wonderful recipe that I make every time, so much that it is good, when I saw this recipe at [ Nadia ](<http://couzinadielnadia.canalblog.com/archives/2008/12/14/10068863.html>) I immediately took the chicken legs out of the freezer, the original recipe had to contain fresh chicken, but not bad, it did the trick, and I swear it was really a treat, despite the Saumac's absence (I do not even know what it is, I have to look for it, Nadia said to replace it with lemon, and that's what I did. 

Ingredients: 

  * Chicken legs 
  * 2 to 3 onions, depending on the size (you can put more, we make you after cooking it is a sublime taste) 
  * Salt 
  * black pepper 
  * Turmeric 
  * Paprika 
  * Cinnamon 
  * Saumac (replaced at home by the juice of a whole lemon, huuuuuuuuuuuum) 
  * olive oil 



![Chicken-palestinien2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/poulet-palestinien21.jpg)

![Chicken-Palestinian-4.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/poulet-palestinien-41.jpg) ![Chicken-palestinien3.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/poulet-palestinien31.jpg)

  1. cut a chicken into pieces, coat with salt, pepper, turmeric, paprika, cinnamon and **sumach** , or like me the juice of a lemon 
  2. slice 2 to 3 onions, put them in your baking tray / pan and cover them with olive oil, 
  3. place on top, the pieces of chicken and mix with onions. 
  4. cover with foil and cook for 1 to 1 1/4 hours, taking care to return them from time to time. 
  5. remove the aluminum and roast for about ten minutes. it's a very simple way that gives a very delicious result and very melting in the mouth 



![Chicken-Palestinian-5.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/poulet-palestinien-51.jpg)

I swear it was very very good, I served this chicken with a baked potato dish, 

good tasting 
