---
title: Mini Tacos with shrimp
date: '2016-02-24'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
tags:
- sauces
- Fast Food
- Mexico
- Easy cooking
- inputs
- Amuses Mouth
- Aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Mini-tacos-aux-crevettes-3.jpg
---
[ ![Mini shrimp tacos 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Mini-tacos-aux-crevettes-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Mini-tacos-aux-crevettes-3.jpg>)

##  Mini Tacos with shrimp 

Hello everybody, 

As I already told you, at home we love everything that is based on [ tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html>) , and among the recipes are at the top of our list, the tacos .... But when I say tacos, it's crunchy tacos, very pungent, with a great touch of acidity, um, nothing that tells you that, I want to put my hand in the screen to take one or two of these mini shrimp tacos. 

[ ![logo-serving hatch-between-amis1-300x300](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/logo-passe-plats-entre-amis1-300x300.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/logo-passe-plats-entre-amis1-300x300.png>)

In any case, it is in this recipe that I put the delicious [ tomato salsa, pico de gallo ](<https://www.amourdecuisine.fr/article-recette-pico-de-gallo-ou-salsa-a-la-tomate.html>) that I shared with you yesterday. 

**Mini Tacos with shrimp**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Mini-tacos-aux-crevettes-5.jpg)

portions:  16  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for shrimps: 

  * 100 gr raw shrimp, peeled and deveined 
  * 2 tbsp. extra virgin olive oil 
  * 2 cloves of garlic, chopped 
  * Salt and pepper 

Mini Tacos shells: 
  * 4 [ flour tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html>) (the extra sweet, if you can find them) 
  * oil 
  * Hot sauce with mayonnaise (Adobo Sauce) 
  * ⅓ cup of mayonnaise 
  * 3 c. fresh cream 
  * 1 C. a harissa coffee, or tabasco. 
  * 2 tbsp. tablespoon lime juice 
  * ½ teaspoon of salt 
  * [ Pico de Gallo ](<https://www.amourdecuisine.fr/article-recette-pico-de-gallo-ou-salsa-a-la-tomate.html>)

and to serve 
  * ½ cup of chopped green cabbage or salad to taste 



**Realization steps** Prepare the shrimps: 

  1. Cut the shrimp into pieces of 1 to 1.5 cm. 
  2. Mix all the shrimp ingredients in a small bowl and set aside while you prepare the other components of your tacos. 

prepare the Mini Tacos shells: 
  1. Preheat the oven to 180 ° C. 
  2. remove the rack from the oven and place it on two bowls to put the tortilla rings. 
  3. Cut 7.5cm diameter slices of your tortillas 
  4. Brush liberally with water to make them soft. 
  5. Place them in the microwave for 15 to 20 seconds, until they become super mellow. 
  6. place as you go on the grid before it cool down and become more manageable. 
  7. Spray with oil, otherwise use a brush (it will be a little difficult with a brush), then cook for 5 to 7 minutes until they are golden brown and crispy. Take out of the oven and let cool down.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/les-tacos-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/les-tacos-.jpg>)

prepare the hot sauce: 
  1. Mix all ingredients in a food processor until smooth. 

prepare the pico de gallo salsa cook shrimp 
  1. Heat a skillet with a thick bottom, add the oil. 
  2. Add the shrimp and cook for 40 seconds, until everything starts to take a nice color. The pieces are small and they cook very quickly. Remove from the pan. 

mounting tacos 
  1. place some salad on the taco shells 
  2. add over shrimps 
  3. then add the salsa pico de gallo 
  4. and garnish generously with the hot sauce. 



[ ![Mini shrimp tacos 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Mini-tacos-aux-crevettes-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Mini-tacos-aux-crevettes-4.jpg>)
