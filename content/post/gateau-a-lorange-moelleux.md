---
title: cake with fluffy orange
date: '2012-12-26'
categories:
- jams and spreads
- sweet recipes

---
Hello everybody, 

it is the season of oranges, so take advantage of this fruit rich in vitamin C, and also well flavored, so here is an orange cake, and the coconut that I concoct, a small [ cake ](<https://www.amourdecuisine.fr/categorie-10678925.html>) has the orange that is soft wish. 

you can also see: 

[ orange cake without gluten ](<https://www.amourdecuisine.fr/article-gateau-sans-gluten-aux-oranges-et-amandes-91051849.html>)

and [ cake with orange ](<https://www.amourdecuisine.fr/article-gateau-a-l-orange-102623009.html>) . 

Ingredients:   


  * 6 eggs 
  * 1 and a half cups of sugar (a glass of 250 ml) 
  * 1 glass and a half sifted flour 
  * 1 packet of baking powder 
  * 1 half glass of coconut 
  * 1 glass of orange juice (if it's canned juice, reduce the amount of sugar) 
  * 1/2 glass of oil 



the method of preparation: 

  1. Wash and grate a big orange 
  2. separate the yolks from the egg whites, and beat the egg white into the snow, with 1 pinch of salt 
  3. add half of the sugar towards the end 
  4. in another bowl, mix all remaining ingredients 
  5. incorporate this mixture any egg white paper 
  6. oil and flour a Palestinian mold and pour the mixture into it 
  7. cook at 160 degrees, check the cooking with the tip of the knife 
  8. take out the cake and sprinkle with icing sugar. 



good tasting 

thank you for your comments, and your passage 

bonne journée 
