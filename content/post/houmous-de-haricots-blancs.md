---
title: hummus of white beans
date: '2018-05-12'
categories:
- appetizer, tapas, appetizer
- dips and sauces
- idea, party recipe, aperitif aperitif
tags:
- Express cuisine
- Vegan Kitchen
- sauces
- aperitif
- Easy cooking
- Mash potatoes
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/houmous-de-haricots-blancs.jpg
---
![hummus of white beans](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/houmous-de-haricots-blancs.jpg)

##  hummus of white beans 

Hello everybody, 

Here is a hummus of white beans that I have not realized for a long time. I discovered this recipe a while ago, while watching a cooking show on one of the local channels. Since each time I prepared white beans, I left a quantity well cooked, without the sauce, to make this hummus just delicious. 

The flavor and strong taste of roasted garlic adds a lot to this fragrant hummus plus fresh basil, it's just a burst of flavors on the palate! Anyway, it's a recipe made for me, that's what I said the first time I had tasted it! And I'm not the only one, every time I make a cassoulet of white beans, my husband comes the next day to ask me if I made hummus of white beans! 

In any case, you can for this recipe, use canned white beans, after the only thing that may take you time to make this hummus, is the preparation of roasted garlic, hihihihi. You can of course recite the recipe according to your desires, besides that's what I do, when I want to enjoy this hummus, and I lack the ingredients. 

For garlic, I roast 2 heads or 3, at once and I keep rotten garlic cloves in a well closed jar, I then use it in many recipes, such as grilled pepper salads.   


[ ![hummus of white beans 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/houmous-de-haricots-blancs-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/houmous-de-haricots-blancs-1.jpg>)
