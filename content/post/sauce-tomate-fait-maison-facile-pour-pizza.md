---
title: Homemade tomato sauce easy for pizza
date: '2017-08-20'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- dips and sauces
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sauce-tomate-fait-maison-facile-1.jpg
---
[ ![homemade tomato sauce easy for pizza](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sauce-tomate-fait-maison-facile-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sauce-tomate-fait-maison-facile-1.jpg>)

##  Homemade tomato sauce easy for pizza 

Hello everybody, 

How to make tomato sauce easy and rich in taste? it's always the question we ask ourselves, and personally when I'm invited to a person's house, and that it presents a tomato sauce to accompany such or such dish, I always ask it begins that it prepared its sauce tomato. 

Each person has his little secrets, and each time we detect a different taste that gives a more tomato sauce ... 

This time, here we are with the secret of Lunetoiles homemade tomato sauce recipe. A very different method to prepare a light sauce, well scented, and especially to make a small reserve of this tomato sauce in jars. 

**Homemade tomato sauce easy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Sauce-tomate-fait-maison.jpg)

portions:  8  Prep time:  15 mins  cooking:  2 mins  total:  17 mins 

**Ingredients**

  * 1.5 kg of seeded and cut tomatoes   
(to have 3 jars of 250 ml of sauce) 
  * 2 red peppers, seeded and cut in eighths 
  * 1 big onion, cut into thin quarters 
  * 4 cloves of garlic, peeled 
  * Leave 1 teaspoon chopped rosemary 
  * 1 small pepper seeded and chopped (optional) 
  * ¼ cup tomato paste (about 70 gr) 
  * 2 tablespoons sugar 
  * 2 tablespoons extra virgin olive oil 
  * 1 teaspoon of salt 
  * ground black pepper 



**Realization steps**

  1. Preheat the oven to 140 ° C (turn on heat for me). 
  2. Prepare the tomatoes, peppers and onion and place in a large roasting pan lined with parchment paper.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sauce-tomate-maison-300x237.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sauce-tomate-maison-300x237.jpg>)
  3. They must be in one layer so that they are roasted and caramelized, use two roasting dishes, if necessary. 
  4. Add garlic, rosemary leaves and chilli, if desired. 
  5. In a small bowl, combine the tomato paste, sugar, olive oil, salt and pepper. 
  6. Put this mixture on the vegetables and stir to coat them well. 
  7. Bake for about 2 hours or until vegetables begin to caramelize and shrivel a little.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sauce-tomate-fait-maison-facile-300x237.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sauce-tomate-fait-maison-facile-300x237.jpg>)
  8. Let the vegetables cool and puree in the food processor or blender. 
  9. The sauce can be refrigerated for up to a week or can be frozen. 
  10. If not, bring the mashed sauce to a boil and while it is very hot, pour it into sterilized jars and seal immediately and turn the pot upside down and let cool completely. 
  11. Keep sterile jars sterilized for months in the pantry. 



[ ![how to make a homemade tomato sauce for pizza](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/comment-faire-une-sauce-tomate-maison-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/comment-faire-une-sauce-tomate-maison-001.jpg>)

Thank you for your visits and comments. 

A la prochaine recette. 
