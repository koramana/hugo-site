---
title: olives cake
date: '2016-07-28'
categories:
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-sale-1-a_thumb.jpg
---
##  Olive cake 

Hello everybody, 

I love salty cakes like this cake with olives, a recipe that Lunetoiles shared with you through the lines of my blog, a cake with olives that I tried several times for my guests and who made the unanimity of "yumi salty cake". 

**olives cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-sale-1-a_thumb.jpg)

Recipe type:  aperitif salty cake  portions:  8  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 200 g flour 
  * 3 eggs 
  * 100 ml of milk 
  * 100 ml of olive oil or sunflower oil 
  * 1 packet of dry yeast 
  * 150 g of feta 
  * 75 g of pitted black olives 
  * mill pepper 
  * butter for the mold 
  * no salt (feta and olives are quite salty) 



**Realization steps**

  1. Preheat the oven to 180 ° C, 
  2. butter a cake tin. 
  3. Cut the feta and the olives in small dice, 
  4. Flour and shake well. 
  5. Mix flour, yeast and beaten eggs, a little pepper. 
  6. Add the oil, milk and diced feta and olives. 
  7. Pour the mixture into the pan and bake for about 45 minutes. 
  8. Check the cooking, then let cool a few minutes before unmolding.   
Serve warm or cold. 



Merci a lunetoiles pour son partage. 
