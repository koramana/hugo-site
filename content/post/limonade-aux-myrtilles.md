---
title: blueberry lemonade
date: '2016-03-27'
categories:
- boissons jus et cocktail sans alcool
- Healthy cuisine
tags:
- Drinks
- desserts
- Easy cooking
- Ramadan
- Organic cuisine
- Juice
- Healthy cuisine
- cocktails

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/limonade-aux-myrtilles-1.jpg
---
[ ![bilberry lemonade 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/limonade-aux-myrtilles-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/limonade-aux-myrtilles-1.jpg>)

##  blueberry lemonade 

Hello everybody, 

When you find yourself with a large amount of blueberries a little acidic to the tastes of children, you have to make sure that they eat the fruits they love so much in one way or another. 

I realized [ blueberry muffins ](<https://www.amourdecuisine.fr/article-muffins-aux-myrtilles-recette-de-muffins-extra-moelleux.html>) , [ blueberry ](<https://www.amourdecuisine.fr/article-financiers-aux-myrtilles.html>) , a [ blueberry crumble ](<https://www.amourdecuisine.fr/article-crumble-aux-myrtilles.html>) (I still have a taste in the mouth, so much that it's good) and other delights to come. But the one who liked it most is this lemonade with blueberries, super refreshing, and just good especially if you control the amount of sugar. I took it to the picnic with my kids, and there was not a drop. It was the first time we went to the park and my kids do not ask for ice cream. 

The recipe is super simple, and it's a realization that will not take more than 2 minutes maximum. Just have a few ice cubes in the freezer for an even more refreshing drink. 

[ ![blueberry lemonade 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/limonade-aux-myrtilles-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/limonade-aux-myrtilles-2.jpg>)   


**blueberry lemonade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/limonade-aux-myrtilles-4.jpg)

portions:  6  Prep time:  2 mins  total:  2 mins 

**Ingredients**

  * 2 glasses of fresh blueberries 
  * granulated sugar (add per spoon, depending on the acidity of the fruit) 
  * ⅔ glass of freshly squeezed lemon juice 
  * 4 glasses of cold water, 
  * slices of lemon to serve (organic lemon) 
  * some blueberries 
  * some mint leaves. 
  * gallows 



**Realization steps**

  1. Place the blueberries, 2 tablespoons of sugar and 1 glass of water in the blinder bowl. 
  2. Blend on medium speed for about 1 minute, until the blueberries are completely pureed. 
  3. Spread the purée of blueberries through a fine mesh sieve. 
  4. Add the lemon juice and the remaining 3 glasses of water. 
  5. Adjust the sugar and water, according to your taste. 
  6. Stir well, and refrigerate. Serve on ice. 



[ ![blueberry lemonade_](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/limonade-aux-myrtilles_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/limonade-aux-myrtilles_.jpg>)
