---
title: Focaccia with rosemary and onion
date: '2018-02-08'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/focaccia-au-romarin-2.jpg
---
[ ![rosemary focaccia 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/focaccia-au-romarin-2.jpg) ](<https://www.amourdecuisine.fr/article-une-focaccia-au-romarin-et-oignon.html/focaccia-004-2>)

##  Focaccia with rosemary and onion 

Hello everyone, 

imagine that to do it you only need your blinder ???? 

**Focaccia with rosemary and onion**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/focaccia-au-romarin-3.jpg)

portions:  6  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 1 sachet of instant yeast (I do not know and despite that I put everything in half, but I use a bag of instant yeast) 
  * 400 ml warm water 
  * 1 C. a sugar coffee 
  * 2 and a half c. a coffee of salt 
  * 60 ml of olive oil 
  * 700 gr of flour (more at least if the flour absorbs well or not) 

for garnish: 
  * 2 tbsp. soup of olive oil 
  * ¼ onion cut into strips 
  * a little fresh rosemary (or dry, which I put) 



**Realization steps**

  1. in the bowl of the blender, place the water sugar and yeast, let act 5 minutes at the max, add oil, salt, and ¾ of the amount of the flour, and shield gently, until that the dough is formed (not having to put all the flour, add the flour as needed, until you get a nice dough, not too sticky.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/focaccia-romarin-oignon-1_thumb1-300x243.jpg)
  2. place your dough in a well-oiled mold, and spread out your dough evenly. 
  3. Cover the dough with a clean cloth and place it in a hot but off oven, until it doubles in volume (in the video the girl says 1h to 1h30, for me and despite the cold in 20 min, already it does well to swell, hihihiih I do not know is it yeast, or the oven that was hot, hihihiih)   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/focaccia-romarin-oignon-2_211-300x243.jpg)
  4. remove your tray, turn on the oven 180 degrees time to garnish the Focaccia 
  5. with the tip of your finger, leave prints on your bread. 
  6. in a bowl, mix the olive oil, rosemary and onion, then with a brush and without breaking the dough that has risen, try to brush the entire surface of your bread. 
  7. then decorate with the rosemary soaked in oil, as well as onion, sprinkle after the focaccai with a little bit of coarse salt. (I had only a fine salt, so I sprinkled the surface with a pinch of salt. 
  8. and cook for 20 to 25 minutes, or depending on your oven. 



![rosemary focaccia 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/focaccia-au-romarin-4.jpg)

And enjoy this wonder! I promise you that this focaccia is the best recipe that I could taste until today, it is soft, very melting in the mouth, with a crispy layer, and especially the taste of olive oil flavored, I do not tell you, a real delight! 

try this focaccia with rosemary and onions and tell me your opinion. 

![rosemary focaccia 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/focaccia-au-romarin-3.jpg)
