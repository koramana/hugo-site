---
title: Figolu homemade
date: '2015-02-28'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours
tags:
- Cakes
- Algerian cakes
- Algeria
- biscuits
- Cookies
- figs
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Figolu-fait-maison.jpg
---
[ ![homemade figolu 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Figolu-fait-maison.jpg) ](<https://www.amourdecuisine.fr/article-figolu-fait-maison.html/sony-dsc-273>)

##  Figolu homemade 

Hello everybody, 

Among the commercial cookies that I like the most, figolus, or Fig rolls, are at the top of the list, impossible that I do not go out and I do not come back with a box of figolu at home, especially that here, they are made from whole grain wheat flour, in addition to a delicious fig stuffing, besides, even my children like it a lot. 

Lunetoiles gave his personal touch to the stuffing of these cookies, so follow the recipe for more details 

[ ![homemade figolu](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Figolu-fait-maison-1.jpg) ](<https://www.amourdecuisine.fr/article-figolu-fait-maison.html/sony-dsc-273>)   


**Figolu homemade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/figolu-fait-maison-3.jpg)

portions:  30  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** Dough: 

  * 190 gr of butter 
  * 90 gr of sugar 
  * 4 egg yolks 
  * ¼ cup of baking powder 
  * 1 pinch of salt 
  * 2 nice pinches of cinnamon powder (not put) 
  * 30 gr of orange juice 
  * Flour (you can mix all-purpose and complete flour) 

Fodder with figs: 
  * 350 gr of soft dried figs 
  * 25 gr of honey 
  * Cinnamon coffee (not put) 
  * 2-3 tablespoons of applesauce 
  * 1 tablespoon orange blossom water 
  * 1 tablespoon of melted butter 
  * 1 tablespoon of apricot jam heated, to work the stuffing with the robot 
  * add 80 gr of almond powder 



**Realization steps**

  1. using a whisk, whip the softened butter with the sugar, 
  2. add the egg yolks one at a time by mixing well between each addition then the orange juice, mix 250 gr of flour with the salt, the cinnamon and the baking powder and incorporate little by little, these 250 gr are not the total amount of flour, it is necessary then to add the flour spoon after spoon until the dough is in a ball and does not stick to the hands, 
  3. cover with a film of food and put in minimum 1h. 

Prepare the stuffing, 
  1. cut the figs and put them in the chopper bowl with honey, cinnamon and applesauce, mix a few minutes to form a firm dough. 
  2. add the rest of the ingredients, then pick up with the almond 
  3. Divide the shortbread into 4 portions, 
  4. lower each portion on 7 mm thick, arrange a quarter of the stuffing over the entire length of the dough and close to form a sausage 
  5. place on a baking sheet covered with baking paper and flatten slightly, then cut the cakes on 2-3 cm wide 
  6. Bake at 180 ° C for 15-20 minutes, the cakes should be lightly browned! 



[ ![homemade figolu1](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/figolu-fait-maison-2.jpg) ](<https://www.amourdecuisine.fr/article-figolu-fait-maison.html/sony-dsc-274>)
