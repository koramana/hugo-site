---
title: trida constantinoise white sauce
date: '2015-04-06'
categories:
- cuisine algerienne
tags:
- Full Dishes
- Lamb

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trida-constantinoise-1.jpg
---
[ ![trida constantinoise](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trida-constantinoise-1.jpg) ](<https://www.amourdecuisine.fr/article-trida-constantinoise-sauce-blanche.html/trida-constantinoise-1>)

##  trida constantinoise white sauce 

Hello everybody, 

Here is a dish well known in eastern Algeria, the constantine trida in white sauce. At home my husband really likes this dish, but to my dismay my husband loves the [ trida in red sauce ](<https://www.amourdecuisine.fr/article-25345529.html> "trida - chicken pasta squares "تريدة بالدجاج"") . It's not something that really makes me happy, because I do not like disfiguring a traditional recipe, but we do our best to satisfy our little entourage. 

Today, I share with you, with immense pleasure, the recipe of the tride constantinoise of my friend Wamani Merou, a recipe to which she gave a personal touch, while keeping the originality of the recipe.   


**trida constantinoise white sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trida-constantinoise.jpg)

portions:  4-6  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * pasta of trida (according to the number of people) 
  * lamb or chicken meat 
  * minced meatballs (optional) 
  * 1 grated onion, 
  * chickpeas dipped the day before 
  * smen or pure butter 
  * salt and black pepper 
  * hard-boiled eggs (optional) 
  * water 



**Realization steps**

  1. In a couscous maker, prepare the sauce with the cut pieces of meat, the grated onion, salt, pepper, and the smen or butter. 
  2. Make return 2 to 3 min. Add 2 liters of water and the chickpeas. 
  3. As soon as the sauce begins to boil, go to the steam (top of the coucoussier) the trida a little oiled. 
  4. after 15 min, remove the trida, pour in a large bowl, sprinkle with a little sauce that is being cooked. 
  5. open the trida between your fingers, 
  6. still wear the evaporation trida. 
  7. Now add the minced meatballs and cook. 
  8. Place the trida in a large pot over low heat, sprinkle with sauce and stir until the trida absorbs the sauce well and is well cooked. 
  9. introduce the trida with halves of boiled eggs, and the meat. 
  10. present with a little cinnamon powder sprinkled on top 



[ ![trida constantinoise](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trida-constantinoise.jpg) ](<https://www.amourdecuisine.fr/article-trida-constantinoise-sauce-blanche.html/trida-constantinoise>)
