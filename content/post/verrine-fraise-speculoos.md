---
title: strawberry verrine speculoos
date: '2012-05-01'
categories:
- amuse bouche, tapas, mise en bouche
- Plats et recettes salees

---
![strawberry verrine speculoos](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x415/2/42/48/75/Images/)

Hello everybody, 

**strawberry verrine speculoos** , a **dessert** quickly, at the last minute, and here is a nice presentation to finish a delicious meal, that you can serve to your guests or your friends, a breeze, but certainly a delight. 

ingredients: 

  * **speculoos** in pieces 
  * a few **strawberries**
  * of the **fresh cream**
  * 1/2 pot of **yogurt**
  * 3 tablespoons of **Orange juice**
  * sugar according to taste 
  * **milk jam** for decoration 



method: 

  1. place the speculoos in pieces in the bottom of the verrines 
  2. soak orange juice 
  3. add the fresh cream in chantilly with the sugar, according to your taste 
  4. add the yogurt gently 
  5. cut the strawberries into pieces 
  6. sprinkle in the cups 
  7. with a pastry bag filled with cream 
  8. and redecorate with strawberries and cream 
  9. decorate with a scoop of milk jam 



![verrine strawberry and speculoos](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x416/2/42/48/75/Images/)

thank you for your comments and your passage 

thank you for staying faithful to the blog 

thank you to all who continue to subscribe 

have a good day 
