---
title: recipe for easy and delicious pancakes
date: '2013-08-18'
categories:
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crepes-aux-oranges-2_thumb.jpg
---
[ ![orange crepes 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crepes-aux-oranges-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crepes-aux-oranges-2_2.jpg>)

Hello everybody, 

my kids keep asking me to make them pancakes this week, and by this cold duck, well we enjoyed these delicious orange-flavored crepes after their return from school. 

**recipe for easy and delicious pancakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crepes-a-l-orange_thumb.jpg)

Recipe type:  Crepes  portions:  12  Prep time:  15 mins  cooking:  5 mins  total:  20 mins 

**Ingredients**

  * 2 eggs 
  * 35 g of sugar 
  * 150 g flour 
  * 400 ml of milk 
  * 50 ml of orange juice 
  * the zest of an orange 
  * 50 g of butter 
  * 2 tablespoons of oil 



**Realization steps**

  1. In a salad bowl put the eggs with the sugar 
  2. Add flour, orange peel, and butter to pieces 
  3. mix well and add the milk gradually   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crepes-a-l-orange-2_thumb_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crepes-a-l-orange-2_thumb_1.jpg>)
  4. then add the orange juice (if it's canned juice, reduce the sugar) 
  5. add oil, mix again. 
  6. cover with plastic wrap and let stand for 2 hours. 
  7. Cook the pancakes in an anti-adhesive pan. 



enjoy with a spread, such as [ spread with gingerbread ](<https://www.amourdecuisine.fr/article-pate-a-tartiner-au-pain-d-epice-98437586.html>) , or with [ applesauce ](<https://www.amourdecuisine.fr/article-compote-de-pomme-98542533.html>) , or bake a cake with these delicious pancakes 

a version of [ crepes natures ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee-114932092.html>) : 

[ ![crepes natures](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crepes-17.bmp_thumb.jpg) ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee-114932092.html>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
