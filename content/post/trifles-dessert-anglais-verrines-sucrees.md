---
title: trifles - english dessert - sweet verrines
date: '2011-11-14'
categories:
- appetizer, tapas, appetizer
- dips and sauces

---
Hello everybody, 

here is the dessert I had prepared last week, for the party at the school of Rayan, an English dessert, which can be adjusted to our tastes, and according to the means of board. 

with us, at home we like it a lot, because I try to make it as sweet as possible, and we eat it all the time, because that of the trade is really too sweet. 

so on your marks, the recipe ... .. 

Ingredients: 

  * of the sponge cake, for me it was vanilla cupcakes (recipe to come) 
  * fresh or canned fruit: pineapple, green grapes, apricots (for me) 
  * fruit jelly (I use the commercial one) 
  * of the [ custard ](<https://www.amourdecuisine.fr/article-creme-anglaise-custard-88689731.html>) (recipe to come in the next article) 
  * 150 ml thick cream 



Preparation: 

  1. In a deep round dish or verrines spread the fruit in pieces 
  2. then cover with biscuits (I do not water because the sponge cake will absorb the fruit juice) 
  3. then decorate the custard 
  4. then prepare the jelly with boiling water, and make a layer on the custard 
  5. With a whisk, put the thick cream in whipped cream, and decorate the verrines (I do not add sugar, but if you like to add according to your taste) 
  6. Cover your clover and decorate with fruits, or dried fruits 
  7. let cool until serving. 



je n’ai pas mis de quantite, car c’est selon votre besoin, et vos envies 
