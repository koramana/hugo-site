---
title: traditional tiramisu without alcohol
date: '2017-11-08'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cheesecakes and tiramisus
- idea, party recipe, aperitif aperitif
- sweet recipes
tags:
- Pastry
- Cakes
- Dessert
- desserts
- To taste
- Easy cooking
- Inratable cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/11/tiramisu-traditionnel-sans-alcool-4-2.jpg
---
![traditional tiramisu without alcohol 4-2](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/tiramisu-traditionnel-sans-alcool-4-2.jpg)

##  traditional tiramisu without alcohol 

Hello everybody, 

Although the traditional tiramisu is one of the favorite desserts at home, but I realized that I have never posted the recipe on my blog! It must be said, that even if we say that this dessert is better after at least 2 days cool, but with us this rule does not apply. 

For my children, do not wait, as soon as they see tiramisu in the fridge, not even they take permission, hihihihi. That's why I often prepare it at night when they are in bed, to make sure that they will enjoy it after their return from school, and all the same, I found a missing verrine, and I have not known until now who has tasted it or what time ??? 

**traditional tiramisu without alcohol**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/tiramisu-traditionnel-sans-alcool-5.jpg)

**Ingredients**

  * 3 big eggs 
  * 100 g of sugar 
  * 250 g of mascarpone (or 300 gr according to your box) 
  * 24 biscuits with spoon 
  * ½ liter of black coffee (sweetened slightly) 
  * 30 g bitter cocoa powder 



**Realization steps**

  1. Separate the egg whites from the yolks. 
  2. whisk the yolks with the sugar 
  3. add the mascarpone with a whisk. 
  4. Beat the egg whites and gently add them with the spatula to the previous mixture. 
  5. Prepare black coffee. Wet the cookies in the coffee. 
  6. Line the bottom of the mold with the cookies. Cover with a layer of cream, egg, sugar, mascarpone. 
  7. Alternate biscuits and cream. Finish with a layer of cream. Sprinkle with cocoa. 
  8. keep cool for at least 24h, enjoy! 



![traditional tiramisu without alcohol 6](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/tiramisu-traditionnel-sans-alcool-6.jpg)
