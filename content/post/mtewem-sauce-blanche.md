---
title: mtewem white sauce
date: '2017-03-25'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/mtewem-sauce-blanche-703x1024.jpg
---
[ ![mtewem white sauce](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/mtewem-sauce-blanche-703x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mtewem-sauce-blanche.jpg>)

##  mtewem white sauce 

Hello everybody, 

The white sauce mtewem is a recipe that I propose to you to realize and to see how this dish of the Algerian cuisine is delicious and rich in flavor. 

A dish of traditional Algerian cuisine that must imperatively be present at least once a week on the table of Ramadan ... Yes, yes, at home this dish is a must, you tell me after a long day of fasting, this dish is the best ideal for recharging well. 

The first time I posted this dish on my blog, I posted it with the photos of my friend L Chahira K. It was a pleasure for me to share his work on my blog. Today, I rest the dish with my photos, I took advantage of redoing new photos, I hope you will love them. 

{{< youtube mzeo_0Cfbn0 >}} 

**mtewem white sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/mtewem-sauce-blanche-1.jpg)

portions:  4  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients**

  * Nice pieces of leg of lamb. 
  * 500g minced meat 
  * An onion 
  * 2 cloves garlic 
  * Chickpeas 
  * Almonds 
  * Salt, pepper, cumin, 
  * 1 egg , 
  * 2 tbsp. cornflour 
  * A little smen and oil 



**Realization steps**

  1. Put the meat, grated onion, salt and pepper in a pot large enough to put our meatballs later. 
  2. simmer a little, then add water, chickpeas and almonds 
  3. cover and cook 
  4. Now prepare the meat stuffing, grate the cloves of garlic, a little salt, pepper, cumin and cahael, pick up the stuffing with an egg. 
  5. make small balls of medium meat and decorate with an almond in the middle. 
  6. Once the lamb meat is cooked, place the meat balls in the pot and let cook. 
  7. let the sauce reduce. And now, it's ready. 



![mtewem white sauce 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/mtewem-sauce-blanche-2.jpg)
