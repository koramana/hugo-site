---
title: leg of lamb with rosemary
date: '2012-12-02'
categories:
- gateaux d'anniversaires, de fetes, celebrations
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/epaule-d-agneau-au-four-009.CR2_thumb.jpg
---
Hello everybody, 

s I share with you the recipe for  leg of lamb roasted and grilled in the oven  , a delight ... .. a very easy way but that takes a lot of time when cooking. 

so plan 3 hours or more, depending on the size of the leg of lamb. 

and if you have some recipes from Aid El Adha that you want to share with us on this blog, send me the pictures on this email, as well as the recipe: 

vosessais@amourdecuisine.fr 

you can also see the recipe of: [ leg of lamb in the pressure cooker ](<https://www.amourdecuisine.fr/article-26570801.html>)

Ingredients: 

  *     * 1 leg of lamb of 2 kilos for me (not too big) 
    * some peeled rosemary 
    * salt, pepper and coriander. 
    * 6 to 7 cloves of garlic 
    * olive oil 
    * Butter 
    * water 



method of preparation: 

  1. Preheat the oven to 220 ° C. 
  2. In a baking dish, place your leg of lamb. Make small incisions in the leg. 
  3. insert in garlic cut on two, you can reduce the amount if you do not like garlic, in any case it's just for the flavor, because at the end of cooking garlic will be so melted, that you will not feel it. 
  4. add rosemary, and the pieces of butter in these small incisions. 
  5. season with salt and pepper and add the coriander powder. 
  6. Drizzle with olive oil. 
  7. add 1 half glass of water and put in the oven. 
  8. place a tray on the top floor of the oven, and put in a little water, to have a wet oven. 
  9. Put in the leg. After 15 minutes of cooking, lower the temperature to 180 ° C and cook for 3 hours. (not required because it depends on your oven) 
  10. every 15 minutes, turn the leg of lamb, and sprinkle with the cooking juice, and add 1 half glass if the sauce evaporates 
  11. also the tray in bs with water, do not let it dry, add water as and when. 
  12. if your leg starts to pick up color, you can cover it with aluminum foil for the last 30 minutes of cooking. 



knowing that my oven is a fan oven, so if your oven is gas, or the heat comes from below, put the water in a glass of water, instead of pouring it into the tray because it will dry faster. 

if the oven is too hot, you can lower the temperature up to 150 degrees., which I did, I prefer that the meat takes time to cook slowly, that to cook in 30 minutes has a heat too high it will burn, and give a dry and hard meat. 

shame for me it was photos taken at night, and I could not take pictures of the cut, but the leg was tender wish, mellow, delicious .... Your turn to try. 

rosemary is about taste and flavor, you can replace with bay leaves, or any other fine herbs that you like. 

and for what not the recipe of the [ Baked lamb shoulder ](<https://www.amourdecuisine.fr/article-epaule-d-agneau-rotie-au-four-113882735.html>) : 

[ ![Baked shoulder of lamb 009.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/epaule-d-agneau-au-four-009.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/article-epaule-d-agneau-rotie-au-four-113882735.html>)
