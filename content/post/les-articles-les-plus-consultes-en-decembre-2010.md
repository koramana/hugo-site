---
title: the most consulted articles in December 2010
date: '2011-02-01'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-d-epice-mercotte-018_thumb1.jpg
---
![https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-d-epice-mercotte-018_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-d-epice-mercotte-018_thumb1.jpg)

##  [ gingerbread very soft, and too good ](<https://www.amourdecuisine.fr/article-pain-d-epice-tres-moelleux-et-trop-bon-49778567.html> "gingerbread very soft, and too good")

![https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cornets-016_thumb21.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cornets-016_thumb21.jpg)

##  [ salted cones with tuna cream ](<https://www.amourdecuisine.fr/article-cornets-sales-a-la-creme-au-thon-49023773.html> "salted cones with tuna cream")

![https://www.amourdecuisine.fr/wp-content/uploads/2011/10/Cornes-de-Gazelle-bicolore-au-miel-056_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/Cornes-de-Gazelle-bicolore-au-miel-056_thumb1.jpg)

##  [ Two-colored gazelle horn pinches honey ](<https://www.amourdecuisine.fr/article-corne-de-gazelle-bicolore-pincee-au-miel-60151822.html> "Two-colored gazelle horn pinches honey")

![https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-miel-5_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-miel-5_thumb1.jpg)

##  [ arayeche with honey almond and coconut ](<https://www.amourdecuisine.fr/article-arayeche-au-miel-amande-et-noix-de-coco-61897731.html> "arayeche with honey almond and coconut")

![https://www.amourdecuisine.fr/wp-content/uploads/2012/08/makrout7_2_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/makrout7_2_thumb.jpg)

##  [ Baked Makrout, Makroud Algerian Cake ](<https://www.amourdecuisine.fr/article-26001222.html> "Baked Makrout Recipe, Makroud Algerian Cake")

![https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tiramisu-o-choco-004_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tiramisu-o-choco-004_thumb.jpg)

##  [ Chocolate Tiramisu ](<https://www.amourdecuisine.fr/article-tiramisu-au-chocolat-ou-ganache-chocolat-mascarpone-62045810.html> "Chocolate Tiramisu, or chocolate ganache / mascarpone")

![https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mhadjeb-069_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mhadjeb-069_thumb1.jpg)

##  [ algerian mhadjebs, stuffed msemens ](<https://www.amourdecuisine.fr/article-mhadjebs-algeriens-msemens-farcis-crepes-farcis-mahdjouba-62744653.html> "algerian mhadjebs, stuffed msemens, stuffed pancakes, mahdjouba")

![https://www.amourdecuisine.fr/wp-content/uploads/2012/07/muffins-au-chocolat-005_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/muffins-au-chocolat-005_thumb1.jpg)

##  [ Vanilla / Chocolate Muffins ](<https://www.amourdecuisine.fr/article-muffins-vanille-chocolat-61854062.html> "Vanilla / Chocolate Muffins")

![https://www.amourdecuisine.fr/wp-content/uploads/2011/08/halwat-tabaa-bel-djeldjlane_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/halwat-tabaa-bel-djeldjlane_thumb1.jpg)

##  [ Halwat tabaa bel jeljlane, shortbread with sesame seeds ](<https://www.amourdecuisine.fr/article-halwat-tabaa-bel-jeljlane-sables-aux-grains-de-sesames-62366278.html> "Halwat tabaa bel jeljlane, shortbread with sesame seeds")

![https://www.amourdecuisine.fr/wp-content/uploads/2012/06/focaccia-004_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/focaccia-004_thumb1.jpg)

##  [ Focaccia with rosemary and onion ](<https://www.amourdecuisine.fr/article-focaccia-au-romarin-et-oignon-62123009.html> "Focaccia with rosemary and onion")

![https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tripes-aux-legumes-2-_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tripes-aux-legumes-2-_thumb.jpg)

##  [ Tripes aux legumes ](<https://www.amourdecuisine.fr/article-tripes-aux-legumes-dowwara-bel-khodra-62337229.html> "Tripe with vegetables, Dowwara bel khodra")
