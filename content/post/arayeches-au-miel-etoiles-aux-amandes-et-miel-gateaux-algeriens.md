---
title: Arayeches with honey, Stars with almonds and honey Algerian cakes
date: '2014-07-25'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeches-au-miel-497_thumb1.jpg
---
##  Arayeches with honey, Stars with almonds and honey Algerian cakes 

Hello everybody, 

I like a lot the arayeches, and the arayeches with honey was a beautiful discovery for me, that I realized several times, without getting tired of it. 

My husband prefers arayeches to honey, rather than arayeches the glazing, because for him it's less sweet, both are beautiful, but it's true that these arayeches honey are too good. the pasta is very easy to make, but especially too, very very good. The recipe for the pasta is from the book: Oriental Cakes, Step by Step 

You can still have the recipe [ arayeches to the glacages ](<https://www.amourdecuisine.fr/article-25345469.html>) . 

عرايش معسل 

**Arayeches with honey, Stars with almonds and honey Algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeches-au-miel-497_thumb1.jpg)

portions:  50  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 250 gr of flour 
  * 75 gr of margarine 
  * 1 pinch of salt 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of white dye 
  * 1 cup of dye (color of your choice, here red, white and blue) 
  * Orange blossom water + water 

the joke: 
  * 250 gr of ground almonds 
  * 125 grams of sugar 
  * 1 C. a vanilla coffee 
  * 1 egg 

garnish: 
  * honey 
  * colorful pearl 



**Realization steps**

  1. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  2. Mix well with your fingertips 
  3. divide the dough into two parts 
  4. for the one you are going to color in white add the dye, then pick up the dough with flower water, add a little water to pick up, continue to knead a few seconds. 
  5. for the other color, take the other half of the shortbread, add the dyes to the orange water until the desired color is obtained, and start to collect the dough with this water, add the water if you need, and knead a few seconds.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-au-miel-1_thumb1-300x243.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-au-miel-1_thumb1-300x243.jpg>)
  6. cover your dough and let it sit on the side. 
  7. for stuffing mix almonds and dry ingredients, collect with egg and set aside. 
  8. lower the dough and pass it to the machine from number 3 to number 7, cut two circles of dough, the first of the dark color with a cookie cutter 8 cm in diameter. the second of the white color with a 9 cm diameter cookie cutter. 
  9. then superimpose them, the big one on the small   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-au-miel-1_thumb1.jpg)
  10. take a ball of 25 grams of the almond stuffing, form a star, place it on the circle of the dough, and pick up the sides of the dough to coat the almond stuffing, without covering it completely, press well so that the dough is well stick to the stuffing, and form well the ends of the star.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-au-miel-2_thumb1.jpg)
  11. decorate with small flowers 
  12. and cook for 10 minutes in a preheated oven at 170 degrees C. 
  13. take it out of the oven, let cool a little, and emerge your cakes in a little hot honey. 
  14. now decorate the small flowers with pearls. 
  15. enjoy. 



le modèle de ce gâteau est tirer du livre :Gâteaux et décoration 1 de madame Lila Bounazo. 
