---
title: gratin of cauliflower and potatoes
date: '2017-06-30'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
tags:
- Full Dish
- Ramadan 2015
- Ramadan
- Algeria
- dishes
- Chicken
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gratin-de-chou-fleur-et-pomme-de-terre.CR2_.jpg
---
[ ![gratin of cauliflower and potato.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gratin-de-chou-fleur-et-pomme-de-terre.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gratin-de-chou-fleur-et-pomme-de-terre.CR2_.jpg>)

##  gratin of cauliflower and potatoes 

Hello everybody, 

At dinner today, this delicious gratin of cauliflower and potatoes. A recipe I saw on my facebook band, and I was just waiting for the opportunity to make it happen. Miraculously, nothing remains, because everyone has food without saying a word, hihihih. 

I hope that this gratin based potatoes and cauliflower will please you, I must just mention that it is a very rich dish, because there is bechamel sauce, there are eggs, there is has some form, and there is the cooking oil ... Yes, very rich, but that children have to eat without making the slightest claim. It must also be said that we do not eat gratins every day, I program a gratin a month, count lasagna ... Because it's super rich dishes.   


**gratin of cauliflower and potatoes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gratin-de-chou-fleur-et-pommes-de-terre.CR2_.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 1 cauliflower of medium size 
  * 4 medium potatoes 
  * a [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html>)
  * 3 eggs 
  * chopped parsley 
  * Gruyere 



**Realization steps**

  1. start by preparing the bechamel sauce to have time to cool. 
  2. clean the cauliflower and cut the rosette not too big 
  3. boil in salted water with a little vinegar, the cauliflower should not be overcooked. 
  4. let it drain in a Chinese. 
  5. peel the potatoes and cut them into slices of almost 8 mm in height 
  6. salt a little and fry to give a golden color to the washers 
  7. let drain on paper towels. 
  8. fry the cauliflower rosettes in a hot oil and let drain on paper towels. 
  9. take a gratin baking tin of almost 22 x 22 cm, butter barely 
  10. cover it with the slices of potatoes. 
  11. remove cauliflower rosettes all over the surface 
  12. beat the eggs, add the chopped parsley, and the béchamel sauce 
  13. pour this mixture on the cauliflower and potatoes. 
  14. garnish with grated cheese. 
  15. put in a preheated oven at 180 degrees C, it should not take more than 15 minutes, because everything is already cooked. 
  16. turn on the oven from above to give a nice color to your gratin. 
  17. and enjoy. 


