---
title: Alsatian kouglof recipe
date: '2018-02-23'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-de-kouglof-alsacien.jpg
---
![Alsatian kouglof recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-de-kouglof-alsacien.jpg)

##  Alsatian kouglof recipe 

Hello everybody, 

You want a successful recipe of Alsatian kouglouf? What is good the **kouglof** this **Alsatian brioche** well stuffed with melting raisins, and this crunch of almonds at the top of the piece ... 

I admit that I often missed **Alsatian kouglof** but I understood from these mishaps, that my fault was to be impatient, and to want to quickly cook my brioche, without giving him enough time for the first lift and again the second thrown in the mold. 

So never hesitate to let your dough rise well in the mold, whatever the brioche that you will achieve. A very important point to have the good taste in a brioche, is the amount of salt. Sometimes when we taste a brioche, we think that it lacks sugar, when we have this bland taste, but in fact it is the lack of salt that gives you that feeling, we must find the right balance, the right amount salt and surely a pinch of salt is not the solution, hihihihi. 

![Alsatian kouglof recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-de-kouglof-alsacien-2.jpg)

So, as I told you, when I got home from school, I degassed the dough and fashioned both **kugelhopf** , I put them in the mold, and covered with a cloth, and put in front of the radiator, I took my son to football, when I came back 2 and a half after, the dough did not really lift (I I said it's gone), then, I put my molds on a board, on the radiator, and leave for another 2 hours (my radiators are not too hot, so do not put them, on your who are maybe hotter). 

Or if you are in a hurry, heat the oven for 5 minutes, turn it off, and place your mussels in it, covered with the cloth. 

{{< youtube UsiJw3efFbo >}} 

**Alsatian kouglof recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-de-kouglof-alsacien-1.jpg)

portions:  12  cooking:  30 mins  total:  30 mins 

**Ingredients**

  * 500 gr of flour 
  * 250 ml of milk, 
  * 100 g of sugar 
  * 1 sachet of instant yeast, 1 tablespoon. 
  * ¼ c. salt 
  * 2 whole eggs 
  * 2 egg yolk 
  * 150 gr of butter at room temperature, 
  * 150 gr of raisins, 
  * butter for the mold. 
  * 24 toasted almonds, or flaked almonds. 
  * icing sugar to sprinkle. 



**Realization steps**

  1. place the raisins in boiling water to inflate. 
  2. place the flour, sugar, salt, yeast in the bowl of the mix, mix together. 
  3. add the egg yolks and the whole eggs and knead. 
  4. now add the warm milk while kneading until you have a dough well sticky. 
  5. knead again at least 10 minutes to have a nice elastic dough. 
  6. now introduce the butter at room temperature in a small amount while kneading. 
  7. knead again for at least 10 minutes. 
  8. let the dough rest and doubled in size, between 45 min and 60 min depending on the temperature of the room. 
  9. take the dough again and degas it on a very lightly floured work surface while slowly adding the amount of raisins. 
  10. butter generously your kouglof mold, and decorate with the almonds. 
  11. form a large donut with your dough and pour into the kouglof mold. 
  12. let your dough rest all the time necessary, and according to the temperature of the room, until the double pate see triple volume, do not hurry especially, if you want buns well ventilated. 
  13. Bake in the preheated oven at 180 ° C and cook for 30 to 35 minutes. or more if your mold is in the ground and depending on the capacity of your oven. 



![recipe of Alsatian kouglof 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-de-kouglof-alsacien-3.jpg)
