---
title: non-bitter orange marmalade
date: '2017-09-19'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/marmelade-d-orange-non-amere-019.CR2_1.jpg
---
![marmalade-and-orange-not-bitter-019.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/marmelade-d-orange-non-amere-019.CR2_1.jpg)

##  non-bitter orange marmalade 

Hello everybody, 

I did not know that we could make an orange marmalade and that it is "not bitter", but when I went to Constantine "my hometown" my aunt presented us with a nice orange jam for breakfast, at first I did not want to touch, because I remembered bitter marmalade ... 

But I was watching others around me, who was eating this spoon jam ???? 

I was tempted, and I plunged the head of the spoon, just to have the smallest piece of orange possible (to feel less bitterness) and surprise, the jam was just a delight, really to fall, not too sweet, we manage to chew the skin of the orange, which lets flow the honey well juice in the mouth, every time you chew ... 

the best thing about all this is that it's not bitter at all ... the surprise ... it is by far the best jam of orange in pieces and with the skin that I could eat of my life. 

Of course I asked for the recipe, but my aunt who prepared it was not at home, and my other aunt, explained the recipe vaguely, the only thing I catch in mind is that she removed the zest from the oranges delicately, not entirely, and that she cut into a cube, and then she steamed, the rest .... I did not pick anything because my aunt and my cousin were talking at the same time and contradicting each other ... I left without the recipe ... sniff sniff. 

![marmalade-and-orange-not-bitter-013.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/marmelade-d-orange-non-amere-013.CR2_1.jpg)

Back home, I did several tests, which of course failed, until I changed the method a little, and I got the most delicious jam, or marmalade of oranges .... 

so follow me very slowly, because I will not give you the ingredients for grammages .... just follow me.   


**non-bitter orange marmalade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/marmelade-d-orange-non-amere-009.CR2_1.jpg)

Recipe type:  preserve  portions:  8  Prep time:  120 mins  cooking:  45 mins  total:  2 hours 45 mins 

**Ingredients** ingredients: 

  * oranges 
  * sugar 
  * water 

utensils: 
  * couscous 
  * wooden spatula 
  * jar for jam 
  * zester 
  * well-peeled knife. 



**Realization steps**

  1. a day before cooking the jam, gently remove the zest from the oranges (look on the photo, no need to go until you only have white skin) 
  2. In a large pot, put hot water, add 1 tablespoon of sugar, and put the oranges, leave the oranges in this water for almost 30 minutes, and change the water. 
  3. the more you repeat this process: hot water + sugar, the more you reduce the bitterness of the skin of oranges. (I do not remember how many times I did that, but surely more than 8 times. 
  4. in the morning, remove the oranges from the water, and on the work surface, cut the oranges into cubes. 
  5. place boiling water in the bottom of the couscous pot, and place the cubes of oranges in the top of the couscous pot. 
  6. Steam the oranges for about 20 minutes. 
  7. change the water from the bottom of couscoussier and pass once again the oranges to the steam. 
  8. when the oranges are tender, remove and keep the water from the bottom of the couscoussier. 
  9. now measure the amount of oranges. 
  10. measure the same amount of water (the one that escaped during evaporation) 
  11. measure three quarters of this quantity in sugar, that is to say for example: 1 glasses of oranges, for a glass of water of steam, for ¾ glass of sugar. 
  12. place the water and the sugar in a pot with a thick bottom, and let the sugar take on a high heat. 
  13. when you are going to have a viscous syrup, which begins to approach the texture of a thick honey, add the pieces of oranges in it. 
  14. lower the heat, and let it boil, until the jam is ready, to check: with a spoon, pour some jam syrup on a cold plate, if the drop freezes on cooling, the jam is ready 
  15. Fill the pots, with the boiling jam using a ladle; close and return them. Leave them upside down until the next day. 



![2013-04-27-marmalade-and-orange-not amere.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/2013-04-27-marmelade-d-orange-non-amere1.jpg)
