---
title: refined apple pie
date: '2014-03-03'
categories:
- Moroccan cuisine
- Cuisine by country
- dessert, crumbles and bars
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tarte-aux-pommes-raffin%C3%A9e-3.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tarte-aux-pommes-raffin%C3%A9e-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tarte-aux-pommes-raffin%C3%A9e-3.jpg>)

Hello everybody, 

Here is a delicious apple pie refined that shares with us Lunetoiles, freshly just out of the oven, this pie is consumed rather warm, if you want to enjoy all these flavors, and this soft infinite ... 

A pie that Lunetoiles found on one of those books she bought at Lidl .... and she is not disappointed with the result. 

What I like about apples is that we always have them at home, so we always have the opportunity to make a recipe with this very rich fruit, whether in taste, or vitamins .... 

personally I like apples like that, in a recipe especially if it is with cinnamon ....   


**refined apple pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tarte-aux-pommes-raffin%C3%A9e-1.jpg)

Recipe type:  Dessert  portions:  12  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 100 g of butter 
  * 250 g of sugar 
  * 2 bags of vanilla sugar 
  * 2 eggs 
  * 100g of flour 
  * 2 tbsp. coffee baking powder 
  * 1 pinch of salt 
  * 100 ml of milk 
  * 900 g of apples   
Butter for the mold 



**Realization steps**

  1. Melt butter in a pan. 
  2. Beat the sugar with vanilla sugar, eggs and butter until the mixture is frothy. 
  3. Mix the flour with the yeast, add it with the salt and the milk to the previous preparation. 
  4. Peel, cut apples into wedges and seed. 
  5. Cut the apple quarters into thin slices and incorporate them into the dough. 
  6. Preheat the oven to 190 ° C (if it is hot to 170 ° C) 
  7. Grease a pie dish (28 cm in diameter) and spread the apple paste. 
  8. Bake for about 45 minutes. 
  9. If the top browns too quickly, possibly cover with parchment paper after 30 minutes of cooking. 
  10. This pie is particularly delicious served lukewarm. 



Nutrition Information kcal:  138 kcal  [ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tarte-aux-pommes-raffin%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tarte-aux-pommes-raffin%C3%A9e.jpg>)

I hope the recipe will please you. Thank you Lunetoiles for these beautiful photos ... 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
