---
title: Mchewek with sesame grains economic Algerian cake in video
date: '2016-06-10'
categories:
- Algerian cakes- oriental- modern- fete aid
tags:
- Ramadan 2016
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Mchewek-aux-grains-de-sesames.CR2_2.jpg
---
![Mchewek-to-grain-of-sesames.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Mchewek-aux-grains-de-sesames.CR2_2.jpg)

##  Mchewek with sesame grains economic Algerian cake in video 

Hello everybody, 

This delicious mchewek with sesame seeds is not only a beautiful and presentable cake, it is also a very good cake, very easy to make, and especially economical, with the few ingredients you will put in it. 

I made you a detailed video to make this cake ... 

The recipe in Arabic: 

In any case, then what the sesame seeds are in the spotlight on this recipe, I invite you if you have not done yet to come to participate in our little game between bloggers (even non-bloggers are invited): [ Recipes around an ingredient # 19: sesame seeds ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient19-grains-de-sesames.html>) in all their states (grains, oil, tahini ...). I am the godmother of this round, go on the article to know more.   


**Mchewek Sesame Grain / Algerian Cake Economic Video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Mchewek-aux-grains-de-sesames.CR2_2.jpg)

Recipe type:  Algerian cake  portions:  50  Prep time:  20 mins  cooking:  12 mins  total:  32 mins 

**Ingredients**

  * 460 gr roasted and ground peanuts 
  * 180 gr biscuits (kind of tea, small butter ...) ground 
  * 150 gr of powdered sugar 
  * 1 C. coffee lemon zest 
  * 1 packet of dry yeast 
  * 1 tbsp. powdered vanilla 
  * 100 gr of melted butter 
  * 2 eggs 
  * egg whites (between 3 or 4) 

To decorate : 
  * sesame seeds 
  * candied cherries 
  * honey with orange blossom water 



**Realization steps**

  1. Put all the ingredients in a large salad bowl. 
  2. Mix everything, then add the melted butter and mix well together. 
  3. And now start incorporating 1 egg, and after two egg whites, and one more egg and one or two egg whites, until you have the consistency of a beautiful dough. 
  4. Let the dough rest, and form small balls of dough, until the dough is used up. 
  5. When you have formed all the little balls of pasta, roll them in the sesame seeds and decorate with a candied cherry. 
  6. Place them in boxes. 
  7. Bake in preheat oven at 180 ° C for 10 to 12 minutes. 
  8. At the end of the oven, sprinkle each cakes with honey and orange blossom water. 



I saw this recipe on an Algerian forum .... 
