---
title: pastry custard without dough, Parisian flan
date: '2017-10-11'
categories:
- cakes and cakes
tags:
- Custard
- desserts
- eggs
- Milk
- Algerian cakes
- To taste
- Egg flan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-patissier-sans-pate-025.CR2_thumb.jpg
---
##  pastry custard without dough, Parisian flan 

Hello everybody, 

I always realized the [ pastry custard with dough ](<https://www.amourdecuisine.fr/article-flan-patissier-100019503.html>) I was afraid that the flan sticks, and that I will find it difficult to cut the shares. 

but I really wanted to test the recipe of pastry pie without dough, besides it is easier ... and faster, because no need to prepare a dough, all that will be needed and prepare a beautiful pastry cream, place it in a floured pastry ring, put on a baking sheet, covered with baking paper (parchment paper), bake the cake ... and here we go, our cake is ready, 

we let cool the time it takes, and we enjoy .... 

the recipe I found it on the blog of Christophe Michalak, and it was a real pleasure to prepare it. 

You can see here the video of the pastry custard with a base of shortbread: 

**pastry custard without dough, Parisian flan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-patissier-sans-pate-025.CR2_thumb.jpg)

Recipe type:  flan  portions:  12  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 750 g of semi-skimmed milk 
  * 2 vanilla pods 
  * 2 combavas (replace with 2 lemons) 
  * 185 gr of cream 
  * 185 gr of sugar 
  * 150 gr of egg yolks (about 8 yolks) 
  * 75 gr of cornstarch 
  * 90 gr of orange blossom 



**Realization steps**

  1. Take the zest of 2 lemons 
  2. Boil milk and cream with vanilla and zest of lemons, in a saucepan 
  3. Let infuse 15 min 
  4. Whisk together sugar, egg yolks and cornstarch 
  5. Add the infused milk to the mixture and mix well. 
  6. Put back in the saucepan and bring everything to a boil without stopping mixing until the cream thickens.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-parisien.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-parisien.CR2_thumb1.jpg>)
  7. Out of the fire, add the orange blossom and leave to soften about 5 minutes in the freezer 
  8. Blend the cooled cream with a blender 
  9. Butter generously the pastry ring and flour it. place it on a baking sheet lined with parchment paper 
  10. Whip the cream (warm) again with a whisk before pouring it into the dish 
  11. Bake at 180 ° C for 30 minutes. 



[ ![blank-pastry-in-pate-025.CR2_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-patissier-sans-pate-025.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-patissier-sans-pate-025.CR2_thumb.jpg>)
