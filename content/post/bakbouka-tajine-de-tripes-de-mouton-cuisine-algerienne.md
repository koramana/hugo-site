---
title: bakbouka tajine of sheep guts-Algerian cuisine
date: '2014-09-27'
categories:
- Algerian cuisine
- Dishes and salty recipes
tags:
- Morocco
- Tunisia
- Aid
- giblets
- Lamb
- sauces
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tripes-aux-legumes-2-_thumb.jpg
---
# 

[ ![tripe with vegetables \(2\)](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tripes-aux-legumes-2-_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tripes-aux-legumes-2-.jpg>)

##  Bakbouka tajine of sheep guts-Algerian cuisine 

Hello everybody, 

A recipe that comes directly from Algeria, from my mother's kitchen, hummmmmmmm. You will tell me: and why did not you do it at home? I will tell you: I really like this dish of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , a tasty dish very rich in taste and vitamin. But here in England, or rather in Bristol, it is impossible to find a butcher who sells the tripe complete with the intestines, and to be honest, in this dish, I really like the guts ... 

finally, I share with you the photos and the recipe of my mother:   
for this dish, you need:   


**bakbouka - Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tripes-aux-legumes-2-_thumb.jpg)

Recipe type:  hand  portions:  4  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * 1 kg of tripe (fat-double, intestines, belly ...) 
  * 1 handful of chickpeas soaked the day before 
  * 4 or 5 carrots 
  * 3 zucchini 
  * 2 tablespoons of oil 
  * 1 tbsp canned tomato soup 
  * 1 onion 
  * 2 cloves garlic 
  * a chopped coriander boot 
  * paprika, black pepper, salt 



**Realization steps**

  1. Clean the tripe, cut into small pieces (about 2 to 3 cm around) 
  2. put them in a done everything, or in a casserole of preferable. 
  3. Add the oil and chopped onion in a thin piece and crushed garlic. 
  4. return to a slow fire. 
  5. Add black pepper, paprika and salt, mix, and leave a little on low heat. 
  6. then cover with water, close the casserole and let it cook. 
  7. in the meantime, scrape and cut the carrots out of two, as well as the zucchini. 
  8. half way through the tripe, immerse the vegetables and the chickpeas, add the tomato concentrate and let it cook over a low heat, while checking the water during the cooking, until the grout is smooth. decorate with coriander. 
  9. and enjoy hot, you can throw a pepper to enhance the taste. 



[ ![tripe with vegetables](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tripes-aux-legumes_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tripes-aux-legumes.jpg>)

merci pour vos commentaires 
