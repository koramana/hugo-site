---
title: genoise with pineapple
date: '2015-10-02'
categories:
- cakes and cakes
- sweet recipes
- pies and tarts
tags:
- Soft pie
- Light desserts
- desserts
- Easy recipe
- Fast Recipe
- tart

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/genoise-a-lananas.jpg
---
[ ![genoise with pineapple](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/genoise-a-lananas.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/genoise-a-lananas.jpg>)

##  genoise with pineapple 

Hello everybody, 

Today I give way to the recipes of my readers who use the link: Submit your recipe, to share their delights with us. 

And it's a recipe for **Samia Z** , she already share these delights on my blog, thank you my dear. Today, it shows us that it is super easy to achieve a sweetness for the taste of children, without too much to take the lead ... A beautiful sponge cake, a delicious fruit in a box, and a beautiful whipped chantilly and voila . So you're ready for these pineapple sponge cake, tartlets that will make you happy?   
You can see in the same principle: my [ strawberry and mascarpone genoises ](<https://www.amourdecuisine.fr/article-tartelettes-fraises-mascarpone.html>)   
  


**genoise with pineapple**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/genoise-a-lananas-1.jpg)

portions:  6  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients** For 6 molds about 10.5 in diameter and 2 cm in height: 

  * 3 eggs 
  * 90 g sugar 
  * 90 g flour 
  * a tiny pinch of salt 
  * a tiny pinch of baking powder to mix with the flour sift 

decoration: 
  * Pineapple in box 
  * chantilly. 



**Realization steps**

  1. beat the eggs, sugar and pinch of salt using a mixer until the mixture doubles in volume and must be thick. 
  2. add the flour little by little by introducing it gently without lumps. 
  3. fill your small molds genoise, super well buttered and floured up to ¾ the height of the mold. 
  4. cook in a preheated oven at 180 degrees C, the cooking will be super rapude in these small mussels, so watch (10 minutes and it can already be cooked) 
  5. unmold the cakes at the oven exit, and let cool on a rack. 
  6. when the Genoese are well chilled, soak them with pineapple juice 
  7. you can decorate with a slice of pineapple and a beautiful flower in whipped cream, or according to your taste. 



Note for this quantity, I get 7 genoises ... not 6 .. hihihih [ ![genoise with pineapple 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/genoise-a-lananas-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/genoise-a-lananas-2.jpg>)   

