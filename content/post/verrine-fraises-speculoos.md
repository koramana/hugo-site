---
title: verrine strawberries speculoos
date: '2011-05-06'
categories:
- Café amour de cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/05/verrine-fraise-011a.jpg
---
![jar strawberry-011a.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/verrine-fraise-011a.jpg) Hello everybody, 

photos that drag on my hard drive for a while, the ... .. a very simple recipe, that I did not want to post, but I said to myself, the blog, is not only to make sofestiquee recipes and who take all the day to do. 

so a super easy dessert that is done in the blink of an eye, and here ....    


**verrine strawberries speculoos**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/verrine-fraise-011a.jpg)

Recipe type:  Dessert  portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * speculoos in pieces 
  * some strawberries 
  * fresh cream 
  * ½ jar of yoghurt 
  * 3 tablespoons of orange juice 
  * sugar according to taste 
  * jam milk for decoration 



**Realization steps**

  1. place the speculoos in pieces in the bottom of the verrines 
  2. soak orange juice 
  3. add the fresh cream in chantilly with the sugar, according to your taste 
  4. add the yogurt gently 
  5. cut the strawberries into pieces 
  6. sprinkle in the cups 
  7. with a pastry bag filled with cream 
  8. and redecorate with strawberries and cream 
  9. decorate with a scoop of milk jam 



![jar-strawberry-008-a.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/verrine-fraise-008-a.jpg)

thank you for your comments and your passage 

thank you for staying faithful to the blog 

thank you to all who continue to subscribe 

bonne journee 
