---
title: el machkouk, mechakek, pistachio crackles
date: '2011-08-25'
categories:
- dessert, crumbles et barres
- Gateaux au chocolat
- cakes and cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/gateaux-algeriens-el-machkouk-1.jpg
---
##  el machkouk, mechakek, pistachio crackles 

###  el machkouk, mechakek, lmachkouk المشقوق 

Hello everybody, 

another Algerian delight, an Algerian almond-based cake is the simple version of Algerian cakes " [ the mchawek ](<https://www.amourdecuisine.fr/article-25345484.html>) "Except that instead of being enrobed in flaked almonds, these cakes are coated in icing sugar. 

an Algerian cake of the simplest, and for average budgets, we can replace the almonds with peanuts and it gives a cake .... huuuuuuuuuuuum 

just for info and for people who do not understand Arabic: el machkouk, means cracked cakes 

I will not be too late to talk to you about everything [ delicious Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) but do not forget to check my categories: 

####  [ oriental Algerian cakes from Aid fete ](<https://www.amourdecuisine.fr/categorie-10678931.html>)

####  [ Dry cakes, petits fours ](<https://www.amourdecuisine.fr/categorie-11814728.html>)

[ Algerian cake without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>)

to find even more Algerian cakes. 

the ingredients for almost 50 pieces: 

  * 500g ground almonds 
  * 300 g sifted icing sugar 
  * 3 to 4 egg whites (depending on the size) 
  * red and blue dye 
  * strawberry aroma 
  * pistachio aroma 
  * icing sugar to coat 
  * whole almonds for decoration 



![algerian cake el mechkouk](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/gateaux-algeriens-el-machkouk-1.jpg)

  1. In a large bowl, mix almonds and sifted icing sugar. 
  2. Wet with egg whites until you get a fairly soft dough, one egg white at a time, and do not put too much, otherwise your cakes will not be well cracks 
  3. cut the dough in 2 
  4. If you choose the pink color, put the red dye with the aroma corresponding to the color: strawberry 
  5. blue dye (it gives green mchekaks) pistachio aroma 
  6. Make balls the size of an egg yolk, 
  7. roll them in sifted icing sugar, 
  8. decorate each ball with a whole almond. 
  9. Arrange the cakes on a plate covered with baking or floured paper 
  10. Make leather in a preheated oven at 180 ° C for about 15 minutes, place the plate in the middle of the oven. 
  11. Put cooled cakes in small boxes. 



thank you for your visits and comments 

thank you to all those who continue to subscribe to my newsletter, yesterday at midnight I was a 2010 subscriber to the newsletter, it makes me very hot in the heart, because it means that my blog is well appreciated, do not forget to check at least one of the boxes, otherwise your registration will not be taken into account 

thank you also for the people of gmail, for their +1, and thank you for "I like" facebook 

good day and saha ramdanekoum   


**el machkouk, mechakek, pistachio crackles**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/gateau-algerien-el-machkouk_thumb.jpg)

portions:  50  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 500g ground almonds 
  * 300 g sifted icing sugar 
  * 3 to 4 egg whites (depending on the size) 
  * red and blue dye 
  * strawberry aroma 
  * pistachio aroma 
  * icing sugar to coat 
  * whole almonds for decoration 
  * algerian cake el mechkouk 



**Realization steps**

  1. In a large bowl, mix almonds and sifted icing sugar. 
  2. Wet with egg whites until you get a fairly soft dough, one egg white at a time, and do not put too much, otherwise your cakes will not be well cracks 
  3. cut the dough in 2 
  4. If you choose the pink color, put the red dye with the aroma corresponding to the color: strawberry 
  5. blue dye (it gives green mchekaks) pistachio aroma 
  6. Make balls the size of an egg yolk, 
  7. roll them in sifted icing sugar, 
  8. decorate each ball with a whole almond. 
  9. Arrange the cakes on a plate covered with baking or floured paper 
  10. Make leather in a preheated oven at 180 ° C for about 15 minutes, place the plate in the middle of the oven. 
  11. Put cooled cakes in small boxes. 


