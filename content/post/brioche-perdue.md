---
title: brioche lost
date: '2016-07-08'
categories:
- Brioches et viennoiseries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/brioche-perdue-012.CR2_1.jpg
---
![brioche-lost-012.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/brioche-perdue-012.CR2_1.jpg)

##  brioche lost 

Hello everybody, 

a recipe that rocked my childhood, I remember when I woke up in the morning, with that smell of that butter that sang in the pan by welcoming these pieces of brioche soaked in milk perfumed with orange blossom ... 

yes it's the recipe for the lost brioche, huuuuuuuuum ... 

so my little story with this recipe, and when I was going to empty my freezer, and I found these slices of [ Ceramic ](<https://www.amourdecuisine.fr/article-le-cramique-brioche-belge-115413567.html>) , that were lost among the little bags of vegetables, and boxes of ice cream ... 

I was going to take these slices of brioche as they were, maybe with a little jam, at breakfast, then I said, no no, I'm going to make a brioche lost ... .. 

ingredients: 

  * 4 to 6 slices of brioche (for me it was the ceramic) 
  * 50 gr of sugar 
  * 1/2 liter of milk 
  * 1/2 teaspoons of vanilla 
  * 2 eggs 
  * Butter 



for garnish: 

  * a few pieces of peaches in a box 
  * 1 tablespoon of honey 
  * 1 tablespoon of sugar 
  * 1 tablespoon of water 



![brioche-lost-019.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/brioche-perdue-019.CR2_1.jpg)

method of preparation: 

  1. in a bowl, mix the milk, sugar, vanilla, and orange blossom water until the sugar dissolves completely. 
  2. in another bowl, beat the eggs in omelette. 
  3. go to the peaches to caramelize them: heat the honey, the sugar and the water in a pan. Caramelize the peaches for 2 to 3 minutes on each side. 
  4. start frying the buns, dip one by one the pieces of brioche in the milk, go slowly, because each piece is going to be well soaked, and risk of crumbling. take the first piece, dip it into the flavored milk mixture, the first side, then the second. 
  5. then dive into the egg omelette on both sides. 
  6. melt a tablespoon of butter in the pan, and dip into it the piece of brioche, 
  7. leave on one side, then turn gently to brown on the other side. 
  8. place the lost brioche on a serving dish, and garnish with the caramelized peach pieces, you can also decorate with a little flaked almonds, and chocolate sauce. 



![brioche-lost-010.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/brioche-perdue-010.CR2_1.jpg)

**brioche lost**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/brioche-perdue-019.CR2_.jpg)

**Ingredients**

  * 4 to 6 slices of brioche (for me it was the ceramic) 
  * 50 gr of sugar 
  * ½ liter of milk 
  * ½ cup of vanilla coffee 
  * 2 eggs 
  * Butter 

for garnish: 
  * a few pieces of peaches in a box 
  * 1 tablespoon of honey 
  * 1 tablespoon of sugar 
  * 1 tablespoon of water 



**Realization steps**

  1. in a bowl, mix the milk, sugar, vanilla, and orange blossom water until the sugar dissolves completely. 
  2. in another bowl, beat the eggs in omelette. 
  3. go to the peaches to caramelize them: heat the honey, the sugar and the water in a pan. Caramelize the peaches for 2 to 3 minutes on each side. 
  4. start frying the buns, dip one by one the pieces of brioche in the milk, go slowly, because each piece is going to be well soaked, and risk of crumbling. take the first piece, dip it into the flavored milk mixture, the first side, then the second. 
  5. then dive into the egg omelette on both sides. 
  6. melt a tablespoon of butter in the pan, and dip into it the piece of brioche, 
  7. leave on one side, then turn gently to brown on the other side. 
  8. place the lost brioche on a serving dish, and garnish with the caramelized peach pieces, you can also decorate with a little flaked almonds, and chocolate sauce. 


