---
title: Cake with orange
date: '2016-11-25'
categories:
- gateaux, et cakes
- idea, party recipe, aperitif aperitif
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-d-orange-015.CR2_thumb1.jpg
---
##  Cake with orange 

Hello everybody, 

here is a super delicious and mellow Orange Cake I often like realized, as I told you recently, following the desire that gave me Lunetoiles, after seeing the pictures of all these delicious cakes, that she sent me and I had the chance to share with you through the lines of this blog, I could sometimes, have the feeling to smell the smell of these cakes coming out of the screen of my computer ... 

So I share with you, this recipe for Cake has orange, that I changed a little compared to the original recipe of Lunetoiles. 

I made this recipe twice, but my first try was a little raplapla, because I used a large mold, the orange cake was perfect in taste and cooking (I found it a little sweet, so I then decreased the sugar) also during my second realization, I lacked milk (when we have two little monsters who prefer to drink milk instead of water, this is the result ..) 

![orange cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-d-orange-015.CR2_thumb1.jpg)

**Cake with orange**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-a-l-orange-010.CR2_thumb1.jpg)

portions:  12  Prep time:  15 mins  cooking:  70 mins  total:  1 hour 25 mins 

**Ingredients** for a mold of 20 x 10 cm 

  * 250g of flour 
  * 150 g of sugar (135 g for me) 
  * 125 g of butter (or oil, I put oil) 
  * ½ glass of milk (120 ml, I put 60 ml) 
  * 1 sachet of baking powder 
  * 1 sachet of vanilla sugar 
  * 2 big eggs (I put 3 to replace the missing milk) 
  * Zest of a big orange 
  * 1 pinch of salt 

icing: (I did not do the frosting, because as I told you, in my opinion it was very sweet) 
  * orange juice + ZESTE (for the decoration) 
  * orange dye 
  * icing sugar 



**Realization steps**

  1. Preheat the oven to 180 ° C (th.6). 
  2. Butter a cake tin or lined with parchment paper. 
  3. In a terrine, break the eggs; add the sugars and beat well together. Add 1 pinch of salt, then gradually pour in the flour, yeast and milk. 
  4. When the mixture is homogeneous, always beat for just melted butter. Add the zest of the orange. 
  5. Mix with a spatula and pour into the mold. 
  6. Bake at 180 ° C (th.6), for 1h to 1h15 (see less, depending on your oven). 
  7. Check the cooking with the tip of a knife, it must come out dry and clean. 
  8. At the exit of the oven, turn out, remove the paper if necessary and let cool on a rack. 
  9. For frosting, gradually mix a little orange juice with icing sugar, until you have a thick consistency, add the orange dye, mix well. 
  10. Pour on the cake, previously placed on a rack. Decorate with orange zest. 
  11. Let dry completely. 


