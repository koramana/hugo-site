---
title: Chicken tajine with potatoes
date: '2016-07-12'
categories:
- cuisine algerienne
- Moroccan cuisine
tags:
- dishes
- Algeria
- Easy cooking
- Ramadan 2016
- inputs
- Ramadan
- Morocco

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/tajine-de-poulet-a-la-pomme-de-terre.jpg
---
![chicken tajine with potatoes](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/tajine-de-poulet-a-la-pomme-de-terre.jpg)

##  Chicken tajine with potatoes 

Hello everybody, 

Who does not like to return home at noon, to find that dish, which he has prepared in the morning before going out well cooked, and ready to serve? 

Personally, I have days like that, or I have to go in all directions to make purchases, or go to appointments, or I do not know what ?! Then I run back home to eat before my husband is there. Sometimes there is only the pressure cooker to save me in this situation. But to be honest there is no better than cooking over low heat. 

I had thought about buying a pressure cooker, especially since I already tasted a dish at my friend prepared in his cookeo. It is the cookeo and you are sure to have healthy recipes and dishes, but here it is not in my budget at the moment. 

So I offered myself an electric Moroccan tagine, it is true that it is small tajine, but it is ideal for the midday dishes especially that it is generally between me and my husband only (the children eat at the school). I often go out this tagine when I go out the day and I know that I come home at noon just before my husband time to set the table. What's good is that I put all my ingredients in the tajine (sometimes I do it at night and I put the tajine-not all the device- in the fridge) and in the morning I just plug the plug, and I do an automatic cooking. 

more 

![chicken tajine with potato 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/tajine-de-poulet-a-la-pomme-de-terre-3.jpg)

When I come back at noon, I find the meat well cooked, a sauce very creamy, just to finalize, as for example for this tajine chicken with potato, on the way home, I removed the chicken legs to roast them in the oven (in order to have a nice color) and I took the opportunity to put the pieces of potatoes to cook in the sauce (I finished cooking the baked potato because I did not have the time) 

You can see the recipe from [ lham mhamer ](<https://www.amourdecuisine.fr/article-lham-mhamer-tajine-de-viande-rotie.html>) that I had already prepared in this same tagine electric.   


**Chicken tajine with potatoes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/tajine-de-poulet-a-la-pomme-de-terre-1.jpg)

portions:  2  Prep time:  15 mins  cooking:  50 mins  total:  1 hour 5 mins 

**Ingredients**

  * 2 whole chicken legs 
  * 1 onion 
  * 2 cloves garlic 
  * 2 ripe fresh tomatoes 
  * 1 small cinnamon stick 
  * 1 lemon confit, cut into medium pieces (leave a quarter for presentation 
  * ¼ teaspoon of ginger 
  * ¼ teaspoon of curry 
  * black pepper, salt, paprika 
  * olive oil 
  * 2 potatoes cut into wedges 
  * a nice handful of olives calamata 



**Realization steps**

  1. turn on the electric tajine 
  2. add the oil, chopped onion, garlic cut lengthwise 
  3. place the chicken pieces, add the lemon pieces, cinnamon, tomato slices 
  4. add the spices on top and place the tajine lid. 
  5. Cook until the chicken becomes tender. 
  6. remove the chicken meats and roast in the oven to give a nice color 
  7. at this time place the potato wedges in the tajine and cook.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/tajine-de-poulet-a-la-pomme-de-terre-4.jpg)
  8. when the potato is cooked, put the chicken pieces in the tajine, add the olives, leave a few minutes and serve with a good matlou3 



![chicken tajine with potato 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/tajine-de-poulet-a-la-pomme-de-terre-2.jpg)

You can see another version of this simple tajine: 

{{< youtube CubS5Yz0WDs >}} 
