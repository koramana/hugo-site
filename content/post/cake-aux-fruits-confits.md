---
title: Cake with candied fruits
date: '2013-06-08'
categories:
- Algerian cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-aux-fruits-confits-01111.jpg
---
Hello everyone, You'll believe that my oven is stubborn when cooking any cakes, but when I get to the cooking of candied fruit cake is a success with hat down! Why ? I do not know .... Is this the recipe? do you have to see and judge? This recipe, the first time it was Djouza who realized it during his visit at home, when I had explained to him the problem that I have with my oven .... She made her cake and it was a Delight ... Since then, I make any cake, & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.6  (  1  ratings)  0 

![cake-with-fruit-candied-011.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-aux-fruits-confits-01111.jpg)

Hello everybody, 

You will believe that my oven is stubborn when cooking any cakes, but when I get to the cooking of candied fruit cake is a success with hat down! Why ? I do not know…. 

Is this the recipe? do you have to see and judge? 

Since then, I make any cake, I miss it, I make this cake .... the recipe is a sublime success ... 

I'm mainly talking about cakes and cakes that require a certain temperature and some cooking time .. This problem I do not have with my [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , basboussa, or [ pies ](<https://www.amourdecuisine.fr/categorie-12347064.html>) .... only cakes that generally burn almost from above, and are poorly cooked inside ... 

So, I thank Djouza a lot for this recipe that I have never missed ... 

PS: If you have a facebook page, and you like my recipes, do not make a full copy of my recipe on your pages, it is an infringement of copyrights .... you like my recipe, share the recipe link ... .. do not enrich your pages with the work of others .... 

**Preparation time: 20 min** **Cooking time: 55 min**

ingredients: 

  * 180 gr of butter 
  * 80 gr of brown sugar 
  * 4 eggs 
  * 1 and a half pack of baking powder 
  * 350 gr of flour 
  * 1 small box of sweetened condensed milk (397 gr) 
  * Juice of half a lemon 
  * 100 gr of candied fruit 
  * 2 boxes of candied cherries (I had one) 



knowing that just like djouza, I used a cake mold 35 cm, otherwise cook the cake in two molds of 20 cm. 

![cake-with-fruit-candied-018.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-aux-fruits-confits-01811.jpg)

method of preparation: 

  1. You have to use a robot to make this cake, place the soft butter in the tank of the robot, and add the sugar. 
  2. Mix together. 
  3. Stir in the first egg and mix again. Add the 2nd, mix and continue in the same way until the last egg. The dough should be soft and creamy (with my robot, I had a little problem, because I did not have the creamy texture, so I put everything in the fridge a few minutes and after I mix with the foot mixer, which helps the dough to become creamy) 
  4. Sift flour and yeast and mix together at low speed. 
  5. Place the mixture in a salad bowl and add the sweetened condensed milk. Stir well. The dough is thick. 
  6. Pour in lemon juice, candied cherries and candied fruit. 
  7. Line a cake tin with parchment paper. Pour in the preparation and put to cook in preheated oven th. 200 ° C for 10 minutes and 180 ° C for 45 minutes. 
  8. Check the cooking by pricking the tip of a knife that should come out dry. 



To finish (optional):   
You can, if you wish, prepare a syrup with vanilla water and sugar (me I dilute some honey with lemon juice in the microwave). With a brush, brush the entire surface of the cake. Let cool.   
This cake is better the next day and two days later. 

![cake-with-fruit-candied-006.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-aux-fruits-confits-00611.jpg)

thanks to you my friends to put a like on my facebook page: 

4.7 stars based on 23 reviews 
