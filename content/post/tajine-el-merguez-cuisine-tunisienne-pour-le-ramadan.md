---
title: tajine el merguez / Tunisian cuisine for Ramadan
date: '2013-07-12'
categories:
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-merguez-cuisine-tunisienne.CR2_1.jpg
---
![tajine-merguez - kitchen tunisienne.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-merguez-cuisine-tunisienne.CR2_1.jpg)

##  tajine el merguez / Tunisian cuisine for Ramadan 

Hello everybody, 

Here is a super delicious dish that I prepared yesterday for Iftar, it is a Tunisian tajine el merguez, a tajine with minced meat pudding with a delicious tomato sauce perfumed with capers and candied lemon. 

A recipe that I saw on a Tunisian TV show, and that I really like, and trying it was a real success, and the minced meat pudding really taste of merguez .... hum a delight. 

![tajine de merguez - flat-Tunisian-to-ramadan.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-de-merguez-plat-tunisien-pour-ramadan.CR2_1.jpg)

**tajine el merguez / Tunisian cuisine for Ramadan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-de-merguez-plat-tunisien-pour-ramadan.CR2_.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 350 gr of minced meat 
  * 3 to 4 chopped onions 
  * 1 candied lemon and a half 
  * 50 gr of capers 
  * 1 tbsp of tomato paste 
  * ½ tbsp of harissa 
  * 1 tablespoon of garlic 
  * 1 teaspoon of fennel powder 
  * 1 cup mint powder 
  * 5 potatoes 
  * salt and black pepper, coriander powder 
  * 3 to 4 tablespoons of olive oil. 
  * oil for frying 



**Realization steps**

  1. fry the onion and garlic in the olive oil until it turns a nice color. 
  2. add the tomato paste and the harissa. then add a little salt, black pepper and tabel. 
  3. water now for almost 1 glass and a half of water. Turn down the heat and let it cook. 
  4. take the minced meat, add some crushed garlic, the fennel powder, the mint powder, a little salt (not too much), a little black pepper, and a little tabel (coriander powder for me) 
  5. mix very well, and if necessary add a little water to this minced meat, so that it becomes easy to handle and that it is tender in the mouth. 
  6. form rolls of almost 8 cm long. 
  7. when the sauce is at its boiling point, dip the minced meat pudding in it. 
  8. cook on low heat. 
  9. towards the end of the cooking add the lemon confit cut in cubes, and the capers. 
  10. let it simmer a little and remove from the heat. 
  11. towards the end, fry the potato, and serve your tajine of merguez garnished with french fries. 



![Tajine el merguez - flat-Tunisian-to-ramadan.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-el-merguez-plat-tunisien-pour-ramadan.CR2_1.jpg)
