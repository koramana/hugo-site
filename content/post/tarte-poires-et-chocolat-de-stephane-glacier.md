---
title: pie and chocolate pie by Stephane Glacier
date: '2015-10-07'
categories:
- sweet recipes
- pies and tarts
tags:
- Cakes
- desserts
- Algerian cakes
- To taste
- Pastry
- Shortbread
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-de-stephane-glacier.jpg
---
[ ![stephane glacier pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-de-stephane-glacier.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-de-stephane-glacier.jpg>)

##  pie and chocolate pie by Stephane Glacier 

Hello everybody, 

Before you get the recipe for this great pie and chocolate pie by Stéphane Glacier, I'll tell you the story: 

Yesterday my neighbor of the landing, an old woman, asked me to go out to buy her a cake or even some muffins to taste with these children who will come to visit her the afternoon, I told her without problem I finish what I I have at hand, and I'll go out .. What I had on hand was the realization of this pie with chocolate pears, I told myself, why not double the quantities, and do 2 pies, one for us, and one for her ... 

that's what I did, I doubled everything especially that the recipe is super easy. But here, I preferred to make a big pie for the neighbor, so I chose my biggest pie plate, a mold of 30 cm in diameter, I smeared the dough, made the white cooking, garnished its pie, put it back in, then I turn to mine. my mold was a little big for the dough I had left, so I got a little dough, hihihi 

after cooking, I was afraid to put a lot of the filling so that it does not flow here and there ... that's why I had a tart "flat", hihihihi 

Nevertheless, the pie of my neighbor was beautiful, very large, super busy, besides she was very happy when she opened the door to see this dessert to taste it just for her and her children ... Not to mention that before leaving, I opened the door to see the whole family who came to thank me for the pie ... What a pleasure. 

I tell you the story, just to tell you that if you use a 22 cm pan, you will have a beautiful, well-filled and creamy tart, yum yum. 

recipe in Arabic: 

**pie and chocolate pie by Stephane Glacier**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-chocolat-et-poire.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * a drop of [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>)

For the chocolate cream 
  * 130 ml of fresh cream 
  * 125g of dark chocolate at 60% 
  * 1 egg + 3 yolks 
  * 70 g of sugar (if your chocolate is not too dark do not put too much sugar) 

For garnish 
  * 1 big box of pears in syrup 
  * 1 nice handful of flaked almonds 
  * icing sugar 



**Realization steps**

  1. prepare the shortbread dough 
  2. Flour your pie pan 22 cm and let cool. 
  3. Preheat the oven to 160 ° C. Cook the pie shell (cold) for 20 minutes. 

Prepare the chocolate cream: 
  1. Boil the cream and pour over the chocolate. 
  2. Mix to homogenize everything. 
  3. Whip the egg with the yolks and sugar for 5 minutes, the mixture will foam and double in volume 
  4. add the melted chocolate and the fresh cream gently with a spatula. 
  5. Drain the pears from the syrup and dry them on paper towels. 
  6. cut them according to your taste and arrange them in the cooked pie shell.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-poire-et-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-poire-et-chocolat.jpg>)
  7. Cover with the chocolate cream, sprinkle with flaked almonds and bake again for 20 minutes th.160 ° C 



[ ![chocolate pear pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-poire-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-poire-au-chocolat.jpg>)
