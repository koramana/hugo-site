---
title: gratin of pasta and broccoli with béchamel sauce
date: '2018-03-30'
categories:
- diverse cuisine
- Cuisine by country
- Unclassified
- recipes of feculents
tags:
- dishes
- Algeria
- Easy recipe
- Full Dish
- Oven dish
- Fast Recipe
- Express cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/03/gratin-de-p%C3%A2tes-et-brocolis-%C3%A0-la-sauce-b%C3%A9chamel.jpg
---
![gratin of pasta and broccoli with béchamel sauce](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/gratin-de-p%C3%A2tes-et-brocolis-%C3%A0-la-sauce-b%C3%A9chamel.jpg)

##  gratin of pasta and broccoli with béchamel sauce 

Hello everybody, 

When I run out of ideas to make food, it is this gratin pasta and broccoli sauce béchamel that I do for my children. 

In fact I make gratin pasta sauce béchamel as a trick to introduce vegetables that my children eat hard times, and it works, sometimes it's cabbage flowers, sometimes broccoli, sometimes Brussels sprouts, and it works without any problem, children love the creamy of this dish so much that they do not notice vegetables embedded in it, hihihihihi 

So here is my version of this gratin pasta and broccoli sauce Bechamel, so good and creamy, it does not last long. 

**gratin of pasta and broccoli with béchamel sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/gratin-de-p%C3%A2tes-et-brocolis-%C3%A0-la-sauce-b%C3%A9chamel-2.jpg)

**Ingredients**

  * 200 gr of broccoli 
  * 300 gr of pasta (the type of your choice) 

For bechamel sauce: 
  * 1 small onion 
  * 1 clove of garlic 
  * 60 gr of butter 
  * 60 gr of flour 
  * 600 ml of milk 
  * salt, black pepper, and coriander + garlic powder 
  * 1 small handful of cheese (edam) 

for decoration: 
  * Edam cheese. 



**Realization steps**

  1. start by boiling the broccoli roses, and the pasta in boiling salted water (in 2 separate pots) 
  2. fry the chopped onion in a little oil until translucent, add the garlic, fry a little and remove from the heat. 

prepare the bechamel sauce: 
  1. Melt the butter in a saucepan and pour the flour. Stir quickly with the spatula and cook for 2 minutes over low heat without stopping stirring until light. 
  2. Remove from heat and gently add the milk while whisking gently. 
  3. Put everything back on the heat and continue to whip to make the mixture thicken until boiling. 
  4. Salt, pepper and add the garlic + coriander powder 
  5. remove from heat, add the onion and garlic mixture, then a handful of grated cheese 

dish mounting: 
  1. in a salad bowl, place pasta cooked and drained with water, 
  2. top with broccoli and pour bechamel sauce, mix gently with a wooden spoon. 
  3. place everything in a baking tin baking pan. 
  4. cover the top with a little cheese, and place in the oven at 180 ° C warm, the time that the dish is well au gratin, remember that the rest of the ingredients and already cooked well. 



![gratin of pasta and broccoli with béchamel sauce 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/gratin-de-p%C3%A2tes-et-brocolis-%C3%A0-la-sauce-b%C3%A9chamel-1.jpg)
