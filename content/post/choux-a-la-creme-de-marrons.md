---
title: cabbage with chestnut cream
date: '2016-01-15'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- sweet recipes
tags:
- Cracker
- Cream puffs
- Whipped cream

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-a-la-cr%C3%A8me-de-marrons-11.jpg
---
##  [ ![cabbage with chestnut cream](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-a-la-cr%C3%A8me-de-marrons-11.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-a-la-cr%C3%A8me-de-marrons-11.jpg>)

##  cabbage with chestnut cream 

Hello everybody, 

cabbage with chestnut cream: A very nice recipe that shares with us today Lunetoiles: cabbages a [ the chestnut cream ](<https://www.amourdecuisine.fr/article-creme-de-marrons-maison.html>) . Lunetoiles is super happy to have managed the pate a cabbage, and here is what she tells us: 

I got 15 cabbages with the ingredients given for cottage cheese, but I only filled 9 because I did not have muslin cream anymore. 

[ ![cabbage with chestnut cream 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-a-la-cr%C3%A8me-de-marrons-21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-a-la-cr%C3%A8me-de-marrons-21.jpg>)   


**cabbage with chestnut cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-a-la-cr%C3%A8me-de-marrons-31.jpg)

**Ingredients** (for 9 cabbages): For the insert: 

  * 9 c. coffee chestnut cream 

For the cracker: 
  * 50 g of flour 
  * 50 g brown sugar 
  * A pinch of fleur de sel 
  * 40 g of butter at room temperature 

For the choux pastry: 
  * 125 g of water 
  * 2 g of sugar 
  * 2 g of salt 
  * 60 g of sweet butter 
  * 80 g T45 flour 
  * 125 g whole eggs 

For the muslin cream with chestnuts: 
  * 2 egg yolks 
  * 40 g of sugar 
  * 10 g cornflour 
  * 10 g flour T45 
  * 250 ml of ENTIER organic milk 
  * ½ vanilla pod 
  * 150 g butter at room temperature 
  * 300 g of chestnut cream 

For the finish: 
  * icing sugar 



**Realization steps** Prepare the cracker (the ideal is to carry out this step the day before): 

  1. In a bowl, mix together the flour, brown sugar and fleur de sel 
  2. Add the ointment butter and mix until a smooth dough 
  3. Spread the dough about 3 to 4 mm thick, between two sheets of parchment paper 
  4. Using a round cutter 3 or 4 cm in diameter, cut 8 dough discs 
  5. Book everything in the freezer   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/pr%C3%A9paration-du-craquelin-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/pr%C3%A9paration-du-craquelin-.jpg>)

Make the puff pastry: 
  1. In a saucepan, pour the water, sugar, salt and butter cut into pieces, and heat over medium heat 
  2. From the first broth, remove the pan from the heat and then pour the flour in rain while stirring for 30 seconds to dry the dough 
  3. Put the pan back on the heat and stir vigorously for 2 minutes without stopping until the dough is smooth and peels off the sides of the pan. 
  4. Transfer the dough into a container and let cool for 10 minutes 
  5. Preheat your oven to 180 ° C rotating heat 
  6. Stir in the eggs one by one, while stirring vigorously with a spatula (wait until the dough is homogeneous before each egg addition) 
  7. The dough is ready when it forms a bird's beak and the groove of a line drawn with the finger in the dough closes   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/preparation-de-la-pate-a-chou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/preparation-de-la-pate-a-chou.jpg>)
  8. Transfer the preparation into a piping bag 
  9. Immediately place cabbages 4 cm in diameter on a baking sheet lined with parchment paper 
  10. Place a frozen cracker disk on each cabbage 
  11. Bake for 32 to 35 minutes 
  12. The cabbages are ready when they are well browned (IMPORTANT: do not open the oven for the duration of the cooking, otherwise the dough will sag)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/r%C3%A9alisation-des-choux-au-craquelin.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/r%C3%A9alisation-des-choux-au-craquelin.jpg>)
  13. place the plate on a rack to let it cool 

Prepare the muslin cream with chestnut cream: 
  1. Sift flour and cornflour 
  2. Whisk together egg yolks and sugar 
  3. Add cornflour and flour while whisking 
  4. Bring the milk and the half vanilla pod split in half and scraped 
  5. When the milk comes to a boil, remove the half vanilla pod and pour half of the hot liquid over the previous mixture while whisking. 
  6. Pour back into pan and cook over medium heat, stirring constantly until cream thickens (about 1 minute)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/la-creme-patissiere.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/la-creme-patissiere.jpg>)
  7. As soon as the cream is ready, pour it on a plate or in a wide dish (it will cool much more quickly) and film on contact 
  8. Let cool to room temperature 
  9. Whisk the butter and cream of chestnuts with electric whisk until the mixture is homogeneous 
  10. Add the cold custard to the mixture and whip until everything is smooth and creamy 
  11. Transfer the cream into a piping bag with a fairly large socket (around 15 mm) 
  12. Book fresh 

Dressage and finishing: 
  1. Cut cabbages in half in thickness 
  2. Put a little muslin cream in each cabbage 
  3. Put on 1 tbsp. coffee chestnut cream in each cabbage 
  4. Finish with a last layer of cream 
  5. Cover with the top of the cabbage crown and sprinkle with icing sugar   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/creme-au-beurre-aux-marrons.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/creme-au-beurre-aux-marrons.jpg>)
  6. Book fresh 



[ ![cabbage with chestnut cream 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-a-la-cr%C3%A8me-de-marrons1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/choux-a-la-cr%C3%A8me-de-marrons1.jpg>)
