---
title: grilled shrimp skewers with sesame
date: '2017-12-14'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- idea, party recipe, aperitif aperitif
- Unclassified
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brochette-de-crevette-029.CR2_.jpg
---
[ ![shrimp skewer 029.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brochette-de-crevette-029.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brochette-de-crevette-029.CR2_.jpg>)

##  grilled shrimp skewers with sesame 

Hello everybody, 

easier and better than this recipe, is going to be a challenge. These grilled shrimp brochettes with sesame are a real treat. The shrimp are marinated in a very delicious marinade flavored with ginger, garlic, and hot pepper, drizzled with a good fillet of sesame oil and soy sauce ... For more details on this recipe, follow me in my little kitchen.    


**grilled shrimp skewers with sesame**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brochette-de-crevette-032.CR2_.jpg)

Recipe type:  Entrance  portions:  4  Prep time:  60 mins  cooking:  10 mins  total:  1 hour 10 mins 

**Ingredients**

  * 220 gr of shrimp (the royal at home in England) 
  * 2 cm fresh ginger 
  * 2 cloves garlic 
  * 1 tablespoon of sesame oil 
  * 1 tablespoon soy sauce 
  * 1 tablespoon rice vinegar or apples. 
  * salt and black pepper 
  * 1 teaspoon flakes of dry pepper (optional) 
  * roasted sesame seeds. 



**Realization steps** prepare the marinade: 

  1. mix crushed garlic, grated ginger, salt, black pepper, hot pepper, sesame oil, soy sauce, and vinegar together. 
  2. place in the shrimp and let marinate 
  3. toast the sesame on a low heat. 
  4. After a minimum of 1 hour marinating, place shrimp in skewers. 
  5. cover the shrimp well with the grilled sesame. 
  6. and grill according to your means, until total cooking shrimp 



[ ![shrimp skewer 026.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brochette-de-crevette-026.CR2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brochette-de-crevette-026.CR2-001.jpg>)
