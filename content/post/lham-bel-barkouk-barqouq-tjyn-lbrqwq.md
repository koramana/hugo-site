---
title: lham bel barkouk barqouq طاجين البرقوق
date: '2012-11-20'
categories:
- recettes sucrees
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-hlou-003_thumb1.jpg
---
##  lham bel barkouk barqouq طاجين البرقوق 

Hello everybody, 

Here is a dish of the [ Algerian cuisine, ](<https://www.amourdecuisine.fr/categorie-12359239.html>) who never leaves the first day of the month of Ramadan, there are even people who like it to be always on their table. a very good sweet salty recipe, called Tajine hlou, or marka hlowa, or more easily: [ Lamb tajine with prunes, almonds and apricots ](<https://www.amourdecuisine.fr/article-tajine-agneau-pruneaux-amandes-abricots-97616823.html>) . 

in any case, at home, I can say that I am the only one who will eat it, not serious, because this dish can be well preserved in the refrigerator. 

you can see more than [ ramadan recipe ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>)

and here is my little recipe, as my mother always did 

**lham bel barkouk barqouq طاجين البرقوق**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-hlou-003_thumb1.jpg)

**Ingredients**

  * mutton 
  * dried prunes (for apricot mix, prune apples) 
  * some dried raisins 
  * ½ onion 
  * some butter or smen 
  * 3 tablespoons crystallized sugar 
  * 2 tablespoons orange blossom water 
  * 1 pinch or 1 cinnamon stick 
  * 1 little saffron or dye 
  * 1 pinch of salt 



**Realization steps**

  1. arrange all the cut meat, butter or smen, chopped onion, dye, cinnamon and salt. 
  2. cover with about ½ liter of water and cook. 
  3. in the meantime, dip the prunes in the water for a few moments, as well as the raisins. 
  4. remove them, dip them in the sauce, add the sugar and orange blossom water, cook on low heat, until the prunes swell. 
  5. reduce the sauce 
  6. you can sprinkle your dish with some blanched and roasted almonds. 



good tasting . 

for another version I recommend the delicious recipe: 

[ chbah essafra ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-chbah-essafra-102664956.html>)

for a tajine hlou recipe without meat: 

![Tajin-hlou-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tajin-hlou-11.jpg) [ TAJINE HLOU AUX POMMES ET ANIS ÉTOILÉ, SANS VIANDE ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html> "tajine hlou with apples and star anise, without meat")
