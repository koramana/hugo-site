---
title: Smoothie fat burner with grapefruit
date: '2017-01-16'
categories:
- boissons jus et cocktail sans alcool
- cuisine brule-graisses, recettes amaigrissantes
- Cuisine saine
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-brule-graisse-a-la-pamplemousse-1.jpg
---
[ ![Smoothie fat burner with grapefruit 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-brule-graisse-a-la-pamplemousse-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-brule-graisse-a-la-pamplemousse-1.jpg>)

##  Smoothie fat burner with grapefruit 

Hello everybody, 

Yet another fat burner Smoothie recipe with grapefruit, but frankly with this smoothie get ready to go back to the corner;). 

I tried this recipe on which I came across by chance when I visited the site: fork and bikini. It was falling because I had a grapefruit in my basket of fruit ... I do not even remember why I had bought this fruit, surely I had spotted a recipe that I had to do, but of course, and as always, I never find the time. 

Here is the version of this fat burner smoothie with grapefruit and lemon: 

**Smoothie fat burner with grapefruit**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-amaigrissant-a-la-pamplemousse.jpg)

portions:  2  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 oranges 
  * 2 grapefruits 
  * 100 g strawberries 
  * 2 natural yogurts 0% 
  * almost 1 glass of water 



**Realization steps**

  1. Peel oranges and grapefruits. 
  2. Wash and cut strawberries. 
  3. Mix the fruits with the yogurts in the bowl of the blinder   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-brule-graisse-pamplemousse.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-brule-graisse-pamplemousse.jpg>)
  4. add the water little by little, until you get the desired smoothie consistency. 
  5. Serve and treat yourself. 



[ ![Smoothie fat burner with grapefruit](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-brule-graisse-pour-perte-de-poids-a-la-pamplemousse.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-brule-graisse-pour-perte-de-poids-a-la-pamplemousse.jpg>)
