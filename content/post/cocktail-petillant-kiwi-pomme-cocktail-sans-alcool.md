---
title: Cocktail sparkling kiwi / apple "cocktail without alcohol"
date: '2013-04-01'
categories:
- Algerian cuisine
- Dishes and salty recipes
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/cocktail-petillant-kiwi-pomme-011.CR2_1.jpg
---
![cocktail sparkling kiwi-apple-011.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/cocktail-petillant-kiwi-pomme-011.CR2_1.jpg)

Hello everybody, 

want a sparkling cocktail, super delicious, it's like a smoothie, but instead of using a dairy product, I use apple scented sparkling water, which you can replace with a gaseuze water from your choice, or else, if you do not want sparkling water, go for a juice. 

Ingredients: 

  * 6 kiwis 
  * 2 medium apples, otherwise 1 big apple 
  * sparkling apple flavor (the quantity according to your taste) 



method of preparation: 

  1. Peel the apples and kiwi, and mix them in a blender. 
  2. Add sparkling water, or the juice of your choice, to get the texture that suits you, light or thick and mix. 
  3. Enjoy fresh. 



![cocktail sparkling kiwi-apple-013.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/cocktail-petillant-kiwi-pomme-013.CR2_.jpg)
