---
title: Stop the culinary plagiarism
date: '2012-05-21'
categories:
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/7116089529_9b75dafb401.jpg
---
![logo Stop Food Plagiarism](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/7116089529_9b75dafb401.jpg)

Hello everybody, 

it's been a long time since I made a joke on my blog, but when it goes beyond the limits, we say we must do something. 

since I put the alert at the top of his blog, on the people who steal the photos, and the recipes in whole and put them on facebook, or then on the forums. 

however it is allowed to take only the photo, if you really want a sharing, but provided that the link back to my blog. 

I receive each time a message from a friend who reports a page on Facebook, who steal the article in Integer .... 

and I do not tell you, I spend all night reported links, a manually a on this page of facebook: 

you can do it too, if you find a facebook page, steal your content ... 

but frankly my friends, I share with you, my recipes, I did not put the right click, because there are people, to print a recipe must copy it on word, because I have a problem with the icon impression of my blog. 

I leave the right click, because sometimes to leave me a comment, it's difficult, so right click on the comment link for it opens in a new window is easier ... 

but I do not put it, so that people come without authorization, take the whole recipe to put it on facebook ... .. 

but please stop, is that at this point, it is not visible at the top of the blog: copyright ???? 

should we do padlocks everywhere on our blogs ??? 

however, it's an easy gesture to click on leave a comment, and ask permission to copy the photo, or copy part of the recipe, or copying the entire article, is more easy than asking permission .... 

it's a matter of morality, principles, and trust between the blogger and those readers. 

why I'm talking about it now, it's amazing that a recipe published only yesterday on my blog, is already copy / paste in full on facebook pages, and not even with a link back, not even to say, thanks to this blogger for this recipe. 

it becomes an abuse, we get tired, we bloggers to put a complete recipe, well detailed at your disposal, knowing that sometimes we are not spared by the bad remarks if unfortunately we forget an ingredient ... 

it is very tiring as a stain, but we say to ourselves, there are many people who like to blog, and we share our small acquaintance with great pleasure with them. 

but when a person, thinks himself so cunning, just takes the recipe, without any words, without evoking a little regret, taking the recipe, like that, without mercy or forgiveness .... Well, it makes me write this article, because for me it's **Flight.**

think a little about this person who gets tired to put work online, I'm not paid for it, I do it for free and with great love, but I do not like that we can take us for ....... 

je n’ose pas dire de mauvais mots sur mon blog, par respect aux personnes qui me respectent et qui visitent ce blog car ils l’aiment, mais ça me déçoit ces personnes qui viennent sur ce blog, juste pour copier un article en entier, afin de le mettre ailleurs et en son nom….. 
