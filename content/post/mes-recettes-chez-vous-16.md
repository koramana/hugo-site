---
title: my recipes at home 16
date: '2012-02-15'
categories:
- amuse bouche, tapas, mise en bouche
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg
---
![my recipes at home](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg)

##  my recipes at home 16 

Hello everybody, 

I'm going to publish this article, because every time I update it to publish, I find emails with your sublime pictures, and your nice words, which make me very happy. 

thank you all, and I will always be happy to share your essays, my recipes, or so, your recipes that I could test or publish on my blog. 

to begin: 

and Amira also said: 

Hi, I did it last week, my kids loved it and I served it with whipped cream and I say a big thank you. 

![SAM_0119](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/72186171_p1.jpg)

on the earpieces assia said: 

Hello Soulef, I tried and honestly it was a "killing" as the young say (although I'm not old ahahah). I will try again because aesthetically they were not very pretty, but delicious. They were too small and a little curled up on herself. So I will try again and I will give you news.   
Hugs 

again this recipe: 

![Cake-to-cafe.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/cake-au-cafe1.jpg)

sweet cake with coffee at Saliha (a reader) 

![tarte aux poires.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/tarte-aux-poires1.jpg)

[ Clafoutis with pear ](<https://www.amourdecuisine.fr/article-clafoutis-aux-poires-98865118.html>) at Selma a reader 

![bread-has-the-meat-hachee.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/pains-a-la-viande-hachee1.jpg)

[ hamburger buns ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee-96050112.html>) at Soraya a reader 

hello, i tried and succeeded to your recipe: buns with minced meat she was great thank you sincerely biz on the recipe of [ tahboult, egg cakes ](<https://www.amourdecuisine.fr/article-tahboult-n-tmelaline-mechouwcha-ou-galette-kabyle-aux-oeufs-54256658.html>) Helene said: Hello,   
Your Kabyle cake with eggs is delightful   
thank you 

![khadija-essais.JPG](http://img.over-blog.com/450x297/2/42/48/75/API/2012-02/khadija-essais.JPG)

Assalam aleikum, 

My dear sister, my name is Khadija, I am Russian and I like Algerian cuisine. I tried some recipes and here are my photos: Aid 2011, the stars, the cakes without cooking 

![griwech.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/griwech1.jpg)

the [ Griwech ](<https://www.amourdecuisine.fr/article-25345475.html>) chez Annie 
