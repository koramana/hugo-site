---
title: chocolate fluffy mini
date: '2011-12-19'
categories:
- dessert, crumbles and bars
- recettes patissieres de base
- recettes sucrees

---
Hello everybody, 

here is a nice recipe for fluffy cakes to fall, a delice, these mini fluffy chocolate, a recipe very simple and very easy: 

Ingredients: 

  * 200 g of dark chocolate. 
  * 3 eggs 
  * 4 tablespoons milk. 
  * 4 tablespoons of butter. 
  * 50 g of sugar. 
  * 3 tablespoons of cocoa 
  * 80 g flour. 
  * 1 teaspoon of baking powder. 
  * 1 coffee vanilla. 



PREPARATION: 

  1. In a bowl whiten eggs, salt, sugar and add milk 
  2. Sift the flour, cocoa, vanilla and yeast, add to the egg mixture. 
  3. stir in the melted chocolate. 
  4. fill in  muffin mussels. 
  5. Bake at 180 ° for 15 minutes. 


