---
title: khobz dar a flour - homemade flour bread خبزالدار بالفرينة
date: '2011-10-31'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/khobz-011_thumb1.jpg
---
##  khobz dar a flour - homemade flour bread خبزالدار بالفرينة 

Hello everybody, 

I really like the **khobz dar with flour** My mother did it all the time, and I confess that enjoying a bowl of milk at breakfast with a piece of bread is just a treat, a thousand times better than taking the most delicious cakes. 

Generally I do the **khobz dar with flour** in a single cake, but for my children I found a lot of fun to make mini breads decorated differently. They really liked 

this khobz dar has flour is a very very light bread, and very delicious. try it and tell me your opinion   


**khobz with flour - bread with flour - homemade bread خبزالدار بالفرينة**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/khobz-011_thumb1.jpg)

**Ingredients**

  * the equivalent of 450 grams of flour. 
  * 1 egg 
  * oil table 4 cases (soup spoon) and a little to wet the hands 
  * 1 and a half salt (1 teaspoon) 
  * 1 case of sugar 
  * black seed and sesame seeds 
  * 2 cases of baker's yeast. 
  * ¼ glass of warm milk 
  * \- lukewarm water. 



**Realization steps**

  1. if you use the map, put all the ingredients in the correct way to your map, and control the water, you must have a soft paste and not too sticky. 
  2. If it's by hand, start by putting the yeast in a small glass of warm water, or if it is an instant yeast incorporate it into your ingredients according to your habits. 
  3. prepare your dough, by mixing all your ingredients, knead by adding lukewarm water little by little, this dough will be very sticky, so you can wet your hands with oil when you can not stand all this dough that sticks to it. 
  4. when you get a very smooth and malleable paste, place it in a covered place, until it doubles or even triple in volume. 
  5. wet your hands with oil, and shape your bread if you want a big cake, or like me, make small balls, which you place in an oiled tray. and put on each ball, a small cross with always the same dough. 
  6. brush with a mixture of egg yolk and milk, and let stand a little longer. (For my part I left my loaves in my cold oven, for almost 15 min, I took out my trays light the oven, and I started cooking. 



![khobz dar with flour](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/khobz-8_thumb1.jpg) ![khobz dar with flour](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/khobz-012_thumb1.jpg)

bonne dégustation. 
