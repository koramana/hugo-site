---
title: dried fruit bars
date: '2016-11-10'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- Algerian dry cakes, petits fours
tags:
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/barres-de-fruits-secs-3.jpg
---
![bar of fruit-dry-3](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/barres-de-fruits-secs-3.jpg)

##  dried fruit bars 

Hello everybody, 

These delicious dried fruit bars can make their place on the table at tea time, as they can be featured with Eid cakes. 

And it was the case with Lunetoiles who prepared these beautiful bars with dried fruits. The principle of these small delights is not too far from [ tartlets with dried fruits ](<https://www.amourdecuisine.fr/article-tartelettes-aux-fruits-secs-caramel-beurre-sale.html>) but with an even easier version ... 

Now we can taste the dried fruit tartlets very often because we can prepare them more quickly, what do you think? 

![bar of fruit-dry-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/barres-de-fruits-secs-1.jpg)

**dried fruit bars**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/barres-de-fruits-secs-2.jpg)

portions:  80  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * 500 g of flour 
  * 1 pinch of salt 
  * 340 g of soft margarine or soft butter 
  * 240 gr of icing sugar 
  * 2 tbsp. coffee vanilla extract 
  * 2 egg yolks 

Garnish : 
  * 125 g slivered almonds 
  * 125 g hazelnuts and halved 
  * 125 g walnuts or pistachios grilled and cut in half (I put two) 
  * some candied cherries (I did not put any) 
  * 80 g of butter 
  * 250 g of honey 
  * 1 C. coffee vanilla extract 



**Realization steps**

  1. Sift the flour and icing sugar. Stir in diced softened margarine, pinch of salt and vanilla extract. 
  2. Crumble with the fingertips, then wet with the egg yolk. 
  3. Pick up the dough in one go. Cover with foil and let cool for 30 minutes. 
  4. Meanwhile melt the butter in a saucepan, stir in honey and vanilla extract. Do not boil. 
  5. Toast slightly all the dried fruits. Cool and add to the mixture. Return to low heat for about 10 minutes. 
  6. Preheat the oven to 200 ° C. 
  7. Introduce the dough into a mold (40x20 cm) and prick it. 
  8. It must be 1 cm thick. Bake at 200 ° C for 15 to 20 minutes until lightly browned. 
  9. Cool and spread the filling over the dough and return to the oven for 10 to 15 minutes. 
  10. The dried fruits must have a golden color. 
  11. Let cool completely, then cut into 5cm squares and decorate each square with half a cherry. 
  12. Place in square boxes. 
  13. The squares are well preserved in an airtight tin box. 



![bar of dry-fruit](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/barres-de-fruits-secs.jpg)
