---
title: Flan Coco Quinoa Weight Watchers (WW diet)
date: '2010-02-08'
categories:
- dessert, crumbles and bars
- panna cotta, flan, and yoghurt
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/02/215789021.jpg
---
![au_four](https://www.amourdecuisine.fr/wp-content/uploads/2010/02/215789021.jpg)

![ingred_flan_coco](https://www.amourdecuisine.fr/wp-content/uploads/2010/02/215788451.jpg)

put the quinoa in the water and bring to the boil, after reducing the fire and cook (there will be no water left) 

![ca_bouille](https://www.amourdecuisine.fr/wp-content/uploads/2010/02/215789931.jpg)

the eggs are made to beat with the sugar, and the coconut milk is added 

after chilling the quinoa, we put it back in ramekins (as I did not have any, I made my custard in the jars of my yogurt maker 

![quinoa](https://www.amourdecuisine.fr/wp-content/uploads/2010/02/215790491.jpg)

pour over the mixture of eggs and milk and place in a bain-marie 

![au_bain_maree](https://www.amourdecuisine.fr/wp-content/uploads/2010/02/2157895011.jpg)

a little coconut is sprinkled on it, and the oven is preheated to 190 degrees and cooked for 35 minutes. 

my verdict, very delicious custard, with the quinoa that gives a slight crunch in the custard 

![mon_flan](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/215791321.jpg)

Enjoy your meal 
