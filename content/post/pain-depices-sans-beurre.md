---
title: gingerbread without butter
date: '2017-11-22'
categories:
- cakes and cakes
- sweet recipes
tags:
- Gingerbread
- Fast Food
- Easy cooking
- fall
- Algerian cakes
- Cakes
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/11/pain-depices-sans-beurre-1.jpg
---
![gingerbread without butter 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/pain-depices-sans-beurre-1.jpg)

Hello everybody, 

If there is a cake that I like to eat every day is gingerbread, I do not know if it's really a cake, but it does not matter. Not only that the gingerbread reminiscent of childhood memories in me, it also awakes all my senses at the first tasting. 

What was I sick when I arrived here in England, and when I wanted to taste gingerbread and found that it does not sell here ????? !! !!!!! 

This is where I started my quest for "the recipe" for gingerbread, the real gingerbread, the one I got used to. Of course it was commercial gingerbread, but that's what my palate got used to, and I confess I did not stop at a recipe, two or three ... If I do not have not tried at least 50 recipes, maybe it's a little! and never satisfied. 

Until the day I tried a recipe of Mercotte, and this gingerbread was just beautiful you can see the recipe on this [ link ](<https://www.amourdecuisine.fr/article-recette-de-pain-d-epice.html>) . You will find a long list of ingredients. I do not also tell you how many times I have done and redone this recipe, whenever I have the desire of gingerbread or recipe with gingerbread. Over time, I adapted the recipe according to what I had at home as ingredients, and it was always a success. 

![gingerbread without butter 3](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/pain-depices-sans-beurre-3.jpg)

Today, I come to share with the recipe of gingerbread without butter, it will work well in this period in shortage of butter !!! Plus it's too good and I really do not find any difference between the two gingerbreads, the one in the butter and the one without. 

**gingerbread without butter**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/pain-depices-sans-beurre-2.jpg)

**Ingredients**

  * 260g of flour (I put 180 gr flour + 80 gr of flour) buckwheat 
  * 40 gr of brown sugar 
  * 1 C. coffee baking soda 
  * 1 teaspoon cinnamon 
  * 1 teaspoon ginger powder 
  * 1 C. coffee spice mixture for gingerbread 
  * ¼ of c. coffee powder star anise 
  * 2 eggs 
  * 100 gr of whole milk + 1 little bitiane to infuse 
  * 340 gr of honey 
  * 20 g candied ginger (optional) 
  * 20 gr of candied orange zest or candied orange 



**Realization steps**

  1. boil the milk in advance with the star anise, then remove from the heat let infuse and warm. 
  2. in a large salad bowl, mix all the dry ingredients: flour, sugar, cinnamon, ginger, spices, and baking soda. 
  3. mix these products well. 
  4. make a pit in the middle, and break the eggs, add the filtered warm milk and the honey. 
  5. incorporate everything well. 
  6. pour the dough into a baking pan lined with baking paper, or a silicone mold. 
  7. Bake in a preheated oven at 160 ° C for 15 minutes, then raise the temperature to 180 ° C. cook another 30 minutes, or according to the good cooking of gingerbread. 
  8. remove from the oven after cooking, allow to cool and unmold. 
  9. when the gingerbread has cooled down, cover it with a cling film and leave at least 24 hours. 
  10. it's better then to savor and savor your gingerbread. 



![gingerbread without butter_](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/pain-depices-sans-beurre_.jpg)
