---
title: quiche with brie
date: '2010-03-08'
categories:
- les essais de mes lecteurs

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/03/quiche_thumb1.jpg
---
and here is my quiche brie, finally long-awaited, because I must tell you, my husband is not a fan of quiches, and so I have to take advantage of his absence to do them, and as he left today in London I enjoy it well to make me one. and it was with a Brie brick that I bought from Lidl a week ago. scented brie at the Basilica, a delight. and what did I have in my fridge, seeing seeing: the rest of the broken dough I made yesterday after my tarte tatin (it falls & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.9  (  2  ratings)  0 

![quiche](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/quiche_thumb1.jpg) ![quiche1](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/quiche1_thumb1.jpg)

and here is my quiche brie, finally long-awaited, because I must tell you, my husband is not a fan of quiches, and so I have to take advantage of his absence to do them, and as he left today in London I enjoy it well to make me one. 

and it was with a Brie brick that I bought from Lidl a week ago. scented brie at the Basilica, a delight. 

and what did I have in my fridge, seeing 

so first I spread the broken dough, and put them in my baking tin, then I made the minced meat in olive oil, with a little salt and condiment (coriander / garlic plus black pepper), I added dried parsley (I was not fresh). 

I added after the zucchini sliced ​​in a slice, I let simmer a little on low heat, and then I put everything on my dough broken 

I made a small garnish with Brie cut into cubes. whisk, down our eggs with milk, a touch of salt, and condiment according to taste (beware of salt). I pour this mixture on the minced meat garnish / zucchini / brie. 

it is covered with a nice layer of grated Gruyère, and preheated to 150 degrees C until you cook your quiche. 

enjoy with a nice salad, and you will see the taste to fall. 

my friend in any case immediately write the recipe. 

I tell you good tasting, and your comments are welcome. 

merci 
