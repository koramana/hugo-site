---
title: 'recipes cakes Choumicha: chocolate madeleine'
date: '2012-11-20'
categories:
- Cupcakes, macarons, et autres pâtisseries
- gateaux, et cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/madeleine-au-chocolat-de-choumicha_thumb1.jpg
---
##  recipes cakes Choumicha: chocolate madeleine 

we have everyone, 

who of you does not like Choumicha, she is pretty, very talented in cooking, besides she has no worries of weight (I have to ask her what diet she does ??? hihihih). 

in any case, for the fact of Eid 2012, I went to pick her home, to make some cakes, that we love a lot at home. 

last time, I passed you the recipe for " [ almond cookies ](<https://www.amourdecuisine.fr/article-petits-fours-aux-amandes-112084773.html>) ", And this time it's madeleines white chocolate, it is hardly that I could save a few for the photos.   


**recipes cakes Choumicha: chocolate madeleine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/madeleine-au-chocolat-de-choumicha_thumb1.jpg)

portions:  40  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * Ingredients: 
  * 3 eggs + 1 egg yolk 
  * 125 gr of icing sugar 
  * 80 gr melted butter then put to room temperature 
  * the juice of 1/2 orange 
  * 125 gr of flour 
  * 1 pinch of salt 
  * apricot jam 

decoration: 
  * white chocolate, or black according to taste. 



**Realization steps**

  1. in a bowl, whisk the sugar and eggs until the sugar cubes disappear. 
  2. stir in the melted butter, then the orange juice. 
  3. then add salt and flour, and put everything to rest for a few minutes in the fridge. 
  4. pour the mixture into a piping bag, and fill with small silicone molds ¾ of the height (the smaller the mussels, the better the cakes will be) 
  5. put a small spoon of jam, in each footprint filled with the cake. 
  6. cover the mussels with baking paper, then place on top of another tray, so that the muffins do not swell too much, and you have a flat surface. 
  7. bake at 180 degrees between 10 to 15 minutes. 
  8. at the end of the oven, remove the cakes from the molds, fill the molds with a little melted chocolate, and put the cakes back in, squeeze a little, and let it sit at room temperature. 


