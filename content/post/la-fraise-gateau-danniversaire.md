---
title: strawberry, birthday cake
date: '2012-01-29'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/fraisier2a_thumb1.jpg
---
![strawberry, birthday cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/fraisier2a_thumb1.jpg)

##  strawberry, birthday cake 

Hello everybody, 

you are on the way to a birthday cake, beautiful, but easy, well here is a sublime cake from our lovely Lunetoiles, well this recipe is in my archives for almost a year, 

and Lunetoiles came to remind me that I did not publish, hihihiih of times I'm really "lazy" ... .. 

then to you this delicious recipe:   


**strawberry, birthday cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/fraisier5a_thumb.jpg)

**Ingredients** The Genoise 

  * 4 eggs 
  * 120g of sugar 
  * 120g of flour 

The cream chiffon 
  * 500 ml of milk 
  * 4 egg yolks 
  * 130g of sugar 
  * 50g cornflour 
  * 1 vanilla pod 
  * 200g of tempered butter 

For garnish 
  * 200g strawberries 
  * 100 ml strawberry coulis 

Decor 
  * 500g almond paste colored in red 
  * 100g almond paste colored in green 
  * 30g of white chocolate 



**Realization steps** Prepare the muslin cream 

  1. Split the vanilla pod in half, scrape the seeds and put all in the milk you will bring to the boil. 
  2. In a bowl, whisk the yolks and sugar until the mixture whitens. Then add the cornflour. 
  3. Pour the boiling milk over the previous mixture, then put it back into the pan. 
  4. Then thicken, stirring constantly. Pour the cream into a salad bowl. 
  5. Add 100g butter to the cream still hot, mix well, film and let cool. 
  6. Add the remaining 100g of butter remaining in the cooled cream, then whisk with an electric mixer until a cream is obtained. 

Prepare the sponge cake 
  1. Put the eggs and the sugar in a cul-de-poule and place it in a bain-marie. 
  2. Whisk with an electric mixer until the mixture is white and triple in volume. It takes about 5 minutes: you have to get a very foamy preparation. 
  3. Remove the salad bowl from the water bath. Pour the flour in rain over the entire surface of the foam and incorporate it with a spatula, lifting the mass. 
  4. A minimum must be mixed to prevent the foam from falling back. 
  5. Pour the preparation into a buttered and floured heart-shaped mold and cook for 30 minutes in a preheated oven at 180 ° C (th.6). 
  6. Once this time has elapsed, remove the sponge cake from the oven, let it cool down before unmolding it on a grid, curved upwards (so as not to crush it). 

Mounting 
  1. Cut the sponge cake in three. 
  2. Place the first slice on a tray, soak it with strawberry coulis, 
  3. put half of the cut strawberries in half, then cover with a third of cream being careful that there is a little more in the center to get a well rounded cutter. 
  4. Repeat the process, then finish with the third slice of sponge cake, 
  5. press lightly and coat with a thin layer of cream. 
  6. Place in the fridge 1 hour before decorating. 
  7. Spread the red almond paste (helping you with icing sugar sprinkled on the roll and the worktop so that the dough does not stick) forming a large triangle that should go well beyond the cake. 
  8. Place it on the strawberry, apply it gently with the flat of the hand. 
  9. using a knife, remove the excess marzipan. If necessary, cut the tip of the strawberry with scissors and smooth with a spatula to erase the scar. 
  10. Spread the green almond paste in a long rectangle and cut out triangles on one side to give the illusion of foliage. 
  11. Place the leaves on the strawberry, cut off the excess dough. 
  12. Create a small tail and other loose leaves and place them on the cake. 
  13. Melt the white chocolate and draw small dots using a greaseproof paper cone and then stretch them by passing a toothpick in the middle to give them the shape of small seeds. 
  14. Keep fresh until ready to serve. 


