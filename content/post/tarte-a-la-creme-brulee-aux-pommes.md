---
title: apple crème brûlée pie
date: '2014-08-23'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-a-la-creme-brulee-aux-pommes-0011.jpg
---
![pie-a-la-creme-brulee-the-apple-001.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-a-la-creme-brulee-aux-pommes-0011.jpg)

##  apple crème brûlée pie 

Hello everybody, 

I really like creme brulee, but I never thought about making a creme brulee pie covering a nice layer of melting apples ... 

here is this delicious pie with apple-creme that Lunetoiles has to realize, and what would I like to have, just a little bit ... yumi yumi ... 

![pie-creme-brulee-to-apples-1-copy-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-creme-brulee-aux-pommes-1-copie-11.jpg)

**apple crème brûlée pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-aux-pommes-a-la-creme-brulee11.jpg)

portions:  8  Prep time:  30 mins  cooking:  60 mins  total:  1 hour 30 mins 

**Ingredients**

  * 1 drop of broken dough (for a mold of 23 to 25 cm in diameter) 
  * 4 apples peeled and cut into 8 
  * 40 g of sugar 
  * !Creme brulee : 
  * 6 egg yolks 
  * 70 g of sugar 
  * 310 ml of cream 30% 
  * ½ teaspoon of vanilla extract 
  * 2 tablespoons icing sugar 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. Place the rack in the bottom of the oven 
  3. Go for a 25-pie pan and bake the dough for 10 minutes. 
  4. Then remove the cooking weights or dry beans and continue for 15 to 20 minutes. 
  5. Remove from the oven. 
  6. Cook the apples now: In a baking dish, or on a baking sheet lined with parchment paper, divide the apple wedges. 
  7. Sprinkle with sugar and bake for 40 minutes. 
  8. Remove from the oven. Let cool. 

Creme brulee 
  1. In a bowl, mix the egg yolks and sugar with a whisk. Stir in cream and vanilla. 
  2. When the apples are warm, place them in a rosette on the bottom of the pastry and pour the creme brulee, bake at 100 ° C for 1 hour. Remove from the oven, let cool. 
  3. Put the pie in the refrigerator for at least 2 hours. 
  4. Sprinkle pie with icing sugar and caramelize quickly with a cooking torch. 



![pie-a-la-creme-brulee-and-the-pommes.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/tarte-a-la-creme-brulee-et-aux-pommes11.jpg)
