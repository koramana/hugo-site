---
title: fig and melon jam
date: '2013-08-21'
categories:
- Algerian cuisine
- diverse cuisine
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-figues-melon-009.CR2_1.jpg
---
![figs jam-melon-009.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-figues-melon-009.CR2_1.jpg)

##  fig and melon jam 

Hello everybody, 

This jam of figs and melon is by far the most delicious, and the best home-made jam I've made until today ... 

A recipe that I realized by chance, after our neighbor passed us a box of figs from his garden, and I who hardly find figs in England, I find myself well spoiled in Algeria. 

So, and as I really like home-made jams, and that is always the best solution, when you have a surplus of fruit, I realized this jam, especially in the presence of a large melon without taste ....   


**fig and melon jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-figues-melon-004.CR2_1.jpg)

Recipe type:  jam  portions:  8  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 500 gr fresh green figs 
  * 500 gr of melon without skin. 
  * 500 gr of sugar 
  * 1 tablespoon of butter 
  * 2 to 3 anis etoilee (badian) 



**Realization steps**

  1. wash the figs and cut them in half. 
  2. cut the melon into a fairly large cube 
  3. place the two fruits in a large salad bowl, pour over the sugar, and leave to macerate overnight. 
  4. the next day, pour the mixture into a heavy-bottomed saucepan, add the butter and star anise. 
  5. cook over medium heat until the syrup is thick. you can test the cooking of the jam by pouring a little syrup on a cold plate, if the syrup is frozen, the jam is ready. 
  6. fill jars that you have spent in boiling water and that you have dried well (I dried mine in the microwave) 
  7. close the jars, and place them upside down until the jam is completely chilled. 



![figs jam-melon-002.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-figues-melon-002.CR2_1.jpg)

you can also see: 

[ FIG jam ](<https://www.amourdecuisine.fr/article-confiture-de-figues-111013344.html>)

[ confitture de figues et mangues ](<https://www.amourdecuisine.fr/article-confiture-de-figues-et-mangues-111341416.html>)
