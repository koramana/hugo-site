---
title: Algerian cake Skandraniette
date: '2013-01-10'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/skandraniette-gateaux-algeriens_thumb1.jpg
---
##  Algerian cake Skandraniette 

Hello everybody, 

a very nice recipe **Algerian cake Skandraniette** , a modern cake all beautiful well colored and well stocked with these beautiful flowers, and since it is very well stuffed with almonds, it's too good! 

This cake is well known, but like all the others [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , he took his share of makeover, and every expert in this field has his wonderful way to embellish the delicious [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , huuuuuuuuuuuuuum. 

this time, and for my part, it's in **edition: The pen, the book of Hafidha "the flavors of hafidha"** that I saw this beautiful and beautiful decoration, so I share with you the recipe of this Algerian Cake Skandraniette, knowing that I preferred to use the dough that I like most, and the stuffing, each one his taste.   


**Algerian cake Skandraniette**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/skandraniette-gateaux-algeriens_thumb1.jpg)

**Ingredients** for the pasta: 

  * 250 gr of flour 
  * 75 gr of margarine 
  * 1 pinch of salt 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of white dye 
  * 1 teaspoon of dye (color of your choice, here red, and blue) 
  * Orange blossom water + water 

the joke: 
  * 3 measures of ground almonds (I use a 200 ml bowl) 
  * 1 measure of crystallized sugar 
  * 1 cup of vanilla coffee 
  * egg as needed 

decoration: 
  * honey 
  * silver beads 
  * egg white 
  * shiny food 



**Realization steps** preparation of the dough: 

  1. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  2. Mix well with your fingertips 
  3. take a little part of this mixture 
  4. in a little flower water mix, dye them to have the color of your choice 
  5. start picking up the dough with this water, 
  6. add water if you need, and knead a few seconds. 
  7. cover your dough and let it rest on the side. 
  8. Now take the rest of the mixture and collect it in a ball with a little water of flower and water. 
  9. knead a little 
  10. turn into a ball and let it rest. 

the joke: 
  1. mix the almond and the sugar, 
  2. add vanilla 
  3. whip an egg, and add it gently to the almonds 
  4. pick up the mixture, if necessary add in small quantity, another egg (no need to add everything) 

shaping: 
  1. take the non-colored pie, spread it very thinly using a baking roll (like me) or the dough machine (I did not have one in Algeria, and I can not do too much with) 
  2. shape with almond stuffing a sausage 3 cm wide, 2 cm high, and its languor, according to the length of the dough you have spread 
  3. roll the dough around the pudding, a turn and a half, and stick with the egg white 
  4. with a knife cut diamonds of the same size. 
  5. to have the leaves in two colors: 
  6. spread a little of the non-colored paste in a thong 
  7. spread the purple paste in thong 
  8. superimpose the two strips 0.5 cm one on the other 
  9. spread out again to stick them well 
  10. cut with a sheet pad 
  11. to make stick three sheets on each cake with egg white 

for the flowers: 
  1. according to the desired colors, spread out the dough finely 
  2. using a 4.5 cm diameter round dies (or 5) cut your circles 
  3. take each circle, fold it in half, then in four 
  4. give a little volume to your flower with your fingers 
  5. in the center of the cake make a hole, with the tip of a brush 
  6. insert the center of the flower in 
  7. you can put two to three flowers according to your choice. 
  8. let the cakes rest for a whole night 
  9. the next day bake in a medium-temperature oven 
  10. after cooking let cool, and dip the cakes in hot honey 
  11. decorate with pearls and shiny food 
  12. present in boxes, and good tasting 



method of preparation: 

you can see this video, to understand, otherwise: 

Thank you for your visits. 

Bonne journée 
