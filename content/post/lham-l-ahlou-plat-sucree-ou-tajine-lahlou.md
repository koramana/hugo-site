---
title: lham l Ahlou sweet dish or tajine lahlou
date: '2017-05-26'
categories:
- Algerian cuisine
- Dishes and salty recipes
tags:
- Algeria
- Morocco
- Ramadan
- Ramadan 2017
- Sweety salty
- Vegetarian cuisine
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg
---
[ ![tajine hlou lham lahlou](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg>)

##  lham l Ahlou sweet dish or tajine lahlou 

Hello everybody, 

a dish of the [ Algerian cuisine, ](<https://www.amourdecuisine.fr/categorie-12359239.html>) who never leaves the first day of the month of Ramadan, there are even people who like it to be always on their table. a very good sweet salty recipe, called Tajine hlou, or marka hlowa, or more easily: [ Lamb tajine with prunes, almonds and apricots ](<https://www.amourdecuisine.fr/article-tajine-agneau-pruneaux-amandes-abricots-97616823.html>) . 

in any case, at home, I can say that I'm the only one who will eat it, not serious, because this dish can be well preserved in the refrigerator. 

you can see more than [ ramadan recipe ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>)

and here is my little recipe, as my mother always did 

The recipe in Arabic: 

**lham l Ahlou sweet dish or tajine lahlou**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg)

portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * mutton 
  * dried prunes 
  * dried apricots 
  * some dried raisins 
  * ½ onion 
  * some butter or smen (clarified butter) 
  * 3 tablespoons of crystallized sugar 
  * 2 tablespoons orange blossom water 
  * 1 pinch of cinnamon powder or 1 stick of cinnamon 
  * 1 little saffron or dye 
  * 1 pinch of salt 



**Realization steps**

  1. arrange in a fact all the cut meat, butter or smen, chopped onion, dye, cinnamon and salt. 
  2. cover with about ½ liter of water and cook, until the meat becomes tender 
  3. in the meantime, put the prunes and apricots in the steam, being careful not to mix them, to keep the beautiful color of each of them. 
  4. cheat the raisins in orange blossom water, so that it swells well. 
  5. towards the end of cooking meat place in punches, apricots and raisins. 
  6. Add sugar and orange blossom water, cook for just a few minutes and remove from heat. 
  7. at the moment of serving, you can garnish your dish with some blanched and grilled almonds. 



[ ![tajine hlou12](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg>)

good tasting . 

for another version I recommend the delicious recipe: 

[ chbah essafra ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-chbah-essafra-102664956.html>)

for a tajine hlou recipe without meat: 

![Tajin-hlou-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajin-hlou-1.jpg) [ HOLY TAJINE WITH APPLES AND STAR ANISE, WITHOUT MEAT ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html> "tajine hlou with apples and star anise, without meat")

or: 

![tajine hlou without meat, tajine lahlou 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/tajine-hlou-sans-viande-tajine-lahlou-1.jpg) [ tajine hlou without meat ](<https://www.amourdecuisine.fr/article-tajine-hlou-viande-tajine-lahlou.html>)  
  
<table>  
<tr>  
<td>

![tajine hlou without meat, tajine lahlou 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/tajine-hlou-sans-viande-tajine-lahlou-1-150x150.jpg) [ **sweet dish, tajine lahlou.** ](<https://www.amourdecuisine.fr/article-lham-l-ahlou-plat-sucree-ou-tajine-lahlou.html>) 
</td>  
<td>

**![Tajin-hlou-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajin-hlou-1.jpg) ** [ **tajine hlou with apples** ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html>) 
</td>  
<td>

**![chbah essafra2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra2-150x150.jpg) ** [ **Chbah essafra** ](<https://www.amourdecuisine.fr/article-chbah-essafra-80666714.html>) 
</td> </tr>  
<tr>  
<td>

**![tagine with spinach 017, Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tajine-aux-epinards-017_thumb.jpg) ** [ **tagine with spinach** ](<https://www.amourdecuisine.fr/article-tajine-aux-epinards-104658865.html>) 
</td>  
<td>

**![chicken tagine with peas and eggs, Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-de-poulet-aux-petits-pois-et-oeufs-cuisine-algeri1.jpg) ** [ **chicken with peas and eggs** ](<https://www.amourdecuisine.fr/article-poulet-aux-petits-pois-et-aux-oeufs-104578606.html>) 
</td>  
<td>

**![raisins mesfouf, raisins raisins](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mesfouf-aux-raisins-secs-seffa-aux-raisins-secs_thumb.jpg) ** [ **mesfouf with raisins, seffa** ](<https://www.amourdecuisine.fr/article-mesfouf-aux-raisins-secs-103720238.html>) 
</td> </tr>  
<tr>  
<td>

**![Artichoke tajine stuffed with chopped meat and eggs](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tajine-d-artichauts-farcis-a-la-viande-hachee-et-oeufs_thum.jpg) ** [ **Tajine with stuffed artichokes** ](<https://www.amourdecuisine.fr/article-tajine-d-artichauts-farcis-97187541.html>) 
</td>  
<td>

**![stuffed cabbages- Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/choux-farcis-au-four-cuisine-algerienne_thumb.jpg) ** [ **Stuffed Cabbage** ](<https://www.amourdecuisine.fr/article-choux-farcis-103681282.html>) 
</td>  
<td>

****![pea tagine with artichokes](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tajine-de-petits-pois-aux-artichauts_thumb11.jpg) ** ** [ **pea tagine with artichokes** ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-aux-artichauts---tajine-jelbana-103530292.html>) 
</td> </tr>  
<tr>  
<td>

**![mtewem 020](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/mtewem-020_thumb.jpg) ** [ **Mtewem** ](<https://www.amourdecuisine.fr/article-mtewem-103198692.html>) 
</td>  
<td>

[ **![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb21.png) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_23.png>)   
[ **Kefta, Hassan kofta bacha, Pasha "كفته حسن باشا** ](<https://www.amourdecuisine.fr/article-35713413.html>) 
</td>  
<td>

**![tajine zitoune](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tajine-zitoune.jpg) ** [ **tajine zitoune, tajine with olives** ](<https://www.amourdecuisine.fr/article-tajine-zitoune-103305066.html>) 
</td> </tr>  
<tr>  
<td>

[ **![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_181.png) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_381.png>)   
[ **Stuffed pancakes with Houriat el matbakh shrimp** ](<https://www.amourdecuisine.fr/article-37186203.html>) 
</td>  
<td>

**![sardine in tomato sauce 032](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sardine-en-sauce-tomate-032_thumb.jpg) ** [ **Sardines in tomato sauce, chtitha sardine** ](<https://www.amourdecuisine.fr/article-chtitha-sardines-103195801.html>) 
</td>  
<td>

**![trebia8](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/trebia8_thumb.jpg) ** [ **TREBIA OR TAJINE OF COURGETTES WITH EGGS** ](<https://www.amourdecuisine.fr/article-53681316.html>) 
</td> </tr>  
<tr>  
<td>

[ **![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_110.png) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_41.png>)   
[ **white chick weight sauce on a bed of eggplant** ](<https://www.amourdecuisine.fr/article-25849479.html>) 
</td>  
<td>

**![tajine-of-peas-and-chard-farcis.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tajine-de-petits-pois-et-cardons-farcis.jpg) ** [ **tajine with peas and stuffed cardoons** ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee-94791294.html>) 
</td>  
<td>


</td> </tr>  
<tr>  
<td>

**![cauliflower in white sauce](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/choufleur-en-sauce-blanche_thumb.jpg) ** [ **Cauliflower in white sauce** ](<https://www.amourdecuisine.fr/article-chou-fleur-en-sauce-blanche-90903107.html>) 
</td>  
<td>

[ **![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_21.png) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_61.png>) [ chilli hot chicken ... .. oven-roasted chicken ](<https://www.amourdecuisine.fr/article-chilli-hot-chiken-poulet-piquant-au-four-44410988.html>) 
</td>  
<td>

**![032 ram](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ram-032_thumb.jpg) ** [ **STUFFED CREPES WITH CHICKEN** ](<https://www.amourdecuisine.fr/article-crepes-farcies-au-poulet-55641173.html>) 
</td> </tr>  
<tr>  
<td>

**![baked dolma 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/dolma-au-four-2_thumb.jpg) ** [ **DOLMA, VEGETABLES FILLED WITH OVEN-FRIED MEAT** ](<https://www.amourdecuisine.fr/article-dolma-legumes-farcis-a-la-viande-hachee-au-four-66035809.html>) 
</td>  
<td>

[ **![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_32.png) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_81.png>)   
[ **ROASTED CHICKEN WITH LEMON AND HONEY** ](<https://www.amourdecuisine.fr/article-33181249.html>) 
</td>  
<td>

**![chakchouka of malika](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/chakchouka-de-malika_thumb.jpg) ** [ **chakchouka, onions with tomatoes and eggs** ](<https://www.amourdecuisine.fr/article-chakchouka-104828146.html>) 
</td> </tr>  
<tr>  
<td>

[ **![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_411.png) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_101.png>) [ **batata kbab, or baked potato, white sauce** ](<https://www.amourdecuisine.fr/article-35393661.html>) 
</td>  
<td>

[ **![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_611.png) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_141.png>)   
[ **typically Algerian chicken rice, Algerian dish** ](<https://www.amourdecuisine.fr/article-25345566.html>) 
</td>  
<td>

[ **![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_811.png) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_181.png>)   
[ **chicken legs with baked vegetables «دجاج بالفرن** ](<https://www.amourdecuisine.fr/article-36629770.html>) 
</td> </tr>  
<tr>  
<td>

**![rice-sauce-provencale.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/riz-sauce-provencale.jpg) ** [ **PROVENÇAL SAUCE RICE** ](<https://www.amourdecuisine.fr/article-riz-sauce-provencale-74327306.html>) 
</td>  
<td>

** **![olives 017](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/olives-017_thumb.jpg) **   
** [ **MUSHROOM OLIVES** ](<https://www.amourdecuisine.fr/article-olives-aux-champignons-58704544.html>) 
</td>  
<td>

**![rice with peas 6](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/riz-aux-petits-pois-6_thumb.jpg) ** [ **SPICY RICE WITH SMALL PEAS AND CHICKEN** ](<https://www.amourdecuisine.fr/article-riz-epice-aux-petits-pois-et-poulet-70463803.html>) 
</td> </tr> </table>
