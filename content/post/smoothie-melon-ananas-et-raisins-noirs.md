---
title: pineapple melon smoothie and black grapes
date: '2016-05-09'
categories:
- boissons jus et cocktail sans alcool
tags:
- Juice
- Fast Food
- Healthy cuisine
- Drinks
- Summer kitchen
- Ramadan 2016
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-melon-ananas-et-raisins-noirs-3.jpg
---
[ ![pineapple melon smoothie and black grapes 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-melon-ananas-et-raisins-noirs-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-melon-ananas-et-raisins-noirs-3.jpg>)

##  pineapple melon smoothie and black grapes 

Hello everybody, 

A smoothie is always welcome in the morning of happiness, it is so good and rich, and above all it is a safe fruit, hihihih. As for this smoothie with pineapple melon and black grapes, the melon and the grapes were well acidic, I speak mostly grapes, the melon when it lacked taste, so the two fruit would finish or in a fruit salad with a lot sugar (something I do not like) or otherwise jam. 

My solution was simple, because with a tablespoon of honey, and another tasty and sweet fruit, we get to have the smoothest smooth that can exist. 

This pineapple and black grape melon smoothie was just too good, and although the melon lacked natural taste, we could smell it good and fresh in this smoothie. Do not hesitate in your recipes of smoothies to add a fruit or remove another, depending on the ways of edges, your smoothie will always be a delight, and a good start for a beautiful day. 

**pineapple melon smoothie and black grapes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-melon-ananas-et-raisins-noirs-5-287x300.jpg)

portions:  4  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * ½ melon 
  * 1 box of pineapple (with its juice, the juice here and pure and natural without additives or added sugar0 
  * 100 grs of black grapes 
  * honey according to taste 
  * ¼ of banana for the creamy side 
  * ice cubes 



**Realization steps**

  1. place all the ingredients in the blinder (without the skin and cut into cubes) 
  2. mix well until you have a smooth smoothie 
  3. Serve well fresh 



Note If the smoothie is too creamy to your taste, add a yoghurt of your taste, a little milk or just water. 

[ ![pineapple melon smoothie and black grapes 6](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-melon-ananas-et-raisins-noirs-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-melon-ananas-et-raisins-noirs-6.jpg>)
