---
title: Tajine with minced meatballs - spinach
date: '2017-11-26'
categories:
- Algerian cuisine
tags:
- Easy recipe
- Algeria
- dishes
- Full Dish
- sauces
- Rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-de-boulettes-de-viande-aux-epinards-1.jpg
---
![meatball tagine with spinach 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-de-boulettes-de-viande-aux-epinards-1.jpg)

##  Tajine with minced meatballs - spinach 

Hello everybody, 

This tagine with meatballs and spinach is just a treat. When I saw the recipe on facebook, a recipe posted by my friend **Souma** , and that I saw all the girls realize it, and to say how beautiful she was, I did not hesitate to realize it too. But here it is, I made it for dinner, so the photos are not at the top of my friends, hihihihi 

This dish or Tajine with minced meatballs-spinach was unanimous at home, I who could have my children were not going to eat spinach, well it happened like a letter to the post. 

try this delicious dish, you will not regret it. 

You can see the recipe video, it will make me happy to see you among my subscribers! 

{{< youtube 2LgPoZTivqQ >}} 

**Tajine with minced meatballs - spinach**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-de-boulettes-de-viande-aux-epinards-2.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** for the sauce: 

  * chicken pieces according to the number of people 
  * 1 grated onion 
  * 3 tomatoes, diced 
  * 1 tablespoon of tomato paste 
  * 1 bay leaf 
  * salt, black pepper, paprika, ginger powder, coriander powder 
  * thyme, ras el hanout. 
  * 2 tablespoons cumin powder 
  * a nice chickpea handle in a box 

for spinach balls: 
  * 100 to 150 gr of minced meat 
  * 3 bunches of spinach 
  * 1 bunch of chopped coriander 
  * 1 bunch of chopped parsley 
  * 1 glass of cooked rice 
  * 1 to 2 cloves of crushed garlic (according to your taste) 
  * the same spices already used in the sauce 
  * 2 eggs 
  * a little flour 
  * oil for frying 



**Realization steps**

  1. In a pot, add the oil, add the chicken pieces, the chopped onion and the garlic 
  2. fry, add the spices, the tomato pieces and the tomato paste 
  3. cook over low heat, then add bay leaf a little water and chickpeas. 
  4. cook until the chicken is cooked through and the sauce is partially reduced. 
  5. prepare the meatballs, cook the steamed spinach. 
  6. let all the water escape and allow to cool. 
  7. mix all the ingredients, form balls. 
  8. roll in the flour, then cook in a hot oil bath. 
  9. drain the meatballs in an absorbent paper 
  10. place the meatballs in the sauce, let simmer for another 10 minutes and turn off the heat. 



![meatball tagine with spinach 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-de-boulettes-de-viande-aux-epinards-3.jpg)
