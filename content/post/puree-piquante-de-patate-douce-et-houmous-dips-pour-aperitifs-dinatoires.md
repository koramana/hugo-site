---
title: hot mashed sweet potato and hummus / dips for dinatory aperitifs
date: '2012-12-30'
categories:
- Algerian cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches

---
Hello everybody, 

here is a puree or [ dips ](<https://www.amourdecuisine.fr/categorie-12358875.html>) for lovers of **sweet savory flavors** , and which marvelously accompanies your [ tapas, aperitifs ](<https://www.amourdecuisine.fr/categorie-10678929.html>) and grills. this dips is based on [ Yam ](<https://www.amourdecuisine.fr/article-que-faire-avec-la-patate-douce-100740149.html>) , mix with a delicious **homemade hummus** , and whose taste is raised with a thin layer of **Espelette pepper** .   
Ingredients: 

  * 2 **Sweet potatoes** averages 
  * 3 cloves of garlic 
  * 1 can of 400 g precooked chickpeas, rinsed and drained 
  * 60 ml of tahini 
  * the pressed juice of 1 lemon 
  * 2 tablespoons olive oil 
  * 1 tablespoon cumin 
  * 1 teaspoon cinnamon 
  * 1/4 tsp of Espelette pepper (or according to your taste) 
  * 1/2 teaspoon of salt 



#  preparation of **homemade hummus**

In the bowl of a blinder, place cloves of garlic, chickpeas, add tahini, lemon juice, olive oil, spices and salt. 

Puree smooth. 

**preparation of the dips** : 

  1. clean and wash the sweet potatoes, 
  2. wrap in foil, and bake in preheated oven for 30 minutes or until tender. 
  3. at the end of cooking, remove the skin. 
  4. add the sweet potato to the bowl of the blinder with the puree of hummus and mix well 
  5. present this puree with a thin layer of Espelette pepper, and if you like, decorate with a little olive oil. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
