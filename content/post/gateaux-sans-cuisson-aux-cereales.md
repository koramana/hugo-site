---
title: Cakes without cooking with cereals
date: '2011-11-12'
categories:
- diverse cuisine
- Dishes and salty recipes
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/011_thumb11.jpg
---
##  Cakes without cooking with cereals 

Hello everybody, 

These cakes without cereal cooking are an easy recipe and easy to make. This recipe must be available to everyone! 

It is a cake without cooking, very melting and super good. and as I get a lot of questions and requests for super quick recipes to make, I liked to share with you one of my recipes of cakes in express cooking!   


**Cakes without cooking with cereals**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/011_thumb11.jpg)

**Ingredients**

  * 200 grs of pastry chocolate 
  * 80 grams of butter 
  * 150 ml of milk 
  * 2 glasses and a half of icing sugar 
  * 2 glasses of grated coconut 
  * 2 grilled and crushed almond tea glasses 
  * 3 beaten eggs 
  * the lemon peel 
  * 1 teaspoon vanilla 
  * Cereals as needed 
  * coconut for decoration 



**Realization steps**

  1. in a saucepan, put the chocolate to melt with the butter and the milk, stir well 
  2. add the beaten eggs and the zest and stir, constantly for 5 minutes. 
  3. off the heat now, add the sugar, vanilla, coconut and almonds 
  4. coarsely crush the cereals and add them to the mixture, until the mixture begins to pick up, but not until the mixture becomes hard, as the chocolate and butter will harden after 
  5. form balls and roll them in the coconut (it's not going to be an easy task, because the mixture is very melting, but it will give a perfect result). 
  6. put in the fridge, for it to stand, and present in boxes 


