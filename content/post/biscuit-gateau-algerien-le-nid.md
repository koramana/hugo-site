---
title: Algerian cake biscuit nest
date: '2013-03-31'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/biscuit-algerien-052-300x225.jpg
---
##  Algerian cake biscuit nest 

Hello everybody, 

here is a Biscuit Algerian cake nest, **fondants** , **soft** , sweet on the palate, **perfumed** , very scented even, that's what I prepared for the drip of yesterday, a recipe for [ fluffy shortbread ](<https://www.amourdecuisine.fr/categorie-12344749.html>) wish, ultra fast, and very easy. 

at first I wanted to realize the **volcano biscuit** but since I did not have **peanuts** nor **almonds** , I thought about decorating the cake at the **coconut** , then, I told myself the coconut, will take a color when cooking .... so better that I leave the coconut fresh, and I decorate the **cake** with, after cooking with a little bit of [ jam ](<https://www.amourdecuisine.fr/categorie-11700642.html>) .... then another idea ... for what not to add **cocoa** , so we can see the decoration with the coconut ... .. 

a **recipe** improvised but a result of **chief** , **cocoa shortbread** to redo absolutely.    


**Algerian cake biscuit nest**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/biscuit-algerien-052-300x225.jpg)

portions:  50  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 230 gr of flour 
  * 20 grams of bitter cocoa 
  * 125 gr of butter 
  * 70 gr of sugar 
  * 2 egg yolks 
  * 1 cup of vanilla sugar 
  * ½ packet of baking powder 

for decoration: 
  * coconut 
  * [ pear and ginger jam ](<https://www.amourdecuisine.fr/article-confiture-de-poires-et-gingembre.html> "pear and ginger jam")



**Realization steps**

  1. Preheat the oven to 180 °. 
  2. In a salad bowl, mix the butter cut in small pieces with the 2 egg yolks, the sugar and the vanilla sugar. 
  3. Add the flour / cocoa mixture, and add it to a smooth dough (do not over-knead the dough). 
  4. form balls of 20 gr each 
  5. place them immediately on a baking sheet, floured or lined with baking paper 
  6. press in the middle to form a well (my daughter likes to make small prints) 
  7. cook for 5 to 10 minutes at first, if you notice that the prints disappear, take out the tray, and press again in the middle 
  8. finish cooking, knowing that the cookie cooks completely and between 15 and 20 minutes. 
  9. take out your cookies from the oven, let it cool 
  10. wrap the edges of the cake in jam, and then in the coconut 
  11. decorate the heart of the biscuit with a nice spoon of jam. 



recipe tested and approved:   
  
<table>  
<tr>  
<td>

[ ![cookie nest at Mima Bellissima](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-nid-chez-Mima-Bellissima-113x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-nid-chez-Mima-Bellissima.jpg>)

at Mima B 


</td>  
<td>

[ ![grandchildren well-cocoa-jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/petits-puits-cacao-confiture-113x150.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/petits-puits-cacao-confiture.png>)

at makla 


</td>  
<td>


</td> </tr> </table>
