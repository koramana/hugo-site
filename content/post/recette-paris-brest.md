---
title: recipe Paris brest
date: '2014-11-24'
categories:
- Buns and pastries
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- cakes and cakes
- Mina el forn
- sweet recipes
tags:
- To taste
- Chiffon cream
- Pralin

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/paris-brest-1-.jpg
---
[ ![recipe paris brest 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/paris-brest-1-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/paris-brest-1-.jpg>)

##  to succeed the recipe of Paris brest with praline chiffon cream 

Hello everybody, 

I leave the nun has the old one in her place, and even if he will ask me to realize it when he goes to see **the show the best pastry chef** Tonight I will not do it. He has beautiful minis **Paris-brest has cream praline chiffon** that I've concocted just for him, because personally and since I was little I never liked these cakes based on dough choux, I like as a bit of them [ miles ](<https://www.amourdecuisine.fr/article-millefeuilles-fait-maison-mille-feuilles.html> "homemade millefeuilles, mille feuilles") it was the first thing that I stole from the pastry box that my father regularly bought, and I left it to my brothers and sisters. 

And even if my **Paris Brest** are stuffed with my delicious [ creme chiffon praline ](<https://www.amourdecuisine.fr/article-creme-mousseline-pralinee.html> "cream chiffon praline") I will not be tempted, I tell you ... Ok. 

I still have to admit that I really like making the recipe for choux pastry, and I think it's the easiest recipe in French pastry, especially if you compare it with puff pastry, hihihiihih 

before you pass my recipe paris brest i ids something: do not laugh too much on the photos, but I am very clumsy with the bag sleeve .... and you, how do you like your Paris brest, do you like them with crème chiffon praline, or you have another cream to put in? 

So to you my recipe paris brest: 

**recipe Paris brest**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/paris-brest-2.jpg)

portions:  8 to 10  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients** for choux pastry: 

  * 250 ml of water 
  * 100 gr of butter 
  * 1 pinch of salt 
  * 1 tablespoon of sugar 
  * 150 gr of flour 
  * 4 to 5 eggs 

Feed: 
  * [ creme chiffon praline ](<https://www.amourdecuisine.fr/article-creme-mousseline-pralinee.html> "cream chiffon praline")

decoration: 
  * flaked almonds 
  * icing sugar 



**Realization steps** Preparation of the choux pastry: 

  1. Bring the water and butter to a boil. 
  2. Remove from heat and add the flour all at once. 
  3. Stir the flour in this liquid with a wooden spoon until it becomes a smooth paste and put back on low heat. 
  4. Stir constantly until the dough comes off the wall of the pan.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-a-choux-300x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-a-choux-300x300.jpg>)
  5. Pour the dough into a salad bowl and let cool. 
  6. break and lightly beat 1 egg. and introduce it into the dough by mixing until it is fully incorporated.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-a-choux-2-241x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-a-choux-2-241x300.jpg>)
  7. Repeat with the other 3 eggs. 
  8. Arrive at the 5th egg, whip the good, and introduce it to the dough in a small amount. If your dough is shiny, do not put the 5 th egg whole, your dough is ready.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-a-choux-4-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-a-choux-4-300x225.jpg>)
  9. To know if you want a big Paris brest or mini paris brest, draw on a parchment leaf a circle of 20 cm in diameter, or circles of almost 10 cm. 
  10. Fill your pastry bag with dough and draw your circle (s) following the drawings you have made. For that, it is necessary to make the first circle, to make another circle in, and then a 3rd circle over, to have a well inflated dough with the cooking 
  11. Decorate with some flaked almonds. 
  12. preheat the oven to 180 ° C and bake your cakes for 20 to 30 minutes. 
  13. turn off the oven and let your cakes cool in with the oven door ajar.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-a-choux-5-300x287.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-a-choux-5-300x287.jpg>)
  14. Preparation of paris brest: 
  15. When the dough has cooled, cut it in half with a bread knife. 
  16. Fill the paris brest with cream praline cream placed in a pastry bag. 
  17. Put the top of the puff pastry and place in the refrigerator for at least 1 hour before eating. 
  18. During the presentation sprinkle the brest paris with a little iced sugar 



[ ![recipe paris brest 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/paris-brest-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/paris-brest-3.jpg>)
