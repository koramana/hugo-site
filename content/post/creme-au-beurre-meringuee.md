---
title: meringuee butter cream
date: '2012-12-25'
categories:
- Bourek, brick, samoussa, slippers

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/creme-au-beurre-a-la-meringue-suisse3.jpg
---
##  meringuee butter cream 

###  Swiss meringue butter cream 1: 2: 3 

Hello everybody, 

this cream butter with Swiss meringue is a very good and smooth cream, ideal for icing cakes or cupcakes, I really like to do because it was really very easy to achieve. 

I decorated [ my birthday cake ](<https://www.amourdecuisine.fr/article-le-gateau-de-mon-anniversaire-101306984.html>) with this well-aerated meringue butter cream. It's called Swiss meringue butter cream 1: 2: 3! Why **1: 2: 3** ? Because first of all it is very easy to realize, then it is done in 3 stages, and the most important thing is the proportions of the ingredients: 

when you make a certain weight of egg white, you double your weight in sugar, and triple its weight in butter, that's great. 

so for my part, I went out my egg whites freezer, because yes, if you do not know, when in a recipe you only use the egg yolk and you said: 

#  what to do with egg whites? 

do not discard them, put them in a bag in the freezer, the ideal is to place an egg white per small sachet, like that, if in a recipe you need a number of egg white, you remove the number of bags, do not defrost your egg whites in the microwave, or in hot water. Or take them out a little early from the freezer, if you remember, or place the bags tightly closed, without water at normal temperature, it will really help in defrosting. 

then we come back to cream, so like me, I take what I have as egg whites, I weigh their weight, then I multiply by two, and it will be the weight of sugar. 

I multiply the weight of the egg whites by three, and it will be the weight of the butter.   


**meringuee butter cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/creme-au-beurre-a-la-meringue-suisse3.jpg)

**Ingredients** to cover a cake of 25 cm generously: 

  * 130 gr of egg white 
  * 260 gr of sugar 
  * 390 gr of butter 
  * vanilla 
  * dye (here pink gel for me) 



**Realization steps**

  1. mix the sugar with the egg white 
  2. place it in a bain-marie, and mix, not whip. mix until the sugar grains are completely dissolved in the eouf white, you can check this by touching the mixture and between your fingers, you must not feel the sugar grain anymore. 
  3. when the mixture is well viscous it is ready, remove it from the bain marie 
  4. whip the good to have a very beautiful meringue, the ideal is to work with your trouble. 
  5. Now change the squeegee of your whip, and start beating the meringue mixture and add the butter slowly. 
  6. at some point, the mixture will attach to the whip squeegee, do not worry, you do not miss your cream, keep whipping while adding the butter, and you'll have a nice butter cream . 
  7. now add the vanilla, and the dye. 
  8. your cream is ready, it is smooth, you can decorate your cupcakes and your cakes. 



  


![butter cream with Swiss meringue](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/creme-au-beurre-a-la-meringue-suisse_thumb1.jpg)

merci. 
