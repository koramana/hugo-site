---
title: peanut sauce
date: '2013-08-23'
categories:
- Algerian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/creme-au-beurre-de-cacahuete-piquante_thumb.jpg
---
[ ![peanut sauce](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/creme-au-beurre-de-cacahuete-piquante_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/creme-au-beurre-de-cacahuete-piquante.jpg>)

Hello everybody, 

here is a very good sauce or cream, to present in a barbecue party, with [ roast chicken ](<https://www.amourdecuisine.fr/article-poulet-roti-au-four-109726524.html>) , or else from [ roasted meat ](<https://www.amourdecuisine.fr/article-viande-rotie-au-citron-89050764.html>) as I did last time. 

ah, we must tolerate the spicy, because, of the spicy, there is in this recipe, hihihihiih   


**peanut sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/creme-au-beurre-de-cacahuete-piquante_thumb.jpg)

Recipe type:  dip  portions:  8  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 200 grams of hot peppers 
  * the juice of 1/2 lemon 
  * peanut butter according to your taste (from 8 to 10 tablespoons) 
  * 2 cloves garlic 
  * salt (according to taste) 



**Realization steps**

  1. clean the peppers, and especially degrade (otherwise it may be more than spicy) 
  2. put in the blinder bowl 
  3. add the garlic 
  4. mash, add lemon to mash 
  5. mix in a bowl, and add the peanut butter spoon, until you have a cream not too runny. 
  6. adjust the salt, and enjoy, with plenty of tissues, hihihiihih, ca piiiiiiiiiiiiiiiiiiiiiiique 
  7. this cream is preserved in a refrigerator in a box, it must be used in small quantity. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
