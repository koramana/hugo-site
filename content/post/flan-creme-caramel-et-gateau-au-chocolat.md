---
title: 'flan: caramel cream and chocolate cake'
date: '2012-07-22'
categories:
- recette a la viande rouge ( halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-gateau-magic-0151.jpg
---
![flan-cake-magic-015.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-gateau-magic-0151.jpg)

##  flan: caramel cream and chocolate cake 

Hello everybody, 

here is a beautiful dessert, which I often prepare for Ramadan, and I realize that I never post it, so here is a great opportunity to be invited to friends, I said I prepare the cake and I take him with me ... 

my friend laughed from the bottom of her heart when she saw me take out my camera, and the pieces of tissue for the bottom of the photo, hihihihi ... take pictures before the kids get off the ground and start asking for the biggest part . 

in any case, a cake with which you will never be disappointed, always a magical result, and an incomparable delight. 

why this cake is a magic cake, because in fact, you prepare the first layer, you prepare the second layer, pour it on the first, and when cooking, you will see two separate layers. 

Ingredients:   


**flan: caramel cream and chocolate cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gateau-au-chocolat-et-creme-caramel-a1.jpg)

**Ingredients** Caramel: 

  * 5 tablespoons of sugar (depending on the size of the mold) 

layer of caramel cream: 
  * 4 medium-sized eggs 
  * 120 ml Nestle Sugar Concentrated Milk 
  * 600 ml of milk 
  * vanilla 

layer of chocolate cake: 
  * 3 eggs 
  * 100 gr of sugar 
  * 120 ml of milk 
  * 120 ml of oil. 
  * 1 cup of vanilla coffee 
  * 1 coffee of nescafe 
  * 3 tablespoons of cocoa 
  * 1 teaspoons of baking powder 
  * 175 gr of flour 



**Realization steps**

  1. prepare the caramel, in a saucepan, cook the sugar without stirring, until you get a nice golden color 
  2. pour the caramel into the mold, which you have recommended to cook the cake (a mold of more than 20 cm in diameter is preferable, in Pyrex). 
  3. let cool well 
  4. In blender bowl, combine eggs, condensed milk, milk and vanilla until mixture is well blended. 
  5. Pour this mixture over the cold caramel, and leave aside 
  6. proceed to the preparation of the cake, heat the milk, add the nescafe, and let cool 
  7. In a bowl, beat the eggs, sugar and vanilla, until bleaching and doubling in volume 
  8. add the oil and whip again 
  9. introduce the milk to the coffee, while mixing 
  10. slowly add the mixture of flour, cocoa and baking powder. 
  11. pour this mixture, on the caramel cream machine ... trying to pour in the middle 
  12. cook in a bain-marie, bake at 180 degrees for 45 minutes. 
  13. after 45 minutes, you can control the cooking of your dessert with the point of the knife. 
  14. remove from the oven, let cool, before putting in the fridge for at least 4 hours 
  15. then spill the dish in a dish, and serve directly ... 


