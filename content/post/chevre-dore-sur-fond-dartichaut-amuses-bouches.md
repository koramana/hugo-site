---
title: Golden goat on artichoke background / amuses bouches
date: '2016-09-12'
categories:
- amuse bouche, tapas, mise en bouche
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/artichauts-chevres-096_thumb.jpg
---
![artichoke-goat-096_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/artichauts-chevres-096_thumb.jpg)

##  Golden goat on artichoke background 

Hello everybody, 

a very easy and good entry, which is prepared in the blink of an eye, a marriage between the sweetness of artichokes, and the irresistible taste of goat cheese. and do not tell yourself that you have to use fresh artichokes, because canned or frozen artichokes will do the trick, but a recipe that I will urge you to do strongly, and if you do not like , or you can not find the goat cheese, use the pie chart, it's going to be just exquisite on.    


**Golden goat on artichoke background / amuses bouches**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/artichauts-chevres-088_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 4 artichoke hearts 
  * pieces of goat cheese 
  * 1 lemon 
  * some salad leaves 
  * Salt and pepper from the mill 



**Realization steps**

  1. Steam the artichokes for about 20 minutes. 
  2. Remove leaves and hay to keep only the bottom. 
  3. Place a goat dung on each bottom. Pepper. 
  4. Arrange them in a dish and go under the grill until the cheeses are golden. 
  5. Prepare a chiffonade of salad. 
  6. Arrange on the salad the artichoke hearts with toast. and enjoy 



[ ![2012-02-12-artichoke-chevres_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/2012-02-12-artichauts-chevres_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/2012-02-12-artichauts-chevres_thumb.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter. 
