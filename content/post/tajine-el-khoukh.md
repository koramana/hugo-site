---
title: tajine el khoukh
date: '2015-02-22'
categories:
- Algerian cuisine
tags:
- Algeria
- Morocco
- dishes
- sauces
- Meatballs
- Minced meat
- Chicken

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tajine-el-khoukh.CR2_.jpg
---
![tajine el khoukh](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tajine-el-khoukh.CR2_.jpg)

##  tajine el khoukh 

Hello everybody, 

Yesterday, normally it was an evening [ chorba Frik ](<https://www.amourdecuisine.fr/article-chorba-frik.html> "chorba Frik") and [ tuna buns ](<https://www.amourdecuisine.fr/article-petits-pains-au-thon.html> "tuna buns") But at the last minute my husband calls to say that he has met friends to him, that he will take them to dinner, and that it will be very nice that I can add to the menu "tajine el khoukh". 

I think my eyes came out of their seats when I heard that, my hand was in the dough, the living room looked like everything except a living room to welcome guests, you imagine after a week of vacation, and the makes it the favorite and unique place where children play, plus: tajine el khoukh ??? seriously, and I look at the time it was already 18:30, I think I was nauseated, but it was necessary to put it. 

You will tell me: why he chose "tajine el khoukh", well this ramadan, he ate this dish my husband! I did it almost every week, and sometimes twice a week. He asked for it and asked for more. But after Ramadan, and with the baby moving in all directions, I never made this delicious Algerian dish again. It takes time, and I just wanted to make dishes that simmers at once and out of the kitchen ... But here, the desire to eat this dish suddenly arises at my husband, and he finds the excuse to invite these friends to do this. 

The tajine el khoukh, is a dish of Algerian cuisine, prepared with white chickpeas sauce, either with mutton or with chicken (to taste) garnished with stuffed mashed potato dumplings minced meatballs already cooked in the sauce. These potato dumplings are then cooked in an oil bath to give them a beautiful golden color. Unfortunately for my part, I did not arrive at the perfect gilding color, because at the last minute my fryer has given up (it always happens when we are in a hurry) and on the hot plate, I'm never satisfied with frying, because the temperature never as I want it). As soon as I redo the dish, I give you new photos. 

[ ![tajine el khoukh](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tajine-el-khoukh-3.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-tajine-el-khoukh.html/tajine-el-khoukh-3-cr2>)   


**tajine el khoukh**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tajine-el-khoukh-2.CR2_.jpg)

portions:  4-5  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients**

  * 4 to 5 chicken legs 
  * 1 generous handful of chickpeas dipped the day before or in a box 
  * 1 onion 
  * Salt, black pepper 
  * 1 tablespoon of smen 
  * 4 medium potatoes 
  * 20 gr Parmesan (optional) 
  * 400 gr of minced meat 
  * 2 egg yolks 
  * some sprigs of chopped parsley 
  * ¼ cup of cumin 
  * crumb of bread deceived in milk 
  * 1 clove of garlic 
  * Oil for frying 
  * 2 egg white 
  * flour 



**Realization steps**

  1. Clean and wash the chicken pieces, 
  2. in a pot, place the chicken pieces, the smen, the chopped onion, the black pepper, the salt and let it return on low heat. 
  3. Add 1 liter of water and boil, add the chickpeas if they are cheated the day before and cook. 
  4. Meanwhile peel, wash and cut the potatoes into small cubes. 
  5. place them in a saucepan filled with salt water and let them boil, watch for the potato to be overcooked and absorb the water. 
  6. Drain the potato after cooking and crush with a fork. 
  7. Add Parmesan (which is not an ingredient in the original recipe, but it gives a good taste) grated 
  8. Add an egg yolk, salt and black pepper, and mix well, always with the fork. Let cool and set aside. 
  9. In a salad bowl, mix the minced meat, with the other egg yolk, the crushed garlic clove, the black pepper, the chopped parsley, the salt, and the cumin 
  10. Add the crumb of bread drained of its milk, and mix well the meat, but not too much. 
  11. Form small pellets the size of a peach kernel. 
  12. and place these dumplings in the boiling white sauce. 
  13. of their cooking, remove and let drain and cool. 
  14. take a small piece of potato, spread it gently in the apple of your hand, put in a minced meatball, and enclose. continue until the potato is exhausted. 
  15. If any meatballs are left, put them back in the sauce. 
  16. If the chicken legs are cooked, turn off the heat, your sauce is ready. 
  17. roll the potato dumplings, in flour, then in beaten egg white, then again in the flour. 
  18. Fry in the hot oil until you have a nice golden color. 
  19. drain on paper towels. 
  20. Place on a plate, placing sauce and chicken pieces in the center, and garnish with peach balls all around. 
  21. Decorate with chopped parsley and lemon pieces. 



[ ![tajine el khoukh](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tajine-el-khoukh-4.jpg) ](<https://www.amourdecuisine.fr/article-tajine-el-khoukh.html/tajine-el-khoukh-4>)
