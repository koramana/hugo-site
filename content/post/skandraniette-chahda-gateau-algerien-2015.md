---
title: Skandraniette chahda, Algerian cake 2015
date: '2015-04-18'
categories:
- gateaux algeriens au miel
- gateaux algeriens- orientales- modernes- fete aid
tags:
- Almond
- biscuits
- Pastry
- Algeria
- Aid
- Ramadan
- Algerian patisserie

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/skandraniette-chahda-1.jpg
---
[ ![skandraniette chahda 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/skandraniette-chahda-1.jpg) ](<https://www.amourdecuisine.fr/article-skandraniette-chahda-gateau-algerien-2015.html/skandraniette-chahda-1>)

##  Skandraniette chahda, Algerian cake 2015 

Hello everybody, 

The skandraniettes are a delicious Algerian cake made of short pastry but well kneaded to give it a little elasticity, which cover a delicious stuffing made from almonds, peanuts, still nuts, or according to the means of edges. 

This new version skandraniette chahda, which means: beehive comes from my friend **Oum Ali** I really like this new look of skandraniettes. On my blog you can find an old version of [ skandraniettes ](<https://www.amourdecuisine.fr/article-skandraniette-gateaux-algeriens.html> "Algerian cake Skandraniette") , or [ triangular skandraniettes ](<https://www.amourdecuisine.fr/article-skandraniette-en-triangle-aux-noisettes-gateau-algerien-2012.html> "hazelnut triangle skandraniette - algerian cake 2012") . 

**Skandraniette chahda, Algerian cake 2015**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/skandraniette-chahda-2.jpg)

portions:  40  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** for the pasta: 

  * 3 measures of flour 
  * 1 measure of melted smen 
  * 1 pinch of salt 
  * 1 egg 
  * vanilla 
  * Orange tree Flower water 

for the stuffing: 
  * ground almonds as needed 
  * 1 tablespoon of sugar 
  * lemon zest 
  * 2 egg yolks 
  * orange blossom water 



**Realization steps**

  1. start by making the dough, drink well. 
  2. Divide into equal balls and let stand. 

Prepare the stuffing: 
  1. mix the almond and the sugar, 
  2. add the lemon zest, stir in the egg yolks slowly and then pick up the dough with the orange blossom water to obtain a nice ball. 
  3. spread the dough finely in a fairly wide rectangle. 
  4. place a stuffing pudding on one of the ends 
  5. roll the dough over to cover the stuffing, make two turns. 
  6. cut cakes almost 5 cm long. and place them on a work surface. 
  7. to make the drawing shahda or beehive use a clean tweezers and new. 
  8. cook in a preheated oven at 180 degrees for 15 minutes or depending on the oven capacity. 
  9. after cooking let cool, and dip the cakes in hot honey 
  10. decorate with food gloss 
  11. present in boxes, and good tasting 



[ ![skandraniette chahda](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/skandraniette-chahda.jpg) ](<https://www.amourdecuisine.fr/article-skandraniette-chahda-gateau-algerien-2015.html/skandraniette-chahda>)

thank you for your visits and comments 

bonne journee 
