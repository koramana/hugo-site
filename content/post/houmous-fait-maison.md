---
title: homemade hummus
date: '2016-11-07'
categories:
- amuse bouche, tapas, mise en bouche
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Houmous-fait-maison-1.CR2_.jpg
---
[ ![Homemade hummus 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Houmous-fait-maison-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Houmous-fait-maison-1.CR2_.jpg>)

##  Homemade hummus 

Hello everybody, 

What I like hummus, but I speak well of hummus homemade, not that of the trade ... Although it is here that I discovered this recipe, but since I prepared it the first time, we do not buy it anymore ... 

Because easier than this mashed chickpea, there is no! In addition you can accommodate the recipe according to your taste, spicy, garlic, garlic, in any case the list is long ... 

I even saw a version of hummus without tahini, I must try this version, and tell you if the taste is there ... 

At home, I and my husband love hummus with [ pita bread ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html> "Lebanese bread, pita bread video") , or then [ falafels ](<https://www.amourdecuisine.fr/article-les-falafels-faits-maison-ultra-facile.html> "the homemade falafels ultra easy") , when to children, they like it with doritos, or celery branches .... it is true that it is too good.   


**homemade hummus**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/houmous-fait-maison-018.CR2_-600x429.jpg)

portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 cloves garlic 
  * 250 gr canned chickpeas, drained, liquid reserved 
  * ¾ teaspoon of salt 
  * 2 tablespoons of tahini (sesame paste) 
  * 3 tablespoons freshly squeezed lemon juice (2 lemons) 
  * 2 tablespoons water or liquid from the chickpeas 



**Realization steps**

  1. Put the robot with the steel knife on   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/houmous-fait-maison-017.CR2_-300x191.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/houmous-fait-maison-017.CR2_.jpg>)
  2. discard the garlic and chop it well. 
  3. Add the rest of the ingredients to the food processor until the hummus becomes a nice puree. 
  4. Taste, for seasoning and add more liquid if you find that the mash is a little thick. 
  5. serve fresh or at room temperature. 



[ ![homemade hummus.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/houmous-fait-maison.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/houmous-fait-maison.CR2_.jpg>)
