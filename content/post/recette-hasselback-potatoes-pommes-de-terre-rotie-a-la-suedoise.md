---
title: recipe hasselback potatoes, Swedish-style potatoes
date: '2016-08-05'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Recette-hasselback-potatoes-pommes-de-terre-r%C3%B4tie-a-la-su%C3%A9doise.jpg
---
##  ![Recipe hasselback potatoes, Swedish-style potatoes](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Recette-hasselback-potatoes-pommes-de-terre-r%C3%B4tie-a-la-su%C3%A9doise.jpg) Recipe hasselback potatoes, Swedish-style potatoes 

Hello everybody, 

A recipe very beautiful to present to young and old alike. Hasselback potatoes or Swedish-style roast potatoes are super delicious and very easy to make Lunetoiles loved to share with us. This Swedish-style potato, also known as Hasselbackpotatis. 

This recipe was present on our beach picnic rug this week, everyone loved so much that they asked for it again for dinner. So these roasted potatoes and well scented with herbs can be presented as a buffet, picnic, to accompany a small salad, it leaves like peanuts ... lol 

![Recipe hasselback potatoes, potatoes roasted Swedish 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/DSC07770.jpg)

**recipe hasselback potatoes, Swedish-style potatoes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/DSC07796.jpg)

portions:  4  Prep time:  5 mins  cooking:  40 mins  total:  45 mins 

**Ingredients**

  * small potatoes 
  * garlic, 
  * thyme, 
  * herbs of Provence 
  * olive oil 



**Realization steps**

  1. Preheat the oven to 210 ° C (th.7) 
  2. Wash your potatoes, place 2 wooden handles (wooden spoon handle) on each side (so as not to cut the potato until the end), then cut into the wooden handle, sprinkle with salt, and add what you want: garlic, thyme, herbes de provence ..., water each potato with a drizzle of olive oil and bake at 210 ° C for 40 to 50 minutes. 
  3. Monitor the cooking with a knife. 



![Recipe hasselback potatoes, Swedish-style potatoes 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/DSC07786.jpg)
