---
title: tagine of shrimp pili pili or piri piri
date: '2016-04-25'
categories:
- Unclassified
- riz
tags:
- tomatoes
- sauces
- Tomato sauce
- Fish
- Algeria
- Morocco
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tajine-de-crevette-pili-pili-1.jpg
---
[ ![tagine of shrimp pili pili or piri piri](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tajine-de-crevette-pili-pili-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tajine-de-crevette-pili-pili-1.jpg>)

##  tagine of shrimp pili pili or piri piri 

Hello everybody, 

Before you pass the recipe for this **shrimp tajine pili pili** , or just: **piri piri shrimp,** I'm sure some people are wondering what is it **pili pili** , or **piri piri** ? The **piri piri** is a small, spicy African red hot pepper. The shrimp tajine pili pili by cons is a dish very famous among our Moroccan friends ... 

The recipe is not too different from **shrimp dish with tomato sauce** , but the addition of this hot pepper, savory enhances this **piri piri prawn tajine** . My husband really likes this dish, but this time, I did not have that chill at home, and I used what I had on hand. It helped me and the kids, because the tomato sauce was not as hot as usual. I'll be more frank, I added the spice at the last minute, after removing my share and the share of my children, and I let the spice play its role in the dish of my husband, who ate and sang , hahahahah. He kept saying, um it's good, it's really hot ... 

I found the recipe for this tagine of pili pili or piri piri, a long time ago on the book of **Rachida Amahouche** , I do not remember the title of the book, and I'm too lazy to go looking for the book and you pass its title ... But in any case, the recipe since, has undergone several changes, because every time, I add or remove an ingredient based on what I have at home. 

[ ![tagine shrimp pili pili or piri piri 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tajine-de-crevette-pili-pili-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tajine-de-crevette-pili-pili-2.jpg>)   


**shrimp tajine pili pili**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tajine-de-crevette-pili-pili-5.jpg)

portions:  4 to 5  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 400 gr of shrimp shelled and deveined 
  * 2 boxes of tomatoes in pieces, or 5 tomatoes and very fresh 
  * 2 cloves garlic 
  * 1 bay leaf 
  * 1 teaspoon dried thyme 
  * ½ teaspoon powdered ginger 
  * ½ teaspoon of cumin powder 
  * ½ tsp of black pepper 
  * salt according to taste 
  * 4 tablespoons extra virgin olive oil 
  * 2 to 3 peppers pili pili 
  * a few sprigs of parsley 
  * a few sprigs of coriander 



**Realization steps**

  1. place the shrimp in a heavy bottom pan, sprinkle with oil and cook. If the shrimp is frozen it will reject a lot of water, take this water, and place aside. 
  2. Finish cooking shrimp in oil, add one to two spoons if needed. 
  3. add over the crushed garlic, and stir until it does not burn. 
  4. remove the shrimp and place on a plate. 
  5. add the tomato cut in small dice in the cooking oil of the shrimp, add over the water that you removed earlier. 
  6. Stir in the bay leaf, thyme, ginger, cumin, black pepper and salt. If you like the spice so now that you can add it. 
  7. when the sauce is a little reduced, put the shrimp in the sauce and simmer for another 10 minutes 
  8. Add the parsley and chopped coriander, mix a little and remove from the heat 
  9. Serve with fries, or rice, otherwise enjoy with a good [ homemade bread ](<https://www.amourdecuisine.fr/article-comment-faire-son-pain-maison.html> "how to make homemade bread")



[ ![tagine of pili pili shrimp or piri piri 4](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tajine-de-crevette-pili-pili-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tajine-de-crevette-pili-pili-5.jpg>)

Merci pour vos visites, et bonne journee 
