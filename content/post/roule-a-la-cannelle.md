---
title: rolled with cinnamon
date: '2017-08-02'
categories:
- Buns and pastries
tags:
- Algerian cakes
- To taste
- delicacies
- Boulange
- Breakfast
- Rolled brioche
- buns

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roul%C3%A9-a-la-cannelle-1.CR2_.jpg
---
[ ![rolled with cinnamon](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roul%C3%A9-a-la-cannelle-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-roule-la-cannelle.html/roule-a-la-cannelle-1-cr2>)

##  rolled with cinnamon 

Hello everybody, 

I really like the taste of cinnamon, and these cinnamon rolls are my favorite snack. I often bought them, but the problem with cakes and desserts in England is always too sweet. I always took my coffee without sugar, to make the balance. 

But since I started making puff pastry, I make cinnamon rolls all by myself at home, and I avoid making fondant decoration on top, because I think that nice, but it adds too much sugar to the recipe, and frankly, I do not personally see the value of this addition, because the cinnamon rolls are deliciously sweet with the stuffing of cinnamon and sugar. 

![rolled with cinnamon](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roul%C3%A9-a-la-cannelle.CR2_.jpg)

**rolled with cinnamon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roule-a-la-cannelle.jpg)

portions:  12  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients** for puff pastry leavened 

  * 450 gr of flour 
  * 1 teaspoon yeast dehydrated yeast 
  * 1 teaspoon of salt 
  * 60 g of sugar 
  * 260 ml of milk 
  * 15 gr of butter 
  * 150 g of special lamination butter at room temperature 
  * water 

for the stuffing: 
  * 4 tablespoons of melted butter 
  * 80 gr of sugar 
  * 2 teaspoons cinnamon 



**Realization steps**

  1. prepare the puff pastry 
  2. In the bowl of the robot, place the flour, salt, yeast and sugar. 
  3. Mix and add the milk and butter to the flour 
  4. Knead to obtain a smooth and homogeneous paste. 
  5. Let rise 1 hour in a temperate place 
  6. When the dough has doubled in volume, put it in the refigerator for 30 minutes to firm it, so it will be easier to work. 
  7. Take the dough out of the fridge, spread it in a rectangle, put the butter in the middle of this rectangle   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-003.jpg) ](<https://www.amourdecuisine.fr/article-35601667.html/brrrrrrrrrr-003>)
  8. fold both sides on it. Put in the fridge 10 minutes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-004-300x225.jpg) ](<https://www.amourdecuisine.fr/article-35601667.html/brrrrrrrrrr-004>)
  9. Repeat the process by turning ¼ turn, put in the fridge for 10 minutes. 
  10. Repeat for the last time (3 rounds in all) and leave in the fridge for 10 minutes. 
  11. Remove the dough from the fridge, spread it in a rectangle of almost 5 mm high 
  12. brush the surface of the dough with 4 tablespoons of melted butter 
  13. spread evenly over the sugar and cinnamon mixture. 
  14. roll the dough into a pudding, cover with cling film and leave in the fridge for 15 minutes. 
  15. remove the pudding from the fridge and cut slices of almost 1 cm and a half.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roul%C3%A9-a-la-cannelle-la-d%C3%A9coupe.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40580>)
  16. place them in a baking sheet lined with parchment paper, or brushed with a little butter. 
  17. cook in a preheated oven at 180 degrees C for 13 to 15 minutes. 
  18. at the end and for a little shine, brush with a little apricot jam heated in the microwave 



![rolled with cinnamon](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roul%C3%A9-a-la-cannelle-3.CR2_.jpg)
