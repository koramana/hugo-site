---
title: Macaroons with lemon curd
date: '2016-03-16'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- Algerian cakes- oriental- modern- fete aid
tags:
- Cakes
- To taste
- desserts
- Delicacies
- Pastry
- Home made
- lemons

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/macarons-au-lemon-curd-1.jpg
---
[ ![macaroons with lemon curd 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/macarons-au-lemon-curd-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Macarons-au-lemon-curd.jpg>)

##  Macaroons with lemon curd 

Hello everybody, 

[ ![endometriosis](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/endometriose.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/05/endometriose.jpg>)

[ ![Macaroons with lemon curd](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Macarons-au-lemon-curd.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Macarons-au-lemon-curd.jpg>)   


**Macaroons with lemon curd**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/macarons-au-lemon-curd-2.jpg)

portions:  40  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** for 40 macaroons: 

  * 200 gr of icing sugar 
  * 110 gr of almond powder 
  * 3 egg whites 
  * 30 gr of sugar 
  * Yellow food coloring 

butter cream with lemon curd: 
  * 2 untreated yellow lemons 
  * 2 eggs 
  * 50 gr of sugar 
  * 1 sheet of gelatin 
  * 50 gr of butter 



**Realization steps**

  1. Mix the icing sugar and the almond powder to obtain a very fine powder. 
  2. turn the egg whites into snow with a pinch of sugar. 
  3. When the mixture starts to foam, add gradually the 30 gr of sugar. 
  4. Slowly increase the speed of the beater and whisk until you obtain a beautiful meringue which forms a bird's beak. 
  5. Add the yellow food coloring and add the good to this meringue. 
  6. Add one-third of almond powder-sieved ice-cream mixture. 
  7. mix with a spatula to soften the mass. 
  8. Add the remaining powder and gently mix with a spatula while lifting the mass. 
  9. Make small piles of 2-3 cm in the piping bag, on a dish lined with baking paper. 
  10. Let crusts 30 minutes or more, normally the hulls do not stick to the finger touching them. 
  11. put in the oven at 170 ° C, 5 minutes closed door, then 6 minutes open door (if you notice a little steam in the oven, open a little oven door before the end of 5 minutes) 
  12. at the exit of the oven, pass a little water under the parchment paper to take off easily the shells of macaroons.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/macarons-et-creme-au-beurre-au-lemon-curd.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/macarons-et-creme-au-beurre-au-lemon-curd.jpg>)

Prepare the butter cream with lemon curd: 
  1. Squeeze the lemons and take out their juices. 
  2. In a salad bowl, pour the lemon juice, the zest of a lemon, the beaten eggs into omelets and the sugar. 
  3. place the bowl in a bain-marie, and whip without stopping until you have a nice cream. 
  4. out of the heat, add the gelatine previously softened in cold water. 
  5. Introduce the butter in a small piece while whisking. 
  6. let the cream cool to room temperature. 
  7. after cooling the hulls, decorate them with the lemon curd butter cream placed in a piping bag. Cover with another hull and reserve. 



[ ![macaroons with lemon curd 2-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/macarons-au-lemon-curd-2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/macarons-au-lemon-curd-2-001.jpg>)
