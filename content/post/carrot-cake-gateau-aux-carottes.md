---
title: carrot cake / carrot cake
date: '2012-07-25'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/carrot-cake-012_thumb1.jpg
---
#  carrot cake / carrot cake 

![carrot cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/carrot-cake-012_thumb1.jpg)

here is my delicious carrot cake, I had not done for over a year. and the recipes posted here and there for the girls have retried me. 

and so I give you my recipe: 

  * 175 gr flour 
  * 120 gr of sugar 
  * 200 gr grated carrots 
  * 175 ml of oil 
  * 100 gr of grapes 
  * 3 eggs 
  * the zest of an orange (I did not, replace with lemon zest) 
  * 1 C. a coffee of cinnamon 
  * 1/2 teaspoon nutmeg 
  * 1 teaspoon of baking powder 
  * 1 teaspoon of baking soda 
  * a pinch of salt 



preheat the oven to 170 ° C   
buttered and floured your mold.   
In a bowl mix, sugar, oil and eggs, then add the peeled and grated carrots, grapes and orange peel. 

In another bowl mix remaining dry ingredients (flour, yeast, baking soda, salt and spices).   
Add the dry ingredients to the first mixture, pour the mixture into the pan and bake in the oven at 170 degrees, about 40 minutes (depending on the oven). 

let cool and unmold   
the galacage was with royal cream, recipe  [ right here  ](<https://www.amourdecuisine.fr/article-37896589.html>)

Thank you for your visit  …… Thank you for your feedback  ............... .. Thank you for registering on my  newsletter 
