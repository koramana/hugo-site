---
title: Nutella butter cream
date: '2015-12-19'
categories:
- basic pastry recipes
- sweet recipes
tags:
- Cupcakes
- Icing
- logs
- desserts
- Cakes
- Based
- Birthday cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-au-beurre-au-nutella-1.jpg
---
[ ![nutella butter cream 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-au-beurre-au-nutella-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-au-beurre-au-nutella-1.jpg>)

##  Nutella butter cream 

Hello everybody, 

In any case, you can also see the [ citrus butter cream ](<https://www.amourdecuisine.fr/article-cupcakes-aux-agrumes.html>) , the [ vanilla butter cream ](<https://www.amourdecuisine.fr/article-la-creme-au-beurre-facile-112774625.html>) , and [ the meringue butter cream ](<https://www.amourdecuisine.fr/article-creme-au-beurre-meringuee.html>) , they are all as successful as each other, in addition to being super delicious.   


**Nutella butter cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-au-beurre-au-nutella-1.jpg)

portions:  10  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 1 glass of softened margarine 
  * 2 glasses of icing sugar 
  * 3 to 4 tablespoons of fresh cream 
  * 2 tablespoons of liquid vanilla 
  * ¾ glass of Nutella 



**Realization steps**

  1. In trouble, whip the butter 
  2. add the sugar and continue to whip, to have a well-aerated cream 
  3. add Nutella, vanilla, and crème fraîche. 
  4. Whip again well to homogenize everything. 
  5. Cool until the moment of decoration. 



[ ![nutella butter cream](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-au-beurre-au-nutella.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-au-beurre-au-nutella.jpg>)
