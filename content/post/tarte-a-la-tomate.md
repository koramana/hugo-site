---
title: tomato pie
date: '2014-01-29'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-thon-et-tomate_thumb.jpg
---
[ ![tuna and tomato pie](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-thon-et-tomate_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-thon-et-tomate.jpg>)

Hello everybody, 

a nice tomato tart flavored with tuna is always better than some tomatoes in the fridge that start to lose their beautiful shiny skin, a box of tuna half-finished, a little cream that I must consume before tomorrow. 

and if you like dirty pies, quichez and dirty cakes you can see: 

the category of [ cake, quiches and pies ](<https://www.amourdecuisine.fr/pizzas-quiches-tartes-salees-et-sandwichs>) .    


**tomato pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-tomate-thon-108_thumb.jpg)

Recipe type:  quiche, Appetizer  portions:  8  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients** the shortbread 

  * 250 grams of flour 
  * 120 grams of butter in pieces 
  * 1 egg yolk 
  * ½ cup of sugar 
  * ½ teaspoon of salt 
  * 50 ml of cold water (+ or - as needed) 

for the stuffing: 
  * ½ tuna can in oil, drained 
  * 3 medium-sized tomatoes 
  * 2 tablespoons mustard 
  * grated cheese 
  * 100 ml of fresh cream 
  * 2 eggs 
  * 1 pinch of nutmeg 
  * thyme 
  * Salt pepper 



**Realization steps**

  1. in the bowl of a blender, put the flour and butter in pieces, mix 
  2. add the egg yolk, sugar, and salt, then mix again 
  3. add the water in small quantities and mix, until the dough forms a ball (go there gently with the water) 
  4. Preheat the oven to 180 ° C. 
  5. Garnish the pie plate with the dough, 
  6. drain the tuna, and wash the tomatoes then cut into slices, salt slightly let drain to reduce its water. 
  7. Spread pie crust with 2 tablespoons mustard, sprinkle with grated cheese, 
  8. spread the tomatoes and crumbled tuna and cover again with the remaining grated cheese. 
  9. whip cream, eggs, nutmeg, salt and pepper. 
  10. Pour this mixture over the tomatoes, and garnish with a little thyme. 
  11. cook at 200 ° C for 35 min. 



## 

[ ![2012-03-03 tuna tomato tart](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2012-03-03-tarte-tomate-thon_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2012-03-03-tarte-tomate-thon_2.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
