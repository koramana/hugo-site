---
title: velvety carrot with scallop walnuts
date: '2016-01-21'
categories:
- Healthy cuisine
- soups and velvets
tags:
- Soup
- Healthy cuisine
- Algeria
- Easy cooking
- Ramadan
- Defi soups and velvets
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-de-carottes-au-cumin.jpg
---
[ ![velvety carrots with caraway](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-de-carottes-au-cumin.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-de-carottes-au-cumin.jpg>)

##  velvety carrot with scallop walnuts 

Hello everybody, 

In winter, a creamy soup or soup never leaves my table, the children are satisfied with two or spoons, but I enjoy it with. especially when my vegetables are super well mixed to give a velvety creamy. I drink it like I drink a glass of juice, and I like it a lot, especially when it's for dinner. I know that I will sleep afterwards as a baby, without having acid reflux, or indigestion problems. 

This time, it is a carrot-flavored cream soup flavored with cumin and garnished with well-poached scallops. 

**velvety carrot with scallop walnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-de-carottes-aux-noix-de-saint-jacques.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 500 gr of carrots 
  * 1 potato 
  * 1 leek 
  * whole milk (as needed) 
  * 1 C. cumin coffee 
  * Walnut St. Jacques 
  * olive oil 
  * salt pepper 



**Realization steps**

  1. cut the leek well wash in puck, and brown in a little oil 
  2. clean the carrots and scrape them, cut them roughly, and add them to the pot. add the diced potato. 
  3. cover with water, add cumin, salt and black pepper 
  4. cook until the carrots become tender. (the water must be well reduced) 
  5. squeeze the scallops with a little olive oil on both sides. Salt, pepper, and drain on baking paper. 
  6. Mix the vegetables, using a blender stand, adding the milk in a small amount until the desired consistency. 
  7. rectify the seasoning 
  8. Introduce the velvety carrots with scallops and add on top, add a touch of mill pepper and chilli if you like 



[ ![velvety carrots with scallops](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-de-carottes-aux-noix-de-saint-jacques1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-de-carottes-aux-noix-de-saint-jacques1.jpg>)
