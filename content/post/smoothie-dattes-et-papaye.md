---
title: smoothie dates and papaya
date: '2016-08-27'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Juice
- Fast Food
- Healthy cuisine
- Drinks
- Summer kitchen
- Ramadan 2016
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/smoothie-papaya-et-dattes.jpg
---
![papaya smoothie and dates](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/smoothie-papaya-et-dattes.jpg)

##  smoothie dates and papaya 

Hello everybody, 

When I realized this smoothie, I wanted to present it in a bowl smoothie bowl well stocked with chia seeds, papaya flaked almonds and others, like the [ smoothie bowl chocolate and peanut butter ](<https://www.amourdecuisine.fr/article-smoothie-bowl-chocolat-beurre-de-cacahuetes.html>) . In the end, my children were there to ask for their share, that I did not have time to do my little presentation, or a smoothie bowl as I wanted. 

At least, this smoothie is so good! I confess one thing, when I put dates in a smoothie the taste is just incomparable, try and you will see, you will not need to add sugar, because the date sugar is more than enough. 

For information, I made this recipe in January. I arranged the pictures to post them, then I do not know how I forgot it !!! I stumbled on the photos and I hope that you will love the recipe, know that you can do without oranges (it's not the season I know, hihihih), a fresh orange juice will make the 'case 

**smoothie dates and passion fruit**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/smoothie-dattes-et-papaya.jpg)

portions:  2  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 tbsp. chia seeds 
  * some toasted almonds 
  * 2 oranges in pieces (otherwise 200 ml of juice) 
  * a handful of frozen red berries 
  * 1 peeled papaya cut into pieces 
  * 5 dates 
  * ice cubes (as needed) 



**Realization steps**

  1. start by mixing the almonds and chia seeds into a fine powder   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/smoothie-de-papaya-et-dattes.jpg)
  2. add the rest of the ingredients (without the ice cubes) and mix to obtain a homogeneous mixture 
  3. towards the end add the ice cubes to have a more or less thick smoothie according to your taste 



![smoothie dates and papaya](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/smoothie-dattes-et-papaya.jpg)
