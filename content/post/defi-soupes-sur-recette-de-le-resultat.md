---
title: Defi soups, on recipe.
date: '2011-12-13'
categories:
- crepes, donuts, donuts, sweet waffles

image: http://amour-de-cuisine.com/wp-content/uploads/2011/12/veloute-de-lentilles-sur-royale-de-foie-gras.160x120.jpg
---
#  [ autumn velvet with sweet potato and squash ](<https://www.amourdecuisine.fr/article-veloute-automnal-a-la-patate-douce-et-la-courge-88167689.html>)

Hello everybody 

a month ago that we launched together on the site: Recipe.de the challenge of the month of November: Soups, 

And here's the result: 

The results : 

###  1st place 

###  2nd place 

###  3rd place 

[ ![Velouté of Lentils on Royale of Foie Gras](http://amour-de-cuisine.com/wp-content/uploads/2011/12/veloute-de-lentilles-sur-royale-de-foie-gras.160x120.jpg)   
Velouté of Lentils on Royale of Foie Gras  ](<http://www.lesoleildanslassiette.com/article-veloute-de-lentilles-sur-royale-de-foie-gras-89535119.html> "Velouté of Lentils on Royale of Foie Gras from Le Soleil on the Plate") By [ The Sun in the Plate ](<http://recettes.de/le-soleil-dans-l-assiette>)

Congratulations to the authors of the 3 winning recipes. Everyone will receive a basket of Fairtrade products offered by the cooperative [ Ethiquable ](<http://www.ethiquable.coop/>) , spécialiste du commerce équitable. Un grand merci à notre généreux sponsor ! 
