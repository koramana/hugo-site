---
title: fatira soudaniya, cone with tabbouleh
date: '2011-05-02'
categories:
- les essais de mes lecteurs

---
& Nbsp; hello everyone, here is a sublime entry, I drip at my friend Sudanese, I do not tell you, I eat 5 like a hungry, so it was light, and too good. in all I did not leave it before having the recipe, and I had to prepare a large amount for the party at school Rayan on the occasion of the marriage of the prince, well, I have to bring back my empty plates. and so I did a lot, I could not take detailed pictures, it will be for next time. so be careful what I'm going to do 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

here is a sublime entry, which I dropped at my friend Sudanese, I do not tell you, I eat 5 as a hungry, so it was light, and too good. 

in all I did not leave it before having the recipe, and I had to prepare a large amount for the party at school Rayan on the occasion of the marriage of the prince, well, I have to bring back my empty plates. 

and so I did a lot, I could not take detailed pictures, it will be for next time. 

So, the measures that I am going to give you, are for a large quantity, so better than you guess on two. 

for the pasta: 

  * 5 glasses of flour (220 ml glass) 
  * 2 sachets of baker's yeast (or 2 tablespoons) 
  * salt 1/2 glass of oil 
  * 1/4 glass milk (or 1 tablespoon milk powder) 
  * 1 egg (even if you guess on two, make an egg) 
  * lukewarm water 



for the tabouleh: 

  * 3 tablespoons of bulgur 
  * 1/2 green pepper 
  * 2 fresh medium tomatoes 
  * 1 handful of pitted green olives 
  * 1 handful of pitted black olives 
  * a little fresh mint 
  * 1/2 onion 
  * 10 sprigs of parsley 
  * the pressed juice of a lemon (or according to taste) 
  * 2 to 3 tablespoons of olive oil 
  * salt 



start with the pasta: 

  1. mix all the ingredients 
  2. knead until the dough becomes soft 
  3. let it rest, until it doubles in volume 
  4. divide into a ball 
  5. spread on a floured surface 
  6. cut into strips of 1 cm wide 
  7. roll them around a cone-shaped mold (cones) 
  8. fry, in an oil bath over medium heat 
  9. detach the cones once cooked, cones and let drain on paper 



method of preparing the stuffing: 

  1. place the bulgur in a bowl, add the boiling water over 
  2. let stand 5 min 
  3. drain and boil in moderately salted water 
  4. when it's soft, drain and let cool 
  5. wash your vegetables, and cut them into small ones 
  6. cut the olives into slices 
  7. mix all your ingredients in a salad bowl 
  8. season to taste, with salt, olive oil, and lemon juice 



decorate the cones with this stuffing, and enjoy 

thank you for your visits 

thank you for all those who continue to subscribe to the newsletter 

and thank you for your comments 

and do not forget if you have tried one of my recipes, send me a picture on this email: 

[ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

I wait 

bonne journée 
