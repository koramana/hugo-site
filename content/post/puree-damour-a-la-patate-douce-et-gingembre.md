---
title: Puree of love with sweet potato and ginger
date: '2012-04-15'
categories:
- appetizer, tapas, appetizer
- Chhiwate Choumicha
- cuisine diverse
- dips and sauces
- idea, party recipe, aperitif aperitif
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/valentin-cake-pops-0491.jpg
---
hello everyone, the approach of Valentine's Day, everyone is looking for delicious recipes, pretty and especially symbolizing this beautiful day, and this recipe that was inspired by my friend Fimere, does not can make you happy, a mashed potato puree, and carrot with a nice touch of powdered ginger and maple syrup ...... a delight to cut your breath with every bite. here are other recipes without delaying the recipe: 3 medium sized sweet potatoes peeled and cut into pieces 2 carrots of medium size & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

approaching **Valentine's day** , everyone is looking for delicious recipes, pretty and especially symbolizing this beautiful day, and this recipe that I was inspired by my friend Fimere, can only make your happiness, mashed at the [ Yam ](<https://www.amourdecuisine.fr/article-que-faire-avec-la-patate-douce-100740149.html>) , and carrot with a nice touch of powdered ginger and maple syrup ...... a delight to cut your breath with every bite. 

here are others [ vegetarian recipes ](<https://www.amourdecuisine.fr/categorie-12348553.html>)

without delay the recipe: 

  * 3 medium sized sweet potatoes peeled and cut into pieces 
  * 2 carrots of medium size 
  * 1 teaspoon of salt 
  * 2 tablespoons of maple syrup (or honey if you do not have any) 
  * 1 cup powdered ginger 
  * 1/2 teaspoon coriander powder 
  * salt and pepper 



method of preparation: 

  1. Put sweet potatoes and carrots in a saucepan, cover with water and add salt. 
  2. Cook until soft. 
  3. Drain while preserving cooking water 
  4. in the bowl of a blender, reduce the mixture to puree, adjusting the consistency to the wish with the addition of cooking water 
  5. Add maple syrup and spices, mix well. 
  6. to serve to wrap heart-shaped ramekins, or special Bavarian heart-shaped pastry molds with cling film. 
  7. fill them with mashed potatoes, let them take shape, then unmould 
  8. you can decorate with the pastry bag and with a maple syrup fillet, and crush over a little black pepper 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

recipe coming soon 

![Valentine-cake-pops-049.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/valentin-cake-pops-0491.jpg)

#  Valentine [ cake pops ](<https://www.amourdecuisine.fr/article-valentine-cake-pops-sucettes-saint-valentin-98998087.html>) , mignardise pour la saint valentin, menu saint valentin 
