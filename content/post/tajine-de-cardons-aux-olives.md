---
title: cardine tajine with olives
date: '2018-03-11'
categories:
- Algerian cuisine
- ramadan recipe
tags:
- Healthy cuisine
- Easy cooking
- Full Dish
- Ramadan 2015

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-aux-cardons-et-olives.jpg
---
[ ![tajine with cardoons and olives](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-aux-cardons-et-olives.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-cardons-aux-olives.html/tajine-aux-cardons-et-olives>)

##  cardine tajine with olives 

Hello everybody, 

You like cardoons, this low-calorie legume rich in minerals (potassium, calcium) and very rich in fiber, so here is a delicious recipe for you, a tajine all good and easy to make, tagine cardoon with olives. You can use green olives, but with purple olives this tajine is even more delicious.  taj 

cardoon with olives 

I pass you the recipe of the [ dolma khorchef or dolma de cardon ](<https://www.amourdecuisine.fr/article-tajine-de-cardons-tajine-bel-khorchef.html> "tajine of cardoons, tajine bel khorchef") , or then the [ couscous with cardoons ](<https://www.amourdecuisine.fr/article-couscous-aux-cardons-couscous-bel-khorchefs.html> "couscous with cardoons, couscous bel khorchefs") . et si vous les rélisez, n’oubliez pas de laisser un commentaire 🙂  cardine tajine with olives  cardine tajine with olives    


**cardine tajine with olives**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-aux-cardons-et-olives.jpg)

portions:  4-6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * chicken in pieces 
  * 2 cloves garlic 
  * 2 bay leaves 
  * black pepper and salt. 
  * 2 tablespoons of butter 
  * a beautiful boot of cardoons, or according to the number of people. 
  * purple olives. 



**Realization steps**

  1. place the chicken pieces in a pot with butter 
  2. grate on garlic cloves. 
  3. add bay leaves, black pepper and a little salt. 
  4. let it come back a little, then water with the equivalent of 3 glasses of water 
  5. cook for 20 minutes, 
  6. At the same time, clean the cardoons and cut them into small pieces, and blanch them in moderately salted boiling water and a few pieces of lemon. 
  7. Drain the cardoons and add them to the sauce. at this point you can remove the cooked chicken pieces. 
  8. cook the cardoons in the sauce then add the olives (already boiled to reduce the salt). 
  9. Let it simmer for a few minutes then remove from heat. 
  10. to add color to the dish, you can fry the chicken pieces in a hot oil base (fry them a little). 



[ ![cardine tajine with olives](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-cardon-aux-olives.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-cardons-aux-olives.html/tajine-de-cardon-aux-olives>)
