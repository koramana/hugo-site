---
title: Gingerbread ......... .. on the map
date: '2008-01-19'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/01/211810091.jpg
---
![bonj15](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/21181249.png)

who does not dream of this good taste of the gingerbread, this pleasant smell, and full of flavors ............ 

Well from one site to another I find a recipe that seems to me to give good taste, I hesitated a little when I saw the amount of honey in it, to which we add more sugar! 

but my friend from bahrein, does it, and told me that her children have adores, so i jump on my ingredients: 

  * 100 ml of milk 
  * 130 gr of flour T55 (strong white flour for me) 
  * 70 gr of complete flour T110 (plain flour for me) 
  * 250 gr of melted honey 
  * 100 gr of very soft butter 
  * 1 egg 
  * 50 gr of brown sugar (white sugar for me) 
  * 1/2 cc of baking soda 
  * 1 sachet of baking powder 
  * 2 tsp of spices (cinnamon, ginger, nutmeg, clove) 
  * 2 cc of almond powder (I did not use, it was deep in my closet, I did not want to look) 



I proceeded, mixing all the ingredients together, I covered the tank of my map with a baking paper, because I did not know if I could put the ingredients like this with honey inside in the tank without having of problem. 

finally, I programmed on cooking only, and 1:00 as cooking time, and I launched my map. 

![S7302049](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/211810091.jpg)

![S7302054](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/211810641.jpg)

here is my gingerbread, so we can even bake it, I wanted to see the difference between oven and map, this time I try the map, the next time I do it in the oven. 

it is really good anyway, the flavor I was looking for. 

![bonj1](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/211813121.gif)
