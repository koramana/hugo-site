---
title: Sbaa el aroussa, Algerian cakes for help
date: '2012-10-25'
categories:
- amuse bouche, tapas, mise en bouche
- idee, recette de fete, aperitif apero dinatoire
- recette de ramadan
- recettes salées

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/Sbaa-laaroussa_thumb.jpg
---
[ ![Sbaa laaroussa](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/Sbaa-laaroussa_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/Sbaa-laaroussa.jpg>)

Hello everybody, 

the  sbaa laaroussa  , or  Sbaa el aroussa  , or  sbi3ates laaroussa  , which means in French the  Fingers Of The Bride  , is a delicious  Algerian cake  in the shape of ice cream sticks that cover a crispy shortbread dough, with a delicious almond stuffing flavored with cinnamon, or lemon zest. 

a recipe that I made at the request of my daughter, who loves cakes with frosting, and moreover these cakes are very easy, compared to arayeches she likes a lot, so it suits me well 

**Sbaa el aroussa, Algerian cakes for help**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/les-gateaux-algeriens-Aid-2012-sbaa-el-aroussa-la-recette_.jpg)

Recipe type:  Algerian cake  portions:  50  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients** for the pasta: 

  * 3 measures of flour 
  * 1 measure of Ghee (smen, a little overflowing) 
  * a tablespoon of icing sugar 
  * a bag of vanilla sugar 
  * 1 part water (half water and half orange blossom water) 

for the almond stuffing: 
  * 3 measure almonds 
  * 1 measure icing sugar 
  * vanilla sugar 
  * the zest of a lemon 
  * 2 tbsp. soup of melted butter 
  * eggs 

the icing recipe: 
  * 2 egg whites 
  * 1 C. soup of lemon juice 
  * 1 tablespoons orange blossom water 
  * Icing sugar 
  * food coloring 
  * 1 teaspoon of oil 

sugar paste to decorate 

**Realization steps**

  1. start preparing the dough, mixing the flour well with the smen. 
  2. add sugar, and vanilla. 
  3. pick up the dough with the orange blossom water, until you obtain a handy paste (do not knead too much, let stand 30 minutes). 
  4. shape small balls. cover with a film paper and book.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/les-doigts-de-la-mariee-gateau-pour-l-aid_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/les-doigts-de-la-mariee-gateau-pour-l-aid_thumb.jpg>)
  5. Prepare the stuffing by mixing all the ingredients, 
  6. Form small sausages. Book. 
  7. Take the dough and roll down finely. 
  8. cut rectangles 15 cm wide, put a stuffing pudding at the edge and roll in pudding, close well the extrimites, 
  9. place as you go on a plate, and continue until the dough is used up. 
  10. bake in preheated oven, 180 ° between 20 and 25 minutes, while monitoring. 
  11. In a bowl, mix the egg whites, lemon and orange blossom water. 
  12. Gradually add the icing sugar until you get a thick icing. 
  13. Make a test on a cake. If the frosting flows, add icing sugar or if it is too thick, pour a few drops of orange blossom water. 
  14. glaze the cakes and let it dry a little before putting up your flowers in sugar paste, and continue the decor with a syringe pastry. 
  15. let it dry in the open air 



[ ![the Algerian cakes Aid el fitr 2012 Sbaa el aroussa](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/les-gateaux-algeriens-Aid-el-fitr-2012-Sbaa-el-aroussa_thum.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/les-gateaux-algeriens-Aid-el-fitr-2012-Sbaa-el-aroussa.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
