---
title: mchewek laassel with peanuts
date: '2016-09-04'
categories:
- Algerian cakes with honey
- gateaux algeriens- orientales- modernes- fete aid
- Gateaux Secs algeriens, petits fours
tags:
- Gateau Aid
- Aid cake
- Ramadan
- Ramadan 2016
- Pastry
- delicacies
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/mchewek-laassel-aux-cacahuetes-7.jpg
---
![mchewek laassel with peanuts 7](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/mchewek-laassel-aux-cacahuetes-7.jpg)

##  mchewek laassel with peanuts 

Hello everybody, 

This mchewek laassel with peanuts or hedgehog cake with honey and peanuts (I do not know if the name hedgehog has its place here, but it's the only word that jumped to my head when I wanted to translate the title, hihihihih) . So I said that these fluffy cakes and soaked in honey are super economical, very easy to make is just a treat. 

I can tell you that it was the first cake I made and succeeded the first time when I was young. At the time it was "The Star Cake" that took all the lights, because not only is it good, it is economical too, comparing it with its godfather and idol the [ almond mchewek ](<https://www.amourdecuisine.fr/article-mchewek-aux-amandes-gateau-algerien.html>) . in time I had completely forgotten this recipe, I do not know why. And here is one day my friend asks me for the recipe for mchewek laassel with peanuts, I went to get in my old recipe book .... And the recipe was there! as I had written it the first time before trying it and with the mention: tested and approved ... I did not hesitate to make the recipe for Eid 2016, and share it with you. 

![mchewek laassel with peanuts 6](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/mchewek-laassel-aux-cacahuetes-6.jpg)

**mchewek laassel with peanuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/mchewek-laassel-aux-cacahuetes-8.jpg)

portions:  30  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 50 gr of butter 
  * 1 glass of sugar 
  * 4 egg yolks 
  * the zest of a lemon 
  * 2 glasses of powdered peanuts 
  * 1 sachet of baking powder 
  * flour to pick up the mixture 
  * decorations: 
  * egg white as needed 
  * crushed peanuts as needed 
  * candied fruits to taste 
  * [ homemade honey syrup ](<https://www.amourdecuisine.fr/article-miel-maison-facile.html>)



**Realization steps**

  1. whisk together the butter, sugar and egg yolks to combine. 
  2. introduce lemon peel, peanuts and baking powder 
  3. pick up by adding the flour, the dough should be malleable but not too hard. 
  4. form dumplings the size of a medium nut. 
  5. dip them in the egg white then in the cachuettes, and place in boxes. 
  6. place the pieces of cake in a baking tin and the adjacent without tightening too tightly to avoid damaging their shapes. 
  7. cook in a preheated oven at 180 degrees for almost 15 minutes, or until the cakes turn a beautiful golden color. 
  8. As soon as you leave the oven, pour a spoon of honey over each piece, you can decorate with candied fruit. 



![mchewek laassel with peanuts 9](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/mchewek-laassel-aux-cacahuetes-9.jpg)
