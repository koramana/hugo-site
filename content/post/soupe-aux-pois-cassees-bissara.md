---
title: broken pea soup, bissara
date: '2018-02-13'
categories:
- soups and velvets
tags:
- Detox
- Diet
- Healthy cuisine
- Vegetarian cuisine
- Algeria
- Easy recipe
- Vegetable soup

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-de-pois-casses_thumb_1.jpg
---
##  broken pea soup, bissara 

Hello everybody, 

_The bissara or broken pea soup is a simple and easy recipe, but very rich it tells you ??? Broken peas are legumes rich in fiber, protein and minerals, as they have many nutritional qualities. and as we always like to have a healthy and balanced food, a velvety peas break, will be only the good come to enjoy this vegetable._

**pea soup breaks, bissara**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-de-pois-casses_thumb_1.jpg)

**Ingredients**

  * 2 bowls of split peas 
  * 1 onion 
  * 2 cloves garlic 
  * 2 portions of cheese (laughing cow) 
  * salt, black pepper, cumin and paprika 
  * a cube of chicken broth. 
  * olive oil 



**Realization steps**

  1. cut the onions into large pieces 
  2. bring back in the oil, to have a beautiful color 
  3. add the split peas, garlic, salt and spices 
  4. leave a little brown 
  5. add the chicken broth 
  6. water with enough water (¾ a liter) 
  7. cover and cook until the peas become tender. 
  8. go to the blinder, with the two portions of cheese 
  9. enjoy it while it's hot, with a drizzle of olive oil, or if you like the acid taste, with a squeezed lemon juice. 


