---
title: Algerian cake - bow ties
date: '2013-02-28'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- idea, party recipe, aperitif aperitif
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-algerien-noeud-papillon_thumb1.jpg
---
##  Algerian cake - bow ties 

Hello everybody, 

here is another recipe that I publish of my [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , a very nice recipe that I found on the book of Ben ghzal salma, edition: The feather, the holiday cakes. 

so I have to say that this is the model that I redid, the dough and the stuffing, I still love my dough, and the stuffing of course you can vary the tastes. 

so I pass you the recipe, and for shaping cakes, you can watch the video: 

attention, those who do not resist the dance, abstain !!!   


**Algerian cake - bow ties**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-algerien-noeud-papillon_thumb1.jpg)

portions:  50  Prep time:  60 mins  cooking:  15 mins  total:  1 hour 15 mins 

**Ingredients** for the pasta: 

  * 250 gr of flour 
  * 75 gr of margarine 
  * 1 pinch of salt 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of white dye 
  * 1 teaspoon of dye (color of your choice, here orange) 
  * Orange blossom water + water 

the joke: 
  * 3 measures of ground almonds (I use a 200 ml bowl) 
  * 1 measure of crystallized sugar 
  * zest of 1 lemon 
  * 1 cup of vanilla coffee 
  * egg as needed 

decoration: 
  * honey 
  * silver beads 
  * egg white 
  * shiny food 



**Realization steps** preparation of the dough: 

  1. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  2. Mix well with your fingertips 
  3. take a little part of this mixture 
  4. in a little flower water mix, dye them to have the color of your choice 
  5. start picking up the dough (let a little, to color it in white) with this water, 
  6. add water if you need, and knead a few seconds. 
  7. turn into a ball and let it rest. 
  8. add white dye to a little flower water, and collect the rest of the flour with, to have the white dough. 

preparation of the stuffing: 
  1. mix the almond and the sugar, 
  2. add the vanilla, and the lemon zest 
  3. stir an egg, and add it gently to the almonds 
  4. pick up the mixture, if necessary add in small quantity, another egg (no need to add everything) 

method of preparation: 
  1. spread the orange dough on a slightly floured surface 
  2. when the dough is fine, cut with a cookie cutter 8 cm in diameter. 
  3. and cut other circles 7 cm in diameter, and 6 cm in diameter 
  4. place a small pudding on the circle of 8 cm 
  5. soak a little egg white and cover the small pudding 
  6. take the 7 cm circle, form a knot and stick it on the pudding 
  7. take the second 6 cm circle again and form another knot and glue it with egg white. 
  8. now take the white paste, and spread it finely. 
  9. form 5 cm circles, and fold it to form flowers. 
  10. make a hole in the cake and insert two or three flowers 
  11. let rest overnight 
  12. the next day bake in a preheated oven at 150 degrees for 15 minutes 
  13. after cooking let cool, and dip the cakes in hot honey 
  14. decorate with pearls and shiny food 
  15. present in boxes, and good tasting 



Thank you for your visits and your comments 

bonne journée 
