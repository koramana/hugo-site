---
title: 'khobz dar: homemade bread'
date: '2014-09-27'
categories:
- Algerian cuisine
- diverse cuisine
- idea, party recipe, aperitif aperitif
- Dishes and salty recipes
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/khobz-dar-3.jpg
---
[ ![khobz dar](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/khobz-dar-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/khobz-dar-3.jpg>)

##  Khobz dar: homemade bread 

Hello everybody, 

I tell you my friends (es) this recipe of khobz dar: homemade bread, for two reasons, the first is that I noticed today, that this article was published the last time, the option: "let a comment "was unchecked (that means the reader could not leave a comment). It's amazing, I do not know how this thing happened, in any case the only comments that were on this, came from facebook .... 

The second reason for going back to this article is the approach of the [ Aid el Kebir ](<https://www.amourdecuisine.fr/article-tag/recette-de-laid-el-kbir-2014>) , or as it is called the sheep festival, and the tradition in some Algerian regions, like the Algerian East where I grew up, wants the women to prepare a good amount of khobz dar the day before the Aid, for then accompany the good dishes that these women will concoct with the mutton meat Aid. 

By your home, what is the tradition? 

Still, at home at home Khobz Dar must be done at least once a week, children love to taste their afternoon tea with a piece of homemade bread. For my husband, a [ soup ](<https://www.amourdecuisine.fr/categorie-10678936.html>) must always be accompanied by a **[ homemade bread ](<https://www.amourdecuisine.fr/pain-pain-traditionnel-galette>) ** . 

then I give you the recipe for the bread that I like to prepare, a super light homemade bread, super soft, and too good. 

more 

You can see in video another recipe of khovz dar well sanded of the traditional Algerian cuisine: 

**khobz dar**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/khobz-dar-4.CR2_.jpg)

portions:  8  Prep time:  45 mins  cooking:  30 mins  total:  1 hour 15 mins 

**Ingredients** for a big bread: 

  * 300 gr of semolina 
  * 300 gr of flour 
  * 3 tablespoons of oil 
  * 1 tablespoon of sugar 
  * 1 tablespoon of instant baker's yeast 
  * 1 cup of baking powder. 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 1 well-whipped egg 
  * Senoudj (seed of nigella) 
  * Sesame seeds 
  * lukewarm water 



**Realization steps** you can work in a robot or a bread machine, or else by hand: 

  1. put the semolina and the flour in a terrine, 
  2. add the oil, and sand in your hands. 
  3. add salt, sugar, milk powder, nigelele seeds and knead 
  4. add the two yeasts and begin to wet your mixture with warm water, while oiling   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/1976904911.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/1976904911.jpg>)
  5. add the beaten egg, and stir well to introduce it into the mixture 
  6. add the water slowly and gently, until you have a very soft and manageable paste,   
the more your dough and well kneaded and wet, the more your bread will be very light and very good. 
  7. leave to rest in a warm place protected from drafts, 
  8. after 1 hour, degas your dough, oil a baking tray, 
  9. spread the dough on it,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/197693561.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/197693561.jpg>)
  10. let it sit again, until bread swells well, 
  11. cover the surface with an egg yolk beaten with a little milk, decorate with seeds of nigella, and grains of sesames, 
  12. Cook in a hot oven at 180 degrees, until you have a nice color. 



at the end of cooking, let it cool slightly before cutting your bread. 

and enjoy your meal. 

# 

[ ![homemade bread, khonz dar](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/khobz-dar.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/khobz-dar.CR2_.jpg>)

Thank you for your visit, 

and if you have one of my recipes, do not forget to share the picture on this link: 

[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous>)

If you have a good recipe to share with us, it will be on this link: [ Proposez votre recette ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")
