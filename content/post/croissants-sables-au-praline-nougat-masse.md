---
title: Shortbread croissants with praline / nougat
date: '2015-01-22'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- sweet recipes
tags:
- Algerian cakes
- Dry Cake
- Cookies
- Economy Cuisine
- biscuits
- To taste
- fondant

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/croissants-sabl%C3%A9-au-pralin%C3%A9-3.jpg
---
[ ![Shortbread croissants with praline / nougat](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/croissants-sabl%C3%A9-au-pralin%C3%A9-3.jpg) ](<https://www.amourdecuisine.fr/article-croissants-sables-au-praline-nougat-masse.html/sony-dsc-261>)

##  Shortbread croissants with praline / nougat 

Hello everybody, 

Here are Croissants shortbread praline / nougat mass (nougat Muss: to _not to be confused with nougat! it's quite firm praline that is found exclusively in Germany,_ _if you can not find one, replace with praline paste)_

Personnelement, j’ai eu la chance de gouter a ce Nougat muss que Lunetoiles m’avait envoyé, et j’avoue que gourmande que je suis, avec mes enfants, on a mangé ce Nougat muss a la cuillère, et ca n’a pas fait long feu… qu’est ce que ça me manque 🙄 ) 

**Shortbread croissants with praline / nougat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Croissants-sabl%C3%A9s-au-pralin%C3%A9-Nougatkipferl-1.jpg)

portions:  60  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 100 g butter at room temperature, 
  * 200 g of Mass Nougat * (Nuss Nougat)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/nuss-nougat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/nuss-nougat.jpg>)
  * 250g of flour, 
  * 1 egg 
  * ½ teaspoon baking powder, 
  * 150 g extra dark chocolate 70% 



**Realization steps**

  1. Melt the praline (Nuss nougat) in a bain-marie at a gentle temperature. 
  2. Mix the softened butter softened with the mass nougat (praline) in order to obtain a homogeneous and smooth mixture. 
  3. Add the egg and mix again vigorously to incorporate it perfectly. 
  4. Add the flour and yeast mixture, mix and form a ball of dough. 
  5. Wrap the dough in foil and put in the fridge for at least 1 hour or overnight. 
  6. At the end of this time, preheat the oven to 180 °. 
  7. Collect the ball of dough, knead it a little to soften it. 
  8. Take a little dough and roll into a cylinder of 0.5 to 0.7 cm in diameter, cut out sections of 6 cm long and shape them into small croissants by refining a little the ends. 
  9. Or use a croissant mold as I used if you have one.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/faconner-les-croissants.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/faconner-les-croissants.jpg>)
  10. Place the croissants on the baking sheet covered with parchment paper, spacing them slightly, cook for 10 minutes. 
  11. At the end of the oven, let the croissants cool on a rack. 
  12. Melt the chocolate in a bain-marie, dip the ends of the biscuits and let it stand on a rack. 



[ ![Shortbread croissants with praline / nougat](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Croissants-sabl%C3%A9s-au-pralin%C3%A9-a-.jpg) ](<https://www.amourdecuisine.fr/article-croissants-sables-au-praline-nougat-masse.html/sony-dsc-262>)
