---
title: msemen
date: '2016-11-04'
categories:
- cuisine algerienne
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries
tags:
- Algerian cakes
- Algeria
- Pastry
- Cakes
- To taste
- Bread
- Boulange

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemen-ou-crepes-feuilletee-1.jpg
---
[ ![msemen](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemen-ou-crepes-feuilletee-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemen-ou-crepes-feuilletee-1.jpg>)

##  msemen 

Hello everybody, 

msemen or puff pastries still known under the names: madlouka, amwarak, maarek is a delight of the traditional Algerian cuisine. And this Msemen come directly from Algeria, from my mother's kitchen. I have to take a hat off to my mother, because I have always been too lazy to make msemens and take a picture, I say it's very difficult to have the hand in the dough and on the camera Photo. 

But my mother did it and more without the help of anyone, thank you my dear mother for "Tes **msemens** I miss you so much, you're the best at cooking, god protect you. 

So here we go my dear ones for the sublime **msemens** de ma maman, et celui qui ne laisse pas un commentaire, je vais être fâché contre lui, ok 😉 lol.   


**msemen**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemenou-crepes-feuilletee-4.jpg)

portions:  20  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * 250 gr of fine semolina 
  * 150 gr of flour 
  * ½ cup of baking yeast (not chemical, I apologize) 
  * ½ teaspoon of salt 
  * more or less than 250 ml of water depending on the absorption of dry products 

For the puff pastry pancakes: 
  * 80 gr. melted butter 
  * 80 gr. oil 
  * Fine semolina 



**Realization steps**

  1. In a large terrine, mix the semolina and the flour add the salt, then the yeast. Incorporate everything together. 
  2. Introduce the warm water gently in a small amount until you have a smooth and homogeneous paste. 
  3. Knead the dough without tearing or breaking it while rolling for at least 15 minutes, continue until you have a smooth and elastic dough. 
  4. make balls the size of a beautiful mandarin 
  5. On a greased surface of the same butter-oil mixture, flatten a dough ball with the apple of your hand, while greasing it,   
[ ![msemen or crepes laminated](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemenou-crepes-feuilletee-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemenou-crepes-feuilletee-3.jpg>)
  6. spread the dough in a circular motion until it becomes fine and transparent. 
  7. grease a little with butter and oil and sprinkle with a little semolina 
  8. fold your circle of dough forming an envelope   
[ ![msemen or crepes laminated](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemen-ou-crepes-feuilletee-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemen-ou-crepes-feuilletee-5.jpg>)
  9. Place your pancakes formed on a greased tray until the exhaustion of the dough, my mother here even made melouis   
[ ![msemen or crepes laminated](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemen-ou-crepes-feuilletee.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemen-ou-crepes-feuilletee.jpg>)
  10. cook your puff pastries in a cast iron tajine, or on a greased crepe 
  11. cook on both sides until it turns a nice golden color. 



[ ![Msemen recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemen-ou-crepes-feuilletee-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/msemen-ou-crepes-feuilletee-6.jpg>)
