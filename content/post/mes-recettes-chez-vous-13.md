---
title: my recipes at home 13
date: '2011-11-01'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- sweet recipes

---
[ for mghaber, rocks in snow ](<https://www.amourdecuisine.fr/article-36147867.html>) Amina said: 

I tried the recipe of this cake lmghabar and I succeeded and it's delicious, so thank you bcp for all these recipes .......... 

on the [ Index of my Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) : Assia said 

good evening soulef   
very happy to write i tried your recipes e at each test hmd a real success thank you for opening this blug e good luck me j lives in spain e jaimerai well keep in touch with you thank you 

[ Biscuit truffles ](<https://www.amourdecuisine.fr/article-truffes-au-chocolat-et-biscuits-65846143.html>) : 

thanks   
kenza   
salam, thank you for your recipe chocolate truffles without cooking that I tried in a cooking workshop for children in a nursery school a real success 

![img_0538.jpg](http://img.over-blog.com/300x225/4/42/91/28/IMG_0538.JPG)

[ Indian rice ](<https://www.amourdecuisine.fr/article-un-riz-indien-tres-delicieux-48188385.html>) at leila's place 

salam aleycoum   
here is the photo sorry for the quality my device makes me hers!   
it was a pure regal this afternoon I eat 3 plates loool to say !!! it's been a long time since I'm looking for the recipe for Indian rice I tested and tested but they were not good at all and I can tell you that I was wasteful and when I made your recipe I did not not put chicken because I was afraid to miss !!!! 

[ Skikrates with coconut ](<https://www.amourdecuisine.fr/article-25345462.html>) at Samia 

Hello Soulef.   
I'm sending you a picture of your skikrates that I had the happiness to make for Eid el fitr (sorry, I did not have time to write to you before ...). For more speed, I used the mold of the mâamoule and I found it very practical.   
Verdict: they were EXCELLENT, melting and tasty ![= D & gt; clap](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/emo491.gif) !!! I had a crazy success 

[ rolls with mascarpone and strawberries ](<https://www.amourdecuisine.fr/article-53001970.html>) at Anais 

Hello my dear soulef,   
I just made one of your recipes: the mascarpone roll and strawberries except that I did it with milk jam anyway your roll is very soft and airy. thank you for your wonderful recipes 

[ mini pizza ](<https://www.amourdecuisine.fr/article-25345449.html>) at Linda's place 

[ frangipane pie with peaches ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-peches-85028616.html>) at Linda's place 

[ barbie cake ](<https://www.amourdecuisine.fr/article-princesse-barbie-gateau-d-anniversaire-86764345.html>) at Mounira 

and if you also try one of my recipes do not forget to send me your photos or even your comments on this email: 

vosessai@gmail.com 
