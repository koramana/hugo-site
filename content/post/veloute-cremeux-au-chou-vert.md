---
title: Creamy cream soup with green cabbage
date: '2018-01-15'
categories:
- soups and velvets
tags:
- Lentil soup
- lentils
- Soup
- Healthy cuisine
- Algeria
- Easy cooking
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-cremeuse-au-chou-vert.jpg
---
[ ![Creamy cream soup with green cabbage](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-cremeuse-au-chou-vert.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-cremeuse-au-chou-vert.jpg>)

##  Creamy cream soup with green cabbage 

Hello everybody, 

I do not often share my recipes on facebook before writing them on my blog, because I do not like girls asking me the recipe, and that I can not give it to them right away. But here we go, last week I was very happy about a small event at home, and I shared the menu of my table (something that is not in my habits). 

Well, it's something I will not regret, because coincidentally, we want to write the recipe, my laptop spits, and does not want to restart! It's been over 3 hours now that I try to restart it, but nothing .. I think he wants a visit to repairers .. My default every time I carry my photos from my camera to the pc, I erase everything from the memory card. 

So to share with you this recipe, I have recovered the photos of facebook. I hope to get the rest of the photos, and especially get my laptop. 

**Creamy cream soup with green cabbage**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/soupe-cremeuse-au-chou-vert.jpg)

portions:  3  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * a green cabbage (960 gr) 
  * 1 onion (125 gr) 
  * 1 tablespoon of butter 
  * 1 potato (125 gr) 
  * 2 glass of fat-free chicken broth 
  * 400 ml of almond milk 
  * ½ teaspoon thyme 
  * 1 bay leaf 
  * 1 to 2 tablespoons lemon juice 
  * salt and black pepper 



**Realization steps**

  1. In a pot, melt the butter 
  2. saute the cube cut onion 
  3. add the cubed potato too, the bay leaf and the thum 
  4. cut the green cabbage into pieces, add to the pot, and add the chicken broth, salt and pepper. 
  5. cook well and reduce in broth using a blender stand.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-au-chou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-au-chou.jpg>)
  6. add the almond milk as you go until you have a creamy velvety soup. 
  7. put back on the fire for a few minutes. 
  8. you can serve this soup with croutons, or pieces of chicken breast sautéed, personally I like the crunchy side of nuts, that's what I usually add over, and it's too good. 



[ ![creamy green cabbage soup](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-cremeux-de-chou-vert.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/velout%C3%A9-cremeux-de-chou-vert.jpg>)

This recipe is for three people, each part is worth 233 kcal 

[ ![calorie cabbage soup](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/calorie-soupe-au-choux.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/calorie-soupe-au-choux.png>)
