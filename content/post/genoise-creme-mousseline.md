---
title: genoise - cream chiffon
date: '2012-07-14'
categories:
- bakery
- Algerian cuisine
- Mina el forn
- pain, pain traditionnel, galette

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/genoise-078_thumb.jpg
---
![genoise - cream chiffon](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/genoise-078_thumb.jpg)

##  genoise - cream chiffon 

Hello everybody, 

a delicious [ cake ](<https://www.amourdecuisine.fr/categorie-10678925.html>) that I had prepared a while ago when I received guests, **Genoese** to the **Chiffon cream** I had prepared at the same time [ a frangipane pie with peaches ](<https://www.amourdecuisine.fr/article-tarte-frangipane-aux-peches-100148664.html>) , I wanted to prepare a [ mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouskoutchou-mouchete-87564683.html>) but as one of the girls told me that she has prepared a mouskoutchou, and that she is going to take her with her, I started thinking about another cake, I thought about the [ Basboussa ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html>) , and [ yoghurt cake ](<https://www.amourdecuisine.fr/article-ben-10-le-gateau-d-anniversaire-de-rayan-54691985.html>) .... then I wanted to do the role of **great pastry chef** , hihihihihi ... 

and it goes the result was great, apart from one thing, I had not a wider mold, or the fact that the sponge cake was a little high for each layer, for a mold like mine, I had to use 3 eggs, to have a cake that I cut in half, and have two layers just ideal, but me with 4 eggs, well I had a cake higher, suddenly, I could not make a cake of 4 diapers, but rather, a cake of 3 layers, otherwise it was going to be a mountain. 

Ingredients: 

for a tray of 22 cm by 40 cm and to cut the cake in 2 and have 2 layers. 

so these ingredients are to double, make the first sponge is pink, then when it is short cooking, prepare the second sponge cake. 

so when I say: cut in half, I'm not talking about cutting the sponge cake in half like a sandwich, but cutting it in two parts. 

[ ![genoise - cream chiffon](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/genoise-088_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/genoise-088.jpg>)

**genoise - cream chiffon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/genoise-a-la-creme-mousseline_thumb.jpg)

Recipe type:  desset  portions:  8  Prep time:  30 mins  cooking:  60 mins  total:  1 hour 30 mins 

**Ingredients** the genoise 

  * 90g flour 
  * 90g caster sugar 
  * 3 eggs 
  * ⅓ sachet baking powder 
  * 1 pinch of salt 
  * pink dye 

the sirup: 
  * 100 ml of water 
  * 100 gr of sugar 

for the muslin cream: 
  * 250 ml of milk 
  * 57 g of sugar 
  * 1 packet of vanilla sugar 
  * 2 egg yolks 
  * 30 g cornflour 
  * 100 g soft butter or margarine 



**Realization steps** preparation of the genoise: 

  1. Separate the egg whites from the yolks. 
  2. Blanch the yolks with the sugar (I make it to the robot). Add the flour. 
  3. Mount the whites until stiff and incorporate them into the machine twice. 
  4. cook at 160 ° C for about 20 minutes. 
  5. Prepare the syrup by making water and sugar 
  6. sprinkle the sponge cake with your prepared syrup, 

prepare the muslin cream: 
  1. Take the butter out of the refrigerator so that it is at room temperature. 
  2. Mix the egg yolks, cornflour and sugar in a saucepan and whisk well. 
  3. Pour the hot milk over, mix at the same time, and put back on medium heat while stirring constantly with a wooden spoon, until the mixture thickens (about 4-5 minutes). 
  4. Out of the fire add half of the butter. incorporate it well, and let cool well. 
  5. when the pastry cream is going to be at room temperature, add the other half of butter in small amount while whisking with an electric mixer, if possible until a light and foamy mass is almost white. At this point, you can add a dye of your choice, and the strawberry aroma if you want. 

cake mounting: 
  1. take the first layer of genoise, which you have sprinkled with syrup, cover it with a thin layer of cream muslin. 
  2. add the second layer of genoise, which is different in color, and cover it again with another layer of muslin cream, and do the same with the remaining layers. 
  3. decorate the cake with half a strawberry, which you can also decorate with a little strawberry jam to give it a little shine. 



[ ![genoise with muslin cream 121](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/genoise-a-la-creme-mousseline-121_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/genoise-a-la-creme-mousseline-121.jpg>)

bonne dégustation, et a la prochaine recette 
