---
title: cake with jam mderbel
date: '2016-12-15'
categories:
- gateaux algeriens- orientales- modernes- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-a-la-confiture-mderbel.jpg
---
![Cake-a-la-jam-mderbel](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-a-la-confiture-mderbel.jpg)

##  cake with jam mderbel 

Hello everybody, 

Here is a cake with mderbel jam well known in the East of the country, but still known on the Algerian territory, a delight that it does in the blink of an eye, so do not hesitate to take your ingredients, and I know that they are at your fingertips, and in the kitchen. 

And you're not going to be disappointed to have made this mderbel jam cake, because it's one of the easiest jam cakes, and it's so good, it's going to blow away ... 

**Algerian cake Mderbal ...... jam cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-algerien-a-la-confiture-mderbel.jpg)

portions:  50  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 6 whole eggs 
  * 180 gr of crystallized sugar (if you like sweet, you can go up to 200 gr, I like it with 180 gr) 
  * 300 ml of table oil (sunflower for me) 
  * 2 bags of baking powder (me it is in box so 2 cac) 
  * the zest of a lemon 
  * lemon extract (not juice, or vanilla if you do not have it) 
  * flour 
  * the jam of your taste (me strawberry, but you can do with apricot jam to which you add a red dye) 
  * orange blossom water 
  * 1 pinch of salt   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-algerien-mderbel-048_thumb1.jpg)



**Realization steps**

  1. beat the whole eggs with the sugar and the salt, until you have a good frothy mixture, add the oil, beat again.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644341-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644341.jpg>)
  2. add the lemon peel, the extract and the yeast, mix well and add the flour little by little until you have a semi-liquid dough 
  3. pour half of this paste into an oiled and floured tray (mine is 30x 40 cm)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644411-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644411.jpg>)
  4. arrange the surface with a spatula 
  5. take the jam and mix it with the orange blossom water to have a liquid consistency, with which you will cover the surface of this cake   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644451-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644451.jpg>)
  6. my layer of jam was too thin, because I thought I had a box in my closet, but with my husband, nothing remains, and so I use the bottom of a box that was in the fridge (strawberry jam that 's he loves so much ..... hahahahaha) 
  7. return now to the remaining dough, and add flour to have a malleable dough, which you can pass to the grater to cover the layer of jam   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644461-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644461.jpg>)
  8. if you find that the dough does not go well in the rasp, do not hesitate to add flour 
  9. after having covered the surface of the cake, bake until cooked at 200 degrees 
  10. brown if necessary   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644561-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644561.jpg>)
  11. When the cake comes out of the oven, sprinkle it with orange blossom water. 
  12. then it is not a watering with a spoon or a bottle, but you take your quill, you cover the mouth with two fingers, and shake the bottle to disperse the flower water on the whole surface. do not abuse anyway 
  13. cut the dough into small squares   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644621-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/234644621.jpg>)
  14. you can present this cake in boxes 



![cake-algerien-a-la-jam-mderbel-1](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-algerien-a-la-confiture-mderbel-1.jpg)

and enjoy, it's a real delight 

especially with the bottom layer, which is very soft and melting. 

I hope you enjoy this recipe. 

[ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-11410973.html>) , [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-11814728.html>) , [ Algerian pastries ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-11814728.html>)

[ cake cake 2011 ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , [ gateau de l’aid ](<https://www.amourdecuisine.fr/categorie-10678931.html>)
