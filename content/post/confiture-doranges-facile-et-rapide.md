---
title: orange jam, easy and fast
date: '2017-12-07'
categories:
- jams and spreads
tags:
- Citrus
- Christmas jam

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/confiture-dorange-2-683x1024.jpg
---
![orange jam 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/confiture-dorange-2-683x1024.jpg)

##  orange jam, easy and fast 

Hello everybody, 

Before making this orange jam easy and fast, taking advantage of the season of oranges to take full of vitamin C, hihihiihih, and as children at home hard to eat this fruit (go to know why ???) I take advantage to make a good and delicious homemade orange jam. 

I also like making orange-based smoothies, or just fresh carrot and orange juice in the morning, it's just a treat.   
This easy and quick orange jam recipe I learned from my mother who always made her reserve of various jams. 

**orange jam, easy and fast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/confiture-dorange-3-1024x636.jpg)

Recipe type:  jam, spread  cooking:  40 mins  total:  40 mins 

**Ingredients**

  * 1 kg oranges (net weight, peeled) 
  * zest of 1 single orange 
  * 800 gr of sugar 
  * 1 stick of cinnamon. 
  * ½ glass of water (optional, if your orange is not too juicy) 



**Realization steps**

  1. clean and brush the oranges. take the zest of an orange (unless you like your jam a bit more bitter, add even more orange zest) 
  2. Stack the oranges raw, leaving nothing of the white skin (this is what gives the bitterness) 
  3. cut the oranges in medium cube while keeping all the juice that flows there 
  4. leave the pips, because it contains pectin and it will help to have a jam not too flowing 
  5. Put all the zests, fruits, seeds, sugar and cinnamon in a pot and let macerate overnight 
  6. Take again your preparation and cook until the jam begins to freeze. 
  7. check the cooking by pouring a little juice into a cold saucer: it must freeze slightly. The cooking time can be longer or shorter depending on the quality of the fruit ... 
  8. Finally put in your sterilized jars, fill up to the rim and place them upside down until completely cool. 



![orange jam 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/confiture-dorange-1-683x1024.jpg)

et voila ma réserve pour cet hiver. 
