---
title: tagine of green beans with chicken and onion
date: '2012-07-18'
categories:
- recettes patissieres de base

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/har-vert-oignons_thumb.jpg
---
##  tagine of green beans with chicken and onion 

Hello everybody, 

tagine of green beans with chicken and onion: I really like green beans, and here is a very fast dish, and very delicious, and especially very practical for the hot days, and that the children will eat without claiming too much that they do not like green beans ... 

This tagine of green beans with chicken and onion is a breeze, just follow the recipe and you will tell me the result. A well balanced dish, too good, especially with a good [ matloue ](<https://www.amourdecuisine.fr/article-matloue-matlou3-matlou-pain-maison-au-tadjine.html>) I assure you that this tajine of green beans with chicken and onion will be often on your menu of the week, hihihih.   


**tagine of green beans with chicken and onion**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/har-vert-oignons_thumb.jpg)

**Ingredients**

  * pieces of chickens 
  * green beans 
  * 3 onions 
  * 4 fresh tomatoes, or canned tomatoes, in pieces 
  * 2 cloves garlic 
  * salt, black pepper, olive oil 



**Realization steps**

  1. in a pressure cooker, fry the onion cut into thin slices in length, add the pieces of chicken, salt and pepper, let return well until the onion becomes very soft. 
  2. add the garlic and tomato cut into pieces, then add the beans, well cleaned. 
  3. close the pressure cooker, and cook. 



bon Appétit 
