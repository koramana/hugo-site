---
title: butter cream
date: '2011-12-20'
categories:
- bavarois, mousses, charlottes, recette l'agar agar
- cheesecakes et tiramisus

---
![butter cream](https://www.amourdecuisine.fr//import/http://img.over-blog.com/500x375/2/42/48/75/Images/)

Hello everybody, 

here is a very good cream, which you can use to decorate your cakes, your logs, and even to make a joke of one of your beautiful birthday cakes 

  * 4 egg yolks 
  * 140 grams of sugar 
  * 30 ml of water 
  * 200 grams of butter 
  * vanilla 



method of preparation 

  1. in a saucepan place the wet sugar with the water and cook until 117 degrees (if you do not have a sugar thermometer, let the syrup cook until you start to see the big bubbles 
  2. beat the egg yolks, and gradually add the still hot syrup, in net on 
  3. whisk at medium speed to avoid hot syrup spraying 
  4. continue to whisk at high speed until the mixture has cooled down 
  5. add the butter in pieces slowly to this mixture, and the mixer should be at medium speed. 


