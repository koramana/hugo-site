---
title: Tunisian tajine with aubergines
date: '2012-07-22'
categories:
- recette a la viande rouge ( halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-tunisien-d-aubergines-012_thumb.jpg
---
#  Tunisian tajine 

[ ![Tunisian tajine with aubergines 012](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-tunisien-d-aubergines-012_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-tunisien-d-aubergines-012.jpg>)

Hello everybody, 

a recipe very easy to make, and too good at the same time, and to avoid too much fat, I prefer to grill aubergines in the oven instead of passing them to the fry. 

[ ![Tunisian tajine with eggplant and chicken](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-tunisien-aux-aubergines-et-poulet_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-tunisien-aux-aubergines-et-poulet.jpg>)   


**Tunisian tajine with aubergines**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-tunisien-d-aubergines-009_thumb.jpg)

Recipe type:  Hand  portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 1 medium eggplant 
  * half of a chicken breast 
  * 2 medium potatoes 
  * 4 cheese in portions 
  * salt, black pepper, garlic powder and paprika. 
  * 5 to 6 eggs (according to your taste) 



**Realization steps**

  1. plant the eggplant, and cut it into slices. add salt and pepper 
  2. Place the rings in a baking tray, a little oil, and put some oil on it with a brush. 
  3. grill in an oven, turning the aubergine slices on the other side, as soon as they turn a color. at the end of the cooking place them on a paper towel. 
  4. peel the potatoes, cut into cubes, salt a little and fry in a frying bath. 
  5. salt and pepper the chicken steak, and fry in a bottom of oil. then cut it in. 
  6. in a baking tin, oiled, put a layer of potato, cover with chicken cube. 
  7. spread over the cheese cut into pieces, then cover well with aubergine slices. 
  8. beat the eggs, salt, pepper, add the garlic powder (or even the crushed garlic) and a little paprika. 
  9. pour this mixture over the eggplant, and cook in a preheated oven at 180 degrees for 15 to 20 minutes, check cooking with the tip of a knife. 
  10. serve hot, warm or cold as desired, with a fresh salad. 



[ ![tajine with eggplant and chicken](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-aux-aubergines-et-poulet_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-aux-aubergines-et-poulet_2.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
