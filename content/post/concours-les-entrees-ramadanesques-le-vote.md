---
title: contest Ramadan entries, the vote
date: '2011-09-05'
categories:
- dessert, crumbles and bars
- gateaux, et cakes
- recettes sucrees

---
![Contest-Ramadan-in-Soulef.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x389/2/42/48/75/Images/)

Hello everybody, 

you can now start voting on the recipe you love most, and who participates in the contest: "Ramadan Entries". 

you can vote on the participation of your choice, leaving a comment with the number of the participation. 

only one vote per person is allowed. 

the top 10 finalists will be shown on September 10th. 

the winner will be elected by a jury and will be designated on September 12th. 

thank you for your comments, votes and visits. 

bonne journee 
