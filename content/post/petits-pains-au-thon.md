---
title: tuna buns
date: '2013-02-01'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/huit-011-300x225.jpg
---
##  tuna buns 

Hello everybody, 

so without delay, here is the recipe:   


**tuna buns**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/huit-011-300x225.jpg)

**Ingredients**

  * 300g of flour 
  * ½cap of sugar 
  * 2 tbsp. soup of milk powder 
  * 2 tbsp. yeast 
  * 1 egg (beaten, I put half in the dough and keep the rest to basting) 
  * 2 tablespoons of soft butter 
  * ½ c. a coffee of salt 
  * warm water (between 80 to 95 ml depending on your flour) 



pick up the dough by hand and knead on the map. then allow to double volume. once the dough is well raised, make long sausages and give them the shape of eight 

Let stand, baste with the remaining egg, then stuff the cavities with what you want. not too much time to think about the farce so I always keep the stuffing of kaouther: 

  * tuna, 
  * mayonnaise, 
  * olive oil (not put my tea was in the oil) 
  * a pinch of salt (not put the tuna was salted) and pepper, 
  * hard boiled eggs and cheese. 



cook in a preheated oven at 180 degrees, and enjoy the delight 

et quand vous commencer a les manger, attention, vous n’allez pas vous arrêter, je vous l’assure. 
