---
title: homemade whole grain bread
date: '2018-04-07'
categories:
- Unclassified
tags:
- Algeria
- Ramadan 2018
- Ramadan
- Khobz eddar
- Bakery
- Wholemeal flour
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/03/pain-maison-complet-3.jpg
---
![homemade whole grain bread](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/pain-maison-complet-3.jpg)

##  homemade whole grain bread 

Hello everybody, 

Among the recipes that you have asked me a lot, here is the wholemeal wholemeal bread and the seeds, a homemade bread, as soft as a beautiful khobz dar made from egg, butter and milk. But here the home-made wholemeal bread I share with you today, contains no butter, no eggs, no milk, and its texture is just sublime. 

I highly recommend this recipe bread without kneading, because not only it is easy to do, it is also too good for health, I know, I do not need to tell you all the benefits and wealth of complete wheat flour, do not the blessed contribution of linseeds and oatmeal! All that to tell you, this bread is just beautifully good from all sides! 

**homemade whole grain bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/pain-maison-complet_.jpg)

**Ingredients**

  * 300 gr of whole wheat flour 
  * 100 gr white flour 
  * 100 ml of olive oil + vegetable oil 
  * 1 C. baking yeast 
  * 1 C. coffee baking powder 
  * 1 C. honey 
  * 1 C. salt 
  * warm water to pick up the dough: between 400 and 450 ml of water 
  * 1 C. tablespoons oatmeal 
  * 1 C. black seed 
  * 1 C. roasted sesame seeds 
  * 1 C. roasted flaxseed 
  * and again these seeds for decoration. 



**Realization steps** in a large salad bowl prepare the leaven: 

  1. place 200 ml of warm water, add the yeast, white flour, sugar and a spoonful of whole flour. 
  2. mix to see a homogeneous device, cover and leave until bubbles on the surface. 
  3. take again the mixture, add the oil, the baking powder, the salt, 200 ml of warm water and the rest of the flour. 
  4. mix with a spatula, if the device is a little hard, add more water. 
  5. let it rest and double again by covering the dough. 
  6. line a baking pan with baking paper. cover with oatmeal. 
  7. place the dough on the oatmeal and spread your bread with your wet hand to have a bread of almost 1 cm in height. 
  8. garnish the top with sesame seeds, nigella and linseed 
  9. let rise, and preheat the oven to 180 ° C 
  10. as soon as the bread comes out, cook for 20 to 30 minutes, depending on your oven, until the bread is well cooked. 



![home full bread 4](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/pain-maison-complet-4.jpg)
