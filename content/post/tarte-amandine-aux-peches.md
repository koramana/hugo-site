---
title: amandine pie with peaches
date: '2016-08-18'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tarte-amandine-aux-peches-1.jpg
---
![almond pie with peaches 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tarte-amandine-aux-peches-1.jpg)

##  amandine pie with peaches 

Hello everybody, 

The amandine pies are my favorites, because they are very easy to do, and successful all the time. I admit that the [ Tarte Bourdaloue ](<https://www.amourdecuisine.fr/article-tarte-bourdaloue-recette-de-tarte-amandine-aux-poires.html>) remains my favorite, but what is good is that we can put all other fruit in the place of pears, besides you can see the different [ pies with almond cream ](<https://www.amourdecuisine.fr/article-tag/creme-damande>) which are on my blog. 

This time, I share with you the pear tart pie, a recipe from my dear Lunetoiles, that I'm sure will please you. 

![almond pie with peaches 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tarte-amandine-aux-peches-3.jpg)

**amandine pie with peaches**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tarte-amandine-aux-peches-1.jpg)

portions:  8  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients** For a pie circle 24 cm in diameter the shortbread dough: 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30 g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

Almond cream: 
  * 120 g of almond powder 
  * 2 eggs 
  * 100 g of sugar 
  * 200 ml whole liquid cream 
  * 1 jar of peaches in syrup 

Decoration: 
  * Scraped almonds 
  * 2 tablespoons strawberry jam 



**Realization steps** The shortbread dough: 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a paste is removed from the sides of the bowl and is non-sticky. 
  4. Let it rest. 
  5. Spread the dough with the roll between 2 sheets of baking paper. 
  6. Smear a pie circle 24 cm in diameter. 
  7. Stir the batter stock with a fork and chill for the time to prepare the filling. 

The almond cream: 
  1. In the bowl of the beater this time equipped with the whisk, beat the eggs with the sugar. 
  2. When the mixture is frothy add the almond powder and the cream. Beat again. 
  3. Arrange the peaches on the pie shell and cover with almond cream.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tarte-amandine-aux-peches-2.jpg)
  4. Put in the oven for 30 minutes at 180 ° C (th.6). Then sprinkle with blanched almonds 15 minutes before the end of cooking. 
  5. Take the pie out of the oven. 
  6. As soon as you leave the oven, brush the whole surface of the pie with a brush of peach syrup. 



![amandine pie with peaches](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/tarte-amandine-aux-peches.jpg) merci lunetoile pour cette recette 
