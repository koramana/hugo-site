---
title: freecycle do you know ???
date: '2009-03-27'
categories:
- Buns and pastries
- Mina el forn

---
Hello friends (is)   
yes I come for a little blah, but I come to talk about freecycle, a great website that far exceeds ebay (the ebay famine)   
eh ben freecycle, is a group located on yahoo, normally it must exist in all European countries (to my knowledge) I know that this group exists in France, in England, it must exist elsewhere, seek to know,   
what is this group: 

The worldwide Freecycle network is made up of a multitude of groups around the globe. It is a movement of people who offer (and recover) objects for free in the city where they live (and around). 

Freecycle groups connect people who want to get rid of objects that clutter them with people who need them. Our goal is to free natural areas of abandoned objects that are still useful. By using what we already have on this planet, we are reducing excessive consumerism, mass production, and reducing its harmful impact on the planet. Another advantage to using Freecycle is that it encourages us to get rid of compulsive acquisitions that we no longer use and encourages everyone to adopt a community attitude. 

#  Freecycle, how does it work? 

Any item posted must be free, legal and suitable for all ages. When you want  TO OFFER  something - whether it's a chair, a fax machine, a piano or an old door, all you need to do is send a message to your group. 

Interested people respond to the message but only the one who  OFFER  receives these messages, and the rest of the exchanges are done outside the list, by private emails.   
Then the donor decides to whom he will give the object from the answers he has received and agrees with him an appointment to come to withdraw. Once the donation is complete, the donor only remains to send a message to the group  LEFT  . 

It is also possible to post messages  NEW RESEARCH  for a member looking for a particular object. 

for freecycle in France it's here: 

you only have to look for your city, and if this group does not exist in your city, you can create a group yourself, and start to advertise it, and start a big recycling.   
pour savoir si ce groupe existe dans votre etat ou ville allez ici: 
