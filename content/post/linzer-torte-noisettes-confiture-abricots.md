---
title: linzer torte hazelnut apricot jam
date: '2016-06-30'
categories:
- cakes and cakes
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/linzer-torte-noisette-confiture-dabricots-1.jpg
---
[ ![Linzer torte apricot jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/linzer-torte-noisette-confiture-dabricots-1.jpg) ](<https://www.amourdecuisine.fr/article-linzer-torte-noisettes-confiture-abricots.html/linzer-torte-confiture-dabricots>)

##  linzer torte hazelnut apricot jam 

Hello everybody, 

Do you like tortoise linzers? My house is cheated and my children do not like it so I look at this cake from afar, deliciously prepared by my dear Lunetoiles. 

And at Lunetoiles this cake or jam pie has its place, moreover you can take a look at these other lizer tortes just as beautiful and delicious both: 

[ Linzer torte with raspberry jam ](<https://www.amourdecuisine.fr/article-linzer-torte-confiture-de-framboises.html> "linzer torte / raspberry jam")

[ linzer torte chocolate and jam of ripe ](<https://www.amourdecuisine.fr/article-tarte-de-linz-au-chocolat-et-a-la-confiture-de-mures-maison.html> "Linz chocolate tart / homemade blackberry jam")

and a recipe for me linzer version tarts: [ tartlets with fig jam ](<https://www.amourdecuisine.fr/article-tarte-a-la-confiture-de-figues-samira-tv.html> "pie with fig jam / samira tv")

I hope you like this delight, then to you the recipe of this linzer torte hazelnut and apricot jam, besides I also pass you the link of the [ homemade apricot jam ](<https://www.amourdecuisine.fr/article-confiture-d-abricots.html> "apricot jam")

PS: I apologize to the girls who posted recipes by following the link: [ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.") , the time that I arrange the introduction, and that I sign the photos and your recipe will be online very soon, and I thank you in advance of this sharing. 

[ ![Linzer torte apricot jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Linzer-torte-confiture-dabricots.jpg) ](<https://www.amourdecuisine.fr/article-linzer-torte-noisettes-confiture-abricots.html/linzer-torte-confiture-dabricots>)   


**linzer torte hazelnut apricot jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/linzer-torte-noisettes-et-confiture-dabricots.jpg)

portions:  12  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 170 g of butter 
  * 120 g of flour 
  * 80 g of whole wheat flour 
  * 60 g hazelnuts, roasted, chilled and finely ground in a food processor 
  * 1 pinch of salt 
  * 100 g of sugar 
  * 35 g whole sugar or brown sugar 
  * 1 egg 
  * ½ vanilla pod, scrape seeds and reserved (optional) 
  * 1 jar of apricot jam (360 gr) 



**Realization steps**

  1. Line a square baking tin 22x22 cm or 23x23 cm with parchment paper and set aside. Whisk together the two flours, the hazelnut powder, and salt in a bowl, set aside. 
  2. In another bowl, beat butter and both medium speed sugars with an electric mixer for about 5 minutes. 
  3. Stir in the egg and vanilla seeds (if desired). 
  4. Add the dry ingredients, and beat until everything is combined. 
  5. Remove the dough obtained from the bowl of the robot and reserve a piece of about ½ cup, which is about 170 gr, wrap the piece in food paper. 
  6. Then transfer the rest of the dough into the prepared pan and press down with a spatula to spread it out, with your floured hands spread the dough into the square pan; refrigerate both pasta for 1 hour. 
  7. After an hour of cool rest: 
  8. Heat the oven to 175 ° C. 
  9. Spread the apricot jam on the bottom of dough previously spread in the baking tin. 
  10. Take the piece of dough of 170 gr, and spread it on a floured surface generously and to the 
  11. using a knife cutter or pizza roulette, cut the dough into 10 strips; arrange five strips across, then arrange the remaining five strips of length to form a grid over the jam. 
  12. Bake until the cake is brown and golden and the jam is bubbling, about 40-50 minutes. Watch the cooking, it depends on the ovens. Let cool completely. 
  13. Unmould and cut into 36 squares. 



[ ![Linzer torte apricot jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/linzer-torte-noisettes-.jpg) ](<https://www.amourdecuisine.fr/article-linzer-torte-noisettes-confiture-abricots.html/linzer-torte-confiture-dabricots-3>)
