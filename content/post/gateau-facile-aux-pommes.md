---
title: easy apple cake
date: '2015-06-10'
categories:
- cakes and cakes
- sweet recipes
tags:
- Algerian cakes
- Pastry
- Ramadan
- Ramadan 2015
- To taste
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateau-magique-aux-pommes-1-a.jpg
---
[ ![magic cake with apples 1 a](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateau-magique-aux-pommes-1-a.jpg) ](<https://www.amourdecuisine.fr/article-gateau-facile-aux-pommes.html/gateau-magique-aux-pommes-1-a>)

##  easy apple cake 

Hello everybody, 

Here is a cake recipe that made the big buzz on our kitchen group .... Easy apple cake a recipe from one of my friends » **My daughter my life** ". 

This cake is super simple to make with ingredients available to everyone, I hope you enjoy it ... 

**easy apple cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateau-magique-aux-pommes.jpg)

portions:  8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 2 eggs 
  * 6 tablespoons of sugar 
  * 7 tablespoons of milk 
  * 4 tablespoons of oil 
  * 9 tablespoons flour 
  * 1 packet of baking powder 
  * apples as needed 



**Realization steps**

  1. whisk eggs and sugar 
  2. add milk and oil while whisking 
  3. incorporate flour and baking powder 
  4. pour the mixture into a buttered and floured mold, 
  5. Put the apple slices to decorate the top of the cake 
  6. cook in a preheated oven at 180 degrees for almost 30 minutes 
  7. at the end of the oven, allow to heat then brush the top with jam or jelly. 



[ ![magic cake with apples 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateau-magique-aux-pommes-2.jpg) ](<https://www.amourdecuisine.fr/article-gateau-facile-aux-pommes.html/gateau-magique-aux-pommes-2>)
