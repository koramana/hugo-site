---
title: kebda mchermla, or lamb liver with chermoula
date: '2018-04-19'
categories:
- Algerian cuisine
- Moroccan cuisine
- Cuisine by country
- Tunisian cuisine
- ramadan recipe
tags:
- dishes
- Ramadan 2018
- Algeria
- Easy cooking
- Ramadan
- Ramadhan
- Full Dish

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/kebda-mchermla-ou-foie-dagneau-%C3%A0-la-chermoula-1.jpg
---
![kebda mchermla, or lamb liver with chermoula 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/kebda-mchermla-ou-foie-dagneau-%C3%A0-la-chermoula-1.jpg)

##  kebda mchermla 

Hello everybody 

Here is a way to prepare lamb liver, a recipe from [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) simple and easy, which is simply **mutton liver with chermoula,** or as it's called **kebda mchermla.**

A super easy dish to make and I'm sure everyone knows how to do it, but many people in the end get a hard and dry liver, which makes the dish immersible, so here is my method of preparation for kebda mchermla , for an impeccable result, and you have as a bonus the video recipe for a guaranteed success! 

You can see even more lamb recipes like: [ baked lamb head ](<https://www.amourdecuisine.fr/article-tete-de-mouton-au-cumin-au-four.html> "head of sheep with cumin, baked") , [ loubia bel ker3ine, white bean cassoulet with lamb's foot ](<https://www.amourdecuisine.fr/article-cassoulet-d-haricots-blanc-aux-pieds-de-mouton-loubia-bel-ker3ine.html> "White bean cassoulet with lamb's feet, loubia bel ker3ine") , or [ osbana ](<https://www.amourdecuisine.fr/article-osbane-osbana-panse-farcie.html> "Osbane - osbana - Stuffed belly") ... and more [ recipe with lamb ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=mouton&sa=Rechercher>)

**kebda mchermla, or lamb liver with chermoula**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/kebda-mchermla-ou-foie-dagneau-%C3%A0-la-chermoula.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 300g of lamb liver. 
  * 3 cloves of garlic 
  * some sprigs of coriander. 
  * salt, black pepper, caraway powder and sweet paprika 
  * 3 c. tablespoon of olive oil. 
  * 2 tomatoes puree 
  * 1 C. puree tomato coffee 
  * 1 C. harissa coffee 
  * 1 C. and a half vinegar 



**Realization steps**

  1. clean the liver of lamb under abundant water and cut it in means. 
  2. In a heavy-bottomed pan, place the oil and let it warm well before adding the liver pieces. 
  3. saute the liver on all sides to have a beautiful golden color, without too much cooking. 
  4. remove the liver from the pan without cooking oil. 
  5. in the mortar, place the garlic cloves, salt, black pepper, caraway and sweet paprika. 
  6. crush everything well, and place the dersa or chermoula obtained in the cooking oil of the liver, add over a little water, so that the garlic does not burn. 
  7. then add the tomato paste and the harissa, 
  8. fry and add the fresh tomato puree 
  9. cook until a little oil appears on the surface and put the liver back in the sauce. 
  10. add on top a little fresh coriander, and vinegar. 
  11. cover and cook for 15 minutes, then remove from heat. 
  12. decorate the dish with fresh coriander before tasting. 



![kebda mchermla, or lamb liver with chermoula 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/kebda-mchermla-ou-foie-dagneau-%C3%A0-la-chermoula-2.jpg)
