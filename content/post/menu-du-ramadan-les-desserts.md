---
title: ramadan menu desserts
date: '2012-07-21'
categories:
- dessert, crumbles et barres
- Chocolate cake
- gateaux, et cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/qalb-elouz-020_thumb1.jpg
---
chamia, kalb ellouz, qalb elouz hello everyone, here is as promised an index of special desserts Ramadan, that I organized for you facilitate the task of finding my recipes easily, I join this index, has my other previous indexes: recipes From Ramadan 2012, Ramadan specials 2012, Ramadan breads, and also Boureks and bricks and there will be plenty of other indexes to come, and even those already online, are in full update. thank you for wanting to subscribe the newsletter, do not miss any new recipe on my blog. & Nbsp; makrout rolls Basboussa ktayef & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.52  (  3  ratings)  0 

![qalb elouz chamia, Algerian patisserie](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/qalb-elouz-020_thumb1.jpg)

[ Chamia kalb ellouz qalb elouz ](<https://www.amourdecuisine.fr/article-chamia-patisserie-algerienne-108117406.html>)

Hello everybody, 

here, as promised, an index of special Ramadan desserts, which I have organized for you, makes it easy to find my recipes easily, I enclose this index with my other previous indexes: [ 2012 ramadan recipes ](<https://www.amourdecuisine.fr/article-recettes-speciales-ramadan-2012-108026021.html>) , [ special dishes ramadan 2012 ](<https://www.amourdecuisine.fr/article-plats-du-ramadan-2012-107954558.html>) , [ ramadan menu the breads ](<https://www.amourdecuisine.fr/article-menu-du-ramadan-les-pains-107046173.html>) , and also [ the boureks and bricks ](<https://www.amourdecuisine.fr/article-recettes-speciales-ramadan-les-boureks-et-bricks-108003546.html>) and there will be plenty of other indexes to come, and even those already online, are in full update. 

thank you for wanting to subscribe the newsletter, do not miss any new recipe on my blog.   
  
<table>  
<tr>  
<td>

![Makrout For Dates](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makrout-aux-dattes_thumb.jpg)   
[ makrout rolls ](<https://www.amourdecuisine.fr/article-makrout-roule-102845781.html>) 
</td>  
<td>

![basboussa with breadcrumbs 008](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-0082.jpg)   
[ Basboussa ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html>) 
</td>  
<td>

![konafa](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/konafa_thumb.jpg)   
[ ktayef has the cream ](<https://www.amourdecuisine.fr/article-mini-konafa-ktayefs-a-la-creme-82031678.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gateau-au-chocolat-et-creme-caramel-a1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-magique-au-chocolat-et-creme-caramel-107207656.html>) [ Magic chocolate cake with egg custard and caramel custard ](<https://www.amourdecuisine.fr/article-gateau-magique-au-chocolat-et-creme-caramel-107207656.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/creme-caramel-au-lait-concentre-sucre.CR2_1.jpg) ](<https://www.amourdecuisine.fr/article-creme-caramel-au-lait-concentre-sucre-116960534.html>) [ Sweet caramelized caramel cream ](<https://www.amourdecuisine.fr/article-creme-caramel-au-lait-concentre-sucre-116960534.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-au-chocolat-033.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-flan-au-chocolat-116222705.html>) [ Chocolate flan ](<https://www.amourdecuisine.fr/article-flan-au-chocolat-116222705.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-014_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-flan-coco-97098371.html>) [ Coconut flan ](<https://www.amourdecuisine.fr/article-flan-coco-97098371.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/creme-brulee-a-la-pate-de-pistache_thumb.jpg) ](<https://www.amourdecuisine.fr/article-creme-brulee-a-la-pistache-115809743.html>) [ Crème brûlée with pistachio ](<https://www.amourdecuisine.fr/article-creme-brulee-a-la-pistache-115809743.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-047.CR2_thumb2.jpg) ](<https://www.amourdecuisine.fr/article-creme-brulee-recette-creme-brulee-115598588.html>) [ Creamy crème brûlée ](<https://www.amourdecuisine.fr/article-creme-brulee-recette-creme-brulee-115598588.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/panna-cotta-a-la-rose-2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-panna-cotta-a-la-rose-99132888.html>) [ Panna cotta with rose ](<https://www.amourdecuisine.fr/article-panna-cotta-a-la-rose-99132888.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-au-nestle-057_thumb.jpg) ](<https://www.amourdecuisine.fr/article-flan-au-lait-concentre-nestle-106499948.html>) [ Flan with concentrated milk ](<https://www.amourdecuisine.fr/article-flan-au-lait-concentre-nestle-106499948.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/riz-indien-048_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-creme-de-riz-indienne-59532236.html>) [ Indian rice cream ](<https://www.amourdecuisine.fr/article-creme-de-riz-indienne-59532236.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flanlouiza_thumb.jpg) ](<https://www.amourdecuisine.fr/article-30828812.html>) [ Flan pastry with puff pastry ](<https://www.amourdecuisine.fr/article-30828812.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-aux-oeufs-055_thumb.jpg) ](<https://www.amourdecuisine.fr/article-flan-aux-oeufs-100561520.html>) [ Egg flan ](<https://www.amourdecuisine.fr/article-flan-aux-oeufs-100561520.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi-0182.jpg) ](<https://www.amourdecuisine.fr/article-mhalbi-creme-dessert-au-riz-56030222.html>) [ Mhalbi, cream of rice, Algerian dessert ](<https://www.amourdecuisine.fr/article-mhalbi-creme-dessert-au-riz-56030222.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/creme-brulee-lait-de-coco-grenade_thumb.jpg) ](<https://www.amourdecuisine.fr/article-creme-brulee-au-lait-de-coco-et-grenade-85865112.html>) [ Crème brûlée with coconut milk and pomegranate ](<https://www.amourdecuisine.fr/article-creme-brulee-au-lait-de-coco-et-grenade-85865112.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/panacota-031_thumb.jpg) ](<https://www.amourdecuisine.fr/article-panna-cotta-lait-de-noix-de-coco-creme-chocolat-61603779.html>) [ fold ](<https://www.amourdecuisine.fr/article-panna-cotta-lait-de-noix-de-coco-creme-chocolat-61603779.html>) [ na cotta with coconut milk / chocolate cream ](<https://www.amourdecuisine.fr/article-panna-cotta-lait-de-noix-de-coco-creme-chocolat-61603779.html>) 
</td>  
<td>

[ > ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/creme-renversee-012a_thumb.jpg) ](<https://www.amourdecuisine.fr/article-creme-renversee-caramel-cardamome-69954842.html>) [ Caramel cardamom spilled cream ](<https://www.amourdecuisine.fr/article-creme-renversee-caramel-cardamome-69954842.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mhalabiya-tricolore_thumb_1.jpg) ](<https://www.amourdecuisine.fr/article-m-halabiya-tricolore-citron-et-fruits-81792006.html>) [ Mhalabiya tricolor lemon / chocolate / fruit ](<https://www.amourdecuisine.fr/article-m-halabiya-tricolore-citron-et-fruits-81792006.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/flancitron4_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-26207923.html>) [ Homemade lemon flan ](<https://www.amourdecuisine.fr/article-26207923.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/creme-caramel-004-a_thumb.jpg) ](<https://www.amourdecuisine.fr/article-creme-caramel-fa-on-la-laitiere-65766980.html>) [ Milk caramel cream ](<https://www.amourdecuisine.fr/article-creme-caramel-fa-on-la-laitiere-65766980.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/yaourt-peche-006-1_thumb.jpg) ](<https://www.amourdecuisine.fr/article-yaourt-aux-peches-confiture-de-peches-58617162.html>) [ Yoghurt with peach jam ](<https://www.amourdecuisine.fr/article-yaourt-aux-peches-confiture-de-peches-58617162.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/yaourt1.jpg) ](<https://www.amourdecuisine.fr/article-25345552.html>) [ Strawberry yogurt ](<https://www.amourdecuisine.fr/article-25345552.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-patissier-sans-pate-022.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/article-flan-patissier-sans-pate-115689774.html>) [ Flan pastry without dough ](<https://www.amourdecuisine.fr/article-flan-patissier-sans-pate-115689774.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-au-flan-2-copie-11.jpg) ](<https://www.amourdecuisine.fr/article-tarte-au-flan-64077148.html>) [ Flan tart ](<https://www.amourdecuisine.fr/article-tarte-au-flan-64077148.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/Mahalabiya-aux-abricots-037-001_thumb2.jpg) ](<https://www.amourdecuisine.fr/article-mahalabiya-aux-abricots-mahalabiya-108613662.html>) [ Mahalabia with apricots ](<https://www.amourdecuisine.fr/article-mahalabiya-aux-abricots-mahalabiya-108613662.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-vanille-chocolat-023_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-flan-vanille-chocolat-108485696.html>) [ Chocolate vanilla flan ](<https://www.amourdecuisine.fr/article-flan-vanille-chocolat-108485696.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/image_thumb1.png) ](<https://www.amourdecuisine.fr/article-53820221.html>) [ Cherry clafoutis and fermented milk ](<https://www.amourdecuisine.fr/article-53820221.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/dc824be849a8f294a3d2226bc3b71b5e1.jpg) ](<https://www.amourdecuisine.fr/article-verrines-flan-et-fruits-108733962.html>) [ Flan and fruit verrines ](<https://www.amourdecuisine.fr/article-verrines-flan-et-fruits-108733962.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/far-breton-aux-prunes-023.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-far-breton-112226619.html>) [ Far Breton with prunes ](<https://www.amourdecuisine.fr/article-far-breton-112226619.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/clafouti-au-pommes-011_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-clafouti-pommes-104323691.html>) [ Apple clafoutis ](<https://www.amourdecuisine.fr/article-clafouti-pommes-104323691.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/image_thumb4.png) ](<https://www.amourdecuisine.fr/article-clafoutis-aux-poires-98865118.html>) [ Pie clafoutis with pears ](<https://www.amourdecuisine.fr/article-clafoutis-aux-poires-98865118.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/clafoutis-aux-abricots-4_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-clafoutis-aux-abricots-115114731.html>) [ Clafoutis with apricots ](<https://www.amourdecuisine.fr/article-clafoutis-aux-abricots-115114731.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tarte-flan-aux-pommes-11.jpg) ](<https://www.amourdecuisine.fr/article-tarte-flan-aux-pommes-117274851.html>) [ Apple pie cake ](<https://www.amourdecuisine.fr/article-tarte-flan-aux-pommes-117274851.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/215791321.jpg) ](<https://www.amourdecuisine.fr/article-25345501.html>) [ Coconut quinoa flan ](<https://www.amourdecuisine.fr/article-25345501.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/verrines-fraises_21.jpg) ](<https://www.amourdecuisine.fr/article-panna-cotta-vanille-et-mousse-fraises-mascarpone-101606363.html>) [ Panna cotta vanilla and mascarpone strawberry mousse ](<https://www.amourdecuisine.fr/article-panna-cotta-vanille-et-mousse-fraises-mascarpone-101606363.html>) 
</td> </tr> </table>
