---
title: Tunisian brick with egg
date: '2018-04-21'
categories:
- Bourek, brick, samoussa, slippers
- Cuisine by country
- Tunisian cuisine
- idea, party recipe, aperitif aperitif
- salty recipes
tags:
- inputs
- Aperitif
- Amuse bouche
- Ramadan 2018
- Algeria
- Easy cooking
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/04/bricks-tunisienne-%C3%A0-loeuf.jpg
---
![Tunisian bricks with egg](https://www.amourdecuisine.fr/wp-content/uploads/2018/04/bricks-tunisienne-%C3%A0-loeuf.jpg)

##  Tunisian brick with egg 

Hello everybody, 

During Ramadan, my husband asks me or annabi boureks, tuna bricks, or Tunisian bricks. It is bricks very rich in taste and above all very nutritious after a long day of fasting. 

I have never been able to publish this recipe for you during Ramadan, because it's bricks that I prepare at the last minute, and directly at the table to taste them, that's why I take advantage today of them to share with you, but as you see, I could only save my share to be the star of the show, hihihih, in any case, she was so beautiful my Tunisian brick egg, I did not break the head to take pictures of all angles, plus, it is too good! 

I accompanied this tuna brig with the egg with a jari constantinois, or chorba frik ... But in general, a fresh salad accompanies well this brig, which is as I said very nutritious. 

**Tunisian brick with egg**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/04/brick-tunisienne-%C3%A0-loeuf-2.jpg)

portions:  5 

**Ingredients**

  * 5 Breadheets (or depending on the number of people) 
  * 5 Eggs 
  * A handful of capers 
  * A can of oil tuna drained from his oil. 
  * A minced onion 
  * Grated cheese 
  * 4 medium potatoes 
  * chopped parsley 
  * Salt and black pepper 
  * Harissa 



**Realization steps**

  1. boil potatoes cleaned and cut into cubes, and cook in salt water 
  2. drain the potatoes well from the water and crush them. 
  3. heat the oil, then lower the heat. 
  4. Take a sheet of brick, place the crushed potato in a circle 
  5. break the egg in the center, salt and pepper. 
  6. top with parsley, crumbled tuna, some capers, harissa and finally cheese 
  7. fold the sheet of brig in the shape of a triangle to enclose the stuffing. 
  8. fry in the oil until the brick takes a nice golden color on both sides bricks on both sides. 
  9. remove and drain on paper towels 



![Tunisian brick with egg 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/04/brick-tunisienne-%C3%A0-leouf-1.jpg)
