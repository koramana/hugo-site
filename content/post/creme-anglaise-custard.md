---
title: cream custard custard
date: '2012-10-27'
categories:
- amuse bouche, tapas, mise en bouche
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/008_thumb.jpg
---
##  cream custard custard 

Hello everybody, 

here is the custard custard recipe, a cream that differs from pastry cream. The custard is liquid, 

It is mainly used to sprinkle a dessert a little dry, or to make delicious [ trifles ](<https://www.amourdecuisine.fr/article-trifles-dessert-anglais-verrines-sucrees-88686582.html>) , these fruity verrines that we like a lot at home. 

Custard custard is also often used to make the famous classic dessert: the [ floating island ](<https://www.amourdecuisine.fr/article-recette-ile-flottante-avec-video.html>) . Yes, custard is a very important ingredient in this recipe. 

To be honest my discovery of custard was a pure coincidence, arrived here in England I wanted to achieve [ thousand sheets ](<https://www.amourdecuisine.fr/article-26287434.html>) quickly for my husband, and I went to buy cream pastry to make quickly, and I bought the custard, the result! Well, we ended up eating mille-feuilles in verrines, because the cream did not want to hold, hihihihih   


**custard, custard**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/008_thumb.jpg)

**Ingredients**

  * 50 cl of milk, 
  * 4 egg yolks, 
  * 100 g of sugar, 
  * vanilla pod 



**Realization steps**

  1. Blanch the egg yolks and sugar. 
  2. Heat the milk and split vanilla in half. 
  3. Pour the boiling milk without the pod into the egg-sugar mixture while whisking, finish with the Maryse. 
  4. Put back in the pan and allow to thicken over a low heat while aerating (to bask) to mix it. 
  5. When the cream laps the spoon, it's ready. 
  6. Pour into a bowl and let cool. 


