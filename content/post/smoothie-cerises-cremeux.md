---
title: creamy cherry smoothie
date: '2014-05-22'
categories:
- juice and cocktail drinks without alcohol
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-cr%C3%A9meux-aux-cerises.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-cr%C3%A9meux-aux-cerises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-cr%C3%A9meux-aux-cerises.jpg>)

##  creamy cherry smoothie 

Hello everybody, 

Back with the smoothies, we take advantage of this heavy heat to refresh ourselves with this delicious creamy cherry smoothie, which we realized Lunetoiles ... 

A recipe very easy to make, and creamy with the addition of Greek yoghurt, of course, you can replace it with natural yogurt, or yogurt flavored with cherries ... It can only be better. 

so follow me for the recipe of this refreshing drink, personally drinks like that, will always be present on my Ramadan table, because it's full of vitamin, after a long day of fasting, what are you say? 

Follow the link for others [ smoothie recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=smoothie&sa=Rechercher>)

**creamy cherry smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-cr%C3%A9meux-aux-cerises.jpg)

Recipe type:  Dessert 

**Ingredients**

  * 250 ml of milk 
  * 1 Greek yoghurt, plain or vanilla 
  * 1 bowl of frozen cherries, previously frozen (330 gr) 
  * 1 C. of vanilla extract 
  * 1 tablespoon agave syrup to sweeten, or more to taste (if not use sugar, or whatever you want) 



**Realization steps**

  1. Place all ingredients in a blender and blend until smooth. 
  2. Enjoy! 



[ ![creamy cherry smoothies](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-cr%C3%A9meux-aux-cerises-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-cr%C3%A9meux-aux-cerises-1.jpg>)

I hope you'll be lucky to try this delicious banana and peanut butter smoothie. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
