---
title: strawberry yogurt
date: '2010-07-07'
categories:
- appetizer, tapas, appetizer
- ramadan recipe
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/07/202848271.jpg
---
![bonj21](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/202851971.gif)

here is my first yoghurt, and I swear it was a delight, I made it in the yoghurt. I can tell you that I have a little play on the ingredients, because I found a yogurt in the bottom of my closet, she was at the ex of my husband, surely. 

so I jump on what I had as ingredients, just to try this machine. 

so I had on hand: 

  * 1/2 box of nestle (milk concentrated sugar) 

  * 3 cases of milk powder 

  * 1 box of strawberry in the syrup 

  * 3 cases of sugar (or according to your taste, mine was just right, not too much sugar, and not less, so in point) 

  * 1 liter of milk 

  * 1 box of natural yoghurt 




I start by crushing the strawberries, draining well of their syrup with a little milk and condensed milk. 

then I add the rest of the milk, the sugar, and the milk powder, mix well, and bring to the boil 

![S7301629](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/202848271.jpg)

after the ebullition, let the mixture cool down to 45 degrees (I did not have the cooking thermometer, so I have to leave lukewarm), and then, I add the box of yoghurt, mix everything well ingredients together. 

![S7301631](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/202849081.jpg)

and put the mixture in the jars with yoghurt, and allow to take during 10 hours. 

![S7301633](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/202849691.jpg)

after the incubation time, remove your jars from the unit, and leave 4 hours before putting in the refrigerator. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/yaourt1.jpg)

I assure you that it's a delight, I do not think I ever ate strawberry yoghurt with such a taste. 

good appeals to all. 

![bye1](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/202884971.gif)
