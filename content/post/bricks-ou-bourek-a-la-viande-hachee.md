---
title: bricks or bourek has chopped meat
date: '2014-06-22'
categories:
- Bourek, brick, samoussa, slippers
- recipe for red meat (halal)
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/bricks-a-la-viande-hachee.CR2_.jpg
---
##  [ ![bricks with meat chopped.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/bricks-a-la-viande-hachee.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/bricks-a-la-viande-hachee.CR2_.jpg>)

##  bricks or bourek has chopped meat 

Hello everybody, 

A very popular starter at home is ground beef bricks, or as we like to call them bourek or ground beef stew. 

A recipe that I prepare often, when the weather is nice, to accompany a salad, or that never leaves our tables of Ramadan that is approaching at a great pace ... 

a recipe very easy to make, a crisp endless every time you crumble a bourek to arrive at this sweet generous layer of minced meat, very fragrant and delicious ....   


**bricks or bourek has chopped meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/briouates-a-la-viande-hachee.CR2_.jpg)

portions:  6  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * brick sheets 
  * 500g minced meat 
  * 1 chopped onion 
  * 1 bunch of parsley 
  * a few portions of cheese 
  * garlic powder, coriander powder 
  * Salt pepper 
  * 100 ml of water 
  * of table oil 
  * cooking oil 



**Realization steps**

  1. Brown the chopped onion in a little oil until it becomes translucent. 
  2. add the minced meat, the spices, the salt, and using a wooden spoon turn the meat so that it is well separated. 
  3. add the water if you think that the meat is not yet well cooked and cook until it is completely reduced. 
  4. off the heat, add the parsley and let cool 
  5. cut the bricks leaves in half. 
  6. place on each half of the leaf, the equivalent of a spoon of minced meat, and place on half of a portion of cheese. 
  7. fold the leaves on the stuffing, to have a triangle 
  8. cook in a warm oil bath on both sides, and place the cooked bricks on paper towels, let drain well. 



[ ![brioche with chopped meat 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/briouates-a-la-viande-hachee-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/briouates-a-la-viande-hachee-1.CR2_.jpg>)
