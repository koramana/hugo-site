---
title: guaranteed karantika قرنطيطة
date: '2017-01-23'
categories:
- amuse bouche, tapas, mise en bouche
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/garantita.jpg
---
[ ![guaranteed karantika قرنطيطة](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/garantita.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/garantita.jpg>)

##  guaranteed karantika قرنطيطة 

Hello everybody, 

Today my brothers asked me for the garantita, a delicious salty pie from Algerian cuisine prepared with chickpea powder, I hesitated a bit to do it because I had never done it. I took my little cookbook on which I noted the recipes I made with my aunts, or shared between friends. I remember that I had the recipe of my aunt Djamila, she does it beautifully. 

Quickly I read the recipe, and put myself in the kitchen ... The result was perfect, I was not disappointed neither me, nor those who ate it, it really made me happy .... Thank you Tata Djamila for your delight. 

So I give you this delicious recipe, which I will consider as [ salty Tart ](<https://www.amourdecuisine.fr/pizzas-quiches-tartes-salees-et-sandwichs>) , and I also advise you [ tuna pie with tuna ](<https://www.amourdecuisine.fr/article-tarte-au-thon-et-tomates.html>) . 

**garantita - karantika قرنطيطة**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/garantita2.jpg)

portions:  16  Prep time:  15 mins  cooking:  60 mins  total:  1 hour 15 mins 

**Ingredients** for a pyrex mold of 22 cm by 33 cm 

  * 2 glasses (220ml) of chickpea flour 
  * 1 glass of milk (optional, you can replaced by water) 
  * 3 glasses and a half of water 
  * 2 eggs 
  * ¼ glasses of oil 
  * 1 cup of coffee and a half of salt (or according to taste) 
  * 1 teaspoon of cumin (I let myself go in the amount of cumin because at home they like) 
  * black pepper 



**Realization steps**

  1. whisk eggs with milk (if not a glass of water), salt, caraway, black pepper and oil 
  2. pour this mixture gently over the chickpea flour, and dissolve the lumps well 
  3. pour over the remaining water, you will have a very liquid mixture (we make you) 
  4. oil the mold, and pour over the mixture 
  5. place in a preheated oven at 180 ° C 
  6. cook between 1 and 1 and a quarter, depending on the oven 



[ ![guaranteed karantika قرنطيطة](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/garantita111.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/garantita111.jpg>)
