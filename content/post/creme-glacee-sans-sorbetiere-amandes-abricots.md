---
title: Ice cream without ice cream almonds apricots
date: '2014-08-31'
categories:
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-magique-sans-sorbetiere-abricots-1.jpg
---
#  ![creme-whippers-magic-in-SORBETIERE-apricot-.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-magique-sans-sorbetiere-abricots-1.jpg)

##  Ice cream without ice cream almonds apricots 

Hello everybody, 

Here is a recipe that is in the archives of my mailbox since the beginning of summer, and that I did not have the chance to publish for lack of time. 

This magic ice cream, vegetable and without sorbet with almonds and apricots comes to me from my dear Lunetoiles and that I thank by the passage for taking care of my blog, when I had a bad connection, I am sure that you you are not even aware of that, hihihihihi. 

we go to his recipe then:   


**Ice cream without ice cream almonds apricots**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-glacee-sans-sorbetiere-aux-abricots1.jpg)

Recipe type:  ice cream, dessert  portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 400g frozen apricots 
  * 4 tbsp. tablespoon almond puree   
(or almond milk, or 1 soya yogurt, or 2 Swiss pups, or 1 yogurt of cow or sheep, or 200g of well-drained cottage cheese, or liquid cream, or soy milk to vanilla ...) 
  * 6 tablespoons agave syrup 
  * coarsely crushed or chopped almonds roasted to serve 



**Realization steps**

  1. Put everything in the bowl of a mixer. 
  2. Start pulse mode with brief pulses, then mix frankly until you have a perfectly creamy consistency. 
  3. Serve immediately or return to the freezer but not more than 4 hours otherwise the ice loses its softness. 
  4. If you need to freeze the ice longer, take it out 20 minutes before eating it. 


