---
title: speculoos tiramisu
date: '2014-09-29'
categories:
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tiramisu-aux-speculoos-1_2-300x228.jpg
---
##  speculoos tiramisu 

Hello everybody, 

Here is a super delicious dessert, to which personally I will not say no. A tiramisu with speculoos, I can not describe the marriage of flavors, between this sublime Italian cheese, the delicious mascarpone, and the most tasty biscuits, the crunchy speculoos with the bewitching taste of the cinnamon and the irresistible crunch of the vergeoise . 

Blows the delicious Tiramisu dessert, takes a Belgian coat, after replacing boudoirs by speculoos. 

But, just like you, I look at these pretty verrines behind the screen, without being able to taste them, because it is a recipe that Lunetoiles has warmly share with us. 

**speculoos tiramisu**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tiramisu-aux-speculoos-1_2-300x228.jpg)

portions:  6  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 500 g of mascarpone 
  * 5 eggs 
  * 5 c. tablespoon caster sugar 
  * 1 box / pack of speculoos 
  * 300 ml of very strong coffee. 
  * 1 C. to s. cocoa powder 



**Realization steps**

  1. Whisk the yolks and sugar until the mixture whitens. 
  2. Add the mascarpone and mix. 
  3. Mount the egg whites very firm and stir into the mixture gently. 
  4. Dip the cookies in the coffee and line the bottom of a dish or individual verrines. 
  5. Cover with a layer of the mascarpone / egg mixture, 
  6. place another layer of cookies and cover with a second layer of mascarpone. And repeat the operation one last time. 
  7. Refrigerate for at least 8 hours. 
  8. Before serving, sprinkle with cocoa. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, 
