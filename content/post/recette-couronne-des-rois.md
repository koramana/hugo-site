---
title: 'recipe: crown of kings'
date: '2013-01-06'
categories:
- Algerian cuisine
- recipe for red meat (halal)
- ramadan recipe

---
Hello everybody, 

here is a sublime crown of kings, or a cake of kings brioche, which I realized yesterday, I started the recipe yesterday ... 

it's the first time, I make a brioche, I'm based on a pasta recipe that rests all night, in truth, I'm impatient, the long-term recipes, hihihihih it's not really my style but I'm trying to improve myself ... 

I say it, and I say it again, the trick to make a brioche, is to be patient ... let me take the time it takes, for my part, I always plan an exit when I shape my cake, if not, make the round trip to see if it has to inflate or not, it will push me to put the brioche in the oven, before that she take the aspect or the size that I want. 

So, for this brioche, I knead the recipe at night, then I let rest all night in front of the radiator (my radiator is not too hot) in the morning, I degassed the dough on a plane of floured work, I shaped the crown ... 

I put it in front of the radiator, for almost 6 hours, when I came back, the dough that did not cover the half of the mold, superbly inflate until it exceeded the edges of the molds by more than 2 cm .... 

I gilded my brioche crown, I garnished it, and baked for 15 to 20 minutes depending on the oven ... 

the result, well I think I'm ready to open a bakery .... hihihihih 

  
Ingredients:   


  * 500 gr of flour 
  * 1 tablespoon of dehydrated yeast, instant yeast. 
  * 1 cup of baking powder 
  * 4 eggs 
  * 5g of salt 
  * 120 gr of sugar (I do not use too much sugar, because I have garnished my bun with pearl sugar and candied cherries) 
  * 30 ml of orange blossom water 
  * 45 ml warm water 
  * 100 gr of butter 



decoration: 

  * 1 egg yolk 
  * 1 pinch of vanilla 
  * 1 cup of milk 
  * pearl sugar 
  * candied cherries 



method of preparation: 

you can prepare this brioche in the bread machine, just to knead and make the first rise .. as you can do it in trouble ... otherwise proceed as follows: 

  1. In a salad bowl, mix the flour, the yeast, the sugar and the salt, 
  2. add water and orange blossom water. 
  3. break the eggs one by one and introduce them to the dough (one egg at a time and pick up) 
  4. if your eggs are big, maybe you will not use the 4 eggs, and if the eggs are small, you can go up to 5 eggs, the most important is to have a malleable dough, but not too sticky. 
  5. begin to knead the dough, introducing the butter gently, by small piece. knead until the butter is well absorbed by the dough, and add another piece, do so until you add all the butter. 
  6. put the dough back in the salad bowl and cover it with a clean cloth. 
  7. let stand at a temperature between 23 and 25 ° C, overnight. 
  8. The next day, degas the dough, spread it on a flour work plan, fold it, and spread again, then fold. Then form a crown, making a hole in the middle of the dough. 
  9. place the crown in a crown-shaped mold that is buttered and floured. 
  10. Let it rise again (the time it takes, depending on the temperature of the room, for me and because it's winter it took more than 6 hours) 
  11. decorate the crown of kings with the egg yolk mixture, milk and vanilla, then sprinkle the pearl sugar on top, and the pieces of candied cherries. 
  12. cook in a preheated oven at 180 ° C, between 25 and 30 minutes, or depending on your oven, in mine it took 20 minutes. 



merci pour vos visites 
