---
title: Buchty easy brioche
date: '2017-03-29'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/buchty-brioche-allemande-au-nutella.CR2_thumb.jpg
---
##  Buchty easy brioche 

Hello everybody, 

**Buchty easy brioche** , or bouchty is a delicious brioche of German origin, with a crumb, it is melting in the mouth, and if we add to that the touch of nutella !!! .... 

In principle this brioche the buchty is usually stuffed with almond paste, but my children chose Nutella. In any case and personally I adopted the recipe for this pasta in many of my brioche recipes. The result is a surprising result every time. 

You can see the video of one of my readers who has tested and approved this recipe: 

{{< youtube 1HE_vYxeCyo >}} 

The advice I can give you, to always succeed your buns, to have a well-running crumb and a brioche so soft and so airy it is to be very patient, take the time to let the dough rise every time .... especially the last step before cooking ... leave the brioche inflated before putting it in the oven ... And you tell me news! 

more 

**Buchty easy brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/buchty-brioche-allemande-au-nutella.CR2_thumb.jpg)

Recipe type:  bakery bun  portions:  10  Prep time:  45 mins  cooking:  25 mins  total:  1 hour 10 mins 

**Ingredients** for two molds   
(a circle has 7 small balls and a rectangle for 12 balls) 

  * 550 gr of flour 
  * 65 gr of sugar 
  * 5 eggs 
  * 3 tablespoons milk powder 
  * 2 tablespoon of potato starch 
  * 1 teaspoon of salt 
  * 100 gr of butter 
  * 100 ml of milk 
  * zest of a lime, and a yellow lemon 
  * 1 tablespoon instant baker's yeast (or a packet) 
  * Nutella for forage, and icing sugar for decoration. 
  * [ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/brioche-a-mie-filante_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/brioche-a-mie-filante_thumb1.jpg>)



**Realization steps** I made the brioche on the bread machine, but you can do it with the petrin or by hand   
(it is necessary to support a little dough which sticks to the hands, at the beginning of the petrissage) 

  1. place all the ingredients in the breadmaker's bowl in the order recommended for your appliance other than the butter. 
  2. let knead until the dough collects well, and add the butter slowly in small pieces. 
  3. if your dough is a bit sticky, add a little flour. 
  4. leave the dough rested and doubled in volume, between 45 min and 60 min. 
  5. Remove the dough and degas it on a worktop with a very light flour. 
  6. shape 16 medium balls or so 18 small 
  7. flatten each dumpling and place in it the equivalent of a teaspoon of nutella 
  8. and dip them in a rectangular or round buttered and floured mold, or covered with baking paper (you can leave nature, or stuff according to your desires, jam, chocolate, almond paste cheese etc ...) 
  9. Cover with a cloth and allow to rise for 1 hour. 
  10. brush the whole surface of the bun with a mixture of egg yolk and 1 tablespoon of milk. 
  11. Bake in a hot oven at 180 ° for about 25 minutes, or depending on the capacity of your oven. 
  12. Once cooked, arrange the bun on a rack and sprinkle with icing sugar after cooling completely. 


