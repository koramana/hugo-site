---
title: Peach tart
date: '2010-07-13'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, chaussons
- cuisine algerienne
- cuisine diverse

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/07/223102771.jpg
---
& Nbsp; the peaches are my best fruit, besides it's already great that I did not hate them during my two pregnancies, my husband only bought me that at my request. ingredients: a box of peach in syrup for the broken dough: 250 gr of flour 1 tsp of sugar 2 pinches of salt 1 point of yeast 150 gr of butter 1 egg 60 ml of milk put the flour with the salt, the sugar and the yeast tip. Add the butter in small pieces and work to obtain a sanding. Stir in the whole egg and a small amount 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

the peaches are my best fruit, besides it's already great that I did not hate them during my two pregnancies, my husband only bought me that at my request. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/223102771.jpg)

ingredients: 

a fishing box in syrup 

for the broken dough: 

  * 250 gr of flour 
  * 1 teaspoon of sugar 
  * 2 pinches of salt 
  * 1 tip of yeast 
  * 150 gr of butter 
  * 1 egg 
  * 60 ml of milk 



put the flour with salt, sugar and yeast. Add the butter in small pieces and work to obtain a sanding. Stir in the whole egg and a small amount of milk, start kneading to mix these ingredients. Pour the rest of the milk and mix well without doing too much work; which would make it elastic and difficult to spread. Form a ball. Put 3H cool or overnight, it will be easier to spread. The next day use the 

for pastry cream: 

  * 2 eggs 
  * 150 ml of milk 
  * 40 gr of sugar 
  * 1 sachet of vanilla sugar 
  * 3 teaspoons of cornflour 



In a salad bowl, mix eggs, sugar and vanilla sugar. Add the cornstarch and mix well. Boil the milk and pour over the egg / sugar / cornflour mixture. Then put back on low heat and allow to thicken. This cream is not really smooth because of the amount of eggs and quantity of milk 

Lower your broken dough, and place it in your pan, prick well with a fork, and bake in a warm oven 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/223102791.jpg)

pour the custard over the dough and spread evenly. Place the cut peaches. Sprinkle with sugar, shape the outline and put in preheated 175 ° oven. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/223102851.jpg)

for the topping, take a drink, pour 2 teaspoons of sugar, about 1 teaspoon of cornflour. Add 4 tablespoons of syrup remaining from the peach box in syrup, mix and add to the remaining syrup. Mix well and allow to thicken over low heat. 

Remove the pie from the oven and pour the topping. Let cool before unmolding 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/223102901.jpg)

thank you bigmumy for your recipe 

really delicious 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/223102941.jpg)

bon appétit 
