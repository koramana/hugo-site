---
title: naan with coriander, Indian bread
date: '2018-05-10'
categories:
- Mina el forn
- bread, traditional bread, cake
tags:
- Galette
- India
- accompaniment
- Easy cooking
- Ramadan 2018
- Boulange
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-pains-indiens-naans-%C3%A0-la-coriandre-2-1024x576.jpg
---
![naan Indian breads with coriander 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-pains-indiens-naans-%C3%A0-la-coriandre-2-1024x576.jpg)

##  naan with coriander, Indian bread 

Hello everybody, 

I really like them **Naan bread** These delicious patties of Indian bread, very mellow, and rich in tastes that go perfectly with any dish, sauce or soup. Today, I introduce you to the  **naans with coriander.**

This dough of Indian bread I adopted since the first time I realized it, besides it is also the base of my loaves [ Naans with cheese ](<https://www.amourdecuisine.fr/article-recette-naans-fromage-inratable-cheese-naans.html>) . The recipe is simple, but I took the opportunity to make a video, you will see in this video how the realization of this Indian bread is a real breeze: 

{{< youtube JaGvP7dgdsQ >}} 

**naan with coriander, Indian bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-pains-indiens-naans-%C3%A0-la-coriandre-1024x576.jpg)

**Ingredients**

  * 500 g of flour 
  * 1 C. baking yeast 
  * 1 C. salt 
  * 1 C. sugar coffee 
  * 1 pinch of baking powder 
  * 125 gr of natural yoghurt 
  * 4 c. vegetable oil 
  * between 180 and 200 ml of lukewarm water 

To garnish: 
  * chopped coriander 
  * table oil 



**Realization steps**

  1. in the bowl of the food processor mix the dry ingredients: flour, baker's yeast, baking powder, salt and sugar. 
  2. then add the oil, yoghurt and water in small amounts while kneading 
  3. Knead the dough for 12 to 15 minutes until smooth. 
  4. Let the dough rest and double the volume for almost 1 h 30 depending on the temperature of the room by covering with a cloth. 
  5. divide the dough into pellets, between 10 and 12 pellets, and place them on a floured space. let stand at least 5 minutes. 
  6. take a pellet and spread on a floured space giving the oval shape to the bread 
  7. oil the surface of the naan and sprinkle with fresh coriander 
  8. go back, spread the naan again. 
  9. then cook on a preheated pan or pancake pan. Place the naans on the surface not garnished with coriander. 
  10. Cover directly with a pot lid so that the naan swells a little and stays soft. 
  11. turn to give a slight color on the second side 
  12. remove and place on a clean cloth or grill to let cool without sweating. 



![naan Indian breads with coriander 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-pains-indiens-naans-%C3%A0-la-coriandre-1-1024x576.jpg)
