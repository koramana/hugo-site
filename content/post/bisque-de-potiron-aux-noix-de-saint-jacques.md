---
title: pumpkin bisque with scallop walnuts
date: '2015-11-03'
categories:
- Algerian cuisine
- diverse cuisine
- soups and velvets
tags:
- Vegetable soup
- Vegetables
- Healthy cuisine
- Easy cooking
- Algeria
- fall
- Soup

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/bisque-de-potiron-aux-noix-de-saint-Jacques-1.jpg
---
[ ![pumpkin bisque with scallop walnuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/bisque-de-potiron-aux-noix-de-saint-Jacques-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/bisque-de-potiron-aux-noix-de-saint-Jacques-1.jpg>)

##  pumpkin bisque with scallop walnuts 

Hello everybody, 

Vegetable soups are a weekly must for me, If I do not eat one, I do not see how my week has passed, hihihihi. 

In any case, this time, and on the occasion of the round: Recipe around an ingredient # 11 The pumpkin, I did not hesitate to realize this bisque of pumpkin all unctuous, presented with walnuts of Saint Jacques, and that I accompanied with garlic bread ... yum yum what is good. 

Previous rounds: 

**pumpkin bisque with scallop walnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/bisque-de-potiron-aux-noix-de-saint-jacques.jpg)

portions:  6  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients**

  * 1 kg of pumpkin flesh 
  * 1 onion 
  * 2 carrots cleaned and cut into cubes 
  * 1 potato 
  * salt, black pepper, curry. 
  * 1 liter of homemade chicken broth 
  * 100 ml of liquid cream. 
  * 40 g of Parmesan cheese 
  * olive oil 
  * mill pepper 
  * Some walnuts of Saint Jacques 



**Realization steps**

  1. Cut the vegetables into cubes, the onion into strips. 
  2. place in a baking tin lined with aluminum foil. 
  3. sprinkle with a little oil and salt 
  4. cook in the oven while stirring occasionally so that it does not burn. 
  5. place the cooked vegetables in a pot with chicken broth and spices. 
  6. cook a little, then reduce to velouté using a mixer foot. 
  7. Add the liquid cream, and mix again, if you think that the cream is too creamy add a little boiling water. 
  8. add grated parmesan, stir and leave aside. 
  9. Brown the scallops 1 min on each side in a bottom of hot oil, brown very well are too much to cook. 



[ ![pumpkin bisque with scallop walnuts 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/bisque-de-potiron-aux-noix-de-saint-jacques-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/bisque-de-potiron-aux-noix-de-saint-jacques.jpg>)

the list of participants: 

1 - Pumpkin bisque with scallop nuts by Soulef [ AMOUR DE CUISINE ](<https://www.amourdecuisine.fr/>)
