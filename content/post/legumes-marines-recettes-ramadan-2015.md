---
title: pickled vegetables / ramadan recipes 2015
date: '2015-05-27'
categories:
- appetizer, tapas, appetizer
- cuisine algerienne
- recette de ramadan
- salades, verrines salees
tags:
- Economy Cuisine
- Algeria
- Morocco
- Ramadan
- Ramadan 2015
- Cucumber
- salads

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/legumes-marin%C3%A9s.jpg
---
[ ![Pickled vegetables](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/legumes-marin%C3%A9s.jpg) ](<https://www.amourdecuisine.fr/article-legumes-marines-recettes-ramadan-2015.html/legumes-marines>)

##  pickled vegetables / ramadan recipes 2015 

Hello everybody, 

And here is a recipe that I like a lot: vegetables pickled in vinegar, I like them a lot and especially carrots and fennels ... At home in Algeria, it is called Variant vegetables, some also called four seasons. In any case, if you like this recipe to be ready for this 2015 ramadan incha allah, it's time to do it, hihihi 

So here is the recipe of my friend of the page: a moment of pleasure, a pity for me here I have to do it with white vinegar, because I do not have the vinegar like that of Algeria. This time it's on when I leave incha allah in Algeria, I'm going to stock up on vinegar, hihihihih 

**pickled vegetables / ramadan recipes 2015**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/legumes-marin%C3%A9s.jpg)

portions:  4-5  Prep time:  12 mins  total:  12 mins 

**Ingredients** vegetables according to your taste, and according to the quantity you want to have: 

  * carrots 
  * fennel 
  * cucumbers 
  * cauliflower 
  * pepper 
  * garlic 
  * bay leaves 



**Realization steps**

  1. Wash your vegetables well 
  2. cut them to your choice 
  3. fill your jar, pack the vegetables well   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/legumes-marin%C3%A9s-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42504>)
  4. add the salt and fill with vinegar 
  5. add some bay leaves 
  6. close the jar well, and marinate away from the light for at least 1 month. 



[ ![pickled vegetables 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/legumes-marin%C3%A9s-2.jpg) ](<https://www.amourdecuisine.fr/article-legumes-marines-recettes-ramadan-2015.html/legumes-marines>)
