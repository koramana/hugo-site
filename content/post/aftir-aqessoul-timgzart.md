---
title: Aftir aqessoul timgzart
date: '2015-04-23'
categories:
- cuisine algerienne
tags:
- Algeria
- dishes
- Full Dish
- Semolina
- Healthy cuisine
- sauces
- Vegetarian cuisine
- Soup

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/aftir-aqessoul-timgzert-.jpg
---
##  [ ![Aftir aqessoul timgzart](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/aftir-aqessoul-timgzert-.jpg) ](<https://www.amourdecuisine.fr/article-aftir-aqessoul-timgzart.html/aftir-aqessoul-timgzert>)

##  Aftir aqessoul Timgzart 

Hello everybody, 

I share with you today this delicious dish that comes from Kabylie, aftir aqessoul, known as: timgzart, thaknifth tadount, tikkarrissine, thafdaoucht and ... the list is long. But what's the matter? it's Algerian crepes in the pot, if you want the word-for-word translation of the name. It is based on crepe dough prepared like that of [ mhadjebs ](<https://www.amourdecuisine.fr/article-mhadjeb-algeriens-mahdjouba-en-video.html> "Mhadjeb Algerians - mahdjouba in video") and [ msemens ](<https://www.amourdecuisine.fr/article-msemen-ou-crepes-feuilletees.html> "Msemen or flaky crepes") but, instead of cooking them in a pan or on a cast iron tajine, the crepes are plunged munitiously into the sauce while trying to keep them open. 

In Algeria, my mother has a special pot made in Kabylie by the care of her aunt, a large pot in clay. When she prepares to take aqessoul, or instead of dipping the crepes directly into the sauce, she covers the pot with the crepe, which will cook with the steam that escapes from the cooking, and after it is done dipping in the sauce to finish his cuissone. In the region of Kabylie where my mother comes from, this dish is generously flavored with mint, in some areas, they add a handful of lentils .... Where do you call this dish, and how do you prepare it? 

![aftir aqessoul](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/timgzert-.jpg)

**Aftir aqessoul: timgzart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/aftir-aqessoul-001.jpg)

portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** for aftir: (crepes) 

  * 1 glass of semolina 
  * ¼ teaspoon of salt 
  * water as needed 
  * oil to spread the dough 

for the sauce: 
  * 1 soup not too full of canned tomato 
  * 1 small onion 
  * 2 cloves garlic 
  * 1 small bunch of coriander 
  * salt, black pepper and mint powder. 
  * green pepper according to taste 



**Realization steps** start by preparing the aftir, crepes: 

  1. by hand, or in a bowl of the robot, mix the semolina salt 
  2. add water to flavor the dough. 
  3. Petrol while rolling the dough, until it becomes very tender, 
  4. add water as needed. The dough must be elastic and flexible 
  5. form small balls the size of a tiny tangerine you must get almost 10 pellets. 
  6. cover with a cling film and let stand.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/aftir.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41657>)

prepare the sauce: 
  1. mix the onion, coriander and garlic in the blinder bowl 
  2. in a pot, add these ingredients, with a little water, canned tomato and chilli   
We do not put the oil in the sauce, because after the dough is going to be spread in the oil, and will give the fat to the sauce.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/aftir-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41658>)
  3. season with salt and black pepper (not too much salt) 
  4. cover with water and let it boil 
  5. when the sauce begins to boil, prepare the pancakes 
  6. Spread a pancake dumpling on an oiled work surface   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/aftir-a1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41659>)
  7. take the dough gently and dip in the sauce   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/aftir-4.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41660>)
  8. let it cook well and before spreading the second meatball   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/DSCF0540.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41661>)
  9. push the side dough with a spoon before adding the second crepe   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/aftir-a2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41662>)
  10. Continue until all the dough is gone. 
  11. at the end of the cooking time, rectify the seasoning according to your taste adding salt or canned tomato. 
  12. Serve this hot dish by filling it generously with dried mint. 



##  [ ![aftir aqessoul](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/aftir-aqessoul.jpg) ](<https://www.amourdecuisine.fr/article-aftir-aqessoul-timgzart.html/aftir-aqessoul>)
