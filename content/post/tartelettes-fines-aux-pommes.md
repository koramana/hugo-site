---
title: thin tartlets with apples
date: '2012-09-05'
categories:
- cuisine algerienne
- cuisine marocaine
- Cuisine par pays

---
hello everyone, I always like to try the recipes of my friends bloggers, and this time, I test a lakbira delights, and speaking of Lakbira, we must say that we will need a lot of time, to be able to all test, because everything is delicious at home. anyway, this time I opt for these delicious apple tartlets, I recommend it Ingredients: - 2 to 3 apples (what I had on hand) - 1 roll of pastry - 1 tsp. and half a cup of apricot marmalade - 3 tbsp sugar in & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

in any case, this time I opt for these delicious apple tartlets, I recommend it 

Ingredients: 

\- 2 to 3 apples (what I had on hand) 

\- 1 roll of puff pastry 

\- 1  ** tsp. and half a tablespoon of apricot marmalade  **

** \- 3 tablespoons caster sugar  **

** \- 50 g diced butter  **

** \- 3 tbsp of apricot marmalade, sifted (for me apricot jam)   
1) Grease a large plate (I put some greaseproof paper!). Cut the apples in half. Remove the hearts, but leave the skin.Cut into thin slices as possible, keeping the shape.   
2) Lower the dough and form a rectangle of 25/36 cm. Cut in half lengthways, then crosswise to obtain six rectangles. Lay them regularly on the plate. Spread 1 tbsp. coffee apricot marmalade on each piece of dough, leaving a 2 cm edge.   
3) Divide the apple slices and arrange them on each piece of dough, leaving a border. Sprinkle with powdered sugar and sprinkle with butter cubes. Chip the edge of the pies. This will allow them to inflate and open.   
4) Cool for at least 20 minutes. Preheat oven to 210 ° C, bake pies for about 20 minutes until puffed and browned. Spoon hot marmalade sifted while still hot. Serve hot or cold.   
**

good tasting, and the next recipe 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
