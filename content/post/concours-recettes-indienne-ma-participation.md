---
title: Indian recipe contest, my participation
date: '2012-05-02'
categories:
- couscous
- Algerian cuisine
- recettes de feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/riz-indien-048_thumb1.jpg
---
![Indian rice 048](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/riz-indien-048_thumb1.jpg)

hello my friends (ies) 

and I participate with the recipe of this [ Indian rice cream, Phirni ](<https://www.amourdecuisine.fr/article-creme-de-riz-indienne-59532236.html>) . 

I wanted to participate with the recipe of [ tandoori chicken ](<https://www.amourdecuisine.fr/article-poulet-tandoori-85541703.html>) but I think there will be plenty of participation with this delicious recipe. 

in any case, we return to the competition  : 

![banner contest - patak-s-.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/banniere-concours-patak-s-1.jpg)

she offers to win Patak's products by making an Indian recipe ... You have  **until May 7** to create a recipe: 

\- If you have **a blog** (cooking or other): publish your recipe (which can be an old recipe) with the photo on your blog with a link to this post and the banner of the contest then leave me a com on this post with a link to your recipe. 

\- If you have a **Facebook account, Flick, Tumblr** .... you can publish your recipe and photo on this support always with a link to this post then send it to me by mail to the following address  [Tentationsculinaires@free.fr]  (without the hooks). 

\- If you do not **no blog** : just send me your recipe and picture by mail always to this address  [Tentationsculinaires@free.fr]  (without the hooks). 

3 lots of Patak's products to win: 

Best Recipe # 1: Tandoori + Mango Curly + Tikka Masala + Madras    
Best Recipe # 2: Mango Curly + Tandoori + Tikka Masala    
Best Recipe # 3: Mango Curly + Tandoori 

and here is an article for fans of  [ Indian recipes ](<https://www.amourdecuisine.fr/article-plats-et-recettes-indiennes-102329306.html>)
