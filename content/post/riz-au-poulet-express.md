---
title: Express chicken rice
date: '2017-12-02'
categories:
- diverse cuisine
- rice
tags:
- Easy recipe
- Algeria
- dishes
- Full Dish
- ramadan 2015
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/riz-au-poulet-express.jpg
---
[ ![express chicken rice](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/riz-au-poulet-express.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/riz-au-poulet-express.jpg>)

##  Express chicken rice 

Hello everybody, 

an express chicken rice for you! it's a recipe that I did not plan to publish on my blog ... It's an express chicken rice recipe that I made quickly, and shared on my kitchen group on facebook ... She liked the girls a lot of the group that they stopped me not to ask again the recipe, and since it is not easy to find on the group, I preferred to put it on the blog, like that, it will be easy for me to pass the link to girls. 

Just for info, as the recipe was quickly made, even taking pictures was fast, hihihihi, just with my cell phone, promised that I redo the recipe, I will make more beautiful pictures.   


**Express chicken rice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/riz-au-poulet-express-2.jpg)

portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients** for the chicken sauce: 

  * chicken drumsticks 
  * salt, black pepper, a little curry, a little chili, a little paprika, 
  * 2 to 3 tablespoons of oil 
  * 1 can of crushed tomato, or 1 kilo of fresh tomatoes in pieces. 

for rice: 
  * 2 glasses of basmati rice 
  * some branches of thyme, 
  * some cardamom 
  * a few grains of coriander, black pepper from the mill, if not crushed black pepper, 
  * ½ chopped onion 



**Realization steps**

  1. in a pressure cooker put the onion in strips 
  2. put the pieces of chicken on top (I had half-thighs in the freezer) 
  3. put the spices on top: salt, black pepper, a little curry, a little chilli, a little paprika,. 
  4. add a can of crushed tomato, or a kilogram of fresh tomato in pieces 
  5. close the casserole and place on low heat (it is necessary to watch the cooking so that it does not burn, for my part I look every 10 minutes. 

Prepare the rice: 
  1. wash the rice under the abundant water until you have a very clear water that emerges from the rice (try to remove all the excess starch) 
  2. in another pot, place a little oil, add a half onion chopped in small cube, fry 
  3. add a little thyme, some cardamom, some coriander seeds, black pepper from the mill, if not crushed black pepper, 
  4. when the onion is just translucent, add the rice. 
  5. saute a little and then cover with water. 
  6. the water must cover the rice, but be higher than the rice, barely 8 mm higher no more. 
  7. cover the marmitte with aluminum foil, then place your lid 
  8. cook on medium heat, when you notice that there is no more steam escaping, remove it from the heat, remove the aluminum, and see if the rice is well cooked. it must be perfect as in the picture below, you can air it gently with a fork. 
  9. serve your rice with the chicken sauce, and have a good appetite. 



[ ![chicken express rice](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/riz-express-au-poulet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/riz-express-au-poulet.jpg>)
