---
title: quince jelly recipe
date: '2017-10-26'
categories:
- jams and spreads
- basic pastry recipes
tags:
- accompaniment
- Based
- verrines
- To taste
- desserts
- Fruits
- Foie gras

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gel%C3%A9e-de-coings.jpg
---
[ ![quince jelly recipe](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gel%C3%A9e-de-coings.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gel%C3%A9e-de-coings.jpg>)

##  quince jelly recipe 

Hello everybody, 

I always thought I had posted the quince jelly recipe on my blog. When I have quince at home, I make quince jam, jelly, quince, tajines, and many other delicacies. I like quince very much, especially since I rarely find it here in Bristol. 

In any case for the occasion, why not go take a look at: the [ From Quince jam ](<https://www.amourdecuisine.fr/article-confiture-de-coing.html>) , the [ Quince paste ](<https://www.amourdecuisine.fr/article-recette-de-pate-de-coing-maison.html>) , [ tarte tatin with quince ](<https://www.amourdecuisine.fr/article-tarte-tatin-aux-coings.html>) , [ almond tart with quince ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-coings.html>) , [ autumn verrines with quince ](<https://www.amourdecuisine.fr/article-verrines-dautomne-coing-tapioca-et-pain-depice.html>) , [ tagine of quinces ](<https://www.amourdecuisine.fr/article-tajine-d-agneau-aux-coings.html>) . 

Otherwise, as I told you for the recipe of the jelly of quince, I thought I posted it on the blog, but when some readers had asked, I did not find the recipe on the blog ... Suddenly Here I made the recipe just for you my readers, I hope you enjoy it. 

What I like for the realization of the jelly of the quince is that it is made with the cooking water of the quinces, with which we would have prepared the quince jam.   


**quince jelly recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gelee-de-coings-1.jpg)

portions:  4  Prep time:  15 mins  cooking:  60 mins  total:  1 hour 15 mins 

**Ingredients**

  * quince cooking water 
  * quince glitches 
  * the quince peels 
  * sugar (same weight as cooking water of quince) 



**Realization steps**

  1. Peel the quince. Then cut them in four. Remove the heart with a small knife (peeling quince is not an easy task, pay attention to your hands) 
  2. keep the seeds and skin and stuff them in a piece of muslin. 
  3. place the pieces of quince and the nouet containing the pepins and the peels in a saucepan. Cover generously with water, almost 2 liters. 
  4. Bring to a boil, cover and cook for about 45 minutes over medium heat until the quince is tender. 
  5. drain the pieces of quince and the pressed nouet in a colander, in another pan, we must keep this cooking water 
  6. Measure the weight of the water obtained, and measure the same weight of sugar. 
  7. Cook the mixture, with the nouet attached to the jam dish over medium heat 
  8. foam each time the foam that forms. and check the cooking of your jelly, if you pour a drop of liquid on a cold plate, and it forms a beautiful little ball that does not spread, your jelly is ready. 
  9. Pour the hot jelly into sterilized jars and close. 



[ ![quince jelly 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gelee-de-coings-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gel%C3%A9e-de-coings.jpg>)
