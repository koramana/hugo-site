---
title: unbreakable string bread without kneading
date: '2018-01-19'
categories:
- bakery
- Mina el forn
- bread, traditional bread, cake
tags:
- To taste
- Boulange
- Morocco
- Algeria
- Ramadan
- House Bread
- Khobz dar

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/01/pain-ficelle-inratable-sans-petrissage-2.jpg
---
![unbreakable string bread without filling 2](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/pain-ficelle-inratable-sans-petrissage-2.jpg)

##  unbreakable string bread without kneading 

Hello everybody, 

If you have never tasted a string of black seed beans, I tell you my friends, you missed a lot of good things in your life, so quickly to your stove to quickly make this recipe string unattainable without kneading. 

This bread string is just to fall, super greedy, super good and above all super easy to achieve. With us this bread is almost always on the table, my children like it a lot, they find it super easy to nibble. And what is good in all this, that the recipe of this bread unalterable and without kneading is just a breeze. Try it and tell me the result. 

This recipe I discovered a few years ago on the forum Eldjelfa, shared by Oum Roumaissa. 

To show you how easy this recipe is, I share the recipe with you: 

**unbreakable string bread without kneading**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/pain-ficelle-inratable-sans-petrissage.jpg)

**Ingredients** for leaven: 

  * 200 ml warm water 
  * 4 c. flour 
  * 1 C. tablespoons sugar 
  * 1 C. instant baker's yeast 

For the pasta: 
  * 50 gr of melted butter 
  * 100 ml of milk 
  * 1 C. sugar coffee 
  * 1 C. salt 
  * flour to pick up the dough 

gilding: 
  * Egg yolk 
  * milk 
  * seed of nigella 



**Realization steps** We start by preparing the leaven: 

  1. in a salad bowl, place the warm water, add the flour, the sugar and the yeast 
  2. mix well, cover and let the yeast take effect, until you have a good sparkling mixture. 

we prepare the pasta: 
  1. Melt the butter, add the milk and sugar, and add the sourdough. 
  2. add the salt, then the flour while mixing until the dough collects, but remains a bit sticky. 
  3. knead 1 minute just the time to homogenize everything. 
  4. place the dough in a clean bowl and allow to double the volume away from drafts. 
  5. Take the dough again, degas it out without too much work, and cut it into 10 or 12 pellets, depending on how long you want to give your bread. 
  6. shape the chopsticks, you can see the video at the top, to see the shaping method. 
  7. place the chopsticks on a tray lined with baking paper. 
  8. brush the chopsticks with the egg yolk and milk mixture. and sprinkle the surface of black seed. 
  9. cook in a preheated oven at 180 ° C until the loaves turn a nice golden color. 



![unbreakable string bread without linting 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/pain-ficelle-inratable-sans-petrissage-1.jpg)
