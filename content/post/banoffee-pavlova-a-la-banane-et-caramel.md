---
title: Banoffee pavlova with banana and caramel
date: '2016-04-21'
categories:
- cakes and cakes
- sweet recipes
tags:
- desserts
- To taste
- delicacies
- meringues
- Chantilly
- Easy recipe
- Fast Recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane-2.jpg
---
[ ![Banoffee pavlova with banana and caramel 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane-2.jpg>)

##  Banoffee pavlova with banana and caramel 

Hello everybody, 

If you ask my children: what is your favorite dessert, they will not hesitate for a second and say: Pavlova !!! I often make this dessert super easy, and what I like in the homemade pavlova is that I can put less sugar in the meringue, less sugar in the whipped cream of the times I do not put even sugar in whipped cream. 

Happy that finally I was able to gather all the ingredients without exception to make my recipe for **banoffee pavlova** , or **pavlova with banana and caramel** , and I promise you something, I did not even have time to take a picture of the cup. It's gone in the blink of an eye, and with the spoon you will not stop at two or three. 

[ ![Banoffee pavlova with banana and caramel 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane-3-a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane-3-a.jpg>)   


**Banoffee pavlova with banana and caramel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane-4.jpg)

portions:  8  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients** for the meringue: 

  * 3 egg whites 
  * 150 gr of sugar 
  * 2 teaspoons of Maïzena 
  * 1 teaspoon of caramel syrup (for the perfume) 

for garnish : 
  * 2 bananas 
  * 200 ml of whole cold liquid cream very cold 
  * 1 C. tablespoon icing sugar 
  * 50 gr of [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-beurre-sale.html>)



**Realization steps**

  1. Preparation of the meringue: 
  2. Preheat the oven to 130 ° C. 
  3. Using a drummer, mount the whites to very firm snow starting at medium speed then gradually increasing. 
  4. Gradually add the sugar to the spoon while the egg whites rise. Then add the caramel syrup and cornflour and mix again. 
  5. At this point you can use the pastry bag to form the meringue disc, I did not do it. I made a record of almost 20 cm well filled, and I just shaped the edges with a fork.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/meringue-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/meringue-.jpg>)
  6. Bake for 1 cooking at 130 ° C. (If you double the amount, increase the cooking time) 
  7. Remove from oven and let cool before taking off.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane.jpg>)
  8. before serving the dessert (at least 1 hour before), put the whole cold cream very cold in whipped cream with 1 tablespoon of sugar 
  9. Spread the whipped cream on the meringue which must be cold. 
  10. cut the bananas into rings and decorate the dessert with. 
  11. sprinkle with salted butter caramel 
  12. Book fresh until served. 



[ ![Banoffee pavlova with banana and caramel 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/banoffee-pavlova-a-la-banane-5.jpg>)
