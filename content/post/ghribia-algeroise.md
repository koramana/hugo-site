---
title: ghribia algeroise
date: '2012-03-31'
categories:
- Café amour de cuisine
- Algerian cakes- oriental- modern- fete aid
- Index

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/ghribiyat-ezite-ghribiya-a-l-huile-ghriba-gateau-algeri11.jpg
---
##  ghribia algeroise 

Hello everybody, 

Frankly, I did not expect that result at all. the Algerian ghribia is too good, my father besides asks me to make him a stock before leaving to England. 

in any case for even more recipe of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , visit the [ index of Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) (I thank Lunetoiles who helped me a lot in the update of the latter)   


**ghribia algeroise**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/ghribiyat-ezite-ghribiya-a-l-huile-ghriba-gateau-algeri11.jpg)

**Ingredients**

  * 1 measure overflowing with oil (I use as measure a water glass of 250 ml) 
  * ½ measure of crystallized sugar (half icing sugar, half crystallized sugar) 
  * 3 measures of flour 



**Realization steps**

  1. in a terrine mix the overflowing oil and the sugar then leave to rest about half an hour. 
  2. after adding the three measures of flour by mixing until you get a dough not too firm but handy (if the dough does not pick up you can add a little oil) I can tell you that for me and because of my flour I still add flour, because I could not form balls, but this did not change at all the good taste of the ghribiya which was too fondant, wonderfully. 
  3. Form dumplings the size of a walnut, put in a tray and bake at 180 ° before heating. 



(you can put on a pinch of cinnamon) 

once cooked, leave to cool before turning out 

suivez moi sur: 
