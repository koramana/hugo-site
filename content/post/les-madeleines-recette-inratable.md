---
title: Madeleines, an inescapable recipe
date: '2013-02-11'
categories:
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/madeleine-chocolat%C3%A9e.jpg
---
[ ![chocolate madeleine](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/madeleine-chocolat%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/madeleine-chocolat%C3%A9e.jpg>)

##  Madeleines, an inescapable recipe 

Hello everybody, 

again a recipe from the archives of my blog, you have forgotten that the .... It was the first time that I had made madeleine, and I admit to having been very happy with the result. Besides, this recipe later had made happy. 

You too, do not deprived yourself of this delight, personally the only thing that stopped me from making these madeleines unbreakable was the mold madeleine. But since I have it, I do not miss at all to do and remake madeleines all good and super perfumed with lemon. 

[ ![unbreakable madeleines 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/madeleines-inratables-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/madeleines-inratables-1.jpg>)

**Madeleines, an inescapable recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/madeleines-inratables.jpg)

portions:  12  Prep time:  20 mins  cooking:  12 mins  total:  32 mins 

**Ingredients**

  * 2 eggs 
  * Zest of a lemon (untreated) 
  * ½ sachet baking powder 
  * 100 gr of flour 
  * 100 gr of butter 
  * 100 gr of sugar 
  * 1 teaspoon of salt 



**Realization steps**

  1. Take the zest of the lemon. 
  2. Mix with 2 whole eggs, sugar and salt. 
  3. Whisk all to obtain a creamy foam. 
  4. Melt the butter in the microwave for 20 seconds. 
  5. Sift the flour and yeast over the previous mixture and mix. 
  6. Finally stir in the butter. Let the dough rest for 2 hours if possible and cool (fridge). 
  7. Place a spoonful of dough in each madeleine mold, 
  8. and put in a hot oven at 200 ° C for 10-12 minutes. 



[ ![irresistible madeleines 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/madeleines-inratbles-31.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/madeleines-inratbles-31.jpg>)

pour la couche de chocolat, vous pouvez mettre un morceau de chocolat dans le moule a madeleine, le mettre au four éteint, le temps que le chocolat fond un peu, et vous placez vos madeleines sur le chocolat fondu, et vous les laissez dans le moule, dans un endroit frais, jusqu’a ce que la madeleine se détache toute seule du moule. et vous avez ce beau nappage au chocolat. 
