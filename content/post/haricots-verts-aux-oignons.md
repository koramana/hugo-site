---
title: green beans with onions
date: '2009-07-03'
categories:
- Coffee love of cooking

---
a very fast dish, and very delicious, and especially very convenient for hot days. 

Ingredients: 

\- Chicken pieces 

\- green beans 

\- 3 onions 

\- 4 fresh tomatoes, or canned tomatoes, in pieces 

\- 2 cloves garlic 

\- salt, black pepper, olive oil 

in a pressure cooker, fry the onion cut into thin slices in length, add the pieces of chicken, salt and pepper, let return well until the onion becomes very soft. 

add the garlic and tomato cut into pieces, then add the beans, well cleaned. 

close the pressure cooker, and cook. 

bon Appétit 
