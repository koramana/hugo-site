---
title: homemade rechta / yennayer 2013
date: '2013-01-10'
categories:
- Algerian cakes- oriental- modern- fete aid

---
Hello everybody, 

The Berber New Year or Yennayer corresponds to January 12th of our Julian calendar. It marks the beginning of the year among the Berbers and devotes the passage of "The Door of the Year". 

Versions differ as to the origin of the name and celebration of that date. Made up of two words, yiwen and agur, yennayer would literally mean the first month, the month of January. 

The Berber New Year is traditionally celebrated everywhere in North Africa. This date from the agrarian calendar has been used by the Berbers since ancient times. It is linked to the cycle of the seasons and celebrated in a festive atmosphere that lasts for two to four days. 

In practice this holiday must be auspicious. It is primarily marked by the consumption of a rich meal. All the guests must leave the table satiated so that the year is prosperous. 

**The story of January and February**

Once upon a time there was an old woman who lived alone with her goat. One year, the month of January brought so much cold, snow and rain that the old woman and her goat could not get out. When January ended, a beautiful sun began to shine. The old woman went out with her goat and exclaimed: 

January, be cursed! You left and you brought me nothing good! 

January went immediately to February and asked him: 

I beg you, my friend February, lend me one of your days that I can come back and silence that old shrew. 

February lent him one of his days. Then, the sky filled with threatening clouds, it began to rain and to snow, the wind began to blow. The old woman and her goat were petrified with cold and died on the spot. 

This is why the last day of January is called the "day loaned" and so the month of February is shorter than the others. 

and you know what, I know this story, my grandmother tell us about it every time, during the Yennayer celebrations .... 

for my part, this year I'm going to make a [ rechta ](<https://www.amourdecuisine.fr/article-rechta-algeroise-91236467.html>) , and here is the home made recipe, a dish that I like a lot, but that I do not do often, because every time I go to Algeria, I forget to buy rechta trade. 

at home, we rarely do this dish, because at home it's the dishes that my father loves that take place, and my father likes it, [ chakhchoukha edfar ](<https://www.amourdecuisine.fr/article-26090486.html>) or [ chekhchukhat biskria ](<https://www.amourdecuisine.fr/article-chakhchoukha-de-biskra-charchoura-biskria-112155334.html>) ... 

I did not know how to make the house recess, but yesterday I talked with my mother and she passed me the recipe, so if you want to eat the recipe today think about preparing the dough the day before ....   
ingredients: 

  * 450 gr of fine semolina 
  * 150 gr sifted flour 
  * 1 tablespoon of table oil 
  * Water at room temperature 
  * 1 teaspoon of salt 



procedure: 

  1. In a large salad bowl, put the semolina salt and flour 
  2. add the oil, amalgam 
  3. spray gently with water until you have a very firm dough, 
  4. knead to homogenize everything 
  5. form a pudding and wrap with a film of food. 
  6. put in the fridge all night. 
  7. The next day take out the dough and leave at room temperature. 
  8. cut out pieces, roll down just a little bit so that you can machine them again. 
  9. if your dough is too soft do not hesitate to sprinkle with flour. 
  10. go first to n ° 1 then n ° 2 and so on go back last time to n ° 7 
  11. you are going to get a very fine dough strip that you go to put on a clean cloth to let dry a little (just a little, if it dries a lot, you may not be able to pass in the compartment spaguitti) 
  12. continue until you have used up all your dough 
  13. take the strips of dough and pass them in your machine to have fine spaghetti. 
  14. coat the Rechta with 2 tablespoons of oil to open them, put them in the top of the couscous for 15 min 
  15. to the escape of steam. spill your pasta in a large salad bowl, sprinkle it a little bit with a glass of water, let it absorb oil and put one last time on the top of the couscoussier. 
  16. like that the dough is ready, and the recipe dish rechta and next [ this link ](<https://www.amourdecuisine.fr/article-rechta-algeroise-91236467.html>) . 



et pour d’autres recettes de plats traditionels algeriens je vous invite a aller voir cet index bien sympa 
