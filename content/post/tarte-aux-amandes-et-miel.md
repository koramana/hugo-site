---
title: almond and honey pie
date: '2013-09-19'
categories:
- dessert, crumbles and bars
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tarte-amande-et-miel1.jpg
---
![almond and honey pie](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tarte-amande-et-miel1.jpg)

##  almond and honey pie 

Hello everybody, 

Here is a very nice pie made from flaked almonds, a tart almond honey, a combination to fall ... an endless crisp, a very different fine tart, but still chewable without looking around. 

Another delicious recipe that shares with us Lunetoiles, to whom I give a big thank you.   


**almond and honey pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tarte-amande-miel1.jpg)

Recipe type:  pie  portions:  8  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 1 ready-made or homemade dough 
  * 1 large tablespoon of whole cream 
  * 1 large tablespoon of honey 
  * 75 g of sweet butter 
  * 100 g of sugar 
  * 150 g slivered almonds 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. Garnish with a pastry pie pan and cook for 10 minutes. 
  3. Meanwhile, in a saucepan heat the whole cream, honey, butter and sugar. 
  4. As soon as the mixture is melted and smooth, add the flaked almonds. 
  5. Spread this mixture over the pre-baked dough and put back in the oven at 180 ° C for 20 minutes, or until the pie is golden brown. 
  6. Let cool then cut into pieces or cut into several pieces like big squares or bars. 
  7. Present on a plate and enjoy. 



![tarte aux almond-and-miel.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tarte-aux-amandes-et-miel1.jpg)
