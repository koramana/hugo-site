---
title: my achievements at home 23
date: '2012-08-17'
categories:
- dessert, crumbles et barres
- cakes and cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/P1080229_thumb1.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

you were many and many to realize full of my recipes in this month, and since I am here in Algeria, I had big problems of connection, or cut of electricity. 

and when I went to take a look at my email: [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>) , I find full of your beautiful messages, with your sublime achievements, 

So, I'll write this article in several times, to be able to publish everything. 

thank you again, and do not forget I am waiting for your achievements for the Aid el Fitr 2012, and I will be very happy to publish the photos of your trays for Aid. 

[ Two-tone Cornets ](<https://www.amourdecuisine.fr/article-cornets-bicolores-aux-amandes-et-noix-90180007.html>) at Amelle's place 

[ buns with minced meat ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee-96050112.html>) at Amelle's place 

[ lemkhidettes ](<https://www.amourdecuisine.fr/article-lemkhidette-gateau-algerien-pour-l-aid-2012-109140262.html>) at Amelle's place 

[ ![P1080229](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/P1080229_thumb1.jpg) ](<http://amour-de-cuisine.com/wp-content/uploads/2012/08/P1080229.jpg>)

![2012-07-28 11.44.05](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/2012-07-28-11.44.051.jpg)

merci a vous pour vos realisations, et attendez tout de suite, le reste des realisations. 
