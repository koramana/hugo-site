---
title: Easy peach juice
date: '2016-06-18'
categories:
- boissons jus et cocktail sans alcool
tags:
- Ramadan 2016
- Easy cooking
- Ramadan
- Drinks
- Summer kitchen
- cocktails
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Jus-de-peches-1.jpg
---
![Peach juice 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Jus-de-peches-1.jpg)

##  Easy peach juice 

Hello everybody, 

During this Ramadan, my husband bought a lot of peaches, a little tart, but super good for my taste. Unfortunately the children were of my opinion, I tried to introduce this delicious fruit in fruit salads, they did not try to taste it. 

I wanted to make a [ peach jam ](<https://www.amourdecuisine.fr/article-confiture-de-peches.html>) but in the end I leaned for a fresh juice, which I will enjoy with pleasure after the fast. To my surprise, even the children liked it, besides now I realize one day out of two, hihihihi fortunately for me that it is an easy recipe.   


**Easy peach juice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Jus-de-peches-2.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 1 kg of peaches 
  * sugar according to taste and according to the acidity of peaches 
  * 2 slices of untreated lemon 
  * water (for a lighter juice) 



**Realization steps**

  1. wash the peaches generously under abundant water. 
  2. cut the peaches into cubes and remove the pips. 
  3. place the peaches in a deep pot. 
  4. add 2 or 3 tablespoons sugar, the slices of lemon and cover with water. 
  5. let boil for 10 to 15 minutes, while skimming each time the foam that is formed. 
  6. When the peaches are tender and cooked, remove from heat and remove the slices of lemon 
  7. pour into the blender bowl, or use a blender stand to mash 
  8. go to Chinese to have a velvety liquid without skin pieces ... 
  9. taste and adjust by adding the sugar, and adding a little water if you find that it is a little thick. 
  10. cool until serving juice. Treat yourself. 



![Peach juice](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Jus-de-peches.jpg)
