---
title: Chicken Biryani
date: '2015-05-22'
categories:
- cuisine indienne
- Cuisine by country
- recette de ramadan
- recettes a la viande de poulet ( halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/biryani-au-poulet-1.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/biryani-au-poulet-1.jpg)

##  Chicken Biryani 

Hello, 

I am Houda, I am a faithful reader of your blog Ms. Soulef, and it is with great pleasure that I want to share with you the recipe that I never miss the **chicken biryani rice** . 

I learned this recipe from neighbors when I lived in the Emirates, because rice is a dish always present on the table of oriental cuisine. 

I hope my **chicken biryani recipe** will please you a lot, and thank you Mrs Soulef who gave us the chance to share our recipes on her very nice blog. 

**Chicken Biryani**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/biryani-au-poulet-001.jpg)

portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 half glass of table oil 
  * 4 to 5 onions 
  * 2 tablespoons of biryani spice 
  * 1 cup black pepper 
  * 1 teaspoon coriander beans 
  * 1 cup of cardamom seeds 
  * 1 cup of turmeric coffee 
  * 1 piece of fresh ginger almost 2 cm 
  * 1 cup powdered ginger 
  * an entire chicken cut into pieces 
  * 5 fresh tomatoes 
  * 1 cup of garam massala 
  * 1 teaspoon of garlic powder 
  * ½ teaspoon yellow food dye 
  * ½ tsp red chilli powder (optional) 
  * 1 big box of natural yogurt 220 ml 
  * 250 gr of basmati rice 



**Realization steps**

  1. cut the onions in pieces and put them in a pot in the oil, and cover the 
  2. cook over low heat until onion is well cooked and transparent.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani-300x238.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani-300x238.jpg>)
  3. When the onion is well cooked, add the mashed tomato, add the spices: turmeric, powdered ginger and fresh, garam massala, garlic powder, red pepper, black pepper, and the spicy seeds.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani1-300x238.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani1-300x238.jpg>)
  4. add the chicken pieces and salt, and let it cook again while stirring, until the chicken turns white.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani2-300x238.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani2-300x238.jpg>)
  5. let it simmer a little, when you start to see the oil rising a little to the surface, add the yogurt, and stir constantly so that it does not stick to the bottom, 
  6. Cook the rice in a large amount of salted water, put bake remove the rice from the heat and rinse so that it does not continue cooking   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/biryani-013-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/biryani-013-300x200.jpg>)
  7. Now take a large baking tin, or a large oven-safe pot 
  8. cover the bottom of the pot with a layer of rice, add over a layer of sauce, then another layer of rice, and then a layer of sauce, do so until there is no more rice   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani3.jpg>)
  9. keep a little sauce. 
  10. take a yellow food coloring, dilute it in a little water and moisten it on the top of the rice   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/2013-10-26-biryani4.jpg>)
  11. put the mold or the pot in the hot oven for almost 10 to 15 minutes, do not cook too much otherwise the rice will be pasty. 
  12. treat yourself 



[ ![chicken biryani](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/biryani-au-poulet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/biryani-au-poulet.jpg>)

Merci Houda, les photos sont réellement sublimes. 
