---
title: Verrines Pearls of Japan and Khaki
date: '2012-09-22'
categories:
- crepes, donuts, donuts, sweet waffles

---
Hello everyone, very pretty and very delicious verrines with Japanese pearls (tapioca) and khaki (plaquemines), to be honest, I had to buy Japanese pearls (tapioca) for a cake recipe, but here I go I have never bought this ingredient in my life, and in truth it comes in two forms, in pearls, what I bought, and in crystals, which was intended for the cake recipe. in the recipe, I had to put the Tapioca crystals, with the milk and leave for 3 hours so that its form a paste (& hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

very pretty and very delicious **Japanese pearls (tapioca) and khaki (plaquemines)** to be frank, I'd buy the **japan beads** (tapioca) for a cake recipe, but voila, I never buy this ingredient in my life, and in truth it comes in two forms, in pearls, what I buy, and in crystals, which was planned for the cake recipe. 

in the recipe, I had to put the tapioca crystals, with the milk and leave for 3 hours so that its form a paste (so the tapioca swells) 

so, do not know, I put the pearls of Japan, 3 hours in the milk, I come back, and nothing has changed. hihihihi, I put in the fridge for another whole night .... still nothing, so I came back to the recipe, I reread, and she said: crystals ... aha !!!! elementary dear Watson !!! 

what should I do, I come back to my box containing tapioca, and I find a recipe in the package. 

it was tapioca verrines (japan pearls) with banana, a recipe coming soon. 

I look at my ingredients, and since there must always be a "but", I have no milk ???? 

I'm looking in my closet, and I have non-sugar condensed milk ... it must be the case, the only problem, yes yes, there is a problem, if I do with condensed milk unsweetened, which has a dark color, and with a layer of banana, well, I was not going have beautiful verrines. so I'm not the recipe of the package and I'm going for another adventure. 

ingredients: 

  * 50 gr of pearls from Japan 
  * 200 ml unsweetened condensed milk 
  * 400 ml of water (you can replace with 600 ml of milk: 200 ml of condensed milk + 400 ml of water) 
  * 25 gr of sugar 
  * 3 to 4 tablespoons fresh cream 
  * 3 persimmons 
  * 6 tablespoons of [ custard ](<https://www.amourdecuisine.fr/article-creme-anglaise-custard-88689731.html>) . 



method of preparation: 

  1. cook the pearls of Japan, with the sugar and milk over low heat while stirring, for almost 40 minutes, or until the pearls are well cooked, they become translucent. 
  2. at the end of cooking, leave aside to cool well, the mixture will become very thick. 
  3. add the cream, and leave aside. 
  4. clean the persimmons, and crush them. 
  5. proceed to mount the verrines (for this quantity I had 3 verrines of 220 ml each, so it depends on the size of your verrines). 
  6. pour half of the tapioca mixture into the verrines. 
  7. cover with a nice layer of crushed kaki 
  8. pour 2 tablespoons of custard in each verrines (or if like me you like custard, use more) 
  9. cover with another layer of the mixture of japanese pearls. 
  10. then decorate according to your choice, and with what you have on hand. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
