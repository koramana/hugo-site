---
title: Pastry of praline with peanuts
date: '2013-09-14'
categories:
- dessert, crumbles and bars
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pate-a-parline-aux-cacahuetes2.jpg
---
![pate-a-PARLINE-to-cacahuetes.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pate-a-parline-aux-cacahuetes2.jpg)

##  Pastry of praline with peanuts 

Hello everybody, 

A delicious peanut pudding made by our beloved Lunetoiles, which she used as ingredients for the preparation of a delicious trianon (recipe to come) 

I admit that I am very tempted by the taste, especially when I imagine this delicious smell that emerges from this smooth pasta ...   


**Pastry of praline with peanuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pate-praline-aux-cacahuetes2.jpg)

Recipe type:  spread  portions:  8  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 250 g of peanuts (or a mixture of 125 g of whole white almonds and 125 g of peanuts, would be much better, that's what I wanted to do, but I had more almonds.) 
  * 200g of powdered sugar 
  * 50g of water 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. Put the peanuts in a baking dish 
  3. Broil for 10 minutes, stirring occasionally and watching, they should not be too brown, because they will continue to caramelize in the sugar. 
  4. Put 200g of sugar in a saucepan, add 50g of water and cook between 125 to 130 ° C check with a cooking thermometer. 
  5. When the sugar reaches the right temperature, add the roasted peanuts all at once and mix quickly until the sugar whitens and recrystallizes. 
  6. It is said that it "sand" or that it "mass". 
  7. Put the pan over medium heat and lightly caramelize the preparation (the darker the caramel, the more praline will be strong in taste). 
  8. When the desired color is obtained, pour the preparation on a plate with baking paper and let cool completely. 
  9. Break into pieces and put everything in a blender (bowl with knife) and mix. 
  10. At the beginning you get a powder, you continue to grind and after a lot of time, you get a paste that forms a fairly compact ball in the robot. 
  11. Continue to operate the robot so that the ball of dough becomes softer and turn into puree. 
  12. This praline can be kept for several months in an airtight jar and in a temperate place. 



Preparation video: 
