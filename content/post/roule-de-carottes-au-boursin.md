---
title: Rolled carrots with the bursary
date: '2017-03-03'
categories:
- amuse bouche, tapas, mise en bouche
- idee, recette de fete, aperitif apero dinatoire
- recettes salées
tags:
- Picnic
- accompaniment
- Cocktail dinner
- Amuse bouche
- Aperitif
- Buffet
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/roul%C3%A9-de-carottes-au-boursin-1.jpg
---
![rolled carrots at the bazaar 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/roul%C3%A9-de-carottes-au-boursin-1.jpg)

##  Rolled carrots with the bursary 

Hello everybody, 

I am very happy to finally share with you this carrot roll recipe at the biddin. I made this super delicious recipe of appetizers ideal for your picnic baskets or to be buffed on holiday tables, so I realized it more than 2 weeks ago, but I could not post it before. 

It's wonderful that the game continues fluidly especially with all these beautiful godmothers who make every effort to ensure that the participants are well informed and have the list of entries just in time: 

I share with you the video of the roll of carrots to the bursary: 

**Rolled carrots with the bursary**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/roul%C3%A9-de-carottes-au-boursin-2.jpg)

portions:  12  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients**

  * 200 gr of carrot puree. 
  * 50 gr of flour 
  * 10 gr of cornflour 
  * 3 eggs (yellow + white) 
  * 1 C. coffee baking powder 
  * 1 pinch of cumin 
  * nutmeg, salt 
  * 250 gr of Boursin cheese 



**Realization steps**

  1. wash and peel the carrots (almost 400 gr) Steam 
  2. puree the carrots and weigh 200 gr. 
  3. preheat the oven to 180 ° C 
  4. In a salad bowl, mix the egg yolks, salt, caraway, nutmeg and carrot puree, 
  5. introduce the mixture flour, corn flour and baking powder. 
  6. With a drummer, mount the whites very firm. 
  7. Gently add the whites to snow in the previous preparation with a maryse 
  8. Pour the dough on a baking tray lined with buttered parchment paper, trying to get a rectangle of regular thickness. 
  9. Bake for 25 minutes. 
  10. When the biscuit is cooked, let it cool, then spread evenly the cheese boursin 
  11. gently roll the carrot biscuit (trying not to break it, and trying to tighten) 
  12. Place the roll on a sheet of film and wrap it tight, before letting it rest for several hours in the refrigerator 
  13. At the time of serving, take the roll out of the plastic film, cut it into slices and enjoy! 



![rolled carrots at the biddin](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/roul%C3%A9-de-carottes-au-boursin.jpg)

And here is the list of participants, I advise you to take a tour to see all these beautiful participations: 

5- Soulef of the blog [ "Kitchen love" ](<https://www.amourdecuisine.fr/>) with a **Roulé de carottes au boursin**
