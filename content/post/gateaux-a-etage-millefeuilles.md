---
title: cakes with millefeuilles layer
date: '2011-10-31'
categories:
- boissons jus et cocktail sans alcool
- cuisine brule-graisses, recettes amaigrissantes
- Detox kitchen
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau-marbre-1_thumb1.jpg
---
![cakes with millefeuilles layer](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau-marbre-1_thumb1.jpg)

##  cakes with millefeuilles layer 

Hello everybody, 

Thanks to Lila, my reader, for sharing this millefeuilles cake recipe that she presented during her sister's engagement. 

millefeuilles cake is actually a cake made of 3 layers of shortcake, one layer is colored with cocoa, then covered with a nice white chocolate decorated with dark chocolate strokes to have the marbled effect of a traditional millefeuilles. 

I hope you will like this recipe.   


**cakes with millefeuilles layer**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau_thumb.jpg)

**Ingredients**

  * 6 eggs 
  * 1 glass of oil 
  * 1 glass of melted butter 
  * 2 glass of sugar 
  * 1 tablespoon vanilla 
  * 1 and a half packets of baking powder 
  * flour 
  * cocoa 
  * white chocolate and some milk chocolate 



**Realization steps**

  1. in a salad bowl put the ingredients in order while mixing as you go 
  2. pick up the dough with flour until you have a paste that does not stick (the dough will look oily is nothing) 
  3. separate the dough in 3 
  4. take a ball of dough then add cocoa until very dark brown color! 
  5. let's go to the assembly take some greaseproof paper to take enough to cover the dish 
  6. then take a plain dough and then flatten it on the parchment paper and then lift 
  7. put on a medium dish, 
  8. take a piece of parchment paper spread the cocoa paste and then lift and return very quickly on the white paste already put (be careful the dough can break is not important to pick up the pieces) 
  9. do the same for the white paste well spread 
  10. prick with a fork. 
  11. put in the oven over medium heat and remove from the oven when it is just brown. 
  12. plant a knife to see if it's cooked. 
  13. let cool then cover with melted chocolate. 



![le-plateau-2 thumb](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/le-plateau-2_thumb1.jpg)

thank you Leila for this delicious recipe. 

Thanks for your visit and your comments. 

bonne fin de semaine. 
