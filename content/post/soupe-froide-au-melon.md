---
title: Cold melon soup
date: '2013-09-24'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/soupe-detox-soupe-froide.CR2_1.jpg
---
[ ![Detox Soup-Soup-froide.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/soupe-detox-soupe-froide.CR2_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/soupe-detox-soupe-froide.CR2_1.jpg>)

Hello everybody, 

This is what I like to prepare the day after a hearty dish like [ lasagna ](<https://www.amourdecuisine.fr/article-lasagne-a-la-bolognaise-120117109.html>) , or any other gratin, a cold soup, a detox soup very refreshing, and very relaxing ... this time I chose a tart sweet soup, this cold melon soup is a delight. 

According to the Vitamix TNC 5200 brochure, it can be used for making smoothies, or as I have made cold soups, and you will get an incomparable creaminess, and velvety soups. 

**Cold melon soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/soupe-detox-soupe-froide.CR2_1.jpg)

Recipe type:  Entrance  portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 1 well ripe melon of about 1.3 kg 
  * the juice of 1 lime 
  * 3 cm of fresh ginger root, peeled and grated 
  * sugar according to your needs 
  * 180 g frozen cherries or any other fruit you like. 
  * 120 ml of orange juice 
  * 2 tablespoons Greek yoghurt 
  * 100 g fresh strawberries for decoration. 



**Realization steps**

  1. Cut the melon in half, remove the seeds and peel it. 
  2. Cut the flesh into pieces and put them in the bowl of your robot. 
  3. Add lemon juice and ginger and mix until smooth and even. 
  4. Pour it into a salad bowl, cover with cling film and reserve in the refrigerator. 
  5. Put the cherries, orange juice and yogurt in the cleaned bowl of the robot. Mix, pour the grout obtained in a salad bowl, cover with cling film and reserve in the refrigerator. 
  6. Divide the melon soup into 4 chilled cups. 
  7. Drop 1 tbsp. tablespoon of cherry coulis on top. 
  8. Decorate with strawberries and serve. 



[ ![Cold soup-au melon.CR2_2](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/soupe-froide-au-melon.CR2_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/soupe-froide-au-melon.CR2_2.jpg>)
