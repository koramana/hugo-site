---
title: Mom contest I love you
date: '2010-05-03'
categories:
- cakes and cakes
- recettes sucrees

---
yes I participate in the game: Some practical details The contest runs from May 1 to May 31, 2010 The recipe will be posted on your blog if you have one, with a link to this page. In order to then summarize all your recipes, we thank you for the photo of your recipe and its link (maximum size of 300 * 400 or 400 * 300) to the address: concours.mamanjetaime@yahoo.fr and to inform us of this sending by depositing a comment on one of our blogs For those who do not have a blog, they & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

yes I participate in the game: 

![](https://www.amourdecuisine.fr//import/http://2.bp.blogspot.com/_ovvvKkqRHIE/S9lxOsFTjVI/AAAAAAAACIM/j2x_dPnhDFQ/s320/)

** Some practical details  **

  * The contest runs from May 1st to May 31st, 2010 
  * The recipe should be posted on your blog if you have one, with a link to this page. 
  * In order to then summarize all your recipes, we thank you for the photo of your recipe and its link (maximum size of 300 * 400 or 400 * 300) to the address: concours.mamanjetaime@yahoo.fr and to inform us of this sending by depositing a comment on one of our blogs 
  * For those who do not have a blog, they can send the recipe and the photo by mail, it will be posted on one of our blog. 
  * You can offer up to 3 recipes. They will be judged according to:   
\- Originality,   
\- Ingredients used and associations,   
\- The love that transpires from the realization. 



** First batch  **

1 set of 4 mini-casseroles (2 squares and 2 rounds)   
1 hinged mold   
1 baking tray   
1 pudding mold 

** Second batch  **

1 set of 4 mini-casseroles (2 squares and 2 rounds)   
1 tart pan with removable bottom 

** Third batch  **

1 set of 4 mini-casseroles (2 squares and 2 rounds) 

I give you the recipe: 

hello everyone, a little late to deliver my little recipe, but must say that I was drowned in the middle of your gifts, open each gift to read your words, you were many and I thank you for that, I think I'll do my birthday again in 15 days ... what do you think? 

I invite you ok, hihihihihih ...... with your gifts, hihihihihi 

Come on, I'll stop "my jokes" as the English say, and go to the recipe, ok ?! 

in truth, this cake was based on a muslin cake with hazelnut, covered with a thin layer of whipped cream, and decorate in the final with almond paste, nothing difficult, everything is easy to do, except for decoration you have to have a little passion, hihihihihi. I did it while my little monsters were sleeping, otherwise you would not have seen a flower garden, but I would say it would snow, and the snow covered the flowers (I know my daughter would eat everything, like she did it elsewhere) 

so at the base, I made the cake chiffon which here my ingredients :( for a square mold of 23 × 23 cm) 

  * 8 eggs 
  * 250 grams of sugar 
  * 3 c. tablespoon of table oil 
  * 6 c.a milk soup 
  * 50 grs of ground hazelnuts 
  * hazelnut extract (or almond as for me) 
  * 210 gr of flour 
  * 2 tablespoons of baking powder 



for decoration: 

  * 700 ml thick cream 
  * sugar according to taste 
  * some pieces of peaches 
  * almond paste colors of your choice for decoration. 



start by preparing the cake. 

preheated the oven to 180 degrees C, 10 min before cooking 

separate the egg whites from the yolks, and turn the egg white into snow with 2 tablespoons of sugar, leave aside 

whisk the egg yolks with the sugar, until the mixture whitens, add the oil while whisking, then the milk, add the hazelnuts powder (I find it at Lidl), then slowly flour and yeast has this mixture. 

add the egg white by mixing with a spatula, making movements up and down, not to break the egg white, and to have an airy mixture. 

pour the mixture of a buttered and floured mold, or covered with parchment paper, and cook for 45 minutes, or depending on the capacity of your oven, you can check the cooking with a knife. 

remove from the oven after cooking and let cool. 

after cooling, cut the cake in half. and soak it with syrup, or like me, with the juice that is in the fishing box. 

add the thick cream in whipped cream with a little sugar according to your taste, then garnish the middle of the cake (you can put the fruits you want in) 

cover with the other half of the cake, and cover the whole cake and put in the fridge. 

during this time prepare it, and color your almond paste, I use for that powdered dye that I buy in Algeria, I used white for white flowers. red not much, for the color of the basket, yellow rose and green, then for the remaining yellow almond paste, I add a little red color to have the small flowers in orange brick. 

for decoration from above, it is necessary to provide you with a punch in the shape of leaf and flower. 

fill all the surface of the cake. 

There you go 

in any case it was a cake very very delicious, we devoured incredibly in two days (between us 4), even my husband find it very good, not too sweet, especially with the bitter taste of almond (in the almond paste) frankly a real happiness. 

once again thank you for all your wishes. 

[ vous aimez mon blog, ajouter le a vos favoris  ](<javascript:if\(window.sidebar\)%7Bwindow.sidebar.addPanel\('amour%20de%20cuisine','https://www.amourdecuisine.fr',''\);%7Delse%20if\(window.external\)%7Bwindow.external.AddFavorite\('http://votre%20site','Le%20Nom%20du%20lien'\);%7D>)
