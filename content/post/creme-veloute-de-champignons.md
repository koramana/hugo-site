---
title: mushroom cream
date: '2017-10-29'
categories:
- soups and velvets
tags:
- inputs
- Hot Entry
- Vegetarian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/velout%C3%A9-de-champignons.jpg
---
![velvety cream of mushrooms](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/velout%C3%A9-de-champignons.jpg)

##  mushroom cream 

Hello everybody, 

I really like this velvety mushroom or mushroom cream, and especially this cold, creamy and well-flavored velouté is just a delight! This velvety mushroom is very comforting as I accompanied a delicious [ khobz dar without kneading ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) . 

As you can see on this video, this mushroom soup is very easy to make, it is too good, very tasty that you will not regret to taste, and you only need few ingredients. 

**velvety cream of mushrooms**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/velout%C3%A9-de-champignons-2.jpg)

Recipe type:  soup, velvety  portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 2 tbsp. tablespoon of olive oil 
  * 1 large leek 
  * 200 to 250 gr of cut white mushrooms 
  * 2 cloves garlic chopped. 
  * 500 ml of beef broth (otherwise a cube of broth diluted in water) 
  * 2 tbsp. maizena dissolved in 2 tablespoons water (optional if your soup is smooth) 
  * 200 ml of fresh cream 
  * 2 branches of thyme 
  * nutmeg 
  * salt and black pepper. 
  * Freshly ground pepper 



**Realization steps**

  1. put the oil in a saucepan over medium heat. 
  2. add the mushrooms and sauté until their water is well reduced. 
  3. add the chopped leek, and let it come back. 
  4. then add the garlic and the beef broth. 
  5. cook covered for 15 minutes. 
  6. add thyme and grated nutmeg 
  7. Remove from heat and, mix this mixture in a blinder or with the diver (foot mixer), until everything becomes smooth, 
  8. Stir in fresh cream and cornstarch if your soup is creamy. 
  9. Serve in bowls and garnish with slices of fried or plain mushrooms. 
  10. sprinkle with some freshly ground pepper. 



![mushroom soup 3](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/velout%C3%A9-de-champignons-3.jpg)
