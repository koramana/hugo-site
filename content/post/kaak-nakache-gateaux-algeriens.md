---
title: kaak nakache - Algerian cakes
date: '2012-11-20'
categories:
- Café amour de cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/kaak-nakache-gateaux-algeriens_thumb1.jpg
---
##  kaak nakache - Algerian cakes 

Hello everybody, 

one of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) that I prepared for the aid el fitr [ algerian cakes 2011 ](<https://www.amourdecuisine.fr/article-aid-el-fitr-2011-les-gateaux-algeriens-82873081.html>) , A simple cake that can be categorized as [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) , this cake looks a lot like [ iced k3ikaates ](<https://www.amourdecuisine.fr/article-k3ik3ate-glace-kaak-glace-103638622.html>) but without the frosting, it is decoree at the **cake tongs ...** at the very beginning our mothers and grandmothers were decorating the [ kaak nakache ](<https://www.amourdecuisine.fr/article-kaak-nekkache-aux-amandes-82530128.html>) with "the tweezers" yes, yes .... 

you see the crowns in the middle, well they are with the tweezers (new, not use before, hihihihihi) 

in any case the result is super nice, but to make a groin of parts I took almost 2 hours, especially me who still finds it difficult to use the clip decoration. 

usually, this cake is stuffed with the **paste of dates** but because I did [ Makrout For Dates ](<https://www.amourdecuisine.fr/article-26001222.html>) I wanted to stuff my **crowns** this time with almonds, a super delicious recipe, from these [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) whom I share with you with great pleasure. 

before starting the recipe I want to explain the word **measured** which is always a question mark for my readers, in the [ Algerian cakes recipes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , our grandmothers who did not have scales at the time, used a kitchen utensil they called measure, this utensil could be: a glass of water, a bowl, a box of margarine, a cup, so it all depends on how much you want to get cake, and the cakes were perfect, so to avoid going out of the recipes of our grandmothers we continue to use the word measure in the ingredients of our recipes.   


**kaak nakache - Algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/kaak-nakache-gateaux-algeriens_thumb1.jpg)

portions:  40  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** for the dough: 

  * 3 measures of flour 
  * 1 measure full of smen 
  * 1 measure of water mix with orange blossom water 
  * 1 pinch of baking powder 
  * 1 teaspoon of vanilla 
  * ½ measure of sugar 
  * 1 pinch of salt 

for the almond stuffing: 
  * 3 measure almonds 
  * 1 measure icing sugar 
  * vanilla sugar 
  * the zest of a lemon 
  * 2 tbsp. soup of melted butter 
  * eggs (to pick up the stuffing) 



**Realization steps**

  1. method of preparation: 
  2. sift the flour, the salt; sugar and vanilla, 
  3. add the melted and cooled fat 
  4. mix everything by rubbing well between the hands to adhere the fat in the flour and then collect with the water of orange blossoms to obtain a smooth and firm paste, 
  5. cover and let stand for half an hour. 
  6. form balls of 30 grs each. 
  7. form a hollow in the ball and fill it with almond stuffing. 
  8. enclose the ball and form a pudding, its length will be equal to your 8 fingers of both hands together 
  9. join both ends to form a circle, if necessary with a little water so that the circle does not open. 
  10. decorate according to your taste with the cake tongs 
  11. place in a floured baking sheet 
  12. cook in a preheated oven at 180 degrees C for 15 minutes 



thank you very much for your visits and comments 

and for more recipes from [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-12344742.html>)

[ the index of Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)

the category of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>)

the category of [ fried Algerian cakes ](<https://www.amourdecuisine.fr/categorie-12344747.html>)

the category of [ Algerian cakes with glaze ](<https://www.amourdecuisine.fr/categorie-12344742.html>)

the category of [ Algerian cakes with honey ](<https://www.amourdecuisine.fr/categorie-12344741.html>)

the category of [ Algerian cakes without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>)

the category of [ Algerian pastries ](<https://www.amourdecuisine.fr/categorie-12135732.html>)

have a good day. 

tags: 

#  gateau kaak nekkache , kaak el nekache , kaak nekkache algerienne, 
