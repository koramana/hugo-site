---
title: Sweet potato gratins
date: '2012-03-07'
categories:
- juice and cocktail drinks without alcohol
- Index
- ramadan recipe
- riz
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-patate-douce_thumb1.jpg
---
![sweet potato gratin](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-patate-douce_thumb1.jpg)

##  Sweet potato gratins 

Hello everybody, 

yum yum these sweet potato gratins, enjoying the sweet potato season, to make delicious sweet potato recipes. 

Lunetoiles benefits anyway and she shares with us these delicious sweet potato gratins, a recipe she saw in a TV show, and it looks very exquisite.   


**Sweet potato gratins**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gratin-de-patate-douces-1_thumb1.jpg)

portions:  8 

**Ingredients**

  * 8 sweet potatoes 
  * 2 tbsp. whole almonds 
  * 1 C. tablespoon of organic olive oil 
  * 1 C. honey 
  * 2 cloves of garlic 
  * 1 bunch of parsley 
  * 400 ml of liquid cream 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. Place the sweet potatoes on the baking tray, bake for approx. 45 min, they must be tender, a knife must be sinking into the flesh. 
  3. Reduce the cream in a saucepan with ¼ garlic clove, salt and pepper over medium heat. 
  4. Chop the garlic and transfer to a bowl. 
  5. Roughly chop the almonds and parsley and mix with the garlic, then add the olive oil and honey. Mix the parsley well. 
  6. Peel the sweet potatoes. 
  7. Put in small casseroles, ½ sweet potato mashed in puree, 1 tsp. of persillade, and 1 tsp. of cream. 
  8. Put a layer of crushed sweet potato and finish with a spoon. of cream. 
  9. Put the mini casseroles in the oven to brown. 


