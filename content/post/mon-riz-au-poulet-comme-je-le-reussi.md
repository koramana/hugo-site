---
title: my chicken rice ... as I succeed
date: '2007-12-23'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/12/203148221.jpg
---
![bonj16](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/203143411.gif)

Yes Yes…. as I succeed, it is rare that I managed a dish of rice, but this one, I can tell you it is my pride ... 

so I do not take long to give you my recipe 

ingredients: 

  * a nice onion 
  * 1 head of garlic (I do not know if I'm in the right lexicon, but when I say a head I mean, the thing that is removed from a clove of garlic) 
  * 2 bay leaves 
  * of rice beans 
  * chicken legs 
  * 400 to 500 grs of beautiful red tomatoes (I use a canned tomato) 
  * 1 case of canned tomato 
  * oil (I cook with olive oil) 
  * salt, black pepper, paprika, tandori massala (it is a mixture of Indian condiments red and full of beautiful smells) 



then we go to the pots: 

I personally do this dish, in a large stove ... I like its cooking 

So I put the onion, and the garlic to the blender, and pour it into the pan with the olive oil, fry the mixture a little, add after the chicken leg, the bay leaves paprika, the salt, black pepper, and the condiments I have on hand. 

I simmer a little, and I add the tomatoes in a box that is already cut into. 

after I add the case of preserved tomato, because the rice likes the tomato, I cook a little, and and I cover well the surface of the chicken with water, and I let everything cook on a low fire. 

![S7301705](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/203148221.jpg)

after cooking the chicken, I remove it, and remove about 2 ladles of this sauce that I put aside. 

I now put the rice, I use rice rice sometimes, but you can add normal rice, so the trick for exact rice cooking without it sticks, and put 1 measure of rice with 2 water measurements. 

for my part, I put 2 bowls of rice with 4 bowls of water, I add it on the sauce already prepare, so I do not cook the rice alone, but on my sauce, I add the raw rice and then I add the necessary water, and I cook on a low heat, 

![S7301706](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/203148801.jpg)

if you find that the rice is not cooked well, you can add a little water, at the end of the cooking you can add the rest of the sauce that you have already removed. 

or, garnish your rice dish with this sauce just before serving. 

![S7301709](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/203164641.jpg)

and good appetite, I swear it's a treat. 

![th_groetjes27](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/203147791.gif)
