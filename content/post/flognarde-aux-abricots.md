---
title: flognarde with apricots
date: '2014-07-01'
categories:
- dessert, crumbles and bars
- panna cotta, flan, and yoghurt
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/flognarde-aux-abricots-2.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/flognarde-aux-abricots-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/flognarde-aux-abricots-2.jpg>)

##  flognarde with apricots 

Hello everybody, 

A delicious dessert with apricots very fresh, this flognarde with apricots of Lunetoiles ... Small cousin of Clafoutis, the flognarde is a dessert native of Limousin ... 

In any case, this unctuous and sweet texture on the palate, gives only a great desire to make this dessert, and I see it super well presented during our Ramadan evenings, ready to taste together and without moderation ... 

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/flognarde-aux-abricots-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/flognarde-aux-abricots-1.jpg>)

If you like my blog, do not forget to subscribe to my newsletter, and if you have made one of my recipes, do not forget to send me the picture on this link: [ my recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home")

**flognarde with apricots**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/flognarde-aux-abricots.jpg)

portions:  6 to 8  Prep time:  10 mins  cooking:  45 mins  total:  55 mins 

**Ingredients**

  * 1 kg of apricots 
  * 1 liter of water 
  * 150 g of sugar 
  * 2 vanilla pods 

For the dough: 
  * 60 cl of milk (or 30 cl of milk + 30 cl of cream) 
  * 100g of flour 
  * 120 g of sugar 
  * 5 eggs 
  * 2 bags of vanilla sugar 
  * cristalized sugar 



**Realization steps**

  1. In a saucepan, put the water, sugar and vanilla beans whole. 
  2. Bring to a boil. 
  3. Wash the apricots quickly under cold water, dry them, open them in half and remove the stones. 
  4. Place the apricots in the boiling syrup and cook over medium heat for about 15 minutes or until the apricots begin to confuse and stew. 
  5. Let cool. 
  6. Preheat the oven on th. 7/210 ° C. 
  7. Butter a rectangular dish or a 26cmx26cm square pan and sprinkle with crystallized sugar. 
  8. Mix all the ingredients of the dough with a whisk or a robot, pour it into the mold. 
  9. Divide the mumps with apricots, sprinkle with a little crystallized sugar and cook for about 45 minutes. 



Note The trick:   
For an even softer texture of the dough, use 30cl of milk and 30cl of liquid cream. [ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/flognarde-aux-abricots-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/flognarde-aux-abricots-3.jpg>)
