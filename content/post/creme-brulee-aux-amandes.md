---
title: Crème brûlée with almonds
date: '2015-01-24'
categories:
- dessert, crumbles and bars
- panna cotta, flan, and yoghurt
- ramadan recipe
tags:
- la France
- Pastry
- desserts
- Algerian cakes
- eggs
- verrines
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/creme-brul%C3%A9e-aux-amandes-1.jpg
---
[ ![creme brulee with almonds 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/creme-brul%C3%A9e-aux-amandes-1.jpg) ](<https://www.amourdecuisine.fr/article-creme-brulee-aux-amandes.html/sony-dsc-266>)

##  Crème brûlée with almonds 

Hello everybody, 

Who of you do not like creme brulee? I'm sure it's going to be a tiny voice from afar that's going to say "me," well, "me," you do not know what you're missing, lol. 

After the [ creme brulee ](<https://www.amourdecuisine.fr/article-creme-brulee-recette-creme-brulee-115598588.html> "Crème brûlée, crème brûlée recipe") nature, the [ creme brulee with pistachios ](<https://www.amourdecuisine.fr/article-creme-brulee-a-la-pistache.html> "crème brûlée with pistachio كريم بريلي بالفستق") , the [ creme brulée with coconut milk and pomegranate ](<https://www.amourdecuisine.fr/article-creme-brulee-au-lait-de-coco-et-grenade.html> "BURNED CREAM WITH COCONUT AND GRANADA MILK") , and [ apple pie creme brulee ](<https://www.amourdecuisine.fr/article-tarte-a-la-creme-brulee-aux-pommes.html> "apple crème brûlée pie") , today comes the turn of the creme brulée with almonds ... A touch of more, a flavor of more, a doubly crunchy burned cream 

A recipe Lunetoiles that is in my emails for 1 year and a half, yes .... I apologize for the delay, but better late than never, is not it? 

And if you like almonds, it will be nice to come play with us: 

[ ![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient-300x163.jpg) ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-2.html> "Recipes around an ingredient # 2")

source: Pinterest 

**Crème brûlée with almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/creme-brulee-aux-amandes.jpg)

portions:  4  Prep time:  10 mins  cooking:  60 mins  total:  1 hour 10 mins 

**Ingredients**

  * 200 ml of almond soy milk (or almond milk) 
  * 30 g of flaked almonds 
  * 2 whole eggs 
  * 2 egg yolks 
  * 120 g caster sugar or (whole cane sugar) 



**Realization steps**

  1. Preheat the oven to 100 ° C (thermostat 3) 
  2. Toast the almonds in a nonstick skillet. 
  3. Crush them roughly and divide them into four flat ramekins. 
  4. Whip the whole eggs with the yolks and 100 g of sugar. 
  5. Add the almond soy milk and mix. 
  6. Divide the mixture over the almonds, about 2 cm thick. 
  7. Place the ramekins on a rack and bake for 1 hour. 
  8. Let cool and keep cool for at least 2 hours. 
  9. When serving, sprinkle sugar cream and caramelize for a few seconds with a torch. 



[ ![creme brulee with almonds 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/creme-brulee-aux-amandes-3.jpg) ](<https://www.amourdecuisine.fr/article-creme-brulee-aux-amandes.html/sony-dsc-267>)
