---
title: Pizza Calzone
date: '2017-02-19'
categories:
- boulange
- pizzas / quiches / tartes salees et sandwichs
tags:
- Easy cooking
- accompaniment
- Ramadan recipe
- inputs
- Kitchenaid
- Algeria
- Cocktail dinner

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/pizza-calzone-1.jpg
---
![pizza calzone 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/pizza-calzone-1.jpg)

##  Pizza Calzone 

Hello everybody, 

Who asked for a pizza calzone? It's the pizza that makes the buzz at home, my children love it crazy. They love it more than the [ ordinary pizza ](<https://www.amourdecuisine.fr/article-pizza-au-thon-et-champignon.html>) not covered, for them it is easier to eat !? 

I am in their request, and I often make this pizza calzone, already you are lucky that I could take these few photos for you, and especially especially to have made a video, with my children who were spinning around and waiting for the release of the pizza oven calzone, making a detailed video was not an easy task for me. 

I used the [ Pizza dough ](<https://www.amourdecuisine.fr/article-pate-a-pizza-maison-recette-facile.html>) which I like the most, besides it is the same dough with which I prepared [ tuna and chakchouka rolls ](<https://www.amourdecuisine.fr/article-petits-pains-au-thon-et-chakchouka.html>) , just to tell you how well I'm adopting this pasta recipe, and how it is the basis in many of my starters, and accompanying recipes. 

**Pizza Calzone**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/pizza-calzone-2.jpg)

portions:  6  Prep time:  40 mins  cooking:  25 mins  total:  1 hour 5 mins 

**Ingredients** For 2 large calzone pizzas: 

  * 3 cups flour (cup = 220 ml) 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 tablespoon baker's yeast 
  * 1 teaspoon of baking powder 
  * 4 to 5 tablespoons of oil 

for the stuffing: 
  * tomato sauce 
  * cachir 
  * black olives in slices 
  * Mozzarella 

decoration: 
  * [ tomato sauce ](<https://www.amourdecuisine.fr/article-sauce-tomate-fait-maison-facile.html>)
  * oregano 
  * thyme 



**Realization steps** prepare the dough: 

  1. Mix all these ingredients and add enough lukewarm water until soft, soft and easy to work. 
  2. Knead a few minutes. Form the dough into a ball and let it rise under cover, protected from drafts until it has doubled in volume. 
  3. degas the dough and divide the two. 
  4. let rise a little, then spread each ball finely to almost 5mm 
  5. garnish half with tomato sauce, slices of cahir, slices of black olive and with a nice amount of mozzarella. 
  6. cover the stuffing with the second half of the unpacked dough. 
  7. weld the edges well, 
  8. garnish the calzones with a thin layer of tomato sauce, oregano and thyme. 
  9. cook in a preheated oven at 180 ° C for 25 minutes, or depending on the capacity of your oven. 
  10. enjoy with a fresh salad. 



![Pizza Calzone](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/pizza-calzone.jpg)
