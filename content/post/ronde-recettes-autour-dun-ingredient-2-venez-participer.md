---
title: 'round "recipes around an ingredient" # 2, come participate'
date: '2012-10-01'
categories:
- dessert, crumbles et barres
- gateaux, et cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/autour-d-un-ingredient-copie-11.jpg
---
![round-of-a-ingredient-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/autour-d-un-ingredient-copie-11.jpg)

Hello everybody, 

I go back to the round "recipes around an ingredient" because after missing the round: [ recipes around the apple ](<https://www.amourdecuisine.fr/article-demi-sphere-pommes-ricotta-sables-110700238.html>) you were many to have another round. 

So here it is, the choice is yours, if you are interested to participate, leave on this article a comment, choosing the ingredient. And tomorrow morning, I will send you to all the girls who leave a comment, an email, with the ingredient of the week. 

You then prepare a recipe with this ingredient, but you do not publish it until next Sunday. And on my email, you pass me the name of the recipe and the link of your blog. I will then draw up a list of all the participating recipes in the round, and I will send it to you on Saturday. 

On Sunday you will publish the recipe you have made, and towards the end you will list that I have you send recipes around that same ingredient. 

So I'm waiting for your comments today with the ingredient of your choice, and questions if you have not understood the principles of this round. and you can re-publish this article at home, to share information with other friends of the blogosphere, knowing that this round is especially for the women. 

merci 
