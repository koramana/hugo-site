---
title: Salted butter caramel mousse
date: '2017-12-28'
categories:
- bavarois, mousses, charlottes, agar agar recipe
tags:
- Easy cooking
- desserts
- Summer kitchen
- Summer
- Algeria
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/mousse-au-caramel-au-beurre-salee-005_thumb1.jpg
---
![caramel mousse with salted butter 005](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/mousse-au-caramel-au-beurre-salee-005_thumb1.jpg)

##  Salted butter caramel mousse 

Hello everybody, 

here is a delicious dessert that I prepared for the children yesterday. My daughter liked it so much that I had my share of it, fortunately I have some spoon from my husband's verrine. 

This caramel mousse with salted butter is a simplicity, that you go directly to the kitchen to make it.   


**Salted butter caramel mousse**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/mousse-au-caramel-au-beurre-sale-005_thumb1.jpg)

portions:  6 

**Ingredients**

  * 100 g caster sugar 
  * 75 g of half-salted butter 
  * 500 ml of liquid cream 
  * 3 egg yolks 
  * 1 gelatin sheet of 8 grs 



**Realization steps**

  1. Soften the gelatin in a bowl of cold water. 
  2. Meanwhile, boil 100 ml liquid cream. 
  3. In a saucepan, heat the sugar until you get a caramel. 
  4. Remove from heat and add the butter cut in small pieces while whisking, then boiling liquid cream. 
  5. stir everything together. (In case the caramel hardens, place the pan over very low heat, until the caramel melts) 
  6. Then add the egg yolks. 
  7. Mix and then add the gelatin previously dewatered. 
  8. Stir the whole so that the gelatin melts in the caramel. 
  9. Put the remaining liquid cream in whipped cream then pour the warm caramel (if it has cooled, heat it slightly) and mix it all together gently. 
  10. Spread the mousse in cups 
  11. book at least 1 hour. 


