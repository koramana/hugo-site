---
title: black and white cake - bavarois black and white
date: '2011-04-09'
categories:
- crepes, donuts, donuts, sweet waffles

---
& Nbsp; & Nbsp; hello everyone lunetoiles esy back with these beautiful cakes, and today she shares with us, this sublime Bavarian, whose base is a fluffy chocolate cake, topped with a mousse with vanilla. discover more details with the recipe. & Nbsp; & Nbsp; Preparation: 1 hour Rest: 4 hours Cooking: 30 minutes For 8 people & nbsp; For the chocolate mellow: * 200 gr of dark chocolate * 150 gr of butter * 200 gr of sugar * 1 tsp. tablespoon icing sugar * 2 tbsp. soup & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.45  (  1  ratings)  0 

**Hello everybody**

Lunetoiles are back with these beautiful cakes, and today she shares with us, this sublime Bavarian, whose base is a fluffy chocolate cake, garnished with a vanilla mousse. 

discover more details with the recipe. 

**Preparation: 1 hour  
Rest: 4 hours   
Cooking time: 30 minutes **

For 8 people 

**For the chocolate mellow:  
* 200 gr of dark chocolate   
* 150 gr of butter   
* 200 gr of sugar   
* 1 tbsp. tablespoon icing sugar   
* 2 tbsp. flour   
* 4 eggs **

For the vanilla mousse:   
* 250 ml of milk   
* 60 gr of sugar   
* 3 egg yolks   
* 2 vanilla pods   
* 150 gr of 20% cottage cheese   
* 4 sheets of gelatin   
* 200 ml whipping cream (whole) 

For the decor:   
* 150 gr of dark chocolate   
* 2 tbsp. tablespoon icing sugar   
* Pearl pearls white sugar (I did not put) 

**Prepare the mellow: melt the dark chocolate with the butter in a bain-marie (or microwave oven three times 30 seconds). Add sugar, icing sugar, eggs and flour.  
Pour this mixture into a 24 cm diameter buttered (or silicone) mold. **

Put in the oven at 180 ° C (th.6) for 30 minutes. At the end of the oven, wait 5 minutes then turn out on a rack. Let cool. 

Prepare the vanilla mousse: boil the milk with vanilla beans split in half and scrape the seeds. Beat the egg yolks with the sugar until blanching. Remove the vanilla pods and pour the warm milk over the egg yolks, off the heat. 

Put back on low heat and allow to thicken without boiling, stirring constantly. The cream should coat the wooden spoon. Add the softened gelatin in cold water and allow to cool. 

Rinse the vanilla pods and keep them for the decoration. Add the cottage cheese and then gently add the whipped cream. 

Assembly: Place the chocolate cake on the serving platter with a dessert ring (24 cm in diameter) dressed with rhodoïd strips. Pour vanilla mousse and refrigerate for 4 hours. 

Remove the circle and rhodoid strips. Sprinkle with icing sugar. 

Prepare the decor: Melt the chocolate in a bain-marie to reach a temperature between 50 and 55 ° C, then lower the temperature to 29 ° C in a bath of water and ice cubes. Finally, raise the temperature to 33 ° C in a bain-marie.   
Spread the chocolate on a strip of rhodoïd, previously cut to the right size, so that it marries perfectly around the cake. (I cut the rhodoid band, to obtain triangles)   
Wait for the chocolate to begin to freeze and place the rhodoïd strip gently on the edge of the cake. 

Let it cure completely before delicately detaching the rhodoid strip.   
To accelerate the hardening of chocolate, you can place a few minutes, the cake in the refrigerator. 

Decorate with pearly sugar pearls and vanilla pods, and place in the fridge until ready to serve. 

**thank you everyone, for your comments and comments**

**bonne journee**
