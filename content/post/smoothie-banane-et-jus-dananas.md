---
title: Banana smoothie and pineapple juice
date: '2015-11-20'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-et-jus-dananas.jpg
---
[ ![banana smoothie and pineapple juice](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-et-jus-dananas.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-et-jus-dananas.jpg>)

##  Banana smoothie and pineapple juice 

Hello everybody, 

Like every morning I enjoy preparing a fresh smoothie, good, delicious and especially rich in nutrients. This morning I started making my own homemade pineapple juice, a treat that pure juice, no added sugar, no coloring, no additives or preservatives, at least like that, I know what I put in my cup of smoothies. 

You can see a little video of pineapple juice extraction, if you do not have a juicer, you can always pineapple in your smoothie. 

{{< youtube Jz3MyMxuP4w >}} 

In all this smoothie is too good, it will boost your energy, it is good for your immune system, good for digestion, and most importantly, great for your skin. According to my book which I took this recipe, this smoothie is rich in Beta-carotene, folic acid, vitamins B1, B3, B6 and Vitamin C, It is rich in calcium, magnesium, potassium, zinc, and proteins .... There is enough to have the desire to take all the amount obtained, hihihihi 

[ ![banana smoothie and pineapple juice-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-et-jus-dananas-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-et-jus-dananas-001.jpg>)
