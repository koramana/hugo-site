---
title: pasta with smoked salmon easy and fast
date: '2018-04-06'
categories:
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pates-au-saumon-fum-029.CR2_thumb.jpg
---
[ ![pasta with smoked salmon 029.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pates-au-saumon-fum-029.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pates-au-saumon-fum-029.CR2_2.jpg>)

##  pasta with smoked salmon easy and fast 

Hello everybody, 

a recipe very simple to make, but super flavored pasta with smoked salmon .... 

and all kinds of pasta are welcome to make this tasty dish, here for my part I used pasta shells a little wide, my children usually prefer this dish with farfalle ... 

In any case it is the ideal recipe if you are in a hurry, it is already time to eat, and you want to concoct a delicious dish.   


**pasta with smoked salmon easy and fast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/oeuf-mimosa-et-pate-au-saumon-fume-038.CR2_thumb.jpg)

**Ingredients**

  * 500 grams of pre-cooked pasta in salt water 
  * 250 ml fresh cream. 
  * fresh onion 
  * a few slices of smoked salmon (depending on the number of people) 
  * a little black pepper 
  * 1 tablespoon of oil 



**Realization steps**

  1. in a skillet, add the oil and add the finely chopped fresh onion. 
  2. add a few pieces of smoked salmon 
  3. simmer a little, the salmon will take a lighter color, then reduce the fire. 
  4. add the cream. 
  5. Salt (if necessary) and pepper. let reduce a little, according to your taste, and how you like that the sauce is on your pasta. 
  6. At the end of cooking, add the remaining pieces of smoked salmon (finely chopped) 
  7. Put on the pasta and serve. 



[ ![pasta with smoked salmon 045.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/pate-au-saumon-fum-045.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/pate-au-saumon-fum-045.CR2_2.jpg>)
