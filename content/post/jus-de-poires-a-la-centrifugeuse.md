---
title: Pear juice in the juicer
date: '2015-11-23'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Orange
- cocktails
- Citrus
- lemons
- Fruits
- To taste
- Drinks

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/jus-de-poires-fait-maison.jpg
---
[ ![homemade pear juice](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/jus-de-poires-fait-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/jus-de-poires-fait-maison.jpg>)

##  Pear juice in the juicer 

Hello everybody, 

A juice without cooking, no added sugar, no water .... nothing but the fruit extract itself. For this recipe I use a juicer, although it takes space in my tiny kitchen, and although I have to disassemble every room to clean it, but I confess that the juice obtained is worth all . 

I enjoy it every morning, either by taking this pure juice as I extracted, or adding it as an ingredient of my smoothies, one thing is sure, I know that in my juice, I have nothing to add, additives, dyes, or anything else you do not even know. 

My children, in any case, love to make their own juice. They are proud to take their little bottle of homemade juice in their lunch box, and tell their classmates that they did it themselves. As is the case for this juice of pears and orange, we enjoy the season of oranges, and what is great is that the oranges are always delicious with any other fruit. 

**Pear juice in the juicer**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/jus-de-poires-1.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 3 pears 
  * 2 oranges 
  * ½ lemon 



**Realization steps**

  1. remove the pear seeds. 
  2. peel the oranges and the half lemon 
  3. place the ingredients in the juicer, and turn it on. 
  4. You can consume the juice directly if your fruits were very fresh, if not put it in the fridge, I preferred to consume it right away, because I like this mousse which settles at the top of this juice, what it is refreshing. 



[ ![pear juice in the juicer](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/jus-de-poires.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/jus-de-poires.jpg>)
