---
title: Griwech Algerian cake in video
date: '2015-07-06'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries
tags:
- Algerian cakes
- Cakes
- Algeria
- Aid cake
- baklava
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/aid-el-fitr-2013-griwech.CR2_.jpg
---
[ ![help el fitr 2013 griwech.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/aid-el-fitr-2013-griwech.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/aid-el-fitr-2013-griwech.CR2_.jpg>) ![Algerian cake griwech 057](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/gateau-algerien-griwech-057_thumb1.jpg)

##  Griwech Algerian cake in video 

Hello everybody, 

Today, I share with you the recipe of Griwech Algerian cake video, a [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) , a crispy delight, well wrapped in honey, which comes to us from the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) it looks like the [ Moroccan Chebakia ](<https://www.amourdecuisine.fr/categorie-12359409.html>) but it's still a typical Algerian cake that our mothers inherited from our great grandmothers. 

At home we like a lot of griweches, the name is maybe weird, but Griweche means crisp, and contrary to what appears in the photo, this Algerian cake is not the family of donuts, no no. Maybe the shape is not too far from those of the carnival donuts, but the fact remains that the griweches are a crispy Algerian cake and fondant wish. 

And I'm not going to share with you the griweche recipe only, but also the preparation video of these delicious Algerian cakes. 

[ ![griwech Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/griwech-gateau-Algerien_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/griwech-gateau-Algerien_2.jpg>)

**Griwech Algerian cake in video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/griwech-gateaux-de-laid-2015-.CR2_.jpg)

portions:  30  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * we go to the ingredients: 
  * 500 gr of flour 
  * 150 gr of butter or margarine (butter for me) 
  * 1 egg 
  * ½ case of sugar (1 tablespoon) 
  * ½ sachet of baking powder (1/2 cac) 
  * ½ case of vinegar 
  * 1 case of orange blossom water 
  * 1 small pinch of salt 
  * honey for garnish 
  * sesame seeds for garnish 



**Realization steps**

  1. Melt the butter without boiling, sift the flour in a bowl, sprinkle with sugar, salt and yeast and mix everything together. 
  2. make a fountain, pour the butter into the center, mix well with the flour, if you have lumps, that is to say large balls of butter coated with flour, pass this mixture through the sieve, rub it well to do it pass, you will have a nice homogeneous mixture flour-butter. 
  3. add the vinegar, the orange blossom water, and the egg a little bit beaten. incorporate all this mixture well into your flour, then wet with water, to have a soft, homogeneous paste, do not work the dough too much so that it does not become elastic. 
  4. divide this dough into a ball and leave to rest under a film of food, or a clean cloth. 
  5. then take balls one at a time and roll them about 2 to 3 mm thick 
  6. cut small rectangles on the wheel, and cut five strips in these rectangles, making four cuts, or if you have a special mold, it will be even better. 
  7. take a rectangle in your left hand, pass your finger with your right hand between the odd straps (1 strap in front, 1 strap behind, etc.) 
  8. Grab the left corner at the top:   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/22225431_3.jpg)
  9. and pull between the thongs, to form a kind of fish stop   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/22225438_3.jpg)
  10. repeat the same operation until all the dough has been used up. 
  11. Once all the cakes are made, dip them one by one into a very hot frying, but on a low heat., brown both sides, and drain well.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/22225446_3.jpg)
  12. dip them in melted honey, then drain, sprinkle on both sides of each cake with slightly grilled sesame seeds. 
  13. the cake was very delicious and melting in the mouth 



[ ![22225462](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/22225462_31.jpg) ](<https://www.amourdecuisine.fr/article-25345475.html>)

This cake can be kept 2 or 3 weeks, After a week if your cakes start to dry out, sprinkle them with a little honey at the time of consumption 

Enjoy your meal. 

Kisses 

[ ![Algerian cake griwech 056](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau-algerien-griwech-056_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/gateau-algerien-griwech-056.jpg>)
