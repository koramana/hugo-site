---
title: cauliflower in white sauce, Algerian cuisine
date: '2017-02-04'
categories:
- cuisine algerienne
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/choufleur-en-sauce-blanche_thumb.jpg
---
[ ![cauliflower in white sauce, Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/choufleur-en-sauce-blanche_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/choufleur-en-sauce-blanche.jpg>)

##  cauliflower in white sauce, Algerian cuisine 

Hello everybody 

Cauliflower in white sauce, here is a recipe of the Algerian cuisine that I was happy to introduce to my children. 

Even my husband who does not like cauliflower, well I was surprised that he had finished his cauliflower dish in white sauce (he who does not like white sauces very much, hihihihi) 

so to you the recipe:   


**cauliflower in white sauce, Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/choufleur-en-sauce-blanche_thumb_1.jpg)

Recipe type:  Algerian cuisine, dish  portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * pieces of chickens 
  * of the [ cauliflower donuts ](<https://www.amourdecuisine.fr/article-beignets-de-chou-fleur.html> "cauliflower donuts")
  * 1 kilo of potatoes 
  * 1 handful of chickpeas 
  * 1 onion 
  * parsley 
  * table oil 
  * salt, black pepper, a pinch of cinnamon, coriander powder. 



**Realization steps**

  1. clean the chicken, or the pieces of chicken, and put them in a pot, 
  2. add the oil, finely chopped onion, and crushed garlic, 
  3. cook over low heat, add a little parsley, and spices, 
  4. add the chickpeas, cover the whole with about 1 liter of water, and cook. 
  5. now, peel and clean the potato, cut it in length, a little wider than the normal fries, salt it a little bit. and go to the frying. 
  6. When the chicken is cooked, take it out of the sauce, take a baking tin, place in the fritters, and the cauliflower fritters. 
  7. sprinkle with half the sauce, cover the pieces of chicken, and place in a warm oven, to give a nice color. 
  8. before serving, sprinkle each dish with a little sauce, and garnish with chickpeas and parsley. 



[ ![cauliflower in white sauce](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/choufleur-en-sauce-blanche_thumb_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/choufleur-en-sauce-blanche_3.jpg>)
