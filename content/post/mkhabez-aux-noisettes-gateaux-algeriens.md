---
title: Mekhabez with hazelnuts algerian cakes
date: '2012-06-14'
categories:
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/mkhabez-aux-noisettes-12_thumb1.jpg
---
![munch with hazelnuts 12](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/mkhabez-aux-noisettes-12_thumb1.jpg)

##  Mekhabez with hazelnuts algerian cakes 

####  .... مخبز بالنوازات ... .. مخبز بالبندق 

Balut everyone, 

Always with [ Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) , the oriental delicacies that are prepared for the occasion of the approaching aid, there is not much left, I know that you are here and to look for what you can do as [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , 

I make it easy for you, trying every time to put a recipe online, and according to your needs [ refined Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , to [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) , and [ cakes without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) , and I will try inchallah to enrich my categories, to satisfy your requests, do not hesitate, if you look for a recipe, and you can not find it, contact me, to be able to orient you 

Here is a beautiful and easy recipe of Lunetoiles Mkhabez with hazelnut cakes from Algeria, a recipe from the book of Mrs. Benberim (Modern Cakes 2) here is this recipe for **dress with hazelnuts.**   


**Mekhabez with hazelnuts algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/mkhabez-aux-noisettes-11.jpg)

portions:  50 

**Ingredients**

  * 250 grams of powdered hazelnuts 
  * 150 grams of icing sugar 
  * ½ cc of hazelnut aroma 
  * 2 egg whites 
  * 1Blazing: 
  * 1 egg white 
  * 60 ml of liquid (half lemon juice, half water) 
  * About 300 grams of icing sugar 

Decor : 
  * Chocolate pastry 
  * Half hazelnut 



**Realization steps**

  1. In a terrine mix the hazelnuts, sugar and aroma. 
  2. Add the egg whites one by one (You may only need one white, depending on the size of the eggs) until you obtain a malleable paste. (If it sticks too much add icing sugar). 
  3. On a floured work surface, spread the dough to a thickness of 1 cm and using a punch, cut rounds of 4 cm in diameter. Arrange them on a baking sheet covered with baking paper and bake in preheated oven at 150 ° C for 10 to 15 minutes. 
  4. Let cool completely. 
  5. Prepare the icing by mixing the egg white, lemon juice and water. Add the icing sugar little by little until you reach the desired consistency. Simply dip a cake into the frosting and see if the frosting is holding and not flowing too much. 
  6. Leave to dry overnight. 
  7. Melt the chocolate in a bain-marie or microwave, fill a pastry bag or like me a simple bag of freezing, cut the end and make the drawings on the top of m'habhabez. 
  8. Decorate with a half hazelnut and allow to dry. 



Bonne journée et a la prochaine recette. 
