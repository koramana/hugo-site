---
title: Still ... .. chocolate rolls.
date: '2008-02-29'
categories:
- Buns and pastries
- Mina el forn
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/02/226857131.jpg
---
& Nbsp; Definitely, we will never get tired of seeing and making chocolate rolls, this time it is passing through mouni that I found myself involved in looking for my ingredients, to embark on a new adventure. Although the preparation of these rolls takes a lot of time, but I find it a delicious job, especially with a dough very beautiful to handle. for the ingredients: 350gr of flour 80gr of milk 50gr of water 1 / 2cc of salt 70gr of sugar 2cc of baker's yeast 130gr of cold butter for the puff pastry so I have 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/226857131.jpg)

Although the preparation of these rolls takes a lot of time, but I find it a delicious job, especially with a dough very beautiful to handle. 

for the ingredients: 

  * 350gr of flour 
  * 80g of milk 
  * 50gr of water 
  * 1 / 2cc of salt 
  * 70gr of sugar 
  * 2cc of baker's yeast 
  * 130gr of cold butter for puff pastry 



so I put my ingredients in order in the machine, and after the end of the program, I put my dough in a bowl that I covered with a film of food paper, and in the fridge for 30 minutes . 

this time, and as mouni said, I took all my time to work the dough. 

the other recipes always talked about 15 minutes of rest between the foliage, but finally, the longer it is left in the fridge, and the more the butter becomes hard, and in the foliage it spreads well without overflowing. 

so at the exit of the fridge, I spread my 130 gr of butter on the dough that I had spread well. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/226856821.jpg)

I fold the edges to lock all the butter 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/226856891.jpg)

I spread out the dough, fold it over three, and cover it with a film of food 

and in the fridge 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/226856911.jpg)

so I did not let myself be haunted by 15 min in the fridge, but every time I was free I took out my dough, I spread it in a rectangle, and fold it over three, like a wallet. 

I did not stop as well, but I went on endless rounds until I noticed that the butter was completely incorporated into the dough. 

after that I fashioned my rolls, which I had stuffed with nutella, and let them rest for an hour and a half. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/226856951.jpg)

after a batch of at least 30 minutes 

I took out my little ones all gilded. 

and let cool. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/226857021.jpg)

I admit that the result was as delicious as it was great. 

and in addition the nutella was always melting 

a thousand times better than chocolate bars that harden after cooling. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/226857041.jpg)

then this picture, and that of what was left of the dough. 

I explain, after cutting my rolls, I have a small ball that I keep in the fridge 2 days 

then I said, I'll try to see how it's going to be the roll after 2 days in the fridge. 

so I spread it 

and as you can see, I sprinkled chocolate powder (chocolate for baby milk) 

so the idea comes from my friend Louiza who told me that she made buns she stuffed with sugar and cocoa, and the result was great. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/226857081.jpg)

so the chocolate powder will be soaked in the butter during cooking, and will give a good chocolate fondant. 

also one thing, we can prepare this puff pastry, put it in the fridge, make it out the day we want to shape our rolls, and the result will always be great. 

Judge by yourself. 

bon appétit 
