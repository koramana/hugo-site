---
title: my achievements at home 35
date: '2013-05-28'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tlitli1.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

a new post to share with you, the recipes of my readers, they have tried from the recipes of my blog ... and it makes me very happy. 

please continue to send me your test photos on my email: [ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

**just a clarification** : 

this email, [ vosessais@gmail.com ](<mailto:vosessais@gmail.com>) is not a contact email, if you have questions to ask me about a recipe, do it in comments, because this email, I only consult when I have time to write the article: my achievements at you, and sometimes I find urgent questions that are more than 1 month old .... 

sorry, I have the comments to answer your questions, and I have this email for the achievements, and I have another email to contact the author, but it's mostly for commercials, hihihihih 

I can not receive everything in the same email, it will be overflowing. 

so, and I'll say it again, if you have an urgent question about a recipe, make it a comment, it will allow me to respond faster, as it allows other visitors to enjoy the answer, and not to rest the question again and again. 

thank you. 

otherwise I share with you today the photos of your achievements: 

soups, breads, cakes, cookies, desserts, salads, appetizers, brioches ... 

I'm so glad you found my blog is rich in recipes, thanks again. 

so for photos of your essays send me on this email:  [ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

![Picture 1933](https://www.amourdecuisine.fr//import/http://img.over-blog.com/500x375/2/54/55/28/mes-image-2/mes-images-3/dossier-4/Mes-images-5/mes-image-6/mes-images-7/)

![pie-in-strawberry-mai.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/API/2013-03/07/)

[ strawberry pie with cream pastry ](<https://www.amourdecuisine.fr/article-tarte-aux-fraises-et-a-la-creme-patissiere-58354661.html>) at Nesrine 

![Cake aux fraises.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/500x500/4/47/77/45/recette-1/)

[ Strawberry cake ](<https://www.amourdecuisine.fr/article-gateau-aux-fraises-117317794.html>) in  [ Cécé ](<http://bienvenuedansmacuisineceline.over-blog.com/>)

![tlitli.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tlitli1.jpg)

[ Tlitli ](<https://www.amourdecuisine.fr/article-tlitli-constantinois-pour-l-annee-hijir-1434-112473751.html>) at Amira D 

what she thinks about it:  j’ai essayer ton tli tli CT une pure merveille merci enormément 🙂 

![tart-with-strawberry-copy-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-aux-fraises-copie-11.jpg)

[ strawberry shortcake ](<https://www.amourdecuisine.fr/article-tarte-sablee-aux-fraises-102110510.html>) at Yasmina A 

what she thinks about it: »  Salam alaikoum, 

First of all thanks for your recipes that really helps me.    
Here is the photo of my strawberry pie that made a big splash with my family !!!    
See you soon In'challah.    
Yasmina " 

[ ![Chahirakay Blog: Jasmine Palace., CHICKEN YASSA.](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/136699249631.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/136699249631.jpg>)

![tabaa.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/225x300/2/42/48/75/API/2013-03/07/)

[ halwat tabaa ](<https://www.amourdecuisine.fr/article-halwat-tabaa-bel-jeljlane-sables-aux-grains-de-sesames-62366278.html>) at Zahira 

![buns-to-the-meat-hachee.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/API/2013-03/07/)

[ buns with chopped meat ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee-96050112.html>) in  Scheherazade 

![pie-in-slitting linda.jpg](http://amour-de-cuisine.com/wp-content/uploads/2013/05/tarte-au-fraises-linda.jpg)

[ macaron strawberry pie ](<https://www.amourdecuisine.fr/article-tarte-fraises-macaron-117152632.html>) at Linda's place 

![tajine-mchimcha.JPG](http://img.over-blog.com/300x300/2/42/48/75/API/2013-03/07/tajine-mchimcha.JPG)

[ tajine of Nefle, or tajine mchimcha ](<https://www.amourdecuisine.fr/article-tajine-de-nefles-farcies-aux-amandes-117638636.html>) at Rocha 

![tajine-of-chicken-with-olive-copy-1.JPG](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x325/2/42/48/75/API/2013-03/07/)

[ chicken tajine with olives ](<https://www.amourdecuisine.fr/article-25345434.html>) at Assia K 

![](http://img.over-blog.com/300x247/2/42/48/75/API/2013-03/07/hachis-parmentier.JPG)

[ shepherd's pie ](<https://www.amourdecuisine.fr/article-hachis-parmentier-103831085.html>) at Assia K 

![chocolate cake without egg](http://amour-de-cuisine.com/wp-content/uploads/2013/05/gateau-sans-oeuf-au-chocolat.jpg)

[ chocolate cake without egg ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-sans-oeufs-recette-delicieuse-108236853.html>) in [ Marcia ](<http://www.marciatack.fr/>)

![bourek au thon.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/API/2013-03/07/)

[ tuna bricks ](<https://www.amourdecuisine.fr/article-bricks-au-thon-recette-facile-et-rapide-117943659.html>) at Myriam 

![20130421_184745](https://www.amourdecuisine.fr//import/http://p9.storage.canalblog.com/93/35/1079012/)

![Sandwich au thon.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/API/2013-03/07/)

[ Tuna sandwich ](<https://www.amourdecuisine.fr/article-ach-47652304.html>) at Aurelie P 

![bniouen.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/API/2013-03/07/)

[ Bniouen ](<https://www.amourdecuisine.fr/article-bniouen-gateau-sans-cuisson-algerien-64061949.html>) at Bibila 

![Petis breads with olives Oxana](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x224/2/42/48/75/API/2013-03/07/)

[ rolls with olives ](<https://www.amourdecuisine.fr/article-pains-aux-olives-cuisine-algerienne-108119096.html>) at Oxana 

![washers au chocolat.JPG](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/API/2013-03/07/)

[ Chocolate Short Cake ](<https://www.amourdecuisine.fr/article-sables-au-chocolat-gateaux-secs-2012-109072093.html>) at Samah A 

![oie_jpg.png](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/API/2013-03/07/)

[ potato gratin with spinach ](<https://www.amourdecuisine.fr/article-gratin-de-pommes-de-terre-a-la-creme-d-epinards-107627554.html>) in [ nasturtium ](<foodgrimoire.blogspot>)
