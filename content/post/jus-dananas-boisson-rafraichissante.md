---
title: Pineapple juice refreshing drink
date: '2016-08-21'
categories:
- recette de ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/jus-d-ananas-boisson-fraiche.CR2_1.jpg
---
![Pineapple juice](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/jus-d-ananas-boisson-fraiche.CR2_1.jpg)

##  Pineapple juice refreshing drink 

Hello everybody, 

here is a pineapple juice refreshing drink, very very refreshing even that I have always prepared but I never had the chance to take pictures, because when I make this pineapple juice, I do not even bother how it disappears, so delicious it is, and refreshing. 

for this pineapple juice, you do not need fresh pineapple, I often use pineapple in limp without the juice if I think it contains a lot of sugar, and the taste is always sublime. You can see that I often prepare pineapple juice with different tastes and combinations, and the result is always a delight. 

![Pineapple juice](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/Jus-frais-a-l-ananas-cocktail-halal.CR2_1.jpg)

**Pineapple juice refreshing drink**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/Jus-frais-a-l-ananas-cocktail-halal.CR2_.jpg)

**Ingredients**

  * 1 liter of water 
  * 70 g ginger wash and peel 
  * 300 gr of fresh pineapple pieces 
  * 100 ml of lemon juice 
  * sugar according to taste 



**Realization steps**

  1. place the ginger, pineapple and lemon juice in a food processor. add the water and mix well until a homogeneous liquid. 
  2. Filter the resulting mixture with a sieve to remove the juice. 
  3. Add the sugar to the juice and mix. 
  4. Keep in the refrigerator for at least 2 hours and serve the cold drink. 



![Pineapple juice](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/jus-d-ananas-fait-maison.CR2_1.jpg)

J’éspère que vous allez avoir la chance de réaliser ce jus d’ananas et me dire votre avis. 
