---
title: 'tajine of gnaouia: sauce gombos meat'
date: '2018-02-15'
categories:
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/riz-au-gombos-et-a-la-viande-d-agneau.CR2_thumb1.jpg
---
##  tajine of gnaouia: sauce gombos meat 

Hello everybody, 

a delicious recipe of okra sauce with pieces of lamb meat, a sauce well reduced to accompany a dish of rice, or just to taste with a piece of bread, a burst of flavors in the mouth, a recipe that never leaves the menu of the month has the maisos, since it was discovered at my friend Sudanese. 

I must say that I was surprised that my husband is asking for this recipe. 

"Bamiya bi erroz" its name in oriental cuisine, or okra in meat sauce, known also with the name of tajine of gnaouia (G'nawiya "to read it well) is a recipe very very easy to achieve, and too good. 

**tajine of gnaouia: sauce gombos meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/riz-au-gombos-et-a-la-viande-d-agneau.CR2_thumb1.jpg)

Recipe type:  Tagine dish  portions:  4  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * 500 g of lamb or mutton 
  * 500 g of okra 
  * 500 g of [ cooked basmati rice ](<https://www.amourdecuisine.fr/article-riz-a-la-corete-mouloukhiya.html> "Rice with corn, Mouloukhiya")
  * 1 onion 
  * 3 cloves of garlic 
  * 1 can of tomatoes in morceax (or 3 fresh tomatoes) 
  * salt pepper 
  * 3 tablespoons extra virgin olive oil, or table oil 
  * 1 small stick of cinnamon. 



**Realization steps**

  1. prepare the rice by following this recipe: [ cooking rice ](<https://www.amourdecuisine.fr/article-riz-a-la-corete-mouloukhiya.html> "Rice with corn, Mouloukhiya")
  2. Cut the meat into pieces and clean it. 
  3. Cut the onions into thin slices, or just cut them. 
  4. over low heat, place the pieces of meat, and the onion, add the oil. 
  5. let sweat over low heat, until the onions become very tender and the meat makes a lot of water 
  6. add salt, black pepper, and cinnamon. 
  7. add the grated garlic, and the tomato in pieces, let it cook again. 
  8. now add the gombos (I use the frozen okra) and ½ glass of water 
  9. let bein cook until the gombos become very tender (if you think you need to add water, play in) 
  10. towards the end of the cooking, let the sauce reduce ... 
  11. serve this sauce of lamb meat okra to accompany your rice. 


