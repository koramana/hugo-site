---
title: A selection of logs and biscuits rolled for the new year 2017/2018
date: '2017-12-30'
categories:
- birthday cake, party, celebrations
- sweet recipes
tags:
- Cake with stages
- Cakes
- Birthday cake
- desserts
- Rolled cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/12/selection-de-buches-1024x576.jpg
---
![selection of logs](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/selection-de-buches-1024x576.jpg)

##  A selection of logs and biscuits rolled for the new year 2017/2018 

Hello everybody, 

Here are my dear readers, a selection of logs and biscuits rolled for the new year 2018. 

There's something for everyone: chocolate, fruit, lemon, rolled biscuit, Bavarian cream, with a tip, a dacquoise, butter cream ... everyone he loves. 

here are the links: 

rolled biscuit / Christmas log with chocolate icing: 

> [ rolled biscuit / Christmas log with chocolate frosting ](<https://www.amourdecuisine.fr/article-biscuit-roule-buche-de-noel-au-glacage-au-chocolat.html>)

  
Cake rolled with chocolate mousse: 

> [ Cake rolled with chocolate mousse ](<https://www.amourdecuisine.fr/article-gateau-roule-a-mousse-chocolat.html>)

  
Chocolate rolled biscuit: 

> [ Chocolate rolled biscuit ](<https://www.amourdecuisine.fr/article-biscuit-roule-chocolat.html>)

  
biscuit rolled with lemon: 

> [ biscuit rolled with lemon ](<https://www.amourdecuisine.fr/article-biscuit-roule-au-citron.html>)

  
Biscuit rolled with lemon mousse: 

> [ Biscuit rolled with lemon mousse ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-mousse-de-citron.html>)

  
biscuit rolled with hazelnuts and cream at philadelphia: 

> [ biscuit rolled with hazelnuts and cream at philadelphia ](<https://www.amourdecuisine.fr/article-biscuit-roule-aux-noisettes-et-creme-au-pheladelphia.html>)

  
Dacquoise cake and chocolate mousse: 

> [ Dacquoise cake and chocolate mousse ](<https://www.amourdecuisine.fr/article-gateau-dacquoise-et-mousse-au-chocolat.html>)

  
raspberry buche chocolate: 

> [ raspberry buche chocolate ](<https://www.amourdecuisine.fr/article-buche-framboise-chocolat.html>)

  
Log praline with caramelized hazelnut chips: 

> [ Log praline with caramelized hazelnut chips ](<https://www.amourdecuisine.fr/article-buche-pralinee-aux-eclats-de-noisettes-caramelisees.html>)

  
rolled jam cookie: 

> [ rolled jam cookie ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture.html>)

  
Mascarpone / strawberry roll: 

> [ Mascarpone / strawberry roll ](<https://www.amourdecuisine.fr/article-roule-mascarpone-fraise.html>)

  
coffee rolls 

> [ coffee rolls ](<https://www.amourdecuisine.fr/article-roules-au-cafe.html>)

  
rolled marbled with lemon cream: 

> [ rolled marbled with lemon cream ](<https://www.amourdecuisine.fr/article-roule-marbre-a-la-creme-au-citron.html>)

{{< youtube HaZQaMeJjK8 >}} 

**A selection of logs and biscuits rolled for the new year's Eve 2018**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/selection-de-buches-1024x576.jpg)

**Ingredients** butter cream 

  * 100 gr of dark chocolate 
  * 150 gr of butter at room temperature 
  * 200 gr of icing sugar 
  * 2 tbsp. cocoa mixed with the icing sugar 

Biscuit Roll: 
  * 4 eggs 
  * 120gr of sugar 
  * 100gr of flour 
  * vanilla 
  * a half c a c of baking powder 



**Realization steps**

  1. we start by blanching the eggs with the sugar until the mixture is creamy, then add the sifted flour mix with yeast and vanilla (if you use vanilla sugar you put it with sugar and eggs) me I used the extract, 
  2. mix gently until the mixture is homogeneous. 
  3. garnish a baking dish with lightly greased baking paper. pour the mixture and put in the preheated oven at 180 degrees and cook for 10 to 15 minutes. 
  4. remove the biscuit from the oven and let cool. 
  5. unmold on aluminum foil then roll with aluminum foil at the same time, leave for a few minutes and then unroll the cake. now he and close to be garnished. 


