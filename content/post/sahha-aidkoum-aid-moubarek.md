---
title: sahha Aidkoum, Aid moubarek
date: '2012-08-19'
categories:
- jams and spreads
- recettes patissieres de base
- recettes sucrees

---
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/aid3-6c22381.gif)

**_assalamou alaikou wa rahamatu Allahi wa barakatouh_ **

**_C_ ** **_Is the end of the holy month of Ramadan, and for this occasion, I wish all the Muslim community a very good Aid, may Allah taalla accept our youth and our prayers._ **

**_bonne fête d’Aid a tout le monde…_ **
