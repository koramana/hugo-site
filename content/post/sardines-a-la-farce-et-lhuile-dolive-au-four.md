---
title: Sardines with stuffing and baked olive oil
date: '2015-06-09'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
tags:
- Summer
- Ramadan 2015
- Easy cooking
- Algeria
- Ramadan
- Fish
- chermoula

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/sardines-farcies-au-four.jpg
---
[ ![stuffed sardines in the oven](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/sardines-farcies-au-four.jpg) ](<https://www.amourdecuisine.fr/article-sardines-a-la-farce-et-l-huile-d-olive-au-four.html/sardines-farcies-au-four>)

##  Sardines with stuffing and baked olive oil 

Hello everybody, 

Sardines from the Mediterranean what I like, and pity I can not find here in England, and I admit that every time I go to Algeria for my holidays, it's the first thing I buy ... Because we miss it. 

Today on our kitchen group on facebook, one of my friends **Pipo Jewelry** to share with us his delicious dish of sardines with stuffing and olive oil in the oven, I have not hesitated to ask him the recipe, that I will try as soon as I find the good sardine, hihihihi 

So you get this little dish out of the oven, I hope you enjoy it. 

**Sardines with stuffing and baked olive oil**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/sardines-farcies-au-four.jpg)

Cooked:  Algerian  portions:  4-6  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * sardines (1 kg) 

the farce (Dersa) 
  * FOR FARCE DERESSA: you need 
  * 3 to 4 garlic cloves 
  * 1 onion 
  * 1 beautiful fresh tomato 
  * Paprika 
  * Cumin 
  * Curry 
  * Ginger powder 
  * Thyme 
  * HERBS OF PROVENCE 
  * SALT 
  * 2 tablespoons of olive oil 
  * chopped parsley and coriander. 



**Realization steps**

  1. Empty, wash and drain the sardines, remove the edge 
  2. place the half of the sardine, one in front of the other in a tray lined with parchment paper 
  3. salt a little 
  4. Prepare the stuffing: 
  5. Stack all the ingredients in a mortar (the mortar is important to have a delicious stuffing) no electric robot 
  6. when the stuffing is homogeneous, add finely chopped parsley and coriander 
  7. spread the stuffing with a spoon on each sardine, and cover with another sardine   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/sardines-a-lhuile-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42728>)
  8. At the end soak with olive oil and bake in a preheated oven at 180 degrees, for almost 20 minutes, or less depending on your oven. 



[ ![sardines with oil 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/sardines-a-lhuile-2.jpg) ](<https://www.amourdecuisine.fr/article-sardines-a-la-farce-et-l-huile-d-olive-au-four.html/sardines-a-lhuile-2>)
