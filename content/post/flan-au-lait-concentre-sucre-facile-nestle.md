---
title: easy sweetened condensed milk custard / Nestlé
date: '2017-04-15'
categories:
- panna cotta, flan, and yoghurt
tags:
- Ramadan
- Ramadan 2015
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-au-nestle-057_thumb.jpg
---
[ ![flan at nestle 057](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-au-nestle-057_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/flan-au-nestle-057.jpg>)

##  easy sweetened condensed milk custard / Nestlé 

Hello everybody, 

here is an express recipe, which is prepared in not even 10 minutes, if you have like me, caramel in bag, after remaining cooking in the oven, but this **Flan at Nestlé** , or **sweetened condensed milk custard** , is just a real delight, very unctuous, very good, and above all, it seems that there is not even eggs in it, so we do not detect the taste at all. 

very easy recipe, very fast recipe, and a very rich flan in taste. 

**concentrated milk flan / Nestlé**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-au-nestle-057_thumb.jpg)

Recipe type:  Dessert  portions:  6  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * 3 eggs 
  * 1 can of concentrated sugar milk (397gr) 
  * 1 measure and ⅓ of milk 
  * a bag of caramel if not prepare the caramel, as for the [ egg flan ](<https://www.amourdecuisine.fr/article-flan-aux-oeufs-2.html> "egg flan")



**Realization steps**

  1. in a salad bowl, break the eggs, add the Nestlé box, and with the empty box measure 1 box and 1 third of milk 
  2. using a whisk, or a "hand mixer" (mixing diver) well mix the mixture 
  3. cover the cake mold, or ramekins with the caramel   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/flan-express-au-lait-concentre-nestle_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/flan-express-au-lait-concentre-nestle_thumb.jpg>)
  4. pour the mixture slowly, 
  5. place in a water bath, in a preheated oven at 180 degrees C for 30 to 40 minutes, or until a blade inserted in it, comes out well dry. (so check according to your oven, it may be that your custard will be cooked in 25 minutes) 
  6. at the end of cooking remove from oven and let cool before putting in fridge. 
  7. turn out when serving, and enjoy. 



[ ![concentrated milk flan](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/flan-au-lait-concentre_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/flan-au-lait-concentre.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
