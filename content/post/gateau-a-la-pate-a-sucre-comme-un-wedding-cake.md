---
title: cake with sugar paste as a wedding cake
date: '2016-11-03'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/sc4-006_thumb1.jpg
---
![cake with sugar paste as a wedding cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/sc4-006_thumb1.jpg)

##  cake with sugar paste as a wedding cake 

Hello everybody, 

This is the first time I try a cake like this: a cake with sugar dough like a wedding cake, must say that the English are well known for their cakes that are always covered with sugar paste (recipe made in 2008). 

For the "Hen Night" of my neighbor (an exit between girlfriend for a person who is getting married soon). She had asked me to make him a simple little cake covered in white and green. Not a great expert of these cakes, I made a cake super airy style [ mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouscoutchou.html>) . that I covered with a beautiful layer of [ butter cream ](<https://www.amourdecuisine.fr/article-creme-au-beurre.html>) and topped with commercial sugar dough. 

so I do not take long to give you the recipe:   


**cake with sugar paste as a wedding cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/sc4-006_thumb.jpg)

**Ingredients** for the mouskoutchou cake: 

  * 5 eggs 
  * 10 tablespoons of table oil or butter 
  * 10 tablespoons of milk 
  * 150 gr of sugar 
  * 200 gr of flour 
  * 2 packages of baking powder 
  * 2 packages of vanilla sugar 
  * 1 pinch of salt 

for the butter cream: 
  * 500 gr icing sugar, 
  * 230 gr soft butter (ointment), 
  * 2 tablespoons of pasteurized milk 
  * 1 teaspoon vanilla / almond aroma / ... 



**Realization steps**

  1. Preparation of the cake: 
  2. Separate the egg whites and rise in snow with 75 gr of sugar and salt. 
  3. Book. 
  4. Beat the egg yolks with the other half of the sugar until you have a foamy mixture that has doubled in volume. 
  5. Gradually add the oil then the milk. 
  6. Gently stir in a spoonful of egg white, mix and add a spoon of flour mixed with the baking powder. Continue alternating a spoonful of egg white and a spoonful of flour until exhaustion. 
  7. Butter and flour a mold of your choice. 
  8. Preheat your oven th. 170 ° C. Cook until the surface is golden brown. 
  9. With the tip of a knife, check the cooking. The blade is dry, remove the cake from the oven. Let cool and unmold. 
  10. after cooling the cake cut it in half and garnish with the jam (orange marmalade for me) and cover with the other part of the cake. 
  11. cover the whole cake with the butter cream: 
  12. Preparation of the butter cream: 
  13. In a large salad bowl put the butter, beat with an electric whisk until it is homogeneous. 
  14. Add the aroma and mix briefly. 
  15. Add the icing sugar little by little by scraping the sides and bottom of the bowl often with a spatula. 
  16. When all the sugar has been added the mixture will be rather dry. 
  17. Add 2 tablespoons milk and whip again on medium speed until easy buttercream becomes light and airy. 
  18. It may take some time depending on your drummer but especially use the average speed for this stage. 



so for my part I use half of the ingredients to cover generously His cake, with a spatula. 

for the sugar dough, as I said, mine is bought, but to prepare the recipe 

**50 grams of marshmallows / marshmallows or white marshmallows  
\- 2 to 3 tablespoons of water.   
\- 1, 5 kg of very thin icing sugar or sieved.   
\- Gel food color preference.   
\- 2 salad bowls, spatula, rolling pin, microwave.   
**   
\- Put 500 gr of icing sugar in a large bowl with 2 tablespoons of water.   
\- Put marshmallows in another microwavable bowl if you do not have a microwave use a bain-marie.   
\- Melt marsmallows. The marshmallows will swell. It takes very little time between 15 to 30 seconds so watch carefully. When all the marshmallows have inflated / melted remove from the microwave.   
\- Pour the marshmallows on the icing sugar in the large bowl.   
\- Mix with a greased wooden spoon or the corkscrew tips of your electric mixer.   
\- When the dough becomes friable ****stop the drummer** ** and go to the manual stage!   
Except the lucky ones who have a big robot type Kitchenaid whose engine will resist.   
I only have a small electric hand mixer then ....   
\- Continue adding icing sugar little by little and continue kneading until you get a smooth ball. It requires more or less icing sugar according to the brand of marshmallows and sugar. Keep a bowl of water with a teaspoon on hand in case it gets too dry. 

for the decoration each person his taste. 

Dommage je n’ai pas pu faire beaucoup de photos car ma voisine est venue vite prendre le gateau, elle a oublié de me faire une coupe du gateau, ou peut être il était tellement bon, qu’elles n’avaient pas résister à tout manger. 
