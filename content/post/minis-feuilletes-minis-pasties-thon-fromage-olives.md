---
title: mini pastries mini pasties tuna cheese olives
date: '2011-06-25'
categories:
- Cupcakes, macaroons, and other pastries
- Chocolate cake
- sweet recipes
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/06/chausson-0231.jpg
---
& Nbsp; hello everyone, here is a small entry, that can be presented as apperitive in a party, or at a buffet, very simple to do, but above all too good, this entry that I will call mini sheets, or so mini pasties in English, is based on puff pastry, which you can of course replace with any other homemade dough. I also like to prepare these delights, when I present a soup or to be accompanied by a fresh salad, for a light menu. this time, I made a very simple stuffing, and here are my ingredients: for 6 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.3  (  1  ratings)  0 

Hello everybody, 

here is a small entry, that can be presented as apperitive in a party, or at a buffet, very simple to do, but above all too good, this entry that I will call minis feuilletes, or then mini pasties in English, is based on puff pastry, which you can of course replace with any other homemade dough. 

I also like to prepare these delights, when I present a soup or to be accompanied by a fresh salad, for a light menu. 

this time, I made a very simple stuffing, and here are my ingredients: for 6 mini sheets 

  * 1 rolls of puff pastry 
  * 1 half onion 
  * 1 clove 
  * 1 to 2 fresh tomatoes 
  * some tuna 
  * grated cheese 
  * pitted green olives 


  1. fry the onion, wash and slice it in a little oil, 
  2. add the crushed garlic, add the chopped tomato, and cook until the sauce is reduced. 
  3. cut the puff pastry into small squares, add a little onion / tomato sauce, 
  4. add some tuna, grated cheese, and sliced ​​olives. 
  5. close the dough on itself, decorate the ends with a fork, 
  6. brush with egg yolk, and sprinkle with some black seed (optional) 
  7. cook in a hot oven. 



good tasting. 

* * *

for more recipes, click on one of the photos: 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/chausson-0231.jpg) ](<https://www.amourdecuisine.fr/article-chaussons-aux-aubergines-et-autres-farces-55865874.html>) [ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/volovent-017_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-37525454.html>) [ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cornets-016_thumb21.jpg) ](<https://www.amourdecuisine.fr/article-cornets-sales-a-la-creme-au-thon-49023773.html>)
