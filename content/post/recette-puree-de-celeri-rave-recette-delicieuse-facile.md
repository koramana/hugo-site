---
title: Mashed recipe of celery root / delicious recipe easy
date: '2013-12-08'
categories:
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pur-e-de-c-leri-rave-021.CR2_thumb1.jpg
---
Mashed recipe of celery root / delicious recipe easy 

Hello everybody, 

What do you say about a homemade mashed potato, with this cold, puree of celery, a real delight, especially to accompany a [ roasted shoulder of lamb in the oven ](<https://www.amourdecuisine.fr/article-epaule-d-agneau-rotie-au-four-113882735.html> "roasted shoulder of lamb in the oven") , a [ leg of lamb roast ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-au-four-aid-el-adha-2012-111759550.html> "roasted leg of lamb, help el adha") or fish, or seafood, like scallop nuts. 

sometimes I prepare this mashed potato instead of [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html> "mashed potatoes") because frankly this puree is a delight ... 

the recipe is very very easy to make. and the taste is sublime ... 

when I first saw the celeriac, I did not know what it was? nor how to cook it. 

I began to peel and cut this vegetable, there was this mixture of smells that emanated, a little the smell of fennel, a little the smell of turnips, very fragrant aromas .... I was tempted to drip the raw vegetable, but it was not sweet like fennel, nor pungent like turnip, to be frank, it was still a little bland .... 

after cooking, it's something else ... and when cooking, I do not tell you those good smells that emerge ... 

**Mashed recipe of celery root / delicious recipe easy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pur-e-de-c-leri-rave-021.CR2_thumb1.jpg)

Recipe type:  entrance, accompaniment  portions:  2  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 250 gr of celery beet peeled and cut into small cubes 
  * 250 ml of milk. 
  * 1 tablespoon and ½ of butter 
  * 1 small handful of grilled and chopped hazelnuts (optional) 
  * 1 little salt and black pepper 



**Realization steps**

  1. Place the pieces of celery and milk in a medium saucepan. 
  2. Bring to the boil, then lower the heat, cover and let it cook 
  3. check it a few times, as the milk may boil, cook until the celeriac is tender, about 15 minutes. 
  4. Let cool slightly, then transfer the celeriac and milk to the bowl of a food processor. 
  5. Add the tablespoons of butter salt and a little black pepper from the grinder. 
  6. puree. 
  7. decorate with roasted and crushed hazelnuts. 
  8. you can present right away, if not warm up in the microwave before serving. 


