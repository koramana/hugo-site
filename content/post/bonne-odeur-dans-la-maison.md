---
title: Good smell in the house
date: '2007-12-20'
categories:
- Buns and pastries
- Mina el forn

---
**![bonj7](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201445251.gif) **

**To get a good smell in the house,**

**you can put some ground cinnamon in a small pot of water**

**and heat on the stove.**

![rose4](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201445361.gif)
