---
title: carrot cake
date: '2014-01-22'
categories:
- Cupcakes, macaroons, and other pastries
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/carrot-cake-cake-aux-carottes-gateaux-aux-carottes.jpg
---
![carrot-cake - cake-with-carrots - gateaux aux carottes.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/carrot-cake-cake-aux-carottes-gateaux-aux-carottes.jpg)

Hello everybody, 

here is a delicious **carrot cake, or carrot cake, or carrot cake.**

it's a [ cake ](<https://www.amourdecuisine.fr/categorie-10678925.html>) very mellow, and super rich in taste, which reminds me a lot the texture of [ gingerbread ](<https://www.amourdecuisine.fr/article-pain-d-epice-tres-moelleux-et-trop-bon-49778567.html>) , another cake that I like a lot. 

Do not think that the icing on the cake is the original icing of a carrot cake, no no, at home my husband does not like creams, and as the carrot cake must be iced, and without this cream, the photos will look like nothing, I had to do this little royal icing ... which was all the same appreciated by my husband.    


**carrot cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/carrot-cake-cake-aux-carottes-gateaux-aux-carottes.jpg)

Recipe type:  dessert  portions:  12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 175 gr flour 
  * 120 gr of sugar 
  * 200 gr grated carrots 
  * 175 ml of oil 
  * 100 gr of grapes 
  * 3 eggs 
  * the zest of an orange (I did not, replace with lemon zest) 
  * 1 C. a coffee of cinnamon 
  * ½ teaspoon nutmeg 
  * 1 teaspoon of baking powder 
  * 1 teaspoon of baking soda 
  * a pinch of salt 



**Realization steps**

  1. preheat the oven to 170 ° C 
  2. buttered and floured your mold. 
  3. In a bowl mix, sugar, oil and eggs, then add the peeled and grated carrots, grapes and orange peel. 
  4. In another bowl mix remaining dry ingredients (flour, yeast, baking soda, salt and spices). 
  5. Add the dry ingredients to the first mixture, pour the mixture into the pan and bake in the oven at 170 degrees, about 40 minutes (depending on the oven). 
  6. let cool and unmold 
  7. the galacage was with [ royal cream ](<https://www.amourdecuisine.fr/article-37896589.html>) . 



![cake-with-carrot - cake-to-carottes.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-aux-carottes-cake-aux-carottes.jpg)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
