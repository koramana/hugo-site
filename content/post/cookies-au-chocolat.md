---
title: chocolate cookies
date: '2012-12-03'
categories:
- pizzas / quiches / tartes salees et sandwichs

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cookies-chocolat71.jpg
---
![Cookies-chocolat7.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cookies-chocolat71.jpg)

Hello everybody, 

here are chocolate cookies that I really want to put in my cup of milk, and leave a moment, before tasting this burst of flavors, huuuuuuuuuuuuuum ... 

so we go to the recipe: 

preparation time: 20 minutes 

  
the ingredients will give:   
2 plates of 12 cookies: 

  * 220 g of dark chocolate of very good quality 
  * 4 tbsp. to s. of butter 
  * 2/3 cup of flour (86 gr) 
  * 1/2 cuil. to c. of baking powder 
  * 1/2 cuil. to c. salt 
  * 2 big eggs 
  * 3/4 cup of brown sugar (170 gr) 
  * 1 tbsp. to c. pure vanilla extract 
  * 250 gr (340 gr normally) of chocolate chips 


  1. Preheat the oven to 180 ° C. 
  2. Heat chocolate and butter in a bain-marie stirring until melted. 
  3. In a bowl, whisk together the flour, baking powder and salt. 
  4. In a bowl, beat eggs with brown sugar, vanilla extract and at high speed until light and fluffy. 
  5. Reduce the speed to low and beat adding the melted chocolate. 
  6. Stir in the flour mixture without mixing too much. 
  7. Stir in chocolate pieces (chocolate chips or chunks). 
  8. Using an ice cream scoop, arrange 2 to 3 cm intervals between baking trays on baking trays. 
  9. bake for 12 minutes, until the cookies are shiny and crunchy but soft in the center 
  10. let cool on the baking sheets for 10 minutes, then transfer to a rack to cool completely. 

"Do not over cook cookies, they are meant to be soft and fluffy. " ![Downloads1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/Downloads11.jpg) thank you for your comments and your passages bonne journee 
