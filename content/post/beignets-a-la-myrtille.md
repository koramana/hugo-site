---
title: donuts with blueberry
date: '2012-03-22'
categories:
- cuisine algerienne
- Dishes and salty recipes
- soupes et veloutes

---
Hello everyone, at home we love donuts, be it salty donuts, or sweet donuts. for my part I prefer by far the sfenj (Algerian pastry) because that's what my mother often prepared, to drip, and what they were light and sweet donuts of my mother. So today, I share with you these blueberry donuts, a delicious fruit mixed with a very light donut dough. a very good sweets sprinkle with powdered sugar or honey, only goes well with your coffee 4 hours. the ingredients: 250 g of fresh blueberries 250 g & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

> at home we really like **donuts** , whether it's [ salted donuts ](<https://www.amourdecuisine.fr/categorie-12348217.html>) , or some [ sweet donuts ](<https://www.amourdecuisine.fr/categorie-11700496.html>) . for my part I much prefer [ sfenj ](<https://www.amourdecuisine.fr/article-25345460.html>) ( [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) ) because that's what my mother often prepared for us, to drip it, and what they were light and sweet **donuts** from my mother. 
> 
> so today, I share with you these **blueberry fritters** , a delicious fruit mixed with a very light donut dough. a very good **cuteness** sprinkle with powdered sugar or honey, just go well with your 4-hour coffee. 

Ingredients: 

  * 250 g fresh blueberries 
  * 250g of flour 
  * 1 packet of dry yeast 
  * 2 eggs 
  * 2 bags of vanilla sugar 
  * 250 ml of milk 



method of preparation: 

  1. wash them **blueberries** and let it drain. 
  2. prepare the dough by mixing the flour, sugar, eggs and milk. 
  3. mix well until you get a smooth dough. Let it rest for 1 hour. 
  4. Once the dough rested, gently add the blueberries. 
  5. Heat oil in a skillet. Cook the donuts gently (so as not to break the blueberries). 
  6. Once cooked, place them on absorbent paper 
  7. sprinkle with icing sugar or honey. Serve warm. 



follow me on: 

Or on 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
