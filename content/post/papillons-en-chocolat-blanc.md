---
title: butterflies in white chocolate
date: '2012-05-02'
categories:
- recettes sucrees
- pies and tarts

---
**[ birthday cake decoration ](<https://www.amourdecuisine.fr/article-bavaroise-fraise-104360210.html>) **

Hello everybody, 

a tutorial for you show how to prepare ** butterflies in white chocolate  ** , ideal for decorating cakes, and especially [ birthday cakes ](<https://www.amourdecuisine.fr/categorie-11700499.html>) : 

for that you will need white chocolate and dye according to your taste. 

draw the butterflies on a white sheet, place your melted and colored chocolate in a small sachet, make a small hole at the corner of the bag and fill the butterfly, after drying the wings of the butterfly, take the melted dark chocolate and fill the body of the butterfly , place your leaf in a place that allows you to have the V shape so that the butterfly is not flat. 

let dry. it will stand alone from the sheet. 
