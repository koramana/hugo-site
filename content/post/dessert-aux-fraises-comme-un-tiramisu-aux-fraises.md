---
title: strawberry dessert as a strawberry tiramisu
date: '2017-05-15'
categories:
- dessert, crumbles and bars
tags:
- Algeria
- Ramadan 2017
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tiramisu-au-fraise-sans-biscuit-008.CR2edited1.jpg
---
![tiramisu au strawberry-in-biscuit - 008.CR2edited.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tiramisu-au-fraise-sans-biscuit-008.CR2edited1.jpg)

##  strawberry dessert as a strawberry tiramisu 

Hello everybody, 

I enjoy myself and the children of these delicious juicy strawberries, when are everywhere in our markets, and at unbeatable prices ... 

And yesterday, when I made a delicious tiramisu without egg, I had a little cream, so I made these little glasses, that I and the children have tasted without leaving a dad, hihihihihi 

I called these verrines "as a tiramisu" because I wanted to add biscuits to the spoon, but I did not have any more .... I used everything in tiramisu. 

**strawberry dessert as a strawberry tiramisu**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tiramisu-aux-fraises-dessert-au-fraise-017.jpg)

**Ingredients**

  * strawberries (it was at random) 
  * 100 gr of mascarpone. 
  * 100 ml of very cold cream 
  * 40 gr of sugar (for the cream) 
  * 2 tablespoons grenadine syrup (optional) 
  * 1 tablespoon of water 
  * 1 tablespoon of orange blossom water 



**Realization steps**

  1. clean the strawberries in abundant fresh water. 
  2. cut them into pieces, and place them in a salad bowl (goblets) 
  3. if you use grenadine syrup, dilute it in a little water, and sprinkle the strawberries with it, and let it absorb, you can adjust the sugar according to your needs. 
  4. if you use orange blossom water, sprinkle your strawberries with it, add a little sugar according to your needs, and leave aside. 
  5. in another bowl, add the cold whipped cream, using an electric whisk, add 20 grams of sugar, and continue whisking. 
  6. in a bowl, beat the mascarpone until it becomes smooth, add the remaining sugar. 
  7. mix the mascarpone with the whipped cream very slowly, to have a beautiful mousse. cool 
  8. at the time of serving, prepare your verrines, place first a beautiful layer of mascarpone mousse, 
  9. then place a layer of strawberries with its juice. 
  10. then put another layer of mascarpone foam, then another layer of strawberries 
  11. finally garnish with mascarpone cream and a whole strawberry. 
  12. enjoy it immediately, this dessert does not need to rest in the fridge all night, hihihihi. 



![tiramisu aux fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tiramisu-aux-fraises1.jpg)
