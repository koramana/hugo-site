---
title: mint syrup / refreshing drink for ramadan 2017
date: '2017-06-02'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/sirop-de-menthe-511.jpg
---
![mint syrup / refreshing drink for ramadan 2017](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/sirop-de-menthe-511.jpg)

##  mint syrup / refreshing drink for ramadan 2017 

Hello everybody, 

In Ramadan, the mint syrup is always at table, because especially after a long hot day, it is only this refreshing taste of mint that will calm my thirst .... But, as there is a big "But", personally it is the commercial mint syrup that I took ... 

So today, I share with Lunetoiles recipe homemade mint syrup, and I'm already going to start preparing my reserve for the month of Ramadan .... what is it for you? 

**mint syrup / refreshing drink for ramadan 2017**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/sirop-de-menthe-311.jpg)

portions:  6  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 1 beautiful bouquet of mint 
  * 1 L of spring water 
  * 1 kg of sugar 
  * green dye (optional) 



**Realization steps**

  1. Take the mint out and put it in a salad bowl. 
  2. Bring the water to a boil and pour over mint. 
  3. Let cool, and let it steep for 24 hours in the refrigerator. 
  4. Filter and measure the volume of the infusion obtained. 
  5. If necessary, add a little water to get 1 L of liquid. 
  6. Pour into a saucepan and add the sugar. 
  7. Bring to a boil, until the syrup begins to boil with a large bubble (100 ° C to 105 ° C). 
  8. Skim. Let cool. 
  9. Add if you want green dye. 
  10. Bottle. 



![Syrup de menthe.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/sirop-de-menthe11.jpg)
