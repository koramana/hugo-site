---
title: ghrif baghrir corsa thighrifine pancakes
date: '2011-04-25'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-018_thumb.jpg
---
#  baghrir 

[ ![baghrir 018](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-018_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-018.jpg>)

Hello everybody, 

I used the bread machine, I added the warm water gently, I used the pizza cycle, 2 times, but I had pancakes very very light and too good especially. 

I started from the principle of Nadia, but using the means at hand, because after my stay at Sihem, the fridge was empty, and yesterday it was a holiday so everything was closed here. 

**ghrif baghrir corsa thighrifine pancakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-014_thumb.jpg)

portions:  20  Prep time:  60 mins  cooking:  15 mins  total:  1 hour 15 mins 

**Ingredients**

  * 3 glasses of fine semolina 
  * 1 glass of flour 
  * 1 tablespoon of oil 
  * ½ teaspoon of baking powder 
  * an instant yeast soup 
  * ½ teaspoon of salt 
  * 1 glass of milk 
  * some water 



**Realization steps**

  1. in the bowl of the bread maker, add all the ingredients in the order recommended. 
  2. start the pizza program 
  3. add a glass of warm water gradually (see less than a glass) 
  4. add the glass of warm milk gradually 
  5. the dough must be very smooth, otherwise add gently warm water 
  6. if the kneading cycle comes to an end, and the dough is not the desired consistency, restart the program. 
  7. let it sit well and double the volume 
  8. remove the bowl from the bread maker, and heat the crepe maker like me or use a crepe pan 
  9. pour the equivalent of a good ladle and spread gently to have a nice circle 
  10. cover with the top of a couscoussier, or a lid very high 
  11. When the holes are well formed on the surface of your pancakes, remove the lid, and let the pancake take a nice color. 
  12. to serve, melt butter, sprinkle the pancakes one by one, and sprinkle with sugar according to your taste 



enjoy 

[ ![2011-04-25 baghrir](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2011-04-25-baghrir_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2011-04-25-baghrir_2.jpg>)

merci pour vos visites, et vos commentaires, bonne journee 
