---
title: crumble with sweet potato and carrot
date: '2012-06-07'
categories:
- Mina el forn
- pain, pain traditionnel, galette

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/crumble-patate-douce-carotte-052_thumb1.jpg
---
##  crumble with sweet potato and carrot 

Hello everybody, 

here is a recipe that was in my archive for 2 months now, a delicious ** sweet potato crumble and carrots  ** . 

a very delicious sweet salty crumble, very easy to make with seasonal ingredients, enjoy with my friends. Personally I like the sweet potato unlike and my children and my husband, and they do not know what they lose ??? 

I still find that the sweet potato here is different from the sweet dough in Algeria which has rather a yellowish color and a rather mealy aspect after cooking. What we liked about the sweet potato grilled on the stove ... What good taste and what a good smell there.   


**crumble with sweet potato and carrot**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/crumble-patate-douce-carotte-052_thumb1.jpg)

**Ingredients**

  * 3 sweet potatoes 
  * 1 onion 
  * 2 carrots 
  * 25 gr of Parmesan cheese 
  * 25 gr of butter 
  * 25 gr of flour 
  * 25 gr of almond powder 
  * 25 gr of durum wheat semolina 



**Realization steps**

  1. method of preparation: 
  2. Preheat the oven to 180 ° C (th 6). 
  3. Mix with your fingertips butter, flour, durum wheat semolina, parmesan and hazelnuts. We obtain a sandy mixture, reserve cool 
  4. Peel the onion and cut into slices 
  5. In a pan of cold water, add the salt, put the onion and boil for 3 minutes. 
  6. Peel the carrots and cut them into 1 cm thick bevel, add the carrots to the onions and cook for 5 minutes 
  7. Peel the sweet potato and cut into 1 cm cubes and add to the other vegetables and cook for 10 minutes 
  8. Drain the vegetables 
  9. Put the vegetables in small cocottes, tamp a little with the spoon and put the crumble on it (you can also mix the vegetables in puree, and add a little milk) 
  10. Bake at 180 ° C (th 6) for 30 minutes. 


