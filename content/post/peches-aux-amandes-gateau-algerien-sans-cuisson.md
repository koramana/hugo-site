---
title: almond peaches, Algerian cakes without cooking
date: '2012-11-02'
categories:
- confitures et pate a tartiner
- dessert, crumbles et barres
- idee, recette de fete, aperitif apero dinatoire
- recettes sucrees

---
Hello everybody, 

Here is a delight of Algerian cuisine, a magnificent Algerian cake, or more precisely, a fruit in almond paste, a cake without cooking, super melting in the mouth, a recipe that one of my readers likes to share with us, thank you Malika. K for sharing. 

ingredients: 

  * 500g almond powder 
  * 250g icing sugar 
  * 1/2 spoon vanilla sugar 
  * 3 egg white 



Prank call: 

  * 1 / 2pack of biscuit (madeleine) 
  * Strawberry jam 
  * Green and red dye 



method of preparation: 

  1. First in a bowl, mix the almonds, the sugar, and the egg white until you have a paste 
  2. Make dumplings the shape of a walnut let stand 10 minutes 
  3. meanwhile, mix the biscuits (madeleine) and mix with jam, make very small balls. 
  4. Then, take the almond paste again, introduce the small dumpling into the big dumpling and form small peaches. 
  5. with the green dye mix with a little fine dough to make leaves that you lay on the peach, 
  6. dilute some red dye in a glass of water. 
  7. Using a cotton swab, color half of the peach and leave to dry for one hour and sprinkle with icing sugar ... good achievement 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
