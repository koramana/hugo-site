---
title: easy birthday cake with chocolate candies
date: '2015-10-19'
categories:
- birthday cake, party, celebrations
- cakes and cakes
- recettes sucrees
tags:
- Algerian cakes
- To taste
- fondant
- desserts
- Pastry
- creams
- Butter cream

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-danniversaire-simple-et-facile-pour-enfant.jpg
---
[ ![simple and easy birthday cake for children](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-danniversaire-simple-et-facile-pour-enfant.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-danniversaire-simple-et-facile-pour-enfant.jpg>)

##  easy birthday cake with chocolate candies 

Hello everybody, 

Yesterday was my daughter's birthday, she asked me to make her a cake that she could decorate herself ... I admit that I liked the idea because I was too tired to spend a sleepless night decorating it with sugar dough or almond paste ... I made a small vote, to find out what they like as a base of cake, and I was surprised that everyone chose the basis of [ Russian cake ](<https://www.amourdecuisine.fr/article-le-gateau-russe-methode-simplifiee-chez-les-patissiers-algeriens.html>) , and its butter cream ... 

Super, it's very easy to achieve, except that I did not have a large tray to cook the cake at one time, and cut three large discs. I had 2 pie pans of the same size, one with a removable base, and the other normal, and a silicone cake pan of the same size, I divided the preparation into the three paper-lined molds Cooking ... the cakes were perfect, because you have to cook them at the same time, I still find that the cakes are a little high ... But it was so well cooked, and very crunchy, we loved it at home ... between we 5 eaten just a quarter of the cake because it is very rich ... 

I believe that such a cake will be enough for 12 people. 

My daughter was good at decorating side, we looked at the night a little on Pinterest to get ideas, in the morning we went to buy the chocolate candy needed to decorate his cake. When the cake was cooked, and the editing did ... I let him do his magic trick. A happiness to see her busy to decorate it, I think it's the most gift I could give him, hihihiih 

[ ![easy kid birthday cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-facile-danniversaire-pour-enfant.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-facile-danniversaire-pour-enfant.jpg>)   


**easy birthday cake with chocolate candies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-danniversaire-bonbon-chocolat.jpg)

portions:  12  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** Russian cake dough: 

  * 450 gr unglazed almonds, toasted and crushed 
  * 225 gr of flour 
  * 1 C. thin yeast soup 
  * 325 gr of butter 
  * 325 g icing sugar 
  * 8 egg whites 
  * butter cream: 
  * 3 eggs 
  * 330 gr icing sugar 
  * 325 gr of butter 

Decoration: 
  * white chocolate buttons 
  * dark chocolate buttons 
  * chocolate fingers 
  * chocolate sweets Malteser 
  * chocolate stars 



**Realization steps** preparation of the butter cream 

  1. beat the eggs and the icing sugar, using a whisk, in a salad bowl place on a bain-marie for 15min 
  2. let cool, then add the butter cut in pieces, while whisking with a mixer and leave to the cool side 

preparation of the dough: 
  1. make the egg whites rise. 
  2. mix the crushed almonds (when I chop the almonds to the robot, I try not to have a big piece, and I use, the fine powder, the well crushed pieces, so I do not use only the crushed almonds ) with sugar yeast, flour and melted butter, 
  3. then incorporate the white in snow gently 
  4. in suites put in a dish (for biscuit roulet), going to the oven to wrap with paper grease and bake at 180 ° C (I used three mold of 25 cm diameter) 
  5. once cooked let cool well 
  6. Cover the first disc with the butter cream you have removed from the fridge at least 15 minutes beforehand. 
  7. Deposit the second disc, and cover it with the butter cream too, 
  8. place the last disc, cover it with the butter cream, as well as the sides 
  9. Decorate the cake with chocolate fingers around the cake 
  10. and decorate the cake according to your taste with the remaining candies 



[ ![birthday cake for child very easy](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-danniversaire-pour-enfant-tres-facile.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-danniversaire-pour-enfant-tres-facile.jpg>)
