---
title: Menu 1st day of iftar of Ramadan
date: '2014-06-28'
categories:
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg
---
[ ![tajine hlou lham lahlou](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg>)

##  Menu 1st day of iftar of Ramadan 

Hello everybody, 

you are already thinking about what you will do on this first day of Ramadan for Iftar ??? So for you help a little, I pass you this small menu, which may well guide you in your choices: 

For the beginning, the tradition is that one prepares a dish sugar, so that ramadan is sugar, hihihihi and that the month passes pleasantly smoothly, (photo above) 

[ lham l Ahlou, sweet dish, tajine lahlou. ](<https://www.amourdecuisine.fr/article-35152846.html>)

if not:   
  
<table>  
<tr>  
<td>

![apple tajine](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajin-hlou-11.jpg) [ tagine with apples, without meat ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html>) 
</td>  
<td>

[ ![chbah essafra2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra2-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra2.jpg>) [ Algerian cuisine, Chbah essafra ](<https://www.amourdecuisine.fr/article-chbah-essafra-%d8%b4%d8%a8%d8%a7%d8%ad-%d8%a7%d9%84%d8%b5%d9%81%d8%b1%d8%a9.html> "chbah essafra شباح الصفرة") 
</td>  
<td>

![tajine de nefles aux-almond 076.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-de-nefles-aux-amandes-076.CR2_2.jpg) [ tajine of stuffed loquat with almonds ](<https://www.amourdecuisine.fr/article-tajine-de-nefles-farcies-aux-amandes-117638636.html>) 
</td> </tr> </table>

Who says sweet tajine, say dip his piece of bread in it to soak it with a scented sauce, and for that, we will have to prepare: 

[ ![matloue-013_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-013_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-013_thumb1.jpg>)

[ galette Matlou3 - مطلوع Algerian Arabic bread ](<https://www.amourdecuisine.fr/article-41774164.html>)

as you can prepare:   
  
<table>  
<tr>  
<td>

![khobz dar, home-made homemade bread 003](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/khobz-dar-pain-maison-sans-petrissage-003_thumb1.jpg) [ khobz dar without food / homemade bread ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) 
</td>  
<td>

![khobz eddar with 3 grains](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/khobz-eddar-aux-3-grains_thumb1.jpg) [ Khobz eddar with 3 grains - easy homemade bread ](<https://www.amourdecuisine.fr/article-khobz-eddar-aux-3-grains-80811114.html>) 
</td>  
<td>

![khobz dar](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/33122139_thumb1.jpg) [ khobz dar ](<https://www.amourdecuisine.fr/article-khobz-dar-108029095.html>) 
</td> </tr> </table>

and now, the kings or the queen of the table: chorba, Jari, harira 

![chorba-frik-012.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-0122.jpg) [ chorba Frik ](<https://www.amourdecuisine.fr/article-25345575.html>)

or:   
  
<table>  
<tr>  
<td>

[ ![chorba bida 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-bida-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35553378.html>)   
[ chorba bayda, white soup with vermicelli and chicken "شربة بيضاء" ](<https://www.amourdecuisine.fr/article-35553378.html>) 
</td>  
<td>

![harira](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/hrira-oranaise-copie-12.jpg) [ harira / soup for ramadan ](<https://www.amourdecuisine.fr/article-harira-soupe-pour-ramadan-2013-118969374.html>) 
</td>  
<td>

![chorba vermicelli 007](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-vermicelle-007_thumb2.jpg) [ Chorba vermicelli, soup for ramadan ](<https://www.amourdecuisine.fr/article-chorba-vermicelle-soupe-a-la-vermicelle-81037155.html>) 
</td> </tr> </table>

to accompany this beautiful soup, there is no more delicious than the bourek or the bricks: 

![bourek-to-apples-to-earth-and-fromage.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourek-aux-pommes-de-terre-et-fromage.CR2_1.jpg) [ bourek with potatoes and cheese / entree for ramadan ](<https://www.amourdecuisine.fr/article-bourek-aux-pommes-de-terre-et-fromage-entree-pour-ramadan-118926220.html>)

the bourek has the potato, economical, and delicious remains my favorite, but you can still realize: 

and also for more freshness on your tables, we like salads: 

![salad-composed-at-rice-to-Ramadan-2013.CR2.jpg](http://amour-de-cuisine.com/wp-content/uploads/2009/08/salade-composee-au-riz-pour-ramadan-2013.CR2_.jpg) [ Mixed salad with rice and tuna / salad for ramadan 2013 ](<https://www.amourdecuisine.fr/article-salade-composee-au-riz-et-thon-salade-pour-ramadan-2013-118909117.html>)

or: 

and as main course, I still like: 

[ minced meat recipe with olives, minced meat tagine with olives ](<https://www.amourdecuisine.fr/article-recette-viande-hachee-aux-olives-tajine-de-viande-hachee-aux-olives-111430261.html>)

or else: 

always a refreshing on the table and it will be for me in these days very hot: 

![Syrup de menthe.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/sirop-de-menthe1.jpg)

[ mint syrup / refreshing drink ](<https://www.amourdecuisine.fr/article-sirop-a-la-menthe-boisson-rafraichissante-118439235.html>)  
  
<table>  
<tr>  
<td>

[ ![homemade lemonade lemonade](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/limonade-au-citron-faite-maison-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/limonade-au-citron-faite-maison.jpg>) [ lemonade, or lemonade with lemon ](<https://www.amourdecuisine.fr/article-citronade-limonade-au-citron-faite-maison.html> "homemade lemonade or lemonade") 
</td>  
<td>

[ ![pina colada cold drink without alcohol](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Pina-colada-sans-alcool.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Pina-colada-sans-alcool.CR2_.jpg>) [ pina colada without alcohol ](<https://www.amourdecuisine.fr/article-pina-colada-boisson-fraiche-sans-alcool.html> "pina colada: cold drink without alcohol") 
</td>  
<td>

[ ![Grenadine granite.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/granit%C3%A9-a-la-grenadine.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/granit%C3%A9-a-la-grenadine.CR2_.jpg>) [ grenadine granita ](<https://www.amourdecuisine.fr/article-granite-a-la-grenadine-ou-granite-au-sirop-de-grenadine.html> "grenadine granita or granita with grenadine syrup") 
</td> </tr> </table>

I have other home-made juice recipes to come, that I will try to post as soon as possible, and now we go to dessert, it's mostly for children, or for the evening: 

![creme-INVERTED-au-lait-focus-sucre.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-renversee-au-lait-concentre-sucre.CR2_1.jpg) [ sweet caramel cream ](<https://www.amourdecuisine.fr/article-creme-caramel-au-lait-concentre-sucre-116960534.html>)  
  
<table>  
<tr>  
<td>

[ ![creme-brulee-047.CR2_thumb2](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-047.CR2_thumb2-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-047.CR2_thumb2.jpg>) Creamy crème brûlée 
</td>  
<td>

[ ![Mahalabia-to-apricot-034_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/Mahalabia-aux-abricots-034_thumb-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/05/Mahalabia-aux-abricots-034_thumb.jpg>) Mahalabia with apricots 
</td>  
<td>

[ ![mhalbi.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi.CR2_1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi.CR2_1.jpg>) Mhalbi, cream of rice, algerian dessert 
</td> </tr> </table>

and for the evening with a good tea, the kings of Ramadan cakes: 

[ ![qalb elouz](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/qlb-005_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35768313.html>)

[ kalb el louz / ramadan dessert / قلب اللوز ](<https://www.amourdecuisine.fr/article-kalb-el-louz-dessert-de-ramadan-118938674.html>)

or: 

or even easier: 

[ ![crispy donuts](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2_.jpg>)

[ donuts with donut iron ](<https://www.amourdecuisine.fr/article-beignets-au-fer-a-beignet-gateau-algerien.html> "donuts with donut iron, Algerian cake")

**1st day menu of Ramadan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg)

portions:  4  Prep time:  60 mins  cooking:  60 mins  total:  2 hours 

**Ingredients**

  * chorba 
  * tajine hlou 
  * bourek 
  * salad 



**Realization steps**

  1. it must be ready before the Maghreb! lol 


