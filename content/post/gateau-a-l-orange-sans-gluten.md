---
title: orange cake without gluten
date: '2015-09-28'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/03/gateau-a-l-orange_thumb.jpg
---
[ ![orange cake without gluten](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/gateau-a-l-orange_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/03/gateau-a-l-orange.jpg>)

##  orange cake without gluten 

Hello everybody, 

A delicious gluten-free orange cake that comes from the kitchen of our dear Lunetoiles. I admit that I personally like the flavor that citrus can bring to cakes. 

This cake has gluten-free orange is super easy to do, yes it's a gluten-free cake (because there is no flour in it) a cake with almonds all fluffy and all melting and super fragrant .... 

So enjoy this season that comes, the season of oranges to enjoy and do this delight, you will surely join.   


**orange cake without gluten**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-a-l-orange-1_thumb.jpg)

Recipe type:  Dessert  portions:  8  Prep time:  25 mins  cooking:  30 mins  total:  55 mins 

**Ingredients**

  * 275 gr ground almonds 
  * 225 gr of sugar 
  * 5 eggs 
  * 2 organic oranges preferably 
  * 65 gr of soft butter 
  * 1 sachet of baking powder 

optional: 
  * 2 drops of bitter almond 

caramel with orange: 
  * 225 gr of sugar 
  * 125 ml orange juice (1/2 cup) 
  * 125 ml of water (1/2 cup) 



**Realization steps**

  1. Wash the oranges and put them in warm water, whole with the skin, over medium heat, boil for about an hour, until they are tender. 
  2. Cut in two or four and mix (whole, with the skin) to obtain a creamy texture. 
  3. Preheat the oven to 160 ° C. 
  4. Butter a round baking tin with a removable bottom and covered with baking paper in the bottom. 
  5. Beat softened butter and sugar, work well until creamy 
  6. then add the eggs, one by one, while beating to obtain a homogeneous mixture. 
  7. Add almond powder, baking powder and mixed oranges. 
  8. Pour the dough into the mold and bake. 
  9. Cook for one hour. Check the cooking by inserting a knife in the center of the cake, it must come out clean and dry. 
  10. While baking the cake, prepare the _orange caramel_ : 
  11. In a saucepan pour the orange juice, water and sugar. Keep boiling for about 15 minutes on medium heat or until a syrup is obtained. 
  12. Sprinkle the cake with icing sugar and serve with a little syrup. 



[ ![orange cake without gluten](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/gateau-a-l-orange-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/03/gateau-a-l-orange-2.jpg>)
