---
title: typically Algerian chicken rice, Algerian dish
date: '2011-12-20'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/216435091.jpg
---
![typically Algerian chicken rice, Algerian dish](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/216435091.jpg)

##  typically Algerian chicken rice, Algerian dish 

Hello everybody, 

I have a friend of mine a Algerian who lives in Bahrain, who always visit me on my blog, and who always encourages me with her pretty comentaries, pity she does not have a blog, because cote cuisine, she it's great. 

she sent me this recipe, which I was anxious to try. 

to be frank, I mixed my ingredients hesitance, because it was all the same different from my recipes, and also, different as taste. 

the result……. I tell you this story first, so I started the recipe for lunch, and as I was afraid of the result, I did a small part only, and as luck would have it, there was an expection of the kitchens the building, to see their state and if they need work, and it was today, so we knock on the door, and it was 4 people as big as bascket ball players (I'm 1m50, so you see the scene). 

they enter the kitchen (it was midday) and one of them blows the others: it smells good here, oh lala, they looked at the pot, I think they did not think anymore about the cooking, but what was in this tiny pot, I was so intimidated, if there were many we would invite them, and my husband said: I give them the invitation, and I tell him, do as if you did not hear anything ... .. if I put the dish on them, it's going to be a wrestling match, you've seen how huge they are, it's not going to be enough for one of them. 

I come back to the recipe and forgive me for talking too much:   


**typically Algerian chicken rice, Algerian dish**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/21643654.jpg)

portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * chicken (the best is the breast) 
  * a head of garlic 
  * rice 
  * fresh tomato (or in box) 
  * 1 case of canned tomato 
  * laurel and thyme 
  * salt, black pepper, cumin and paprika 
  * maizina 
  * frying oil.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/216435551.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/216435551.jpg>)



**Realization steps**

  1. first of all, marinate the chicken that you cut into pieces, with salt, black pepper, cumin and paprika, roll in maizina, and fry.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/216435951.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/216435951.jpg>)
  2. drain well. 
  3. prepare white rice, cooking the equivalent of one measure of rice with two measures of water, do not hesitate to add water to your rice, if you see that it does not tender as you want.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/216436211.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/216436211.jpg>)
  4. Prepare a sauce with a little oil, 1 head of garlic crush, you add the tomato and fresh tomato, lauriet, thyme, salt, pepper, caraway, and paprika. 
  5. simmer a little, add your chicken, let simmer a little. 
  6. divide your sauce in half, in one add the cooked rice, and the other part, decorate with the rice before serving 



![rice](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/216436541.jpg)

bon appetit. 
