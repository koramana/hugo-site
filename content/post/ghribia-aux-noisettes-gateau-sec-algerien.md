---
title: ghribia with hazelnuts / algerian dry cakes
date: '2014-07-25'
categories:
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/ghribia-aux-noisettes-033.CR2_.jpg
---
![ghribia-to-nuts-033.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/ghribia-aux-noisettes-033.CR2_.jpg)

##  ghribia with hazelnuts / algerian dry cakes 

Hello everybody, 

The ghribya, or ghoriba is the cake that I like most, very melting in the mouth, very easy to achieve. What I like most is these intoxicating smells that escape the oven during cooking, to scent the whole house ... 

One thing that evokes a lot of childhood memories in me, during the long ramadan nights, when my mother started to prepare the Eid cakes, she put us to bed, to do these little delights ... and Oh it was nice to smell those smells at home ... I remember that early in the morning, I woke up gently, and followed the smell to find where my mother would hide those cakes she made the day before and .... you know the rest, hihihihi. 

Today I share with you this hazelnut ghribia .... a melting delicacy that you would not regret to realize. 

**ghribia with hazelnuts / algerian dry cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/ghribia-aux-noisettes-023.CR2_.jpg)

Recipe type:  Algerian cake  portions:  30  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients** For thirty pieces, 

  * 300 gr of ghee, smen (clarified butter) 
  * 150 gr of icing sugar 
  * 110 ml of table oil 
  * 100 gr of hazelnuts in powder 
  * a little vanilla 
  * flour (almost 1 kilo) 
  * some whole hazelnuts roasted. 



**Realization steps**

  1. Beat the smen with the icing sugar with an electric whisk, until it becomes creamy and double or even triple volume. 
  2. Then add the oil in a net while whisking. 
  3. You must have a nice cream like a mousse or whipped cream. 
  4. Add the hazelnuts and vanilla. 
  5. Then pick up with flour until you get a paste. 
  6. Make balls of dough according to the size of your mold. 
  7. Flour the mold cavity so that it does not stick, put a ball of dough. Press and return to the table 
  8. place in on a baking tray and place the hazelnut in the center of the cake. Do so until the dough is exhausted. 
  9. Cook at 170 ° C, for 10 to 13 minutes, you have to really watch, they just have to dry, but not more than 15 minutes. 
  10. remove from oven, allow to cool well before unmolding. 
  11. This ghribia is kept in an airtight box for almost 3 weeks, if you can resist it. 



#  hazelnut shorts bread / Algerian sweets ghoriba 

Ingredients: 

  * 300 g ghee, 
  * 150 g icing sugar 
  * 110 ml oil 
  * 100 g hazelnuts powder 
  * 1 tsp vanilla 
  * flour (enough to have a nice dough) 
  * some whole roasted hazelnuts. 



Instructions: 

  1. beat ghee and icing sugar with an electric whisk, until it becomes creamy fluffy and it doubles or even triples in volume. 
  2. add oil and keep beatting. You must have a good cream like a nice whipped cream. 
  3. fold in hazelnuts powder and vanilla. 
  4. Stir in the flour to get a smooth paste. 
  5. roll dough into balls, and press each ball in a maamoul mold. 
  6. Flour the mold cavity so it does not stick. 
  7. place the cookies on a non-stick baking sheet and pick a hazelnut at the center of each cookie. 
  8. Bake at 170 ° C for 10 to 13 minutes, until bottom of the cookie is light brown. 
  9. to remove from the oven 
  10. you can keep this ghribia or algerian short breads in an airtight container for almost 3 weeks. 


