---
title: Stop your harassment on the comments
date: '2009-01-03'
categories:
- Coffee love of cooking

---
Hello everybody,   
I'm sorry, you'll notice that I made a moderation in the comments, why?   
because there are people who have nothing to do but to come and harass people on their blogs.   
and in addition they do not even have the audacity to leave their coordinates, this morning I received a "so-called" comment from a person who comes to me a lesson in morality, to tell me, to stop to sell to me, and to show me to people, that from his point of view, she has the impression that I need to show myself and show my private life ... .. 

so the ... .. me I say it's a jealous person, who has broken arms who does not even know how to make fries, who does not even have an email according to his words, so this is the first time that she is on the internet because even my 7 year old daughter has an email. 

I apologize to everyone, it's not that this person has me wrong, I do not know people like that, but it's that I noticed, that I'm not the only one to be a victim of such comments, coming from ignorant people, who do not even know what is a blog. 

So I say it again and again, that's my blog I put on what I want, even if I want to put the pictures of my wedding, she does not come to tell me that I sell, or that it hides behind the word Haram, which becomes an easy word, and used when one wants to make pretexts. 

it is a very high word, having a lot of value, which one must not put in any context. 

therefore to this person, and to others, before coming to judge others, begin by judging yourself, because the good prophet said in the meaning of the hadith: a smile in the face of your brother is a sadaka, whereas is it of you who come, and who devalorisis a work as huge as a blog, in which women like us do recipes not to show it, but to help other people, who can be begin a conjugal life, or else, begin life alone, or even for men who are far from their families and who want to have fun by preparing a dish that their mothers made them. 

hontes a vous, c’est ce que je vous dis 
