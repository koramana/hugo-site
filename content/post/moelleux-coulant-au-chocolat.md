---
title: mellow chocolate flowing
date: '2013-03-05'
categories:
- Algerian cuisine

---
Hello everybody, 

it flows, it melts, go to your spoons, because here is a sublime recipe for a chocolate cake with a very smooth heart. 

do not blame me, it's not me who made this torture, huuum, it's Lunetoiles, but the first plate is for me, so nobody touches ... 

but do not worry, there is something for everyone. 

For 12 mellow. 

Ingredients: 

  * 250 g dark chocolate, 
  * 250 g butter, 
  * 8 eggs, 
  * 125 g caster sugar, 
  * 75 g flour, 
  * melted butter for mussels. 



method of preparation: 

  1. Preheat your oven to 200 ° C (heat 6-7). 
  2. Cut the chocolate into pieces in a bowl and melt with the butter in the microwave or bain-marie. 
  3. Meanwhile, beat the eggs and sugar in a bowl until the mixture is lightly fluffy, then stir in the flour. 
  4. Butter each individual mold with a brush and place them on the baking sheet. 
  5. Whip the melted butter and chocolate to obtain a homogeneous mixture. 
  6. Add it to the egg-sugar-flour mixture. 
  7. Divide the dough into the molds and bake for 6 minutes. 
  8. Enjoy the mellow on leaving the oven. 


