---
title: the Chinese
date: '2016-12-03'
categories:
- Buns and pastries
- Mina el forn
tags:
- To taste
- Custard
- Easy cooking
- Boulange
- buns
- Rolled brioche
- Cinnamon rolls

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chinois-reussi.jpg
---
![Chinese-managed](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chinois-reussi.jpg)

##  The Chinese 

Hello everybody, 

I already published several Chinese recipes on my blog, [ right here ](<https://www.amourdecuisine.fr/article-27039105.html>) and [ the ](<https://www.amourdecuisine.fr/article-25345321.html>) , with different pasta, and sincerely, this pasta in this recipe of Chinese remains the best. 

A successful Chinese, it's a successful dad, a very fast crumb! The Chinese was very airy, and of course too good. Moreover, this recipe is often the big success for my readers, and I would be happy to count you among her, so if you made this recipe and you have succeeded, send me the picture on my email: 

![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ![Chinese-to-nadia](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chinois-chez-nadia.jpg)

**the Chinese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/le-chinois-030_thumb11.jpg)

portions:  10  Prep time:  40 mins  cooking:  25 mins  total:  1 hour 5 mins 

**Ingredients** for the pasta 

  * 250 gr of flour 
  * 50 gr of butter 
  * 50 gr of powdered sugar 
  * 7 grams of salt 
  * 1 teaspoon of baking powder 
  * 1 sachets of instant baker's yeast 
  * a coffee of vanilla extract 
  * 1 egg 
  * \+ or- 75 ml of milk 

for the cream pastry: (half of the quantities for me) 
  * 250 ml of milk 
  * 2 egg yolks 
  * 50 gr of sugar 
  * 1 teaspoon of vanilla 
  * 30 gr of cornflour 
  * 100 grs of grapes. 
  * gilding: 
  * 1 egg yolk 
  * 1 tablespoon of milk 
  * 1 little vanilla 



**Realization steps** Prepare the pastry cream: 

  1. Mix the egg yolks, cornflour, sugar and vanilla in a saucepan and whisk well. 
  2. Pour the hot milk over, mix at the same time, and put back on medium heat while stirring constantly with a wooden spoon until the mixture thickens (about 4-5 minutes). 
  3. Allow to cool, covering with a film of food, so that it does not form a thick film. 

Prepare the brioche: 
  1. Mix the flour, sugar, salt, baker's yeast, baking powder, vanilla. 
  2. Add the melted butter, mix with the egg and squeeze well between the hands. 
  3. Add the warm milk to pick up the mixture into a dough ball. 
  4. Knead by hand 15 minutes in a bowl 8 minutes. 
  5. Put the dough in a bowl, cover with cling film and allow to double the volume (a personal tip I preheated the oven at 250 degrees for 1 minute, I turn off the oven and I put my salad bowl. It is very important that the bowl is covered with food film and no clean linen) the dough rises quickly normally after one hour the dough will be ready to shape. 
  6. On a floured worktop lower your dough to 5 mm thick and make a rectangle. 
  7. Spread the cream over the entire surface of the rectangle. 
  8. Spread the raisins on top and roll the dough on itself in the width direction 
  9. It is best to measure the dough roll in the refrigerator for half an hour to make it easy to cut. 
  10. With the help of a bread knife cut the roll of pasta with 11 washers and put in the oiled and floured mold it is very important (put 3 washers in the center and the 8 other washers around leaving some space between they. 
  11. Put in a warm place and allow to double the volume another time. 
  12. brush the surface with an egg yolk mix with a tablespoon of milk. 
  13. bake leather in preheated oven at 180 degrees for 20 to 25 minutes. 
  14. Let cool before unmolding. 



![chinese brioche](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chinois_thumb11.jpg)
