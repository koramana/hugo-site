---
title: my thousand leaves lights (yes, yes)
date: '2008-02-11'
categories:
- Algerian cuisine
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219554781.jpg
---
![miles](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219554781.jpg)

yes, here he does not make the delicious mille feuilles, that I always eat in Algeria, I think they do not even know them. 

so when the urge for a good cake comes to us, well it must have been done. 

the thousand leaves are my favorite cakes, so to let me do a little without forgetting the diet, I prepared them with sweetener, hahahahaha 

as long as we can afford to craft our recipes in our own way, it just has to be great 

go, go to the ingredients: 

  * 1 roll of puff pastry 




for pastry cream: 

  * 1 egg 

  * 1/4 liter of milk 

  * 50g of sugar (36g of sweetener for me) 

  * 35 g of flour 

  * a packet of vanilla sugar 




for icing: 

  * icing sugar (sweetener for me) 
  * a chocolate pie (coconut powder for me) 

prepare the pastry cream:   
Put the milk to boil. Meanwhile, mix in a bowl the egg, sugar, vanilla sugar and flour. When the milk is boiling, immediately pour it into the salad bowl. Put back in the pan for a few minutes so that the liquid becomes the consistency of a cream. 




![milles5](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219555431.jpg)

Roll out the puff pastry about 3 mm. Prick with a fork so that it does not swell and put it in the oven on 6, 180 ° C. 

put it between two trays, so that it does not swell too much. 

![milles6](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219555961.jpg)

to divide the cooked dough into three equal parts 

![milles4](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219556441.jpg)

Spread the pastry cream on a first layer of dough, then do the same with a second. Assemble them. 

![milles3](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219556771.jpg)

Wait until the cake is cold to spread the frosting, made with a lot of icing sugar mixed with a little water. 

![miles2](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219556951.jpg)

Make a drawing with the rest of the frosting, mixed with a slab of melted chocolate, or just like me with a little coconut powder 

![miles distant1](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219557281.jpg)

make a marbled drawing by passing a hill blade one time up and the other down 

![milles9](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219557671.jpg)

put your cake in the refrigerator, and let the cream take well, and the icing dry well, to be able to cut your thousand leaves 

![milles8](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219557881.jpg)

for my part, I have 4 thousand leaves, all crunchy, and siculating for my taste. 

![milles7](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219558161.jpg)

my husband liked it a lot. 

![milles10](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/219558511.jpg)

bonne appétit 
