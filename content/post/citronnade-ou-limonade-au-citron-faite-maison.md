---
title: homemade lemonade or lemonade
date: '2017-04-20'
categories:
- juice and cocktail drinks without alcohol
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/citronnade-ou-limonade-au-citron-fait-maison.jpg
---
##  [ ![lemonade or homemade lemonade](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/citronnade-ou-limonade-au-citron-fait-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/citronnade-ou-limonade-au-citron-fait-maison.jpg>)

##  homemade lemonade or lemonade 

Hello everybody, 

Lemonade or homemade lemonade! The sunny days are beginning to peek, and the sun begins to respond to its warmth, and what's more refreshing with a lemonade, or home-made lemonade flavored with mint ??? 

For me it's a must, refreshing and energizing, this lemonade is also a pleasure to realize, especially when we have pretty lemon tree at home ... ![homemade lemonade or lemonade](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/citronnade.jpg)

I do not speak about me, because unfortunately, I do not have a lemon tree, but I remember very well lemons of my grandparents in Constantine, and especially the lemonade that my grandmother prepared all along the year, it never failed at home, I remember that my grandmother, pressed the lemons after zester, she was drying the lemon peel to perfume are [ croquets ](<https://www.amourdecuisine.fr/article-gateaux-secs-les-croquants-croquets.html>) , where [ halwat tabaa ](<https://www.amourdecuisine.fr/article-halwat-tabaa-bel-jeljlane-sables-aux-grains-de-sesames.html>) and freeze the lemon juice in sachets, or even make lemon syrup, then dilute it in water. Oh that I miss these sunny days ... 

In any case, the lemonade almost never missed at home, finally I thought, because every time we went to visit my grandparents in Constantine, we were entitled to this drink refreshing to accompany the delicious dish of [ chakhchoukha ](<https://www.amourdecuisine.fr/article-chekhchoukha-constantinoise-chakhchoukha-de-constantine.html>) , from [ Tlitli ](<https://www.amourdecuisine.fr/article-tlitli-constantinois.html>) or [ trida ](<https://www.amourdecuisine.fr/article-25345529.html>) , hummmmmmmmmmmmmm .. 

I do not imagine this summer, accompany my homemade lemonade dishes, a little heavy to digest, especially in summer ... 

But I can imagine a big glass of ice-cold lemonade when it's really hot, in the evenings of Ramadan, or after a nap a little light in a hot weather.   


**Lemonade or lemonade with lemon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/limonade-au-citron-faite-maison.jpg)

portions:  6  Prep time:  15 mins  cooking:  5 mins  total:  20 mins 

**Ingredients**

  * 1.5 liters of water 
  * 6 lemons (untreated) 
  * 5 tablespoons of honey 
  * handle of fresh mint 
  * ice cubes 



**Realization steps**

  1. Heat 200 ml of water with honey in a small saucepan until the honey is completely under it. 
  2. squeeze 4 lemons to extract the juice. 
  3. Pour the juice and honey water into a pitcher. Add remaining cold water, fresh mint and 2 lemons cut into pieces 
  4. Refrigerate for 30 to 40 minutes. 
  5. Serve this homemade lemonade with ice cream and enjoy. 



##  [ ![homemade lemonade or lemonade](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/citronnade-faite-maison-ou-limonade-au-citron.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/citronnade-faite-maison-ou-limonade-au-citron.jpg>)
