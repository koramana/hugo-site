---
title: stuffed chicken rolls with curry
date: '2017-05-16'
categories:
- bakery
- Bourek, brick, samoussa, slippers
- pizzas / quiches / pies and sandwiches
tags:
- Easy cooking
- accompaniment
- Ramadan
- inputs
- Algeria
- Ramadan 2016
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/petits-pains-farcis-au-poulet-au-curry-1.jpg
---
![stuffed chicken rolls with curry 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/petits-pains-farcis-au-poulet-au-curry-1.jpg)

##  stuffed chicken rolls with curry 

Hello everybody, 

It's that sort of chicken stuffed buns with curry, or stuffed otherwise than I realize when I'm out of bread at home, and I want to add some pep to the table. 

These stuffed buns are easy to pass and everyone loves them at home, which always makes me happy, because generally, it's not easy to satisfy everyone. But with these rolls, and whatever the form or the joke, the result is the total satisfaction of each and especially me, because after spending easily 2 hours in the kitchen, it's nice to see that the tray is empty on Table. 

my husband always blames me that I do not do much for him to take with him to work, but between us 12 pieces is not much ???? Ah, the greedy ones! 

**stuffed chicken rolls with curry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/petits-pains-farcis-au-poulet-au-curry.jpg)

**Ingredients**

  * 3 cups flour (cup = 250 ml) 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 tablespoon baker's yeast 
  * 1 teaspoon of baking powder 
  * 4 to 5 tablespoons of oil 

for the stuffing: 
  * 1 large onion cut into a thong 
  * 100 gr of chicken breast cut into cubes 
  * 2 cloves garlic grated 
  * chopped coriander 
  * 100 gr of mushrooms 
  * 1 C. teaspoon of curry powder 
  * salt and black pepper 
  * 2 fresh tomatoes cut into cubes. 
  * 3 c. extra virgin olive oil 
  * grated cheese 

for garnish: 
  * 1 egg yolk 
  * a bit of milk 
  * seed of nigella 



**Realization steps**

  1. Mix all these ingredients and add enough lukewarm water until soft, soft and easy to work. 
  2. Knead a few minutes. Form the dough into a ball and let it rise under cover, protected from drafts until it has doubled in volume. 
  3. degas the dough and shape even balls (between 55 and 60 gr) 

preparation of the stuffing: 
  1. sauté the mushrooms in a little oil, drain and set aside. 
  2. in the same oil, sweat the onion, when it gets a nice color, add the chicken cut into cubes. 
  3. fry a little, then add the grated garlic. and season with salt, black pepper and curry. 
  4. add chopped fresh coriander, and near the end crushed tomato. 
  5. let it come back a little, then remove from the heat and add the mushrooms in this little sauce, and let cool. 
  6. take again the dumplings which rested in an oiled tray. and with the bottom of a water glass, press to shape a well. 
  7. trace it well with your fingers to have a large cavity, which you can fill with the chicken stuffing. 
  8. after filling all the rolls, brush the dough with the yellow mixture of water and milk. 
  9. garnish with black seed or sesame seeds according to taste, and garnish stuffing with a little shaping. 
  10. let stand a little, then cook in a pre-heat oven at 180 ° C for almost 20 minutes. 



![stuffed chicken rolls with curry 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/petits-pains-farcis-au-poulet-au-curry-2.jpg)
