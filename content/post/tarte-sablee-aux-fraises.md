---
title: strawberry shortbread pie
date: '2012-03-23'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya
- Gateaux Secs algeriens, petits fours

---
hello everyone, I do not know about you, but here at home it's strawberries at a dream price, and not only in England, apparently even in France, and Lunetoiles did not miss the opportunity to us to do this little delight, I swear, if I had not made a fruit salad for dessert today, I would make this recipe, which really makes you want ... so if your strawberry tray is still in the fridge , do not hesitate to concoct this marvel ...... the ingredients: for 8 people and a pie of 24 cm shortbread dough: 120 gr & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

I do not know about you, but here at home it's the strawberries at a dream price, and not only in England, apparently even in France, and Lunetoiles did not miss the opportunity to make us this little delight, I swear, if I had not made a fruit salad for dessert today, I would make this recipe, which really makes you want ... 

so if your strawberry tray is still in the fridge, do not hesitate to concoct this wonder ...... 

Ingredients: 

for 8 people and a pie of 24 cm 

shortbread : 

  * 120 gr of soft butter cut into pieces 
  * 70 gr of icing sugar 
  * 30 gr of almond powder 
  * 1 pinch of salt 
  * 1 egg 
  * 200 gr of flour 



Almond cream: 

  * 75 gr of soft butter cut into pieces 
  * 75 gr of almond powder 
  * 75 gr of sugar 
  * 5 gr of flour 
  * 2 eggs 



for decoration: 

  * 250 gr of strawberries 
  * 4 c. strawberry jam or jelly 



the stages of realization: 

  1. Mix the butter by adding the icing sugar, then successively the almond powder, the salt, the egg and the flour. 
  2. Form a ball of dough, wrap it in a film of food and let rest at least 2 hours. 
  3. Preheat your oven to 180 ° 
  4. Roll out the dough, put it in your pan and cook for 20 minutes. 
  5. Let cool and reserve. 
  6. In a salad bowl, work in soft butter cream. 
  7. Add the sugar, the almond powder and the flour. Whip to get a homogeneous mixture. 
  8. Relax this preparation with the eggs that you incorporate one by one. 
  9. Pour on the bottom of cooled dough and bake for 15 minutes. 
  10. Let cool completely. 
  11. Cover with strawberries that have been previously cut and cut in half. 
  12. Melt 4 tablespoons strawberry jam in a saucepan and gently coat the fruit. 
  13. Place in the fridge for 1 hour. 



follow me on: 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
