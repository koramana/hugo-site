---
title: tajine hlou varied without meat
date: '2015-06-23'
categories:
- Algerian cuisine
tags:
- '2015'
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-hlou-vari%C3%A9-sans-viande-1.jpg
---
[ ![tajine hlou varied without meat 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-hlou-vari%C3%A9-sans-viande-1.jpg) ](<https://www.amourdecuisine.fr/article-tajine-hlou-varie-sans-viande.html/tajine-hlou-varie-sans-viande-1>)

##  tajine hlou varied without meat 

Hello everybody, 

In Ramadan, tajine hlou is a super important dish to put first on the table ... It must be said that this dish is also majestically its place in the biggest tables. A noble dish, rich in ingredients beneficial to the human body. Ingredients that find a new flavor in this dish while keeping their wealth of vitamins, minerals and fiber. 

The tajine hlou presentations are diverse and varied, one can start from [ lham lhlou ](<https://www.amourdecuisine.fr/article-lham-lahlou-ou-marka-hlowa-ramadan-karim-2014.html>) , the best known as salty sweet tajine in Algeria, to go to different variants all as delicious as the others as: 

[ Chbah essafra ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-chbah-essafra.html>) , [ tajine hlou with apples ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande.html>) (without meat too), [ lham hlou betmar ](<https://www.amourdecuisine.fr/article-lham-hlou-bet-tmar-tajine-hlou-aux-dattes-farcies.html>) , and [ lham lahlou to the cloaks ](<https://www.amourdecuisine.fr/article-tajine-de-nefles-farcies-aux-amandes.html>) . The list is even bigger, and the variations of this dish remain to explore ... For today, it is this tajine hlou varied without meat of **Mr. Bouaskeur** that I share with you, a recipe she spotted at: **Kiko Benakano**   


**tajine hlou varied without meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-hlou-vari%C3%A9-sans-viande-2.jpg)

portions:  4  Prep time:  5 mins  cooking:  20 mins  total:  25 mins 

**Ingredients**

  * a glass of dried prunes 
  * a glass of dried apricots 
  * ½ glass of raisins 
  * 1 nice handful of dates well fleshy 
  * sugar according to taste 
  * Orange tree Flower water 
  * a cinnamon stick 
  * water to water 
  * lemon 



**Realization steps**

  1. start with clean apricots, dried prunes and raisins and let them swell a bit in the water. 
  2. In a saucepan, put the apricots, prunes, raisins, cinnamon stick. (you can do without one of the ingredients according to your taste, and add others, like here, no apricots) 
  3. Cover them with sugar, then sprinkle them with water 
  4. add 3 slices of lemon, a little orange blossom water 
  5. cook for 20 minutes 
  6. towards the end, add the dates, let simmer a few minutes and remove the pan from the heat 
  7. present the dish decorated with grilled almonds. 



[ ![tajine hlou varied without meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-hlou-vari%C3%A9-sans-viande.jpg) ](<https://www.amourdecuisine.fr/article-tajine-hlou-varie-sans-viande.html/tajine-hlou-varie-sans-viande>)
