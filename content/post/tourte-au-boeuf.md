---
title: beef pie
date: '2017-02-08'
categories:
- amuse bouche, tapas, mise en bouche
- cuisine algerienne
- cuisine diverse
- pizzas / quiches / tartes salees et sandwichs
- recette a la viande rouge ( halal)
- recette de ramadan
tags:
- Salty pie
- la France
- Pastry
- Easy cooking
- Fast Food
- inputs
- pies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tourte-au-boeuf-et-pommes-de-terre.jpg
---
[ ![beef pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tourte-au-boeuf-et-pommes-de-terre.jpg) ](<https://www.amourdecuisine.fr/article-tourte-au-boeuf.html/tourte-au-boeuf-et-pommes-de-terre>)

##  beef pie 

Hello everybody, 

That's what I realized with two big steak of beef, that my husband asked me to go to the grill and accompany it with a salad. I preferred to make a beef pie covered with a thin layer of potato slices. My husband was disappointed at first seeing the pie, I explained to him that by this cold I could not grill the meat and open the windows to escape all the cooking smoke. 

But after having tasted the pie, he was quite convinced, he really liked the recipe, and I know that now, when he's going to buy steaks, it's going to be for me to prepare this pie, oh yes, hihihihi 

[ ![beef pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tourte-au-boeuf-1.jpg) ](<https://www.amourdecuisine.fr/article-tourte-au-boeuf.html/tourte-au-boeuf-1>)   


**beef pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tourte-au-boeuf.jpg)

**Ingredients** for the pasta: 

  * 250 gr of flour 
  * 120 gr of butter in pieces 
  * 1 egg yolk 
  * ½ cup of sugar 
  * 1 teaspoon dried parsley 
  * ¼ cup of cumin 
  * ½ teaspoon of salt 
  * 50 ml of cold water (+ or - as needed) 

for the meat stuffing: 
  * 2 steaks medium beef 
  * ½ chopped onion 
  * 1 clove of garlic 
  * 1 handful of frozen vegetables (carrots, but, peas) 
  * salt and black pepper 
  * ¼ cup of cumin 
  * 2 tablespoons of table oil 
  * 1 glass of water 
  * a little gruyère 
  * 2 potatoes or as needed 



**Realization steps** prepare the dough: 

  1. in the bowl of a blender, put the flour and butter in pieces, mix 
  2. add the egg yolk, sugar, salt, parsley and cumin mix again 
  3. add the water in small quantities and mix, until the dough forms a ball (go there gently with the water) 
  4. cover the dough ball with cling film, and leave in the fridge for 30 minutes 
  5. Spread the dough, and go for a quiche pan covered with a piece of cooking paper, prick with a fork.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pate-sal%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pate-sal%C3%A9e.jpg>)
  6. Cook the dough for 10 to 12 minutes 

Prepare the meat: 
  1. Cut the meat into small cubes 
  2. in a skillet, put the meat and chopped onion, add the oil 
  3. cover the pan and let it come back until the meat throws its water (you have to stir from time to time). 
  4. add the vegetables, salt, black pepper and cumin, cover with water and cook, until the meat becomes very tender, and the sauce a little reduced (it is not necessary that there remains much of water.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/viande.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/viande.jpg>)
  5. fill the dough base with this vegetable meat, with just a little of its juice (2 tablespoons more, if there is any juice left, keep it for the end of cooking the pie. 
  6. grated a little gruyère over   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/viande-dans-la-tarte.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/viande-dans-la-tarte.jpg>)
  7. cut the peeled and washed potatoes into a thin slice. 
  8. cover well the top of the pie not to leave the meat visible. 
  9. brush the rings with a little oil, add a little salt and a little black pepper.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pomme-de-terre.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pomme-de-terre.jpg>)
  10. baked in a preheated oven at 180 degrees for 30 minutes, or until the potato is cooked through. 
  11. at the end of the oven, sprinkle the top of the pie with 2 tablespoons of meat cooking juice. 



[ ![beef pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tourte-au-boeuf-et-pommes-de-terre1.jpg) ](<https://www.amourdecuisine.fr/article-tourte-au-boeuf.html/tourte-au-boeuf-et-pommes-de-terre-2>)
