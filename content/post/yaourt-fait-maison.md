---
title: homemade yoghurt
date: '2018-04-26'
categories:
- panna cotta, flan, and yoghurt
- sweet recipes
tags:
- Easy cooking
- Fast Food
- Healthy cuisine
- Kitchen without egg
- Cakes
- verrines
- Sweet Verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/yaourt-fait-maison-1.jpg
---
![homemade yoghurt 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/yaourt-fait-maison-1.jpg)

##  Homemade yoghurt 

Hello everybody, 

On my blog, you will find several yogurt recipes like: [ salted butter caramel yoghurt ](<https://www.amourdecuisine.fr/article-yaourt-au-caramel-au-beurre-sale.html> "Salted butter caramel yoghurt") , [ spicy clementines yoghurt ](<https://www.amourdecuisine.fr/article-yaourt-aux-clementines-epicees.html> "spicy clementine yoghurt") , or [ yogurt with peaches ](<https://www.amourdecuisine.fr/article-yaourt-aux-peches-confiture-de-peches.html> "peach yoghurt \(peach jam\)") . 

So today I share with you the simple and easy recipe of homemade yoghurt ... I added some strawberry jam and apricot jam at the request of my children. But I assure you one thing, that once you go there with home-made yogurts, you will not stop at one try! 

{{< youtube 9tY5z0m7W4M >}} 

**homemade yoghurt**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/yaourt-fait-maison-3.jpg)

portions:  6-7  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 1 L milk. 
  * 1 jar of yogurt. 
  * 3 c. sugar soup. 
  * 3 c. milk powder (for this time I did not put it because I did not have any more) 
  * strawberry jam (optional) 
  * Apricot jam (optional) 



**Realization steps**

  1. Boil half of the milk. 
  2. Mix the remaining milk with the sugar, the milk powder and the yoghurt pot. 
  3. Gradually pour the hot milk over the mixture while stirring. 
  4. You can put jam at the bottom of the jars 
  5. Pour your device into pots and place them in a large rectangular mold. 
  6. Fill the mold with hot (but not hot) water and put everything in the oven, preheated, but turned off (This promotes fermentation) 
  7. and leave the pots there until the yogurt is taken, or 4 to 6 hours. 
  8. Refrigerate. You can keep this homemade yogurt up to two weeks in the refrigerator. 
  9. And it's ready ..! 



Note If you have a yogurt maker like the one on the video, just put the pots in for at least 10 hours. ![homemade yogurt 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/yaourt-fait-maison-2.jpg)
