---
title: Cantonese rice from Louiza
date: '2009-06-16'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/06/33538302_p1.jpg
---
![rice](https://www.amourdecuisine.fr/wp-content/uploads/2009/06/33538302_p1.jpg)

and here is the recipe:   
\- 300g of basmati rice preferably   
\- 3 eggs cooked omlette and cut into small pieces   
\- 150 g cooked and shelled shrimp   
\- 3 tablespoons of oil   
\- 4 small peas   
\- 20g of mushrooms   
\- 1 onion   
cook the rice and rinse with cold water.   
finely chop the onion and sauté it in the oil, add the shrimp in small pieces, the mushrooms in small pieces.   
then add the peas and cook for 3 minutes.   
add the rice salt pepper, add the pieces of eggs   
and 1 cup of soy sauce if you like   
heat 4 minutes and serve hot   
it's a very easy dish that can be made with leftovers, for example a rest of rice   
of proteins ...   
let yourself be guided by your imagination   
if you have a wok, prepare this rice in this one 

![Rice1](https://www.amourdecuisine.fr/wp-content/uploads/2009/06/33538317_p1.jpg) [ ](<http://amour-de-cuisine.com/wp-content/uploads/2009/06/33538317.jpg>)
