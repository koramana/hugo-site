---
title: Cream of beans
date: '2011-01-11'
categories:
- boissons jus et cocktail sans alcool
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/veloute-de-feves-002-1_thumb1.jpg
---
##  Cream of beans 

Hello everyone, 

the cold is back, finally here it is in England, between the wind, the rain, and this cold duck, me it is only a soup with vegetables or a delicious velvet which warms me really well. 

This time, my choice fell on a bean veloute, especially with the little bag of frozen beans, which I bought almost a month ago, because sometimes I like to steam the beans, then season them with cumin and salt, and eat them like that, we call it: "maleh we bnine" is to say "salty and good" 

so today is the recipe for this delicious bean veloute, which I will share with you. 

**Cream of beans**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/veloute-de-feves-002-1_thumb1.jpg)

portions:  2 

**Ingredients**

  * 2 glasses of water of frozen beans (what I had) 
  * 1 small onion 
  * 1 clove of garlic 
  * 1 medium sized potato 
  * ½ cube of chicken broth (jumbo, or knorr) 
  * a little salt (better to add towards the end not too salty) 
  * 2 tablespoons of table oil (olive oil for me) 
  * 1 tablespoon of fresh cream 
  * black pepper 



**Realization steps**

  1. In a saucepan, pour the olive oil 
  2. gently brown the beans with the chopped onion, the garlic cut in half, and the potato, peeled and cut into cubes 
  3. cook until all ingredients are a bit translucent. 
  4. add the half cube of chicken broth 
  5. cover with a little water and cook. 
  6. at the end of cooking. mix your mixture in the bowl bowl of a blinder, or with a mixer foot. 
  7. add the cream, mix again 
  8. if your veloute is a little thick to your taste, add a little boiling water 
  9. serve this delicious soup, decorate with a drizzle of olive oil, and a little spice (for me it was a mixture of black pepper, red and yellow) 


