---
title: Blueberry financiers
date: '2013-04-03'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/financier-aux-myrtilles-041.CR2_1.jpg
---
![Financial-to-blueberry-041.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/financier-aux-myrtilles-041.CR2_1.jpg)

Hello everybody, 

Another recipe with blueberries, this time it's financial mellow, and too good, with these blueberries, which have exploded well, and melted in the heart of blueberries. 

Ingredients: 

  * 70 g of almond powder 
  * 4 egg whites 
  * 100 g of sugar 
  * 40 g of flour 
  * 70 g of butter 
  * 1 pinch of baking powder 
  * 60 g of blueberries (frozen or fresh) 



![Financial-to-blueberry-016.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/financier-aux-myrtilles-016.CR2_1.jpg)

method of preparation: 

  1. Melt the butter until it turns a nutty color. Book. 
  2. Mix the sugar, the almond powder, the flour, the yeast. 
  3. Add the egg whites, mix until a homogeneous mixture is obtained. 
  4. Add the butter in 3 times while incorporating it completely into the dough each time. 
  5. defrost blueberries if you use frozen blueberries. 
  6. Pour the dough in your mold to the financiers, evenly distribute the blueberries on the dough without mixing the two elements. 
  7. Bake for 25 minutes at 180 ° c. 



![Financial-to-blueberry-043.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/financier-aux-myrtilles-043.CR2_1.jpg)
