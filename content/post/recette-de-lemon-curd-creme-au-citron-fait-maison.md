---
title: recipe of homemade lemon curd / lemon cream
date: '2018-03-27'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/lemon-curd-fait-maison-014.CR2_thumb1.jpg
---
recipe of homemade lemon curd / lemon cream 

Hello everybody, 

recipe lemon curd / homemade lemon cream, creamy cream always present at home, because my husband loves a lot. 

When I first arrived in England, there were always jars of lemon curd in the refrigerator, which my husband bought to garnish tartlets, and prepared his lemon pie, in his own way, quickly ... well fact. 

To be honest I did not even know what was this lemon curd, until the day, when I asked unont "google": what is Lemon Curd ??? and he was so nice in sending me to Aunt Wikipedia, who explained to me that Lemon Curd is a spread or a dessert topping, usually lemon. 

lemon creams ( _lemon curds_ ) homemade were traditionally served with bread or scones, with tea for tea, as an alternative to jams, and also as a garnish for cakes, pastries and pies. They were usually prepared in small quantities because they do not keep as well as jams, but this difficulty is partially overcome by the use of domestic refrigeration. Industrial creams contain preservatives and thickeners. 

another search took me to a site that gave the recipe, which I adopted "forever":   


**recipe of homemade lemon curd / lemon cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/lemon-curd-fait-maison-014.CR2_thumb1.jpg)

Recipe type:  spreadable cream  portions:  4  Prep time:  20 mins  cooking:  5 mins  total:  25 mins 

**Ingredients**

  * the juice and zest of 2 lemons 
  * 120 g of sugar (you can put more according to your taste) 
  * 75 g of butter 
  * 3 eggs 
  * 1 tablespoon cornflour 



**Realization steps**

  1. beat with a whisk, the eggs with the sugar until whitening. 
  2. Add lemon juice, lemon peel and cornflour, diluted in a little water. 
  3. In a saucepan, pour the mixture. Add the butter in small pieces. 
  4. Stir the whole with a wooden spoon, over low heat, between 4 and 5 minutes, until thick, 
  5. at the first boil then remove from heat, let cool 
  6. keep in glass jars, in the fridge for about a week in the refrigerator. 


