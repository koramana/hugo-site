---
title: olive breads, Algerian cuisine
date: '2014-06-10'
categories:
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-aux-olives-011_thumb.jpg
---
[ ![olive bread 011 Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-aux-olives-011_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-aux-olives-011.jpg>)

##  olive breads, Algerian cuisine 

Hello everybody, 

To taste a good dish, a good soup or just to accompany a salad, there is no more delicious than homemade bread. 

Fragrance your homemade bread is always nice, and add one more ingredient will only improve the taste of bread, as these olive breads, you want to eat in it without stopping to find this taste well appreciated olives. 

[ ![olive bread, olive bread](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-aux-olives-012_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-aux-olives-012.jpg>)   


**olive breads, Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-aux-olives-011_thumb.jpg)

Recipe type:  bread  portions:  12  Prep time:  40 mins  cooking:  25 mins  total:  1 hour 5 mins 

**Ingredients**

  * 450 gr of flour 
  * 150 gr of semolina 
  * 1 teaspoon of salt 
  * 1 tablespoon yeast baker 
  * 2 tablespoons milk powder 
  * 1 tablespoon of sugar 
  * 4 tablespoons of table oil or extra virgin olive oil 
  * a nice handful of pitted green olives 
  * warm water (between 280 and 300 ml) everything depends on dry ingredients. 
  * 1 tablespoon of black seed 
  * 1 egg yolk for decoration 
  * 1 tablespoon of milk for decoration 



**Realization steps**

  1. Mix the flour, semolina and salt. 
  2. Stir in the oil and sand well between your hands. 
  3. Add yeast, black seed, milk powder, and sugar. 
  4. add warm water and knead to have a smooth and malleable paste.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-maison-pain-aux-olives_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-maison-pain-aux-olives_thumb1.jpg>)
  5. place in a large salad bowl a little oiled and let double volume. 
  6. Degas a little and add the olives (I had to cut the olives in two, because my children do not like large pieces, I left only a few whole pieces). 
  7. cut the dough into 12 equal pieces 
  8. work on a surface a little floured to form your rolls 
  9. let shelter from drafts. 
  10. when the breads swell well, turn the oven on at 180 degrees C 
  11. brush the bread with the egg yolk and lai mixture, 
  12. make nicks with a sharp knife. 
  13. Put in leather for 20 minutes or according to your oven. 



[ ![olive bread 008, Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-aux-olives-008_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pain-aux-olives-008.jpg>)
