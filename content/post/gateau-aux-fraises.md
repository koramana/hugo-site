---
title: Strawberry cake
date: '2015-05-16'
categories:
- gateaux, et cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/barres-aux-fraises-crumble-de-fraises_thumb.jpg
---
#  [ ![strawberry bars - strawberry crumble](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/barres-aux-fraises-crumble-de-fraises_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/barres-aux-fraises-crumble-de-fraises_2.jpg>)

Hello everybody, 

And here is a delicious strawberry cake that is worth a try ... The strawberry season is coming up, so let's take advantage of this short period to enjoy mouth-watering recipes based on this fruit.  Strawberry cake 

What I can say however, that we here in England, we have this fruit all year long, and at very affordable prices ... it's great at once, because with children I make a lot of strawberry recipe ....  Strawberry cake 

the easiest recipe is the strawberry smoothie, and with the kids, have made this creamy drink different every time. 

We come back to this delicious cake of strawberries, which is a recipe of Lunetoiles, which has the base is a good cake with butter and strawberries, decorated with a nice layer of crumble ...  Strawberry cake 

[ ![strawberry crumble](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/crumble-de-fraises_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/crumble-de-fraises_2.jpg>)   


**Strawberry cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/barres-aux-fraises-crumble-de-fraises_thumb-290x195.jpg)

Recipe type:  Dessert  portions:  16  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients** for the streusel: 

  * 115 g unsalted butter at room temperature 
  * 150 gr of brown sugar 
  * ¼ c. salt 
  * 165 gr all-purpose flour 

for the bars: 
  * 500 gr strawberries hulled and sliced ​​¼ thick 
  * 2 tbsp. to S. of light brown sugar 
  * 170 gr of flour 
  * 1 sachet of baking powder 
  * ½ c. salt 
  * 170 gr unsalted butter, at room temperature 
  * 225 gr of powdered sugar 
  * 3 big eggs 
  * 1 C. pure vanilla extract 



**Realization steps**

  1. Preheat the oven to 180 ° C. Butter a 22 cm square pan and line it with parchment paper, leaving an excess of 2 cm by 2 cm of sides exceeded. Set aside. 

for the streusel: 
  1. Whisk together butter, brown sugar and salt. Add the flour and mix with a fork until you get large crumbs. Refrigerate until ready to serve.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/barres-de-fraises-crumble-de-fraises_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/barres-de-fraises-crumble-de-fraises_2.jpg>)

for the bars: 
  1. In a medium bowl, combine strawberries, brown sugar, and 30 g flour. In another medium bowl, whisk remaining flour, baking powder and salt. In a large bowl, using an electric mixer, beat butter and powdered sugar until light. Beat the eggs, one at a time. With the low speed beater, beat with vanilla, then the flour mixture. Spread the dough into the prepared pan. Garnish with strawberries, then cover with prepared streusel. 
  2. Cook until browned and a toothpick inserted in the center comes out with a little moist crumbs attached, about 50 to 55 minutes. Cool completely in the mold. Using the excess parchment paper, lift the cake from the mold. Cut into bars and sprinkle with icing sugar. Serve as is, or with a little whipped cream. 



[ ![strawberry crumble 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/crumble-de-fraises-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/crumble-de-fraises-2_2.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
