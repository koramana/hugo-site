---
title: Strawberry tart with Nawel muslin cream
date: '2009-03-30'
categories:
- dessert, crumbles and bars
- birthday cake, party, celebrations
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/03/257642431.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2009/03/257642431.jpg)

yes, you will say yet another recipe, from another person, oh yes ...... you have to take a habit now, because a blog is made for sharing, and it's not that it's because it's is my blog, that I only have to publish my recipes, I'm not a chef, who can always make a new recipe, I'm a mom, with a husband who is difficult to eat what he does not know as recipe (what he likes the traditional my husband) a non-greedy child, to make him swallow the frits like other children, I must go out of their way. 

and so, a blog is a blog sharing, and I'm happy to share with you, the recipes of my dear friends, who they share their recipes with me (for what you privez ????) 

and here is the recipe as she wrote it to me: 

**Dough:**

250g flour 

125g butter 

1 egg 

2 cases of sugar 

1 pinch of salt 

1 case of milk (if you need to pick up the dough) 

mix in a bowl or salad bowl the solids after you add the butter and an egg and milk that will help pick up the dough. 

Let the dough rest in the refrigerator (1h) 

**Chiffon cream:**

1/2 l of milk 

sugar (according to taste 4 cases) 

4 cases of cornstarch 

2 egg yolks 

1 box Whole Liquid Cream mounted in whipped cream 

50 gr of butter 

dilute the cornflour in the milk, add the egg yolks and the caster sugar, mix 

put on low heat and stir until thickened then add the butter off the heat mix energetically 

let cool 

place the liquid cream in whipped cream (with a little sugar) and chill 

when the custard is completely cold, whip it with an electric mixer and stir in whipped cream 

étalez la crème sur la tarte. décorez a votre guise. 
