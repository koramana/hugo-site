---
title: easy greek moussaka
date: '2017-07-08'
categories:
- Algerian cuisine
- diverse cuisine
- Dishes and salty recipes
- recipe for red meat (halal)
tags:
- eggplant
- Algeria
- Morocco
- Ramadan
- Bolognese

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/moussaka-grecque-facile.jpg
---
![moussaka, Greek-easy](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/moussaka-grecque-facile.jpg)

##  easy greek moussaka 

Hello everyone, 08 

yum yum an easy Greek moussaka that comes out of the oven! I could hardly do these photos for you, especially the nighttime photos that I do not really like to take, but my children really like moussaka, that's why I often prepare this dish the evening. 

The Greek moussaka is just to die for, and I really like this way of baking slices of eggplant just brushing with a thin layer of oil and grilled in a pan or on a plancha for a super good moussaka, not too much oily, just a treat! 

**easy greek moussaka**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/moussaka-grecque-facile-1.jpg)

portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 2 eggplants 
  * 600 gr of tomatoes 
  * 1 small stick of cinnamon 
  * 1 pinch of chilli powder 
  * 450 gr of minced meat 
  * 1 onion 
  * 2 cloves garlic 
  * olive oil 
  * salt pepper 
  * 2 bay leaves 

for the white sauce: 
  * 2 tbsp. half-salted butter 
  * 500 ml whole milk 
  * 3 c. flour 
  * 2 medium eggs 
  * salt pepper 
  * nutmeg 



**Realization steps**

  1. wash and cut the tomatoes into small cubes (without their seeds) 
  2. place the tomato in a pan, add a little drizzle of olive oil and let the tomato reduce in sauce without burning.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tomate.jpg)
  3. towards the end of the cooking add a little salt and black pepper. Book. 
  4. wash eggplants and slice almost 1 cm 
  5. brush them with olive oil on both sides and cook in a frying pan or griddle   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/aubergine.jpg)
  6. remove the slices of eggplant and place on paper towels and salt them. 
  7. cut the onion into small pieces and fry with a little olive oil.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/oignon.jpg)
  8. Now make the meat come back in a little oil with a little chopped garlic. 
  9. when the meat turns a beautiful color, add the bay leaves, the cinnamon stick, and a little salt. 
  10. add the tomato sauce and the already cooked onion, let it come back and reduce the sauce, remove at the end of the cooking the bay leaves and the cinnamon.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/facebook2.jpg)

Now proceed to mount the mossaka 
  1. Put a layer of grilled eggplant slices in a large gratin dish. 
  2. Add a good layer of meat and cover with eggplant.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/20161014_112502.jpg)
  3. Preparation of the white sauce: 
  4. place the cooking butter in a saucepan. Add the flour and mix well. 
  5. Add the milk very gradually, mixing to avoid lumps. 
  6. Season with salt and pepper and add a little nutmeg. 
  7. Out of the fire, add the beaten egg omelette, mixing well.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/montage-mossaka.jpg)
  8. Pour the sauce on the last layer of eggplant and bake at 190 ° C until a nice color (20 to 30 minutes). 



![moussaka, Greek Easy-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/moussaka-grecque-facile-2.jpg)
