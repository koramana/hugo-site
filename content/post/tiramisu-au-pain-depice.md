---
title: tiramisu with gingerbread
date: '2013-12-30'
categories:
- diverse cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tiramisu-noix-pain-d-epice-028_thumb1.jpg
---
##  tiramisu with gingerbread 

Hello everybody, 

The tiramisu .... I can not stop to say how much I like this dessert, on my blog you will find the classic tiramisu, the [ tiramisu light or tiramisu diet ](<https://www.amourdecuisine.fr/article-25345349.html>) , the [ tiramisu without eggs ](<https://www.amourdecuisine.fr/article-tiramisu-vous-ai-je-dit-a-quel-point-je-l-aime--44008438.html>) , of [ tiramisu with homemade mascarpone ](<https://www.amourdecuisine.fr/article-tiramisu-avec-du-mascarpone-fait-maison-55272756.html>) , [ chocolate tiramisu ](<https://www.amourdecuisine.fr/article-tiramisu-au-chocolat-ou-ganache-chocolat-mascarpone-62045810.html>) .... 

So I like it with all the sauces, and today it's going to be a tiramisu with some [ homemade gingerbread ](<https://www.amourdecuisine.fr/article-pain-d-epice-tres-moelleux-et-trop-bon-49778567.html>) . 

**tiramisu with gingerbread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tiramisu-noix-pain-d-epice-028_thumb1.jpg)

Recipe type:  dessert, verrines  portions:  6  Prep time:  20 mins  cooking:  1 min  total:  21 mins 

**Ingredients**

  * 400 grs of mascarpone 
  * 4 eggs 
  * 150 grams of sugar 
  * 200 grams of nuts 
  * 13 slices of gingerbread 
  * 1 big cup of the strong 



**Realization steps**

  1. crush the nuts roughly, and grill them in a pan with 20 grs of sugar, let cool 
  2. beat the rest of the sugar with the egg yolks, until the mixture whitens. 
  3. add the mascarpone and whip 
  4. turn the egg whites into firm snow. incorporate the delicately mixed precident 
  5. remove crusts of gingerbread cut in medium size, leave a slice for decoration 
  6. soak the gingerbread pieces of the strong 
  7. put a layer of gingerbread in your verrines 
  8. cover with a layer of the mascarpone preparation 
  9. place another layer of gingerbread, and some toasted walnuts 
  10. cover with the preparation 
  11. decorate with crumbs of gingerbread, and the rest of grated walnuts 
  12. now we come to the most difficult part ... book in the fridge for 4 hours before enjoying, and I know who will not resist .... 



the video recipe: 

{{< youtube MtvA0QLDY_Q >}} 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
