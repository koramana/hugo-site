---
title: Baked Salmon with Sweet Spice Butter
date: '2015-08-28'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/saumon-au-beurre-doux-epice_thumb-300x224.jpg
---
##  Baked Salmon with Sweet Spice Butter 

Hello everybody, 

I love salmon a lot, and since my diet, I eat it almost every two days and so I have to vary not to take the routine, and here is a very good recipe that I find in one of my books, too good, of course the recipe was originally with white wine, I put white vinegar instead. 

**Baked Salmon with Sweet Spice Butter**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/saumon-au-beurre-doux-epice_thumb-300x224.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 4 servings of salmon 
  * 2 glasses of white vinegar 
  * 100 g of butter 
  * 1 teaspoon of curry 
  * 1 teaspoon of turmeric 
  * ½ teaspoon of ginger 
  * ½ teaspoon coriander powder 
  * 1 tablespoon acacia honey 
  * coarse salt pepper 



**Realization steps**

  1. Remove the butter from the fridge in advance. When it starts to soften, work it with a fork so that it is flexible. 
  2. Mix the spices and add them to the butter by thoroughly mixing all the ingredients. 
  3. Preheat the oven to 210 ° C (thermostat 7). 
  4. Place the salmon portions in a baking dish and pour in the white vinegar. 
  5. Spread salmon with spice butter. 
  6. bake for about 20 minutes. 
  7. After cooking, remove the dish from the oven. 
  8. Keep the salmon warm on the serving platter; pour the cooking juices into a small saucepan, 
  9. add honey and vinegar and reduce by half on high heat. 
  10. Pour this sauce over the fish. 
  11. Sprinkle with coarse salt and crushed pepper from the grinder 



![2012-02-10 gratin of crepes florentines1](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/2012-02-10-gratin-de-crepes-florentines1_thumb1.jpg)

I served these beautiful slices of salmon with [ beetroot tartare with goat cheese ](<https://www.amourdecuisine.fr/article-tartare-de-betteraves-rouges-au-fromage-de-chevre-99710160.html>) , 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
