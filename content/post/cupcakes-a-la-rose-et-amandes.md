---
title: cupcakes with rose and almonds
date: '2012-03-16'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/cupcake-a-la-rose-005_thumb1.jpg
---
##  cupcakes with rose and almonds 

Hello everybody, 

a bouquet of flowers for the beginning of spring, but please a consumable bouquet, hihihiih, yes my friends, here is a sublime recipe of cupcakes with rose water, that I prepared for the day of the day of the woman has My son's school, and it was a great success, the cupcakes were very very mellow, a real treat, and for the cream, I used the butter cream with Swiss meringue, a real delight this cream. 

a recipe that I advise you, a hit, but when it comes from a well-known site, you have to wait for a chef's result. 

I changed a little in the ingredients, so I recipe you as I did: 

**cupcakes with rose and almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/cupcake-a-la-rose-005_thumb1.jpg)

portions:  12  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 160 g of sugar 
  * 2 eggs 
  * 90 g melted butter 
  * 100g of flour 
  * 50 g crushed flaked almonds 
  * 20 g of almond powder 
  * 1 teaspoon of baking powder 
  * 80 ml of milk 
  * 2 tbsp. orange blossom water 
  * ½ c. cinnamon powder 
  * 1 pinch of salt 
  * [ Swiss meringue butter cream ](<https://www.amourdecuisine.fr/article-creme-au-beurre-meringuee.html>) (using 35 gr of egg white, and adapt the other measures) 



**Realization steps**

  1. Mix the flour, almond powder and chopped almonds, yeast, cinnamon, salt. 
  2. Whip the sugar with the eggs 
  3. add the melted butter. 
  4. Stir in the dry mixture in small quantities, alternating with the milk and the orange blossom water, until obtaining a homogeneous paste. 
  5. Fill au with 12 cupcakes and put in the oven for 20 minutes at 180 ° C (th.6). 
  6. after cooling the cupcakes, decorate them with the butter cream according to your taste, here it is using a special foliage sleeve, and depositing the cream in a small pile, starting with the cupcakes, then each time we end a circle, we come closer to the middle, until the cupcake is well decorated. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
