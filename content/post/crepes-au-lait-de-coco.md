---
title: pancakes with coconut milk
date: '2014-02-15'
categories:
- dessert, crumbles and bars
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-au-lait-de-coco-037.CR2_thumb.jpg
---
[ ![crepes with coconut milk 037.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-au-lait-de-coco-037.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-au-lait-de-coco-037.CR2_2.jpg>)

##  pancakes with coconut milk 

Hello everybody, 

pancakes with coconut milk all soft, and very rich in taste that I prepared yesterday, to make another recipe, which I will publish very quickly for you .. 

But in the end, I had to redo them, because my children enjoy them as they were, without jam, or Nutella, yes even with a little honey, they liked this sweet, and taste very pronounced delicious coconut milk… 

and I was very happy with these pretty pancake stoves that I bought at Ikea, of all sizes, I even made pretty pancake, and for the first time they were all the same size, hihihihi ... .a our pans, and hihihi kitchen scale 

[ ![crepes with coconut milk.](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-au-lait-de-coco.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-au-lait-de-coco.CR2_2.jpg>)   


**pancakes with coconut milk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-au-lait-de-coco-009.CR2_thumb.jpg)

Recipe type:  Dessert  portions:  8  Prep time:  15 mins  cooking:  5 mins  total:  20 mins 

**Ingredients**

  * 2 eggs 
  * 1 glass of 220 ml of coconut milk 
  * 120 ml of milk 
  * ½ teaspoon Vanilla extract 
  * 150 gr of flour 
  * 15 gr of sugar 
  * 2 tbsp. Yeast 
  * 1 pinch of salt 
  * 1 little water if you judge the dough is very thick. 



**Realization steps**

  1. sift the flour, place in a large salad bowl 
  2. add salt, sugar, yeast and vanilla 
  3. make a well and break the eggs. 
  4. Mix starting at the center and gradually go to the edges of the bowl so as not to have lumps of flour. 
  5. Slowly add the coconut milk and milk mixture while still beating. 
  6. Cook in the pan, if the dough is too thick, add a little water. 



[ ![crepes with coconut milk](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-au-lait-de-coco_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-au-lait-de-coco_2.jpg>)
