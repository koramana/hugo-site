---
title: Sweet pie dough
date: '2013-05-16'
categories:
- bakery
- Algerian cuisine
- Cuisine by country
- Unclassified
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pate-a-tarte-sucree-0011.jpg
---
![pate-a-pie-sweetened-001.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pate-a-tarte-sucree-0011.jpg)

Hello everybody, 

Yet another basic recipe, to make pies that will delight your family and friends. This sweet pie is a recipe that our dear Lunetoiles used in many of these achievements. 

A super easy recipe, which gives a very good result. 

**Preparation time: 10 min** **Cooking time: 01 min**

Ingredients: 

  * 250g of flour 
  * 90 g of sugar 
  * 100 g of butter 
  * 4 egg yolks 

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/trans1.gif)

method of preparation: 

  1. Put the flour and sifted icing sugar in a bowl. 
  2. Add the butter in the ointment and squeeze it. 
  3. When there are no more large pieces of butter, stir in the egg yolks. 
  4. Reserve cold for 30 minutes. 
  5. Lower the dough to 3 mm thick on a floured worktop and go for a circle 28 cm in diameter. 
  6. Pass around the edge with the roller to drop the excess dough. 



![pate-a-pie-sucree.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pate-a-tarte-sucree1.jpg)
