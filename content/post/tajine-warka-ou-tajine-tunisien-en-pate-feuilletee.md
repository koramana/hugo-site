---
title: tajine warka, or Tunisian tagine in flaky pastry
date: '2014-10-24'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- diverse cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- pizzas / quiches / pies and sandwiches
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe
tags:
- Full Dish
- Lamb tagine
- Aperitif
- Cocktail dinner

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute-1.jpg
---
[ ![Tunisian tajine in crust 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute-1.jpg>)

##  tajine warka, or Tunisian tajine puff pastry 

Hello everybody, 

This is one of the most delicious Tunisian tajines that you can taste in your life. This tajine warka, or Tunisian tajine puff pastry is just to fall. A recipe that we had to eat several times during Ramadan, but that I did not have time to publish. 

I admit that I almost forgot about this recipe, and I forgot that I did not publish it, until my daughter asked me yesterday, she did not remember her name, and she is went up the picture on my computer ... 

Yes, yes, my kids are very familiar with the folders and files they should not touch on Mom's PC. They are always going to draw on Paint, and save their masterpieces, but they know that you should not delete a photo without telling mom. 

And when she showed me the picture, I noticed that the pictures of this Tunisian tajine crust are not famous. A little click in the head !!!, I went to see on my blog, and I saw that I never published the recipe. Surely because I was preparing for my trip to Algeria. Finally, it's never too late to do it right. 

A recipe that I ate when I was little and lived in Drean, a small town in the El Taref wilaya, which happens to be a border town of Tunisia. Our neighbor at the time was doing all the time, of course she was preparing the homemade dough, not automatically puff pastry as I did in my recipe, something that I often do when we want to enjoy this tajine, and that I do not have time to prepare the dough. 

[ ![Tunisian tajine in crust](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute.jpg>)

the stuffing is very easy to make, and you can always put the ingredients of your choice in it, but I will still give you the recipe for this delicious stuffing with minced meat.   


**tajine warka, or Tunisian tagine in flaky pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute-002.jpg)

portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * a roll of puff pastry 
  * 350 gr of minced meat 
  * 4 tablespoons rice cooked in salt water 
  * 1 chopped onion 
  * 1 grated carrot 
  * 3 tablespoons grated cheese (red cheese for me) 
  * 3 portions of cow cheese that laughs 
  * 3 eggs 
  * salt, black pepper, coriander powder, garlic powder 
  * a few sprigs of chopped parsley 
  * olive oil 

decoration: 
  * egg-milk mixture 
  * seeds of nigelles 
  * some green olives. 



**Realization steps**

  1. In a skillet with a bottom of olive oil, fry the meat with the chopped onion. 
  2. add the grated carrot, stir to introduce everything, and crush the minced meat. 
  3. Salt, pepper and season, 
  4. add the chopped parsley and cook until all the sauce is reduced, then let cool. 
  5. Then add to the meat: the rice, the grated cheese, the cubed cheese cut into cubes, and the 3 eggs beaten a little. 
  6. Arrange the stuffing in the center of the puff pastry, and cover with the sides.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute2.jpg>)
  7. you can of course make the decoration of your choice.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute-5.jpg>)
  8. Brush the top with a mixture of egg and milk, decorate with nigella seeds, green olives ... 
  9. Bake at 160 ° for 40 minutes, 



Note The puff pastry cooks over medium heat so that it does not become pasty, give it time to inflate.   
Brush the puff pastry at high temperature, so that it is quickly colored on the surface, but remains pasty on the inside.   
The cooking time can be less than 40 minutes, just the time that the pasta thighs well. [ ![Tunisian tajine in crust-001](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute-001.jpg>)
