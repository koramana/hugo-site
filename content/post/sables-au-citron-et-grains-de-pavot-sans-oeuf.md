---
title: Lemon shortbread and poppy seeds without egg
date: '2017-06-16'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- sweet recipes
tags:
- Algerian cakes
- Oriental pastry
- Algerian patisserie
- Cookies
- Shortbread cookies
- biscuits
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/sabl%C3%A9-au-citron-et-grains-de-pavot-sans-oeuf-2.jpg
---
[ ![shortbread with lemon and poppy seeds without egg 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/sabl%C3%A9-au-citron-et-grains-de-pavot-sans-oeuf-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/sabl%C3%A9-au-citron-et-grains-de-pavot-sans-oeuf-2.jpg>)

##  Lemon shortbread and poppy seeds without egg 

Hello everybody, 

To taste it today, I prefer to make shortbread with lemon (lemon curd) and poppy seeds, without egg, ie shortbreads, like that, I would not have to break my head for at least 3 or 4 days, to reflect to a recipe for the taste. 

These shortbreads are to fall, easy to make, without eggs, of course you must plan to make the lemon cream, [ lemon curd ](<https://www.amourdecuisine.fr/article-lemon-curd-creme-au-citron-fait-maison.html>) in advance, otherwise you can opt for a jam a little acid, or a lemon marmalade, it will give a flawless result.   


**Lemon shortbread and poppy seeds without egg**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/sabl%C3%A9-au-citron-et-grains-de-pavot-sans-oeuf_.jpg)

portions:  30  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients** for + or- 30 pieces depending on the shape of your mold has sanded: 

  * 340 gr of butter 
  * 100 gr of crystallized sugar 
  * 80 gr of icing sugar 
  * 1 cup of vanilla coffee 
  * 520 gr of flour (+ or - depending on the quality of the flour) 
  * 1 pinch of salt 
  * 2 tablespoons poppy seeds 
  * decoration: 
  * [ Lemon curd ](<https://www.amourdecuisine.fr/article-lemon-curd-creme-au-citron-fait-maison.html>)
  * icing sugar 



**Realization steps**

  1. Preheat the oven to 180 degrees C. 
  2. Place the parchment paper over 2 cookie sheets and set aside. 
  3. In a mess, whip the butter, sugar, vanilla sugar and icing sugar at medium speed until it gives a well-aerated cream 
  4. Introduce the flour and salt in a small quantity, when the dough begins to form, add the poppy seeds, and collect with the rest of the flour. 
  5. spread the dough (not all the dough you can divide into two balls) between 2 large sheets of parchment paper to prevent it from sticking to the roll, or to the work surface. 
  6. you can spread it to 0.5 cm thick. 
  7. form shortbreads and arrange them as you go on the oven-baking molds. 
  8. Bake for about 10-12 minutes or until lightly browned around the edges. (they must not be golden or have too much color) 
  9. Let the shortbreads cool completely on a wire rack. 
  10. pick up two pieces with lemon curd. 
  11. Sprinkle the top of the shortbread with the icing sugar. and let it dry and stick well before eating. 



[ ![shortbread with lemon and poppy seeds without egg](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/sabl%C3%A9-au-citron-et-grains-de-pavot-sans-oeuf.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/sabl%C3%A9-au-citron-et-grains-de-pavot-sans-oeuf.jpg>)
