---
title: Pizza queen with merguez video
date: '2014-09-01'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pizza-aux-merguez1.jpg
---
#  ![pizza-to-merguez.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pizza-aux-merguez1.jpg)

##  Pizza queen with merguez video 

Hello everybody, 

Who wants a pizza? a delicious pizza queen merguez, a delight I do not tell you ... 

a recipe very easy to achieve especially using the magic paste ... 

When the filling, it is according to your choice, and your means ...   


**Pizza queen with merguez video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/pizza-aux-merguez-avec-la-pate-magique-300x200.jpg)

Recipe type:  pizza  portions:  8  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * [ magic pasta ](<https://www.amourdecuisine.fr/article-pate-magique.html> "Magic paste")
  * tomato sauce 
  * merguez 
  * green pepper 
  * mushrooms 
  * pitted black olives 
  * pitted green olives 
  * Mozzarella 

Tomato sauce: 
  * 4 to 5 fresh tomatoes, if not two boxes of cut tomatoes. 
  * 2 cloves garlic 
  * oregano and herbs from Provence 
  * salt, black pepper, and thyme 
  * 2 tablespoons of olive oil. 



**Realization steps**

  1. prepare the magic dough, by hand or at the bread machine. 
  2. prepare the tomato sauce, placing all the ingredients in a pot and cook until reduced. 
  3. grill the merguez just a little ... so that it will continue to cook after on the pizza. 
  4. cut the green pepper into slices. 
  5. cut the mushrooms into strips. 
  6. degas the pizza dough, and spread it in a circle on a flour area. 
  7. garnish with the chilled tomato sauce. 
  8. then decorate with pepper slices, mushroom slices, olives and grated mozzarella cheese. 
  9. cook the pizza in a preheated oven at 180 degrees C for 15 to 20 min. 
  10. present this hot pizza, and treat yourself. 



**Video** of preparation: 
