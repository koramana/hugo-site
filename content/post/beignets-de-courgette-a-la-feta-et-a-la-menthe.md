---
title: Zucchini fritters with feta and mint
date: '2014-03-18'
categories:
- tips and tricks

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/beignets-courgette-feta-et-menthe_thumb.jpg
---
[ ![zucchini, feta and mint donuts](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/beignets-courgette-feta-et-menthe_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/beignets-courgette-feta-et-menthe.jpg>)

Hello everybody, 

a really tasty recipe, you can not imagine the fondant taste of zucchini and feta cheese, under a beautiful crunchy and soft layer at the same time of the layer of donuts. 

when the flavors, it will really burst all your taste buds, a recipe that I invite you to do without hesitation, because not only it is good, but also very easy to do.    


**Zucchini fritters with feta and mint**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/beignets-de-courgettes-feta-et-menthe_thumb1-300x224.jpg)

portions:  12  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * Ingredients (for 12 donuts): 
  * 2 zucchini not too big 
  * 100 g of feta 
  * 12 fresh mint leaves 
  * 200 g flour 
  * ½ sachet of baking powder 
  * 1 egg 
  * 220 ml of milk 
  * salt pepper 
  * 12 wooden skewers 
  * oil for cooking 



**Realization steps** Donuts Spread : 

  1. In a salad bowl, place the flour, yeast, a pinch of salt, pepper and mix. 
  2. Add the egg and milk gently to avoid lumps, until the meat becomes creamy, not too liquid and thick enough to coat the zucchini. 
  3. Allow time to prepare the stuffed zucchini. 

Preparation of zucchini: 
  1. Using a thrifty cut zucchini in the direction of the length in thin slices. 
  2. Spread on a plate and salt slightly. 
  3. Cut the diced feta about 2 cm apart, then put a mint leaf on each feta dice. 
  4. Wrap each piece of cheese in a slice of zucchini and prick with a skewer gently. 
  5. Heat the oil bath in a frying pan. 
  6. Dip each roll of zucchini into the donut dough, and brown in warm oil for about 3 minutes. 
  7. Serve hot. 



[ ![zucchini fritters, feta and mint](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/beignet-de-courgettes-feta-et-menthe_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/beignet-de-courgettes-feta-et-menthe_2.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter. 
