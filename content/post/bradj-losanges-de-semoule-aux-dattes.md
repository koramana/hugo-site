---
title: Bradj semolina lozenges with dates
date: '2018-02-25'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/lbradj_2.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/lbradj_2.jpg)

##  Bradj semolina lozenges with dates 

Hello everybody, 

Bradj Semolina lozenges with dates: A delight of Eastern Algerian cuisine, a mix of crispy and melting semolina pasta that generously covers a nice, spicy stuffing of date paste. 

known as Bradj, or Mbardja, these dates semolina lozenges usually prepared in early spring or more particularly on February 28/29. 

[ ![bradjs 021a](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradjs-021a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradjs-021a.jpg>)

the bradj, Mbardja, lbradj, and I do not know if there are any other nominations, are magnificent shortbread cookies, stuffed with date paste well flavored with cinnamon powder, and powder of clove, a delight, I do not tell you and it makes its success. 

the video of these bradjs: 

{{< youtube cAnO3VK7mY4 >}} 

but this little delight can also be prepared any day of the year, because, it is so good accompanied by a nice cup of fermented milk (lben ... .. butter milk), or even with a good cup of coffee milk. 

go without talking too much as I usually do, we go to the ingredients: 

[ ![the bradjs](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/les-bradjs.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/les-bradjs.jpg>)   


**Bradj ..... Semolina lozenges with dates**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradjs-012a.jpg)

portions:  50  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** for the pasta: 

  * 3 measures of medium semolina 
  * 1 measure of melted butter 
  * salt 

for the date paste: 
  * 500 gr of date paste 
  * 1 pinch of cinnamon 
  * 1 clove of clove. 
  * 2 tablespoons of smen or oil 
  * 1 tablespoon of honey or jam. 



**Realization steps** prepare the pasta: 

  1. put the semolina in a terrine, pour over the melted butter, and mix well, add the salt, and sprinkle the semolina gradually with the water, mixing until having a malleable paste (do not knead it, so that it does not become elastic). share the dough in 2 and let it rest. 

prepare the date paste: 
  1. prepare the date paste, mixing the dates cleaned and grilled, add a pinch of cinnamon, a pinch of ground clove, and knead in a little oil and jam. 
  2. flatten 1 of the 2 balls in the shape of a slab, flatten also the date paste, and lay it on the semolina cake, then flatten the second ball, and put it on the other 2 patties to cover the date paste well. 
  3. be careful the semolina paste is of the kind that crumbles easily so do not make big cakes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradj-montage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradj-montage.jpg>)
  4. now flatten the whole thing with the apple of your hands, to have a final slab with 1 diameter of 1 cm and a half or so me, according to your taste. 
  5. cut into rhombuses or triangles big enough.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradj-18.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradj-18.jpg>)
  6. cook on an ungreased sheet.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradjs-014-a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradjs-014-a.jpg>)
  7. have a good time on both sides. 



[ ![bradjs 014 a](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradjs-014-a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bradjs-014-a.jpg>)

delicious as I said with a mix of crispy and melting at the same time, Bradj semolina lozenges with dates 

S’il n’était pas déjà minuit maintenant, j’irais prendre une autre pièce, je salive déjà, enfin je laisse ça à demain matin avec un bon bol de lait chaud. 
