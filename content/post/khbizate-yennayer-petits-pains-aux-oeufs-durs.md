---
title: khbizate yennayer, buns with hard boiled eggs
date: '2016-01-13'
categories:
- Brioches et viennoiseries
tags:
- buns
- Milk brioche
- Kenwood
- Boulange
- Galette
- Kitchenaid
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/khbizate-yennayer-1.jpg
---
[ ![khbizate yennayer 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/khbizate-yennayer-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/khbizate-yennayer-1.jpg>)

##  khbizate yennayer, buns with hard boiled eggs 

Hello everybody, 

Among the traditions to yannayer, women prepare khbizate yennayer, or rolls with boiled eggs. My mother often made them, she gave them this shape, of course with a hard egg in the middle, and we called them the Hniwnate. Besides, I have to share his recipe with you as much as possible. 

[ ![khobz-012_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/khobz-012_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/08/khobz-012_thumb1.jpg>)

But today it's yennayer khbizete that Nadira S. shares with us. Frankly her hard-boiled egg rolls are so beautiful that I really want to put my hand in my screen to take one. 

and this is what Nadira S tells us: 

salam Alaykoum, here is what I prepared for this yennayer 2016, khbizate yennayer for my little girls and little cousins, it remains to finish with dried fruits, oranges and sweets. It's our custom to ghazaouet for years yennair good! 

I remember when I was little, our moms were preparing them, and in our neighborhood we went out at night to watch, and our mothers told us: do not eat your khobizet because there is **agoust yennaayer** who will come at night will raze you, because you have not waited and as, we were afraid we ate them after 2 or 3 days and yes, we believed everything we were told at the time. 

**khbizate yennayer, buns with hard boiled eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/khbizate-yennayer-1.jpg)

portions:  9  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients**

  * 500 gr of flour 
  * 50 gr of sugar 
  * 100 gr of butter 
  * 1 coffee of salt 
  * 1 tablespoon dried yeast 
  * 2 eggs 
  * warm milk (place the broken eggs in a glass, and add enough milk to have a total of 300 ml of egg and milk mixture) 

decorations: 
  * 9 hard boiled eggs 
  * almonds 
  * sugar 
  * Egg yolk 



**Realization steps**

  1. mix all the dry ingredients. 
  2. pick it up with the mixture of egg and milk well whipped. 
  3. oil and add butter as you go. 
  4. let the dough rise a good hour and then degass. 
  5. place in the fridge overnight, in the morning remove them, leave at room temperature a little, then make meatballs. 
  6. I had 9 with that amount. pat a little the meatballs to make a small cake, and place a hard egg in the middle 
  7. brush with egg yolks, sprinkle a little sugar and poke 3 or 4 almonds 
  8. (I had crushed almonds) 
  9. let rise for twenty minutes, then bake at 180 degrees C for about thirty minutes or depending on your oven. 



[ ![khbizette yennayer](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/khbizette-yennayer.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/khbizette-yennayer.jpg>)
