---
title: Lemon muffins and poppy seeds
date: '2018-02-13'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- sweet recipes
tags:
- Crumble
- Cakes
- Fluffy cake
- Soft
- To taste
- Dessert
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/muffins-au-citron-et-graines-de-pavots-6.jpg
---
[ ![lemon muffins and poppy seeds](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/muffins-au-citron-et-graines-de-pavots-6.jpg) ](<https://www.amourdecuisine.fr/article-muffins-au-citron-et-graines-de-pavot.html/muffins-au-citron-et-graines-de-pavots-6>)

##  Lemon muffins and poppy seeds 

Hello everybody, 

To taste it yesterday, it was a muffin party with the kids, we made some great lemon muffins and poppy seeds. These muffins were soft and fragrant. The children were very proud of the result because they did it, Rayan was responsible for the weighing, and it was very precise, and it pleased him to find the right number. In Ines's role was to sift the flour, mix the dry products, put the boxes in the muffin pans. But I think what they liked the most was to put the crumble on top. 

Even for the photos, the kids loved playing the photographers, so sorry for the quality of the photos, but that's my kids' recipe, the decor they chose, especially the flower, my daughter insisted that the flower is well on the photo she would take. 

[ ![lemon muffins and poppy seeds 5](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/muffins-au-citron-et-graines-de-pavot-5.jpg) ](<https://www.amourdecuisine.fr/article-muffins-au-citron-et-graines-de-pavot.html/muffins-au-citron-et-graines-de-pavot-5>)   


**Lemon muffins and poppy seeds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/muffins-au-citron-et-graines-de-pavot-4.jpg)

portions:  12  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 290 gr of flour 
  * 100 gr of granulated sugar 
  * 50 gr of brown sugar 
  * 3 tablespoons poppy seeds 
  * 2 teaspoons of baking powder 
  * ¼ teaspoon of baking soda 
  * 1 pinch of salt 
  * 120 gr of melted butter 
  * juice and zest of 2 limes (5 to 6 tablespoons juice) 
  * juice and zest of 1 orange (almost 80 ml) 
  * 2 large eggs 
  * 120 gr of Greek yogurt or natural yoghurt 
  * 1 teaspoon of vanilla extract 

for the crumble: 
  * 45 gr of flour 
  * 1 tablespoon brown sugar 
  * 1 tablespoon of sugar 
  * 2 tablespoons melted butter 
  * vanilla extract 



**Realization steps** Prepare the crumble: 

  1. mix all the ingredients with the fork and leave to rest. 

prepare the muffins: 
  1. Preheat the oven to 200 degrees C. Place the 12 boxes in the muffin cups 
  2. In large bowl, mix flour, powdered sugar, brown sugar, poppy seeds, baking powder, baking soda, and salt until well blended. set aside. 
  3. In another medium bowl, mix the melted butter, lemon juice, lemon zest, orange juice and zest until combined. 
  4. Add the eggs, one at a time, whisking after each addition. 
  5. Introduce yoghurt and vanilla. 
  6. Pour the liquid mixture over the dry ingredients and mix gently together until it is homogeneous, but do not overdo it. The dough should be very thick. 
  7. Pour the dough into the muffin cups, filling them with ¾ 
  8. garnish with crumble 
  9. Bake for 5 minutes at 200 degrees C. Then reduce the oven temperature to 175 ° C and continue cooking for 10 to 13 minutes until the top is lightly browned. A toothpick inserted in the center should come out clean. 
  10. Let cool for 10 minutes before unmolding. 



[ ![Lemon muffins and poppy seeds](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Muffins-au-citron-et-graines-de-pavot.jpg) ](<https://www.amourdecuisine.fr/article-muffins-au-citron-et-graines-de-pavot.html/muffins-au-citron-et-graines-de-pavot>)
