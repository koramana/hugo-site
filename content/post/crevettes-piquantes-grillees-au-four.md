---
title: steak prawns roasted in the oven
date: '2016-10-03'
categories:
- cuisine diverse
- Healthy cuisine
- fish and seafood recipes
- riz
tags:
- Grill
- inputs
- Barbecue
- Shellfish
- Hot Entry
- Sea food
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/crevettes-piquantes-grillees-au-four-2.jpg
---
![Shrimp-pungent-roasted Crust-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/crevettes-piquantes-grillees-au-four-2.jpg)

##  steak prawns roasted in the oven 

Hello everybody, 

yumi yumi what good are these hot prawns grilled in the oven! I usually make these barbecue-pricked prawns, but it's a pity that when I planned to make this recipe to participate in the recipe game around a # 21 ingredient ... 

Well that day, it rained ropes, and I could not start the barbecue. My solution for eating these shrimps, which had had time to marinate all night in a delicious hot piri piri sauce, was to grill them in the oven (under the grill). 

The prawns roasted in the oven were too good indeed, but being used to this recipe barbecued, I had that lacked the smell of grilling ... to do again after the beautiful days, and surely I will share the photos with you. 

So with my recipe of oven-roasted hot prawns, I take part in round 21, of our recipe game around an ingredient of which Coco de nice is the godmother and to propose shrimp as a star ingredient. 

Before you leave, I want to introduce our next sponsor: 

Previous rounds: 

![Shrimp-pungent-roasted Crust-4](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/crevettes-piquantes-grillees-au-four-4.jpg)

**steak prawns roasted in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/crevettes-piquantes-grillees-au-four-3.jpg)

**Ingredients**

  * royal shrimp (frozen for me) 

for piri piri sauce: (you can double the quantities if you want a dip for the barbecue) 
  * 4 c. tablespoon lime juice 
  * 100 ml extra virgin olive oil 
  * 120 ml white vinegar 
  * 1 C. cayenne pepper 
  * 1 C. minced garlic 
  * 1 C. paprika 
  * 1 teaspoon salt 
  * 1 tablespoon chili flake 



**Realization steps** prepare piri piri sauce: 

  1. Mix all ingredients and let stand at least 6 hours 
  2. let the shrimp thaw and drain from their water. 
  3. mix with the marinade and let stand overnight (mix occasionally)   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/crevettes-au-four.jpg)
  4. cook the shrimp on the barbecue, otherwise place them in a baking tin, lined with baking paper, and place under a hot grill. 



![shrimp-spicy-grilled Crust](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/crevettes-piquantes-grillees-au-four.jpg)

and here is the list of participants in this round: 

**_ \- 1 Soulef  [ A love of cooking  ](<https://www.amourdecuisine.fr/>) Stir-fried steak prawns  _ **
