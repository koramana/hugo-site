---
title: palms with pesto
date: '2015-06-24'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
tags:
- Ramadan 2015
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/palmiers-au-pesto-1.jpg
---
[ ![palms with pesto](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/palmiers-au-pesto-1.jpg) ](<https://www.amourdecuisine.fr/article-palmiers-au-pesto.html/sony-dsc-293>)

##  Palms with pesto, 

Hello everybody, 

Here is a recipe realized with the closed eyes, can be the only small work to do is to realize the [ homemade pesto ](<https://www.amourdecuisine.fr/article-pesto-maison.html>) if you do not have a jar of commerce. 

A recipe as simple as delicious, and I thank my dear Lunetoiles for this little delight and these pretty pictures, I really wanted to put my hand in the screen, to take two or three palms to accompany my [ chorba Frik ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) rather yesterday it was a [ chorba beida ](<https://www.amourdecuisine.fr/article-chorba-beida.html>) for me, therefore, these palms with pesto would have accompanied him well. 

**palms with pesto**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/palmiers-au-pesto-3.jpg)

**Ingredients**

  * 1 puff pastry pure butter 
  * Pesto 



**Realization steps**

  1. Unroll the puff pastry, fold down each side to obtain a square, which you cover entirely with pesto. 
  2. Draw a line in the middle to locate, for shaping. 
  3. Roll one edge of dough to the middle of the square, then do the same for the other side: wrap the double pudding you just got in cling film and place it in the freezer until it is hardened . 
  4. Preheat the oven to 210 ° C. 
  5. Take out the dough, cut it into 1 cm thick slices. 
  6. Arrange them on the baking sheet covered with baking paper. 
  7. Bake for about 15 minutes: the dough should be golden, and the pesto still green. 



[ ![palms with pesto 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/palmiers-au-pesto.jpg) ](<https://www.amourdecuisine.fr/article-palmiers-au-pesto.html/sony-dsc-295>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
