---
title: tajine potato with whiting
date: '2015-11-25'
categories:
- Algerian cuisine
- Moroccan cuisine
tags:
- Full Dish
- Ramadan 2015
- Ramadan
- Ramadhan
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-el-hout-2.jpg
---
[ ![tajine el batata bel hout 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-el-hout-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-el-hout-2.jpg>)

##  tajine potato with whiting 

Hello everybody, 

Here is a dish that comes directly from Algeria, from the kitchen of my mother. A great tajine, tajine el batata bel hout, or tajine potatoes with fish. You can choose any fish, my mother here chose Whiting. 

This tajine can be cooked in a pot, or preferably in a Moroccan tajine. 

**tajine potato with whiting**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-el-hout-1.jpg)

portions:  2  Prep time:  10 mins  cooking:  35 mins  total:  45 mins 

**Ingredients** for the marinade of the fish: 

  * some whiting 
  * 2 cloves garlic crushed 
  * ginger powder 
  * powdered cumin 
  * salt, coriander powder and hot red pepper powder. 

for the potato: 
  * 3 to 4 medium potatoes (or depending on the number of people) 
  * 2 ripe fresh tomatoes 
  * 1 onion 
  * the same spices above (pay attention to salt) 



**Realization steps**

  1. marinate cleaned whiting and cut into pieces of almost 4 cm in garlic mixture, ginger, cumin, salt, coriander, some chopped coriander, and dried red pepper. 
  2. peel the potatoes and cut each in two lengthwise and then three. 
  3. sauté onions and cut peeled tomatoes into small cubes 
  4. add spices, ginger, cumin, salt, coriander powder, and dried red pepper and chopped coriander. 
  5. add the potato to this marinade and leave well absorbed all. 
  6. in a pot, put 2 tablespoons of oil (you can cook in a Moroccan tajine). 
  7. add the water, just to cover the potato. and put on the fire (so we do not need to simmer the ingredients in the oil) 
  8. let it boil a little. 
  9. half way through the potato, add the fish pieces with their marinade. 
  10. cook over low heat, the sauce must be very good. 



[ ![tajine el hout](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-el-hout.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-el-hout.jpg>)
