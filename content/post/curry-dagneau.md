---
title: Lamb curry
date: '2016-07-09'
categories:
- diverse cuisine
- Dishes and salty recipes
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-dagneau-023.jpg
---
[ ![Lamb curry 023](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-dagneau-023.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-dagneau-023.jpg>)

##  Lamb curry 

Hello everybody, 

I bring you this recipe that I made in March 2014, at the request of one of my readers, and when I reread the introduction, it awoke a lot of memories in my memory, so I preferred the keep ... I hope my dear "Zaynabe" is the recipe you were looking for !!! 

You will believe me if I tell you that this curry of lamb is the recipe that I have cooked properly since my birth ??? 

yes, well, as a friend said: in love of cooking is her husband who cooks, lol .... 

Finding myself alone with the baby and my two other children, I have not had time to do things right since the arrival of this new member at home .... and to eat, well I passed the relief to my husband, despite him .... He who never knew how to boil an egg ... well here he is cooking dishes and recipes that I did not expect, I think he is now ready to participate in "Master Chef UK" ... lol No, we must not push the cork too much, but we can say that we could not eat a good meal anyway ... 

So yesterday at dinner, and after having a nice nap, and that baby slept with Dad, I could find a moment of freedom and joy to take my pan, and prepare a good curry with lamb, a recipe that I like it a lot, and that I never publish ... 

But, to take pictures, it was not easy at night, so I left a side to take pictures in the light of the day ... But after preparing the essentials to make the paparazzi, Mr baby c ' is awake, and impossible to calm ... So sorry for the quality of the photos that were limited to 6, among them, two or three pictures of the baby ... 

[ ![Lamb curry 025](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-dagneau-025.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-dagneau-025.jpg>)

Still, despite the pictures, this dish is a delight that can either be eaten with a good homemade bread, [ pita bread ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html> "Lebanese bread, pita bread video") , [ naans bread ](<https://www.amourdecuisine.fr/article-les-naans-a-la-coriandre.html> "naans, indian bread") , or so with a good [ rice ](<https://www.amourdecuisine.fr/article-riz-a-la-corete-mouloukhiya.html> "Rice with corn, Mouloukhiya") well flavored 

I'm quick to give you the recipe, before baby does, and I hope I will not forget anything about this recipe.   


**Lamb curry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-dagneau-019.CR2_.jpg)

Cooked:  Indian  Recipe type:  Hand  portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 500 gr of mutton 
  * 2 medium onions 
  * 2 cm fresh ginger 
  * 2 fresh tomatoes 
  * some curry leaves 
  * 3 seeds of cardamom 
  * 1 small cinnamon stick 
  * 2 tablespoons of oil 
  * 4 cloves 
  * 1 cup of cumin in grain 
  * 1 cup of cumin powder 
  * ½ tsp of black pepper 
  * ¼ cup of turmeric coffee 
  * 1 tablespoon of coconut powder 
  * 2 tablespoons coriander powder 
  * ¼ bunch of fresh coriander 
  * salt according to taste 
  * spicy according to taste 
  * strong paprika according to taste 



**Realization steps**

  1. put the oil in a hot skillet, 
  2. add unmilled whole spices: cardamom, cinnamon, cloves, cumin, curry leaf. 
  3. let it simmer well, then add chopped onion on top. 
  4. stir until onion becomes slightly brown 
  5. add salt, crushed ginger, 
  6. now add the medium cut cubed agnea meat. 
  7. let it come back, until it releases its water. 
  8. add black pepper, coriander powder, cuminen powder 
  9. cover with water and let cook for 20 to 30 minutes until the lamb becomes tender (add water if necessary). 
  10. add the chopped tomato, the chopped fresh coriander, the sharp cut, for the people who like the spicy. 
  11. add the coconut powder, and make for another 10 to 15 min. 
  12. cook until sauce is reduced. 



[ ![Lamb curry 021](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-dagneau-021.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/curry-dagneau-021.jpg>)
