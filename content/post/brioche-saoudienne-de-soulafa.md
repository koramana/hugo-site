---
title: Saudi Brioche, from Soulafa
date: '2009-02-27'
categories:
- cakes and cakes

---
Here is a brioche that I love to shape, and enjoy it, I do it twice, so much that it was very good. I found it at Soullinkafa's, and she explains the shaping very well. Ingredients: 500g of flour 100g of softened butter 180ml of warm water (to control according to your flour and dough, I would say +/- 160ml for me) 3 cs of milk powder 3 cs of sugar (for me 100g) 1 package baker's yeast (1 spoon and a half for & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

**Ingredients:**

**500g of flour  
100g softened butter   
180ml of warm water (to control according to your flour and dough, I would say +/- 160ml for me)   
3 tablespoons of milk powder   
3 tablespoons of sugar (for me 100g)   
1 packet of baker's yeast (1 spoon and a half for me to drink in the water)   
2 rounds of orange blossom   
2 medium eggs **

**1 pinch of salt**

**1 egg yolk + some milk for gilding**

**chocolate nutella**

**Preparation:**

**In a hollow dish, pour the flour and make a well, then put in order:**

**eggs, yeast, sugar, milk powder, orange blossom and salt. Work by adding lukewarm water little by little until a ball is formed.**

**Knead well then add softened butter, and continue kneading until fat is absorbed.**

**Cover with a clean cloth and let the dough rest until doubled in size.  
De-gas and then divide the dough into small balls, stuff each ball with a small piece of chocolate and solder well. **

**Arrange the meatballs without sticking them in a pie plate (buttered and floured or on a sheet of baking paper). Let rise a few minutes.  
Brush with egg yolk and milk and sprinkle with almond, black seed, sesame seeds, granulated sugar ... **

et voila une tres belle et bonne brioche. 
