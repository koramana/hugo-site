---
title: lettuce and peas soup
date: '2015-03-05'
categories:
- Algerian cuisine
- recette de ramadan
- soupes et veloutes
tags:
- Chicken
- Potato
- Velvety
- Vegetables
- Vegetables soup
- Broth
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/soupe-a-la-laitue.jpg
---
[ ![lettuce and peas soup](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/soupe-a-la-laitue.jpg) ](<https://www.amourdecuisine.fr/article-soupe-la-laitue-et-petits-pois.html/soupe-a-la-laitue>)

##  Lettuce soup with peas 

Hello everybody, 

You know better now my friend Fleur DZ, because if you go on the articles "my recipes tested and approved", you would have surely read the name of flower Dz which are repeated again and again, a faithful reader, who loves cooking, and who loves sharing. 

This time, it's her recipe that is featured on my blog, a soup she's trying for the first time, based on lettuce leaf, yes a lettuce soup and small The result as she says is a real delight, then it's up to you to try, and surely you'll love it. 

**lettuce and peas soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/soupe-de-laitue-2.jpg)

portions:  4-5  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients**

  * 2 tablespoons of oil 
  * a half white chicken 
  * a chicken bouillon cube 
  * 2 potatoes cut in four 
  * a big bowl of peas (frozen for me) 
  * ½ bowl of chopped green lettuce 
  * fresh chervil 
  * 1 minced onion 
  * salt and pepper. 

Decoration: 
  * croutons 
  * chervil 
  * the fresh liquid cream (not put) 



**Realization steps**

  1. fry the onion in the oil 
  2. add your chicken and let brown a few minutes 
  3. add the peas and the potatoes, the chervil and the bouillon cube. add salt and pepper 
  4. Cover with enough water and simmer about 20 minutes until the peas and apples become tender 
  5. at this point, add your lettuce and boil about 4 to 5 minutes. 
  6. Mixex your soup in smooth puree 
  7. serve it decorate with croutons and chervil, finish with the cream if desired. 



![lettuce and peas soup](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/soupe-de-laitue-1.jpg)
