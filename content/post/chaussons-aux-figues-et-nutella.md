---
title: slippers with figs and nutella
date: '2016-08-14'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/chaussons-aux-figues-et-nutella-3.CR2_thumb1.jpg
---
##  slippers with figs and nutella 

Hello everybody, 

still a recipe for figs, yes I take full advantage, and you must do the same as it is the season, one of my friends blogger told me that she has frozen figs, I must return to it, and know that it would be the result after thawing, do you already have frozen figs ??? 

I prepared the recipe of the slippers in the pan (recipe to come), and the dough was so delicious, I liked to make a sweet recipe with this same dough, and the result, a marvel, I do not hide it not. 

**slippers with figs and nutella**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/chaussons-aux-figues-et-nutella-3.CR2_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for the pasta: 

  * 500 g flour. 
  * 1 teaspoon salt. 
  * 100 g melted butter. 
  * 1 egg. 
  * 1 tbsp instant yeast. 
  * 150 to 200 ml of warm water for the dough 

for the stuffing: 
  * Figs 
  * Nutella chocolate 



**Realization steps**

  1. put the flour and the salt in a salad bowl or the bowl of the robot, mix 
  2. add the baking powder, then the melted butter, sand the dough 
  3. add the egg and water gradually, work well the dough. 
  4. form a ball and let rise the dough (sheltered drafts) under a towel, for about 1 hour (for my part, I put everything in the bread machine, I program "pizza pie" and I turn on the machine) 
  5. then divide the dough into 12 equal balls. 
  6. spread each ball in a circle 
  7. decorate with a nice spoon of Nutella, then drop over the sliced ​​fig 
  8. close the slipper well then solder well with a fork. 
  9. cook the slippers as you go, in a large skillet 4 to 5 minutes on each side without adding fat. 



I hope that just like me, you will love the recipe. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

merci. 
