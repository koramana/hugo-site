---
title: kitchen love recipes tested and approved 78
date: '2015-09-14'
categories:
- the tests of my readers
tags:
- desserts
- Cakes
- Algeria
- Morocco
- dishes
- tajine
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-58.html/dyouls-de-kari-ben>)

Hello everybody, 

During my stay in Algeria, I must tell you that it was impossible to save all the photos you sent me, your achievements from my blog. 

Do not be worried, I will publish inchallah, all the photos of your achievements, but for the moment, I post the pictures I received from girls these past 3 days ... 

I thank you all for your fidelity and your comments, I try to answer as soon as possible, but here are days, where it is almost impossible for me to touch the computer. 

Thank you to you who realize my recipes with confidence, happy for you to have succeeded, and even more happy to see the pictures of your achievements. 

Do not hesitate too who have not yet sent the photos of your achievements, share them with me, or by email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

[ ![barbie](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/barbie.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/barbie.jpg>)

[ barbie princess, birthday cake ](<https://www.amourdecuisine.fr/article-princesse-barbie-gateau-d-anniversaire.html>) at Rima 

[ ![grandchildren well-cocoa-jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/petits-puits-cacao-confiture.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/petits-puits-cacao-confiture.png>)

[ ![cheesecake flan](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/flan-cheesecake.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/flan-cheesecake.jpg>)

[ Flan cheesecake ](<https://www.amourdecuisine.fr/article-flan-cheesecake.html>) at Joujou M. M 

[ ![schichttorte Nihal](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/schichttorte-Nihal.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/schichttorte-Nihal.jpg>)

[ Schichttorte ](<https://www.amourdecuisine.fr/article-schichttorte-ou-gateau-a-etages-alsacien.html>) at Aravinda Nihel 

[ ![Almond Tart with Apricots](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-amandine-aux-abricots.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-amandine-aux-abricots.jpg>)

[ amandine pie with apricots ](<https://www.amourdecuisine.fr/article-tarte-aux-abricots-et-creme-d-amande.html>) at Hamdi N. A 

[ ![bourdaloue pie at louisa's place](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-bourdaloue-chez-louisa.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-bourdaloue-chez-louisa.jpg>)

[ Tarte Bourdaloue ](<https://www.amourdecuisine.fr/article-tarte-bourdaloue-recette-de-tarte-amandine-aux-poires.html>) at Gouaidia L 

[ ![meatloaf, pipo](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pain-de-viande-pipo.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pain-de-viande-pipo.jpg>)

[ meatloaf ](<https://www.amourdecuisine.fr/article-pain-de-viande.html>) at Pipo Bijoux 

[ ![batata kbab](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/batata-kbab.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/batata-kbab.jpg>)

[ batata kbab ](<https://www.amourdecuisine.fr/article-pomme-de-terre-au-four-batata-kbab.html>) at Malika B. 

[ ![potato tart bechamel](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-pomme-de-terre-bechamel.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-pomme-de-terre-bechamel.jpg>)

[ salted potato and chicken pie ](<https://www.amourdecuisine.fr/article-tarte-salee-sans-pate-au-poulet-et-pomme-de-terre.html>) chez Malika B. 
