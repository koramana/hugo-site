---
title: my recipes at home 38
date: '2013-12-06'
categories:
- dessert, crumbles and bars
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

Thanks to all the people who have tried to contact me by email, to send me the pictures of the recipes you have to try from my blog. 

I put you, a system much faster so that you can send me your photos, just go on this page: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll love ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link to your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)

the link will always be visible to you, it is in the menu, at the top of the page .... 

so, now we start the parade, recipes that you have tested from my blog? 

![Soup de crevettes5-8394120f0c66327a69dead224bf2f16df3353d68](https://www.amourdecuisine.fr/wp-content/gallery/1/soupe-de-crevettes5-8394120f0c66327a69dead224bf2f16df3353d68.jpg)

![decembre2013-013-702b6e7afd3e0e3b8cdb9febccaf70a0e7844cac](https://www.amourdecuisine.fr/wp-content/gallery/1/decembre2013-013-702b6e7afd3e0e3b8cdb9febccaf70a0e7844cac.jpg)

[ Almond tart with quince ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-coings.html> "almond tart with quince / quince recipes") at Nadia, a reader 

[ ![esses](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/esses.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/esses.jpg>)

I know that there are not many recipes this time, because people sent me photos on an email address to which I can not access it anymore ... 

I hope that now with the system to put the same photo online, it will be easier ... 

je vous dis merci et a la prochaine…. 
