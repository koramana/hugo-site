---
title: Butter apple pie
date: '2017-09-20'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tarte-aux-pommes-au-beurre-3.jpg
---
![apple pie with butter 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tarte-aux-pommes-au-beurre-3.jpg)

##  Butter apple pie 

Hello everybody, 

Apple pie 

With great pleasure I share this recipe that Ratiba generously shared with us. 

{{< youtube 2rBsE7pTDjc >}} 

The recipe in Arabic: 

**Butter apple pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tarte-aux-pommes-au-beurre-4.jpg)

Recipe type:  pie dessert  portions:  8  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients** _shortbread:_

  * 300 gr of flour 
  * 150 gr of butter 
  * 1 egg 
  * 2 tablespoons sugar 
  * 2 tablespoons cold water 

_butter cream:_

  * 160 gr of butter 
  * 120 gr of sugar 
  * 2 eggs 
  * a pinch of cinnamon 

_for garnish:_

  * 3 apples 



**Realization steps** _preparing the dough_

  1. in the bowl of a blinder, mix the butter and the flour 
  2. add egg and sugar 
  3. remove from the bowl of the blinder, and add the water slowly to form a beautiful ball 
  4. cover with a film of food, and cool for a few minutes 

_preparation of the cream and filling of the pie shell:_

  1. beat eggs and sugar until they double in size 
  2. meanwhile melt the butter, until boiling 
  3. add to the mixture while whisking 
  4. spread the dough in a 20 cm by 30 cm mold 
  5. peel, seed and cut the sliced ​​apples 
  6. arrange them evenly on the dough 
  7. pour over the butter cream 
  8. bake at 180 ° for 30 to 40 minutes (or depending on your oven) 



Note personally, I think that the cream was a little much, and that it covered the apples, so I removed before the end of cooking the crust that covered all the apples, and I put the pie in the oven   
I prefer the final look, a thousand times better than when the apples were all covered.   
in any case, between us 4, this big pie did not even have time to rest cool. ![apple pie with butter 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tarte-aux-pommes-au-beurre-2.jpg)
