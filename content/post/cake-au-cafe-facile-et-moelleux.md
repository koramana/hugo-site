---
title: coffee cake, easy and fluffy
date: '2013-01-21'
categories:
- dessert, crumbles et barres
- gateaux, et cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-cafe-21.jpg
---
![coffee cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-cafe-21.jpg) ![coffee cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-cafe-12.jpg)

##  coffee cake, easy and fluffy 

Hello everybody, 

I shared the link of this recipe on social networks, and anyone who made it was more than convinced .... you want to try too? 

Do not forget if you too have made one of my recipes, come and pass me the picture on this email: 

![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ![cake-cafe-11.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-cafe-111.jpg)

**coffee cake, easy and fluffy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-cafe-1.jpg)

**Ingredients**

  * 3 eggs 
  * 200g of muscovado sugar (simple for me) 
  * 120g of soft butter 
  * 200g of flour 
  * 3 c. good quality coffee (3 cases of hot water in which I dissolve 3 cac of nescafe) 
  * 3 tablespoons cocoa powder 
  * A pinch of salt 
  * A vanilla pinch powder or a vanilla sugar packet 
  * Two teaspoons, shaved, baking powder 



**Realization steps**

  1. Preheat the oven to medium temperature. 
  2. Start by whipping the sugar and butter 
  3. add coffee, and vanilla, 
  4. continue beating for two minutes then add the eggs one by one while continuing to beat. 
  5. In a bowl, mix together, flour, salt, yeast and cocoa powder. 
  6. Add little by little to the first mixture and stir with a wooden spatula until a homogeneous mixture is obtained. 
  7. Butter a cake mold, sprinkle with cocoa, shake the mold to remove the extra and pour the dough. 
  8. Bake for 15 to 20 minutes, check with a knife, remove and let cool before unmolding. 
  9. Sprinkle with icing sugar just before serving and savor. 



![cake cafe 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cake-cafe-31.jpg)

bonne dégustation 
