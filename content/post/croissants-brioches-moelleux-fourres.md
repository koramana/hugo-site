---
title: stuffed brioche croissants
date: '2011-05-09'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/05/croissant-briochee1_thumb1.jpg
---
##  stuffed brioche croissants 

Hello everybody, 

So, I first made brioche croissants stuffed with almonds, and the second time with pastry cream. 

**stuffed brioche croissants "الكرواسون المحشو" - soft brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/croissant-briochee1_thumb1.jpg)

portions:  20  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * 500gr of flour 
  * 1 egg 
  * 100 gr of softened butter 
  * 1 tablespoon baker's yeast 
  * 250 ml of water (+ or - depending on the flour) 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 4 tablespoons of sugar (6 for me) 
  * 1 egg yolk + a few drops of vinegar to brown. 
  * (Milk powder and water can be replaced by liquid milk but the result is better with milk powder) 



**Realization steps**

  1. Mix all the ingredients without the butter, knead well and add the softened butter at the end. 
  2. knead again to incorporate butter 
  3. Let the dough rise in a draft-free place, once the dough has risen divide into balls. 
  4. flatten a ball and cut triangles with a pizza wheel or knife. 
  5. Stuff them if you want marzipan, [ custard ](<https://www.amourdecuisine.fr/article-creme-patissiere.html>) , chocolate, or Nutella (I love stuffing Nutella) by placing the stuffing on the edge of the widest triangle, and roll to the tip. 
  6. Fold the edges back to form a crescent shape and place as you go on a buttered baking sheet. 
  7. Cover them and let them rise again. 
  8. Once the croissants are raised, brush with egg yolk mixture and drops of vinegar. 
  9. Bake in a preheated oven at 200 degrees for 15 to 20 minutes. 


