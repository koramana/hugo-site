---
title: I participate in the halwati shop contest
date: '2011-09-08'
categories:
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/aid-el-fitr-2011-gateaux-Algeriens-002_thumb.jpg
---
I participate with this photo:  ![aid-el-fitr-2011-gateaux-Algerians-002 thumb](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/aid-el-fitr-2011-gateaux-Algeriens-002_thumb.jpg) and to see my other cakes that I had to prepare during the feast of the aid you can see:  the video:  or look at this article:  [ AID EL FITR 2011 ALGERIAN CAKES ](<https://www.amourdecuisine.fr/article-aid-el-fitr-2011-les-gateaux-algeriens-82873081.html> "Aid el Fitr 2011 Algerian cakes")
