---
title: the palms in homemade puff pastry
date: '2015-05-05'
categories:
- Brioches et viennoiseries
- recettes sucrees
tags:
- Pastries
- Boulange
- Pastry
- Breakfast
- To taste
- buns
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/les-palmiers-1.jpg
---
[ ![the palms in puff pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/les-palmiers-1.jpg) ](<https://www.amourdecuisine.fr/article-les-palmiers-en-pate-feuilletee.html/les-palmiers-1>)

##  homemade puff pastry palms 

Hello everybody, 

Back with recipes based puff pastry, and here is today the recipe for sugar palms with homemade puff pastry. Yes I know that the recipe is simple to make, but it's with homemade puff pastry, it's no longer an easy recipe, hihihih. These palms are made with a puff pastry made differently from the [ puff pastry ](<https://www.amourdecuisine.fr/article-croissants-feuilletes-au-chocolat.html> "Chocolate pastry croissants") that I made myself. 

This recipe is for my friend **Wamani Merou** , she made a foliage using 5 simple turns ... Read the recipe for more information, and for a result as beautiful as his.   


**the palms in homemade puff pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/les-palmiers-1.jpg)

portions:  20  Prep time:  60 mins  cooking:  20 mins  total:  1 hour 20 mins 

**Ingredients** for the pasta: 

  * 700g of flour 
  * 1 tablespoon of oil 
  * 10 gr of salt 
  * 350 gr water. 

For the tourage 
  * 500g of butter 

for the finish: 
  * Granulated sugar. 



**Realization steps**

  1. in the tank of the robot, place the flour, salt and oil 
  2. add the water slowly, until the paste is collected well to form a pudding that does not stick to the walls (it is not necessary to add all the water) 
  3. on the worktop, form a ball, then cut the top of the dough with a knife, forming a cross.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/pate-feuilett%C3%A9e-300x272.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41882>)
  4. Wrap it in a sheet of film paper, and let it cool for at least 30 minutes. 
  5. flour a work plan, place the dough on it, and flatten with the fingers the 4 points of the ball of dough. 
  6. Lower the dough respecting the cross shape. Leave the center thicker. Place the butter in the center at room temperature. Shape it into a rectangle with the dimensions of the dough so that it covers the center of the dough correctly.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/pate-feuillet%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41883>)
  7. Fold the sides of the dough over the butter as if you are closing an envelope. Weld the ends well, to cover all the butter. 
  8. beating with a rolling pin so that the butter is well distributed inside the tempera. However, it is necessary to keep a well square form to the dough. 
  9. Roll the dough lengthwise, making sure that the butter does not pierce the dough. Form a long rectangle about 1 cm thick. 
  10. Rotate the dough a quarter of a turn, and fold the dough in half like a wallet. 
  11. Lower the dough again in the length, rotate the rectangle a quarter of a turn counterclockwise and fold the dough in three. like the previous round. 
  12. As you did 2 simple turns. Score 2 points in the dough so you do not get wet. 
  13. put in the fridge for 30 minutes, then take the dough again, roll it out again into a fairly long rectangle, then fold it in 3. 
  14. put it back in the fridge, and do the same again for the two remaining towers. 
  15. After the 5th turn, and the rest of your dough, remove it, spread and sprinkle the surface with powdered sugar. 
  16. roll the left side to the middle, do the same on the right side. 
  17. cool down for at least 30 minutes. 
  18. Remove the dough and cut pieces of almost 1 cm. 
  19. place them in a baking pan lined with baking paper. 
  20. cook in a preheated oven at 160 degrees for 20 minutes. 
  21. you can garnish with a little jam to give a shiny side to the palm tree 



[ ![the palms in puff pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/les-palmiers.jpg) ](<https://www.amourdecuisine.fr/article-les-palmiers-en-pate-feuilletee.html/les-palmiers>)
