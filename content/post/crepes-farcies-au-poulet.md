---
title: stuffed crepes with chicken
date: '2013-11-11'
categories:
- diverse cuisine
- Cuisine by country
- dips and sauces

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/09/2010-08-19-ram_21.jpg
---
![stuffed crepes with chicken](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/2010-08-19-ram_21.jpg)

##  stuffed crepes with chicken 

Hello everybody, 

Here is a beautiful recipe that I always like to prepare to impress my guests, and it works every time, it is a very delicious recipe. While it takes a little time to prepare, but frankly, it's worth it. 

In another round, I pass you the link of a recipe is identical [ with shrimp stuffed crepes ](<https://www.amourdecuisine.fr/article-gratin-de-crepes-aux-crevettes.html> "gratin pancakes with shrimps") but for this time I replaced the shrimp with chicken, the taste was totally different and beautiful 

I put here the link of the [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video") used after in the recipe   


**stuffed crepes with chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/ram-032_thumb.jpg)

Recipe type:  dish  portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * for the crepe dough: (must say that I made half of the ingredients, and I had a good amount of pancakes, already half is in the freezer) 
  * 400 g flour 
  * 3 eggs 
  * 50 g of melted butter 
  * 1 liter of milk 
  * A pinch of salt 
  * 1 tablespoon of sugar 
  * for the stuffing: 
  * 3 chicken thighs cooked and stewed 
  * 250 g mushrooms 
  * 2 cloves garlic 
  * 2 tbsp. chopped parsley 
  * 1 C. soup of lemon juice 
  * Black pepper to taste 
  * ½ teaspoon of salt 
  * a little nutmeg 
  * 100 g of pitted olives (I did not put any) 
  * A sliced ​​onion 
  * Half a red pepper grilled and cut into pieces (not put) 
  * a quantity of bechamel sauce (recipe below) 
  * 2 tbsp. butter 



**Realization steps**

  1. Start with the crepe dough: Mix the dry ingredients, then add the milk, eggs and melted butter and mix well with a whisk if you have one. 
  2. Let the dough rest a little while. 
  3. heat a frying pan and rub it with a little butter, 
  4. pour the amount of a small ladle and spread it well on one side, then on the other side too. and let it cool a little 
  5. In a frying pan over high heat put the butter and fry the mushrooms until it is a beautiful gilding, add the lemon juice, parsley, salt and black pepper and set aside. 
  6. now prepare the stuffing: on the other hand put some butter and fry the onions and garlic, 
  7. then add the already cooked chicken pieces too, 
  8. add the other ingredients and a bit of Béchamel sauce. 
  9. Stuff each pancake with a large spoonful of stuffing and place in a well-buttered pan, cover with a little béchamel sauce and sprinkle with grated cheese then put in a hot oven to grill for 15 to 20 minutes. minutes 
  10. serve hot with a nice salad. 



thank you for your comments, and thank you for your visits 

bonne journée 
