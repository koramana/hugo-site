---
title: my tajine of small weights with artichoke hearts
date: '2008-03-08'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/229841102.jpg
---
& Nbsp; just like a good tajine hot in these cold winter days (it's almost autumn would tell me, but my friends here ........ we can not even see the summer ??? ? !!!!!) Anyway, this morning I wanted small weights, and as I still love that there are hearts of artichokes, I disturb my husband in the middle of work, so that he bought me, I will never imagine the little weights without it. finally, all the ingredients were there, and so in the kitchen: small weights (my frozen ones) hearts of artichokes (also frozen) chicken legs & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/229841102.jpg)

nothing like a good tajine hot on these cold days of winter (it's almost autumn would tell me, but my friends here ...... .. we can not even see the summer ???? !!! !!) 

in any case, this morning I wanted small weights, and as I still love that there are hearts of artichokes, I disturb my husband at work, so he bought me, I will never imagine small weights without it. 

finally, all the ingredients were there, and so in the kitchen: 

  * small weights (my frozen ones) 
  * artichoke hearts (also frozen) 
  * chicken legs (my husband does not like meat too much, he will eventually give me a rooster) 
  * an onion 
  * 1 carrot (this must be present too) 
  * chopped parsley 
  * a bay leaf 
  * salt, pepper, paprika, coriander / garlic (my favorite spice 
  * canned tomato 
  * oil 



fry onion, in oil, add chicken legs, canned tomato, spices, let simmer a little, then add bay leaf and chopped parsley, carrot sliced, and cover all with water. 

to boil add the small weights, and just before the end of the cooking, add the hearts of artichokes cut in quarter. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/229841121.jpg)

serve well hot 

et bonne appétit 
