---
title: Rolled potatoes and sweet potato
date: '2015-02-09'
categories:
- Algerian cuisine
- diverse cuisine
- idea, party recipe, aperitif aperitif
tags:
- inputs
- Vegetarian cuisine
- Healthy cuisine
- Easy cooking
- Economy Cuisine
- Ramadan
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roul%C3%A9-de-pomme-de-terre-nawel.jpg
---
[ ![rolled potatoes](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roul%C3%A9-de-pomme-de-terre-nawel.jpg) ](<https://www.amourdecuisine.fr/article-roule-pommes-de-terre-et-patate-douce.html/roule-de-pomme-de-terre-nawel>)

##  Rolled potatoes and sweet potato 

Hello everybody, 

Here is a recipe that will easily go into the entrance during the winter, or a hearty dish in summer accompanied by a nice salad, a delicious roll of potato and sweet potato. This is a recipe from my friend from Ireland Nawel Zellouf, she has already shared with us her delicious [ tajine from monkfish to chermoula ](<https://www.amourdecuisine.fr/article-tajine-de-lotte-la-chermoula.html> "Monkfish tajine with chermoula") . Not only does she make good and delicious dishes, she also makes beautiful pictures. 

**Rolled potatoes and sweet potato**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roul%C3%A9-de-pomme-de-terre.jpg)

Recipe type:  Entrance  portions:  5-6  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients**

  * 3 potatoes 
  * ½ sweet potato 
  * ½ onion 
  * 2 small shallots 
  * Four triangles of cheese 
  * 4 eggs 
  * 1 tablespoon of sour cream (if not fresh cream and a drizzle of lemon juice) 
  * parsley 
  * ½ cube of chicken knorr (or Jambo) 
  * Salt, pepper, nutmeg 
  * special cheese sandwich or burger 



**Realization steps**

  1. grated potatoes cleaned and peeled 
  2. grated half sweet potato 
  3. finely chop onions and parsley 
  4. mix all these ingredients, with the whipped eggs, the crumbled cheese and the sour cream 
  5. season with salt, black pepper, nutmeg and half knorr cube 
  6. spread the mixture evenly, in a baking tin lined with a sheet of baking paper 
  7. cook in a preheated oven at 190 degrees C for almost 30 minutes 
  8. remove the tray from the oven, place the special sandwich moldings to cover the surface 
  9. roll with the help of the baking paper 
  10. then put back in the oven so that it takes a pretty color for 5 to 7 minutes 
  11. let cool a little before cutting and taste 



[ ![rolled potatoes](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/roul%C3%A9-de-pomme-de-terre-nawel-1.jpg) ](<https://www.amourdecuisine.fr/article-roule-pommes-de-terre-et-patate-douce.html/roule-de-pomme-de-terre-nawel-1>)

Thank you nawel Zellouf for this nice recipe. 

J’espere qu’elle vous plaira bien cette recette, et je vais attendre les photos de vos essais 
