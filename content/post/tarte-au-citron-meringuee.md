---
title: lemon meringue pie
date: '2016-09-28'
categories:
- dessert, crumbles and bars
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-au-citron-meringuee-119.CR2_thumb.jpg
---
![lemon meringuee tart 119.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-au-citron-meringuee-119.CR2_thumb.jpg)

##  lemon meringue pie 

Hello everybody, 

you want a piece, this delicious lemon meringue pie? a melting piece, acid, sweet, balanced in taste .... all the flavors in one bite. my friend when she tasted, and this is the first time she eats it, was really amazed at this marriage of sweets, and this burst of flavor. 

She is not party anyway without the recipe !!! ... 

At home, the lemon pie is almost every week at the table of the taste, but my husband prefers it without the meringue, so it's always [ tartlets with lemon ](<https://www.amourdecuisine.fr/article-25345343.html>) for him, and [ French meringue ](<https://www.amourdecuisine.fr/article-meringue-a-l-ancienne-recette-tres-facile-114688303.html>) for kids. 

this time, and because I invited my Sudanese friend, I told my husband, I'm going to make the real lemon tart, if you do not like the meringue, you can take it out of you, and you Know what? Well, he has to eat, and to love! Frankly, we will never understand their fundamentals !!! hihihihih. 

for the pasta: [ easy sanded paste ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>)   


**lemon meringue pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-au-citron-a-la-meringue-056.CR2_thumb.jpg)

Recipe type:  dessert pie  portions:  8  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients**

  * sanded paste easy to make. 
  * for lemon cream: 
  * 100 ml of lemon juice 
  * the zest of two lemons 
  * 300 ml of water 
  * 4 egg yolks 
  * 150 gr of sugar 
  * 4 tablespoons of cornflour 
  * 30 gr of butter 
  * For French meringue: 
  * 4 egg whites 
  * 250 gr of caster sugar 
  * 1 pinch of salt 



**Realization steps**

  1. prepare the shortbread: In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a dough is detached from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Darken a pie circle 28-30 cm in diameter. 
  6. Prick the bottom of the dough with a fork. 
  7. cover with a baking sheet, and fill with dry vegetables (for me it was glass tablets for vase ornament) and put in the oven 20 min at 180 ° C, then remove the legumes and the leaf and continue the cooking 10 minutes. 
  8. prepare the lemon cream: 
  9. in a saucepan place the water, the lemon juice and the cornflour. 
  10. thicken over low heat, just a little. 
  11. in a salad bowl whip the sugar and the egg yolks, add the lemon peel. 
  12. remove the pan from the heat and let cool a little, then pour a little of this mixture on the mixture of egg yolk to dilute it a little. 
  13. put all the mixture back in the pan, and cook on low heat while stirring with a wooden spoon, until you obtain a creamy cream. 
  14. remove the cream from the heat and introduce the butter into small pieces. 
  15. let cool a little, and fill the pie shell. 
  16. prepare the meringue: 
  17. whip the whites into firm snow with the pinch of salt. 
  18. Add the sugar in small amounts while continuing to beat. and until the egg whites become shiny. 
  19. decorate the pie with this meringue according to your taste. 
  20. caramelize the meringue using a blowtorch. and if he looks like mine, and does not want to light up, well put in the oven to light from above, while watching, it immediately takes the golden color, it was not even 3 minutes for me, and I almost burned the meringue, hihihihi 



[ ![lemon meringuee tart 052.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-au-citron-meringuee-052.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-au-citron-meringuee-052.CR2_2.jpg>) [ ![lemon meringuee tart 055.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-au-citron-meringuee-055.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-au-citron-meringuee-055.CR2_2.jpg>)

Do not hesitate to make this pie without the meringue, it is just as delicious. 

You can prepare the shortbread dough in advance, cook the pie crusts, and freeze them until the day you want to make tarts. 

Je vous passe ici la vidéo de la préparation de cette tarte: 
