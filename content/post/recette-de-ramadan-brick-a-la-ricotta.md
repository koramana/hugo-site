---
title: recipe of Ramadan brick with ricotta
date: '2013-06-25'
categories:
- panna cotta, flan, and yoghurt
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/brick-a-la-ricotta-016.CR2-copie-12.jpg
---
![recipe of Ramadan brick with ricotta](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/brick-a-la-ricotta-016.CR2-copie-12.jpg)

##  recipe of Ramadan brick with ricotta 

Hello everybody, 

for the story of this recipe from ramadan brick to ricotta: we prepared a little feast between girlfriend and whose theme was Lebanese cuisine, and with [ Lebanese Tabbouleh ](<https://www.amourdecuisine.fr/article-taboule-libanais-118708599.html>) I prepared these delicious ricotta bricks, with pine nuts, to be in the theme, since pine nuts, are widely used in oriental cuisine .... 

I wanted this brick recipe to be with a special cheese, that a friend had promised to bring me back, but she forgot (hihihihi, or so it was confiscated by the customs ...) 

"Never mind" ... the result is great with ricotta cheese, and during the meal, these bricks did not last long ...   


**ramadan / brick recipe with ricotta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/brick-a-la-ricotta-002.CR2_1.jpg)

portions:  4  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * leaves of bricks (for me it was the special long leaves samoussa) 
  * 250 g ricotta 
  * 1 handful of green olives pitted) 
  * Some sprigs of parsley 
  * 1 handful of pine nuts 
  * black pepper, salt 
  * nutmeg 
  * Oil for frying 



**Realization steps**

  1. In a skillet, brown the pine nuts so they are crispy. 
  2. In a bowl, mix ricotta, finely chopped parsley and sliced ​​olives 
  3. Add the pine nuts, black pepper and salt to adjust the taste. 
  4. Put some of this stuffing on the side of the bricks sheet and close to shape small triangles, stick the end with egg white.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/brick-a-la-ricotta-001.CR2_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/brick-a-la-ricotta-001.CR2_1.jpg>)
  5. Fry in a hot oil. 
  6. Serve warm accompanied by a nice salad, or a delicious [ chorba ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) . 



![recipe of Ramadan brick with ricotta](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/brick-a-la-ricotta-007.CR2_1.jpg)
