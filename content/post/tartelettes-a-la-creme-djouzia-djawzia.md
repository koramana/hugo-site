---
title: Tartlets with djouzia cream, djawzia
date: '2015-04-30'
categories:
- gateaux algeriens au glaçage
- gateaux algeriens- orientales- modernes- fete aid
tags:
- Algerian cakes
- To taste
- Cookies
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tartelette-djawzia.jpg
---
[ ![Tartlets with djouzia cream, djawzia](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tartelette-djawzia.jpg) ](<https://www.amourdecuisine.fr/article-tartelettes-a-la-creme-djouzia-djawzia.html/tartelette-djawzia>)

##  Tartlets with djouzia cream, djawzia 

Hello everybody, 

**Tartlets with djouzia cream, djawzia** : Do you like el Djouzia, or dajwzia this Constantinian mignardise that make in mouth, a texture between the softness of the marshmallow and the small sticky of the [ nougat ](<https://www.amourdecuisine.fr/article-nougat-blanc-aux-fruits-secs.html> "White nougat with dried fruits") . Personally, I love Djouzia. I remember when I was little, I waited impatiently when my grandmother came to visit us, I was just waiting for the moment when she starts unpacking her bag, to get out the bag of [ bsisa ](<https://www.amourdecuisine.fr/article-bsissa-la-tamina-de-l-est-algerien.html> "Bsissa: the tamina of eastern Algeria") , and the pretty packet of Djouzia, sometimes it was in pretty sugar mills that my mother keeps preciously until today. 

So this little cake, or these small tartlets with djouzia cream, djawzia of all the beautiful stuffed cream djouzia is a recipe of a friend **Tim De Bouj,** who generously shares this little delight with us. what is good in this recipe is that you will eat djawzia without worrying if it sticks in your hands or not, hihihi. 

**Tartlets with djouzia cream, djawzia**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tartelette-djawzia-1.jpg)

portions:  30  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** for tartlets in shortbread 

  * 250 g butter. 
  * 2 yellow and 1 whole egg. 
  * 1 teaspoon of vanilla. 
  * 2 tablespoons sugar. 
  * flour as needed. 

for mousse or djouzia cream. 
  * 3 egg whites. 
  * 1 pinch of salt. 
  * 300 g of honey (150g honey, 150g honey bee) for me I put 300g of honey El mordjen, it has the same taste as that of bee. 
  * 1 glass of sugar. 
  * 1 glass of water. 
  * 160 gr crushed nuts. 



**Realization steps** Preparation of the cream: 

  1. put the sugar and water in a saucepan and cook. 
  2. mount the whites with the pinch of salt in snow and leave aside. 
  3. after cooking the syrup add the honey and heat just a little. 
  4. pour the honey mixture syrup over the white in a small net and continue beating. 
  5. increase the speed to the maximum and continue to beat until the foam is cool and it becomes firm. 
  6. in the end add the nuts. Mix with a spatula.   
[ ![Tartlets with djouzia cream](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tartelettes-djawzia-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41669>)
  7. pour the mousse into the bottom of the tarts using a pastry bag or just a spoon 
  8. decorate with nuts and let dry overnight. 



[ ![Tartlets with djouzia cream, djawzia](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tartelettes-djawzia.jpg) ](<https://www.amourdecuisine.fr/article-tartelettes-a-la-creme-djouzia-djawzia.html/tartelettes-djawzia>)
