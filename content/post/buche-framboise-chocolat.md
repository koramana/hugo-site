---
title: raspberry buche chocolate
date: '2016-12-22'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles et barres
- Chocolate cake
- recettes sucrees
tags:
- desserts
- To taste
- Breakfast
- Ramadan
- Algeria
- Algerian cakes
- delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/buche-framboise-chocolat-21.jpg
---
[ ![chocolate raspberry buche 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/buche-framboise-chocolat-21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/buche-framboise-chocolat-21.jpg>)

##  raspberry buche chocolate 

Hello everybody, 

My friend Mina Minano liked to share with you through my blog her raspberry chocolate Bavarian recipe that she presented in the shape of a log ... A little dessert she made for her family yesterday, and she shared the most pictures here on my blog. Thank you my dear for this delight. 

a log made of a chocolate biscuit, garnished with a beautiful raspberry mousse and a raspberry insert, covered with a beautiful so girly mirror. 

I really like these bavarois all melting in the mouth, besides it's been a while since I did one, because I have more halal gelatin, and with agar agar, I do not have still tried, I think I have to put myself, because these delicious desserts I miss a lot. 

Do not forget you too, just like Mina minano, you can post your recipe on this link: [ Submit your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette>) , and if you can not place your photos, send them on this email, I'll take care of it: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

[ ![chocolate raspberry beech 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/buche-framboise-chocolat-31.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/buche-framboise-chocolat-31.jpg>)   


**raspberry buche chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/buche-framboise-chocolat-11.jpg)

portions:  8  Prep time:  60 mins  cooking:  20 mins  total:  1 hour 20 mins 

**Ingredients** for the chocolate biscuit 

  * 2 eggs 
  * 50 g of flour 
  * 50 g of sugar 
  * 50 g dark chocolate 
  * 10 g of butter 

Raspberry mousse: 
  * 330 g raspberries: 
  * 60 g caster sugar 
  * 300 g of liquid cream 
  * 60 g icing sugar 
  * 6 sheets of gelatin 
  * Raspberry insert: 
  * the rest of the raspberry puree 
  * 1 glass of water 
  * 2 tablespoons sugar 
  * 1 leaf and a half of gelatin 

ganache: 
  * 100 g of liquid cream 
  * 100 g of dark chocolate 

The mirror: 
  * 75 ml of mineral water 
  * 11 g of gelatin 
  * 150 gr of sugar 
  * 150 grams of glucose 
  * 150 gr of white chocolate 
  * 100 gr of condensed milk 
  * 5 gr of pink raspberry dye 
  * 2 g of titanium oxide or white dye powder. 



**Realization steps** chocolate biscuit 

  1. Preheat your oven to 180 ° c. 
  2. In a bain-marie, melt the chocolate with the butter. 
  3. Mount the whites in snow. 
  4. Blanch the yolks with the sugar. 
  5. Then add the chocolate, then the whites and then the flour. 
  6. Spread on a baking sheet, so you can cut 2 strips of the width and length of the mold (my mold is 35 x 8 cm, so I spread approximately 35 x 13 cm). 
  7. Bake 8 to 10 min. 
  8. Let cool and cut 2 strips, one of 8 cm (width of your mold) and you have left a small of about 5 cm. 

raspberry mousse: 
  1. Mix the raspberries until you get a puree. Put them in Chinese to remove all the small grains. Put this mashed pot in a saucepan over medium heat and add the caster sugar. 
  2. Remove from the heat at the first boil and add 4 leaves and a half gelatin previously softened in cold water. 
  3. Remove 1 small ladle of this preparation and put it in a small saucepan (it will be used for the preparation of the jelly). 
  4. Let cool to room temperature. 
  5. Add the whipped cream and add the icing sugar. Book fresh. 
  6. When the raspberry puree is cold, mix little by little in the whipped cream. 
  7. Pour half of the foam into the bottom of the mold   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/moule-a-buche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/moule-a-buche.jpg>)
  8. place the small sponge cake in the middle and cover with remaining mousse. 
  9. Reserve in the freezer. 

Raspberry insert: 
  1. Put the small saucepan with the rest of the raspberry purée, add the water, the sugar. 
  2. Off the fire add the gelatin. stir to homogenize and let cool. 
  3. Pour this jelly on the raspberry mousse when it is solidified. Return to the freezer. 

Ganache: 
  1. Bring the cream to a boil, remove from heat and add the chocolate in small pieces. 
  2. Melt for a moment and whisk until smooth. 
  3. Let cool for a moment and pour over the jelly when it is taken. Then close with the other biscuit 

the mirror: 
  1. Bring the water, sugar and glucose to a boil. 
  2. Add the milk and chocolate. 
  3. Remove from heat immediately, wait until the chocolate is melted and add the softened gelatin. 
  4. Add the colorant until you get the color you want. 
  5. Allow to thicken and cool to room temperature 
  6. Take the log out of the freezer, unmold and cover ... and let it defrost in the fridge. 
  7. decorate with colored white chocolate circles with a brush.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/deco-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/deco-chocolat.jpg>)



[ ![raspberry buche chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/buche-framboise-chocolat-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/buche-framboise-chocolat-.jpg>)
