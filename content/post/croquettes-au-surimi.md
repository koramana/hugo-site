---
title: croquettes surimi
date: '2018-05-19'
categories:
- idea, party recipe, aperitif aperitif
tags:
- Amuse bouche
- inputs
- Fast Food
- Easy cooking
- Ramadan
- Ramadan 2018
- accompaniment

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/10/croquettes-au-surimi-6.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/croquettes-au-surimi-6.jpg)

##  croquettes surimi 

Hello everybody, 

want croquettes, here is a very good recipe of croquettes surimi, what I like in these croquettes, and they are not at all fat, although they are fried .... 

So I have fun to vary the pranks, and at home we like it. 

An ideal recipe for a buffet, or on a ramadanque table, or in a picnic basket or just for a light meal.   


{{< youtube 4rVBTG0GipQ >}} 

  


**croquettes surimi**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/croquettes-au-surimi-6.jpg)

portions:  6  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 500 gr of potatoes. 
  * 10 sticks of surimi 
  * 3 servings of cheese the laughing cow 
  * 1 hard egg 
  * a few sprigs of parsley 
  * a few sprigs of coriander 
  * 2 cloves garlic 
  * salt, black pepper and a pinch of cumin. 

for breading: 
  * flour 
  * egg + 1 teaspoon of oil 
  * breadcrumbs 



**Realization steps**

  1. crumble the fingers of surimi, 
  2. peel the potatoes, cut into cubes, salt a little, and steam 
  3. crush the mashed potatoes. 
  4. add the crushed hard egg, 
  5. stir in the cheese portions, and mix well by hand 
  6. season with the spices and salt, according to your taste. 
  7. prepare sticks or fingers of almost 30 gr each   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-au-surimi-b_thumb_11.jpg)
  8. roll these sticks into the flour, then into the egg and oil mixture. 
  9. then continue the breadcrumbs in bread crumbs 
  10. place in the refrigerator for at least 1 hour. 
  11. cook in a bath of hot oil, just to have a nice color. 



![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/croquettes-au-surimi-5.jpg)
