---
title: bites of aubergines with feta cheese
date: '2013-12-07'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/amuse-bouche-aperitif-aubergine-feta.CR2_-1024x682.jpg
---
[ ![appetizer appetizer aubergine feta.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/amuse-bouche-aperitif-aubergine-feta.CR2_-1024x682.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/amuse-bouche-aperitif-aubergine-feta.CR2_.jpg>)

Hello everybody, 

do you want me to tell you the story of this recipe for feta cheese eggplant bites ??? 

Well it was during a visit of my friend Sally at home, or we are used to each of us prepare a thing, and we tasted together while chatting with friends ... 

Then she comes with these bites, when I saw, it surprised me that there was no sauce, and that in fact, all the ingredients were put on one another, then a fillet of olive oil and baked ??? 

I was expecting a dry taste in the mouth, flavors that do not get married, finally I expected everything except fall in love with this recipe, even my husband who is really someone difficult, to eat and in large quantities ... Of course Sally did not leave without the recipe, which is really easy, but too good, that I assure you .... 

**bites of aubergines with feta cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/bouchees-aux-aubergines.CR2_-1024x712.jpg)

Recipe type:  aperitif, appetizer  portions:  4  Prep time:  10 mins  cooking:  60 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 roll of pastry 
  * 1 medium eggplant 
  * ½ red onion 
  * 1 little feta cheese 
  * pitted green olives   
for me, as for Sally, we used spicy crushed olives that I had brought from Algeria. 
  * 1 whole lemon 
  * salt, 
  * olive oil 
  * basil 
  * thyme 



**Realization steps**

  1. spread the puff pastry on a not too deep muffin pan, and let the dough take the shape of the cavities   
do not rush the dough or insert it into the cavities so that it does not break. 
  2. preheat the oven to 150 degrees C. 
  3. cut the aubergine into a slice of almost 1 cm, then each slice into four or six portions. 
  4. fry the pieces of eggplant in a pan, with a little water, salt and black pepper   
it's not boiling in the water 
  5. let the pieces drip after cooking on paper towels.   
we must change the paper, because it is really necessary that eggplants disgorge this water. 
  6. When the eggplants are drained well of their cooking water, place on the puff pastry, in each cavity some pieces of eggplant. 
  7. place on top a few pieces of sliced ​​onions. 
  8. add feta, a piece of lemon, and an olive. 
  9. sprinkle over a little thyme and basil.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/apero-aubergines-et-feta.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/apero-aubergines-et-feta.jpg>)
  10. sprinkle each cavity with a drizzle of olive oil 
  11. cut the square puff pastry around each cavity with a roulette wheel. 
  12. press a little on the ingredients so that they are in the cavity. 
  13. cook for 45 to 1 hour, or until puff pastry swells well, and is golden brown. 
  14. let cool before serving 



[ ![aubergines.CR2 mouths](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/bouchees-aux-aubergines.CR2_-1024x712.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/bouchees-aux-aubergines.CR2_.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
