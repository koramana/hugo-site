---
title: Tchektchouka / Algerian cuisine / بصل بطوماطيش
date: '2012-07-24'
categories:
- verrines sucrees

---
hello everyone, a malika recipe, and malika tells us: when I'm short of ideas, or I see that there are only 45 minutes left and I did not eat anything, I run to look for some onions, a little garlic, fresh tomato, and if by chance I have pepper at home it does the trick, if I have eggs, it's even better, otherwise it will be a chakchouka very simple, and at home they like this dish, I'm doing it for the moment, but the robot will give me hellip; 

##  Overview of tests 

####  vote by post 

**User Rating:** Be the first one!  0 

Hello everybody, 

a malika recipe, and malika tells us: 

when I am short of ideas, or I see that there is only 45 minutes left and I have not done anything to eat, I run to get some onions, a little garlic, fresh tomato, and if by chance I have pepper at home it does the trick, if I have eggs, it's even better, otherwise it's going to be a simple chakchouka, and at home they like this dish, I do it for the moment, but the robot will give me a good hand if I win it. 

my ingredients: 

  * Two or three onions 
  * three tomatoes, 
  * pepper (optional), 
  * 4 eggs, 
  * garlic, 
  * 1/2 glass of water 
  * 4 tablespoons of olive oil, 
  * salt, pepper, paprika, coriander powder 
  * parsley 



**method of preparation:**

  1. Cut the onions and garlic. Sauté in a pan with olive oil over medium heat 
  2. Meanwhile, cut the tomatoes with their seeds and add them to the onions. Simmer a little for 10 minutes. 
  3. Add the spices, salt and pepper, then pour the water. 
  4. if you with peppers, clean them of pips and cut into small pieces. 
  5. Add the peppers to the sauce. Simmer for 10 minutes. Check the salt. 
  6. Then add the eggs one by one 
  7. cut the parsley, add it over 
  8. Cook the eggs in the covered chakchouka on a low heat for easy cooking. 10 minutes is normal enough. 
  9. serve well with a nice cake [ matlou3 ](<https://www.amourdecuisine.fr/article-41774164.html>)


