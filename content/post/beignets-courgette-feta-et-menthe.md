---
title: zucchini, feta and mint donuts
date: '2013-11-16'
categories:
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/beignets-de-courgettes-feta-et-menthe_thumb.jpg
---
Hello everybody, 

a really tasty recipe, you can not imagine the fondant taste of zucchini and feta cheese, under a beautiful crunchy and soft layer at the same time of the layer of donuts. 

when the flavors, it will really burst all your taste buds, a recipe that I invite you to do without hesitation, because not only it is good, but also very easy to do.    


**zucchini, feta and mint donuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/beignets-de-courgettes-feta-et-menthe_thumb.jpg)

portions:  4  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 2 zucchini not too big 
  * 100 g of feta 
  * 12 fresh mint leaves 
  * 200 g flour 
  * ½ sachet of baking powder 
  * 1 egg 
  * 220 ml of milk 
  * salt pepper 
  * 12 wooden skewers 
  * oil for cooking 



**Realization steps**

  1. Donuts Spread : 
  2. In a salad bowl, place the flour, yeast, a pinch of salt, pepper and mix. 
  3. Add the egg and milk gently to avoid lumps, until the meat becomes creamy, not too liquid and thick enough to coat the zucchini. 
  4. Allow time to prepare the stuffed zucchini. 
  5. Preparation of zucchini: 
  6. Using a thrifty cut zucchini in the direction of the length in thin slices. 
  7. Spread on a plate and salt slightly. 
  8. Cut the diced feta about 2 cm apart, then put a mint leaf on each feta dice. 
  9. Wrap each piece of cheese in a slice of zucchini and prick with a skewer gently. 
  10. Heat the oil bath in a frying pan. 
  11. Dip each roll of zucchini into the donut dough, and brown in warm oil for about 3 minutes. 
  12. Serve hot. 



you can see the video of preparation here: 

{{< youtube HQGJFGrzloY >}} 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
