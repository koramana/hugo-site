---
title: baba ghanouj
date: '2017-05-14'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- dips and sauces
- houriyat el matbakh- fatafeat tv
- vegetarian dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/baba-ghanouj-3.jpg
---
![baba ghanouj 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/baba-ghanouj-3.jpg)

##  Baba ghanouj 

Hello everybody, 

Baba ghanouj, this salad of oriental cuisine made from grilled eggplants that almost everyone likes. 

My mother always prepared this recipe, but I did not know it was called: Baba ghenouj, or aubergine caviar, for us it was a grilled aubergine salad, which we enjoyed with good [ matlou3 ](<https://www.amourdecuisine.fr/article-41774164.html>) Without the touch of sesame cream of course, because this product was not easy to find at the time. 

recipes in Arabic: 

**baba ghanouj**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/baba-ghanouj-1.jpg)

Recipe type:  salad, entree  portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 2 medium eggplants 
  * 2 tablespoons sesame cream (tahina) 
  * ½ glass of lemon juice 
  * 2 cloves garlic 
  * Salt and pepper 
  * olive oil 



**Realization steps**

  1. Wash the eggplants. grill them either in the oven or on the grill; 
  2. when they have become tender, take out of the oven and put them in a bag to sweat well. 
  3. remove the skin. 
  4. crush the aubergines with a fork. 
  5. add the sesame cream and the rest of the ingredients. 
  6. mix with a fork. 
  7. put in the serving dish, and decorate with olive oil. 



![baba ghanouj](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/baba-ghanouj.jpg)
