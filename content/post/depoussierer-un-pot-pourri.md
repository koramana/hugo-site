---
title: Dust a potpourri
date: '2007-12-22'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

---
**![bonj9](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201446331.gif) **

**To dust a potpourri, first put it in a colander.**

In a separate pot, bring water to a boil. 

Place the colander over the boiling water for a few minutes. Shake. 

The potpourri will be dusted and refreshed by the steam of the water. 

You can use the same process with dried flowers by passing them over the steam. 

** **

![bonj14](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201446501.gif)
