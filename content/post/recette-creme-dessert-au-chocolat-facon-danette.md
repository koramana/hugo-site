---
title: Danette chocolate dessert cream recipe
date: '2018-03-04'
categories:
- panna cotta, flan, and yoghurt
- sweet recipes
tags:
- Kitchen without egg
- flan
- Milking
- To taste
- Easy Dessert
- Easy cooking
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/Recette-cr%C3%A8me-dessert-au-chocolat-fa%C3%A7on-danette-683x1024.jpg
---
![Danette chocolate dessert cream recipe](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/Recette-cr%C3%A8me-dessert-au-chocolat-fa%C3%A7on-danette-683x1024.jpg)

##  Danette chocolate dessert cream recipe 

Hello everybody, 

I'm sure each of you has your own chocolate dessert recipe chocolate way danette, this little dessert, that we taste at any time and around the world, who does not know the cream danette ?! 

At home, this cream dessert chocolate way danette is always present whatever the season, whatever the occasion, my children love madly, and as you know me, I do not like the products of the trade. I do not say that I do not like this brand, but when I have the chance to reproduce a recipe, and when I know what I put on the plates of my children, I have a clear conscience. 

In addition, we must admit one thing, this recipe is really a breeze, in a jiffy, and it's already cool, ready for tasting, and the ingredients to make this chocolate dessert recipe danette way are always present at home, is not it? 

**Danette chocolate dessert cream recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/Recette-cr%C3%A8me-dessert-au-chocolat-fa%C3%A7on-danette-2.jpg)

portions:  4  Prep time:  5 mins  cooking:  10 mins  total:  15 mins 

**Ingredients**

  * 350 ml whole milk 
  * 280 ml whole cream 
  * 30 gr of cornflour 
  * 30 grams of bitter cocoa (you can put less) 
  * 90 gr of sugar 



**Realization steps**

  1. in a saucepan, place some milk, cornflour and cocoa 
  2. stir well to avoid lumps, and have a homogeneous mixture 
  3. add the remaining milk, liquid cream, and sugar 
  4. stir well and place the pan on the heat. 
  5. keep stirring constantly until the mixture thickens 
  6. pour all into verrines, let cool a little and cool until tasting. 



![Chocolate dessert recipe chocolate way danette 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/Recette-cr%C3%A8me-dessert-au-chocolat-fa%C3%A7on-danette-1-662x1024.jpg)
