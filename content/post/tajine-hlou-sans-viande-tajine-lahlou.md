---
title: tajine hlou without meat, tajine lahlou
date: '2017-05-26'
categories:
- Algerian cuisine
- Cuisine by country
tags:
- Algeria
- Morocco
- Ramadan
- Ramadan 2017
- Sweety salty
- Vegetarian cuisine
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/tajine-hlou-sans-viande-tajine-lahlou-1.jpg
---
![tajine hlou without meat, tajine lahlou 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/tajine-hlou-sans-viande-tajine-lahlou-1.jpg)

tajine hlou without meat, tajine lahlou 

Hello everybody, 

not a Ramadan without Tajine lahlou is not it? So here is the recipe for: tajine hlou without meat, tajine lahlou, and if you want other tajine recipes hlou, why not see: 

[ lham lahlou ](<https://www.amourdecuisine.fr/article-lham-l-ahlou-plat-sucree-ou-tajine-lahlou.html>)

[ tajine hlou with dates ](<https://www.amourdecuisine.fr/article-lham-hlou-bet-tmar-tajine-hlou-aux-dattes-farcies.html>)

[ tajine hlou with apples ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande.html>)

[ tajine hlou to the nudles ](<https://www.amourdecuisine.fr/article-tajine-de-nefles-farcies-aux-amandes.html>)

[ tajine hlou varied ](<https://www.amourdecuisine.fr/article-tajine-hlou-varie-sans-viande.html>)

and especially the most good: [ chbah essafra ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-chbah-essafra.html>)

I hope that you will find a recipe for you to start your Ramadan as is customary, with a sweet dish on your table. 

**tajine hlou without meat, tajine lahlou**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/tajine-hlou-sans-viande-tajine-lahlou-2.jpg)

**Ingredients**

  * 2 glasses and a half of cold water (500 ml) 
  * 1 glass of sugar minus 2 fingers 
  * prunes placed in boiling water to inflate 
  * dried apricots placed in boiling water to inflate 
  * 1 C. soup of smen 
  * 1 C. vanilla 
  * 1 small stick of cinnamon, 
  * almond flowers chbah essafra to garnish the dish. 



**Realization steps**

  1. place the cold water in a saucepan, add the smen and cinnamon 
  2. Bring to the boil, and the first bubbles introduce prunes and dried apricots without stirring with the wooden spoon, it is better to shake the whole pot, so that the prunes do not discard their colors and do not crush. 
  3. we let it boil over medium heat, as soon as we see that the prunes have inflated well, we introduce the sugar. 
  4. we let it boil again, and from the beginning boiling we introduce the almond roses, or shabh safra just so that they absorb the sauce. 
  5. we perfume with vanilla, and it's a matter of taste, you can do without it. 
  6. boil for a few minutes and add the orange blossom water last, 
  7. We remove fire. this dish can be kept without problem in the refrigerator at least 4 days. 



![tajine hlou without meat, tajine lahlou 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/tajine-hlou-sans-viande-tajine-lahlou-2.jpg)
