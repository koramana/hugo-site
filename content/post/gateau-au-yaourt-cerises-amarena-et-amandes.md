---
title: cherry and amarena cherry yogurt cake
date: '2015-05-13'
categories:
- dessert, crumbles and bars
- cakes and cakes
- recettes sucrees
tags:
- To taste
- lemons
- Cake
- Algeria
- desserts
- Soft
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/g%C3%A2teau-au-yaourt-cerises-amarena-et-amandes-1.jpg
---
[ ![cherry and amarena cherry yogurt cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/g%C3%A2teau-au-yaourt-cerises-amarena-et-amandes-1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-cerises-amarena-et-amandes.html/gateau-au-yaourt-cerises-amarena-et-amandes>)

##  cherry and amarena cherry yogurt cake 

Hello everybody, 

Looking for a good cake recipe for afternoon tea? For what not a [ Yogurt cake ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-moelleux.html> "spongy yoghurt cake") ? Does it tell you? So here is a delicious yogurt cake cherries amarena and almonds Lunetoiles. In any case the photos say everything, a beautiful texture, a good volume, only to dive his little spoon to know the taste, and I'm sure that the taste is only sublime. 

You can also see the [ cake with strawberry yoghurt ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-aux-fraises.html> "Strawberry yoghurt cake") , because it's the strawberry season, or the [ Chocolate Yogurt Cake ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-facile.html> "chocolate cake") . 

[ ![cherry and amarena cherry yogurt cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/g%C3%A2teau-au-yaourt-cerises-amarena-et-amandes-2.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-cerises-amarena-et-amandes.html/gateau-au-yaourt-cerises-amarena-et-amandes-2>)   


**cherry and amarena cherry yogurt cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/g%C3%A2teau-au-yaourt-cerises-amarena-et-amandes.jpg)

portions:  8  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 1 natural yogurt with organic whole milk (keep the pot, it serves as a measure for the other ingredients) 
  * 2 eggs 
  * cherries amarena (black cherries cherries with Lidl syrup it is the mark italiamo) 
  * ½ pot of sunflower vegetable oil 
  * 1 and a half cup of brown sugar 
  * ⅓ pot of cherry syrup 
  * 3 pots of flour 
  * 1 packet of dry yeast 
  * 1 tablespoon of cider vinegar 
  * 1 cup whole almonds with skin soaked in water several hours before 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. Drain the cherries in syrup and save the juice, set aside. 
  3. Drain the almonds and grind them in a robot. 
  4. In a salad bowl, break the eggs and add the brown sugar. 
  5. Pour the yoghurt and keep the pot, it will serve as a unit of measure (so it will be well washed afterwards). 
  6. Whisk eggs, brown sugar and yogurt well. 
  7. Add the ground almonds and mix well. 
  8. Add sunflower oil and apple cider vinegar while mixing well. 
  9. Pour the flour and the yeast, stir well. 
  10. Then ⅓ pot of cherry syrup and at the end the cherries amarena. 
  11. Pour the preparation into a jacketed mold (buttered and floured) unless it is silicone. 
  12. Bake for about 45 minutes at 180 ° C 
  13. If the cake gilds too quickly, cover it with a sheet of aluminum foil. 
  14. The cake is ready when you plant a tooth-to-heart cure. If the toothpick is dry, it is cooked. 
  15. You can serve with slices of the cake with cherry syrup. 



[ ![cherry and amarena cherry yogurt cake 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/g%C3%A2teau-au-yaourt-cerises-amarena-et-amandes-3.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-cerises-amarena-et-amandes.html/gateau-au-yaourt-cerises-amarena-et-amandes-3>)
