---
title: qalb elouz mahchi or stuffed
date: '2017-05-29'
categories:
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/qalb-elouz-mahchi-farci-1.jpg
---
![qalb elouz stuffed mahchi 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/qalb-elouz-mahchi-farci-1.jpg)

##  qalb elouz mahchi or stuffed 

Hello everybody, 

And here is the recipe for qalb elouz mahchi or stuffed that you have asked me so much, I hope you will achieve and succeed! For the base, I chose the recipe for [ qalb elouz at the lben ](<https://www.amourdecuisine.fr/article-qalb-el-louz-au-lben-babeurre-recette-inratable.html>) , a recipe that you will not miss inchallah unless you have a very poor quality of semolina. 

The most important thing is the amount of sugar, I have noticed that, depending on the quality of the semolina, sometimes you have a very good sugar taste, so exactly as it should be, but sometimes just by changing the semolina, I find that the qalb elouz is a little sweeter than usual. 

So, knowing that there is sugar in the syrup, sometimes in the dough instead of putting the three measures of sugar against the 6 measures of coarse semolina, I use 2 and a half measures, and even sometimes 2 measures and 1/4. 

So a tip, if you have a good quality meal, follow the recipe as directed, otherwise reduce the amount of sugar in the dough! 

**qalb elouz mahchi or stuffed**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/qalb-elouz-mahchi-farci-2-2.jpg)

**Ingredients**

  * 6 glass of coarse semolina (a glass of 180 ml) 
  * 3 glass of sugar 
  * 1 glass of lben buttermilk 
  * 1 glass of oil 
  * 100 ml of orange blossom water 

for the stuffing: 
  * 6 c. powdered almond 
  * 1 C. cinnamon powder 

for the syrup: 
  * 6 glasses of water 
  * 3 glasses of sugar 
  * 2 tablespoons of orange blossom water 



**Realization steps**

  1. Preheat your oven to 200 °. 
  2. Prepare the syrup by boiling it for about ten minutes. 
  3. Mix the semolina, the sugar and the salt, until no more differentiating the ingredients. 
  4. Add the lben, the oil and the orange blossom, you get a paste a little liquid. Let rest 15 min. 
  5. Meanwhile mix the ingredients of the dough. 
  6. Place half of the dough in a mold with high edges, previously oiled. 
  7. Sprinkle the stuffing and then the other half of the dough. 
  8. Equalize with your hand or spatula. 
  9. Then draw squares with a knife and place an almond on each square. 
  10. lower the temperature of the oven to 180 ° and bake for 1 hour. The cake must have a beautiful golden color, as soon as you see the edges take a brown color, do not hesitate to raise the cake higher in your oven. 
  11. Once the cake is cooked, sprinkle half the syrup.Replace in your oven but still hot, so that it absorbs the syrup.Add the rest of the syrup and let cool. 


