---
title: Many apologies my dear readers
date: '2016-07-16'
categories:
- Café amour de cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/pardon-excuse.jpg
---
![sorry-sorry](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/pardon-excuse.jpg)

Google Image 

##  Many apologies my dear readers 

Hello everybody, 

I am writing this message, to apologize for the two newsletters sent yesterday and the day before and which brought you to pages that were not found. 

I apologize for this inconvenient, The first after sending the newsletter about the recret: 

[ Half melting moons with sesame seeds ](<https://www.amourdecuisine.fr/article-demi-lunes-fondantes-aux-grains-de-sesame.html>)

This article was supposed to be online on July 15th, and by mishandling, it was online on the 14th, I put the article back in draft, without knowing that the newsletter was already gone. 

The second: [ Soup with a grimace ](<https://www.amourdecuisine.fr/article-soupe-de-grimace.html>) , an article of revolt of bloggers, after the unacceptable flight of our recipes to all bloggers by a phony website .... This article also appeared on July 15th, then we had to postpone it to July 16th because of all the events happening around us. 

Having postponed these two articles to later dates meant that you could not find pages that could not be found. I had to explain this to you, because I received more than 100 emails which I could not answer, because I go on vacation tomorrow. 

I say thank you for following me, and stay true to my blog. 

Soulef ( Amour de Cuisine) 
