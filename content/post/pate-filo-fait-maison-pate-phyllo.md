---
title: homemade filo paste pate phyllo
date: '2017-06-07'
categories:
- bakery
- crepes, donuts, donuts, sweet waffles
- recipes of feculents
tags:
- Ramadan
- Ramadan 2016
- Ramadan 2017
- Morocco
- Algeria
- pastilla
- Based

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/06/pate-filo-fait-maison-pate-phyllo.jpg
---
![homemade filo paste pate phyllo](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/pate-filo-fait-maison-pate-phyllo.jpg)

##  homemade filo paste pate phyllo 

Hello everybody, 

Are you like me who still loves home? Today we go for a homemade recipe of filo paste, or pate phyllo, a dough that I bought a lot before, but I had a lot of disappointments! 

But since I discovered no recipe for filo dough or pate phyllo, since that day I always do it at home. Even if the process will parraitre long, well it is not at all, because the dough is so flexible that spreading becomes a breeze. 

A dough that does not need rest time, or a lot of techniques, you just have to have your roll pastry and voila! 

**homemade filo paste pate phyllo**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/pate-phyllo-fait-maison-pate-filo.jpg)

**Ingredients**

  * 2 cups (250 mL) all-purpose flour 
  * 2 tbsp. coffee baking powder 
  * ½ teaspoon of salt 
  * ⅓ cup of unsalted butter 
  * ¾ cup warm milk 
  * 1 teaspoon white vinegar 
  * cornflour to spread the filo paste. 



**Realization steps**

  1. In a bowl, mix 2 cups flour, baking powder, salt, unsalted butter, milk and vinegar. 
  2. Knead for 5 minutes, or 10 minutes by hand. 
  3. Divide the dough into 4 pieces and cut each piece over 3 to have 12 pieces 
  4. Form the dough into balls. Roll each piece to almost 1 mm thick 
  5. Stack the 12 circles with 1 tbsp. Tea corn starch between each layer. 
  6. in the end, roll with the pastry roller from the center to the sides to refine the phyllo dough. Roll the car carefully and apply even pressure to the dough. Flip and stretch the short pieces with your hand. 
  7. When the dough is ready, wrap it with plastic and refrigerate or freeze it. 



![homemade filo paste or phyllo pate](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/pate-filo-ou-pate-phyllo-fait-maison.jpg)
