---
title: Log praline with caramelized hazelnut chips
date: '2015-12-28'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- biscuits
- Biscuit Roll
- Chiffon cream
- Rolled
- Holidays
- Dessert
- New Year

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/b%C3%BBche-pralin%C3%A9e-aux-%C3%A9clats-de-noisettes-caram%C3%A9lis%C3%A9es.jpg
---
[ ![Log praline with caramelized hazelnut chips](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/b%C3%BBche-pralin%C3%A9e-aux-%C3%A9clats-de-noisettes-caram%C3%A9lis%C3%A9es.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/b%C3%BBche-pralin%C3%A9e-aux-%C3%A9clats-de-noisettes-caram%C3%A9lis%C3%A9es.jpg>)

##  Log praline with caramelized hazelnut chips 

Hello everybody, 

You can see a little video that Nour-riture realized to show us how she made the decoration with white caramel. 

{{< youtube Qy_X0_6zMVI >}} 

Do not forget too, you can share your recipes on my blog by clicking on the link: [ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette>)

[ ![praline log with caramelized hazelnut chips 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/b%C3%BBche-pralin%C3%A9e-aux-%C3%A9clats-de-noisettes-caram%C3%A9lis%C3%A9es-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/b%C3%BBche-pralin%C3%A9e-aux-%C3%A9clats-de-noisettes-caram%C3%A9lis%C3%A9es-2.jpg>)   


**Log praline with caramelized hazelnut chips**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/b%C3%BBche-pralin%C3%A9e-aux-%C3%A9clats-de-noisettes-caram%C3%A9lis%C3%A9es-300x205.jpg)

portions:  6  Prep time:  60 mins  cooking:  20 mins  total:  1 hour 20 mins 

**Ingredients** for 6 persons : For the rolled biscuit: 

  * 180g of eggs (about 4 eggs) 
  * 60g of sugar 
  * 20g of hazelnut oil 
  * 20g of acacia honey 
  * 50g of flour 
  * 1g baking powder (baking powder) 

For the praline (I used that of the trade) 
  * 80g caster sugar 
  * 2g of fleur de sel 
  * 120g roasted hazelnuts (without skin) 

For pastry cream 
  * 250g of whole milk 
  * 1 vanilla pod 
  * 60g of egg yolk (about 4) 
  * 50g of sugar 
  * 20g of cornstarch 
  * 1 pinch of fleur de sel 

For caramelized hazelnuts (I used those from the trade) 
  * 150g of sugar 
  * 35g of water 
  * 300g roasted hazelnuts (without skin) 
  * 15g half salted butter 

For creamed praline cream 
  * 150g soft soft butter (put half only) 

For finishing 
  * 20g of icing sugar 
  * White chocolate   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ingredient-de-la-buche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ingredient-de-la-buche.jpg>)

white caramel: 
  * sugar 
  * broken pistachios, almonds, walnuts and hazelnuts   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/caramel-cuit-a-blanc.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/caramel-cuit-a-blanc.jpg>)



**Realization steps** Step 1: the biscuit 

  1. Heat in a bain-marie 4 eggs and sugar, bring to 60 ° C. Off the heat, whisk for a few minutes until the temperature drops to 35 degrees and the mixture becomes airy 
  2. Mix with flour and baking powder (sieved) with a whisk 
  3. Heat the oil and honey at 40 ° (you can use the microwave) and then add it to the preparation 
  4. Dress in a frame (not buttered) placed on a sheet of aluminum foil 
  5. Bake about 20 minutes at 180 degrees 
  6. Decadrate the cookie while it is hot and cool to room temperature   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/biscuit-roul%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/biscuit-roul%C3%A9.jpg>)

Step 2: hazelnut praliné 
  1. In a saucepan, cook the sugar until it is amber and add the fleur de sel 
  2. Pour the caramel over the hazelnuts and mix well 
  3. Cool a small time at room temperature on a baking sheet 
  4. When the mixture is lukewarm, crush all in the blender to obtain praline (semi-liquid consistency) 

Step 3: the caramelized hazelnuts 
  1. In a saucepan bake the sugar with the water at 115 ° 
  2. Pour the hazelnuts into the sugar syrup and let them caramelise over medium heat while stirring constantly 
  3. Add the half-salted butter 
  4. Reserve at room temperature 

Step 4: The praliné muslin cream 
  1. Boil the milk with the vanilla bean and a pinch of salt 
  2. Whisk the 4 egg yolks, sugar and cornstarch 
  3. Add the vanilla milk mixture and boil until the mixture thickens, to obtain a pastry cream 
  4. Keep cold with a film of film on contact   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-patissiere.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-patissiere.jpg>)
  5. As soon as the custard is warmed, whisk it with the soft butter and praline 
  6. Book   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-mousseline-pralin%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/creme-mousseline-pralin%C3%A9.jpg>)

Step 6: Finishing 
  1. Cut the biscuit on a size of 20x25 cm by removing the small skin 
  2. Arrange on a small piece of cream praline cream 
  3. Sprinkle with caramelized hazelnut chips 
  4. Roll gently 
  5. Book a night in the fridge   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/remplissage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/remplissage.jpg>)

Step 7: Final training 
  1. Smooth the chilled log with the extra praliné muslin cream 
  2. Cover with caramelized hazelnut chips 
  3. Sprinkle with icing sugar 
  4. melt the white chocolate, shape it so that it has 2 rectangular bars of the same size as the log 
  5. when the chocolate is hard, garnish the sides of the log with it.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dressage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dressage.jpg>)



[ ![praline log with caramelized hazelnut chips 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/b%C3%BBche-pralin%C3%A9e-aux-%C3%A9clats-de-noisettes-caram%C3%A9lis%C3%A9es-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/b%C3%BBche-pralin%C3%A9e-aux-%C3%A9clats-de-noisettes-caram%C3%A9lis%C3%A9es-1.jpg>)
