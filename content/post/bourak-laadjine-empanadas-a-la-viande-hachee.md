---
title: Bourak laadjine / Empanadas with chopped meat
date: '2014-06-20'
categories:
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourak-laadjine-039.CR2_1.jpg
---
![Bourak laadjine / Empanadas with chopped meat](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourak-laadjine-039.CR2_1.jpg)

##  Bourak laadjine / Empanadas with chopped meat 

Hello everybody, 

A special Ramadan Algerian starter, delicious, crispy with a nice ground meat stuffing, known as Bourak laadjine, or bourek with homemade dough, which looks a lot like empanadas ... 

I made you a detailed video of the preparation of this recipe, and I hope you enjoy it.   


**Bourak laadjine / Empanadas with chopped meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourak-laadjine-039.CR2_-300x200.jpg)

portions:  6  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** dough: 

  * 3 measure of flour (a glass = 200ml) 
  * 1 measure of semulin 
  * 1 eggs 
  * ½ measure of oil. 
  * salt 
  * a little water 

prank call: 
  * 300 gr of minced meat 
  * 1 onion. 
  * 2 cloves garlic 
  * 2 tablespoons of oil 
  * salt, black pepper, cinnamon, cumin, coriander powder 
  * 2 eggs 

Oil for frying 

**Realization steps**

  1. Prepare dough: Mix semolina, flour and salt. Add the oil and rub between the hands to impregnate well. 
  2. add the egg and mix again. 
  3. Moisten with water gradually until a smooth, supple paste is obtained. Let it rest. 
  4. Prepare the stuffing: In a skillet, pour the oil, add the onion and garlic. 
  5. when the onion turns a nice color, add the minced meat with the spices and a little water. 
  6. Let it come back until the meat is cooked. Add parsley and beaten eggs, cook a little and remove from heat. 
  7. spread the dough on a floured work surface until it is 1 cm thick. 
  8. place a spoon of stuffing, not too close to the edge. 
  9. close the dough and pinch the good to enclose the stuffing. 
  10. cut the slippers with the roulette.and pinch with a fork. 
  11. fry the slippers in a hot oil bath and serve the lukewarm. 



![Burak-laadjine-039.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourak-laadjine-039.CR2_1.jpg)
