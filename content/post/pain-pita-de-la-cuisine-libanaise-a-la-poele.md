---
title: pita bread from the Lebanese cuisine at the stove
date: '2017-01-18'
categories:
- boulange
- Mina el forn
- pain, pain traditionnel, galette
tags:
- East
- Easy cooking
- Algeria
- Arabic bread
- Ramadan
- Bakery
- Kitchenaid

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/01/pain-pita-de-la-cuisine-libanaise-3.jpg
---
![pita bread from Lebanese cuisine 3](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/pain-pita-de-la-cuisine-libanaise-3.jpg)

##  pita bread from the Lebanese cuisine at the stove 

Hello everybody, 

The pita breads are always present at home, my children love to eat their sandwich made with pita breads of Lebanese cuisine ... today my pita bread, my Lebanese breads or my pita breads of the Lebanese cuisine are cooked in the pan . 

You can also see another version, the whole wheat flour pita breads, but baked this time, a delight! 

**pita bread from the Lebanese cuisine at the stove**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/pain-pita-de-la-cuisine-libanaise-5.jpg)

portions:  8  Prep time:  40 mins  cooking:  5 mins  total:  45 mins 

**Ingredients** For leaven: 

  * 1 C. dried baker's yeast 
  * 250 ml warm water 
  * 180 g. White flour 

for the continuation: 
  * 2 tablespoons of oil (I use olive oil) 
  * 1 C. salt 
  * 180 g. White flour 



**Realization steps** Start by preparing the leaven: 

  1. In the bowl of the robot, place the yeast baker, add warm water (not hot) and flour 
  2. mix with the spatula, and add the first part of the flour. 
  3. let the yeast react and form bubbles (at least 20 minutes away from drafts). 

preparation of the pita paste: 
  1. place the bowl of the mess in the appliance. 
  2. add the oil and the salt, 
  3. turn on the kneader and add the flour. 
  4. knead for at least 6 minutes for a smooth, smooth dough. 
  5. place the dough in oiled salad bowl, brush the surface with oil so as not to have a dry crust. 
  6. let rise between 1h30 and 2h until the double dough volume. 
  7. degas the dough on a floured space, and form 8 balls of the same size. 
  8. place them as you go on a floured plate. 
  9. cover and let stand 30 minutes. 
  10. take each ball, spread it to almost 3 mm thick and let stand 5 minutes   
it is very important that the dough disk rest at least 5 minutes before cooking to have well inflated pitas. 
  11. cook the pitas in a well-oiled pan or pancake pan. 
  12. cook the Lebanese pita bread on the first side until you have bubbles, then turn on the other side. 
  13. do not hesitate to turn the bread on both sides, you will see, that the bread is well inflated, allowing a uniform cooking with steam ...   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/pain-pita-de-la-cuisine-libanaise-1.jpg)
  14. Pita breads will deflate on cooling, which is normal. 
  15. just let it cool and eat sandwiches or to accompany your dishes. 
  16. you can heat them before serving, in the toaster. 



![pita bread from Lebanese cuisine 4](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/pain-pita-de-la-cuisine-libanaise-4.jpg)
