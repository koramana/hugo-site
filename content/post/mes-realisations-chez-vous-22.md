---
title: my achievements at home 22
date: '2012-07-30'
categories:
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/77238752_p1.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

here is a nice selection of recipes from my blog, tried by my faithful readers, as well as my blogger friends. 

I was very happy to receive all your photos on my email [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)

and if you too, you have tried one of my recipes, it will be nice to share with us the photo. 

if i forgot one of the achievements you sent me on email, please remember me. 

![IMG_0823](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/77238752_p1.jpg)

merci pour ce merveilleuse recette 🙂 c’est testé et approuvé 

![SAM_2483](https://www.amourdecuisine.fr//import/http://storage.canalblog.com/84/40/820201/)

![pomme_de_terre_chermoula](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/pomme_de_terre_chermoula1_31.jpg)

[ batata charmoula ](<https://www.amourdecuisine.fr/article-batata-mchermla-pommes-de-terre-a-la-charmoula-108433526.html>) in [ Samar ](<http://mesinspirationsculinaires.over-blog.com/>)

[ chicken stuffed with meat ax ](<https://www.amourdecuisine.fr/article-blanc-de-poulet-farci-a-la-viande-hachee-108485423.html>) , at Lady D's place 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/DSC046941.jpg)

![Potato-parsley-spinach 4290](http://img.over-blog.com/500x500/5/51/86/36/Gratin-de-pommes-de-terre-epinards/Gratin-de-pommes-de-terre-epinards-4290.JPG)

[ potato gratin with cream of spinach ](<https://www.amourdecuisine.fr/article-gratin-de-pommes-de-terre-a-la-creme-d-epinards-107627554.html>) at Vero 

![Photo 1189](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/Photo-11891.jpg)

![mderbel cake with jam2w](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mderbel-gateau-a-la-confiture2w1.jpg)

![DSC00485.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/DSC004851.jpg)

[ khobz dar without fluffing ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) in [ Celine kitchen ](<http://bienvenuedansmacuisineceline.over-blog.com/> "Celine")

![2012-07-28 11.44.05](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/2012-07-28-11.44.051.jpg)

[ Basboussa ](<https://www.amourdecuisine.fr/article-basboussa-gateau-de-semoule-patisserie-algerienne-108142909.html>) at Rocha 

[ kalb elouz ](<https://www.amourdecuisine.fr/article-35768313.html>) at Rocha 

[ tuna maakouda ](<https://www.amourdecuisine.fr/article-maakouda-au-thon-45819035.html>) at Kenza 

[ matlouh ](<https://www.amourdecuisine.fr/article-matlouh-a-la-semoule-et-farine-108390194.html>) at kenza A 

[ breads with olives ](<https://www.amourdecuisine.fr/article-pains-aux-olives-cuisine-algerienne-108119096.html>) at Kenza A 

[ buns with chopped meat, ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee-96050112.html>) at Slom DDD 

[ Zaalouk ](<https://www.amourdecuisine.fr/article-zaalouk-d-aubergines-107254425.html>) , at Angel's place 

[ chicken tajine with peas and eggs ](<https://www.amourdecuisine.fr/article-tajine-de-poulet-aux-petits-pois-et-aux-oeufs-108145162.html>) at Angel's place 

[ khobz edar ](<https://www.amourdecuisine.fr/article-khobz-dar-108029095.html>) at Angel's place 

[ magic cake ](<https://www.amourdecuisine.fr/article-gateau-magique-au-chocolat-et-creme-caramel-107207656.html>) at Leila E 

[ Saudi brioche ](<https://www.amourdecuisine.fr/article-28192543.html>) at Samar er Shainez 

[ Chicken nuggets ](<https://www.amourdecuisine.fr/article-croquettes-de-poulet-107257102.html>) chez Samar et Shainez 
