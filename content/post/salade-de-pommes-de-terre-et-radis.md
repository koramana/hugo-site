---
title: Potato and radish salad
date: '2014-12-22'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-pomme-de-terre-radis-et-son-coulis-020_thumb1.jpg
---
##  Potato and radish salad 

Hello everybody, 

Here is a very tasty [ salad ](<https://www.amourdecuisine.fr/categorie-10678933.html>) that I prepared for noon, a **potato radish salad, with a delicious coulis of potatoes** . it is to accompany the [ Chicken couscous ](<https://www.amourdecuisine.fr/article-couscous-au-poulet-103194885.html>) which remained of the day before. 

This salad is full of crunchy flavors, especially with this creamy sauce that has garnished it. 

**Potato and radish salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-pomme-de-terre-radis-et-son-coulis-020_thumb1.jpg)

portions:  2  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 2 medium sized potatoes 
  * 2 medium shallots 
  * 8 radishes 
  * some lettuce leaves 
  * 1 teaspoon mustard 
  * the juice of 1/2 lemon 
  * 2 tablespoons of olive oil 
  * salt pepper 
  * 1 cup of oregano 



**Realization steps**

  1. the method: 
  2. peel the potatoes and cut them into cubes of medium size 
  3. boil them in salted water 
  4. when the potatoes are tender, drain and keep their water. 
  5. in the bowl of a blender, take the equivalent of half a potato (cooked cubes) 
  6. add the biker, olive oil, salt, lemon juice, black pepper, oregano, and mix. 
  7. add the water of the potato, gently to this mixture while mixing, to have a smooth sauce. 
  8. cut radishes and onions into thin slices 
  9. cut lettuce 
  10. mix everything in a salad bowl 
  11. and decorate with the potato coulis. 



thank you for your visits and comments 

bonne journee 
