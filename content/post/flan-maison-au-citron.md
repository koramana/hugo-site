---
title: Lemon homemade flan
date: '2011-04-16'
categories:
- crepes, waffles, fritters

---
this is what I prepared with the salted butter caramel cream I made last time, a real treat, we really enjoyed it. 

Ingredients: 

  * 2 eggs 



Beat the eggs well with the salted butter caramel recipe [ right here ](<https://www.amourdecuisine.fr/article-26082242.html>) add milk and lemon juice. 

prepare a caramel sugar (mine I prepared with brown sugar) cover the bottom of ramekins with baking caramel, put the mixture in, and place all in a bain-marie 

and bake it preheated to 180 degrees 

cook for 30 minutes or according to your oven, remove and let cool before putting in the fridge. 
