---
title: rice croquette stuffed with gruyere or maakoda with rice
date: '2011-08-21'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/croquette-de-riz1.jpg
---
if you like this recipe vote: 30 here is hello everyone, here is another recipe participating in the contest "ramadanque entrees" do not forget there are only 9 days, because the last deadline will be August 30, so if you have recipes and you want to participate in the contest, I'm waiting for your recipes, thank you. So here is the recipe of Houaria, a reader of my blog, that I thank for the recipe, and that I believe according to the ingredients, must be a good recipe: & nbsp; & Nbsp; a glass of rice that bakes very quickly and deforms and & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.8  (  4  ratings)  0 

![croquette de riz.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/croquette-de-riz1.jpg)

if you like this recipe vote: 30 a [ right here ](<https://www.amourdecuisine.fr/article-concours-les-entrees-ramadanesques-le-vote-83357324.html>)

Hello everybody, 

here is another recipe to participate in the contest "ramadanque entrees" do not forget there are only 9 days left, because the last deadline will be August 30, so if you have recipes and want to participate in the contest, I wait your recipes, thank you. 

So here is the recipe of Houaria, a reader of my blog, that I thank for the recipe, and that I believe according to the ingredients, must be a good recipe: 

  * a glass of rice that is cooked very quickly and deforms and makes a paste 
  * 3 garlic cloves crushed 
  * salt 
  * pepper 
  * a little chopped parsley 
  * a little saffron 
  * grated gruyere for stuffing and decorating for breading: 
  * 1 or 2 egg whites 
  * chaplure 


  1. cook the rice until it becomes a dough to let cool a long time to the refregerator ... 
  2. add salt, pepper, garlic, saffron, mix well 
  3. form balls and make a hole in the middle and stuff the guyere 
  4. to flatten the ball, to pass to the egg white then to the chaplure to finish the quantity of the mixture and to fry 
  5. at the end put some gruyere gratée on the croquettes and go under the oven rack to melt the gruyere 
  6. decorate with ketchup and served with lettuce or according to taste 



good tasting, and thank you very much houaria for your participation, and good luck in the contest 

![Contest-Ramadan-in-Soulef.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/Concours-ramadan-chez-Soulef11.jpg)

[ COMPETITION OF RAMADANESQUE ENTRANCE ](<https://www.amourdecuisine.fr/article-prolongation-du-concours-entrees-ramadanesques-80041938.html>)

WHEN THE PARTICIPATING RECIPES ARE THE SUMMARY 

[ SUMMARY "RAMADANESQUE ENTRANCES", PARTICIPATIONS ](<https://www.amourdecuisine.fr/article-recapitulatif-entrees-ramadanesques-les-participations-78554354.html> "recapitulatory")

if you have already sent me your recipe and it is not on the recap, contact me, thank you. 

bonne journee et saha randankoum 
