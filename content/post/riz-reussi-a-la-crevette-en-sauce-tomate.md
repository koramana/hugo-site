---
title: Successful rice with shrimp in tomato sauce
date: '2017-01-28'
categories:
- cuisine algerienne
- cuisine diverse
- Cuisine par pays
- recette a la viande rouge ( halal)
- riz
tags:
- Algeria
- Full Dish
- Ramadan
- dishes
- Healthy cuisine
- Ramadan 2107
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/01/riz-aux-crevettes-en-sauce-tomate-4.jpg
---
![rice with shrimps in tomato sauce 4](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/riz-aux-crevettes-en-sauce-tomate-4.jpg)

##  Successful rice with shrimp in tomato sauce 

Hello everybody, 

Today I share with you the recipe for Rice made with shrimp in tomato sauce, a recipe that many of my readers asked me, especially especially how to cook rice successfully. So for a recipe from **successful rice** , I share with you the video of realization of this delicious dish. 

I will try to explain how I can recipe. I hope it will help you. To you then my recipe for Rice made with shrimp in tomato sauce. 

Knowing that for this recipe, I use basmati rice, I use the pressure cooker for this cooking, but you can use a pot with a glass lid, to monitor the cooking. 

{{< youtube VdRT7ESxhR4 >}} 

**Successful rice with tomato sauce with shrimps**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/riz-aux-crevettes-en-sauce-tomate-5.jpg)

portions:  6 

**Ingredients** For shrimps in tomato sauce: 

  * 2 tbsp. tablespoon of olive oil 
  * ½ onion 
  * 2 cloves garlic 
  * 2 bay leaves 
  * some branches of thyme 
  * 500 gr of shrimp 
  * 500 grs of beautiful red tomatoes (I use a canned tomato) 
  * salt, black pepper, curry, ginger, mix: Garlic / coriander powder 
  * fresh coriander 

For rice: 
  * 2 glasses and a half of rice (240 ml glass) 
  * 2 tbsp. oil soup 
  * ¼ onion 
  * salt, black pepper, thyme, bay leaf, little cinnamon stick, 
  * coriander seeds, Kebebe seeds, cardamom seeds 
  * 1 small piece of star anise 



**Realization steps** prepare shrimp in tomato sauce: 

  1. defrost the shrimp in a pan, and sauté a little. 
  2. remove the shrimp and place in a bowl 
  3. fry in the cooking oil chopped onion 
  4. add the bay leaves and thyme. 
  5. add the chopped tomato, crushed garlic, and season your sauce. 
  6. let reduce the sauce, then add the shrimp and coriander chopped. 
  7. let it come back a little, then remove from the fire. 

rice preparation 
  1. wash the rice until the water is clear   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/2011-04-26-riz-reussi1_thumb1.jpg)
  2. in a casserole place the oil and add all the spices in seeds. 
  3. add the chopped onion and garlic, sauté a little 
  4. now drain the rice well, add it and sauté 
  5. add the warm water and cover the rice, the water must be higher than the rice of at least 1 cm, 
  6. close the casserole and cook for 10 minutes. 
  7. present the rice garnished with shrimp in sauce and enjoy. 



![Successful rice with shrimp in tomato sauce 1](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/riz-reussi-027_thumb1.jpg)
