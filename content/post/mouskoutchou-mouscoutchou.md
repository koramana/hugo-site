---
title: mouskoutchou / mouscoutchou
date: '2012-07-22'
categories:
- crepes, beignets, donuts, gauffres sucrees
- gateaux algeriens frit

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mouscoutchou-11.jpg
---
![mouscoutchou-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mouscoutchou-11.jpg) ![mouskoutchou.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mouskoutchou1.jpg)

##  mouskoutchou / mouscoutchou 

here is the mouskoutchou cake that my husband loves the most, if I am, it's going to be this cake every day, no need for bavarois, or brownies, or cakes with pastry cream ...... which does not help me, for feed my blog ... .. hihihihi 

and because I make this cake every day, I never put the recipe, nor took pictures .... but at the request of my sister-in-law, who also loves her, I will publish it to you.   


**mouskoutchou / mouscoutchou**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mouscoutchou-11.jpg)

portions:  8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 4 eggs 
  * the zest of 1 lemon 
  * 1 cup of vanilla coffee 
  * 150 gr of sugar 
  * 3 tablespoons of milk 
  * 2 tablespoons of baking powder 
  * 150 gr of flour 
  * 3 tablespoons of oil 



**Realization steps**

  1. preheat oven to 170 degrees C 
  2. separate the egg white from the yolk, in a bowl beat the egg white with a pinch of salt, in firm snow, add 2 cases of sugar, and continue beating. 
  3. in another bowl, beat the eouf yellow, add the lemon zest and vanilla, then add the sugar, be careful this step is important, because putting the sugar directly on the egg yolk, give this bad smells on the eggs, but the fact of first beating the eouf yellow then after adding the sugar will make this bad smell disappear (the sulfur is released from the egg yolk when it is all alone) 
  4. add the milk, then with a spatula, add the flour and the yeast and mix, finally add the oil, add it well to the mixture, add 2 tablespoons of egg white, mix quickly to lighten the mixture, then add the rest egg white, and delicately incorporate it so as not to break it, and to have a well-aired cake. 
  5. flour and butter a mold, for me it was a koglouf mold. 
  6. pour your mixture in it, and place in the oven for 30 min. 
  7. my oven I lit only bottom, it allows the muslin cake to inflate well. 



![mouskoutchou-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mouskoutchou-11.jpg) ![mouscoutchou-2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mouscoutchou-21.jpg) ![mouscoutchou-3.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mouscoutchou-31.jpg) ![muskoutchou 3](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mouskoutchou-31.jpg) ![mouscoutchou-4.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mouscoutchou-41.jpg)

let cool a little before unmolding. 

if you like the taste of chocolate you can decorate your cake with a delicacy. 

good tasting 

I also propose: 

![https://www.amourdecuisine.fr/wp-content/uploads/2012/07/cholate-cake-609_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/cholate-cake-609_thumb1.jpg)

###  [ le mouskoutchou au chocolat ](<https://www.amourdecuisine.fr/article-38356047.html> "chocolate mouskoutchou")
