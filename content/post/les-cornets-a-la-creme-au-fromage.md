---
title: cornets with cheese cream
date: '2011-08-15'
categories:
- recettes patissieres de base

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/cornets-au-fromage1.jpg
---
![cornets with cheese cream](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/cornets-au-fromage1.jpg)

##  cornets with cheese cream 

Hello everybody 

Here are delicious little cornets: corn crisps from Kitchen 2010, a blog reader, who had participated with this recipe in an old game on my blog. 

I love the entrances in the shape of cornets it's always nice, our side hell is always present to remind us how much we enjoyed ice cream cones. 

the recipe is super simple and I hope that you will be trying to achieve it.   


**cornets with cheese cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/cornets-au-fromage.jpg)

portions:  8 

**Ingredients**

  * 1 roll of puff pastry, 
  * mussels in the form of a cone (found at markets), 
  * 1 egg, 

cheese cream: 
  * 2 eggs, 
  * 2 to 3 tablespoons maizena, 
  * ¼ l milk, 
  * salt pepper, 
  * 4 cheeses portions 
  * grated cheese. 



**Realization steps**

  1. Lower the roll of puff pastry on a floured work surface 
  2. cut it into strips of 1 cm, 
  3. roll up each strip on a mold that has the shape of a cone without space and of course without covering the bottom of the mold. cast them with the beaten egg and arrange them vertically in a tray, leather in a furnace at 200º 10 to 15 mn maximum, (one supervises the cooking). To leave the oven, to let cool and demoulder carefully not to break them. 
  4. Beat the eggs with the maizena, salt and pepper, add the milk, pour into a saucepan and cook, add the cheese, serving a portion until the cream is thick. sometimes. 
  5. Fill the cornets with cream cheese, line the edges with grated cheese, serve on a bed of lettuce and serve. 
  6. Note: you can stuff stuffed chicken cones or shrimp. 
  7. you can also add any kind of cheese to the cheese cream. 


