---
title: hazelnut chocolate bites
date: '2012-06-12'
categories:
- confitures et pate a tartiner

---
hello everyone, participation in the contest "kitchen express" continues, and there are only 3 days left, to put online the list of participating recipes online .... today it is a sweet recipe that comes from a reader "fatima B", and they are very pretty it is chocolate and hazelnut bites. & Nbsp; Ingredients: 100 gr of hazelnut powder 40 gr of sugar 1 egg white apricot jam ball chocolate vermicelli coconut preparation: Preheat the oven to 170 ° C. In a salad bowl, mix by hand the hazelnut powder, the sugar & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.45  (  1  ratings)  0 

the entries for the contest "cuisine express" continue, and there are only 3 days left, to put online the list of participating recipes online .... 

today it is a sweet recipe that comes from a reader "fatima B", and they are very pretty it is chocolate and hazelnut bites. 

Ingredients: 

  * 100 gr of hazelnut powder 
  * 40 gr of sugar 
  * 1 egg white 
  * apricot jam 
  * ball chocolate vermicelli 
  * coconut 



preparation: 

  1. Preheat the oven to 170 ° C. 
  2. In a salad bowl, mix by hand the hazelnut powder, the  sugar and egg white. 
  3. Shape balls and place them on a plate with paper  cooking. 
  4. Cook for 10 to 15 minutes maximum. 
  5. The bites are cooked when the bottom is golden. 
  6. Coat with apricot jam and then chocolate vermicelli or  coconut or spread. 
  7. They can be kept for several days in an airtight box. 


