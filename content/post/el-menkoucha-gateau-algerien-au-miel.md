---
title: el menkoucha algerian cake with honey
date: '2015-11-17'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- sweet recipes
tags:
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake
- Almond

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/menkoucha-8.jpg
---
[ ![menkoucha 8](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/menkoucha-8.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/menkoucha-8.jpg>)

##  el menkoucha algerian cake with honey 

Hello everybody, 

El Menkoucha is a delicious and super nice Algerian cake with honey. The menkoucha, or that means the pinch cake, looks a bit like the [ baklawa ](<https://www.amourdecuisine.fr/article-baklawa-constantinoise.html>) but here we only realize two layers of dough in their hearts a beautiful layer of almond stuffing. It looks like [ makrout sniwa ](<https://www.amourdecuisine.fr/article-makrout-sniwa-ou-makrout-facon-baklawa.html>) except that here the dough is flour-based. 

The cake is very easy to realize, the only thing difficult is to pinch the cake and try to keep the line, hihihihi. 

I already had the chance to taste this cake when my little sister had realized it during the past Aid Day. This cake is just super melting in the mouth, and full of crispy. 

You can keep the cakes in airtight boxes. For my pastries and honey cakes, I store them in baking tins to keep them from dust and to prevent the cakes from drying out. By storing them in boxes like that, I place between each layer of cake a sheet of aluminum, so that the shape of the cakes remains intact. I'm doing that with my [ ghribia ](<https://www.amourdecuisine.fr/article-ghribiya-algeroise-ghribia-a-l-huile-gateau-algerois.html>) , [ shortbread ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-fraises.html>) , or any other cake ... It's a way to preserve your cake pieces without damage, your guests will never know that your cake dates from a few days ago. 

The recipe this time is made by my friend and sister Samia Z ... She still made beautiful pictures despite using a mobile phone. A recipe she found on the forum djelfa, recipe by Oum Amani. 

**el menkoucha algerian cake with honey**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/menkoucha-51.jpg)

portions:  40  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** for the pasta: 

  * 250 gr of melted butter and at room temperature 
  * 500 gr of flour 
  * 1 small egg (if the eggs are big, use a half) 
  * 1 pinch of salt 
  * vanilla 
  * 1 little orange blossom water to pick up the dough 

for the stuffing: 
  * 500 gr of crushed toasted almonds 
  * 2 tablespoons of grilled sesame 
  * ½ glass of crystallized sugar 
  * 1 little orange blossom water 

to water: 
  * 500 gr of homemade honey 
  * Orange tree Flower water 



**Realization steps** start by preparing the dough: 

  1. place the flour, salt and butter in a large salad bowl, 
  2. Sand in your hands, then sprinkle orange blossom water in small amounts to collect the dough without kneading it, 
  3. divide the dough into 2 equal parts, cover them and leave to rest 

prepare the stuffing: 
  1. mix almonds, sugar and sesame seeds and sprinkle with orange blossom water, 
  2. Take a ball of dough, and lower it on a sheet of baking paper having the same size of your mold, the dough should be of a thickness of 3 to 4 mm. 
  3. transfer the dough to the mold, and cover it with the layer of almond stuffing, spreading it evenly. 
  4. lower the other ball of dough, like the first, and put it on the layer of almonds, press gently, so that the dough sticks well to almonds. then cut the excess dough.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/menkoucha-les-etapes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/menkoucha-les-etapes.jpg>)
  5. cut rectangles in the cake of almost 3 cm by 7 cm using a sharp knife. 
  6. decorate the cake with a cake tong (nakache). If possible, cover the cake and let it dry overnight, so that the shape of the tongs remains intact 
  7. bake the cake at 180 degrees C, for about 40 minutes or according to your oven, until the surface of the cake is lightly browned. 
  8. Sprinkle the cake with the honey lightened with orange blossom water as soon as it comes out of the oven. and leave well rested before cutting cake pieces. 



[ ![el mankoucha 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/el-mankoucha-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/menkoucha-8.jpg>)
