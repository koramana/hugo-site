---
title: Strawberry plant
date: '2013-04-23'
categories:
- dessert, crumbles and bars
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/fraisier-1_thumb.jpg
---
Hello everybody, 

Strawberry, a recipe that I really like to realize, but you should never let your husband make a fruit salad, he made the 3 trays of strawberries that were in the fridge, suddenly, no Strawberry. 

when Lunetoiles send me the photos and the recipe, I really wanted to go to the act, in any case, fortunately that the Lunetoiles strawberry is the !!! 

For 10-12 people 

ingredients: 

  1. a [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-poids-de-plume-facile-103501869.html>) (with 3 eggs) 
  2. [ Chiffon cream ](<https://www.amourdecuisine.fr/article-genoise-creme-mousseline-103155968.html>) (with 500 ml of milk) 
  3. Syrup (100 ml of water + 125 gr of sugar + 30 ml of strawberry juice) 
  4. 400g strawberries 
  5. 250g of pink almond paste 



method of preparation: 

  1. Start with [ Chiffon cream ](<https://www.amourdecuisine.fr/article-genoise-creme-mousseline-103155968.html>) to give him time to cool down. 
  2. Prepare the [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-poids-de-plume-facile-103501869.html>) . 
  3. after cooking, turn out after a few minutes. 
  4. prepare the syrup, bring the water and sugar to the boil and add the strawberry juice after cooling. 
  5. Cut 2 rectangles the size of the pastry frame 
  6. Place the frame on the serving dish, place a half of [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-poids-de-plume-facile-103501869.html>) that you soak with syrup. 
  7. Place all around the halves of strawberries that should not reach the top of the frame, strew the rest on the bottom of [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-poids-de-plume-facile-103501869.html>) . 
  8. Put some of the [ Chiffon cream ](<https://www.amourdecuisine.fr/article-genoise-creme-mousseline-103155968.html>) starting with the trick and trying to fill the gap between the strawberries. 
  9. Soak the 2nd half of [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-poids-de-plume-facile-103501869.html>) , try to cut the tower so that it is a little smaller than the first part, and so that the side of the [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-poids-de-plume-facile-103501869.html>) does not appear in the outer part of the cake. 
  10. Spread the rest of the [ Chiffon cream ](<https://www.amourdecuisine.fr/article-genoise-creme-mousseline-103155968.html>) in a thin layer so that the marzipan adhere more easily. 
  11. Cool at least 6 hours. 
  12. Unmould the strawberry with a knife tip on the top. 
  13. Spread the marzipan on a little icing sugar to prevent it from sticking to the worktop. Cut a rectangle by using the frame and position it on the cake. 
  14. Put your finger around the edge to adhere the dough to the cream. 
  15. Decorate with rose marzipan. 



**Strawberry plant**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/fraisier-1_thumb.jpg)

portions:  12  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients** For 10-12 people 

  * a genoise (with 3 eggs) 
  * muslin cream (with 500 ml of milk) 
  * Syrup (100 ml of water + 125 gr of sugar + 30 ml of strawberry juice) 
  * 400g strawberries 
  * 250g of pink almond paste 



**Realization steps**

  1. Start with the muslin cream to give it time to cool. 
  2. Prepare the sponge cake. 
  3. after cooking, turn out after a few minutes. 
  4. prepare the syrup, bring the water and sugar to the boil and add the strawberry juice after cooling. 
  5. Cut 2 rectangles the size of the pastry frame 
  6. Place the frame on the serving platter, place a half of sponge cake and soak with syrup. 
  7. Place all around the halves of strawberries that should not reach the top of the frame, strew the rest on the bottom of genoise. 
  8. Put some of the cream muslin starting with the turn and trying to fill the gap between the strawberries. 
  9. Soak the second half of sponge cake, try to cut the tower so that it is a little smaller than the first part, and so that the side of the genoise does not appear in the outer part of the cake. 
  10. Spread the rest of the cream in a thin layer so that the marzipan adhere more easily. 
  11. Cool at least 6 hours. 
  12. Unmould the strawberry with a knife tip on the top. 
  13. Spread the marzipan on a little icing sugar to prevent it from sticking to the worktop. Cut a rectangle by using the frame and position it on the cake. 
  14. Put your finger around the edge to adhere the dough to the cream. 
  15. Decorate with rose marzipan. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
