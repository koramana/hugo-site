---
title: Raspberry amandine cake / gluten-free cake
date: '2013-09-15'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/Cake-amandine-aux-framboises2.jpg
---
![Cake-amandine aux framboises.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/Cake-amandine-aux-framboises2.jpg)

Hello everybody, 

We do not finish with the delicious recipes Lunetoiles, today is a delicious cake with raspberries, she made mini version, it's really too cabbage ... 

A super delicious cake, and the most beautiful gluten-free .... then to our utensils ...   


**Raspberry amandine cake / gluten-free cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/Cake-amandine-a-la-framboise3.jpg)

Recipe type:  cake, taste  portions:  12  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 3 eggs 
  * 150 g of whole sugar 
  * 150 g of corn flour 
  * 80 g of cornstarch 
  * 1 sachet of baking powder 
  * 80 g of almond powder 
  * 1 C. coffee of vanilla natural essence 
  * 10 c. tablespoon of milk 
  * 150 g of very soft butter 
  * 2 trays of raspberries 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. In a bowl, beat the eggs with the sugar. 
  3. Stir in flour, starch, yeast, ground almonds, vanilla essence, milk and very soft butter until smooth. 
  4. Pour half of the mixture into a cake pan (or mussels) buttered and floured, gently add 1 layer of raspberries so that they remain whole during cooking. 
  5. Put in the oven for 15 minutes and lower the temperature of the oven to 160 ° C and continue cooking for another 25/30 minutes (if you use mini molds) and cook 50 minutes (for a large cake mold) or until that the blade of a knife driven into the center comes out clean and dry. 
  6. Taste after cooling with a good tea, or milk. 


