---
title: Pavlova, kiwi and cranberry recipe
date: '2017-08-28'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pavlova-aux-kiwi-021.CR2_1.jpg
---
![Pavlova, kiwi and cranberry recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pavlova-aux-kiwi-021.CR2_1.jpg)

##  Pavlova, kiwi and cranberry recipe 

Hello everybody, 

The pavlova is the most favorite dessert for my children, of course, everything that contains "meringue" is "yumi" for them ... Moreover, meringues are always present at home, or I make meringues at home, like the [ ancient meringue ](<https://www.amourdecuisine.fr/article-meringue-a-l-ancienne-recette-tres-facile-114688303.html>) or I buy that of the trade ... 

So here is a small variation of this delicious dessert, I liked to read the little story of this delight, and here it is: 

" The **Pavlova** is a meringue dessert named in honor of Russian ballerina Anna Pavlova. Its specificity is to be crisp on the outside and soft on the inside. The pavlova is usually topped with whipped cream and / or ice cream and covered with fruit. 

Dessert was invented after a trip from Pavlova to Australia and New Zealand. Both countries claim the invention of pavlova, which is a source of conflict. 

Dessert is very popular and forms an important part of the gastronomy of both countries, being frequently eaten during celebrations such as the Christmas meal. Source: Wikipedia 

Ingredients: 

  * of the [ meringues ](<https://www.amourdecuisine.fr/article-meringue-a-l-ancienne-recette-tres-facile-114688303.html>) in the form of a circle, or a single, wide meringue 
  * fresh cream whipped cream 
  * sugar (just to sweeten the whipped cream. 
  * kiwis 
  * dry cranberrys or cranberries 
  * [ strawberry sauce ](<https://www.amourdecuisine.fr/article-32373332.html>)



![Pavlova, kiwi and cranberry recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pavlova-aux-kiwi-010.CR2_1.jpg)

method of preparation: 

  1. preperz the meringue base as indicated in the recipe. 
  2. whip the cream in a whipped cream with a little sugar 
  3. garnish the bottom of meringue, with this whipped cream. 
  4. decorate with kiwi slices, and dry cranberries 
  5. and it will be even more delicious to accompany a net of [ strawberry sauce ](<https://www.amourdecuisine.fr/article-32373332.html>) . 



**Pavlova, kiwi and cranberry recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pavlova-aux-kiwi-022.CR2_1.jpg)

**Ingredients**

  * meringues in the shape of a circle, or a single meringue 
  * fresh cream whipped cream 
  * sugar (just to sweeten the whipped cream. 
  * kiwis 
  * dry cranberrys or cranberries 
  * strawberry sauce 



**Realization steps**

  1. preperz the meringue base as indicated in the recipe. 
  2. whip the cream in a whipped cream with a little sugar 
  3. garnish the bottom of meringue, with this whipped cream. 
  4. decorate with kiwi slices, and dry cranberries 
  5. and it will be even more delicious to accompany a drizzle of strawberry sauce. 


