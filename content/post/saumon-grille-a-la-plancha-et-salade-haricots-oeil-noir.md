---
title: Grilled Salmon a la plancha and Salad beans black eye
date: '2013-01-14'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine

---
Hello everybody, 

here is a wonderful grilled salmon recipe with a very rich salad of black eye beans. 

so the opportunity came, when my husband bought salmon, and asked me to grill him, right away the idea of ​​accompanying it with this black eye bean salad came.   
preparation of salmon pavers: 

  * clean the salmon pavers. 
  * marinate with: lemon juice, a little salt, a little black pepper, a little salmon, a little cumin, a little garlic (powdered or chopped) and put in the fridge for 4 to 5 minutes hour. 



and after passing them to the grill or the plancha 

.... a real delight, I do not tell you 

you noticed the flying egg, it's Ines taking his share, hihihihi 

black eye bean salad 

the recipe comes from djouza so a small copy / paste: 

  * _**Black eye beans dipped the day before** _
  * _**1 onion diced (fresh white onion in stem for me)** _
  * _**1 diced pepper (red or green) (I only had frozen grilled, so I made it back in oil to clear the water)** _
  * _**1 can of tuna in slice** _
  * _**Some pieces of feta** _
  * _**1 hard egg** _
  * _**Olives (green cut into some)** _
  * _**1 small chilli diced (I did not put)** _
  * _**Mix parsley, coriander, mint (I have not put)** _



_**Vinegar and oil and salt** _

The day before, dip the beans in a large basin of water to which you can add a little bicarbonate. Leave all night. The next day thoroughly rinse the beans and cook them in a pot of salt water. Drain and let cool. Cut the diced onion and pepper, chilli. Put half of the can of tuna slice and part of the feta. Season to taste and mix. 

Rectify the seasoning and finish with a slice of tuna and remaining cheese. Decorate with the hard-boiled egg and the olives and add some parsley and mint. Refrigerate until ready to serve. 

Enjoy your meal 

Thank you for your visit 

Thank you for your feedback 

et Merci de vous inscrire sur ma newsletter 
