---
title: the buns - bread balls
date: '2010-02-16'
categories:
- Index
- ramadan recipe

---
& Nbsp; this morning to accompany the chorba of noon (soup), I prepared this recipe of Buns that I find on the blog, of a friend to me Imane, it does not live too far from me, here in England , her blog is in English, but she will do translation in French soon. we liked it, they were super lightweight, and especially great for sandwiches, so I do not think I'll stop at this test. the recipe after translation: - 1/2 glass of water (120 ml) - 1/2 glass of milk (120 ml) - 2 tbsp. 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

this morning to accompany the chorba of noon (soup), I prepared this recipe of Buns that I find on the blog, from a friend to me 

we liked it, they were super lightweight, and especially great for sandwiches, so I do not think I'll stop at this test. 

the recipe after translation: 

\- 1/2 glass of water (120 ml) 

\- 1/2 glass of milk (120 ml) 

\- 2 tbsp. melted butter and chilled 

\- 1 C. soup of sugar 

\- 1/2 c. a coffee of salt 

\- 3 glasses of flour (400 gr) 

\- 1/2 c.a instant yeast coffee. 

Place the ingredients in the bowl of the bread maker in the order recommended for your machine   
and start the pate program.   
At the end of the cycle, remove the dough from the machine and place on a lightly floured surface and knead a little. 

Divide it into 8 equal pieces and form balls.   
sprinkle the surface of each ball, and place on a baking tray well floured, cover with a sheet of food film and let them double in size (about 40 min), for my part I put them in an oven not lit, and I put down a pan filled with hot water.   
When the BAPS has almost doubled in volume, remove the food film and sprinkle a little more flour and make the gilding with a little milk then sprinkle the surface with sesame seeds.   
Cook the buns in a preheated oven at 200 º C / GM6 for 15 min, or until they are browned around the edges and cooked. 

un régal, essayez la recette, vous allez l’aimer 
