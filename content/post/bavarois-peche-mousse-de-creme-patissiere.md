---
title: Bavarian peach / cream mousse pastry
date: '2012-09-18'
categories:
- cuisine algerienne
- cuisine diverse
- recette a la viande rouge ( halal)
- recette de ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/rayan-birthday-cake-09811.jpg
---
hello everyone, a recipe from my archives, which makes its success, when I do, my father and my brothers love this Bavarian, so for a party like the beautiful evenings of Ramadan, I prepared this delicious Bavarian a several times .. always the same recipe, just have a little imagination, the good fresh cream that can make whipped cream, and especially the gelatin hallal, must I tell you that I made my little stock . this delicious Bavarian, had as a biscuit base a delicious Russian with peanuts, the first layer is a foam to the skies. 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

a recipe of my archives, which makes its success, when I do it, my father and my brothers like this  Bavarian  So for an evening like the beautiful Ramadan evenings, I have prepared this delicious Bavarian on several occasions .. 

always the same recipe, just have a little imagination, the good cream that we can make whipped cream, and especially the gelatin hallal, must I tell you that I made my little stock. 

this delicious Bavarian, was based on biscuit a delicious Russian peanuts, the first layer is a peach mousse, the second is a mousse pastry cream, and finally a jelly colored orange. 

for the base the recipe is [ right here ](<https://www.amourdecuisine.fr/article-bavarois-russes-chocolat-54058016.html>) . 

for the peach mousse you need: 

250 ml of fresh cream (I buy a bottle of 450 ml that I mounted in whipped cream and divided in two) 

100 gr of sugar (or according to your taste) 

200 gr of syrup peach 

1 sheet of gelatin (in Algeria it is big leaves of 10 grs, I use that 8 gr can be a next time I will decrease the dose) 

for the custard mousse: 

200 ml of fresh cream 

2 egg yolks 

300 ml of milk 

120 gr of sugar 

1 cup of vanilla coffee 

2 tablespoon cornflour 

6 grams of gelatin in foil 

for decoration: 

jelly 

red and yellow dye 

![Bavarian peaches](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/rayan-birthday-cake-09811.jpg)   


my pastry circle, which is a square, (I do not know if I can call it circle, hihihihihi) is 23 cm X 23cm 

Beat the liquid cream with the sugar until you have a whipped cream and reserve in a cool place. 

Put the gelatin in a little water so that it swells. 

Mix the peaches to obtain a puree (you can put more than this amount to have more taste, I do not mix too much and leave pieces) 

put the puree on a low heat, add the swollen gelatin, and mix until it is well melted, let cool and incorporate the whipped cream gently without breaking it, pour the device on the base biscuitée. 

now prepare your pastry cream as usual 

put the gelatin in the water so that it swells 

beat the egg yolks with the sugar until it turns white, add the cornstarch, then the hot milk while whisking. 

put the whole thing on low heat, add the gelatin and stir until it becomes thick, let cool and incorporate the whipped cream, and pour over the layer of peach mousse. 

now take the gelatin in a saucepan, over low heat, add the dyes to have a nice orange color. when the jelly is well liquefied pour it slowly on the last layer. 

let it all take place in a cool place, and enjoy 

Saha Ramdankoum and thank you for all your visits. 

do not forget to subscribe to the newsletter to come back as soon as I publish a new recipe. 

bisous 
