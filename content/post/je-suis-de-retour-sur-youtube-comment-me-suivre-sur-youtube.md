---
title: I'm back on Youtube, how to follow me on youtube?
date: '2017-03-18'
categories:
- Café amour de cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/youtube-not-1024x1024.jpg
---
![youtube not](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/youtube-not-1024x1024.jpg)

Hello everybody, 

Yes! You read it well .. I'm back on youtube and here is the link of my channel: 

I rejected my return on youtube for a moment under the pretext that I did not have the time, or that I will not be able to make videos of quality. But here it was, it was enough to take the step, and to get into the bath, after all comes slowly. 

I had created my youtube channel a few years ago, at the beginning I just did a montage of photos and did not put anything detailed on my videos, because I wanted to send the visitors of my channel to my blog. 

Over time I realized that the public of my youtube channel and that of my blog is totally different, so I started to improve my videos, to try to give more details possible and like that, my followers on youtube would be well satisfied. 

But in late 2013, with my pregnancy and my delivery, I dropped my youtube channel. Logically, taking care of a baby is not an easy task ... But my channel kept growing and gaining volume, and my followers were asking for my return to my channel all the time. Something I did at the first opportunity. 

I am very happy to set the record straight, I try every time to add new on my videos, to do an even more elaborate work, something that takes me a lot of time. But here I am, I can not find my audience. 

I tapped at every door, I solicited the help of facebookains, so there's a big disappointment on Facebook, because facebook people do not want to get out of this blue space ... they want everything to be in, they want that the recipe is completely write on facebook, they want the videos to be completely on facebook ... they want me to answer questions on Facebook ... They want me to be 24h on Facebook ... 

In any case, as I told you, the return on my channel is a step taken, but I do not see you on my channel ... I see all these figures at the top of my chain: 57K subscribers, but I do not see them on my videos ... and I say to myself: but what's wrong, do not people like the content of my work ??? 

After several searches, I understood something pure, simple and stupid ... The fault is not in my subscribers, the fault is not at home, the fault is on youtube, or at youtube ... Yes, this great Video hosting platform does not stop making changes and changes. 

The most recent change: the notification bell: ![continued 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/suite-2-1024x374.jpg) ![continued 3](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/suite-3-1024x284.png)

and yes, this little bell, which is right there at the bottom of my bar, or at the bottom of each of my videos made all the difference. People do not see it, people do not know it, it's new, but it's the cause of my problem: 

![34dd3e7c30c1a6623de3b6ada6f8fc5c](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/34dd3e7c30c1a6623de3b6ada6f8fc5c-150x150.jpg)

Yes, since the addition of this bell on our youtube channels, any subscriber on my channel (and especially old subscribers) who would not click on it, will never know that I'm back on youtube, all my 52k subscribers that I had before adding this bell do not know I'm going to update on my channel. 

All those subscribers who would not have activated this bell are just ghosts on my channel, more figures that do not know anything about my channel ... And the hard part is that uncle youtube gives us no way to contact these members and tell them: I'm here, my videos are back ... sniff sniff, what a disappointment! 

And frankly, the people of youtube you had not found more stupid than that to do ... find a solution so that I find my public ... The people are not all computer scientists and are not alerted to all the changes that you go to do… 

Thank you my dear readers for taking the trouble to read this post ... Thank you for your loyalty, and especially do not forget to subscribe, activate the bell and leave a comment, maybe, you will be the next winner of the gift, following the draw on my youtube channel. 

Kiss… Soulef, Amour de cuisine. 
