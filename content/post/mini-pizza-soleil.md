---
title: Mini pizza sun
date: '2016-10-10'
categories:
- Unclassified
tags:
- Pizza dough
- Aperitif
- inputs
- Algeria
- Italy
- Easy cooking
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mini-pizza-soleil-2.jpg
---
![pizza mini-sun-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mini-pizza-soleil-2.jpg)

##  Mini pizza sun 

Hello everybody, 

mini pizza sun, a recipe that made its buzz on facebook. I would have liked to do this too, but as my kitchen is still in full swing, I pass my hand to my friend Wamani Merou. 

![mini pizza sun-4](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mini-pizza-soleil-4.jpg)

**Mini pizza sun**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mini-pizza-soleil-3.jpg)

portions:  4-6  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** For the pasta: 

  * 2 glasses of milk (1 glass = 240 ml) 
  * ½ glass of oil 
  * 2 tbsp. butter 
  * 1 C. tablespoons sugar 
  * 1 C. salt 
  * 1 packet of instant yeast 
  * 1 egg 
  * 5 to 6 glasses of flour 

for the stuffing: 
  * 1 little tomato sauce 
  * black olives in slices 
  * green olives in slices 
  * tomatoes cut into cubes 
  * green pepper cubes 
  * mozzarella cheese 



**Realization steps** start by preparing the dough: 

  1. you can use the petrin to make the dough, just mix the dry ingredients, 
  2. add the oil, then introduce the warm milk gradually to pick up the dough. 
  3. towards the end knead the dough by introducing the butter, then let rise. 
  4. degas the dough then start shaping. 
  5. divide the dough into balls the size of a medium orange. 
  6. use the bottom of a glass to squeeze the middle of the ball to shape a well   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/fa%C3%A7onnage-pizza-soleil.bmp.jpg)

source: video of Yemek-askim 

  7. with a knife cut into the rounded part of the dough   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/faconnage-pizza-soleil-1.jpg)

source video yemek-askim 

  8. roll each end then pinch to form a petal   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mini-pizza-soleil-225x300.jpg)
  9. fill the bottom of each mini pizza with a little tomato sauce and the rest of the ingredients   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mini-pizza-soleil-1-225x300.jpg)
  10. sprinkle with mozzarella cheese and bake in a preheated oven at 180 ° C 



![mini pizza sun-5](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mini-pizza-soleil-5.jpg)
