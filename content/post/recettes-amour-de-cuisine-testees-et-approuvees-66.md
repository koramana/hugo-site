---
title: Kitchen love recipes tested and approved 66
date: '2015-04-27'
categories:
- les essais de mes lecteurs
tags:
- desserts
- Cakes
- Algeria
- Morocco
- dishes
- tajine
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-58.html/dyouls-de-kari-ben>)

Hello everybody, 

Back with the photos of your achievements that you made from the recipes of my blog. Your sharing makes me more and more pleasure, and warms my heart. It's a pleasure to see this readers' confidence grow from day to day, it's a pleasure to know that more and more of you are following my blog, and trust the published recipes on it. Continue to send me the pictures on my email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

Do not deprive me of this happiness my friends, and the more you are many to try my recipes and to leave your comments, the more I have the will to make even more delights ... I continue with your encouragements. 

[ ![Strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/confiture-de-fraise.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/confiture-de-fraise>)

[ Strawberry express jam ](<https://www.amourdecuisine.fr/article-confiture-de-fraises-express-au-micro-onde.html> "strawberry jam «express, in the microwave»") at Mil lakhdari 

[ ![image2](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/image2.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/image2>)

[ breads with olives ](<https://www.amourdecuisine.fr/article-pains-aux-olives-cuisine-algerienne.html> "olive breads, Algerian cuisine") at Souhila Z 

[ ![nest of tomatoes-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/nid-de-tomates-001.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/nid-de-tomates-001>)

[ stuffed tomatoes with eggs ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs.html> "stuffed tomatoes") at Fardadou Gharbi 

[ ![express jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/confiture-express.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/confiture-express>)

[ Strawberry express jam ](<https://www.amourdecuisine.fr/article-confiture-de-fraises-express-au-micro-onde.html> "strawberry jam «express, in the microwave»") at Hichem Nihed 

[ ![griwech at Radia Mhd](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/griwech-chez-Radia-Mhd.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/griwech-chez-radia-mhd>)

[ Griwech ](<https://www.amourdecuisine.fr/article-griwech-gateau-algerien-en-video.html> "Griwech Algerian cake in video") at Radia Mhd 

[ ![stuffed tomatoes from Wamani](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tomates-farcies-de-Wamani.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/tomates-farcies-de-wamani>)

[ stuffed tomatoes ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs.html> "stuffed tomatoes") at Wamani merou 

[ ![tajine el warka at Cece's place](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tajine-el-warka-chez-Cece.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/tajine-el-warka-chez-cece>)

[ ![shortbread with jam cre puscule](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sabl%C3%A9-a-la-confiture-cre-puscule1.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/sable-a-la-confiture-cre-puscule-2>)

[ shortbread with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture.html> "shortbread with jam") at Cré puscule 

[ ![tajine with spinach wamani](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tajine-aux-epinards-wamani.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/tajine-aux-epinards-wamani>)

[ Tajine with spinach ](<https://www.amourdecuisine.fr/article-tajine-aux-epinards.html> "tagine with spinach") at Wamani Merou 

[ ![Strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/confiture-de-fraises1.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/confiture-de-fraises-2>)

[ Strawberry express jam ](<https://www.amourdecuisine.fr/article-confiture-de-fraises-express-au-micro-onde.html> "strawberry jam «express, in the microwave»") at my daughter's house my life 

[ ![makrout sniwa](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/makrout-sniwa1.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/makrout-sniwa-2>)

[ makrout sniwa ](<https://www.amourdecuisine.fr/article-makrout-sniwa-ou-makrout-facon-baklawa.html> "makrout sniwa or makrout way baklawa") at Sonia Ch-ha 

[ ![makrout sniwa 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/makrout-sniwa-2.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/makrout-sniwa-2-2>)

[ makrout sniwa ](<https://www.amourdecuisine.fr/article-makrout-sniwa-ou-makrout-facon-baklawa.html> "makrout sniwa or makrout way baklawa") at Dalila Mak 

[ ![rolled to the potato](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-a-la-pomme-de-terre.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/roule-a-la-pomme-de-terre>)

[ rolled to the potato ](<https://www.amourdecuisine.fr/article-roule-de-pomme-de-terre-a-la-viande-hachee.html> "rolled potato with minced meat") at Malika Bouaskeur 

[ ![Meringue](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/meringue.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/meringue>)

[ ![lemon tart amina](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tarte-au-citron-amina.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/tarte-au-citron-amina>)

[ lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee.html> "lemon meringue pie") at Amina Dadou 

![makrout sniwa rima](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/makrout-sniwa-rima.jpg)

[ ![strawberry shortcake](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/gateau-sabl%C3%A9-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/gateau-sable-aux-fraises-2>)

[ crispy strawberry cake with strawberries ](<https://www.amourdecuisine.fr/article-gateau-sable-croustillant-aux-fraises.html> "crispy strawberry cake with strawberries") at Amina Dadou 

[ ![rolled potato](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-pomme-de-terre.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/roule-pomme-de-terre>)

Potato roll with minced meat at Chemss dine 

[ ![tajine spinach](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tajine-epinards.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/tajine-epinards>)

[ spinach tajine ](<https://www.amourdecuisine.fr/article-tajine-aux-epinards.html> "tagine with spinach") at Tifa Nahanis 

[ ![crispy strawberry cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/gateau-croustillant-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/gateau-croustillant-aux-fraises>)

[ Crispy shortbread cake with strawberries ](<https://www.amourdecuisine.fr/article-gateau-sable-croustillant-aux-fraises.html> "crispy strawberry cake with strawberries") at Malika Bouaskeur 

[ ![croquet virus](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/croquet-virus.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/croquet-virus>)

[ croquet with jam ](<https://www.amourdecuisine.fr/article-croquet-a-la-confiture-gateaux-secs-a-la-confiture.html> "Croquet with jam: dry cakes with jam") at Fleur DZ 

[ ![rfiss constantinois](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/rfiss-constantinois.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/rfiss-constantinois-3>)

[ Rfiss constantinois ](<https://www.amourdecuisine.fr/article-rfiss-constantinois.html> "rfiss constantinois") at Sali Nice 

[ ![spinach tajine](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/tajine-depinards.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-66.html/tajine-depinards>)

[ tagine with spinach ](<https://www.amourdecuisine.fr/article-tajine-aux-epinards.html> "tagine with spinach") chez Amina Dadou 
