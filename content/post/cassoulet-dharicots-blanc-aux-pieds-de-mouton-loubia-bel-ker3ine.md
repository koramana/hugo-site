---
title: White bean cassoulet with lamb's feet, loubia bel ker3ine
date: '2014-10-07'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cassoulet-d-haricots-au-pied-de-mouton_thumb-238x300.jpg
---
##  White bean cassoulet with lamb's feet, loubia bel ker3ine 

Hello everybody, 

Many people at the feast of the aid prefer to realize chtitha bouzellouf, with us it is a little different, my father and my brothers love the cassoulet with white beans, well, for this feast of the Aid ' they asked my mother. 

She introduced them to a good white bean cassoulet called loubia bel ker3ine, Loubia bel keriine, لوبية بالكرعين 

and she shared these photos and the recipe with us. Too bad, I do not do this dish, because my husband does not like too much, so I will find myself eating this dish all alone, something I do not like too much .... because I want to prepare dishes and dishes that are tasted together. 

**White bean cassoulet with lamb's feet, loubia bel ker3ine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cassoulet-d-haricots-au-pied-de-mouton_thumb-238x300.jpg)

portions:  4  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients**

  * 350 g of white beans dipped the day before in water. 
  * legs or feet of mutton or lamb 
  * 1 C. tomato paste 
  * 3 c. oil soup 
  * 3 cloves of garlic 
  * 1 bay leaf, and one or two branches of thyme 
  * 2 tomatoes 
  * a little onion 
  * salt 
  * water 
  * cumin 
  * black pepper 



**Realization steps**

  1. clean well, cut and wash the feet of lamb. 
  2. Prepare a dersa (Algerian pesto) by pounding or crushing the garlic with the spices. 
  3. In a casserole, put the lamb feet, add over the dersa, the bay leaf, the thyme, the oil and finally the water. 
  4. Let the lamb feet cook for 1 hour. 
  5. In another coconut, cook the beans for 15 minutes in a slightly salty water. 
  6. cook and remove from heat when the beans are very tender. 
  7. then add the white beans to the lamb's feet 
  8. add the tablespoon of tomato paste diluted in a little water and continue cooking until the lamb feet and white beans are well cooked. 
  9. Serve your white beans and lamb feet in hot sauce. with good matlou3 or khobz dar. 



Bon appétit. 
