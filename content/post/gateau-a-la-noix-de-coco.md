---
title: cake with coconut
date: '2016-11-29'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gateau-a-la-noix-de-coco-019_thumb1.jpg
---
##  cake with coconut 

Hello everybody, 

A cake has very light coconut, and too good, especially this butter flavor. Frankly I really like the taste of butter in cake and cakes, it's a flavor that makes me go back 30 years, when we ate these cakes that my mother prepared us with fresh cow butter, it's was a delight. 

This cake is just too good, try it and tell me your opinion.   


**cake with coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gateau-a-la-noix-de-coco-019_thumb1.jpg)

**Ingredients**

  * ingredients: 
  * 5 eggs 
  * 80 ml of milk 
  * 2 tablespoons of melted butter 
  * 1 glass of sugar (250 ml) 
  * 1 glass of ground coconut (passed by the coffee grinder) 
  * 1 glass of flour 
  * 2 bags of baking powder 
  * 1 sachet of vanilla sugar 
  * 2 to 3 tablespoons sweetened condensed milk 
  * coconut for decoration. 



**Realization steps**

  1. method of preparation: 
  2. beat the eggs with the sugar until they have a whitish foam. 
  3. add the melted butter while whisking. 
  4. stir in the milk without stopping whipping. 
  5. mix together sifted flour, yeast, coconut, and vanilla. 
  6. stir in the mixture gently with the eggs. 
  7. pour the mixture into a buttered and floured baking pan or lined with baking paper. 
  8. cook in a preheated oven between 30 and 35 minutes 
  9. at the exit of the oven, unmold and let cool. 
  10. brush with a brush with a little sweetened condensed milk, then cover with a thin layer of coconut. 


