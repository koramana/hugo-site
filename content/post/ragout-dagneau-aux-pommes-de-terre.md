---
title: lamb stew with potatoes
date: '2016-11-06'
categories:
- Algerian cuisine
tags:
- dishes
- Meat
- Algeria
- Ramadan
- la France
- Today's special
- tajine

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/ragou-dagneau-aux-pommes-de-terre.jpg
---
![stew-lamb-to-apples-Earth](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/ragou-dagneau-aux-pommes-de-terre.jpg)

##  lamb stew with potatoes 

Hello everybody, 

Here is the kitchen that my husband prefers and a lamb stew with potatoes is his favorite dish, finally if we add to the list the [ white beans in red sauce ](<https://www.amourdecuisine.fr/article-haricots-blancs-en-sauce-rouge.html>) , and dishes of traditional Algerian cuisine. 

I prepared this lamb stew with potatoes for dinner, and as I've always told you, I do not like to take the photos at night because they do not reflect too much the quality of the dish. 

This Ragout is just a delight! Usually I do my lamb stew with only potatoes, but my little one likes carrots a lot, so I introduced this vegetable to the dish, and that made the stew even more flavorful.   


**lamb stew with potatoes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/ragou-d-agneau-aux-pommes-de-terre-1.jpg)

portions:  6  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 5 to 6 pieces of lamb (here lamb mouse and leg of lamb) 
  * 1 chopped onion 
  * 1 clove of garlic 
  * 1 bay leaf 
  * salt, black pepper, paprika, 1 pinch of cinnamon powder 
  * 1 tbsp tomato concentrate 
  * 2 carrots (optional) 
  * 1 kilo of potatoes 
  * olive oil 
  * water 



**Realization steps**

  1. fry the chopped onion in a little olive oil until it becomes translucent. 
  2. add the pieces of washed and dried meat and let it come back. 
  3. add the bay leaf, season with salt and black pepper, add cinnamon, paprika and tomato paste. 
  4. clean the carrots and cut them into slices. 
  5. add to the meat and cover everything with boiling water (I use a pressure cooker here, so I do not put too much water because the cooking of the meat will be faster) 
  6. peel the potatoes and cut them in quarters. 
  7. when the meat is almost cooked and tender, add the potato. 
  8. let the potatoes cook well. 
  9. when the sauce is smooth, remove from the heat, and enjoy. 


