---
title: Puree peas mint and mascarpone goat mousse and minced meat kebab
date: '2014-02-08'
categories:
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/Puree-de-petits-pois-a-la-menthe-et-mascarpone-mousse-de-.jpg
---
_**[ ![Puree-de-peas-a-la-mint-and-mascarpone-mousse-de-](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/Puree-de-petits-pois-a-la-menthe-et-mascarpone-mousse-de-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/10/Puree-de-petits-pois-a-la-menthe-et-mascarpone-mousse-de-.jpg>) ** _

##  Puree peas mint and mascarpone goat mousse and minced meat kebab 

**Mashed peas with mint and mascarpone / fresh goat mousse / spicy minced meat kebab**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/Puree-de-petits-pois-a-la-menthe-et-mascarpone-mousse-de-.jpg)

portions:  4  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 500g of peas 
  * 4 tablespoons of mascarpone 
  * a cubed broth a few mint leaves 

For fresh goat mousse: 
  * 10cl of liquid cream 
  * 100g of fresh goat cheese 

For kebabs of minced meat: 
  * 500gr of minced meat 
  * coriander 
  * cumin 
  * garlic powder 
  * paprika 
  * salt pepper 



**Realization steps**

  1. Bring the water with the cubed bouillon and mint leaves to the boil, add the peas and cook for 10 minutes. 
  2. drain the peas and mix them with the mascarpone. 
  3. Mix the minced meat with the spices then form skewers and cook them 10min on an electric barbecue or a la plancha 
  4. Finally, mix the goat with the liquid cream and put in the siphon to make a whipped cream. 
  5. if you do not have a siphon, mix the goat cheese and add it to the liquid cream that you whip up with a whisk. 
  6. and proceed to training. 



Note you can accompany a small sauce (balsamic vinegar with a little lemon juice)   
here is good appetite =) 
