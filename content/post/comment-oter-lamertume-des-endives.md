---
title: How to remove the bitterness of chicory
date: '2012-02-18'
categories:
- les essais de mes lecteurs
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/02/endives.jpg
---
![How to remove the bitterness of chicory](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/endives.jpg)

##  How to remove the bitterness of chicory 

Hello everybody, 

The **endive** or **chicory** ( **_Intybus Cichorium_ convar. _foliosum_ ** ) is a biennial plant, but grown as an annual plant, of the family Asteraceae, grown for its "chicons", whitened shoots obtained by forcing, consumed as raw or cooked vegetables. 

She is called _chicory_ in Belgium and the north of France, and _endive_ elsewhere. 

Endives can be stored for several days in the refrigerator crisper or in the cellar in a plastic bag. The maintenance in the dark is important, because it avoids the appearance of green zones, bitter, under the effect of the light. (Source: wikipedia). 

now, we're back to how to remove the bitterness with chicory: 

To remove the bitterness of chicory, simply remove with a knife a small cone at the base of the bud because it is the source of all the bitterness of this vegetable. but if you want to use the endive cooked in a recipe, you can only add a [ Piece of sugar ](<https://www.amourdecuisine.fr/article-mes-coeurs-de-sucre-99329879.html>) to the cooking water. 

merci pour votre passage, et je vais essayer d’enrichir, la categorie trucs et astuces au fur et a mesure, si vous avez des idees, partagez les avec moi sur mon email: vosessais@gmail.com 
