---
title: Zviti, slata bou mahrass
date: '2017-08-29'
categories:
- Algerian cuisine
tags:
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/08/Zviti.jpg
---
[ ![Zviti, slata bou mahrass](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/Zviti.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/Zviti.jpg>)

##  Zviti or Slata boumahrass 

Hello everybody, 

Today, I take you to Algeria, more precisely on the highlands at the edge of the desert, Bousaada.   
It is nicknamed the "city of happiness", probably due to the fact that the city is the oasis closest to the coast.   
Today's recipe is a specialty of the region, but less known than the chakhchukha.    
It is a semolina bread, which is crushed with mortar, with tomato, peppers, garlic, and coriander, hence its name "Slata boumahrass", the mahrass being the mortar in Arabic.   
It is served with a glass of whey, it plays a role of dressing for the stomach, given the spice of zviti.   
It is traditionally eaten on the mortar, mine being top small, I have all transferred to a plate. 

**Zviti, slata bou mahrass**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/Zviti.jpg)

portions:  6  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** Here are the ingredients for the cake: 

  * 400 g of semolina 
  * 1 teaspoon of salt 
  * water 

Ingredients for the sauce: 
  * 2 large tomatoes 
  * ½ red pepper 
  * 1 green peppers 
  * 4 cloves of garlic 
  * 1 bunch of fresh coriander 
  * ½ teaspoon of fennel seeds 
  * ½ teaspoon of coriander seeds 
  * salt 
  * 40 g of butter 



**Realization steps** to make the cake: 

  1. Mix semolina and salt, gradually add water, knead, without delay. 
  2. It is just necessary that the dough is a little flexible. 
  3. Shape into a ball and leave to rest for 15 minutes. 
  4. Lower the dough into a slab. 
  5. And cook it on both sides. 

prepare the sauce: 
  1. Grill tomatoes, pepper and chillies on a wire rack. 
  2. Boil a little water by adding to concentrated tomato. 
  3. remove the skins from the vegetables, and the seeds. 
  4. Cut in roughly. 
  5. In the mortar, place the seeds of fennel, cilantro, fresh coriander and salt. 
  6. Crush with mortar.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/Zviti-Slata-Boumahrass.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/Zviti-Slata-Boumahrass.jpg>)
  7. Add the vegetables, and crush. 
  8. Then add in pieces, the cake cut into pieces, we add little by little. If the mixture is too dry, add the water with the concentrated tomato. 
  9. Proceed as follows until exhaustion of the cake. 
  10. I did not put the whole galette, my mortar is too small. 
  11. Once well pounded, add the butter and mix. Pour into a plate if you want, and garnish with chopped coriander. 



[ ![Zviti, slata bou mahrass](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/zviti-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/zviti-1.jpg>)
