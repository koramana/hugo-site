---
title: hrira with vegetables
date: '2018-03-21'
categories:
- Algerian cuisine
- Dishes and salty recipes
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/harira-aux-legumes.CR2_.jpg
---
[ ![harira with vegetables](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/harira-aux-legumes.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/harira-aux-legumes.CR2_.jpg>)

##  hrira with vegetables 

Hello everyone, 

If you ask me what are your favorite dishes I will say without hesitation: 

  * [ Tunisian mulukhiya ](<https://www.amourdecuisine.fr/article-mouloukhiya-ou-mloukhia-au-boeuf.html>)
  * [ Amekfoul Kabyle ](<https://www.amourdecuisine.fr/article-couscous-aux-legumes-amekfoul-cuisine-algerienne.html>) (couscous with evaporated vegetables drizzled with olive oil) 
  * the [ Hrira Oranaise ](<https://www.amourdecuisine.fr/article-hrira-oranaise.html>)
  * [ Boureq annabi ](<https://www.amourdecuisine.fr/article-bourek-annabi-carres-viande-hachee-et-oeuf.html>)



So if you want to invite me, you know what to do, hihihihi 

Then the hrira or harira, there are several varieties, and the harira with dry vegetables and fresh that my mother has always prepared for us is the best, try it and give me your opinion. 

The recipe in Arabic: 

**hrira, ramadan soup with vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/hrira-aux-legumes.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * chicken (you can even add meat) 
  * 1 carrot 
  * 1 potato 
  * 1 zucchini 
  * ½ leek 
  * 1 turnip 
  * 1 stalk of celery (or leaves, it would be even better) 
  * 1 handful of chickpeas soaked the day before 
  * 1 handful of white beans soaked the day before 
  * 1 handful of lenses 
  * 1 bunch of parsley 
  * 1 bunch of coriander 
  * 1 tablespoon of tomato paste 
  * onion + 1 garlic clove 
  * black pepper, salt, ½ teaspoon of cubébe, ½ teaspoon of caraway 
  * oil 



**Realization steps** for this dish you need 2 pots 

  1. in a pot, put all the vegetables washed and peeled and cut, add the beans, lentils, celery, parsley, and a little coriander, add water and a little salt and let cook well. 
  2. in the other pot, put the chicken and / or the meat, add the onion and garlic to the blinder, the tomato, the condiments, and the chickpeas (if you put the meat, leave until what the meat cooks a bit to add the tomato, because the tomato makes the meat harden) 
  3. add tomato, chopped coriander and cover with water, cook over medium heat. 
  4. after cooking the vegetables of the first pot, pass them to the blinder, and put aside. 
  5. when cooking the meat you can add the vegetable broth, let it cook a little. 
  6. if you do it with chicken only, you can pull your chicken, crumble it, and then add it to your pot, it's easier to eat, I do not like to dip my fingers in the sauces. 
  7. serve well garnished with coriander. 
  8. one more thing, you can control the smoothness of this dish, namely you like it thick, or so light, so you can play on the amounts of vegetables. 



[ ![harira or hrira with vegetables-001](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/harira-ou-hrira-aux-legumes-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/harira-ou-hrira-aux-legumes-001.jpg>)
