---
title: Squares with mixed nuts and 2 chocolates
date: '2016-01-02'
categories:
- appetizer, tapas, appetizer
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid
- Chocolate cake
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Carr%C3%A9s-aux-noix-mixtes-et-aux-2-chocolats-2.jpg
---
[ ![Squares with mixed nuts and 2 chocolates 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Carr%C3%A9s-aux-noix-mixtes-et-aux-2-chocolats-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Carr%C3%A9s-aux-noix-mixtes-et-aux-2-chocolats-2.jpg>)

##  Squares with mixed nuts and 2 chocolates 

Hello everybody, 

This is the first recipe of the new year, but it's a Lunetoiles recipe that's on my inbox since November 2014, yes, just to show you how much I can not make ends meet, hihihihi. But better late than never, and a nice and good recipe, will not it? 

Lunetoiles likes this kind of delights super easy to make, super easy to shape, and super good ... Besides me too, and this recipe I like a lot. You can see the [ almond version ](<https://www.amourdecuisine.fr/article-carres-aux-amandes-sans-cuisson.html>) and [ the peanut version ](<https://www.amourdecuisine.fr/article-rkhama-zellige-nougat-aux-cacahuetes-et-sesames-grillees.html>) . 

more 

[ ![Squares with mixed nuts and 2 chocolates 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Carr%C3%A9s-aux-noix-mixtes-et-aux-2-chocolats-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Carr%C3%A9s-aux-noix-mixtes-et-aux-2-chocolats-3.jpg>)   


**Squares with mixed nuts and 2 chocolates**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Carr%C3%A9s-aux-noix-mixtes-et-aux-2-chocolats-1.jpg)

portions:  30  Prep time:  30 mins  total:  30 mins 

**Ingredients**

  * 400 g mixed nuts (almonds, hazelnuts, walnuts, cashew nuts) grilled with golden sesame seeds 
  * 100 ml of honey (perfumed with orange blossom) 
  * 150 g of dark chocolate 
  * 150 g of white chocolate 
  * A pinch of cinnamon 
  * Golden sesame seeds for decoration 



**Realization steps**

  1. Put the mixture of nuts in a kitchen chopper and crush them roughly. 
  2. Transfer the mixture to a bowl. 
  3. Add honey and cinnamon. 
  4. Mix with a spatula or wooden spoon crushed walnuts, cinnamon and honey. 
  5. In a rectangle mold of 30cm / 20cm or a square mold of 24 x 24 cm, place a parchment paper and pour the mixture. 
  6. Spread evenly over the entire surface and flatten with the bottom of a glass or with your hand. 
  7. Melt 150 g of white chocolate in a "bain marie" over very low heat. 
  8. When the white chocolate is melted, pour and spread over the entire surface. 
  9. Sprinkle with sesame seeds. 
  10. Put in the refrigerator for 1 hour and 30 minutes. 
  11. Remove from the refrigerator. 
  12. Take another parchment paper larger than the first and turn the pan on top of it. 
  13. Melt 150 g dark chocolate in a "bain marie". 
  14. When the dark chocolate is melted, pour it over the entire surface (now you have two sides with chocolate). 
  15. Sprinkle with sesame seeds. 
  16. Refrigerate for 1 hour and 30 minutes. 
  17. Remove from the refrigerator and cut squares the size you want. 



[ ![Squares with mixed nuts and 2 chocolates 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Carr%C3%A9s-aux-noix-mixtes-et-aux-2-chocolats.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Carr%C3%A9s-aux-noix-mixtes-et-aux-2-chocolats.jpg>)
