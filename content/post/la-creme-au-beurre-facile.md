---
title: easy butter cream
date: '2013-11-14'
categories:
- Cupcakes, macaroons, and other pastries
- basic pastry recipes
- sweet recipes
tags:
- Cupcakes
- Icing
- Butter cream
- logs
- Christmas
- desserts
- Cakes
- Based

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/09/cupcake-039_thumb1.jpg
---
[ ![easy butter cream](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/cupcake-039_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/09/cupcake-039.jpg>)

##  easy butter cream 

Hello everybody, 

a butter cream easy to make, very supple, unctuous and light despite the butter in it, which you can use to decorate your cakes, your cupcakes, your logs, and even to make a stuffing of one of your beautiful birthday cakes . 

If you want the chocolate version, you can see the recipe of the [ ultra easy chocolate butter cream ](<https://www.amourdecuisine.fr/article-creme-au-beurre-chocolat-ultra-facile.html>) , and here is the video of realization: 

**easy butter cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-au-beurre-a-la-meringue-suisse3_thumb.jpg)

Recipe type:  basic pastry recipe  portions:  8  Prep time:  15 mins  cooking:  5 mins  total:  20 mins 

**Ingredients**

  * 4 egg yolks 
  * 140 grams of sugar 
  * 30 ml of water 
  * 200 grams of butter 
  * vanilla 



**Realization steps**

  1. in a saucepan place the wet sugar with the water and cook until 117 degrees (if you do not have a sugar thermometer, let the syrup cook until you start to see the big bubbles 
  2. beat the egg yolks, and gradually add the still hot syrup, in net on 
  3. whisk at medium speed to avoid hot syrup spraying 
  4. continue to whisk at high speed until the mixture has cooled down 
  5. add the butter in pieces slowly to this mixture, and the mixer should be at medium speed. 



[ meringuee butter cream ](<https://www.amourdecuisine.fr/article-creme-au-beurre-a-la-meringue-suisse-101356259.html>)

the cream exposed to the light, gives a good reflection, is not it? 

[ ![butter cream with Swiss meringue3](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-au-beurre-a-la-meringue-suisse3_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/creme-au-beurre-a-la-meringue-suisse3.jpg>)

video de preparation : 
