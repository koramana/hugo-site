---
title: tiramisu with walnuts and gingerbread
date: '2013-11-27'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tiramisu-noix-pain-d-epice-041_thumb_1.jpg
---
##  tiramisu with walnuts and gingerbread 

Hello everybody, 

the tiramisu .... I do not stop to say how much I like this dessert, on my blog you will find the classic tiramisu, the [ tiramisu light or tiramisu diet ](<https://www.amourdecuisine.fr/article-25345349.html>) , the [ tiramisu without eggs ](<https://www.amourdecuisine.fr/article-tiramisu-vous-ai-je-dit-a-quel-point-je-l-aime--44008438.html>) , of [ tiramisu with homemade mascarpone ](<https://www.amourdecuisine.fr/article-tiramisu-avec-du-mascarpone-fait-maison-55272756.html>) , [ chocolate tiramisu ](<https://www.amourdecuisine.fr/article-tiramisu-au-chocolat-ou-ganache-chocolat-mascarpone-62045810.html>) .... 

so I love it with all the sauces, and today it's going to be a tiramisu with some [ homemade gingerbread ](<https://www.amourdecuisine.fr/article-pain-d-epice-tres-moelleux-et-trop-bon-49778567.html>) . 

When I saw this sublime tiramisu recipe ... .. everything was at home, sublime ...   


**tiramisu with walnuts and gingerbread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tiramisu-noix-pain-d-epice-041_thumb_1.jpg)

Recipe type:  sweet verrine  portions:  6  Prep time:  15 mins  cooking:  5 mins  total:  20 mins 

**Ingredients**

  * 400 grs of mascarpone 
  * 4 eggs 
  * 150 grams of sugar 
  * 200 grams of nuts 
  * 13 slices of gingerbread 
  * 1 big cup of the strong 



**Realization steps**

  1. crush the nuts roughly, and grill them in a pan with 20 grs of sugar, let cool 
  2. beat the rest of the sugar with the egg yolks, until the mixture whitens. 
  3. add the mascarpone and whip 
  4. turn the egg whites into firm snow. incorporate them gently into a mixture precedent 
  5. remove the crusts of the gingerbread cut in medium size, leave a slice for the decoration 
  6. soak the pieces of strong tea gingerbread 
  7. put a layer of gingerbread in your verrines 
  8. cover with a layer of the mascarpone mixture 
  9. place another layer of gingerbread, and toasted nuts 
  10. cover with the preparation 
  11. decorate with crumbs of bread and the rest of roasted nuts 
  12. now we come to the most difficult part ... book in the fridge for 4 hours before eating, and I know who will not resist .... 



Bonne journée 
