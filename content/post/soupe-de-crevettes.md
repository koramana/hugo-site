---
title: Shrimp soup
date: '2017-04-12'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/soupe-de-crevettes-014a_thumb2.jpg
---
##  Shrimp soup 

Hello everybody, 

here is my shrimp soup, a very good recipe that I really like, and even my children liked it, which really made me happy. 

When I was young, I remembered that my mother made us this soup with fresh shrimps and with their carcasses. The taste is just wonderful, especially with fresh shrimp from our beautiful Mediterranean. But me in this recipe I prepare my shrimp soup with frozen shrimps. Still, it's still good, ok! 

{{< youtube 6ZbeH4eIrP8 >}} 

the recipe without delay:   


**Shrimp soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/soupe-de-crevettes-014a_thumb2.jpg)

Recipe type:  soup  portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * shrimp (I buy the frozen) 
  * an onion 
  * a clove of garlic 
  * 2 bay leaves 
  * chopped fresh coriander 
  * thyme 
  * 1 case of pureed tomato 
  * oil 
  * salt, black pepper, paprika 
  * rice 



**Realization steps**

  1. in a pot, the onion and garlic are roasted and then blended, in the oil, 
  2. and we add the preserved tomato. 
  3. then add the shrimp, and the condiments, let it simmer a little 
  4. and then add laurel, coriander, thyme and cover with water, and let it cook 
  5. at the end of cooking if you are the type who does not like light soup, you can add a handful of rice, allow time to cook, and remove from the heat. 


