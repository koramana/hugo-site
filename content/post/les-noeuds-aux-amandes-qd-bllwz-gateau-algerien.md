---
title: Nuts with almonds, عقدة باللوز Algerian cake
date: '2011-10-31'
categories:
- les essais de mes lecteurs

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/noeud2_thumb1.jpg
---
The video of the recipe in detail is here (I create another blog, special for videos of my recipes, to lighten the blog) & nbsp; and here is the recipe for these little delights, which I found in a book by Houria Amirouchi, "Houria 1 Decoration Cakes", that the recipe was not explained too much on the book, but when you break your head we arrive at the right result. and here are the ingredients: for the dough: - 3 measures of flour - 1/2 measure of smen, ghee - 1 pinch of salt - 1/2 egg - 1/2 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.06  (  4  ratings)  0 

[ ![node2](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/noeud2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-36513347.html>)

and here is the recipe for these little delights, which I found in a book by Houria Amirouchi, "Houria 1 Decoration Cakes" 

It must be said, that the recipe was not explained too much on the book, but when one breaks the head, one arrives at the good result. 

and here are the ingredients: 

for the dough: 

\- 3 measures of flour 

\- 1/2 measure of smen, ghee 

\- 1 pinch of salt 

\- 1/2 egg 

\- 1/2 measure of water and 1/2 measure of orange blossom water, mixed. 

\- white food coloring 

or if you do not like the pasta with the measures, I recommend this pasta: 

250 gr of flour 

75 gr of margarine 

1 pinch of salt 

1 teaspoon of vanilla 

1 teaspoon of white dye 

1 cup of dye 

Orange blossom water + water 

for the stuffing: 

\- 1 measure of medium ground almond (finely ground for me) 

\- 1 measure icing sugar 

\- 2 teaspoons of vanilla 

\- 2 teaspoons of lemon zest 

\- 2 eggs 

\- Orange tree Flower water 

\- green food coloring. 

[ ![node1](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/noeud1_thumb.jpg) ](<https://www.amourdecuisine.fr/article-36513347.html>)

Preparation: 

mix the ingredients of the dough to have a soft and malleable paste, form balls and let rest. 

mix the ingredients of the almond stuffing, to have a paste not too sticky. 

now, if you use the machine, pass the dough to Numbers 4, 6, and 8 and then cut it with a number 7 mold, I still do not know how to use the machine, hihihihihi, I use the baker's roll until a fine paste, and I cut with a cookie cutter 7 cm in diameter. 

spread the almond stuffing with the roll until you get a height of 5 cm, and cut pucks 6 cm in diameter, 

place these stuffing rings on the slices of dough, and cover them with another slice of 6 cm diameter. 

decorate with a knife this dough, then put a little stuffing in the middle, wet two wedges of the dough with a little egg white, and pick them up to the middle to form your knot, and decorate with an almond. 

put in the oven at 180 degrees, and at the end immerse them in lightened honey. 

do not forget to subscribe to my newsletter to be aware of the update of my articles. 

[ ![Node3](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/noeud3_thumb.jpg) ](<https://www.amourdecuisine.fr/article-36513347.html>)

bisous 
