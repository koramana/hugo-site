---
title: Mhalabia has orange
date: '2015-02-20'
categories:
- sweet verrines
tags:
- Kitchen without egg
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mhalabia-a-lorange.jpg
---
[ ![mhalabia has orange](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mhalabia-a-lorange.jpg) ](<https://www.amourdecuisine.fr/article-mhalabia-lorange.html/mhalabia-a-lorange-2>)

##  Mhalabia has orange 

Hello everybody, 

Here is a nice dessert to the taste of the orange, a mhalabia sweet in the mouth, and rich in taste. This orange mhalabia is a recipe from a friend and reader Oum thaziri, who was better known as Batila. She has already shared with us through the lines of my blog: [ Diamond heart ](<https://www.amourdecuisine.fr/article-coeurs-de-diamant-gateaux-algeriens-2014.html>) . 

So enjoy this delicious mhalabia, as long as it is the season of oranges. and if you like these well-scented desserts, I share with you: [ mahalabia has rose water ](<https://www.amourdecuisine.fr/article-mahalabiya.html> "Mahalabiya") , [ mahalabiya with apricots ](<https://www.amourdecuisine.fr/article-mahalabiya-aux-abricots-mahalabiya.html> "mahalabiya with apricots, mahalabiya") , and [ mhalabia with lemon and fruits ](<https://www.amourdecuisine.fr/article-m-halabiya-tricolore-citron-et-fruits.html> "mhalabia - tricolor lemon and fruits") .   


**Mhalabia has orange**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mhalabia-a-lorange-1.jpg)

portions:  6  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** first layer: 

  * ½ l of milk, 
  * 2 tablespoons of maizena, 
  * vanilla, 
  * 2 to 3 tablespoons sugar according to taste, 
  * Orange tree Flower water 

2nd layer: 
  * ½ l of fresh orange juice, 
  * 2 c soups souped with maizena, 
  * 2 to 3 tablespoons of sugar according to taste 
  * the juice of half a lemon 



**Realization steps** first layer: 

  1. take 3 tablespoons of milk, and dissolve the maizena in it. 
  2. In a saucepan put the milk, the sugar, the mixture cornstarch and milk, the vanilla and the water of orange blossom 
  3. Mix with a wooden spoon over medium heat without stirring until the cream has thickened. 
  4. Pour into verrines without filling them too much and leave to cool. 

this layer: 
  1. mix the cornflour with 3 tablespoons of orange juice, to homogenize well. 
  2. in a saucepan, place all the ingredients, mix with a wooden spoon over medium heat until the cream has thickened. 
  3. gently fill the verrines and return to the fridge. 



[ ![mhalabia has orange](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mhalabia-a-lorange-2.jpg) ](<https://www.amourdecuisine.fr/article-mhalabia-lorange.html/mhalabia-a-lorange-2>)
