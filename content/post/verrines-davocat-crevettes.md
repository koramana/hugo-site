---
title: avocado verrines - shrimps
date: '2012-12-08'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

---
Hello everybody, 

here are some very good ones ** [ dirty verrines ](<https://www.amourdecuisine.fr/categorie-10678933.html>) ,  ** of the ** avocado verrines and shrimps  ** , or more precisely ** avocado cream  ** , on a thin layer of **carrot salad rapee** , and of ** roasted shrimp  ** . a recipe very easy to make but super delicious. 

besides I write you the recipe, and I really want to go buy a bag of shrimps to remake for me, because I just dripped to see the salt in it. 

you can see the recipe of the [ avocado salad with tuna ](<https://www.amourdecuisine.fr/article-salade-d-avocat-58843418.html>) , hum a delight 

ingredients: 

  * shelled shrimp (frozen for me) 
  * 1 clove of garlic 
  * a soup of olive oil 
  * salt, black pepper, cumin and thyme 
  * 2 carrots of medium size 
  * 1 clove of garlic 
  * salt 
  * juice of half a lemon 
  * a medium-sized lawyer 
  * 4 to 5 tablespoons melted philadelphia cheese 
  * black pepper 
  * salt if necessary 



method 

  1. in the blinder bowl put the cleaned avocado and cheese 
  2. puree and season with salt and black pepper 
  3. cool 
  4. brown the shrimp in the oil, 
  5. add the crushed garlic, and season 
  6. let the shrimp cook until the water is completely evaporated 
  7. let cool well 
  8. clean the carrots and grate them. 
  9. add the grated garlic 
  10. season with salt and lemon juice 
  11. in small verrines place in the beginning shrimp, carrot salad, and finally cream of cheese 
  12. decorate according to your taste 
  13. place in the fridge until serving 



thank you for your visits 

have a good day 

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
