---
title: Ore smoothie
date: '2015-09-21'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-aux-oreo-2.jpg
---
[ ![oreo smoothie 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-aux-oreo-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-aux-oreo-2.jpg>)

Hello everybody, 

Another smoothie, and oreo .... I see my little drooling already far, hihihih ... At home they like the oreos, not moir to be frank ... I see nothing delicious in this cake, I find it too sweet for my taste ... Finally, it is like cakes in England, they always like a little more sugar in their cakes ... 

In any case, a smooth and delicious drink that shares with us Lunetoiles, thank you my sweet for sharing. 

**Ore smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothies-aux-oreo-1.jpg)

portions:  2  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 4 Oreos 
  * 2 tablespoons of chocolate sauce 
  * 1 cup of milk 
  * 6 scoops of vanilla ice cream 
  * 4 ice cubes 
  * 2 frozen bananas 
  * 3 drops of vanilla essence 



**Realization steps**

  1. Place a large glass in the refrigerator to cool. 

Vanilla Smoothie: 
  1. Add half ice cream (3 teaspoon vanilla ice cream), half a cup of milk, two ice cubes and one frozen banana in the blender along with the vanilla essence. 
  2. Mix until you get a consistent thickness, then pour into the glass that has been in the refrigerator and place it in the refrigerator to keep it cool. 

Chocolate Smoothie: 
  1. Add 3 Oreo and the rest of the ingredients to the blender. 
  2. Mix until smooth. 
  3. Take big glasses; alternate by pouring the vanilla smoothie and the chocolate smoothie into the glasses (as for a marbled) 
  4. Once the glasses are full, decorate with chocolate sauce as circles on top of the smoothies. 
  5. Put a toothpick in the chocolate sauce to create fun patterns. 
  6. Crumble the last Oreo on the top. 
  7. Enjoy! 



[ ![Ore smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-aux-oreo.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/smoothie-aux-oreo.jpg>)
