---
title: tuna slippers
date: '2014-09-21'
categories:
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chausson-a-la-poele-005.CR2_thumb1.jpg
---
![slipper with the stove 005.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chausson-a-la-poele-005.CR2_thumb1.jpg)

Tuna slippers 

Hello everybody, 

Here is a recipe for dough with slippers that I adopt, since having tried it, I even made it in a sweet version, [ slippers with figs ](<https://www.amourdecuisine.fr/article-chaussons-aux-figues-et-nutella.html> "slippers with figs and nutella") and it was a treat. 

It's a little like the stuffed kesra stuffed pancake, known as Kesrat echahma, but it's a smaller version and stuffed with tuna ... 

**tuna slippers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chaussons-a-la-poele-au-thon-016.CR2_2-300x199.jpg)

portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for the paste of slippers: 

  * 500 g flour. 
  * 1 teaspoon salt. 
  * 100 g melted butter. 
  * 1 egg. 
  * 1 tbsp instant yeast. 
  * 150 to 200 ml of warm water for the dough 

for the tuna stuffing: 
  * 1 can of tuna 
  * 2 onions 
  * 3 tomatoes 
  * 2 cloves garlic 
  * salt, pepper, coriander powder. 



**Realization steps** first prepare the stuffing, 

  1. cut the sliced ​​onions 
  2. return to the oil you drain from the can of tuna. 
  3. add the crushed garlic, then the tomatoes cut into small cubes, and add the condiments according to your taste, let everything cook well. 
  4. let cool 
  5. when the stuffing is cold, add in the tuna. 

prepare the pasta now:   
[Url: 1] [img: 1] [/ url] 

  1. put the flour and the salt in a salad bowl or the bowl of the robot, mix 
  2. add the baking powder, then the melted butter, sand the dough 
  3. add the egg and water gradually, work well the dough. 
  4. form a ball and let rise the dough (sheltered drafts) under a towel, for about 1 hour (for my part, I put everything in the bread machine, I program "pizza pie" and I turn on the machine) 
  5. then divide the dough into 12 equal balls. 
  6. spread each ball in a circle 
  7. garnish with a good amount of tuna stuffing, not too much to avoid overflowing during cooking. 
  8. close the slipper well then solder well with a fork. 
  9. cook the slippers as you go, in a large skillet 4 to 5 minutes on each side without adding fat. 
  10. enjoy with a nice salad. 



click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
