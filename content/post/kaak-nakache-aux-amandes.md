---
title: Kaak nakache with almonds
date: '2014-07-22'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/kaak-nakache-3_thumb1.jpg
---
#  ![Kaak nakache with almonds](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/kaak-nakache-3_thumb1.jpg)

##  Kaak nakache with almonds 

Hello everybody, 

here is the recipe of one of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) that I prepare for the aid el fitr [ 2011 ](<https://www.amourdecuisine.fr/article-aid-el-fitr-2011-les-gateaux-algeriens-82873081.html>) this time it's a **Algerian cake** , at the same time simple, but which takes time to do, because the decoration is done with the **cake tongs** , at the very beginning our mothers and grandmothers were decorating the kaak nakache with "the tweezers" yes, yes .... 

you see the crowns in the middle, well they are with the tweezers (new, not use before, hihihihihi) 

in any case the result is super nice, but to make a groin of parts I took almost 2 hours, especially me who still finds it difficult to use the clip decoration. 

usually, this cake is stuffed with the **paste of dates** but because I did [ Makrout For Dates ](<https://www.amourdecuisine.fr/article-26001222.html>) I wanted to stuff my **crowns** this time with almonds, a super delicious recipe, from this [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) whom I share with you with great pleasure. 

Before starting the recipe I want to explain the word **measured** which is always a question mark for my readers, in the [ Algerian cakes recipes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , our grandmothers who did not have scales at the time, used a kitchen utensil they called measure, this utensil could be: a glass of water, a bowl, a box of margarine, a cup, so it all depends on how much you want to get cake, and the cakes were perfect, so to avoid going out of the recipes of our grandmothers we continue to use the word measure in the ingredients of our recipes.   


**Kaak nakache with almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/kaak-nakache_2-300x224.jpg)

portions:  45  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 3 measures of flour 
  * 1 measure full of smen 
  * 1 measure of water mix with orange blossom water 
  * 1 pinch of baking powder 
  * 1 teaspoon of vanilla 
  * ½ measure of sugar 
  * 1 pinch of salt 

for the almond stuffing: 
  * 3 measure almonds 
  * 1 measure icing sugar 
  * vanilla sugar 
  * the zest of a lemon 
  * 2 tbsp. soup of melted butter 
  * eggs (to pick up the stuffing) 



**Realization steps**

  1. sift the flour, the salt; sugar and vanilla, 
  2. add the melted and cooled fat 
  3. mix everything by rubbing well between the hands to adhere the fat in the flour and then collect with the water of orange blossoms to obtain a smooth and firm paste, 
  4. cover and let stand for half an hour. 
  5. form balls of 30 grs each. 
  6. form a hollow in the ball and fill it with almond stuffing. 
  7. enclose the ball and form a pudding, its length will be equal to your 8 fingers of both hands together 
  8. join both ends to form a circle, if necessary with a little water so that the circle does not open. 
  9. decorate according to your taste with the cake tongs 
  10. place in a floured baking sheet 
  11. cook in a preheated oven at 180 degrees C for 15 minutes 



thank you very much for your visits and comments 

bonne journée. 
