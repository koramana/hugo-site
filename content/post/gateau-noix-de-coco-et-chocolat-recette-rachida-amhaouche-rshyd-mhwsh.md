---
title: coconut and chocolate cake, recipe "rachida amhaouche" رشيدة امهاوش
date: '2013-03-19'
categories:
- juice and cocktail drinks without alcohol
- Detox kitchen
- Healthy cuisine
- recipes at least 200 Kcal

---
Hello everybody, 

here is a delicious recipe for coconut cake, which I found on the book Rachida amhaouche, and I love to prepare to present it in the party at school for my children ... 

then I pass you the ingredients and I put you the changes that I made: 

Ingredients: 

  * 450 gr of flour 
  * 1 small glass of melted butter (100 grs for me) 
  * 200 gr of caster sugar 
  * 1 sachet of baking powder 
  * 1 sachet of vanilla sugar 
  * 4 eggs 
  * 300 grams of chocolate 
  * 180 grs of grated coconut 
  * 150 ml of milk that I have added (missing ingredient of the original recipe) 

In a container, put the flour, the melted butter, 100 gr of caster sugar, the yeast, the vanilla sugar, the egg yolks (reserve the whites) and mix well, and this is what we see between our hands a sandy dough that can not even be shaped, and we say there is something missing ????, so I choose to choose the milk as an ingredient that will give me a maliable dough all thinking of ultimately seeing a fluffy cake 

and then I add 150 ml of milk 

place this dough on a baking tin well butter. 

in a bain-marie melt the 300 gr of chocolate, pour the chocolate over the dough and let soften a little. 

to assemble the egg whites in snow by gradually incorporating 100 grs of caster sugar, 

at the end add the grated coconut and mix from bottom to top in a circular motion with a spatula. 

Wait until the chocolate cools and spreads the egg whites over the snow, cook in a preheated oven at 190 degrees. 

let the cake cool a little at the exit of the oven before cutting it into edges or triangles (it's very beautiful also in triangles) 

I was really in a hurry, so I had to cut the cake very quickly, and I was treated to chocolate that was flowing everywhere (after I tell myself why I'm gaining weight, hihihihihi) 
