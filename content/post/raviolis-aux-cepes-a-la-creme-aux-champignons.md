---
title: porcini ravioli with mushroom cream
date: '2017-11-03'
categories:
- diverse cuisine
- Cuisine by country
- recipes of feculents
tags:
- Pasta
- Mushroom sauce
- sauces
- Italy
- Full Dish
- dishes
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/11/raviolis-aux-c%C3%A8pes-%C3%A0-la-cr%C3%A8me-aux-champignons-1.jpg
---
![ravioli with ceps with mushroom cream 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/raviolis-aux-c%C3%A8pes-%C3%A0-la-cr%C3%A8me-aux-champignons-1.jpg)

Hello everybody, 

We love the pasta at home, frankly it's the only dish, where you hear nobody claim! Miracle of Italian cuisine. Yes, because at home we love [ Bolognese lasagna ](<https://www.amourdecuisine.fr/article-lasagne-a-bolognaise.html>) , the [ ravioli ](<https://www.amourdecuisine.fr/article-raviolis-au-saumon-et-sauce-rosee.html>) (whatever their joke), [ pasta with smoked salmon ](<https://www.amourdecuisine.fr/article-pates-au-saumon-fume-facile-et-rapide.html>) , [ gratin pasta with tuna ](<https://www.amourdecuisine.fr/article-gratin-de-pates-au-thon.html>) ... and I pass the list is very long. 

For today, I share with you the recipe for porcini ravioli (Porcini, mushrooms) with mushroom cream, I will present this recipe for entry, in small quantities, because it is a very hearty dish and very rich. 

I had thought of all the possible recipes with wild mushrooms, but to be honest, the homemade ravioli stuffed with tasty boletus made me salivate, although the recipe was going to take a long time, but frankly, prepare these own ravioli, to make its own farce according to its taste, is worth all the trouble of the world ... Greedy that I am !! 

First of all I started making ravioli, I made a good amount, with different stuffings, and I put in the freezer ... to have my pasta on hand, whenever you want to have a pasta fiesta at home. 

{{< youtube Ct3jFC430aY >}} 

Before you pass the recipe for these cep mushroom ravioli, I want to thank Martine, for this nice ingredient that she offered us, as I would also like to announce the next godmother of our game: 

I also thank all the previous godmothers of our game, without them, we would not be here, in this round today: 

You can see the video of the preparation of these porcini ravioli with mushroom cream here: 

**porcini ravioli with mushroom cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/raviolis-aux-c%C3%A8pes-%C3%A0-la-cr%C3%A8me-aux-champignons.jpg)

**Ingredients** for the pasta: 

  * 3 medium eggs at room temperature 
  * 300 gr of flour 
  * 1 C. tablespoon of olive oil 
  * salt 

for stuffing ravioli: 
  * 2 tbsp. tablespoon of olive oil 
  * 2 cloves garlic 
  * 50 gr dried ceps (hydrated in 500 ml boiling water) 
  * salt and black pepper 
  * 2 tbsp. grated Parmesan cheese 
  * 2 tbsp. chopped parsley. 

for mushroom cream: 
  * 1 C. butter (lightened if possible) 
  * 1 C. tablespoon of olive oil 
  * 150 gr of various mushrooms (you can always use porcini mushrooms) 
  * salt and black pepper 
  * 1 C. basil (optional, personal I really like) 
  * 1 shallot cut into a slice. 
  * 240 ml of liquid cream 
  * 1 C. coffee with truffle oil 
  * 2 tbsp. Parmesan 
  * 2 tbsp. chopped parsley. 



**Realization steps** prepare the ravioli pasta: 

  1. place the flour, eggs, salt and oil in the bowl. operate the kneader at reduced speed, and let the dough get picked up, if your eggs are a little small, and the flour does not pick up completely, just add a little water. 
  2. pick up the ball pasta, cover with cling film, and let rest on your worktop. 
  3. Prepare the stuffing: 
  4. Moist dry ceps in boiling water between 30 minutes and 1 hour, drain and wring out their water, and cut roughly. 
  5. sauté the crushed garlic in the oil, when it takes a nice color, we add the mushrooms. 
  6. cook for 6 to 8 minutes until reduced. 
  7. add salt and pepper, then add parmesan and parsley. 
  8. fry a little and puree in the blender. 

preparation of ravioli: 
  1. Take the dough after a rest of at least 30 minutes, and spread it out, to the roller patissier (if you want to work your bisepses), perso I preferred to work in the mill. 
  2. divide the dough in half, so as not to have very long bungs. and spread on a well floured plan. 
  3. fold it over three, spread it out again, and fold again, then start spreading it finer, squeezing the rolls of your dough machine each time. until you have a band with a thickness of almost 1 mm. 
  4. put the strip on a flouring plan, use the ravioli mold to delicately trace the impression (this will allow you to know where to put the stuffing) 
  5. place the equivalent of a teaspoon of stuffing in the middle. 
  6. with a brush, put some egg white in the inside of the print, and cover with another layer of dough. 
  7. press well to adhere both pasta, it is best not to leave too much air between the two pasta. 
  8. cut the ravioli with the help of your ravioli mold, and place them on a clean cloth. 
  9. place the water to boil and proceed to the preparation of the cream. 

preparation of the cream: 
  1. melt the butter with a little oil 
  2. add the mushrooms cleaned, washed and cut in the pan, make well jump 
  3. dirty, pepper and add basil (if you like otherwise it's optional) 
  4. place the ravioli in boiling water for 6 to 8 minutes. 
  5. add the liquid cream to the mushrooms, and let it cook 
  6. the ravioli are removed from the water and placed directly in the sauce pan. let's go back a few minutes 
  7. truffle oil, parmesan and parsley are added 
  8. let it simmer for a few minutes, remove and serve immediately. 



![ravioli with porcini mushroom cream 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/raviolis-aux-c%C3%A8pes-%C3%A0-la-cr%C3%A8me-aux-champignons-2.jpg)

**1- Soulef** of the blog [ Love of cooking ](<https://amourdecuisine.fr/>) _with some_ raviolis aux cèpes avec une crème aux champignons 
