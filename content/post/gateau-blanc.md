---
title: white cake
date: '2008-01-14'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- diverse cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- pizzas / quiches / pies and sandwiches
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/01/210222761.jpg
---
this is a cake that I prepare based on genoise and cream with fresh cream; for the genoise: 120g flour 120g caster sugar 4 eggs 1/2 sachet baking powder 1 pinch of salt Beat the eggs with the caster sugar at maximum speed, add the flour to which the baking powder and salt have been added. Butter and flour a mold, pour the mixture and put in oven 15min at 180 ° Cut in half horizontally. Syrup: 600ml water 300g caster sugar 1 sachet vanilla sugar Prepare a syrup with the three ingredients and & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![bonj21](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/210219261.gif)

this is a cake that I prepare based on genoise and cream with fresh cream; 

![S7301970](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/210222761.jpg)

for the genoise: 

  * 120g flour 
  * 120g caster sugar 
  * 4 eggs 
  * 1/2 sachet baking powder 
  * 1 pinch of salt 



Beat the eggs with the caster sugar at maximum speed, add the flour to which the baking powder and salt have been added. 

Butter and flour a mold, pour the mixture and put in the oven 15min at 180 ° 

![S7301955](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/210224441.jpg)

Cut in half horizontally. 

** Syrup:  **

  * 600ml water 
  * 300g caster sugar 
  * 1 sachet vanilla sugar 



Prepare a syrup with the three ingredients and sponge the two circles of sponge very well, until the syrup flows by the edges. 

**_ Garnish :  _ **

  * 400ml fresh cream 
  * 5 tablespoons icing sugar (or according to taste) 
  * some drops of aroma to your taste 
  * almond paste 
  * Chocolate Chip 



Beat the cream with the icing sugar to have a firm whipped cream and divide in two parts. 

Spread the first on the first disc of sponge cake, cover with the second disc and spread the second part of the whipped cream, 

![S7301957](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/210225601.jpg)

![S7301958](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/210226141.jpg)

![S7301961](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/210227091.jpg)

![S7301970](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/210222761.jpg)

I finished the filling with almond paste and chocolate peppers. 

I wanted to make the almond paste beautiful flower but adding the liquid dye (I have no powder) my almond paste has become very mole. 

have a good meal 

![bye](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/210227551.gif)
