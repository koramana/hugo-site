---
title: Celeste Chicken Salad
date: '2011-01-07'
categories:
- Bourek, brick, samoussa, slippers
- houriyat el matbakh- fatafeat tv
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/salade-au-poulet11.jpg
---
today, I'm going to share a recipe with you that comes directly from Piru, from a friend and reader very dear to my heart: Celeste, she is very nice, she calls me sometimes on the phone, she always leaves me comments, a very nice woman. So it is with great joy and pleasure that I share with you this nice recipe, which she made on the occasion of Christmas, it is a small salad in the shape of a fir tree. so without delay, I give you his recipe: 2 cooked chicken breasts 6 celery stems chopped 3 cups of seedless red grapes, cut in half 10 walnuts & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![salad au poulet.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/salade-au-poulet11.jpg)

today, I'm going to share a recipe with you that comes directly from Piru, from a friend and reader very dear to my heart: Celeste, she is very nice, she calls me sometimes on the phone, she always leaves me comments, a very nice woman. 

So it is with great joy and pleasure that I share with you this nice recipe, which she made on the occasion of Christmas, it is a small salad in the shape of a fir tree. 

so without delay, I give you his recipe: 

  * 2 cooked chicken breasts 
  * 6 chopped celery stalks 
  * 3 cups of seedless red grapes, cut in half 
  * 10 walnuts 
  * 1 ½ cup yogurt with a little lemon juice 
  * 1 ½ cup mayonnaise 
  * Salt, pepper, fresh rosemary 
  * Cherries for decoration 



preparatation 

  1. chop and peel the chicken and chop it roughly. 
  2. In a large bowl, combine chicken, celery, raisins and walnuts. 
  3. In another bowl, combine yogurt, lemon, mayonnaise, lemon juice and seasonings. 
  4. Combine it with the chicken mixture. 
  5. Cover a tree-shaped mold with a food film 
  6. place the mixture in it and press with the back of the spoon 
  7. cover with another layer of food film and refrigerate 
  8. invert the salad when serving or an hour before 
  9. Decorate with cherries. 



Thank you Celeste for this salad, and have the next recipe. 

bisous 
