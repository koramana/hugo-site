---
title: cheese slippers
date: '2016-06-24'
categories:
- amuse bouche, tapas, mise en bouche
- Bourek, brick, samoussa, slippers
tags:
- Ramadan
- Easy recipe
- Algeria
- Hot entries
- inputs
- Vegetarian cuisine
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chaussons-au-fromage-1.jpg
---
![cheese slippers 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chaussons-au-fromage-1.jpg)

##  Cheese slippers, 

Hello everybody, 

One of my readers recently asked me for the recipe for simple and easy cheese-based slippers ... I was sure I already put the recipe on my blog, but when I went to get it, there was no recipe ... . It's amazing when we are sure of one thing and in the end we are completely next to the plate! What head do we have? 

And yet I was more than sure I put it, I remember when Lunetoiles had sent me the photos of his realization, I had sorted the photos, I had signed them, and put on the blog, and I had even made an intro ... Niet! Looks like it never knows past !!! No matter, I always keep the emails of Lunetoiles and the photos she sends me, so here's her recipe cheese slippers. 

![cheese slippers 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chaussons-au-fromage-2-686x1024.jpg)

**cheese slippers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chaussons-au-fromage.jpg)

portions:  20  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** For the dough: 

  * 300 g of flour 
  * 200 g butter diced 
  * 1 small egg roughly beaten 
  * 5 cl of milk 
  * 2 tbsp. white vinegar 

For garnish : 
  * 120 g of feta 
  * 120 g of ricotta 
  * 1 C. coffee milk 
  * 2 tbsp. finely grated Parmesan cheese 
  * ½ c. coffee of dried mint crumbled between the fingers 
  * 1 small egg 
  * ½ c. sweet paprika 

For icing: 
  * 1 small egg 
  * 1 C. coffee milk 



**Realization steps** Prepare the dough: 

  1. Put in a salad bowl the flour, the butter and ½ teaspoon of salt. 
  2. Mix to form a sandy mixture. 
  3. Stir in the egg and milk and continue kneading to obtain a smooth paste. Add the vinegar and knead to form a ball. Flatten it slightly, wrap it in plastic wrap and put it about 1 hour in the refrigerator. 
  4. Preheat the oven to th. 6/180 ° and cover 2 baking sheets with parchment paper. 

Prepare the filling: 
  1. Crush the feta with a fork in a bowl and knead to form a smooth paste. 
  2. Stir in ricotta, parmesan and mint. Add 1 egg and paprika, and mix well. 

For icing: 
  1. Whisk 1 egg with the milk in a bowl. 

Shaping: 
  1. Spread the dough on a work surface lightly floured in 1 layer of 3 mm thick. 
  2. Cut out, with a punch or with a glass, circles of Ø 10 cm, losing as little space as possible. 
  3. Collect the leftovers if there are a lot, knead them and spread the dough to form new circles. 
  4. Apply icing on the edges with a food brush.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chaussons-au-fromage-4.jpg)
  5. Pour 1 teaspoonful of filling into the center of each circle, spreading it slightly in oval (the amount of filling should be sufficient to make the half-moons generous, but without the risk of exploding when cooking). 
  6. Fold the rounds of dough by grasping it between your thumb and forefinger to cover the filling. 
  7. Pull on both ends to lengthen and taper the half-moons a little. Press the edges firmly to close them, then hem them by slightly raising the bottom edge to properly seal the trim. 
  8. Arrange the half-moons on the baking sheets and brush them with the remaining glaze. 
  9. Bake one plate at a time, about 20 minutes. Let cool a little so that the half-moons are hot but not hot. 



![cheese slippers 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chaussons-au-fromage-3-799x1024.jpg)
