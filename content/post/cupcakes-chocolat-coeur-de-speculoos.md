---
title: chocolate cupcakes speculoos heart
date: '2016-07-26'
categories:
- Cupcakes, macaroons, and other pastries
- Chocolate cake
tags:
- desserts
- ganache

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/cupcakes-spartacus-6_thumb_11.jpg
---
##  chocolate cupcakes speculoos heart 

Hello everybody, 

Spartacus Cupcakes, a weird name for these delicious and beautiful chocolate cupcakes containing a speculoos heart. 

**chocolate cupcakes speculoos heart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/cupcakes-spartacus-6_thumb_11.jpg)

**Ingredients** For 20 cupcakes: 

  * 140 g flour 
  * ½ bag of baking powder 
  * 6 eggs 
  * 160g caster sugar 
  * 400 g of dark chocolate 
  * 240g of soft half-salt butter 
  * 1 jar of speculoos paste 

Topping: 
  * 450 g cream cheese 
  * 4 tbsp. creamy icing sugar 
  * 1 tbsp. tablespoon of speculoos 
  * 5 speculoos biscuits crumbled 
  * pralines 



**Realization steps**

  1. Preparation: 
  2. Preheat the oven to 165 ° C (th.5-6). 
  3. Melt dark chocolate in a bain-marie. 
  4. In a bowl mix the flour and the yeast. 
  5. In another bowl, beat the eggs with the powdered sugar. 
  6. Add the melted dark chocolate, to the egg / sugar mixture, mix well. Add the butter, then add the flour-yeast mixture. 
  7. Arrange paper boxes in muffin pans. 
  8. Pour a pastry in each box and put a spoon. teaspoon of speculoos paste. 
  9. Pour the rest of the dough until ¾. 
  10. Bake the cupcakes for 15-20 minutes at 165 ° C. 
  11. Let cool completely. 

Topping: 
  1. In a bowl, whip the cream cheese and icing sugar with an electric mixer. 
  2. Add the speculoos paste. Transfer this preparation into a piping bag with a star-shaped tip. 
  3. Decorate your cupcakes. Add on the top some crumbs of speculoos and some crushed praline. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
