---
title: Moroccan Batbout "بطبوط مغربي"
date: '2014-05-22'
categories:
- Moroccan cuisine
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/batbout-marocain-pain-maison-marocain.CR2-001.jpg
---
[ ![Moroccan batbout or Moroccan homemade bread](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/batbout-marocain-pain-maison-marocain.CR2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/batbout-marocain-pain-maison-marocain.CR2-001.jpg>)

##  Moroccan Batbout "بطبوط مغربي" 

Hello everybody, 

Here are super fondants and delicious Moroccan batbouts that I like to prepare quite often, because children love them a lot in sandwiches, they are delicious, light, and in addition very easy to do and cook. 

a very good [ bread ](<https://www.amourdecuisine.fr/categorie-10678924.html>) house of the [ Moroccan cuisine ](<https://www.amourdecuisine.fr/categorie-12359409.html>) , which can easily accompany many dishes and salads, and that you can present stuffed according to your taste. 

**Moroccan batbout "بطبوط مغربي"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/batbout-marocain.CR2_.jpg)

portions:  10  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * 1 kilo of flour 
  * 1 case of salt 
  * 40 gr of baker's yeast. 
  * 1 little table oil 
  * Luke warm water. 



**Realization steps**

  1. pour the flour on a work plan, 
  2. make a pit, add the salt on the flour and then add fresh yeast, a small trickle of table oil and gradually pour warm water while kneading. 
  3. It takes a good twenty minutes of kneading efficient, energetic; Do not hesitate to knead with your fists, the dough will make small noises "clak" "clak" when it is well kneaded. 
  4. In the end, you have to have a nice soft dough while remaining manipulable. 
  5. We pass a little oil on the formed ball of dough so that it does not form skin and then we cover it with a clean cloth and let it rise, well lift ... 
  6. Once raised, with the fist we degas the dough and then on a floured work plan, we form small balls the size of an egg. 
  7. Then we spread each ball, a bun of about 5 mm thick, do not make them thick otherwise they will be heavy, not hollow inside. 
  8. We put the rolls on a clean floured napkin and cover them with another fabric + 1 small blanket so that they rise quickly. 
  9. It is really necessary to let them rise a little before cooking them in a stove. 



[ ![Moroccan batbout 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/batbout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/batbout1.jpg>)

bonne degustation 
