---
title: Dress in the blank
date: '2012-11-27'
categories:
- appetizer, tapas, appetizer
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/mkhabez-au-flan-521.CR2_thumb1.jpg
---
##  Dress in the blank 

Hello everybody, 

here is a super delicious dry cake Mkhabez the flan, very easy to make, and super presentable with this touch of frosting that coats the cake generously. 

This small dry cake takes the look of a mkhabez, and what adds the most, it is this powder of flan which one incorporates there, and which gives an additional flavor. 

**Dress in the blank**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/mkhabez-au-flan-521.CR2_thumb1.jpg)

**Ingredients**

  * 1 packet of pistachio flan (the large sachet which makes 1 liter of flan) 
  * 1 teaspoon of pistachio aroma 
  * 120 gr of butter 
  * 1 egg 
  * 1 yellow 
  * 1 teaspoon baking powder 
  * 250 gr of flour (if the egg is large, add a little more flour to have a malleable dough not sticky) 

icing: 
  * 1 egg whites 
  * 1 tbsp of lemon juice 
  * 1 teaspoon of rosewater 
  * Icing sugar 
  * Green food coloring 



**Realization steps**

  1. In a bowl mix the bag of flan with the aroma of pistachio, and the soft butter, 
  2. Add egg and yolk while mixing. 
  3. gently stir in the flour with the baking powder 
  4. Form a ball of dough, which you will spread on a floured worktop. 
  5. Cut out shapes of your choice with small pieces, because the cake swells a lot during cooking. 
  6. place the cakes on a baking sheet, covered with baking paper, 
  7. cook 15 to 20 while watching, let cool before frosting. 
  8. Prepare the icing with the ingredients mentioned above, it must not be flowing or too thick, 
  9. always test on a cake and grind by adding icing sugar until you get the desired texture. 
  10. let it dry in the open air before storing the cake in an airtight container. 


