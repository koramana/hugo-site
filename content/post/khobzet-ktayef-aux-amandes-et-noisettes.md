---
title: khobzet ktayef with almonds and hazelnuts
date: '2016-01-06'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- sweet recipes
tags:
- Algerian cakes
- Algeria
- Algerian patisserie
- delicacies
- Aid
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/khobzet-ktaifs-1.jpg
---
[ ![khobzet ktaifs 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/khobzet-ktaifs-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/khobzet-ktaifs-1.jpg>)

##  khobzet ktayef with almonds and hazelnuts 

Hello everybody, 

During Ramadan, and after [ qalb elouz ](<https://www.amourdecuisine.fr/article-qalb-el-louz-au-lben-babeurre-recette-inratable.html>) and the [ Basboussa ](<https://www.amourdecuisine.fr/article-besboussa-gateau-algerien.html>) , khobzet ktayef is a must for tea at night ... I always liked the ktayefs especially when I find the beautiful dough in the trade, because with ktayefs, we do not need to stay two hours or more to make a good dessert ... 

Just air the dough (angel hair), put the fat and then spread it in the mold ... when the stuffing variations are many, and each means these edges. And besides, it's small enough to have stuffed qtayefs, you do not need a kilo of almonds, or hazelnuts, or anything else. 

So we must admit that qtayefs is a little child's game, you just know cook khobzet ktayefs ... to know if you like a sweet dough in the mouth, or a dough a little crispy. 

For me this time, it is a khobzet ktayef with almonds and hazelnuts, very sweet on the palate, very rich in perfume and flavors ... a dessert that did not last long .... 

recipe in Arabic: 

[ ![khobzet ktaifs 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/khobzet-ktaifs-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/khobzet-ktaifs-3.jpg>)   


**khobzet ktayef with almonds and hazelnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/khobzet-ktaifs-5.jpg)

Recipe type:  Algerian cake  portions:  12  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 1 packet of pasta with ktayefs (400 gr) 
  * 100 gr of butter 

prank call: 
  * 100 gr roasted and coarsely ground almonds 
  * 150 gr roasted and coarsely ground hazelnuts 
  * ¼ cup of cinnamon powder 
  * orange blossom water to pick up 
  * and a few tablespoons of sugar (not a lot you will sprinkle with syrup thereafter) 
  * 50 gr of butter in pieces for cooking 

Syrup: 
  * 500 g of sugar 
  * ½ l of water, 
  * orange blossom water according to taste 
  * 2 to 3 tablespoons of honey 



**Realization steps**

  1. air the ktayefs and try to separate the dough, then add the melted butter 
  2. Rub gently between your hands to absorb the fat in the ktayefs. 
  3. Butter a mold about 30 cm in diameter, 
  4. put a first layer of ktayefs, well compact especially. 
  5. cover this layer with hazelnut almond stuffing. 
  6. Then cover the almond stuffing with another layer of ktayefs 
  7. place pieces of butter on top, and bake in a preheated oven at 180 degrees C for almost 20 min, on one side   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/ktayefs.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/ktayefs.jpg>)
  8. You can like me turn the cake in another mold of the same size to cook the other side. 
  9. Cook for another 20 minutes 
  10. Meanwhile prepare the syrup with sugar and water over low heat, after boiling it should be boiled for about 15 to 20 minutes. 
  11. At the end add a little orange blossom water and a few spoons of honey 
  12. Sprinkle the potatoes a little warm, with the syrup a little hot, or even lukewarm, to obtain soft and crispy ktayefs on the outside. 
  13. Or sprinkle them with cold syrup and hot katayefs for crispy ktayefs. 



[ ![khobzet ktaifs](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/khobzet-ktaifs.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/khobzet-ktaifs.jpg>)
