---
title: Rolled potato cheese garlic and herbs
date: '2017-07-17'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- ramadan recipe
- recipes of feculents
tags:
- inputs
- Vegetarian cuisine
- Healthy cuisine
- Easy cooking
- Economy Cuisine
- Ramadan
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Roul%C3%A9-de-pomme-de-terre-au-fromage-ail-et-fines-herbes-1.jpg
---
[ ![Potato roll with garlic and herbs cheese](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Roul%C3%A9-de-pomme-de-terre-au-fromage-ail-et-fines-herbes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Roul%C3%A9-de-pomme-de-terre-au-fromage-ail-et-fines-herbes-1.jpg>)

##  Rolled potato cheese garlic and herbs 

Hello everybody, 

The potato rolls are always a delight that can accompany a soup, a salad, even a [ roast leg ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-au-four-aid-el-adha-2012-111759550.html>) or one [ roast chicken ](<https://www.amourdecuisine.fr/article-poulet-roti-au-four.html>) . 

I already realized a [ rolled potato with minced meat ](<https://www.amourdecuisine.fr/article-roule-de-pomme-de-terre-a-la-viande-hachee.html>) who always comes back to my table at the request of my little tribe. 

Today Lunetoiles shares with us a version without meat, all well perfumed, and here is what she tells us: 

Here, as I wanted to prepare the potato roll, which goes around the blogs and even on the Facebook group, I chose to garnish mine cheese spread with garlic and herbs, it is eaten cold and accompanied by a salad. I find that the onion grilled brings a better flavor to the rolled, and that the potato marries too well with the fresh cheese garlic and herbs.   


**Rolled potato cheese garlic and herbs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Roul%C3%A9-de-pomme-de-terre-au-fromage-ail-et-fines-herbes.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 1 kg of potatoes 
  * 1 onion 
  * 4 eggs 
  * 2 C. Cream soup 
  * Salt pepper 
  * 200 g garlic and herbs cheese 
  * portion of cheese (laughing cow) 



**Realization steps**

  1. Finely chop the onion. Fry in a hot pan with a drizzle of olive oil, salt and pepper until coloring. 
  2. Peel the potatoes and rinse them under cold water. Grate them with the big holes of the rasp. 
  3. Drain them in a colander pressing them with the back of a spoon. 
  4. In a salad bowl whip the omelette eggs with the cream and add the grated potatoes and the brown onion. 
  5. Salt, pepper. 
  6. Pour the preparation on a baking sheet lined with parchment paper, in a large rectangle about 5 mm thick. 
  7. Bake for 30 minutes at 180 ° C - 190 ° C until well browned. 
  8. Remove the tray from the oven. 
  9. Spread cheese portions (laughing cow) and fresh cheese garlic and herbs all over the surface. 
  10. Roll it up and wrap it in cling film. 
  11. Let the roll stand in the refrigerator for 1 hour before slicing. 



[ ![Potato roll with garlic and herbs cheese](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Roul%C3%A9-de-pomme-de-terre-au-fromage-ail-et-fines-herbes-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Roul%C3%A9-de-pomme-de-terre-au-fromage-ail-et-fines-herbes-2.jpg>)
