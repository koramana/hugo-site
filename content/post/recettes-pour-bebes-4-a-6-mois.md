---
title: Recipes for BEBES (4 to 6 months)
date: '2008-04-05'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/04/240413011.jpg
---
Now Ines is 5 months old, she is going to be 6, and I started 2 months ago to feed her. 

and as I know that to make beautiful recipes for babies is not easy at all, I get a book here, Annabel Karmel, it must be said that there are good ideas on it, 

I share them with you, for a start here are 2 recipes: 

** Mashed Zucchini and Banana  **

![purreeBB](https://www.amourdecuisine.fr/wp-content/uploads/2008/04/240413011.jpg)

no you're not dreaming, it's mashed Zucchini and Banana, my daughter likes it a lot, she can eat it without stopping. 

Ingredients: 

  * 1 Courgette 
  * 1 banana 



cook zucchini, washed, peeled and diced with steam, until tender. 

put in a blender, and add the sliced ​​banana, mix everything together, and give to your baby, what will he enjoy? 

if you see that you have a lot, share before serving, and put half in a box with lid that can go to the freezer, and so you have a small dish for your baby in reserve, if you do not have the time, you serve him that after a short passage in the microwave. 

you will see baby will really love that. 

** Mashed sweet peas and sweet potatoes  **

![purreebb1](https://www.amourdecuisine.fr/wp-content/uploads/2008/04/240423731.jpg)

another delight, with a very sweet taste 

Ingredients: 

  * 1 sweet potato 
  * 1 handful of small weights 



boil the small weights and the sweet potato washed, peeled and diced until it is soft. 

pass them to the mixer, it's a little dry add the water of their boiling. until it becomes a mix that your baby can eat. 

si vous avez une grande quantité, gardez la moitie au congélateur pour un autre jour. 
