---
title: mushrooms stuffed with bolognese
date: '2016-04-06'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- Healthy cuisine
tags:
- Easy cooking
- Express cuisine
- Oven
- Ramadan
- dishes
- Algeria
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/champignons-farcis-a-la-bolognaise-3.jpg
---
[ ![mushrooms stuffed with bolognese](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/champignons-farcis-a-la-bolognaise-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/champignons-farcis-a-la-bolognaise.jpg>)

##  mushrooms stuffed with bolognese 

Hello everybody, 

Hum mushrooms stuffed with bolognese !!! Yesterday at the market, I did not resist these pretty wide mushrooms that could be the basis of a nice recipe. All the way back I was thinking about a recipe, an original joke to garnish these beautiful mushrooms. 

[ ![Food Battle](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bataille-food.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bataille-food.png>)

For the round # 33 Olivier chose the theme **The good life:**

Around this theme, two ingredients imposed: 

  * an Italian cheese (mozzarella, mascarpone, ricotta, parmesan ...) 
  * a fruit or seasonal vegetable (carrot, leek, chicory, cabbage ... avocado, pineapple, apple, passion fruit ....) 



![mushrooms stuffed with bolognese](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/champignons-farcis-a-la-bolognaise.jpg)

**mushrooms stuffed with bolognese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/champignons-farcis-a-la-bolognaise-1.jpg)

portions:  6  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 6 big mushrooms of Paris 
  * [ bolognaise sauce ](<https://www.amourdecuisine.fr/article-sauce-bolognaise-maison-halal-et-sans-vin.html>)
  * grated mozzarella 
  * olive oil for mushrooms 



**Realization steps**

  1. clean the mushrooms well and remove the feet. (do not wash mushrooms in abundant water, mushrooms absorb water and may reject it during cooking) 
  2. chop these feet and prepare your bolognese sauce as usual by introducing the mushrooms into the legs when cooking. let the sauce reduce well. 
  3. brush the mushrooms with the olive oil on all sides. 
  4. fill them with the well-reduced Bolognese sauce 
  5. garnish with grated mozzarella, and pre-heat oven at 180 ° C for 12 to 15 minutes 



![mushrooms stuffed with bolognese](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/champignons-farcis-a-la-bolognaise-4.jpg)
