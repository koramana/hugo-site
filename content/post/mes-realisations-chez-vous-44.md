---
title: my achievements at home 44
date: '2014-01-23'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

Once again, I share with you your achievements and recipes that you like on my blog. A nice list, and successful tests, so thank you all for your loyalty. 

I am very happy now because girls are beginning to understand the blog system, and know where to go to post the photos of their essays. 

Otherwise for those who do not know, I put you on a special page, or you can send me your photos, just go to this page: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll like ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link of your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)

So I pass you the photos of these faithful readers, waiting to receive yours: 

[ lemon cream pie ](<https://www.amourdecuisine.fr/article-tarte-au-lemon-curd.html> "lemon pie / lemon curd pie") ( [ lemon curd ](<https://www.amourdecuisine.fr/article-lemon-curd-creme-au-citron-fait-maison.html> "recipe of homemade lemon curd / lemon cream") ) at Amel (a reader) 

[ truffles the coconut ](<https://www.amourdecuisine.fr/article-truffes-a-la-noix-de-coco.html> "Truffles with coconut") revisited by Souhila (a faithful reader). 

[ Khobz dar without fluffing ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html> "khobz dar without food / homemade bread") at farnanita ida kitchen 

[ chocolate cookies ](<https://www.amourdecuisine.fr/article-biscuit-de-noel-au-chocolat.html>) , the reader did not mention her name. 

[ Kounafa has cream ](<https://www.amourdecuisine.fr/article-mini-konafa-ktayefs-a-la-creme.html> "Mini konafa, ktayefs with cream") at flower dz 

[ homemade bread ](<https://www.amourdecuisine.fr/article-comment-faire-son-pain-maison.html> "how to make homemade bread") at flower dz 

[ Floating island ](<https://www.amourdecuisine.fr/article-recette-ile-flottante-avec-video.html> "floating island recipe / with video") at flower dz 

[ Chicken mustard ](<https://www.amourdecuisine.fr/article-cuisses-de-poulet-a-la-moutarde-au-four-111087660.html> "Chicken legs with mustard, baked") at flower dz 

[ Ice cream cakes ](<https://www.amourdecuisine.fr/article-gateaux-economique-samira-kaikaates-au-flan.html> "Samira economic cakes, kaikaates with flan") at malika 

[ Olive rolls ](<https://www.amourdecuisine.fr/article-pain-aux-olives.html> "olive bread") at Suson 

[ How to make homemade bread ](<https://www.amourdecuisine.fr/article-comment-faire-son-pain-maison.html> "how to make homemade bread") at Naziha's place 

[ churros ](<https://www.amourdecuisine.fr/article-25345478.html> "recipe of Churros / Chichis, Spanish donut") , [ khechkhach ](<https://www.amourdecuisine.fr/article-khechkhach-ou-khochkhach-oreillettes-gateau-algerien.html> "khechkhach or khochkhach / Algerian cake earplugs") , and [ muffins ](<https://www.amourdecuisine.fr/cupcakes-macarons-et-autres-patisseries>) at lila33 

his remark: 

great recipes my sister I enjoy and I regale all my family, I never miss your recipes thank you thank you 

thank you all and have your next photos and why ps your own recipes, which you can post here: 

[ proposer votre recette ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")
