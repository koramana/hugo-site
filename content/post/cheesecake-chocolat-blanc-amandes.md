---
title: White chocolate cheesecake - almonds
date: '2014-11-20'
categories:
- cheesecakes and tiramisus
- sweet recipes
tags:
- Cake
- desserts
- Ramadan
- Algerian cakes
- To taste
- Soft
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/cheesecake-chocolat-blanc-amandes11.jpg
---
![cheesecake-chocolate-white-amandes1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/cheesecake-chocolat-blanc-amandes11.jpg)

##  White chocolate / almond cheesecake 

Hello everybody, 

Here is a white chocolate cheesecake and manse that was a great success, when I first posted it, there are more than 4 years ago. A recipe that I highly recommend, for small parties, or just for a small snack. 

**White chocolate cheesecake - almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/cheesecake-chocolat-blanc-amandes2.jpg)

portions:  8-10  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients** for the base: 

  * 150 gr of biscuits 
  * 50 gr toasted almonds 
  * 50 gr of melted butter 
  * ½ c. a coffee of almond extract 

first layer: 
  * 120 gr of white chocolate 
  * 50 gr of thick cream 
  * 200 g unsalted fresh cheese (soft cheese) 
  * 20 gr of sugar 
  * 1 whole egg plus one egg yolk 
  * almond extract 

2nd layer: 
  * 450 soure cream, or sweet and sour cream 
  * 50 gr of sugar 
  * ½ c. almond extract 



**Realization steps**

  1. preheat the oven to 180 degrees C 10 min before the start of cooking 
  2. prepare a mold with removable base by buttering it a little. 
  3. In a blender, mix the biscuits with the almonds to have a fine mixture, then add the melted butter and the almond extract then mix again. 
  4. pour this mixture into your mold, and heap the good with the back of a spoon, try to raise a little the odds (1.5 cm around) 
  5. cook for 5 minutes. 
  6. remove from the oven and lower the temperature to 150 degrees C 
  7. at this time, prepare the first layer, melt the white chocolate with the cream on low heat, let cool on the side. 
  8. beat the fresh cheese with the sugar, add the eggs one by one, add the cream / chocolate mixture. 
  9. while whisking, add the almond extract, then pour the mixture on your precooked base, and cook for 45 to 55 min. if the top of the cake begins to pick up color, reduce the temperature a bit. 
  10. remove the cake from the oven and return the oven to 200 degrees C 
  11. Now, for the second layer, whip the sour cream with the sugar and almond extract until the mixture becomes smooth. 
  12. pour the mixture on the first layer, and put back in the oven, cook for 5 min, then turn off the oven, but leave the cake in for one hour, the oven door must be ajar. 



![cheesecake-chocolate-white-almond-2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/cheese-cake-chocolat-balnc-amande-21.jpg)

remove the pan from the oven, pass a knife all around to separate the cake from the pan. put everything in the fridge until the cheesecake is cool, and after removing the lathe of the mold, and serve you. 

a true happiness, with the irresistible taste of the almond. 

thank you for your comments and thank you for wanting to subscribe to my newsletter if you like to have all my new recipes, preview. 

bisous 
