---
title: mchewek with almonds and peanuts
date: '2015-07-23'
categories:
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian patisserie
- Aid cake
- Algeria
- Delicacies
- delicacies
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/mchewek-amandes-et-cacahuetes-2.jpg
---
[ ![mchewek almonds and peanuts 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/mchewek-amandes-et-cacahuetes-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/mchewek-amandes-et-cacahuetes-2.jpg>)

##  mchewek with almonds and peanuts 

Hello everybody, 

This time, I start with a Samia Z recipe, a recipe for almond and peanut mchewek, a recipe she saw on Samira TV, pistachio mchewek, but then she did not have pistachios at home, she preferred to make this pretty model with a mixture of almonds and peanuts    


**mchewek with almonds and peanuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/mchewek-amandes-et-cacahuetes-3.jpg)

portions:  30  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients**

  * 2 measures of almond powder 
  * 1 measure crushed peanuts. 
  * eggs as needed 
  * vanilla 
  * 1 measure of sugar 
  * lemon zest 

dough: 
  * 3 glasses of flour 
  * ½ glass of butter 
  * 1 pinch of salt 
  * Orange tree Flower water. 

Decoration: 
  * crushed almonds 
  * honey syrup 
  * green dye. 
  * maizena to work the dough 



**Realization steps**

  1. in a large salad bowl, put the flour in the salt, then add the melted butter and lukewarm. 
  2. sand between your fingers to incorporate fat. 
  3. then add the orange blossom water to pick up the dough. go slowly to not have a dough that sticks to the hands. 
  4. let the dough rest. and prepare the stuffing. 
  5. mix all the ingredients in a bowl, add the eggs one by one, mixing with each addition to have a paste that picks up, the amount of eggs depends on the size of your measurement. 
  6. on your work plan covered with cornstarch, make dumplings with the stuffing of almost 30 gr, then spread in pudding and form a ring. 
  7. now take a little of the dough and spread out on the maizena covered work surface, you can use the dough machine 
  8. Cut slices of the same diameter as the rings. 
  9. take the stuffing rings, cover the goods with egg whites, and submerge them in a bowl containing crushed peanuts or almonds, according to your taste. 
  10. place the mchewek rings on the dough ring, also brushed with egg white so that it sticks. 
  11. for the decoration, take a little of the dough, and spread even more finely than the base of the cakes. 
  12. take a little dye in a little orange blossom water. dip the mold you will use in the dye, tap a little to remove the excess and cut your pucks that you will bend in bloom.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/faux-mchewek-aux-pistaches-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/faux-mchewek-aux-pistaches-4.jpg>)
  13. place the flowers in the ring. (for more explanation watch the video) 
  14. place the cakes as you go in a baking tray sprinkled with flour, and preheated to 150 degrees C. 
  15. out of the oven, decorate your cakes with just a little honey, for more taste, but it is not mandatory. 



Note a piece of advice from Samia Z:   
compared to the cooking, I left the oven well heated after when I put the cakes in the oven, I lowered the temperature until thermostat 3, so the cakes cook quickly as the dough is Fine, I tried once to cook at 150 ° C as on video, but unfortunately the cakes were too hard, 

[ ![mchewek almonds and peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/mchewek-amandes-et-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/mchewek-amandes-et-cacahuetes.jpg>)
