---
title: festive meal / salmon grilled with honey and sesames
date: '2012-12-23'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

---
Hello everybody, 

Here is a quick and easy recipe for the "loveurs" of salty sweet dishes, and especially of this delicious fish rich in unsaturated fatty acid (omega 3), and other salmon. melting inside and crunchy with this beautiful layer of grilled sesame that covers it, present with a thin layer of fried zucchini, this recipe is a "must" to accompany even a simple salad. 

ingredients: 

2 salmon pavers 

\- Toasted sesame 

\- 3 tablespoons of honey 

\- the juice of half a lime 

\- 1 zucchini - Olive oil - salt and black pepper 

method of preparation: 

  1. Wash and clean the zucchini, cut into slices, season with a little salt and black pepper. 
  2. fry them in a bottom of olive oil, and place on a paper towel to reduce the fat of cooking 
  3. dirty and pepper the salmon pavé. 
  4. mix the honey with the lemon, and grill the sesame seeds if they are not already grilled. 
  5. dip the salmon pieces in the honey and lemon mixture, then in the sesame seeds to cover them well. 
  6. in a frying pan, put just the equivalent of 3 to 4 tablespoons of olive oil, and cook the salmon over medium heat on both sides. 
  7. introduce, your salmon by garnishing them with slices of fried zucchini. 



and for others [ salmon recipes ](<https://www.amourdecuisine.fr/article-que-faire-avec-du-saumon-100745656.html>) , I share with you this article, which will surely interest you, 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
