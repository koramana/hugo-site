---
title: Ines birthday cake recipe
date: '2010-10-19'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine

---
Ines birthday cake with almond paste Bavarian speculoos / almonds / mascarpone / strawberry & nbsp; the video of the recipe is here at last I have time to post the recipe for the birthday cake of my Ines, she was very happy yesterday in front of his cake, with his brother, his sister, me and his father. but it tired me so much that I could not write the recipe yesterday, so I start it, despite that it will take me crazy time birthday cake Ines at the almond paste / bavarois speculoos / mascarpone / strawberry so here is the cake cut of Ines & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.85  (  1  ratings)  0 

[ Ines birthday cake with almond paste Bavarian speculoos / almonds / mascarpone / strawberry ](<https://www.amourdecuisine.fr/article-la-recette-du-gateau-d-anniversaire-d-ines-59220048.html>)

Finally, I have the time to post the recipe for the birthday cake of my Ines, she was very happy yesterday before his cake, with his brother, his sister, me and his father. but it tired me so much that I could not write the recipe yesterday, so I start it, despite that it will take me a crazy time 

[ Ines birthday cake with almond paste / bavarois speculoos / mascarpone / strawberry ](<https://www.amourdecuisine.fr/article-la-recette-du-gateau-d-anniversaire-d-ines-59220048.html>)

So here is the cup of Ines cake and I pass you the recipe: 

my cake had: 

a speculoos / toasted almonds base 

a mousse pastry cream / mascarpone 

a strawberry mousse 

for the base of speculoos it is very simple, according to the size of the pastry circle, a box and a half of speculoos (or even any other biscuit) 

100 gr of grilled skinned almond, melted butter to have a paste that can be crammed without problem 

so grind the speculoos, then grind the roasted almonds, mix them, and add the melted butter very slowly until you have a paste that can stand 

Mascarpone custard mousse: 

  * 500 ml of milk 
  * 57 g of sugar 
  * 1 packet of vanilla sugar 
  * 2 egg yolks 
  * 30 g cornflour 
  * 250 gr of mascarpone 
  * 300 ml of fresh cream 
  * 8 grams of gelatin 



place the gelatin in a little water and let it swell. 

Mix the egg yolks, cornflour, flour and sugars in a saucepan and whisk well.    
Pour the hot milk over, mix at the same time, and put back on medium heat while stirring constantly with a wooden spoon until the mixture thickens (about 4-5 minutes). 

off the fire incorporate a small amount of mascarpone and whisk well 

let cool and add the remaining mascarpone while whisking 

put the cream in whipped cream with a tablespoon of sugar. 

squeeze the gelatin and melt it on a bain-marie, and add it to the pastry cream mascarpone mixture 

then add the whipped cream in small quantity, without breaking it, to have a well ventilated foam 

pour this mousse on the basis of speculoos. and put it in the fridge for it to take. 

strawberry mousse: 

300 grs fresh strawberries (or in box) 

100 grams of sugar 

300 ml of fresh cream 

8 grams of gelatin 

red dye. 

cut the strawberries into pieces, add the sugar, cook a little over low heat, if you find that you have a lot of juice, drain a little, my daughter liked the strawberry juice hihihiih 

Now put the cream in whipped cream, with a tablespoon of sugar and a little red dye, to have a beautiful pink foam. 

have the gelatin swell in a little water, wring it out and incorporate it into the strawberry compote, whisk it well, let it cool, and add the whipped cream. 

Pour this mousse over the mascarpone mousse and put back in the fridge so that everything goes well. 

now we go to the preparation of the decoration, 

we start with the flowers, I used sugar paste that I colored in shrimp a little dark and the other a little clearer 

spread the sugar dough over a surface sprinkled with sugar, until you get a thin layer. 

cut circles, according to your taste for the size of the flower, I made circles of 4 cm and others of 3 cm. 

shape a cone of almost 2 cm and a half high, and start sticking the pastry circles one by one, use a little water at the base of each circle, so that it sticks, insert the circles, to have a beautiful flower, let the flower dry a little, on a paper that you have placed in a glass to not spoil the shape of your flower, after a few minutes, return to your flower, and deform a little petals, to give the pace of a real flower. 

now prepare the butterflies, and for that you will need white chocolate and dye according to your taste. 

<   
span> draw the butterflies on a white sheet, place your melted and colored chocolate in a small sachet, make a small hole at the corner of the bag and fill the butterfly, after drying the wings of the butterfly, take the melted dark chocolate and fill the body butterfly, place your leaf in a place that allows you to have the V shape so that the butterfly is not flat. 

let dry. it will stand alone from the sheet. 

so I wanted to make intercalated form between pink and mauve. to have the shape on the photo, to make the right shapes, use a sheet of paper 

now, remove the pastry ring very slowly and draw rectangles with the height of the cake, and the width of each petal, and stick each rectangle with on the side of the cake. 

do not do my stupidity, or I start by placing the almond paste in the pastry circle, as you see in the picture, and I pour my Bavarian mousses the next day trying to unmold the cake, the almond paste melted because of the moss, and I almost screwed up the cake 

here is the picture of my stupidity, so better use the decoration just an hour before presenting the cake 

thank you for your comments yesterday it made me happy, and Ines to love the cards and the photos she had. 

bonne soirée, et j’espère n’avoir oublier aucun détail 
