---
title: spongy yoghurt cake
date: '2017-11-24'
categories:
- birthday cake, party, celebrations
- cakes and cakes
- basic pastry recipes
- sweet recipes
tags:
- To taste
- desserts
- Pastry
- Algerian cakes
- Based
- Fast Food
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/gateau-au-yaourt-1.jpg
---
![cake with yoghurt 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/gateau-au-yaourt-1.jpg)

##  **spongy yoghurt cake**

Hello everybody, 

One again [ basic cake ](<https://www.amourdecuisine.fr/categorie-12359350.html>) , which you can use in the preparation of [ birthday cakes ](<https://www.amourdecuisine.fr/categorie-11700499.html>) , the cake **yogurt** , a cake very delicious, very soft and very melting. This recipe is a success all the time, originally I put 6 pots of flour, but I found that the cake was a little heavy, so sometimes according to my flour, I use 5 pots or so 5 and a half pots, and the result is just perfect. 

I often use this cake, you can find it in: 

[ the Ben 10 birthday cake ](<https://www.amourdecuisine.fr/article-ben-10-le-gateau-d-anniversaire-de-rayan-54691985.html>)

[ the barbie birthday cake ](<https://www.amourdecuisine.fr/article-princesse-barbie-gateau-d-anniversaire-86764345.html>)

[ the spongebob cake ](<https://www.amourdecuisine.fr/article-gateau-bob-l-eponge.html> "cake sponge bob")

This cake with yoghurt and so easy to do, and in addition gives a great result, you will love the recipe that is done in a jiffy, and most importantly, the cake is super good as a base for birthday cakes for children. 

here is the cup of the cake, and I can assure you that it was magnificent, delicious, melting, 

**spongy yoghurt cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/gateau-au-yaourt-2.jpg)

portions:  12  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients**

  * 2 jars of natural yoghurt 
  * 6 eggs 
  * 3 jars of sugar (when I say jar, I use the jar of yogurt as a measure) 
  * 1 C. a vanilla coffee 
  * 5 and a half pots of flour 
  * 2 tbsp. tablespoon of baking powder 
  * 1 pot of oil 



**Realization steps**

  1. Mix eggs, sugar, yoghurt, 
  2. add the oil and vanilla. 
  3. Mix the flour and baking powder, add to the mixture, 
  4. Butter the mold, pour the dough and cook for 45 minutes to 1 hour at 180 ° depending on the oven. 
  5. Check the cooking with a needle or knife. 
  6. Let cool on rack 



![Yogurt cake](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/gateau-au-yaourt.jpg)

thank you for your comments, and thank you to subscribe to the newsletter to be up to date with each new publication. 

et ça c’est le gateau au yaourt présenté comme gateau d’anniversaire, avec une belle décoration pour enfant. 
