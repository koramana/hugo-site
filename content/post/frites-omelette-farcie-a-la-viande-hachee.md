---
title: omelette fries stuffed with minced meat
date: '2018-04-29'
categories:
- crepes, waffles, fritters
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
- Dishes and salty recipes
- salty recipes
tags:
- Easy cooking
- Fast Food
- Algeria
- Ramadan 2018
- Ramadan
- eggs
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/04/frites-omelette-farcie-%C3%A0-la-viande-hach%C3%A9e.jpg
---
![omelette fries stuffed with minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2018/04/frites-omelette-farcie-%C3%A0-la-viande-hach%C3%A9e.jpg)

#  omelette fries stuffed with minced meat 

Hello everybody, 

Today I share with you the dish that I make when I'm short of ideas, omelette fries stuffed with minced meat, a recipe of all simplicity, without making headaches, plus guaranteed that everyone will love at home. 

This recipe of omelette fries stuffed with minced meat is a complete dish, just a small salad to accompany it and voila, a royal dinner for your children, hihihihih. Ah, even the grown-ups, because my husband loves, moreover he often asks for it. 

**omelette fries stuffed with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/04/frites-omelette-farcie-%C3%A0-la-viande-hach%C3%A9e-2.jpg)

**Ingredients**

  * 4 medium potatoes cut into cubes 
  * between 6 and 8 eggs (according to your taste) 
  * oil for frying 

for the stuffing: 
  * 100 gr of minced meat or less 
  * cumin and salt 
  * 1 clove of garlic 
  * cheese portion (2 to 3) 
  * 1 handful of cheese 
  * 2 tbsp. extra virgin olive oil 



**Realization steps** Start by preparing the stuffing: 

  1. fry the minced meat in a hot pan with a little oil to give it a nice color. 
  2. add garlic, salt and caraway, and cook well. 
  3. fry the potato cubes in a hot oil and drain on paper towels. 
  4. salt at the end of cooking. 
  5. break the eggs in a large salad bowl, and whip them a little. 
  6. add the cheese, a little salt and black pepper. 
  7. then we add the cube of fried potatoes. 
  8. put a bottom of oil in a stove with a bottom thick of almost 22cm, let warm then lower the fire. 
  9. pour in half of the egg mixture and fries. 
  10. garnish with the minced meat, then cover with the second layer of egg and fried. 
  11. let cook well over a low heat, when the first surface is cooked, turn it to cook on the big side. 



![omelette fries stuffed with minced meat 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/04/frites-omelette-farcie-%C3%A0-la-viande-hach%C3%A9e-1.jpg)
