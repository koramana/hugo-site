---
title: strawberry rice pudding
date: '2016-09-22'
categories:
- dessert, crumbles and bars
- sweet recipes
- riz
tags:
- To taste
- The start of the school year
- Algeria
- desserts
- Ramadan
- Easy cooking
- Rice cream

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/riz-au-lait-aux-fraises.jpg
---
![Rice milk-aux strawberries](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/riz-au-lait-aux-fraises.jpg)

##  strawberry rice pudding 

Hello everybody, 

My children often ask me that I prepare rice pudding to taste it, they like a lot, and not to fall into the routine, I try every time to make a small variation, a touch, something more but without touching the rice cream they love, well perfumed with vanilla. 

I had this little cashew picking strawberries that had been made over the weekend, it's wonderful to pique these delicacies by itself. It's a great adventure for my children, they were looking for the ripest fruits and they were competing who would fill his little basket first. 

For me it was super beautiful to walk in nature between these pretty strawberries, to look for the most beautiful strawberries and show my children how to pick strawberries without destroying the whole plant. 

Most importantly, we ate the strawberry in all these states, I realized a [ strawberry jam ](<https://www.amourdecuisine.fr/article-confiture-de-fraises-allegee.html>) just sublime, a [ strawberry sauce ](<https://www.amourdecuisine.fr/article-32373332.html>) , with which I decorated these verrines of rice pudding and still full of other delights. 

![milk-rice-with-strawberry-2-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/riz-au-lait-aux-fraises-2-2.jpg)

**strawberry rice pudding**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/riz-au-lait-aux-fraises-2.jpg)

portions:  6  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients** for the cream of rice: 

  * 150 g of rice 
  * 800 ml of milk 
  * 70 g caster sugar 
  * 50 of liquid cream 
  * 1 vanilla pod 

decoration: 
  * nuts in pieces. 
  * for decoration: 
  * [ Strawberry sauce ](<https://www.amourdecuisine.fr/article-sauce-de-fraises-coulis-de-fraises-facile-et-rapide.html>)
  * strawberries 



**Realization steps**

  1. preparation of the cream of rice: 
  2. Pour the milk, cream, sugar and vanilla bean melted in a saucepan. Wear to shudder. 
  3. remove the pod, rub the seeds and leave on medium heat 
  4. when the milk is hot, pour the rice and mix. 
  5. Cook for 30 minutes, stirring regularly. 
  6. when the rice is well cooked, present in verrines 
  7. decorate with strawberry sauce and strawberry pieces to taste 



![milk-rice-with-strawberry-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/riz-au-lait-aux-fraises-1.jpg)
