---
title: easy and quick chocolate fudge
date: '2013-08-24'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/fondant2_thumb1.jpg
---
![fondant2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/fondant2_thumb1.jpg)

Hello everybody, 

Here is a chocolate cake recipe that will only be blessed by this intolerable cold. 

I enjoyed it with my children, and I admit you, they did not wait for it cool, no question ... .. 

so, without much delay I pass you, the delicious recipe 

![flowing1 006-1](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/coulant1-006-1_thumb.jpg)

**easy and quick chocolate fudge**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/coulant1-004-1_thumb.jpg)

Recipe type:  dessert  portions:  8  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * For 8 people 
  * 4 eggs 
  * 200 g of 60% chocolate 
  * 2 tbsp. soup of milk 
  * 100 ml of liquid cream 
  * 1 tablespoon of flour 
  * 50g of almond 
  * 125g of sugar 



**Realization steps**

  1. Preheat the oven to 180 ° 
  2. Melt chocolate with cream and milk 
  3. Stir in the yolks one by one, then the flour and ground almonds 
  4. Beat the whites in firm snow with the caster sugar 
  5. Incorporate them gently to the previous preparation 
  6. Pour into a 24 cm diameter mold 
  7. Cook for 30-35 minutes (or depending on the oven) 
  8. At the end of the oven unmold on a rack and let cool 
  9. Serve warm! 



![flowing1 011-1](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/coulant1-011-1_thumb.jpg)

Un délice que je vous conseille vivement, 
