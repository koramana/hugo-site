---
title: Baskets of chicken burritos
date: '2015-12-04'
categories:
- appetizer, tapas, appetizer
- crepes, waffles, fritters
- diverse cuisine
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
tags:
- tortillas
- Sandwich
- Wrap
- inputs
- Amuse bouche
- Cheese
- tomatoes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Panier-de-burritos-1.CR2_.jpg
---
[ ![1.CR2 burritos baskets](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Panier-de-burritos-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Panier-de-burritos-1.CR2_.jpg>)

##  Baskets of chicken burritos 

Hello everybody, 

My children and my husband love everything that is based on [ tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html>) In fact, sometimes I do a dozen every two weeks, so I do not have to do them every week. 

When the idea of ​​this recipe, I saw on TV on the program: My kitchens rules Australia ... The candidates had prepared recipes that should appeal to children ... one of the couples had made these baskets of burritos at chicken. My children were watching with the program and they asked me to do it ... 

I immediately noticed the recipe while the candidates explained it, I do not know if I noticed everything, but in any case the result was a real delight, my children liked it a lot, they ate 3 each ... 

**Baskets of chicken burritos**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/panier-de-Burritos-2.jpg)

portions:  12  Prep time:  25 mins  cooking:  30 mins  total:  55 mins 

**Ingredients**

  * 4 [ flour tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html>)
  * grated cheese 
  * some lettuce leaves 
  * some chive stalks 

for the sauce: 
  * 2 tablespoons mayonnaise 
  * 1 tablespoon of fresh liquid cream 
  * 1 fillet of lemon 
  * 1 pinch of salt 

for the chicken: 
  * 1 chicken breast, cut into 2 cm cubes 
  * 1 teaspoon powdered cumin 
  * ½ teaspoon coriander powder 
  * ½ teaspoon sweet paprika 
  * ½ teaspoon dried oregano 
  * salt and pepper to taste 
  * 1½ tablespoons of olive oil 
  * ½ finely chopped red onion 
  * a few slices of red pepper, cut into 1 cm pieces 
  * a few slices of green pepper, cut into 1 cm pieces 
  * a few slices of yellow pepper, cut into 1 cm pieces 
  * 1 crushed garlic clove 
  * 2 medium tomatoes, finely chopped 
  * 1 nice handful of corn, drained 



**Realization steps**

  1. Preheat oven to 180 ° C. Spray a muffin pan with 12 cavities of olive oil. 
  2. cut each tortilla, 3 slices of almost 9 cm diameter, 
  3. Insert the tortilla rings into the cavities, and oil the inside again 
  4. Cook for about 7 minutes or until lightly browned, watch as they may swell a little, just press again with a spoon, to give them the shape of the cavities. 

For the chicken, 
  1. mix chicken, spices and oregano in a bowl. 
  2. Heat 2 teaspoons of the oil in a large saucepan. Cook the chicken until it is golden brown. Put aside. 
  3. put some more oil in the same pan. Fry the onion until it is tender. 
  4. Add the cubes of peppers and garlic. Cook, stirring occasionally, for about 5 minutes. 
  5. Add tomatoes and corn kernels. Simmer covered, for 15 minutes. Stir in the chicken, simmer a little until the sauce is completely reduced. 
  6. For assembly, take the tortilla baskets, place some grated cheese at the base. 
  7. Garnish each basket with salad. 
  8. add the chicken mixture, a little sauce, enocre cheese and finally the chives. Serve with [ guacamole ](<https://www.amourdecuisine.fr/article-recette-du-guacamole.html>) . 



[ ![baskets of burritos 2.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/paniers-de-burritos-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/paniers-de-burritos-2.CR2_.jpg>)
