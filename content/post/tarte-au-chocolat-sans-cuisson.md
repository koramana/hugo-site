---
title: chocolate tart without cooking
date: '2015-03-16'
categories:
- sweet recipes
- tartes et tartelettes
tags:
- Vegan Kitchen
- oat
- Cashew nut
- dates
- desserts
- Pastry
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tarte-au-chocolat-sans-cuisson-1.jpg
---
[ ![chocolate tart without cooking](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tarte-au-chocolat-sans-cuisson-1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-au-chocolat-sans-cuisson.html/sony-dsc-277>)

##  chocolate tart without cooking 

Hello everybody, 

Here is a recipe for chocolate pie without cooking, which will please you. It is a vegan recipe rich in vitamins and fiber. 

This recipe is a sharing of lunetoiles, and here is what she tells us about this chocolate pie without cooking: 

I love more and more vegan recipes and raw diet, because not only the base is food raw and alive, It is a different kitchen from the normal kitchen where everything is cooked and we lose the vitamins . Also, it's super simple, fast and very good This chocolate pie without cooking is too simple to realize, just have the ingredients on hand natural raw cashews, fresh dates, coconut oil, maple syrup, oat flakes. 'It's a fat-free pie, such as butter and sugar, so it's healthy ... 

![chocolate tart without cooking 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tarte-au-chocolat-sans-cuisson-3.jpg)

**chocolate tart without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tarte-au-chocolat-sans-cuisson-2.jpg)

portions:  8  Prep time:  20 mins  total:  20 mins 

**Ingredients** For the base: 

  * ½ cup natural raw cashews (or almonds, walnuts, etc.) 
  * 1 + ½ cup oatmeal 
  * ½ cups of dates 
  * 2 tablespoons melted coconut oil 
  * 2 tablespoons unsweetened cocoa powder 
  * 2 to 3 tablespoons maple syrup 
  * 1 to 2 scoops of water 

For garnish: 
  * 1 + ½ cup natural raw cashews 
  * 3 tablespoons of cocoa powder 
  * 1 cup of water 
  * ⅓ cup of maple syrup 
  * ¼ cup coconut oil, melted 
  * 1 teaspoon of vanilla bean paste 
  * 2 teaspoons instant espresso (optional) 
  * 1 pinch of salt 

Decoration: 
  * Unsweetened cocoa powder / chopped cashew nuts 



**Realization steps**

  1. Start at the base, mixing the nuts, oatmeal, cocoa powder, maple syrup, coconut oil and dates in a food processor. 
  2. Keep on the robot in pulsation mode until everything starts to break down and become a compact paste. 
  3. If necessary, add 1 to 2 tablespoons water. When it forms a paste, remove it from the robot. 
  4. Transfer the resulting dough into a rectangle mold, which has been lightly greased with coconut oil. 
  5. Press with your fingers, to spread evenly, going up on the edge. 
  6. Once the base is evenly squeezed into the pan, place it in the refrigerator. 
  7. For the filling: simply mix all the ingredients in a blender at high speed. 
  8. The consistency should be like thick yogurt, it's normal, it will firm once put in the refrigerator. 
  9. Remove the base of the refrigerator and pour the filling on the dough. 
  10. Sprinkle some chopped nuts to decorate. 
  11. Place in the fridge for several hours (1 full day for me) for it to firm up (or in the freezer for a shorter time) - the more it is left in the fridge, the more the flavors develop so I found that it was even more delicious the next day. 
  12. Remove from the fridge, and sprinkle with cocoa powder and slice and enjoy! 
  13. Keep it stored in the refrigerator for a few days. 



[ ![chocolate tart without cooking 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tarte-au-chocolat-sans-cuisson.jpg) ](<https://www.amourdecuisine.fr/article-tarte-au-chocolat-sans-cuisson.html/sony-dsc-280>)
