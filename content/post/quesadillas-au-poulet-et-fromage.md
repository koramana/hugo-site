---
title: chicken and cheese quesadillas
date: '2017-09-29'
categories:
- diverse cuisine
- Cuisine by country
- pizzas / quiches / pies and sandwiches
tags:
- Mexico
- tortillas
- inputs
- accompaniment
- Buffet
- Amuse bouche
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/09/quesadillas-au-poulet-et-fromage-1.jpg
---
![chicken and cheese quesadillas 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/quesadillas-au-poulet-et-fromage-1.jpg)

##  chicken and cheese quesadillas 

Hello everybody, 

Today we're going for a simple and easy recipe for chicken and cheese quesadillas. This recipe is one of the favorites of my husband, he may have asked me at least once a week for lunch, especially that with us, children do not come back from school for lunch. So my husband takes this opportunity, for me to prepare these quesadillas with chicken and cheese as piss that he can tolerate! 

Personally I go molo molo with the spice, but my husband loves a lot, hihihih he still has some Spanish roots! 

This time, head to Mexico to make this delicious recipe for chicken and cheese quesadillas. Certainly, we put some cheese in this recipe (full even) but I like personal version even more lightening, you can see that towards the end of the video: 

**chicken and cheese quesadillas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/quesadillas-au-poulet-et-fromage.jpg)

**Ingredients** For quessadillas (for 2 people) 

  * 3 to 4 [ tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html>)
  * 1 chicken breast cut into strips 
  * the spice for fajitas 
  * salt 
  * ½ large onion cut into cubes 
  * red, green and yellow pepper (1 half each) 
  * 1 garlic clove, grated. 
  * grated red cheese 
  * grated cheddar 
  * olive oil for cooking ingredients 
  * and butter for cooking quesadillas 

For the fajita spice: 
  * 1 C. cornstarch (optional) 
  * 2 tbsp. chili powder 
  * 1 C. salt 
  * 1 C. paprika 
  * 1 C. sugar 
  * ½ c. onion powder (optional) 
  * ½ c. garlic powder 
  * ¼ c. cayenne pepper 
  * ½ c. ground cumin 



**Realization steps**

  1. Start by filling the sliced ​​chicken breast with fajita powder and salt. 
  2. Cut the onion, and cubed peppers, fry in one to 2 tablespoon of olive oil. 
  3. add chopped garlic, salt and fajita powder. 
  4. when these ingredients are well cooked, remove from heat. 
  5. fry chicken strips in a little oil until cooked through. 
  6. in a large pan over low heat, melt ½ tsp. butter. 
  7. place a tortilla on it. 
  8. sprinkle over both cheese to cover. 
  9. add a layer of the mixture, onion / peppers 
  10. add a layer of chicken. 
  11. decorate with a nice layer of cheese 
  12. cover with another tortilla, and turn with a plate as for an omelette. 
  13. place a little butter, and put the quesadilla on the uncooked tortilla. 
  14. let it brown then remove from the heat. 
  15. enjoy immediately. 


