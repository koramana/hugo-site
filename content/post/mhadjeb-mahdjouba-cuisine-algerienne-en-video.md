---
title: mhadjeb, mahdjouba, Algerian cuisine, video
date: '2012-07-15'
categories:
- cuisine algerienne
- gateaux algeriens- orientales- modernes- fete aid
- Pâtisseries algeriennes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mhadjeb-069_thumb.jpg
---
#  mhadjeb, Algerian cuisine. 

[ ![mhadjeb, Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mhadjeb-069_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mhadjeb-069.jpg>)

Hello everyone, 

here are the mhadjeb, a  [ Algerian traditional recipe ](<https://www.amourdecuisine.fr>) ,  Made from semolina, which is peppered until it gives a soft and elastic cheese, which can then be rolled into a very very thin layer, to stuff it with taste and cabbage. with a [ tchektchouka ](<https://www.amourdecuisine.fr/article-chakchouka-104828146.html>) , sometimes a stuffing with chopped meat, sometimes with cheese, or sometimes, without stuffing, to accompany a good tea.    


**mhadjeb, mahdjouba, Algerian cuisine, video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mhadjeb-069_thumb.jpg)

portions:  15  Prep time:  60 mins  cooking:  15 mins  total:  1 hour 15 mins 

**Ingredients** for the pasta: 

  * a little semolina 
  * salt 
  * some water 
  * let it rest, and prepare the stuffing. 
  * 2 onions 
  * 3 tomatoes 
  * 2 cloves garlic 
  * salt, pepper, coriander powder 
  * tuna 
  * cut the onions into strips and sauté in the oil that you drain from the tuna tin. 
  * add the garlic crushed, and then the tomatoes cut into small cubes, and add the condiments according to your taste, let everything cook well, and then let cool. 
  * when the stuffing is cold, add in the tuna. 
  * after that form balls with the paste obtained, and let rest, to open the dough easily. 
  * smear a work plan well with the oil, spread over the balls of pasta one by one and fill with stuffing, tcheek tchukuka. 



**Realization steps** prepare the paw: 

  1. the semolina, to which the salt is mixed, is made to wet with water until it is easy to handle, 
  2. knead very well without cutting the dough,   
so we try to knead without separating one piece from another we add the water in small quantity, and we knead, and we add and knead, until the dough becomes very soft, and when we stretch it it does not tear like a chewing gum.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/mhadjeb-067_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/mhadjeb-067_thumb.jpg>)
  3. fold the dough like an envelope on the stuffing, and cook on a thick and well oiled tadjine or a crepe. 



[ ![mhadjeb 065](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mhadjeb-065_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mhadjeb-065.jpg>)

bon appétit 
