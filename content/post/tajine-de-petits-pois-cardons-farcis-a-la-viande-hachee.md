---
title: tajine of peas and cardoons stuffed with minced meat
date: '2014-02-20'
categories:
- Cupcakes, macaroons, and other pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cardon-farci.jpg
---
#  tagine of cardoon / peas. 

![cardoon-farci.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/cardon-farci.jpg)

Hello everybody, 

I have never made recipes to Cardon on my blog, just because in Bristol, I never find, so that I fall on it, I take the opportunity to make good recipes, or recipes that I have not eaten for some time now, like this recipe, that my mother used to do often. 

so I pass you the recipe: 

**tajine of peas and cardoons stuffed with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tajine-de-petits-pois-et-cardons-farcis.jpg)

Recipe type:  Hand  portions:  4  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * 1 kg of cardoons 
  * 300 g minced meat 
  * 1 onion and a half 
  * pieces of chicken or meat 
  * 2 tablespoons of rice 
  * 1 egg 
  * 3 tablespoons of oil 
  * 2 handful of peas 
  * 1 bunch of parsley 
  * black pepper 
  * cinnamon 
  * salt 



**Realization steps**

  1. Clean the cardoons, and cut them into pieces 6 to 7 cm long. 
  2. Remove the wires on each side and remove the smooth skin that covers the hollow side of the card. 
  3. Rinse and put and blanch in a saucepan of salt water. 
  4. In a large pot, if possible, fry the grated onion 
  5. add the chicken pieces, black pepper, cinnamon and salt. 
  6. Cover with water and add the peas and chopped parsley. 
  7. Mix the minced meat with ½ grated onion, chopped parsley, egg yolk, a pinch of black pepper and salt. 
  8. Take a little stuffing and roll the cigar-shaped, surround it with two pieces of cardoon forming a small package. 
  9. Tie this bundle with a strong thread, or prick with two toothpicks 
  10. Continue the operation until the pieces of cardoon are exhausted. 
  11. Immerse them in the sauce. Cover and cook for 5 to 10 minutes 
  12. When the cards are soft, reduce the sauce and bind with an egg yolk diluted in lemon juice. 
  13. Let reduce the sauce. 



![tajine de cardon.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tajine-de-cardon.jpg)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
