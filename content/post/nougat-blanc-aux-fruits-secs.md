---
title: White nougat with dried fruits
date: '2015-02-03'
categories:
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries
- ramadan recipe
tags:
- Almond
- desserts
- Confectionery
- eggs
- Egg white
- delicacies
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/nougat-blanc-aux-amandes.CR2_.jpg
---
[ ![white nougat with almonds.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/nougat-blanc-aux-amandes.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-nougat-blanc-aux-fruits-secs.html/nougat-blanc-aux-amandes-cr2-2>)

##  White nougat with dried fruits 

Hello everybody, 

A homemade Nougat recipe tempt you? Well, here is one, and it's the best homemade nougat recipe I've ever tried, because I must admit that I had several failures well before this recipe, sometimes a very hard nougat, sometimes very sticky, and the failures pushed me every time to try the same recipe, three or four times, or just go for a new one. 

In any case, try this nougat recipe, respect especially the cooking temperature of the sugar, because that is what will give you the perfect result. 

This nougat is just crunchy, crunchy with small pieces of almonds and pistachios well grilled, or the fondant taste of cramberries a little tart. In any case we must pay attention to the caloric intake of this sweets, moreover when my husband saw me make the recipe, he told me: there is so much sugar in the nougat? I told him: so what did you think, he told me, I'm never going to eat any more, hihihi, he has interest anyway. In any case, the habit in Algeria is to consume this delight during the holy month of Ramadan, because after a day of fasting, a small square of nougat, will not hurt too much. 

[ ![white nougat with dried fruits](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/nougat-balnc-aux-fruits-secs-1a.jpg) ](<https://www.amourdecuisine.fr/article-nougat-blanc-aux-fruits-secs.html/nougat-balnc-aux-fruits-secs-1a>)

**White nougat with dried fruits**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/nougat-blanc-aux-fruits-secs-2.jpg)

portions:  30  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * edible rice leaves 
  * 2 glasses and ¼ caster sugar (1 glass of 240 ml) (250 gr) 
  * ½ glass of liquid glucose: 80 gr 
  * ⅓ glass of honey: 50 gr 
  * 2 egg white 
  * 1 glass of roasted almonds 
  * 1 glass of roasted pistachios 
  * 1 a glass of dried cranberries 
  * 2 tablespoons of rose water 



**Realization steps**

  1. oil and line a 30cm x 20cm mold with rice leaves.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/feuille-dazyme.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/feuille-dazyme.jpg>)
  2. Place the sugar, glucose, honey and 2 tablespoons of water in a heavy-bottomed saucepan.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/ingredient-de-nougat-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/ingredient-de-nougat-1.jpg>)
  3. Stir over low heat for about 10 minutes or until glucose and sugar dissolve and the mixture comes to a boil. 
  4. Simmer without stirring for 10 minutes or until the mixture reaches 165 ° C on a sugar thermometer.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/temperature.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/temperature.jpg>)
  5. using an electric mixer, whip the egg whites with snow. 
  6. add the syrup in a regular stream while continuing to whip.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/unnamed-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/unnamed-3.jpg>)
  7. Continue beating for 1 minute or until the mixture is well blended. Stir in almonds, pistachios, cranberries and rose water.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/raisins-secs-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/raisins-secs-001.jpg>)
  8. Pour into the prepared pan and spread evenly (it will be sticky and warm enough).   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/nougat-pret-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/nougat-pret-001.jpg>)
  9. Cover with the rest of the rice paper, and try to make the surface uniform by pressing,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/IMG_1392.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/IMG_1392.jpg>)
  10. Let cool and cut into squares or small rectangles. you can pack in candy paper and keep in an airtight box in a cool place.   
Do not refrigerate the nougat because it will become sticky. 



[ ![white nougat with dried fruits](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/nougat-blanc-aux-fruits-secs-3.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-nougat-blanc-aux-fruits-secs.html/nougat-blanc-aux-fruits-secs-3-cr2>)

_**Les participantes :** _
