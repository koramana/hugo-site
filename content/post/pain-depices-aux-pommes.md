---
title: Apple gingerbread
date: '2017-02-01'
categories:
- dessert, crumbles et barres
- gateaux, et cakes
- recettes sucrees
tags:
- Cake
- Four quarters
- Algerian cakes
- Soft
- Mellow cake
- To taste
- Valentine's Day

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/pain-depices-aux-pommes.CR2_.jpg
---
[ ![gingerbread with apples.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/pain-depices-aux-pommes.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/pain-depices-aux-pommes.CR2_.jpg>)

##  Apple gingerbread 

Hello everybody, 

I like gingerbread to an impossible point, especially that here in England, we find none, except if I go to London, where there are Algerian grocery stores, which sell it. Sometimes, I make my stock of Algeria, just to have a few times, two beautiful slices of gingerbread at breakfast, what I like it. 

For a while, I've been able to recipe this [ gingerbread ](<https://www.amourdecuisine.fr/article-pain-d-epice-tres-moelleux-et-trop-bon.html> "fluffy gingerbread") super mellow, that I share with each time my friends who are looking for a good recipe well successful gingerbread, do not hesitate also to realize this recipe, because it is a delight. 

more 

This time, I have another version, a gingerbread with apples, fluffy wish, very rich in taste, well melted in the mouth, and well perfumed, especially with the touch of caramel more ... 

Try this recipe, but I advise you to double the quantities, if you want to make this gingerbread in a cake mold. In this recipe, I made 6 apple gingerbread mini cakes, each cake was almost 12 cm, I had bought this mold a while ago in Lidl, and it's great because the cakes do not stick in the background, more cake are so beautiful, in this form of heart ...   


**Apple gingerbread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/pain-depices-aux-pommes-1.CR2_.jpg)

portions:  6  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients**

  * 100 gr of melted butter 
  * 130 grams of brown sugar 
  * 1 egg 
  * 3 medium sized apples 
  * 130 gr of flour 
  * ¼ c. a coffee of salt 
  * ½ teaspoon of baking powder 
  * ½ cup of baking soda 
  * 1 cup cinnamon powder 
  * ½ teaspoon of spices for gingerbread 



**Realization steps**

  1. Preheat the oven to 180 degrees C. 
  2. butter the bottom of the mold. 
  3. In a large bowl, whisk together the melted butter, sugar and egg until fluffy. 
  4. Stir in the apples and cut into small cubes. 
  5. In another bowl, sift together flour, salt, baking powder, baking soda, cinnamon and spices. 
  6. Stir the flour mixture into the liquid mixture until everything is homogeneous. Fill the cake imprints, or the cake mold. 
  7. Bake 35 minutes in the preheated oven, or until a toothpick inserted in the center comes out clean. 



[ ![apple gingerbread.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/pain-depices-aux-pommes.CR2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/pain-depices-aux-pommes.CR2-001.jpg>)
