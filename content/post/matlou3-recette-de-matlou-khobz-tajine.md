---
title: Matlou3, recipe of mathew khobz tajine
date: '2016-11-11'
categories:
- bakery
- Algerian cuisine
- Mina el forn
- pain, pain traditionnel, galette

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/Matlou3-recette-de-matlou-khobz-tajine-1.jpg
---
![matlou3-Recipe of Matlou-khobz-tagine-1](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/Matlou3-recette-de-matlou-khobz-tajine-1.jpg)

##  Matlou3, recipe of mathew khobz tajine 

Hello everybody, 

Who wants a little Matlou3, recipe mathew tahine khobz? I lost the photo album of my khobz tajine, and I thank my friend Samy aya who gave me her photos to present my article Matlou3 recipe mathew khobz tajine. 

this matlou3 is the best Algerian homemade bread, matlou3 or matlouh or matlou khobz tajine. This is the best bread that can accompany a good [ Lamb stew ](<https://www.amourdecuisine.fr/article-ragout-dagneau-aux-pommes-de-terre.html>) or [ loubia ](<https://www.amourdecuisine.fr/article-haricots-blancs-en-sauce-rouge.html>) when it's cold with duck! 

I used my bread machine to make my bread, so I'm sure my bread will rise well even if it's really cold 

![matloue-013](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-013.jpg)

**Matlou3, recipe of mathew khobz tajine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/Matlou3-recette-de-matlou-khobz-tajine-2.jpg)

portions:  4  Prep time:  45 mins  cooking:  15 mins  total:  1 hour 

**Ingredients**

  * 450 gr of semolina (mix between fine and medium for me) 
  * 2 tablespoons baking yeast 
  * 1 cup of salt 
  * 1 case of sugar 
  * 1 case of milk 
  * 1 case of oil 
  * 250 to 300 ml of water   
(I add in small quantities and I monitor the state of the dough)   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/matloue-005_thumb.jpg)



**Realization steps**

  1. put the ingredients in your mouth as you usually do. 
  2. and start the pizza dough program. (45 min for me) 
  3. At the end of the program, take out your dough, make 2 balls, and let rest (I made two patties, one big and beautiful, and the other average, for my rayan). 
  4. cover his balls and let stand 15 to 20 minutes. 
  5. spread the balls in circles with the apple of your hands, and let it rest for another 30 minutes.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-003_thumb1.jpg)
  6. for what's left, I put my crepiere on thermostat 2 (mine her thermostat goes up to 7) 
  7. cook the cake on both sides. 
  8. me, I cook also the turn of the cake, because I like when it is well cooked. 



{{< youtube Y3njqdX4BBs >}} 

![matlou3-Recipe of Matlou-khobz-tagine](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/Matlou3-recette-de-matlou-khobz-tajine.jpg)

it was a delight, 

good appetite to you. 
