---
title: cheesecake mascarpone / chocolate
date: '2014-03-07'
categories:
- couscous
- Algerian cuisine
- vegetarian dishes
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cheesecake-mascarpone-chocolat-019_thumb.jpg
---
[ ![cheesecake mascarpone chocolate 019](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cheesecake-mascarpone-chocolat-019_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cheesecake-mascarpone-chocolat-019.jpg>)

Hello everybody, 

not at all diet this little delight, so do not blame me, it's really greedy, hihihiih for me the solution was to take him to my son's school to enjoy it with moms around a little tea, hihihiih 

must say that other moms have a good laugh when they saw me out my little camera, and bring with me a little decoration. 

So I pass you this recipe which is too good, I regret having only a small part, but I prefer it to have 2 kilos more where I do not want, hihihihi    


**cheesecake mascarpone / chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cheesecake-mascarpone-chocolat-019_thumb.jpg)

Recipe type:  Dessert  portions:  12  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 300 gr of biscuits (I had everything I could get on my hands: bourbons, coffee cookies, rich tea) 
  * 100 gr of melted butter 
  * 250 grs of mascarpone 
  * 250 grs of cheese such as pheladelphia or cheeses 
  * 200 ml of fresh cream 
  * 4 eggs 
  * 150 grams of sugar 
  * 3 tablespoons of cocoa 
  * juice of half a lemon 
  * zest of a lemon 



**Realization steps**

  1. reduce the powdered biscuits 
  2. Mix the biscuits with the melted butter and heap a high-sided mold with, put in the fridge until use 
  3. whip cheese, masacrpone and fresh cream with sugar 
  4. add the 3 eggs one by one whisking between each addition 
  5. divide this mixture into two 
  6. in the first mixture add lemon juice and zest, whip 
  7. add the remaining egg and cocoa to the other mixture and beat, if you like, add a tablespoon of sugar 
  8. pour the mixes according to your taste on the base 
  9. cook for almost 1 hour in an oven preheated to 160 degrees C. the middle of the cake must remain shaky. 
  10. at the end of cooking turn off the oven and leave the cake in it. 



Note just a tip:   
  
At the end of baking the cake, try to detach the cake from the edges of the mold because the cheesecake usually shrinks, and if the edges stick, the cheesecake will crack in the middle. 

[ ![cheesecake mascarpone chocolate 011](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cheesecake-mascarpone-chocolat-011_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cheesecake-mascarpone-chocolat-011.jpg>)

[ ![chocolate mascarpone cheesecake 014](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cheesecake-mascarpone-chocolat-014_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cheesecake-mascarpone-chocolat-014.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
