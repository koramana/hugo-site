---
title: my achievements at home 29
date: '2012-10-29'
categories:
- dessert, crumbles et barres
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tarte_aux_pommes_au_beurre3_31.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

I contunais the ball of your sublime super successful recipes that you achieve by inspiring you from my blog, and it makes me really happy. 

and I am still waiting with great joy, to receive your photos on my email: [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)

![tarte_aux_pommes_au_beurre3](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tarte_aux_pommes_au_beurre3_31.jpg)

[ apple pie with butter ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-au-beurre-67056341.html>) in [ Samar ](<http://mesinspirationsculinaires.over-blog.com>)

plateau of Algerian cakes for the aid el kebir 2012 at Linda 

[ chocolate fingers without cooking ](<https://www.amourdecuisine.fr/article-gateau-algerien-gateau-sans-cuisson-doigts-au-chocolat-107284273.html>)

[ jam sands ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-lait-72889483.html>)

[ baklawa constantinoise ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-aux-amandes-et-cacahuettes-61018034.html>)

the tray of saliha B cakes 

[ Shortbread with jam ](<https://www.amourdecuisine.fr/article-25345485.html>)

[ Makrout For Dates ](<https://www.amourdecuisine.fr/article-makrout-aux-dattes-et-miel-108089189.html>)

[ White and black chocolate lasagna ](<https://www.amourdecuisine.fr/article-rkhama-zellige-nougat-aux-cacahuetes-et-sesames-grillees-94973408.html>)

[ Katayfe ](<https://www.amourdecuisine.fr/article-32532261.html>)

[ Shortbread with jam ](<https://www.amourdecuisine.fr/article-25345485.html>) at Assia C 

[ Zellige, cake without cooking ](<https://www.amourdecuisine.fr/article-rkhama-zellige-nougat-aux-cacahuetes-et-sesames-grillees-94973408.html>) at K.M 

[ Swiss rolled ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture-a-minuit-45608781.html>) with jam at Mira M 

[ Swiss rolled ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture-a-minuit-45608781.html>) marbled chocolate at Mira's. M 

[ Zellige, cake without cooking ](<https://www.amourdecuisine.fr/article-rkhama-zellige-nougat-aux-cacahuetes-et-sesames-grillees-94973408.html>) at Denise's place 

[ Mghaber ](<https://www.amourdecuisine.fr/article-gateau-algerien-lmghaber-104611279.html>) at Radia. O 

[ Cheese stuffed chicken nuggets ](<https://www.amourdecuisine.fr/article-croquettes-de-poulets-farcies-de-fromage-46949639.html>) at Lila's. F 

[ garantita ](<https://www.amourdecuisine.fr/article-garantita-ou-karantika-94535875.html>) at malika's. K 

[ pizza ](<https://www.amourdecuisine.fr/article-pate-a-pizza-moelleuse-et-croustillante-111129186.html>) chez Malika .K 
