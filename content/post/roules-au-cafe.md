---
title: coffee rolls
date: '2010-04-23'
categories:
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/04/roul%C3%A9-au-caf%C3%A9.jpg
---
##  coffee rolls 

Hello everybody, 

My husband is very fond of rolled cakes. He always likes to have some to taste it. But I do not like it to be always a [ rolled with jam ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture.html> "rolled jam cookie") , so every time I try to make a new version, a new farce, like this [ rolled with mascarpone and strawberries ](<https://www.amourdecuisine.fr/article-roule-mascarpone-fraise.html> "Mascarpone / strawberry roll") . This time I left for a simple mocha cream, because I really like the taste of coffee in cakes. 

**coffee rolls**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/roul%C3%A9-au-caf%C3%A9.jpg)

portions:  8  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * Biscuit Roll: 
  * 3 eggs 
  * 85 gr of sugar 
  * 100 gr of flour 
  * 1 cup of baking powder 
  * 1 pinch of salt 
  * 2 cases of hot milk 
  * 2 drops of coffee extract (you can put coffee in the milk to taste coffee, or replace with vanilla or lemon zest) 

for the cream: 
  * 250 grs of cream 
  * coffee extract 
  * a spoon of instant coffee (which I added directly to the crème fraîche) 
  * icing sugar according to your taste 



**Realization steps**

  1. preheat the oven to 220 ° C. 
  2. In a bowl, whisk egg whites with 2 tablespoons sugar and pinch of salt. 
  3. beat the egg yolk with the sugar, Sift the flour with the yeast, add the flour and the milk to the roll at the same time. 
  4. now, add the egg white very slowly to the mixture without breaking it. 
  5. pour the mixture into a baking dish (40 * 30 cm), lined with parchment paper. Bake 8 to 10 minutes. 
  6. Unmould, roll immediately with parchment paper. Let cool on a rack. 



prepare the whipped cream, adding the cream to which you added the soluble coffee and the coffee extract, and slowly the sugar. 

**Unroll** the cake, remove the paper, spread the whipped cream. Roll again, slice and serve 

must say that my cream was not famous, hence the difficulty of rolling the cake. 

[ ![rolled in coffee 1](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/roul%C3%A9-au-caf%C3%A9-1.jpg) ](<https://www.amourdecuisine.fr/article-roules-au-cafe.html/roule-au-cafe-1>)
