---
title: Salmon steak with tomato
date: '2015-08-13'
categories:
- Algerian cuisine
- diverse cuisine
tags:
- inputs
- Fast Food
- Easy cooking
- Healthy cuisine
- Summer
- Algeria
- Fish

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/08/darnes-de-saumon-au-four-1.jpg
---
[ ![salmon steaks with tomato](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/darnes-de-saumon-au-four-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/darnes-de-saumon-au-four-1.jpg>)

##  Salmon steak with tomato 

Hello everybody, 

Salmon is a very fat fish (the good fat of course), that's why I always avoid its frying. Generally when I have salmon at home, I always marinade in a good marmalade, and then I grill it on a plancha. 

This time it's with a delicious recipe from **Wassila Hbr** We're going to taste these salmon steaks with tomatoes, a recipe she learned from her mother-in-law and that she shares generously with us. 

Do not forget my friends, if you like tomatoes, come join us in our little game: [ recipes around an ingredient # 9: the tomato ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-9-la-tomate.html>)

**Baked salmon steaks**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/darnes-de-saumon-au-four-2.jpg)

portions:  6  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 6 slices of fresh salmon 
  * 1 kilo of fresh and firm tomatoes 
  * Olive oil 
  * thyme, 
  * bay leaves, 
  * parsley 
  * a head of garlic, 
  * salt, black pepper, 
  * 1 cup of cumin powder 



**Realization steps**

  1. take the salmon traps, salt and pepper 
  2. brown them in a drizzle of olive oil one minute on each side, reserve. 
  3. In a pot, put fresh washed tomatoes cut in slices, in the same cooking oil of salmon 
  4. crush the whole garlic head after removing the germs in it for a better tasting 
  5. add to the crushed garlic, the cumin then mix with a teaspoon, 
  6. add salt, black pepper, thyme and chopped parsley 
  7. then put this dersa on the tomatoes 
  8. add over the already golden salmon steaks 
  9. cook over low heat without stirring 
  10. do not add water cooked quickly, cooking lasts a few minutes 
  11. enjoy with a good baker's bread or homemade 
  12. accompany with rice or fries. 



[ ![salmon steaks with tomato](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/darnes-de-saumon-au-four-150x113.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/darnes-de-saumon-au-four.jpg>)
