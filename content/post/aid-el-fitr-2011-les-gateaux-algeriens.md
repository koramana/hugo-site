---
title: Aid el Fitr 2011 Algerian cakes
date: '2011-08-24'
categories:
- basic pastry recipes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/aid-el-fitr-2011-gateaux-algeriens-037a1.jpg
---
& Nbsp; & Nbsp; Hello everyone, This is a big Muslim holiday that welcomes us this year, Aid el Fitr 2011, I address all Muslims, to wish them a good feast, the good God accepts our fast, and our prayers , that love and peace, reign on this world. all my wishes of happiness, of joy to everyone. If you want to share your plates of cakes of the Aid with us, you can send me your photos on my email: vosessais@amourdecuisine.fr and now I share with you photos of my table of help live from my house & hellip ; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

It is a great Muslim festival that welcomes us this year, Aid el Fitr 2011, I address all the Muslims, to wish them a good feast, the good god accepts our fast, and our prayers, that love and peace, reign on this world. 

all my wishes of happiness, of joy to everyone. 

If you want to share your plates of cakes of the Aid with us, you can send me your photos on my email: 

vosessais@amourdecuisine.fr 

and now I share pictures of my help table live from my house in Algeria 

a variety of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-12344741.html>)

different [ Algerian pastries ](<https://www.amourdecuisine.fr/categorie-12135732.html>)

The [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) with almonds and honey 

[ Mchekla in leaves ](<https://www.amourdecuisine.fr/article-mchekla-aux-feuilles-gateaux-modernes-algeriens-85426355.html>)

and 

[ ties tie the knot ](<https://www.amourdecuisine.fr/article-les-gateaux-algeriens-noeuds-papillons-84677207.html>)

[ El machkouk ](<https://www.amourdecuisine.fr/article-el-machkouk-mechakek-lmachkouk-82438591.html>)

[ Skendraniettes ](<https://www.amourdecuisine.fr/article-skandraniette-gateaux-algeriens-83562645.html>)

[ Ghribia bel jouz ](<https://www.amourdecuisine.fr/article-ghribia-bel-jouz-ghribiya-aux-noix-82339821.html>)

[ Peanut and nutella sands ](<https://www.amourdecuisine.fr/article-aid-el-fitr-2011-les-gateaux-algeriens-82873081.html>) (recipe to come) 

[ Mchekla sheet ](<https://www.amourdecuisine.fr/article-mchekla-aux-feuilles-gateaux-modernes-algeriens-85426355.html>)

[ Arayeche ](<https://www.amourdecuisine.fr/article-25345469.html>)

[ Pistachio Sands and Apricot Jam ](<https://www.amourdecuisine.fr/article-sables-aux-pistaches-et-confiture-d-abricots-83935371.html>)

[ El machkouk ](<https://www.amourdecuisine.fr/article-el-machkouk-mechakek-lmachkouk-82438591.html>)

[ kaak nakache ](<https://www.amourdecuisine.fr/article-kaak-nekkache-aux-amandes-82530128.html>)

[ Griweche ](<https://www.amourdecuisine.fr/article-griweche-griwech-gateau-algerien-frit-101738380.html>)

[ Baklawa constantinoise ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-aux-amandes-et-cacahuettes-61018034.html>)

[ Makrout el Koucha ](<https://www.amourdecuisine.fr/article-26001222.html>)

![aid-el-Fitr-2011-cake-Algerian-037a.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/aid-el-fitr-2011-gateaux-algeriens-037a1.jpg)

Encore une fois, joyeuses fetes, et esperant que chaque annee, on va feter l’aid en plein joie et bonheur 
