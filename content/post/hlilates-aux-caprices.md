---
title: Hilates to whims
date: '2015-07-16'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
- sweet recipes
tags:
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/hlilates-aux-caprices-1.jpg
---
[ ![hilates to whims 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/hlilates-aux-caprices-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/hlilates-aux-caprices-1.jpg>)

##  Hilates to whims 

Hello everybody, 

I'm trying to put all the recipes that are waiting for moderation on my blog online, and especially I try to put the recipes that are adequate with the day of the Aid el Fitr which is approaching day by day quickly, besides it will be the night of doubt today, and we will know if the feast of Aid el Fitr is going to be Friday, or so Saturday. 

I know several women who are always looking for easy cakes for the festival of Aid, and here is a recipe that will surely please many of you, especially the amateurs and lovers of the famous Algerian sweets » **the whims** A sweet that can not be found anywhere else, this sweet that has always been the one to comfort us, or just to please us. A recipe shared with us by a friend and reader: **Hassina Fjkh** whom I greet in passing, and thank you very much for sharing this. 

**Hilates to whims**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/hlilates-aux-caprices-2.jpg)

portions:  40  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients**

  * 60 pieces of caprice 
  * 4 tablespoons of milk 
  * 250 gr of butter 
  * 2 egg yolks 
  * ½ tablespoon of baking powder, or ½ bag 
  * vanilla 
  * 1 glass of cornflour 
  * flour to pick up the dough 
  * Decoration: 
  * Chocolate 
  * Nestle caramel 



**Realization steps**

  1. Start by melting whims in the milk over low heat 
  2. then add the butter, always over low heat, and mix until you have a homogeneous mixture. 
  3. remove from heat and allow to cool, the mixture will be a little thicker. 
  4. add the egg yolks and beat with the beater 
  5. then add yeast, vanilla and cornflour. 
  6. Stir in the flour slowly until you have a paste that will collect, do not put too much flour, otherwise your dough will crumble, and you will not be able to pick it up. 
  7. spread the dough as for shortbread on a lightly floured plan, or for easier work on food film. 
  8. cut cakes with a cookie cutter of your choice. 
  9. and cook in a preheated oven at 180 degrees, for 10 to 12 minutes, you must be careful 
  10. after cooling cakes, stick each two pieces together with Nestlé milk caramel 
  11. and cover your cakes with melted chocolate in a bain-marie. 
  12. Decorate according to your taste. 



[ ![hilates to whims 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/hlilates-aux-caprices-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/hlilates-aux-caprices-3.jpg>)
