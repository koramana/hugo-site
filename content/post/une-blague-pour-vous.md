---
title: a joke for you
date: '2009-04-16'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/08/le-jeu-d-agenda1.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/le-jeu-d-agenda1.jpg)

a little joke;   
**A Parisian couple decided to go on a weekend at the beach and stay at the same hotel as they did 20 years ago during their honeymoon.  
But at the last moment, because of a problem at work, the woman can not take her Thursday.   
It was therefore decided that the husband would take the plane on Thursday, and his wife the next day.   
The man arrives as planned and after renting the hotel room, he realizes that in the room there is a computer with internet connection. He then decides to send a mail to his wife, but he is mistaken in writing the address.   
Thus in Perpignan, a widow who has just returned from the funeral of her husband who died of a heart attack receives the email.   
The widow consults her mailbox to see if there are any messages from family or friends. Thus, when she reads the first of them, she faints.   
His son enters the room and finds his mother lying on the floor, unconscious, at the computer's foot.   
On the screen, you can read the following message:   
To my beloved wife,   
I arrived safely You will certainly be surprised to hear from me now and in this way. Here they have computers and you can send messages to those you like.   
I just arrived and checked that everything was ready for your arrival, tomorrow Friday.   
I can not wait to see you.   
I hope your trip will be as good as mine. **

P.S .: You do not have to bring a lot of clothes   
: Il fait une chaleur d’enfer ici ! 
