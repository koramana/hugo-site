---
title: fluffy cake with yoghurt and chocolate
date: '2016-03-15'
categories:
- Chocolate cake
- birthday cake, party, celebrations
- cakes and cakes
- recipes of feculents
- sweet recipes
tags:
- Anniversary
- Pastry
- la France
- Algerian cakes
- Easy cooking
- Cake
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-au-yaourt-au-chocolat-1.jpg
---
[ ![fluffy cake with yoghurt and chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-au-yaourt-au-chocolat-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-danniversaire-en-or-8.jpg>)

##  fluffy cake with yoghurt and chocolate 

Hello everybody, 

I often make the cake as a base for birthday cakes, and I just noticed that I never had to post the recipe on my blog. Each time I take the cake in photos before the cut, but once cut, no chance to take a picture, hihihihi. Just this time, we cut the cake, we tasted and savored our share, and I had in mind to take pictures in the morning at ease while the children would be at school, and here is my husband tell me, I can take the rest for my friends, we will watch a football game together, and it will be nice to take this together. 

So quickly, and under the strong neon light, and just with my mobile phone, I made quick shots of the cup, before my husband won, hihihih. 

This chocolate cake is ideal as a birthday cake base, or just to make it quickly and decorate with a nice chocolate sauce, if you have guests unexpectedly, then feel free to print the recipe, and the stick on your fridge, it will be useful at any time! 

As you can see from this photo, my chocolate cake was the base of a pretty birthday cake that I quickly decorated with a [ butter cream ](<https://www.amourdecuisine.fr/article-la-creme-au-beurre-facile-112774625.html>) to which I added a little mascarpone for a creamy taste and a sweet touch of salty. I added on the cake sugar flowers trade. when to the golden lace, I prepared and pasted at the last minute, because I did not know whether to paste them directly to the cream to melt the sugar. Leaves and small butterflies are based on [ homemade almond paste ](<https://www.amourdecuisine.fr/article-pate-damande-maison-massepain.html>) , which I decorated with a golden glitter powder. 

[ ![fluffy cake with yoghurt and chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-danniversaire-en-or-8.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-danniversaire-en-or-8.jpg>)   


**fluffy cake with chocolate yoghurt**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-yaourt-au-chocolat-super-moelleux.jpg)

portions:  8  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 1 jar of natural yoghurt 
  * 2 pots of flour 
  * 1 jar of Maïzena 
  * 1 and a half jars of sugar 
  * ½ pot of table oil 
  * 1 packet of dry yeast 
  * 1 jar of chocolate powder without sugar   
If you only have sweet chocolate powder just add a jar of sugar 
  * 3 eggs 



**Realization steps**

  1. Preheat the oven to 180 ° 
  2. Mix yogurt, oil and eggs 
  3. Add flour, Maizena, sugar, yeast, and chocolate powder 
  4. Pour into a mold 
  5. Cook 40 minutes 



Note for my part I doubled the quantities, and I cooked the cake in three round mold of 20 cm each. I had records a little high. 

[ ![cake yoghurt with chocolate super fluffy](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-yaourt-au-chocolat-super-moelleux.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-yaourt-au-chocolat-super-moelleux.jpg>)
