---
title: Lassi of mango
date: '2012-12-31'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/lacet-de-mangue_thumb.jpg
---
[ ![mango lace](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/lacet-de-mangue_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/lacet-de-mangue_2.jpg>)

Hello everybody, 

we take advantage of this beautiful sun that radiates and warms our hearts, pity that here in England it will not be for 2 or 3 months of summer, but home is 1 week, or 3 to 4 days .... 

in any case, with this heat, we make out of our drawers those recipes that were preciously hidden in winter ... 

this time it's a delicious  ** lassi of mango  ** , huuuuuuuuuum, I had a good time .... it's cool ...    


**Lassi of mango**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/lacet-de-mangue_thumb.jpg)

Recipe type:  Dessert, drinks  portions:  3  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 300 gr of mango 
  * 400 grams of plain yoghurt; 
  * 1 to 2 tablespoons of sugar (it depends on the mango if it is sweet or not) 
  * ½ teaspoon of ginger powder (optional) 
  * 120 ml of milk 



**Realization steps**

  1. place all the ingredients in the bowl of a blender. 
  2. mix very well, if you think that the consistency is too thick, you can add a little milk. 
  3. for more freshness, add ice cubes at the last moment. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
