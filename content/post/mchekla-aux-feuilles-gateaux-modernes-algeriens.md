---
title: mchekla with leaves - Algerian modern cakes
date: '2017-06-21'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian cakes
- Cakes
- Algeria
- Aid cake
- baklava
- Ramadan 2017
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mchekla-aux-feuilles-gateaux-algeriens-2011.jpg
---
##  mchekla with leaves - Algerian modern cakes 

Hello everybody, 

here is another recipe from [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , one of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-12344741.html>) which has been modernized. I realized this pretty [ cuteness ](<https://www.amourdecuisine.fr/categorie-10678931.html>) during the 2011 aid holiday, so you can see [ Algerian cakes from the aid ](<https://www.amourdecuisine.fr/article-aid-el-fitr-2011-les-gateaux-algeriens-82873081.html>) , I already posted the recipe [ Algerian cake ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) , the [ knots in butterflies ](<https://www.amourdecuisine.fr/article-les-gateaux-algeriens-noeuds-papillons-84677207.html>) ,as well as [ skandraniettes ](<https://www.amourdecuisine.fr/article-skandraniette-gateaux-algeriens-83562645.html>) , [ dry cakes ](<https://www.amourdecuisine.fr/categorie-11814728.html>) s: [ shortbread with strawberry jam ](<https://www.amourdecuisine.fr/article-25345485.html>) , the [ pistachio shortbread ](<https://www.amourdecuisine.fr/article-sables-aux-pistaches-et-confiture-d-abricots-83935371.html>) , or [ shortbread with apricot jam ](<https://www.amourdecuisine.fr/article-sables-aux-pistaches-et-confiture-d-abricots-83935371.html>) . 

in any case, I'm always going to explore the sublime books of [ modern Algerian pastries ](<https://www.amourdecuisine.fr/categorie-10678931.html>) that I bring with me on my last trip to Algeria, so wait, even more of these sublime recipes. 

otherwise for the mchekla recipe with leaves, I found it on Samira's book, feather edition: **Samira decoration cakes.**

and here is the recipe for the simple and modern mchekla on youtube: 

{{< youtube D9Jk1Krtr5s >}} 

So as I told you, the dough, I remain faithful to the dough that I like:   


**mchekla with leaves - Algerian modern cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mchekla-aux-feuilles-gateaux-algeriens-2011.jpg)

**Ingredients** for the pasta 

  * 250 gr of flour 
  * 75 gr of margarine 
  * 1 pinch of salt 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of white dye 
  * 1 teaspoon of dye (color of your choice, here orange) 
  * Orange blossom water + water 

the joke: 
  * 3 measures of ground almonds (I use a 400ml bowl) 
  * 1 measure of crystallized sugar 
  * zest of 1 lemon 
  * 1 cup of vanilla coffee 
  * egg as needed 

decoration: 
  * honey 
  * silver beads 
  * egg white 
  * shiny food 



**Realization steps** preparation of the dough: 

  1. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  2. Mix well with your fingertips 
  3. take a little part of this mixture 
  4. in a little flower water mix, dye them to have the color of your choice 
  5. start picking up the dough (let a little, to color it in white) with this water, 
  6. add water if you need, and knead a few seconds. 
  7. turn into a ball and let it rest. 
  8. add white dye to a little flower water, and collect the rest of the flour with, to have the white dough. 

stuffing: 
  1. mix the almond and the sugar, 
  2. add the vanilla, and the lemon zest 
  3. stir an egg, and add it gently to the almonds 
  4. pick up the mixture, if necessary add in small quantity, another egg (no need to add everything) 

for the method of preparation: 
  1. you can always follow the video of mchekla with leaves 
  2. lower both pasta on a work surface 
  3. lower the stuffing to a thickness of 1.5 cm 
  4. line the dough on a work plan sprinkled with flour 
  5. put on the stuffing lowered   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mchekla-aux-feuilles-gateau-algerien_thumb1.jpg)
  6. cut rounds with a mold 5 cm in diameter 
  7. cut out orange and white leaves 
  8. put some egg white on the almond stuffing 
  9. paste the colored paste sheets alternating colors 
  10. let it dry all night 
  11. cook in an average oven 
  12. after cooking, dip the cakes in honey 
  13. then drain on a rack 
  14. decorate with shiny food, and pearls 


