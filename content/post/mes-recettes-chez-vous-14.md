---
title: my recipes at home 14
date: '2011-12-10'
categories:
- the tests of my readers
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg
---
![2011-09-04 for the moment tout1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg)

Hello everybody, 

as every time, my faithful readers and my friends of the blogosphere, try some of my recipes, and sharing with me, and frankly it makes me really happy that they appreciate the recipe. 

if you too, you have tried one of my recipes, do not worry about the quality of the photos, I understand that sometimes you make the photos with your webcam, sometimes it is with the mobile phone, and this is not the most important, the most important thing is that you have to try the recipe, and that you love it, and share it with me, is a great joy. 

so if you too, you have tried one of my recipes, send me your link, or your photos on this email: 

amour2cuisine@gmail.com 

so I leave room for your wonderful tests: 

![Recipes-5139.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/Recettes-51391.jpg)

![DSC03108.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/DSC031081.jpg)

![http://img.over-blog.com/320x316/0/58/03/91/desserts/desserts-2/DSCN1383.JPG](http://img.over-blog.com/320x316/0/58/03/91/desserts/desserts-2/DSCN1383.JPG)

[ donuts ](<https://www.amourdecuisine.fr/article-donuts-faciles-et-delicieux-68158130.html>) at Anais Moha (faithful reader) 

[ Barbie birthday cake ](<https://www.amourdecuisine.fr/article-princesse-barbie-gateau-d-anniversaire-86764345.html>) at Mess mes 

[ Mderbel ](<https://www.amourdecuisine.fr/article-25345474.html>) at Anais Moha 

![Photo1266.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/Photo12661.jpg)

[ cheese chicken croquettes ](<https://www.amourdecuisine.fr/article-croquettes-de-poulets-farcies-de-fromage-46949639.html>) in [ Sousou-food ](<http://www.sousou-culinaire.com/>)

[ kaak nekkache ](<https://www.amourdecuisine.fr/article-kaak-nekkache-aux-amandes-82530128.html>) at Sonia (faithful reader) 

[ creme brulee with coconut milk ](<https://www.amourdecuisine.fr/article-creme-brulee-au-lait-de-coco-et-grenade-85865112.html>) at Katia (faithful reader) 

[ Pasta gratin with tuna ](<https://www.amourdecuisine.fr/article-gratin-de-pates-au-thon-87232101.html>) at Fabia (faithful reader) 

[ mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouskoutchou-mouchete-87564683.html>) chez dahbia ( fidele lectrice) 
