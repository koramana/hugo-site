---
title: Biscuit end base for birthday cake
date: '2013-11-23'
categories:
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/biscuits-fin2-1024x572.jpg
---
[ ![Biscuit end base for birthday cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/biscuits-fin2-1024x572.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/biscuits-fin2.jpg>)

##  Biscuit end base for birthday cake 

Hello everybody, 

A sponge cake that you can use as a base for your birthday cakes? more on video ... 

So here is the recipe or the fine biscuit that I always use to prepare my birthday cakes, certainly I have to prepare 3 to 4 cakes, all depends on the height of the biscuit, but the result is superb ... a birthday cake well airy, you choose the height you want, and above all you can put the colors of your choice for each biscuit ... 

A cake that I adopted since I realized it the first time ... You can replace a quantity of the flour by a little almond powder, and the result will be only better ... 

So here, I will pass you the ingredients for a single biscuit, if you want three biscuits, multiply the ingredients by three ...   


**Biscuit end / base for birthday cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/biscuits-fin-1024x713.jpg)

Recipe type:  basic pastry recipe  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 2 eggs 
  * 60 gr of sugar 
  * 60 gr of flour 
  * 1 pinch of baking powder 



**Realization steps**

  1. beat the eggs thoroughly with the sugar until the mixture is white and very airy. 
  2. add flour and baking powder, gently without breaking the mixture. 
  3. cook in a hot oven for 15 to 20 minutes (depending on the capacity of your oven) 



{{< youtube 4dvegkPn3yQ >}} 

[ ![fine cookies](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/biscuits-fin-1024x713.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/biscuits-fin.jpg>)
