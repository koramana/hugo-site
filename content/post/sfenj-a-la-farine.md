---
title: Sfenj has flour
date: '2016-11-24'
categories:
- crepes, beignets, donuts, gauffres sucrees
- fried Algerian cakes
tags:
- Boulange
- Algeria
- To taste
- desserts
- Pastry
- Cakes
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sfenj-a-la-farine-3-1.jpg
---
![Sfenj has flour 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sfenj-a-la-farine-3-1.jpg)

##  Sfenj has flour 

Hello everybody, 

Who does not like sfenjs? These delicacies that have rocked our childhood, I remember when we were little and went from school, at the time we lived on the top floor of a small building, and we felt from afar the smell of frying donuts. Curious that I was, I was looking for the source of this beautiful smell on each floor, and what relieved me to know that the smell did not come from the neighbors, hihihihi ... I ended the remaining floor in jumping like a grasshopper just to get home faster. I do not tell you how I knocked on the door so that my mother opens it, you imagine the scene ... 

Today, I share with you the original recipe for flour sfenj, just as I learned from my dear mother, with the cooking tips and success of this recipe. 

![Sfenj has flour](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sfenj-a-la-farine-4.jpg)

I admit that despite my [ sfenj with semolina ](<https://www.amourdecuisine.fr/article-sfenj-algerien.html> "Algerian sfenj") are very good, because I like all the crispness that brings the semolina to the texture of the sfenj, remains that the sfenj with the flour marks me much, especially when they are as perfect as those of khfafji. 

So today it is with pleasure that I share with you the recipe for flour sfenj on video! this video is not perfect because my little one was spinning between my feet ... I was really afraid of cooking oil. In any case you can see the video that I realized for you from Sfenj to flour: 

**Sfenj has flour**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sfenj-a-la-farine-1.jpg)

portions:  5-8  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 500 gr of flour 
  * 2 tablespoons sugar 
  * 1 tablespoon dehydrated baker's yeast 
  * 1 teaspoon of salt 
  * 1 sachet of baking powder 
  * lukewarm water 
  * oil for frying. 



**Realization steps**

  1. in a small bowl, mix the baker's yeast, a little sugar and warm water and let the yeast grow 
  2. place the flour in a large bowl. 
  3. add the remaining sugar, baking powder and salt. 
  4. mix, when the yeast has risen well, add it to the mixture 
  5. introduce the water in small quantity to have a very soft paste and a little sticky. 
  6. oil the dough by lifting it up and tapping it against the working terrine. 
  7. continue this gesture to aerate the pulp well, and give it elasticity. 
  8. When the dough is well kneaded and elastic. cover and let the dough double in size.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sfenj-a-la-farine-2-1.jpg)
  9. take the paste again after doubling the volume, degas it (oil your hands well) 
  10. prepare dumplings with the dough the size of a beautiful mandarin 
  11. place 2 stoves, one with just a little oil over medium heat, 
  12. and another with a little more oil on higher fire. 
  13. take the ball between your well oiled hands, make a hole and place it in the warm oil pan. 
  14. let it up gently and form a few balls on top, then transport it to the hot oil stove. 
  15. cook well on both sides. then carry on paper towels. 
  16. enjoy with a good tea, a little jam, honey, or just coated in sugar. 



![Sfenj has flour](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/sfenj-a-la-farine-8.jpg)
