---
title: Apple pie with salted butter caramel
date: '2017-10-08'
categories:
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/apple-pie-au-caramel-au-beurre-sal%C3%A9-5.jpg
---
[ ![apple pie with salted butter caramel 5](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/apple-pie-au-caramel-au-beurre-sal%C3%A9-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/apple-pie-au-caramel-au-beurre-sal%C3%A9-5.jpg>)

##  Apple pie with salted butter caramel 

Hello everybody, 

I take advantage of these pretty apples that I found at an unbeatable price at the market, and in addition, they were fleshy, juicy, and very rich in taste, to prepare a delicious autumn apple pie at [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html> "salted butter caramel") . 

This pie is just falling, I adopted for its preparation the classic recipe for apple pie, but because I add salted butter caramel I greatly reduced the amount of sugar ... and the result was perfect. 

For the decoration, I looked for the special mold apple pie in my closet, and impossible to find it, I think my husband threw it thinking it was a piece of no use, hihihihi, So I chose the first impression mold that came to hand, I had already lost enough time looking for the other mold. 

**Apple pie with salted butter caramel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/apple-pie-dautomne-au-caramel-au-beurre-sal%C3%A9-024.jpg)

portions:  10  Prep time:  30 mins  cooking:  60 mins  total:  1 hour 30 mins 

**Ingredients** [ Salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html> "salted butter caramel") : 

  * 1 glass of sugar (240 ml glass) 
  * 60 ml of water 
  * 100 gr of salted butter 
  * 100 ml of liquid cream 

[ Shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast") : 

  * 300 g of flour 
  * 180 g of butter 
  * 40 g of crystallized sugar 
  * 50 g icing sugar 
  * 30 g of almond powder 
  * 1 sachet of vanilla sugar 
  * 2 egg yolk 
  * 1 pinch of salt 

stuffing of apples: 
  * the juice of 2 lemons 
  * 6-7 sweet and tart apples 
  * 80 gr of sugar 
  * ½ tsp ground cinnamon 
  * 1 Pinch of freshly grated nutmeg 
  * 2 tablespoons all-purpose flour 

garnish: 
  * 1 egg 
  * a little bit of sugar 



**Realization steps** salted butter caramel preparation: 

  1. mix together the sugar and water in a medium saucepan and cook on medium-low heat until the sugar is dissolved. 
  2. Add the butter and bring to a slow boil. Continue cooking over medium heat until mixture becomes dark golden brown, almost copper colored. 
  3. Remove from the heat and slowly add the liquid cream, paying attention 
  4. Whip the final mixture to introduce the cream and let cool while you prepare the apple filling and the shortbread. 

Preparation of the shortbread dough: 
  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a dough is detached from the sides of the bowl and is non-sticky. 
  4. pick up and put in the fridge for 15 min. 

preparation of the apple filling: 
  1. in a ceramic bowl place the lemon juice. 
  2. peel, epipine and slice apples with a knife or fine mandolin. 
  3. Place the apple slices in the lemon juice and sprinkle with 2 tablespoons of sugar. Let it soften slightly and release some of their juice, 20 to 30 minutes. 
  4. In a small bowl, place the sugar, add spices and flour, and mix well. 
  5. Drain the apples with their juices and add over the mixture sugar and spices. 
  6. mounting of the magpie: 
  7. spread the dough short on a mold of almost 22 cm in diameter, and keep the falls. 
  8. Spread the apple filling in the prepared pie shell, cram well and leave the center slightly higher in the center.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/apple-pie-au-caramel-au-beurre-sal%C3%A9-008.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/apple-pie-au-caramel-au-beurre-sal%C3%A9-008.jpg>)
  9. Pour ½ cup caramel sauce evenly over the apples (you can use ¾ cup for a sweet pie). 
  10. decorate the upper crust using the design of your choice. 
  11. Refrigerate the pie in the refrigerator for 10-15 minutes. 
  12. Meanwhile preheat the oven to 180 degrees C 
  13. Brush the dough with the egg wash, and sprinkle with the desired amount of sugar Place the pie in the oven for 50 to 60 min. Or depending on the capacity of your oven. 
  14. Cool completely on a rack for 2-3 hours. 



[ ![autumn apple pie with salted butter caramel](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/apple-pie-dautomne-au-caramel-au-beurre-sal%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/apple-pie-dautomne-au-caramel-au-beurre-sal%C3%A9.jpg>)

Merci pour vos visites et commentaires, Passez une belle journee. 
