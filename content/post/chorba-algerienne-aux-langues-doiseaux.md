---
title: Algerian chorba with bird tongues
date: '2018-04-24'
categories:
- Algerian cuisine
tags:
- Algeria
- Easy cooking
- Fast Food
- Ramadan 2018
- Ramadan
- Algerian dish

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-1.jpg
---
![Algerian chorba with bird tongues 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-1.jpg)

##  Algerian chorba with bird tongues 

Hello everybody, 

Who says Ramadan, said Chorba! Because there is no more good than a hot soup after a language day of Lent. My husband does not have to decide with him, because for him it is [ chorba frik or jari ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) during all ramadan (yes it's weird, but he only likes chorba frik). As for me, I like varied and I do not just soups, I also make veggie soups that I just love to sip, what happiness! 

Today, I share with you my Algerian soup (yes because there is the Moroccan version and the Tunisian version) so I share with you my Algerian chorba with the languages ​​of birds, this soup is just superb, just have the right balance salt, tomato, to have the tastiest taste of these small pasta (Orzo among others), because frankly it is not easy to have this delicious balance to have the corba to the languages ​​of birds the best possible , yumiiiiiiiiiiiiiiii! 

{{< youtube VZAXAt2dQgw >}} 

The recipe in Arabic: 

![Algerian chorba with bird tongues 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-2.jpg)

**Algerian chorba with bird tongues**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-4.jpg)

portions:  6  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 200 g lamb meat 
  * some meatballs minced (optional) 
  * 1 fresh tomato 
  * 1 tablespoon of tomato paste 
  * 1 onion 
  * salt, black pepper and paprika 
  * 1/4 cup of cinnamon 
  * 1 teaspoon dried mint 
  * a bunch of coriander 
  * 1 celery stalk 
  * 1 handful of chickpeas (I use chickpeas in a box) 
  * 1 glass of 100 ml of bird tongues 
  * 1 tablespoon extra virgin olive oil 
  * 1 tablespoon of smen 



**Realization steps**

  1. place the cubed meat in a pot with oil and smen. 
  2. add chopped onion, chopped coriander and celery stalk. 
  3. let everything come back until the onion becomes translucent and the meat releases its water. 
  4. Add paprika, salt, black pepper, canned tomato 
  5. add the chopped tomato, mint and cinnamon. Leave for a few minutes. 
  6. Add the water (almost 2 liters), cover the pot and cook for a tender meat. 
  7. now add the glass of bird tongues and chickpeas. 
  8. watch, stirring occasionally so that the pasta does not stick to the bottom. 
  9. present with a little chopped coriander. 



![Algerian chorba with bird tongues 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-3.jpg)
