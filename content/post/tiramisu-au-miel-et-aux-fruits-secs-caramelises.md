---
title: Tiramisu with honey and caramelized dried fruits
date: '2018-05-04'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cheesecakes and tiramisus
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tiramisu-au-miel-et-au-fruits-secs-caram%C3%A9lis%C3%A9es-1-832x1024.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tiramisu-au-miel-et-au-fruits-secs-caram%C3%A9lis%C3%A9es-1-832x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tiramisu-au-miel-et-au-fruits-secs-caram%C3%A9lis%C3%A9es-1.jpg>)

##  Tiramisu with honey and caramelized dried fruits 

Hello everybody, 

Tiramisu honey and dried fruits caramelized, hum that it must be delicious, but of course, a tiramisu is always good, melting, rich in flavor, and especially especially, I love it .... 

This recipe, and unfortunately, it's not me who realized it, sniff, sniff, but rather Lunetoiles .... but why I'm not his neighbor, at least I would not have to look behind the screen and dream from afar to plunge my teaspoon to enjoy a piece .... 

In any case you are many to ask me the blog Lunetoiles, and I tell you again, Lunetoiles does not have a blog, it's a passion for cooking, who likes to share his recipes at home, despite that I really insist on her to make a blog, but she does not want to, because she thinks it's a lot of work ... 

She likes to post her recipes at home, and I'm not going to say no, hihihihih.   


**Tiramisu with honey and caramelized dried fruits**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tiramisu-au-miel-et-au-fruits-secs-caram%C3%A9lis%C3%A9es-784x1024.jpg)

Recipe type:  Dessert  portions:  12  Prep time:  30 mins  cooking:  3 mins  total:  33 mins 

**Ingredients**

  * 4 eggs 
  * 6O gr of sugar 
  * 500 g of mascarpone 
  * 3 Cuil. Honey soup 
  * 1 box of 36 biscuits with spoon 
  * 1 very strong coffee bowl (300 ml) 

For caramelized dried fruits: 
  * 80 gr of a mixture of slivered almonds, pine nuts and pistachio, or other 
  * 40 gr of powdered sugar 



**Realization steps** First, caramelize the dried fruits: 

  1. In a non-fat frying pan, toast almonds, pine nuts and pistachios. 
  2. When the pan is hot, sprinkle with 40 g sugar, mix. 
  3. Let the dry caramelize, stirring to separate that they do not form a compact pile. 
  4. Remove from heat and place the caramelized dried fruit on a sheet of baking paper to allow them to cool. 

Prepare the tiramisu base: 
  1. Heat the honey. 
  2. Separate whites from yellows. 
  3. Beat the egg whites tightly in a large bowl. 
  4. Beat the yolks and 60 g of sugar in a salad bowl until the mixture whitens, then add the hot honey and mix well. 
  5. Then add the mascarpone, and mix well. 
  6. Stir in the snow gradually and gently using a spatula or wooden spoon with the previous mixture (snow white, honey, yellow, sugar). 
  7. Now add the almonds, pistachios and caramelised pine nuts (keep a handful for decoration). 
  8. Dip the biscuits in the coffee spoon and line the bottom of a dish or individual dish. 
  9. Cover with a layer of mascarpone mixture, eggs, honey, dried fruit. 
  10. Put a row of sponge biscuits soaked in coffee and cover with a second layer of mascarpone mixture ... 
  11. Sprinkle with the handle of caramelized dried fruits to decorate. 
  12. Refrigerate for at least 4 hours for individual cups and several hours for a dish. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tiramisu-au-miel-et-au-fruits-secs-caram%C3%A9lis%C3%A9es-3-685x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/Tiramisu-au-miel-et-au-fruits-secs-caram%C3%A9lis%C3%A9es-3.jpg>)

if you have tried one of my recipes, send me the picture here: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
