---
title: Missed cakes with lemon and cherries
date: '2014-03-01'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/03/G%C3%A2teaux-manqu%C3%A9s-au-citron-et-cerises-1.jpg
---
[ ![Missed cakes with lemon and cherries](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/G%C3%A2teaux-manqu%C3%A9s-au-citron-et-cerises-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/G%C3%A2teaux-manqu%C3%A9s-au-citron-et-cerises-1.jpg>)

Hello everybody, 

Very nice pictures of these missed cakes with lemon and cherries, Lunetoiles had shared with me, there is almost a year, but I never had the chance to post the recipe ... 

Sweet cakes with desire, rich in taste, perfumed with lemon, a recipe worthy of being made .... 

Without delay, I pass you the recipe, and I hope to be able to post the other recipes of Lunetoiles who queute on my mailbox: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

**Missed cakes with lemon and cherries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/G%C3%A2teaux-manqu%C3%A9s-au-citron-et-cerises-3.jpg)

Recipe type:  Dessert  portions:  8  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 4 eggs 
  * 300 g of cherries of your choice 
  * 100 g of flour + 10 g for the mold 
  * 70 g of butter + 10 g for the mold 
  * 100 g caster sugar 
  * 1 untreated lemon 
  * icing sugar 
  * salt 



**Realization steps**

  1. Preheat the oven th. 7 (210 ° C). 
  2. Wash, then thinly slice the lemon. 
  3. Melt the butter gently. 
  4. Break the eggs by separating the whites from the yolks. 
  5. Whip the yolks with the sugar until the mixture whitens and then add the flour little by little. 
  6. Pour the melted butter, the lemon zest. 
  7. Beat the egg whites with a pinch of salt. Then add them gently to the previous preparation. 
  8. Wash and pit the cherries. 
  9. Reserve 100 g and add the others to the cake batter. 
  10. Butter and flour individual mussels. Pour the dough and bake. 
  11. After 10 minutes of cooking, put the remaining cherries on the cake, lower the thermostat to 6 (180 ° C) and continue cooking for 10 minutes. 
  12. When the cakes are cooked, wait 15 minutes before unmolding them and then powder them with icing sugar. 



[ ![Missed cakes with lemon and cherries](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/G%C3%A2teaux-manqu%C3%A9s-au-citron-et-cerises-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/G%C3%A2teaux-manqu%C3%A9s-au-citron-et-cerises-2.jpg>)

Thanks Lunetoiles for the recipe: 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
