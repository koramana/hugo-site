---
title: mini pizza with the brains of the lamb
date: '2016-11-01'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/mini-pizza-a-la-cervelle-dagneau.jpg
---
![mini pizza with the brains of lamb](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/mini-pizza-a-la-cervelle-dagneau.jpg)

##  mini pizza with the brains of the lamb 

Hello everybody, 

mini pizza has the brains of the lamb: Yes my friends you read the title well, and it's not a recipe for halloween, whatever it will please many people who are celebrating halloween, lol. So today I share with you my delicious recipe mini pizza with brains of lamb, which I realized in mini version, why mini? because I have a little story to tell you. 

While cleaning the brains of the lamb, I wanted to discover a little anatomy to the children, I call them in the kitchen, and I show them, the brains of the lamb, telling them that we have a brain a little like that, bigger, and I started doing "the mom who knows everything". My children had their eyes wide open, and the ears even more to capture everything I told them, when my elder asked me the question: but what are you going to do with this brain mom ?, I tell him, we will eat, he says to me: what? are we going to be zambies ??? (What? We will become zambies ???) brakes, zzzzzt ... 

and I who wanted to make them a beautiful pizza with the brains of lamb ... had to see their heads, with one who closes the eyes, and the other who already takes his stomach in his arms, as if he really hurts, well ... for actors, I have at home! 

So the only solution for mini pizzas, and like that, everyone will choose his own filling, something that is not difficult to satisfy my children: 

[ ![mini pizza 002](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/mini-pizza-002.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/mini-pizza-002.jpg>)

yes already the olive I put it just for the photo, because my children do not like olives on their pizzas. 

So when this mini pizza has the brains of the lamb, I will not tell you how much I love the brains of lamb or mutton, and since when I did not eat it? But recently we met a Turkish butcher, who cuts meat like us in Algeria, the Pakistanis here, who generally hold halal butchers, do not offer much choice apart from the thigh or shoulder of the lamb, only the minced meat is found. 

So since I know this butcher, I enjoy it, it sells almost everything, except the merguez, which is a shame. And the brains, we buy now every time, we find the cool at home, and I always make [ brains in tomato sauce ](<https://www.amourdecuisine.fr/article-cervelle-d-agneau-en-sauce-tomate-89450259.html> "brains of lamb in tomato sauce - chtitha mokh") . and I thought, brains in tomato sauce, we had a good time with bread, so why not garnish my pizza with it? and it was a delight, according to the testimony of my husband, who liked the taste. 

**mini pizza with the brains of the lamb**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/mini-pizza-a-la-cervelle-dagneau-1.jpg)

portions:  4 - 6  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * [ magic pasta ](<https://www.amourdecuisine.fr/article-pate-magique.html> "Magic paste")
  * 4 to 5 fresh tomatoes 
  * 2 cloves garlic 
  * salt, black pepper, thyme, oregano 
  * olive oil 
  * 100 g of lamb brains, or less depending on taste 
  * Mozzarella 



**Realization steps** prepare the magic dough: 

  1. Mix all these ingredients and add enough lukewarm water until soft, soft and easy to work. 
  2. Knead a few minutes. 
  3. Form the dough into a ball and let it rise under cover, protected from drafts until it has doubled in volume.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/pate-magique.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/pate-magique.jpg>)

Prepare the tomato sauce: 
  1. in a saucepan, cook the tomato cut in pieces, with all the remaining ingredients, except the brains 
  2. When the tomato sauce is well cooked, remove the fire, and throw the brains cut in pieces, I did not cook the brains, because, it cooks very quickly, so it will cook in the oven.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/mini-pizza-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/mini-pizza-3.jpg>)

pizza preparation: 
  1. spread the pizza dough on a floured space 
  2. cut circles with an imprinted mold almost 10 cm in diameter.   

  3. decorate the circles with the preparation, and according to your taste. 
  4. bake until cooking mini pizza 
  5. at the end of the oven decorate with mozzarella and serve immediately. 



[ ![Lamb's baked pizza](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/pizza-a-la-cervelle-dagneau.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/pizza-a-la-cervelle-dagneau.jpg>)
