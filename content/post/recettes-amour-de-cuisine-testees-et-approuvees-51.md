---
title: Kitchen love recipes tested and approved 51
date: '2015-02-01'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

A big thank you to all of you, for your comments, and especially after trying one of my recipes, even if you missed it, I prefer to know, because sometimes, when writing a recipe, we make a small mistake which makes some people miss the recipe, when you contact me, you will allow me to review and correct the recipe. 

For those who have tested, and taken pictures, follow this page to send them to me: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") . **

Especially do not forget to mention the name of the recipe, the photo by itself, is not enough 

You can also join the group [ cook with love of cooking ](<https://www.amourdecuisine.fr/article-mes-realisations-chez-vous-49.html>) , a group sharing ideas and recipes. 

If you are a blogger, it will be nice to leave the link to your blog too, and if you are not a blogger, your name will be good enough. Also if you have tested and approved a recipe from my blog, and you have no photos, just leave a comment in the recipe in question. your opinion will surely help people who want to try the recipe. 

[ ![KODAK Digital Still Camera](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-a-lorange-de-Yveline-B.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-a-lorange-de-Yveline-B.jpg>)

[ carrot cake ](<https://www.amourdecuisine.fr/article-cake-aux-carottes.html> "carrot cake") at Yveline B 

[ ![2014-11-07-1362](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/2014-11-07-1362-1024x575.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/2014-11-07-1362.jpg>)

[ Stuffed cabbage from Houriat el matbakh ](<https://www.amourdecuisine.fr/article-choux-farcis-a-la-viande-hachee.html> "stuffed cabbage with chopped meat") , at Fatima zohra of tlemcen. 

[ ![mini kounafa of rachida dj](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/mini-kounafa-de-rachida-dj.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/mini-kounafa-de-rachida-dj.jpg>)

[ mini kounafa ](<https://www.amourdecuisine.fr/article-mini-konafa-ktayefs-a-la-creme.html> "Mini konafa, ktayefs with cream") at Rachida Dj 

[ ![Wamani merou](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Wamani-merou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Wamani-merou.jpg>)

[ Basboussa ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html> "algerian patisserie, Basboussa in video بسبوسة سهلة") at Wamani M 

[ ![lemon nanzy pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tarte-au-citron-nanzy.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tarte-au-citron-nanzy.jpg>)

[ lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee.html> "lemon meringue pie") of nanzy 

[ ![Cornet1](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Cornet1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Cornet1.jpg>)

[ Almond Cornets ](<https://www.amourdecuisine.fr/article-cornets-aux-amandes-et-noix-gateaux-algeriens.html> "almond and nut cones - Algerian cakes") at Anti Ahla 

[ ![Basque cake at Sali's](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-basque-chez-Sali.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-basque-chez-Sali.jpg>)

[ Basque cake ](<https://www.amourdecuisine.fr/article-gateau-basque-la-confiture-de-cerises-noires.html> "Basque cake with black cherry jam") at Sali Nice 

[ ![Anti esses](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/les-esses-Anti.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/les-esses-Anti.jpg>)

[ the esses, Algerian cakes ](<https://www.amourdecuisine.fr/article-les-esses-gateaux-aux-amandes-gateau-algerien-2015.html> "Esses, cakes with almonds, Algerian cake 2015") at a reader who did not mention her name 

[ ![cake the braid.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-la-tresse.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-la-tresse.bmp.jpg>)

[ Algerian cake braid ](<https://www.amourdecuisine.fr/article-la-tresse.html> "the braid الظفيرة حلوى جزائرية Algerian cakes") to a reader who did not mention her name. 

[ ![bread stuffed with Wissal meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pain-farcie-a-la-viande-de-Wissal.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pain-farcie-a-la-viande-de-Wissal.jpg>)

[ breads stuffed with minced meat ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee.html> "buns with chopped meat") from Wissal T 

L’une de vous va trouver que ces photos ne sont pas sur cet article, ne vous en faites pas, ça va être dans l’article prochain 
