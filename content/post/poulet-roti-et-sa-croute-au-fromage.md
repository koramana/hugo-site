---
title: roasted chicken and cheese crust
date: '2017-09-24'
categories:
- Algerian cuisine
- diverse cuisine
tags:
- Poultry
- Mustard
- Oven

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-roti-et-sa-croute-au-fromage-1.jpg
---
[ ![Roasted chicken and its cheese crust 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-roti-et-sa-croute-au-fromage-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-roti-et-sa-croute-au-fromage-1.jpg>)

##  roasted chicken and cheese crust 

Hello everybody, 

Who does not like chicken? at home I have to realize it at least once a week ... And if it's with fries, I'm not telling you: it's party time at home. This time, I realized my roast chicken in a totally different way, I saw that on the Samira TV channel, too bad I did not keep the name of the good woman who made the recipe, because I do not I am not always this chain because of my children. 

Besides, even for this recipe, I followed the explanations at the beginning, until she put the chicken in the oven, and after, I do not even know how she finished cooking, because my children were returned from school and changed the chain to see: Shaun the sheep, hihihihi. 

In this recipe, we cut the chicken side of the chest to have a unique piece (photo in the method of realization) we cook the well buttered chicken in a little [ homemade chicken broth ](<https://www.amourdecuisine.fr/article-bouillon-de-poulet-fait-maison.html>) preferably in the oven, in a tray covered with an aluminum foil, and when the chicken is well cooked, it is made au gratin with a beautiful layer of biker, breadcrumbs and cheese ... So the recipe interests you? 

**roasted chicken and cheese crust**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-roti-et-sa-croute-au-fromage-2.jpg)

portions:  6  Prep time:  15 mins  cooking:  90 mins  total:  1 hour 45 mins 

**Ingredients**

  * a big full chicken (or as for me baby chickens) 
  * ½ liter of chicken broth, if not make a cube of bouillon in half a liter of water) 
  * 50 gr of butter in pieces 
  * a good drizzle of extra virgin olive oil. 
  * 2 bay leaves 
  * some branches of thyme 
  * salt and black pepper 
  * spices of your choice (I put some caraway and some paprika) 
  * a few cloves of garlic cut on 4 in lengths 

to brown: 
  * biker 
  * breadcrumbs 
  * grated cheese 



**Realization steps**

  1. wash the chicken well. 
  2. cut the length of the side of the chest just in the middle to have a single piece of chicken 
  3. place it on a board and press well with your hands to have a fairly flat piece of chicken.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/la-coupe-du-poulet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/la-coupe-du-poulet.jpg>)
  4. salt and pepper generously the chicken on both sides. 
  5. place the chicken in a baking tray, on the pieces of garlic (to flavor it when cooking) 
  6. pour a little chicken broth into the tray (not all the amount, leave a little to deglaze after the juice of cooking the chicken) 
  7. add the pieces of butter over the chicken. 
  8. add bay leaf, thyme, and spices, then drizzle with olive oil   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/avant-de-mettre-au-four.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/avant-de-mettre-au-four.jpg>)
  9. cover the tray with a sheet of aluminum foil, and place in a preheated oven at 200 degrees C, depending on the weight of the chicken (allow 20 minutes for each kilo of chicken)   
the cooking of my chickens lasted almost two hours, as they were in two different trays, and once I placed one up and the other down, and then I alternated for an even cooking of the two chickens. 
  10. When the chicken is cooked, remove it from the oven, brush it with a layer of mustard, add the bread crumbs on top, then the grated cheese. 
  11. gratin from above to give the chicken a beautiful color. 
  12. Divide the cooking juice with a little remaining chicken broth. pass the Chinese to have a juice without pieces 
  13. enjoy chicken with french fries or [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html>)



[ ![roasted chicken and its cheese crust](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-roti-et-sa-croute-de-fromage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-roti-et-sa-croute-de-fromage.jpg>)
