---
title: Spinach and raclette cheese quiche
date: '2015-09-17'
categories:
- pizzas / quiches / pies and sandwiches
tags:
- Salty pie
- Vegetables
- inputs
- Algeria
- la France
- Easy cooking
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Quiche-aux-%C3%A9pinards-et-au-fromage-a-raclette-1.jpg
---
[ ![Spinach and raclette cheese quiche 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Quiche-aux-%C3%A9pinards-et-au-fromage-a-raclette-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Quiche-aux-%C3%A9pinards-et-au-fromage-a-raclette-1.jpg>)

##  Spinach and raclette cheese quiche 

Hello everybody, 

Announce that we will eat a quiche does not make great happiness at home ... There is only my daughter, and my little one who loves quiches ... So I must always provide something in parallel for my eldest and his dad ... 

[ ![Spinach and raclette cheese quiche 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Quiche-aux-%C3%A9pinards-et-au-fromage-a-raclette-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Quiche-aux-%C3%A9pinards-et-au-fromage-a-raclette-2.jpg>)   


**Spinach and raclette cheese quiche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Quiche-aux-%C3%A9pinards-et-au-fromage-a-raclette.jpg)

**Ingredients** 1 homemade pastry: 

  * 250g of flour T45 or T55 
  * 3cl of water 
  * 1 egg 
  * salt 
  * 125g of ointment butter 

garnish: 
  * 400 g young fresh spinach sprouts 
  * 1 onion 
  * 3 eggs 
  * 30 cl of liquid cream 
  * slices of raclette cheese 
  * Salt pepper 
  * olive oil 



**Realization steps** for the pasta: 

  1. Pour the flour on a work surface and add the butter ointment with your fingertips, merging the mixture. You will get a sandy mix like a crumble dough. 
  2. Make a well and add the egg, water and salt. Stir in the liquid while kneading. 
  3. When a ball begins to form, pour it on the worktop. 
  4. Stretch the dough 5 to 6 times with the palm of your hand on the work surface 
  5. form a ball. Wrap it in plastic wrap and store in the refrigerator for 2 hours before using. 

Mounting: 
  1. Go for a pie dish 28 cm in diameter with the broken dough. Remove the excess dough with a knife and prick the bottom with a fork. 
  2. Reserve the dough in the refrigerator while preparing the filling. 
  3. Peel and slice the onion. 
  4. Heat a pan with a little olive oil and sweat the onion for 5 minutes. 
  5. Add the young spinach leaves and cook. Salt, pepper. Stir regularly. 
  6. When the spinach is cooked, let cool. 
  7. Whisk eggs and cream into a bowl. Salt, pepper. 
  8. Take out the mold from the refrigerator, put some slices of raclette cheese in the bottom. 
  9. When the spinach is lukewarm, put them on the pie shell. 
  10. Sprinkle egg / cream mixture. 
  11. Place over slices of raclette cheese. 
  12. Preheat your oven to 180 ° C and bake for 30 to 40 minutes. 
  13. You can spend the last moment under the grill to brown the cheese. 



[ ![Spinach and raclette cheese quiche](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Quiche-aux-%C3%A9pinards-et-au-fromage-a-raclette-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Quiche-aux-%C3%A9pinards-et-au-fromage-a-raclette-3.jpg>)
