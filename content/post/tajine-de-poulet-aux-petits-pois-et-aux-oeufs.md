---
title: chicken tajine with peas and eggs
date: '2012-07-14'
categories:
- gateaux algeriens au miel
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/poulet-aux-petits-pois-et-aux-oeufs_thumb1.jpg
---
##  chicken tajine with peas and eggs 

Hello everybody 

here is a very delicious **chicken tajine with peas and eggs** , A recipe from [ Algerian cuisine  ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , a very simple and easy recipe. 

you can also see: 

[ tajine jelbana, tajine of peas with artichokes ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-aux-artichauts---tajine-jelbana-103530292.html>) . 

[ tagine peas with a cardoon ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee-94791294.html>) . 

[ velvety peas ](<https://www.amourdecuisine.fr/article-41284308.html>) . 

**chicken tajine with peas and eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/poulet-aux-petits-pois-et-aux-oeufs_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * Ingredients: 
  * 4 to 5 chicken legs 
  * 750 gr of frozen peas (you can use fresh peas) 
  * 4 to 5 eggs 
  * 1 tablespoon of canned tomato 
  * 3 tablespoons of oil 
  * 2 to 3 carrots 
  * ½ onion 
  * 1 clove of garlic 
  * salt and black pepper 
  * ½ tsp of coriander / garlic powder mixture 
  * ½ liter of water 



**Realization steps**

  1. In a pot, fry the chickens, with a little oil, onion and garlic. 
  2. add carrots cleaned and sliced 
  3. Add spices and salt and simmer for 10 minutes. 
  4. Add the tomato and cover with water 
  5. when the mixture starts to boil introduce the peas 
  6. cook until the sauce is completely reduced. 
  7. After cooking, remove the chicken pieces. 
  8. Put the mixture in a baking dish. 
  9. break the eggs over, and bake for 15 minutes in an oven preheated to 150 degrees C. 
  10. you can roast the chicken in the oven, or pass for a moment in a hot oil bath 
  11. Serve with chicken pieces. 


