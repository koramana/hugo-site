---
title: potato gratin with shrimp
date: '2009-05-08'
categories:
- Algerian cuisine
- Algerian cakes without cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/05/226507041.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/226507041.jpg)

sometimes we try to do with what we have at home, and today I made a gratin of potatoes with the 100 grams of shrimp that were in my freezer. 

so my ingredients were: 

  * 3 potatoes (we are not numerous at home) 
  * 100 gr of shrimp 
  * 1 can of fresh cream 
  * milk 
  * salt, black pepper and my spice that I really like garlic / coriander 



so I steamed my washed and peeled potatoes, cut into slices. 

and at the same time I sautéed the shrimp in a little olive oil 

in a small dish of gratin, put a layer of potato, season with the condiments you like, cover with shrimp. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/226506791.jpg)

cover with the rest of the potato, pour the amount of milk that will barely cover the last layer of potato. 

finally cover with the cream, and sprinkle the top with a little black pepper (I add my spice garlic / coriander) 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/226506961.jpg)

put in the oven, until the potato is cooked, and no more liquid milk 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/226507041.jpg)

serve hot,   
and it was a treat. 

in any case I liked it. 

bon appétit. 
