---
title: Tuna Stuffed Msemen / مسمن محشي
date: '2013-09-05'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- idea, party recipe, aperitif aperitif
- Unclassified
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/mhadjeb-069_thumb.jpg
---
##  Msmes stuffed with tuna 

Hello everyone, 

Here's one [ Algerian traditional recipe ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , based on semolina that is carefully greased to obtain a very very mellow dough, which is stuffed after the taste, and when cooked in a slit tajine .... 

a delight to do absolutely. [ Algerian cuisine with photo ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-avec-photo-103322241.html>)   


**Tuna Stuffed Msemen / مسمن محشي**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/mhadjeb-069_thumb.jpg)

Recipe type:  Algerian dish  portions:  10  Prep time:  60 mins  cooking:  20 mins  total:  1 hour 20 mins 

**Ingredients** dough: 

  * a little semolina 
  * salt water 

for the stuffing: 
  * 2 onions 
  * 3 tomatoes 
  * 2 cloves garlic 
  * salt, pepper, coriander powder 
  * tuna 



**Realization steps** dough: 

  1. wet the semolina and the salt, with water, gently until it is easy to handle, 
  2. oil very well without tearing the dough, so we try to petrire without separating a piece of another to give elasticity to the dough, and we add the water in small quantities, we petrit, and we add the water it is heated until the dough becomes very soft, and when it is stretched it does not tear, like a shewing gum. 
  3. let it rest, and prepare the stuffing. 

the joke: 
  1. cut the sliced ​​onions 
  2. return to the oil you drain from the can of tuna. 
  3. add the crushed garlic, then the tomatoes cut into small cubes, and add the condiments according to your taste, let everything cook well. 
  4. let cool 
  5. when the stuffing is cold, add in the tuna. 
  6. return to the dough which has rested well, and begin to form balls the size of a mandarin, 
  7. let it rest again, so that you can spread the dough easily. 
  8. coat the work surface well, with the oil, spread the balls of pasta one by one and fill with stuffing. 
  9. fold the dough like an envelope on the stuffing, and cook on a very thick and well oiled Tajine or a crepe. 



bon appétit 
