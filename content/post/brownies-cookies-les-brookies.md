---
title: Brookies brownies cookies
date: '2015-11-08'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/BROOKIES-NOIX-DE-COCO-2.jpg
---
[ ![Brookies brownies cookies](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/BROOKIES-NOIX-DE-COCO-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/BROOKIES-NOIX-DE-COCO-2.jpg>)

##  Brookies brownies cookies 

Hello everybody, 

Ah brownies what I like, and I avoid realizing greedy as I am, I can eat all the cake "in one go" and all alone .... 

I thank anyway Lunetoiles for this nice recipe brownie cookies Brookies: marbled version with coconut, the Brookies has coconut, the wonderful association between cookies and brownies ... I fund !!! 

you can also see other great brownie recipes, like brownie with peanut, [ Hazlenut Brownie ](<https://www.amourdecuisine.fr/article-brownies-aux-noix.html>) , [ brownie chocolate hazelnuts ](<https://www.amourdecuisine.fr/article-brownies-au-chocolat-et-noisettes.html>) , [ brownie cheesecake ](<https://www.amourdecuisine.fr/article-brownie-cheesecake-recette-facile-delicieuse.html>) , [ chocolate chocolate cheesebrownie ](<https://www.amourdecuisine.fr/article-cheesebrownie-cafe-chocolat-cheesecake-brownie.html>) , [ Brookies ](<https://www.amourdecuisine.fr/article-les-brookies.html>)

more 

[ ![Brookies brownies cookies](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/BROOKIES-NOIX-DE-COCO-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/BROOKIES-NOIX-DE-COCO-2.jpg>)

**Brookies brownies cookies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/BROOKIES-NOIX-DE-COCO-300x213.jpg)

portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** for brownie dough: 

  * 100 g of butter 
  * 2 eggs 
  * 50 g of chocolate 
  * 8 c. tablespoon of coconut milk 
  * 100 g of sugar 
  * 100g of flour 

for the cookie dough: 
  * 125 g of soft butter 
  * 100 g caster sugar 
  * 1 egg 
  * 100g of flour 
  * 1 C. coffee baking powder 
  * 4 c. grated coconut 
  * 100 g of chocolate 



**Realization steps**

  1. Preheat the oven to th. 6/180 °. 
  2. Start with the brownie dough: melt the butter in a bain-marie with the chocolate and the coconut milk. 
  3. Beat the eggs with the sugar, add the mixture of butter, chocolate and coconut milk, then the flour. 
  4. Mix well. 
  5. Pour the dough into a buttered and floured square dish. 
  6. Prepare the cookie dough: in a bowl, crush the butter with the sugar, then add the egg. 
  7. Add flour and baking powder, grated coconut and chopped chocolate. 
  8. Pour the cookie dough over the brownie dough and spread it well. 
  9. Bake for 40 minutes. 
  10. Let cool and unmold. 



[ ![Brookies brownies cookies](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/BROOKIES-NOIX-DE-COCO-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/BROOKIES-NOIX-DE-COCO-3.jpg>)
