---
title: Aid moubarek has everyone
date: '2008-12-08'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/12/baklawa-filo-81.jpg
---
On this beautiful occasion I say to everyone: 

**Happy feast of the help, I hope that their sacrifice is accepted by the good god.**   
and above all I hope everyone has a good day full of family meeting of love and tenderness.   
it is not my case and the case of my children, one is a thousand places of all the family, but I know that you are all there, with us.   
I prepared some cakes for this occasion:   
the Baklawa: 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/12/baklawa-filo-81.jpg)   
you will find the recipe [ right here ](<https://www.amourdecuisine.fr/article-25555887.html>)

I also prepared, crunchy: 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/12/Picture-17651.jpg)   
the recipe coming soon 

and a little bniouen: 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/12/Picture-17151.jpg)

recipe [ right here ](<https://www.amourdecuisine.fr/article-25345483.html>) . 

Encore une fois bonne fete de l’aid a tout le monde. 
