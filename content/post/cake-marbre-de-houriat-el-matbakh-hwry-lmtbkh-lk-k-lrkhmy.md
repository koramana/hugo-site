---
title: 'Marble cake from el matbakh, حورية المطبخ: الكعكة الرخامية'
date: '2013-03-21'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-marbre-1_2.jpg
---
##  Marble cake from el matbakh, حورية المطبخ: الكعكة الرخامية 

Hello everybody, 

Marbled cake of houriat el matbakh, حورية المطبخ: الكعكة الرخامية: yesterday I redid this delicious marble cake of Houria, and I told myself this is an opportunity to renovate my old photos, and blow dust this recipe, so, this cake is to do and remake without stopping, so that it is too good, light, and super presentable. 

**Marble cake from el matbakh, حورية المطبخ: الكعكة الرخامية**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-marbre-1_2.jpg)

**Ingredients**

  * 2 glass of flour 
  * 1 glass of sour 
  * 1 glass of melted butter, or oil, or half the butter / oil mixture (for me it was oil) 
  * ¾ glass of milk 
  * 1 pinch of salt 
  * 2 cac vanilla 
  * 5 large eggs 
  * 2 cases of cornstarch 
  * 3 cac of baking powder 
  * 3 cases of cocoa 
  * 2 cases of apricot jam 



**Realization steps**

  1. separate, the egg yolks from the whites, beat the egg whites well with a pinch of salt, and a little sugar, then set aside   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/32795199.jpg)
  2. mix the egg yolks with the remaining sugar, and vanilla, add the melted butter, or oil gently while whisking. then add the jam, and finally add the milk. 
  3. Now add the flour + baking powder mixture and mix gently. 
  4. and finally add the egg bench gently with a wooden spoon, without breaking it, try to have a well-aired mixture. 
  5. separate, this mixture has two equal parts, in the first part add the cocoa, and in the other part add the cornstarch.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/32795200-300x225.jpg)
  6. now, to have the marbling, pour into a cake mold butter and flour, alternately a spoon of the cocoa mixture and a spoon of the mixture of corn starch,   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/327952071.jpg)
  7. you can use a large soup spoon (for each mixture) or even see a small ladle, to have a well-defined marbling.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/32795212.jpg)
  8. cook in a preheated oven at 200 degrees C, then after 5 min reduce the temperature to 180 degrees C, bake for 35 minutes. or on your oven. 



and you will be astonished at the taste, and the result of this marbled cake of houriat el matbakh, حورية المطبخ: الكعكة الرخامية, a true delight, to do and redo, I invite you .... 

Enjoy your meal. 

thank you 
