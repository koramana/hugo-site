---
title: Berry couscous salad with beets
date: '2017-07-06'
categories:
- couscous
- recipes of feculents
- salads, salty verrines
tags:
- Ramadan 2016
- Ramadan
- Easy cooking
- Healthy cuisine
- Algeria
- Mixed salad
- Varied salad

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-de-couscous-perl%C3%A9-et-betterave.jpg
---
[ ![baked couscous salad and beetroot](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-de-couscous-perl%C3%A9-et-betterave.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-de-couscous-perl%C3%A9-et-betterave.jpg>)

##  Berry couscous salad with beets 

Hello everybody, 

In fact I made this salad with pearl couscous and beetroot twice with even more ingredients (what I had at home) and we were not disappointed each time. 

[ ![beaded couscous salad and beetroot 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-de-couscous-perl%C3%A9-et-betterave-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-de-couscous-perl%C3%A9-et-betterave-2.jpg>)

You can also because you always have time to come and participate in the [ new round, the subject of which is apricot ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-30-labricot.html>) I'm the godmother, so I'm waiting for your entries on the article in question! 

**Berry couscous salad with beets**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-de-couscous-perl%C3%A9-et-betterave-3.jpg)

portions:  4  Prep time:  5 mins  cooking:  30 mins  total:  35 mins 

**Ingredients**

  * 2 glasses of beaded couscous (small shots, you can use regular couscous) cooked in salt water 
  * 2 medium cooked beets 
  * 1 handful of chopped parsley leaves 
  * 1 handful of fresh dill leaves, chopped 
  * 1 handful of nuts, coarsely chopped 
  * salt and freshly cracked black pepper 
  * ¼ glass of olive oil 
  * juice of 1 lemon 



**Realization steps**

  1. cut the beets into small cubes while they are still hot. 
  2. Mix with the beaded couscous to make a beautiful pink color. 
  3. add chopped herbs and nuts. 
  4. prepare a vinaigrette with lemon juice and olive oil, and add to the salad. 
  5. season with salt and black pepper, mix well. 
  6. Refrigerate for at least an hour or two to completely cool, and to enhance the flavors. 
  7. Check the seasoning and serve garnished with some nuts and herbs. 



[ ![beaded couscous salad and beetroot 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-de-couscous-perl%C3%A9-et-betterave-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-de-couscous-perl%C3%A9-et-betterave-5.jpg>)

et voila la liste des participantes a cette ronde: 
