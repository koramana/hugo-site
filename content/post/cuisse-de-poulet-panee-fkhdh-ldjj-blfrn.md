---
title: paned chicken leg أفخاذ الدجاج بالفرن
date: '2017-04-17'
categories:
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/poulet-rotis-doritos_thumb.jpg
---
[ ![breaded chicken leg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/poulet-rotis-doritos_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/poulet-rotis-doritos.jpg>)

##  Breaded chicken leg 

Hello everybody, 

Hum, from [ roasted chicken ](<https://www.amourdecuisine.fr/categorie-12364208.html>) with a good skin **crunchy** , **tender** inside, and scented .... with doritos, yes **Doritos** , it is delicious **tortilla chips** Great **crunchy** , which forms a nice crust on the chicken ... a taste to discover, and I recommend it. 

you can replace doritos with oats, unsweetened cereals, or just flour and breadcrumbs,   


**paned chicken leg أفخاذ الدجاج بالفرن**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/poulet-rotis-doritos_thumb.jpg)

Recipe type:  dish  portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 3 to 4 complete chicken legs. 
  * 3 tablespoons of olive oil. 
  * 1 teaspoon of cumin. 
  * 1 teaspoon coriander powder. 
  * ½ teaspoon of turmeric. 
  * ½ teaspoon of black pepper. 
  * salt according to taste. 
  * 2 to 3 pack of Doritos 



**Realization steps**

  1. clean the chicken legs.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/poulet-a-la-doritos_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/poulet-a-la-doritos_thumb.jpg>)
  2. the day before, mix the olive oil with all the spices. 
  3. place the chicken legs in a bag and pour over this marinade. 
  4. close the bag, rotate in all directions, so that the marinade infiltrates everywhere. 
  5. leave to spawn all night. 
  6. in the morning crush the doritos slowly. 
  7. cover the chicken legs with the crushed doritos. 
  8. place the legs in a baking dish preheated to 180 degrees C 
  9. cook for 40 to 50 minutes, depending on the oven capacity. 
  10. serve with a very fresh salad. 



[ ![breaded chicken doritos](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/poulet-doritos_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/poulet-doritos.jpg>)
