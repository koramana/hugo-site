---
title: Hazelnut and orange basboussa
date: '2015-01-03'
categories:
- Algerian cakes with honey
- oriental delicacies, oriental pastries
- ramadan recipe
tags:
- Pastry
- desserts
- Algerian cakes
- Cakes
- Semolina Cake
- Semolina
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/basboussa-aux-noisettes-et-oranges-1.jpg
---
[ ![basboussa with hazelnuts and oranges](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/basboussa-aux-noisettes-et-oranges-1.jpg) ](<https://www.amourdecuisine.fr/article-basboussa-aux-noisettes-et-oranges.html/basboussa-aux-noisettes-et-oranges-1>)

##  Hazelnut and orange basboussa 

Hello everybody, 

And here is my recipe that participates in the game: recipes around an ingredient. It is a hazelnut and orange basboussa, a recipe super easy to make, and too good. 

No need for a lot of ingredients to make this basboussa, nor heavy equipment, a wooden spoon will do all the work for you. Oh I forget you need a good grater or a zester too. 

**Recipe from the magazine: Good Food, issue: November 2008**

[ ![basboussa with hazelnuts and oranges](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/basboussa-aux-noisettes-et-oranges-2.jpg) ](<https://www.amourdecuisine.fr/article-basboussa-aux-noisettes-et-oranges.html/basboussa-aux-noisettes-et-oranges-2>)   


**Hazelnut and orange basboussa**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/basboussa-aux-noisettes-et-oranges-5.jpg)

portions:  8  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients**

  * 100 gr of shelled hazelnuts 
  * 50 gr fine semolina 
  * 120 gr of powdered sugar 
  * 1 cup of baking powder 
  * 2 big oranges 
  * 4 medium eggs 
  * 200 ml of table oil 
  * 50 gr of icing sugar 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. Oil a 23 cm savarin tin 
  3. Put the hazelnuts in a pan and grill over medium heat, stirring frequently 
  4. Let cool and crush in a fine powder if possible in a blender. 
  5. mix the hazelnut powder, the semolina, the powdered sugar and the baking powder. 
  6. Grate finely the zest of an orange and mix with eggs and oil. Beat well with a wooden spoon, then incorporate the dry ingredients. 
  7. Pour the mixture into the prepared baking pan and bake for 30-40 minutes, until the blade of a knife inserted in it goes out dry. 
  8. While the cake is cooking, gently remove the zest from the other orange with a zester. 
  9. Put in a saucepan the juice of the two oranges, the sugar. Bring to a boil, then simmer gently for 5 minutes. 
  10. Towards the end, introduce the orange zest. 
  11. When the cake is cooked, allow to cool in the mold slightly, before unmolding on a plate. 
  12. While the cake is still hot, prick it with a toothpick and pour the syrup over to soak it well. 
  13. Serve as it is, or with orange segments. The cake will be refrigerated in an airtight container for 3-4 days. 



[ ![basboussa with hazelnuts and oranges](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/basboussa-aux-noisettes-et-oranges-4.jpg) ](<https://www.amourdecuisine.fr/article-basboussa-aux-noisettes-et-oranges.html/basboussa-aux-noisettes-et-oranges-4>)

As I told you this is a recipe that participates in the game: 

[ recipes around an ingredient # 1 ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-1.html> "Recipes around an ingredient # 1")

et voila les recettes des filles qui ont participé a ce jeu: 
