---
title: mojito without alcohol, cocktail mint and lime
date: '2016-05-18'
categories:
- boissons jus et cocktail sans alcool
tags:
- Ramadan 2016
- Easy cooking
- Ramadan
- Juice
- Drinks
- Summer kitchen
- cocktails

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-a-lananas-7.jpg
---
[ ![mojito without alcohol, cocktail mint and lime](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-a-lananas-7.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-a-lananas-7.jpg>)

##  mojito without alcohol, cocktail mint and lime 

Hello everybody, 

For those who follow me, they must know that I really like homemade drinks, be it smoothies, fresh juice in the juicer or lemonade. Besides, when I'm lazy to make drinks, I prepare fruit-scented waters ... Frankly it's just refreshing at will, a thousand times better than soft drinks and too sweet. 

And then looking for more, I saw that we could perfume a mojito with lots of fruits and their juices .... Wow !!! I took advantage of this little pineapple that I had. Quickly with my juicer, I had his juice extracted, to have a mojito without alcohol very fresh with fresh juice .... A delight. 

[ ![mojito without alcohol, cocktail mint and lime](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-a-lananas-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-a-lananas-4.jpg>)   


**mojito without alcohol with pineapple**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-10.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 300 ml pineapple juice 
  * 1 lime cut into 8 quarters 
  * 2 tablespoons brown sugar   
watch out for sugar if you use commercial pineapple juice 
  * 10 large fresh mint leaves 
  * 200 ml of sparkling water (lemonade) 
  * ice cubes 



**Realization steps**

  1. place 4 lemon wedges in each glass   
(I use 2 glasses of 250 ml each) 
  2. place on the mint leaves, then 1 tablespoon of sugar in each glass. 
  3. With a pestle, crush the lemon and mint so that the juice mixes with the sugar.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-a-lananas-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-a-lananas-5.jpg>)
  4. Add the pineapple juice and fill the glass with crushed ice (to the top) and fill with the sparkling water or lemonade. 
  5. Decorate according to your taste and enjoy immediately. 



[ ![mojito without alcohol, cocktail mint and lime](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-a-lananas-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-a-lananas-6.jpg>)

[ ![mojito without alcohol, cocktail mint and lime 10](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-10.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/mojito-sans-alcool-10.jpg>)
