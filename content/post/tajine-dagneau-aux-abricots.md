---
title: Lamb tagine with apricots
date: '2013-09-27'
categories:
- dessert, crumbles and bars
- Algerian cakes without cooking
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg
---
# 

#  [ ![tajine hlou lham lahlou](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg>)

#  tajine lahlou, lham lahlou 

a tasty dish, a marriage of sweet and salty in a dish to taste with a good homemade bread. 

Tajine never goes away on the first day especially of the month of Ramadan, there are even people who like it to be always on their table. 

in any case, at home, I can say that I'm the only one who will eat it, not serious, because this dish can be well preserved in the refrigerator. 

and here is my little recipe, as my mother always did 

**Lamb tagine with apricots**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg)

Recipe type:  dish, Algerian cuisine  portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * Lamb meat 
  * dried prunes (for apricot mix, prune apples) 
  * some dried raisins 
  * ½ onion 
  * some butter or smen 
  * 3 cases of crystallized sugar 
  * 2 cases of orange blossom water 
  * 1 pinch or 1 cinnamon stick 
  * 1 little saffron or dye 
  * 1 pinch of salt 



**Realization steps**

  1. arrange all the cut meat, butter or smen, chopped onion, dye, cinnamon and salt. 
  2. cover with about ½ liter of water and cook. 
  3. in the meantime, dip the prunes in the water for a few moments, as well as the raisins. 
  4. remove them, dip them in the sauce, add the sugar and orange blossom water, cook on low heat, until the prunes swell. 
  5. reduce the sauce 
  6. you can sprinkle your dish with some blanched and roasted almonds. 



good tasting . 

for a tajine hlou recipe without meat: 

![apple tajin](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tajin-hlou-1.jpg) [ TAJINE HLOU AUX POMMES ET ANIS ÉTOILÉ, SANS VIANDE ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html> "tajine hlou with apples and star anise, without meat")
