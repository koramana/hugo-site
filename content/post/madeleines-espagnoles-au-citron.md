---
title: Spanish madeleines with lemon
date: '2017-03-26'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/madeleine-espagnole-au-citron-1.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/madeleine-espagnole-au-citron-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/madeleine-espagnole-au-citron-1.jpg>)

Place this tag where you want the button to render. 

##  Spanish madeleines with lemon 

Hello everybody, 

What do I like the name of this recipe when lunetoiles passed me: Magdelinas de Lemon, Pretty Pretty, is not it? but the problem if I give this name to my recipe, google will take me for a Spanish blog, hihihihi, and I have no connection with the Spanish language, so we will stay on our feet, and we will name this recipe with its name that suits it: Spanish madeleines with lemon ... 

Do you like it, hihihihi, but if one of you is Spanish, and give me the recipe in Spanish, I'll probably publish it here, lol   


**Spanish madeleines with lemon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/madeleines-espagnoles-au-citron-2.jpg)

Recipe type:  Dessert  portions:  12-15  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 2 eggs 
  * 130 gr of sugar 
  * 120 ml of sunflower oil 
  * 70 gr of cream with 35% fat 
  * 175 gr of flour 
  * ½ packet of baking powder 
  * The zest of 1 lemon 



**Realization steps**

  1. First put the trays in the muffin pan. 
  2. Put eggs and sugar in a salad bowl and beat with an electric whisk for 6 minutes. 
  3. Add the lemon zest and beat another 6 minutes. 
  4. Add the oil and cream and mix for 3 minutes. 
  5. Add the flour and the yeast. 
  6. Mix for 20 seconds. 
  7. Let stand for the time to preheat your oven. 
  8. Preheat the oven to 180ºC. 
  9. Pour the dough into the cavities of the cake pan, a little over half and sprinkle on each a little sugar. 
  10. Put in the oven at 180ºC for 20 minutes. 



Spanish Cupcakes / magdelenas 

Magdalenas are like cupcakes, they are sweet with a small hint of lemon, fluffy and rich. 

In Spain, they eat at breakfast with "café con leche,". 'Magdalenas' are a great treat to any breakfast 

Ingredients 

  * 2 eggs 
  * 130 grams of sugar 
  * 120 ml of sunflower oil 
  * 70 gr cream 35% fat 
  * 175 grams of flour 
  * ½ package baking powder 
  * Zest of 1 lemon 

preparation 

  1. Preheat oven to 180 º C (370 F). 
  2. Before starting Place paper liners in a cupcake pan. 
  3. In a bowl of a standing electric mixer, beat the eggs and sugar for 6 minutes or until light and fluffy. 
  4. Add the lemon zest and beat 6 minutes more. 
  5. Add oil and cream for 3 minutes. 
  6. Whisk the flour and baking powder in a medium bowl, 
  7. Slowly for the dry ingredients in the bowl of your mixer, mixing until just incorporated and totally scraped down the sides. 
  8. Fill each liner a bit more than halfway, and sprinkle on each other sugar. 
  9. Bake for about 20 minutes or until a toothpick inserted in the center comes out totally clean. 
  10. Immediately dump the cupcakes on the counter and place on a wire rack to cool. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/madeleines-espagnoles-au-citron.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/madeleines-espagnoles-au-citron.jpg>)

Now if you have tried one of my recipes, send me the picture here: 

**[ Mes recettes chez vous ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **
