---
title: Pineapple and Gingerbread Trifle
date: '2015-04-03'
categories:
- dessert, crumbles and bars
- recettes sucrees
- sweet verrines
tags:
- biscuits
- desserts
- verrines
- Chantilly
- creams
- mascarpone
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trifles-a-lananas-.jpg
---
[ ![Pineapple and Gingerbread Trifle](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trifles-a-lananas-.jpg) ](<https://www.amourdecuisine.fr/article-trifles-a-lananas-et-pain-depice.html/trifles-a-lananas>)

##  Pineapple and Gingerbread Trifle 

Hello everybody, 

If there is a recipe that I will strongly advise, it's going to be this pineapple and gingerbread trifle recipe. This is the first time I make gingerbread pineapple, and frankly I do not know why I've never done it before, however, the gingerbread and my favorite cake .... Ah, but what have I missed so far, so do not make my bet and try to make recipes with pineapple and gingerbread, you will not be disappointed. 

**Pineapple and Gingerbread Trifle**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trifles-a-lananas-et-pain-depice.jpg)

portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 8 slices of pineapple 
  * 4 slices of [ gingerbread ](<https://www.amourdecuisine.fr/article-pain-d-epice-moelleux.html> "fluffy gingerbread")
  * A few spoons of pineapple juice 
  * a handful of dried cranberries (cranberries) 
  * 100 ml of fresh cream 
  * 50 ml of mascarpone 
  * 2 to 3 tablespoons of sugar (according to taste) 



**Realization steps**

  1. cut the pineapple slices into small pieces 
  2. put the gingerbread a little coarsely 
  3. in 4 clean verrines, place the equivalent of a pineapple slice at the bottom of each. 
  4. place over the equivalent of a slice of gingerbread crumbs 
  5. cover with another layer of pineapple pieces. 
  6. sprinkle each verrine with a tablespoon of pineapple juice 
  7. add the cream in whipped cream with a little sugar 
  8. whip the mascarpone to make it soft. 
  9. add the mascarpone to the whipped cream 
  10. scatter some cranberries on each verrine 
  11. decorate the verrine with cream, and some cranberries 
  12. referge at least 2 hours before serving 



[ ![Pineapple and Gingerbread Trifle](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trifles-a-lananas-et-pain-depice-2.jpg) ](<https://www.amourdecuisine.fr/article-trifles-a-lananas-et-pain-depice.html/trifles-a-lananas-et-pain-depice-2>)

the participants in this tour are: 

_*[ Soulef ](<https://www.amourdecuisine.fr/>) , blog https://www.amourdecuisine.fr/ with Trifles with pineapple and gingerbread. _

_* and[ Christelle ](<%20http://christelle56.over-blog.com>) of the blog http: //christelle56.over-  blog.com avec : panna cotta chocolatée à l’ananas. _
