---
title: cake without cooking, chocolate fingers
date: '2018-03-24'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateaux-sans-cuisson-doigts-au-chocolat_thumb1.jpg
---
##  cake without cooking, chocolate fingers 

Hello everybody, 

always with the recipes of **cake without cooking** , that I prepared often, before embarking on the adventure of the blog, and that I would like to share with you, not only it is easy to realize, but also, it is very good, and very pretty to present . 

easy ingredients, and accessible to everyone, and the most important that we can adapt them according to our tastes and what we have, at home. 

look at the link, for more [ cakes without cooking   
](<https://www.amourdecuisine.fr/categorie-11745248.html>)   


**cake without cooking, chocolate fingers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateaux-sans-cuisson-doigts-au-chocolat_thumb1.jpg)

portions:  30  Prep time:  20 mins  cooking:  2 mins  total:  22 mins 

**Ingredients**

  * 1 box of biscuits 
  * 1 glass (220 ml) of coconut 
  * 5 tablespoons of melted butter 
  * jam ( [ peach jam ](<https://www.amourdecuisine.fr/article-confiture-de-peches-un-delice.html> "Peach jam .... a delight") for me) but you can one of your choice. 
  * dark chocolate for decoration 
  * coconut for decoration 



**Realization steps**

  1. in the bowl of a blender, crush the biscuits powder 
  2. add the coconut 
  3. then stir in the butter. 
  4. pick up the dough with the jam until you have a paste that picks up. 
  5. prepare rolls or fingers 8 cm long and 2.5 cm in diameter 
  6. put in the fridge for 1 hour, so that the dough fits well 
  7. melt the chocolate in a bain-marie 
  8. place your fingers on a rack, and cover them gently with the melted chocolate, using a spoon 
  9. sprinkle the coconut on the sides, before the chocolate freezes 
  10. take the chocolate well. 
  11. melt 2 to 3 squares of chocolate, and place them in a cone of baking paper or a bag and pass over the cakes to draw scoffers. 
  12. let it freeze, and put them in boxes, in a cool place (not in the fridge otherwise you will have a point on the chocolate) 
  13. can be kept for 3 days in an airtight container in a cool place. 


