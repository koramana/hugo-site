---
title: gratin of sardine dumplings in tomato sauce, from houriya el matbakh
date: '2015-10-12'
categories:
- houriyat el matbakh- fatafeat tv
tags:
- Garlic
- Easy recipe
- Algeria
- inputs
- accompaniment
- Oven cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/gratin-de-boulettes-de-sardine_thumb1.jpg
---
##  gratin of sardine dumplings in tomato sauce, from houriya el matbakh 

Hello everybody, 

Like many of you, I am a big fan of the television channel "Fatafeat", and especially of the sublime houriya in its emission: Houriyat el matbakh, 

and during my last stay, she prepared a nice recipe of sardine dumplings gratin, I tried them, and it was a delight, especially with the fillet of lemon juice, huuuuuuuuuuuum, I do not tell you !!! So I give you the recipe. 

**gratin of sardine dumplings in tomato sauce, from houriya el matbakh**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/gratin-de-boulettes-de-sardine_thumb1.jpg)

portions:  6  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 1 kg of sardines, 
  * 1 kg of fresh tomatoes, 
  * 1 cup of water, 
  * 1 bay leaf, 
  * 1 teaspoon of sweet paprika, 
  * 6 cloves of garlic, 
  * A small bunch of chopped coriander, 
  * 1 tablespoon ground cumin, 
  * Salt and pepper to taste, 
  * 1 tablespoon mint, 
  * ½ cup soaked or half cooked cooked rice, 
  * 1 tablespoon vinegar or lemon juice, 
  * 2 tbsp. olive oil, 
  * Green peppers, 



**Realization steps**

  1. Clean the sardines, remove the head and stop, as well as the tail. 
  2. then chop, Add the soaked rice and spices and chopped coriander, then chopped garlic. 
  3. Mix well and leave in the refrigerator 
  4. Peel the tomatoes and grate them finely, 
  5. Put the olive oil in a saucepan on the fire and add the remaining garlic, bay leaf and chilli, 
  6. cook the mixture for a few minutes then add salt, black pepper and water, 
  7. And leave the mixture on low heat until it is reduced. 
  8. take the mixture of sardines, and form balls and put them in the tomato sauce, in a baking tin. 
  9. Add a few slices of green and red pepper, 
  10. Cover and bake, 
  11. at the end of the oven, decorate with a little parsley and a lemon filet 



thank you for your visits 

bonne journee 
