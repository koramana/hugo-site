---
title: the bniouene bniouen lbniouen
date: '2011-10-31'
categories:
- gateaux, et cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/l-binouen-010.jpg
---
![the bniouene bniouen lbniouen](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/l-binouen-010.jpg)

##  the bniouene bniouen lbniouen 

Hello everybody, 

the bniouene bniouen lbniouen is delicious cake without cooking, which can be realized in the blink of an eye and more, it is too good. 

the bniouens are super simple to do, I usually do my recipe at random, but for you, I will try to give some measures, I hope that it will help you.   


**the bniouene bniouen lbniouen**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/lbniouen-4.jpg)

**Ingredients**

  * 2 boxes of biscuits (in England it's tea biscuit, in France it's LU cakes ???) 
  * softened butter 
  * honey according to taste 
  * Turkish halwat (I have to look for his name in French, but it's a kind of sugar sesame paste, I like that) 
  * cocoa 
  * a box of dark chocolate 
  * crushed peanuts or almonds or nuts, pistachios (all this is optional) for me pistachios 



**Realization steps**

  1. we start by passing the biscuits to the blender. 
  2. and add after the cocoa, and the Turkish halwat, mix everything well, then add honey, crushed pistachios and soft and softened butter, mix well, add honey according to taste, and butter as the dough becomes manageable, be careful not to put too much. 
  3. take the mixture in a tray, which can be put in the refrigerator, and spread our dough, by hand until having a height which goes from 3 cm to 4 cm. 
  4. We place the chocolate in a bain marie, to melt it, and we cover our cake, you can decorate the top of the chocolate with coconut, or peanuts, or other. 
  5. we place in the fridge. 
  6. after the dough becomes stronger, we cut into lozenges, and we place our little delights in boxes. 



![the bniouene bniouen lbniouen](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/bniouen-331.jpg)

bon appetit 
