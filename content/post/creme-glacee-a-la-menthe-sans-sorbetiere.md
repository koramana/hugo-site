---
title: ice cream with mint without ice cream maker
date: '2016-09-01'
categories:
- crème glacée, et sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/creme-glac%C3%A9e-%C3%A0-la-menthe-sans-sorbetiere.jpg
---
![ice cream with mint without sorbetiere](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/creme-glac%C3%A9e-%C3%A0-la-menthe-sans-sorbetiere.jpg)

##  ice cream with mint without ice cream maker 

Hello everybody, 

The **ice cream with mint without ice cream maker** is my girl's favorite. Not only this ice cream is good, but I find it more refreshing than all other ice creams, plus it is an ice cream that is done in a jiffy and without ice cream maker. 

I only insist on whipped cream, I did it once with whipped cream, but the result was a little sweet for my taste (but not for my daughter who liked it too much), so to have the best result, use the fresh liquid cream, to have a just perfect result. 

**ice cream with mint without ice cream maker**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/creme-glac%C3%A9e-%C3%A0-la-menthe-sans-sorbetiere-2.jpg)

**Ingredients**

  * 2 cups fresh cream. 
  * 200 ml sweetened condensed milk 
  * ½ c. coffee peppermint extract 
  * 3 drops of green food coloring 
  * 100 ml of mint syrup 
  * 1 cup chopped chocolate chips. 



**Realization steps**

  1. In a large bowl, whip the cold cream very cold at high speed for about 10-15 minutes until stiff peaks begin to form. 
  2. In another bowl, mix milk, peppermint extract, food coloring, and mint syrup 
  3. add the chopped chocolate.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/creme-glac%C3%A9e-a-la-menthe.jpg)
  4. Pour the whipped cream into your sweetened condensed milk mixture in small amounts and mix gently to combine. 
  5. Pour into a mold in the freezer and cover it (I use a special box for ice cream). 
  6. Put in the freezer for 6 hours or even better, for a whole night. 



![ice cream with mint without sorbetiere 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/creme-glac%C3%A9e-%C3%A0-la-menthe-sans-sorbetiere-1.jpg)
