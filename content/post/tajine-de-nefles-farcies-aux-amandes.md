---
title: tajine of stuffed loquat with almonds
date: '2016-05-13'
categories:
- Algerian cuisine
- recette de ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tajine-de-nefles-aux-amandes-076.CR2_.jpg
---
![tajine de nefles aux-almond 076.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tajine-de-nefles-aux-amandes-076.CR2_.jpg)

##  tajine of stuffed loquat with almonds 

Hello everybody, 

Here is a delicious dish, which my mother usually prepared on the first day of Ramadan, especially when Ramadan was in the medlar season (known as Zaarour, or za3rour in eastern Algeria, and Mchimcha in the Algerian center) ... 

and what is good this dish, a good tajine of loaves stuffed with almonds, a tajine with sweet salty flavors, well scented with cinnamon .... hum !!! what did I love? 

yesterday, my husband went shopping, he found loquats, it's the beginning of the season I think, of these fruits that I have not eaten for more than 7 years, but they were not as good that I expected. 

it was not really serious for me, because already I knew what I was going to do with these medlars .... the tajine of my mother .... yes! 

Ingredients: 

  * 200 gr of lamb meat 
  * 1 tablespoon and a half of butter 
  * 350 gr of sugar 
  * some saffron filament 
  * 1/2 teaspoon of cinnamon powder 
  * 1/2 teaspoon of salt 
  * 1 cinnamon stick 
  * 2 star anise, star anise 
  * 600 gr of medium sized loquats 
  * 250gr almonds powdered 
  * 1 egg white 
  * 1/4 cup of cinnamon 
  * 1 nice handful of raisins 



![tajine de nefles aux-almond 082.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tajine-de-nefles-aux-amandes-082.CR2_.jpg)

method of preparation: 

  1. In a pot, sauté the pieces of meat over low heat, with butter, saffron, cinnamon powder, salt, cinnamon stick and star anise. 
  2. when the meat turns a beautiful color, cover it with 1 liter of water and let it cook. 
  3. Prepare the stuffing by mixing the ground almonds, a tablespoon of sugar, the cinnamon and the egg white (I use enough egg white to have a paste that holds well and picks up) 
  4. Wash, peel, delect the loaves delicately without cutting the piece in half. 
  5. stuff the loquats with the prepared almond paste. 
  6. When the meat is cooked, remove it and set aside 
  7. add the remaining sugar to the sauce and carefully arrange the stuffed loaves. 
  8. Cook for ten minutes over very low heat to preserve the beautiful appearance of loquats. 
  9. When they are cooked, remove them delicately, put the meat back in the sauce and the raisins. 
  10. Let reduce on low heat. 
  11. when the sauce is well reduced, put the stuffed loaves back in, and turn off the heat. 
  12. present with a good homemade bread. 



**tajine of stuffed loquat with almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tajine-de-nefles-aux-amandes-082.CR2_.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 200 gr of lamb meat 
  * 1 tablespoon and a half of butter 
  * 350 gr of sugar 
  * some saffron filament 
  * ½ teaspoon of cinnamon powder 
  * ½ teaspoon of salt 
  * 1 cinnamon stick 
  * 2 star anise, star anise 
  * 600 gr of medium sized loquats 
  * 250gr almonds powdered 
  * 1 egg white 
  * 1/4 cup of cinnamon 
  * 1 nice handful of raisins 



**Realization steps**

  1. In a pot, sauté the pieces of meat over low heat, with butter, saffron, cinnamon powder, salt, cinnamon stick and star anise. 
  2. when the meat turns a beautiful color, cover it with 1 liter of water and let it cook. 
  3. Prepare the stuffing by mixing the ground almonds, a tablespoon of sugar, the cinnamon and the egg white (I use enough egg white to have a paste that holds well and picks up) 
  4. Wash, peel, delect the loaves delicately without cutting the piece in half. 
  5. stuff the loquats with the prepared almond paste. 
  6. When the meat is cooked, remove it and set aside 
  7. add the remaining sugar to the sauce and carefully arrange the stuffed loaves. 
  8. Cook for ten minutes over very low heat to preserve the beautiful appearance of loquats. 
  9. When they are cooked, remove them delicately, put the meat back in the sauce and the raisins. 
  10. Let reduce on low heat. 
  11. when the sauce is well reduced, put the stuffed loaves back in, and turn off the heat. 
  12. present with a good homemade bread. 


