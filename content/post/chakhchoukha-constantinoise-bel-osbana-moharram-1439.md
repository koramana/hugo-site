---
title: Chakhchukha Constantine beautiful osbana Moharram 1439
date: '2017-09-21'
categories:
- Algerian cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/09/Chakhchoukha-constantinoise-bel-osbana-1.jpg
---
![Chakhchukha Constantine beautiful osbana Moharram 1439](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/Chakhchoukha-constantinoise-bel-osbana-1.jpg)

##  Chakhchukha Constantine beautiful osbana Moharram 1439 

Hello everybody, 

It is with great pleasure that I address the Muslim community that will very soon celebrate the first day of the Muslim Year, the 1st Muslim Year 1439, which is the first month of the Muslim calendar and one of the most important. For this occasion, I share with you a traditional recipe of Algerian cuisine, Chakhchoukha Constantine with osbana, or osbane, Chakhchoukha constantinoise bel osbana for Moharram 1439. 

You can see here the method of the preparation of [ chakhchoukat edffar ](<https://www.amourdecuisine.fr/article-preparation-de-chekhchoukhat-dfar.html>) . This dish is much prepared in eastern Algeria, on this beautiful occasion. 

In any case I pass you the video of this beautiful Chakhchukha 3osbana: 

{{< youtube OL4kMjqrR0Q >}} 

**Chakhchukha Constantine beautiful osbana for Moharram 1439**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/Chakhchoukha-constantinoise-bel-osbana-3.jpg)

**Ingredients**

  * For osbana: 
  * sheep's guts with lungs and intestines 
  * 1 handful of precooked chickpeas 
  * 1 tablespoon of tomato paste 
  * 1 onion 
  * 2 cloves garlic. 
  * 1 bunch of coriander 
  * 1 tablespoon of paprika 
  * ½ tsp of black pepper 
  * 1 tablespoon of cumin 
  * 1 tablespoon of ras el hanoute 
  * salt 

for the chakhchoukha and the sauce: 
  * the osbana pouches 
  * 2 tablespoons tomato paste (or one if you are not too many and you will do a small amount) 
  * salt 
  * paprika 
  * black pepper 
  * 1 onion 
  * 2 cloves garlic 
  * 1 tablespoon of butter 
  * 3 tablespoons extra virgin olive oil 
  * 1 nice handful of chickpeas (I do a lot, we love at home) 
  * 1 kg of chekhchoukhat dfar   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Pr%C3%A9paration-de-chekhchoukhat-dfar.jpg)



**Realization steps** preparation of osbana: 

  1. Clean the guts, and place the casings and lungs for 10 minutes in salt water for easy cutting. 
  2. cut the belly into 5 or 6 large pieces, so that each piece folds in 2 will form a pocket about 10 cm side 
  3. cut the falls, lungs, and casings into cubes. 
  4. put these in a terrine 
  5. add the onion and grated garlic, chopped coriander, chickpeas, paprika, black pepper, cumin, salt, ras el hanout and canned tomato. 
  6. mix everything well 
  7. sew the pockets (the rough side inside) with a thick needle and a solid thread, leaving an opening to introduce the stuffing 
  8. Gently fill these stuffing pockets without overloading them 
  9. then finish sewing the opening 
  10. prick with a fork. 

preparation of sauce and chakhchoukha: 
  1. at the bottom of a couscous (or a pressure cooker), put some oil, onion and garlic on the blinder, the tomato paste, the osbana pouches, the salt and the rest of the spices, 
  2. let it simmer a little, then add the water, I will say a lot of water. Do not forget if the chickpeas are tender and precooked, put them just at the end of cooking, otherwise if they are soaked the day before, you have to put them now. 
  3. In a boil, put the crumbs of galettes in a terrine, and sprinkle with a little of your sauce, let a little imbibe, then put in the top of the couscoussier, and cook them with the steam. 
  4. remove after steam escaping, and sprinkle again with a little sauce (which is always entails cooking) 
  5. let still imbibe, and put back in the top of the couscoussier. 
  6. repeat until the crumbs are tender. 
  7. if necessary add water to your sauce, and adjust salt and tomato. 
  8. at the end and just before serving, put the crumbs of cakes in a pot, sprinkle with more sauce (not too much) 
  9. remove from the heat and put in a serving dish, garnish with chick peas, osbana and a little butter (what I like the part where there is butter, huuuuuuuuuum) 



![Constantin Chakhchukha bel osbana 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/Chakhchoukha-constantinoise-bel-osbana-2.jpg)
