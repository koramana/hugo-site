---
title: smoked salmon risotto
date: '2017-08-26'
categories:
- diverse cuisine
- Healthy cuisine
- rice
tags:
- dishes
- Italy
- Healthy cuisine
- Easy recipe
- Algeria
- Full Dish
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/risotto-au-saumon-fum%C3%A9-3.jpg
---
[ ![smoked salmon risotto 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/risotto-au-saumon-fum%C3%A9-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/risotto-au-saumon-fum%C3%A9-4.jpg>)

##  smoked salmon risotto 

Hello everybody, 

I realize that this is the first risotto recipe that I post on my blog, and yet I do the risotto at least 1 time per 15 groin .... Why this lack of publication of risotto, well by that the risotto at home and the express recipe when I do not know what to do and that there are only 15 minutes, so impossible to take this photo. 

Unlike today, I was alone because the children were out with their dad, and I remained to clean, yes! At noon, I began to be hungry, I did not know what to do, I opened the fridge, and there was this box already started smoked salmon, a box of mascarponea half completed that I had to consume today .... What was I going to do? well on a risotto. 

In addition, I had a little time to take some pictures of my delicious smoked salmon risotto, all good, all creamy, all tasty. If you like smoked salmon, this resotto is for you. 

My recipe is lemon based, can beings that will interest you to realize it and participate in our game: 

[ ![smoked salmon risotto 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/risotto-au-saumon-fum%C3%A9-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/risotto-au-saumon-fum%C3%A9-4.jpg>)   


**smoked salmon risotto**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/risotto-au-saumon-fum%C3%A9-5.jpg)

portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 1 onion, finely chopped 
  * 2 tbsp. tablespoon of olive oil 
  * 350 gr of rice for risotto, like Arborio 
  * 1 garlic clove, finely chopped 
  * 1 liter and ½ of vegetable broth 
  * 170 gr chopped smoked salmon (not filleted) 
  * 85 gr of mascarpone 
  * 3 c. chopped parsley 
  * the zest of 1 grated lemon, 
  * the juice of a lemon (or according to taste) 



**Realization steps**

  1. Fry the onion in the oil for 5 minutes. 
  2. Add the rice and garlic, then cook for 2 minutes, stirring continuously. 
  3. Pour one-third of the vegetable broth and cook the rice, stirring occasionally, until the broth is completely absorbed, 
  4. add half the rest of the broth and continue cooking, stirring a little more frequently, until it is absorbed again. 
  5. Pour remaining broth, mix and simmer until rice is cooked while stirring. 
  6. Remove the pot from the heat, add salmon, mascarpone, parsley and chopped lemon zest. 
  7. Add a little pepper from the mill on top, but not salt, because the salmon is quite salty. Let stand 5 minutes, add a little lemon juice according to your taste. 
  8. Serve the risotto garnished with the remaining smoked salmon, and enjoy! 



[ ![smoked salmon risotto 7](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/risotto-au-saumon-fum%C3%A9-7.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/risotto-au-saumon-fum%C3%A9-7.jpg>)
