---
title: lemon curd lemon cream pie
date: '2013-06-03'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- ramadan recipe
- fish and seafood recipes
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-au-lemon-curd-007.CR2_thumb1.jpg
---
##  lemon curd lemon cream pie 

Hello everybody, 

And here is the lemon curd lemon cream pie as prepared by my husband. When he saw that I had [ homemade lemon curd ](<https://www.amourdecuisine.fr/article-lemon-curd-creme-au-citron-fait-maison.html>) as soon as he saw the lemon curd, he quickly went to buy the commercial shortbread dough, to prepare his lemon curd pie as he used to do when he was single 

My husband is a big fan of the [ lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee.html>) , and it must be said that the pastry chef of the district where he grew up in Algeria, made good pastry. When he arrived here in England, he did not find a pastry chef who would satisfy his request, so he was having fun buying lemon curd, pie crusts, and here is his little lemon pie ... 

Ingredients: 

  * [ lemon curd ](<https://www.amourdecuisine.fr/article-lemon-curd-creme-au-citron-fait-maison-115475222.html>) homemade (depending on the size of the pie shell) 
  * [ homemade shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>)
  * almond for decoration. 



**lemon curd lemon cream pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-au-lemon-curd-007.CR2_thumb1.jpg)

portions:  6  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * lemon curd homemade (depending on the size of the pie shell) 
  * homemade shortbread 
  * almond for decoration. 



**Realization steps**

  1. prepare the shortbread, as you see on the video. 
  2. prepare the lemon curd 
  3. decorate the pie shell with lemon cream 
  4. you can decorate with almonds. 
  5. place in the fridge until serving time. 


