---
title: triangles of makrout rolled with dates
date: '2016-06-27'
categories:
- gateaux algeriens frit
- Algerian cakes- oriental- modern- fete aid
tags:
- Ramadan
- Algeria
- Algerian cakes
- Cakes
- delicacies
- Ramadan 2016
- Eid

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/triangle-de-makrout-roul%C3%A9-aux-dattes-1.jpg
---
![makrout triangle rolled with dates 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/triangle-de-makrout-roul%C3%A9-aux-dattes-1.jpg)

##  triangles of makrout rolled with dates 

Hello everybody, 

Eid el Fitr is fast approaching, and I'm sure there are some among you, who have already started making delicious Algerian cakes. Personally approaching Eid, I first start with [ dry cakes ](<https://www.amourdecuisine.fr/gateaux-secs-algeriens-petits-fours>) (click on the link) or [ ghribia and shortbread ](<https://www.amourdecuisine.fr/gateaux-sables-ghribia>) that these cakes can remain good and soft for at least 3 weeks, namely the climate and how to keep them. 

You can see the index [ easy and economical dry cakes ](<https://www.amourdecuisine.fr/article-gateau-algerien-facile-et-economique-pour-l-aid-117847224.html>) by clicking on the picture: 

[ ![cakes secs.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/gateaux-secs.bmp.jpg) ](<https://www.amourdecuisine.fr/article-gateau-algerien-facile-et-economique-pour-l-aid-117847224.html>)

Then I make the icing cakes, like the [ Arayeche ](<https://www.amourdecuisine.fr/article-arayeches-gateaux-algeriens.html>) , [ tcharek ](<https://www.amourdecuisine.fr/article-tcharek-glace-corne-de-gazelle-algerien-au-glacage.html>) and [ mkhabez ](<https://www.amourdecuisine.fr/article-mkhabez-aux-noix-gateau-algerien-en-video.html>) I generally prepare the cake and leave the frosting for the last two days before the help, so I'm sure I have a good frosting icing. You can click on this link: [ Algerian cakes with frosting ](<https://www.amourdecuisine.fr/gateaux-algeriens-au-glacage>) for more recipes. 

and when there are only 4 days left for the help, I make the cakes stuffed with almonds, or the [ honey cakes ](<https://www.amourdecuisine.fr/gateaux-algeriens-au-miel>) , the [ Makrout ](<https://www.amourdecuisine.fr/article-makrout-au-four-makroud-gateau-algerien-%d9%85%d9%82%d8%b1%d9%88%d8%b7-%d8%a7%d9%84%d9%83%d9%88%d8%b4%d8%a9.html>) and the [ baklawa ](<https://www.amourdecuisine.fr/article-baklawa-constantinoise.html>) . 

If I want to do [ fried Algerian cakes ](<https://www.amourdecuisine.fr/gateaux-algeriens-frits>) like the [ Griwech ](<https://www.amourdecuisine.fr/article-griwech-gateau-algerien-en-video.html>) for example, it's the last day that I do that, same thing for [ cakes without cooking ](<https://www.amourdecuisine.fr/gateaux-algeriens-sans-cuisson>) because we can not reserve these cakes any longer. 

The recipe in Arabic: 

These makrouts are rolled in sesame seeds, so here is a good idea for you, if you want to participate in our round: [ Recipes around an ingredient # 19: sesame seeds ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient19-grains-de-sesames.html>)

[ ![sesame seeds Game](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/les-grains-de-s%C3%A9sames-jeu-300x183.jpg) ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient19-grains-de-sesames.html>)

![makrout triangle rolled with dates 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/triangle-de-makrout-roul%C3%A9-aux-dattes-2-780x1024.jpg)

**triangles of makrout rolled with dates**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/triangle-de-makrout-roul%C3%A9-aux-dattes-3.jpg)

portions:  30  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** for the pasta: 

  * 3 measure of medium semolina 
  * 1 measure of melted butter and lukewarm 
  * 1 egg 
  * 1 pinch of salt 
  * Water + orange blossom water 

Prank call : 
  * paste of dates previously kneaded with oil and orange blossom water to soften it 

frying and deco: 
  * Oil for frying 
  * Honey 
  * Sesame seeds 



**Realization steps** Prepare the dough: 

  1. In a large jefna (large salad bowl), sand well the medium semolina with the butter between the hands, to make penetrate the fat in the semolina. 
  2. Break an egg into a container and whip it, then add the pinch of salt, water and orange blossom water; 
  3. Pour this mixture on the semolina by mixing gently with the tips of the fingers. 
  4. Mix without kneading the dough, until a dough.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/pate-de-makrout.jpg)
  5. Put a long piece of plastic film (cellophane) on your worktop. 
  6. Take a piece of semolina paste and form a rectangle on the plastic film. 
  7. Take a piece of date paste and spread it in a rectangle 1/2 cm thick but a little smaller than that of semolina and then place the rectangle of dates on the semolina rectangle. 
  8. Using the plastic film roll it on itself (as for a rolled cake) and solder it.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/fa%C3%A7onnage-de-makrout.jpg)
  9. Wrapping it well with the plastic film will help shape it while smoothing it. 
  10. Press all the way down to make it thinner while smoothing it with plastic film. 
  11. Then using both hands form a long triangular pudding, and turn it on each side, while giving it a triangular shape. 
  12. Smooth using the plastic film. 
  13. Cut out the long triangular pudding obtained in equal triangles (makrouts) and place them on a dish. 
  14. Continue until you are exhausted.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/makrout-triangles.jpg)
  15. Fry makrouts in very hot oil in a pan. As soon as they turn golden, remove and drain 
  16. dip the fried makrouts immediately in heated honey. 
  17. Allow them to soak up the honey before removing and roll the sides of the triangle into the sesame seeds. 



![makrout triangle rolled with dates](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/triangle-de-makrout-roul%C3%A9-aux-dattes.jpg)
