---
title: Charlotte aubergine
date: '2013-12-15'
categories:
- crepes, donuts, donuts, sweet waffles

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/charlotte-a-laubergine-007.CR2_.jpg
---
[ ![charlotte with eggplant 007.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/charlotte-a-laubergine-007.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/charlotte-a-laubergine-007.CR2_.jpg>)

##  Charlotte aubergine 

Hello everybody, 

Another dish that I saw on the Samira TV channel, this aubergine charlotte, or aubergine charlotte, is actually the name of the recipe to ring well in my ear, when the chef had said, and I leave everything, to see how it's going to be. 

I have to tell you, it's the same boss who realized [ tartlets with fig jam ](<https://www.amourdecuisine.fr/article-tarte-a-la-confiture-de-figues-samira-tv.html> "pie with fig jam / samira tv") that I already publish, a few days ago, sorry I do not know the name of this chef, but I know that she has a lot of book for sale in Algeria ... 

I come back to this delicious charlotte au aubergine, a super delicious dish, during its presentation, the chef had made this aubergine charlotte individual version, in circles pastry, but that I could not find mine, in the closet clutter, I did not bother to look in, and I use my gratin mold. 

the result was not bad, and the recipe is a real delight ... to remake absolutely.   


**Charlotte aubergine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/charlotte-a-laubergine-013.CR2_.jpg)

Recipe type:  dish  portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 2 medium sized aubergines 
  * 200 gr of minced meat. 
  * ½ onion. 
  * 2 cloves garlic 
  * chopped parsley 
  * 1 egg 
  * salt, black pepper. 
  * 2 pieces of bread, if not a slice of stale baguette. 
  * 2 tablespoons of milk. 
  * 2 fresh tomatoes. 
  * frying oil 



**Realization steps**

  1. Cut the aubergines into thin slices. 
  2. season with a little salt, and cover with a little flour. 
  3. fry the eggplant in a hot oil, but not until fully cooked, the eggplant should not be crisp, but rather flexible to cover the meat just now. 
  4. let drain on paper towels. 
  5. In a salad bowl, mix the minced meat, onion, garlic, chopped parsley and egg. 
  6. season with a little salt and black pepper. 
  7. dip the pieces of bread in the milk, and leave aside. 
  8. squeeze the bread a little from the milk and add it to the minced meat mixture. 
  9. take a baking tin, oil it a little. 
  10. cut the tomato into a slice and cover the entire surface of the mold. 
  11. add a little salt, black pepper. 
  12. place the eggplants all around the mold, and let a little overflow ... 
  13. put the minced meat inside, and cover with the upright eggplant.   
the meat should be well covered with eggplant. 
  14. place the mold in a preheated oven at 180 degrees and cook for 20 to 30 minutes. 
  15. take out of the oven and let cool a little before spilling the aubergine charlotte in a serving dish. 



[ ![charlotte with eggplant 016.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/charlotte-a-laubergine-016.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/charlotte-a-laubergine-016.CR2_.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
