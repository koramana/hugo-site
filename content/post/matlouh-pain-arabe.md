---
title: matlouh arabic bread
date: '2017-01-21'
categories:
- Mina el forn
- pain, pain traditionnel, galette
tags:
- Boulange
- Baker's yeast
- East
- Easy cooking
- Algeria
- Ramadan
- Kitchenaid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/matlouh-pain-arabe-%C3%A0-la-semoule-1.jpg
---
![matlouh, arabic bread with semolina 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/matlouh-pain-arabe-%C3%A0-la-semoule-1.jpg)

##  matlouh arabic bread 

Hello everybody, 

A beautiful Arabic bread matlouh, light and airy, a recipe from the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) . A traditional recipe that differs from one Algerian home to another. Today I share with you the version of matlouh 100% semolina Arabic bread. 

matlouh was always present on our table when we were young, my mother prepared it daily. What memory! when I came back from school, and at each passage in front of the houses that were drawing the way to my house, I felt the **invasive odor** which was emerging during the **cooking this delicious bread** , **lightweight** , **airy** and fragrant. Some add **seed of nigella** , others **grains of anise** , some, when it's almost cooked, and before removing the bread from **tajine** , brush a light layer of milk, huuuuuuuuuuuuuuuum! the delicious Arabic bread, the sublime **galette or kesra khmira** , our magnificent **matlouh, or matlou3** . 

and I also recommend the recipe [ homemade bread khobz eddar ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>)

**arabic bread - matlou - matlou3**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/matlouh-pain-arabe-%C3%A0-la-semoule.jpg)

portions:  2  Prep time:  40 mins  cooking:  10 mins  total:  50 mins 

**Ingredients** for 2 patties   
(on the video you have the ingredients of 3 patties) 

  * 450 gr of semolina (mix between fine and medium for me) 
  * 2 teaspoons of baker's yeast 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 tablespoon of milk 
  * 1 teaspoon of baking powder 
  * Butter 
  * 250 to 300 ml of water (depending on the absorption of the semolina) 



**Realization steps**

  1. place the semolina in the bowl of the kneader, add on top, the salt, the sugar, the two yeast and mix 
  2. introduce the warm water gently to have a nice ball. the dough should be a bit sticky. 
  3. remove the dough from the mess, and grease by hand for a few minutes on a buttered worktop. 
  4. put the dough back in the bowl of the mess, cover it, and let it rise. 
  5. after doubling in volume, take the dough again and divide it in half. 
  6. place them on a clean cloth covered with a little semolina. 
  7. cover his balls and let stand 15 to 20 minutes. 
  8. spread the balls in circles with the apple of your hands, and let it rest for another 30 minutes. 
  9. Cook on a clay tajine (traditional cooking on the tripod: tabouna) or on an uncrepiere, or new electric stone tajines. 



cooking on the electric tajine stone: 

{{< youtube Y3njqdX4BBs >}}  {{< youtube aWQjgw0cJtw >}} 

Bon appétit a vous. 
