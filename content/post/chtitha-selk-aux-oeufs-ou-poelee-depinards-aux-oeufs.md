---
title: chtitha selk with eggs or fried spinach with eggs
date: '2017-05-18'
categories:
- Algerian cuisine
- diverse cuisine
- Cuisine by country
tags:
- dishes
- Full Dish
- Easy cooking
- Healthy cuisine
- Ramadan
- Algeria
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/chtitha-selk-aux-oeufs-poelee-depinards-aux-oeufs-1.jpg
---
![chtitha selk with eggs, poached with spinach and eggs 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/chtitha-selk-aux-oeufs-poelee-depinards-aux-oeufs-1.jpg)

##  chtitha selk with eggs or fried spinach with eggs 

Hello everybody, 

With this chtitha selk with eggs, or fried spinach with eggs, you will be surprised that even the children will finish their plates! I like to call this magic dish when my kids ask me what I'm going to do at dinner. 

It's really magical that children eat their food without complaining or grumbling. And this beautiful pan fried spinach and egg is just a delight, a complete dish, easy to achieve. 

But here is an hour before the start of this little feast, we had plenty of rain, hihihihi, fortunately that where we had everything installed, was a little near the entrance of the building, so we could all evacuate inside are too much damage, hihihih. 

So everyone came back home with everything he had prepared hoping that tomorrow it will be more beautiful to remake this little feast, and enjoy a good barbecue before Ramadan. 

**chtitha selk with eggs or fried spinach with eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/chtitha-selk-aux-oeufs-poelee-depinards-aux-oeufs-3.jpg)

**Ingredients**

  * a nice bunch of spinach, or 450 gr of frozen spinach 
  * 4 cloves of garlic 
  * harissa, or dried pepper 
  * 2 fresh tomatoes, crushed 
  * 1 C. tomato paste 
  * 3 potatoes cubed and fries 
  * salt, black pepper, and olive oil 
  * eggs according to the number of people 



**Realization steps**

  1. cut the spinach in small quantities, and place them gradually in the pan with a bottom of oil. 
  2. cover and cook and continue cutting and adding spinach until all the amount is gone. 
  3. when the spinach is reduced, add the crushed garlic, salt and black pepper, 
  4. add over chilli, crushed tomato and tomato paste (diluted in a little water) 
  5. when the spinach is almost cooked, add the fried potato cubes (if necessary add a little water) 
  6. make then in the dish and break the eggs in each well. 
  7. season with salt and pepper, cover and cook on medium heat. 
  8. serve immediately with a good homemade bread. 



![chtitha selk with eggs, poached with spinach and eggs 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/chtitha-selk-aux-oeufs-poelee-depinards-aux-oeufs-2.jpg)
