---
title: chocolate fondant and speculoos
date: '2013-03-18'
categories:
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/Fondant-au-chocolat-et-brisures-de-speculoos-0011.jpg
---
![Fondant au chocolate-and-chips-from-spiced-001.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/Fondant-au-chocolat-et-brisures-de-speculoos-0011.jpg)

##  chocolate fondant and speculoos 

Hello everybody, 

So who wants this delicious chocolate fondant and speculoos? A recipe super simple to make but full of crispness and sweetness with these speculoosqui chips that adorn the chocolate fondant. 

**chocolate fondant and speculoos**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/Fondant-au-chocolat-et-brisures-de-speculoos1.jpg)

**Ingredients**

  * 200 g of chocolate 
  * 200 g of butter 
  * 160 g of sugar 
  * 4 eggs 
  * 100 g of maizena 
  * 150 gr of speculoos 
  * 2-3 tbsp. tablespoons [ speculoos paste ](<https://www.amourdecuisine.fr/article-la-pate-a-speculoos-maison-hommage-a-micky-63142494.html>)



**Realization steps**

  1. Preheat the oven to 150 ° C (thermostat 5). 
  2. Melt the chocolate with the butter. 
  3. Pour the mixture into a salad bowl, add the sugar and mix. 
  4. Let cool, then add the eggs and maizena.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/Fondant-au-chocolat-et-brisures-de-speculoos-11.jpg)
  5. Mix until smooth, without lumps. 
  6. Pour some dough into a 24 cm square baking pan lined with parchment paper. 
  7. Break some speculoos and put on top. Cover with the dough of melted speculoos evenly. 
  8. Break the rest of the speculoos (put some aside) mix them with the remaining dough. 
  9. Pour the dough into the mold, spread out well. And put on top, the remaining chips of speculoos. 
  10. Bake and cook for 25 to 30 minutes at 150 ° C (thermostat 5). 



![chocolate fondant and speculoos](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/Fondant-au-chocolat-et-speculoos1.jpg)
