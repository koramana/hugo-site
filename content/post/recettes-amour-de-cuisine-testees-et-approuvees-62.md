---
title: cooking love recipes tested and approved 62
date: '2015-03-24'
categories:
- les essais de mes lecteurs
tags:
- desserts
- Cakes
- Algeria
- Morocco
- dishes
- tajine
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-58.html/dyouls-de-kari-ben>)

Hello everybody, 

I wanted to fix myself to a day in which I publish this article of my recipes tested and approved by you my dear readers, I told myself if I can put this article once per year it will be good. But when I start to put on the article the photos of your recipes, I see it that fill up at once with your beautiful photos, and so that the poster article without problem is better not to exceed 20 photos on So, I have to follow the rhythm and posted your delights as I am at the right number of photos. 

It's really a pleasure to see, and put your photos on my blog, thank you, again and again, thank you a thousand times from the bottom of my heart to all those who do their best to send me and share with me the result of their pretty experience, you are very nice. For those who do not know, you can send me your photos either on email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

Do not deprive me of this happiness my friends, and the more you are many to try my recipes and to leave your comments, the more I have the will to make even more delights ... I continue with your encouragements. 

![chocolate shortbread hearts 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/coeur-au-chocolat-a.jpg)

[ Chocolate Short Cake ](<https://www.amourdecuisine.fr/article-sables-au-chocolat-gateaux-secs-2012.html> "Shortbread cookies, dry cakes 2014") at Exotic Flower (a moment of pleasure) 

[ ![Lemon rings at Ibtis SAm](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/anneaux-citronn%C3%A9s-chez-Ibtis-SAm-300x255.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/anneaux-citronnes-chez-ibtis-sam>)

[ lemon rings with almonds ](<https://www.amourdecuisine.fr/article-anneaux-citronnes-aux-amandes-effiles.html> "lemon rings with slender almonds") at Ibtis Sam 

[ ![makrout sniwa hafred](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa-hafred.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/makrout-sniwa-hafred>)

[ makrout sniwa ](<https://www.amourdecuisine.fr/article-makrout-sniwa-ou-makrout-facon-baklawa.html> "makrout sniwa or makrout way baklawa") at Hafred kitchen 

[ ![skandraniettes](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/skandraniettes.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/skandraniettes>)

[ Skandraniettes ](<https://www.amourdecuisine.fr/article-gateau-algerien-skandraniettes.html> "Algerian cake, Skandraniettes") at Hafred kitchen 

[ ![sweet with lemon nicole](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/moelleux-au-citron-nicole.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/moelleux-au-citron-nicole>)

[ In soft Citron ](<https://www.amourdecuisine.fr/article-moelleux-au-citron.html> "In soft Citron") at Nicole Gozzi 

[ ![strawberry at Rachid rach](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/fraisier-chez-Rachid-rach.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/fraisier-chez-rachid-rach>)

[ strawberries tree ](<https://www.amourdecuisine.fr/article-fraisier.html> "Strawberry plant") at Rachida Rach 

[ ![semolina tamina at wamani](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tamina-de-semoule-chez-wamani.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/tamina-de-semoule-chez-wamani>)

[ tamina ](<https://www.amourdecuisine.fr/article-tamina.html> "Tamina") at Wamani merou. 

[ ![dripping bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-degoulinant.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/pain-degoulinant>)

[ bread dripping with both cheese ](<https://www.amourdecuisine.fr/article-pain-degoulinant-aux-deux-fromages.html> "Bread dripping with both cheeses") at Audrey G. 

[ ![brioche with oil at souhila](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/brioche-a-lhuile-chez-souhila.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/brioche-a-lhuile-chez-souhila>)

[ Brioche with oil ](<https://www.amourdecuisine.fr/article-brioche-a-l-huile.html> "Brioche with oil") at Souhila A. 

[ ![makrout sniwa from souma London](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa-de-souma-London.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/makrout-sniwa-de-souma-london>)

[ makrout sniwa ](<https://www.amourdecuisine.fr/article-makrout-sniwa-ou-makrout-facon-baklawa.html> "makrout sniwa or makrout way baklawa") at Souma London 

[ ![velvety mushroom rachida](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/velout%C3%A9-de-champignon-rachida.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/veloute-de-champignon-rachida>)

[ Cream of mushroom soup ](<https://www.amourdecuisine.fr/article-creme-de-champignons-soupe-de-champignons-113278343.html> "mushroom soup / mushroom soup") at Rachida Rach 

[ ![carrot cake at Felur dz's](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-au-carotte-chez-Felur-dz.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/gateau-au-carotte-chez-felur-dz>)

[ carrot cake ](<https://www.amourdecuisine.fr/article-gateau-aux-carottes.html> "carrot cake") at flower Dz 

[ ![Minced meat bread from Naila](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-a-la-viande-hach%C3%A9e-de-Naila.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/pain-a-la-viande-hachee-de-naila>)

[ breads stuffed with minced meat ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee.html> "buns with chopped meat") at Arwa N. 

[ ![pita bread at Naila's](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-pita-chez-Naila.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/pain-pita-chez-naila>)

[ Pita bread ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html> "Lebanese bread, pita bread video") at Arwa N 

[ ![tuna rolls at Naila's](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/roul%C3%A9s-au-thon-chez-Naila.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-62.html/roules-au-thon-chez-naila>)

[ Tuna rolls ](<https://www.amourdecuisine.fr/article-les-roules-au-thon.html> "tuna rolls") chez Arwa N 
