---
title: fruit paste with orange
date: '2017-02-05'
categories:
- confitures et pate a tartiner
- dessert, crumbles et barres
- idee, recette de fete, aperitif apero dinatoire
- recettes sucrees
tags:
- Valentine's Day

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/pate-a-fruit-a-lorange-013.CR2_-682x1024.jpg
---
![fruit paste with orange](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/pate-a-fruit-a-lorange-013.CR2_-682x1024.jpg)

##  fruit paste with orange 

Hello everybody, 

**fruit paste with orange** : I really like fruit pasta, but sometimes I'm too lazy to realize it, because I have to stay up, in front of my gas stove to stir, and stir ... stir again ... and I'm not telling you what it is it's boring to do it, especially when it starts to splash here and there, and burn me ... 

So last time I asked my friend Lunetoiles to send me a little Vitpris, with which the cooking of pasta fruit, takes a much shorter time and she sent me a nice package ... great, So it's up to me to take advantage of it. 

For a start, I preferred to start with a fruit of season, and I gait orange fruit paste ... So for this fruit paste has orange, I used oranges well acid that nobody did not want to eat at home, and here is the magic ... a dough of fruit which left quickly fast with the children .. 

But if you do not have Vitpris, you can still make the fruit paste, without this ingredient, but as I said at the beginning ... it will take you 20 to 25 minutes to take 5 minutes with the Vitpris ... .   


**fruit paste with oranges**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/pate-a-fruit-a-lorange-009.CR2_-1024x658.jpg)

Recipe type:  dessert  portions:  6  Prep time:  10 mins  cooking:  5 mins  total:  15 mins 

**Ingredients**

  * 180 gr of peeled oranges 
  * 220 gr of sugar 
  * 1 bag of Alsa vitpris 



**Realization steps**

  1. Prepare the oranges, peel them, cut into pieces and pass them to the mixer to have a homogeneous mash that weighs 180 gr. 
  2. Put your fruits in a saucepan. and add the sugar. 
  3. Take 2 tablespoons that you mix with 1 sachet of Vitpris and sprinkle this mixture on your fruit. Mix everything. 
  4. Add the remaining sugar, mix again and place the pan on a low heat while stirring. Once the sugar is completely melted, bring to a boil, cook for 5 minutes without stirring. 
  5. Once cooked, pour the preparation in a mold preferably rectangular, previously covered with parchment paper, if not made like, pour into silicone molds, I opted for mini-financial molds. 
  6. Let the fruit paste cool down at room temperature for a few hours. 
  7. Once cooled, cut the fruit paste into squares and coat with crystallized sugar 



Orange fruit paste 

Ingredients 

  * 180 gr of peeled oranges 
  * 220 grams of sugar 
  * 1 packet of Alsa vitpris, or Arrow root 



preparation 

  1. peel the oranges, cut them into pieces and place them in a blender and pur ... 
  2. For the pure fruit in a saucepan and add the sugar. 
  3. Take off 2 tablespoons of sugar and mix it with Vitpris 
  4. sprinkle this mixture over the fruit and stir. 
  5. place the pan on a low heat, do not stop stirring. Once in a while, cook for 5 minutes, stirring constantly. 
  6. Once cooked, for the mixture into a rectangular pan, covered with parchment paper, or in a siliconepan, 
  7. let it cool at room temperature for few hours. 
  8. Ounce fully cooled, cut the paste on squares and coat on granulated sugar 



[ ![fruit paste with orange](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/pate-a-fruit-a-lorange-004.CR2_-671x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/pate-a-fruit-a-lorange-004.CR2_.jpg>)

J’espere que vous allez aimer cette réalisation, je vous dis merci, et a la prochaine recette. 
