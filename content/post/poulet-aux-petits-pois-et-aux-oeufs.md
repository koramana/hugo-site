---
title: chicken with peas and eggs
date: '2012-05-06'
categories:
- ramadan recipe

---
hello everyone a very delicious chicken tagine with peas and eggs, which I prepared for the dinner of yesterday, and that we really like, a recipe of the Algerian cuisine, a very simple recipe and easy. you can also see: tajine jelbana, tajine peas with artichokes. tagine peas with cardoon. velvety peas. ingredients: 4 to 5 chicken legs 750 gr frozen peas (you can use fresh peas) 4 to 5 eggs 1 tbsp canned tomato 3 tbsp. 

##  Overview of tests 

####  please vote 

**User Rating:** 2.9  (  9  ratings)  0 

Hello everybody 

a very delicious **chicken tajine with peas and eggs** , that I prepared for the dinner of yesterday, and that we really liked, a recipe of the [ Algerian cuisine  ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , a very simple and easy recipe. 

you can also see: 

[ tajine jelbana, tajine of peas with artichokes ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-aux-artichauts---tajine-jelbana-103530292.html>) . 

[ tagine peas with a cardoon ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee-94791294.html>) . 

[ velvety peas ](<https://www.amourdecuisine.fr/article-41284308.html>) . 

Ingredients: 

  * 4 to 5 chicken legs 
  * 750 gr of frozen peas (you can use fresh peas) 
  * 4 to 5 eggs 
  * 1 tablespoon of canned tomato 
  * 3 tablespoons of oil 
  * 2 to 3 carrots 
  * 1/2 onion 
  * 1 clove of garlic 
  * salt and black pepper 
  * 1/2 teaspoon of coriander / garlic powder mixture 
  * 1/2 liter of water 



**Preparation** : 

  * In a pot, fry the chickens, with a little oil, onion and garlic. 
  * add carrots cleaned and sliced 
  * Add spices and salt and simmer for 10 minutes. 
  * Add the tomato and cover with water 
  * when the mixture starts to boil introduce the peas 
  * cook until the sauce is completely reduced. 
  * After cooking, remove the chicken pieces. 
  * Put the mixture in a baking dish. 
  * break the eggs over, and bake for 15 minutes in an oven preheated to 150 degrees C. 
  * you can roast the chicken in the oven, or pass for a moment in a hot oil bath 
  * Serve with chicken pieces. 


