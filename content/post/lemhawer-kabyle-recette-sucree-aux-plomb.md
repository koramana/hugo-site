---
title: lemhawer kabyle, sweet recipe with lead
date: '2015-01-12'
categories:
- Algerian cuisine
- ramadan recipe
tags:
- Vegetarian cuisine
- Sustainable cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/lmhawer-kabyle-001.jpg
---
[ ![lemhawer kabyle](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/lmhawer-kabyle-001.jpg) ](<https://www.amourdecuisine.fr/article-lemhawer-kabyle-recette-sucree-aux-plomb.html/lmhawer-kabyle-001>)

##  lemhawer kabyle, sweet recipe with lead 

Hello everybody, 

And here's a recipe coming out of the old drawers of Kabyle cuisine, a recipe that my mother often prepared for us, when she did not have the time, or did not know what to make us eat, lemhawer kabyle, a sweet recipe for the little ones lead or berkoukes, rich in fiber and protein that we liked a lot. 

In our Kabyle ancestors, and especially in the family of my mother Larbaa Nath Irathen, this recipe was presented at festivals, births, circumcisions, mawlid nabawi sharif, especially especially the Berber New Year. I take this opportunity to wish you a happy new year: 

" ** Assegas amegaz a lahvav, Yennayer thamervouhth, aradyawin assaed ad lahna, delfouruh Incha Allah  ** "   


**lemhawer kabyle, sweet recipe with lead**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/lamhawer-kabyle-de-ma-mere.jpg)

portions:  2-3  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 2 glasses of fine berkoukes (my mother rolls it herself, it is thinner than the normal berkoukes) 
  * 2 hard boiled eggs 
  * sugar according to taste 
  * 1 handful of white beans cooked in salted water with a spoonful of oil.   
[ ![Lamhawer Kabyle of my mother](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pour-le-mhawer-kabyle-290x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pour-le-mhawer-kabyle-290x300.jpg>)
  * 3 to 4 tablespoons of olive oil from bled (not extra virgin, lol) 



**Realization steps**

  1. oil the little lead a little 
  2. steam through the top of the couscous pot for 20 to 25 minutes 
  3. remove from heat, place in a bowl and moisten with a good amount of water, add the equivalent of half a tablespoon of salt or less depending on the quality of berkoukes. 
  4. let it rest a little, then put again to steam,   
[ ![Lamhawer Kabyle of my mother](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/plombs-pass%C3%A9s-a-la-vapeur-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/plombs-pass%C3%A9s-a-la-vapeur-300x225.jpg>)
  5. repeat the process until you have cooked and tender lead in the mouth. 
  6. remove from heat, put in a bowl, sprinkle well with olive oil. 
  7. add the white beans 
  8. add the powdered sugar according to your taste 
  9. before serving, decorate with quarters of boiled eggs. 



[ ![Kabyle Lamhawer of Kabyle Kitchen](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/lmhawer-kabyle-cuisine-kabyle.jpg) ](<https://www.amourdecuisine.fr/article-lemhawer-kabyle-recette-sucree-aux-plomb.html/lmhawer-kabyle-cuisine-kabyle>)
