---
title: Tiramisu with speculoos and chicory
date: '2015-03-08'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Dessert
- Bavarian

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tiramisu-aux-speculoos-4.jpg
---
![tiramisu-to-spiced-4](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tiramisu-aux-speculoos-4.jpg)

##  Tiramisu with speculoos and chicory 

Hello everybody, 

A sublime tiramisu with speculoos and liquid chicory made by our dear Lunetoiles .... The catch in the story is that this recipe is more than 2 years old, hihihihi ... Lunetoiles sent me the recipe and photos to me from May 2011 ... and I had completely forgotten 

A recipe that deserved to be published immediately, but was lost in my archives, fortunately that sometimes I do not just the big cleaning of the house, and the pc, I also do the big cleaning of my email box, hihihihi 

[ ![tiramisu-to-spiced](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tiramisu-aux-speculoos.jpg) ](<https://www.amourdecuisine.fr/article-tiramisu-aux-speculoos-et-chicore.html/tiramisu-aux-speculoos-5>)

**Tiramisu with speculoos and chicory**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/tiramisu-aux-speculoos-1.jpg)

portions:  8  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients** For the foam: 

  * 400 g of mascarpone 
  * 4 eggs 
  * 100 g of sugar 
  * 4 sheets of gelatin 
  * 2 tbsp. liquid chicory 
  * 400 g of speculoos 
  * 70 g of butter 



**Realization steps**

  1. Reduce to powder 150 g of speculoos. 
  2. Add 70 g melted butter and mix well. 
  3. Place a 24 cm (or 8 cm diameter) circle of entremet (or use a hinged mold) on a baking sheet lined with parchment paper (or a soft mat). 
  4. Place a strip of rhodoid on the walls. 
  5. Pour the mixture into the speculoos and strain well with the back of a tablespoon. Place in the fridge for 30 minutes. 
  6. Separate the egg yolks from the whites. Beat the egg yolks with the sugar until the mixture whitens then add the mascarpone and chicory and whisk another 2 minutes. 
  7. Dissolve the gelatin (previously softened in cold water for a few minutes) in the microwave in 2 tablespoons water, mix it with a little preparation of mascarpone and add it to the rest of the cream. Then gently add snow-whites. 
  8. Pour ⅓ of the foam, deposit a layer of speculos leaving 1 cm of free foam on the edge. 
  9. Cover again with 1/3 of the foam, then the biscuits again and the remaining foam. 
  10. Mix the remaining biscuits and cover all the mousse. 
  11. Place in the freezer for 4 hours. 
  12. Turn the tiramisu on a serving dish, decorate it with chocolate. 



![tiramisu-to-spiced-3.jpg](http://img.over-blog.com/555x422/2/42/48/75/API/2013-03/06/tiramisu-aux-speculoos-3.jpg)
