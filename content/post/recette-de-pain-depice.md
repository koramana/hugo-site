---
title: gingerbread recipe
date: '2012-11-01'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya
tags:
- Honey
- Holidays
- To taste
- Algerian cakes
- Cakes
- Cake
- la France
- Christmas cake
- Gingerbread
- Spice
- Bread
- Christmas

---
#  fluffy gingerbread, honey gingerbread 

the recipe for mercotte is a real treat, I did not have all the ingredients, but I try not to get too far from its recipe. 

so I'm giving you this delicious recipe, and I'm going to put my little changes right in front (it's not that I wanted to change the ingredients, but that's what I had at home) 

  * 135g of  ** milk  ** half skim 
  * 1  ** clove  ** , 
  * 90g of butter, 
  * 80g of  ** brown sugar  ** , (brown sugar for me,  ** Demerara  ** ) 
  * 110g of liquid honey and if possible of fir, (honey of  ** eucalyptus  ** for me ) 
  * 2  ** small eggs  ** 35g, 
  * 20g of candied orange peel or  ** candied orange  ** , 
  * the zest of a lemon. 
  * 105g of flour  ** T45  ** , (Normal flour, or  ** plain flour  ** here in  ** England  ** ) 
  * 60g of flour  ** buckwheat  ** , (  ** Brown flour  ** ) 
  * 1g of fleur de sel (normal salt for me) 
  * 3g of  ** baking soda  ** be 1cc, 
  * 1g of ginger powder, 
  * 2g cinnamon powder, 
  * 2g of gingerbread spices, 
  * 1g of licorice powder, (I did not have one, and I do not know) 
  * 18g of  ** molasses powder  ** , 
  * 20g of ginger confit, (I think that I have abused on this side, 50 grs for me, so I like the taste) 
  * 30g of flaked almonds. (I had not put) 



Mix the powders -fins, bicarbonate, spices,  ** muscovado  ** \- with candied orange zest and fleur de sel. Add them to the previous mixture and add the ginger in thin slices, the almonds and the lukewarm milk. 

Preheat the oven to 170 °, butter and flour the mold. Add the dough to 2/3. Let stand 10min, then bake 35min [to check as always according to your oven]. 

look at me it's delicious pieces of candied ginger 

une vrai reussite, merci beaucoup mercotte, ta recette un vrai bonheur pour moi. 
