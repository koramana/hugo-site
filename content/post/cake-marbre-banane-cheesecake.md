---
title: marbled cake banana cheesecake
date: '2017-10-07'
categories:
- cheesecakes and tiramisus
- cakes and cakes
- sweet recipes
tags:
- Breakfast
- To taste
- Algeria
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/10/cake-marbr%C3%A9-banane-cheesecake.jpg
---
![marbled cake banana cheesecake](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/cake-marbr%C3%A9-banane-cheesecake.jpg)

marbled cake banana cheesecake 

Hello everybody, 

This is the second time I publish this banana cheesecake marbled cake, I made the cake the first time for our recipe game around an ingredient, I called it [ banana-cheese cake ](<https://www.amourdecuisine.fr/article-banane-cheese-cake.html>) I admit that I was not satisfied that day with the result, but my children liked it very much. This gave me the opportunity to do it again and again since, until the desired result. 

This marbled banana cheesecake cake is so mellow, melting in the mouth and tasty that it will not last long. Besides, it's difficult for me to make photos of this cake, so small hands around me are quickly stretched to take the first cut piece, as soon as I turn my back! 

**marbled cake banana cheesecake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/cake-marbr%C3%A9-banane-cheesecake-2.jpg)

**Ingredients** For the ingredients of the banana cake layer: 

  * 2 ripe bananas. 
  * 1 egg 
  * 110 gr of brown sugar 
  * 90 gr of melted butter 
  * 2 tbsp. vanilla sugar 
  * 1 C. coffee baking soda 
  * 1 pinch of salt 
  * 220 gr of flour 

For the cheesecake layer: 
  * 170 gr Philadelphia full (full fat) 
  * 60 gr of sugar 
  * 1 egg 
  * ½ c. vanilla 
  * 3 c. flour 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. for this recipe I use a silicone cake mold, but if you use another mold, wrap with baking paper, or butter well and flour the mold. 

layer of banana cake: 
  1. In a large bowl, mix mashed bananas with butter, sugar, eouf, vanilla, baking soda and salt. 
  2. Stir until combined and add flour, stirring until incorporated. Set it aside. 

layer of cheesecake 
  1. In another bowl, beat cream cheese and sugar until smooth. 
  2. Add egg and vanilla and mix until smooth, 
  3. Finally, add the flour and mix until combined. 

mounting 
  1. Spread half of the dough of the banana cake in the bottom of your mold. 
  2. Thoroughly and evenly spread the cheesecake layer over the dough. 
  3. Cover with remaining layer of banana cake. 
  4. Bake for 40 to 45 minutes, or depending on your oven (it may take 1 hour, especially for baking the top layer) 
  5. If the cake browns too quickly before cooking, cover the top with aluminum foil. 
  6. Let the cake cool for 20 to 30 minutes before removing from the mold. Let cool before serving. 



![marble cake banana cheesecake 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/cake-marbr%C3%A9-banane-cheesecake-1.jpg)
