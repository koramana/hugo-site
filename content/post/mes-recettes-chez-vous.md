---
title: my recipes at home
date: '2010-11-20'
categories:
- couscous
- Algerian cuisine
- recettes de feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/11/56804809_p1.jpg
---
[ Orange cake / chocolate ](<https://www.amourdecuisine.fr/article-cake-marbre-chocolat-orange-46108134.html>) at Nacy's (a faithful reader) 

[ Griwech ](<https://www.amourdecuisine.fr/article-mon-griwech-un-delicieux-gateau-algerien-56649504.html>) at Papillon bleu (a faithful reader) 

[ lightning ](<https://www.amourdecuisine.fr/article-39444151.html>) always blue butterfly 

[ Larayech with honey ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-60064269.html>) from Mimi (a faithful reader) 

[ cake with orange ](<https://www.amourdecuisine.fr/article-25345557.html>) at Celeste (a faithful reader) 

[ basboussa with breadcrumbs ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html>) at del della (a faithful reader) 

photo taken with the webcam 

in any case I am sure she is very good 

[ Griwech ](<https://www.amourdecuisine.fr/article-mon-griwech-un-delicieux-gateau-algerien-56649504.html>) from Meriem (a faithful reader) 

![](https://www.amourdecuisine.fr//import/http://2.bp.blogspot.com/_auPnLDIULZU/TIULv1d3UVI/AAAAAAAAAMA/2rGb2U66s1Y/s640/)

[ Chinese ](<https://www.amourdecuisine.fr/article-27039105.html>) at Faten a faithful reader 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/56804809_p1.jpg)

and if you also try one of my recipes, it will make me very happy to share this with my readers, so do not hesitate to send me a picture on my email: 

[ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

kisses 
