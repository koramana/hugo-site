---
title: Algerian Flag cake recipe, Lucky Cake
date: '2010-06-18'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, chaussons

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/06/101_0483_thumb1.jpg
---
& Nbsp; the world cup looks disastrous this year, because all the teams showed themselves still below our expectations. Finally I am not at all "foot moud" but a small delight for the eyes ok, because no one will eat this delicious cake, not even me that I go back to you an old recipe of one of my very faithful reader. the Algerian national team is playing today against the English team, my native country against my country of residence, hihihihih I do not know for whom to be happy nor for whom to be "sad" like & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.58  (  3  ratings)  0 

[ ![101_0483](https://www.amourdecuisine.fr/wp-content/uploads/2010/06/101_0483_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-42436429.html>)

the world cup looks disastrous this year, because all the teams showed themselves still below our expectations. 

Finally I'm not at all "foot moud" but a small delight for the eyes ok, because no one will eat this delicious cake, not even me that I go back to you an old recipe from one of my very faithful reader. 

the Algerian national team plays today against the English team, my native country against my country of residence, hihihihih 

I do not know who to be happy or for whom to be sad, as the English say. 

I add this paragraph, after the 0-0 equality: 

This cake is a lucky cake, an idea to bloom my mind, if this photo goes around the blogs supporters of the Algerian team, maybe we will win in front of America, and England makes a defeat or equality before slovenia, and we will be qualified !!!!! ????? 

So, I'm giving you one of Neo Amouna's recipes, 

the Algerian flag 

the dough or the sponge cake: 

  * 4oeuf 
  * 100g of sugar 
  * 5 cups of oil 
  * 120 gr of flour 
  * 1/2 teaspoon baking powder 



beat the sugar and eggs well, then add the oil, beat again, then add the flour mixture baking powder, and mix with a wooden spoon. 

butter and flour a mold a round, pour the mixture, and place in an oven over medium heat for almost a quarter of an hour. 

after cooling, divide the sponge cake in half, and sprinkle the two halves with a syrup, and leave aside. 

prepare the whipped cream: 

first method: 

with fresh cream: 

  * 200 ml of very cold liquid cream very cold 
  * 2 tablespoons of sugar 
  * a little vanilla 



at least half an hour before preparing your whipped cream, pour it into a salad bowl with the beater whisk, which you will use in the refrigerator. 

when everything is cold, remove them from the refrigerator, remove them, beat the cream. 

your cream is transformed, and your drummer forms furrows, after 5 minutes, the cream attaches to the whip of the drummer. 

2nd method, or the method of Neo: 

she uses cream powder whipped cream that sells in Algeria, she jousted this powder with cold milk (quantity indicated on the bag) she whips until she has a nice cream, she adds to it so that the cream does not fall not, 4 tbsp. 1 tablespoon butter. soup of honey or icing sugar, according to your taste. 

for garnish 

use jelly to which you add honey for a better taste 

take the first disc of sponge cake, cover with whipped cream, and cover with the other disc, garnish once more with whipped cream, 

take the jelly to which you will add green dye, decorate half the cake with this jelly, 

take a little more jelly with some red dye, and form the star and crescent. 

and then garnish the cake with the fruit you have. 

[ ![neo2](https://www.amourdecuisine.fr/wp-content/uploads/2010/06/neo2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-42436429.html>)
