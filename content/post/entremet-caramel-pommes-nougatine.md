---
title: Entremet Caramel apples nougatine
date: '2016-07-19'
categories:
- bavarois, mousses, charlottes, recette l'agar agar

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/02/entremet-Caramel-pommes-nougatine1.jpg
---
![dessert-Caramel-apple-nougatine.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/entremet-Caramel-pommes-nougatine1.jpg)

##  Entremet Caramel apples nougatine 

Hello everybody, Here again is a sublime recipe, shared with us **Lunetoiles.** Fortunately this delight is not near me, especially since I try to lose some weight. Finally I'm not telling you that I'm on a diet, because I haii all diets, I do not want to torture me any more, as I do not want to eat anything anymore. That's why recently I'm a little far from my kitchen, especially that I'm on vacation in Algeria, and that we make the family visits, and the trouble of connection here. Ingredients:   
Nougatine circles 

  * 175 g sugar 
  * 5 ml of water 
  * 100 g of flaked almonds 



Prepare all the ingredients. Put in a thick-bottomed pan, sugar with water   
Cook until you get a caramel blond on medium heat. Then add the flaked almonds.   
Mix well to coat the caramel almonds . Pour the nougatine on a sheet of baking paper.   
Cover quickly with a second sheet of parchment paper, then using a rolling pin, spread well, and quite finely, the nougatine. Let it cool completely.   
When it is cold, break the nougatine into small pieces. Mix nougatine finely   
Put a baking paper on a plate, a circle of 18 to 20 cm.   
Make a circle with half of the nougatine powder and cook at 160 ° for 5 to 8 mm (walleye). Let cool completely. Do the same with the other half of the powdered nougatine.   
Let cool and reserve. 

![dessert caramel apple-nougatine.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/entremet-caramel-pomme-nougatine1.jpg)

Carambar Tiles: Preheat oven to 190 ° C (tea 6).   
Take some carambars. 1 carambar = 2 chips   
Cut them in half.   
Place them on a baking sheet covered with a sheet of parchment paper.   
Bake for about 5 minutes, watch as it happens very quickly!   
From the oven exit take the parchment paper and place it on a rolling pin so that the caramel take the rounded shape of the roll. Leave thus until total cooling! Then take off very gently because they break easily. ![dessert caramel-apple-nougat-3.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/entremet-caramel-pommes-nougatine-31.jpg) Biscuit recipe joconde chocolate print   
For the biscuit decor: 

  * 35 gr of butter 
  * 25 gr of sugar 
  * 3 eggs + 3 whites 
  * 35 gr of flour 
  * 10 gr of cocoa 
  * 150 gr of icing sugar 
  * 150 gr of ground almonds 

Prepare the biscuit decor: mix 15 gr of butter and 15 gr of sugar. Add 1/2 egg white and 10 gr sifted flour with 10 g cocoa. Using a 1.5 mm pastry bag, draw arabesque on a baking tray lined with parchment paper. Put the plate in the freezer. Meanwhile melt 20g butter and allow to cool. Preheat oven to 210 ° C (th.7). Mix the icing sugar, the almond powder and 25 gr of flour. Add 3 eggs and mix. Beat 2 whites in snow with 10 gr of sugar and stir in the mixture. Add the melted butter. Pour on the frozen arabesques. Put immediately in the oven for 8 minutes at 210 ° C (th.7). Unmould after cooling. ![Dessert caramel-apple-nougat-5.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/Entremet-caramel-pommes-nougatine-51.jpg) For the chocolate biscuit: 

  * 3 eggs 
  * 50 gr of sugar 
  * 30 gr of chocolate 
  * 30 gr of butter 
  * 20 gr of cornflour 
  * 30 gr of flour 



Prepare the chocolate biscuit: beat the yolks and half of the sugar until the mixture whitens and doubles in volume. Add the melted chocolate with the butter and mix. Beat the egg whites with the rest of the sugar and gently add them with a spatula to the previous mixture. Sift the flour and cornflour, and add them to the mixture. Pour the paste into a silicone mold or a 20 cm circle placed on baking paper. Put in the oven for 30 minutes at 180 ° C (th.6). Unmount after 10 minutes. Let cool. 

Dress a circle with a 24 cm diameter rhodoïd and place on a dish. Spread strips of biscuits decor 6 cm high. Place the chocolate biscuit in the center. 

![dessert caramel-apple-nougat-6.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/entremet-caramel-pommes-nougatine-61.jpg)

Bed of caramelized apples: 

  * 3 gala royal apples 
  * 75g of powdered sugar 
  * 60g of butter 
  * Peel and cut each apple into cubes. 

Bake the sugar until you get a caramel . Decant by adding the butter in small cubes.   
Out of the fire mix vigorously until the butter is incorporated perfectly.   
You have then a caramel flowing but thick. Put in a dish the apples and top with caramel . Cover with aluminum and cook at 180 ° for 25 to 30 minutes.   
Let cool. Drain the apples, but keep the caramel With a brush, soak the biscuit decor and the chocolate biscuit with the caramelized apples.   
Lay the bed of caramelized apples on the chocolate biscuit, and place a circle of Nougatine. ![dessert caramel-apple-nougat-7.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/entremet-caramel-pommes-nougatine-71.jpg) Vanilla cream: 

  * 100 gr of milk 1 egg yolk 
  * 1/2 vanilla pod 25g sugar 
  * 1/2 sheet   
gelatin 125g cream 
  * 7 g cornflour 



Heat the milk with the scraped vanilla bean and let it infuse for about 30 minutes. Covers a plate.   
Blanch the yolk with the sugar, add the cornflour and pour the hot milk. Mix well.   
Pour back into pan and cook while whisking until thickened. Boil, remove the vanilla bean and cook another 2 mm cream   
Soften the gelatin in cold water, squeeze it and add it to the cream. Let cool .   
Pour the custard cream into a salad bowl.   
Whip the cream with whipped cream. Stir in 2 tablespoons to loosen the pastry cream and add the rest gently with a spatula. 

Pour the vanilla cream on the nougatine disc and place over the second Nougatine disc. Reserve cool. 

![dessert caramel-apple-nougat-8.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/entremet-caramel-pommes-nougatine-81.jpg)

Caramel mousse: 

  * 125 g of sugar, 
  * 100 g of liquid cream, 
  * 3 sheets of gelatin, 
  * 1/2 liter of whipped cream. 



Prepare all the ingredients.   
Put the gelatin to soak in cold water.   
Make a caramel with the sugar and a little water.   
Cook until you get a blonde color. (If the caramel is not cooked enough, the taste will not be pronounced enough).   
Immediately decant with the cream (heated) while stirring with a wooden spoon, taking care not to burn yourself.   
Pour in a bowl and add the gelatin well éssorée. Let cool and stir occasionally.   
Now put your cream whipped cream, it must be frothy.   
Mix your whipped cream with the caramel maker to obtain a homogeneous mousse.   
Spread the caramel mousse on the Nougatine disc.   
Smooth the whole.   
Leave to cool and then toss with apples. 

Apple pepper: 

  * 6 apples 
  * Butter 
  * 2 tbsp. tablespoons sugar 
  * 2 bags of pie glaze 
  * 8 tbsp. tablespoons sugar 
  * 200 ml of apple juice 

Peel and cut the apples in 8. Heat the butter in a frying pan and brown the quarters on each side. At the end of cooking, sprinkle with sugar (2 tablespoons) and let caramelise. Let cool on a plate. Place the apples in a rosette on the caramel mousse. Make the topping: put in a saucepan the apple juice, 8 tbsp. tablespoons sugar, and 2 bags of topping, heat while stirring. In the first few bouillons, remove from heat and let cool for 3 minutes, stirring occasionally. With a brush, cover the apples with this glaze. Let take 1 hour in the fridge.Décercler your entremet. Remove the rhodoid tape.   
Decorate with carambar tiles. ![dessert-apple-nougat-9.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/entremet-pommes-nougatine-91.jpg) waw !!! you have finished reading ??? you have to have the patience of Lunetoiles to make an entremet parreil is not it? in any case, I enjoyed myself just with reading hat beats Lunetoiles for this wonder thank you for your comments and good day 

**Entremet Caramel apples nougatine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/entremet-caramel-pommes-nougatine-6.jpg)

**Ingredients** Circle of nougatine 

  * 175 g sugar 
  * 5 ml of water 
  * 100 g of flaked almonds 

Tiles in Carambar: 
  * Some carambars 

the biscuit recipe joconde chocolate print 
  * For the biscuit decor: 
  * 35 gr of butter 
  * 25 gr of sugar 
  * 3 eggs + 3 whites 
  * 35 gr of flour 
  * 10 gr of cocoa 
  * 150 gr of icing sugar 
  * 150 gr of ground almonds 

For the chocolate biscuit: 
  * 3 eggs 
  * 50 gr of sugar 
  * 30 gr of chocolate 
  * 30 gr of butter 
  * 20 gr of cornflour 
  * 30 gr of flour 

Bed of caramelized apples: 
  * 3 gala royal apples 
  * 75g of powdered sugar 
  * 60g of butter 

Vanilla cream: 
  * 100 gr of milk 1 egg yolk 
  * ½ vanilla pod 25g sugar 
  * ½ sheet 
  * gelatin 125g cream 
  * 7 g cornflour 

Caramel mousse: 
  * 125 g of sugar, 
  * 100 g of liquid cream, 
  * 3 sheets of gelatin, 
  * ½ liter of whipped cream. 

Apple pepper: 
  * 6 apples 
  * Butter 
  * 2 tbsp. tablespoons sugar 
  * 2 bags of pie glaze 
  * 8 tbsp. tablespoons sugar 
  * 200 ml of apple juice 



**Realization steps** prepare the nougatine circle 

  1. Prepare all the ingredients. Put in a thick-bottomed pan, sugar with water 
  2. Cook until you get a toffee on medium heat. Then add the flaked almonds. 
  3. Mix well to coat the caramel almonds. Pour the nougatine on a sheet of baking paper. 
  4. Cover quickly with a second sheet of parchment paper, then using a rolling pin, spread well, and quite finely, the nougatine. Let it cool completely. 
  5. When it is cold, break the nougatine into small pieces. Mix nougatine finely 
  6. Put a baking paper on a plate, a circle of 18 to 20 cm. 
  7. Make a circle with half of the nougatine powder and cook at 160 ° for 5 to 8 mm (walleye). Let cool completely. Do the same with the other half of the powdered nougatine. 
  8. Let cool and reserve. 

Carambar Tiles: Preheat oven to 190 ° C (tea 6). 
  1. Take some carambars. 1 carambar = 2 chips 
  2. Cut them in half. 
  3. Place them on a baking sheet covered with a sheet of parchment paper. 
  4. Bake for about 5 minutes, watch as it happens very quickly! 
  5. From the oven exit take the parchment paper and place it on a rolling pin so that the caramel takes on the rounded shape of the roll. Leave thus until total cooling! Then take off very gently because they break easily. 

prepare the biscuit recipe joconde chocolate print 
  1. Prepare the biscuit decor: mix 15 gr of butter and 15 gr of sugar. 
  2. Add ½ egg white and 10 gr sifted flour with the 10 gr of cocoa. 
  3. Using a 1.5 mm pastry bag, draw arabesque on a baking tray lined with parchment paper. Put the plate in the freezer. Meanwhile melt 20g butter and allow to cool. Preheat oven to 210 ° C (th.7). Mix the icing sugar, the almond powder and 25 gr of flour. Add 3 eggs and mix. Beat 2 whites in snow with 10 gr of sugar and stir in the mixture. Add the melted butter. Pour on the frozen arabesques. Put immediately in the oven for 8 minutes at 210 ° C (th.7). Unmould after cooling. 
  4. Prepare the chocolate biscuit: beat the yolks and half of the sugar until the mixture whitens and doubles in volume. Add the melted chocolate with the butter and mix. Beat the egg whites with the rest of the sugar and gently add them with a spatula to the previous mixture. Sift the flour and cornflour, and add them to the mixture. Pour the paste into a silicone mold or a 20 cm circle placed on baking paper. Put in the oven for 30 minutes at 180 ° C (th.6). Unmount after 10 minutes. Let cool. 

prepare the chocolate biscuit: 
  1. Dress a circle with a 24 cm diameter rhodoïd and place on a dish. 
  2. Arrange strips of biscuits 6 cm high. 
  3. Place the chocolate biscuit in the center. 

prepare the bed of caramelized apples: 
  1. Peel and cut each apple into cubes. 
  2. Bake sugar until you have a caramel. 
  3. Decant by adding the butter in small cubes. 
  4. Out of the fire mix vigorously until the butter is incorporated perfectly. 
  5. You have then a caramel flowing but thick. Put the apples in a dish and top with caramel. 
  6. Cover with aluminum and cook at 180 ° for 25 to 30 minutes. 
  7. Let cool. Drain the apples, but keep the caramel. Using a brush, soak the biscuit decor and the chocolate biscuit, with the 
  8. caramelized apples. 
  9. Lay the bed of caramelized apples on the chocolate biscuit, and place a circle of Nougatine. 

prepare the vanilla cream: 
  1. Heat the milk with the scraped vanilla bean and let it infuse for about 30 minutes. Covers a plate. 
  2. Blanch the yolk with the sugar, add the cornflour and pour the hot milk. Mix well. 
  3. Pour back into pan and cook while whisking until thickened. Boil, remove the vanilla bean and cook another 2 mm cream 
  4. Soften the gelatin in cold water, squeeze it and add it to the cream. Let cool . 
  5. Pour the custard cream into a salad bowl. 
  6. Whip the cream with whipped cream. Stir in 2 tablespoons to loosen the pastry cream and add the rest gently with a spatula. 
  7. Pour the vanilla cream on the nougatine disc and place over the second Nougatine disc. Reserve cool. 

Caramel mousse: 
  1. Prepare all the ingredients. 
  2. Put the gelatin to soak in cold water. 
  3. Make a caramel with the sugar and a little water. 
  4. Cook until you get a blonde color. (If the caramel is not cooked enough, the taste will not be pronounced enough). 
  5. Immediately decant with the cream (heated) while stirring with a wooden spoon, taking care not to burn yourself. 
  6. Pour in a bowl and add the gelatin well éssorée. Let cool and stir occasionally. 
  7. Now put your cream whipped cream, it must be frothy. 
  8. Mix your whipped cream with the caramel maker to obtain a homogeneous mousse. 
  9. Spread the caramel mousse on the Nougatine disc. 
  10. Smooth the whole. 
  11. Leave to cool and then toss with apples. 

Apple pepper: 
  1. Peel and cut the apples in 8. Heat the butter in a frying pan and brown the quarters on each side. At the end of cooking, sprinkle with sugar (2 tablespoons) and let caramelise. Let cool on a plate. Place the apples in a rosette on the caramel mousse. Make the topping: put in a saucepan the apple juice, 8 tbsp. tablespoons sugar, and 2 bags of topping, heat while stirring. In the first few bouillons, remove from heat and let cool for 3 minutes, stirring occasionally. With a brush, cover the apples with this glaze. Let take 1 hour in the fridge.Décercler your entremet. Remove the rhodoid tape. 
  2. Decorate with carambar tiles. 


