---
title: panna cotta with brown cream
date: '2017-12-24'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles and bars
- panna cotta, flan, and yoghurt
- ramadan recipe
- sweet recipes
- sweet verrines
tags:
- verrines
- spéculoos
- Agar Agar

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/panna-cotta-a-la-creme-de-marron-2.jpg
---
[ ![panna cotta with brown cream 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/panna-cotta-a-la-creme-de-marron-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/panna-cotta-a-la-creme-de-marron-2.jpg>)

##  panna cotta with brown cream 

Hello everybody, 

Here I go to exploit the small stock of homemade brown cream that I prepared recently. and I start with the favorite dessert of my children, panna cotta. 

Generally my children like the panna cotta nature, without garnish ... But I had seen a recipe for panna cotta with brown cream, so I decorated 4 verrines with jelly a cream of brown, and others not .. In the end, nothing escaped my children, they eat those not garnished, and even garnished with the [ Chestnut cream ](<https://www.amourdecuisine.fr/article-recette-de-la-creme-de-marrons-maison-113097294.html>) ... 

It makes me happy, and at least like that, I found the trick with them, as it at least I will have recipes to share with you, hihihihi.   


**panna cotta with brown cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/panna-cotta-a-la-creme-de-marron-4.jpg)

portions:  8  Prep time:  15 mins  cooking:  6 mins  total:  21 mins 

**Ingredients**

  * 600 ml whipping cream 
  * 320 ml of milk 
  * 100 g of sugar 
  * 2g of agar-agar (or 2 sheets of gelatin) 
  * 1 teaspoon powdered vanilla 

for brown cream jelly: 
  * 100 ml of water 
  * 100 g of [ Chestnut cream ](<https://www.amourdecuisine.fr/article-recette-de-la-creme-de-marrons-maison-113097294.html>)
  * ½ teaspoon agar agar 
  * decoration: 
  * 1 biscuit speculoos crushed 



**Realization steps**

  1. In a saucepan, mix the milk cream, sugar and vanilla. 
  2. Heat 2 minutes and add the agar-agar mixed with a little powdered sugar. Boil 1 min. 
  3. Pour into cups and let cool to room temperature then put in the fridge. 

Preparation of the cream: 
  1. In a saucepan, pour the water and the cream of chestnuts. Heat on low heat. 
  2. Add the agar agar, let it boil for 1 minute and remove from the heat 
  3. spread on the surface of the panna cotta cups. 
  4. and decorate with a little speculoos powder.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/panna-cotta-a-la-creme-de-marron.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/panna-cotta-a-la-creme-de-marron.jpg>)

photo: Samsung Galaxy S6 edge. 




[ ![panna cotta with brown cream 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/panna-cotta-a-la-creme-de-marron-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/panna-cotta-a-la-creme-de-marron-3.jpg>)
