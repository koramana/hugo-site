---
title: cake sponge bob
date: '2015-02-12'
categories:
- dessert, crumbles and bars
- birthday cake, party, celebrations
- cakes and cakes
- sweet recipes
tags:
- Birthday cake
- Anniversary
- desserts
- Child
- Sugarpaste
- Yogurt cake
- Almond paste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/gateau-bob-leponge-.CR2_.jpg
---
[ ![cake sponge bob .CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/gateau-bob-leponge-.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-gateau-bob-leponge.html/gateau-bob-leponge-cr2>)

##  cake sponge bob 

Hello everybody, 

You like the cartoon series Sponge Bob or Sponge Bob, my children and even me, we like a lot ... Yes we can watch SpongeBob at any age, ok, hihihihi. 

This cake was at the request of my daughter, for a while now she's asking me to make the cake SpongeBob, and since it was my son's birthday last Sunday, I took the opportunity to make with one stone 2 blows, of course my baby did not understand anything, but he amused himself by knocking the poor sponge bob, while his brother and sister were playing small balloons and bursting the small pop bottles of thread crazy (I hope it's the right appointment) 

We feast with this delicious cake, and especially that it is a birthday cake very easy to prepare, yes, it's the first time I make a cake decoration, without being really exhausted. 

SpongeBob, we love you at home, and I was happy to taste you and me children, hihihihi 

[ ![cup of cake bob sponge](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/coupe-du-gateau-bob-leponge.jpg) ](<https://www.amourdecuisine.fr/article-gateau-bob-leponge.html/coupe-du-gateau-bob-leponge>)   


**cake sponge bob**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/gateau-bob-leponge-.CR2_.jpg)

portions:  12  Prep time:  90 mins  cooking:  50 mins  total:  2 hours 20 mins 

**Ingredients** [ Yogurt cake ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-moelleux.html> "spongy yoghurt cake")

  * 2 jars of natural yoghurt 
  * 6 eggs 
  * 3 jars of sugar   
(when I say pot, I use the yoghurt pot as a measure) 
  * 1 C. a vanilla coffee 
  * 4 pots of flour   
(In this cake I made a mistake and I added another pot of flour so my cake was a little thick, but still good, hihihihi) 
  * 2 tbsp. tablespoon of baking powder 
  * 1 pot of oil 
  * [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline.html> "muslin cream \(ideal for strawberry\)")
  * Decoration: 
  * almond paste from the trade (marzipan) 
  * brown dye 
  * red dye 
  * yellow dye 
  * blue dye 
  * black dye 
  * white sugar paste 



**Realization steps**

  1. Mix eggs, sugar, yoghurt, 
  2. add the oil and vanilla. 
  3. Mix the flour and baking powder, add to the mixture, 
  4. Butter a rectangular mold 15 x 25 cm, pour the dough and bake for 45 minutes to 1 hour at 180 ° C depending on the oven. 
  5. Check the cooking with a needle or a knife that must come out well dry 
  6. Let cool on rack 
  7. Tell the cake on two, 
  8. decorate the first part with [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline.html> "muslin cream \(ideal for strawberry\)")
  9. drop the second part 
  10. cover the cake with the rest of the cream muslin 
  11. then start decorating the cake with the almond paste in part, adding the appropriate dyes to each part 
  12. For the very white parts on bob sponge I used the white sugar pie 



[ ![cake sponge bob.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/gateau-bob-leponge.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-gateau-bob-leponge.html/gateau-bob-leponge-cr2-2>)
