---
title: my light yogurt peach special diet and diabetes
date: '2010-10-23'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/10/217458981.jpg
---
![spoon](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/217458981.jpg)

So, for you, if you like my yoghurt, change the ingredients. 

mine were: 

  * 400 ml of semi-skimmed milk (just to make 4 yogurts) 

  * 50 ml plain yoghurt (low in fat) (1.5%) 

  * peach infusions (I did not have the aroma peach) 

  * 4 Rounded Sugar Sweetener Case 

  * 3 cases of semi-skimmed milk powder 

  * peaches in syrup. 




![yaourt_ingredients](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/217459521.jpg)

start by heating the milk, and put in a sachet or 2 infusion (my infusion was peach and blackcurrant, the final taste was great), drain and let cool a little, add after, the yogurt nature, the sugar, milk powder, and whip. 

![lait_infuser](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/217460481.jpg)

garnish your yoghurt pots with pieces of peach, which you have spent in the microwave for a few seconds, and that you have subsequently drained well (because the water released by the fruits may affect the quality of the yogurt) 

![peches_aux_coupes](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/217461121.jpg)

pour your mixture of milk + yoghurt into the jars, and put them in the yogurt maker, leave for 10 hours 

![filling](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/217462321.jpg)

after 10 hours, remove from the unit, allow to cool before placing in the refrigerator. 

![mon__yaourt](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/217463671.jpg)

Enjoy your meal. 

Thank you for your feedback 

if you like this article click on the button I like below 

thank you for wanting to subscribe to my newsletter if you want to receive all updated on my blog 

thank you for everything 
