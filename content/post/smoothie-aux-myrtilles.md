---
title: Blueberry smoothie
date: '2016-08-16'
categories:
- juice and cocktail drinks without alcohol
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/smoothie-aux-myrtilles-015.CR2_.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/smoothie-aux-myrtilles-015.CR2_.jpg)

##  Blueberry smoothie 

Hello everybody, 

yet another blueberry recipe, yes it's a fruit that I like a lot, and this time I made a delicious blueberry smoothie ... 

I had a small open and unfinished fishing box that was lying in the fridge, I added these pieces of peaches to mix them with blueberries, and I left some for the decoration .......... 

a smoothie "so smoothiiiiiiiiiiiiie" ... I pass you the recipe, and I savor my verrine while waiting ... .. 

**Blueberry smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/smoothie-aux-myrtilles-014.CR2_.jpg)

Recipe type:  drink smoothies  portions:  3  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 1 glass of blueberries 
  * 1 glass of peaches   
canned 
  * 2 frozen bananas 
  * 2 cups of yoghurt 
  * honey to sweeten the recipe if you need it   
I did not put 
  * water or ice, as needed 



**Realization steps**

  1. Add all the ingredients to the bowl of the blinder, mix. 
  2. If the mixture is too thick, you can add a little water. If it's not cool enough, add a handful of ice cubes and blend until smooth. 
  3. Pour into glasses, and decorate with a few slices of peaches. 



![smoothie-to-blueberry-008.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/smoothie-aux-myrtilles-008.CR2_.jpg)
