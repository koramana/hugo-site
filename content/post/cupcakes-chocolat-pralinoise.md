---
title: Chocolate Cupcakes
date: '2017-01-05'
categories:
- dessert, crumbles et barres
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/01/Cupcakes-chocolat-pralinoise-4.jpg
---
![cupcakes-chocolate-pralinoise-4](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/Cupcakes-chocolat-pralinoise-4.jpg)

##  Chocolate Cupcakes 

Hello everybody, 

Cupcakes are one of the easiest cakes to have made, the only thing that will take time is rather the decoration of cupcakes. today, I share with you the recipe for chocolate cupcakes pralinoise, I hear you say: oh the killing, yes it's a killer these pretty cupcakes while melting, covered with a beautiful layer of chocolate praline crunchy ... 

The recipe comes directly from the Lunetoiles kitchen, which does not miss the opportunity to work wonders and share them with us. 

![cupcakes-chocolate-pralinoise-6](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/Cupcakes-chocolat-pralinoise-6.jpg)

**Chocolate Cupcakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/Cupcakes-chocolat-pralinoise-5.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 2 eggs 
  * 80 g of butter 
  * 100 g of dark chocolate 
  * 80 g caster sugar 
  * 40 g ground almonds 
  * 30 g of flour 

For the roses of the sands: 
  * 40 g of cereals in petals 
  * 100 g of Pralinoise (Poulain 1848) 



**Realization steps**

  1. In a bain-marie, melt chocolate with butter. 
  2. Whisk 2 egg yolks + sugar until the mixture whitens, and stir in the almond powder and flour. Mix well. Stir in the chocolate to this previous mixture. 
  3. Beat the egg whites into firm snow and gently stir in with a spatula so as not to "break" the egg whites. 
  4. Pour the mixture into 6 muffin cups, or 6 ramekins filled with a large box and 160 ° C (th 5) for 30 minutes. 
  5. At the exit of the oven let cool on a grid, then unmold. 
  6. Melt the Pralinoise in the microwave or bain-marie and add the cereals. Delicately with a spoon mix not to break the petals but so they are all coated with chocolate. Put small piles with the spoon on the muffins and allow to harden. 



![cupcakes-chocolate-pralinoise-2](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/Cupcakes-chocolat-pralinoise-2.jpg)
