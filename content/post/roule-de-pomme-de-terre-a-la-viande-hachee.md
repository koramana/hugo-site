---
title: rolled potato with minced meat
date: '2018-02-26'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe
tags:
- inputs
- Vegetarian cuisine
- Healthy cuisine
- Easy cooking
- Economy Cuisine
- Ramadan
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-a-la-pommes-de-terre-et-viande-hach%C3%A9e.CR2-001.jpg
---
[ ![rolls potato with chopped meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-a-la-pommes-de-terre-et-viande-hach%C3%A9e.CR2-001.jpg) ](<https://www.amourdecuisine.fr/article-roule-de-pomme-de-terre-a-la-viande-hachee.html/roule-a-la-pommes-de-terre-et-viande-hachee-cr2-001>)

##  rolled potato with minced meat 

Hello everybody, 

This appetizer is just to die for, a delicious roll of potato with minced meat, which I make every time I do not have time to cook, because this recipe is super easy to make, and does not take a lot of cooking time, the only thing that will take time, is grating the potato. 

This minced meat potato roll is eaten warm, and can be reheated in the oven without losing its flavor. The most beautiful thing and you can realize several variants of this delight: [ rolled potato and sweet potato ](<https://www.amourdecuisine.fr/article-roule-pommes-de-terre-et-patate-douce.html>) , or [ rolled zucchini ](<https://www.amourdecuisine.fr/article-roule-la-courgette-et-thon.html>) ... and you, what could be your version? 

**rolled potato with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-de-pommes-de-terre-1.jpg)

portions:  4-5  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 3 good size potatoes 
  * ½ onion 
  * 2 small shallots 
  * Four triangles of cheese 
  * 4 eggs 
  * 1 tablespoon of sour cream (if not fresh cream and a drizzle of lemon juice) 
  * parsley 
  * ½ cube of chicken knorr (or Jambo) 
  * Salt, pepper, nutmeg 
  * special cheese sandwich or burger 
  * minced meat cooked in a little oil seasoned with salt, garlic, black pepper and chopped parsley 



**Realization steps**

  1. grated potatoes cleaned and peeled 
  2. finely chop onions and parsley 
  3. mix all these ingredients, with the whipped eggs, the crumbled cheese and the sour cream 
  4. season with salt, black pepper, nutmeg and half knorr cube 
  5. spread the mixture evenly, in a baking tin lined with a sheet of baking paper 
  6. cook in a preheated oven at 190 degrees C for almost 30 minutes 
  7. meanwhile prepare the minced meat 
  8. remove the tray from the oven, place the special sandwich moldings to cover the entire surface 
  9. add over the minced meat 
  10. immediately roll with the baking paper 
  11. then put back in the oven so that it takes a nice color for 5 to 7 minutes 
  12. let cool a little before cutting and tasting 



[ ![rolls potato with chopped meat 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roule-de-pommes-de-terre-1.jpg) ](<https://www.amourdecuisine.fr/article-roule-de-pomme-de-terre-a-la-viande-hachee.html/roule-de-pommes-de-terre-1>)

recipe tested and approved by:   
  
<table>  
<tr>  
<td>

[ ![rolled potato](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/roul%C3%A9-de-pomme-de-terre-150x92.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/roul%C3%A9-de-pomme-de-terre.jpg>)

at Hayat Lainser 


</td>  
<td>

[ ![rolled nessrine](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/roul%C3%A9-nessrine-150x110.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/roul%C3%A9-nessrine.jpg>)

at Nesrine Sissou 


</td>  
<td>

[ ![rolled](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/roul%C3%A9-150x101.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/roul%C3%A9.jpg>) 
</td> </tr>  
<tr>  
<td>

[ ![rolled potato Radia](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/roul%C3%A9-de-pomme-de-terre-Radia-150x90.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/06/roul%C3%A9-de-pomme-de-terre-Radia.jpg>)

at Radia 


</td>  
<td>

[ ![rolled potato](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/roul%C3%A9-de-pomme-de-terre-113x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/05/roul%C3%A9-de-pomme-de-terre.jpg>)

in 


</td>  
<td>

[ ![rolled potato](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-de-pomme-de-terre-150x113.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-de-pomme-de-terre.jpg>) 
</td> </tr>  
<tr>  
<td>

[ ![rolled potato](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-pomme-de-terre-98x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-pomme-de-terre.jpg>) 
</td>  
<td>

[ ![rolled to the potato](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-a-la-pomme-de-terre-113x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/04/roul%C3%A9-a-la-pomme-de-terre.jpg>) 
</td>  
<td>


</td> </tr> </table>
