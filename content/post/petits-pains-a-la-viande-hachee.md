---
title: buns with chopped meat
date: '2017-07-27'
categories:
- appetizer, tapas, appetizer
- pizzas / quiches / pies and sandwiches
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/buns-a-la-viande-hachee_thumb1.jpg
---
##  buns with chopped meat 

Hello everybody, 

Here are very good rolls, slippers, or even named buns that I often prepare, because at home, they have a crazy success. This is a recipe that I re-archive from my blog, and I promise you that this recipe has always had great success with my readers.You can give them the shape that want, for this time, I just rolled , make nicks on one of the ends of the dough, which gives them this pretty trellis finish, 

So, I'll quickly pass you this easy recipe. For the dough, I'm always faithful to the dough [ pizza ](<https://www.amourdecuisine.fr/article-pate-a-pizza-maison-moelleuse-et-croustillante.html>) and for which I became an addict, and that I advise everyone. 

{{< youtube QxzENUWYwo8 >}} 

**buns with chopped meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/buns-a-la-viande-hachee_thumb1.jpg)

Recipe type:  accompaniment, entrance  portions:  12  Prep time:  45 mins  cooking:  25 mins  total:  1 hour 10 mins 

**Ingredients** for the dough:   
(to make 10 to 12 slippers, depending on their size) 

  * 3 glasses of flour (200 ml glass) 
  * 1 tablespoon instant yeast 
  * 1 tablespoon of sugar 
  * 1 teaspoon of salt 
  * 3 tablespoons milk powder 
  * 4 tablespoons of olive oil or even table 
  * 1 cup of baking powder 
  * water as needed 

for the stuffing: 
  * 250 g minced mutton 
  * 1 onion 
  * Olive oil 
  * 2 cloves garlic 
  * 1 bunch of parsley 
  * 1 tbsp tomato concentrate diluted in a little water 
  * Salt, pepper, coriander powder, cumin (1/4 cup of coffee) 

For gilding: 
  * 1 egg yolk 
  * 1 tablespoon of milk 
  * seed of nigella 



**Realization steps** Prepare the dough 

  1. Mix all the ingredients in the dough and add enough lukewarm water until soft, soft and easy to work. 
  2. Knead a few minutes. 
  3. Form the dough into a ball and let it rise under a cloth, protected from drafts until it has doubled in volume. 

prepare the stuffing 
  1. In a skillet, pour a splash of olive oil. Add chopped onion, grated garlic and chopped parsley. 
  2. Let it come back until the onion is translucent. 
  3. Add the minced meat, salt and pepper and add the spices 
  4. Simmer on low heat. 
  5. At the end of cooking, add the diluted tomato and simmer for a few minutes. 

Prepare the rolls 
  1. Once the dough has doubled in volume, degas it and divide it into 10 to 12 balls. 
  2. Preheat the oven to 180 ° 
  3. Spread the dough on a floured work surface. 
  4. Put a line of stuffing in the middle 
  5. with a roulette wheel, draw lines in one end 
  6. cover the stuffing with the end not drawn, then fold the other side over. 
  7. put your buns on a floured baking sheet and let them double in size. 
  8. When it has risen well, brush with a mixture of milk and egg yolk and sprinkle with seeds of nigella. 
  9. Bake for 20 to 25 min (depending on the oven) (and especially not going to respond to comments like me, I almost burn my buns, hihihihi) 
  10. enjoy with a very good salad 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
