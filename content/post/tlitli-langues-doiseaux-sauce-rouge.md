---
title: Tlitli, birds languages ​​red sauce
date: '2011-03-19'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-4.jpg
---
##  Tlitli, birds languages ​​red sauce 

Hello everybody, 

Before you post the recipe of this Tlitli, bird languages ​​red sauce, I do not tell you the number of recipes that I prepare, and that I say to myself "not worth publishing" under the pretext that everyone knows the recipe, or then each his own way of preparing this recipe, but at the same time, I do not tell you the number of people who come to ask me this same recipe that I went to repel not to publish on the blog. 

this time, almost 20 girls in the space of 3 days have asked me for this recipe, the Tlitli recipe, also known as the languages ​​of birds. so that I try to prepare a traditional dish every week (and I speak of Couscous, chakhchoukhat eddfar, Trida square, rechta ......) this week it was the turn of the Tlitli, a pity that I had forgotten to take the pictures of the steps. 

You can also see the [ tlili with minced meatballs ](<https://www.amourdecuisine.fr/article-tlitli-constantinois-pour-l-annee-hijir-1438.html>) , and if you want to know if you can used these pasta otherwise, you can see the recipe for the [ chorba tongue of birds ](<https://www.amourdecuisine.fr/article-chorba-algerienne-aux-langues-doiseaux.html>) , click on the photo 

[ ![Algerian chorba with bird tongues 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/chorba-alg%C3%A9rienne-aux-langues-doiseaux-4.jpg) ](<https://www.amourdecuisine.fr/article-chorba-algerienne-aux-langues-doiseaux.html>)   


**Tlitli, birds languages ​​red sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tlitli-2_thumb_1.jpg)

**Ingredients**

  * for the sauce: 
  * 500g of chicken (thighs in this recipe) 
  * 1 big onion 
  * 2 cloves garlic 
  * olive oil 
  * a generous handful of chick peas 
  * salt, black pepper, spice mixture "coriander / garlic" 
  * 1 tbsp and a half tomato concentrate 

500 gr of tlitli (bird languages) 
  * 1 tablespoon of table oil 
  * 1 tablespoon of butter 
  * 2 to 3 hard boiled eggs 



**Realization steps**

  1. Sauté the chopped chicken pieces with the finely chopped onion oil and crushed garlic in a couscous pot 
  2. add the tomato concentrate 
  3. add salt and spices, and let simmer 
  4. add the chickpeas and cover with 1 and ½ to 2 liters of hot water. 
  5. take the pack of bird tongues, coat it with a case of oil so that it does not stick during the passage to the steam. 
  6. pour them into the top of the couscoussier and let steam for about 15 to 20 minutes, 
  7. pour into a bowl, salt, pepper and sprinkle with a little hot water. 
  8. let this water absorb, then cook again on the steam 
  9. pour in the bowl, sprinkle with 2 or 3 ladles of sauce, allow to absorb and cook again on steam. 
  10. after cooking chicken remove with cooked chickpeas, place in another small pot and reserve sauce. 
  11. Pour the tlitli, in a pot, add the sauce very slowly, ladle by ladle, turning on medium heat so as not to 
  12. do this until the tlitli absorbs the sauce well, and is well tender and cooked, 
  13. add the butter spoon 
  14. rectify the seasoning if necessary 
  15. serve in a deep plate, decorate with meat and chickpeas in the middle, boiled eggs 



thank you for your comments and visits 

et merci a tout ceux qui ce sont abonnes a ma newsletter 
