---
title: beetroot salad with feta cheese
date: '2016-01-18'
categories:
- salads, salty verrines
tags:
- Mixed salad
- Winter salad
- Vinaigrette
- inputs
- Healthy cuisine
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-de-betterave-au-feta-2.jpg
---
![beetroot salad with feta cheese](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-de-betterave-au-feta-2.jpg)

##  beetroot salad with feta cheese 

Hello everybody, 

Here is a beet salad with feta super easy to achieve, and especially too good ... I know everyone knows how to make a beetroot salad, but with the addition of parsley and feta cheese, this salad takes another dimension. 

Sometimes for even more crunchy, I put crushed nuts on it ... what's good. These salads are always refreshing and just perfect despite their simplicity. Yes simplicity is sometimes the key to the success of a recipe, in any case if you have ideas for simple recipes of salads I am interested, just leave a word in comment. 

you can see the recipe of the [ beetroot salad in verrines ](<https://www.amourdecuisine.fr/article-verrines-pour-aperos-betteraves-et-crevettes.html>) she is beautiful too. 

**beetroot salad with feta cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-de-betterave-au-feta-1.jpg)

portions:  4  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 4 beets 
  * 2 tbsp. parsley 
  * 2 cloves garlic 
  * 3 c. lemon juice 
  * ½ c black pepper 
  * salt according to taste 
  * 3 c. tablespoon of olive oil 
  * 60 gr of Feta cheese 



**Realization steps**

  1. Boil the beets over medium heat for 45 minutes or until it can be easily pricked with a knife. 
  2. remove the skin that must come off easily. 
  3. cut the beets into cubes and do the same with the feta cheese. 
  4. Coarsely chop the parsley 
  5. Mix the beetroot, feta, parsley with the lemon vinaigrette and serve. 

For lemon vinaigrette 
  1. Mix all ingredients in a bowl and whisk lightly. 



[ ![beet salad with feta cheese](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-de-betterave-au-feta.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-de-betterave-au-feta.jpg>)
