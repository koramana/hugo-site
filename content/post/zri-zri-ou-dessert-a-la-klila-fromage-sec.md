---
title: Zri zri, or dessert with klila dry cheese
date: '2015-02-22'
categories:
- Algerian cuisine
- Algerian cakes without cooking
tags:
- Algerian cakes
- Algeria
- Algerian patisserie
- Easy cooking
- Oriental pastry
- Delicacies
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/zri-zri-1.jpg
---
[ ![zri zri, or dessert to the klila](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/zri-zri-1.jpg) ](<https://www.amourdecuisine.fr/article-zri-zri-ou-dessert-la-klila-fromage-sec.html/zri-zri-1>)

##  Zri zri, or dessert with klila dry cheese 

Hello everybody, 

Before you talk about my little story with Zri zri, I tell you a little about the main ingredient of this recipe: the klila. I'm sure there are many who know the klila, but not everyone. the klila is the cheese that is removed from lben a bit sour and scalded, we get this cheese that separates from the water, we switch to Chinese, and let it drain well. 

In the eastern region of Algeria, the women prepare the klila, and make it dried, in the open air, until it becomes very hard, then the mixture in powder, to have a kind of milk in powder, but with a slightly acid taste. 

Oum Ali, a friend of the bayadh who gave me this recipe, confirms that klila is very good for lowering cholesterol levels in the blood. 

My little story with this recipe from Zri zri, comes back more than 15 years ago, when I saw my mother-in-law "Allah yerhamha" who bought liters and liters of cow's milk, which she left curdling, to become a rayeb (raib), then shaking vigorously all day long, so that it becomes a delicious lben (buttermilk), with lots of pieces of butter. 

Then she took a quantity of this lben, which she boiled, to separate the cheese (klila) from the water. she was draining it all night, in a large clean cloth, and she left it dried in the open air. I admit that I found the smell very unpleasant, and I wondered, what she would prepare with that. 

Then she reduced the klila powder and kept in large jars. and on the D-day she made us the zri zri whose taste has remained frozen until today in my mouth. 

Otherwise a big thank you to Oum Ali for this beautiful recipe, and his generous sharing. 

**Zri zri, or dessert with klila dry cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/zri-zri-1.jpg)

portions:  30  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 500g of fleshy and honeyed dates 
  * 550g of ground klila 
  * 2 tablespoons of smen hor or else olive oil hor (my beautiful mother used the butter very fresh cow, not trade) 



**Realization steps**

  1. place the seedless dates in the top of the couscoussier, and leave until they become very tender. 
  2. add the klila on top, when the steam escapes from among the grains of klila, place all in a large terrine. 
  3. Add the smen, and crush between your hands to mix well. 
  4. you can shape them into balls, or as Oum ali did, use a maamoul mold. 
  5. enjoy with a good lben. 



[ ![zri zri, or dessert to the klila](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/zri-zri.jpg) ](<https://www.amourdecuisine.fr/article-zri-zri-ou-dessert-la-klila-fromage-sec.html/zri-zri>)
