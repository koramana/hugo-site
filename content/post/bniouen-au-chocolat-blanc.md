---
title: white chocolate bniouen
date: '2016-10-29'
categories:
- Algerian cakes without cooking
- gateaux algeriens- orientales- modernes- fete aid
- Gateaux au chocolat
- sweet recipes
tags:
- desserts
- Pastry
- To taste
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/bniouen-au-chocolat-blanc-683x1024.jpg
---
![bniouen-au-chocolate-white](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/bniouen-au-chocolat-blanc-683x1024.jpg)

##  white chocolate bniouen 

Hello everybody, 

I love the bniouen cake without cooking that saves the bet in all occasions. For Eid past, I prepared white chocolate bniouen and speculoos ... a delight. 

usually to do my [ bniouen ](<https://www.amourdecuisine.fr/article-bniouen-gateau-sans-cuisson-algerien.html>) I form a rectangle with the dough, I cover it with dark chocolate, and I cut squares on cooling. I also have a version of [ stuffed bniouen ](<https://www.amourdecuisine.fr/article-gateau-algerien-sans-cuisson-bniouen-farci-101810986.html>) , frankly this version is a marvel. 

This time I liked to make a contrast of colors for my binouen a chocolate biscuity background covered with a beautiful layer of white chocolate. I used silicone molds that gave a just top result!   


**white chocolate bniouen**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/bniouen-au-chocolat-blanc-2.jpg)

**Ingredients**

  * 2 boxes of biscuits speculoos 
  * softened butter 
  * honey according to taste 
  * Turkish halwa (halva) 
  * unsweetened cocoa 
  * a box of white chocolate 
  * crushed roasted peanuts 



**Realization steps**

  1. place the cookies in a freezer bag, close and crush the cookies with a rolling pin. 
  2. add the cocoa, the Turkish halwa and crush with your hands so that the halwa is well mixed with the biscuit. 
  3. add a small amount of softened butter and honey, taste to see if you need to add more honey or cocoa, you do not want cakes too sweet. 
  4. Add the butter until you have a dough that picks up and holds well 
  5. take small amounts of the dough and go into the silicone molds of your choice, put in the freezer for 5 minutes and remove to get the right shape. 
  6. place the cakes on a tray and chill. 
  7. melt white chocolate in a bain-marie, and decorate the bottoms of the melted chocolate molds 
  8. immediately place the cakes in the mold and press a little. 
  9. put in the freezer for at least 30 minutes for the white chocolate to freeze. 
  10. remove the cakes and place on the tray and cool. 
  11. continue in the same way with the rest of the cakes. 
  12. present the cakes in boxes, the cakes are kept cool! 



![bniouen-au-chocolate-white-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/bniouen-au-chocolat-blanc-1.jpg)
