---
title: ghribia two-tone spiral turkish cake
date: '2017-06-23'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
tags:
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/06/ghribia-bicolore-en-spirale-1-683x1024.jpg
---
![ghribia two-tone spiral 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/ghribia-bicolore-en-spirale-1-683x1024.jpg)

##  ghribia two-tone spiral turkish cake 

Hello everybody, 

They are sublime beautiful spiral two-tone ghribia, a Turkish cake that made me look for a while, because many girls had realized and shared on facebook. 

So here is my turn this time, and the cakes are very beautiful. My children loved it especially with the little surprise in the heart of cakes, I stuffed some pieces with Turkish delight, others with white chocolate, and the children were curious to know what was in the heart of their ghribia pieces. 

I made for you a short video of this recipe, do not forget to encourage me on youtube: 

**ghribia two-tone spiral turkish cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/ghribia-bicolore-en-spirale-2.jpg)

**Ingredients**

  * 250 gr of butter at room temperature 
  * 150 gr iced sugar 
  * 150 gr of cornflour 
  * 70 ml of table oil 
  * vanilla 
  * ½ teaspoon of baking powder 
  * 2 tbsp. cocoa powder 
  * flour (about 1 glass and a half flour, the glass measures 250 ml, you can use more depending on the absorption of your flour) 
  * white chocolate chips. 



**Realization steps**

  1. whip the butter and sugar to have a nice cream 
  2. add the table oil and continue whisking. 
  3. then add cornstarch, vanilla and baking powder. 
  4. add a glass of flour and mix well. 
  5. then divide the dough on two, add the cocoa to one part, then pick up with the flour. 
  6. pick up the white part again with the flour. 
  7. take a little of the white paste and form two white rolls, 
  8. and take some of the cocoa paste and form two black pudding. each bead must have a diameter of 1 cm and a half not more. 
  9. stick a white pudding with a black pudding, and put the other black pudding on the white and the other white on the black, to have a checkerboard of 4 colors. 
  10. cut with a knife pieces of almost 1 cm and a half 
  11. place inside 2 chocolate chips. 
  12. roll between the apples of your hands to give a rounded shape to the cake. 
  13. place as you go in a tray. 
  14. bake at 180 ° C for 15 to 20 minutes, or until cakes are cooked through. 



![Two-tone spiral ghribia_](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/ghribia-bicolore-en-spirale_.jpg)
