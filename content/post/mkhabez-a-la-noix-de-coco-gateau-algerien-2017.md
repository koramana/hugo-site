---
title: try the coconut cake Algerian 2017
date: '2017-03-09'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/mkhabez-%C3%A0-la-noix-de-coco-1.jpg
---
![muddle with coconut 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/mkhabez-%C3%A0-la-noix-de-coco-1.jpg)

##  try the coconut cake Algerian 2017 

Hello everybody, 

Starting for the recipe of these beautiful coconut mkhabez that will make their place in the list of Algerian cakes 2017 ... Yes personally, I will be happy to present these beautiful mkhabez coconut on my table Eid el fitre 2017 insha'Allah, what about you? 

If you do not have the necessary material to make this pretty decoration of mkhabez, you can always see the [ recipe and decoration of Lunetoiles ](<https://www.amourdecuisine.fr/article-mkhabez-a-la-noix-de-coco.html>)   


**try the coconut cake Algerian 2017**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/mkhabez-%C3%A0-la-noix-de-coco-2.jpg)

portions:  30  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 3 measures of coconut 
  * 1 measure of crystallized sugar 
  * 1 the zest of a lime (lime) 
  * a few drops of lemon juice 
  * 1 teaspoon of vanilla 
  * 1 egg + egg yolk 

Frosting: 
  * 2 egg whites (more or less depending on the amount of cakes) 
  * 3 tablespoons lemon juice 
  * 5 tablespoons of orange blossom water 
  * 1 C. honey, or table oil for shine 
  * white dye 
  * between 500 and 700 gr of icing sugar 

Decoration: 
  * Sugarpaste 
  * white dye + pearly dye 
  * stencil for cake   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/stencil-plaque-225x300.jpg)



**Realization steps**

  1. place the coconut, the sugar and the lemon peel in the bowl to be blended and mix in finer powder. (If your coconut is not new, it may throw off its oil, so add a tablespoon of flour. 
  2. place the powder mixture in a large salad bowl, add a little lemon juice 
  3. pick up the dough with an egg, and if the dough does not pick up well, add egg yolk. Personally, I pick up the dough between two food films, I press on continuously until having a nice dough. 
  4. spread the dough on a slightly floured work surface at a height of almost 2 cm. 
  5. cook in a preheated oven at 160 ° C for almost 15 minutes. 

Prepare the icing: 
  1. Prepare the icing with the ingredients mentioned above, it must not be flowing or too thick, 
  2. always test on a cake and grind by adding icing sugar until you get the desired texture. 
  3. let the icing dry, then prepare the decoration. 
  4. Spread out the sugar dough, place the stencil on it, spread it out again with the roller pastry, so that the drawing will stand out. 
  5. Always with the stencil on, use the brush to apply the color blue and the pearly color. 
  6. remove the stencil, then cut off the pucks of the same size as the mkhabez 
  7. pass a little egg white on the mkhabez and paste the slice of the dough. 



![muck in the coconut_](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/mkhabez-%C3%A0-la-noix-de-coco_.jpg)
