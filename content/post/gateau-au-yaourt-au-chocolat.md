---
title: Chocolate Yogurt Cake
date: '2017-04-11'
categories:
- birthday cake, party, celebrations
- gateaux, et cakes
- sweet recipes
tags:
- Soft
- Anniversary
- Pastry
- la France
- Algerian cakes
- Fluffy cake
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-au-chocolat-1.jpg
---
[ ![Chocolate Yogurt Cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-au-chocolat-1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-facile.html/gateau-au-chocolat-1>)

##  Chocolate Yogurt Cake 

Hello everybody, 

I made this chocolate yogurt cake for the anniversary of our wedding. For this occasion I wanted to prepare for my husband the first cake he had eaten with my hand, the chocolate cake with yogurt. I wanted it to be the same decoration, the same recipe, the same ganache ... and it was the case, hihihihi. 

I had to look in my old calendar, where I kept all the recipes that I found here and there, or that I wrote down television, newspaper coupons, recipes my friends had the faculty ... I think on my agenda, there are more recipes than on my blog, hihihihi. 

In any case, I found my recipe, and I was super happy about that, because the recipe of my chocolate cake is of incomparable ease, in addition, you will need 1 egg only, so that you said? 

[ ![Chocolate Yogurt Cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-au-chocolat-3.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-facile.html/gateau-au-chocolat-3>)   


**chocolate cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-au-chocolat-2.jpg)

portions:  8  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients**

  * 1 egg 
  * 200 gr of powdered sugar 
  * 170 gr of natural yoghurt 
  * 60 ml of table oil 
  * 1 cup of coffee and a half of vanilla extract 
  * 120 ml of liquid coffee 
  * 40 gr of bitter cocoa 
  * 120 gr of flour 
  * ½ cup of baking soda 
  * ½ teaspoon of baking powder 

for the ganache: 
  * 150 gr of 65% dark chocolate 
  * 150 ml of fresh cream 
  * ½ teaspoon of vanilla extract 
  * 30 gr of butter 

Decoration: 
  * coconut 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. Line a round baking pan with 22 cm of baking paper, otherwise use a silicone mold. 
  3. In large bowl, mix egg, sugar, yoghurt, oil, vanilla and beat with a robot, or even by hand until smooth and combined. 
  4. Add coffee (it should not be hot), sifted cocoa powder, and whisk vigorously until dough is smooth and lump free. 
  5. Add flour, baking soda and baking powder until well blended.   
personally I used the hand whip, and in 1 minute everything was well combined. 
  6. Pour the batter into the prepared pan and bake for about 25 minutes, or until a toothpick inserted in the center comes out clean. 
  7. Allow the cake to soften a little and turn on a rack to cool. 

Prepare the ganache: 
  1. Roughly chop the chocolate and melt it in a bain-marie pan. 
  2. Meanwhile, in another saucepan, bring the liquid cream to a boil. 
  3. Pour the cream in three times over the melted chocolate. 
  4. Using a spatula, gently mix in small circles, starting from the center of the container to obtain a homogeneous mixture. 
  5. Let the mixture cool for 10 minutes, then add the cut butter. 
  6. Mix until your ganache is smooth and shiny. 
  7. Cover your ganache with cling film and let it rest in the refrigerator 15 minutes before use. 
  8. Cover the cake with the ganache, and if you want, you can always decorate with grated coconut. 



[ ![Chocolate Yogurt Cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-facile.html/gateau-au-chocolat-4>)
