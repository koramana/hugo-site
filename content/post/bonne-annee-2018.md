---
title: Happy new year 2018
date: '2018-01-01'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/01/bonne-annee.jpg
---
![happy New Year](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/bonne-annee.jpg)

Happy New Year 2018, 

It is with great pleasure that I address all the readers of my blog, who made my dream a reality. Many of you were following me this year and in previous years, I hope to count you once again among my faithful for this year 2018. 

I hope that I will continue to make recipes that will please you and that you always press it to encourage me. 

Bonne année gourmande, et que du bonheur pour tout le monde. Merci. 
