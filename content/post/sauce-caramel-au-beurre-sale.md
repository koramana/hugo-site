---
title: caramel and salted butter sauce
date: '2013-02-16'
categories:
- diverse cuisine
- Cuisine by country
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/caramel-au-beurre-sale-014_thumb1.jpg
---
##  caramel and salted butter sauce 

Hello everybody 

Salted butter caramel sauce to spread a crust? or to garnish your pies? or why not just one on your desserts, huuuuuuuuuuuuuum. 

We succumb to the errisistible taste of this sweet salty sauce, a simple and easy recipe. I used this caramel for the realization of these delicious [ salted butter caramel mousse ](<https://www.amourdecuisine.fr/article-mousse-caramel-au-beurre-sale-101394974.html>) and it was a killing. 

As I have many recipes in which salted butter caramel sauce was a main ingredient. Activity 

so me, I do not like the taste of the crème fraîche in this recipe and besides I did not have any, so I made my salted butter caramel sauce with milk. 

for the method:   


**caramel and salted butter sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/caramel-au-beurre-sale-014_thumb1.jpg)

**Ingredients**

  * 240 grs of crystallized sugar 
  * 200 ml of milk (or 150 ml of fresh cream) 
  * 70 grams of salted butter 



**Realization steps**

  1. put the sugar to cook in a saucepan, to have a sweet caramel, not to burn too much, 
  2. add after the milk while stirring, until the caramel is well incorporated and melts in this milk, 
  3. add the butter off the heat, and stir a little. 
  4. let cool, and put in a jar for use, later .......... 



en tout cas, moi j’ai fait des petits délices avec ça, que je posterai prochainement 
