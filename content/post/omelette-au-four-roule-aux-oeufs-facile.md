---
title: easy baked omelette / egg roll
date: '2016-06-14'
categories:
- pizzas / quiches / pies and sandwiches
tags:
- Ramadan 2016
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/omelette-au-four.CR2_thumb1.jpg
---
##  easy baked omelette / egg roll 

Hello everybody, 

How to make a baked omelette while changing from the ordinary omelette that is just preparing by breaking the eggs, adding the salt, and voila in the pan for a few minutes? 

This omelette baked is different, but super good as a starter, to accompany a salad, or for a light meal. the only difficulty will be to grill the peppers, remove the skin, and cut them ... 

Yes that's all ! You will say: I am the queen of fast recipes without a headache, hihihihi, 

**easy baked omelette / egg roll**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/omelette-au-four.CR2_thumb1.jpg)

portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 220 ml of milk 
  * ⅓ glass (220 ml) all-purpose flour (I put less flour, because my husband likes it to be light) 
  * 8 eggs 
  * Salt to taste 
  * ¼ cup of black pepper 
  * 2 cups of ketchup 
  * 2 red peppers, grilled, seeded, cleaned and diced. 
  * 1 caramelized onion in a little oil. 
  * 1 glass of cheddar cheese 



**Realization steps**

  1. Preheat oven to 180 degrees C 
  2. cover an 18 x 25 cm baking tray with parchment paper (leave the paper overflowing) 
  3. In a bowl, mix the flour and the milk. 
  4. Add the eggs, ketchup, salt and pepper and mix well with a whisk. 
  5. Pour the dough into the prepared baking pan. 
  6. Sprinkle with onions, red pepper, evenly on top. 
  7. Bake for 20 minutes, until eggs are cooked through. 
  8. Sprinkle the cheese on top and bake for another 6-8 minutes 
  9. Remove from oven and let cool for 5 minutes. 
  10. Lift the parchment paper from one of the shorter sides and roll the omelette well, remove the paper and slice 
  11. Serve hot 


