---
title: creamy chocolate cake with cream
date: '2015-04-02'
categories:
- cakes and cakes
- sweet recipes
tags:
- Anniversary
- Pastry
- la France
- Algerian cakes
- Easy cooking
- Cake
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-moelleux-au-chocolat-a-la-creme.jpg
---
[ ![creamy chocolate cake with cream](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-moelleux-au-chocolat-a-la-creme.jpg) ](<https://www.amourdecuisine.fr/article-gateau-moelleux-au-chocolat-la-creme.html/gateau-moelleux-au-chocolat-a-la-creme>)

##  creamy chocolate cake with cream 

Hello everybody, 

Which of you does not melt at the sight of a chocolate cake? Whether it is a [ chocolate cake very fluffy ](<https://www.amourdecuisine.fr/article-cake-au-chocolat-moelleux.html> "fluffy chocolate cake") , or a cake [ Gluten-free molten chocolate cake ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-sans-gluten.html> "gluten-free chocolate cake") , a [ chocolate cake with yoghurt ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-facile.html> "chocolate cake") , or a [ chocolate mouskoutchou ](<https://www.amourdecuisine.fr/article-le-mouskoutchou-au-chocolat.html> "chocolate mouskoutchou") , why not a [ royal chocolate ](<https://www.amourdecuisine.fr/article-royal-au-chocolat-ou-trianon.html> "Royal chocolate or trianon") .... All these delights will always tempt us. We forget everything when we see chocolate, are not you of my opinion? 

The recipe of today, is a recipe prepared by the care of a mother with her children, one feels a lot of love and delicacy by looking at this pretty cake soft chocolate with the cream with the zest of lemon, covered with a shiny layer of chocolate ganache. This is the recipe of my friend Wamani Merou, it makes me really happy to see his recipes on my blog, I hope you will succeed this recipe, when you try it.   


**creamy chocolate cake with cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-moelleux-au-chocolat-a-la-creme-2.jpg)

portions:  8  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** for the cake: 

  * 4 egg 
  * 1 glass of sugar (200 ml glass) 
  * 2 tablespoons of nescafe (reduced to a fine powder and sieved) 
  * 1 glass of milk 
  * 1 glass of oil 
  * 2 tablespoons of cocoa 
  * 2 glasses of flour 
  * 1 sachet of baking powder 

cream: 
  * ½ l of milk (500 ml) 
  * 3 tablespoons of sugar 
  * vanilla 
  * 2 tablespoon cornflour 
  * the zest of a lemon 

chocolate ganache: 
  * 200 gr of chocolate with 55% cocoa. 
  * 200 ml cream 
  * 1 knob of butter 



**Realization steps**

  1. In the robot bowl, work the eggs, sugar, nescafe, milk and oil. 
  2. Gently introduce the mixture: flour, baking powder and cocoa. 
  3. divide the dough into two equal parts. 
  4. pour each part into a buttered and floured tray of 15 x 30 cm 
  5. cook in a preheated oven at 180 degrees C for 12 to 15 minutes 
  6. Prepare the cream, mixing all the ingredients in a saucepan 
  7. stir over low heat until creamy. leave aside. 
  8. now prepare the ganache, heat the cream and butter over medium heat. 
  9. remove from the heat, and introduce the chocolate, stir with a wooden spoon, until the chocolate melts completely. 

mounting: 
  1. standardize the surface of the cake by cutting the curved side. 
  2. take the first cake (you can make a light syrup of 100 gr of sugar with 100 gr of water, and soak your cake, to give it an even more mellow side) 
  3. cover the first cake with the vanilla cream 
  4. place the second cake over, soak it with a little syrup, and cover with chocolate ganache. 
  5. You can decorate the cake according to your taste. 



[ ![creamy chocolate cake with cream](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-moelleux-au-chocolat-a-la-creme-1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-moelleux-au-chocolat-la-creme.html/gateau-moelleux-au-chocolat-a-la-creme-1>)

recipe tested and approved:   
  
<table>  
<tr>  
<td>

[ ![Oumabdallah chocolate cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-au-chocolat-Oumabdallah-150x125.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-au-chocolat-Oumabdallah.jpg>)

at Oumabdallah l 


</td>  
<td>

[ ![fluffy chocolate cake amina dadou](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateau-moelleux-au-chocolat-amina-dadou-150x112.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateau-moelleux-au-chocolat-amina-dadou.jpg>)

at Amina Dadou 


</td>  
<td>

[ ![malika chocolate cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/gateau-au-chocolat-malika-150x113.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/04/gateau-au-chocolat-malika.jpg>)

at Malika B 


</td> </tr> </table>
