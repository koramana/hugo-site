---
title: old-fashioned apple cake
date: '2016-11-06'
categories:
- cakes and cakes
- sweet recipes
tags:
- Butter
- To taste
- Pastry
- fall
- desserts
- Easy cooking
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/gateau-aux-pommes-a-l-ancienne_thumb1.jpg
---
##  old-fashioned apple cake 

Hello everybody, 

The apple is one of the fruits I love most, and eating an old-fashioned apple cake only makes me dream. 

So without delay, I deliver you the recipe for this beautiful cake ..   


**old-fashioned apple cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/gateau-aux-pommes-a-l-ancienne_thumb1.jpg)

portions:  8  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients**

  * 6 apples of Reinette type, 
  * 200 gr of flour, 
  * 120 gr of powdered sugar, 
  * 2 eggs, 
  * 1 lemon, 
  * 50 gr of butter, 
  * 100 ml of milk, 
  * 1 teaspoon of yeast, 
  * 1 tablespoon orange blossom water 
  * 1 pinch of cinnamon, 
  * 1 pinch of salt, 

CREAM AT OLD 
  * 80 gr of powdered sugar, 
  * 70 gr of butter, 
  * 1 egg 



**Realization steps**

  1. method of preparation: 
  2. Preheat the oven to 160 °. 
  3. Peel, seed and cut the apples into thin slices. Sprinkle with lemon juice and reserve. 
  4. Melt the butter. 
  5. Sift the flour with the yeast into a container. add the eggs and whisk together. 
  6. Slowly add the milk, melted butter, caster sugar and salt. 
  7. Mix until smooth and homogeneous. 
  8. Add cinnamon and orange blossom water, then gently add half of the apples. 
  9. Mix until they are completely covered with dough. 
  10. Butter a round pan, pour the dough mixture, and arrange the remaining apples on the dough. 
  11. Bake and cook for 25 minutes. 

Meanwhile, prepare the old-fashioned cream: 
  1. melt the butter, add the beaten egg and the sugar. Mix vigorously all and reserve. 
  2. The first cooking done, pour the cream on the cake and continue cooking for 30 minutes (depending on your oven). At the end of the oven let cool before unmolding. 



Thank you Lunetoiles for this delicious cake. Thanks to Soso for this nice recipe. And if you have apples and you run out of ideas, this index of [ apple recipes ](<https://www.amourdecuisine.fr/article-que-faire-avec-des-pommes-100741309.html>) vous aidera certainement, alors qu’allez faire avec vos pommes? 
