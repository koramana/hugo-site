---
title: pancakes with florentine
date: '2013-02-17'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gratin-de-crepes-florentines-068a1.jpg
---
![gratin-of-pancakes-Florentine-068a.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gratin-de-crepes-florentines-068a1.jpg)

Hello everybody, 

here is a very good recipe crepes, a real delice, that I have to concotter this week, and that everyone adore at home, and most importantly, they eat the spinach without detecting them, I really like recipes like that. 

For pancakes: 

  * 250 g buckwheat flour 
  * 2 eggs 
  * 2 tablespoons of oil 
  * 400 ml of water 
  * 1 pinch of salt. 
  * [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile-99197863.html>)



For the stuffing: 

  * 400 g spinach 
  * 400 g ricotta 
  * 200 g of parmesan cheese 
  * 2 eggs 
  * 6 tablespoons tomato coulis 
  * salt pepper 



![2012-02-10-gratin-of-pancakes-florentines.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/2012-02-10-gratin-de-crepes-florentines.jpg)

method of preparation: 

prepare the pancakes: 

  1. in a bowl, form a well with the dry elements, 
  2. break the eggs in the center and mix vigorously with the whisk, adding the milk gradually. 
  3. To be sure that there are no more lumps, pass the preparation in a colander or blender. 
  4. Let the dough rest for 1 hour at room temperature. 
  5. heat a pan or pancake and oily. 
  6. Pour a ladle of dough by spreading it and cook each pancake on both sides. Return as soon as the coloring. 



the joke: 

  1. cook the spinach with a little water, wring out and leave to cool 
  2. mix with ricotta and whipped eggs. add the Parmesan cheese. 
  3. spread each pancake with this stuffing, roll the stretches, 
  4. cut each roll into lozenges of the same size. 
  5. Preheat the oven to 210 degrees C 
  6. Place the pancake lozenges in a baking tin. 
  7. Top them with béchamel, and garnish with tomato coulis, sprinkle with a little parmesan cheese. 
  8. make gratin between 10 to 15 min 



![gratin-of-pancakes-Florentine-059.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-crepes-florentines-0591.jpg)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
