---
title: cupcakes with oreos
date: '2017-11-15'
categories:
- Cupcakes, macaroons, and other pastries
- sweet recipes
tags:
- Cakes
- Algerian cakes
- Cake
- To taste
- Easy recipe
- Breakfast
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/11/cupcakes-aux-oreos-1.jpg
---
![cupcakes with oreos 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/cupcakes-aux-oreos-1.jpg)

##  cupcakes with oreos 

Hello everybody, 

These cupcakes at Oreos are so good that with my children I could only save a few pieces for photography. 

![ob_d97cb4_cup-01](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/ob_d97cb4_cup-01.jpeg)

![ob_8ae1a9_nb](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/ob_8ae1a9_nb.jpg)

**cupcakes with oreos**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/cupcakes-aux-oreos-4.jpg)

portions:  20 

**Ingredients**

  * 220 g flour 
  * 160 g of butter 
  * 200 g of sugar 
  * 2 tbsp. cocoa 
  * 1 sachet of baking powder 
  * 4 eggs 
  * 30 cl whole cream (30% fat or more) 
  * 2 tbsp. Mascarpone 
  * 1 C. tablespoon iced sugar 
  * 20 Oreos 



**Realization steps**

  1. Commencez par fouetter le beurre et le sucre, vous pouvez le faire au robot, batteur électrique ou la main 🙂 
  2. Add the eggs 1 by 1, beat well between each egg 
  3. Then add unsweetened cocoa, flour and yeast, stir briefly to obtain a smooth paste 
  4. Separate 12 oreos, arrange half with the white filling in the bottom of each box, mix in fine powder the other 12 halves (which have only the black biscuit) 
  5. Pour the mixture into cupcakes and bake for 20 minutes at 180 ° C 
  6. Meanwhile, mix the cream, the cold mascarpone, and the icing sugar and whip until you get a thick whipped cream. 
  7. Fill the pocket with a small socket and decorate the cupcakes, this cahntilly, sprinkle the top with Oreso powder.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/cupcakes-aux-oreos-2.jpg)
  8. and garnish with a half Oreo. 



![cupcakes with oreos 5](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/cupcakes-aux-oreos-5.jpg)
