---
title: balah el sham, honey oriental pastry
date: '2015-05-03'
categories:
- gateaux algeriens au miel
- gateaux algeriens frit
- gourmandises orientales, patisseries orientales
tags:
- Algerian cakes
- Algeria
- Algerian patisserie
- delicacies
- Aid
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balah-el-sham-1.jpg
---
[ ![balah el sham honey oriental pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balah-el-sham-1.jpg) ](<https://www.amourdecuisine.fr/article-balah-el-sham-patisserie-orientale-au-miel.html/balah-el-sham-1>)

##  balah el sham, honey oriental pastry 

Hello everybody, 

What do I like about these little delicacies of Eastern cuisine known as: balah el sham, which means in French: the dates of Syria (peace reigns and returns to this beautiful country). We call this delight like that, because each piece is almost the size of a date. 

Balah el sham is actually a sweetness made with a cabbage paste, fried in a hot oil bath, then dipped in honey. Although these cupcakes are very similar to the zalbia el banane Algerian, but the two pastries are different, because zalbia el banana is crispy on the outside and melting inside, we bite into a zalbia, but balah el sham is super fondant, just a nice thin layer a little crunchy, and the rest is an endless fondant. Do not think that you will stop at a room !!! 

Following the recipe, you will have a good amount of balah el sham, I advise you if you are not many, not to dip all your cakes in the syrup. Keep the rest of the cakes in an airtight container, and when you want to eat them, place the desired amount in a preheated oven for a few minutes, then dive into the honey, and the cakes will keep their flavors and delights, as if you're coming from cook them. 

Previous rounds: 

![balah el sham honey oriental pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balah-el-sham-patisserie-orientale-au-miel.CR2_.jpg)

**balah el sham, honey oriental pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balah-el-sham-dessert-du-ramadan-2015.CR2_.jpg)

portions:  10  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 250 ml of water 
  * 50 gr of butter 
  * 50 gr of vegetable oil 
  * 3 eggs 
  * vanilla according to taste (I put the caramel extract) 
  * 200 gr of flour 
  * 1 tablespoon of sugar 

for the syrup: 
  * honey diluted in a little water and scented with lemon zest. 



**Realization steps**

  1. In a large saucepan, combine water, sugar, oil and butter over low heat until boiling 
  2. Sift the flour, and add it all at once to the butter-water mixture, stirring with a wooden spoon until the dough comes off the pan, then remove from heat. 
  3. let this dough cool for at least 30 minutes. then place it in the bowl of your robot 
  4. Add the eggs one by one, while mixing, until the dough becomes homogeneous 
  5. do the same thing with the second and the third egg. 
  6. place the dough in a piping bag with a fluted socket 
  7. heat in the oil bath, and cut with scissors small flakes 5 cm long while dipping directly into the oil. 
  8. Drain and dip in the syrup immediately 



[ ![balah el sham honey oriental pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balah-el-sham-patisserie-orientale-au-miel-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-balah-el-sham-patisserie-orientale-au-miel.html/balah-el-sham-patisserie-orientale-au-miel-1-cr2>)

Et voila la liste des autres participantes a cette ronde autour du » miel » faites un saut pour voir tout leurs délices: 
