---
title: grape jam
date: '2015-08-15'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/confiture-de-raisins-014_thumb.jpg
---
[ ![grape jam 014](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/confiture-de-raisins-014_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/09/confiture-de-raisins-014.jpg>)

##  grape jam 

Hello everybody, 

In the month of Ramadan, my husband went to the market, he bought 2 kilos of grapes, he was fasting so he could not drip these grapes. 

After the course, I do not tell you how acidic these grapes were, to make you turn your head ... 

I could not throw 2 kilos of grapes like that, the only solution was a good jam that will camouflage all this acidity 

**grape jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/confiture-de-raisin_thumb.jpg)

Recipe type:  jam, canned  portions:  6  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 2 kilos of grapes 
  * 1200 grs of sugar 
  * the juice of half a lemon 



**Realization steps**

  1. Clean the grapes, and put them in a special copper jam pot, or like me in a pot with a round bottom. 
  2. Pour in the sugar and lemon juice, and cook until thickened. 
  3. Test the cooking by dropping a few drops on a cup. If the jam is gelled right away, it is ready, otherwise let it cook again and repeat the test regularly. 
  4. Fill the pots with boiling jam with the ladle; close and return them. 
  5. leave them upside down until the next day. 



jam was a delight, and everyone preferred grapes like that, and not like walking them. 

Thank you for your feedback. 

you can also see: 

[ ![tomato jam 008](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/confiture-de-tomate-008_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/09/confiture-de-tomate-008.jpg>)

[ la Confiture de Tomate ](<https://www.amourdecuisine.fr/article-confiture-a-la-tomate-56415193.html>)
