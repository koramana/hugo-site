---
title: tcharek msaker Horn of gazelle coated with icing sugar
date: '2014-07-18'
categories:
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/tcharek-msakar.jpg
---
#  tcharek msaker Horn of gazelle coated with icing sugar 

Hello everyone, 

always with the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) and another delight of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) known in several forms: [ iced chara ](<https://www.amourdecuisine.fr/article-soltan-el-meida-tcharek-glace-corne-de-gazel-avec-glacage-55370825.html>) , [ Tcharek El Ariane ](<https://www.amourdecuisine.fr/article-tcharek-el-ariane-53899937.html>) , and [ charak maasel ](<https://www.amourdecuisine.fr/article-corn-de-gazelle-au-nekkache-a-la-pince-59752732.html>) Today is the recipe for a charak covered with icing sugar, hence its name **tcharek msaker Horn of gazelle coated with icing sugar** that lunetoiles liked to share with you. 

I advise you in any case to explore my list of [ traditional Algerian cakes ](<https://www.amourdecuisine.fr/article-gateaux-traditionnels-algeriens-100466409.html>) while sipping a nice glass of mint tea, you'll surely enjoy it.   


**tcharek msaker Horn of gazelle**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/tcharek-msakar.jpg)

portions:  50  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for the pasta: 

  * 3 measures of flour (500 gr) 
  * 1 measure of melted fat smen or margarine (166 gr) 
  * 1 pinch of salt 
  * 2 tablespoons sifted icing sugar 
  * 1 C. dessert vanilla extract 
  * 1 teaspoon of orange blossom water 
  * water needed 

for the stuffing: 
  * 2 measures of finely ground almonds (200 gr) 
  * 1 measure sifted icing sugar (100 gr) 
  * 1 C. melted fat 
  * 1 C. dessert vanilla extract 
  * Orange tree Flower water 

for the syrup: 
  * ½ liter of water 
  * 1 teaspoon of orange blossom water 
  * 250 gr of crystallized sugar 



**Realization steps**

  1. Put the sifted flour with the icing sugar, vanilla extract and salt. 
  2. Make a fountain and add the melted and cooled fat. 
  3. Rub well with the palms of your hands to penetrate the fat into the flour. 
  4. Moisten with orange blossom water and the necessary water by kneading well until soft and homogeneous. 
  5. Divide it into small balls the size of a small tangerine, place in a tray, cover with food paper and let stand. 
  6. Meanwhile prepare the stuffing with the ingredients given above and in the order of presentation. 
  7. Shape small sausages 8 cm long and half a centimeter in diameter. 
  8. Place them on a dish sprinkled with icing sugar. 
  9. On a floured work surface, lower the balls of pasta, cut circles about 10 cm, and spread the circles more finely in a slightly oval shape. 
  10. Place the stuffing pudding on the edge of the circle, roll while pressing slightly while giving the shape of a crescent. 
  11. Place the cakes on the baking sheet and bake in the oven (180 ° C). 
  12. They must have a golden color. 
  13. Remove from the oven and let cool. 
  14. Prepare the syrup with the given ingredients. Boil the syrup for 15 minutes and until it thickens.   

  15. Dip the cakes in the cooled syrup, drain slightly and roll in icing sugar. 
  16. Let dry. The next day, roll the cake again in the icing sugar. 
  17. Present in boxes. 


