---
title: bavarian biscuit joconde mousse with cream custard
date: '2014-10-22'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cheesecakes and tiramisus
- dessert, crumbles and bars
- birthday cake, party, celebrations
- basic pastry recipes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/bavarois-a-la-creme-custard-cerises-et-kaki.jpg
---
[ ![bavarian biscuit joconde mousse with cream custard](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/bavarois-a-la-creme-custard-cerises-et-kaki.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/bavarois-a-la-creme-custard-cerises-et-kaki.jpg>)

##  **bavarian biscuit joconde mousse with cream custard**

Hello everybody, 

Bavarian biscuit Joconde custard mousse: Finally a little time that I steal discreetly while baby sleeps, to post the cake recipe I made for the birthday of my daughter. 

A cake biscuit Joconde biscuit bavarois custard cream that we loved, especially my husband who found that despite all these layers of cream (mousse) it does not have that taste of fat in the mouth, something that makes me sure , a lot of fun. For I must confess to you that my husband is a great critic, nothing escapes him, and not entitled to error, lol. 

So for the birthday of my daughter, I decided to make a Bavarian, can be a lot of people find that the Bavarians take a lot of time to realize, but for me it is not the case ... It is just enough to organize a little, and the cake can be ready in almost 1h30 at most. 

For this, I started by preparing the two fruit jellies, for this Bavarian cake I prepared a cherry jelly, and a jelly khaki (plates). Then I put the cream in whipped cream that I then placed in the fridge. 

I then went on to prepare the joconde biscuit, and when the latter cooled down, I prepared custard cream, so that I could prepare custard custard mousse and proceed to assemble the bavarois ... 

So you want all that in details, here is the recipe for you. 

[ ![bavarian biscuit joconde mousse with cream custard](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/bavarois-creme-custard-cerises-et-kaki-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/bavarois-creme-custard-cerises-et-kaki-1.jpg>)   


**bavarian biscuit joconde mousse with cream custard**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/bavarois-creme-custard.jpg)

portions:  12  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients** Joconde biscuit with pistachios:   
for 2 biscuits of 24 cm 

  * 50 gr of ground almonds 
  * 50 gr raw pistachios pruned 
  * 75 gr of crystallized sugar 
  * 25 gr of flour 
  * 125 gr of eggs   
2 whole eggs + 1 yellow for me 
  * 95 gr of egg whites   
3 egg white 
  * 40 gr of sugar 
  * Orange juice to soak the biscuit 
  * 15 gr of melted and cooled butter 

Cherry jelly: 
  * 150 gr of frozen cherries 
  * 3 to 4 c. soup of sugar   
or more according to your taste 
  * 8 grams of gelatin 

Jelly of persimmon: 
  * 150 gr of well-walled persimmon 
  * 2 to 3 tbsp. soup of sugar   
or more according to your taste 
  * 8 g of gelatin 

custard mousse - custard cream: 
  * 500 gr of milk mix and fresh cream 
  * 1 vanilla pod, scraped 
  * 125 gr of sugar 
  * 120 yellow grams   
4 egg yolks 
  * 14 g of gelatin 
  * 500 gr of whipped cream 



**Realization steps** Joconde cookie preparation: 

  1. In the bowl of a food processor, finely chop pistachios. 
  2. Add the caster sugar and the almond powder and chop again. 
  3. Place this mixture in a salad bowl, add the flour and the whole eggs. 
  4. Whisk for 5 minutes until you get a thick ribbon of dough, 
  5. Now whip the egg whites into the snow, gradually add over 40 grams of sugar and continue whisking until stiff peaks form. 
  6. Mix one-third of the meringue with the first mixture. Add the rest of the meringue and gently stir in, leaving the mixture well ventilated. 
  7. Add the melted butter and mix gently until it is all incorporated. 
  8. Divide this dough in half and spread it in a regular layer, in two round baking molds   
for my part I made a biscuit of almost 25 cm and another of 23 cm. 
  9. Bake at 180 degrees C for about 12 minutes or until they turn a light golden color on top. 
  10. let the cakes cool and keep them in the refrigerator until you assemble the Bavarian. 

prepare the cherry jelly: 
  1. soften gelatin in iced water 
  2. reduce thawed cherries partially pureed 
  3. place them in a saucepan with the sugar and bring to a boil. 
  4. When the puree comes to a boil, lower the heat and add the gelatin. whisk to completely dehydrate the gelatine in the puree. 
  5. pour this jelly into a mold of almost 23 cm and place in the freezer. 

prepare the kaki jelly: 
  1. soften gelatin in iced water 
  2. reduce the kakis well mashed walls 
  3. place them in a saucepan with the sugar and bring to a boil. 
  4. When the puree comes to a boil, lower the heat and add the gelatin. whisk to completely dissolve gelatin in mashed potatoes. 
  5. pour this jelly into a mold of almost 23 cm and place in the freezer. 

Prepare the custard cream mousse- custard cream: 
  1. In a saucepan, boil the mixture of milk and fresh cream with the vanilla bean and half of the sugar. 
  2. At the same time whisk the yolks and the other half of the sugar in a bowl. 
  3. Soften the gelatin in cold water. 
  4. When the fresh cream milk mixture comes to a boil, pour gently over the mixture of egg yolks and sugar while continuing to whisk. 
  5. Return the mixture to the saucepan and cook until the cream slightly thickens and coats the back of the spoon. 
  6. Filter this cream in a fine strainer in a clean bowl. Add the softened gelatin and whisk. 
  7. Place this bowl in an ice bath so that it cools faster. 
  8. When it is cold to the touch, add the whipped cream and mix gently until it is all incorporated. 

Assembling the biscuit:   
I use a circle pastry adjustable, that I set at almost 24 cm. 

  1. cut the ends of the cake so that it is the size of the circle, and place them on a presentation mold. 
  2. soak it well with orange juice (not too much) 
  3. gently pour some English cream mousse, to cover the cake 
  4. place over the disk of cherry jelly, and exert a gentle pressure to let the air escape. 
  5. Cover once more with a layer of English cream mousse. 
  6. place the second Joconde cookie, soak it with a little orange juice. 
  7. cover with a layer of English cream mousse. 
  8. place the kaki jelly disc now. 
  9. finally cover with another layer of English cream mousse 
  10. decorate the cake according to your taste with the rest of the mousse 
  11. and place in the fridge for at least 3 hours. 
  12. to remove the circle, warm it up a bit by using a hair dryer, and remove the circle gently 



[ ![bavarian biscuit joconde mousse with cream custard](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/bavarois-creme-custard-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/bavarois-creme-custard-001.jpg>)

J’espère que je n’ai rien oublié, sinon faites le moi savoir svp. 
