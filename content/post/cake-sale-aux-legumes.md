---
title: salty cake with vegetables
date: '2011-08-19'
categories:
- Cupcakes, macaroons, and other pastries
- Algerian cakes with icing
- gateaux algeriens- orientales- modernes- fete aid
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/cake-sale-aux-legumes-033_thumb1.jpg
---
![dirty cake with vegetables 033](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/cake-sale-aux-legumes-033_thumb1.jpg)

##  salty cake with vegetables 

Hello everybody, 

with this infernal heat, in this month of Ramadan, in Algeria, we eat almost nothing, so much the first thing we consume is water at will. 

for me it is a velvet, always accompanied by a light starter, today is a salty cake super fondant with vegetables, frankly, we feast well. 

then you want the recipe? already my mother and my sister-in-law have taken note:   


**salty cake with vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/cake-sale-aux-legumes-034_thumb1.jpg)

**Ingredients**

  * 200 gr of flour 
  * 3 eggs 
  * 1 sachet of baking powder 
  * 100 ml of milk 
  * 100 ml of oil 
  * a pinch of salt 
  * a pinch of black pepper 
  * 1 carrot 
  * ½ courgette 
  * 1 handful of pitted green olives 
  * 1 handful of pitted black olives 
  * 2 portion of cow cheese that laughs 
  * a little gruyère (optional) 



**Realization steps**

  1. clean the vegetables, cut them in, and boil just a little in salt water 
  2. cut the olives into slices, and reserve 
  3. cut the cheese and cheese 
  4. whisk the eggs, add the milk and oil 
  5. add salt and pepper 
  6. Mix the flour and yeast 
  7. add this mixture gently to the eggs 
  8. flour the mixture of vegetables, and olive in a colander and shake 
  9. incorporate it into the previous mix 
  10. pour into a cake mold butter or wrap with baking paper 
  11. place in a preheated oven at 180 degrees for almost 45 minutes 
  12. Check the cooking, then let cool a few minutes before unmolding. 



bonne journée, et saha ramdankoum 
