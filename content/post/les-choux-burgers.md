---
title: Cabbages burgers
date: '2015-12-14'
categories:
- appetizer, tapas, appetizer

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-burgers.jpg
---
[ ![Cabbages burgers](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-burgers.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-burgers.jpg>)

##  Cabbages burgers 

Hello everybody, 

Here with what I dazzle every time my guests, I make them cabbage burgers for the aperitif. They are just gorgeous these innovative burgers has crunchy texture. In fact, I replaced the burgers with a cabbage paste to get out of the ordinary. These cabbages burgers not only that they are good, they are also super presentable. Promise that you will enjoy them ....   
For the garnish you can garnish them according to your taste ... Here it is cabbage burgers with minced meat. We go to the recipe? Thank you for your support, **Tema's cuisine** . 

**Cabbages burgers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/les-choux-Burgers.jpg)

portions:  6  Prep time:  30 mins  cooking:  25 mins  total:  55 mins 

**Ingredients** For cabbage paste: 

  * 100 g of butter. 
  * 150 g flour. 
  * ¼ L of water. 
  * from 3 to 4 eggs. 
  * pinch of salt. 
  * a bit of milk. 
  * Sesame seeds. 

For garnish: 
  * some green salad leaves. 
  * slices of tomato. 
  * cooked slices of minced meat (about 300 g of salt and pepper ground meat mixed with 1 grated onion and parsley chopped into small slices and cooked either in a pan or roasted). 
  * slice of cheese to choose. 
  * mayonnaise. 
  * ketchup 



**Realization steps** cabbage paste: 

  1. in a saucepan pour the water, add the butter and pinch of the salt and put it on the bonfires. 
  2. as soon as the mixture begins to boil, remove it from the heat and add the flour at one time, 
  3. mix quickly with the whisk or a wooden spoon to avoid lumps. 
  4. put again the pan again on a soft fire and mix until obtaining a paste which does not stick to the walls of the pan. 
  5. move the resulting dough into a salad bowl and let it warm. 
  6. stir in the eggs 1 by 1 and mix gradually. You will get a smooth dough. 
  7. to have sprouts that look like a burger bread I give you a little trick that I myself use, put in a small cup a little milk, take two tablespoons. Dip the two spoons in milk and take a little of the dough try to shape a ball using the two spoons. 
  8. place your cabbages in a baking dish lined with baking paper. 
  9. brush the puffs with a brush with milk and sprinkle the top with a little sesame seeds. 
  10. bake the cabbages at 200 degrees C for 20 to 30 minutes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-burger-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-burger-1.jpg>)
  11. after cooking leave them in the oven off until the cabbages get cold. 
  12. we go to dressage, as I already told you you can garnish them to your taste. 
  13. Cut the cabbage with a knife   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-burger-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-burger-2.jpg>)
  14. put on each half of cabbage, a little salad, a slice of tomato, mayonnaise and ketchup, a slice of minced meat and finally the cheese. 
  15. Close the cabbages and that's ready to eat. 


