---
title: flat mullet of Algerian cuisine
date: '2017-05-17'
categories:
- Algerian cuisine
- recipe for red meat (halal)
- ramadan recipe
tags:
- Minced meat
- Ramadan
- kofta
- Ramadan 2016
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Mhawet-cuisine-algerienne.jpg
---
[ ![flat mullet of Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Mhawet-cuisine-algerienne.jpg) ](<https://www.amourdecuisine.fr/article-mhawet-plat-de-la-cuisine-algerienne.html/mhawet-cuisine-algerienne>)

##  flat mullet of Algerian cuisine 

Hello everybody, 

Here is the mhawet, or mhawet a delicious dish of Algerian cuisine, which is always present on our Ramadan table at least once a week. My mother always prepared it when we received guests, I remember that I was always there to help her to cover the quarts of hard-boiled eggs carefully ground meat well scented with garlic and a little caraway. 

Then I had to cook them in a bath of hot oil, until the minced meat took a nice color, and cooked inside, I took advantage of my mother's back to enjoy a piece, hihihihi , yes always greedy. 

You can see here the most simplified version of this dish, the chicken muffler: 

{{< youtube TuKoDXhC9D4 >}} 

In any case, this dish is often asked by my husband, besides he likes all the dishes with minced meat, between us, who does not like minced meat? 

Today I share with you this recipe for mhawk or ground meat stuffed with boiled eggs that I made during the month of Ramadan, and that I had completely forgotten. 

[ ![flat mullet of Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Mhawet-plat-de-la-cuisine-algerienne.jpg) ](<https://www.amourdecuisine.fr/article-mhawet-plat-de-la-cuisine-algerienne.html/mhawet-plat-de-la-cuisine-algerienne>)   


**mhawet: dish of Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mhawet-viande-hachee-aux-oeufs-durs.jpg)

Cooked:  Algerian  portions:  4-5  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients** for the sauce: 

  * a glass of chickpeas deceived the day before, or in a box 
  * 400 g mutton 
  * 1 onion 
  * salt and black pepper 
  * 2 to 3 tablespoons of table oil 

for the dersa: 
  * 1 pepper (if not replace with the half of a small pepper) 
  * garlic 
  * salt 

for minced meat: 
  * 5 eggs 
  * 350 g minced meat 
  * 1 cloves minced garlic 
  * ½ bunch of chopped parsley 
  * ½ cup of cumin 
  * 1 egg yolk 
  * salt and black pepper 
  * crumb of wet bread with milk (it gives a softer meat) 
  * of flour 
  * oil for frying 



**Realization steps**

  1. Prepare hard-boiled eggs. 
  2. Prepare the ground meat by mixing it with all the ingredients 
  3. cut the boiled eggs in four lengthwise 
  4. Coat each quarter of egg with minced meat, roll in a little flour, then fry in a hot oil, cook on medium heat. 
  5. place your pieces on paper towels. 
  6. Prepare the sauce, chop the onion and fry in a little oil 
  7. Add the meat pieces salt and black pepper, 
  8. crush chilli, garlic and salt with a mortar 
  9. add the dersa to the meat, let go a little, add the chickpeas and cover the equivalent of 1 liter of water.   
I prefer to use the pressure cooker, 
  10. Close and cook. 
  11. When the sauce is ready, place it in a dish, arrange the meatballs and enjoy. 



[ ![flat mullet of Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mhawet-boulettes-de-viande-hachee-aux-oeufs.jpg) ](<https://www.amourdecuisine.fr/article-mhawet-plat-de-la-cuisine-algerienne.html/mhawet-boulettes-de-viande-hachee-aux-oeufs>)
