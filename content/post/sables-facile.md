---
title: easy sandblasted
date: '2012-12-23'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- dips and sauces
- houriyat el matbakh- fatafeat tv
- vegetarian dishes

---
Hello everybody, 

of **delicious shortbread** good melting and very tasty, with these bursts of **pistachios** crispy, these [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) , are always requested by my guests, and I do not fail to realize them with a lot of love. 

a recipe **easy** , **fast** , and **successful** every time. and if you like them [ shortcakes ](<https://www.amourdecuisine.fr/categorie-12344749.html>) , [ Algerian biscuits ](<https://www.amourdecuisine.fr/categorie-11814728.html>) , and [ dry cakes ](<https://www.amourdecuisine.fr/categorie-11814728.html>) , follow me…. 

Ingredients: 

  * 250 gr of butter at room temperature 
  * 100 gr of cornflour 
  * 125 gr of icing sugar 
  * 50 gr grilled pistachios and roughly crushed 
  * 2 egg yolks 
  * vanilla, or pistachio aroma. 
  * 1 sachet of baking powder 
  * flour according to your need 



decoration: 

  * apricot jam 
  * crushed pistachios (almost 50 grs) 
  * coconut 


  1. in a bowl, whip the butter and sugar until the mixture doubles in volume, and is very creamy. 
  2. add the egg yolks and continue beating. 
  3. stir in maizena mixed with baking powder and vanilla 
  4. dry the mixture with the flour, to see a beautiful soft and malleable dough. 
  5. spread the dough on a floured work surface, has a thickness of 4 mm 
  6. cut with a piece of your taste, for me it was a piece square 
  7. place your cakes delicately on a floured baking sheet 
  8. cook 10 to 15 minutes at 180 degrees in a preheated oven 
  9. Take out of the oven and let cool down 
  10. decorate the pieces with the hole at the top, with the help of a brush, with deliue jam with a little orange blossom water. 
  11. sprinkle top with a little coconut and pistachios 
  12. assemble each 2 pieces with jam 
  13. fill the cavity with jam, and leave for at least half a day 
  14. can be well preserved in a hermitic box (if you will resist) 



follow me on: 

Or on 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

thank you. 

bonne journee. 
