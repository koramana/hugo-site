---
title: chilli hot chicken - spicy chicken in the oven
date: '2015-07-17'
categories:
- chicken meat recipes (halal)
tags:
- Onions
- Oven
- Spice
- Pestle
- Olive oil
- marinades
- Economy Cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/chilli-poulet-001_thumb1.jpg
---
##  chilli hot chicken - spicy chicken in the oven 

Hello everybody, 

Since I discovered the baking bags I do not stop making delights with it. No need for liquid, no need for too much fat, you place your ingredients, in the bag, you close, you make two or three holes in the bag so that it does not explode, and after 45 min, you have a dish full of flavors, huuuuuuuuuum 

The first few times, I made chicken vegetables in these bags, I do not tell you the taste, because no need to put the water, and it is not a steam cooking, and you have at the end the flavor of all your mixed vegetables, without losing their qualities. 

![chicken chilli 001](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/chilli-poulet-001_thumb1.jpg)

**chilli hot chicken - spicy chicken in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/chilli-poulet-005_thumb1.jpg)

portions:  4  Prep time:  10 mins  cooking:  45 mins  total:  55 mins 

**Ingredients** Recently, I introduced my spicy roast chicken to my husband, and since then he has not stopped demanding that: 

  * chicken legs or whole chicken (with skin) 
  * 1 C. mustard soup 
  * 2 tbsp. chili powder (chilli powder) 
  * ½ c. black pepper 
  * salt 
  * 1 fresh tomato cut into pieces 
  * 1 little thyme 
  * 2 cloves garlic 



**Realization steps** you can marinate your chicken 1 h to 2 with these ingredients, or if you're in a hurry, do not worry, it's great 

  1. place all your ingredients in the bag, make 2 to 3 holes, and place in an ovenproof pyrex container, put in a preheated oven. 
  2. notice, there is no oil and there is no water, but when cooking, you will find a juicy sauce in your bag, and very very good. 
  3. cook for 45 minutes, put your bag in a deep plate, keep the sauce, and if like me you like a golden color of your chicken, place it in the oven to make it brown for 10 min. 
  4. at the exit, bake the chicken pieces with the sauce, and decorate your plate according to your taste. 



attention is piquaaaaaaaaaaaaaaaaaaaaaaaaaaaa 

is it Mexican ????? 

hihihihihihi 

un régal, et vous êtes invités (es) 
