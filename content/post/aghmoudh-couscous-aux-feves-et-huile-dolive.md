---
title: Aghmoudh Couscous with beans and olive oil
date: '2018-05-10'
categories:
- couscous
- Algerian cuisine
- vegetarian dishes
- recipes of feculents
tags:
- Algeria
- dishes
- Vegetarian cuisine
- Easy cooking
- Semolina
- Full Dish
- Steam cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/couscous-aux-f%C3%A8ves-2-664x1024.jpg
---
![Aghmoudh: Couscous with beans and olive oil](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/couscous-aux-f%C3%A8ves-2-664x1024.jpg)

##  Aghmoudh Couscous with beans and olive oil 

Hello everybody, 

Here is a traditional dish that comes from our beautiful kabylie Couscous with beans and olive oil known as Aghmoud. If you go to any small village in Kabylie, you will be amazed at the variations and different ways to prepare and present the couscous. We do not stop at [ Chicken couscous ](<https://www.amourdecuisine.fr/article-couscous-au.html> "couscous with chicken, Algerian cuisine") , at [ couscous with meat ](<https://www.amourdecuisine.fr/article-25345493.html> "Couscous with meat") , or to [ Kabyle couscous with dried meat ](<https://www.amourdecuisine.fr/article-couscous-kabyle-a-la-viande-sechee-couscous-au-guedid.html> "Kabyle couscous with dried meat: couscous with guedid") . Couscous is a dish almost every day in some families, who even adopts it as a meal for dinner, or to vary it every time. 

Like this couscous with beans, you can also see the [ steamed vegetable couscous ](<https://www.amourdecuisine.fr/article-couscous-aux-legumes-amekfoul-cuisine-algerienne.html> "Couscous with vegetables / amekfoul, Algerian cuisine") : [ Amekfoul ](<https://www.amourdecuisine.fr/article-couscous-aux-legumes-amekfoul-cuisine-algerienne.html> "Couscous with vegetables / amekfoul, Algerian cuisine") , another dish very rich and tasty especially with this very particular smell of olive oil of the bled (Zite azzemour robust trees that cover all the mountain bottoms of our beautiful Algeria), it is not the oil of extra virgin olive. Yes, with these steamed couscous, you have to be generous with the amount of olive oil you will put in it. 

Just to tell you, if you made a couscous to eat, and you still have some steamed couscous without sauce, do Aghmoudh, you'll see, it's a delight.   


**Aghmoudh Couscous with beans and olive oil**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/couscous-aux-f%C3%A8ves_.jpg)

**Ingredients**

  * 500 g of couscous 
  * 350 gr of fresh beans with the skin 
  * olive oil 



**Realization steps**

  1. For the preparation of couscous, you can see the video at the bottom or at the top. 
  2. on the video I use a double couscoussier, like that the cut beans are put at the bottom of the top of the couscoussier (the first) and on the second I put the couscous, but you can do everything in a single couscoussier 
  3. salt the beans, and cover and let cook, when the beans are cooked, put on the couscous, and leave, until the steam begins to escape between the grains of the couscous. 
  4. pour this mixture into a large terrine, mix well the beans with couscous under a drizzle of olive oil. 



[ Cooking couscous with steam ](<https://www.amourdecuisine.fr/article-cuisson-du-couscous-a-la-vapeur.html> "Cooking couscous with steam")

{{< youtube VBPmnMTvKCY >}}  {{< youtube n1A2FGkyjq8 >}} 

![Aghmoudh Couscous with beans and olive oil 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/couscous-aux-f%C3%A8ves-1-683x1024.jpg)

Thanks and good appetite 
