---
title: Fritters with jam
date: '2017-02-25'
categories:
- crepes, beignets, donuts, gauffres sucrees
- gateaux algeriens frit
- recettes sucrees
tags:
- Boulange
- Algeria
- To taste
- desserts
- Pastry
- Cakes
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/beignets-a-la-confiture-1a.CR2_.jpg
---
[ ![donuts with jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/beignets-a-la-confiture-1a.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-beignets-la-confiture.html/beignets-a-la-confiture-1a-cr2>)

##  Fritters with jam 

Hello everybody, 

These donuts with jam are a marvel, a recipe quickly made, well done, I did not expect such a result, especially with the little stupidity that I made with the ingredients, lol. 

So coming back to nonsense now, the first thing, the amount of baker's yeast, 20 gr seemed a lot, so I asked Cécé, who confirmed that she put 20 grams of yeast baker. I put the baker's yeast, when Cécé told me that it is cubic yeast, I had only instant yeast ... After a search Cécé told me that we must replace 20 gr of yeast cube by a packet of instant yeast so 7 gr, ouhla !!! I put almost triple, hihihihi. 

I was waiting for my dough to rise and I was watching from afar, lest it overflow to the neighbors, hihihih, but it's all well, arriving at the stage before frying, I realize that I do not I have more oil ... I call my husband urgently, luckily for me, he was just at the supermarket to buy jam, because there was more at home, hahahahaha ... I did not even know that I did not have jam, lol 

Nice story for this jam donut recipe, but apparently these stories always come to me with donuts and [ donuts ](<https://www.amourdecuisine.fr/article-donuts-faciles-et-delicieux.html> "Homemade donuts easy and delicious") . 

[ ![donuts with jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/beignets-a-la-confiture-2a.jpg) ](<https://www.amourdecuisine.fr/article-beignets-la-confiture.html/beignets-a-la-confiture-2a>)   


**Fritters with jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/beignets-a-la-confiture.jpg)

portions:  18 to 20  Prep time:  60 mins  cooking:  10 mins  total:  1 hour 10 mins 

**Ingredients**

  * 250 ml warm milk 
  * 550 g flour 
  * 20 g of baker's yeast cube or 7 gr of instant yeast 
  * 60 g of soft butter 
  * 50 g caster sugar 
  * 2 eggs 
  * 1 pinch of salt 
  * oil for frying 
  * jam for fodder 
  * icing sugar for garnish 



**Realization steps**

  1. In the bowl of the robot, put the flour, the yeast and the soft butter 
  2. start kneading. 
  3. Add the caster sugar, salt, eggs and warm milk. 
  4. Knead for 10 to 15 minutes until the dough comes off the sides of the bowl. 
  5. Let the dough rise for 1 hour away from the draft, covering it with a clean cloth.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/levee-de-pate-a-beignets.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/levee-de-pate-a-beignets.jpg>)
  6. Degas the dough on a worktop flour slightly. 
  7. cut dough pieces of almost 50 grams each and make dumplings. 
  8. place the meatballs in a floured tray, flatten them so that you do not have pasty pastries that are too thick.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/IMG_1410.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/IMG_1410.jpg>)
  9. Let rise again 40 minutes. 
  10. Heat oil in a large pan. 
  11. Cook the donuts by watching them until they turn a beautiful golden color on both sides. 
  12. Using a pastry bag, stuff the donuts with jam and sprinkle with icing sugar. 



[ ![donuts with jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/beignets-a-la-confiture-3a.jpg) ](<https://www.amourdecuisine.fr/article-beignets-la-confiture.html/beignets-a-la-confiture-3a>)

I invite you to see another recipe of donuts extra mellow, this time I took the pleasure of stuffing my donuts nutella .... A massacre. 

If you like my videos, do not forget to subscribe to my youtube channel, and share my videos with your friends! 

Now to you the recipe of soft donuts with nutella: 

{{< youtube OilMW7viqDQ >}} 
