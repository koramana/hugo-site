---
title: light and easy cucumber salad recipe
date: '2016-05-18'
categories:
- salades, verrines salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/salade-concombre-et-sesame-034.CR2_thumb1.jpg
---
##  light and easy cucumber salad recipe 

Hello everybody, 

Here is a very delicious light and easy cucumber salad recipe, which I like realized often, this recipe is super original with the touch of sesame grilled over, and a slightly sweet sauce with the addition of honey Acacia. 

To be honest, when there is a salad like that at home I attack directly onions red, I have a great weakness for this vegetable well up. But with the addition of sesame seeds, even cucumbers that are not my big weak, were just good. 

If you're not like me, and you like cucumbers, why not see the [ cucumber salad with yogurt ](<https://www.amourdecuisine.fr/article-salade-de-concombre-carotte-au-yaourt-86830728.html>) , and also the [ tzatziki ](<https://www.amourdecuisine.fr/article-tzatziki-105328008.html>)

**light and easy cucumber salad recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/salade-concombre-et-sesame-034.CR2_thumb1.jpg)

portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 minced cucumbers 
  * ½ small red onion, thinly sliced 
  * a few sprigs of chopped coriander 
  * 1 tablespoon sesame oil. 
  * 2 teaspoons or according to the taste of vinegar 
  * 1 teaspoon and ½ acacia honey 
  * Salt and pepper 
  * 2 teaspoons grilled sesame seeds 



**Realization steps**

  1. in a salad bowl, place the slices of cucumber, red onion and chopped coriander, 
  2. mix until it is evenly distributed. 
  3. In a small bowl, beat sesame oil, vinegar and acacia honey. 
  4. Season the seasoning and add salt and pepper to taste. 
  5. Pour over the cucumbers and mix gently. 
  6. Sprinkle with grilled sesame seeds. 
  7. keep cool and serve. 



merci pour votre visite et commentaire 
