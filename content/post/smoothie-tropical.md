---
title: Tropical smoothie
date: '2015-11-25'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-tropical-1.jpg
---
[ ![tropical smoothie 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-tropical-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-tropical-1.jpg>)

##  Tropical smoothie 

Hello everybody, 

It's a cold duck, but the sun is this morning. I could not help but dream for a moment, close my eyes and see me in the summer season, enjoy the sun, and windows all open to let the beautiful morning breeze wake my children gently. 

I opened my eyes and I saw myself with my robe of hot room, grrrr it is very cold, 4 degrees C thermometer of the kitchen, nevertheless I want to sip the summer! Can we do that? Well yes, with a tropical smoothie, we have the sun in our glass, hihihihih. 

**Tropical smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-tropical.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 1 orange peeled and cut in quarters 
  * 1 banana cut into slices 
  * 2 tablespoons of coconut 
  * 1 mango cubed and frozen 
  * the juice of half a pineapple 
  * half a pineapple cube 



**Realization steps**

  1. I had the pineapple juice extracted in the centrifuge. 
  2. place the coconut in the blinder bowl and reduce to a fine powder.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/noix-de-coco.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/noix-de-coco.jpg>)
  3. place the remaining ingredients in the blinder bowl and mix. 
  4. pass the mixture to the Chinese to have a creamy smoothie and without fiber of mango or orange 
  5. enjoy very fresh. 



[ ![Tropical smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-tropical.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-tropical.jpg>)
