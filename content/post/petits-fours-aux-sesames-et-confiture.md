---
title: petit fours with sesame and jam
date: '2013-09-11'
categories:
- dessert, crumbles and bars
- basic pastry recipes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/petits-fours-aux-sesames-et-confiture1.jpg
---
![canapés aux sesames-and-confiture.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/petits-fours-aux-sesames-et-confiture1.jpg)

Hello everybody, 

Delicious petit fours with sesame and jam made by Lunetoiles, according to a recipe taken from Mrs. Benberim's book "Biscuits". 

The recipe had to be made in silicone molds, and in each imprint there is a cavity, which will fill the petit fours with jam when cooked ... 

So here is the method revisited by Lunetoiles that will allow you to make these cakes without the need for these silicone molds.   


**petit fours with sesame and jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/petits-fours-aux-sesames-et-a-la-confiture-1.jpg)

Recipe type:  cookies,  portions:  30  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 250 g of butter 
  * 100 g of sugar 
  * 1 pinch of salt 
  * vanilla 
  * 380 g flour 
  * apricot jam + red, yellow and green dye 
  * 200 g sesame seeds 



**Realization steps**

  1. Work the butter and sugar into cream, add the pinch of salt, vanilla and flour sifted and mix until a soft dough. 
  2. Form balls of 30 g, coat them with sesame seeds and pack them in silicone molds in the shape of heart, square or other, if not made as in the photo above. 
  3. Bake until golden brown. Let cool and unmold. 
  4. Garnish the middle of each cake with colored jelly or jam. 


