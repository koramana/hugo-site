---
title: Chicken and potato dumplings
date: '2013-12-19'
categories:
- juice and cocktail drinks without alcohol
- cooking fat-burning, slimming recipes
- Detox kitchen
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/quenelles-de-pomme-de-terre-et-poulet-2_thumb.jpg
---
##  Chicken and potato dumplings 

Hello everybody, 

These chicken and potato dumplings, which are between potato croquettes, and chicken meatballs, this potato and chicken dumpling recipe was a great success, my kids really love it, and I admit that my children are very difficult, and the fact that they enjoyed this recipe, without grumbling or boycotting the table, is something that made me really happy. 

a recipe a little improvised, but easy to do, because at first I wanted to make a little maakouda, then fried chicken balls, but finding myself with a small amount of chopped chicken, and mashed potatoes, I think to combine the two mixtures, and here I am with a very very good recipe.   


**Chicken and potato dumplings**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/quenelles-de-pomme-de-terre-et-poulet-2_thumb.jpg)

Recipe type:  entrance, amuse bouche  portions:  6 

**Ingredients**

  * 150 grs of chopped white chicken 
  * 125 grs potato cooked and mashed 
  * 1 chopped onion 
  * 2 tablespoons of oil 
  * 2 cm grated fresh ginger 
  * ½ teaspoon of salt 
  * 1 cup of turmeric coffee 
  * 2 tablespoons chopped coriander 
  * 20 gr of flour 
  * oil for frying 



**Realization steps**

  1. in a frying pan brown the chopped onion in the oil. 
  2. in a container, mix the chopped chicken breast with mashed potato, golden onion, coriander and spices 
  3. make small dumplings in the shape of dumplings. 
  4. boil 1 liter of salt water 
  5. dip the dumplings gently in this for almost 5 min, then remove. 
  6. put on paper towels to remove excess water. 
  7. put these quenelles in flour 
  8. fry these dumplings in a bath of hot oil, until you get a nice golden color 
  9. place the dumplings on paper towels. 
  10. present them on a bed of salad, and enjoy them with, mayonnaise, ketchup, homemade tomato sauce, bechamel, or hummus (my husband really likes hummus) 



bonne dégustation, et a la prochaine recette 
