---
title: artichokes stuffed with minced meat
date: '2012-07-21'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/artichauts-farcis_thumb1.jpg
---
##  artichokes stuffed with minced meat 

Hello everybody, 

here is a very good dish of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , stuffed artichokes, that I do not prepare often, simply because here in Bristol I do not always find artichokes, and if I find frozen artichoke hearts, frankly I do not like at all the taste. 

my husband when he buys beautiful artichokes, no question that I make a dish with, it must be a [ salad ](<https://www.amourdecuisine.fr/categorie-10678933.html>) , that is boiling whole artichokes in salt water with pieces of lemons then prepare a very good good acid vinaigrette, to taste each end of the sheet with ...... a delight. 

so when I went to Algeria, I took my stock there, hihihiihi beautiful flowers, and I made for my husband this salad he loves for almost every day, the first week, but in same time, I hide 3 artichokes to make this tagine stuffed artichokes, that of course my husband does not eat, because quite simply, he does not like the white sauce .... 

do you think it disappointed me ???? Well, not at all, because I really like this dish, and I feasted, hihihihiihi 

a great gourmet, good food. 

So for the photos, it's not the best at all, because first of all, I dropped my camera twice, well it's like that when you have the camera all the time. the kitchen. 

secondo, I'm doing my fast, and so the pictures are taken at night, and the pictures at night, well that's not my strongest point, and thirdly, I was really hungry, I did not even see the final photos before finishing my dish. 

**artichokes stuffed with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/artichauts-farcis_thumb1.jpg)

**Ingredients**

  * 4 to 5 artichoke hearts 
  * 500g minced meat 
  * 1 grated onion 
  * 1 smen coffee 
  * 2 tablespoons of olive oil 
  * 1 packet of black pepper, salt 
  * 1 handful of rice 
  * parsley, 
  * 4 to 5 eggs 
  * 2 cloves of garlic 
  * 1 egg white 
  * salt, pepper, cinnamon. 
  * water. 



**Realization steps**

  1. method of preparation: 
  2. clean artichokes, remove leaves and hay, 
  3. change them to lemon to avoid blackening them 
  4. boil for 10 minutes in salt water 
  5. drain and reserve 
  6. place the rice in boiling salted water and reserve 
  7. Mix the minced meat with the garlic and the chopped perslice, 
  8. add the egg white, salt, pepper, a little cinnamon. mix well, 
  9. form dumplings with half that amount, and cool 
  10. to the remaining ground meat add the drained rice and set aside 
  11. in a pot, fry onion cut into small cube with smen, oil, salt, pepper, and cinnamon, 
  12. Sauté for a long time over low heat, stirring occasionally. 
  13. Cover with hot water, add chopped parsley and cook. 
  14. garnish the artichoke bottoms with the minced meat filling and form a hollow on the inside 
  15. place these funds garnished in the white sauce, 
  16. add the minced meatballs   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/qarnoun-mahchi_thumb1.jpg)
  17. when cooking meat, break an egg in each hollow (it may be small, if you do not flatten the meat well, so think about making a good hollow) 
  18. cook another 5 minutes and remove from heat 



if you like artichokes I also advise you: 

[ golden goat on artichoke background ](<https://www.amourdecuisine.fr/article-chevre-dore-sur-fond-d-artichaut-100098265.html>) . 

[ bourak in bloom with artichoke cream ](<https://www.amourdecuisine.fr/article-fleurs-frites-farcies-a-la-creme-d-artichauts-102277133.html>)

[ pea tagine and artichoke hearts ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-aux-artichauts---tajine-jelbana-103530292.html>)

j’espère que je n’ai oublier aucune étape, car je suis vraiment fatiguée, et il se fait très tard, donc, merci de me dire si vous ne comprenez pas un truc. 
