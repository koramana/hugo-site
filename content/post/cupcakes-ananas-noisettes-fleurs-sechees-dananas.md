---
title: pineapple / hazelnut cupcakes / dried pineapple flowers
date: '2011-03-31'
categories:
- les essais de mes lecteurs

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/03/cupcake-a-la-fraise_thumb1.jpg
---
Hello everybody, 

it's hard enough for me to get to the pc in the morning when I'm working, and it's going slowly, when we have to take care of the children, to eat, to clean up, and then when I take my daughter his school I come back to do the wallpaper, or then paint it, so I do not think that in the middle of all that I can touch the pc, already it was difficult for me just now to validate some comments, answer to some readers, and make visits to some blogger friends, I already have the eyes that close the ............... 

finally to not leave you without recipe, I post you this recipe of my dear Lunetoiles, which on my email since already March 11, I know that you like the recipes of Lunetoiles, and me too, I arrange the photos and I saliva, hihihiihih 

It's pineapple and hazelnut cupcakes, topped with a white chocolate and hazelnut ganache cream and decorated with dried flowers of pineapple !!! 

she was inspired by cupcakes by martha stewart 

**Dried pineapple flowers:**

  1. Cut the top and bottom of the pineapple with a large, sharp knife. 
  2. Cut the sides (peel) Remove the remaining eyes with a Parisian spoon. 
  3. Cut the anans very finely so that the slices is almost translucent. 
  4. Arrange them on a plate covered with a silicone mat or baking. 
  5. Bake at 110 ° C to dry slowly, 30 min, then turn and cook another 30 min (or a little more). 
  6. Just keep an eye on them. Once the slices are shriveled and almost finished dried, transfer them to a muffin pan so that the edges curl and take the shape of a flower. 
  7. Let stand overnight in this manner. To keep the shape of flowers. 



**Frosting (to do the day before)**

Ganache with white chocolate and hazelnuts: 

  * 200 gr of white chocolate with hazelnut chips (or add 2 tablespoons crushed hazelnuts, toasted) 
  * 250 ml whole liquid cream 



Melt the day before, the white chocolate and 50 gr of white chocolate in the microwave, 2 times 30 sec. 

Let cool and add the rest of the cream. Keep cool one night. 

**Pineapple and hazelnut cupcakes**

For 20 cupcakes approx. 

  * 230 gr of flour 
  * 1/2 cuil. coffee baking soda 
  * 1 pinch of salt 
  * 11 tbsp. melted butter and cooled 
  * 1 + 1/4 cuil. coffee vanilla extract 
  * 300 gr of sugar 
  * 2 big eggs 
  * 2/3 cup crushed pineapple, drained 
  * 2/3 cup chopped hazelnuts, toasted 


  1. Preheat the oven to 180 ° C. 
  2. Garnish cupcake cups with boxes. 
  3. In a bowl, mix the flour, baking soda, salt. Put aside. 
  4. In the bowl of an electric mixer, combine the butter, vanilla, sugar and beat until smooth and consistent. 
  5. Add the eggs one by one, mixing well between each addition. 
  6. Blend at low speed, to incorporate the dry ingredients until they are incorporated. 
  7. Slowly add the pineapple and hazelnuts with a spatula, until you obtain a homogeneous mixture. 
  8. Pour the dough into each mold 3/4 of the way up. 
  9. Bake for 20-22 min, until a toothpick inserted in the center comes out clean. 
  10. Cool in the pan for 5-10 minutes, then transfer to a rack and allow to cool completely. 



**Frosting:**

Ganache with white chocolate and hazelnuts: 

The next day beat the ganache with an electric mixer, to have a ganache mounted as for a whipped cream. 

Pour this ganache into a piping bag with a star socket. Garnish your cupcakes and decorate with a dried pineapple flower. 

Keep cool. 

thank you for all your visits, and all your sublime comments and as you like the recipes of luntoiles do not forget: 

Lunetoiles is waiting for your support, voting for her: 

[ the cupcakes in pink ](<https://www.amourdecuisine.fr/article-cupcakes-aux-fraises-a-la-creme-meringuee-68717764.html>)

![cupcake-a-la-fraise thumb](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/cupcake-a-la-fraise_thumb1.jpg)

thank you 
