---
title: chicken and potatoes roasted in the oven
date: '2014-02-15'
categories:
- leaving the experts

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/kebab-006_thumb.jpg
---
#  Oven-Roasted Potatoes. 

Hello everybody, 

very easy to do, and well delicious, the ingredients: 

**chicken and potatoes roasted in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/kebab-006_thumb.jpg)

portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * chicken pieces 
  * potatoes 
  * 1 onion 
  * 1 clove 
  * 1 handful of chickpeas, or more according to your taste 
  * parsley 
  * oil 
  * salt and black pepper 



**Realization steps**

  1. clean the chicken, or chicken pieces, and put them in a pot, add the oil, the finely chopped onion, and the crushed garlic, cook over low heat, add a little parsley, cover all about 1 liter of water, and cook. 
  2. now, peel and clean the potato, cut it in length, a little wider than the normal fries, salt it a little bit. and go to the frying. 
  3. when the chicken is cooked, take it out of the sauce, take an oven-baking tin, place in the frits, sprinkle with half of the sauce, cover the pieces of chicken, and place in a hot oven, to give a beautiful color. 
  4. for people who love eggs, you can beat an egg and pour it on the surface, otherwise, it's like my husband, leave like that. 
  5. before serving, sprinkle each dish with a little sauce, and garnish with chickpeas and parsley. 



bonne dégustation. 
