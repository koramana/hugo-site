---
title: maple apple muffins and walnut crumble
date: '2016-11-03'
categories:
- Unclassified
tags:
- Cookies
- biscuits
- desserts
- To taste
- Easy cooking
- Cakes
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/muffins-pomme-%C3%A9rable-et-crumble-aux-noix-1.jpg
---
![muffins apple-maple-and-nut crumble aux-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/muffins-pomme-%C3%A9rable-et-crumble-aux-noix-1.jpg)

##  maple apple muffins and walnut crumble 

Hello everybody, 

![muffins apple-maple-and-nut crumble aux-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/muffins-pomme-%C3%A9rable-et-crumble-aux-noix-2.jpg)

![muffins apple-maple-and-nut crumble aux-3](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/muffins-pomme-%C3%A9rable-et-crumble-aux-noix-3.jpg)

**maple apple muffins and walnut crumble**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/muffins-pomme-%C3%A9rable-et-crumble-aux-noix-5.jpg)

portions:  10  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** For 10 muffins: 

  * 1 big egg 
  * 60 g of butter 
  * 50 g of blond sugar 
  * 110 g of flour 
  * ½ teaspoon baking powder 
  * ½ teaspoon of cinnamon 
  * 65 g of heavy cream (or yoghurt) 
  * ½ teaspoon of vanilla extract 
  * 80 g maple syrup 
  * 1 golden apple 

For the crumble: 
  * 30 g of butter 
  * 30 g of blond sugar 
  * 30 g of flour 
  * 30 g of nuts 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. In a bowl, melt the butter for 30 seconds in the microwave. Add sugar, egg, vanilla, maple syrup and cream. Mix. 
  3. Stir in flour and baking powder. Mix well. 
  4. Peel the apple and cut it into small cubes. Add it to the dough. 
  5. Fill each imprint of the muffin tin with the dough until ¾. 
  6. Prepare the crumble: sand the butter with the sugar and the flour. Stir in the coarsely mixed nuts. Spread the crumble evenly over the muffins. 
  7. Bake for about 20 minutes (the tip of the knife should come out dry). 
  8. Leave to cool a little and gently unmold. 



![muffins apple-maple-and-nut crumble aux-4](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/muffins-pomme-%C3%A9rable-et-crumble-aux-noix-4.jpg)

As I told you, this round was very successful: 

**List of participants and their recipe  
**
