---
title: pepper rolls
date: '2011-04-13'
categories:
- les essais de mes lecteurs

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/04/2010-08-19-ram2_21.jpg
---
& Nbsp; hello everyone, always with a small appetizer that accompanies our good and delicious chorba, and as the tuna rolls have made their success, we stop asking them at home. I did them again, not one, not two, not three times, but I changed the joke. we stay in simplicity, and we try to be in the budget of each family, in this holy month of Ramadan & nbsp; the ingredients: the dough for rolls already present on the blog here 2 fried peppers 2 fresh tomatoes 2 boiled eggs 1 small box of & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.86  (  1  ratings)  0 

Hello everybody, 

always with a small starter that accompanies our good and delicious chorba, and as the tuna rolls have made their success, we stop asking them at home. 

I did them again, not one, not two, not three times, but I changed the joke. 

we stay in simplicity, and we try to be in the budget of each family, in this holy month of Ramadan 

Ingredients: 

the dough for roules already present on the blog [ right here ](<https://www.amourdecuisine.fr/article-les-roules-au-thon-55009430.html>)

2 fried peppers 

2 fresh tomatoes 

2 boiled eggs 

1 small tin of tuna 

prepare a tomato sauce with both tomatoes after peeling, cut into small pieces and cooked in a little oil with a little garlic, and seasoned according to your taste 

![https://www.amourdecuisine.fr/wp-content/uploads/2011/04/2010-08-19-ram2_21.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/2010-08-19-ram2_21.jpg)   


after the dough has doubled in volume, degas it, and spread it in a large rectangle, decorate with the tomato sauce, then with the peppers cut into small pieces, the crushed hard-boiled eggs and then the tuna crumbled 

roll the dough on itself, do not forget to brush the last side of the dough with a little egg so that it does not open. 

cut pieces of 2 cm wide, it's a bit difficult, because the stuffing may come out, but you can arrange it 

place your rolls on an oiled plate, and let rise a little. 

before placing in the oven, brush the rolls with a little egg yolk. 

cook in a hot oven. 

and enjoy the delight 

Thank you for your feedback, 

thank you for your subscription to the newsletter, it makes me really happy 

et merci beaucoup aux filles qui me cliquent sur les liens sponsorises, vous m’aider beaucoup merci les filles 
