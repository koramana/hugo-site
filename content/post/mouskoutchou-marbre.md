---
title: marbled mouskoutchou
date: '2016-02-28'
categories:
- dessert, crumbles and bars
- Algerian cakes- oriental- modern- fete aid
- cakes and cakes
- sweet recipes
tags:
- Algerian cakes
- Algeria
- Cake
- Genoese
- Chocolate cake
- Pastry
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mouskoutchou-marbr%C3%A9.jpg
---
[ ![marbled mouskoutchou](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mouskoutchou-marbr%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mouskoutchou-marbr%C3%A9.jpg>)

##  marbled mouskoutchou 

Hello everybody, 

Back to the sweet recipes and it is with a great classic of the Algerian cuisine, the famous mouskoutchou that Lunetoiles shares with us this nice revisit that makes the buzz here and there! And that's what she tells us about this sublime marbled mouskoutchou: 

I made a marbled mouskoutchou today and I kept the method for the realization of [ Madeleines ](<https://www.amourdecuisine.fr/article-les-madeleines-a-la-coque-en-chocolat.html>) which is to put a portion of glucose syrup in the dough, I do not know, but I find that it brings a lot to the final texture of the cake. I really wanted to test and see the result with cakes and cakes, so that's why I put half sugar and half glucose syrup. 

[ ![marbled mouskoutchou 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mouskoutchou-marbr%C3%A9-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mouskoutchou-marbr%C3%A9-2.jpg>)

**marbled mouskoutchou**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mouskoutchou-marbr%C3%A9-3.jpg)

portions:  8  Prep time:  15 mins  cooking:  50 mins  total:  1 hour 5 mins 

**Ingredients**

  * 4 eggs 
  * 130 g of sugar 
  * 130 g of glucose / fructose syrup 
  * 1 C. powdered vanilla 
  * 260 ml of fermented milk (buttermilk) 
  * 200 ml of neutral oil 
  * 1 sachet of baking powder 
  * 330 g flour 
  * 3 c. unsweetened cocoa 
  * icing sugar for decoration 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. Beat the eggs with the sugar and vanilla until the sugar is dissolved. 
  3. Add the glucose syrup and mix. 
  4. Add milk and oil and mix well. 
  5. Stir in yeast and sifted flour and whisk until smooth and evenly prepared. 
  6. Pour the third of the preparation into a small bowl, add the cocoa and mix well. 
  7. Pour the two pasta alternately in the buttered and floured mold. 
  8. Using a toothpick go back and forth to get a nice marbling. 
  9. Bake for about 20 minutes then lower the temperature to 160 ° C and continue cooking for 30 minutes. 
  10. The cake should be well inflated and golden. 
  11. Monitor the cooking, check if the cake is cooked by inserting a toothpick, if it comes out clean, it's cooked. 
  12. Once the cake has cooled, unmold it on a plate and sprinkle with icing sugar. 



[ ![marbled mouskoutchou 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mouskoutchou-marbr%C3%A9-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mouskoutchou-marbr%C3%A9-1.jpg>)
