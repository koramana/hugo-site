---
title: avocado salad with tuna
date: '2014-12-11'
categories:
- salads, salty verrines
tags:
- inputs
- la France
- Easy cooking
- aperitif
- Aperitif
- Buffet
- Tapas

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-d-avocat-020_thumb1.jpg
---
![avocado salad with tuna](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-d-avocat-020_thumb1.jpg)

##  avocado salad with tuna 

Hello everyone, 

I'm posting you today a little [ salad ](<https://www.amourdecuisine.fr/categorie-10678933.html>) which I prepared a moment ago. It's a salad that we like a lot at home, and I often prepare it. 

It must be said that the avocado marries perfectly with tuna, finally the avocado accompanies super well any other food. If like me you like the lawyer, you should definitely love the [ guacamole ](<https://www.amourdecuisine.fr/article-recette-du-guacamole.html> "guacamole") also. 

You can also try the recipe [ mimosa egg with avocado cream ](<https://www.amourdecuisine.fr/article-oeuf-mimosa-creme-d-avocat.html> "Avocado mimosa-avocado egg") , the [ avocado quinoa salad ](<https://www.amourdecuisine.fr/article-salade-de-quinoa-asperge-et-avocat.html> "asparagus and avocado quinoa salad") , or the [ avocado terrine with salmon ](<https://www.amourdecuisine.fr/article-terrine-d-avocat-au-saumon-fume.html> "smoked salmon advocado terrine")   


**avocado salad with tuna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-d-avocat-026_thumb1.jpg)

portions:  2  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 1 lawyer 
  * 1 green onion 
  * ½ can of tuna in the oil 
  * the juice of 1/2 lemon 
  * salt 
  * chopped parsley 



**Realization steps**

  1. Cut the avocados in half, remove the core. With the knife, draw inside the crossed lines, which will form cubes, 
  2. With a spoon, remove the fruit, you will see, it's magic, the cubes are perfect, is the flesh is intact 
  3. put in a bowl, add the tuna in pieces, the onion cut into small pieces, the chopped parsley, season with lemon juice and salt. 
  4. fill the flesh of the avocado halves with this stuffing, and present it with a very good salad 



[ ramadan recipe ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>)

bonne dégustation 
