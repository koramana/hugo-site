---
title: tapas / appetizer / stuffed olives
date: '2012-12-23'
categories:
- Bourek, brick, samoussa, slippers

---
Hello everybody, 

very delicious **tapas** , which is prepared in two, three tricks, and you have a nice recipe for **aperitif,** Stuffed olives with a delicious cream cheese, covered with a beautiful **crispy layer** . 

They are [ appetizers and appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) I always like to present to accompany a fresh [ salad ](<https://www.amourdecuisine.fr/categorie-10678933.html>) , for a **light dinner** , despite my husband's love for the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) but he likes it a lot, these little ones [ appetizers and appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) . 

Ingredients: 

  * 200 g olives (large olives) green, drained and rinsed 
  * 200 gr of ricotta cheese. 
  * 50 gr of goat cheese. 
  * 1 clove garlic crushed. 
  * 1 tablespoon finely chopped parsley. 
  * cup of flour. 
  * 1 egg white slightly beaten. 
  * 1 cup of bread crumbs. 
  * oil for frying. 



method of preparation: 

  1. In a small bowl, mix ricotta, goat cheese, chopped parsley, black pepper and garlic. Mix well. 
  2. place this mixture in a piping bag and using an opening cup as wide as the olives hole. fill the cavities of the olives with this stuffing. 
  3. turn on the fryer, or heat a bath of oil. coat the olives in the flour, then in the egg white, then in the bread crumbs. 
  4. do this until the olives are all breaded. start cooking by placing a handful of olives in the hot oil. watch the cooking because it goes very fast. 
  5. Continue until you brown all the olives. place on a paper towel, then present the olives on skewers with a fresh salad. 



bonne dégustation et merci 
