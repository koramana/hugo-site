---
title: Poultry's liver terrine
date: '2017-11-07'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- ramadan recipe
- chicken meat recipes (halal)
tags:
- Chicken
- Amuse bouche
- accompaniment
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Terrine-de-foie-de-volaille-2.jpg
---
[ ![poultry's liver terrine](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Terrine-de-foie-de-volaille-2.jpg) ](<https://www.amourdecuisine.fr/article-terrine-de-foies-de-volaille.html/terrine-de-foie-de-volaille-2>)

##  Poultry's liver terrine 

Hello everybody, 

This chicken liver terrine recipe was a huge hit with my husband and my family in Algeria, especially during the month of Ramadan, I had a nice reserve in jars in the fridge, and my husband was feasted, well not only him, I had my share. 

This terrine of chicken liver is super easy to make, you can make a nice amount, in advance that you can keep carefully in the fridge, for at least 3 weeks (if you resist). 

The recipe originally contained a lot of butter, it seems that it gives more smoothness to the terrine of chicken liver and that it spreads this pâté easily on a toast, moreover in breadth it went like butter peanuts in my children, and I admit that it surprised me a lot. But personally, I preferred not to put too much butter in my recipe. The recipe also contained brandy, which I preferred to replace with boiling chicken, and the end result was just a treat. 

One important thing to mention, do not cook too much poultry, give them just enough time to change the color, it gives a clearer color to the terrine, and a supreme taste. 

**Poultry's liver terrine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/terrine-de-foie-de-volaille-3.jpg)

portions:  5-6  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 500 g chicken liver 
  * 90 g butter at room temperature 
  * 1 onion, finely chopped 
  * 1 crushed garlic clove 
  * 1 tablespoon chopped fresh thyme 
  * 1 cube of chicken broth 
  * 80 ml of water 
  * 60 ml of fresh cream. 
  * salt and pepper 



**Realization steps**

  1. In a large pan, melt half of the butter over medium heat. 
  2. Add the onion and garlic and cook over low heat until they turn a nice color. 
  3. Add the liver in pieces and thyme, then stir over medium heat until they change color. 
  4. Add water, crumble over the Knorr cube and simmer for 2 minutes over low heat. then remove from the fire. 
  5. Place the livers and liquid in the robot's bowl and blend until smooth. 
  6. Add the other half of the butter (at room temperature) and the cream, season with salt if it is lacking and black pepper, 
  7. Mix again until everything is well incorporated. 
  8. Pour into a cake mold covered with cling film, or in glass jars, smooth the surface. Cover with a thin layer of melted butter for better preservation and refrigerate until firm. 
  9. Serve with toast. 



[ ![chicken liver terrine](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/terrine-de-foie-de-volaille-1.jpg) ](<https://www.amourdecuisine.fr/article-terrine-de-foies-de-volaille.html/terrine-de-foie-de-volaille-1>)
