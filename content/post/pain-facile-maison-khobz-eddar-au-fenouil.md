---
title: 'easy homemade bread: khobz eddar with fennel'
date: '2012-07-16'
categories:
- gateaux, et cakes
- idee, recette de fete, aperitif apero dinatoire
- sweet recipes

---
hello everyone, here is a very delicious homemade bread recipe aka Khobz eddar in Arabic, a very soft bread, very light, very airy, and above all, too good. and on this blog you will find: Galette matlou3 - مطلوع Algerian Arabic bread Khobz eddar - homemade bread Khobz eddar - homemade bread Khobz eddar butter - homemade bread Khobz has flour - bread with flour - homemade bread خبزالدار بالفرينة & nbsp; home made bread with three seeds خبز الدار بالينسون, السمسم و الخبة السودا & nbsp; for a single cake: 250 grs of semolina 250 grs & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 0.35  (  1  ratings)  0 

Hello everybody, 

here is a very delicious recipe of  **homemade bread** alias  **Khobz eddar** in Arabic, a very soft bread, very light, very airy, and above all, too good. 

and on this blog you will find: 

##  [ Galette matlou3 - مطلوع Algerian Arabic bread ](<https://www.amourdecuisine.fr/article-41774164.html>)

##  [ Khobz eddar - homemade bread ](<https://www.amourdecuisine.fr/article-25345314.html>)

##  [ Khobz eddar - homemade bread ](<https://www.amourdecuisine.fr/article-25345314.html>)

##  [ Khobz eddar with butter - homemade bread ](<https://www.amourdecuisine.fr/article-25345448.html>)

##  [ Khobz has flour - bread with flour - homemade bread خبزالدار بالفرينة ](<https://www.amourdecuisine.fr/article-35092800.html>)

for a single cake: 

  * 250 grs of semolina 
  * 250 grams of flour 
  * 1 tablespoon of sugar 
  * 1 tablespoon baker's yeast (instant) 
  * 1 cup of baking powder 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 3 tablespoons of oil 
  * 1 tablespoon of black seed 
  * 1 tablespoon of sesame seeds 
  * 1 tablespoon of anise seeds 
  * 1 whole egg 
  * lukewarm water as needed 



for garnish: 

  * 1 egg yolk 
  * 1 cup of milk 
  * 3 drops of vinegar (to avoid the smell of eggs) 
  * black seed, sesame and anise. 



method of preparation: 

  * if you use the bread maker, add all the mixes in the bowl avoiding the direct contact of salt and baker's yeast 
  * if you do it by hand: 


  1. mix the flour, the semolina, the sugar, the salt, and the oil, to sand well. 
  2. add over the two yeasts, and the milk powder, as well as the mixture of grains at your disposal and mix 
  3. make a well, and pour the scrambled egg 
  4. start jetting the water at medium temperature and pick up the dough 
  5. knead the dough by tearing the ball, do not roll, otherwise you will have a very elastic bread 
  6. when the dough is soft and a little sticky in your hands, place it in an oiled place, and cover 
  7. let it rise well, and degas the dough by kneading it in the same previous way, 
  8. let another raise another, then degass again 
  9. place the dough in an oiled baking sheet. 
  10. lower to 1.5 cm thick 
  11. cut the edge of your dough using a roulette or a chisel as in the picture above 
  12. pinch the two ends of the strips with your thumb and index it to finally get the shape of a flower 
  13. cover and let the volume double again 
  14. garnish the flower with the yolk whipping with a little milk and vinegar 
  15. and decorate with sesame, nigella and anise seeds 
  16. cook in a preheated oven at 200 degrees C for 15 to 25 minutes or longer depending on the capacity of your oven 



thank you for your visits and comments 

and saha ramdanekoum 

bonne journée 
