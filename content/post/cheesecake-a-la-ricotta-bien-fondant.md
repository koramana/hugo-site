---
title: cheesecake with ricotta fondant
date: '2018-03-01'
categories:
- cheesecakes and tiramisus
- dessert, crumbles and bars
- sweet recipes
tags:
- Holidays
- desserts
- Cakes
- Cake
- To taste
- Algerian cakes
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cheesecake-009_thumb_1.jpg
---
![cheesecake with ricotta fondant](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cheesecake-009_thumb_1.jpg)

##  cheesecake with ricotta 

Hello everybody, 

I love cheesecakes, and I try with all the cheese, today it's a ricotta cheesecake that I share with you. he is super good, super fondant and endless sweetness. 

I presented this ricotta cheesecake with a good filet of [ Strawberry coulis ](<https://www.amourdecuisine.fr/article-sauce-de-fraises-coulis-de-fraises-facile-et-rapide.html>) but you can decorate with any other sauce.   


**cheesecake with ricotta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cheesecake-006_thumb.jpg)

**Ingredients**

  * 125 gr of biscuits (digestive for me) 
  * 100 grams of candied fruit (orange for me) 
  * 65 gr of melted butter 
  * 150 ml of fresh cream 
  * 575 gr of ricotta cheese (replace with any other cheese) 
  * 100 gr of sugar 
  * vanilla 
  * 2 large eggs 
  * 225 gr strawberries 
  * 25 to 50 grams of sugar 
  * zest and juice of an orange 



**Realization steps**

  1. preheat the oven to 170 degrees C. 
  2. cover the bottom of a mold with a removable base of 20 cm diameter, with baking paper 
  3. place the crushed cookies and the candied fruit in the blinder, add the 50 grs of melted butter and mix 
  4. pour it all into the mold and pile up gently (do not over-pile or the base will be too hard) 
  5. always in the blinder, mix the cream, the ricotta cheese, the sugar, the vanilla and the eggs 
  6. add the remaining melted butter, and mix again for a few seconds. 
  7. pour the mixture on the basis of biscuits 
  8. place all in the preheated oven, and cook for almost 1 hour. until the cake's turn is golden, but the heart of the cake remains quivering. 
  9. turn off the oven, and let the cheese cake cool in it. 
  10. place in the fridge for at least 8 hours 
  11. wash the strawberries, place them in the blinder with 25 grs of sugar, the juice and the orange zest 
  12. all well, add the rest of the sugar, according to your taste, not to oblige all the quantity. 
  13. go to a Chinese to remove the seeds, and put in the fridge 
  14. present your cheese cake with this delicious coulis, and enjoy 



[ ![cheesecake with ricotta fondant](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/2011-05-17-cheesecake_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/07/2011-05-17-cheesecake_2.jpg>)
