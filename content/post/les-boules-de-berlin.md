---
title: The balls of Berlin
date: '2011-02-17'
categories:
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/02/boule-de-Berlin-1.jpg
---
![Fathead Berlin-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/boule-de-Berlin-1.jpg) ![balls-of-berlin-2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/boules-de-berlin-2.jpg)

![boules de Berlin-4.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/boules-de-Berlin-4.jpg)

because as I told you, the recipe is so perfect that you do not need to add anything. 

so I say it aloud: recipe **tested and approved**

now, after all this talk, we go to the ingredients: 

  * 500 gr of flour 
  * 2 big eggs 
  * 1/2 glass of sugar (100 gr0 
  * 2 bags of vanilla sugar 
  * 1/2 glass of oil (100 ml) 
  * 3 tablespoons baker's yeast (30 gr) 
  * 1/2 glass of milk or water (100 to 150 ml) 
  * 1 big pinch of salt 



I allow myself to make a copy / paste: 

Mix all the ingredients and work the dough with the robot, MAP (bread machine) or by hand: it must have the consistency of a soft bread dough that does not stick to the fingers (initially it is normal that the dough sticky: it must adhere to the wall of the bowl or the tank to end by detaching) 

Let the dough rest about 1 hour (or use the paste function of your MAP) 

Spread the dough on your work surface to a thickness of about 2 cm and cut out discs about 6 to 8 cm in diameter using a punch or a glass. 

Leave to rest again. Fry on medium heat (be careful if the fire is too soft they will be raw inside and if it is too strong they will burn, I heat the oil of the pan over high heat and then I lower the temperature, the ideal is to fry them (with the recommended temperature for donuts on some electric fryers) and _start with not too big donuts until you master the cooking_

You can stuff them with jam or **[ custard ](<https://www.amourdecuisine.fr/article-26492437.html>) ** once they are cooked using a syringe 

Thank you for your visits and your comments 

bonne dégustation 
