---
title: Paquerette de Soissons
date: '2012-04-16'
categories:
- Brioches et viennoiseries

---
Hello everyone, here is a wonderful recipe that you like to share Lunetoiles with you. a recipe very very good, a delicious cake, which should remind you of this flower, the daisy. as lunetoiles told me she did not have an orange macaron, suitable for this cake, nor also the good sleeve, but I see it does a remarkable job. So the ingredients: For the pastry 240 g flour 150 g butter 90 sugar 30g almond powder 1 sachet vanilla sugar 1 egg yolk 1 pinch of salt for others: 200 g of red fruits & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

Here is a wonderful recipe that Lunetoiles love to share with you. 

a recipe very very good, a delicious [ cake ](<https://www.amourdecuisine.fr/categorie-10678925.html>) who will have to remind you of this flower, the daisy. 

as  Lunetoiles told me that she did not have an orange button, suitable for this cake, nor the right socket, but I see she does a remarkable job. 

so the ingredients: 

For shortbread dough 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 



for the others: 

  * 200 g frozen red fruits 
  * 120 g of almond powder 
  * 2 eggs 
  * 100g of sugar 
  * 200 ml whole liquid cream 



For ganache and deco: 

  * 200 g of white chocolate 
  * 250 g whole liquid cream 
  * 1 big yellow or orange macaroon 
  * 1 tbsp. chopped fresh pistachios 


  1. The day before, melt the white chocolate and the 50 g of the micron wave cream 2 x 30 sec. 
  2. Let cool and add the rest of the cream. 
  3. Keep cool one night. 
  4. Mix the flour, sugar, vanilla sugar and salt. 
  5. Add the diced butter and mix with the whisk K until you get a big shortbread. 
  6. Add the egg yolk and knead until a smooth, non-sticky paste is obtained. 
  7. Allow the time to cool in food film. 
  8. Spread the dough between 1 baking sheet. 
  9. Cover a pie circle 24 cm in diameter after carefully buttering. 
  10. Stir with a fork and put in the fridge until it is time to prepare the filling. 
  11. Whisk the eggs with the sugar. 
  12. almond powder and cream. 
  13. Drop the still frozen fruit on the pie shell and cover with almond cream. 
  14. Put in the oven for 30 minutes at 180 ° C (th.6). 
  15. Let cool. 



The next day whip the ganache, to obtain a mounted ganache and using a pocket with a socket to Saint Honoré, form the petals of the daisy. 

Place in the center a large macaroon with fresh chopped pistachios. 

Let stand 1 hour in the fresh air before eating. 

thank you for all your comments 

bonne journee 
