---
title: tagine of lamb with quince
date: '2018-01-07'
categories:
- Algerian cuisine
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tajine-aux-coings-003.CR2_-755x1024.jpg
---
[ ![tagine of lamb with quince](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tajine-aux-coings-003.CR2_-755x1024.jpg) ](<https://www.amourdecuisine.fr/article-tajine-d-agneau-aux-coings.html/tajine-aux-coings-003-cr2>)

##  tagine of lamb with quince 

Hello everybody, 

I continue my little recipes with this delicious fruit, quince, and today I share with you this tagine tajine recipe, I will not count you since when I did not eat it, but it's done still very long ... 

Because as I always told you, I hardly found the quince here in England, and when I went to Algeria, it was always out of season ... 

So I take the opportunity that I have this beautiful fruit at home, and I share with you this delicious tajine ... I'll be honest, I never cook this Tagine au par before, and as I could not to join my mother to have her recipe, I had to contact Ratiba who passed me the original recipe of the tajine with the Algerian quince, and I can tell you that it was a treat, I really like .... I had forgotten that taste.   


**tagine of lamb with quince**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tajine-aux-coings-004.CR2_-1024x682.jpg)

Recipe type:  tajine, Algerian cuisine  portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 2 quinces 
  * 200 gr of lamb meat 
  * 1 onion 
  * 1 cinnamon stick 
  * some saffron pistils 
  * salt 
  * black pepper 
  * 2 tablespoons of olive oil 
  * flaked almonds or whole toasted 



**Realization steps** Prepare the quinces 

  1. peel the quince and remove the pepins. 
  2. cut in small quarters 
  3. steam them with some saffron pistils   
do not overcook, because it will become tender very quickly, so you have to watch. 

Prepare the sauce: 
  1. in a pot, place the pieces of mutton, 
  2. add the oil and onion, and let it come back 
  3. add salt (not too much because the sauce will be reduced) black pepper, saffron pistils. 
  4. cover with a little water and let cook well. 
  5. when the meat is tender and the sauce is moderately reduced, add in the pieces of quince already steamed. 
  6. let simmer for a few minutes and reach the fire. 
  7. during the presentation decorate with a little almonds. 



[ ![tagine of lamb with quince 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tajine-aux-coings-008.CR2_-1024x682.jpg) ](<https://www.amourdecuisine.fr/article-tajine-d-agneau-aux-coings.html/tajine-aux-coings-008-cr2>)
