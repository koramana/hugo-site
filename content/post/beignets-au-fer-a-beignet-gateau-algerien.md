---
title: donuts with donut iron, Algerian cake
date: '2017-06-18'
categories:
- Algerian cakes with honey
- fried Algerian cakes
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet-2.CR2_.jpg
---
[ ![donuts with donut iron, Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet-2.CR2_.jpg>)

##  donuts with donut iron, Algerian cake 

Hello everybody, 

a recipe for Algerian cake to eat, we call it donuts, but in fact it's a crunchy-mellow mixture. This cake is a real delight, they are also called window of paradise, or chebbak el jenna. 

This cake is easy to prepare, but you have to have passion when cooking. to make a donut after another, to take his time so that the paste sticks to the iron, and does not fall, hihihih 

If you are impatient, or you have too many people rode around you, do not do it, lol ... Anyway, I tell you something, if like me, you live in a pay or there is no zlabiya, this recipe will accompany your tea evenings of Ramadan.   


**donuts with donut iron, Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2_.jpg)

portions:  40  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * 180g flour 
  * 10g cornflour 
  * 2 eggs 
  * 180 ml of milk 
  * 1 tablespoon of sugar 
  * 1 cup of vanilla coffee 
  * 1 pinch of salt (optional) 



**Realization steps**

  1. Beat the eggs well, 
  2. add the cornstarch and continue beating, 
  3. then add the sugar, the vanilla, the milk and the pinch of salt 
  4. while whisking, add the flour in a small amount, until you have a dough that is neither too flowing nor too thick   
preferably, try cooking, if the dough is good, continue, if it is too liquid, add a little flour. 
  5. put oil in a small stove, 
  6. put your mold there to warm up 
  7. take the mold and put it directly into the preparation to cover the edges with the dough, it should not cover the top, leave in the dough, do not remove immediately, so that it does not fall back. 
  8. put the mold in oil, the donut will fall alone. 
  9. brown the donuts on both sides. 
  10. At the end of cooking, dip your cakes in preheated honey to which you have added some orange blossom. 



{{< youtube f0Yqj >}} 

[ ![donuts with donut iron, Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2-001.jpg>)
