---
title: quick and easy sandblast
date: '2013-02-09'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-au-citron-meringuee-019.CR2_thumb.jpg
---
##  quick and easy sandblast 

Hello everybody, 

A basic pastry recipe, very important, to make beautiful pies, such as almonds, apple pies, lemon pies, and many other delicious pies. 

the ultra-melted shortbread paste, very easy to make, very simple, and that will make you happy. I do not know who this recipe comes from, but since Lunetoiles gave it to me during one of these productions, especially its amandine pies ... this pasta does not leave me any more, and I adopt it as a base for all my pies. 

Ingredients: 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 



method of preparation: 

more 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a dough is detached from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Darken a pie circle 28-30 cm in diameter. 
  6. Prick the bottom of the dough with a fork. 
  7. cover with a baking sheet, and fill with dry vegetables (for me it was glass tablets for vase ornament) and put in the oven 20 min at 180 ° C, then remove the legumes and the leaf and continue the cooking 10 minutes. 



**quick and easy sandblast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-au-citron-meringuee-019.CR2_thumb.jpg)

Prep time:  5 mins  cooking:  20 mins  total:  25 mins 

**Ingredients**

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 



**Realization steps**

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a dough is detached from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Darken a pie circle 28-30 cm in diameter. 
  6. Prick the bottom of the dough with a fork. 
  7. cover with a baking sheet, and fill with dry vegetables (for me it was glass tablets for vase ornament) and put in the oven 20 min at 180 ° C, then remove the legumes and the leaf and continue the cooking 10 minutes. 


