---
title: 'recipes around a banana ingredient # 13'
date: '2015-12-05'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
[ ![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg>)

source: Pinterest 

Hello everybody, 

**Viane** chose for this round: **The banana** as a featured ingredient. So I invite you to participate in this game, a friendly game of sharing where there is neither winner nor loser, just the pleasure of discovering new blogs, and why not discover yours, as well as share different recipes around of a single ingredient. [ ![Banana challenge](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/D%C3%A9fi-banane.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/D%C3%A9fi-banane.jpg>)

**Viane** will contact you on the 26th ** December  ** to have the title of your recipes, and also to establish the list of participants in the game, you must reply to his email before the ** 1  ** **January** that it will in turn transmit to all participants before  **January 3** . 

New of the month: 

You can also share your title of recipe, and the link of your blog when you would have realized it, like that, even if the godmother does not manage to contact you, she can always find the title of your recipe on the comments of this post: 

_**Principle of the game** _

_1- Choose a seasonal ingredient and accessible to all corners of the world._

_2- The sponsor will publish this article on her blog with the links of the two bloggers who initiated this game_

_3 - Announce the ingredient that will be the star of the current month._

_4 - The bloggers who want to participate, will contact directly the animator of the game so that she can already establish the list of the participants in the game (the bloggers can if they want to announce the game on their blogs, to make it known to their friends , so we can have even more participants)._

Les rondes précédentes: 
