---
title: amaretti fluffy - amaretti Sassello
date: '2016-05-26'
categories:
- Algerian cakes- oriental- modern- fete aid
- Gateaux Secs algeriens, petits fours
- recettes sucrees
tags:
- Algerian cakes
- Easy cooking
- Ramadan 2016
- To taste
- Algeria
- Pastry
- Fluffy cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello-1.jpg
---
[ ![amaretti fluffy - amaretti Sassello 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello-1.jpg>)

##  amaretti fluffy - amaretti Sassello 

Hello everybody, 

Today I share with you a recipe from my dear friend Lunetoiles, I will not tell you much about the recipe of these amaretti amaretti or amaretti Sassello, I let Lunetoiles told you his little story: 

Amaretti (means bitters), these are traditional Italian biscuits, an old recipe that is made with just egg whites, sugar and sweet or bitter almonds, some amaretti are prepared with almonds from the cores apricots, but if you use natural almonds, add the natural aroma of bitter almond, a few drops are enough to bring the perfume. 

[ ![amaretti fluffy - amaretti Sassello 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello-2.jpg>)   


**amaretti fluffy - amaretti Sassello**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello-3.jpg)

portions:  20  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients** For twenty biscuits: 

  * 3 egg whites (weighs about 80 gr) 
  * 100 gr of icing sugar 
  * 210 gr of white almond powder 
  * 3 to 4 drops of natural aroma of bitter almond 
  * icing sugar to roll cakes in   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello.jpg>)



**Realization steps**

  1. Preheat the oven to 150 ° rotating heat with a rack in the middle. 
  2. Using an electric mixer beat the 3 egg whites until stiff. 
  3. Stir in the white almond powder and icing sugar (sifted to ensure that there are no lumps), then add 3 or 4 drops of the natural aroma of bitter almonds. 
  4. Mix gently with the spatula. 
  5. Form balls of about 20 gr and put them on a plate covered with parchment paper, spacing them. 
  6. Roll the amaretti into the icing sugar and place on a baking sheet lined with parchment paper. 
  7. Cook at 150 ° C for about fifteen minutes (cookies should not color and stay white). 
  8. Cool on a rack and store for 1 to 2 weeks in an airtight container. 



Note _variants_ :   
you can roast the almond powder for 10 minutes at 150 ° C   
Then mix it with the sugar to make it very fine powder. [ ![amaretti fluffy - amaretti Sassello 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/amaretti-moelleux-amaretti-Sassello-4.jpg>)
