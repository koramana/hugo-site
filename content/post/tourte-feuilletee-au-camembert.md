---
title: Puff pastry pie
date: '2015-08-09'
categories:
- appetizer, tapas, appetizer
tags:
- Meat pie
- Meat
- Algeria
- Morocco
- Easy cooking
- Summer recipe
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/08/tourte-feuillettee-au-camembert-31.jpg
---
[ ![pie leafed with camembert 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/tourte-feuillettee-au-camembert-31.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/tourte-feuillettee-au-camembert-31.jpg>)

##  Puff pastry pie 

Hello everybody, 

I like Camembert a lot, and when I see it in a recipe, I eat the recipe with my eyes closed. This was the case with this delicious recipe from **Lgrd Isa** , A pie puff pie, very easy to make, and especially too good. 

I hope you will try this beautiful entry that will be easily accompanied by a light colored salad, especially in summer. This flaky recipe can be used to your liking by adding the ingredients you like, it is simple, fast and pretty. 

**pie leaf with pie chart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/tourte-feuillet%C3%A9e-au-camembert-1.jpg)

portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * A puff pastry or homemade is better .. 
  * Potatoes cooked in advance 
  * 2 onions 
  * 1 large pie or another reblochon style cheese 
  * Minced meat 
  * Olive oil 



**Realization steps**

  1. Fry the onions and potatoes in the olive oil, then add the minced meat 
  2. Spread the puff pastry and cut into 8 triangles (as in the picture) 
  3. Divide the camembert into 3 in its length 
  4. Put the bottom of the cheese in the center then add the stuffing potato onions and chopped meat 
  5. Then put the pie in the middle and add a new layer of onion potato and meat to the top and put the top of the camembert over the top,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/tourte-feuillet%C3%A9e-au-camembert.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/tourte-feuillet%C3%A9e-au-camembert.jpg>)
  6. Then fold down the sides of the pastry on the stuffed camembert, making sure to weld the dough so that the camembert does not run through. 
  7. Then beat an egg yolk and spread over the whole pastry with a brush.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/tourte-feuillett%C3%A9e-au-camembert-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/tourte-feuillett%C3%A9e-au-camembert-2.jpg>)
  8. Preheat oven to 200 degrees remove when golden 


