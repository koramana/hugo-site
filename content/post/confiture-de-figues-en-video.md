---
title: Jam of figs in video
date: '2017-08-16'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/confiture-de-figues1.jpg
---
![jam-de-figues.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/confiture-de-figues1.jpg)

##  Jam of figs in video 

Hello everybody, 

As I already have, I take full advantage of my presence in Algeria, and figs of the neighbor ... and I share with you the delicious recipe for fig jam, that I realize every year and I have never had the chance to take pictures and publish. 

I'm posting you this video, and I hope you'll succeed. 

**Jam of figs in video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/confiture-de-figues1.jpg)

Recipe type:  jam, 

**Ingredients**

  * 750 gr of fresh figs 
  * 350 gr of sugar 
  * ½ glass of water 
  * the juice of 1/2 lemon 
  * ½ teaspoon of butter 
  * star anise 
  * cinnamon 



**Realization steps**

  1. you can see the method of realization in the video 


