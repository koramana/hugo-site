---
title: homemade merguez recipe
date: '2015-02-26'
categories:
- recipe for red meat (halal)
tags:
- dishes
- Algeria
- cassoulet
- Meat
- Healthy cuisine
- inputs
- Amuse bouche

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/merguez-fait-maison-aux-boyaux-en-collagene-a.jpg
---
[ ![homemade merguez](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/merguez-fait-maison-aux-boyaux-en-collagene-a.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-merguez.html/merguez-fait-maison-aux-boyaux-en-collagene-a>)

##  homemade merguez recipe 

Hello everybody, 

Who does not like merguez ?, I'm sure that no one has raised their hand, hihihih, in any case here is a home made merguez recipe 100%, by the unanimous vote of all the members from my little family, I hope that even at home, the vote will be the same. 

My story with the merguez, here in England is super long, we only eat the delicious merguez, once a year, if by chance, we have the chance to go to London, to buy from an Algerian butcher, who does it beautifully. Of course eating the merguez once a year is not the life we ​​dream of, hihihihi. 

So, I was looking scattered here in Bristol, or even in the cities around, to find a good butcher, but to no avail ... Recently, on the cooking group that I created on facebook, one of the facebookaines, to publish beautiful merguez home made, leaving a comment to say, that I can not find the halal hoses, to make my merguez, one of the girls answers me to tell me that I can used "the vegetarian casing" 

[ ![vegetarian-casing-gut au-halal collagen-a](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/vegetarian-casing-boyaux-au-collagene-halal-a.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-merguez.html/vegetarian-casing-boyaux-au-collagene-halal-a>)

Its equivalent in France is: halal collagen casings. Of course right after reading this, I went to search ebay, and amazon, and I found that he sold these "casings". you think I hesitated a second to click on the button "buy it now", well not at all, and immediately after another purchase of madness: 

[ ![unit-a-merguez](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/appareil-a-merguez.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-merguez.html/appareil-a-merguez-2>)

Admittedly, it was not an expensive device, but I wanted to do things properly. 

A small note for collagen casings: 

  * you do not need to put them in the water, they tear themselves to the touch of the water. 
  * a little difficult to make the knots between the merguez, because they keep their old form, but once at rest in the fridge, the form is good, and even at cutting the merguez do not reopen. 
  * when cooked they burn a little compared to natural casings, but they do not ruin the shape of the merguez. 
  * their taste is neutral, so they will not change the taste of your merguez. 



If I have another remark as I try, I will add it here. 

Now you have the recipe:   


**homemade merguez recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/merguez-fait-maison-1.CR2a.jpg)

portions:  20  Prep time:  30 mins  total:  30 mins 

**Ingredients**

  * 1 kg lamb meat with its fat (I chose the shoulder, do not remove the fat you need at least 200 gr of fat for 800 gr of meat) 
  * 1 tablespoon and a half ground fennel seeds 
  * 1 tablespoon of Paprika; 
  * 2 crushed garlic clove 
  * 1 tablespoon harissa minimum, more for those who like to eat spicy, (I did not put children because I did not know how to go to be the result) 
  * 2 tablespoons extra virgin olive oil. 
  * 1 cup of cumin powder 
  * 1 teaspoon and ¼ fine salt, 
  * 1 teaspoon of ground pepper, 
  * 1 tablespoon dried garlic 
  * 1 teaspoon of coriander seed powder. 
  * 1 tablespoon dried parsley 
  * 1 tablespoon of dried coriander 



**Realization steps**

  1. Cut the meat into pieces, and go through the chopper (large hole piece)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/merguez-preparation-a.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-merguez.html/merguez-preparation-a>)
  2. place the minced meat on a large plate, 
  3. Add above all the spices, and the oil, and mix gently by hand.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/assaisonement-de-merguez-a.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-merguez.html/assaisonement-de-merguez-a>)
  4. place the merguez piece in your machine now, always leave the room with large holes. 
  5. place the casings gently in the funnel (funnel), and start the machine.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/remplissage-du-merguez1-a.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-merguez.html/remplissage-du-merguez1-a>)
  6. When you see the meat coming to the end of the merguez funnel, make a knot, and start filling the intestines, so as not to have air in it. 
  7. always leave the equivalent of 10 cm of unfilled casings to be able to separate the merguez. 
  8. When the casings are well filled, roll each 12 cm of merguez on itself to make the knot, between each two merguez. 
  9. so that the knots do not open, roll the first merguez towards the front, but roll the second merguez towards the rear. every time. made the opposite gesture so that the merguez do not reopen. 
  10. prick with a fork, to let the air escape, and leave to rest in the fridge at least 4 hours, before cooking. 



[ ![merguez-done-maison.a](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/merguez-fait-maison.a.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-merguez.html/merguez-fait-maison-a>)
