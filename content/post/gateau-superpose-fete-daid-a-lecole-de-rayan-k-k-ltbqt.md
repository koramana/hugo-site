---
title: bake cake Aid's party at the school of rayan "كعك الطبقات"
date: '2010-05-21'
categories:
- Indian cuisine
- Cuisine par pays
- recette de ramadan
- chicken meat recipes (halal)

---
Once again, I am putting you an old recipe .... the one I made, at the feast of the celebree aid at my son's school. a visit to Meriem, and here is the ideal cake. and without delay here is the recipe: Ingredients: - 700 gr of flour - 300 gr of melted butter - 2 cs of caster sugar - 1 cc of baker's yeast - 3 cc of baking powder (one sachet) - Milk and eau de fleur orange - Jam Preparation - Mix sifted flour, baking powder, sugar, melted butter, then yeast & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 0.45  (  1  ratings)  0 

once again, I'm putting you an old recipe ... 

that I made them, at the feast of aid celebree at the school of my son. 

and without delay, here is the recipe: 

_ ingredients:   
_

**_ \- 700 gr of flour  _ **

**_   
\- 300 gr of melted butter  _ **

**_   
\- 2 tablespoons caster sugar  _ **

**_   
\- 1 teaspoon baker's yeast  _ **

**_   
\- 3 cc of baking powder  _ ** **_ (A sachet)  _ **

**_   
\- Milk and water of orange blossom  _ **

**_   
\- Jam  _ **

_ Preparation  _

\- Mix sifted flour, baking powder, sugar, melted butter, then brewer's yeast, sabler by hand, add the mixture of milk and orange blossom water, to obtain a soft dough. 

\- Divide the dough into 5 equal parts, let stand 10 minutes. 

Spread the first ball with the roll, cover with jam, cover with layer of dough, and jam, until the last ball (the last layer is of dough). 

Cook in a preheated oven at 180 degrees for 15 to 20 minutes, prick with a knife tip if it comes out clean is cooked, if not cook again, to watch the cooking closely. 

After cooking, glaze the chocolate surface, sprinkle with crushed peanuts (optional), for me it was flaked almonds 

** For icing:  ** **_ \- 20 cl of crème fraîche or liquid cream   
\- 200 g of dark chocolate preferably   
_ ** **_ \- 40 gr of butter  _ **   
\- Bring the 20 cl of cream to a boil in a heavy-bottomed saucepan. Off the heat, add the 200 g of finely grated dark chocolate, mix with a wooden spoon, stirring gently until it is melted and finish with the knob of butter. Mix your frosting gently otherwise it may not shine and bubble. Let cool and thicken slightly, stirring occasionally   
Employez le glaçage tiède et encore liquide. Étalez avec une spatule pour recouvrir le gâteau qui doit être, lui, complètement froid. 
