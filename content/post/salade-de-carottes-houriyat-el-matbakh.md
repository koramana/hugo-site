---
title: Carrot salad, houriyat el matbakh
date: '2012-04-08'
categories:
- juice and cocktail drinks without alcohol
- Cuisine saine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/salade-de-carottes-2_thumb.jpg
---
Hello everybody, 

a **carrot salad** hot, what do you say? ... I really like this salad, I usually mix it with white turnip, I prepare a dersa (spicy mixture of condiment, and garlic with harissa), but I saw on the tv channel: **fatafeat TV** , during the cooking show of **[ houriyat el matbakh ](<https://www.amourdecuisine.fr/categorie-11131465.html>) ** another method, and I really like to test its method, I'm not at all on it, because it is a very delicious recipe. 

you can see another carrot salad: [ mashed carrot salad ](<https://www.amourdecuisine.fr/article-salade-puree-de-carottes-81318269.html>) .    


**Carrot salad, houriyat el matbakh**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/salade-de-carottes-2_thumb.jpg)

Recipe type:  salad  portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 500 gr of carrots. 
  * 1 tablespoon olive oil. 
  * 3 cloves of garlic, chopped 
  * ½ tbsp canned tomato 
  * 100 ml of water 
  * 1 cup of cumin powder. 
  * Salt and black pepper from the mill 
  * 1 hot pepper (optional) 
  * ½ teaspoon of caraway (optional) 
  * chopped parsley for garnish 
  * Black or green olives for garnish 
  * Boiled eggs for garnish (optional) 
  * lemon juice 



**Realization steps**

  1. clean the carrots, cut into thin slices and steam for 5 minutes. 
  2. Put the olive oil in a pan with chopped garlic, at this point you can add chilli for those who wish and the tomato spoon canned 
  3. mix so that it does not burn for almost 3 minutes. 
  4. Add the cooked carrots and cook over low heat, stirring gently for 4 to 5 minutes 
  5. Add water, cumin, black pepper and salt 
  6. leave until the sauce is well reduced. 
  7. remove the mixture from the heat and add the squeezed lemon. 
  8. Present in the serving dish decorated with chopped parsley, black or green olives and cut hard boiled eggs. 


