---
title: Dolma zucchini zucchini with minced meat
date: '2017-12-26'
categories:
- Algerian cuisine
tags:
- Algeria
- dishes
- tajine
- meatballs
- Healthy cuisine
- Algeria
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dolma-aux-courgettes-2.jpg
---
[ ![Dolma zucchini and zucchini with minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dolma-aux-courgettes-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dolma-aux-courgettes-2.jpg>)

##  Dolma zucchini and zucchini with minced meat 

Hello everyone, other members of my small tribe. And when I do a little control of my diet and I'm asked for a [ vegetable dolma ](<https://www.amourdecuisine.fr/article-dolma-legumes-farcis-a-la-viande-hachee-au-four.html>) Or a dolma batata (dolma a potato). 

I make two recipes, one for them with all the vegetables they like, and one for me with zucchini (it will save me from eating potatoes that I like a lot, it's not a diet, but just a small Control), of course I'm not lying to say that I will eat this dolma without bread, I love the bread, but to avoid eating the crumb, I eat my dolma with a small cake of bread pita, yum yumiii. I'm already hungry at midnight while I'm writing the recipe, hihihiih.   


**Dolma zucchini and zucchini with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/dolma-aux-courgettes.jpg)

portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 3 or 4 beautiful zucchini 
  * 1 bowl of chickpeas in a box 
  * 1 large chopped onion 

stuffing and dumplings 
  * 300 gr of minced meat 
  * 1 egg 
  * 3 tablespoons of breadcrumbs 
  * ½ bunch of chopped parsley 
  * 2 garlic cloves finely grated 
  * 1 cup of cumin powder 
  * 1 teaspoon coriander powder 
  * Extra virgin olive oil 
  * Salt pepper 



**Realization steps**

  1. clean the zucchini and cut into pieces of almost 5 cm and empty the heart with the tip of a teaspoon 
  2. Prepare the stuffing by mixing the minced meat, grated garlic, chopped parsley, salt, pepper, an egg and bread crumbs, 
  3. mix well and stuff zucchini. 
  4. the remaining stuffing, form dumplings 
  5. in a pot, put the olive oil, add the onion and 1 clove garlic grated, fry 5 min, add the spices, salt and pepper. add between 1 glass and 1 glass and a half of water. 
  6. cook until onions are tender. 
  7. pour half of this sauce into a gratin dish. 
  8. add the chickpeas to the rest of the sauce, let it simmer a little and remove from the heat. 
  9. place the stuffed zucchini, the minced meatballs, a little chopped parsley and cover with aluminum foil. 
  10. cook in a preheated oven at 180 degrees C 
  11. when serving, add a little sauce and chickpeas. 



Je suis une grande fan de la courgette, ce qui n’est pas le cas 
