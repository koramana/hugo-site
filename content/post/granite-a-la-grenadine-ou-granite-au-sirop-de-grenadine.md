---
title: grenadine granita or granita with grenadine syrup
date: '2017-08-07'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/granit%C3%A9-a-la-grenadine-2.jpg
---
[ ![grenadine granita or granita with grenadine syrup](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/granit%C3%A9-a-la-grenadine-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/granit%C3%A9-a-la-grenadine-2.jpg>)

##  grenadine granita or granita with grenadine syrup 

Hello everybody, 

Hot days begin to peek, and we tend to find a refreshing drink or dessert to relieve our breathless hearts. For my part, the granita remains one of my favorite refreshing dessert, whether it's during the super hot afternoons, or the Ramadan evenings after the prayer, when you want to quench your thirst. 

For me last year, during Ramadan and during my pregnancy, there was only the granite that accompanied me all evening, I was afraid of ice cream that could contain eggs, and that were me discouraged, and especially I was too lazy to prepare homemade ice cream, so I find the granite easier to do, and most importantly, very very refreshing. 

and what's really good is that with granita we are not limited to a single recipe, or a single ingredient, or to follow the measurements. A large container, a freezer, a fairly fierce fork as I preferred to say, lol, and a big cup to knead everything. 

The recipe is spicy, because you will mix the ingredients according to your taste. It's not magic, it's not a cake you're going to miss, and it's not because of one more ingredient, or one less ingredient, that the recipe is going to be a disaster ... 

Take pleasure in making this refreshing dessert, put the ingredients you prefer most, and most importantly, try to remember to scrape your mixture every hour, not to be with an ice block or an iceberg, lol.   


**grenadine granita or granita with grenadine syrup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/granit%C3%A9-a-la-grenadine.CR2_.jpg)

Recipe type:  Dessert  portions:  6  Prep time:  360 mins  total:  6 hours 

**Ingredients**

  * grenadine syrup adjust with water according to your taste 
  * the juice of a lemon 
  * the juice of an orange 



**Realization steps**

  1. Mix all the ingredients to have a delicious juice 
  2. pour the juice into a fairly large pan   
I prefer wide mussels than deep mussels, the preparation will be easier in the wide molds. 
  3. place the mold with the juice in the fridge 
  4. every hour, remove the mold and with the help of a fork, and according to the hardness of the ice, scratch to have pretty crystals of ice. 
  5. repeat the operation every hour, you will have a mixture rich in crystals that will have the texture of the snow. 
  6. Divide the granite into previously chilled glasses. Sprinkle with pomegranate seeds if you have them. 
  7. Serve immediately 



[ ![Grenadine granite.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/granit%C3%A9-a-la-grenadine.CR2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/granit%C3%A9-a-la-grenadine.CR2-001.jpg>)
