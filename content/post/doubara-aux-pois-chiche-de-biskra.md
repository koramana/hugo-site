---
title: Biskra chickpea doubara
date: '2014-02-17'
categories:
- panna cotta, flan, and yoghurt
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/pois-chiche-a-la-doubara_thumb.jpg
---
#  [ ![chickpea with the doubara](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/pois-chiche-a-la-doubara_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/pois-chiche-a-la-doubara.jpg>)

##  Biskra chickpea doubara 

Hello everybody, 

a super delicious dish of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) prepared for **Biskra** , usually **piquancy** , with a light sauce, but my mother prepares it that way, so she prepares a chickpea cassoulet in white sauce, then she prepares the **doubara sauce** , and everyone can add the amount of this sauce he wants in his dish. 

then the recipe as we prepare it at home: Doubara biskria, chickpea with the doubara, or chickpea cassoulet with spicy tomato sauce.   


**Biskra chickpea doubara**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/doubara_thumb.jpg)

Recipe type:  Algerian cuisine 

**Ingredients**

  * a bowl of chickpeas dipped the day before. 
  * ¼ onion, 
  * 2 cloves of garlic 
  * a tomato 
  * a medium bunch of coriander, 
  * 1 tablespoon of hrissa, 
  * 1 cup canned tomato, 
  * salt, black pepper, coriander powder, a little cumin 
  * 1 little table oil, 
  * 2 tablespoons water to dissolve the tomato 
  * the Hrissa, 



**Realization steps**

  1. drip the chickpeas 
  2. in a mortar, crush the garlic with salt and caraway 
  3. to make return this mixture (derssa) in a little oil, 
  4. cover with water and throw in the chickpeas, and cook. 
  5. In a bowl, mix tomato paste, harissa, olive oil, a pinch of salt and pepper. 
  6. add the chopped garlic, chopped coriander, and spice, salt and pepper 
  7. Sprinkle with a little cumin, 
  8. add the fresh tomato cut into pieces, 
  9. at the moment of serving, introduce the doubara with the chickpeas, and each one can be used to his taste. 



[ ![chickpea cassoulet](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/cassoulet-de-pois-chiche_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/cassoulet-de-pois-chiche_2.jpg>)
