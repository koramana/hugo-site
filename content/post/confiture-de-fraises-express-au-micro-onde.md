---
title: express strawberry jam, in the microwave
date: '2015-04-16'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/confiture-de-fraise-004_thumb.jpg
---
##  express strawberry jam, in the microwave 

Hello everybody, 

We do our weekly shopping, when we saw beautiful red strawberries at a dream price, my husband to buy almost 1 kilo and a half. Arriving at home, I made a fruit salad with it, and luckily the strawberries had no taste !!! Like all men, Mr said to me: throw them ... .. what ???? to throw strawberries is not avarice, but a kilo of strawberries is a pain. 

So the only solution is a jam, because if I make a Bavarian or a pie, I have to eat it all alone, because in the family, it's not big cake eaters. I fell, during my walks on friends blogs, the method of kouky, a jam in the microwave, an express jam, I admit to have been super tempted.   


**strawberry jam: "express, microwave"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/confiture-de-fraise-004_thumb.jpg)

portions:  6  Prep time:  5 mins  cooking:  20 mins  total:  25 mins 

**Ingredients**

  * 1 kilogram of strawberries 
  * 700 gr of sugar 
  * the juice of half a lemon 



**Realization steps**

  1. wash, plant and cut the strawberries in half 
  2. Put the strawberries and the lemon juice in a deep casserole in the microwave oven. 
  3. Cover and cook 8 min at 900 watts (must be done 4 min to 2 times) 
  4. Stir in the sugar, mix well 
  5. Cook without covering 20, in increments of 5 minutes turning each time 
  6. Monitor to avoid overflows   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/2011-10-15-confiture-de-fraises_thumb.jpg) ](<https://www.amourdecuisine.fr/article-confiture-de-fraises-express-au-micro-onde.html/2011-10-15-confiture-de-fraises_thumb>)
  7. Let cool a little to test the consistency, a drop of jam frozen on a cold surface 
  8. For the potting, heat the jam and pour it into a scalded pot, close and place upside down until cool 



[ ![strawberry jam 024](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/confiture-de-fraises-024_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/confiture-de-fraises-024.jpg>)

bonne journée 
