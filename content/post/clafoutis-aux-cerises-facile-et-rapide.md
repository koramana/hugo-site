---
title: quick and easy cherry clafoutis
date: '2015-05-29'
categories:
- dessert, crumbles and bars
- panna cotta, flan, and yoghurt
- recette de ramadan
tags:
- Ramadan 2015
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/clafoutis-aux-cerises-1.jpg
---
##  [ ![Cherry clafoutis](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/clafoutis-aux-cerises-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/clafoutis-aux-cerises-1.jpg>) quick and easy cherry clafoutis 

Hello everybody, 

Cherries ... what I like this fruit, but I'll be very frank, I eat it fresh, or sometimes smoothie when the cherries are not sweet ... 

So I'm not too lucky to make recipes with this fruit that is in season now ... unless sometimes I find this fruit in the frozen department ... 

I thank very much Luentoiles who shared with us today his recipe for cherry clafoutis .... his clafoutis then very well succeeded in any case ... a big congratulations ... 

And you, what are you going to do with cherries?   


**quick and easy cherry clafoutis**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/clafoutis-aux-cerises-2-245x300.jpg)

portions:  8  Prep time:  10 mins  cooking:  40 mins  total:  50 mins 

**Ingredients**

  * 500 to 600 grams of pitted cherries 
  * 4 eggs (5 so small) 
  * 90 gr of caster sugar 
  * 30 gr of sugar brown sugar 
  * 2 bags of vanilla sugar 
  * 160 gr of flour (or 100 gr of flour + 60 gr of almond powder) 
  * 40 cl of milk 
  * 1 pinch of salt 
  * 1 little butter for the mold 



**Realization steps**

  1. Wash, stump, pit the cherries. 
  2. Garnish with a buttered mold and sprinkle with vanilla sugar. 
  3. In a salad bowl, mix the flour, a pinch of salt and the sugars, then add the eggs and whip. 
  4. Add the milk little by little. 
  5. Pour this mixture over the cherries, and cook for 35-40 minutes in a preheated oven at 200 ° C. 
  6. Let cool completely and unmold. 
  7. Store a few hours in the refrigerator, the clafoutis will be better! 



[ ![cherry clafoutis 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/clafoutis-aux-cerises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/clafoutis-aux-cerises.jpg>)
