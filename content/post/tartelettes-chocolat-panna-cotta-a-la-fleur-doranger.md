---
title: tartlets chocolate panna cotta with orange blossom
date: '2018-04-13'
categories:
- Chocolate cake
- pies and tarts
tags:
- Based
- Pastry
- desserts
- To taste
- Algerian cakes
- Shortbread
- Shortbread

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelette-chocolat-et-pannacotta-a-leau-de-fleur-doranger-1.jpg
---
[ ![chocolate panna cotta tartlet with orange blossom water 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelette-chocolat-et-pannacotta-a-leau-de-fleur-doranger-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelette-chocolat-et-pannacotta-a-leau-de-fleur-doranger-1.jpg>)

##  tartlets chocolate panna cotta with orange blossom 

Hello everybody, 

Do you like chocolate? me too and I'm just looking for the first opportunity to make a chocolate-based recipe, to chew in with a toothy, hihihih. 

This time, I made a duet of flavor and endless scents with these chocolate tartlets and their half sphere of panna cotta with orange blossom. With this recipe I participate for the first time in the beautiful game Foodista: 

[ ![Foodista_challenge_16](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Foodista_challenge_16.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Foodista_challenge_16.jpg>)

The theme of **Foodista # 16** East : **Flower power!** **Two rules:** \- a **ingredient** : the **flowers** \- a **decor element** : the **flowers**

**tartlets chocolate panna cotta with orange blossom**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelette-chocolat-et-pannacotta-a-leau-de-fleur-doranger.jpg)

portions:  6  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients** [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>) : 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

chocolate filling 
  * 50 g of milk 
  * 120 g of whipping cream 
  * 120 g of 60% chocolate 
  * 12 g of butter 
  * 2 small eggs 

panna cotta with orange blossom water: For the panna cotta garnish: 
  * 300 ml of whipping cream 
  * 140 ml of milk 
  * 50 g of sugar 
  * 1 gr of agar-agar (or 1 gelatin sheet) 
  * 2 tablespoons orange blossom 
  * 1 vanilla pod. 



**Realization steps**

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a dough is detached from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Go for the tartlets, prick the bottom of the dough with a fork. 
  6. cover with a baking sheet, and fill with dry vegetables (for me it was glass tablets for vase ornament) and put in the oven 20 min at 180 ° C, then remove the legumes and the leaf and continue the cooking 10 minutes. 

for garnish: 
  1. Preheat the oven to 170 ° C. 
  2. Crush the chocolate in a bowl. 
  3. In a saucepan, boil the milk, butter and cream. 
  4. Pour this mixture on the chocolate. Mix gently, making sure not to make bubbles. 
  5. Beat the eggs lightly, then add them to the mixture. 
  6. Pour this mixture into the bottom of the tartlets previously cooked to white. 
  7. turn off the oven and let it take 15 minutes 
  8. Check the cooking by tapping lightly, the pie should be slightly flickering. And otherwise you can leave it even more. 

prepare the panna cotta: 
  1. In a saucepan, mix the milk, cream, sugar and vanilla bean melted in half. 
  2. Heat for 2 minutes, remove the vanilla pod, scratch the inside with the tip of a knife, and return the grated seeds to the milk. 
  3. add the agar-agar mixed with a little powdered sugar. Boil 1 min. 
  4. remove from the heat, add the orange blossom, mix a little and pour into the silicone molds in the shape of spheres. 
  5. put in the freezer so that it holds well and can unmold cleanly. 
  6. when the panna cotta has taken well, place the spheres on the tarts, return to the cool. 



[ ![chocolate tart and panna cotta with orange blossom water 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelette-chocolat-et-pannacotta-a-leau-de-fleur-doranger-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/tartelette-chocolat-et-pannacotta-a-leau-de-fleur-doranger-2.jpg>)
