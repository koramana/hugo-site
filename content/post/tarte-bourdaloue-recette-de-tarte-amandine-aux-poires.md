---
title: bourdaloue pie / pear tart recipe
date: '2013-02-28'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/amandine-016_thumb1.jpg
---
##  bourdaloue pie / pear tart recipe 

Hello everybody 

here is a very good pie, a beautiful sand pie, covered with a pretty cream almond, or frangipane as we like to call it, topped with beautiful pear, and see you, well I tell you, this delight is not going not stay 15 minutes on your table. 

I really like frangipane pies, and that's not what's missing on the blog, I'm giving you a short list, between recipes to me, and recipes to my dear friend Lunetoiles: 

[ FRANGIPAN PIE WITH FIGS ](<https://www.amourdecuisine.fr/article-tarte-frangipane-aux-figues-86627640.html> "frangipane pie with figs")

[ PIE WITH FRANGIPANE AND CHEESE CREAM ](<https://www.amourdecuisine.fr/article-26492437.html> "pie with frangipane and pastry cream")

[ RHUBARBE / FRANGIPANE PIE ](<https://www.amourdecuisine.fr/article-31499515.html> "Rhubarb tart / Frangipane")

[ AMANDINE PIE WITH FISHERIES ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-peches-85028616.html> "amandine pie with peaches")

[ AMANDINE PIE RASPBERRY PEARS ](<https://www.amourdecuisine.fr/article-tarta-amandine-poires-framboises-81218958.html> "raspberry pears amandine pie")

[ AMANDINE PIE WITH STRAWBERRIES ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-fraises-73876989.html> "Strawberry almond tart")

[ AMANDINES WITH FILLED PASTA ](<https://www.amourdecuisine.fr/article-38772190.html> "Amandines with puff pastry")   


**bourdaloue pie / pear tart recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/amandine-016_thumb1.jpg)

portions:  8  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients** the shortbread 

  * 200 gr of flour 
  * 100 gr of butter 
  * 75 gr of sugar 
  * 1 egg 

For frangipane cream 
  * 100 gr of butter 
  * 90 gr of sugar 
  * 1 tablespoon cornflour 
  * 2 eggs 
  * 125 gr of ground almonds 
  * 1 can of pear in its juice 

for the topping (mine) 
  * 2 tablespoons apricot jam 
  * 1 cup of acacia honey (I will have to use my honey anyway, hihihihi) 
  * 1 teaspoon of orange blossom water 



**Realization steps**

  1. Preparation of the dough 
  2. In a bowl, cut the butter into small pieces. Add the flour, sugar and a pinch of salt. Crush with your fingertips to get a sanding. Add the egg and gather quickly and form a ball. Cover with a film and let cool for 20 min. 
  3. Preparation of frangipane 
  4. Mix the softened butter with the sugar. Add the eggs then the almonds and the flour. Stir well. 
  5. Lower the sweet dough and prick with a fork (I spread it directly on the mold with removable bottom). Garnish the bottom of the pie with the frangipane cream. Place the sliced ​​pears and cook in a preheated oven. 170 ° approximately 30 to 40 min. When the top is golden. Remove and let cool. 
  6. prepare the topping by passing the ingredients over low heat just so that the mixture is a little liquid and you can easily brush the pie with. 



en tout cas c’est un réel délice, qu’on a bien apprécié 
