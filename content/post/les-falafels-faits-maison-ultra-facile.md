---
title: the homemade falafels ultra easy
date: '2015-05-17'
categories:
- amuse bouche, tapas, mise en bouche
- crepes, gauffres, beignets salees
- cuisine diverse
- idee, recette de fete, aperitif apero dinatoire
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/falafel-03.jpg
---
[ ![falafel 03](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/falafel-03.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/falafel-03.jpg>)

##  the homemade falafels ultra easy 

Hello everybody, 

Will you believe me if I tell you that this recipe for falafel is on my pc since May 2013, and I never had the chance to publish it ... 

This recipe for falafel is the easiest version of homemade falafel, I've seen it on youtube, it's a chef recipe Oussama, the recipe is super easy, just put all the ingredients in the blinder and voila ... next time, I will share with you the original recipe, in which we use the meat grinder ... it takes time, but it's a real delight, and it's going to be the recipe of my Sudanese friend, I Eat often at home, it's an endless pleasure.    


**the homemade falafels ultra easy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/falafel-023.CR2_.jpg)

Recipe type:  entrance, amuse bouche  portions:  4  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * ¾ a cup of wet beans the day before (without their skin). 
  * ¾ a cup of chickpea wet the day before. 
  * ¼ cup chopped parsley. 
  * 1 medium onion diced, 
  * ¼ cup fresh coriander and chopped dill 
  * ½ cup of leek. 
  * 1 teaspoon coriander powder. 
  * salt and black pepper 
  * ¼ teaspoon cumin powder 



**Realization steps**

  1. put all the ingredients in the blinder bowl, make sure there is no liquid, so the chickpeas and beans must be dried (use a paper towel to remove the excess of water). 
  2. shield everything together several times, to have a homogeneous mixture. 
  3. use two spoons to form dumplings, or small quenelles. 
  4. cook in a hot oil bath, until falafels get a nice golden color. 
  5. you can serve the falafels with the [ spicy sauce with yoghurt ](<https://www.amourdecuisine.fr/article-sauce-piquante-au-yaourt.html> "Hot sauce with yoghurt") , It's a delight 



[ ![falafel 021.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/falafel-021.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/falafel-021.CR2_.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
