---
title: like a strawberry tiramisu
date: '2015-11-09'
categories:
- dessert, crumbles and bars
- sweet recipes
tags:
- mascarpone
- Dessert
- Kitchen without egg
- desserts
- Without cooking
- Italy
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/comme-un-tiramisu-aux-fraises-1.jpg
---
[ ![as a strawberry tiramisu 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/comme-un-tiramisu-aux-fraises-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/comme-un-tiramisu-aux-fraises-1.jpg>)

##  like a strawberry tiramisu 

Hello everybody, 

This weekend, the kids decided to make a dessert or cake with me. So because of my little cooking, and because even Mr. baby wanted to have the hand in the dough, I thought that making a dessert without cooking would be the best thing to do. I had a box of mascarpone in the fridge, I thought of a tiramisu ... Then I said, too much coffee for children ... In addition I had more boudoirs! In any case I kept the idea of ​​making a dessert that looks like a tiramisu, but with strawberries that children like a lot ... 

The children were super happy when they made this dessert, by putting the biscuits on one side of the other, cut the strawberries ... They especially liked the idea of ​​removing "the hat of the strawberry" as they called it using a straw, hihihih. 

This dessert was a crazy success at home, and according to my husband, I have to do it as often as possible (if I follow his advice, I will post every week a different photo of the same dessert, hihihihi) 

**like a strawberry tiramisu**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/comme-un-tiramisu-aux-fraises-2.jpg)

portions:  12  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 1 box of rectangular biscuit (like butter, they are not too sweet) 
  * 1 box of mascarpone 
  * 1 box of whole liquid cream (cream or double cream) 
  * 1 teaspoon of vanilla extract 
  * 100 gr of sugar (I put less, but each his taste) 
  * 200 gr of strawberries 
  * strawberries for decoration 



**Realization steps**

  1. whisk together, mascarpone, liquid cream, sugar and vanilla 
  2. Spread a thin layer of cream mixture in a 20 x 30 cm mold. 
  3. spread the cookies over the cream to cover the whole mold (even if you have to crack the cookies) 
  4. Spread a nice layer of the cream mixture 
  5. place the pieces of strawberries on top, gently push the strawberries into the cream. 
  6. cover once more with a layer of biscuits, then cream. 
  7. Continue until the cream is used up (for my part I made three layers of biscuits) 
  8. decorate the dessert with strawberries, cover the mold with cling film and chill for at least 4 hours, the biscuits will soften a bit, and the cut will be easier. 



[ ![like a strawberry tiramisu](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/comme-un-tiramisu-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/comme-un-tiramisu-aux-fraises.jpg>)
