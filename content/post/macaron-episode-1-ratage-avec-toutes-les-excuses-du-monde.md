---
title: 'macaron: episode 1 misfire with all the excuses of the world'
date: '2008-12-15'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

---
like everyone else, I went to the stage: want to succeed the macaroons, 

well on first try, includes first failure, and as I say with all the excuses of the world: 

  1. a drummer not high end, it is rather a mixer foot has low capacity, so the egg white was not so famous 
  2. a bag with a funnel very small, I had to continue with a spoon. 
  3. I did not have the green as a dye to have a beautiful brown, I add instead of blue. 
  4. no icing sugar, but rather crystal sugar go blender 
  5. and what crowns it all is an oven that starts with a temperature of 220 degrees C, and to have a lower temperature, the oven lights up and down at the same time. 



** INGREDIENTS:  **

  * 110gr of almond powder 
  * 110gr of icing sugar 
  * 1cake of bitter cocoa 
  * 90gr of egg whites (3 small white or 2 and 1/2 for large eggs) 
  * 110gr of caster sugar 
  * 1st pinch of salt to mount the whites 
  * green and red dye. 



** STEPS:  **

finely mix the almond powder and the icing sugar for several minutes. 

we obtain a homogeneous and very fine powder (we can even sift it) 

We weigh our egg whites, we add our pinch of salt and we start to snow them, when they start to become sparkling, add a third of the sugar semolina (little bit, no need to weigh the third to the balance, do the egg, the main thing is to stay at 110gr of caster sugar), we beat long egg whites by adding the rest of the sugar as and when; they will gradually form a beautiful meringue. 

we can add at this stage a few drops of dye and beat again, mix the two masses delicately, that is to say the so much for so much (so much for it is in fact the mixture of almond powder and icing sugar) with the French meringue and macaronner without breaking the meringue, 

to that mix gently with your spatula by incorporating the two devices delicately and without, the result must be homogeneous and brilliant, neither liquid nor too condensed, 

place small heaps on baking paper in the pastry bag 

let crunch 30min (crunch, ie rest small macaroons, which will have them smooth and not cracked) I sprinkled some sesame on it and meanwhile preheat the oven to 160 ° 

cook for 15min, then for cooking, and to prevent our macaroons from crunching and sagging, there is the trick of the 3 plates, for that, it is enough to superpose 2 or 3 identical plates of which only the first one wear the macaroons and leave the oven door open, it will not only allow to cook macaroons at an ideal temperature but also develop the ruffle at the base of each macaroon, I did not get ... .. 

finally for a start it was good. 

kisses 
