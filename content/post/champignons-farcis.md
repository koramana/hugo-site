---
title: stuffed mushrooms
date: '2017-03-30'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/champignon-farcis-aux-artichauts-et-epinards_thumb.jpg
---
[ ![mushroom stuffed with artichokes and spinach](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/champignon-farcis-aux-artichauts-et-epinards_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/champignon-farcis-aux-artichauts-et-epinards.jpg>)

##  stuffed mushrooms 

Hello everybody, 

a [ Entrance ](<https://www.amourdecuisine.fr/categorie-10678929.html>) **easy** to achieve and very delicious, and no need to dirty all your trays, that's what I say facing these **stuffed mushrooms** , a recipe easy to make, mushrooms stuffed with a stuffing a little creamy and also very rich. A joke composed of **artichoke hearts** , of **spinach** , from **Parmesan cheese** , **Mozzarella** , and so on, a **very tasty stuffing** , well scented, who will hang you at the table to enjoy these mushrooms while they are still hot, huuuuuuuuuuuuuuu, 

You can also see the bolognese mushrooms, a delicacy of the latter. I invite you to explore even more [ recipes of appetizers, appetizers, appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) .... according to your means and according to your time, you will surely find recipes that will suit you. Without forgetting the index of [ ramadan recipes ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>)

It remains to taste these **mushrooms** I'm sure it'll be in your next **basket** of shopping.   


**stuffed mushrooms**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/champignon-farcis-artichauts-epinards_thumb.jpg)

portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 3 white mushrooms (10 to 12 cm in diameter) 
  * 2 portions of cheese the laughing cow 
  * 2 tablespoons mayonnaise 
  * 2 hearts of grilled artichokes, or steamed in small cubes 
  * 1 cup chopped spinach frozen, thawed and drained 
  * 2 cloves of garlic, chopped 
  * 2 tablespoons freshly grated Parmesan cheese 
  * 15 gr of mozzarella cut into small cubes. 
  * A pinch of black pepper   
for the ingredients of the stuffing, and according to your mushrooms you can decrease or increase the doses. mine I did not stuff them too much, because being on a diet, I wanted to reduce a little the portion I'm going to eat. 



**Realization steps**

  1. method of preparation: 
  2. Preheat the oven to 180 degrees C. 
  3. clean and wipe the mushrooms, remove the feet and gently remove them to make room for the stuffing. 
  4. In a large bowl, combine cheese, mayonnaise, diced artichoke hearts, spinach, garlic, parmesan cheese, black pepper and mozzarella. 
  5. Fill each mushroom cap with a little stuffing. 
  6. Place the mushrooms on a baking sheet (the tray must have raised sides, because the mushrooms will throw a little water) 
  7. cook in the oven for 15-20 minutes, or until the mushrooms are tender and the filling is golden brown. 
  8. Serve hot. with a nice fresh salad, or to change for what not with a [ tabbouleh ](<https://www.amourdecuisine.fr/article-taboule-libanais-118708599.html> "Lebanese tabouleh recipe") . 



[ ![2012-03-27 stuffed mushroom](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/2012-03-27-champignon-farcis_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/2012-03-27-champignon-farcis_2.jpg>)

you can also see the recipe of: 

[ Stuffed artichokes ](<https://www.amourdecuisine.fr/article-tajine-d-artichauts-farcis-97187541.html>)

[ stuffed olives ](<https://www.amourdecuisine.fr/article-brochettes-d-olives-farcies-102948239.html>)

[ tomates farcis ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs-101484090.html>)
