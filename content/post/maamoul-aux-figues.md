---
title: Maamoul with figs
date: '2012-04-20'
categories:
- crepes, beignets, donuts, gauffres sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateaux-secs-2_thumb11.jpg
---
[ ![Maamoul with figs](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateaux-secs-2_thumb11.jpg) ](<https://www.amourdecuisine.fr/categorie-10678931.html>)

##  Maamoul with figs 

Hello everybody, 

Another recipe classified in the dungeons, this fig maamoul that I had prepared during the feast of the aid: [ Algerian cakes from the aid ](<https://www.amourdecuisine.fr/article-bonne-fete-d-aid-kbir-et-mes-gateaux-algeriens-88105516.html>) , and that I completely forgot to post. 

I really like it [ maamoul ](<https://www.amourdecuisine.fr/article-maamoul-100449886-comments.html>) dates, or with any other joke, it looks a lot like [ Makrout ](<https://www.amourdecuisine.fr/article-26001222.html>) this [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-11814728.html>) that I like a lot. 

This time so it's with a stuffing with dried figs. and I do not tell you, for fans of figs, it's a delight. 

[ ![maamoul with figs, dry cakes](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/maamoul-aux-figues-gateaux-secs_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)   


**Maamoul with figs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateaux-secs-2_thumb1.jpg)

**Ingredients**

  * 250 g of fine semolina 
  * 250g of flour 
  * 250 g melted butter at room temperature 
  * 1 teaspoon vanilla 
  * 2 tablespoons sugar 
  * 1 pinch of salt 
  * ½ teaspoon of baking powder 
  * ½ teaspoon of water + flower water 

prank call: 
  * 200 gr of dried figs 
  * 25 gr of butter 
  * 3 to 4 tablespoons of honey (or according to taste, sometimes figs are not sweet at all) 
  * flower water to have a nice non-sticky stuffing. 



**Realization steps**

  1. Mix the ingredients dry and add the melted butter, 
  2. sand with your fingers, to incorporate the butter. 
  3. Add the mixture of water + flower water gently to collect the dough 
  4. form a ball and cover   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/maamoul-aux-figs-gateaux-secs-patisserie-orientale_thumb1.jpg)
  5. keep cool for 1 hour (for me it was all night) 
  6. Now prepare the stuffing, mixing the cut figs with the blinder, remove the hard part. 
  7. add butter and honey 
  8. remove the blinder, it will be very sticky, gently add the flower water, just to have a nice stuffing. 
  9. Take a small ball of dough the size of a walnut and flatten it in the hand 
  10. put a small amount of stuffing in the hollow. Close the ball and place it in the special mold MAAMOUL floured, 
  11. press a little, then pat a little on the table and drop the cake. 
  12. Cook in a preheated oven at 180 degrees until the cake is golden brown. 
  13. At the end of the oven, sprinkle with icing sugar. 


