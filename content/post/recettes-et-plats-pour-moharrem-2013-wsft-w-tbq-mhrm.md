---
title: recipes and dishes for moharrem 2013 وصفات و أطباق محرم
date: '2013-11-02'
categories:
- birthday cake, party, celebrations
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/couscous-611_thumb.jpg
---
##  recipes and dishes for moharrem 2013 وصفات و أطباق محرم 

Hello everybody, 

The Muslim community, will very soon celebrate the first day of the Muslim Year, 

the 1st Muharram, which is the first month of the Muslim calendar and one of the most important. 

and on this occasion, some families like to make a meal just to enjoy together. so I am putting together here a little index of the delicious recipes that are prepared for this occasion: **[ Chicken couscous ](<https://www.amourdecuisine.fr/article-couscous-au.html> "couscous with chicken, Algerian cuisine") ** ![https://www.amourdecuisine.fr/wp-content/uploads/2013/11/couscous-611_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/couscous-611_thumb.jpg) couscous very repute, and always present in the majority of families, light, rich in vegetables, and especially very tasty **[ Chakhchukhat eddfar ](<https://www.amourdecuisine.fr/article-chekhchoukha-constantinoise-chakhchoukha-de-constantine.html> "Constantinese Chekhchukhah, Constantine's Chakhchukha") ** ![https://www.amourdecuisine.fr/wp-content/uploads/2013/11/rus-007_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/rus-007_thumb.jpg) one of the most famous dishes, especially in eastern Algeria, in any case I like a lot. **[ Trida ](<https://www.amourdecuisine.fr/article-trida.html> "trida") **

another delicious dishes, the **[ Chakhchukha, or mfermsa ](<https://www.amourdecuisine.fr/article-chakhchoukha-de-biskra-charchoura-biskria-112155334.html> "Biskra chakhchoukha, Charchura Biskria") **

![biskra chakhchoukha, trida, mfermsa, charchoura](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chakhchoukha-de-biskra-trida-mfermsa-charchoura_thumb.jpg)

**[ Couscous with meat ](<https://www.amourdecuisine.fr/article-25345493.html> "Couscous with meat") **

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/25340955.jpg)

**[ how to cook couscous with steam ](<https://www.amourdecuisine.fr/article-comment-cuire-le-couscous-111575933.html> "how to cook couscous") **

![cooking steamed couscous 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cuisson-du-couscous-a-la-vapeur-3_thumb1.jpg)

you also have: 

[ ![couscous-Kabyle-a-la-cornille](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/couscous-kabyle-a-la-cornille-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/couscous-kabyle-a-la-cornille.jpg>)

[ le couscous Kabyle a la viande séchée ](<https://www.amourdecuisine.fr/article-couscous-kabyle-a-la-viande-sechee-couscous-au-guedid.html> "Kabyle couscous with dried meat: couscous with guedid")
