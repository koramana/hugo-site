---
title: Thousand leafs with fondant in cafe
date: '2011-03-28'
categories:
- amuse bouche, tapas, mise en bouche
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/03/millesf-010_thumb1.jpg
---
![Miles 010](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/millesf-010_thumb1.jpg)

Hello everybody, 

here is a cake that I like to prepare that I do not want to work too much my nostrils, because sometimes prepare a cake and think of his "look" and deco, hhihihiihih, well it's a broken head. 

So, when I am at the end of my strength and my husband is throwing me this sentence: "then there is nothing for breakfast tomorrow? " 

you imagine my head ?? !!! 

so the simplest of things is a thousand sheets, because the dough leaflets trade is still in the fridge, for moments like that, so quickly has the recipe that I always make eyes closed ... ..   


**Thousand leafs with fondant in cafe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/millesf-011_thumb1.jpg)

Recipe type:  pastry, dessert  portions:  12  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 3 rolls of puff pastry (trade for this time) 
  * 1 liter of custard 
  * fondant (I use the sugar dough already ready) 
  * 3 pieces of dark chocolate 

for pastry cream: 
  * 1 liter of milk 
  * 100g of flour 
  * 200 g sugar 
  * 4 egg yolks 
  * 1 vanilla pod 

for the glazing: 
  * 100 gr of sugar paste 
  * a little water 
  * one tablespoon of soluble coffee 



**Realization steps** prepare the pastry cream: 

  1. Put the milk to boil. 
  2. Meanwhile, beat the egg, sugar, vanilla sugar and flour in a bowl. 
  3. When the milk is boiling, immediately pour it into the salad bowl. Put back in the pan for a few minutes so that the liquid becomes the consistency of a cream. 

preparation of the cake 
  1. Roll out the puff pastry about 3 mm. Prick with a fork so that it does not swell and put it in the oven on 6, 180 ° C. 
  2. put it between two trays, so that it does not swell too much. 
  3. Spread the pastry cream on a first layer of dough, then do the same with a second. Assemble them. 
  4. Wait until the cake is cold to spread the frosting, 

for the glazing: 
  1. add water gradually to the sugar paste until it is well melted 
  2. spread this fondant on the last layer of the mille feuille 
  3. melt the chocolate, and place it in a sachet 
  4. make a hole and draw parallel lines on the fondant 
  5. make marbled drawings by passing a hill blade one time up and the other down 
  6. put your cake in the refrigerator, and let the cream take well, and the icing dry well, to be able to cut your thousand leaves 



merci pour vos commentaire et vos visites 
