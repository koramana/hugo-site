---
title: Nutella-chocolate pancake batter, easy and fast
date: '2016-08-26'
categories:
- crepes, donuts, donuts, sweet waffles
tags:
- Breakfast
- Algerian cakes
- To taste
- Easy recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pate-a-crepes-au-nutella_thumb1.jpg
---
![Nutella-chocolate pancake batter, easy and fast](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pate-a-crepes-au-nutella_thumb1.jpg)

##  Nutella-chocolate pancake batter, easy and fast 

Hello everybody, 

Nutella pancake batter / chocolate crepe, easy and fast, We'll never get tired of these delicious pancakes made with Nutella, yes you read well, pancakes to the taste of chocolate, but no matter which, Nutella, this dough spread with hazelnuts and cocoa, then a pancake dough with this taste .... 

This recipe is in Lunetoiles she shared with us, and that makes me personally too envy. surely I will soon realize it. 

On the blog you will find other recipes of crepes, [ crepes natures ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee.html> "plain pancakes, for sweet or savory garnish") , [ crepes with the taste of orange ](<https://www.amourdecuisine.fr/article-crepes-aromatisees-a-l-orange.html> "orange flavored pancakes") , [ crepes with coconut milk ](<https://www.amourdecuisine.fr/article-crepes-au-lait-de-coco.html> "pancakes with coconut milk") ... 

as you can find plenty of [ recipes with crepes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=crepes&sa=Rechercher>) . 

Do not forget my friends, if you realized one of the recipes of my blog, share with me the photos of your achievements on the following email: 

![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg)

**Nutella pancake batter / chocolate crepe, easy and fast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pate-a-crepes-au-chocolat_thumb1.jpg)

portions:  20  Prep time:  5 mins  cooking:  10 mins  total:  15 mins 

**Ingredients**

  * 250g of flour 
  * 1 pinch of salt 
  * ½ liter of milk 
  * 3 eggs 
  * 160 g of nutella 
  * butter for cooking 



**Realization steps**

  1. In a blender or robot put the flour, salt, milk, eggs, nutella. 
  2. Mix for 2 minutes at full power.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pate-a-crepes-au-nutella-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pate-a-crepes-au-nutella-1_thumb1.jpg>)
  3. Pour the dough into a bowl. 
  4. Let rest. 
  5. Cook the pancakes with butter. 


