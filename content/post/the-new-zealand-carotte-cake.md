---
title: the new zealand carrot cake
date: '2018-03-02'
categories:
- cakes and cakes
tags:
- desserts
- Cakes
- To taste
- Pastry
- Breakfast
- Easy cooking
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/the-new-zealand-cake-019_thumb1.jpg
---
##  the new zealand carrot cake 

Hello everybody, 

here is a super delicious recipe the new zealand carrot cake that I had to sting to participate in the game: 

our recipes have to be published today at 7 am, and I'm waiting for you, with this delicious cake: the new zealand cake, which comes out a carrot cake, new zealand version. 

It must be said that the choice was difficult, because the blog gaelle degorgee good and beautiful recipe, but I opt for this cake, because I like carrot cake and I love spices. 

so without delay we go to the recipe.   


**the new zealand cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/the-new-zealand-cake-019_thumb1.jpg)

**Ingredients**

  * 3 eggs 
  * ¾ cup brown sugar 
  * ½ cup of powdered white sugar 
  * ¼ cup of oil 
  * 1 teaspoon vanilla powder 
  * 2 cups of whole wheat flour (330 gr of normal flour for me) 
  * ½ teaspoon of salt 
  * 2 teaspoons cinnamon powder 
  * ½ teaspoon ginger powder 
  * 1 tip of nutmeg 
  * ½ sachet of yeast 
  * ¼ cup of milk 
  * 3 cups grated carrots 
  * 1 cup grated coconut 
  * ½ cup of pecan nuts (normal nut for me) 



**Realization steps**

  1. Preheat the oven to 180 ° C (th.6). 
  2. Whisk eggs and sugars, 
  3. then add the oil and vanilla flavor. 
  4. Pour together the flour, salt, yeast and spices 
  5. Then add the milk, grated carrots, coconut and whole pecans. 
  6. The dough is quite thick, with lots of carrots: it's normal. 
  7. Pour the dough into a buttered and floured baking tin. 
  8. Bake and cook for about 1 hour. 
  9. Let cool and unmold. 



un bien belle aventure, et surtout un delicieux cake 
