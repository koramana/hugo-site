---
title: Irish lamb stew
date: '2015-02-13'
categories:
- diverse cuisine
tags:
- Vegetables
- Meat
- dishes
- tajine
- Full Dish
- Sustainable cuisine
- Lamb tagine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/ragout-dagneau-irlandais.jpg
---
[ ![Irish lamb stew](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/ragout-dagneau-irlandais.jpg) ](<https://www.amourdecuisine.fr/article-ragout-dagneau-irlandais.html/ragout-dagneau-irlandais>)

##  Irish lamb stew 

Hello everybody, 

A delicious recipe that comes directly from Ireland, the cuisine of my friend Nawel Zellouf. I think now you're starting to get used to his delicious food. 

This time she shares with us the real recipe for Irish Lamb Stew "Irish lamb stew", she had it from a real chef, and I'm up for personally trying this recipe very soon. 

A well-scented stew, rich in ingredients, especially in reduced sauce, to garnish a delicious [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html> "mashed potatoes") . 

**Irish lamb stew**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/ragout-dagneau-irlandais.jpg)

Cooked:  Irishwoman  Recipe type:  dish  portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * Pieces of lamb shank 
  * An onion 
  * A clove of garlic 
  * 2 carrots 
  * Some broccoli flowers 
  * 200 gr of mushroom 
  * A bay leaf 
  * Fresh thyme. 
  * 1 teaspoon of concentrated tomato 
  * 2 tablespoons of butter 
  * 2 tablespoons flour 
  * 1 glass and ¼ water. 
  * Salt, black pepper, just a little bit of cinnamon 

for potato puree 
  * 1kg boiled and mashed potatoes 
  * Butter 
  * fresh cream 
  * salt, pepper and nutmeg. 



**Realization steps**

  1. In a cooking pot with a thick bottom, put the butter and cut meat in cubes 
  2. let it come back, until the meat rejects its water well. 
  3. add all the vegetables, and sauté, 
  4. when the sauce is reduced, add the flour to thicken the sauce 
  5. When the red is good, add the water 
  6. add spices, tomato, bay leaf and thyme 
  7. cook until everything becomes very tender and the sauce is well reduced. 
  8. prepare the mashed potato, and introduce it with the meat stew. 


