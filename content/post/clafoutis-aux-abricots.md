---
title: Clafoutis with apricots
date: '2014-06-16'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/clafoutis-aux-abricots-4_thumb1.jpg
---
##  Clafoutis with apricots 

Hello everybody, 

yes I know, this is not the season of apricots, but here is this recipe is in the archives of my email, and I decided to arrange to program it at the right time, but here is the recipe is put online. 

then I post the recipe for this delicious clafoutis with apricots, make with fresh apricots from the garden of Lunetoiles. 

**Clafoutis with apricots**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/clafoutis-aux-abricots-4_thumb1.jpg)

portions:  8  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * Ingredients: 
  * 4 eggs 
  * 120g of sugar 
  * A packet of vanilla sugar 
  * 80g of flour 
  * 20g of cornflour 
  * A C. coffee baking powder 
  * 250 ml of milk 
  * 30g of melted butter 
  * apricots 



**Realization steps**

  1. Beat eggs with sugar and vanilla sugar 
  2. Add flour, maizena, yeast and mix well. 
  3. Add the butter and finally the milk 
  4. Wash the apricots, cut in half. 
  5. Spread them at the bottom of a baking pan covered with baking paper 
  6. Pour the dough and cook in a preheated oven at 180 ° C for about 40 minutes. 
  7. At the end of the oven let cool to room temperature before serving. 



régalez vous bien car c’est un réel délice 
