---
title: Homemade brioche
date: '2016-10-27'
categories:
- Brioches et viennoiseries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/brioche-maison-103.CR2_thumb1.jpg
---
[ ![homemade brioche 103.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/brioche-maison-103.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-brioche-fait-maison.html>)

##  Homemade brioche 

Hello everybody, 

I really like homemade brioches, and this recipe that I will share with you, is really the best, a brioche mie very fast, a brioche, very light, super soft .... 

I do not think I'm going to change this dough, and I'm going to do it in all the sauces as they say ... .. because frankly, it's "La Brioche" that I have always looked for. 

[ ![tlitli constantinois 022](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tlitli-constantinois-022_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-brioche-fait-maison.html>)

Without delay I give you the recipe, but I bring to your attention a very important point, be passions with your buns .... 

I started kneading my brioche at noon .... and it was not in the oven until 5 pm, because after degassing and shaping the buns, I went to pick up the children from school, and then back home, it was already their sport session, so I left my buns in drafts, under a cloth. 

when I came back, the buns had pushed well, so they were super light, and they quickly cooked, I do not know, if it is the effect of the oven, because it has cooked in 15 min max .... 

but in any case, the buns were very very light, my husband very much appreciated, he who always criticize my buns. 

and the kids thought they were buns from the baker ... .. no, no. 

[ ![homemade brioche 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/brioche-maison-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-brioche-fait-maison.html>)   


**Homemade brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/Brioche-fait-maison._thumb1.jpg)

portions:  8  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * 550 gr of flour 
  * 65 gr of sugar 
  * 5 eggs 
  * 3 tablespoons milk powder 
  * 2 tablespoon of potato starch 
  * 1 teaspoon of salt 
  * 100 gr of butter 
  * 100 ml of milk 
  * zest of a lime, and a yellow lemon 
  * 1 tablespoon instant baker's yeast (or a packet) 

decoration: 
  * 1 egg yolk 
  * 1 tablespoon of milk 
  * pearl sugar. 



**Realization steps**

  1. I made the brioche to the bread machine, but you can realize it to the petrin or to the hand (it is necessary to support a little the paste which sticks to the hands, at the beginning of the petrissage) 
  2. place all the ingredients in the breadmaker's bowl in the order recommended for your appliance other than the butter. 
  3. let knead until the dough collects well, and add the butter slowly in small pieces. 
  4. if your dough is a bit sticky, add a little flour. 
  5. leave the dough rested and doubled in volume, between 45 min and 60 min. 
  6. degas the dough and cut it into 3 balls (according to your cake molds, I made 3 buns in cake molds of 22cm by 10 cm) 
  7. spread out each ball in a rectangle, 1 cm high and roll it on itself, do not make a big pudding, and if it is larger than the mold, cut with the knife. 
  8. place the sausages in the floured and buttered molds, or covered with baking paper. 
  9. let the buns rise between 1 and 1:30 (for my part I leave almost 4 to 5 hours) 
  10. heat the oven to 180 degrees C, brush the buns with the mixture: egg yolk and milk. 
  11. decorate with pearl sugar. 
  12. bake between 20 minutes and 30 minutes, or depending on the capacity of your oven. 
  13. remove from the oven and let cool on a wire rack. 



[ ![homemade brioche 086.](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/brioche-maison-086._thumb1.jpg) ](<https://www.amourdecuisine.fr/article-brioche-fait-maison.html>)
