---
title: biscuit rolled with lemon
date: '2016-12-28'
categories:
- dessert, crumbles and bars
- gateaux, et cakes
- recettes sucrees
tags:
- Fruits
- desserts
- To taste
- Breakfast
- Ramadan
- Algeria
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Roul%C3%A9-au-citron-1.jpg
---
[ ![Lemon roll 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Roul%C3%A9-au-citron-1.jpg) ](<https://www.amourdecuisine.fr/article-biscuit-roule-au-citron.html/roule-au-citron-1>)

##  biscuit rolled with lemon 

Hello everybody, 

A delightful version of the biscuit rolled with lemon of my friend **Tim De Bouj** , that I liked, besides I did not hesitate to realize its lemon cream, to make in my turn, a super [ delicious biscuit rolled with lemon mousse ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-mousse-de-citron.html> "Biscuit rolled with lemon mousse") all melting, and super good. 

The biscuit genoise recipe is different from mine, but we can see the texture of the cake is super airy. In any case now you have two recipes of biscuits rolled with lemon, you choose the one you like the most.   


**biscuit rolled with lemon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Roul%C3%A9-au-citron.jpg)

portions:  10  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** for the rolled biscuit: 

  * 4 eggs. 
  * 150 g of sugar. 
  * 75 g flour. 
  * 75 g cornflour. 
  * ½ teaspoon of baking powder. 
  * lemon zest 

Lemon Cream: 
  * 1 egg. 
  * 150 g of sugar. 
  * zest of a lemon. 
  * 100 ml of lemon juice. 
  * 100 ml of water. 
  * 1 tablespoon cornflour. 

garnish: 
  * icing sugar. 



**Realization steps** prepare the dough: 

  1. in a bowl, beat the eggs with the sugar until the mixture is whitening. 
  2. Add lemon peel, cornflour, flour, and baking powder. Stir well. 
  3. in a dish lined with parchment paper, pour the mixture and bake in a preheated oven at 180 degrees C for 10 min. 

meanwhile prepare the cream: 
  1. in a saucepan, pour the sugar, water and lemon juice bring to a boil 
  2. in a bowl mix the egg with the cornflour and pour the mixture lemon water while hot on this mixture and whip vigorously, 
  3. put everything back in the saucepan over low heat until thickened, pull from the heat and allow to cool. 
  4. after cooking, put the biscuit on a wet towel and roll to take shape. 
  5. remove the towel, spread the cream on the biscuit and roll. 
  6. Sprinkle with icing sugar. 



[ ![Lemon roll](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Roul%C3%A9-au-citron.jpg) ](<https://www.amourdecuisine.fr/article-biscuit-roule-au-citron.html/roule-au-citron>)
