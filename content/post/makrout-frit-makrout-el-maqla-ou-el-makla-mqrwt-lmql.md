---
title: Makrout Fried, makrout el maqla or el makla مقروط المقلة
date: '2012-04-17'
categories:
- idee, recette de fete, aperitif apero dinatoire

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makrout-aux-dattes_thumb.jpg
---
[ ![Makrout Fried, makrout el maqla or el makla مقروط المقلة](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makrout-aux-dattes_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/makrout-aux-dattes.jpg>)

##  Makrout Fried, makrout el maqla or el makla مقروط المقلة 

Hello everybody, 

Makrout Fried, makrout el maqla or el makla مقروط المقلة: here is a delicious [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) , that I personally like a lot, every time I go down in **Algeria** , I ask my father to buy me, those of the local pastry chef, are irresistible, a pastry that melts in the mouth, and with the taste of stuffed dates fried, huuuuuuuuuum, it is absolutely necessary to try it, or buy it, hihihihihi .... 

so when I prepared the [ bradj ](<https://www.amourdecuisine.fr/article-bradj-gateau-de-semoule-100622694.html>) last week, I had a few dates stuffing, and I had this great desire to enjoy this **pastry** that I like a lot. 

my husband loves more the [ Baked makrout ](<https://www.amourdecuisine.fr/article-26001222.html>) , or [ makrout el koucha ](<https://www.amourdecuisine.fr/article-26001222.html>) , as we like to call it, so I enjoy the taste this delicious and **fondant makrout** with a nice cup of **Mint flavored tea.**

you can see other recipes from the [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) . and here all the recipes of [ Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) .   


**Makrout Fried, makrout el maqla or el makla مقروط المقلة**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/makroud-makrout-frit-300x225.jpg)

portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 400 gr of semolina 
  * 80 gr of flour 
  * 160 gr of smen (salted butter) 
  * ½ teaspoon of granulated sugar, 
  * a pinch of salt 
  * Water, orange blossom water 

for the stuffing of dates: 
  * 250g of date paste 
  * vegetable oil 
  * ½ teaspoon of cinnamon powder 
  * ½ coffee of clove powder 

Oil for frying, honey and cracked almond. 

**Realization steps**

  1. in a bowl, put the semolina, the flour, the sugar and the pinch of salt and the fat, mix everything. 
  2. pick it up with the mixture of water and orange blossom water, so as to obtain a paste that is neither too dry nor too soft, 
  3. let rest the dough. 
  4. during this time, prepare the date stuffing, if your date paste is too hard, go to the steam, it will become very soft. 
  5. knead the date paste with a little oil, add the cinnamon and the clove powder. 
  6. spread the semolina paste on the worktop 5 mm thick, 
  7. add on the date paste, also spread apart to almost 4 mm 
  8. start rolling the two pasta together to form a coil. 
  9. then cut small rolls less than 2 cm wide 
  10. fry the rolls in a bath of hot oil until they turn a beautiful golden color, 
  11. drain them, and dip them directly into the cold syrup! Or let cool and dip in the hot syrup, to have a good result. 
  12. you can decorate and it is optional the edges of the rolls with crushed almonds. 



[ ![Makrout Fried, makrout el maqla or el makla مقروط المقلة](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/patisserie-algerienne-makrout-escargot_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/patisserie-algerienne-makrout-escargot_2.jpg>)

[ ![Makrout Fried, makrout el maqla or el makla مقروط المقلة](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/makroud-makrout-frit_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/makroud-makrout-frit.jpg>)
