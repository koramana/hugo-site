---
title: gazelle horns with hazelnuts
date: '2015-06-28'
categories:
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian cakes
- Algeria
- Algerian patisserie
- delicacies
- Aid
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cornes-de-gazelle-aux-noisettes-2_thumb3.jpg
---
##  gazelle horns with hazelnuts 

Hello everybody, 

Here is a recipe that we love all the horns of gazelles, I like a lot, especially when the dough is super fine and crisp, and the interior is crunchy and melting .... yum yum, to fall. 

I was lucky enough to eat gazelle horns during the circumcision feast of my girlfriend's son, and her mother had made horns of gazelles, peanuts .... ah lalalala, I will never forget this taste,. 

Today's recipe is a preparation for Lunetoiles, bravo my dear, for this delight and for this success. 

**gazelle horns with hazelnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cornes-de-gazelle-aux-noisettes-2_thumb3.jpg)

portions:  30  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** Dough: 

  * 1kg of flour 
  * 400g of melted butter 
  * The first pinch of salt 
  * 2 eggs 
  * dozens of tablespoons of orange blossom 
  * a little vanilla if you like) 
  * water to pick up the dough 

Hazelnut stuffing: 
  * 500g coarsely ground hazelnuts 
  * 240gr of powdered sugar 
  * 2 teaspoons cinnamon 
  * 50-60 gr of melted butter 
  * Orange Blossom 



**Realization steps**

  1. Begin by preparing the stuffing by mixing all the ingredients mentioned, add a little orange blossom while working the stuffing that must be easily held, 
  2. take small balls of 20 grams (I have weighed each time) 
  3. to lengthen them in pudding then in croissants, you can help yourself with a glass to form a pretty croissant, I advise you to form all the croissants before going to the shaping of the horns of gazelle 
  4. To prepare the dough, start by working the flour, salt and melted butter, 
  5. then add the rest of the ingredients. 
  6. Add water slowly to obtain a smooth paste that is easy to work with. 
  7. Take a little dough and roll it out finely, 
  8. arrange a croissant of almond and cover with dough by marrying the shape of the croissant by pressing slightly to make adhere the paste 
  9. use a notched wheel to cut off the surplus dough to form a beautiful gazelle horn, 
  10. with the help of a Nekkache, to form motives on the whole of the horn, I made them rather discrete but if you wish them more marked be careful not to tear the dough 
  11. Bake at 180 ° for 15 minutes, you must monitor the horns of gazelles that should not brown too much, leaving the oven to cool and ready! 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
