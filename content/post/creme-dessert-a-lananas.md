---
title: Dessert cream with pineapple
date: '2015-04-16'
categories:
- panna cotta, flan, et yaourt
- recettes sucrees
- verrines sucrees
tags:
- biscuits
- desserts
- verrines
- Fruits
- Healthy cuisine
- Trifles
- Kitchen without Milk

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/creme-dessert-a-lananas-1.jpg
---
[ ![Dessert cream with pineapple](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/creme-dessert-a-lananas-1.jpg) ](<https://www.amourdecuisine.fr/article-creme-dessert-a-lananas.html/creme-dessert-a-lananas-1>)

##  Dessert cream with pineapple 

Hello everybody, 

The story of this pineapple dessert cream recipe When we launch the game of the last round: [ recipes around one ingredient: pineapple ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-4-ananas.html> "recipes around an ingredient # 4 Pineapple") , I launched the same challenge on the kitchen group on facebook, and one of my friends to make this recipe. 

A very creamy cream dessert pineapple qu'à realize Wamani merou, a friend of mine. Frankly, I really liked this dessert recipe without milk, and it is with great pleasure that I share the recipe with you, I hope it will please you.   


**Dessert cream with pineapple**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/creme-dessert-a-lananas.jpg)

portions:  4-6  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 3 eggs 
  * 2 to 3 tablespoons of sugar 
  * The juice of half a can of pineapple or even a little more 
  * juice of a half lemon 
  * 1 tablespoon of butter 
  * vanilla cookies as needed 



**Realization steps**

  1. mix all the ingredients in a pyrex bowl 
  2. put on a bain marie and stir constantly 
  3. as thickened cream, out of heat add a little butter 
  4. well mixed and let cool 
  5. Edit with vanilla cookies and decorate according to your taste 



[ ![Dessert cream with pineapple](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/dessert-a-lananas-150x150.jpg) ](<https://www.amourdecuisine.fr/article-creme-dessert-a-lananas.html/dessert-a-lananas>)
