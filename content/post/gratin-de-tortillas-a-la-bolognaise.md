---
title: bolognese tortillas gratin
date: '2016-06-02'
categories:
- Algerian cuisine
- diverse cuisine
- recette de ramadan
tags:
- Pasta gratin
- Bechamel
- dishes
- Full Dish
- Minced meat
- Ramadan 2016
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognase-003.jpg
---
[ ![recipe gratin with bolognese tortillas](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognase-003.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognase-003.jpg>)

##  Bolognese tortilla gratin recipe 

Hello everybody, 

For lunch we ate delicious gratin bolognese tortillas, which I prepared with commercial tortillas. What I do not like, I like to do [ homemade flour tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html> "Flour tortillas") but when you're overwhelmed and you just want to eat, we do with what we have on hand! But my husband is a big fan of dishes and recipes based on **tortillas,** and because he saw that it's been a while that I did not prepare tortillas or recipes with yesterday he came back with 4 packets of tortillas on hand, first there was an offer, and secondly, he wanted me prepare one of my surprise recipes with tortillas. 

With some **tortillas** we can do wonders, it is not just used to roll to cover a salad and a piece of chicken breast, that is to say make them in "wraps". At home, it's always a new and different recipe, I do with what I have at home, as for example this recipe for [ pizza tortillas ](<https://www.amourdecuisine.fr/article-pizza-tortillas-aux-champignons.html> "Pizza mushroom tortillas") these [ chicken quesadillas ](<https://www.amourdecuisine.fr/article-quesadillas-au-poulet.html>) , or the [ chicken burritos baskets ](<https://www.amourdecuisine.fr/article-paniers-de-burritos-au-poulet.html>) . 

Today is a **bolognese tortillas gratin** it's not magic as a recipe, it's simple and easy. the time that the Bolognese sauce cooked in my assets, I prepared the Bolognese ... 

![recipe of bolognese tortilla gratin](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognaise-1.jpg)

15 minutes later, I was already rolling the tortillas to cover the Bolognese ... It only remained to prepare a small tomato sauce to accompany the dish. Because I personally do not like the Bolognese sauce too flowing that may give a bad result in the end. and so that the dish is not too dry, I add this tomato sauce on the side ... And here is a gratin of tortillas bolognese as good as easy.   


**bolognese tortillas gratin**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognase-020.jpg)

portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 6 to 8 [ tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html> "Flour tortillas") , depending on the number of people, and the width of the dish 
  * [ bolognaise sauce ](<https://www.amourdecuisine.fr/article-sauce-bolognaise-maison-halal-et-sans-vin.html> "homemade Bolognese sauce / halal and without wine")
  * [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video")
  * 1 handful of mozzarella 

tomato sauce express: 
  * 3 fresh tomatoes, or a box of crushed tomatoes 
  * 1 crushed garlic clove 
  * 2 branches of thyme 
  * salt and black pepper 
  * 2 tablespoons of oil. 



**Realization steps**

  1. Prepare the tortillas, if you do not have the one of the trade 
  2. prepare the Bolognese sauce 
  3. prepare the béchamel sauce 
  4. take a tortilla and cover with a thin layer of Bolognese sauce, the sauce is not too liquid ok.   
![recipe of bolognese tortilla gratin](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognaise-2.jpg)
  5. roll the tortilla to cover the Bolognese. 
  6. cut pieces of almost 4 cm each   
![recipe of bolognese tortilla gratin](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognaise-6.jpg)
  7. place the little rolls as you go in the mold on the béchamel, making sure to tighten them so that they do not unfold   
![recipe of bolognese tortilla gratin](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognaise-5.jpg)
  8. do so until you fill your mold.   
![recipe of bolognese tortilla gratin](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognaise-6-001.jpg)
  9. place the dish in a preheated oven at 180 degrees C and cook for almost 15 minutes, or until the dish turns a beautiful color. 
  10. When baking the gratin, prepare the tomato sauce, placing all the ingredients in the pan, watch for a nice smooth sauce.   
![recipe of bolognese tortilla gratin](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognaise-4.jpg)
  11. Serve the dish immediately out of the oven, and Enjoy! 



[ ![recipe gratin with bolognese tortillas](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognase-010.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognase-010.jpg>)
