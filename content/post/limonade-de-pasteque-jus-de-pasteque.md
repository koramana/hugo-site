---
title: watermelon-watermelon juice
date: '2016-05-13'
categories:
- juice and cocktail drinks without alcohol
tags:
- Drinks
- Easy cooking
- Ramadan
- Organic cuisine
- Ramadan 2016
- Healthy cuisine
- cocktails

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/limonade-de-pasteque-5.jpg
---
[ ![watermelon lemonade 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/limonade-de-pasteque-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/limonade-de-pasteque-5.jpg>)

##  watermelon-watermelon juice 

Hello everybody, 

The watermelon lemonade, or if you want you can also call it watermelon juice is the number 1 drink for months in the summer. You will never open my fridge without finding a pitcher or a bottle of watermelon juice. 

It is refreshing, it is super moisturizing, it is well scented and too good as juice. In addition to all this it is a drink very easy to prepare, and regardless of the quality of the watermelon, sweet or just moderately good, instead of throwing a watermelon you buy and which you are disappointed in his taste, do in a good watermelon lemonade, and you'll see, you're not going to go out for the whole summer. 

[ ![watermelon lemonade](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/limonade-de-pasteque.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/limonade-de-pasteque.jpg>)   


**watermelon-watermelon juice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/limonade-de-pasteque-6.jpg)

portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 500 gr of watermelon cut into cubes 
  * powdered sugar according to your taste and according to the quality of the watermelon 
  * the juice of a lemon 
  * water according to your taste too 
  * slices of lemon 
  * mint leaves 
  * ice cubes 



**Realization steps**

  1. place the watermelon cubes in the bowl of a blender. 
  2. mix until everything is well liquified 
  3. pass through a fine-mesh strainer 
  4. In a large pitcher, mix the watermelon juice with the water, lemon juice and sugar until the sugar is dissolved. Adjust the amounts of sugar and water according to your taste 
  5. Add mint leaves and slices of lemon and chill 
  6. When serving, add ice cubes and enjoy. 



![watermelon-watermelon juice 2 lemonade](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/limonade-de-pasteque-2.jpg)
