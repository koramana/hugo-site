---
title: watermelon jam video
date: '2013-08-23'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-pasteque-1024x919.jpg
---
[ ![watermelon jam](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-pasteque-1024x919.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-pasteque.jpg>)

##  watermelon jam video 

Hello everybody, 

For you this watermelon jam video, an ideal recipe for you if you came across a watermelon not sweet and tasteless, and it is really worth throwing it especially if it is all red and juicy. 

We can easily make a watermelon juice, a juice that I like a lot and a recipe coming soon. 

This time I chose to make a jam ... yes I am in the middle of jam season, besides I already prepare 4 to 6 jams,   


**watermelon jam video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-pasteque-1024x919.jpg)

Recipe type:  jam  portions:  8  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * watermelon 
  * sugar 



**Realization steps**

  1. the recipe is detailed in the video 



There are recipes that are already on the blog like: 

[ melon jam ](<https://www.amourdecuisine.fr/article-confiture-de-melon-110080011.html>)

[ fig and melon jam ](<https://www.amourdecuisine.fr/article-confiture-de-figues-et-melon-119628571.html>)

[ grape jam ](<https://www.amourdecuisine.fr/article-confiture-de-raisins-56300967.html>)

[ pehes jam ](<https://www.amourdecuisine.fr/article-confiture-de-peches-un-delice-62938233.html>)

and the remaining recipes are coming. 

pour le moment je vous laisse avec la video: 
