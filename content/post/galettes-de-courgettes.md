---
title: Zucchini patties
date: '2017-06-01'
categories:
- appetizer, tapas, appetizer
tags:
- Vegetables
- Fast Food
- Aperitif
- Cheese
- Street Food
- Economy Cuisine
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/galettes-aux-courgettes-2.jpg
---
[ ![zucchini cakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/galettes-aux-courgettes-2.jpg) ](<https://www.amourdecuisine.fr/article-galettes-de-courgettes.html/galettes-aux-courgettes-2>)

##  Zucchini patties 

Hello everybody, 

Zucchini patties to serve as a starter, or to eat in a snack (sandwich), yes yes, hihihi. In fact my husband ate them like that, and he does not know how to stop at a cake, with harissa. 

[ ![zucchini cakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/galettes-de-courgettes-3.jpg) ](<https://www.amourdecuisine.fr/article-galettes-de-courgettes.html/galettes-de-courgettes-3>)   


**Zucchini patties**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/galettes-de-courgettes.jpg)

portions:  15-20  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 500 gr of zucchini 
  * 40 gr grated Cheddar cheese 
  * 20 gr grated Parmesan cheese (otherwise 60 gr cheese of your choice, in between) 
  * ¾ cup of breadcrumbs. 
  * 3 eggs 
  * salt, black pepper, and garlic powder 



**Realization steps**

  1. Grate the washed zucchini 
  2. Add the grated cheeses 
  3. whisk the eggs and stir them into the mixture 
  4. now introduce the bread crumbs and season. 
  5. heat a small bath of oil (not too much oil) and pour with a tablespoon small heaps of grated zucchini mixture. 
  6. shape with the spoon to give a nice shape to your patties, 
  7. cook on both sides and place on paper towels, you can serve immediately. Otherwise if you prepare them in advance, heat in the oven before serving. 



[ ![zucchini cakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/galettes-de-courgettes-1.jpg) ](<https://www.amourdecuisine.fr/article-galettes-de-courgettes.html/galettes-de-courgettes-1>)
