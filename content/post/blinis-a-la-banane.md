---
title: banana blinis
date: '2013-10-12'
categories:
- bavarois, mousses, charlottes, agar agar recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/blinis-a-la-banane_thumb.jpg
---
Hello everybody, 

Here is a recipe that children often ask, since they drip, these blinis very soft and tasty to the taste of crushed banana and mixed with the dough and scented with orange peel, a flavor with each bite, especially with this maple syrup coulis, an irresistible taste.   


**banana blinis**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/blinis-a-la-banane_thumb.jpg)

Recipe type:  crepes with dessert  portions:  4  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 150 gr of flour 
  * 1 teaspoons of baking powder 
  * 1 cup of sugar 
  * a pinch of salt 
  * 250 ml of buttermilk 
  * 1 egg 
  * 2 tablespoons melted butter 
  * 3 ripe bananas 
  * Orange zest 

maple syrup cream: 
  * 85 gr of butter 
  * 4 tablespoons maple syrup 



**Realization steps**

  1. sift the flour, add the baking powder, sugar, and salt. 
  2. then add buttermilk, egg and melted butter and beat well. 
  3. crush the bananas and introduce to the mixture 
  4. add the orange peel and let stand for 5 minutes. 
  5. gently oil a pan, and start cooking your blini, pouring the equivalent of almost 1 large soup mix to form a small circle. 
  6. cook until holes appear on the surface, then bake on the other side 
  7. finish until the dough is exhausted. 
  8. for the maple syrup cream, beat the butter with the maple syrup until it becomes creamy. 
  9. serve immediately your warm blinis with a nice spoon of this cream, and slices of banana. 



good tasting, and the next recipe 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
