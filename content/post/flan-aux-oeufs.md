---
title: Egg flan
date: '2015-03-24'
categories:
- panna cotta, flan, et yaourt
tags:
- Easy cooking
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/flan-aux-oeufs-060_thumb2.jpg
---
##  Egg flan 

Hello everybody, 

A dessert that I often prepared with my mother, especially during Ramadan, when I was young. Very tasty, very unctuous, easy to make this custard egg custard and pleasantly melting. What I like most is especially that we do not need to stand in front of the stove and mix constantly so that it does not stick to the bottom. 

An invaluable recipe, timeless, indestrônable and the most important very easy to do ....   


**Egg flan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/flan-aux-oeufs-060_thumb2.jpg)

Recipe type:  Dessert  portions:  8  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * ingredients: 

For the custard (for my custard I made half of the ingredients): 
  * \- 8 eggs 
  * \- 1 liter of milk 
  * \- 120 gr of powdered sugar 
  * \- 1 vanilla pod 

For the caramel: 
  * \- 70 gr of sugar 
  * \- 2 tbsp. tablespoons water 



**Realization steps**

  1. Turn on the thermostat oven 7 (200 °). 
  2. Pour the milk into a saucepan. 
  3. Cut the vanilla pod in half lengthwise. With a sharp knife, scrape the small seeds and put them in the milk. 
  4. Add the pieces of vanilla bean. Cook on low heat, remove from heat just before boiling. 
  5. Meanwhile, break the eggs in a large bowl, add the sugar, whip vigorously for 2 to 3 minutes until the mixture is frothy. 
  6. Remove the pieces of vanilla bean from the milk. Pour a small amount of milk over the eggs, stirring quickly to avoid cooking. Continue to pour the milk gradually without stopping. When all the milk is incorporated turn another few seconds. 
  7. Heat 1 liter of water in a saucepan (it's for the bain-marie). 
  8. Put the sugar cubes and 2 tablespoons of water in the flan dish. Put the mold on medium heat. Let caramelize. 
  9. As soon as the caramel starts to take a little color, tilt the mold in all directions to spread the caramel on the walls. 
  10. Put the flan dish in a larger dish. 
  11. Pour the egg custard into the caramelized pan. Pour the hot water into the mold containing it. 
  12. Put in the oven 45 minutes. 
  13. At the end of cooking the texture of the custard is well fixed. to check the cooking, plant a knife in the middle, the blade must come out without trace of liquid. 
  14. Remove the mold from the oven and the water bath, let cool to room temperature. When the custard is cold, put in the refrigerator. 
  15. Take out just before serving. 


