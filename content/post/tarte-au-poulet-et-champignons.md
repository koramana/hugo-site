---
title: chicken and mushroom pie
date: '2017-11-01'
categories:
- pizzas / quiches / pies and sandwiches
tags:
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-au-poulet-et-champignons.jpg
---
[ ![chicken and mushroom pie](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-au-poulet-et-champignons.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-au-poulet-et-champignons.jpg>)

##  chicken and mushroom pie 

Hello everybody, 

When I'm doing [ Oven-roasted chicken ](<https://www.amourdecuisine.fr/article-poulet-roti-au-four.html>) at home they eat thighs and wings, but a large part of the chicken's breast remains, I think it happens to all families. Frankly, it hurts me to throw away what's left, so I'm often able to make a recipe with it. 

My husband always likes me to make him a spicy tomato sauce and I put the pieces of chicken in it and voila you're done. On the other hand, I like to make quiches or salted pies, and this chicken and mushroom pie is the one I do most often and even if my husband pretends not to like it, but I promise you that with a good salad he eats much more than me, hihihihih 

![chicken and mushroom pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-au-poulet-et-champignons-2.jpg)

You can see here a different version with bechamel sauce to garnish the pie instead of an egg and cream stuffing, it's a recipe made by my friend Lynda Akdader, and here is the video: 

**chicken and mushroom pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-au-poulet-et-champignons-3.jpg)

portions:  8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** [ salty pie dough ](<https://www.amourdecuisine.fr/article-pate-a-tarte-salee.html>)

  * 250g of flour 
  * 30 ml of cold water 
  * 1 egg 
  * salt 
  * 125 gr of ointment butter 

the joke: 
  * a precooked chicken breast (if fresh, cut into cubes, salt and fry in a mixture of butter and oil) 
  * 200 gr mushrooms cut into slices 
  * 1 finely chopped shallot 
  * 2 tbsp. tablespoon of olive oil 
  * some branches of parsley 
  * 1 tablespoon of butter 
  * 3 eggs 
  * 250 ml of fresh cream 
  * 1 little nutmeg powder 
  * 100 g grated Gruyère cheese   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-au-poulet-et-champignons-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-au-poulet-et-champignons-1.jpg>)



**Realization steps** preparation of the pasta: 

  1. Pour the flour on a work surface and add the butter ointment with your fingertips, merging the mixture. You will get a sandy mix like a crumble dough. 
  2. Make a well and add the egg, water and salt while kneading. 
  3. When a ball begins to form, pour it on the worktop. 
  4. Stretch the dough 5 to 6 times with the palm of your hand on the work surface, form a ball. 
  5. Wrap it in plastic wrap and store in the refrigerator for 2 hours before using.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-a-tarte-sal%C3%A9e-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-a-tarte-sal%C3%A9e-2.jpg>)
  6. spread out the dough, go into a pie plate and place in the fridge for the preparation of the stuffing. 

preparation of the stuffing: 
  1. Heat the oil and butter in a frying pan, and sauté the shallot 
  2. As soon as they are translucent, add the mushrooms. Cook for 5 minutes over high heat. Add salt and pepper. 
  3. Add the chicken cubes and sauté a little. Reserve off the heat. 
  4. In a bowl, beat the eggs with the cream, chopped parsley and nutmeg. Add salt and pepper. Add the mushrooms, shallots and chicken mixture. 
  5. remove the pie plate from the fresh, prick the surface of the dough with a fork and cover the dough with a piece of baking paper, 
  6. pour dry beans on the paper, and cook the dough for about 15 minutes. 
  7. After 15 minutes of cooking, remove the dough, remove the baking paper and line the bottom of grated Gruyère pie. 
  8. Pour on the device. Decorate with a little gruyere and cook in a preheated oven at 180 ° C for 15 to 20 minutes. 



![chicken and mushroom pie 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/tarte-au-poulet-et-champignons-4.jpg)
