---
title: Moroccan Harira
date: '2016-11-18'
categories:
- cuisine marocaine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/harira-soupe-marocaine1.jpg
---
[ ![harira moroccan soup1](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/harira-soupe-marocaine1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/harira-soupe-marocaine1.jpg>)

##  Moroccan Harira 

Hello everybody, 

A Moroccan harira that I strongly advise you, this rich soup harira is just a delight, even my children who are a bit difficult, have eat this recipe to accompany matloua bread in the oven, without any protest, lol ... 

I must confess something to you all the same, I was a little puzzled when the amount of tomatoes used in the recipe, because I was afraid to have a harira a little acid, strong happy, the recipe is a great success ... because the addition of vermicelli, and flour-based binder, will remove all the acidity ... the result: an enticing soup   


**Moroccan Harira**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/harira-soupe-marocaine.jpg)

portions:  4  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 150 g of meat of your choice cut into small cubes 
  * 1 minced onion 
  * 1 small bunch of chopped parsley 
  * 1 bunch of chopped fresh coriander 
  * 100 g of chickpeas in box of their skin. 
  * ½ teaspoon of turmeric, 
  * ½ teaspoon of ginger, 
  * ½ c. pepper, 
  * 2 tbsp. salt 
  * 450 g cut tomatoes (canned) 
  * 1 tablespoon of tomato paste 
  * Some sliced ​​celery stalks (without leaves) 
  * 50 g of lentils 
  * 1 to 2 tbsp. tablespoon of olive oil 
  * ½ teaspoon of rancid butter (smen) 
  * 2 liters of water 
  * 1 handful of vermicelli 

for the binder: 
  * 25 g flour 
  * a little water 



**Realization steps**

  1. In a casserole, put olive oil, onion, meat, parsley with celery. 
  2. sauté until the onion becomes translucent, and the meat begins to reject its water. 
  3. Add a little water, spices and salt, and let it come back a bit. 
  4. Add the lentils, dried chickpeas soaked the day before in water (if you use canned chickpeas, they are precooked, it will be necessary to put them at the end of cooking), the tomato cut into pieces (without the skin ), tomato paste, butter. 
  5. Wet it all with a good amount of water (2 liters), cover and cook until the chickpeas become tender (if you used chickpeas deceived) or the meat becomes tender and cooked. 

Preparation of the binder: 
  1. Dilute the flour in water to have a liquid preparation without lumps (pass this preparation in a Chinese if necessary). 
  2. Once the chickpeas and lentils are cooked, leave the casserole on the heat and add the vermicelli, add the chickpeas in a box, let the vermicelli cook for a few minutes. 
  3. Add the binder (flour preparation) gradually continue stirring to prevent the flour sticking to the bottom. It will be necessary to add the binder by step until having a velvety soup (the more you add more the harira becomes thick). 
  4. Cook for another 2 to 3 minutes over medium heat, stirring constantly. 
  5. At the end of the cooking, add the chilled coriander. 



![Moroccan Harira](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/harira-soupe-marocaine1.jpg)

{{< youtube sDecSHHKkaw >}} 
