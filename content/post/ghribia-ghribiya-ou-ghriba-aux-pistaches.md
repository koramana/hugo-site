---
title: Ghribia, ghribiya or ghriba with pistachios
date: '2012-04-13'
categories:
- gateaux algeriens au miel
- Algerian cakes- oriental- modern- fete aid

---
& Nbsp; & Nbsp; Hi everyone, I forgot that I did not post the recipe for these delicious ghribia (halawiyat) very fondant, which look a lot like monticaos, and shortbread cookies & nbsp; for the ingredients: a glass and a half of smen, ghee (a glass of 250 ml) a glass of icing sugar + or - 3 glasses and a half flour (depending on the absorption of the flour) a glass of pistachio finely ground green dye a few drops of pistachio extract Beat the smen and icing sugar using an electric mixer for at least 20 minutes until you reach it. 

##  Overview of tests 

####  please vote 

**User Rating:** 4.5  (  2  ratings)  0 

Hello everyone, 

I forgot that I did not post the recipe for these delicious [ ghribia ](<https://www.amourdecuisine.fr/categorie-12344749.html>) (halawiyat) which are very similar to **monticaos** , and **shortbread cookies**

for the ingredients  : 

  * a glass and a half of smen, ghee (a glass of 250 ml) 
  * a glass of icing sugar 
  * \+ or - 3 ½ cups of flour (depending on the absorption of the flour) 
  * a glass of finely ground pistachio 
  * green dye 
  * a few drops of pistachio extract 


  1. Beat the smen and icing sugar with an electric mixer for at least 20 minutes until the mixture is sparkling (this is the secret of success). 
  2. Add a little green dye and pistachio extract. 
  3. add the pistachios reduced to powder 
  4. Stir the flour in a small amount and mix gently to collect the dough. 
  5. Add some flour if you see that the dough is sticking to your hands. 
  6. Preheat the oven to 160 degrees. 
  7. take a little of the dough, and put it in a mold Maamoul well flour 
  8. put in a lightly floured tray, continue until the dough has run out 
  9. Bake your ghribia for about 25 to 30 minutes until the cakes are lightly browned. 



merci pour vos commentaires 
