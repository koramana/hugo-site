---
title: guacamole
date: '2016-08-22'
categories:
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/guacamole-006.CR2_.jpg
---
![guacamole-006.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/guacamole-006.CR2_.jpg)

##  guacamole 

Hello everybody, 

the guacamole name, pronounced "gaucamolé", comes from the Spanish name of the avocado "aguacate". The guacamole in fact is just a mexican-style avocado puree, which says Mexican recipe, said spicy recipe, well-stocked, huuuuuuuuum ... 

There are different recipes and different ways to prepare guacamole, you can also prepare the simplest version that consists of reducing the avocado mashed and add salt, lemon juice and a little chilli crushed. 

Personally, I really like the lawyer who is always present in our salad. This food is rich in lipids and unsaturated fatty acids that are beneficial for cardiovascular health. And when a lawyer is too ripe to be presented in salad, he finishes his guacamole, and for people who have never tried this recipe, I do not tell you the delight.   


**guacamole**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/guacamole-009.CR2_.jpg)

Recipe type:  salad  portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 3 ripe avocados 
  * 1 tomato 
  * 1 onion 
  * fresh coriander 
  * the juice of half a lemon 
  * tabasco (if not a little red pepper) 
  * salt 



**Realization steps**

  1. cut the avocados in half and remove the stones. 
  2. remove the pulpit with a spoon and place in a large bowl. 
  3. crush the avocados with a fork. 
  4. cut the tomato into cubes, without removing the juice, and add it on the avocados. 
  5. add the tabasco, according to your taste, otherwise if you do not have, add the chill well chopped. 
  6. chop coriander and onion, and add to the mixture. 
  7. season with lemon juice and salt. 
  8. enjoy with doritos, crisps, or just with a good piece of bread ... a delight 



![guacamole-017.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/guacamole-017.CR2_.jpg)
