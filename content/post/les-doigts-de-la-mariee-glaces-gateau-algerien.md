---
title: Frozen Bride's Fingers / Algerian Cake
date: '2013-05-20'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/Sbaa-laaroussa.jpg
---
##  Frozen Bride's Fingers / Algerian Cake 

Hello everybody, 

The fingers of the bride known in Arabic as sbaa laaroussa, or Sbaa el aroussa, or sbi3ates laaroussa, are delicious Algerian cakes, in the shape of ice-cream sticks that cover a crispy shortbread dough, with a delicious stuffing with almonds perfumes with cinnamon, or lemon zest. 

a recipe that I made at the request of my daughter, who loves cakes with icing, and moreover these cakes are very easy, compared to [ arayeches ](<https://www.amourdecuisine.fr/article-arayeche-larayeche-aux-pistaches-gateau-algerien-87986224.html>) that she likes a lot, so it suits me well 

**Frozen Bride's Fingers / Algerian Cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/Sbaa-laaroussa.jpg)

Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** the ingredients for the pasta: 

  * 3 measures of flour 
  * 1 measure of Ghee (smen, a little overflowing) 
  * a tablespoon of icing sugar 
  * a bag of vanilla sugar 
  * 1 part water (half water and half orange blossom water) 

for the almond stuffing: 
  * 3 measure almonds 
  * 1 measure icing sugar 
  * vanilla sugar 
  * the zest of a lemon 
  * 2 tbsp. soup of melted butter 
  * eggs 
  * 1the icing recipe: 
  * 2 egg whites 
  * 1 C. soup of lemon juice 
  * 1 tablespoons orange blossom water 
  * Icing sugar 
  * food coloring 
  * 1 teaspoon of oil 
  * sugar paste to decorate 



**Realization steps**

  1. start preparing the dough, mixing the flour well with the smen. 
  2. add sugar, and vanilla. 
  3. pick up the dough with the orange blossom water, until you obtain a handy paste (do not knead too much, let stand 30 minutes). 
  4. shape small balls. cover with a film paper and book. 
  5. Prepare the stuffing by mixing all the ingredients, 
  6. Form small sausages. Book.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/les-doigts-de-la-mariee-gateau-pour-l-aid_thumb1.jpg)
  7. Take the dough and roll down finely. 
  8. cut rectangles 15 cm wide, put a stuffing pudding at the edge and roll in pudding, close well the extrimites, 
  9. place as you go on a plate, and continue until the dough is used up. 
  10. bake in preheated oven, 180 ° between 20 and 25 minutes, while monitoring. 
  11. In a bowl, mix the egg whites, lemon and orange blossom water. 
  12. Gradually add the icing sugar until you get a thick icing. 
  13. Make a test on a cake. If the frosting flows, add icing sugar or if it is too thick, pour a few drops of orange blossom water. 
  14. glaze the cakes and let it dry a little before putting up your flowers in sugar paste, and continue the decor with a syringe pastry. 
  15. let it dry in the open air 


