---
title: Homemade caramel flan or whims
date: '2015-04-26'
categories:
- dessert, crumbles et barres
- panna cotta, flan, et yaourt
tags:
- Ramadan
- Ramadan 2015
- verrines
- Holidays
- Dessert
- Custard
- creams

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/flan-maison-au-caramel-ou-caprices-1.CR2_.jpg
---
[ ![homemade caramel flan or caprices 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/flan-maison-au-caramel-ou-caprices-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-flan-maison-au-caramel-ou-caprices.html/flan-maison-au-caramel-ou-caprices-1-cr2>)

##  Homemade caramel flan or whims 

Hello everybody, 

Here is a dessert that deserves to be tried and enjoyed during a ramadanque evening, or just between family. This homemade caramel flan or the whims of Algeria, you can even use carambars is just a delight. 

Here in England I used toffy of trade or 100 gr of these sweets contain 65 gr of sugar. So some of the caramel sweets that you will use, try to see the sugar level in it, or do like me: melt the candies in small amounts in hot milk in the microwave, until you have the sweet taste you prefer . This homemade flan, which looks a lot like cream pastry, taste of caramel is really a delight, besides at home we loved it. Try it and tell me your opinion. 

**Homemade caramel flan or whims**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/flan-maison-au-caramel.CR2_.jpg)

portions:  4  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 500 ml of milk 
  * 100 gr of toffy or whims 
  * 2 egg yolks 
  * 2 tablespoons of cornflower soup 
  * caramel fragrance, if not vanilla 
  * Decoration: 
  * fresh cream whipped cream 
  * [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html> "salted butter caramel")



**Realization steps**

  1. take the equivalent of 2 tablespoons of milk 
  2. in a microwave-safe bowl, place remaining milk and half candies 
  3. preheat by 1 minute stroke, 
  4. remove from microwave, stir to melt candies and taste to make sure sugar 
  5. keep adding candies until you are satisfied with the taste of sugar   
you do not have to put 100 grams of candy, as it is possible that you use more. 
  6. transfer the mixture to a heavy-bottomed saucepan 
  7. in a bowl, mix remaining milk, cornflour and egg yolks and vanilla, until well blended. 
  8. stir in this caramel milk mixture, place the pan on low heat 
  9. stir with a spatula until the mixture thickens. 
  10. pour immediately into small ramekins, let cool a little and chill until serving time. 
  11. Decorate with whipped cream and a drizzle of salted butter caramel, or according to your taste. 



[ ![homemade caramel flan or whims](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/flan-maison-au-caramel-ou-caprices.jpg) ](<https://www.amourdecuisine.fr/article-flan-maison-au-caramel-ou-caprices.html/flan-maison-au-caramel-ou-caprices>)
