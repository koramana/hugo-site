---
title: 'dolma batata: potatoes with chopped meat'
date: '2017-06-06'
categories:
- Algerian cuisine
- recipe for red meat (halal)
- ramadan recipe
tags:
- Algeria
- Ramadan
- dishes
- Easy cooking
- Healthy cuisine
- Vegetables
- tajine

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/dolma-batata-002.jpg
---
[ ![dolma batata 002](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/dolma-batata-002.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/dolma-batata-002.jpg>)

##  Dolma batata: potatoes with chopped meat 

Hello everybody, 

A classic of Algerian cuisine: batata dolma, or potatoes stuffed with minced meat, a dish omnipresent on all Algerian tables, for one occasion or another ... 

With us too, this recipe for dolma batata is almost present every 15 years on our table, we love it and here it is. We like it [ dolma with vegetables ](<https://www.amourdecuisine.fr/article-dolma-legumes-farcis-a-la-viande-hachee-au-four.html> "Dolma, vegetables stuffed with meat baked in the oven") , the dolma batata red sauce, oh yes I must tell you that is always the war between me and my husband, said red sauce, and I say white sauce ... The war broke out again this time, and know How did you win the challenge? I told her: you want dolma red sauce, you go to the kitchen, hihihihihi ... in your opinion that she was his answer? No, I like dolma batata white sauce, it was just to tease you, hihihiih tease me, that's the best.   


**dolma batata: potatoes with chopped meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/dolma-batata-pommes-de-terre-a-la-viande-hachee.jpg)

portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 6 to 8 medium and regular potatoes if possible 

for the sauce 
  * 1 onion 
  * a few sprigs of parsley 
  * 3 tablespoons of oil 
  * ¼ tablespoon cinnamon 
  * Salt and black pepper 
  * 1 nice handful of fresh or frozen peas 

for the minced meat stuffing: 
  * 300 gr of minced meat 
  * A clove of garlic 
  * 1 egg yolk 
  * Salt and pepper 
  * 3 tablespoons of rice cooked in salt water 
  * Parsley 



**Realization steps** Prepare the stuffing: 

  1. prepare the stuffing, mixing the minced meat, with a little parsley, salt of black pepper. 
  2. Add crushed garlic, egg yolk, and pre-cooked rice. Leave aside. 

prepare the sauce: 
  1. fry the onion in the oil with the spices, watch not to burn. 
  2. cover with the equivalent of one liter of water and bring to a boil. 
  3. dig wells in the potatoes, then fill them with the minced meat stuffing. 
  4. With the rest of the meat chopped, prepare meatballs. 
  5. place the stuffed potatoes and meatballs in the sauce, 
  6. add the peas, then cook until the potatoes become tender, and the sauce thickens. 
  7. remove from heat and sprinkle with chopped parsley. 
  8. you can also add a touch of black pepper to the mill. 



[ ![dolma batata, potatoes stuffed with minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/dolma-batata-pommes-de-terre-farcies-a-la-viande-hachee.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/dolma-batata-pommes-de-terre-farcies-a-la-viande-hachee.jpg>)

j’espere que la recette va bien vous plaire, je vous dis bonne degustation, et a la prochaine recette… 
