---
title: Chicken nuggets
date: '2014-10-22'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- Roast chicken
- Amuse bouche

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-de-poulet-009_thumb1.jpg
---
##  Chicken nuggets 

Hello everybody, 

in Ramadan, and before having the blog, I often prepared these chicken nuggets, and my husband liked a lot, I liked a lot, because I prepared all at night, I put in the fridge, and the next day just before to prepare the table, I passed them to the frying, just to take the color. 

it's delicious and also very presentable, a nice tapas recipe, or starter I prepared yesterday for a light dinner, and that the children have a great love. 

Note  : People who can not validate their registrations, will be deleted after 2 days, so they can try again to register, but it is really important to validate and confirm the email you receive on your box, from feedburner .   


**Chicken nuggets**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-de-poulet-009_thumb1.jpg)

Recipe type:  appetizer, aperitifs  portions:  4  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients** for 20 croquettes of 26 gr each 

  * half of a chicken breast 
  * 2 hard boiled eggs 
  * 2 potatoes 
  * 2 portions of the cheese the laughing cow 
  * salt, pepper, garlic powder 

for breading: 
  * flour 
  * egg + 1 teaspoon of oil 
  * breadcrumbs 



**Realization steps**

  1. cut the chicken breast into pieces, salt, pepper and cook in a little oil   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/2012-06-21-croquettes-de-poulet_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/2012-06-21-croquettes-de-poulet_thumb1.jpg>)
  2. peel the potatoes, cut into cubes, salt a little, and steam 
  3. pass the chicken breasts to the blender. 
  4. crush the mashed potatoes. 
  5. add the crushed hard eggs,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-de-poulet-recette-du-ramadan_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-de-poulet-recette-du-ramadan_thumb.jpg>)
  6. stir in the cheese portions, and mix well by hand 
  7. prepare meatballs of almost 26 gr each 
  8. roll these dumplings into the flour, then into the egg and oil mixture. 
  9. then continue the breading of these dumplings in breadcrumbs 
  10. place in the refrigerator for at least 1 hour. 
  11. put the meatballs in a hot oil bath for 5 minutes, place on paper towels (although it will not be too fat). 



[ piquant butter with peanut butter ](<https://www.amourdecuisine.fr/article-creme-piquante-au-beurre-de-cacahuete-89052515.html>) [ tzatziki ](<https://www.amourdecuisine.fr/article-tzatziki-105328008.html>)
