---
title: matlouh with semolina and flour
date: '2012-07-22'
categories:
- recettes a la viande de poulet ( halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/matloue-a-la-farine-027_thumb111.jpg
---
![matlouh with semolina and flour](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/matloue-a-la-farine-027_thumb111.jpg)

##  matlouh with semolina and flour 

Hello everybody, 

Here is a very delicious matlouh with semolina and flour, or as it is called matloue or matlou3 or matlou that I realized to accompany a good bowl of chorba, watch the video at the bottom! 

Usually I do my matoulou with semolina, but this time I did not have enough, so I added a little flour for an even better result, a matlouh well ventilated, super light and melting in the mouth. 

in a bowl put the ingredients dry, start adding water slowly while kneading. 

knead the dough and if you see that it lacks water do not hesitate to add slowly.   


**matlouh with semolina and flour**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/matloue-a-la-farine-025_thumb11.jpg)

**Ingredients**

  * the ingredients: for 2 large cakes 
  * 2 glasses of semolina (1 glass of 220 ml) 
  * 1 glass of flour 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 tablespoon instant yeast 
  * a coffee of baking powder 
  * 3 tablespoons of oil 
  * 250 ml of warm water (more or less depending on the absorption of semolina and flour) 



**Realization steps**

  1. hand kneading can take at least 15 mins. when you find that the dough is well kneaded, start introducing the oil, and this by wetting the work surface with the oil and knead on it, repeat this action until the oil is used up 
  2. let your dough rise until it doubles in volume, in a place sheltered from drafts. 
  3. after emergence, take the paste on an oiled surface, degazez a little, and form 2 balls. 
  4. place the balls of galettes on a clean cloth and on which you sprinkle some semolina, to avoid that the paste sticks to the tormenting one. 
  5. cover the balls with another cloth, and also another clean cloth so that the dough retains its heat. 
  6. when the balls double in size, lower them by hand, to have a circle of 22 to 25 cm in diameter over a thickness of 1.5 cm and let rise again. 
  7. do not hesitate to sprinkle the semolina surface if it sticks to your hand. 
  8. let it rise again, at least 45 minutes, because everything depends on the temperature of the room. 
  9. cook on a preheated tadjine (earthen pan) or on a pancake pan. on both sides. also try to cook the contour of the patties 
  10. and good tasting. 



I presented this cake to accompany: 

![algerian chorba 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-5-e1464526583859.jpg)

a [ algerian chorba ](<https://www.amourdecuisine.fr/article-chorba-algeroise.html>)

{{< youtube vFZwa3c0cGY >}} 
