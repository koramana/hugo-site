---
title: the sun cake, Chamsiya Algerian cakes
date: '2016-06-16'
categories:
- gateaux algeriens au miel
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/mchekla-4.bmp.jpg
---
##  the sun cake, Chamsiya Algerian cakes 

Hello everybody, I am in full works, I do not tell you .... and even more overwhelmed with the preparation of the ftour for us, and the dinner for the children .... So I apologize if I put you this recipe that is on my blog for a few years already, 2011 to be more precise! time flies. It is a delicious Algerian cake, which appears difficult to achieve, but in fact it is a breeze. While it takes a little time to achieve, but the result is so beautiful and above all too good, that can not resist. 

So the recipe is taken from Ms. Dahak Malika's book, "Dahak Cakes," Feather Edition 

the recipe in Arabic: 

ingredients: 

dough: 

\- 6 measures of flour 

\- 1 measure of margarine 

\- a pinch of salt 

\- 1 teaspoon of vanilla 

\- Water + orange blossom water 

![mchekla 4.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/mchekla-4.bmp.jpg)

the joke: 

\- 500 gr of almond 

\- 250 gr of sugar 

\- lemon zest 

\- 2.5 tablespoon margarine 

\- eggs 

the decoration: 

\- Almonds 

\- Honey 

\- Egg whites 

Preparation: 

starting with the dough, in a bowl, mix the flour, margarine, salt and vanilla then pick up with the mixture water + water of orange blossom. until a smooth dough is obtained 

prepare the stuffing: in a bowl, mix the almonds, sugar, lemon zest and margarine, collect with the eggs until you obtain a firm stuffing. 

Lower the machine dough to Number 2, 4 and 5 (I always do it with my baking roll) cut circles using an 8cm (6cm for me) cookie cutter, then put on each circle a layer of stuffing, 

and cover with another circle of dough. 

![mchekla 1](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/mchekla-1.jpg)

To form small cornets with the same stuffing and the same dough (it is not to explain in the book, but I made small circles of 2,5 to 3 cm of diameter, I form balls of almonds of about 1 cm and I form cones, not too much almond because cooking almond stuffing swells and can overflow) 

![mchekla](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/mchekla.jpg)

stick the cornets with egg white and place them on the cakes already formed and covered with egg white. Put an almond in the middle, let stand for 4 hours (all night for me, because I prepared this cake around midnight), then bake at 180 degrees C 

After cooking, immerse them in the honey. 

kisses   


**the sun cake, Chamsiya Algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/soleil-019-300x284.jpg)

**Ingredients** dough: 

  * 6 measures of flour 
  * 1 measure of margarine 
  * a pinch of salt 
  * 1 teaspoon of vanilla 
  * Water + water of orange blossom 

the joke: 
  * 500 gr of almond 
  * 250 grams of sugar 
  * lemon zest 
  * 2.5 tablespoons margarine 
  * eggs 

the decoration: 
  * Almonds 
  * Honey 
  * Egg whites 



**Realization steps**

  1. starting with the dough, in a bowl, mix the flour, margarine, salt and vanilla then pick up with the mixture water + water of orange blossom. until a smooth dough is obtained 

prepare the stuffing: 
  1. in a bowl, mix the almonds, sugar, lemon zest and margarine, collect with the eggs until you obtain a firm stuffing. 
  2. Lower the machine dough to Number 2, 4 and 5 (I always do it with my baking roll) cut circles using an 8cm (6cm for me) cookie cutter, then put on each circle a layer of stuffing, and cover with another circle of dough. 
  3. To form small cornets with the same stuffing and the same dough (it is not to explain in the book, but I made small circles of 2,5 to 3 cm of diameter, I form balls of almonds of about 1 cm and I form cones, not too much almond because cooking almond stuffing swells and can overflow) 
  4. stick the cornets with egg white and place them on the cakes already formed and covered with egg white. Put an almond in the middle, let stand for 4 hours (all night for me, because I prepared this cake around midnight), then bake at 180 degrees C 
  5. After cooking, immerse them in the honey. 


