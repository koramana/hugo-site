---
title: Mini-stuffed triangles
date: '2011-03-18'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/newsletter1.jpg
---
& Nbsp; hello everyone, delicious stuffed triangles that I prepared yesterday to accompany my veloute pea, we feast well, and here is a simple recipe for a quick meal: ingredients: 1 roll of pie puff an egg hard a small handful of green olive three slice of red cheese or cheddar (or according to your choice) 3 portions of cheese "the laughing cow" or other a small box of tuna seed of nigella to garnish an egg yolk for gilding . spread out the dough, cut it in square, and stuff each square with the & hellip; 

##  Overview of tests 

####  vote here 

**User Rating:** 4.39  (  3  ratings)  0 

Hello everybody, 

delicious stuffed triangles that I prepared yesterday to accompany my  [ peas velvety  ](<https://www.amourdecuisine.fr/article-41284308.html>) , we feast well, and here is a simple recipe for a quick meal: 

ingredients: 

  * 1 roll of puff pastry 
  * a hard egg 
  * a small handful of green olives 
  * three slices of red cheese or cheddar cheese (or your choice) 
  * 3 servings of cheese "the laughing cow" or other 
  * a small box of tuna 
  * grain of nigella to garnish 
  * an egg yolk for gilding. 



close the triangular squares, garnish them with egg yolk and nigella seeds, 

bake, and enjoy 

thank you for your visits and comments 

thank you for subscribing to my newsletter if you have not done so yet: 

![newsletter](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/newsletter1.jpg)

and if you have tried one of my recipes, do not forget to send me the pictures on this email: 

vosessais @ gmail. 

merci 
