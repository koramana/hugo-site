---
title: Brioche pudding with banana and nutella
date: '2014-04-17'
categories:
- Buns and pastries
- cakes and cakes
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pudding-briochee-a-la-banane-et-nutella-022.CR2_.jpg
---
[ ![brioche pudding with banana and nutella 022.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pudding-briochee-a-la-banane-et-nutella-022.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pudding-briochee-a-la-banane-et-nutella-022.CR2_.jpg>)

Hello everybody, 

Before you post the recipe for this pudding brioche with banana and nutella, I tell you my story with English cakes. 

When I first arrived here, the first thing I did was buy a chocolate cake from the big Thorntons bakery. When I passed the shop, and I saw all these delights, I could not resist ... 

So when I arrived home, I prepared a good latte, and I cut a good piece of cake, the first spoon ... .. I had the head that turned, so it was sweet. 

the next day, I cut a small piece, I put a lot of coffee with the milk, I did not put sugar, and again, I could not eat the cake ... 

Then I noticed that sugar was too high in chocolates, yogurts, and many other things, so I did not buy any more cakes here, or eat chocolate, unless it's dark chocolate. . Even the English recipes each time I tried them, I absolutely had to reduce the sugar. 

A recipe super easy to achieve, just follow the steps and you will really love this little cake. 

[ ![banana pudding with banana and nutella 001.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pudding-briochee-a-la-banane-et-nutella-001.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pudding-briochee-a-la-banane-et-nutella-001.CR2_.jpg>)

This method is totally different from the other methods of making pudding that requires putting pieces of bread or buns in milk and eggs overnight in the fridge:   


**Brioche pudding with banana and nutella**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pudding-briochee-a-la-banane-et-nutella-004.CR2_.jpg)

Cooked:  English  Recipe type:  Dessert  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 3 to 4 tablespoons of nutella 
  * 3 to 4 brioche rolls. 
  * 2 big eggs 
  * 2 tablespoons fresh cream. 
  * 200 ml of whole milk 
  * 1 teaspoon of vanilla extract 
  * 3 ripe bananas 
  * a little butter at room temperature. 



**Realization steps**

  1. Cut the bread rolls brioche in length on three. 
  2. spread a little butter on one of the surfaces of the brioche slices. 
  3. butter a baking tin   
(I made half of the ingredients so I used a small mold of almost 12 cm by 18 cm) 
  4. heap the pieces of brioche delicately in the mold to leave no space. 
  5. cut the bananas into slices, and align them all around the mussels. 
  6. whip the milk, cream, eggs, vanilla and nutella, and gently pour this mixture over the buns. 
  7. let the buns absorb this liquid well, but do not bother with your fingers. 
  8. cover the mold with a nice layer of cling film, and turn the mold to invert it on a larger dish and let it absorb again. 
  9. after 15 to 20 minutes invert the mold once more, continue to turn the pan upside down, so that the pieces of brioche absorb the liquids without piling them up. 
  10. after almost half past one, preheat the oven to 150 degrees, and cook the pudding for almost 15 minutes.   
the cake should not burn, but rather leave it until the mixture containing the eggs cooks. 
  11. the pudding will swell well, remove from the oven, let cool a little and enjoy warm.   
you can prepare puddings in mini molds, you can serve them in these molds, or remove the pudding from the mold and present it. 



[ ![brioche pudding with banana and nutella 012.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pudding-briochee-a-la-banane-et-nutella-012.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pudding-briochee-a-la-banane-et-nutella-012.CR2_.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
