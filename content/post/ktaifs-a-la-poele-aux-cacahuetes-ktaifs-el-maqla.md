---
title: Kthafs in the pan with peanuts ktaïfs el maqla
date: '2017-05-28'
categories:
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries
- Algerian pastries
tags:
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/kta%C3%AFfs-%C3%A0-la-po%C3%AAle.jpg
---
![ktaïfs in the pan](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/kta%C3%AFfs-%C3%A0-la-po%C3%AAle.jpg)

##  Kthafs in the pan with peanuts ktaïfs el maqla 

Hello everybody, 

Today I am going to share with you the recipe for Ktaïfs in the peanut pan ktaïfs el maqla, a recipe super easy to make, Thanks to my friend Lynda Akdader for sharing with us this recipe. 

These ktaïfs el maqla, or ktaïfs in the pan, can be prepared in the blink of an eye, and in addition if you replace almonds of the original recipe of ktaïfs by almonds, you have a recipe economic and super delicious and that will delight your little tribe and even guests at the improvisite. 

This recipe is ideal during Ramadan, after standing all day preparing the table iftar, your dessert of the evening of Ramadan, can be prepared in no time, then to you the video: 

**Kthafs in the pan with peanuts ktaïfs el maqla**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/ktaifs-el-maqla-aux-cacahuetes.jpg)

**Ingredients** For the syrup: 

  * 2 glasses of water 
  * 1 glass of sugar 
  * 1 cinnamon stick 
  * 1 slice of lemon 

For Ktaifs: 
  * 300 gr of paste with ktaifs 
  * 100 gr of melted smen 
  * 100 ml of table oil 

For the stuffing: 
  * 300 gr of peanuts 
  * 100 gr of sugar 
  * 1 C. cinnamon 
  * Orange tree Flower water. 



**Realization steps**

  1. Start by making the syrup, place all the ingredients in a saucepan and boil for 15 minutes. 
  2. disentangle the ktaïfs gently and sprinkle them gently with the mixture of oil and melted smen 
  3. prepare the stuffing, mixing the ingredients to have a nice dough that stands well. 
  4. take a thick bottom tepal frying pan, put 2 good spoons of smen and let melt 
  5. put half of the ktaïfs on the pan, 
  6. then arrange the stuffing, cover with the other half of ktaïf 
  7. sprinkle mixture smen and oil, and add if necessary smen on the walls. 
  8. cook and watch the cooking by lightly lifting the ktayef to monitor if the bottom surface is well cooked. 
  9. once the ktaif become golden, turn it with a flat plate. 
  10. put back without the pan with a good tablespoon of smen. 
  11. when cooked, sprinkle the ktafl pancake with the syrup twice to keep the crisp of the cake. 
  12. put in a serving dish and let cool before eating. 



![ktaifs el maqla](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/ktaifs-el-maqla.jpg)
