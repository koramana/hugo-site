---
title: potato salad with pesto
date: '2018-05-03'
categories:
- salads, salty verrines
tags:
- Easy cooking
- Fast Food
- Algeria
- Ramadan 2018
- Ramadan
- inputs
- accompaniment

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/05/salade-de-pommes-de-terre-au-pesto-1.jpg
---
![Potato salad with pesto 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/salade-de-pommes-de-terre-au-pesto-1.jpg)

##  potato salad with pesto 

Hello everybody, 

For my part I chose the basil that I love, but I can not tell you how much I like this herb, so I directly thought about pesto, because that's just how my little one Family eats basil, so I took advantage of that sunny day when we had scheduled a BBQ to accompany the grilled meat with this super refreshing pesto potato salad. 

{{< youtube SrRO2gXuEtg >}} 

The godmothers of the previous rounds: 

**potato salad with pesto**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/salade-de-pommes-de-terre-au-pesto-2.jpg)

**Ingredients**

  * 10 small potatoes 
  * 4 to 5 green onions 

pesto sauce: 
  * 1 glass of spinach 
  * ½ glass of basil leaves 
  * ¼ glass of parmesan cheese 
  * ¼ glass of pine nuts 
  * 2 cloves garlic 
  * 3 c. mayonnaise 
  * 3 c. extra virgin olive oil 
  * freshly squeezed lemon juice 
  * salt and black pepper 



**Realization steps**

  1. wash the potatoes well, and boil in salted water 
  2. prepare the pesto sauce by mixing all the ingredients in the blinder bowl. 
  3. adjust the sauce to your taste 
  4. cut the potatoes into a slice. 
  5. cut the green onion into a slice 
  6. sprinkle with the pesto sauce and mix well. 
  7. present the salad garnished with a little parmesan cheese. 



![potato salad with pesto-2](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/salade-de-pommes-de-terre-au-pesto-2-1.jpg)

Et voila la liste des participantes, faites un tour vous allez bien aimer toutes ces belles recettes: 
