---
title: appetizers with smoked salmon
date: '2016-10-18'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/aperos-dinatoires-au-saumon-fume.CR2_2.jpg
---
![aperos dinatoires-au-salmon fume.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/aperos-dinatoires-au-saumon-fume.CR2_2.jpg)

##  appetizers with smoked salmon 

Hello everybody, 

here are dinner appetizers, delicious appetizers, very easy to make, that I presented during the visit of my friends, they really liked ... .. 

smoked salmon appetizers, very simple, just prepare, at least 3 hours in advance, if not a night in advance for an even better result. 

![roll-to-salmon-smoke-018.CR2-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/roule-au-saumon-fume-018.CR2-copie-12.jpg)

**appetizers with smoked salmon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/amuses-bouches-au-saumon-fume2.jpg)

Recipe type:  appetizer, aperitif  portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * a few slices of bread 
  * cheese in portions 
  * smoked salmon 
  * Red bell pepper 
  * poppy seeds, otherwise nigella. 



**Realization steps**

  1. remove the hard edges of slices of bread, and flatten a loaf with a baking roll 
  2. spread the cheese in thin layer over 
  3. place the slices of smoked salmon to cover the bread 
  4. spread one more layer of cheese. 
  5. place at one end of the slices of red pepper. 
  6. roll the bread, and then roll the food film, so that it keeps the form well. 
  7. leave in the fridge, 3 hours minimum, otherwise a night of preferable. 
  8. at the moment of presenting, remove the food film, roll the pudding in a little butter if not a little cheese, then roll them again on poppy seeds. 
  9. cut slices of almost 1cm and a half, and present 



{{< youtube YLfdcw9DQG0 >}} 
