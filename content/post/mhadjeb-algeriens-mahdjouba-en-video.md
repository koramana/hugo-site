---
title: Mhadjeb Algerians - mahdjouba in video
date: '2016-10-04'
categories:
- crepes, waffles, fritters
- Algerian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mhadjeb.jpg
---
**[ ![mhadjebs](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mhadjeb.jpg) ](<https://www.amourdecuisine.fr/article-mhadjeb-algeriens-mahdjouba-en-video.html/mhadjeb>) **

##  Mhadjeb Algerians - mahdjouba in video 

Hi everyone, the flu takes me back, I can not even open my eyes, so I'm resting this recipe, which I really wanted to do today, but I'm beyond my strength. 

it's a [ Algerian traditional recipe ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , based on semolina that makes it very very mug dough, and then that stuffing, I do not take long to give you the recipe [ Algerian cuisine with photo ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-avec-photo-103322241.html>)   


**Mhadjeb Algerians - mahdjouba**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mhadjeb-069_thumb1.jpg)

Recipe type:  Algerian cuisine  portions:  4  Prep time:  60 mins  cooking:  10 mins  total:  1 hour 10 mins 

**Ingredients** for the pasta: 

  * a little semolina 
  * salt 
  * some water 

for the stuffing: 
  * 2 onions 
  * 3 tomatoes 
  * 2 cloves garlic 
  * salt, pepper, coriander powder 
  * tuna 



**Realization steps** prepare the pasta: 

  1. wet the semolina and the salt, with water, gently until it is easy to handle, 
  2. oil very well without tearing the dough, so we try to petrire without separating a piece of another to give elasticity to the dough, and we add the water in small quantities, we petrit, and we add the water it is heated until the dough becomes very soft, and when it is stretched it does not tear, like a shewing gum. 
  3. let it rest, and prepare the stuffing. 

Prepare the stuffing: 
  1. cut the sliced ​​onions 
  2. return to the oil you drain from the can of tuna. 
  3. add the crushed garlic, then the tomatoes cut into small cubes, and add the condiments according to your taste, let everything cook well. 
  4. let cool 
  5. when the stuffing is cold, add in the tuna. 
  6. return to the dough that has been well rested, and start to form balls the size of a tangerine, 
  7. let it rest again, so that you can spread the dough easily. 
  8. coat the work surface well, with the oil, spread the balls of pasta one by one and fill with stuffing.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mhadjeb-067_thumb.jpg) ](<https://www.amourdecuisine.fr/article-recette-mhadjeb-algerien-msemens-farcis-crepes-farcis.html/mhadjeb-067_thumb-3>)
  9. fold the dough like an envelope on the stuffing, and cook on a very thick and well oiled Tajine or a crepe. 



![mhadjeb 065](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mhadjeb-065_thumb1.jpg)

{{< youtube PN5JSE9KkPc >}} 

bon appétit 
