---
title: Algerian cake / chocolate pyramids, without cooking
date: '2013-07-26'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/gateau-sans-cuisson-les-pyramides_thumb1.jpg
---
##  Algerian cake / chocolate pyramids, without cooking 

Hello everybody, 

I really like them [ **cakes without cooking** ](<https://www.amourdecuisine.fr/categorie-11745248.html>) , and I do not tell you how much I did at the beginning of my marriage, because I did not have a good oven, I did all kinds, and all possible combinations, and I dig up a recipe that I made a lot that time, but I try to get out of the balls ... 

why not pyramids, not easy to achieve when you do not have a mold, but I like to do the challenge .... 

**Preparation time : 20 min  ** **Cooking time : 10 minutes  **

Ingredients: 

for a groin of pieces (medium size) 

  * 200 gr of grilled flour 
  * 100 gr of grilled milk powder 
  * 50 gr of icing sugar 
  * 100 gr of melted butter 
  * honey 
  * White chocolate 



[ ![cake without cooking the pyramids](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/gateau-sans-cuisson-les-pyramides_thumb1.jpg) ](<https://www.amourdecuisine.fr/categorie-10678931.html>)

method of preparation: 

  1. grill the flour in a pan, watching and turning often with a wooden spoon, and pass through the sieve 
  2. also grind the milk powder in a pan, and watching carefully, and often turning with a wooden spoon, it's really delicate, then pass it to the sieve 
  3. put the two mixtures in a tank 
  4. add the powdered sugar, the melted butter, and pick up with the honey 
  5. At this point, already you can drip your cake, if you think the cake and dry but sweet, you can add butter. 
  6. form balls of 23 to 25 gr each, if you can shape pyramids, it is good if not remaining in the form of the balls. 
  7. put the cake in the fridge for 30 min. 
  8. Melt the chocolate in a bain-marie, place the cakes on a rack 
  9. place a piece of baking paper underneath, and pour melted chocolate over each cake to cover it. 



for the final decoration: I used a little icing sugar, red dye, and a little water, mix well to have a nice mix, put in a bag, or a paper cone, and decorate the cakes. 

for more cakes: [ Algerian cake ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

**Algerian cake / chocolate pyramids, without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/les-pyramides-gateau-sans-cuisson_thumb.jpg)

**Ingredients**

  * 200 gr of grilled flour 
  * 100 gr of grilled milk powder 
  * 50 gr of icing sugar 
  * 100 gr of melted butter 
  * honey 
  * White chocolate 



**Realization steps**

  1. grill the flour in a pan, watching and turning often with a wooden spoon, and pass through the sieve 
  2. also grind the milk powder in a pan, and watching carefully, and often turning with a wooden spoon, it's really delicate, then pass it to the sieve 
  3. put the two mixtures in a tank 
  4. add the powdered sugar, the melted butter, and pick up with the honey 
  5. At this point, already you can drip your cake, if you think the cake and dry but sweet, you can add butter. 
  6. form balls of 23 to 25 gr each, if you can shape pyramids, it is good if not remaining in the form of the balls. 
  7. put the cake in the fridge for 30 min. 
  8. Melt the chocolate in a bain-marie, place the cakes on a rack 
  9. place a piece of baking paper underneath, and pour melted chocolate over each cake to cover it. 


