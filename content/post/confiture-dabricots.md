---
title: apricot jam
date: '2014-06-18'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-d-abricots-3_thumb.jpg
---
[ ![apricot jam](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-d-abricots-3_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-d-abricots-3_2.jpg>)

##  apricot jam 

Hello everybody, 

Here is an apricot jam delicious and easy to achieve that Lunetoiles has shared with me for a while now, but I had not published because it was not seasonal ... 

But after thinking, I told myself why not put it, especially since Lunetoiles had realized with apricots that she had even frozen, apricots apricot apricot, and as there were many, she put it in the freezer, and here is the sublime apricot jam as delicious as the season. It's a good idea I find, and the result is a superb apricot jam.   


**apricot jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-d-abricots-5_thumb.jpg)

Recipe type:  jam  portions:  10  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients**

  * For 1 kg of apricot 
  * 500 gr of sugar (or 250 gr of fructose) 
  * 1 lemon 
  * 100 ml of water 



**Realization steps**

  1. Wash and cut the apricots in half, removing the kernel. 
  2. Place the apricots in a pot with sugar, zest, water and lemon juice. 
  3. Bring to a boil and cook for 20/25 minutes. Foam, and poke. Flip the pots. Cool until completely cool. 



[ ![apricot jam](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-d-abricots_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-d-abricots_2.jpg>)
