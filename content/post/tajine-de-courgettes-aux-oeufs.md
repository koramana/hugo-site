---
title: zucchini tajine with eggs
date: '2014-01-29'
categories:
- Algerian cuisine
- Dishes and salty recipes
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/trebia8-2_thumb.jpg
---
[ ![trebia8-2](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/trebia8-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/trebia8-2_2.jpg>)

Hello everybody, 

a delicious [ Algerian dish ](<https://www.amourdecuisine.fr/cuisine-algerienne>) [ vegetarian ](<https://www.amourdecuisine.fr/plats-vegetariens>) that I ate often when I was little, and that I take advantage to share with you, because it is a good dish of summer, very simple. 

the name trebia, which comes from the origin of the word merbia, comes from the fact that this dish always gives small quantities, even if we make a lot of zucchini, well the result is a small dish, do not say that I do not have not warned, hihihihihihi    


**zucchini tajine with eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/trebia8_thumb.jpg)

Recipe type:  Algerian cuisine  portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 kilos of zucchini 
  * 1 fresh tomato 
  * ½ tablespoon of tomato paste 
  * 1 onion 
  * 2 cloves garlic 
  * 2 tablespoons of oil 
  * salt, black pepper, paprika, spices according to your taste 
  * ½ bunches of coriander 
  * 4 eggs 



**Realization steps**

  1. clean and cut the sliced ​​zucchini 
  2. place it in the pot, with the oil 
  3. add tomato cut into pieces, and tomato concentrate 
  4. add the onion and the well chopped garlic 
  5. let it all brown, add the salt (be careful the zucchini tends to be a little salty and condiment 
  6. add the chopped coriander. 
  7. let it simmer on a low heat 
  8. add a small amount of water just to help in cooking the zucchini, not to submerge the dish. 
  9. leave until the zucchini is tender, 
  10. towards the end of cooking, break the eggs over, let it cook a little, and put out the fire 



[ ![trebia8-1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/trebia8-1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/trebia8-1_2.jpg>)

serve hot or tepid according to your taste, and have a good appetite 

and saha ramdankoum 

follow me on: 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
