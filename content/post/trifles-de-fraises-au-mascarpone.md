---
title: strawberries with mascarpone strawberries
date: '2017-12-22'
categories:
- dessert, crumbles and bars
- sweet verrines
tags:
- biscuits
- desserts
- verrines
- Chantilly
- creams
- Fruits
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/trifles-de-fraises-au-mascarpone-1.jpg
---
[ ![Strawberry Trifle with Mascarpone 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/trifles-de-fraises-au-mascarpone-1.jpg) ](<https://www.amourdecuisine.fr/article-trifles-de-fraises-au-mascarpone.html/trifles-de-fraises-au-mascarpone-1>)

##  strawberries with mascarpone strawberries 

Hello everybody, 

Who does not like a sweet dessert just after a good meal? For my part it's super important, besides my children always claim something sweet at the end of the meal, I must say that cakes is not really their stuff. For them, it's either a fruit salad like this [ citrus salad ](<https://www.amourdecuisine.fr/article-salade-d-agrumes-salade-d-oranges-pamplemousse-mandarine.html> "citrus salad, mandarin orange grapefruit salad") , [ fruit salad in jelly ](<https://www.amourdecuisine.fr/article-salade-de-fruits-en-gelee-2.html> "fruit salad in jelly") or trifles. 

What I like about trifles is that you do not have to limit yourself to this or that ingredient. You can see that I already have to post a recipe for [ strawberry trifles ](<https://www.amourdecuisine.fr/article-trifles-aux-fraises.html> "Trifles with strawberries") with Bavarian mousse. There are also these delicious [ pineapple trifle and gingerbread ](<https://www.amourdecuisine.fr/article-trifles-a-lananas-et-pain-depice.html> "Pineapple and Gingerbread Trifle") . When it comes to these mascarpone strawberry trifles, it's a Lunetoiles achievement that used the rest of [ four quarter pure Breton butter ](<https://www.amourdecuisine.fr/article-recette-quatre-quart-pur-beurre-barre-bretonne.html> "Four quarter pure butter recipe: Breton bar") , recipe she found on my blog. 

[ ![Strawberry Trifle with Mascarpone 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/trifles-de-fraises-au-mascarpone-2.jpg) ](<https://www.amourdecuisine.fr/article-trifles-de-fraises-au-mascarpone.html/trifles-de-fraises-au-mascarpone-2>)   


**strawberry clovers with mascarpones**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/trifles-de-fraises-au-mascarpone.jpg)

portions:  6  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 3 slices of [ quarter quarter pure butter ](<https://www.amourdecuisine.fr/article-recette-quatre-quart-pur-beurre-barre-bretonne.html> "Four quarter pure butter recipe: Breton bar")
  * 50 to 100 gr of mascarpone (according to taste) 
  * 1 whole cream of liquid cream has 30% of MG very cold (the best is to put it in the refrigerator the day before) 
  * 50 gr of brown sugar brown (for whipped cream) 
  * 2 sachets of vanilla sugar (for whipped cream) 
  * strawberries as needed 
  * 1 tablespoon + ½ brown sugar brown sugar (to macerate the strawberries) 



**Realization steps**

  1. Start by washing, planting and slicing strawberries 
  2. transfer them to a bowl, add the sugar, 
  3. stir and let stand 30 minutes. This will allow the strawberries to make juice. 
  4. Put the liquid cream very cold in whipped cream, when it begins to be frothy incorporate the sugar brown sugar, vanilla sugar and mascarpone, and continue to beat. 
  5. Cut the quarters of the four quarter pure butter into cubes and store them in the bottom of 6 glasses.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/Downloads.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41540>)
  6. Divide the sugar strawberries with their juices in the glasses and finish with a layer of mascarpone whipped cream. 
  7. Decorate according to your taste. 
  8. Place in the fridge for several hours before serving. 



[ ![Mascarpone Strawberry Trifle 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/trifles-de-fraises-au-mascarpone-3.jpg) ](<https://www.amourdecuisine.fr/article-trifles-de-fraises-au-mascarpone.html/trifles-de-fraises-au-mascarpone-3>)
