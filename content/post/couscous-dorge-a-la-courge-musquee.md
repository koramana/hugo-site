---
title: Barley couscous with butternut squash
date: '2017-01-20'
categories:
- cuisine algerienne
- cuisine marocaine
- Cuisine par pays
tags:
- Algeria
- Full Dish
- Vegetables
- dishes
- Ramadan
- Algerian couscous
- Chicken

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/01/couscous-dorge-a-la-courge-musqu%C3%A9e-1.jpg
---
![barley couscous with butternut squash 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/couscous-dorge-a-la-courge-musqu%C3%A9e-1.jpg)

##  Barley couscous with butternut squash 

Hello everybody, 

Barley couscous or black couscous is made from one of the richest cereal grains in nutrients. This grain, grown for millennia, is high in fiber and naturally low in fat and contains important minerals, vitamins, antioxidants and is ideal for lowering blood cholesterol. 

In any case, this barley couscous with butternut squash, is not only going to be nutritious for you, it is also too good, especially with all the seasonal vegetables that garnish it.   


**Barley couscous with butternut squash**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/couscous-dorge-a-la-courge-musqu%C3%A9e-2.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * chicken pieces according to the number of people 
  * 1 kg of black couscous or barley. 
  * 5 medium carrots 
  * 4 turnips 
  * 4 potatoes 
  * 1 butternut squash 
  * a nice handful of chickpeas dipped the day before in water with a little baking powder 
  * 2 medium onions. 
  * 3 tablespoons oil. 
  * salt, black pepper, ginger, hrour (optional) 
  * 2 sticks of cinnamon. 
  * saffron   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/16144724_1393138650699239_2081494953_n-225x300.jpg)



**Realization steps**

  1. start by turning the chopped onion and the chicken pieces in the oil. 
  2. add all the remaining ingredients: carrots, turnip, potato, butternut squash, chickpea, and spices. 
  3. add almost 1 liter of water and cook until well cooked. 

for couscous: 
  1. wash the couscous in a bowl and let it drain a little 
  2. place it in a large bowl as soon as it absorbs the water well, rub it in your hand with a little oil, add the salt. 
  3. steam in the top of the couscoussier and cook for 15 minutes after the steam has escaped. 
  4. put back in the terrine and water with water (do not over-water it, because it does not absorb too much water like the usual couscous). 
  5. let it absorb again, before steaming it again. 
  6. at the end of the cooking, add pieces of butter, and rub the couscous in your hands, so that it absorbs the butter. 
  7. you can see the cooking video below. 
  8. present the couscous decorated with your vegetables and water according to your taste. 



{{< youtube n1A2FGkyjq8 >}} 
