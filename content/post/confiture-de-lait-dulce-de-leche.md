---
title: Milk jam, dulce de leche
date: '2017-01-22'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/dulce-de-leche-confiture-de-lait-3_thumb1.jpg
---
![dulce de leche, milk jam 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/dulce-de-leche-confiture-de-lait-3_thumb1.jpg)

##  Milk jam, dulce de leche 

Hello everybody, 

Milk jam, or dulce de leche, is a recipe that has the basis is by baking the milk with a good amount of sugar, and that perfumes with vanilla, 

This mixture cooks for a long time on a low heat, while watching. Now the color and texture of this jam, depends on its cooking time, the more you cook, the more the jam will be more creamy and a darker color. 

Today, Lunetoiles shares with us the old recipe, and soon, I will give you the easiest and most uncomfortable recipe.   


**Milk jam, dulce de leche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/dulce-de-leche-confiture-de-lait_thumb1.jpg)

**Ingredients**

  * 1 liter of whole milk 
  * 350 g sugar (half brown sugar and half sugar powder) 
  * 1 vanilla pod 



**Realization steps**

  1. Pour milk, sugars and scraped vanilla into a pot with high edges. 
  2. Bring to a boil and boil over very low heat for 1:30 to 2 hours, mixing every 10 minutes. 
  3. The volume of the liquid decreases by a good half. 
  4. When the preparation turns a little darker and begins to thicken, remove the vanilla pod and mix well without stopping. 
  5. The milk jam is ready when it is well coated on the back of the spoon. 
  6. Pour into glass jars 



bonne realisation 
