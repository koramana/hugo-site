---
title: tartlets with strawberries and mascarpone
date: '2015-04-22'
categories:
- pies and tarts
tags:
- Soft pie
- Light desserts
- desserts
- Easy recipe
- Fast Recipe
- Strawberry tart
- Genoese

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tartelettes-genoise-aux-fraises_thumb1.jpg
---
##  tartlets with strawberries and mascarpone 

Hello everybody, 

Here is a recipe for **tartlets** made of ** Strawberry Sponge Cake  ** and ** mascarpone  ** , that can be done very quickly, because the cream of mascarpone is a breeze even not need to get out your drummer for that. 

Generally, I prepare pie funds in advance, whether they are a [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-inratable.html> "unbreakable genesis") , [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast") , or [ pastry ](<https://www.amourdecuisine.fr/article-recette-pate-brisee-maison-facile.html> "Easy homemade broken pasta recipe") I cook and I put in the freezer, and if I have a friend who comes unexpectedly, I quickly prepare a cream and I garnish according to the fruits of season or that I have on hand. 

**tartlets with strawberries and mascarpone**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tartelettes-genoise-aux-fraises_thumb1.jpg)

portions:  6  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for 6 tartlets: 

  * 2 eggs 
  * 60 gr of sugar 
  * 60 gr of flour 
  * ½cc of baking powder 
  * A few drops of vanilla essence 
  * ½ teaspoon of oil 
  * Decoration: 
  * 100 gr of mascarpone 
  * 100 gr of whipping cream 
  * sugar according to taste 
  * juice of 1/2 lemon 
  * Orange zest. 
  * strawberries, a few mint leaves and icing sugar to garnish. 



**Realization steps**

  1. Separate whites from yellows, 
  2. mix the yeast with the flour. 
  3. In the bowl of the robot (for me it was with a normal whisk in a bowl), place the egg whites and put them in snow. 
  4. When they start to froth, add the sugar and whip until a shiny and firm meringue is obtained. 
  5. Add the egg yolks, the oil and the flour immediately. Do not let the robot run too long. 
  6. fill the pies with special sponge cake pies 
  7. cook in a preheated oven at 180 degrees for 10 minutes or less, watch the cooking well 
  8. meanwhile, in another bowl, mix with a manual whisk, the mascarpone, the cream, add the sugar according to your taste 
  9. add the orange zest and the lemon juice. 
  10. cool 
  11. after cooking and cooling the pie bases, and using a pastry bag, fill the sponge cake with the mascarpone cream, and decorate with strawberries according to your taste. 



and here are other strawberry recipes: 

merci pour vos visites et bonne journée. 
