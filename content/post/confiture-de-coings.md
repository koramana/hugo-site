---
title: From Quince jam
date: '2018-01-10'
categories:
- jams and spreads
tags:
- Cakes
- To taste
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/confiture-de-coings.jpg
---
![From Quince jam](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/confiture-de-coings.jpg)

##  From Quince jam 

Hello everybody 

Another quince-based recipe, and this time it's quince jam, which I personally love, and I still like it in my little cupboard. So I do not need to tell you how many jars I have prepared. 

At my family in Constantine, the quince jam is a must they must always present to the guests, I even believe that the quince jam is a family tradition that marks the East of Algeria. 

The recipe in Arabic: 

then we go to the recipe: 

**quince jam, معجون سفرجل**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/confiture-de-coings-1.jpg)

Recipe type:  jam  portions:  8  Prep time:  30 mins  cooking:  25 mins  total:  55 mins 

**Ingredients**

  * 1 kg of quince 
  * 800 gr of sugar 
  * 200 ml of water 
  * 1 lemon 
  * 1 small cinnamon stick 
  * 2 star anise. 



**Realization steps**

  1. Peel the quinces, cut them in quarter and remove the seeds as well as all the granular part which surrounds them. 
  2. Put the seeds in a small purse (a cloth that is closed with a string that serves to diffuse the quince pectin (natural jelly contained in the seeds) 
  3. Squeeze the lemon on the quince, add the sugar, and water and leave everything in one place for 1 hour (see even one night, if you are passionate) 
  4. put the purse that contains the seeds in a saucepan with cinnamon, and star anise and boil with water. 
  5. pour this water into a heavy-bottomed saucepan, or a bowl of jam 
  6. add the mixture quince and lemon sugar on 
  7. simmer over low heat for about 2 hours, until the syrup is thick and the fruit is red. 
  8. remove the seed stock exchange 
  9. Put in sterilized pots and close immediately, turn the pots. 



![quince jam 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/confiture-de-coings-2.jpg)
