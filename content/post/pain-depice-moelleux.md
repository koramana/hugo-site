---
title: fluffy gingerbread
date: '2014-12-25'
categories:
- cakes and cakes
tags:
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-d-epice-mercotte-016_thumb.jpg
---
![gingerbread mercotte 016](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-d-epice-mercotte-016_thumb.jpg)

##  fluffy gingerbread 

The recipe for mercotte is a real treat, I did not have all the ingredients, but I tried not to go too far from its recipe. 

So I give you this delicious recipe, and I'm going to put my little changes right in front (it's not that I wanted to change the ingredients, but that's what I had at home) 

**fluffy gingerbread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-d-epice-mercotte-018_thumb.jpg)

Recipe type:  cake  portions:  12  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients**

  * 135g of semi-skimmed milk, 
  * 1clove, 
  * 90g of butter, 
  * 80g of vergeoise, (brown sugar for me, Demerara) 
  * 110g of liquid honey and if possible of fir, (eucaliptus honey for me) 
  * 2 small eggs of 35g, 
  * 20g candied orange zest or candied orange, 
  * the zest of a lemon. 
  * 105g of flour T45, (normal flour, or plain flour here in England) 
  * 60g of buckwheat flour, (Brown flour) 
  * 1g of fleur de sel (normal salt for me) 
  * 3g of baking soda or 1cc, 
  * 1g of ginger powder, 
  * 2g cinnamon powder, 
  * 2g of gingerbread spices, 
  * 1g of licorice powder, (I did not have one, and I do not know) 
  * 18g molasses powder, 
  * 20g of ginger confit, (I think that I have abused on this side, 50 grs for me, so I like the taste) 
  * 30g of flaked almonds. (I had not put) 



**Realization steps**

  1. Bring the milk to a boil, 
  2. add the clove, 
  3. bake out of the heat 5min then filter. 
  4. In a saucepan over low heat butter butter and honey and, when the mixture is lukewarm, add the beaten eggs and smooth well. 
  5. Mix the powders - greens, bicarbonate, spices, muscovado- with candied orange zest and fleur de sel. 
  6. Add them to the previous mixture and add the ginger in thin slices, the almonds and the lukewarm milk. 
  7. Preheat the oven to 170 °, 
  8. butter and flour the mold. Add the dough to the ⅔. Let stand 10min, 
  9. then bake 35min [to check as always according to your oven]. 



[ ![mercotte gingerbread 017](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-d-epice-mercotte-017_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-d-epice-mercotte-017.jpg>)

look at me it's delicious pieces of candied ginger 

a real success, thank you very much mercotte, your recipe a real treat for me. 

[ ![2010-05-04 gingerbread mercotte](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/2010-05-04-pain-d-epice-mercotte_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/2010-05-04-pain-d-epice-mercotte_2.jpg>)
