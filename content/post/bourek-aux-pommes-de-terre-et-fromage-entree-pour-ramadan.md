---
title: bourek with potatoes and cheese / entree for ramadan
date: '2016-09-26'
categories:
- ramadan recipe
tags:
- Easy cooking
- Brick leaf
- inputs
- Aperitif
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourek-aux-pommes-de-terre-et-fromage.CR2_1.jpg
---
![bourek-to-apples-to-earth-and-fromage.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourek-aux-pommes-de-terre-et-fromage.CR2_1.jpg)

##  bourek with potatoes and cheese 

Hello everybody, 

Here is my favorite bourek recipe, the potato and cheese boureks, that my mother prepared during Ramadan, and that we loved most, and I have a great memory with this recipe, I remember when I was almost 12 years old, at the end of the CEM (Deran, Taref) there was a little boy who sold these boureks with spicy potatoes, and we were queuing just to have a 5 dinar coin at the time .... And what was it good ... 

And I confess one thing, although I make this recipe myself now, but I can not find the taste of the boy's boureks ... huuuuuum souvenir 

**bourek with potatoes and cheese / entree for ramadan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourek-pour-ramadan-a-la-pomme-de-terre-bourek-batata.CR2_1.jpg)

**Ingredients**

  * Ingredients: for 10 boureks 
  * leaves of bricks. 
  * 3 to 4 medium potatoes 
  * 1 onion 
  * ½ bunch of chopped parsley, or according to taste 
  * 1 handful of pitted green olives 
  * 1 cup of harissa coffee (or according to your taste) 
  * 200 grams of grated cheese 
  * salt, black pepper, coriander powder 
  * egg white to stick 
  * oil for frying 



**Realization steps**

  1. Wash, peel and cut the potatoes into large cubes, and cook in a little salted water. 
  2. cut the onion into small ones, and fry in a little oil, until it becomes translucent. 
  3. crush the potato drain from its cooking water, and place it in a large salad bowl. 
  4. over add cooked onion, chopped parsley, harissa, salt, black pepper and coriander powder. Mix well. 
  5. then add the grated cheese and the olives cut in slices. 
  6. fill in the bricks with this stuffing, fold them, and stick the last end with a little egg white. 
  7. cook in a hot oil bath. 
  8. drain the cooked boureks on paper sopalin, and present lukewarm ... 



![brick-to-apple-Earth - bourek-bel-batata.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/brick-aux-pomme-de-terre-bourek-bel-batata.CR2_1.jpg)

méthode de préparation: 
