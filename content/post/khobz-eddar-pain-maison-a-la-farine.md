---
title: Khobz eddar - homemade bread - flour
date: '2008-02-01'
categories:
- couscous
- Algerian cuisine
- Dishes and salty recipes
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216161231.jpg
---
& Nbsp; Mouni is a very good cook, even if she is absent for the moment from the blog, but we still like to visit her to see her news, and even if her recipes have been published for a long time, it is always a pleasure to read them and do them. today, I had a crush on his homemade bread Khobz eddar, and it was a great success: so without delay I give you the recipe: ingredients: 500 gr 1cas flour and 1/2 baker's yeast 1 teaspoon of salt 1 teaspoon of oil, about 100 ml (I had & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

today, I had a crush on his homemade Khobz eddar bread, and it was a great success: 

![khobz5](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216161231.jpg)

so without delay I give you the recipe: 

ingredients: 

  * 500 gr of flour 
  * 1cas and 1/2 baker's yeast 
  * 1 cup of salt 
  * 1 teaspoon of oil is about 100 ml (I had misread the measure it was 10 cl, I read 10 ml, all the same the bread was great, next time I'm careful) 
  * 2 eggs less one yellow for gilding 
  * 1cc of sugar 
  * 1 large glass of milk + warm water (total 310 ml) 
  * Nigella, a little milk 



I put all the ingredients on the map, I program: program pate, and forward for 2h20 of petrissage, levee, degazage ... .. and here is my dad: 

![khobz1](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216161101.jpg)

at the end of this time, we degas the dough and knead a little bit, and we shape according to the desired shape, personally I formed a crown that I cut with scissors (at intervals of 4-5cm): 

![khobz3](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216162181.jpg)

and I made a beautiful little braid: 

![khobz2](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216162291.jpg)

I put my loaves in trays oils (excuse the absence of accents, but with an English keyboard I lose myself), and I doré with the mixture egg yolk + milk and parsemais the top with nigelle 

and I leave it up 30 to 40 minutes 

![khobz7](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216162591.jpg) ![khobz4](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216162621.jpg)

and then I bake in an oven preheated to 180 degrees for 20 minutes 

![khobz5](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216162831.jpg)

beautiful crumb 

beautiful bread 

![S7302273](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216434611.jpg)

a delight 

![khobz6](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/216162901.jpg)

Thank you Mouni 

et bonne appetit 
