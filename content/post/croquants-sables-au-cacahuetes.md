---
title: Crunchy shortbread with peanuts
date: '2011-01-04'
categories:
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/croquant-sables-au-cacahuette.jpg
---
& Nbsp; Ingredients: 3 eggs 155 gr of sugar 180 ml of oil 1 sachet of baking powder 1 sachet of vanilla sugar Salt 1 glass of ground peanuts (roughly) Lemon rind of the flour according to its absorbency 1 yellow of egg for gilding mixed with soluble coffee Using a beater, mix the sugar and the eggs. Add the oil, salt and vanilla. Continue beating and add the ground peanuts (I brown them in the oven before you smell them well), add the lemon peel and the baking powder. & Hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.75  (  1  ratings)  0 

![crisp-sands-to-cacahuette.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/croquant-sables-au-cacahuette.jpg)

_ Ingredients:  _

  * _ 3 eggs  _
  * _ 155 gr of sugar  _
  * _ 180 ml of oil  _
  * _ 1 sachet of baking powder  _
  * _ 1 sachet of vanilla sugar  _
  * _ Salt  _
  * _ 1 glass of ground peanuts (roughly)  _
  * _ Lemon zest  _
  * _ flour according to its absorbency  _
  * _ 1 egg yolk for gilding mixed with soluble coffee  _



![crisp-sands-to-peanuts-3-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/croquant-sables-au-cacahuettes-3-11.jpg)

  1. With a beater, mix the sugar and the eggs. 
  2. Add the oil, salt and vanilla. 
  3. Continue beating and add the ground peanuts (I brown them in the oven before we feel good), 
  4. add lemon zest and baking powder. 
  5. At this stage, I mix the dough by hand by gradually incorporating the flour until obtaining a soft and malleable dough. 
  6. On a work surface sprinkled with flour, lower the dough to a thickness of 1 cm. 
  7. Cut the patties with a serrated punch (for my part I also made flowers and half moons). 
  8. Arrange the pancakes in a dish covered with parchment paper. 
  9. Brush the surface of the patties with the beaten egg yolk. 
  10. With the back of a fork draw lines in one direction then in another so that they cross each other. 
  11. Preheat the oven at 175 ° C for about 30 mm until the surface is golden brown.  ![crisp-sands-to-peanuts-21.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/croquant-sables-au-cacahuettes-211.jpg)   



