---
title: pancake cake with lemon curd
date: '2017-01-01'
categories:
- crepes, beignets, donuts, gauffres sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-de-crepes-au-lemon-curd-007.CR2_thumb1.jpg
---
##  Pancake cake with lemon curd 

Hello everybody, 

I really like cakes of pancakes, which, to read the recipe, it is believed that it is long, but indeed it is the easiest cakes in the world, and in addition, very mellow, very melting in the mouth, huuuuuuum 

and here it's a cake I made with coconut milk pancakes, and homemade lemon curd. 

I do not tell you this double flavor of coconut and lemon, frankly a delight it really tastes.   


**pancake cake with lemon curd**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-de-crepes-au-lemon-curd-007.CR2_thumb1.jpg)

**Ingredients**

  * 1 dozen [ pancakes with coconut milk ](<https://www.amourdecuisine.fr/article-crepes-au-lait-de-coco.html> "pancakes with coconut milk") (depending on the height you want to have) 
  * [ lemon curd ](<https://www.amourdecuisine.fr/article-lemon-curd-creme-au-citron-fait-maison.html> "recipe of homemade lemon curd / lemon cream") ( as required) 



toothpicks (to hold the cake in place, while the lemon cream takes in the fridge, then remove them) 

you can garnish your cake with seasonal fruit, lemon peel, or just like that. 

For people who think that lemon curd recipes are too sweet, I say no, when the lemon curd is homemade, it's up to you to judge the sugar level you put in it, it's not like that trade, or the taste of sugar is really too much ... 

and that is what is good in the recipes that are prepared at home, we can change the quantities, or even the ingredients, according to our desires. 

it is super easy to assemble this cake, after cooking the pancakes, and preparing the lemon curd, knowing that you do not have to make coconut pancakes, you can make some [ plain pancakes ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee-114932092.html>) . 

so I'll imagine this cake with the [ Nutella pancakes ](<https://www.amourdecuisine.fr/article-pate-a-crepe-au-nutella-crepe-au-chocolat-facile-et-rapide-115021705.html>) , huuuuuuum chocolate and lemon what a delight. 

so as I told you, use toothpicks for the cake to hold at first, and before serving remove them. 

also, when you proceed to the realization of the cake and its mounting, place the lemon curd in the heart of the crepe, and do it layer after layer. 

at the end, press gently to make the distribution of the lemon curd uniform, do not press too hard. 

car au moment de la découpe, le lemon curd et encore pressez par le couteau, sauf si comme moi, vous utilisez un couteau électrique, qui coupera le gâteau sans faire bouger la crème a l’intérieur. 
