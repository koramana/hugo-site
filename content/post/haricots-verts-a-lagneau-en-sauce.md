---
title: green beans with lamb in sauce
date: '2015-03-18'
categories:
- cuisine algerienne
- Moroccan cuisine
- Tunisian cuisine
- recette a la viande rouge ( halal)
- ramadan recipe
tags:
- Full Dish
- Morocco

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/haricots-verts-a-lagneau.jpg
---
[ ![green beans with lamb](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/haricots-verts-a-lagneau.jpg) ](<https://www.amourdecuisine.fr/article-haricots-verts-a-lagneau-en-sauce.html/haricots-verts-a-lagneau>)

##  green beans with lamb in sauce 

Hello everybody, 

I really like green beans, well, not when I was a child so as not to lie to you. But since my mother made us this recipe, none of us let her plate fill. This way of baking green beans is just a delight. 

Follow this recipe, and I'm sure your children will lick their chops ... What's great too, is that you do not have to make this recipe of green beans in sauce with lamb only, ca works great too with chicken: [ tagine of green beans with chicken and onion ](<https://www.amourdecuisine.fr/article-tajine-d-haricots-verts-au-poulet-et-oignon.html> "tagine of green beans with chicken and onion")   


**green beans with lamb in sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/haricots-verts-en-sauce-a-lagneau.jpg)

portions:  4  Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients**

  * pieces of lamb 
  * 1 kg of green beans (frozen for me) 
  * 3 onions 
  * 2 carrots 
  * 4 fresh tomatoes, or canned tomatoes, in pieces 
  * 2 cloves garlic 
  * salt, black pepper, olive oil 



**Realization steps**

  1. in a pressure cooker, fry the onion cut into thin slices in length, add the pieces of meat, salt and pepper, let return well until the meat releases all its water. 
  2. add the garlic and tomato cut into pieces, the carrots cut 
  3. add over the beans, well cleaned. 
  4. add the equivalent of 1 half green of water 
  5. close the pressure cooker, and cook until the meat becomes tender. 
  6. If you think that your dish contains a lot of sauce, let it cook uncovered for a few more minutes until the sauce is reduced. 



[ ![green beans with lamb](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/jwaz-zarigo-haricots-verts-a-lagneau.jpg) ](<https://www.amourdecuisine.fr/article-haricots-verts-a-lagneau-en-sauce.html/jwaz-zarigo-haricots-verts-a-lagneau>)
