---
title: Couscous with meat
date: '2012-04-11'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/25340993.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/25340993.jpg)

Hello everybody, 

a very old recipe from 2009 when louiza, who now has her blog to share her delicious meat couscous with me. 

you know what makes me laugh, I make couscous almost every 15 years, but I never take pictures, because I tell myself that I have these articles on my blog, well .... yet I have to renovate these photos. 

at the time I was on canalblog, and I did a lot of frames and I do not know what else, hihihihihi. 

then I give you this recipe of louiza Couscous vegetables and meat that really gives desire. 

so, the recipe, well yes I give it to you:   


**Couscous with meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/25340993.jpg)

portions:  6  Prep time:  40 mins  cooking:  45 mins  total:  1 hour 25 mins 

**Ingredients**

  * 1 kg of meat 
  * 4 carrots 
  * 3 zucchini 
  * green beans 
  * Chickpea 
  * turnips 
  * 3 onions 
  * 1 tablespoon of oil 
  * 2 tablespoons canned tomato 
  * paprika 
  * 1 sachet of saffron 
  * 1 teaspoon cinnamon 
  * black pepper 
  * salt for couscous, prepare it in this way. 



**Realization steps** for the sauce, 

  1. start by grating the onions, and put them in the blender, add your condiments with the meat and the oil, let it simmer a little, add the vegetables peeled, and cut according to your taste, add a little water and let it simmer again ( it is better to leave the zucchini to put in the last place because it cooks right away)   
![](https://www.amourdecuisine.fr/wp-includes/js/tinymce/plugins/wpgallery/img/t.gif)
  2. when you see that the meat is half cooked, add the tomato, and cover with water, 15 minutes before the end of cooking, you can then add your zucchini. 
  3. If your chickpea is in a box, you can also add it to the end of cooking just like zucchini. 
  4. but if it is chickpea deceived the day before, put it at the beginning of cooking. 



and I hope you will love this Couscous just like me 

huuuuuuuuuuuuuuuuuuuuum 
