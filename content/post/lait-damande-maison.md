---
title: Homemade milk of almond
date: '2015-01-11'
categories:
- ice cream and sorbet
- basic pastry recipes
tags:
- Dessert
- Pastry
- desserts
- Cakes
- Based
- Algerian cakes
- Confectionery

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/02/lait-d-amande-1_thumb1.jpg
---
[ ![homemade almond milk](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/lait-d-amande-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-lait-d-amande-maison.html>)

##  Homemade milk of almond 

Hello everybody, 

It happens to you to find a recipe that you like, and by reading the ingredients, you come across a name that you do not know or an ingredient that does not sell at the corner store! 

the almond milk, it sounds good, and already we imagine the taste, it must be exquisite, and when Lunetoiles passed me the recipe, I was looking for time to be in front of the pc, for the upload. 

so without delay here is the recipe   


**Homemade milk of almond**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/lait-d-amande-1_thumb1.jpg)

portions:  4  Prep time:  5 mins  total:  5 mins 

**Ingredients** for 1 bottle 

  * 1 cup whole almonds, not pruned 
  * 4 cups of mineral water 



**Realization steps**

  1. Soak the almonds: put the almonds in a bowl, and cover with water, let soak 8 hours. 
  2. Drain the almonds, and rinse several times. 
  3. Put the almonds in the bowl of the blinder with the 4 cups of water. 
  4. Mix for 2 minutes while taking breaks. 
  5. Then filter with a very fine cotton cloth over a carafe. In two or three times, gently letting the almond milk obtained, and making a knot, and pressing on the laundry to extract all the almond milk. 
  6. At the end, once you have squeezed the laundry and get all the juice, put the almond milk in a glass bottle.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/lait-d-amande-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/02/lait-d-amande-2.jpg>)
  7. And keep it cool. You can keep it 4 days cool. 
  8. Put the remaining almond pulp in a bowl, you should use it quickly, in a cake for example to accompany this almond milk.   
Treat yourself ! 



#  [ ![almond milk3](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/lait-d-amande3_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-lait-d-amande-maison.html>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

Remember, if you like almonds, and if you want to participate in the game: 

[ Recipes around an ingredients # 2 ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-2.html> "Recipes around an ingredient # 2") : The almond 

[ ![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient-300x163.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg>)

source: Pinterest 

laissez un commentaire sous l’article en question et dites: je participe 
