---
title: Reduce odors in the refrigerator
date: '2007-12-26'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- Coffee love of cooking
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201449531.jpg
---
**![bonj15](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/20144945.png) **

**To reduce odors in a refrigerator, simply wash it with warm vinegar water.**

Leave on for 15 to 20 minutes then rinse thoroughly with water. 

![th_cutefridge](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201449531.jpg)

**To reduce odors in a refrigerator, simply roll a piece of sugar into the cigarette ash.**

Then place it on a small plate and have it burn in the refrigerator. 

![th_fridge](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201449591.jpg)

**To reduce the odors of the refrigerator, just put down some corks.**

![th_lyt](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201449821.jpg)

![bonj1](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201449921.gif)
