---
title: Christmas cookies
date: '2012-12-23'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
- ramadan recipe

---
Hello everybody, 

I arrive at the last minute, because I wanted to make another biscuit, but I was too taken, grrrrrrrrrrrrr, I do not like when it happens, hihihiih 

and that's what is done ... .. 

Ingredients: 

  * _3 measures of flour (I used a 160 ml glass)_
  * _1 measure lukewarm melted butter._
  * _almost 1 measure of the mixture water + water of orange blossom_
  * lemon zest 



**_Icing_ **

  * _1 egg white_
  * _1 tablespoon orange blossom water_
  * _1 teaspoon lemon juice_
  * _1 teaspoon of oil_
  * _dye_
  * _icing sugar_



**_method of preparation:_ **

  1. _In a bowl, sift the flour and add the measure of melted and cooled butter._
  2. _Rub with your hands so that the flour absorbs the butter, and then sieve for the mixture to be well incorporated._
  3. _Gradually add the mixture of water and orange blossom water and collect the dough into a ball without kneading._
  4. _Cover and let stand about twenty minutes._
  5. _Roll out the dough using a rolling pin and use a cookie cutter of your choice to cut the shape of your choice (my daughter at this stage had fun)_ _._
  6. _with another little cookie cutter, make a hole in the cupcake_
  7. _cook for 10 to 15 minutes in a preheated oven_



_**icing:** _

  1. _place the egg white in a large bowl and mix gently with a fork_
  2. _add orange blossom water, lemon juice and oil._
  3. _stir in the icing sugar gently with a tablespoon while mixing until you get a thick cream._
  4. _Test with a cake, if the glaze is too thick, add a little orange blossom water and if it is too liquid add icing sugar._
  5. _glaze all your cakes and let them dry completely in the open air before placing them in an airtight container._



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
