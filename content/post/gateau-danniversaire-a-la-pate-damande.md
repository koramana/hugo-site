---
title: birthday cake with marzipan
date: '2013-11-01'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/jardin2_thumb.jpg
---
Hello everyone, 

here is a super simple and delicious birthday cake almond paste, I put you online at the request of many of my readers ... 

in truth, this cake was based on a muslin cake with hazelnut, covered with a thin layer of whipped cream, and decorate in the final with almond paste, nothing difficult, everything is easy to do, except for decoration you have to have a little passion, hihihihihi. I did it while my little monsters were sleeping, otherwise you would not have seen a flower garden, but I would say it would snow, and the snow covered the flowers (I know my daughter would eat everything, like she did it elsewhere) 

**birthday cake with marzipan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/jardin2_thumb.jpg)

Recipe type:  birthday cake  portions:  12  Prep time:  30 mins  cooking:  25 mins  total:  55 mins 

**Ingredients** so at the base, I made the cake chiffon which here my ingredients :( for a square mold of 23x23 cm) 

  * 8 eggs 
  * 250 grams of sugar 
  * 3 c. tablespoon of table oil 
  * 6 c.a milk soup 
  * 50 grs of ground hazelnuts 
  * hazelnut extract (or almond as for me) 
  * 210 gr of flour 
  * 2 tablespoons of baking powder 

for decoration: 
  * 700 ml thick cream 
  * sugar according to taste 
  * some pieces of peaches 
  * almond paste colors of your choice for decoration. 



**Realization steps** start by preparing the cake. 

  1. preheated the oven to 180 degrees C, 10 min before cooking 
  2. separate the egg whites from the yolks, and turn the egg white into snow with 2 tablespoons of sugar, leave aside 
  3. whisk the egg yolks with the sugar, until the mixture whitens, 
  4. add the oil while whisking, then the milk, incorporate the hazelnuts in powder (I find it at Lidl), then slowly mix the flour and the yeast with this mixture. 
  5. add the egg white by mixing with a spatula, making movements up and down, not to break the egg white, and to have an airy mixture. 
  6. pour the mixture of a buttered and floured mold, or covered with parchment paper, and cook for 45 minutes, or depending on the capacity of your oven, you can check the cooking with a knife. 
  7. remove from the oven after cooking and let cool. 
  8. after cooling, cut the cake in half. and soak it with syrup, or like me, with the juice that is in the fishing box.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/jadin-fleuri_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/jadin-fleuri_thumb.jpg>)

Decoration 
  1. add the thick cream in whipped cream with a little sugar according to your taste, then garnish the middle of the cake (you can put the fruits you want in) 
  2. cover with the other half of the cake, and cover the whole cake and put in the fridge. 
  3. during this time prepare it, and color your almond paste,   
I used for that dye powder that I buy in Algeria, I used white for white flowers. red not much, for the color of the basket, yellow rose and green, then for the remaining yellow almond paste, I add a little red color to have the small flowers in orange brick.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/birth-023_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/birth-023_thumb.jpg>)
  4. first, I measure the size of each side of my cake, and I prepare a white rectangle, and a pink rectangle then, the white rectangle, 
  5. I cut strips of 1.5 cm wide, in and of 23 cm long (my cake was 23 cm of side), and the pink strips, I cut strips also of 1.5 cm wide and 8 cm long (height of my cake) and I braid the thong like a basket, and each time you prepare a side, it is fixed on the cake, and we put the cake in the fridge , until filling the cake. 
  6. for decoration from above, it is necessary to provide you with a punch in the shape of leaf and flower. 
  7. fill all the surface of the cake. 



in any case it was a cake very very delicious, we devoured incredibly in two days (between us 4), even my husband find it very good, not too sweet, especially with the bitter taste of almond (in the almond paste) frankly a real happiness. 

once again thank you for all your wishes. 

merci pour vos votes, bisous 
