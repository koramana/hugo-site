---
title: Maamoule has the Turkish halwa dry cakes
date: '2016-06-25'
categories:
- gateaux algeriens- orientales- modernes- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/maamoul-a-la-halwa-turc-010.CR2_1.jpg
---
![Maamoule has the Turkish halwa dry cakes](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/maamoul-a-la-halwa-turc-010.CR2_1.jpg)

##  Maamoule has the Turkish halwa dry cakes 

Hello everybody, 

A delicious dry cake very melting and superb, a beautiful and easy to handle dough, a maamoul Turkish halwa that I enjoy, whether during the preparation of the dough, or while forming the cakes. 

the mold is sublime, it is a mooncake mold that I buy in Algeria, watch the video, and give me your opinion 

and for even more cake for the help, look at this index: 

[ Algerian cake / Algerian cakes 2016 ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)

The recipe in Arabic: 

For about thirty pieces   


**Maamoule has Turkish halwa / dry cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/maamoul-a-la-halwa-turc-010.CR2_1.jpg)

Recipe type:  Algerian cake  portions:  30  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients**

  * 250 g margarine at room temperature 
  * 4 egg yolks 
  * 150 g icing sugar 
  * 150 g cornflour 
  * 2 tbsp. vanilla coffee 
  * 1 sachet of baking powder 
  * Flour (to pick up the dough) 

Prank call : 
  * Turkish Halwa 



**Realization steps**

  1. Whisk the margarine with the icing sugar with an electric whisk or by hand. 
  2. Once the margarine and sugar are well whipped, introduce the egg yolks and mix well. 
  3. Then add the vanilla and whip again. 
  4. Introduce the cornflour very slowly, and when you have incorporated all the cornstarch in the mixture, incorporate the flour and the yeast until you have a smooth and very manageable paste. 
  5. Shape dough balls of 30 gr each. It all depends on the size of your mold, if your mold is smaller, make smaller balls. 
  6. Take a ball of dough and flatten it a little bit, put some Turkish halwa in the center, then close to lock the Turkish halwa and form a ball. 
  7. Flour the work plan. Place the ball of dough on the floured surface, place your mold on top and put pressure on your mooncake mold, to form the cake. 
  8. Or you can flour your ball of dough, and put it in the mold by pressing a little, then put the mold facing your work surface, and exert pressure to form the cake. 
  9. And continue until the dough is exhausted. 
  10. Once all the cake pieces are ready, put them in a preheated oven at 180 ° C. for 10 to 12 minutes. 


