---
title: Homemade daisy bread Khobz eddar with 3 grains
date: '2017-04-23'
categories:
- bakery
- Algerian cuisine
- Mina el forn
- bread, traditional bread, cake
- Dishes and salty recipes
tags:
- Ramadan
- Ramadan 2017
- Semolina
- Boulange
- Algeria
- Pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/khobz-eddar-aux-3-grains_thumb1.jpg
---
##  Homemade daisy bread Khobz eddar with 3 grains 

Hello everybody, 

here is a very delicious recipe of  **homemade bread** alias  **Khobz eddar** in Arabic, a very soft bread, very light, very airy, and above all, too good. 

You can see another version of this bread on video, the dough bread with super fluffy flour and super easy to make: 

**Homemade daisy bread Khobz eddar with 3 grains**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/khobz-eddar-aux-3-grains_thumb1.jpg)

**Ingredients** for a single cake: 

  * 250 grs of semolina 
  * 250 grams of flour 
  * 1 tablespoon of sugar 
  * 1 tablespoon baker's yeast (instant) 
  * 1 cup of baking powder 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 3 tablespoons of oil 
  * 1 tablespoon of black seed 
  * 1 tablespoon of sesame seeds 
  * 1 tablespoon of anise seeds 
  * 1 whole egg 
  * lukewarm water as needed 

for garnish: 
  * 1 egg yolk 
  * 1 cup of milk 
  * 3 drops of vinegar (to avoid the smell of eggs) 
  * black seed, sesame and anise. 



**Realization steps**

  1. if you use the bread maker, add all the mixes in the bowl avoiding the direct contact of salt and baker's yeast 
  2. if you do it by hand: 
  3. mix the flour, the semolina, the sugar, the salt, and the oil, to sand well. 
  4. add over the two yeasts, and the milk powder, as well as the mixture of grains at your disposal and mix 
  5. make a well, and pour the scrambled egg 
  6. start jetting the water at medium temperature and pick up the dough 
  7. knead the dough by tearing the ball, do not roll, otherwise you will have a very elastic bread 
  8. when the dough is soft and a little sticky in your hands, place it in an oiled place, and cover 
  9. let it rise well, and degas the dough by kneading it in the same previous way, 
  10. let another raise another, then degass again 
  11. place the dough in an oiled baking sheet. 
  12. lower to 1.5 cm thick 
  13. cut the edge of your dough using a roulette or a chisel as in the picture above 
  14. pinch the two ends of the strips with your thumb and index it to finally get the shape of a flower 
  15. cover and let the volume double again 
  16. garnish the flower with the yolk whipping with a little milk and vinegar 
  17. and decorate with sesame, nigella and anise seeds 
  18. cook in a preheated oven at 200 degrees C for 15 to 25 minutes or longer depending on the capacity of your oven 



bonne journée 
