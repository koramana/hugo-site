---
title: bricks with potatoes and tuna / ramadan appetizer
date: '2013-07-15'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- ramadan recipe
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/brick-aux-pommes-de-terre-et-thon-entree-de-ramadan.CR2_1.jpg
---
![brick-to-apples-to-earth-and-tuna - entry-de-ramadan.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/brick-aux-pommes-de-terre-et-thon-entree-de-ramadan.CR2_1.jpg)

##  bricks with potatoes and tuna / ramadan appetizer 

Hello everybody, 

We like the terracotta bricks at home, and what is good with this joke is that we can vary it to infinity, with what we have at home .... 

Today, I made this joke adding tuna, and I do not tell you the delice ... a recipe not to be missed ... 

**bricks with potatoes and tuna / ramadan appetizer**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/brick-thon-pomme-de-terre-bourek-bel-batata.CR2_1.jpg)

portions:  10  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * leaves of bricks. 
  * 3 to 4 medium potatoes 
  * 1 onion 
  * 1 can of tuna 
  * ½ bunch of chopped parsley, or according to taste 
  * 1 handful of pitted green olives 
  * 1 cup of harissa coffee (or according to your taste) 
  * 200 grams of grated cheese 
  * salt, black pepper, coriander powder 
  * egg white to stick 
  * oil for frying 



**Realization steps**

  1. Wash, peel and cut the potatoes into large cubes, and cook in a little salted water. 
  2. cut the onion into small ones, and fry in a little oil, until it becomes translucent. 
  3. crush the potato drain from its cooking water, and place it in a large salad bowl. 
  4. over add cooked onion, chopped parsley, harissa, salt, black pepper and coriander powder. Mix well. 
  5. then add the grated cheese, and the olives cut in rounds then the tuna in pieces delicately. 
  6. fill in the bricks with this stuffing, fold them, and stick the last end with a little egg white. 
  7. cook in a hot oil bath. 
  8. drain the cooked boureks on paper sopalin, and present lukewarm ... 


