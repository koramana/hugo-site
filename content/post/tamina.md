---
title: Tamina
date: '2014-01-13'
categories:
- birthday cake, party, celebrations
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tamina-gateau-a-la-semoule-grillee-004-a_21.jpg
---
##  Tamina 

Hello everybody, 

This morning, I woke my children with the smell of grilled semolina, I wanted to prepare the Tamina, this magnificent honey delight, that usually Algerian families present to their guests when they come to congratulate them for a new born to the House. 

I still like semolina cakes, like [ Basboussa ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html>) , the [ qalb el louz ](<https://www.amourdecuisine.fr/article-qalb-el-louz-ou-chamia-86960968.html>) , not to mention makrouts ( [ Baked makrout ](<https://www.amourdecuisine.fr/article-26001222.html>) , [ makrout rolled ](<https://www.amourdecuisine.fr/article-makrout-roule-102845781.html>) ) and [ bradj ](<https://www.amourdecuisine.fr/article-bradj-gateau-de-semoule-100622694.html>) . 

a tradition too and to prepare this delight on the day of [ Mawlid Nabawi echarrif ](<https://www.amourdecuisine.fr/article-plats-mawlid-nabawi-67191047.html>) , which is the case for me today, I prepared them for this wonderful occasion, and my children loved it, especially since it was scented with a bit of cinnamon and "hror" a spicy spice that we use in Algeria. 

so without delay, I share with you this recipe, which despite everything, is at random: 

**Tamina**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tamina-gateau-a-la-semoule-grillee-004-a_21.jpg)

portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 2 measure of grilled semolina 
  * ½ measure of almond powder (optional) 
  * 1 measure of honey 
  * 1 measure of butter 
  * hror (optional) 
  * cinnamon 



**Realization steps**

  1. grill the semolina over a low heat until it turns a nice golden color 
  2. add the almond powder and grill a little more 
  3. cool a little 
  4. melt the butter and add the honey 
  5. at this point you can incorporate the Hror, but like me my children do not eat too much spice, I only use the hror to garnish 
  6. pour over the mixture almond and semolina in rain, while mixing (the dough must be neither too firm nor too light, so do not put too much semolina) 
  7. pour into a nice dish and decorate according to your taste 
  8. the most beautiful touch is the drawings makes a bit of cinnamon and hror 
  9. enjoy with a good tea 



Note the grilled semolina must be very cold, when it is added to the honey and butter mixture otherwise, you will not have a good result, so the best is to prepare the semolina in advance, and put in an airtight box. 

thank you for your visits and comments 

and good day. 

and for more recipes from [ Algerian cakes ](<https://www.amourdecuisine.fr/gateaux-algeriens-orientales-modernes-fete-aid>)

[ the index of Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)

the category of [ Algerian cakes ](<https://www.amourdecuisine.fr/gateaux-algeriens-orientales-modernes-fete-aid>)

the category of [ fried Algerian cakes ](<https://www.amourdecuisine.fr/gateaux-algeriens-frits>)

the category of [ Algerian cakes with glaze ](<https://www.amourdecuisine.fr/gateaux-algeriens-au-glacage>)

the category of [ Algerian cakes with honey ](<https://www.amourdecuisine.fr/gateaux-algeriens-au-miel>)

the category of [ Algerian cakes without cooking ](<https://www.amourdecuisine.fr/gateaux-algeriens-sans-cuisson>)

the category of [ patisseries algeriennes ](<https://www.amourdecuisine.fr/patisseries-algeriennes>)
