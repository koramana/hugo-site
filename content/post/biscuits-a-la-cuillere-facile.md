---
title: biscuits with easy spoon
date: '2016-09-08'
categories:
- Algerian dry cakes, petits fours
- sweet recipes
tags:
- To taste
- The start of the school year
- Algeria
- desserts
- Algerian cakes
- Cakes
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/biscuits-a-la-cuillere-1.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/biscuits-a-la-cuillere-1.jpg)

##  biscuits with easy spoon 

Hello everybody, 

Here is a recipe **biscuits with easy spoon** which is really worth doing and doing. I know that these cookies are everywhere, at a very affordable price, but I assure you that the homemade sponge cookies are really good, and the taste is totally different from the ones in the shop. 

When I plan to make a dessert, I prepare them in advance, and frankly the dessert is just a delight .... But sometimes my children or my husband ask me for their dessert at the last minute, and I use those of the trade. While the dessert is good and delicious, but for me it lacks the taste of more: that of homemade spoon cookies! 

I hope to organize myself more to make my 100% homemade desserts, especially with this super easy recipe of biscuits with a spoon. 

**biscuits with easy spoon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/biscuits-a-la-cuillere.jpg)

portions:  50  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 3 eggs 
  * 90 gr of crystallized sugar 
  * 35 gr of maizena 
  * 35 gr of flour 
  * 1 pinch of salt 
  * icing sugar 



**Realization steps**

  1. preheat the oven to 180 ° C 
  2. whisk the whites into the bowl, then slowly add the sugar while whisking until you have the bird's beak. 
  3. whip the egg yolks a little bit to liquefy them. 
  4. introduce them to the whites using a spatula without breaking them. 
  5. Sift the mixture of flour and cornstarch on the egg mixture and gently introduce it with the maryse.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/fa%C3%A7onnage-des-biscuits-%C3%A0-la-cuill%C3%A8re.jpg)
  6. fill the dough in a pastry bag, cut the end to have a diameter of 1 cm. 
  7. Punch cookies of the same size if possible, 
  8. sprinkle generously with the icing sugar cookies. and cook for 12 to 15 minutes. 
  9. let cool a little before unmolding cookies. 
  10. Cook 11/12 minutes at 180 ° 



![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/biscuits-a-la-cuillere-2.jpg)
