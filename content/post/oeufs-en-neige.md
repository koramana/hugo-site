---
title: eggs in snow
date: '2007-12-28'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/12/204532091.jpg
---
**![bonj19](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201451361.gif) **

**Egg whites mount faster in snow if,**

**add a pinch of icing sugar and,**

**a few drops of lemon juice.**

![th_byebye](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/204532091.jpg)
