---
title: cheese bourek
date: '2010-08-18'
categories:
- crepes, waffles, fritters

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/08/menu-0451.jpg
---
##  cheese bourek 

Hello everybody, 

because I had a little bit of yesterday's joke, I thought, I'm going to make a small amount of stuffed bourak cheese. and it was good, we liked it. 

for the recipe, I'm going to copy you at ratiba 

**cheese bourek**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/menu-0451.jpg)

**Ingredients**

  * brick sheets 
  * for around 300 gr of white cheese kind cow kiri 
  * 3 cloves of garlic 
  * a bunch of finely chopped parsley 
  * an egg 
  * 3 to 4 c. tablespoon of milk 



**Realization steps**

  1. put all the ingredients in a saucepan and measure over low heat, turning the mixture with a wooden spoon until all the cheese has melted   
![](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/2010-08-14-menu_thumb1.jpg)
  2. in any case this quantity gave me 10 medium-sized boureks. 
  3. let cool, then fill the pastry sheets and fry in the hot oil 
  4. you can measure them in the oven and brush them with butter to avoid fried 



Merci et bonne journee 
