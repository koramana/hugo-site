---
title: crown pizza
date: '2015-04-08'
categories:
- pizzas / quiches / tartes salees et sandwichs
tags:
- Pizza dough
- Algeria
- Cheese
- inputs
- dishes
- Easy cooking
- Bakery

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/pizza-couronne-3.jpg
---
[ ![crown pizza](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/pizza-couronne-3.jpg) ](<https://www.amourdecuisine.fr/article-pizza-en-couronne.html/pizza-couronne-3>)

##  Pizza in a crown 

Hello everybody, 

Who does not like pizza? I'm sure even the most wayward of us loves pizza. We all ate pizza with all the sauces and all the possible fillings. Today is a new pizza look that we have, a nice presentation from my friend **Wamani Merou,** a woman well gifted in cooking and who makes very nice recipes, I am happy that she shares these delights with us on my blog. 

I hope that just like me you like the recipes that share with us these women well endowed in cooking. 

**crown pizza**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/pizza-couronne-2.jpg)

portions:  4  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 350 g flour 
  * Salt and sugar 
  * 9 g of instant dry yeast 
  * 3 tablespoons of olive oil 
  * Lukewarm water 
  * The choice of garnish 



**Realization steps**

  1. prepare the pizza dough and let it rise until it doubles in size 
  2. degas the dough and spread it in a circle, it is better to place the pizza dough in the baking dish. 
  3. place the filling of your choice in a circle, avoiding the center of the pizza   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/pizza-couronne.jpg) ](<https://www.amourdecuisine.fr/article-pizza-en-couronne.html/pizza-couronne>)
  4. cut with a sharp knife the center of the pizza as in the picture   
[ ![crown pizza](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/pizza-couronne-1.jpg) ](<https://www.amourdecuisine.fr/article-pizza-en-couronne.html/pizza-couronne-1>)
  5. fold each triangle on a part of the filling then weld it under the extreme edge of your dough 
  6. cook the pizza in a preheated oven at 180 degrees C for almost 15 to 20 minutes depending on your oven. 
  7. Enjoy hot with a drizzle of olive oil 



[ ![crown pizza](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/pizza-couronne-2.jpg) ](<https://www.amourdecuisine.fr/article-pizza-en-couronne.html/pizza-couronne-2>)
