---
title: grilled with apples
date: '2017-02-18'
categories:
- recettes sucrees
- tartes et tartelettes
tags:
- Apple pie
- compote
- Linz pie
- Linzertorte
- pies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/grill%C3%A9-aux-pommes-3.jpg
---
##  [ ![grilled with apples 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/grill%C3%A9-aux-pommes-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/grill%C3%A9-aux-pommes-3.jpg>)

##  grilled with apples 

Hello everybody, 

You think of the Lunetoiles recipe where she shared with us: [ apple marmalade ](<https://www.amourdecuisine.fr/article-marmelade-de-pommes.html>) ? She made this marmalade, to make this grilled apples, or apple pie way Linzer. 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

[ ![grilled with apples 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/grill%C3%A9-aux-pommes-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/grill%C3%A9-aux-pommes-2.jpg>)   


**grilled with apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/grill%C3%A9-aux-pommes-1.jpg)

**Ingredients** For 16 parts (yes it is better to eat small portions, it's a lot sweet) Pastry : 

  * 200 g flour 
  * 100 g of butter 
  * 1 pinch of salt 
  * 1 pinch of baking powder 
  * Water 

Garnish : 
  * 1 big pot of [ apple marmalade ](<https://www.amourdecuisine.fr/article-marmelade-de-pommes.html>)



**Realization steps** In the robot: 

  1. Put the flour and butter cut in pieces in a robot with steel blades. 
  2. add a pinch of salt and a pinch of baking powder. 
  3. Pulp until the mixture is like sand. 
  4. Then add the water little by little, while activating the robot, until it forms a ball of dough. 

By hand : 
  1. Put the flour on a baking board. 
  2. Make a hole in the center, add a pinch of salt, a pinch of baking powder and butter in small pieces. 
  3. Work the dough, that is, incorporate as much butter flour as possible, then add water while working until you can form a ball of dough. 
  4. Do all this as quickly as possible. The less the dough is handled, the lighter it is. 
  5. Spread this dough with a roll. 
  6. Flour a pie pan, press the dough over, cut the edges and prick the bottom with a fork. 
  7. Fill with apple marmalade. 
  8. With the leftovers of broken pasta, form a ball and spread on a floured work surface. 
  9. Make small strips and put them in a grid. 
  10. Cook for 30 minutes at 170 degrees C. 
  11. Cut in small parts. 



[ ![grilled with apples 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/grill%C3%A9-aux-pommes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/grill%C3%A9-aux-pommes.jpg>)
