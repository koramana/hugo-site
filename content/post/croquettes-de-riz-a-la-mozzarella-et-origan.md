---
title: rice croquettes with mozzarella and oregano
date: '2018-01-03'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- recipes of feculents
- salty recipes
- rice
tags:
- inputs
- dishes
- Easy cooking
- Healthy cuisine
- Economy Cuisine
- accompaniment
- Express cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/12/croquettes-de-riz-%C3%A0-la-mpzzarella-1-683x1024.jpg
---
![Rice croquettes with mpzzarella 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/croquettes-de-riz-%C3%A0-la-mpzzarella-1-683x1024.jpg)

##  rice croquettes with mozzarella and oregano 

Hello everybody, 

To be honest, I use a lot of oregano in my kitchen, and it was difficult for me to find a recipe to honor it properly, especially since I love it all the time, besides the tizane d Oregano is my favorite in winter, especially when I have cough (I drink cups and cups, hihihihi) 

To make these rice croquettes with mozzarella and oregano, I reconciled myself with my old Italian cookbook: "What's cooking Italian" by Penny Stephens, a very old book hidden in my little library under the tones of maths books and science of my children! it took me a lot of effort to find it, but it was worth it because these croquettes did not last long, my children have everything to eat, fortunately I was saved some parts for my husband, and who had the chance to make my stars in photos. 

and here is the video recipe for you: 

{{< youtube u4sP0jZxkos >}} 

So before going to the recipe, we are going with the list of all the previous rounds, their themes and their godmothers: 

**rice croquettes with mozzarella and oregano**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/croquettes-de-riz-%C3%A0-la-mpzzarella-2.jpg)

**Ingredients**

  * 150 gr of round rice, arborio 
  * 2 tbsp. tablespoon of olive oil 
  * 1 medium onion, finely chopped 
  * 1 crushed garlic clove 
  * ½ red pepper cut into cubes 
  * 1 C. dry oregano 
  * 450 ml vegetable broth or chicken broth 
  * 75 gr grated or cubed mozzarella. 
  * oil for frying 



**Realization steps**

  1. fry the onion and garlic in the oil until you have a nice translucent color 
  2. add the red pepper and sauté again. 
  3. then add the rice and fry to coat well in the oil 
  4. add the oregano, and begin to sprinkle the mixture with small amounts of chicken broth. Each time we water almost 150 ml, and let the rice well absorb the broth before adding another amount of broth. 
  5. when you have exhausted all the broth and the rice is well cooked, remove from heat and allow to cool well. 
  6. take a quantity of cooked rice, the equivalent of a large tablespoon. spread it on a sheet of cling film, put on the mozzarella and enclose to form the ball, (I prefer to form the balls with the film of food film to keep all the starch of the rice, which risks sticking to the hands, and the dumplings will crumble during cooking) continue until the total amount of rice is exhausted (you will have the equivalent of 12 croquettes) 
  7. heat a bath of oil and cook the croquettes in just to have a beautiful golden color. 
  8. remove the dumplings frying delicately, because the mozzarella is super fondant inside, enjoy very hot. 



![Croquettes Rice At The Mozzarella](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/croquettes-de-riz-%C3%A0-la-mpzzarella.jpg)

et voila la liste des participantes ainsi que de toutes les recettes à base d’origan, régalez vous: 
