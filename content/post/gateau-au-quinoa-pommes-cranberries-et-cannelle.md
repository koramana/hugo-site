---
title: quinoa cake, apples, cranberries and cinnamon
date: '2013-12-21'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/dessert-au-quinoa-pomme-et-cranberries.CR2_.jpg
---
[ ![dessert with apple quinoa and cranberries.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/dessert-au-quinoa-pomme-et-cranberries.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/dessert-au-quinoa-pomme-et-cranberries.CR2_.jpg>)

##  quinoa cake, apples, cranberries and cinnamon 

Hello everybody, 

It tells you a dessert without guilt, a very delicious dessert, very rich in taste, with the small crunchy quinoa, and roasted almonds ... and this infinite sweetness of soft and tender apples ... without forgetting the bewitching scent of cinnamon to take you too as far as possible by this cold weather ... 

And believe me, it's so good, and more "very healthy" ... it will accompany very well a bowl of hot milk, or just a drizzle of natural yoghurt, or Greek yogurt top with a little cranberries ... 

What's great about this recipe is that we do not need to dirty all our kitchen utensils, everything goes in your baking pan ... hop hop hop and it's already baked ... 

a very easy recipe, which I share with great pleasure with you: 

**quinoa cake, apples, cranberries and cinnamon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-au-quinoa-et-cranberries.CR2_.jpg)

Recipe type:  cake, dessert  portions:  16  Prep time:  10 mins  cooking:  60 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 cup uncooked quinoa (240 ml cup) 
  * 1 teaspoon and a half of cinnamon 
  * ½ teaspoon of nutmeg 
  * ⅛ of teaspoon of ground cloves 
  * 2 apples, peeled, diced 
  * ¼ cup dried cranberries. 
  * 2 eggs 
  * 2 cups of milk. 
  * ¼ cup maple syrup 
  * ⅓ cup chopped almonds (it'll be good if it's grilled) 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. butter lightly a baking tin of 20 cm on the side. 
  3. In a small bowl, mix the uncooked quinoa with the spices. Pour into the mold buttered. 
  4. add the dried apples and cranberries on top. 
  5. In this same little bowl, beat the eggs. Whip milk and maple syrup. 
  6. Pour the egg mixture and milk over the quinoa mixture of apples and cranberries. Stir gently with a wooden spoon. 
  7. Do not sprinkle on chopped almonds. 
  8. Bake for an hour or until no more liquid is left in the cake. 
  9. Let cool, then cover and refrigerate. 
  10. At the time of serving, cut a square, heat it in the microwave for a few seconds, and enjoy according to your taste. 



Apple Cinnamon Gluten-Free Quinoa Breakfast Bake 

#####  INGREDIENTS: 

  * 1 cup uncooked quinoa 
  * 1 1/2 teaspoons cinnamon 
  * 1/2 teaspoon nutmeg 
  * 1/8 teaspoon ground cloves 
  * 2 apples, peeled, diced 
  * 1/4 cup cranberries 
  * 2 eggs 
  * 2 cups milk 
  * 1/4 cup maple syrup 
  * 1/3 cup almonds, chopped 



###  DIRECTIONS 

  1. Preheat the oven to 350 ° F. Lightly grease has a 7-by-11-inch baking dish (an 8-by-8 works too). 
  2. In a small bowl, mix the uncooked quinoa with the spices. For into greased dish. 
  3. Sprinkle the apple and cramberries on top of the quinoa. 
  4. In that same small bowl, beat the eggs. Whiskey in the milk and maple syrup. 
  5. For the egg and milk mixture over the top of the fruit and quinoa. Lightly stir to partially submerge the fruit. Sprinkle the chopped almonds on top. 
  6. Bake for one hour or until the pot is mostly set with a small amount of liquid left. 
  7. Allow to cool, and then cover and refrigerate. In the morning, cut a square, it's a plate, and enjoy alone with a few dollops of Greek yogurt. 



[ ![cake with quinoa.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-au-quinoa.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-au-quinoa.CR2_.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
