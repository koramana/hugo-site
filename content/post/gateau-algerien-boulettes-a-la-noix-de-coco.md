---
title: Algerian cake / coconut dumplings
date: '2013-08-01'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/les-gateaux-algeriens-Aid-el-fitr-2012-boules-de-neige_thum1.jpg
---
Hello everybody, 

Here is an Algerian cake recipe, super good, very easy to make, and above all, it is an Algerian cakes without cooking, what I like cakes without cooking. 

it's coconut snowballs, stuffed with a nice stuffing of biscuits mixed with nutella, which coats a grilled almond crisp. 

and as we approached the el kirir 2012, many of you had asked me for this delicious recipe, so I share it with you .... 

**Algerian cake / coconut dumplings**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/les-gateaux-algeriens-Aid-el-fitr-2012-boules-de-neige_thum1.jpg)

Recipe type:  algerian cake 2014  portions:  30  Prep time:  30 mins  total:  30 mins 

**Ingredients**

  * grilled almonds (I put on the spit, because all this cake, is just at random) 
  * a box of biscuits. 
  * Nutella 
  * coconut 
  * sweetened condensed milk 



**Realization steps**

  1. method of preparation: 
  2. first, toast the almonds. and let cool. 
  3. crush the biscuits, I did not powder it. 
  4. add nutella to the biscuits, until you obtain a soft and malleable paste.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/boulettes-de-neige-a-la-noix-de-coco-gateaux-sans-cuisso1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/boulettes-de-neige-a-la-noix-de-coco-gateaux-sans-cuisso1.jpg>)
  5. coat each almond with this mixture, do not mix a lot, otherwise you may have large pellets in the end. 
  6. now mix the coconut with sweetened condensed milk, also to have a dough that picks up. 
  7. coat the nutella dumplings with the coconut paste. 
  8. then roll the meatballs into powdered coconut, and your cake is ready. 



in any case, this cake has been a great success, and personally I will do it again for the help el Kebir 2012 

Source: Forum el djelfa. 
