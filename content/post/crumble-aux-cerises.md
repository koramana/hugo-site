---
title: cherry crumble
date: '2015-05-30'
categories:
- dessert, crumbles and bars
- sweet recipes
tags:
- desserts
- Cakes
- To taste
- Algerian cakes
- Pastry
- Ramadan
- Ramadan 2015

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/crumbles-aux-cerises-1.jpg
---
[ ![cherry crumbles 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/crumbles-aux-cerises-1.jpg) ](<https://www.amourdecuisine.fr/article-crumble-aux-clafoutis.html/crumbles-aux-cerises>)

##  cherry crumble 

Hello everybody, 

Another cherry recipe that has found its cellar hibernation in my email ... ... almost 2 years it wisely waiting to be online this recipe for cherry crumble ... I thank my friend Lunetoiles that m to mention this morning that she remembers having made a strawberry crumble, but she can not find the photos anymore .... a little search in my mailbox, and I found that she sent me the photos, fortunately. 

So here, enjoy my dear cherry season to realize these little crumbles .... and thank you Lunetoiles for your delights. 

**cherry crumble**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/crumbles-aux-cerises-3.jpg)

portions:  4  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 100 g of butter 
  * 130 g of flour 
  * 80 g marzipan 
  * 80 g of sugar 
  * about fifty cherries 



**Realization steps**

  1. Preheat the oven to 200 ° C 
  2. Pit all the cherries and put them in small pie molds or a large pie pan. 
  3. Mix all the other ingredients in a bowl until they are crumble-like.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/crumble-aux-cerises.jpg) ](<https://www.amourdecuisine.fr/article-crumble-aux-cerises.html/crumble-aux-cerises>)
  4. Place the dough on the cherries and bake at 200 degrees for approx. 20 minutes or until crumble is crisp and golden. 



[ ![cherry crumbles](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Crumbles-aux-cerises-.jpg) ](<https://www.amourdecuisine.fr/article-crumble-aux-clafoutis.html/crumbles-aux-cerises>)
