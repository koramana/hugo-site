---
title: Spinach slipper
date: '2011-12-20'
categories:
- Non classé @en
- recettes sucrees
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/chaussons-epinards-2a11.jpg
---
![Spinach slipper](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/chaussons-epinards-2a11.jpg)

##  Spinach slipper 

Hello everybody, 

Today, I share with you this delicacy that comes directly from Algeria, from my friend Ouahiba, whom I knew, when she started to send me recipe photos of my blog that she made, and then she offered me photos of these recipes, and that I like to share with you. 

It's wonderful to share, and I like it, that now, it's more like before, women were always trying to keep their secrets of successful recipes, but now, the notion of sharing has won our hearts, and it's is a very good thing 

therefore, I share with you this delight:   


**Spinach slipper**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/chaussons-epinards-2a.jpg)

**Ingredients**

  * 2 spinach bunches, cut and steamed or just by the steak when using only the leaves 
  * 200gr of minced meat 
  * 100g green olives, pitted and sliced ​​and desalted 
  * Garlic (quantity according to taste) 
  * Black pepper 
  * A little paprika 
  * Cumin (not a lot) 
  * Spice according to taste (I put lahrissa) 
  * Processed cheese type cow laughing 
  * Fresh cream to thicken the mixture and add sweetness if not bechamel sauce 
  * olive oil 
  * Salt 



**Realization steps**

  1. In a pan cook the minced meat in olive oil with all the spices except cumin 
  2. Add spinach if cooked, add olives and reduce on high heat 
  3. Add the cheese and mix, then the cream and let reduce the maximum 
  4. If we put bechamel we do it out of the fire 
  5. Add the caraway, season the seasoning (salt) and let cool 
  6. So you can add mushrooms. 
  7. For dough, I used semi-flaky dough or quick puff pastry, (but I'm not putting Swiss puffs just the flour and puff pastry margarine and the rest even preparation. 
  8. Roll out the dough at 3mm cut out circles to coat the egg rims put stuffing in the center and solder in slipper, coat with egg the surface and sprinkle with unsweetened sesame seeds 
  9. Bake in preheated oven at 200 ° C 


