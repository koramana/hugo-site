---
title: extra fluffy coffee cake
date: '2014-03-13'
categories:
- Bourek, brick, samoussa, slippers
- Cuisine by country
- Tunisian cuisine
- idea, party recipe, aperitif aperitif
- salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/cake-cafe-2.jpg
---
![Cake-cafe-2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/cake-cafe-2.jpg) ![coffee cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/cake-cafe-1.jpg)

##  extra fluffy coffee cake 

Hello everybody, 

from the fact that I have not made any changes to the recipe I am copying / pasting.    


**extra fluffy coffee cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/cake-cafe-3.jpg)

Recipe type:  cake dessert  portions:  10  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 3 eggs 
  * 200 g muscovado sugar (simple for me) 
  * 120 g of soft butter 
  * 200 g flour 
  * 3 tablespoons of strong coffee of good quality (3 tablespoons hot water in which I dissolve 3 teaspoons of nescafe) 
  * 3 tablespoons cocoa powder 
  * A pinch of salt 
  * A pinch of vanilla powder or a bag of vanilla sugar 
  * Two level teaspoons of baking powder 



**Realization steps**

  1. Preheat the oven to medium temperature 180 degrees C. 
  2. Start by whipping the sugar and butter with electric whisk, add the coffee, and vanilla, 
  3. continue beating for two minutes then add the eggs one by one while continuing to beat. 
  4. In a bowl, mix together flour, salt, yeast and cocoa powder. 
  5. Add little by little to the first mixture and stir with a wooden spatula until a homogeneous mixture. 
  6. Butter a cake mold, sprinkle with cocoa, shake the mold to get rid of extra cocoa and pour the dough. 
  7. Bake for 35 to 40 minutes, check the cooking with a knife, 
  8. remove from oven and let cool before unmolding. 
  9. Sprinkle with icing sugar when serving and savor. 



![cake-cafe-11.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/cake-cafe-11.jpg)

good tasting 

pour la recette en arabe: 
