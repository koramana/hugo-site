---
title: tuna and mushroom pizza
date: '2017-04-06'
categories:
- appetizer, tapas, appetizer
- pizzas / quiches / pies and sandwiches
tags:
- Bakery
- Pizza dough

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pizza-au-thon-010.jpg
---
[ ![tuna and mushroom pizza](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pizza-au-thon-010.jpg) ](<https://www.amourdecuisine.fr/article-pizza-au-thon-et-champignon.html/pizza-au-thon-010>)

##  tuna and mushroom pizza 

Hello everybody, 

When it's holidays, kids ask and ask for pizza for lunch, and frankly, this does not bother me, because with my [ magic pasta ](<https://www.amourdecuisine.fr/article-pate-magique.html> "Magic paste") I know I will not miss my pizza. 

Also I want to say it again, the pizza at home is a pleasure, each one garnishes his pizza according to his taste: there is who wants it without cheese, there is who wants it without olives ... When to me, I let myself go, hihihih, I put everything I have in it, and this time it was: tuna, mushrooms, black olives, mozzarella, dried basil ... 

So are we going for this tuna margherita pizza?   


**tuna and mushroom pizza**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pizza-au-thon-012-300x192.jpg)

portions:  4  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * [ magic pasta ](<https://www.amourdecuisine.fr/article-pate-magique.html> "Magic paste")
  * tomato sauce 
  * 1 can of tuna 
  * mushrooms 
  * pitted black olives 
  * Mozzarella 
  * Tomato sauce: 
  * 4 to 5 fresh tomatoes, if not two boxes of cut tomatoes. 
  * 2 cloves garlic 
  * Dried basil 
  * salt, black pepper, and thyme 
  * 2 tablespoons of olive oil. 



**Realization steps**

  1. prepare the magic dough, by hand or at the bread machine. 
  2. prepare the tomato sauce, placing all the ingredients in a pot and cook until reduced. 
  3. cut the mushrooms into strips. 
  4. degas the pizza dough, and spread it in a circle on a flour area. 
  5. garnish with the chilled tomato sauce. 
  6. then decorate the slices of mushrooms, olives, tuna and grated mozzarella cheese. 
  7. cook the pizza in a preheated oven at 180 degrees C for 15 to 20 min. 
  8. present this hot pizza, and treat yourself. 



[ ![tuna and mushroom pizza](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pizza-au-thon-017.jpg) ](<https://www.amourdecuisine.fr/article-pizza-au-thon-et-champignon.html/pizza-au-thon-017>)
