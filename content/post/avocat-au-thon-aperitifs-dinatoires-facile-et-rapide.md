---
title: tuna avocado quick and easy aperitifs
date: '2012-12-30'
categories:
- Algerian cuisine
- diverse cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-d-avocat-020_thumb1.jpg
---
##  tuna avocado quick and easy aperitifs 

Hello everyone, 

I'm posting you today a little [ salad ](<https://www.amourdecuisine.fr/categorie-10678933.html>) : Tuna avocado a quick and easy dinatory aperitifs that I prepared this week, and that we really liked, I do not tell you the delight. A recipe very very simple, but very presentable. 

It is always ideal to present this recipe: tuna avocado quick and easy dinatory aperitifs in a buffet, or for friends as an aperitif, it's just to fall.   


**tuna avocado quick and easy aperitifs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-d-avocat-020_thumb1.jpg)

**Ingredients** for two people 

  * 1 lawyer 
  * 1 green onion 
  * ½ can of tuna in the oil 
  * the juice of 1/2 lemon 
  * salt 
  * chopped parsley 



**Realization steps**

  1. Cut the avocados in half, remove the core. With the knife, draw inside crossed lines, which will form cubes, for that, with a spoon, remove the fruit, you will see, it's magic, the cubes are perfect, is the flesh is intact 
  2. put in a bowl, add the tuna in pieces, the onion cut into small pieces, the chopped parsley, season with lemon juice and salt. 
  3. fill the flesh of the avocado halves with this stuffing, and present it with a very good salad 



[ recette du ramadan ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>)
