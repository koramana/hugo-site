---
title: Eggplant pie
date: '2015-05-07'
categories:
- cuisine algerienne
- cuisine diverse
- houriyat el matbakh- fatafeat tv
- plats vegetariens
- recette de ramadan
tags:
- Algeria
- Vegetables
- Healthy cuisine
- Easy cooking
- Full Dish
- Ramadan 2015
- sauces

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/papetons-daubergine-6.jpg
---
[ ![eggplant papetons](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/papetons-daubergine-6.jpg) ](<https://www.amourdecuisine.fr/article-papetons-d-aubergine.html/sony-dsc-286>)

##  Eggplant pie 

Hello everybody, 

Again with another aubergine recipe. This time, it's a recipe of my dear **Lunetoiles** who shared with us the recipe for aubergine papetons, a specific dish from the city of Avignon that comes in the form of a flan made with aubergine caviar and eggs accompanied by a coulis of fresh tomatoes . 

Generally, the eggplant papet is made in a cake mold, but I really like this mini version of Lunetoiles and its way of cooking the custard in muffin cups, which gives a nice individual version of this dish.   


**Eggplant pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/papetons-daubergines-3.jpg)

portions:  4-5  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 2 large aubergines (500 g) 
  * 4 cloves of garlic 
  * 50 cl of tomato coulis 
  * 4 eggs 
  * 100 g grated Gruyère cheese 
  * 2 sprigs of fresh thyme (or 1 teaspoon dried thyme) 
  * 3 tbsp. tablespoon of olive oil 
  * salt and pepper 



**Realization steps**

  1. Peel and cut the eggplant into cubes. 
  2. Peel and slice garlic cloves. 
  3. Heat the oil in a pan and fry the aubergines with the garlic. 
  4. Add thyme of salt and pepper. 
  5. Cook for 10 to 15 minutes. 
  6. Mix the eggplant pulp in a robot. 
  7. Beat the omelette eggs in a bowl and stir in the aubergine purée. 
  8. Butter and flour ramekins and pour the preparation (I prefer to use silicone molds, it is easier to unmold for this recipe).   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/70-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41949>)
  9. Bake 25 to 30 minutes at 180 ° C th.6 
  10. Unmold eggplant papetons. 
  11. In a gratin dish, pour tomato coulis. 
  12. Put the eggplant papetons in the dish,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/papetons-daubergines-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41946>)
  13. top the papetons with the tomato coulis and sprinkle with grated Gruyere cheese on top of the aubergines. 
  14. Go under the grill of the oven until the cheese melts are golden and gratinated. 



[ ![eggplant squares2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/papetons-daubergines2.jpg) ](<https://www.amourdecuisine.fr/article-papetons-d-aubergine.html/papetons-daubergines2>)
