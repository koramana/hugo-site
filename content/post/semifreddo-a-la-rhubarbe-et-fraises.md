---
title: semifreddo with rhubarb and strawberries
date: '2017-06-03'
categories:
- ice cream and sorbet
- sweet recipes
tags:
- Without ice cream maker
- Stewed Rhubarb
- Red berries ice cream
- Red fruits
- Ramadan
- Ramadan 2017
- Easy recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/06/semifreddo-%C3%A0-la-rhubarbe-et-fraises-1-683x1024.jpg
---
![semifreddo with rhubarb and strawberries 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/semifreddo-%C3%A0-la-rhubarbe-et-fraises-1-683x1024.jpg)

##  semifreddo with rhubarb and strawberries 

Hello everybody, 

By this heat it must be said that there is no more delicious than a frozen dessert, like a good semifreddo with rhubarb and strawberries! This dessert is to consume without moderation, especially after a long day of fasting, I do not tell you when I put my little piece of semifreddo rhubarb and strawberry in front of my surfing the net, I enjoy it thoroughly. 

In any case, if you like video recipes, why not look at the recipe for this rhubarb and strawberry semifreddo: 

**semifreddo with rhubarb and strawberries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/semifreddo-%C3%A0-la-rhubarbe-et-fraises.jpg)

**Ingredients**

  * 150 gr rhubarb in pieces 
  * 150 gr strawberries in pieces 
  * 150 g caster sugar 
  * 2 tbsp. tablespoons water 
  * 2 cm fresh ginger 
  * 4 eggs (whites separated from yolks) 
  * 100 g icing sugar 
  * 300 ml whipping cream 
  * 50 g of meringues broken into pieces 



**Realization steps**

  1. Put rhubarb and strawberries in a pan with the powdered sugar and 2 tbsp. of water. 
  2. Heat gently until the sugar is dissolved, simmer for 10 to 15 minutes or until the fruit becomes tender but keeps its shape. 
  3. add the grated ginger and remove from heat and let cool. 
  4. In a large bowl, whisk the egg whites with an electric whisk, then set aside. In another bowl, whisk the yolks with the icing sugar until they are pale and begin to thicken. 
  5. Finally, in a third bowl, whip the whipped cream. 
  6. Add the mixture of yolks in the whipped cream until they are well combined, then add the egg whites and half of the broken meringues. 
  7. pour 1/3 of the cream mixture into a cake pan, and place in the freezer until it takes. 
  8. remove from the freezer and pour ⅓ of the rhubarb sauce and strawberries over it .. 
  9. Garnish with another third of the cream mixture and return to the freezer for another 30 minutes. 
  10. Once this layer is well taken, cover with ⅓ rhubarb sauce and strawberries, followed by a last layer of cream. Cover with cling film and return to the freezer for at least 3 hours. 
  11. To serve, take it out 5 minutes before, place in a serving dish and garnish with remaining rhubarb sauce and strawberries and remaining meringue pieces. Cut and serve immediately. 



![semifreddo with rhubarb and strawberries 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/semifriddo-%C3%A0-la-rhubarbe-et-fraises-2.jpg)

et voila la liste des participantes à cette ronde, ainsi que les titres de leurs réalisations: 
