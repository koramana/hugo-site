---
title: Trianon of Meriame
date: '2010-06-13'
categories:
- Algerian cakes- oriental- modern- fete aid
- Gateaux Secs algeriens, petits fours

---
This is the recipe of a faithful reader, I was happy that she sent me this delicious cake for me to share with you. thank you very much Meriame, it's a cake she had prepared for Easter, but it does not matter, it's never too late to share with you his great work. the recipe as she tells me is a mix between the recipe Eryn and Guylaine For a circle between 13 and 16 cm in diameter: For a large cake, just double the proportions. For a circle of 26 cm in diameter, it will only be higher & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 0.6  (  1  ratings)  0 

This is the recipe of a faithful reader, I was happy that she sent me this delicious cake for me to share with you. thank you very much Meriame, it's a cake she had prepared for Easter, but it does not matter, it's never too late to share with you his great work. 

** For a circle between 13 and 16 cm in diameter:  **

_ Pour un grand gâteau, il suffit de doubler les proportions. Pour un cercle de 26 cm de diamètre, il n’en sera que plus haut 🙂  _

For the choco-almond cookies base  : 

\- 40 g dark chocolate 70%   
\- 1 egg   
\- 10 g cornflour   
\- 10 g almond powder   
\- 1 pinch of salt 

For the praline feuillantine  : 

\- 100 g of dessert pralinoise   
\- 25 g of praline   
\- 65 g of pancakes (8 packs of 2) 

For pure chocolate mousse  : 

\- 100 g of dark chocolate at least 70%   
\- 2 eggs   
\- 1 pinch of salt 

Chocolate contour  : 

\- 40 g dark chocolate 70% 

Prepare the biscuit base  : 

\- Preheat the oven to 160 ° C. Melt the broken chocolate in pieces in a bain-marie or microwave (without cooking, every 20 seconds). Add the egg yolk and mix vigorously. Add the cornflour and the almond powder. The dough is thick and forms a ball. Beat the egg white with a pinch of salt. Incoporate with a third, stirring briskly with chocolate and then add the remaining 2/3 to the maryse, as gently as possible. The dough will not be liquid anyway, it will remain very thick. 

\- Butter and flour the circle, place it on a baking sheet covered with parchment paper and pack in the chocolate mixture. Bake for  10 minutes at 160 ° C.  Then slide the circle and its contents gently onto a dish, removing the parchment paper. 

2 /  Prepare the praline feuillantine  : 

Melt the broken praline in pieces in a bain-marie or microwave. Add praline and crushed lace crêpes. Mix well and pour on the biscuit base in the circle, pressing well with the back of a spoon. Place in the fridge. 

3 /  Prepare the dark chocolate mousse  : 

Melt the dark chocolate broken in pieces in a bain-marie or microwave with a drop of water. Mix the egg yolks energetically. Let cool. Whisk the egg whites with a pinch of salt and gently add them to the chocolate by raising the dough well with a spatula. Pour over the praline feuillantine, smooth with a spatula and place in the fridge for 3 hours 

Deco 

Melt 40 g dark chocolate in pieces  _ (bain-marie or microwave in several times).  _ mix well to homogenize. Coat a 3 cm wide band of rhodoïd  _ (or parchment paper, or transparent),  _ Let stand for a few seconds at room temperature, decant the cake, then circle this band  _ (chocolate side towards the cake).  _ Refrigerate 20 minutes, remove the rhodoïd delicately. Allow 20 minutes in the fridge, gently remove the rhodoïd 

** For the final decor:  **

QS of kadayf 

25 g melted butter 

chocolate eggs 

Chocolate decorations 

Untangle and place the kadaïf on 1 cm in the circle of 16 cm, pass 25 g melted butter with the brush and bake for 12 mn in the oven at 180 ° C. 

Finishing: 

Retirez délicatement le cercle, posez le kadaïf sur le dessus. Décorez avec des œufs 
