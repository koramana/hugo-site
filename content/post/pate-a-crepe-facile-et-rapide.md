---
title: easy and fast pancake batter
date: '2018-02-05'
categories:
- crepes, donuts, donuts, sweet waffles

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-a-l-orange_thumb.jpg
---
##  easy and fast pancake batter 

Hello everybody, 

Here is a recipe for pancake batter easy and fast, especially if you're like me and your children do not stop asking for pancakes? 

To make fast I realize the recipe for my pancake batter easy and fast? a guaranteed recipe and a bluffing result. 

This time in my pancake batter easy and fast I added a little orange juice and zest to have pancakes well scented, the taste is intense. In any case I really like the taste of the orange in my desserts and snacks. and this easy and fast pancake batter will really make you happy, and do not risk leaving your table to snack for a long time.   


**easy and fast pancake batter**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-a-l-orange_thumb.jpg)

portions:  40  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 2 eggs 
  * 35 g of sugar 
  * 150 g flour 
  * 400 ml of milk 
  * 50 ml of orange juice 
  * the zest of an orange 
  * 50 g of butter 
  * 2 tablespoons of oil 



**Realization steps**

  1. In a salad bowl put the eggs with the sugar 
  2. Add flour, orange peel, and butter to pieces 
  3. mix well and add the milk gradually 
  4. then add the orange juice (if it's canned juice, reduce the sugar) 
  5. add oil, mix again. 
  6. cover with plastic wrap and let stand for 2 hours. 
  7. Cook the pancakes in an anti-adhesive pan. 



enjoy with a spread, such as  [ spread with gingerbread ](<https://www.amourdecuisine.fr/article-pate-a-tartiner-au-pain-d-epice-98437586.html>) , or with  [ applesauce ](<https://www.amourdecuisine.fr/article-compote-de-pomme-98542533.html>) , or bake a cake with these delicious pancakes 

a version of [ crepes natures ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee-114932092.html>) : 

[ ![crepes natures](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-17.bmp_thumb.jpg) ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee-114932092.html>)
