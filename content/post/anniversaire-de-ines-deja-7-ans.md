---
title: Anniversary of Ines already 7 years
date: '2014-10-18'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- Coffee love of cooking
- cakes and cakes
- sweet recipes
tags:
- Birthday cake
- Cakes
- Tutorial
- Soft
- fondant
- Cake
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/ines-birthday-032.CR2_.jpg
---
[ ![Anniversary of Ines already 7 years](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/ines-birthday-032.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/ines-birthday-032.CR2_.jpg>)

##  Anniversary of Ines already 7 years 

Hello everybody, 

No recipe today, it's my daughter's birthday. I concocted this delicious and beautiful birthday cake, which we loved at home. 

Too bad, I did not apply too much in the decoration of the cake, with my baby who was turning here and there, and especially that the dentition tires him a lot, I did not stop at the time on the decoration. 

I went to ebay, where I bought her the rice paper decoration with the subject: My little pony, the favorite cartoon of my Ines. 

So click to see the recipe of this cake: 

###  [ **bavarian biscuit joconde mousse with cream custard** ](<https://www.amourdecuisine.fr/article-bavarois-biscuit-joconde-mousse-a-la-creme-custard.html>)

[ ![ines birthday 039](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/ines-birthday-039.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/ines-birthday-039.jpg>)

[ ![ines birthday 044.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/ines-birthday-044.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/ines-birthday-044.CR2_.jpg>) [ ![ines birthday 046](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/ines-birthday-046.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/ines-birthday-046.jpg>)

Merci pour vos visites, et a la prochaine. 
