---
title: ghribya with almonds and sesame seeds
date: '2017-04-29'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
tags:
- Algeria
- Algerian cakes
- Cakes from L Aid
- Cookies
- Fondant cakes
- Easy cooking
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/04/ghribya-aux-amandes-et-graines-de-s%C3%A9sames-2.jpg
---
![almond ghribya with sesame seeds 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/ghribya-aux-amandes-et-graines-de-s%C3%A9sames-2.jpg)

##  ghribya with almonds and sesame seeds 

Hello everybody, 

this almond ghribya with sesame seeds comes directly from the kitchen of my little sister Lunetoiles. It is a pleasure for me to see her take a taste in photography. 

**ghribya with almonds and sesame seeds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/ghribya-aux-amandes-et-graines-de-s%C3%A9sames-1.jpg)

**Ingredients**

  * 500 gr of flour T55 
  * 150 gr of icing sugar 
  * 1 large glass of almonds with chopped skin 
  * 1 large glass (the same glass) of sesame seeds half whole seeds, half ground seeds 
  * 2 glass of table oil (always measured with the same glass) 
  * 2 bags of baking powder 
  * 2 tbsp. vanilla powder 
  * 1 pinch of salt 
  * 1 tbsp. a cinnamon coffee 
  * ½ cuil. fresh baker's yeast 



**Realization steps**

  1. Sift the flour into the relel then sift the icing sugar with vanilla and baking powder 
  2. make a well and put the almonds in the center and the sesame seeds 
  3. put the cinnamon, pinch of salt, the fresh yeast well crumbled 
  4. Mix then add the 2 glasses of oil and mix 
  5. pick up the dough with the palms of the hands 
  6. preheat your oven to 160 degrees rotating heat 
  7. shape balls of 25 gr 
  8. by flattening them slightly 
  9. bake at 160 ° C rotating heat 
  10. until they crack, 12 minutes for me 
  11. you can leave as is, or sprinkle with icing sugar 


