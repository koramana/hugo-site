---
title: Adhress, adghess / Algerian cuisine
date: '2013-06-27'
categories:
- shortbread cakes, ghribiya
- oriental delicacies, oriental pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/adhress-cuisine-algerienne1.jpg
---
![adhress - kitchen algerienne.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/adhress-cuisine-algerienne1.jpg)

##  Adhress, adghess / Algerian cuisine 

Hello everybody, 

Adhress, or adghess, an Algerian recipe that is based on the first milk of the cow or goat, after she gave birth, ie the Colustrum, known in Arabic under the name el lba, اللبى . 

But what is really nice is that even without this milk, we can prepare this delicacy that is found between a cheese and an omelette with eggs ... it can be made with unsweetened condensed milk. 

and you, do you know this recipe, at home, it has another name ??? 

**Adhress, adghess / Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/adhress-adghess-cuisine-algerienne1.jpg)

portions:  8  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 1 liter of raw goat's milk or cow's milk otherwise 3 boxes of unsweetened condensed milk 
  * 20 eggs (you can put up to 30 eggs) 
  * a spoonful of salt 
  * 3 tablespoons of olive oil 



**Realization steps**

  1. In a large bowl, beat the egg with an electric mixer or by hand, with the salt. 
  2. Add the raw milk of goat or cow and then 1 C to S of oil. 
  3. Beat the mixture well. 
  4. Heat the remaining oil in the pot, pour the mixture and cover with a lid. 
  5. Cook over very low heat. In cooking it will swell and double to triple volume, and it makes a lot of juice, it's normal. 
  6. Check the cooking with a knife inserted in the center, it must come out clean and when you touch with your finger, it must be firm. 
  7. Wait 5 minutes, you will see the adhress retract and cool down. 
  8. Then turn out, spill on a wire rack, or in a couscoussier to drain all the water. 
  9. Let drain well and put on a plate. 
  10. Put several hours cool before serving. 



method of preparation: 

{{< youtube kIrsBIaQAZU >}} 

![adghess - kitchen algerienne.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/adghess-cuisine-algerienne1.jpg)

4.6 stars based on 13 reviews 
