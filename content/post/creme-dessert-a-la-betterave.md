---
title: Beet cream
date: '2007-11-08'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- Dishes and salty recipes
- recipe for red meat (halal)
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/11/188880821.jpg
---
![beet](https://www.amourdecuisine.fr/wp-content/uploads/2007/11/188880821.jpg)

40 cl of milk   
70 g of sugar   
3 egg yolks   
30 grams of cornflour   
1 vanilla pod   
400 grams of cooked beet 

_Ingredients for beet crisps_ : 

1 small raw beet   
icing sugar 

and some pistachios for decoration 

_Preparation of the cream_ : 

1) Work the egg yolks with the sugar and cornflour until the mixture whitens   
2) Bring the milk and the vanilla pod (split lengthwise and take the seeds with the tip of a knife) to a boil, then slowly pour this hot milk (remove the vanilla pod) on the egg yolks, by turning regularly with a wooden spoon   
3) Put everything back in the pan and cook your cream over low heat, turning until it thickens   
4) Peel the beets, cut into small cubes and finely mix   
5) Remove the cream from the heat, add the mixed beet, mix   
6) Put in the fridge so that the preparation is very fresh 

_Preparation of beet crisps_ : 

1) Peel the beetroot and slice thinly with a mandolin, put on a plate lined with baking paper   
2) Sprinkle a little icing sugar   
3) Put in the preheated oven at 100 degrees until the slices become crispy (about 1 hour and 30 minutes)   
4) Decorate verrines with cooled beet crisps and coarsely chopped pistachios 
