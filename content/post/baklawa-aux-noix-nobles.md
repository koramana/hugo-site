---
title: baklawa with walnuts
date: '2011-11-16'
categories:
- dessert, crumbles and bars
- gateaux algeriens- orientales- modernes- fete aid
- gateaux, et cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-aux-noix-nobles1_thumb11.jpg
---
hello everyone, I have to post the recipes of Algerian cakes, prepared during the feast of the aid, and I will start with the recipe for Lunetoiles, because it is already written, so I will only make a copy paste. the recipe comes from Mouni .... ingredients: Pate: use a measure, the same all along the recipe, for example a 300ml bowl or a 250ml glass (the larger the container is and the larger the proportions will be) 5 measures of flour 1measurement of melted butter 1 / 2cc salt water (possibly added) 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

I have to post the Algerian cake recipes, prepared during the aid party, and I will start with the Lunetoiles recipe, because it is already written, so I'm going to copy and paste. 

the recipe comes from Mouni .... 

ingredients: 

**Dough** : 

use one measure, the same all along the recipe, for example a 300ml bowl or a 250ml glass (the larger the container and the larger the proportions will be) 

  * 5 measures of flour 
  * 1 measure of melted butter 
  * 1 / 2cc of salt 
  * 1 egg 
  * water (possibly with added orange blossom water) 



**Prank call:**

  * 3 measures of coarsely ground walnuts: or noble walnuts: cashews, walnuts, almonds and hazelnuts 
  * a little less than one measure of powdered sugar 
  * 2 teaspoons of cinnamon (see more according to taste) 
  * 3 - 4 tablespoons of melted butter 
  * Orange tree Flower water 



**Assembly:**

Melted butter 

**Finish:**

Melted honey with orange blossom water (about 1kg of honey) 

method: 

  1. Start by making the dough, for that mix all the ingredients by adding little by little water to collect the dough, 
  2. divide it into 12-14 small balls, half will serve for the lower and upper layer of dough, cover them with cling film and let them rest minimum 30min, ideally 1h. 
  3. meanwhile prepare the stuffing by mixing all the ingredients by adding a little orange blossom, reserve. 
  4. take a ball of dough and start to spread it with a roll, the dough must be so thin that you have to see the worktop through it, in a previously buttered tray, gently put it down, then brush it with butter . 
  5. Repeat this operation with the other 5 sheets (or 6 if you chose the "14 balls" option), taking care to butter between each sheet and chasing any air bubbles with a curing agent. tooth 
  6. then arrange the filling with a light pressure and then cover with the rest of the leaves as for the first layer 
  7. then cut into lozenges and plant half almonds (or whole, one pinion) in each diamond, bake in oven preheated to 175 ° for about 45min-1h depending on the size of the tray, it is necessary to monitor and adapt according to the power of the oven and the thickness of the baklawa, 
  8. As soon as the top is golden brown, remove the tray from the oven and gently brush with honey syrup before putting it back in the oven, ideally let the baklawa rest for 12 hours before cutting it 



![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-aux-noix-nobles1_thumb11.jpg)
