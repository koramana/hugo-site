---
title: Shortbread cookies
date: '2012-11-19'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/gateau-sec-045.jpg
---
##  Shortbread cookies 

Hello everybody, 

Here are shortbread cookies that the children loved, because everyone had his own name, on his biscuit. hihihihi, nobody touches the name of the other, otherwise it will be revenge. 

waiting to find a good recipe for **small butters** , I had fun using this cookie cutter, to realize these [ delicious cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) these [ shortbread biscuits ](<https://www.amourdecuisine.fr/categorie-12344749.html>) . a success all the time. 

and I'm going to be frank, I'm sick (it's been three days in bed because of the flu) I just hid these few pieces to take pictures, because the children and my husband did not could not resist this **melted shortbread** . 

you can find on this blog, a wide choice of [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) , a nice list of [ Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) .... so make a jump. 

Ingredients: 

  * 220 g of butter, at room temperature 
  * 100 g icing sugar 
  * 2 egg yolks 
  * 300 g of flour 
  * 50 g cornflour 
  * 1 vanilla extract 
  * pinch of salt 



method of preparation: 

  1. Place the butter in a bowl and whip it to make it soft. 
  2. Add the sugar, at once and whip again to have a nice sweet cream. 
  3. Add egg yolks, vanilla extract, and salt, whip again. 
  4. incorporate the cornflour delicately 
  5. then pick up the dough with the flour, to have a nice smooth dough. 
  6. shape your cupcakes, according to the cookie cutter of your choice 
  7. bake in the preheated oven at 170 degrees for 15 to 20 minutes (do not forget the second tray as I bake, hihihihih) 



click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

thank you.   


**Shortbread cookies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/gateau-sec-045.jpg)

**Ingredients**

  * 220 g of butter, at room temperature 
  * 100 g icing sugar 
  * 2 egg yolks 
  * 300 g of flour 
  * 50 g cornflour 
  * 1 vanilla extract 
  * pinch of salt 



**Realization steps**

  1. Place the butter in a bowl and whip it to make it soft. 
  2. Add the sugar, at once and whip again to have a nice sweet cream. 
  3. Add egg yolks, vanilla extract, and salt, whip again. 
  4. incorporate the cornflour delicately 
  5. then pick up the dough with the flour, to have a nice smooth dough. 
  6. shape your cupcakes, according to the cookie cutter of your choice 
  7. bake in the preheated oven at 170 degrees for 15 to 20 minutes (do not forget the second tray as I bake, hihihihih) 


