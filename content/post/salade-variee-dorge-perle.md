---
title: Varied salad of pearl barley
date: '2015-12-11'
categories:
- salads, salty verrines
tags:
- Medley
- Summer
- inputs
- Fast Food
- Easy cooking
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Salade-dorge-perl%C3%A9-vari%C3%A9.jpg
---
[ ![Mixed pearl barley salad](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Salade-dorge-perl%C3%A9-vari%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/Salade-dorge-perl%C3%A9-vari%C3%A9.jpg>)

##  Varied salad of pearl barley 

Hello everybody, 

Here is the salad that my husband and children ask me most often, sometimes it's twice a week. I admit that I made them this Ebly-based salad, that I brought with me from Algeria, but they like a lot, that my stock has arrived at zero. 

I bought a packet of pearl barley last time, and I made them the salad with, and frankly, they saw no difference, ohhhhé !!! 

**Varied salad of pearl barley**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-dorge-perl%C3%A9-vari%C3%A9e.jpg)

**Ingredients**

  * 250 g of pearl barley 
  * 1 tablespoon of mustard 
  * 1 tablespoon of mayonnaise (you can adjust the quantity according to your taste) 
  * sunflower oil 
  * lemon juice 
  * salt (gently with salt) 
  * pickles 
  * but in box 
  * green olives 
  * black olives 
  * tomatoes for salad 
  * [ pickled carrots ](<https://www.amourdecuisine.fr/article-legumes-marines-recettes-ramadan-2015.html>)



**Realization steps**

  1. boil pearl barley in salted water for 10 to 15 minutes until it is tender 
  2. drain and let cool 
  3. in a salad bowl, put cooked barley, olives, maize, pickled gherkin, cut tomato and diced carrots.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-vari%C3%A9e-a-lorge-perl%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-vari%C3%A9e-a-lorge-perl%C3%A9.jpg>)
  4. mix together the mayonnaise, mustard, lemon juice, oil, salt, and mix to have the perfect taste for you. 
  5. Pour over your barley mixture, and mix. 
  6. This salad is very rich, it can replace a meal, my children love to eat this salad, with chicken supremes or roast chops. 


