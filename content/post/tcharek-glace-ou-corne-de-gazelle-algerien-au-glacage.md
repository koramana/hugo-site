---
title: Frozen Tcharek, or Algerian gazelle horn at the glaze
date: '2017-06-18'
categories:
- Algerian cakes- oriental- modern- fete aid
tags:
- Ramadan
- Algerian cakes
- Algerian patisserie
- Oriental pastry
- Algeria
- Ramadan 2017
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/tcharek-glace-gateaux-de-laid-2014-.CR2_.jpg
---
[ ![Frozen Tcharek, or Algerian gazelle horn at the glaze](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/tcharek-glace-gateaux-de-laid-2014-.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/tcharek-glace-gateaux-de-laid-2014-.CR2_.jpg>)

##  Frozen Tcharek, or Algerian gazelle horn at the glaze 

Hello everybody, 

Generally, I never present my table Aid without the presence of: Soltan El Meîda, Frozen Tcharek or gazelle horn ice-cold, simply, because my daughter likes either the ice chafed or the [ arayeches galcées ](<https://www.amourdecuisine.fr/article-arayeche-ou-larayeche.html>) it is a must for her, besides everyone has his favorite cake, when I eat all the cakes without exception, and my little fault, if I am invited to someone and he presents me a table of cakes well stocked, well, I have to taste each cake .... hihihihi 

finally we come back to this cake, and I share with you some photos that are mine and others that are my friend Lunetoiles. 

For more [ Algerian cakes ](<https://www.amourdecuisine.fr/article-gateau-algerien-et-gateaux-orientals-pour-laid-el-fitr-2014.html>) , visit this article 

**Frozen Tcharek, or Algerian gazelle horn at the glaze**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/tcharek-glacee-aid-el-fitr-2013.CR2_.jpg)

portions:  30-35  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** Dough : 

  * 3 measure of flour 
  * 1 measure of melted smen 
  * 1 pinch of fine salt 
  * 2 tbsp. tablespoon icing sugar 
  * 1 C. dessert vanilla extract 
  * 1 teaspoon of orange blossom water 
  * Water needed 

Prank call : 
  * 2 measures of almonds 
  * 1 measure icing sugar 
  * 1 C. soup of smen 
  * Orange tree Flower water 

Frosting: 
  * 1 glass of syrup water (prepared with half a liter of water, 250 gr of crystallized sugar) 
  * 3 egg whites 
  * 2 tbsp. lemon juice 
  * 2 tbsp. tablespoon of orange blossom water 
  * shiny silver food 
  * sifted icing sugar 



**Realization steps**

  1. Sift flour with icing sugar, vanilla extract and salt. 
  2. Make a fountain.and add the smen melted, and cooled. Rub with the palm of your hand. Wet with orange blossom water and the necessary water by kneading well until you get a soft and manageable dough. 
  3. Divide into small balls of 25 to 30 gr, place in a tray, cover with foil and set aside. 
  4. Meanwhile, prepare the stuffing, with the ingredients given. Shape small sausages 8 cm long and half a cm in diameter. Place in a dish and cover with food paper. 
  5. On a floured work surface, lower the balls of dough into a slightly oval shape. 11 cm long. Place the stuffing pudding, on the edge of the center, roll while pressing lightly while giving the shape of a charek. 
  6. Place the cakes in an ungreased dish and bake in a preheated oven at 150 ° C for 25 to 30 minutes. They must have a golden color. 
  7. Let cool; 
  8. Meanwhile, prepare the icing, in a container, mix the syrup, the egg whites, the juice of a filtered lemon and orange blossom water. 
  9. Add the sieved icing sugar, with a wooden spoon, to a small amount, stirring until a thick dough is obtained. 
  10. Ice cake. Allow to dry completely, then pass the shiny silver food over the entire surface. 
  11. Take the cake, decorate with icing put in a pastry bag, or a (freezing bag perforated in a corner) and decorate by zigzagging. 



thanks to all of you, and thanks to you. 

if you want to share with me your recipes, send them to me at the email address at the top of the blog. 

saha ftourkoum 
