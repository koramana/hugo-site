---
title: spinach pie and nigella lawson feta
date: '2016-10-23'
categories:
- pizzas / quiches / tartes salees et sandwichs
tags:
- accompaniment
- inputs
- Amuse bouche
- Aperitif Aperitif
- Algeria
- Ramadan
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tarte-aux-%C3%A9pinard-et-f%C3%A9ta-de-nigella-lawson-698x1024.jpg
---
![tart-with-spinach-and-feta-of-nigella lawson,](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tarte-aux-%C3%A9pinard-et-f%C3%A9ta-de-nigella-lawson-698x1024.jpg)

##  spinach pie and nigella lawson feta 

Hello everybody, 

I woke up this morning on this spinach pie recipe and nigella lawson feta that Lunetoiles realized and liked to publish on my blog. 

And as we leave tomorrow on vacation and I have done nothing since the work in my kitchen I immediately sign the pictures of this spinach pie and feta nigella lawson Lunetoiles sent me to share with you. 

**spinach pie and nigella lawson feta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tarte-aux-%C3%A9pinard-et-f%C3%A9ta-de-nigella-lawson-1.jpg)

portions:  6  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 1 roll of broken dough 
  * 1 onion, finely chopped 
  * 250 g fresh or frozen spinach (slightly thawed) 
  * Olive oil 
  * 150 g of feta cheese 
  * 2 large eggs 
  * 200 g cottage cheese or cheese 
  * 2 tablespoons grated parmesan cheese + 2 tablespoons grated Parmesan (for garnish) 
  * 1 teaspoon dried oregano or chopped fresh mint 
  * Pepper 
  * Pine nuts (optional) 



**Realization steps**

  1. Preheat the oven to 200 ° C. 
  2. Place the broken dough roll in the buttered pie plate. 
  3. Lay a sheet of parchment paper over it. 
  4. Fill with dry vegetables and cook for 15 minutes. 
  5. Remove the mold from the oven. Remove the pulses and the parchment paper. 

Prepare the stuffing: 
  1. Brown the onions in the olive oil. 
  2. Add the spinach slightly thawed (if frozen) and lower the heat. 
  3. Let cook gently for 5 minutes. 
  4. Drain in a colander pressing with a spoonful of wood, to extract the maximum amount of water. 
  5. Combine feta cheese, eggs, cottage cheese or faisselle, 2 tablespoons Parmesan cheese and oregano or mint in a bowl. 
  6. Pepper ONLY. 
  7. Add the spinach / onions mixture drained and cooled. Mix. 
  8. Fill the bottom of the mixture. 
  9. Bake 30 minutes. 
  10. Sprinkle with grated parmesan at the end of the oven or a few pine nuts. 



![tart-with-spinach-and-feta-of-nigella lawson-2-](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tarte-aux-%C3%A9pinard-et-f%C3%A9ta-de-nigella-lawson-2-736x1024.jpg)
