---
title: Mixed potato salad
date: '2015-02-24'
categories:
- appetizer, tapas, appetizer
- salades, verrines salees
tags:
- inputs
- Amuse bouche
- Cocktail dinner
- Vegetable salad
- Vegetables
- Olive
- eggs

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/salade-de-pomme-de-terre-compos%C3%A9e-1.CR2_.jpg
---
[ ![1.CR2 mixed potato salad](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/salade-de-pomme-de-terre-compos%C3%A9e-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-salade-de-pomme-de-terre-composee.html/salade-de-pomme-de-terre-composee-1-cr2>)

##  Mixed potato salad 

Hello everybody, 

In Algeria, who says table for guests, said a salad class in the middle of the table, it is very important. Rare that you are invited to someone's place and he presents you a green salad with olives only. So more your salad is composed and rich in color, the more it pleases the guests, hihihihi 

With the [ tajine el khoukh ](<https://www.amourdecuisine.fr/article-tajine-el-khoukh.html> "tajine el khoukh") of the past time, and between the [ chorba Frik ](<https://www.amourdecuisine.fr/article-chorba-frik.html> "chorba Frik") and [ tuna buns ](<https://www.amourdecuisine.fr/article-petits-pains-au-thon.html> "tuna buns") That's what I prepared as a salad for my husband's guests. I must admit that the photos I share with you date of Ramadan 2013, because I did not have the chance to take a picture of the salad I put on the table. There were only men, and I did not have the audacity to make the paparazzi before them, hihihihi 

But in any case, remains that the principle is the same. Side decoration, you are spoiled for choice. 

On this salad you see, well it was my son who made the decoration. I prepared the carrot salad in advance, the beetroot salad, and it was up to him to do the training. It's a pleasure to see him put by small spoons, small portions here and there, so that it does not fall, and also for it to be perfect ... I am very proud of him.   


**Mixed potato salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/salade-de-pomme-de-terre-compos%C3%A9e-1.CR2_.jpg)

portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 200 gr small potatoes cooked with skin in salt water 
  * 2 tablespoons of a [ mayonnaise ](<https://www.amourdecuisine.fr/article-mayonnaise-fait-maison-rapide-et-facile.html> "homemade mayonnaise fast and easy")
  * the juice of 1/2 squeezed lemon 
  * extra virgin olive oil according to taste 
  * Salt and pepper from the mill 
  * a box of tuna with oil 
  * 2 to 3 hard boiled eggs 
  * beet salad with vinaigrette 
  * carrot salad with vinaigrette 
  * some pickles 
  * canned corn 
  * washed green salad 
  * Cherry tomatoes 
  * green olives 



**Realization steps**

  1. remove the skin from the baked potato, and cut it into cubes 
  2. prepare a vinaigrette with mayonnaise, lemon juice, oil and season with salt and black pepper (not too much salt anyway) 
  3. mix the potato, tuna and boiled eggs in this sauce.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/salade-de-pomme-de-terre-compos%C3%A9e-2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40648>)
  4. and do the training according to your taste.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/salade-de-pomme-de-terre-composee-3.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40649>)



[ ![mixed potato salad.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/salade-de-pommes-de-terre-compos%C3%A9e.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-salade-de-pomme-de-terre-composee.html/salade-de-pommes-de-terre-composee-cr2>)
