---
title: Lahmacun the authentic recipe of Turkish pizza
date: '2017-10-17'
categories:
- diverse cuisine
- Cuisine by country
- pizzas / quiches / pies and sandwiches
tags:
- Pan pizza
- Algeria
- Amuse bouche
- Aperitif
- Sandwich
- Easy cooking
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/10/pizza-turque-lahmacun-recette-authentique-de-la-pizza-%C3%A0-la-viande-hach%C3%A9e.jpg
---
![Turkish pizza lahmacun authentic recipe of minced meat pizza](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/pizza-turque-lahmacun-recette-authentique-de-la-pizza-%C3%A0-la-viande-hach%C3%A9e.jpg)

Hello everybody, 

A well-loved recipe at home, Turkish pizza or lahmacun or minced meat pizza. Today, I share with you the real and authentic Turkish lahmacun pizza, as I learned from my neighbor and Turkish friend, whatever this version is me and my husband who loves it the most. my children prefer the [ quick turkish pizza ](<https://www.amourdecuisine.fr/article-lahmacun-pizza-turque.html>) , ie with a dough well raised, and soft. 

In any case, today I share with you the original recipe and video, I hope that with the video, you will succeed more easily this very rich entry (perso I consider this Turkish pizza, lahmacun come as a dish well rich, there is everything in it, starches, proteins, vegetables ...) that ask for more. 

**Lahmacun the authentic recipe of Turkish pizza**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/pizza-turque-lahmacun-recette-authentique-de-la-pizza-%C3%A0-la-viande-hach%C3%A9e-1.jpg)

**Ingredients** for 8 large Turkish or 12 medium pizza: for the pasta: 

  * 3 glasses of 250 ml of flour 
  * 2 tbsp. instant yeast instant 
  * 1 C. tablespoons sugar 
  * 1 C. salt 
  * lukewarm water to pick up the dough. 

for Turkish lahmacun pizza toppings: 
  * 300 gr of minced meat 
  * 1 green pepper, 1 red pepper and 1 orange pepper, finely chopped (otherwise 3 medium green peppers) 
  * 1 medium onion, chopped 
  * 2 chopped tomatoes 
  * ½ bunch of chopped parsley 
  * 1 C. tomato soup with tomato paste 
  * 80 ml of olive oil or sunflower oil 
  * salt, black pepper, cumin and paprika 



**Realization steps**

  1. place all the ingredients in a large bowl and add the water slowly while kneading, to have a very soft and malleable ball (I like it a bit sticky anyway) 
  2. pick up the dough into a ball and place it in a salad bowl, cover with a clean cloth and let it rise until it doubles in volume. 

for the stuffing: 
  1. start finely chopping the ingredients separately using a robot 
  2. place the minced meat in a large salad bowl and add the rest of the ingredients, mix everything well to have a homogeneous mixture. Keep cool 
  3. Once the dough is lifted, degas it on the well-floured work surface and divide it into 8 large balls or 12 small ones (this will depend on the size of your stove). cover and let rest. 
  4. spread each ball finely over a floured space, place on clean cloth, cover and let stand at least 5 minutes. 
  5. take each cake, spread on the stuffing with a spoon, adjust the surface well. 
  6. place to cook on a pancake pan or a large pan and covering with a lid so that the stuffing cooks well. 
  7. remove from the heat when the pizza is golden brown underneath. enjoy it hot! 



![Lahmacun the authentic recipe of Turkish pizza](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/pizza-turque-lahmacun_.jpg)
