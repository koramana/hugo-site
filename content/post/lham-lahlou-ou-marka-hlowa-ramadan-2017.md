---
title: lham lahlou or marka hlowa ramadan 2017
date: '2012-07-19'
categories:
- amuse bouche, tapas, mise en bouche
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg
---
[ ![tajine hlou lham lahlou](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg>)

##  lham lahlou or marka hlowa ramadan 2017 

Hello everybody, 

It is Ramadan for the year 2017, I wish this holy month is full of peace and serenity, may Allah accept our youth, strengthen our faith, and guide us to the right path.   
قال رسول الله محمد (ص): ايها الناس! انه قد اقبل اليكم شهر الله بالبركة و الرحمة و المغفرة 

here is a dish of the [ Algerian cuisine, ](<https://www.amourdecuisine.fr/categorie-12359239.html>) who never leaves the first day of the month of Ramadan, there are even people who like it to be always on their table. a very good sweet salty recipe, called Tajine hlou, or marka hlowa, or more easily: [ Lamb tajine with prunes, almonds and apricots ](<https://www.amourdecuisine.fr/article-tajine-agneau-pruneaux-amandes-abricots-97616823.html>) . 

in any case, at home, I can say that I'm the only one who will eat it, not serious, because this dish can be well preserved in the refrigerator. 

you can see more than [ ramadan recipe ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>)

and here is my little recipe, as my mother always did   


**lham lahlou or marka hlowa ramadan 2017**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg)

portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * mutton 
  * dried prunes 
  * dried apricots 
  * some dried raisins 
  * ½ onion 
  * some butter or smen (clarified butter) 
  * 3 tablespoons of crystallized sugar 
  * 2 tablespoons orange blossom water 
  * 1 pinch of cinnamon powder or 1 stick of cinnamon 
  * 1 little saffron or dye 
  * 1 pinch of salt 



**Realization steps**

  1. arrange in a fact all the cut meat, butter or smen, chopped onion, dye, cinnamon and salt. 
  2. cover with about ½ liter of water and cook, until the meat becomes tender 
  3. in the meantime, put the prunes and apricots in the steam, being careful not to mix them, to keep the beautiful color of each of them. 
  4. cheat the raisins in orange blossom water, so that it swells well. 
  5. towards the end of cooking meat place in punches, apricots and raisins. 
  6. Add sugar and orange blossom water, cook for just a few minutes and remove from heat. 
  7. at the moment of serving, you can garnish your dish with some blanched and grilled almonds. 



[ ![lham lahlou or marka hlowa ramadan 2017](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg>)

good tasting . 

for another version I recommend the delicious recipe: 

[ chbah essafra ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-chbah-essafra-102664956.html>)

for a tajine hlou recipe without meat: 

![Tajin-hlou-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajin-hlou-11.jpg) [ TAJINE HLOU AUX POMMES ET ANIS ÉTOILÉ, SANS VIANDE ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html> "tajine hlou with apples and star anise, without meat")
