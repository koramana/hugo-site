---
title: Mango and orange smoothie
date: '2015-11-24'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-orange-mangue-2.jpg
---
[ ![orange mango smoothie 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-orange-mangue-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-orange-mangue-2.jpg>)

##  Mango and orange smoothie 

Hello everybody, 

It's not because it's cold, you have to go without fruit, homemade juices, or smoothies. Yes for some to take a bowl of hot milk coffee is the morning comfort. For me to take a smoothie full of colors, and rich in taste is my morning comfort, and my booster of the day. 

To be honest, I'm not the style to chew in an apple all day long, nor to eat a whole fruit after my meals .... So, I find my refuge in my smoothies, I know that I take full of my fruits, and all they can contain in one drink. I move with my big glass from place to place, or I come back to my bed if the children are still asleep, I watch TV until it's time for them to wake up. 

This time, I enjoy a delicious smoothie made with mango and orange ... A treat. For my part I used in my recipe juice extracted from two oranges, you can use the juice box. 

**Mango and orange smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-mangue-et-orange-1.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * two orange juice 
  * 1 mango 
  * 1 banana 



**Realization steps**

  1. place the ingredients in the blinder bowl and mix   
for more freshness, peel and cut the banana into pieces, and in the fridge for at least 1 hour, I put my fruit at night to the freezer. 
  2. enjoy it right away. 



[ ![orange mango smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-orange-mangue.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-orange-mangue.jpg>)
