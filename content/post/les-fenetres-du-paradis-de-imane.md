---
title: the windows of Imane's paradise
date: '2009-03-19'
categories:
- dessert, crumbles and bars
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2-001.jpg
---
![chebbak el jenna, donuts with donuts](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2-001.jpg)

yesiiiiiiiiiiiii, since the mold is there, I take this opportunity to make this cake, that my little rayan likes a lot. 

I confess that this cake, bad to be easy to prepare, but a little difficult to handle, 

if you are impatient, or you have too many people rode around you, do not do it because ... .. 

finally I do not hesitate to give you the recipe: 

**_ingredients_ ** : 

  * 50g flour 
  * 50g cornflour 
  * 4 eggs 
  * 1 pinch of salt (optional) 



Beat the eggs, add the cornflour and continue beating, add the flour while beating 

![S7302037](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/211142261.jpg)

put oil in a small saucepan, the best and cook small amount 

put your mold there to warm up 

![S7302036](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/211147481.jpg)

take the mold and put it directly into the preparation to cover the edges with the dough, it should not cover the top 

Put it in oil while holding the mold, leave a little until it begins to brown and slide the cake with a fork, brown both sides. 

![S7302041](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/211147781.jpg)

At the end of cooking, dip your cakes in preheated honey to which you have added some orange blossom. 

![S7302046](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/211148211.jpg)

yes it's beautiful and pretty, but it's not that it 

there are the failures: 

yes, so if you try and miss them, you're not the only one 

sometimes, when I put the dough into the dough, the dough fell and I had to prick it with a fork because it was half-cooked. 

when I say fall back, that is to say, as soon as I put back my dough mold, the dough that had to stick to it fell back into the normal dough, 

failures. 

do not take this from the wrong side, it's a very delicious cake. 

go another beautiful photo to forget the failure: 

![donuts with donut iron](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet-2.CR2_.jpg)
