---
title: makrout from Tlemcen
date: '2015-03-19'
categories:
- gateaux algeriens au miel
- fried Algerian cakes
- Algerian cakes- oriental- modern- fete aid
tags:
- Oriental pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-de-tlemcen-aux-oeufs-1.jpg
---
[ ![makrout Tlemcen](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-de-tlemcen-aux-oeufs-1.jpg) ](<https://www.amourdecuisine.fr/article-makrout-de-tlemcen.html/makrout-de-tlemcen-aux-oeufs-1>)

##  makrout from Tlemcen 

Hello everybody, 

I still have not had the chance to make this recipe, despite that I tasted this makrout Tlemcen just recently in the Sbou3 that we had organized for a friend. One of the Algerians realized this delight, and I confess that the makrout texture was a little different, I knew after it was the egg makrout, verdict: too good, and super fondant. 

Regarding the recipe for this makrout Tlemcen, it's a recipe that one of my friends who liked to share it with us, My friend **Hafred Kitchen** . My friend has herself taken the recipe Lyly ratatouille, and just like me, she agrees that this makrout is just to fall, I think the result is super well done.   


**makrout from Tlemcen**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-de-tlemcen-aux-oeufs-2.jpg)

portions:  30  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 500 gr medium grilled semolina 
  * 125 gr of powdered sugar 
  * 50 gr of melted butter 
  * 125 ml of oil 
  * ½ bowl of ground almonds 
  * 3 or 4 eggs 
  * 1 package of vanilla 
  * Lemon zest 
  * Water of orange blossoms 



**Realization steps**

  1. Roast the semolina in the pan, let cool, 
  2. in a salad bowl, place the grilled semolina, add the sugar, vanilla, ground almonds, lemon zest, melted butter, oil and 2 tablespoons orange blossom water, mix well. 
  3. add the eggs one by one, until you have a paste that picks up (3 in this recipe) 
  4. make small pieces in the shape of eggs, then fry them in a hot oil bath 
  5. after cooking, pass the mkrouts in the honey 



![makrout Tlemcen](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-tlemcen-aux-oeuf.jpg)
