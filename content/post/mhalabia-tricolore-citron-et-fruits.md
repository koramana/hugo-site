---
title: mhalabia - tricolor lemon and fruits
date: '2011-08-18'
categories:
- salades, verrines salees

---
Hello everybody, 

my sister-in-law is fond of delicacies, and for those Ramadan evenings, she prepares these delicious sweet, tricolor, sweetened jars. 

she does not have special verrines, she has to use water glasses, yes, each one has her own means. 

nevertheless, the paparazzi who was me, hihihihi did not miss the opportunity to take a picture of them. 

so I share with you this delicious recipe, my advice, do not use glasses of 220 ml, because although it was too good, but we can not reach the end of his glass, hihihihih, yes, we do not abuse ok, do not say: "I missed my diet because I followed the recipe of Soulef to the letter" hihihihihi 

so we go to the recipe: 

  * seasonal fruit cuts in. 
  * sugar according to taste 
  * Orange tree Flower water 
  * some crushed cookies 



first layer: 

  * 250 ml of milk 
  * 1 tablespoon and a half of sugar (or according to your taste) 
  * 1 tablespoon cornflower soup 
  * 1 cup of vanilla coffee 



second layer: 

  * 250 ml of milk 
  * 2 tablespoons sugar 
  * 1 tablespoon cornflower soup 
  * 1 tablespoon of bitter cocoa 



3rd layer: 

  * 250 ml of pressed lemon juice 
  * 3 tablespoons of sugar 
  * 1 tablespoon cornflower soup 



method of preparation: 

  1. wash and cut the fruits 
  2. add sugar and flower water 
  3. reserve aside 
  4. put in your verrines the equivalent of one tablespoon of this mixture, and reserve 



first layer preparation: 

  1. heat the milk with the sugar, 
  2. dilute the cornflour with a little milk, 
  3. incorporate it into the milk sounds stop stirring with a wooden spoon, over low heat 
  4. add the vanilla, continue stirring until it becomes thick 
  5. let cool and fill your verrines equitably 
  6. garnish with a layer of fruit, and crushed cookies 



do the same thing with the second layer but adding cocoa this time 

and decorate again with fruits and biscuits 

for the 3rd layer 

  1. mix the sugar and the squeezed lemon juice, if necessary add sugar 
  2. dilute the cornflour in a little water, and add it to the juice a little lukewarm 
  3. put back on the heat and stir constantly until it becomes thick 
  4. fill your verrines 
  5. decorate with fruit and reserve in the fridge for at least 4 hours 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

bonne journée 
