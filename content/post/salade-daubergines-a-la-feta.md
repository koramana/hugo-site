---
title: eggplant salad with feta cheese
date: '2015-12-31'
categories:
- salads, salty verrines
tags:
- Mixed salad
- Winter salad
- Vinaigrette
- inputs
- New Year
- Holidays
- Aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-daubergine-au-feta-1.jpg
---
[ ![eggplant salad with feta 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-daubergine-au-feta-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-daubergine-au-feta-1.jpg>)

##  eggplant salad with feta cheese 

Hello everybody, 

I really like eggplants, and I like to eat this vegetable differently every time, moreover on my blog, it is not the eggplant recipes that are missing, hihihihi. 

This time it's a salad with eggplant, a salad quite different from [ baba ghanouj ](<https://www.amourdecuisine.fr/article-baba-ghanouj.html>) that I like crazy, sometimes I do for almost 4 kilos of eggplants, and I reserve it in the fridge in a hermetic box, I eat a whole week ... 

this time it's thanks to Lunetoiles that I came across this recipe. Originally for this recipe we must fry the slices of tomatoes and aubergines, but I went to the light version, that is to say bake the two ingredients in the oven with a small trickle of olive oil . 

[ ![eggplant salad with feta 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-daubergines-a-la-feta-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-daubergines-a-la-feta-2.jpg>)   


**eggplant salad with feta cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-daubergines-au-feta.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 1 eggplant 
  * 3 to 4 medium tomatoes 
  * feta according to taste 
  * 2 cloves garlic 
  * chopped parsley 
  * salt 
  * lemon 
  * olive oil 



**Realization steps**

  1. cut the aubergine washed in slices, salt and leave to disgorge its water 
  2. wipe the eggplants with paper towels, place them in a small bowl, add 1 tablespoon oil and try to impregnate each puck. 
  3. place them in a baking tin lined with aluminum foil 
  4. grill in the oven, and turn to grill on the other side. 
  5. cut the tomatoes into slices, salt and add the olive oil, and grill it like the aubergines. 
  6. chop the garlic finely, add a little lemon juice and a little olive oil 
  7. garnish your serving dish with aubergine and tomato slices 
  8. crush the feta over it. garnish with the chopped parsley, and add the little vinaigrette on top .... this salad comes beautifully with a good [ baked matloue ](<https://www.amourdecuisine.fr/article-khobz-matloua-fel-koucha-matlou3-au-four.html>)



[ ![eggplant salad with feta 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-daubergines-a-la-feta-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-daubergines-a-la-feta-3.jpg>)
