---
title: Salmon with honey and grilled sesame
date: '2013-10-23'
categories:
- bakery
- crepes, donuts, donuts, sweet waffles
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/saumon-au-sesame-024_thumb.jpg
---
Hello everybody, 

Here is a quick and easy recipe for the "loveurs" of salty sweet dishes, and especially of this delicious fish rich in unsaturated fatty acid (omega 3), and other salmon. melting inside and crunchy with this beautiful layer of grilled sesame that covers it, present with a thin layer of fried zucchini, this recipe is a "must" to accompany even a simple salad. 

**Salmon with honey and grilled sesame**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/saumon-au-sesame-024_thumb.jpg)

Recipe type:  dish, entree  portions:  2  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 2 salmon pavers 
  * grilled sesame 
  * 3 tablespoons of honey 
  * the juice of half a lime 
  * 1 zucchini 
  * Olive oil 
  * salt and black pepper 



**Realization steps**

  1. Wash and clean the zucchini, cut into slices, season with a little salt and black pepper. 
  2. fry them in a bottom of olive oil, and place on a paper towel to reduce the fat of cooking 
  3. dirty and pepper the salmon pavé. 
  4. mix the honey with the lemon, and grill the sesame seeds if they are not already grilled. 
  5. dip the salmon pieces in the honey and lemon mixture, then in the sesame seeds to cover them well. 
  6. in a frying pan, put just the equivalent of 3 to 4 tablespoons of olive oil, and cook the salmon over medium heat on both sides. 
  7. introduce, your salmon by garnishing them with slices of fried zucchini. 



and for others [ salmon recipes ](<https://www.amourdecuisine.fr/article-que-faire-avec-du-saumon-100745656.html>) , I share with you this article, which will surely interest you, 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
