---
title: tomato soup, perfect and velvety
date: '2017-12-19'
categories:
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/soupe-de-tomate-parfaite-et-velout%C3%A9e-690x1024.jpg
---
![tomato soup, perfect and velvety](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/soupe-de-tomate-parfaite-et-velout%C3%A9e-690x1024.jpg)

##  tomato soup, perfect and velvety 

Hello everybody, 

I always thought that the tomato soup would be acid, and that we would stop at the second spoon, but when I was in Turkey, I had eaten at the restaurant, and I had asked for the recipe at Chef ... He did not give me everything, but he told me that it contains dried tomatoes ... 

Last time, I was watching TV, when I saw a chef prepare this recipe, I did not remember all the ingredients because I arrived at the last moment, but I tried to do it, following his method, and the result, a tomato soup, well velvety, not acid as it is believed, unctuous and delicious wish. 

The only comment from my husband: "oh if you had put shrimp in shrimp," I do not know why? but even me, I thought about it !!! 

{{< youtube 3i_giB9 >}} 

**tomato soup, perfect and velvety**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-de-tomates-006_thumb11.jpg)

portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 1 tablespoon olive oil. 
  * 2 cloves garlic minced 
  * 1 chopped onion 
  * 1 tablespoon of tomato paste 
  * 1 can of diced tomatoes, drained, otherwise 500 gr of fresh tomatoes without seeds 
  * 6 Dried tomatoes (mine in the oil) 
  * 1 cube of chicken broth or vegetable broth 
  * 1.5 to 2 glasses of water 
  * Fresh cream (optional, for decoration, I did not have any) 
  * Chopped basil (optional, for decoration, I did not have any) - Some leaves 



**Realization steps**

  1. Heat the oil in a saucepan over medium heat. 
  2. add the minced garlic to the hot oil. Once you can smell the garlic, add the onions with a pinch of salt and the tomato paste. Brown for 3 minutes. 
  3. Add tomatoes, dried tomatoes, and bouillon cube 
  4. Cover with water and bring to a boil and simmer covered for 10 minutes. 
  5. Let cool, then pass all to the blinder or mix with the mixer foot mashed. 
  6. season to taste with salt and black pepper if you want 
  7. garnish with chopped basil and cream if you use. 



[ ![Soup-of-tomato-022_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-de-tomates-022_thumb11.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-de-tomates-022_thumb11.jpg>)
