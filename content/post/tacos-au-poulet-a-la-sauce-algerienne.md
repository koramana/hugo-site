---
title: chicken tacos with Algerian sauce
date: '2018-03-16'
categories:
- bakery
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- salty recipes
tags:
- tortillas
- Cheese
- Ramadan
- Sandwich
- Algeria
- Fast food
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/03/tacos-au-poulet-%C3%A0-la-sauce-alg%C3%A9rienne-1.jpg
---
![chicken tacos with Algerian sauce 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/tacos-au-poulet-%C3%A0-la-sauce-alg%C3%A9rienne-1.jpg)

##  chicken tacos with Algerian sauce 

Hello everybody, 

Among the most popular sandwiches at home, chicken tacos go to the first place, especially when I season them with homemade Algerian sauce, I do not tell you! 

It is true that chicken tacos with Algerian sauce are a delight, the only "snag" and always have [ tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine-recette-en-video.html>) at home, otherwise the recipe becomes a bit long. But the result is worth it. 

I'm not going to tell you that I only have one recipe for chicken tacos, I make my chicken tacos, I put in everything I love, and especially what is available at home . And it's always better than the subsequent recipe, yes when it's good, it is! 

**chicken tacos with Algerian sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/tacos-au-poulet-%C3%A0-la-sauce-alg%C3%A9rienne-2.jpg)

**Ingredients** Prepare the chicken: 

  * 2 chicken breasts 
  * salt, black pepper, cumin, and fajitas spices (if not spices to your taste) 
  * 2 cloves garlic 
  * olive oil 

Algerian sauce: 
  * mayonnaise 
  * Harissa 
  * chopped onion 
  * freshly squeezed lemon fillet 
  * cumin 

to garnish the tacos: 
  * of the [ tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine-recette-en-video.html>)
  * sauteed chicken (ingredients at the top) 
  * Algerian sauce 
  * lawyer (optional) 
  * Red onion 
  * grated shaping (edam for me) 



**Realization steps**

  1. start by frying the chicken with the listed ingredients in a bottom of oil. 
  2. cook until the chicken takes a nice color. 
  3. mix the ingredients of the Algerian sauce, to have a nice sauce, taste to adjust the taste. 
  4. garnish the tortillas with a little Algerian sauce 
  5. lay a layer of chicken 
  6. add the sliced ​​avocado, the sliced ​​onion, a little more sauce 
  7. and garnish with a nice handful of cheese. 
  8. fold the tortillas to enclose the contents, and cook in a panini press, or in a pan on both sides. 
  9. enjoy it as long as it's hot! 



![chicken tacos with Algerian sauce-2](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/tacos-au-poulet-%C3%A0-la-sauce-alg%C3%A9rienne-2-1.jpg)
