---
title: chestnut soup
date: '2016-11-05'
categories:
- soupes et veloutes
tags:
- Healthy cuisine
- creams
- Velvety
- Vegetables
- Easy cooking
- Fast Food
- Brown

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/soupe-de-chataigne-1.jpg
---
![chestnut soup](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/soupe-de-chataigne-1.jpg)

##  chestnut soup 

Hello everybody, 

Chestnut soup is my soup of autumn, a comforting soup that I like to concoct when it's cold, and especially to taste the chestnuts otherwise. 

For this chestnut soup, I did not use fresh chestnuts, because unfortunately I can not find fresh chestnuts, or chestnuts in shell, why? 

![chestnut puree en](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/chataigne-en-puree.jpg) But I'm lucky to find at any time of the season this product in commerce: chestnut purée, often used for desserts and stuffing (stuffing chicken or turkey). 

Besides, it is often with this puree that I prepare my [ homemade chestnut cream ](<https://www.amourdecuisine.fr/article-recette-de-la-creme-de-marrons-maison-113097294.html>) . It must be said that this puree greatly reduces the preparation time of the chestnut cream. 

Same thing to prepare my chestnut soup in the blink of an eye my soup is ready to taste or to sip as I prefer. So if you do not have this mashed potatoes, well, get ready to wash, peel and cook your chestnuts, hihihihih.   


**chestnut soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/soupe-de-chataigne.jpg)

portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 1 medium onion 
  * 2 tbsp. tablespoon of olive oil 
  * 700g cooked chestnuts or chestnut purée 
  * 600 ml of [ chicken broth ](<https://www.amourdecuisine.fr/article-bouillon-de-poulet-fait-maison.html>)
  * 2 medium potatoes 
  * 300 to 400 of milk 
  * 100 ml whole liquid cream 
  * salt pepper 
  * whipped cream for serving 
  * chopped chives 
  * nutmeg 



**Realization steps**

  1. Cut the onion into pieces and sauté in the olive oil to heat in a large saucepan 
  2. Cook for 2-3 minutes, stirring then add the peeled potato, washed and cut into cubes 
  3. Add the chicken broth, salt and pepper as needed. 
  4. when the potato is well cooked, mix the mixture with the blender or blender foot. 
  5. Add the chestnut puree and mix again to have a good velvety. 
  6. add the milk according to the consistency you want. 
  7. add the liquid cream to give more consistency to your soup. Rectify the seasoning. 
  8. Present the soup with a little whipped cream, grated nutmeg and chopped chives. 

If you use the shelled chestnut: (the cooking time will be at least 30 minutes longer) 
  1. Cook the chestnuts without the shell in the chicken broth with the chopped onion. 
  2. halfway through cooking add the cubes of potato and milk. 
  3. at the end of the cooking, switch to the mixer adding liquid cream. 



![chestnut soup](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/soupe-de-chataigne-2.jpg)
