---
title: Half melting moons with sesame seeds
date: '2016-07-15'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/demi-lunes-fondantes-aux-grains-de-s%C3%A9sames.jpg
---
![half melting moons with sesame seeds](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/demi-lunes-fondantes-aux-grains-de-s%C3%A9sames.jpg)

##  Half melting moons with sesame seeds 

Hello everybody, 

I chose sesame seeds in all the states, I admit that I like sesame seeds, and that I try to make recipes with each time, that's why I do not hesitate a second to choose sesame seeds as the star of this round. 

And to participate, I chose the cake that I like the most, the [ ghribia ](<https://www.amourdecuisine.fr/article-ghribiya-ghribia-aux-noix-gateau-algerien.html>) to which I wanted to add a touch of sesame seeds, and not that ... I also added a little tahini, frankly this ghribia shaped half-moons is just a treat. So if you want to taste it, try the recipe and tell me your opinion.   


**Half melting moons with sesame seeds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/demi-lunes-fondantes-aux-grains-de-s%C3%A9sames-2.jpg)

portions:  40  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients**

  * 200 gr of smen (solid, unmelted) 
  * 150 gr of icing sugar 
  * 50 gr of tahini 
  * 80 gr of grilled sesame seeds 
  * flour to pick up the dough. 

Decoration: 
  * egg white 
  * Sesame seeds 



**Realization steps**

  1. whip the smen and sugar until you have a cream, it can last 20 minutes, be passionate, it is the secret of the success of a ghribia (I used the electric whip for this operation). 
  2. Introduce the tahini continue whipping. 
  3. add the sesame seeds, and collect the whole with flour, until you have a malleable paste. 
  4. spread the dough in a small amount on a lightly floured work surface and shape half moons 
  5. place the half moons in a baking tray, not buttered. 
  6. garnish with egg white and sesame seeds. 
  7. cook in a preheated oven at 160 degrees C, between 10 and 15 minutes. watch the cooking let cool before unmolding ghribia pieces. 



![half melting moons with sesame seeds 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/demi-lunes-fondantes-aux-grains-de-s%C3%A9sames-1.jpg)

et voila la liste des participantes: 
