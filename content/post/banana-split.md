---
title: Banana split
date: '2015-05-23'
categories:
- ice cream and sorbet
tags:
- Ramadan 2015
- ice cream
- Summer
- Easy recipe
- Algeria
- Without ice cream maker
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/banana-split-1.jpg
---
[ ![banana split 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/banana-split-1.jpg) ](<https://www.amourdecuisine.fr/article-banana-split.html/banana-split-1>)

##  Banana split 

Hello everybody, 

always with homemade recipes, refreshing recipes that will soothe and soften the hot days. A banana split you know? The **banana split** is a frozen dessert traditionally composed of a banana cut lengthwise, presented with three scoops of ice cream, all topped with a hot chocolate sauce and whipped cream. 

I often ate it to be frank when I arrived here in England, but I must say that I was super thin at the time, hihihih, and eat such a greed does not count too much in my eyes ... I had this luck at the time of eating as an 'Ogre' without taking a gram more by weight, it was great this beautiful time the ... .. 

Now I'm happy to do it for my children only, because he likes it a lot, but it's still a long time since I've done it. Especially since I could not tolerate giving them the ice creams of the trade, and that I did not have time with the baby, to make three kinds of ice cream, to be even more frank, I hate to make the ice cream made from egg creams, wait until it cools ... and blah, blah ... 

But this recipe from my friend **Oum Ali** , which is becoming popular recently, I like it, and that's why I share with you his banana split recipe and three balls of homemade ice cream 

**Banana split**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/banana-split.jpg)

portions:  4  Prep time:  45 mins  total:  45 mins 

**Ingredients**

  * banana according to the number of people 

Pistachio flavored ice cream: 
  * 100 gr of whipped cream (fixed whipped cream) 
  * 100 gr of milk 
  * 100 gr of pistachio powdered custard 

ice cream apricot taste: 
  * 100 gr of whipped cream (fixed whipped cream) 
  * 100 gr of milk 
  * 100 g of apricot flavored flan powder 

ice cream taste strawberry: 
  * 100 gr of whipped cream (fixed whipped cream) 
  * 100 gr of milk 
  * 100 gr of powdered custard flan. 

for decoration: 
  * whipped cream 
  * homemade caramel 



**Realization steps**

  1. Start by making the ice cream separately. 
  2. Add the whipped cream powder to the cold milk, and whip until you have peaks. 
  3. introduce the flan and whip again. 
  4. place in the freezer, and remove every 30 minutes to whip a little mixture, otherwise use an ice cream maker. 
  5. in a serving dish, place the banana peel, add three scoops of ice cream, 
  6. decorate with whipped cream and caramel. 



[ ![banana split 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/banana-split-2.jpg) ](<https://www.amourdecuisine.fr/article-banana-split.html/banana-split-2>)
