---
title: Dessert with caramelized figs
date: '2013-09-22'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/dessert-aux-figues-caramelisees.CR2_1.jpg
---
![dessert-with-figs-caramelisees.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/dessert-aux-figues-caramelisees.CR2_1.jpg)

Hello everybody, 

As it is always the season of the figs, I take full advantage to taste them either fresh, or in an improvised recipe, or a recipe that I find on a book, a blog, or on TV. 

**Dessert with caramelized figs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/dessert-aux-figues-noix-et-yaourt-grecque.CR2_2.jpg)

portions:  2  Prep time:  15 mins  cooking:  5 mins  total:  20 mins 

**Ingredients**

  * 2 figs 
  * About 4-6 nuts 
  * 2 tbsp. tablespoon maple syrup (30 ml) 
  * About 150 ml of Greek yogurt 



**Realization steps**

  1. Wash and cut the figs into slices. 
  2. Cut the nuts into thin dice and mix in maple syrup in a bowl. 
  3. Heat the skillet over medium heat and when hot, add the figs and maple-nut syrup mixture. 
  4. Mix with a wooden spatula without breaking the fruit. 
  5. Cook for 3-4 minutes or until figs start to caramelize. 
  6. Return the fruit regularly while cooking. 
  7. Pour the Greek yoghurt into the serving dish and garnish with caramelized figs 



You can already see, the video for the details of the recipe ... 

Et promis des que j’ai le temps, je vous redige le tout. 
