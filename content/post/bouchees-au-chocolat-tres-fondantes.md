---
title: chocolate bites, very melting
date: '2012-12-08'
categories:
- bakery
- Algerian cuisine
- Mina el forn
- bread, traditional bread, cake
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/bniouen-farci-078_thumb.jpg
---
[ ![stuffed bniouen 078](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/bniouen-farci-078_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/bniouen-farci-078.jpg>)

Hello everybody, 

here is a delicious [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , and [ without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) , so do not bother to say I'm lazy, you'll prepare in 30 min max, but do not blame me if towards the end you will find only 2 pieces in the tray, because it's too good. 

for my part I prepared them at night, and let it dry, in the morning to take pictures, I found only 4 pieces, with my 3 little monsters, who woke up, a little before me, and ben c ' was the cute catch for them ...... 

the recipe for you without delay knowing that the stuffing is what I had left of the [ Moorish almond paste with coconut ](<https://www.amourdecuisine.fr/article-mauresque-a-la-noix-de-coco-gateau-algerien-101325541.html>) , that I had been preparing just recently.   


**chocolate bites, very melting**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/bniouen-farci-075_thumb.jpg)

Recipe type:  Algerian cake  portions:  50  Prep time:  20 mins  cooking:  2 mins  total:  22 mins 

**Ingredients**

  * 2 boxes of biscuits 
  * softened butter 
  * honey according to taste 
  * 2 tablespoons orange marmalade (it gives a sublime taste) 
  * Turkish halwat, or sesame halva. 
  * cocoa (5 to 6 tablespoons, for cocoa lovers like me, otherwise go slowly) 
  * a box of white chocolate 
  * [ almond paste from the Moorish cake ](<https://www.amourdecuisine.fr/article-les-mauresques-kefta.html> "the Moorish kefta") (for the stuffing) 



**Realization steps**

  1. crush the cookies a little and place them in the bowl of the blinder 
  2. add the rest of the ingredients (except the almond paste) and mix until you have a nice homogeneous paste (if necessary add the butter and or honey, according to your taste) 
  3. take the almond paste, and form balls of 2 cm in diameter 
  4. take a quantity of the biscuit pate, put it in the hollow of your hand place the heart almond paste ball and enrobe. 
  5. keep going until exhaustion dough and stuffing.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2012-03-18-bniouen-farci_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2012-03-18-bniouen-farci_thumb.jpg>)
  6. melt the white chocolate in a bain-marie or in the microwave, and immerse the balls of bniouen, for my part I prefer to put the balls of bniouen on a grid and cover with the melted white chocolate. 
  7. let it dry a little, and if you want, you can decorate with a little melted dark chocolate, to give the effect of raillures. 



[ ![stuffed bniouen 069](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/bniouen-farci-069_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/bniouen-farci-069.jpg>)

follow me on: 

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
