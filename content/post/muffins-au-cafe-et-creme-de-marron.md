---
title: Coffee muffins and brown cream
date: '2009-06-30'
categories:
- jams and spreads

---
ingredients: for 6 to 8 

150 g flour   
1/2 teaspoon baking powder   
1 pinch of salt   
1 point of baking soda (optional)   
50 g of sugar   
120 ml of milk   
2-3 cases of freeze-dried coffee "Espresso" (+ or - according to your tastes)   
1 egg   
40 g melted butter   
8 cc convex cream of brown 

Preheat the oven to 180 ° c. 

In a bowl, mix with the fork the flour, sugar, yeast, salt and baking soda. In another bowl, dissolve the milk with the coffee, add the egg and melted butter. Beat lightly with a fork. 

Pour the liquid mixture over the dry mixture and mix with the spoon just enough to incorporate the flour (3-4 turns are enough). The dough must be lumpy. 

Using a spoon, pour a little dough in the molds add 1 cc of brown cream and cover with dough 3/4 of the boxes (or molds).   
Bake for 15-20 minutes (depending on size). Wait 5 minutes and unmold if the muffins are cooked in molds. Let cool on rack. 

merci a talons hauts et cacao pour ce délice. 
