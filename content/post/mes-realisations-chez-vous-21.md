---
title: my achievements at home 21
date: '2012-06-28'
categories:
- gateaux, et cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/1339092413551.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

as every time, I share with you the recipes from home, that you like to make at home, and that you like to share on this article. 

Thank you, for this trust you make in my kitchen, I am very happy every time when I receive your comments, after a great success. 

And if you have other recipes you have made from my kitchen, I invite you to send me the pictures on this email: 

[ vosessais@amourdecuisine.fr ](<mailto:contact@amourdecuisine.fr>)

![](https://www.amourdecuisine.fr//import/http://www.looklady.com/wp-content/uploads/2012/05/)

[ ![Blog of lesdelicesdecanstantine: the delights of Constantine, tomatoes stuffed for a June in red](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/1339092413551.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/1339092413551.jpg>)

[ tomato nest ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs-101484090.html>) in [ the delights of constantine ](<http://lesdelicesdecanstantine.cuisineblog.fr>)

![Loujayn's blog: cooking delight, stuffed tomato](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/Tomate-farcie1.jpg)

[ Zaalouk with eggplant ](<https://www.amourdecuisine.fr/article-zaalouk-d-aubergines-107254425.html>) at Lamia: 

Hello soulef, I was very tempted to test your Zalouka recipe, I find that his way of preparing it is very simple, original. so I made it for today at noon, its cooking is fast, the vegetables of the season and the taste I do not tell you hayell, the feeling that I had in the first bite is that it seems that the vegetables were grilled, in any case that's what I felt. Very delicious recipe thank you bcp with the scent of olive oil is a real delight. I sent you the photos in the section your tests. to + 

[ flower garden ](<https://www.amourdecuisine.fr/article-la-recette-du-jardin-fleuri-mon-gateau-d-anniversaire-46564330.html>) at Aichala 

[ Spiderman ](<https://www.amourdecuisine.fr/article-spiderman-le-gateau-d-anniversaire-de-rayan-81081843.html>) at Aichala 

[ genoise mascarpone and caramilized apple ](<https://www.amourdecuisine.fr/article-genoise-mascarpone-pommes-caramelisees-67729142.html>) at Aichala 

[ bavarian heart strawberries ](<https://www.amourdecuisine.fr/article-bavarois-coeur-fraise-98221705.html>) at Aichala 

[ bavarois pear chocolate ](<https://www.amourdecuisine.fr/article-bavarois-poire-vanille-58928958.html>) at aichala 

[ Bavarian pears and chocolate ](<https://www.amourdecuisine.fr/article-28471689.html>) at aichala 

genoise with butter cream at aichala 

[ sugar cake ](<https://www.amourdecuisine.fr/article-37650983.html>) at aichala. 

![](https://www.amourdecuisine.fr//import/http://2.bp.blogspot.com/-wDXWjmx1h6c/T99NuAmgTsI/AAAAAAAAAxg/OeZoRznxw7E/s1600/)

merci a vous tous et toutes, pour ces delices, et a vos prochaines realisation. 
