---
title: salty cake with vegetables for appetizers or base for appetizer, easy and fast
date: '2012-12-30'
categories:
- diverse cuisine
- Cuisine by country
- Dishes and salty recipes
- recipe for red meat (halal)

---
Hello everybody, 

a recipe that I like a lot, an easy entry to prepare and vary according to your tastes, just keep the basic recipe, and the ingredients in it, each one his desires 

for this time, I used grilled peppers, carrot steamed, camembert and olives. 

ingredients: 

  * 200 gr of flour 
  * 3 eggs 
  * 1 sachet of baking powder 
  * 100 ml of milk 
  * 100 ml of oil 
  * a pinch of salt 
  * a pinch of black pepper 
  * 1 carrot steamed 
  * 3 grilled peppers (one green one red and one yellow) 
  * 1 handful of pitted black olives 
  * 1 medium slice of camembert 



method of preparation: 

  1. clean the peppers, cut them into pieces, 
  2. cut the carrot cooked in cubes too 
  3. cut the olives into slices, and reserve 
  4. cut the camembert into 
  5. whisk the eggs, add the milk and oil 
  6. add salt and pepper 
  7. Mix the flour and yeast 
  8. add this mixture gently to the eggs 
  9. flour the mixture of vegetables, and olive in a colander and shake 
  10. incorporate it into the previous mix 
  11. pour into a cake mold butter or wrap with baking paper 
  12. place in a preheated oven at 180 degrees for almost 45 minutes 
  13. Check the cooking, then let cool a few minutes before unmolding. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
