---
title: chocolate mouskoutchou
date: '2014-11-02'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/04/cholate-cake-609_thumb.jpg
---
##  Chocolate mouskoutchou 

Hello everybody, 

My husband really likes the [ mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouscoutchou.html> "mouskoutchou / mouscoutchou") or [ mouskoutchou ](<https://www.amourdecuisine.fr/article-28557136.html> "muskoutchou, muskoutchou, muslin cake.") . This Algerian cake, can be present every day on its breakfast tray, and it will never have enough ... 

So for this time, I wanted to make a small change, and I realized this chocolate mukoutchou, a recipe that I found on Douda's blog. 

This chocolate version of the mouskoutchou is a real delight, and it is with great pleasure that I share with you the recipe. 

**chocolate mouscoutchou**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/cholate-cake-609_thumb.jpg)

Recipe type:  cake  portions:  8  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients**

  * 5 eggs 
  * 200 ml of milk 
  * 20 gr of roasted flaked almonds   
(optional) 
  * 200 gr of dark chocolate 
  * 300 gr of flour 
  * 200 gr of sugar 
  * 2 bags of yeast 
  * 1 knob of butter for the mold 
  * 3 c. oil soup 



**Realization steps**

  1. Preheat your oven to 180 ° (th6) 
  2. Melt the chocolate in the microwave by adding the butter hazelnut   
(or if like me you do not have any, to the pan with a touch of milk, personally I have incorporated the milk as and to the extent that the chocolate dissolves well and incorporates in the dough more easily) 
  3. In a bowl, beat the eggs 
  4. then add the sugar, the oil, the milk, the melted chocolate 
  5. while mixing, pour the yeast and the flour to obtain a homogeneous paste. 
  6. Pour the mixture into a buttered and floured mold   
(if antidepressant not necessary biensur !!) 31 cm in diameter (I used a mold-shaped crown). 
  7. Bake 30 to 35 minutes. The dough must double in volume. 
  8. After cooking garnish the still hot almond cake. 



  


Thank you for your visit 

Thank you for your feedback 

and thank you to register on my newsletter 

for a variety without chocolate: 

[ ![mouskoutchou-soft-mouskoutcha-meskouta-mouscoutch11](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mouskoutchou-moelleux-mouskoutcha-meskouta-mouscoutch11.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mouskoutchou-moelleux-mouskoutcha-meskouta-mouscoutch11.jpg>)

##  [ mouscoutchou,mouskoutchou, gâteau mousseline. ](<https://www.amourdecuisine.fr/article-28557136.html> "muskoutchou, muskoutchou, muslin cake.")
