---
title: index of Algerian dishes
date: '2012-04-01'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/hrira-oranaise-copie-12.jpg
---
hello everyone, I try here on this article to put all the Algerian dishes that I could realize, it is vast and different, it is the little that I realized, and I hope to be able to realize still more of our delicious Algerian dishes. & Nbsp; chorba bayda, white soup with vermicelli and chicken "شربة بيضاء chorba fric - Jari bel frik - Green wheat soup crisp HRIRA, SOUP WITH FRESH AND DRY VEGETABLES, CHICKEN CHORBA VERMICELLE, SOUP WITH VERMICELLA lham l Ahlou, sweet dish, tajine lahlou Kefta, kofta Hassen bacha, Pasha "كفته حسن باشا batata kbab, or potato & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.6  (  2  ratings)  0 

Hello everybody, 

I try here on this article to put all the Algerian dishes that I could realize, it is vast and different, it is the little that I realized, and I hope to be able to realize even more of our delicious dishes Algerians.   
  
<table>  
<tr>  
<td>



######  ![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_513.png)

######  [ chorba bayda, white soup with vermicelli and chicken «شربة بيضاء ](<https://www.amourdecuisine.fr/article-35553378.html>)


</td>  
<td>



######  ![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_1013.png)

######  [ chorba fric - Jari bel frik - Crisp green wheat soup ](<https://www.amourdecuisine.fr/article-25345575.html>)


</td>  
<td>

![hrira-Oran-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/hrira-oranaise-copie-12.jpg) ** [ HARIRA, FRESH, DRIED VEGETABLE SOUP WITH CHICKEN ](<https://www.amourdecuisine.fr/article-hrira-soupe-aux-legumes-frais-et-secs-au-poulet-65270080.html>) ** 
</td> </tr>  
<tr>  
<td>

![chorba vermicelli 007](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-vermicelle-007_thumb2.jpg) ** [ CHORBA VERMICELLE, SOUP WITH VERMICELLA ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-chorba-aux-vermicelles-103136782.html>) ** 
</td>  
<td>



######  ![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_1114.png)

######  [ lham l Ahlou, sweet dish, tajine lahlou ](<https://www.amourdecuisine.fr/article-35152846.html>)

###### 


</td>  
<td>



######  ![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb24.png)

######  [ Kefta, Hassan kofta bacha, Pasha "كفته حسن باشا  ](<https://www.amourdecuisine.fr/article-35713413.html>)


</td> </tr>  
<tr>  
<td>



######  ![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_414.png)

######  [ batata kbab, or baked potato, white sauce ](<https://www.amourdecuisine.fr/article-35393661.html>)


</td>  
<td>



######  ![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_614.png)

######  [ rice  ](<https://www.amourdecuisine.fr/article-25345566.html>) [ with typical Algerian chicken, Algerian dish  ](<https://www.amourdecuisine.fr/article-25345566.html>)


</td>  
<td>



######  ![picture](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/image_thumb_814.png)

######  [ chicken legs with baked vegetables «دجاج بالفرن  ](<https://www.amourdecuisine.fr/article-36629770.html>)


</td> </tr>  
<tr>  
<td>

![trebia8](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/trebia8_thumb3.jpg) **[ TREBIA OR TAJINE OF COURGETTES WITH EGGS ](<https://www.amourdecuisine.fr/article-53681316.html>) ** 
</td>  
<td>

![baked dolma 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/dolma-au-four-2_thumb.jpg) **  
[ DOLMA, VEGETABLES FILLED WITH OVEN-FRIED MEAT ](<https://www.amourdecuisine.fr/article-dolma-legumes-farcis-a-la-viande-hachee-au-four-66035809.html>) ** 
</td>  
<td>

![pea tagine with artichokes](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tajine-de-petits-pois-aux-artichauts_thumb2.jpg) [ pea tagine with artichokes ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-aux-artichauts---tajine-jelbana-103530292.html>) 
</td> </tr>  
<tr>  
<td>

![tajine-of-peas-and-chard-farcis.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tajine-de-petits-pois-et-cardons-farcis1.jpg) **   
[ TAJINE OF SMALL PEAS AND CARDONS FILLED WITH THE HATCHED MEAT ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee-94791294.html>) ** 
</td>  
<td>

![Tajin-hlou-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajin-hlou-11.jpg) **   
[ HOLY TAJINE WITH APPLES AND STAR ANISE, WITHOUT MEAT ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html>) ** 
</td>  
<td>

![Artichoke tajine stuffed with chopped meat and eggs](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tajine-d-artichauts-farcis-a-la-viande-hachee-et-oeufs_thum1.jpg)   
[ FARCIS ARTICHOKE TAJINE ](<https://www.amourdecuisine.fr/article-tajine-d-artichauts-farcis-97187541.html>) 
</td> </tr>  
<tr>  
<td>

![tajine zitoune](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tajine-zitoune3.jpg) **   
[ TADJINE ZITOUNE ](<https://www.amourdecuisine.fr/article-25345434.html>) ** 
</td>  
<td>

![chakhchoukha4](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chakhchoukha4_thumb2.jpg) **   
[ CHAKHCHOUKAT ED DFAR ](<https://www.amourdecuisine.fr/article-26090486.html>) ** 
</td>  
<td>

![trida](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/trida_thumb_12.jpg) **   
[ Trida ](<https://www.amourdecuisine.fr/article-trida-102621404.html>) ** 
</td> </tr>  
<tr>  
<td>

![rechta at navis 006](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/rechta-au-navis-006_thumb1.jpg) [ **RECHTA ALGEROISE** ](<https://www.amourdecuisine.fr/article-rechta-algeroise-91236467.html>) 
</td>  
<td>

![couscous beautiful bakbouka](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/couscous-bel-bakbouka_thumb.jpg) [ **   
Couscous beautiful osbane كسكسي بالعصبان »  ** ](<https://www.amourdecuisine.fr/article-couscous-bel-osbane-96835375.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tlitli-1_thumb.jpg)   
[ ** TLITLI CONSTANTINOIS pan > ** ](<https://www.amourdecuisine.fr/article-tlitli-constantinois-97828740.html>) 
</td> </tr>  
<tr>  
<td>

![tripe with vegetables \(2\)](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tripes-aux-legumes-2-_thumb.jpg) ** [ Bakbouka ](<https://www.amourdecuisine.fr/article-bakbouka-cuisine-algerienne-103194168.html>) ** 
</td>  
<td>

![Couscous beautiful osbane-001](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/Couscous-bel-osbane-001_thumb1.jpg) [ **   
OSBANE, OSBANA, PANSE FARCIE  ** ](<https://www.amourdecuisine.fr/article-osbane-osbana-panse-farcie-96833347.html>) 
</td>  
<td>

![doubara](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/doubara_thumb3.jpg) [ **   
DOUBARA WITH BISKRA CHICK PEAS  ** ](<https://www.amourdecuisine.fr/article-doubara-aux-pois-chiche-de-biskra-97928775.html>) 
</td> </tr>  
<tr>  
<td>

![k 014](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/k-014_thumb1.jpg) [ **   
LAMB BOWL IN TOMATO SAUCE  ** ](<https://www.amourdecuisine.fr/article-cervelle-d-agneau-en-sauce-tomate-89450259.html>) 
</td>  
<td>

![mtewem 020](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/mtewem-020_thumb1.jpg) **[ Mtewem ](<https://www.amourdecuisine.fr/article-mtewem-103198692.html>) ** 
</td>  
<td>

![lamb liver in sauce 004](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/foie-d-agneau-en-sauce-004_thumb2.jpg) [ **   
LAMB / MOUTON LIVER IN KEBDA MCHERMLA TOMATO SAUCE  ** ](<https://www.amourdecuisine.fr/article-foie-d-agneau-de-mouton-en-sauce-tomate-kebda-mchermla-87902209.html>) 
</td> </tr>  
<tr>  
<td>

![chbah essafra 074](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chbah-essafra-074_thumb1.jpg) CHBAH ESSAFRA شباح الصفرة 
</td>  
<td>

![mkartfa 038a](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/mkartfa-038a_thumb3.jpg) **M'KERTFA, MKERTFA, MKETAA, مقطعة مقرطفة** 
</td>  
<td>

![ros-Bratel feve-en sauce.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ros-bratel-feve-en-sauce3.jpg)   
[ **Bratel ros** ](<https://www.amourdecuisine.fr/article-ros-bratel-feves-en-sauce-92108205.html>) 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/232102043.jpg)   
** [ aghmoudh   
Couscous with beans ](<https://www.amourdecuisine.fr/article-25345506.html>) ** 
</td>  
<td>

![cauliflower in white sauce](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/choufleur-en-sauce-blanche_thumb.jpg)   
**[ Cauliflower with white sauce ](<https://www.amourdecuisine.fr/article-chou-fleur-en-sauce-blanche-90903107.html>) ** 
</td>  
<td>

![mhadjeb 069](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mhadjeb-069_thumb1.jpg)   
** [ Mhadjeb ](<https://www.amourdecuisine.fr/article-mhadjebs-algeriens-msemens-farcis-crepes-farcis-mahdjouba-62744653.html>) ** 
</td> </tr>  
<tr>  
<td>

![Baghrir 014](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/baghrir-014_thumb2.jpg) **[ baghrir ](<https://www.amourdecuisine.fr/article-ghrif-baghrir-corsa-thighrifine-crepes-epaisses-72487808.html>) ** 
</td>  
<td>

![couscous 611](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/couscous-611_thumb.jpg) ** [ Chicken couscous ](<https://www.amourdecuisine.fr/article-couscous-au-poulet-103194885.html>) ** 
</td>  
<td>

![head of sheep with caraway cumin 041](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tete-de-mouton-au-cumin-au-four-041_thumb.jpg) ** [ Baked sheep's head ](<https://www.amourdecuisine.fr/article-tete-de-mouton-au-cumin-au-four-64875408.html>) ** 
</td> </tr>  
<tr>  
<td>

![sardine in tomato sauce 032](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sardine-en-sauce-tomate-032_thumb3.jpg) **[ Chtitha sardines ](<https://www.amourdecuisine.fr/article-chtitha-sardines-103195801.html>) ** 
</td>  
<td>

![chickpeas in chicken sauce 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/pois-chiches-en-sauce-au-poulet-13.jpg) ** [ Chtitha homous   
or Chtitha bel jadj ](<https://www.amourdecuisine.fr/article-pois-chiches-en-sauce-au-poulet-62813115.html>) ** 
</td>  
<td>

![tajine aux olives-farcies.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tajine-aux-olives-farcies3.jpg) ** [ tajine zitoune ](<https://www.amourdecuisine.fr/article-25345434.html>) ** 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/25340955.jpg) ** [ Couscous with meat ](<https://www.amourdecuisine.fr/article-25345493.html>) ** 
</td>  
<td>

![maakouda 014](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/maakouda-014_thumb3.jpg) [ Maakouda ](<https://www.amourdecuisine.fr/article-maakouda-au-thon-45819035.html>) 
</td>  
<td>

![shrimp soup 014a](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/soupe-de-crevettes-014a_thumb2.jpg) [ Shrimp soup ](<https://www.amourdecuisine.fr/article-soupe-de-crevette-85354072.html>) 
</td> </tr>  
<tr>  
<td>

![hmiss](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/218925283.jpg)   
[ Hmiss ](<https://www.amourdecuisine.fr/article-25345564.html>) 
</td>  
<td>

![baked fish 092](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/poisson-au-four-092_thumb14.jpg)   
[ Baked sardines ](<https://www.amourdecuisine.fr/article-sardines-au-four-97716578.html>) 
</td>  
<td>

![garantita1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/garantita14.jpg)   
[ garantita ](<https://www.amourdecuisine.fr/article-garantita-ou-karantika-94535875.html>) 
</td> </tr> </table>
