---
title: banana smoothies strawberries yogurt and honey
date: '2014-05-01'
categories:
- juice and cocktail drinks without alcohol
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-banane-fraises-yaourt-et-miel-1.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-banane-fraises-yaourt-et-miel-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-banane-fraises-yaourt-et-miel-1.jpg>)

Hello everybody, 

Always in the mood smoothies, and yet another recipe has Lunetoiles, this time it is a banana smoothie and strawberries ... hum we take advantage of this fruit, as long as it is always the season. 

Strawberry banana smoothies yoghurt and honey, super rich as ingredients is not it? and surely a delight to taste as much as possible. 

Me anyway, when I prepare my smoothies, I do not add sugar or honey, sometimes a little bit of acacia honey for a more fragrance, but I must say that I like the tart in a smoothy, like that of the taste of strawberry, but if you do not tolerate the acidulous taste too much, honey just soften the acidity of strawberries. 

**banana smoothies strawberries yogurt and honey**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-banane-fraises-yaourt-et-miel-1-290x195.jpg)

portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 bananas cut in pieces and frozen 
  * Dozens of fresh strawberries, hulled and cut into pieces 
  * 2 stirred yoghurt 
  * 1 tbsp. coffee orange flower water 
  * 2 Cuil. honey 



**Realization steps**

  1. Blend everything in a blender until you get a velvety texture. 
  2. Pour immediately into glasses, and decorate with fillets of honey. 
  3. Enjoy! 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-banane-fraises-yaourt-et-miel.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-banane-fraises-yaourt-et-miel.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
