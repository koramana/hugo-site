---
title: lemon chicken
date: '2016-07-17'
categories:
- Moroccan cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/POULET-AU-CITRON-057.CR2_1.jpg
---
![CHICKEN-IN-LEMON-057.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/POULET-AU-CITRON-057.CR2_1.jpg)

##  lemon chicken 

Hello everybody, 

I share with you, the dish that I prepared for a special family evening, a delicious lemon chicken ... pity that the photos, do not give the fair value of this very delicious dish (it's always like that the photos) at night, and above all ... everyone waited for the dish to be served ... no time to make the paparazzi ....), a recipe that I go back from the archives of my blog, at the request of one of my readers, and it suits me well because I'm too taken today, and I do not have time to post one of my new recipes. 

**lemon chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/POULET-AU-CITRON-057.CR2_-300x199.jpg)

portions:  4  Prep time:  10 mins  cooking:  45 mins  total:  55 mins 

**Ingredients**

  * 1 whole chicken hollowed 
  * 1 [ lemon confit ](<https://www.amourdecuisine.fr/article-citron-confit.html> "lemon confit salty")
  * 6 tablespoons olive oil. 
  * 1 cube of chicken broth 
  * some saffron pistils 
  * ½ teaspoon of pepper 
  * 1 teaspoon ginger powder 
  * ½ teaspoon of turmeric 
  * ½ cup of ras el hanout 
  * 6 onions 
  * 6 cloves of garlic 
  * 1 small bunch of coriander 
  * potatoes, to roast to the end in the sauce 



**Realization steps**

  1. place the spices in a large bowl: rasse el hnout, saffron, black pepper, turmeric, ginger and bouillon cube, add enough water to dilute the whole. 
  2. Marinate the whole chicken in this mixture of spices, under the skin, and indoors. leave at least 1 hour. 
  3. Put the oil in a pot large enough, otherwise a casserole. 
  4. add the chicken in the hot oil. 
  5. Peel the garlic. and cut it into thin slices. 
  6. watch and return the chicken so that it does not burn, and that it takes a beautiful color of all the dimensions. 
  7. Add all the garlic on top. 
  8. Peel and cut the onions. 
  9. Add to the pot. 
  10. Cut the bunch of coriander and chop it finely and add it to the pot. 
  11. cut the lemon confit in half, keep half, and cut the other half, add all on the chicken. 
  12. Cook, stirring occasionally, between 1 and 1:30 depending on the size of the chicken. add a little water if necessary. 
  13. when the chicken is cooked, remove it from the sauce, 
  14. put the sauce in a baking dish, add the cleaned and quarter cut potato to this sauce, add a little water and cook while covering the dish with aluminum foil. 
  15. when the potato is cooked, put the chicken back and let it take a soft golden color. 


