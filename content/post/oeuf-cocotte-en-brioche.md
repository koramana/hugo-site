---
title: egg casserole brioche
date: '2017-04-28'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
- recette de ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/oeuf-cocotte-en-brioche-1.jpg
---
[ ![egg casserole in brioche 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/oeuf-cocotte-en-brioche-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/oeuf-cocotte-en-brioche-1.jpg>)

##  egg casserole brioche 

Hello everybody, 

Here is a recipe super easy to achieve, and super economical, but really stalls the stomach, a recipe for egg casserole brioche, which comes from the kitchen of Lunetoiles, it must smell good !!! 

It's really nice as an idea, and it's a good accompaniment, as it is very nice to present when you go out on a picnic, or even for a light meal at noon, accompanied by a light tomato sauce, or a simple salad. 

I already see another version of this recipe, filling the little buns, a little tuna before breaking the egg over. 

I had already made a recipe a little different, replacing the buns with tomatoes: [ Tomato Nests ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs.html> "stuffed tomatoes")

**egg casserole brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Oeuf-cocotte-en-brioche.jpg)

portions:  4  Prep time:  5 mins  cooking:  30 mins  total:  35 mins 

**Ingredients**

  * 4 french buns 
  * 4 small eggs 
  * 4 tablespoons fresh cream 
  * Flower of salt 
  * Grilled pepper 



**Realization steps**

  1. Preheat oven to 160 ° C (th5). 
  2. Cut the buns' heads and gently empty the crumb of the buns, noting too much. 
  3. Break the eggs one by one into a bowl, season and pour 1 egg into each bun. 
  4. Cover with a tablespoon of fresh cream and rest your head. 
  5. Bake the buns in the oven for 30 minutes at 160 ° C. 
  6. Enjoy hot! The egg flowing on the bun ... a real treat! 



[ ![egg casserole brioche 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/oeuf-cocotte-en-brioche-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/oeuf-cocotte-en-brioche-2.jpg>)
