---
title: zucchini flower frittata
date: '2018-03-24'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
tags:
- eggs
- Italy
- aperitif
- omelettes
- Fast Recipes
- Cheese

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/frittata-aux-fleurs-de-courgettes-au-four-1.jpg
---
[ ![zucchini flower frittata](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/frittata-aux-fleurs-de-courgettes-au-four-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/frittata-aux-fleurs-de-courgettes-au-four-1.jpg>)

##  zucchini flower frittata 

Hello everybody, 

This is the weekend, and it's not easy to wake up relaxed, to reflect what you can prepare for lunch. All week long to run here and the, anyway, we dare to hope that the weekend is sleep until 10am, lol, and relax in bed for at least 30 minutes, before going as a Koala all soft in the kitchen, for an even more relaxed breakfast in front of the TV .... Ding, ding, but you're dreaming, I think ???, go stand up, it's 7:30, baby starts crying because he needs to eat .... the others start bickering in their rooms fighting each other who will take control of the tele-control, to watch the program he wants ... And sir, who says to you: shut the door behind you and calm the children STP .... Yes, yes it is the life of a mother ... 

And while she manages all this, she looks at the time and it's already 11:30, it must be done quickly, because in less than 30 minutes, it will start: "mom I'm hungry! what to eat? " 

So for days like that, there's no easier than an omelette, or a frittata, and why not a baked frittata while you're there, for a healthy lunch not too fat for kids? Blows, here is the delicious recipe for baked zucchini flower frittata, shared with us Lunetoiles, and I'm sure it will please you, now remains to find the flowers of zucchini, something that is not easy for me here in England, what about your side? 

**zucchini flower frittata**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/frittata-aux-fleurs-de-courgettes-au-four-001.jpg)

portions:  3  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 6 big eggs 
  * 11 large fresh zucchini flowers 
  * 2 tbsp. Greek yoghurt 
  * 2 tbsp. Parmesan 
  * 2 tbsp. pecorino 
  * 60 gr of smoked prolvolone cheese or stringy curd cheese 
  * a few sprigs of fresh parsley, washed and chopped 
  * ground black pepper 
  * salt 



**Realization steps**

  1. Gently wash the zucchini flowers, remove the peduncle and the hairy green twigs. 
  2. Halve each flower, and cut into smaller pieces. 
  3. In a large bowl, break the eggs, add yoghurt, parmesan and pecorino, abundant fresh parsley, zucchini flowers cut into squares, and salt and pepper.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/frittata-aux-fleurs-de-courgettes-au-four-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/frittata-aux-fleurs-de-courgettes-au-four-3.jpg>)
  4. Mix with a fork until the mixture is creamy and lump free. 
  5. Arrange the mold of 22 cm diameter, a sheet of parchment paper, and pour the mixture, add the smoked cheese, or vote choice. 
  6. Sprinkle with pepper and a pinch of salt. 
  7. Bake at 180 ° C for 20 to 25 minutes. 
  8. Until the surface is golden. 



[ ![baked zucchini flower frittata 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/frittata-aux-fleurs-de-courgettes-au-four-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/frittata-aux-fleurs-de-courgettes-au-four-2.jpg>)

Merci a Lunetoiles pour cette delicieuse recette, je l’essayerai surement, des que j’ai l’occasion de tomber sur les fleurs de courgettes. 
