---
title: homemade praline
date: '2011-11-23'
categories:
- amuse bouche, tapas, mise en bouche
- Bourek, brick, samoussa, slippers
- crepes, waffles, fritters
- cuisine diverse

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/praline-maison-004a1.jpg
---
![praline-house-004a.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/praline-maison-004a1.jpg)

Hello everybody, 

this recipe I prepared yesterday night, because of the recipe that will come, so sorry for the night photos, it's really not the joy ... hihihiih 

this recipe is that of lunetoiles she had put in a recipe, and I preferred to present it separately with photos.   


**homemade praline**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/praline-maison-008a1.jpg)

Recipe type:  basic pastry recipe  portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients** For 400 g of praline paste: 

  * 120 g whole almonds with skin 
  * 120 g of whole hazelnuts with the skin 
  * 160 g of sugar 
  * 15 g of water 



**Realization steps**

  1. Pour the sugar into a pan. Put on medium heat. 
  2. Add water and start boiling. 
  3. When the sugar is at 121 ° C, (the sugar begins to form thicker and thicker bubbles, the syrup becomes thick) add the almonds at once. 
  4. Mix well with a wooden spoon to cover each almond with sugar. 
  5. At this moment, the sugar will sand, that is to say that it will crystallize again. Always continue mixing. 
  6. The sugar will then reshape, this time caramelizing. 
  7. Once all the caramelized sugar is removed from the heat. 
  8. Pour all the contents of the pan on a baking paper. 
  9. Let cool completely. Once this caramel plate is hard, break it into pieces. 
  10. One or more pieces can be kept for a cake decoration (nougatine). 
  11. Put the rest in a blender. And begin with successive pulsations. 
  12. After a few seconds we already get a powdered praline that can be put aside if desired for ice cream or praline creams. Keep this praline in an airtight container. 
  13. For the praline paste, let the mixer turn until it is almost liquid. It depends on the power of the mixer, the more you leave everything in the mixer running, the more the dough will be crushed. 
  14. After an average of 5 minutes of grinding, the praline paste is obtained. Keep this paste of praline in a jam jar clean in the cool, it is in a sealed box. 



![praline-house-008a.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/praline-maison-008a1.jpg)

the dough praline on the picture is not yet liquid, I did not want to use the blinder at midnight, I disturb the children, and neighbors. 

et juste a cote, c’est la pâte a praliné chocolatée 
