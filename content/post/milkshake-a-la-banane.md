---
title: Milkshake with banana
date: '2016-06-14'
categories:
- juice and cocktail drinks without alcohol
tags:
- Drinks
- smoothies
- Easy cooking
- Ramadan 2016
- To taste
- Juice
- Summer kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/milkshake-%C3%A0-la-banane-1.jpg
---
![milkshake with banana 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/milkshake-%C3%A0-la-banane-1.jpg)

##  Milkshake with banana 

Hello everybody, 

The banana milkshake is the favorite snack of my children, and to be even me it suits me a lot to prepare in two seconds a banana milkshake or any other fruit of their choice, to easily stay 1 hour to make a cake to present on their return from school. 

I absolutely wanted to share this recipe with you, I'm sure your children will love you a lot, and you'll at least have one less task to do after having lunch. 

A milkshake is simple! A milkshake with banana is all good! 

The recipe in Arabic: 

**Milkshake with banana**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/milkshake-%C3%A0-la-banane-2.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 2 bananas 
  * 4 beautiful scoops of vanilla ice cream 
  * 300 ml whole milk 
  * 1 teaspoon of vanilla sugar 



**Realization steps**

  1. In the blender bowl, place the sliced ​​bananas, milk, vanilla ice cream and vanilla sugar 
  2. Mix everything until you have a liquid and homogeneous paste. 
  3. Serve immediately, yum yum 



![milkshake with banana](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/milkshake-%C3%A0-la-banane.jpg)
