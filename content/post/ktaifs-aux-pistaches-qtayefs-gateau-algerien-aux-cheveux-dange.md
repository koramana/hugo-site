---
title: Pistachio ktaives, Qtayefs, Algerian angel haired cake
date: '2009-12-15'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/12/ketaif7_thumb.jpg
---
the recipe in Arabic here is a delicious Algerian pastry, it looks like the baklawa, but it is easier to handle on one side, it needs less stuffing of another, but only you have to find the pasta ktaif, that we do not find everywhere, which is a pity. Pistachio ktaives, Qtayefs, Algerian angel-haired cakes Prepared by: love of food Servings: 12 Prep time: & nbsp; 20 mins cooking: & nbsp; 20 mins total: & nbsp; 40 mins Ingredients 2 balls of qtayef 1 bowl of ground pistachios 3 cases of sugar & frac12; c. a cinnamon coffee & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.6  (  1  ratings)  0   
<table>  
<tr>  
<td>

[ ![ketaif algerois](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/ketaif7_thumb.jpg) ](<https://www.amourdecuisine.fr/article-32532261.html>) 
</td>  
<td>

Here is a delicious Algerian pastry, it looks like the baklawa, but it is easier to handle on one side, it needs less stuffing of another, but only it is necessary to find the paste a ktaif, that one does not not everywhere, which is a shame. 
</td> </tr> </table>

[ ![http://a10.idata.over-blog.com/3/49/82/14/API/2009-12/ketaif4_4.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/550x412-c11.png) ](<https://www.amourdecuisine.fr/article-32532261.html>)

**Pistachio ktaives, Qtayefs, Algerian angel haired cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/ketaif7_thumb.jpg)

portions:  12  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 2 balls of qtayef 
  * 1 bowl of ground pistachios 
  * 3 cases of sugar 
  * ½ c. a cinnamon coffee 
  * ½ glass of orange blossom water 
  * 100 gr of butter 
  * for the syrup: 
  * 500 gr of sugar 
  * ½ liter of water 
  * 2 cases of orange blossom water 



**Realization steps**

  1. prepare the pistachios, bake them if necessary, crush them, add the sugar and cinnamon, moisten with 1/2 glass of orange blossom water. 
  2. generously butter a round baking dish made of metal or pyrex 
  3. cover the bottom of the dish with a ball of qtayef scattering 
  4. spread the pistachio stuffing over the whole surface. 
  5. cover the whole with the 2nd qtayef ball by scattering it and taking care to well enclose the pistachio stuffing. 
  6. Sprinkle well with butter, and brown both sides in the oven for about ½ hour, or according to your oven. 

meanwhile prepare the syrup. 
  1. pour the sugar and water into a saucepan and cook for 20 to 30 minutes after boiling until the syrup flows in a thin stream. 
  2. perfume the syrup with orange blossom water.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/550x412-c4.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/12/550x412-c4.png>)
  3. when the cake is golden, remove it from the oven. 
  4. sprinkle with syrup and let cool, before cutting and serving 



[ ![ketaif3](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/ketaif31.jpg) ](<https://www.amourdecuisine.fr/article-32532261.html>)

[ ![ketaif6](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/ketaif61.jpg) ](<https://www.amourdecuisine.fr/article-32532261.html>)

bonne dégustation. 
