---
title: Bavarian speculoos strawberries
date: '2013-05-12'
categories:
- diverse cuisine
- Cuisine by country
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/bavarois-fraises-speculoos-3_thumb1.jpg
---
![Bavarian strawberries speculoos 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/bavarois-fraises-speculoos-3_thumb1.jpg)

##  Bavarian speculoos strawberries 

Hello everybody, 

still a delice delice, yes again a recipe Lunetoiles, she is very very talented, and it is with great pleasure that I share his recipes with you through my blog, this recipe [ Bavarian ](<https://www.amourdecuisine.fr/categorie-12125855.html>) , 

**Bavarian speculoos strawberries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/bavarois-fraises-speculoos_thumb1.jpg)

portions:  12  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients** For the speculoos background: 

  * 200g of speculoos 
  * 60g of soft butter 

The strawberry bavarois: 
  * 40 cl of whole cold liquid cream 
  * 70g of powdered sugar 
  * 6 g of gelatin 
  * 200g strawberry coulis 

The strawberry mirror: 
  * 200g strawberry coulis 
  * 30g of sugar 
  * the juice of half a lime 
  * 3g of gelatin 



**Realization steps**

  1. In a blender, reduce the powdered speculos, add the butter and mix. 
  2. squeeze the resulting mixture at the bottom of the pastry circle and place in a cool place 

prepare the Bavarian: 
  1. soften the gelatin sheets in a bowl of cold water (for 5 min) 
  2. Beat the whole liquid cream with the sugar until you get a whipped cream, 
  3. place the cool 
  4. wash and cut strawberries, mix them in a mixer to obtain a coulis (about 200g of coulis) 
  5. heat and then dilute the gelatin (softened and wrung out) 
  6. let cool completely, then add this coulis to the whipped cream, mix 
  7. pour this cream in the mold, on the bottom of speculoos and let cool for 1h minimum. 

Then prepare the strawberry mirror: 
  1. heat 200g of strawberry coulis (mixed with lime juice), 
  2. dilute a sheet of gelatin (which must be softened in cold water for 5 min and wrung out) 
  3. remove from heat, and let cool completely, the grout should be almost cold, if it turns into jelly, just warm a little. 
  4. Pour this sauce "almost cold" on the bavarois and return to the fridge at least 4 hours before unmolding. (preferably a whole night) 
  5. To facilitate demolding, simply pass a knife all around the Bavarian before removing the circle! 



merci ma chere Luentoiles pour ce delice. 
