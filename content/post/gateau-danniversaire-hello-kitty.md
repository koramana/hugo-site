---
title: hello kitty birthday cake
date: '2012-12-12'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles and bars
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateau-d-anniversaire-ines-5-ans-007.CR2_2.jpg
---
##  hello kitty birthday cake 

Hello everybody, 

I had completely forgotten that I had not published my daughter's birthday cake recipe **hello kitty birthday cake** . 

This hello kitty birthday cake is made with fine cakes soaked in syrup, then covered with generous layers of [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline.html>) , finally covered [ almond paste ](<https://www.amourdecuisine.fr/article-pate-damande-maison-massepain.html>) . I hope that the recipe of this cake will please you, thank you 

it's never too late to publish the recipe for such a delight: 

**hello kitty birthday cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateau-d-anniversaire-ines-5-ans-007.CR2_2.jpg)

**Ingredients** fine cake: for each round biscuit 25 cm in diameter 

  * 2 eggs 
  * 60 gr of sugar 
  * 60 gr of flour 
  * 1 pinch of baking powder 
  * pink dye   

  * I made 4 cookies, so I used 8 eggs in total, the first layer, I add 2 spoons of cocoa, and I decrease 2 spoons of flour. 

syrup: 
  * 200 gr of sugar 
  * 300 ml of water 
  * vanilla 

for decoration: 
  * Chiffon cream 
  * sugarpaste 
  * almond paste 
  * gel dyes (it's the best): pink, orange, green, blue and yellow. 



**Realization steps** first prepare the syrup: 

  1. mix all the ingredients, and put on medium heat, at the first boiling turn off the heat, and let cool. 
  2. Prepare the biscuits end: 

beat the eggs thoroughly with the sugar until the mixture is white and very airy. 
  1. add the flour and baking powder, cook in a hot oven, for 15 to 20 minutes (depending on the capacity of your oven) 

mounting 
  1. place the first layer of fine biscuit, and soak it a little (not too much) with the prepared syrup. 
  2. pour over one third of the cream muslin, and cover the cake well. 
  3. place the second layer of biscuit on top, soak generously with the syrup, and cover with another third of the cream muslin. 
  4. drop another cookie, and do the same. 
  5. place the last layer of the cake, soak it once more generously with the syrup, and cover the whole cake with the rest of the muslin cream. 
  6. place in the fridge.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateau-d-anniversaire-hello-Kitty.CR2_thumb1.jpg)
  7. Spread almond paste on a work surface sprinkled with icing sugar, so that it does not stick, and cover the cake. 
  8. color the sugar dough (I use the commercial sugar dough) of the different colors of your choice. 
  9. spread out each dumpling of colored sugar paste, and cut rectangles, which you will stick with just a little water 
  10. according to your means, make flowers or colorful butterflies, which you will stick on the cake. 
  11. spread the white sugar pie. draw the face of hello kitty on a piece of paper, cut with a chisel to have the shape of the face, draw this face on the dough to be able to cut out the form of hello kitty. 
  12. color some sugar paste with black, and shape the eyes and whiskers (mustaches) of hello kitty. 
  13. with pink sugar paste, shape the pink hello kitty ribbon. 


