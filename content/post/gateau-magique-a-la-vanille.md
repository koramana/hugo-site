---
title: magic cake with vanilla
date: '2014-12-20'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Cake
- Mellow cake
- Algerian cakes
- To taste
- desserts
- Soft
- Sweet cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateau-magique-a-la-vanille-5.jpg
---
![recipe of the magic cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateau-magique-a-la-vanille-5.jpg)

Magic cake with vanilla 

Hello everybody, 

I know you're going to tell me, oh the magic cake, we know we see it everywhere on the blogosphere, but just like you, I too was super tempted to make this magic cake vanilla, especially after the test of my [ magic caramel chocolate cake ](<https://www.amourdecuisine.fr/article-gateau-magique-au-chocolat-et-creme-caramel.html> "magic cake with chocolate and caramel cream") who made his success, and who was the recipe for ramadan dessert home at home for 2 years. 

At the release of the new version of the magic cake, I do not tell you, I tried all possible versions, and we tasted this cake, without stopping during Ramadan this year. I thought it was easier, because instead of making a flan and a dessert, I made this cake: 2 in 1, and the round was well played. 

Besides, I did so much, that I became a professional, I could have a beautiful layer of flan well uniform (I will not show you the photo of my first magic vanilla cake, the custard was high here, down there, the layer of the cake Angel: Angel cake, was rapla-pla, cracked) in any case impossible to put this in public, hahaha 

With time, I understood my little mistakes, and I'll give you my little tips to have a **magic cake** perfect. 

[ ![recipe of the magic cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateau-magique-a-la-vanille-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateau-magique-a-la-vanille-3.jpg>)

**magic cake with vanilla**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateau-magique-a-la-vanille-2.jpg)

portions:  12  Prep time:  15 mins  cooking:  50 mins  total:  1 hour 5 mins 

**Ingredients**

  * 4 whole eggs 
  * 130 gr of sugar 
  * 125 gr of butter 
  * 115 grams of flour 
  * 500 ml warm whole milk 
  * 2 teaspoons of vanilla extract 



**Realization steps**

  1. whisk the egg whites until the snow and add 2 tablespoons of sugar towards the end, whip again 
  2. In another bowl, beat the egg yolks with the sugar, and the vanilla until you have a creamy mixture. 
  3. Add the melted butter and continue beating for one minute. 
  4. then introduce the flour while whisking. 
  5. Add the milk gently (it must be warm, otherwise the butter will harden and make grumaux), whip until it is well incorporated. 
  6. Now gently add the egg whites to the snow using a hand whisk (it does not work with the spatula, believe me) but do not break the egg white too much, just make circular movements for the white is incorporated as a foam. 
  7. Pour the dough into a greased circular dish 20 cm in diameter or 20 x 20 cm square. For my part I used a silicone mold, and for the good trick to remove the cake without screwing it up, I put in the mold the base of one of my molds removable base, you can see in the picture the line in green, it's a glass base of my pie plate   
[ ![recipe of the magic cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateau-magique-a-la-vanille-011.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateau-magique-a-la-vanille-011.jpg>)
  8. after pouring the mixture, I make a turn with the manual whisk, to make sure to have a uniformity in the cake. If you use a mold with a removable base, cover well with baking paper because the dough is too liquid, it may leak. 
  9. Bake the cake in the preheated oven at 180 degrees C for about 50 minutes or until the top turns a beautiful golden color (in my oven it took 45 minutes of cooking when I move the mold and I see that the center , so it is well cooked). 
  10. let cool in the mold at least 20 minutes before unmolding. 
  11. Serve the cake after sprinkling the top with powdered sugar. 



[ ![recipe of the magic cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateau-magique-a-la-vanille-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateau-magique-a-la-vanille-4.jpg>)
