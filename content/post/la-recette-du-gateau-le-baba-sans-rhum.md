---
title: the cake recipe the rum-free baba
date: '2012-12-11'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/baba1-0011_31.jpg
---
[ ![baba without rum](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/baba1-0011_31.jpg) ](<https://www.amourdecuisine.fr/article-42502008.html>)

##  the cake recipe the rum-free baba 

Hello everybody, 

many of you who saw the "best pastry" program came to ask me that it would be the rum-free baba recipe, so I'll give you my recipe. 

I remember the time, or I ate this baba in the Algerian pastries, a real delight, melting in the mouth, and with this delicious taste of orange syrup. 

The recipe in Arabic: 

**the cake recipe the rum-free baba**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/baba1-003_1.jpg)

portions:  6  Prep time:  45 mins  cooking:  30 mins  total:  1 hour 15 mins 

**Ingredients**

  * 225 gr of flour 
  * 25 gr of sugar 
  * 5g of salt 
  * 12 grams of instant yeast 
  * 2 eggs 
  * 100 ml of water 
  * 50 gr of butter at room temperature 



**Realization steps**

  1. mix the dry ingredients together, add the eggs, then the water, and knead, you can use the kneader or then the bread machine, or even knead by hand, as I did myself, you can see the video of the kneading method. 
  2. so you have to raise the dough so that the dough is well ventilated. 
  3. stir in the butter and continue to mix in the same way 
  4. put in savarin molds, or a well-buttered baba, place in a slightly hot oven (not light) or you have placed a pot filled with hot water. as these cakes usually lie to rise in a damp place. 
  5. after emergence, bake in an oven heated to 180 degrees. 
  6. let cool, and sprinkle with syrup, and water it as much as you can, if your baba are hot the syrup should be a little warm not hot, if the babas are cold, the syrup should be hot. 
  7. water the first time, let it absorbed a little, then water another time, let it absorb too, then water a 3rd time, and a 4th time. 



[ ![baba1 008-1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/baba1-008-1_3.jpg) ](<https://www.amourdecuisine.fr/article-42502008.html>)

[ ![baba 019-1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/baba-019-1_3.jpg) ](<https://www.amourdecuisine.fr/article-42502008.html>)

preparation: 

  * 500 ml of water 
  * 500 gr of sugar 
  * orange dune juice or a lemon 



mix the water and sugar, put on the heat until boiling, then add the juice and remove from heat. 

garnish the babas according to your taste 

recipe tested and approved:   
  
<table>  
<tr>  
<td>

[ ![baba has the orange at Walid S](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/baba-a-lorange-chez-Walid-S-150x84.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/baba-a-lorange-chez-Walid-S.jpg>)

at Walid Sofia 


</td>  
<td>

[ ![baba has the orange](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/baba-a-lorange-150x113.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/baba-a-lorange.jpg>) 
</td>  
<td>


</td> </tr> </table>
