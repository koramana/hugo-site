---
title: batbouts stuffed with tuna
date: '2016-09-28'
categories:
- Bourek, brick, samoussa, chaussons
- Chhiwate Choumicha
- Mina el forn
- pain, pain traditionnel, galette
- pizzas / quiches / pies and sandwiches
- recette de ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/batbouts-farcis-au-thon-recettes-de-ramadan-2014.CR2_.jpg
---
[ ![batbout stuffed with tuna](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/batbouts-farcis-au-thon-recettes-de-ramadan-2014.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/batbouts-farcis-au-thon-recettes-de-ramadan-2014.CR2_.jpg>)

##  batbouts stuffed with tuna 

Hello everybody, 

That's how kids like to eat [ Moroccan batbouts ](<https://www.amourdecuisine.fr/article-batbout-marocain.html> "Moroccan Batbout "بطبوط مغربي"") that I shared with you last time. As I told you, kids love to eat them as a sandwich, and that's one of the pranks they like the most, tuna stuffed batbouts. 

I always like to prepare this recipe, especially when I want to get the children out, I prepare them these tuna stuffed batbouts, a smoothee, and hop out for a little picnic. 

What I like most is that back home, children will not ask for anything to eat, they take their shower, and in bed .... I do not have to run in all directions to make the dinner, and I do not worry that the children on an empty stomach, I know very well that they played a lot, and especially that they ate all the batbouts stuffed with tuna, which I took with me ... 

But, do not believe that this recipe is only for children, no! I often prepare it during the month of Ramadan, when I want to avoid frying, and especially when I am tired, and I do not want to prepare the matloue, and then the bouraks. I prepare the Moroccan batbouts. For the stuffing, you can prepare the stuffed batbouts with tuna, [ chicken stuffed chicken ](<https://www.amourdecuisine.fr/article-batbout-farcis-au-poulet.html>) or then [ Batbouts Stuffed with Avocado and Potato ](<https://www.amourdecuisine.fr/article-batbouts-a-l-avocat-et-pommes-de-terre.html>) . 

**batbouts stuffed with tuna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/recettes-de-ramadan-2014-batbouts-farcis-au-thon.CR2_.jpg)

portions:  10  Prep time:  30 mins  total:  30 mins 

**Ingredients**

  * [ Moroccan batbouts ](<https://www.amourdecuisine.fr/article-batbout-marocain.html> "Moroccan Batbout "بطبوط مغربي"")

for the stuffing: 
  * 3 hard boiled eggs 
  * 250 gr canned tuna 
  * 1 handful of green olives 
  * a few sprigs of parsley (I put a lot) 
  * mayonaise according to your taste 



**Realization steps**

  1. Start by preparing the batbouts as indicated on the recipe of [ Moroccan batbout ](<https://www.amourdecuisine.fr/article-batbout-marocain.html> "Moroccan Batbout "بطبوط مغربي"")

Prepare the stuffing: 
  1. in a salad bowl, crush the boiled eggs 
  2. add over the crumbled tuna 
  3. then add the sliced ​​olives, the chopped parsley, and the mayonaise to taste 
  4. adjust the seasoning with a little salt, and a little pepper from the mill 
  5. make an incision in the batbouts to give them the shape of a sandiwch 
  6. stuff them with a nice spoon stuffed with stuffing. 



[ ![batbouts stuffed with tuna 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/batbouts-farcis-au-thon-1a.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/batbouts-farcis-au-thon-1a.CR2_.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
