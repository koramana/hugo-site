---
title: dried fruit cake
date: '2015-03-30'
categories:
- gateaux, et cakes
- sweet recipes
tags:
- Easy cooking
- Inratable cooking
- Cakes
- desserts
- To taste
- Soft
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cake-aux-fruits-secs-2.jpg
---
[ ![dried fruit cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cake-aux-fruits-secs-2.jpg) ](<https://www.amourdecuisine.fr/article-cake-aux-fruits-secs.html/cake-aux-fruits-secs-2>)

##  dried fruit cake 

Hello everybody, 

Are you ready to taste it? so join me to enjoy this super delicious dried fruit cake, there is some crunch in it, yum yum. 

This cake recipe with dried fruit is very sweet and melting in the mouth is a recipe of my friend: **A moment of pleasure** whom I salute strongly, and who generously shares these delights through the lines of my blog. Thank you very much my dear, it's a pleasure for me this sharing, and it's recipes that I will surely try soon. 

[ ![dried fruit cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cake-aux-fruits-secs-3.jpg) ](<https://www.amourdecuisine.fr/article-cake-aux-fruits-secs.html/cake-aux-fruits-secs-3>)   


**dried fruit cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cake-aux-fruits-secs-4.jpg)

portions:  12  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients**

  * 175 gr of butter at room temperature 
  * 5 eggs 
  * 10 tablespoons of sugar 
  * 1 and a half sachets of baking powder 
  * 1 packet of vanilla 
  * flour as needed (usually 20 tablespoons for me) 
  * a handful of crushed nuts 
  * and for the topping, dry fruits according to your taste. 
  * jam. 



**Realization steps**

  1. whip the sugar and butter for a nice cream 
  2. Introduce 1 egg at a time 
  3. Whenever you add an egg and whip it, add 4 tbsp flour and mix. 
  4. Do this until the quantity of egg has been used up 
  5. at the end introduce the baking powder and vanilla 
  6. add the crushed nuts and mix gently for the walnut and the yeast is well introduced into the mixture. 
  7. pour this mixture into a buttered and floured mold   
as in this photo I added raisins washed and coated in flour and I added them on the top of the cake before baking [ ![dried fruit cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cake-aux-fruits-secs.jpg) ](<https://www.amourdecuisine.fr/article-cake-aux-fruits-secs.html/cake-aux-fruits-secs>)
  8. Bake in a preheated oven at 180 degrees C, between 40 and 45 minutes 
  9. take it out of the oven, brush the top of the cake with jam and decorate with dried fruits, according to your taste. 



[ ![dried fruit cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cake-aux-fruits-secs-4.jpg) ](<https://www.amourdecuisine.fr/article-cake-aux-fruits-secs.html/cake-aux-fruits-secs-4>)
