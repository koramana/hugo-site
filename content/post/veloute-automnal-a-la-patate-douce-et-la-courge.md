---
title: autumn velvet with sweet potato and squash
date: '2017-01-08'
categories:
- soups and velvets
tags:
- inputs
- Vegetarian cuisine
- Algeria
- Healthy cuisine
- Easy cooking
- Accessible Kitchen
- Soup

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/veloute-patate-douce-021_thumb1.jpg
---
##  autumn velvet with sweet potato and squash 

Hello everybody, 

Here is a delightful [ soup ](<https://www.amourdecuisine.fr/soupes-et-veloutes>) , a very good [ velvety ](<https://www.amourdecuisine.fr/soupes-et-veloutes>) autumnal pasta and squash. Honestly I like the velvety especially by this cold, but I discovered that I tend to love the sweet salty velvety .. 

This velvety autumnal sweet dough and squash, or rather wintry (great confusion on seasonal vegetables, but here is now can even find summer vegetables on the shelves)! So this Velouté is just too good. 

And believe me, the little cream over, with this orange blossom flavor is for many, it gives this velvety another dimension ... it's just too good! 

**autumn velvet with sweet potato and squash**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/veloute-patate-douce-021_thumb1.jpg)

Recipe type:  soup, vegetarian recipe  portions:  4  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 1 tablespoon of olive oil 
  * 1 onion, coarsely chopped 
  * 1 teaspoon curry powder 
  * 3 medium sweet potatoes, peeled and diced 
  * 2 peppermint squash, peeled and cubed (which you can replace with pumpkin) 
  * 1 liter of chicken broth 
  * Maple syrup according to taste (3 tablespoons for me) 
  * 3 sprigs of fresh thyme 
  * 1/8 c. freshly grated nutmeg 
  * coarse salt and freshly ground pepper 

for the cream: 
  * 200 ml thick cream 
  * 2 tablespoons orange blossom water 
  * The zest of lemon 
  * 1/2 spoon of chopped fresh thyme 
  * freshly ground pepper 



**Realization steps**

  1. Heat the oil in a thick bottom saucepan over medium heat. 
  2. Add the onion and curry powder and cook until the onion is almost translucent, about 4 minutes. 
  3. Add sweet potatoes, squash, broth, maple syrup, thyme and nutmeg. 
  4. Bring the broth to a boil, lower the heat 
  5. simmer, uncovered, until the potatoes are tender, about 30 minutes. 
  6. Remove the sprigs of thyme. Let the soup cool slightly. 
  7. Puree until smooth 
  8. Warm soup before serving and season with salt and pepper if necessary. 

for the cream 
  1. In the bowl of an electric mixer equipped with the whip attachment, whip the thick cream until you have soft peaks. 
  2. Add orange blossom water and lemon zest and continue whisking until firm. 
  3. decorate the soup with a nice spoon of this cream and sprinkle with chopped fresh thyme. 


