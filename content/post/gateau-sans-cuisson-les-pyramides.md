---
title: cake without cooking, the pyramids
date: '2012-05-06'
categories:
- Mina el forn
- pain, pain traditionnel, galette

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/gateau-sans-cuisson-les-pyramides_thumb1.jpg
---
hello everyone, cakes without cooking, I like a lot, and I do not tell you how much I did at the beginning of my marriage, because I did not have a good oven, I did all kinds, and all combinations possible, and I dig up a recipe that I did a lot at that time, but I try to get out of the balls ... why not pyramids, not easy to achieve when you do not have a mold, but I love to do the challenge ... ingredients: for a groin of pieces (medium size) 200 gr grilled flour & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

the [ **cakes without cooking** ](<https://www.amourdecuisine.fr/categorie-11745248.html>) I really like, and I do not tell you how much I did at the beginning of my marriage, because I did not have a good oven, I did all sorts, and all possible combinations, and I dig up a recipe that I was doing a lot at that time, but I try to get out of the balls ... 

why not pyramids, not easy to achieve when you do not have a mold, but I like to do the challenge .... 

Ingredients: 

for a groin of pieces (medium size) 

  * 200 gr of grilled flour 
  * 100 gr of grilled milk powder 
  * 50 gr of icing sugar 
  * 100 gr of melted butter 
  * honey 
  * White chocolate 



[ ![cake without cooking the pyramids](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/gateau-sans-cuisson-les-pyramides_thumb1.jpg) ](<https://www.amourdecuisine.fr/categorie-10678931.html>)

method of preparation: 

  1. grill the flour in a pan, watching and turning often with a wooden spoon, and pass through the sieve 
  2. also grind the milk powder in a pan, and watching carefully, and often turning with a wooden spoon, it's really delicate, then pass it to the sieve 
  3. put the two mixtures in a tank 
  4. add the powdered sugar, the melted butter, and pick up with the honey 
  5. At this point, already you can drip your cake, if you think the cake and dry but sweet, you can add butter. 
  6. form balls of 23 to 25 gr each, if you can shape pyramids, it is good if not remaining in the form of the balls. 
  7. put the cake in the fridge for 30 min. 
  8. Melt the chocolate in a bain-marie, place the cakes on a rack 
  9. place a piece of baking paper underneath, and pour melted chocolate over each cake to cover it. 



for the final decoration: I used a little icing sugar, red dye, and a little water, mix well to have a nice mix, put in a bag, or a paper cone, and decorate the cakes. 

for more cakes: [ gateau algerien ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)
