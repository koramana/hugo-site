---
title: minced meatballs
date: '2012-12-23'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/riz-aux-boulettes-de-viande-hachee-018_thumb1.jpg
---
##  minced meatballs 

Hello everybody, 

Delicious and tender minced meatballs in sauce, which I bake to accompany my rice or just to enjoy with a nice little piece of bread. 

I can tell you that this dish of oven-minced meatballs is often present at our place, because I am used every time I buy ground meat to make some meatballs that I put in the freezer, and when I do not know what to cook, I quickly make this dish. 

You can see [ how to cook and succeed rice ](<https://www.amourdecuisine.fr/article-comment-reussir-la-cuisson-du-riz.html>) on this link.   


**minced meatballs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/riz-aux-boulettes-de-viande-hachee-018_thumb1.jpg)

**Ingredients**

  * ingredients: 
  * 500 gr of minced meat 
  * 50 gr bread crumbs dipped in milk (which will give the tender side to minced meat) 
  * 2 crushed garlic cloves 
  * salt, black pepper. 
  * 1 pinch of cumin 
  * 1 onion 
  * 1 green pepper 
  * 1 red pepper (not spicy for me, but if you like spicy, put in a little) 
  * 1 can of chopped tomatoes, otherwise 4 fresh tomatoes 
  * olive oil 
  * ½ glass of water. 
  * cooked rice 



**Realization steps**

  1. start with the meatballs, mix the minced meat, garlic, crumb of bread drained of its milk, spices, and cumin. 
  2. form medium meatballs and leave aside. 
  3. in a baking pyrex mold, preferably rectangular, put a little oil. 
  4. cut the onion into strips, the peppers too, and put everything in the mold. 
  5. add the chopped tomato, crushed garlic clove, salt and black pepper. 
  6. mix well and garnish with the meatballs. 
  7. Pour on half the glass of water (about 120 ml). 
  8. cover the pyrex molds with aluminum foil, and bake in preheated oven at 200 degrees C, for 20 to 30 minutes, or depending on the capacity of your oven 
  9. if you like to have a lot of sauce to accompany your rice, get out of the oven. otherwise if you like a creamy sauce, remove the aluminum foil, and let reduce the sauce. 



serve hot with a little rice, or even a [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html>) . 

merci. 
