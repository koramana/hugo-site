---
title: Bread dripping with both cheeses
date: '2016-04-15'
categories:
- appetizer, tapas, appetizer
- cuisine diverse
- idea, party recipe, aperitif aperitif
- Mina el forn
- pain, pain traditionnel, galette
- pizzas / quiches / tartes salees et sandwichs
- Dishes and salty recipes
- recettes de feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/pain-degoulinant-au-fromage.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/pain-degoulinant-au-fromage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/pain-degoulinant-au-fromage.jpg>)

##  Bread dripping with both cheeses 

Hello everybody, 

I know, you've seen this recipe here and there on the blogosphere, but it does not matter ... This bread recipe dripping with two cheeses from Lunetoiles, has been hibernating in the archives of my computer since September 2013. An easy recipe to prepare, and especially fast and delicious ... 

You can prepare the casserole yourself, as you can use the commercial bread. This recipe is often present at home, we want a "snack", I realize this bread with all that I have as cheese in the fridge, besides you can see my recipe [ bread dripping cheese and mushrooms, ](<https://www.amourdecuisine.fr/article-pain-degoulinant-au-fromage-champignon.html>) too good. 

**Bread with two cheeses**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/pain-degoulinant-au-deux-fromages-1.jpg)

Recipe type:  Appetizer, appetizer  portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 1 loaf of uncut bread 
  * 200 g cheese: Raclette, Reblochon, Maroilles, Chimay, Gouda fruity, Comté, Emmental .... 
  * 1 bunch of chives and a few basil leaves 
  * 40 g of butter 
  * 1 tbsp. salt flower (optional) 



**Realization steps**

  1. Cut the loaf of bread into slices without going to the bottom, leave 1 cm. 
  2. In one direction, then in the other. 
  3. The second cut is more delicate, go easy and get a good bread knife. 
  4. Cut the cheeses into slices, without removing the crusts if there are any, then place them in each cut. 
  5. Chop the chives and basil and sprinkle the bread generously, spreading the squares aside so that the squares sink into the crumb. 
  6. Place the bread in a baking dish. 
  7. Put the butter to melt and brush the top of the bread. 
  8. Sprinkle with fleur de sel. 
  9. Put in the oven for 10 to 15 min at 200 ° C (th.7). 



[ ![dripping bread with two cheeses](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/pain-degoulinant-au-deux-fromages-1024x793.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/pain-degoulinant-au-deux-fromages.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
