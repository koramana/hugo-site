---
title: Baked sardines
date: '2013-05-28'
categories:
- Tunisian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/poisson-au-four-088_thumb1.jpg
---
![Baked fish 088](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/poisson-au-four-088_thumb1.jpg)

##  Baked sardines 

Hello everybody, 

This dish of Baked Sardines is one of the first dishes that I ask my mother to realize when I go to Algeria. 

My husband prefers fried sardines or [ sardines with chermoula ](<https://www.amourdecuisine.fr/article-sardines-farcies.html>) but this dish of sardines baked well with parsley is my favorite dish. 

Too bad here in England we do not find the fresh sardine of our beautiful Mediterranean to frequently make this dish of sardines baked parsley, a light dish, easy to prepare and especially super delicious.   


**Baked sardines**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/poisson-au-four-092_thumb1.jpg)

**Ingredients**

  * 500 grs of sardines 
  * 1 medium onion 
  * an average bunch of parsley 
  * salt pepper 
  * 3 to 4 tablespoons of table oil. 



**Realization steps**

  1. clean the sardines, remove the head and stop it, if you can not stand too, remove the tail. 
  2. chop the onion and parsley 
  3. season this mixture with salt and black pepper 
  4. in an ovenproof dish of 18 cm diameter, put the oil 
  5. put a first layer of fish 
  6. sprinkle over a little onion / parsley mixture 
  7. cover with another layer of sardines 
  8. sprinkle the parsley / onion mixture again 
  9. continue until the exhaustion of the sardine 
  10. put a lid on your dish, and place in a medium-temperature oven. 
  11. cook for 20 minutes while watching, 
  12. remove the lid, and let reduce the sauce a little 
  13. serve hot, garnished with a little parsley, and lemon juice, if you do not like too much fat 



merci pour votre visite, 
