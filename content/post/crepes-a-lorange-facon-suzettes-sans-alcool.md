---
title: Pancakes with orange way suzettes without alcohol
date: '2018-02-01'
categories:
- bakery
- crepes, donuts, donuts, sweet waffles
- sweet recipes
tags:
- Sweet crepes
- Pancake batter
- Candlemas
- To taste
- Easy cooking
- Algerian cakes
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/cr%C3%AApes-%C3%A0-lorange-fa%C3%A7on-suzette-5.jpg
---
![Pancakes with orange way suzettes without alcohol](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/cr%C3%AApes-%C3%A0-lorange-fa%C3%A7on-suzette-5.jpg)

##  Pancakes with orange way suzettes without alcohol 

Hello everybody, 

My story with these non-alcoholic orange pancakes is 15 years ago, when I was in Strasbourg, and out among colleagues, we went to a creperie well known Laba. I remember that beautiful smell that came out, um! 

All my colleagues had asked for crispy crepes, when I asked the seller how they were prepared and he told me: outbreaks in alcohol, I was super disappointed, I ordered Nutella pancakes, but I devoured with my eyes the crispy pancakes of my colleagues. To remedy this, I often prepare my orange crepes way suzettes without alcohol, my way. I do not know if it looks like the original recipe, but one thing is for sure, my orange pancakes are too good, and no one ever ate them at home, without asking for the recipe ... They were even the star of a picnic outing with moms last year, warmed up at the barbeque ... it was a treat. 

Only thing with the photos that I share with you, I am disappointed that the photos do not reflect all the beauty of these pancakes, it was so gray when taking a photo, that I find not beautiful with the light of nothingness that my husband was just to have good pictures ... But in the end I do not like all this light over my pancakes (to be done again when it will be more beautiful) 

{{< youtube pi1BdpMTtLc >}} 

**Pancakes with orange way suzettes without alcohol**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/cr%C3%AApes-%C3%A0-lorange-fa%C3%A7on-suzette-6.jpg)

portions:  2  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 4 [ plain pancakes ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee.html>)
  * freshly squeezed juice of 3 oranges 
  * finely peeled zest of 1 orange 
  * orange zest to decorate 
  * 3 c. tablespoons sugar 
  * 2 tbsp. butter 
  * vanilla extract 



**Realization steps**

  1. place a heavy bottom pan, add orange juice, orange zest, sugar and vanilla extract. 
  2. let it boil until it becomes a little honeyed 
  3. add the butter and let it return for another 2 minutes. 
  4. take back the ⅔ of this syrup and leave the rest in the pan 
  5. pour a little syrup in a large dish, place a pancake and let it absorb this syrup. 
  6. fold the pancake on 4 and put it back in the pan. 
  7. continue in the same way with the rest of the pancakes. 
  8. let it boil a little. 
  9. and present your pancakes filled with a little lemon zest, to taste immediately. 



![pancakes with orange style suzette 4](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/cr%C3%AApes-%C3%A0-lorange-fa%C3%A7on-suzette-4.jpg)
