---
title: Indian recipes
date: '2012-03-26'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/chicken-tandoori-poulet-tandoori-1_thumb.jpg
---
![chicken tandoori chicken tandoori 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/chicken-tandoori-poulet-tandoori-1_thumb.jpg)

Hello everybody, 

Indian cuisine is always a kitchen to explore to infinity, spices that we will never know enough, there are so many, and among real Indians, the spices they use are sometimes specific to each family and this prepared at the time of realization of the dish, which can give a touch more to a same dish. 

so today, I try to regroup the few delicious recipes that I had the chance to make, and there will be even more in the future: 

[ ![Indian rice cream, Phirni](http://amour-de-cuisine.com/wp-content/uploads/2012/03/creme-de-riz-indienne-phirni.160x120.jpg) Indian rice cream, Phirni  ](<https://www.amourdecuisine.fr/article-creme-de-riz-indienne-59532236.html> "Indian rice cream, Phirni of cooking love at soulef") 03/26/12 20:12  Cream of Indian Rice with Cardamom and Nutmeg, or Phirni badam hello everyone, an Indian recipe is always a pleasure to taste, and when it's a dessert, it's probably even better, so my story with this dessert is that ... 

[ ![Very delicious Indian rice](http://amour-de-cuisine.com/wp-content/uploads/2012/03/riz-indien-tres-delicieux.160x120.jpg) Very delicious Indian rice  ](<https://www.amourdecuisine.fr/article-un-riz-indien-tres-delicieux-48188385.html> "Very delicious Indian rice of love of cooking at soulef") 9/30/11 8:07 AM  hello everyone, a photo not level .... I know it, but this dish super good I prepared them the day I invited nawel home, must I tell you that this is the photo I did just at the end of cooking the last key before to put ... 

[ ![Samoussa has chopped meat, Samoussas](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/samoussa-a-la-viande-hachee-samoussas.160x1201.jpg) Samoussa has chopped meat, Samoussas  ](<https://www.amourdecuisine.fr/article-samoussa-a-la-viande-hachee-samoussas-88499027.html> "Samoussa has chopped meat, Samoussas of cooking love at soulef") 11/11/11 13:20  hello everybody, since Ramadan, I had not made bourak, brig or samosas, and last week, my husband had to buy bricks leaves, and she asked me to do some boureks, I did not want to do some boureks at the ... 

[ ![Rice balls with tuna](http://amour-de-cuisine.com/wp-content/uploads/2012/03/boulettes-de-riz-au-thon.160x120.jpg) Rice balls with tuna  ](<https://www.amourdecuisine.fr/article-boulettes-de-riz-au-thon-97137268.html> "Rice dumplings with tuna of cooking love at soulef") 1/15/12 10:34 AM  hello everyone, here is a recipe that made me an eye that I went to Fimere, I love rice and peanuts, well this recipe was really for me, and there is not that it There is tuna in it, these bites were too good ... 

[ ![Coconut vanilla chicken](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/poulet-coco-vanille.160x1201.jpg) Coconut vanilla chicken  ](<https://www.amourdecuisine.fr/article-poulet-coco-vanille-100441086.html> "Coconut vanilla chicken of kitchen love at soulef") 2/29/12 13:39  hello everyone, Want new flavors, want a different touch to your kitchen, want an exotic escape by the cold, towards the end of the world and the sun with a very nice recipe of Reunion, very tasty, well scented, ... 

[ ![Naans with coriander](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/naans-a-la-coriandre.160x1201.jpg) Naans with coriander  ](<https://www.amourdecuisine.fr/article-les-naans-a-la-coriandre-66684378.html> "Naans to the coriander of kitchen love at soulef") 2/8/11 9:30 AM  Hi everyone, I really like naans, these delicious Indian cakes, very tender, and full of tastes. Yesterday I had to prepare a delicious Chorba, and I wanted to accompany him ... 

[ ![Tandoori chicken](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/poulet-tandoori-100.160x1201.jpg) Tandoori chicken  ](<https://www.amourdecuisine.fr/article-poulet-tandoori-85541703.html> "Tandoori chicken of kitchen love at soulef") 01/10/11 10:06  hello everyone, the chicken is always present at home, and we prefer it to us more than meat, and my husband always asks me to make a different recipe. moreover, many of you have loved KFC CHICKEN, CHICKEN ... 

[ ![Indian Samoussa](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/samoussa-indienne.160x1201.jpg) Indian Samoussa  ](<https://www.amourdecuisine.fr/article-samoussa-indienne-97307661.html> "Indian love Samoussa cooking at soulef") 1/17/12 3:36 PM  hello everyone, for an Indian menu, I searched on the internet and I find this delicious recipe of Indian samousa, at Meriem I really like, the composition of the dough, and especially the stuffing, so it was for me the ideal recipe ... 

[ ![Chicken Tikka](http://recettes.de/images/blogs/amour-de-cuisine-chez-soulef/poulet-tikka.%0A160x120.jpg) Chicken Tikka  ](<https://www.amourdecuisine.fr/article-poulet-tikka-99061936.html> "Chicken tikka of kitchen love at soulef") 2/14/12 12:50 PM  hello everyone, a traditional recipe from India spice country. Like most Indian recipes, the chicken is spicy but not spicy, a recipe that prepares in two to three strokes, and after a good time in the marinade, you will be able to ... 

[ ![Indian sauce chicken](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/poulet-sauce-indienne.160x1201.jpg) Indian sauce chicken  ](<https://www.amourdecuisine.fr/article-poulet-sauce-indienne-100546187.html> "Chicken indian sauce of kitchen love at soulef") 01/03/12 18:17  hello everyone, here is a delicious dish improvised for today's lunch, mashed potatoes, my husband loves a lot, but not children, so I enjoy that they have a little bit of yesterday's dinner, so I wanted to accompany the mashed potatoes with a good ... 
