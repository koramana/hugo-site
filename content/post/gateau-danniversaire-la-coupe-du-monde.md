---
title: birthday cake the world cup
date: '2014-07-31'
categories:
- birthday cake, party, celebrations

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-danniversaire-rayan-067.CR2_.jpg
---
_[ ![birthday cake rayan 067.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-danniversaire-rayan-067.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-danniversaire-rayan-067.CR2_.jpg>) _

##  _birthday cake the world cup_

_Hello everybody,_

_Yes I know, you will tell me: you are still in the area of ​​the world cup, I will tell you: well no, it's not me, but when you have a little boy who has only 9-year-old, who for the first time was able to follow Aa Z's football world cup, we can expect his birthday cake to be on the subject, is not it?_

_Well, that's the cake I gave him, though I said it was impossible for me to make a birthday cake, but my son insisted so much, he even cried, that I could not resist , and despite the fatigue, and despite that I just rested sleepless nights during the preparations of[ algerian cakes of the Aid el Fitr 2014 ](<https://www.amourdecuisine.fr/article-mes-gateaux-algeriens-aid-el-fitr-2014.html> "My Algerian cakes Aid el fitr 2014") ... I realized for my son and his dad, this birthday cake, yes a Birthday 2 in 1, lol ... _

_My son was 9 years old on the day of the feast of aid el fitr, July 28th, and it was impossible for me to make him a cake today ... the anniversary of his step, it is today And although my husband does not like to celebrate his birthday, I made them that cupcake just for the sake of tasting it together, not for partying._   


**birthday cake the world cup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-danniversaire-rayan-067.CR2_.jpg)

portions:  20  Prep time:  60 mins  cooking:  60 mins  total:  2 hours 

**Ingredients**

  * [ fine cake ](<https://www.amourdecuisine.fr/article-biscuit-fin-base-pour-gateau-danniversaire.html> "Biscuit end / base for birthday cake")
  * [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline.html> "muslin cream \(ideal for strawberry\)")
  * [ hazelnut praline ](<https://www.amourdecuisine.fr/article-pralin-aux-noisettes.html> "praline with hazelnuts")

decoration: 
  * sugarpaste 
  * jelly 
  * green dye 
  * yellow dye 
  * red dye 



**Realization steps**

  1. prepare 3 biscuits 
  2. prepare the [ custard ](<https://www.amourdecuisine.fr/article-creme-patissiere.html> "custard") with 500 ml of milk 
  3. prepare the praline with hazelnuts 
  4. assemble the cream muslin, whipping the cream pastry with 250 gr of good quality butter (I used the Algerian margarine brand Mani) 
  5. towards the end, add the hazelnut praline 
  6. assemble the cake: a layer of biscuit, a layer of muslin cream with hazelnut praline, a biscuit layer, a layer of muslin cream with hazelnut praline, a layer of biscuit and finally a layer of hazelnut muslin cream. 
  7. spread the dough on a surface covered with icing sugar, turn the dough each time, so that it does not stick to the work surface. 
  8. cover the cake with the sugar dough. 
  9. draw the drawing of your choice on the cake, 
  10. fill the contours with sugar diluted in a little water. 
  11. fill the decoration with colored jelly according to your choice. 
  12. Keep chilled until serving. 



_[   
![birthday cake rayan 066](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-danniversaire-rayan-066.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-danniversaire-rayan-066.jpg>) _
