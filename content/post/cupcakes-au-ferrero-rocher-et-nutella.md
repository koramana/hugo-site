---
title: ferrero and nutella cupcakes
date: '2015-12-19'
categories:
- Cupcakes, macaroons, and other pastries
- gateaux, et cakes
- recettes sucrees
tags:
- Easy cooking
- desserts
- Pastry
- Algerian cakes
- Inratable cooking
- To taste
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/cupcakes-au-ferrero-rocher-2.jpg
---
[ ![cupcakes with ferrero rock 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/cupcakes-au-ferrero-rocher-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/cupcakes-au-ferrero-rocher-2.jpg>)

##  ferrero and nutella cupcakes 

Hello everybody, 

It's cupcakes are a bombshell of gluttony, do not come tell me: Soulef I got fat by eating these cupcakes .... hihihihi, perso I will eat them with their eyes closed so as not to feel too guilty, and if it goes wrong (not to say it turns round), well I'm going to blame kiko, ok, hihihihih   


**ferrero and nutella cupcakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/cupcakes-aux-ferrero-rocher-et-nutella-1.jpg)

portions:  12  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients** for cupcakes 

  * 1 glass of flour 
  * 1 glass of sugar 
  * ¾ bitter cocoa glass 
  * 1 teaspoons of baking powder 
  * Coffee bicarbonate 
  * A coffee of salt 
  * 3 tablespoons of oil 
  * ¾ glass of fermented milk 
  * ¾ glass of hot milk 
  * 2 eggs 
  * 1 teaspoon liquid vanilla 
  * 24 pieces of ferrero rock. (you can replace with a chocolate of your taste) 

decoration: 
  * [ nutella butter cream ](<https://www.amourdecuisine.fr/article-creme-au-beurre-au-nutella.html>)



**Realization steps**

  1. in the bowl of the kneader, place all the dry ingredients 
  2. add the eggs and the oil and mix 
  3. add while mixing, fermented milk and water (paste and liquid) 
  4. preheat the oven to 180 degrees C 
  5. place the boxes in muffin cups 
  6. fill them up with almost half 
  7. place a piece of ferrero rock in the middle   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/etapes-de-pr%C3%A9paration-des-cupcakes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/etapes-de-pr%C3%A9paration-des-cupcakes.jpg>)
  8. still cover with dough, to have ¾ filled boxes 
  9. bake for 20 minutes 
  10. after cooking and cooling the cupcakes, garnish them with [ nutella butter cream ](<https://www.amourdecuisine.fr/article-creme-au-beurre-au-nutella.html>) using a pastry bag. 



[ ![cupcakes with ferrero rock 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/cupcakes-au-ferrero-rocher-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/cupcakes-au-ferrero-rocher-3.jpg>)
