---
title: Verrines hummus tomatoes cucumber carrots
date: '2011-09-09'
categories:
- amuse bouche, tapas, mise en bouche
- gateaux algeriens sans Cuisson
- gateaux algeriens- orientales- modernes- fete aid
- Chocolate cake
- sweet recipes

---
hello everyone, here are very very good verrines, which I find while browsing the internet, but to be frank I do not remember, if it was on a site or a blog, or a forum, so please if it's your recipe, I'll be happy to put the link on it. otherwise, very simple, very presentable, and above all too good. for ingredients, you will need: hummus (knowing that for the ingredients, you must see according to the number of people) fresh tomato cucumber olives black olives (I did not have any) carrot lemon juice & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.65  (  2  ratings)  0 

Hello everybody, 

here are very very good verrines, which I find while browsing the internet, but to be honest I do not remember, if it was on a site or a blog, or a forum, so please if it's your recipe, I'll be happy to put the link on it. 

otherwise, very simple, very presentable, and above all too good. 

for ingredients, you will need: 

  * hummus (knowing that for the ingredients, you must see according to the number of people) 
  * fresh tomato 
  * cucumber 
  * black olives (I did not have any) 
  * carrot 
  * lemon juice 
  * olive oil and salt 


  1. start by putting a small layer of hummus in your verrines 
  2. prepare the vinaigrette, with the lemon juice, the olive oil and the salt 
  3. cut the tomato into a cube, and put it in the second layer, season with the vinaigrette 
  4. cut the cucumber into cubes, and put it in the 3rd layer, season. 
  5. cut the olives in a slice and put all around in the verrines 
  6. grated carrots, season and decorate the center of verrines 
  7. decorate to your taste, and cool until serving 



thank you for your visits and comments 

bonne journée 
