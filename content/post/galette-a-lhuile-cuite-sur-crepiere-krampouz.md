---
title: Galette with oil cooked on crepiere Krampouz
date: '2008-03-15'
categories:
- appetizer, tapas, appetizer
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/03/44210-31.jpg
---
you see this beauty: it's my first birthday present (I'm waiting for the second one) so since the time I go to bigmumy, I loved her crepe pantry on which she could cook all her delights: Algerian crepes, cakes kabyles, roast pratas .... and so on, the list is very long, one day I ask her she cooked on what, and she tells me about the pancake ........... yes the pancake is sold in France, but in England .........: ((I looked is finally found on ebay, and my husband ordered it for my birthday, and it was a great, good gift & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/44210-31.jpg)

you see this beauty: 

it's my first birthday present (I'm waiting for the second one) 

yes the crepe is sold in France, but in England .........: (( 

I searched for it and finally found it on ebay, and my husband ordered it for my birthday, and it was a great, good gift (my cake made its effect) 

Yesterday, I received my crepe maker and since then I have not stopped taking it out of her box to do this or that thing, firstly practical, because we place it where we want it does not warm up from below (so even on a table, it's good to cook and watch TV, hahahahaha. 

and she is light, and cooks like a real tri-foot gas (that of my mother misses me a lot, but I love my crepe) 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/232414651.jpg)

I put it on my stove, I can tell you my galette was cooked in me of 10 min 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/232414701.jpg)

oh yes the recipe for the cake, I had to talk about that: 

So for the ingredients: (I did not measure so much I wanted to inaugurate my crepe maker) 

  * the average semolina (300 gr for a cake) 
  * salt (1/2 cac you can in any case test the taste) 
  * oil (1/2 teaspoon) 
  * 1/2 baking yeast (or chemical, just to give lightness to the cake) 
  * some water 



mix the semolina well with the oil, add the salt, mix well to incorporate, then add the yeast and wet with water to have a malleable paste. 

knead a little, tearing the dough to avoid the elastic aspect of the cake. 

let a little rest, turn on my crepe maker (sorry heat your cookware) I use on thermostat 5. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/232414791.jpg)

and here is a beautiful galette cooked in a fraction of time. 

I admit that since I am here in England I have not cooked this little delight of our dear Algeria. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/232414841.jpg)

so this galette goes wonderfully with everything, even if you have guests, I swear she will be enjoying with a nice cup of milk. 

me anyway I like a lot 

bonne Appétit 
