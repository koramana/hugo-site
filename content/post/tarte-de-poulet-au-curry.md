---
title: curry chicken pie
date: '2017-09-15'
categories:
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
tags:
- quiches
- Salty pie
- Easy cooking
- Fast Food
- Algeria
- Express cuisine
- Full Dish

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/09/tarte-de-poulet-au-curry-2.jpg
---
![curry chicken pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/tarte-de-poulet-au-curry-2.jpg)

##  curry chicken pie 

Hello everybody, 

I love making quiches and pies salty for lunch, because my children do not like too much, and the fact that with the English school system, they do not come back for lunch, so I always take the opportunity to make a starter, and a simple salad, with what was left of dinner, and here we are with a well-stocked lunch table. 

This time, I had a little bit of chicken breast, after making some [ chicken fajitas ](<https://www.amourdecuisine.fr/article-fajitas-poulet-wraps-tortillas-poulet.html>) for the evening, so I wanted to make a quiche with a flavor that I like, curry! and the chicken curry quiche, or the salty chicken curry pie, was a frank success, my husband in any case did not give me the chance to take pictures of the whole pie, he had started it on when I prepared my scene to make the pictures !!!   


I accompanied this chicken curry pie, with a [ mashed carrot salad ](<https://www.amourdecuisine.fr/article-salade-puree-de-carottes.html>) and what are we enjoying this accompaniment! 

**curry chicken pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/tarte-de-poulet-au-curry.jpg)

**Ingredients** the shortbread 

  * 250 grams of flour 
  * 120 grams of butter in pieces 
  * 1 egg yolk 
  * ½ cup of sugar 
  * ½ teaspoon of salt 
  * 50 ml of cold water (+ or - as needed) 

for the stuffing: 
  * 1 chicken breast 
  * 2 chopped olives 
  * chopped coriander 
  * salt, black pepper, curry powder 
  * 150 ml of fresh cream 
  * 3 eggs 
  * 2 tbsp. tablespoon of olive oil 
  * gruyere grated (optional) 



**Realization steps**

  1. in the bowl of a blender, put the flour and butter in pieces, mix 
  2. add the egg yolk, sugar, and salt, then mix again 
  3. add the water in small quantities and mix, until the dough forms a ball (go there gently with the water) 
  4. Fry the chopped onions in the olive oil until they are well melted. 
  5. Add chicken breasts cut into small cubes 
  6. Add the curry, salt and pepper, cook until the meat is golden and the sauce is well reduced. 
  7. In a salad bowl, whisk the eggs with the cream. Add salt and pepper to taste. 
  8. Spread the shortcrust pastry in a pie pan, pour the chicken mixture. 
  9. garnish with chopped coriander and egg and cream mixture. 
  10. you can garnish with a little gruyere grated 
  11. Bake at 180 ° C for almost 45 minutes or depending on the capacity of your oven. 



![curry chicken pie 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/tarte-de-poulet-au-curry-1.jpg)
