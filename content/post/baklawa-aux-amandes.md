---
title: Baklawa with almonds
date: '2012-02-16'
categories:
- Index
- recette de ramadan
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/02/baklawa161.jpg
---
Baklawa with almonds the measure being a glass The dough: 12 measure of flour (for me 1 kg of flour) 1 measure of semolina very fine 2 measure of smen 2 measure of water added rose water 1 pinch of salt The stuffing : 800 gr of almonds roasted and finely chopped 270 gr of icing sugar Rose water 1kg of honey Butter Prepare the dough the day before: Mix the flour, salt and then smen and lastly water with rose water , until a homogeneous and flexible dough is obtained. Let stand 15 min, then the & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![baklawa16.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/baklawa161.jpg)

**Baklawa with almonds** the measure being a glass 

Dough:   
12 measure of flour (for me 1 kg of flour)   
1 measure of very fine semolina   
2 smen measure   
2 measure of water with rose water   
1 pinch of salt   
The joke :   
800 gr roasted and finely chopped almonds   
270 gr icing sugar   
Rose water 

1kg of honey   
Butter 

![baklava-17.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/baklawa-171.jpg)   
Prepare the dough the day before:   
Mix the flour, the salt then the smen and lastly the water added with rose water, until obtaining a homogeneous and supple paste.   
Let stand 15 min, then knead with the hands, and divided into 13 equal balls. Wrapped in a film paper and left to rest overnight. 

The joke :   
Mix almonds, icing sugar and rose water, mixing well. 

![baklava-18.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/baklawa-181.jpg)

Coat a round tray with melted butter using a brush. Take a ball of dough and spread it very finely until it becomes almost transparent by sprinkling it with mazena, when it sticks.   
And put it in the tray being careful not to wrinkle it. Coat with melted butter with a brush, then do the same again, you need to get 7 leaves at the bottom and 6 leaves at the top.   
Once the 7th sheet, spread the stuffing carefully with the back of a tablespoon. Then take a ball of dough and redo the same way as the previous sheets.   
At the last leaf, let dry for 15 minutes, then cut into a diamond shape.   
Prick each diamond of an almond and coat with melted butter.   
Bake medium for almost an hour. 

A few minutes before the end of cooking, heat the honey and sprinkle the whole cake at the end of the oven.   
Let it sit in the honey for 24 hours.   
Cut out and store in boxes. 

![baklawa9.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/baklawa91.jpg)

thanks to lunetoiles 

thank you for all your comments 

and thank you for subscribing to my newsletter if you have not done so yet. 

j’attends toujours les photos de vos gateaux de l’Aid. surtout si vous avez essayer une de mes recettes. 
