---
title: extra crisp homemade granola
date: '2014-09-25'
categories:
- diverse cuisine
- dessert, crumbles and bars
- sweet recipes
tags:
- Breakfast
- muesli
- Cereal
- Vegan cuisine
- Vegetarian cuisine
- Kitchen without Milk
- Kitchen without egg

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/granola-fait-maison-extra-croustillant-002.jpg
---
[ ![crunch granola or extra crisp homemade granola 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/granola-crunch-ou-granola-maison-extra-croustillant-2.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/granola-crunch-ou-granola-maison-extra-croustillant-2.png>)

##  extra crisp homemade granola 

Hello everybody, 

So early in the morning, what do you like for your breakfast? A croquette with a thin layer of extra-light butter, or a half-baguette spread well with jam and butter .. A small croissant, or a large piece of cake ... A glass of fresh milk or a good thick smoothie ... Or maybe you are having your English breakfast, do you know the English breakfast? It tells you early in the morning a dish filled with white beans in red sauce, with two sausages, an egg on a plate ... and many other things aside until you do not know, is it 6 o'clock in the morning, or it's already noon ??? !!! that's the English breakfast, lol ... 

For my part, I will surely not opt ​​for the last choice. Usually, since I'm here in England, I like a Muesli, with some fresh cut strawberries, and some dry cranberries, but I'm going to make you laugh, I never put milk in, no will be a spoon, and a glass of milk to accompany him. 

Strange as a way to eat cereals, but to be honest, I like crispy, I really like to feel those pieces of cereal crunched in my mouth, it's my morning anti-stress melody. I close my eyes, and it's as if I'm going to run in a big field of wheat, and this very gentle wind that makes these ears dance, and create this beautiful melody, so close your eyes and run with me, open well your arms to feel those sweet stalks of wheat caress your hand. Do not open your eyes, otherwise you will see, that your son has spilled his glass of milk playing with these small figures of football table, or maybe you will see your husband who jealously eats a big piece of cake, without having the worry of putting on weight (how do you know how to keep the line)? 

I never put milk in my Mueslis, because it could make them lose all that crunchy, and I would have a porridge, that I will eat quickly, just to finish the plate and have a little boost to do the household that will only wait for me to do it .... 

But in all that, I did not know that you could make muesli, or granola at home, and that this granola will remain crispy, even with milk (no need to dirty your glass then, lol) 

So thank you very much Lunetoiles who gave me the recipe for this extra crisp homemade granola, which since I got the recipe, I now realize my own granola, with all the flavors that I like, a choice to infinite for a healthy breakfast. 

**extra crisp homemade granola**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/granola-fait-maison-extra-croustillant-002.jpg)

portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 4 cup oats flakes (330 g) 
  * 2 cups of nuts   
(for me a mix of walnuts, almonds, pecan nuts, hazelnuts, cashews) (240 g) 
  * ½ cup of maple syrup (100 ml) 
  * ½ cup of melted coconut oil (or your favorite oil) (100 ml) 
  * 2 teaspoon of vanilla extract 



**Realization steps**

  1. Preheat the oven to 160 ° C. 
  2. Line a high-sided baking sheet with a sheet of baking paper and set aside. 
  3. In a large salad bowl, mix the oatmeal and the nuts. 
  4. In a bowl, mix the coconut oil (melted) with the maple syrup, and the vanilla, then pour it over the oatmeal / walnut mixture. 
  5. Stir the oatmeal mixture to coat all the dry ingredients and pour into the plate. 
  6. Spread the mixture press flat with a spatula. 
  7. Bake for about 40 minutes or until the edges begin to brown. 
  8. You can stir every 15 minutes. 
  9. Cool the granola thoroughly, then transfer to a large, airtight container for storage. 



[ ![crisp homemade granola](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/granola-maison-croustillant-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/granola-maison-croustillant-1.jpg>)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
