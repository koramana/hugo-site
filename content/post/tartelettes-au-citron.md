---
title: tartlets with lemon
date: '2010-05-28'
categories:
- vegetarian dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267511.jpg
---
![tartocitron](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267511.jpg) ![trtocitron](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267561.jpg)

a girl asked me the recipe for lemon tartlets that she had already tried and that she loved, but that she did not find, so I go back to it, with pleasure, knowing that in the right column there is a search module, you just type the title of the recipe, or an ingredient of the recipe, and it will give you a lot of articles among which you will surely find what you are looking for. 

No, no, I have not forgotten the meringue, but that's how my husband loves these little lemon tartlets. 

then the ingredients: 

** for the shortbread: (de celine)  **

  * _ 250g of flour  _
  * _ 125 g cold butter in small pieces  _
  * _ 1 egg yolk  _
  * _ 2 tablespoons sunflower oil  _
  * _ 2 tablespoons of milk  _
  * _ 2 tablespoons of sugar.  _
  * _ 1 pinch of salt.  _



Sand the flour between the fingers with the cold butter. Mix with your fingertips until you get a sand. 

![trtocitron10](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267621.jpg) ![tartocitron1](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267721.jpg)

Then add the sugar and the salt. Mix quickly.   
Add the egg yolk, oil and milk and mix everything together. Knead quickly (do not over-knead the dough to obtain a beautiful sanded texture) until you obtain a beautiful dough that is curled up.   
If you get a nice dough a little firm, you can use it immediately, otherwise leave the few hours in the fridge 

![trtocitron9](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267791.jpg)

(take it out a good half hour before using it to make it easier to spread). 

![trtocitron8](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267831.jpg) ![trtocitron7](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267861.jpg)

The dough is baked in a preheated oven at 180 degrees, for 15 minutes, or according to your oven. 

** for lemon cream:  **

  * 1/4 l of water or milk for me. 
  * 100ml of lemon juice. 
  * 30g of cornflour. 
  * 100g of sugar. 
  * 3 egg yolks. 
  * 1 zest of lemon. 
  * 50g of butter. 
  * 1 pinch of salt. 



![trtocitron5](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267911.jpg)

mix the egg yolks with the sugar, then add the lemon peel and cornflour meanwhile heat the milk with the lemon juice without boiling, pour a little milk on the egg yolks to dilute the preparation and transfer all to the saucepan and mix over medium heat until the cream has thickened, add the butter in small pieces and allow to cool. 

![trtocitron1](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267931.jpg) ![trtocitron4](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318267951.jpg)

![trtocitron13](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318268001.jpg) ![trtocitron2](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318268051.jpg)

In any case, it's a real treat, with this soft, sandy paste that crumbles easily on the touch of the teeth, and with a lemon cream, just acid according to taste, neither too strong nor normal, a good acid taste that hides the sugar that is there. 

![trtocitron3](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/318268151.jpg)

as always, I find myself helpless in the choice of photos, I love them all (well, luckily I did not put everything to you) 

so forgive me if I do too much. 

Good tasting. 
