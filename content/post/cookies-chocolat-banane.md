---
title: Banana chocolate cookies
date: '2016-12-29'
categories:
- Gateaux Secs algeriens, petits fours
tags:
- To taste
- Algerian cakes
- desserts
- Cakes
- Easy cooking
- biscuits
- Gourmet gift

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/cookies-chocolat-banane-001.jpg
---
##  Banana chocolate cookies 

Hello everybody, 

here are sublime chocolate and banana cookies to share with us my friend Lunetoiles, 

very simple, very easy to achieve, and especially for you get rid of this banana in the fruit basket, which begins to take this brown color, and nobody wants to eat ... ..   
so to you the recipe:   


**Banana chocolate cookies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/cookies-chocolat-banane-001.jpg)

portions:  16 

**Ingredients**

  * 300 g of flour 
  * 125 g of butter 
  * 120 g of sugar 
  * 1 egg 
  * ½ packet of baking powder (7 g) 
  * Nuggets or small pieces of chocolate 
  * A ripe banana 
  * A pinch of salt 



**Realization steps**

  1. In a salad bowl, mix the flour with the baking powder, 
  2. then add the sugar, pinch of salt 
  3. and finally add the chocolate chips. Whisk. 
  4. In a bowl, crush the ripe banana with a fork, and add it to the previous mixture. 
  5. Melt the butter, beat the egg for a few seconds with a fork and mix the beaten egg with the butter and pour over the previous preparation 
  6. Mix well. Form a ball, and let it cool for 30 minutes. 
  7. Flour your hands, and take a small ball of dough the size of an apricot and flatten it. 
  8. Place on a baking sheet lined with parchment paper spacing enough. 
  9. Bake in a preheat oven at 180 ° C for 10 minutes. 
  10. At the exit of the oven, let cool them then take off the plate and keep them in a box. 
  11. Enjoy yourself !!! 



thank you my dear lunetoiles for the recipe 

thank you for the comments 

et si vous aimez le blog, ca va me faire plaisir de vous voir abonner a la newsletter 
