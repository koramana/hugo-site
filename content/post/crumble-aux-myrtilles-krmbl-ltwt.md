---
title: Blueberry Crumble كرامبل التوت
date: '2016-08-05'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crumble-aux-myrtilles-017.CR2_1.jpg
---
#  ![blueberry crumble 017.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crumble-aux-myrtilles-017.CR2_1.jpg)

##  Blueberry Crumble كرامبل التوت 

Hello everybody, 

You will believe me if I tell you that I buy frozen blueberries each time, to taste them just like that, without sugar or anything else. They are too good, better than fresh blueberries when I buy them off season of course! 

But as I always like to share with you good, delicious and easy recipes, I share with you this delicious blueberry crumble, not only is it easy, it's also super good. 

**Blueberry Crumble كرامبل التوت**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crumble-aux-myrtilles-013.CR2-copie-11.jpg)

Recipe type:  dessert  portions:  4  Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients**

  * 2 cups frozen blueberries (1 cup 250 ml) 
  * zest and juice of 1 lemon 
  * ⅓ cup of sugar 
  * 2 tablespoons of cornflour 

for the crumble: 
  * 1 cup flour 
  * ⅓ cup of sugar 
  * ¼ cup brown sugar 
  * 1 pinch of salt 
  * 1/4 teaspoon of cinnamon 
  * ½ cup of cold butter, diced (knowing that you are going to have a surplus of crumble dough, for my part I put a lot in my verrines) 



**Realization steps**

  1. Preheat the oven to 180 degrees C. 
  2. Mix the blueberries, lemon juice, lemon peel, sugar, cornflour and let sit for a few minutes so that the blueberries become juicy. 
  3. fill your ramekins. 
  4. prepare the crumble, mix the flour, sugar, brown sugar, salt and cinnamon together with a fork. 
  5. Sprinkle the crumble on the blueberries and place the ramekins on a baking sheet, covered with parchment paper. 
  6. Cook between 30 and 35 minutes, or until crumbles turn golden. 
  7. Serve hot with whipped cream, vanilla ice cream, or just plain! No matter how you eat it, just dig and enjoy! 



![crumble-to-blueberry-022.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/crumble-aux-myrtilles-022.CR2_1.jpg)
