---
title: Lebanese tabouleh recipe
date: '2013-08-23'
categories:
- birthday cake, party, celebrations
- cakes and cakes
- basic pastry recipes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/salade-008a_thumb1.jpg
---
Hello everybody, 

this Lebanese salad, the Lebanese tabouleh is a delice, to present as a buffet, as a starter, on a picnic outing, or on your Ramadan tables, and on this article I have compiled an index of [ ramadan recipe ](<https://www.amourdecuisine.fr/article-menu-ramadan-plats-chorbas-et-entrees-55268353.html>) . 

without delay I pass you the ingredients:   


**Lebanese tabouleh recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/salade-008a_thumb1.jpg)

Recipe type:  salad  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 3 tablespoons of bulgur (or instead crushed wheat) 
  * ½ pepper 
  * ½ cucumber 
  * 2 fresh medium tomatoes 
  * 1 handful of pitted green olives 
  * ½ onion 
  * 10 strands of parsley 
  * the pressed juice of a lemon (or according to taste) 
  * 2 to 3 tablespoons of olive oil 
  * salt 



**Realization steps**

  1. place the bulgur in a bowl, add the boiling water over 
  2. let stand 5 min 
  3. drain and boil in moderately salted water 
  4. when it's soft, drain and let cool 
  5. wash your vegetables, and cut them into small ones 
  6. cut the olives into slices 
  7. mix all your ingredients in a salad bowl 
  8. season to taste, with salt, olive oil, and lemon juice 



enjoy this salad, with bread, or without bread, present it as a starter, or for hot days, present it with a barbecue, this salad is a real delight. 

to eat without moderation, hihihihihiihih 

thank you for your visits and comments 

merci a tout ceux qui continuent de s’abonner a ma newsletter, ca me fait réellement plaisir, tout ces gens qui veulent être a jour avec toute nouvelle publication chez moi. 
