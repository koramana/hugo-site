---
title: Algerian cakes Eid el fitr 2017
date: '2016-07-06'
categories:
- Coffee love of cooking
- gateaux algeriens- orientales- modernes- fete aid
- Index
tags:
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/gateaux-alg%C3%A9riens-aid-2016.jpg
---
![Algerian cakes help 2017](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/gateaux-alg%C3%A9riens-aid-2016.jpg)

##  Algerian cakes Eid el fitr 2017 

Hello everybody, 

Sahha Aidkoum! It is with great joy that we welcome today Aïd el fitr 2017. After spending a very pleasant month, the holy month of Ramadan. A month that is a great opportunity for us to get closer to the good god. Insha'Allah Siyam maqboul, may the good God accept our fast, and forgive our sins, 

Thank you to my readers who were very loyal to the blog, you were many, and I hope you find on my blog, what you were looking for. 

On the occasion of this blessed day, I say to you, Happy feast of Eid el fitr. I wish all happiness, health and success to everyone. 

Insha'Allah, our Muslim brothers in Gaza and Syria are finding peace and serenity, and hoping for peace and brotherhood everywhere in this world, Amine. 

![help image](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/aid-image.png)

I would be very happy to invite you all to come and enjoy these cakes that I prepared on this occasion: 

![mkhabez Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/mkhabez-Aid-2016.jpg)

[ Mkhabez ](<https://www.amourdecuisine.fr/article-mkhabez-gateaux-aux-amandes.html>) with almonds well perfumed with lemon 

![Syrian chamiya Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/chamiya-syrienne-Aid-2016.jpg)

[ Syrian Chamiya, or the baklawa pyramid ](<https://www.amourdecuisine.fr/article-baklawa-chamiya-syrienne.html>)

![Kaak nakache with dates Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/Kaak-nakache-aux-dattes-Aid-2016.jpg)

[ Kaak nakache with dates ](<https://www.amourdecuisine.fr/article-kaak-nakache-aux-dattes.html>) super fondant 

![makrout way maamoul Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/makrout-fa%C3%A7on-maamoul-Aid-2016.jpg)

[ Makrout way maamoul ](<https://www.amourdecuisine.fr/article-makrout-facon-maamoul.html>) (he made his success!) 

![makrout Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/makrout-Aid-2016.jpg)

[ Makrout el Koucha ](<https://www.amourdecuisine.fr/article-makrout-el-koucha-en-video-gateau-de-semoule-et-dattes.html>) (the must at home) 

![dried fruit tartlets Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/tartelettes-aux-fruits-secs-Aid-2016.jpg)

[ Tartlets with dried fruit and salted butter caramel ](<https://www.amourdecuisine.fr/article-tartelettes-aux-fruits-secs-caramel-beurre-sale.html>)

![shortbreads with brown cream Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/sabl%C3%A9s-%C3%A0-la-creme-de-marron-Aid-2016.jpg)

Shortbreads with brown cream ( [ shortbread dough here ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-fraises.html>) ) 

![shortbread and its dome bniouen Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/sabl%C3%A9s-et-son-dome-bniouen-Aid-2016.jpg)

shortbread and its dome bniouen (recipe coming soon) 

![shortbread cookies Jam 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/sabl%C3%A9s-a-la-confiture-Aid-2016.jpg)

[ Shortbread with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-fraises.html>) done by my daughter Ines. 

![Shortbread macaroons Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/Sabl%C3%A9s-macarons-Aid-2016.jpg)

[ Shortbread macaroon with peanuts ](<https://www.amourdecuisine.fr/article-gateau-sable-macaron-aux-cacahuetes.html>)

![ghribia with sesame seeds Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/ghribia-aux-grains-de-sesames-Aid-2016.jpg)

[ ghribia (half moons) with sesame seeds ](<https://www.amourdecuisine.fr/article-demi-lunes-fondantes-aux-grains-de-sesame.html>)

![mchewek laassel with peanuts Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/mchewek-laassel-aux-cacahuetes-Aid-2016.jpg)

[ Mchewek laassel with peanuts ](<https://www.amourdecuisine.fr/article-mchewek-laassel-aux-cacahuetes.html>)

![bnouen with peanuts Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/bniouen-aux-cacahu%C3%A8tes-Aid-2016.jpg)

Bniouen aux cacahuetés et chocolat blanc ( recette à venir) 
