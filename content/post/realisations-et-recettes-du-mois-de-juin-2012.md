---
title: achievements and receipts for the month of June 2012
date: '2012-07-01'
categories:
- Cupcakes, macaroons, and other pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-au-surimi-012-b_thumb.jpg
---
![croquettes au surimi 012 b](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-au-surimi-012-b_thumb.jpg)

Hello everybody, 

it's already July, but before I start cooking for a new month (hihihihi) I'll give you a recap of the recipes and articles I posted during the month of June. 

I hope you will love this recap.   
  
<table>  
<tr>  
<td>

![aumoniere with rhubarb 034](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/aumoniere-a-la-rhubarbe-034_thumb1.jpg) 
</td>  
<td>

[ Aumoniere with rhubarb ](<https://www.amourdecuisine.fr/article-aumoniere-a-la-rhubarbe-98763994.html>) 
</td> </tr>  
<tr>  
<td>

![Algerian cake braid](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/la-tresse-2_11.jpg) 
</td>  
<td>

[ the braid الظفيرة حلوى جزائرية Algerian cakes ](<https://www.amourdecuisine.fr/article-la-tresse-61395654.html>) 
</td> </tr>  
<tr>  
<td>

![ice cream with banana](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/creme-glacee-a-la-banane_thumb.jpg) 
</td>  
<td>

[ Banana / peanut butter ice cream ](<https://www.amourdecuisine.fr/article-creme-glacee-banane-beurre-de-cacahuetes-106247252.html>) 
</td> </tr>  
<tr>  
<td>

![garantita.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/garantita.jpg) 
</td>  
<td>

[ karantika ](<https://www.amourdecuisine.fr/article-karantika-97567223.html>) 
</td> </tr>  
<tr>  
<td>

![flour tortillas](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tortillas-a-la-farine_thumb1.jpg) 
</td>  
<td>

[ Flour tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine-106300512.html>) 
</td> </tr>  
<tr>  
<td>

![donut khefaf](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignet-khefaf-060-1_thumb1.jpg) 
</td>  
<td>

[ Algerian donut, Sfenj ](<https://www.amourdecuisine.fr/article-beignet-algerien-sfenj-103615952.html>) 
</td> </tr>  
<tr>  
<td>

![Bissara](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-de-pois-casses_thumb1.jpg) 
</td>  
<td>

[ Bissara ](<https://www.amourdecuisine.fr/article-bissara-97380828.html>) 
</td> </tr>  
<tr>  
<td>

![the-Chinese-030_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/le-chinois-030_thumb1.jpg) 
</td>  
<td>

[ the Chinese ](<https://www.amourdecuisine.fr/article-le-chinois-106336647.html>) 
</td> </tr>  
<tr>  
<td>

![matloue with flour 027](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/matloue-a-la-farine-027_thumb11.jpg) 
</td>  
<td>

[ Index of breads, pancakes, crepes, pastries .... ](<https://www.amourdecuisine.fr/article-index-des-pains-galettes-crepes-viennoiseries-55212113.html>) 
</td> </tr>  
<tr>  
<td>

![lahmacun 052](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/lahmacun-052_thumb_11.jpg) 
</td>  
<td>

[ ramadan recipe ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>) 
</td> </tr>  
<tr>  
<td>

![chocolate muffins 005](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/muffins-au-chocolat-005_thumb1.jpg) 
</td>  
<td>

[ Vanilla / Chocolate Muffins ](<https://www.amourdecuisine.fr/article-muffins-vanille-chocolat-61854062.html>) 
</td> </tr>  
<tr>  
<td>

![Algerian cake Skandraniette 1](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/algerien-gateau-skandraniette-1_thumb1.jpg) 
</td>  
<td>

[ Skandraniette Algerian cakes ](<https://www.amourdecuisine.fr/article-skandraniette-gateaux-algeriens-83562645.html>) 
</td> </tr>  
<tr>  
<td>

![matlou3, matloue](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/236969641.jpg) 
</td>  
<td>

[ arabic bread - matlou - matlou3 ](<https://www.amourdecuisine.fr/article-pain-arabe-matloue-100634655.html>) 
</td> </tr>  
<tr>  
<td>

![flan at nestle 057](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-au-nestle-057_thumb.jpg) 
</td>  
<td>

[ concentrated milk flan / Nestlé ](<https://www.amourdecuisine.fr/article-flan-au-lait-concentre-nestle-106499948.html>) 
</td> </tr>  
<tr>  
<td>

![crumble sweet potato carrot 058](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/crumble-patate-douce-carotte-058_thumb1.jpg) 
</td>  
<td>

[ crumble with sweet potato and carrot ](<https://www.amourdecuisine.fr/article-crumble-a-la-patate-douce-et-carotte-106549762.html>) 
</td> </tr>  
<tr>  
<td>

![rolls strawberry mascarpone](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/roule-mascarpone-fraise-1.jpg) 
</td>  
<td>

[ Mascarpone / strawberry roll ](<https://www.amourdecuisine.fr/article-53001970.html>) 
</td> </tr>  
<tr>  
<td>

![algerian cake el machkouk](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/gateau-algerien-el-machkouk_thumb1.jpg) 
</td>  
<td>

[ algerian cake el mechkouk ](<https://www.amourdecuisine.fr/article-gateau-algerien-el-mechkouk-106633916.html>) 
</td> </tr>  
<tr>  
<td>

![ketchup chicken active](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/poulet-ketchup-actifry_thumb.jpg) 
</td>  
<td>

[ chicken with ketchup / chicken in the active ](<https://www.amourdecuisine.fr/article-poulet-au-ketchup-poulet-dans-l-actifry-106661474.html>) 
</td> </tr>  
<tr>  
<td>

![cookies](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/cho-four-002_thumb.jpg) 
</td>  
<td>

[ dry cakes, chocolate rings ](<https://www.amourdecuisine.fr/article-gateaux-secs-rondelles-au-chocolat-106682511.html>) 
</td> </tr>  
<tr>  
<td>

![caramel pineapple dessert](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/dessert-d-ananas-au-caramel_thumb1.jpg) 
</td>  
<td>

[ Rotisserie pineapple with vanilla caramel ](<https://www.amourdecuisine.fr/article-ananas-roti-au-caramel-de-vanille-106683736.html>) 
</td> </tr>  
<tr>  
<td>

![smoked salmon tagliatelle](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tagliatelle-au-saumon-fume_thumb1.jpg) 
</td>  
<td>

[ Smoked Salmon Tagliatelli ](<https://www.amourdecuisine.fr/article-tagliatelles-au-saumon-fume-106719529.html>) 
</td> </tr>  
<tr>  
<td>

![slipper spinach cod goat \(2\)](http://img.over-blog.co%0Am/555x416/2/42/48/75/API/2012-06/10/chausson-epinard-cabillaud-chevre--2-_thumb.jpg) 
</td>  
<td>

[ goat cheese spinach spinach ](<https://www.amourdecuisine.fr/article-chausson-d-epinards-cabillaud-chevre-106721781.html>) 
</td> </tr>  
<tr>  
<td>

![gratin of noodles with tuna and peas.](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gratin-de-nouilles-au-thon-et-pois.25_thumb1.jpg) 
</td>  
<td>

[ gratin of noodles with tuna and peas ](<https://www.amourdecuisine.fr/article-gratin-de-nouilles-au-thon-et-aux-pois-107020045.html>) 
</td> </tr>  
<tr>  
<td>

![spicy peanut butter cream](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/creme-au-beurre-de-cacahuete-piquante_thumb.jpg) 
</td>  
<td>

[ spicy cream with peanut butter ](<https://www.amourdecuisine.fr/article-creme-piquante-au-beurre-de-cacahuete-89052515.html>) 
</td> </tr>  
<tr>  
<td>

![spaghetti omelette](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/omelette-aux-spaguettis_thumb1.jpg) 
</td>  
<td>

[ spaghetti omelette ](<https://www.amourdecuisine.fr/article-omelette-aux-spaghettis-106725983.html>) 
</td> </tr>  
<tr>  
<td>

![chocolate hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/bouchees-chocolat-noisettes_thumb1.jpg) 
</td>  
<td>

[ hazelnut chocolate bites ](<https://www.amourdecuisine.fr/article-bouchees-chocolat-noisettes-106786575.html>) 
</td> </tr>  
<tr>  
<td>

![mkhabez coconut b](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mkhabez-noix-de-coco-b_thumb1.jpg) 
</td>  
<td>

[ mkhabez a coconut, Algerian cake ](<https://www.amourdecuisine.fr/article-mkhabez-a-la-noix-de-coco-gateau-algerien-106912372.html>) 
</td> </tr>  
<tr>  
<td>

![try with hazelnuts 11](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/mkhabez-aux-noisettes-11_thumb1.jpg) 
</td>  
<td>

[ Mekhabez with hazelnuts algerian cakes ](<https://www.amourdecuisine.fr/article-mkhabez-aux-noisettes-59413009.html>) 
</td> </tr>  
<tr>  
<td>

![help sweet 014](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/aid-sweet-014_thumb1.jpg) 
</td>  
<td>

[ algerian cake arayeche ](<https://www.amourdecuisine.fr/article-gateau-algerien-arayeche-106920538.html>) 
</td> </tr>  
<tr>  
<td>

![k3i-002_2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/k3i-002_21.jpg) 
</td>  
<td>

[ the recipe of Algerian cake bracelets ](<https://www.amourdecuisine.fr/article-40397484.html>) 
</td> </tr>  
<tr>  
<td>

![peanut butter mousse](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mousse-au-beurre-de-cacahuete_thumb1.jpg) 
</td>  
<td>

[ Peanut butter mousse ](<https://www.amourdecuisine.fr/article-mousse-au-beurre-de-cacahuete-106872400.html>) 
</td> </tr>  
<tr>  
<td>

![asparagus with chive cream](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/asperge-a-la-creme-au-ciboulette_thumb_11.jpg) 
</td>  
<td>

[ Asparagus with chive cream ](<https://www.amourdecuisine.fr/article-asperges-a-la-creme-au-ciboulette-106895194.html>) 
</td> </tr>  
<tr>  
<td>

![tchatchouka](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tchatchouka_thumb1.jpg) 
</td>  
<td>

[ chakchouka (tomato onions) بصل بطوماطيش ](<https://www.amourdecuisine.fr/article-tchakchouka-oignons-tomates-106932038.html>) 
</td> </tr>  
<tr>  
<td>

![eggplant rolls 028](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-028_thumb1.jpg) 
</td>  
<td>

[ eggplant rolls ](<https://www.amourdecuisine.fr/article-des-roules-d-aubergine-101909220.html>) 
</td> </tr>  
<tr>  
<td>

![Couscous salad with surimi 016](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/salade-de-couscous-au-surimi-016_thumb1.jpg) 
</td>  
<td>

[ couscous salad with Surimi ](<https://www.amourdecuisine.fr/article-salade-de-couscous-au-surimi-98901237.html>) 
</td> </tr>  
<tr>  
<td>

![cake pops dry cakes](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/cake-pops-gateaux-secs_thumb1.jpg) 
</td>  
<td>

[ Cake pops ](<https://www.amourdecuisine.fr/article-cake-pops-106940859.html>) 
</td> </tr>  
<tr>  
<td>

![Oven Baked Bream](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/dorade-au-four_thumb1.jpg) 
</td>  
<td>

[ Oven Baked Bream ](<https://www.amourdecuisine.fr/article-dorade-au-four-106947200.html>) 
</td> </tr>  
<tr>  
<td>

![ftirate cake Algerian](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/ftirate-gateau-algerien_thumb2.jpg) 
</td>  
<td>

[ algerian cake ftirates ](<https://www.amourdecuisine.fr/article-gateau-algerien-ftirate-107015988.html>) 
</td> </tr>  
<tr>  
<td>

![face 009](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/face-009_thumb.jpg) 
</td>  
<td>



####  [ chicken nuggets stuffed with cheese ](<https://www.amourdecuisine.fr/article-croquettes-de-poulets-farcies-de-fromage-46949639.html>)


</td> </tr>  
<tr>  
<td>

![Beet tartare with goat cheese 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Tartare-de-betterave-au-fromage-de-chevre-1_thumb1.jpg) 
</td>  
<td>

[ Beetroot Tartar with Goat Cheese ](<https://www.amourdecuisine.fr/article-tartare-de-betteraves-rouges-au-fromage-de-chevre-99710160.html>) 
</td> </tr>  
<tr>  
<td>

![Cantonese couscous 040](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/couscous-cantonais-040_thumb1.jpg) 
</td>  
<td>

[ Cantonese couscous, wok couscous ](<https://www.amourdecuisine.fr/article-couscous-cantonais-couscous-au-wok-85211638.html>) 
</td> </tr>  
<tr>  
<td>

![Palestinian chicken, chicken tajine](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/poulet-palestinien-51.jpg) 
</td>  
<td>

[ chicken tagine ](<https://www.amourdecuisine.fr/article-tajine-de-poulet-107094288.html>) 
</td> </tr>  
<tr>  
<td>

![S7302046](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/211148211.jpg) 
</td>  
<td>

[ donuts with donut iron, Algerian cake ](<https://www.amourdecuisine.fr/article-beignets-au-fer-a-beignet-gateau-algerien-107094983.html>) 
</td> </tr>  
<tr>  
<td>

![c f 002](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/c-f-002_thumb.jpg) 
</td>  
<td>

[ Shrimp pancakes - Houriat el matbakh - Fatafeat tv ](<https://www.amourdecuisine.fr/article-crepes-farcies-a-la-crevette-de-houriat-el-matbakh-96334539.html>) 
</td> </tr>  
<tr>  
<td>

![olives bread 011](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/pain-aux-olives-011_thumb.jpg) 
</td>  
<td>

[ olive bread ](<https://www.amourdecuisine.fr/article-pain-aux-olives-107156686.html>) 
</td> </tr>  
<tr>  
<td>

![gateau au chocolate-and-cream-caramel-a.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gateau-au-chocolat-et-creme-caramel-a1.jpg) 
</td>  
<td>

[ magic cake with chocolate and caramel cream ](<https://www.amourdecuisine.fr/article-gateau-magique-au-chocolat-et-creme-caramel-107207656.html>) 
</td> </tr>  
<tr>  
<td>

![zaalouk with eggplant](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/zaalouk-aux-aubergines_thumb.jpg) 
</td>  
<td>

[ Zaalouk with eggplants ](<https://www.amourdecuisine.fr/article-zaalouk-d-aubergines-107254425.html>) 
</td> </tr>  
<tr>  
<td>

![chicken nuggets 004](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/croquettes-de-poulet-004_thumb.jpg) 
</td>  
<td>

[ Chicken nuggets ](<https://www.amourdecuisine.fr/article-croquettes-de-poulet-107257102.html>) 
</td> </tr>  
<tr>  
<td>

![rice salad 030](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/salade-au-riz-030_thumb1.jpg) 
</td>  
<td>

[ rice salad / mixed salad ](<https://www.amourdecuisine.fr/article-salade-de-riz-salade-composee-107263522.html>) 
</td> </tr>  
<tr>  
<td>

![Alsatian cake with cottage cheese 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateau-alsacien-au-fromage-blanc-1_thumb1.jpg) 
</td>  
<td>

[ Alsatian cake with cottage cheese ](<https://www.amourdecuisine.fr/article-gateau-alsacien-au-fromage-blanc-107349720.html>) 
</td> </tr>  
<tr>  
<td>

![bourak with spinach and tuna 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/bourak-aux-epinards-et-au-thon-1_thumb.jpg) 
</td>  
<td>

[ Boureks with spinach and tuna ](<https://www.amourdecuisine.fr/article-boureks-aux-epinards-et-thon-107386648.html>) 
</td> </tr>  
<tr>  
<td>

![potato mchermla 007](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/pomme-de-terre-mchermla-007_thumb1.jpg) 
</td>  
<td>

[ batata mchermla / potatoes with charmoula ](<https://www.amourdecuisine.fr/article-batata-mchermla-pommes-de-terre-a-la-charmoula-107448683.html>) 
</td> </tr>  
<tr>  
<td>

![dziriettes with coconut](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/carres-en-fleur-083_thumb1.jpg) 
</td>  
<td>

[ Algerian cake, coconut dziriettes ](<https://www.amourdecuisine.fr/article-gateau-algerien-dziriettes-a-la-noix-de-coco-107470239.html>) 
</td> </tr>  
<tr>  
<td>

![chicken bourak with bechamel](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/bourak-au-poulet-a-la-bechamel_thumb.jpg) 
</td>  
<td>

[ Bourek chicken with bechamel ](<https://www.amourdecuisine.fr/article-bourek-au-poulet-a-la-bechamel-107496092.html>) 
</td> </tr>  
<tr>  
<td>

![ricota10_2](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/ricota10_2_thumb.jpg) 
</td>  
<td>

[ pineapple ricotta pie with candied orange peel ](<https://www.amourdecuisine.fr/article-tarte-ricotta-ananas-et-aux-ecorces-confite-d-orange-106916175.html>) 
</td> </tr>  
<tr>  
<td>

![croquettes au surimi 012 b](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-au-surimi-012-b_thumb.jpg) 
</td>  
<td>

[ croquettes surimi ](<https://www.amourdecuisine.fr/article-croquettes-au-surimi-107537496.html>) 
</td> </tr>  
<tr>  
<td>

![croquet cake Algerian](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquet-gateau-algerien_thumb1.jpg) 
</td>  
<td>

[ dry cakes croissants, croquets ](<https://www.amourdecuisine.fr/article-gateaux-secs-les-croquants-croquets-107572938.html>) 
</td> </tr>  
<tr>  
<td>

![nectarine pie with cream-1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tarte-nectarine-a-la-creme-1_thumb1.jpg) 
</td>  
<td>

[ cream nectarine pie ](<https://www.amourdecuisine.fr/article-tarte-aux-nectarines-a-la-creme-88034837.html>) 
</td> </tr>  
<tr>  
<td>

![hnif6-1](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/hnif6-1_fc156e54-2b14-4515-8004-7795b31604e61.jpg) 
</td>  
<td>

[ Algerian cake hnifiettes ](<https://www.amourdecuisine.fr/article-gateau-algerien-hnifiettee-106912001.html>) 
</td> </tr>  
<tr>  
<td>

![baklawa 059, Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/baklawa-059_thumb1.jpg) 
</td>  
<td>

[ baklawa, baklawa almonds cacahuetes baklawa qsentina algerian cakes ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-aux-amandes-et-cacahuettes-61018034.html>) 
</td> </tr> </table>
