---
title: Quiche with goat cheese tomatoes confit and basil
date: '2014-10-08'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-a-la-tomate1.jpg
---
![pie-in-the-tomate.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-a-la-tomate1.jpg)

##  Quiche with goat cheese tomatoes confit and basil 

Hello everybody, 

A tart with confit tomatoes, huuuuuuum .... 

Here is this delicious quiche with goat cheese, tomatoes and basil, which Lunetoiles has done recently, and that it shares with us .... You can find plenty of [ quiche recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=quiche&sa=Rechercher>) , recipes tested and approved on several occasions by readers of this blog. 

And if you too, you have tried one of the recipes of this blog, do not forget to send me the picture on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

**Quiche with goat cheese tomatoes confit and basil**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-a-la-tomate-246x300.jpg)

portions:  4  Prep time:  10 mins  cooking:  35 mins  total:  45 mins 

**Ingredients**

  * Preparation time: 10 min Cooking time: 35 min 
  * Ingredients: 
  * 1 pastry or puff pastry 
  * 60 g candied tomatoes 
  * 1 teaspoon and ½ basil 
  * 1 long white goat cheese (180 g) 
  * 2 eggs 
  * 2 egg yolks 
  * 25 cl of semi skimmed milk 
  * 25 cl of fresh cream (or use 50 cl of fresh cream without adding milk) 
  * salt 
  * pepper 



**Realization steps**

  1. Preheat the oven to th.6 (180 °). 
  2. Spread your dough in a pie pan, prick it with a fork, and place an aluminum foil filled with dry legumes and pre-bake it. . 
  3. Cut the candied tomatoes into small pieces and put them aside. 
  4. Crush the goat cheese with a fork, and leave aside too. 
  5. Beat the eggs and yolks with a whisk, then add the cream and milk, salt, pepper, goat cheese, tomato confit and basil, and mix well. 
  6. remove the aluminum foil, pour the mixture over the dough and bake for 30 to 35 minutes. 



![Quiche au chevre - tomato-confit-and-basilic.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/Quiche-au-chevre-tomates-confites-et-basilic1.jpg)
