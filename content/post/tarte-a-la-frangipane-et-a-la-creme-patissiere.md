---
title: pie with frangipane and pastry cream
date: '2011-02-28'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/02/tfrang3_thumb1.jpg
---
hello everyone, today I still put you back one of my old recipes, it's recipes that keep their flavors even if they are in the archives, hihihihi then this delicious pie made of frangipane, covered with a thin pastry cream layer. it is necessary to drip, to know that one can not do without a delice parreil. so without delay I pass you the ingredients: crushed pate: 250 gr of flour 1 teaspoon of sugar 2 tongs of salt 1 tip of baking powder 1 egg 150 gr of butter 60 ml of milk Creme pastry: & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.1  (  2  ratings)  0 

[ ![tfrang3](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/tfrang3_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-26492437.html>) [ ![trang3](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/trang3_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-26492437.html>)

Hello everybody, 

still today I'm putting you back one of my old recipes, it's recipes that keep their flavors even if they are in the archives, hihihihi 

then this delicious tart with frangipane base, covered with a thin layer of cream pastry. 

it is necessary to drip, to know that one can not do without a delice parreil. 

so without delay I pass you the ingredients: 

pastry: 

  * 250 gr of flour 
  * 1 coffee with sugar 
  * 2 pinches of salt 
  * 1 tip of baking powder 
  * 1 egg 
  * 150 gr of butter 
  * 60 ml of milk 



Custard: 

  * 300 ml of milk 
  * 4 tablespoons of sugar 
  * 30 grs of maizena 
  * 2 eggs 
  * a little vanilla 



Frangipane cream: 

  * 125 gr of soft butter 
  * 125 grams of sugar 
  * 2 eggs 
  * 2 tablespoons flour 
  * 150 gr of ground almonds 
  * raisins 



[ ![tfranging](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/tfranging_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-26492437.html>)

starting with preparing the broken dough, 

  1. mix the flour and the butter, with the fingertips, 
  2. add the salt, the sugar, the yeast, then the beaten egg, 
  3. pick up without kneading with milk 
  4. let a little rest, 



and proceed to the preparation of the frangipane cream, 

  1. knead the soft butter with the sugar, 
  2. add the beaten eggs, the almonds (you can add the almond extract for more taste), 
  3. then add the flour. and raisins (it's optional) 



spread out your broken dough, prick well with the fork, pour over the frangipane cream, and place in a preheated oven until the dough and cream are cooked. 

during this time, prepare the pastry cream, 

  1. Boil the milk. 
  2. In a bowl, beat the eggs and sugar to whiten them well. 
  3. Add the maizena, mix 
  4. pour over the milk, stirring with the whisk vigorously. 
  5. When the cream thickens, remove from heat and let cool. 



[ ![tfrang](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/tfrang_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-26492437.html>) [ ![tfrang2](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/tfrang2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-26492437.html>)

[ ![tfrang1](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/tfrang1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-26492437.html>) [ ![tfrang4](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/tfrang4_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-26492437.html>)

at the end, garnish the pie with the pastry cream, and let cool. 

bonne dégustation. 
