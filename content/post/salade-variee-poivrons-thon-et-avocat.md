---
title: varied salad peppers tuna and avocado
date: '2016-01-30'
categories:
- salads, salty verrines
tags:
- Mixed salad
- Winter salad
- Vinaigrette
- inputs
- Healthy cuisine
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-vari%C3%A9e-et-poivron-et-thon-1.jpg
---
[ ![mixed salad and pepper and tuna 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-vari%C3%A9e-et-poivron-et-thon-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-vari%C3%A9e-et-poivron-et-thon-1.jpg>)

##  varied salad peppers tuna and avocado 

Hello everybody, 

My husband is very fond of salads, not too much and especially not in winter, I do not know why, but the salad for me is refreshing in summer ... 

In any case, when I make a dish for lunch and I have this feeling that my husband is not really liked, I do parallel with a salad well varied, well loaded like that he will eat to his heels, hihihih 

Today I made him a salad with avocado, red onion, tuna, red pepper, green pepper, cherry tomatoes and corn, well you see the picture, I got only one sheet of salad…. the rest my husband left nothing, hi hi hi. 

**varied salad peppers tuna and avocado**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-vari%C3%A9e-et-poivron-et-thon.jpg)

portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * well-washed green salad leaves 
  * 1 wall lawyer 
  * ½ red pepper 
  * ½ green pepper 
  * 1 medium red onion 
  * 2 hard boiled eggs 
  * corn 
  * ½ can of tuna in oil 
  * cherry tomatoes 

Vinaigrette: 
  * extra virgin olive oil 
  * freshly squeezed lemon juice 
  * salt 



**Realization steps**

  1. in a salad bowl, place the diced onion, the two diced peppers, the diced avocado (you can leave some slices to garnish then). 
  2. add the corn and tuna in a crumb. 
  3. prepare the vinaigrette and add to your vegetables, mix and adjust the taste. 
  4. fill the salad leaves with this mixture. 
  5. place them on the serving platter. 
  6. decorate according to your taste, with cherry tomatoes, corn, slices of avocado and quarters of hard egg. 



[ ![mixed salad and pepper and tuna 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-vari%C3%A9e-et-poivron-et-thon-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/salade-vari%C3%A9e-et-poivron-et-thon-2.jpg>)
