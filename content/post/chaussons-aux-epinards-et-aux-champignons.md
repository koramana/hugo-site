---
title: slippers with spinach and mushrooms
date: '2018-01-25'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- Healthy cuisine
- Ramadan 2018
- Algeria
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/chaussons-epinards-champignons-012.CR2_thumb1.jpg
---
![turnips spinach mushrooms 012.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/chaussons-epinards-champignons-012.CR2_thumb1.jpg)

##  slippers with spinach and mushrooms 

Hello everybody, 

Delicious turnovers with spinach and mushrooms crispy heart melting, what do you think? 

These slippers made of sautéed spinach, sautéed mushrooms, and then covered with melted cheese are to die for, I wanted to use mozzarella, but I did not have any more. so I used melted cheese for hamburger. 

**slippers with spinach and mushrooms**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/chaussons-epinards-champignons-021.CR2_2.jpg)

**Ingredients**

  * 1 bunch of fresh chopped spinach. 
  * 7 - 8 mushrooms cut into small pieces. 
  * 2 garlic cloves, finely chopped. 
  * 1 tsp black pepper 
  * squares of cheese special hamburger. 
  * 2 rolls of dough feuilletees (for 12 small slippers) 
  * Olive oil 
  * Salt 



**Realization steps**

  1. In a pot. Heat the olive oil. 
  2. Add the garlic and sauté. 
  3. Add the chopped spinach, mushrooms, salt, black pepper. Mix everything well. 
  4. Cook for 20 minutes. 
  5. Thaw and cut your puff pastry sheets into the desired shape (rectangle 12 cm by 5 cm for me).   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chaussons-epinards-champignons_thumb.jpg)
  6. Fill with about 2 tablespoons of the stuffing. cover with cheese and close carefully. Be careful not to overfill your dough. 
  7. brush the top of your egg yolk slippers, to have a nice color towards the end. 
  8. cook in a preheated oven at 150 degrees C, until the slippers swell well then increase the oven to 180 degrees C to brown the slippers. 


