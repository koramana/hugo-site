---
title: Couscous bel maamar has minced meat
date: '2015-12-15'
categories:
- couscous
- Algerian cuisine
- recipes of feculents
tags:
- Algeria
- dishes
- Semolina
- Healthy cuisine
- Full Dish
- Morocco
- Chicken

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/couscous-bel-maamar.jpg
---
![Couscous bel maamar has minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/couscous-bel-maamar.jpg)

##  Couscous bel maamar has minced meat 

Hello everybody, 

**Couscous bel maamar has minced meat** : Today I share with you a recipe for couscous from a coastal city in western Algeria: Mostaghanem, couscous bel maamar, taam bel maamar, or couscous has minced meat. This couscous is typically from the region of Mostaghanem, its particularity is this topping of grilled meat and grilled semolina that is put on couscous. 

I was not surprised to see this couscous, because I already knew, my aunt whose husband is from Mostaghanem told me about it once, but I never had the chance to taste it at Couscous bel maamar to the minced meat, because my tent itself did not know the recipe. that's why I was so happy when my friend **Fradadou Gharbi** published the recipe on our cooking group on facebook. Like that at least, I'll be able to try it and pass the recipe to my aunt.   


**Couscous beautiful maamar-a minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/couscous-a-la-viande-hach%C3%A9e.jpg)

portions:  6  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * 1 kg of couscous 

For the sauce 
  * Chicken pieces 
  * A grated onion 
  * oil 
  * Salt 
  * Pepper 
  * cinnamon powder 
  * Kababa (cubèbe) 
  * Chickpea 
  * water 
  * for the maamar or the minced meat filling: 
  * 500 gr of minced meat 
  * 100 gr of chicken liver 
  * A grated onion 
  * 1 tablespoon of oil 
  * 1 tablespoon of butter 
  * Salt 
  * Pepper 
  * kababa (Cubèbe) 
  * Half a bunch of fresh mint 
  * ½ glass of grilled semolina 



**Realization steps**

  1. Cook the [ couscous steamed ](<https://www.amourdecuisine.fr/article-cuisson-du-couscous-a-la-vapeur.html>)

prepare the sauce: 
  1. In a casserole, or the bottom of a couscoussier, fry the chicken pieces in a little oil, the onion in strips. 
  2. Add salt, black pepper, cinnamon, and kebaba, 
  3. add almost 1 liter of water, and cook (add the chickpeas at the beginning of the cooking if they are just soaked, otherwise if it is canned chickpeas, add towards the end of cooking), let it cook until the chicken becomes tender and cooked. 

Preparation of the maamar: 
  1. In a small casserole, put a base of oil and butter, fry the chopped onion. 
  2. Add ground meat, chicken liver cut into cubes, salt, black pepper, cinnamon, kebaba, and chopped mint 
  3. Add 1 glass of chicken sauce, cook 15 to 20 minutes. 
  4. Grill the semolina in a heavy-bottomed frying pan ... 
  5. pour the semolina into the well-reduced minced meat sauce, stir and leave on a low heat for a few minutes. the sauce must be totally reduced. 
  6. And the maamar is ready! 

presentation: 
  1. fill a large plate with buttered couscous, decorate with maamar, and sprinkle with chicken sauce. 
  2. Put the chicken pieces on it, and enjoy. 



[ ![couscous bel maamar has minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/couscous-bel-maamar-a-la-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/couscous-bel-maamar-a-la-viande-hach%C3%A9e.jpg>)
