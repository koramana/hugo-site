---
title: 'recipes around an ingredient # 10 zucchini'
date: '2015-09-07'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
[ ![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg>)

source: Pinterest 

**Christelle** chose for this round: **The courgette** as a featured ingredient. So I invite you to participate in this game, a friendly game of sharing where there is neither winner nor loser, just the pleasure of discovering new blogs, and why not discover yours, as well as share different recipes around of a single ingredient. [ ![zucchini](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/courgette-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/courgette.jpg>)

** Christelle  ** will contact you before  **October 1** to have the title of your recipes, and also to establish the list of participants in the game, which she will in turn pass on to all participants before ** October 3  ** . 

_**Principle of the game** _

_1- Choose a seasonal ingredient and accessible to all corners of the world._

_2- The sponsor will publish this article on her blog with the links of the two bloggers who initiated this game_

_3 - Announce the ingredient that will be the star of the current month._

_4 - The bloggers who want to participate, will contact directly the animator of the game so that she can already establish the list of the participants in the game (the bloggers can if they want to announce the game on their blogs, to make it known to their friends , so we can have even more participants)._

Les rondes précédentes: 
