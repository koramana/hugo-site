---
title: Quiche with minced meat
date: '2015-11-30'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-viande-hach%C3%A9e.jpg
---
[ ![quiche with minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-viande-hach%C3%A9e.jpg>)

##  Quiche with minced meat 

Hello everybody, 

Another recipe from my friend Tema, the cook of Tome, a super rich page on facebook, with lots of delicious recipes on it. Tema liked to share with us her delicious minced meat quiche, and here is her story: 

I had guests and I tried the Fares (Samira Tv) minced meat quiche recipe and it was too good so for those who missed the episode on the Samira TV channel here is the recipe: 

**Quiche with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-viande-hach%C3%A9e-1.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** Dough broken 

  * 250g of flour. 
  * 125g of butter. 
  * 1 egg yolk. 
  * 1 pinch of salt. 
  * cold milk to pick up the dough. 

Prank call: 
  * 1 piece of chef's cheese or other brand. 
  * 1 onion. 
  * 300 g ground meat. 
  * 1 glass of fresh cream. 
  * 1 glass of milk. 
  * 2 eggs. 
  * 1 tablespoon of flour. 
  * salt and pepper. 
  * grated cheese. 



**Realization steps** Preparation 1 / of the dough: 

  1. In a bowl, pour the flour then the butter and sand all, 
  2. add the egg yolk and collect the dough, if the dough does not collect, add a little cold milk, 
  3. cover the resulting dough with a film of food and leave it there for a few minutes. 

Preparation of the stuffing: 
  1. Cook the onion in a little oil, add the minced meat, salt and pepper. 
  2. In a bowl mix the two eggs with the cream, milk, flour, salt and pepper and leave the mixture next to it. 
  3. Spread the broken cake and place it in a pie plate, prick the surface with a fork and cover the dough with a piece of baking paper, 
  4. put some dry beans on the paper or whatever you have 
  5. cook the dough for about 15 minutes. 
  6. After 15 minutes of cooking, remove the dough, remove the baking paper and spread the piece of cheese on the dough 
  7. then put the minced meat, and then pour the mixture of fresh cream, eggs ......   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-viande-hach%C3%A9e-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-viande-hach%C3%A9e-2.jpg>)
  8. And finally garnish with grated cheese. 
  9. Cook the quiche again until you have a nice golden color. 



[ ![quiche with minced meat 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-viande-hach%C3%A9e-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-viande-hach%C3%A9e-3.jpg>)
