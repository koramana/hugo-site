---
title: fluffy marbled cake with yogurt
date: '2017-02-07'
categories:
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Pastry
- To taste
- Easy cooking
- Algerian cakes
- Chocolate
- Marbled cake
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gateau-marbr%C3%A9-moelleux-au-yaourt.jpg
---
![fluffy marbled cake with yogurt](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gateau-marbr%C3%A9-moelleux-au-yaourt.jpg)

##  fluffy marbled cake with yogurt 

Hello everybody, 

This yummy marbled muffin cake is one of the recipes I make most often for a quick snack for my kids, and I realized that I had never posted the recipe on my blog. As we say, it is never too late to do well, so here is the recipe and with a video for you: 

We really like yogurt cakes at home because they are too soft, super melting and perfect for a snack, for a birthday cake base. Do not hesitate to cover this cake with a thin layer of melting chocolate, or a lemon icing to give more pep to your cake! 

**marbled cake with yogurt**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gateau-marbr%C3%A9-moelleux-au-yaourt-3.jpg)

portions:  12  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients** I use the yoghurt pot to measure the ingredients: 

  * 2 jars of yoghurt 
  * 3 jars of sugar 
  * 1 jar of butter 
  * ½ pot of table oil 
  * 6 pots of flour 
  * 4 eggs 
  * 2 bags of baking powder 
  * 2 tbsp. cocoa 
  * 3 c. liquid coffee 
  * 1 cup of vanilla coffee 



**Realization steps**

  1. preheat the oven to 170 ° C 
  2. beat the eggs and sugar until blanching 
  3. add yogurt, melted butter and oil and whisk again. 
  4. stir in sifted flour and baking powder, mix well. 
  5. divide the dough in half, add to the first part of the vanilla and add to the second part, the cocoa, the liquid coffee, and the chocolate extract. 
  6. butter and flour your mold (mine is a mold with a chimney of almost 23 cm) 
  7. pour a thin layer of the vanilla paste, add over a thin layer of the chocolate paste, then a thin layer of the vanilla paste, and another chocolate until the dough exhausts. 
  8. cook for 40 minutes, or depending on your oven, let cool completely before unmolding. 



![fluffy marbled cake with yogurt 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gateau-marbr%C3%A9-moelleux-au-yaourt-1.jpg)
