---
title: lemon squares
date: '2016-03-04'
categories:
- cakes and cakes
- rice
tags:
- Easy cooking
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/05/carr%C3%A9s-au-citron-3.jpg
---
[ ![squares with lemon 3](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/carr%C3%A9s-au-citron-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/05/carr%C3%A9s-au-citron-3.jpg>)

##  lemon squares 

Hello everybody, 

[ ![endometriosis](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/endometriose.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/05/endometriose.jpg>)

So, I began to think about a yellow recipe, I wanted to make a dish made of rice, then I thought of a yellow bavarois .... But in the end, I decided to make the lemony squares we love a lot at home. And frankly I had to make the photos super fast, because everyone was the want to take his Lemon bar ... 

When you go to read the recipe, you will understand why I liked making these lemon squares or lemon bars, it is very easy to achieve and even its cooking is super easy, you just have to be a little patient to taste them, it must cool a little anyway, hihihihi. 

I had just a little trouble, I find that the layer of lemon cream is not too high, I do not know why it is marked at home that it is a layer with 2 eggs, I find that is too small, in my opinion the next time I double the amount of cream for squares well rich in taste and color. 

In any case, it's a recipe I've been doing for years, the pictures on this article were very old, here is the opportunity to put new photos. You can also see the version of [ lemon squares of Lunetoiles ](<https://www.amourdecuisine.fr/article-carres-au-citron.html>) for large quantities and another composition of the cream. So we go right to the ingredients: 

the sanded base: 

  * 175 gr of flour 
  * 125 gr of butter 
  * 50 gr of sugar 



cream: 

  * 150 gr of sugar 
  * 3 c. soup of flour 
  * 1/2 c. coffee baking powder 
  * 1/4 c. a coffee of salt 
  * 3 medium eggs 
  * juice of 1 lemon half 
  * zest of 1 lemon and a half 
  * icing sugar for decoration 



[ ![squares with lemon 2](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/carr%C3%A9s-au-citron-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/05/carr%C3%A9s-au-citron-2.jpg>)

Preparation: 

preheat the oven to 170 ° C 10 min before cooking. cover the bottom of a 20.5 cm square baking pan with baking paper. place the ingredients of the dough in the bowl of your mixer, and mix. the dough will pick up, heap the mixture with a spoon, in your mold. Cook for 20 minutes, or until a golden dough is obtained. 

Meanwhile, in a blender, mix, sugar, flour, baking powder, salt, eggs, lemon juice, lemon peel. Mix until you obtain a homogeneous mixture, and pour on the already precooked base. Put back to cook for 20 to 25 min, or until the cream from above is good, but remains soft in the middle. 

At the end of the oven, garnish with fine sugar, cut into squares and allow to cool before unmolding. 

[ ![squares with lemon 1](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/carr%C3%A9s-au-citron-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/05/carr%C3%A9s-au-citron-1.jpg>)

Good tasting, and thank you for your visits and comments.   


**lemon squares**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/carr%C3%A9s-au-citron-2.jpg)

portions:  8  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients** the base: 

  * 175 gr of flour 
  * 125 gr of butter 
  * 50 gr of sugar 

cream: 
  * 150 gr of sugar 
  * 3 c. soup of flour 
  * ½ c. coffee baking powder 
  * ¼ c. a coffee of salt 
  * 3 medium eggs 
  * juice of 1 lemon half 
  * zest of 1 lemon and a half 
  * icing sugar for decoration 



**Realization steps**

  1. preheat the oven to 170 ° C 10 min before cooking. 
  2. cover the bottom of a 20.5 cm square baking pan with baking paper. 
  3. place the ingredients of the dough in the bowl of your mixer, and mix. the dough will pick up, heap the mixture with a spoon, in your mold. Cook for 20 minutes, or until a golden dough is obtained. 
  4. Meanwhile, in a blender, mix, sugar, flour, baking powder, salt, eggs, lemon juice, lemon peel. 
  5. Mix until you obtain a homogeneous mixture, and pour on the already precooked base. 
  6. Put back to cook for 20 to 25 min, or until the cream from above is good, but remains soft in the middle. 
  7. At the end of the oven, garnish with fine sugar, cut into squares and allow to cool before unmolding. 


