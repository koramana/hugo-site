---
title: friendship
date: '2009-07-08'
categories:
- Buns and pastries

---
_** > * A simple friend, when he comes to your house, acts like a guest.   
> * A real friend opens your fridge and uses it.   
> * A simple friend never saw you crying.   
A true friend has the shoulders soaked with your tears.   
> * A simple friend does not know the names of your parents.   
> * A real friend has their phone numbers in his address book.   
> * A simple friend brings a bottle of wine to your party.   
> * A real friend arrives early to help you cook and stays late to help you clean.   
> * A simple boyfriend hates when you call after he goes to bed.   
> * A real friend asks you why you took so long to call.   
> * A true friend inquires about your romantic love story.   
> * A simple friend could blackmail you with it.   
A simple friend thinks friendship is over when you have a fight.   
A real friend calls you after a fight.   
> * A simple friend expects you to always be there for him.   
> * Un véritable ami est toujours là pour toi. ** _
