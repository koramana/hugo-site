---
title: 'pasta salad: easy and quick recipe'
date: '2015-08-11'
categories:
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/salade-de-pates-2.CR2_1.jpg
---
![salad-of-pasta-2.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/salade-de-pates-2.CR2_1.jpg)

##  pasta salad: easy and quick recipe 

Hello everybody, 

When the weather is nice, I'm lazy to go to the kitchen, and I want to do just a dish or a recipe, which will not take me more than 30 minutes (already 30 minutes is a lot ... hihihihi ) ... 

In situations like that, I like to make salads, and the pasta salad is at the top of my list. 

I will not tell you that I have a special recipe that I make imperatively every time, in fact with the pasta salad, I use everything I can have at hand .... 

this time, there were gherkins, carrots, smoked salmon, eggs .... a nice list of ingredients ... 

**pasta salad: easy and quick recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/salade-de-pates.CR2_1.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 350 gr of dough (choose here shells) 
  * 2 carrots 
  * 1 handful of pickles 
  * 100 gr of smoked salmon 
  * 2 hard boiled eggs 
  * salt, black pepper, olive oil and vinegar 



**Realization steps**

  1. Cook the pasta as indicated on the package. 
  2. Once cooked, let them cool about twenty minutes. To prevent them from sticking. 
  3. Pour a drizzle of oil over 
  4. Cut the gherkins in 
  5. hard-boiled eggs, and carrots in small cubes. 
  6. cut the smoked salmon into small pieces too. 
  7. In a large salad bowl, mix the pasta and all the other ingredients. 
  8. Season with salt, vinegar, a touch of black pepper and mix again. 
  9. serve immediately ... 



![salad-of-pasta-1.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/salade-de-pates-1.CR2_1.jpg)
