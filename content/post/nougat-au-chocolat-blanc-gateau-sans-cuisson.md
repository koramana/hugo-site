---
title: White chocolate nougat - cake without cooking
date: '2012-01-09'
categories:
- pizzas / quiches / tartes salees et sandwichs
- Plats et recettes salees
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/Zellij-mignardise-2012_thumb1.jpg
---
##  White chocolate nougat - cake without cooking 

Hello everybody, 

when my friend Lunetoiles gave me her wonderful Rkhama recipe ( [ cake without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) ) I published the past week, I was waiting for the first opportunity to make this pretty confectionery, it must be said that the ingredients are at the door of everyone, so just find the time and opportunity to do it. 

so before yesterday, my friends who were looking for an opportunity to come for tea at home (is not it girls ???? ... .. they know each other, and I hear them laugh in their corners .... that you are !!!!) So, for them, the fact that I passed my exam (in addition it is only the beginning ... .. I wonder what will wait for me when I will have the license , maybe it's going to be a dinner ...... I have to get ready, hihihihiih ....) I have to celebrate this success with them, it's okay, because when we are surrounded and we share a joy it's already a beautiful thing. 

So for tea, I opt for easy easy, hihihihi. Hum, and not that, I had to drip the thousand leaves, this sublime French pastry they never had the chance to drip, you can see the recipe here [ thousand leaves in cafe ](<https://www.amourdecuisine.fr/article-41158909.html>)

**White chocolate nougat - cake without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/Zellij-mignardise-2012_thumb1.jpg)

portions:  25  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 1 kg roasted and mixed peanuts 
  * 1 bowl of grilled sesame seeds 
  * 1 teaspoon cinnamon 
  * 400g of white chocolate (dark chocolate here) 
  * 400 g of milk chocolate 
  * honey (to pick up the peanut paste) 



**Realization steps**

  1. method of preparation: 
  2. Melt dark chocolate in a bain-marie Mix peanuts, sesame, cinnamon, and honey until you can form a ball, 
  3. cover a worktop with a sheet of food film, 
  4. put your ball on it, then cover it with another sheet of food film, 
  5. spread out with a baking roll, then put this dough in a mold (a tray, a fried noodle, depending on the width and length obtained) 
  6. remove the first sheet of the cling film, and apply the first layer of melted chocolate 
  7. To go faster, let this chocolate layer in the refrigerator, 
  8. during this time, melt the milk chocolate in a bain marie, and a little dark chocolate aside, and fill a cone for decoration 
  9. cover the other side with milk chocolate, and start decorating 
  10. With dark chocolate, draw parallel lines on the layer of milk chocolate, 
  11. then with the help of a knife draw lines perpendicular to the first lines drawn, to make mottles 
  12. let take, then cut the cupcakes according to the desired size and shape 



and here is the detailed recipe in a video: 

merci mes amies pour la visites, passez une belle journee et a la prochaine recette 
