---
title: waffles at 7up
date: '2012-01-21'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gateaux d'anniversaires, de fetes, celebrations
- cakes and cakes

---
Hello everybody, 

waffles houses we all love is not it, and if one adds a plus, as here it is with the 7 up a soft drink, at home we do not drink too much at home soft drinks, but my neighbor who was going to leave _Jamaica_ , to pass me 2 cans, and a recipe with, a delight its recipe (chicken with coca cola, recipe to come), 

then, we come back to these waffles which were too good and delicious, and it is already a miracle that I could take these few photos: 

the recipe is that of louiza, but instead of sparkling water I put 7up. 

ingredients: 

  * 300g of flour (self raising for me) 
  * 3 big eggs 
  * 1 salt salt 
  * 75g of butter 
  * 1/2 sachet of yeast (I did not put) 
  * 150ml milk 
  * 150ml water or sparkling water (7 up for me) 
  * 60g of sugar 
  * 2 vanilla sachets 
  * 2c has flower water 



start by mixing the dry ingredients, and mix the liquid ingredients, then mix everything together to have a liquid that flows a little thick (not too much) and cook in a waffle iron. 

it was a delight, 

and here is a small selection of my old recipes. 

kisses 
