---
title: Strawberry and pistachio financiers
date: '2018-05-01'
categories:
- Cupcakes, macaroons, and other pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Financiers-fraise-pistache-2.jpg
---
[ ![Strawberry and pistachio financiers](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Financiers-fraise-pistache-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Financiers-fraise-pistache-2.jpg>)

##  Strawberry and pistachio financiers 

Hello everybody, 

Another strawberry-based, yes there is something to enjoy with your supply of strawberries. These financiers strawberry and pistachio come all hot from the kitchen of Lunetoiles, so who is it who wants ??? 

To change almond financiers, Lunetoiles has to add a little pistachio paste and garnished with a strawberry in the middle, I'm sure it's fine cakes are to die for, as melting. and perfumed with pistachio ... I imagine this sweetness that emanates from it. 

In any case on the blog, you can find different recipes from [ financial ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=financiers&sa=Rechercher>) , and full of [ recipes made from strawberries ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=fraises&sa=Rechercher>) , and some [ Pistachio paste recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=pate+de+pistache&sa=Rechercher>) , so treat yourself to trying these recipes, you will really be satisfied. 

And as I will never tire of saying it, if you have tried one of my recipes, leave me a comment under the recipe, or send the photo of your realization on this link: [ my recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") and I will be happy to share these photos on one of my articles: [ the tests of my readers ](<https://www.amourdecuisine.fr/les-essais-de-mes-lecteurs>) . 

If you are also like Lunetoiles, and you want to share your own recipes on my blog, follow this link to be able to post, your recipe and your photos, I will be super happy to publish them on my blog. 

[ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

So now, to your aprons, and your furnaces to taste these delicious financiers at tea time, or at breakfast.   


**Strawberry and pistachio financiers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Financiers-fraise-pistache-1.jpg)

Recipe type:  Dessert  portions:  12  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 4 egg whites, 
  * 120 gr of almond powder, 
  * 50 gr of flour, 
  * 130 gr of icing sugar, 
  * 70 gr of butter 
  * 1 pinch of baking powder 
  * 25 gr of pistachio paste 
  * 12 strawberries washed and hulled 
  * 20 g whole pistachio pruned 



**Realization steps**

  1. Melt the butter gently in a saucepan until it turns a color and smell nutty. 
  2. Cut off the heat and let cool. 
  3. Gently mix the icing sugar, flour, almond powder and yeast. 
  4. Add the egg whites and mix thoroughly. 
  5. Add the butter to the dough without beating. 
  6. Preheat the oven to 180 ° C 
  7. Butter the imprints of a muffin pan. 
  8. Pour the paste into each imprint and add 1 strawberry to each imprint. 
  9. Sprinkle with whole pistachios pruned 
  10. Cook in a hot oven for 12 to 15 minutes (depending on your oven, watch the cooking!) 
  11. The financiers must be golden.   
Enjoy! 



[ ![Strawberry and pistachio financiers](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Financiers-fraise-pistache.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Financiers-fraise-pistache.jpg>)

Thank you Lunetoiles for this beautiful recipe of Financiers strawberry and pistachios, and thank you for all your precious help, during this period, or with the baby, I'm still not back to my camera to take pictures of my achievements. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
