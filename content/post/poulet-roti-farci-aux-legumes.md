---
title: Roasted chicken stuffed with vegetables
date: '2010-12-08'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/12/poulet-farci-1_thumb1.jpg
---
##  Roasted chicken stuffed with vegetables 

Hello everyone, 

here is a recipe that I prepare almost every week, but that I have never had the opportunity to take pictures, because my husband does not even give me the time to put it at the table, already its release from In the oven, he begins to bite a piece here and there, or a piece of skin, and my chicken is still deformed. 

this time, and despite that I did not let it touch, but the photos were of very poor quality because it's nighttime shots, and my camera is starting to do anything at night, so I get you request to excuse the quality of these photos, although the chicken was too good and tasty, with a very crispy skin. 

a very simple recipe, the stuffing depends on the tastes and what you have at home, but it always gives a taste to your chicken.   


**Roasted chicken stuffed with vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/poulet-farci-1_thumb1.jpg)

**Ingredients**

  * 1 chicken almost 1.5 kilos (or even baby chickens as the English call them here) 
  * vegetables of your choice or taste (for me: 1 potato, 1 handful of peas) 
  * some olives (for the stuffing always) 
  * salt, black pepper, thyme, olive oil, 3 to 4 cloves of garlic 



**Realization steps**

  1. for my part I marinated the chicken in the morning, to do it at dinner. 
  2. after having cleaned the chicken, take the garlic and crush it well with the salt, the black pepper, to pass this mixture under the skin of your chicken, to impregnate all the chicken, 
  3. the rest pass over the skin and sprinkle with a little thyme 
  4. place your chicken in a bag or in a pot that goes to the fridge and let marinate well. 
  5. Now prepare your vegetables, for my part, I passed the cubes of potatoes cleaned and rinsed and steamed with peas and a clove of garlic cut finely. 
  6. halfway through cooking, remove from heat, add the sliced ​​olives, and add a little butter. 
  7. have your chicken stuffed with this vegetable mix, and close the chicken with a toothpick. 
  8. string the chicken's legs and place on a rack over a baking dish filled with water. 
  9. in a small bowl, mix 2 to 3 tablespoons of olive oil, with ½ teaspoon of honey (not much, the honey gives a nice color indeed to the chicken, but my husband manages to detect the sweet taste, and he tells me not to put any more, but I like it a lot, so I put it in secret, hihihihi) and a little salt. 
  10. induce chicken from this mixture, and cook to preheat. 
  11. every 10 to 15 min, induce your chicken again with this mixture, sometimes, I recover the liquid from the mold or there was water (because the water has force evaporates, and it remains a good juice, so I get it back, I put some water in the mold, and I add the chicken with the juice, until the chicken is completely cooked. 
  12. knowing that cooking a chicken depends on the oven, but in an oven at 180 degrees C, the cooking time is 25 minutes for every 500 grams. 
  13. keep the rest of the juice and serve it with the chicken or basting the chicken stuffing 
  14. and enjoy. 



merci 
