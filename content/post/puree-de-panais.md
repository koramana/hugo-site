---
title: From mashed parsnip
date: '2013-02-02'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/puree-de-panais-015.CR2_thumb11.jpg
---
![puree of parsnips 015.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/puree-de-panais-015.CR2_thumb11.jpg)

##  From mashed parsnip 

Hello everybody, 

here is a delicious recipe and easy to achieve that I tasted at a friend's house, mashed parsnip, I had that I never cook with parsnip, because I thought at first that it is white radish "اللفت "But a bit weird form. 

but in fact, looking at the name on the vegetable shelf, it was: "Parsnip" and looking on Wikipedia, I find that it's completely different from the white radish. 

Finally, when I drip this dish at my friend, well I never thought it was based on this vegetable, I really wanted to detect the taste without her telling me, but I not completed. 

so here I am trying to prepare this mashed potato, which goes well with breaded fish, and it was a treat. 

The mashed parsnip has very little replaced the [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre-112935763.html>) , to accompany your roasts, [ baked lamb shoulder ](<https://www.amourdecuisine.fr/article-epaule-d-agneau-rotie-au-four-113882735.html>) as I had tasted at my girlfriend, or any other.   


**From mashed parsnip**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/puree-de-panais-012.CR2_thumb11.jpg)

portions:  2  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * 500 gr of parsnips 
  * 1 potatoes 
  * 1 clove of garlic 
  * 2 portions of cheese (like the laughing cow) 
  * 100 ml of milk 
  * 10 g of butter 
  * Salt pepper 



**Realization steps**

  1. peel and clean parsnips and potatoes 
  2. Cut the vegetables into pieces 
  3. cut the garlic in half 
  4. In a large saucepan, melt the butter and fry the vegetables a little 
  5. Cover with a little water and milk, add the salt and black pepper, and cook for 20 minutes. 
  6. Crush the press puree (and if you use a mixer, do not mix too much, it may change texture) 
  7. Add the cheese, and enjoy. 



![From mashed parsnip](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/puree-de-panais_thumb1.jpg)
