---
title: my achievements at home 45
date: '2014-05-06'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

A little late to post your recipes that you try by taking inspiration from my blog, and thank you very much for this trust, because just like you, I can not try a recipe if I'm not sure of the result. Again, many of you sent me photos of your recipes using the page **[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll like ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link of your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)

I then pass you the photos I received from my faithful readers, waiting to receive yours: 

[ ![spiderman_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/spiderman_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/spiderman_thumb.jpg>)

[ Spider man birthday cake ](<https://www.amourdecuisine.fr/article-spiderman-le-gateau-d-anniversaire-de-rayan.html> "Spiderman cake, rayan's birthday cake") at Hayat (a reader) 

[ ![cake-Algerian el machkouk_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/gateau-algerien-el-machkouk_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/08/gateau-algerien-el-machkouk_thumb1.jpg>)

[ Machqouq ](<https://www.amourdecuisine.fr/article-el-machkouk-mechakek-lmachkouk.html> "el machkouk, mechakek, lmachkouk المشقوق") or machkouk and the [ peaches ](<https://www.amourdecuisine.fr/article-peches-aux-amandes-gateau-algerien-sans-cuisson.html> "almond peaches, Algerian cakes without cooking") at Souhila (a reader) 

[ Nougat with sesames and peanuts ](<https://www.amourdecuisine.fr/article-rkhama-zellige-nougat-aux-cacahuetes-et-sesames-grillees.html> "Rkhama Zellige, Nougat with peanuts and grilled sesame") at souhila (a reader) 

[ Chile con carne ](<https://www.amourdecuisine.fr/article-chili-con-carne-recette-facile-a-la-viande-hachee.html> "chili con carne easy recipe with minced meat") at FleurDZ 

[ rkhama ](<https://www.amourdecuisine.fr/article-rkhama-zellige-nougat-aux-cacahuetes-et-sesames-grillees.html> "Rkhama Zellige, Nougat with peanuts and grilled sesame") at fleurDZ 

[ tajine malsouka tunisian ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks.html> "Tunisian cuisine: Tunisian Malsouka-tajine") at Fleur DZ 

[ cauliflower in white sauce ](<https://www.amourdecuisine.fr/article-chou-fleur-en-sauce-blanche-cuisine-algerienne-108257740.html> "cauliflower in white sauce, Algerian cuisine, ramadan recipe") , at farnanita ida cuisine 

[ the mona ](<https://www.amourdecuisine.fr/article-la-mouna.html> "The Mouna") at Souhila 

[ Homemade bread with semolina ](<https://www.amourdecuisine.fr/article-comment-faire-son-pain-maison.html> "how to make homemade bread") at Suson 

[ pecan pie ](<https://www.amourdecuisine.fr/article-tarte-aux-noix-de-pecan.html> "pecan pie") at Soumia Chibani 

[ hnifiyates ](<https://www.amourdecuisine.fr/article-hnifiettes-gateau-algerien.html> "hnifiettes, Algerian cake") at Adimou 

[ shortbread with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-lait.html> "sands with milk jam") with a reader 

[ Tiramisu ](<https://www.amourdecuisine.fr/article-tiramisu-vous-ai-je-dit-a-quel-point-je-l-aime.html> "Tiramisu……. Did I tell you how much I love him?") at Samira 

[ Sands with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-lait.html> "sands with milk jam") to a reader who did not mention her name 

[ The Mouna ](<https://www.amourdecuisine.fr/article-la-mouna.html> "The Mouna") at Sweetcook and freetime 

[ Strawberry tart ](<https://www.amourdecuisine.fr/article-tarte-aux-fraises.html> "Strawberry tart") to a reader who did not mention her name 

[ chicken soup ](<https://www.amourdecuisine.fr/article-soupe-de-poulet-veloute-de-poulet.html> "Chicken / chicken soup") at DZ flower 

[ Egg custard ](<https://www.amourdecuisine.fr/article-flan-aux-oeufs.html> "Egg flan") at fleurDZ 

[ baghrir ](<https://www.amourdecuisine.fr/article-baghrir.html> "recipe baghrir / thousand holes") at DZ flower 

[ faci bread with minced meat ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee.html> "buns with chopped meat") at fatiha's place 

[ chicken with mustard ](<https://www.amourdecuisine.fr/article-cuisses-de-poulet-a-la-moutarde-au-four-111087660.html> "Chicken legs with mustard, baked") at FleurDZ 

Clementines roasted in the pan, gluten-free recipe of a reader who did not leave her name, this recipe does not come from my blog, so if this girl wants to post this recipe on the blog, we must follow this link: 

[ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

[ brioche with chive cream ](<https://www.amourdecuisine.fr/article-pain-brioche-a-la-ciboulette.html> "Brioche bread with chives") at DZ flower 

[ spilled cream with cardamom ](<https://www.amourdecuisine.fr/article-creme-renversee-caramel-cardamome.html> "Crème renversée caramel cardamome") at fleurDZ 

[ basboussa with coconut ](<https://www.amourdecuisine.fr/article-basboussa-a-la-noix-de-coco.html> "Basboussa with coconut") chez fleurDZ 
