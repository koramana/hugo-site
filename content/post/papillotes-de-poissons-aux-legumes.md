---
title: fillet of fish with vegetables
date: '2017-10-05'
categories:
- diverse cuisine
- Cuisine by country
- Unclassified
- fish and seafood recipes
tags:
- inputs
- Easy cooking
- Fast Food
- Healthy cuisine
- Diet
- Slimming kitchen
- Dietetic cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/10/papillotes-de-poissons-aux-l%C3%A9gumes-1.jpg
---
![fillet of fish with vegetables 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/papillotes-de-poissons-aux-l%C3%A9gumes-1.jpg)

##  fillet of fish with vegetables 

Hello everybody, 

Here is a very tasty and rich dish: papillotes fish with vegetables prepared by the care of my dear Lunetoiles, and this is what she tells us: 

Salam aleykoum soulef, 

Yesterday, I made fresh vegetables and white fish, I had bought filets of black place in Lidl, as it was very long that I wanted to send you the recipe of papillotes and share it on the blog, especially the how to fold the pappilotes, how to fold the papillotes, so that she really steam! Because since I discovered this way of doing the folding of papillotes in a video of the chef **Jamie Oliver** in one of his restaurants, I adopted it, I always do my curls like that since. It's perfect! 

the food cooks perfectly with the steam, one sees it well with cooking, they inflate a lot! 

One day we will try to make you the method of folding video, for the moment I am attaching a photo montage. 

**fillet of fish with vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/papillotes-de-poissons-aux-l%C3%A9gumes.jpg)

**Ingredients**

  * Papillotes of fish 
  * folding twists for a successful steam cooking 
  * 6 fish steaks of choice (I used black locust) 
  * 3 or 4 small fresh zucchini 
  * 2 fresh and ripe tomatoes 
  * green beans, diced and cut in half 
  * 2 onions 
  * 3 cloves of garlic 
  * salt pepper 
  * fish spices 
  * curry 
  * olive oil 
  * Fresh parsley 
  * aluminum foil   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/papillotes-de-poissons-aux-l%C3%A9gumes-3.jpg)



**Realization steps**

  1. Wash and peel the zucchini while keeping the skin. 
  2. Cut into thin cubes. 
  3. Wash and cut the tomatoes into cubes. 
  4. Chop the garlic and finely chop the onion 
  5. Preheat the oven to 180 ° C 
  6. Cut a large piece of aluminum. 
  7. Put a splash of olive oil and then put the fish, 
  8. salt and pepper, sprinkle with fish spices 
  9. put chopped garlic onions, diced zucchini and green beans, pieces of tomatoes 
  10. season with salt and pepper, add a little curry powder and a drizzle of olive oil. 
  11. Close the foil (photos of the folding of the foil) 
  12. Bake 1 hour. 
  13. Once cooked the foil must be inflated. 
  14. I cook 1 hour for these papillotes because of the fresh beans that take a long time to cook. 
  15. Otherwise count 30 minutes for any other vegetables. 

folding of papillotes:   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/pliage-papillote.jpg)

  1. place the ingredients in order in a large sheet of aluminum, leave half of the sheet empty and fold over the ingredients. 
  2. fold the sheet out of two, then fold each side 1 cm inwards, 2 times, and press firmly to close. 
  3. place the wraps gently on a baking tray   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/papillote-bien-gonfl%C3%A9e.jpg)



![papillotes of fish with vegetables 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/papillotes-de-poissons-aux-l%C3%A9gumes-2.jpg)
