---
title: Happy new year 2016
date: '2015-12-31'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/bonne-annee.jpg
---
[ ![happy New Year](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/bonne-annee.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/bonne-annee.jpg>)

##  Happy new year 2016 

Hello and good year, 

It is with great pleasure that I address all the readers of my blog, who made my dream a reality. You were many to follow me this year and the years before, I hope to count you again among my visitors for this year 2016. 

I hope that I will continue to make recipes that will please you and that you will always encourage me. 

Bonne année gourmande, et que du bonheur pour tout le monde. Merci. 
