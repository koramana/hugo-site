---
title: fig and mango jam
date: '2016-09-01'
categories:
- jams and spreads
tags:
- To taste
- Fruits
- Vegetarian cuisine
- Breakfast
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-figues-et-mangues-009.CR2_thumb1.jpg
---
![0](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-figues-et-mangues-009.CR2_thumb1.jpg)

##  fig and mango jam 

Hello everybody, 

Another jam of figs you will say, oh yes I'm full for the winter, and even to use in my recipes. If you have not seen them, here is my [ fig jam on video ](<https://www.amourdecuisine.fr/article-confiture-de-figues-en-video.html>) , the [ fig and melon jam ](<https://www.amourdecuisine.fr/article-confiture-de-figues-et-melon.html>) or again [ fig jam with almonds ](<https://www.amourdecuisine.fr/article-tarte-aux-figues-et-amandes.html>)

This time, I prepared a mixed jam with mango and green figs .... I did not use fresh fig, but fig in box, I wanted to know, how will be the result with this canned fig, like that, I will not deprive myself of this **fig jam** that I like a lot. 

The result of course was a sublime success, a creamy jam and very rich in taste. And as you have noticed in all my jams, I do not like mixing, I like to keep the fruit in pieces, I taste well my jam like that .... But you can pass the fruit to the blender before cooking. 

**fig and mango jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-figues-et-mangues-004.CR2_thumb.jpg)

portions:  6  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 500 gr fresh green figs 
  * 500 gr of chairs of mange 
  * 600 gr of sugar (knowing that my fig and my mango were sweet, so I used less sugar) 
  * the juice of half a lemon 
  * 1 tablespoon of butter (to prevent the foam from forming, I read it on a blog, I do not know which one, but this formula is magic). 



**Realization steps**

  1. if the figs are fresh, wash them and dry them, then cut them into cubes, if it is in a can (canned) just drain the figs from their water, and cut into cubes. 
  2. clean the mangoes and cut the pulpit into cubes too. 
  3. place in a terrine, add the sugar and lemon juice, and leave to marinate in the fridge for at least 4 hours. 
  4. put the pan on high heat, just to boil, add the butter, then lower the temperature. 
  5. watch the cooking, and stir from time to time so that it does not stick to the bottom of the pan 
  6. clean the jam jars, me after washing, I pass them in the microwave, they dry well and are hot to put the jam in it. 
  7. as soon as the jam is ready, freeze well, fill the jam jars with, and turn them over, and let them cool well. 



alors vous voulez un pot ou une cuillère de ma délicieuse confiture? 
