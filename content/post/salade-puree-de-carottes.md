---
title: carrot puree salad
date: '2016-07-21'
categories:
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/salade-carottes-016.jpg
---
##  carrot puree salad 

Hello everybody, 

I had the chance to taste this salad, during a meeting between Algerians to celebrate the feast of Eid, but I could not ask the recipe to the girl who had done it, she had left the celebrate a little rather. 

Anyway, I searched here and there, I asked on the forums, and I spoke with my mother who had a similar recipe, and that I did not hesitate to do without waiting. The salad is just too good, as good as the one I ate at the party. 

If you like carrot salads you can already see the recipe [ carrots with chermoula ](<https://www.amourdecuisine.fr/article-carottes-a-la-chermoula-charmoula.html>) , or even [ salad ](<https://www.amourdecuisine.fr/article-salade-variee-poivrons-thon-et-avocat.html>)

**carrot puree salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/salade-carottes-016.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 2 carrots cooked in slightly salty water 
  * 2 cloves garlic 
  * 1 cafe harissa 
  * ½ teaspoon of cumin 
  * salt 
  * juice of a lemon 
  * olive oil 
  * olive for the deco 



**Realization steps**

  1. place the carrots cooked in the mixer the carrots 
  2. add the garlic, the harissa, the olive oil, and mix a shot 
  3. then add cumin, lemon juice and salt 
  4. place in a presentation dish 
  5. put it in the fridge and present with black olives, or according to your taste 



et saha ramdankoum 
