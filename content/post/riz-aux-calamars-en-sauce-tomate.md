---
title: Rice with squid in tomato sauce
date: '2017-10-09'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- ramadan recipe
- fish and seafood recipes
- rice
tags:
- Sea food
- dishes
- Algeria
- Morocco
- Ramadan
- Easy cooking
- Full Dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/riz-aux-calamars-et-sauce-tomate-21.jpg
---
[ ![Rice with squid in tomato sauce](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/riz-aux-calamars-et-sauce-tomate-21.jpg) ](<https://www.amourdecuisine.fr/article-riz-aux-calamars-en-sauce-tomate.html/riz-aux-calamars-et-sauce-tomate-2-2>)

##  Rice with squid in tomato sauce 

Hello everybody, 

An easy and super simple dish to make, squid rice in tomato sauce. A popular recipe at home, but you'll tell me, is there anything tastier than the taste of squid? I would answer you, Yes everything becomes a delight in the company of squid. 

A recipe I learned from my mother, a simple recipe, easy and delicious. The most difficult and prepare good rice to accompany it. A task that can be easy for some, but not for everyone. But I hope that following the method I would share with you, you will have a super successful rice. 

Otherwise if you like squid, you can see the stuffed calamari video on youtube: 

**Rice with squid in tomato sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/riz-au-calamars-en-sauce-tomate-11-300x200.jpg)

portions:  5  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** for squid in tomato sauce: 

  * 700 gr of cleaned squid 
  * 800 gr of crushed fresh tomatoes 
  * 3 tablespoons of extra virgin olive oil 
  * salt, black pepper. 
  * ½ teaspoon powdered ginger 
  * ½ teaspoon of cumin powder 
  * 2 cloves garlic 
  * 2 bay leaves 
  * some branches of thyme 
  * ½ glass of water as needed 

for rice: 
  * 1 glass and a half of rice (240 ml glass) 
  * 2 tablespoons of oil 
  * 1 teaspoon coriander 
  * 3 green cardamom seeds 
  * 2 branches of thyme 
  * salt and black pepper 
  * 1 clove of garlic 
  * water 



**Realization steps** Prepare the rice: 

  1. generously wash the rice in a Chinese, under the abundant water, until a clear water escapes from among the grains of rice   
in this way you will significantly reduce the starch rate for not having rice sticking is super important. 
  2. let it drain well. 
  3. in a pot with lid, sauté, garlic coriander seeds thyme, and cardamom grain in oil, 
  4. add over the rice, stir a little with the wooden spoon, so that it does not burn 
  5. add salt and black pepper. 
  6. cover with water until the water exceeds the rice by almost 8 mm. 
  7. cover the pot with aluminum foil, then place the lid on top. 
  8. cook for 10 to 15 minutes over medium heat. 
  9. Watch for the cooking, if you notice that the steam has diminish remove directly from the fire. 
  10. remove the lid, let cool before gently separating the grains with a fork. 
  11. Leave aside. 

prepare the squid in tomato sauce: 
  1. Cut the squid into pieces. 
  2. place the pan on medium heat with the oil in it. 
  3. when the oil is hot, add the squid, stir a little, and cover. 
  4. let the squid cook a little, it will return its water 
  5. when this water decreases a little, remove the squid with a slotted spoon. 
  6. place the tomato in the same pan now, add all the remaining ingredients, and cook. 
  7. when the sauce boils well, put the squid in the sauce, add hot water if necessary and cook. 
  8. Stir from time to time, and watch the squid. 
  9. Do not over cook, otherwise it will become rubbery. 
  10. Once the squid is tender, remove the pan from the heat. 
  11. Serve immediately with rice. 



[ ![Rice with squid in tomato sauce](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/riz-au-clamars-sauce-tomates1.jpg) ](<https://www.amourdecuisine.fr/article-riz-aux-calamars-en-sauce-tomate.html/riz-au-clamars-sauce-tomates-2>)
