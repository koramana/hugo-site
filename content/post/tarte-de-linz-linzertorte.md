---
title: Linzertorte Linz Pie
date: '2016-11-22'
categories:
- sweet recipes
- tartes et tartelettes
tags:
- fall
- To taste
- Cakes
- desserts
- Easy cooking
- Algerian cakes
- jam

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/Tarte-de-Linz-Linzertorte-4.jpg
---
![pie-de-Linz-linzertorte-4](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/Tarte-de-Linz-Linzertorte-4.jpg)

##  Linzertorte Linz Pie 

Hello everybody, 

Here is a friend who loves linzertorte linz pie! Yes it is my dear Lunetoiles who tells me his little story: 

I redid a linz pie, it was 1 year that I had not prepared and I really wanted, because I love this pie, it reminds me when I was little, I ate the linz pie that my mother was preparing for me. 

I did not put the spices in my dough or cocoa, however, I replaced the sugar vergeoise as I had not, by sugar muscovado, I was seduced by the dough, which is very very beautiful, we see that it is flexible and creamy, and the fact that it can work on the socket pocket for the decor. 

This tart of linz or linzertorte is beautiful, it was necessary to see what it gave off as a good smell during the baking, it made you want to eat even more, and I felt that I was going to feast. 

![pie-de-Linz-linzertorte-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/Tarte-de-Linz-Linzertorte-1.jpg)

**Linzertorte Linz Pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/Tarte-de-Linz-Linzertorte-2.jpg)

**Ingredients** For 2 pies: 

  * 250 g of soft butter 
  * 100 g of sugar 
  * 100 g brown verbena (or muscovado) 
  * 200 g ground almonds 
  * 275 g flour 
  * 1 pinch of fleur de sel 
  * 1 egg 
  * 1 sachet of vanilla sugar 
  * 1 tbsp. coffee cup of cinnamon 
  * 1 tbsp. coffee bean unsweetened cocoa 

Filling for pies: 
  * 2 jars of 400 gr each of jam, perfume of your choice (raspberries, currants, blackberries, strawberries ... etc) 



**Realization steps**

  1. To work in the robot the butter ointment with the sugars until creamy consistency. 
  2. Add the egg and mix well. 
  3. Then finish by adding the pinch of salt, flour, almond powder and spices (I did not put) by not working the dough too much. Just that everything is well amalgamated and homogeneous. 
  4. Wrap ⅔ dough in a food film and put in the fridge for several hours, this will make it easier to spread. 
  5. But keep ⅓ of the dough that will serve us for the decor has the fluted sleeve in a plastic film at room temperature especially not cool. 
  6. After several hours of cool rest 
  7. Take the dough out of the fridge for 20 minutes before rolling it down between two sheets of cellophane plastic film to obtain a thickness of 2 mm. 
  8. You can flour it lightly.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/Tarte-de-Linz-Linzertorte-3.jpg)
  9. Garnish the dough with a buttered and floured pie circle on a baking sheet lined with baking paper. And also removable bottom tartlets or well-buttered tartlet circles, previously placed on a baking sheet lined with parchment paper. 
  10. Adhere with the thumb the borders. Prick with a fork. 
  11. Garnish the bottom of jam dough of your choice and make braces to the piping bag with a fluted tip by arranging harmoniously on top. 
  12. Cool again so that the braces retain their shape when cooked. (Can stay one night in the fridge at this stage) 
  13. Bake in preheated oven at 180 ° C for 40 min. 
  14. Let cool before removing the circle, otherwise the pie cracks. 
  15. Unmould on a cooling rack before presenting on a serving platter. 
  16. Store at room temperature. 



Note The remarks of isabelle:   
Unlike other pies, the Linzertorte is better after 2 days. It can be kept for a week and even freeze. In all cases, it is used at room temperature.   
If you are in a hurry, you can use 1 jar of raspberry jam, which I do most of the time. ![Pie-de-Linz-linzertorte-5](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/Tarte-de-Linz-Linzertorte-5.jpg)

On the blog and thanks to Lunetoile you can find several other Linzertorte Linz Pie recipes: 

[ **Linzer** torte with raspberry jam ](<https://www.amourdecuisine.fr/article-linzer-torte-confiture-de-framboises.html>)

[ **linzer** torte hazelnut apricot jam ](<https://www.amourdecuisine.fr/article-linzer-torte-noisettes-et-confiture-abricots.html>)

[ Linz chocolate tart / homemade blackberry jam ](<https://www.amourdecuisine.fr/article-tarte-de-linz-au-chocolat-et-a-la-confiture-de-mures-maison.html>)

[ tarte a la confiture de figues / samira tv ](<https://www.amourdecuisine.fr/article-tarte-a-la-confiture-de-figues-samira-tv.html>)
