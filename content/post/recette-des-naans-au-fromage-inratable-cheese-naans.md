---
title: recipe of naans with cheese unratable / cheese naans
date: '2018-01-27'
categories:
- bakery
- Indian cuisine
- Cuisine by country
- bread, traditional bread, cake
tags:
- Galette
- Bread
- India
- accompaniment
- Indian bread
- Easy cooking
- Ramadan 2018

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/01/recette-des-naans-au-fromage-inratable-2.jpg
---
![recipe of naans with irresistible cheese cheese naans](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/recette-des-naans-au-fromage-inratable-2.jpg)

##  recipe of naans with cheese unratable / cheese naans 

Hello everybody, 

If there is a recipe that is unanimously appreciated at home, it is this recipe of naans cheese unattainable, or as they are called cheese naans. This is the favorite snack of my children. I love it too, plus this cheese naan recipe is super simple to make. I make a good amount of a sudden, I freeze what we have not eaten, and I take it to my children by heating in the pan, they are like fresh! 

**recipe of naans with unalterable cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/recette-des-naans-au-fromage-inratable-1.jpg)

**Ingredients**

  * 500 g of flour 
  * 1 C. baking yeast 
  * 1 C. salt 
  * 1 C. sugar coffee 
  * 1 pinch of baking powder 
  * 125 gr of natural yoghurt 
  * 4 c. vegetable oil 
  * between 180 and 200 ml of lukewarm water 

To garnish: 
  * spreadable cheese, here phéladelphia 
  * butter for basting at the end of cooking. 



**Realization steps**

  1. in the bowl of the robot mix the dry ingredients: flour, baker's yeast, baking powder, salt  & sugar. 
  2. then add the oil, yoghurt and water in small amounts while kneading 
  3. Knead the dough for 12 to 15 minutes until smooth. 
  4. Leave the dough rested and doubled in volume for almost 1 h 30 depending on the temperature of the room by covering it with a cloth. 
  5. Form between 12 and 14 small balls (depending on how big you want to have your cheese naans). 
  6. Spread each time 2 balls with the rolling pin on the floured worktop at the same size. 
  7. Spread the laughing cow (for me it's Philadelphia) on a dough disc, leaving 1 cm all around, put the second disc of dough over and solder the edges by pressing well with the fingers. 
  8. Cook for 5 minutes on each side on a heavy-bottomed pan, griddle or electric pancake pan. 
  9. Arrange on a plate and brush with a knob of butter ... 



![recipe of naans with unalterable cheese](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/recette-des-naans-au-fromage-inratable.jpg)
