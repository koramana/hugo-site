---
title: Fondants with peanuts / dry cakes
date: '2013-07-30'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/gateau-algerien-fondant-aux-cacahuetes.jpg
---
![Algerian cake, fondant with peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/gateau-algerien-fondant-aux-cacahuetes.jpg)

Hello everybody, 

These peanut fondants are well-flavored dry biscuits, and super fondant made by our dear Lunetoiles, I hope you enjoy this recipe, so follow us for details of how to prepare this delicacy. 

**Fondants with peanuts / dry cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/Fondants-aux-cacahuetes.jpg)

Recipe type:  Algerian cake  portions:  50  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 400 g blanched peanuts, unsalted 
  * 300 g of flour 
  * 160 g melted butter 
  * 60 g of peanut oil (minimum) 
  * 240 g icing sugar 
  * 2 teaspoons cinnamon 
  * 2 crushed gum arabic knife tip (optional) 



**Realization steps**

  1. Start by roasting the flour: prepare a larger quantity (minimum 250g) that can then be kept in an airtight container. 
  2. Spread the flour on the oven plate preheated to 160 ° C about twenty minutes (minimum) stirring regularly, it must have a nice sand color. 
  3. Let cool. 
  4. Then roast the whole peanuts and peel them for about 10 minutes. 
  5. Let cool. 
  6. Mix the peanuts with the icing sugar, in a fine powder. 
  7. Mix with sifted flour, gum arabic and cinnamon. 
  8. Melt the butter and mix with the oil. 
  9. Pour this mixture on the powders and amalgamate all by hand. 
  10. The dough must have a wet and compact appearance. 
  11. If it's too dry, add oil per tablespoon. 
  12. Preheat the oven to 160 °. 
  13. Make dumplings the size of a ping pong ball. 
  14. Place them on the baking tray lined with parchment paper, spacing them about 5 cm apart. 
  15. Start by baking only 3 fondants. 
  16. If they are spread in patties, add roasted flour and sieved to have a more compact mass. 
  17. Continue cooking until you get a cracked look at the top of the cookies. 
  18. Let cool completely before gently removing them from the plate. 
  19. They are very friable, a sudden spatula is enough for them to collapse. 



![dry cake, fondant with peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/gateau-sec-fondant-aux-cacahuetes.jpg)

# 
