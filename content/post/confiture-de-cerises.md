---
title: Cherry jam
date: '2017-06-30'
categories:
- jams and spreads
tags:
- Express cuisine
- lemons
- Inratable cooking
- Summer
- Toasts
- desserts
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/confiture-de-cerises-1.jpg
---
[ ![cherry jam 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/confiture-de-cerises-1.jpg) ](<https://www.amourdecuisine.fr/article-confiture-de-cerises.html/confiture-de-cerises-1>)

##  Cherry jam 

Hello everybody, 

The cherries were not bad, there were good and sweet, as there were those who were tasteless ... Anyway, we preferred to eat very fresh, especially when we know how to choose and spotted good cherries. But here we go, the more we ate the good ones, and the more it remained the least good !!! ??? 

My only solution, was either to make cakes, or a jam that I can keep for next winter ... 

**Cherry jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/confiture-de-cerises-2.jpg)

portions:  6  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 1 kg of pitted cherries (I had a job there) 
  * 2 glasses and ⅓ of sugar (240 ml glass) 
  * 1 lemon 



**Realization steps**

  1. In a large pot with a thick bottom (casserole minute for me), over medium heat, cook the cherries and ⅓ glass of sugar. 
  2. With a wooden spoon, gently stir and crush the cherries to allow the sugar to enter and extract the juice from the cherries. 
  3. Add the juice and zest of the lemon. 
  4. Cook for about 20 minutes. 
  5. Add the remaining sugar and cook on medium-high heat, stirring often, 
  6. cook for another 30 minutes. 
  7. Once the mixture begins to thicken (try this test: in a cold plate, pour a little liquid jam if the liquid runs quickly when you tilt the plate, it's not yet cooked, if it freezes a little and sinks gently, remove the jam from the fire. 
  8. Fill jars, made of clean glass with this jam, close them well, and turn them on the lid, for at least 1 hour. 



[ ![cherry jam 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/confiture-de-cerises-3.jpg) ](<https://www.amourdecuisine.fr/article-confiture-de-cerises.html/confiture-de-cerises-3>)
