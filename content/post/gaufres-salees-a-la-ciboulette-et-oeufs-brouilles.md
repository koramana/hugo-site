---
title: salted waffles with chives and scrambled eggs
date: '2014-01-23'
categories:
- appetizer, tapas, appetizer
- dips and sauces
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gaufre-a-la-ciboulette_thumb.jpg
---
[ ![waffle with chives](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gaufre-a-la-ciboulette_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gaufre-a-la-ciboulette.jpg>)

Hello everybody, 

a very easy recipe to make is ready just after 10 min, a delight these scrambled eggs with chives on very delicious and soft waffles with chives. so do not hesitate to try it because it is a delight.    


**salted waffles with chives and scrambled eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gaufres-a-la-ciboulette-025_thumb.jpg)

portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for waffles: 

  * 150 gr of flour 
  * 1 and a half tablespoons of baking powder 
  * 1 pinch of salt 
  * 250 ml of milk 
  * 1 egg 
  * 2 tablespoons melted butter 
  * 4 tablespoons fresh chives finely chopped 

garnish: 
  * 8 eggs 
  * 4 tablespoons fresh cream 
  * 25 gr of butter 
  * salt and pepper 
  * chives to decorate 



**Realization steps**

  1. mix the flour, baking powder, salt in a large salad bowl 
  2. add milk, egg, melted butter, chives, and whip 
  3. when the mixture is very supple and homogeneous, let it rest for 5 min 
  4. start cooking in your waffle maker, and if you do not have one, a non-stick pan will do the job 
  5. now beat the eggs with the cream, adjust the salt and black pepper according to your taste 
  6. in a frying pan, melt the butter, 
  7. cook the eggs by mixing gently, the mixture should remain a little creamy 
  8. serve immediately by filling the waffles with these scrambled eggs, and garnish with chopped chives 



[ ![waffle with chives2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gaufre-a-la-ciboulette2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gaufre-a-la-ciboulette2_2.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

et merci de vous inscrire a la newsletter. 
