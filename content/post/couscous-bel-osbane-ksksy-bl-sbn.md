---
title: Couscous beautiful osbane كسكسي بالعصبان
date: '2014-10-24'
categories:
- couscous
- Algerian cuisine
- Dishes and salty recipes
- recipes of feculents
tags:
- Algeria
- Morocco
- Tunisia
- giblets
- dishes
- Full Dish
- Vegetables

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-bel-bakbouka_thumb.jpg
---
[ ![couscous beautiful bakbouka](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-bel-bakbouka_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/01/couscous-bel-bakbouka.jpg>)

##  Couscous beautiful osbane كسكسي بالعصبان 

Hello everybody, 

Finally the delicious recipe of this sublime couscous osbane, I do not tell you how much I like this dish. Especially, when each spoon, we mix the sweetness of couscous well cooked and these pieces of the stuffing of the belly super spicy. huuuuuuuuuuuuuuuuum, an incomparable taste, an irresistible dish, that I love enormously. 

So today I write the recipe, and I salivate in front of these photos, I feel the good smell from behind my screen ... .. I see the smoke that emerges smoothly from the baffles of my laptop ... Stop me if not the next photo you will find an empty plate, because I will already be going to eat everything, if one of you is going to be quick to go back, it will find me in obvious delays, hihihihihi 

I will stop my little delirium, and I will give you the recipe, and I pass to the whole Muslim community, all my best wishes for the year Hijri 1436. 

![happy New Year](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/bonne-annee_thumb1.png)

**Couscous beautiful osbane كسكسي بالعصبان**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/couscous-bel-3osbane_thumb.jpg)

Recipe type:  Algerian dish  portions:  4  Prep time:  40 mins  cooking:  45 mins  total:  1 hour 25 mins 

**Ingredients**

  * 4 or 5 [ osbanes ](<https://www.amourdecuisine.fr/article-3osban-algerien-comment-faire-un-3osbane.html> "Algerian 3osban, how to make a 3osbane")
  * 1 kg of rolled couscous 
  * 1 handful of chickpeas soaked the day before 
  * 1 big onion 
  * 1 tablespoon tomato concentrate 
  * 3 tablespoons of table oil 
  * 2 to 3 carrots 
  * 2 medium sized turnips 
  * 2 medium size zucchini 
  * ½ tsp of black pepper 
  * ½ cup of paprika 
  * salt 
  * butter to gently coat the couscous. 



**Realization steps**

  1. put the osbanes, chopped onions, tomato paste, oil, carrots and turnips, peeled and cut in half and in medium pieces, into the pot of a couscous pot. 
  2. add oil, spices and salt 
  3. cook slowly for a few minutes 
  4. then wet with almost 2 liters of hot water 
  5. put back on high heat, and plunge the chickpeas 
  6. after boiling, place the couscous top filled with couscous on the pot 
  7. cook ¼ of an hour or until the steam escapes well between the couscous grains, remove the couscous, and leave the pot on the fire 
  8. Sprinkle the couscous with about 1 bowl of water, releasing the grains from each other 
  9. let it swell for a few minutes 
  10. now add the zucchini as it cooks faster than the turnips and carrots and the couscous put back on the pot 
  11. repeat this operation a second time by spraying with salt water and put back a 3rd time if necessary 
  12. at the end lightly butter the couscous and pour into a deep dish 
  13. once cooked osbanes and chickpeas, arrange them on the couscous with the vegetables, and sprinkle broth according to your taste 



method of preparation: 

#  [ more detailed method for preparing couscous here ](<https://www.amourdecuisine.fr/article-25345446.html>)

# 

#  [ ![Couscous beautiful osbane-001](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/Couscous-bel-osbane-001_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/01/Couscous-bel-osbane-001.jpg>)

merci pour vos visites et bonne journee 
