---
title: rice salad / mixed salad
date: '2014-04-21'
categories:
- rice
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/salade-composee-au-riz_thumb.jpg
---
[ ![salad made with rice](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/salade-composee-au-riz_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/09/salade-composee-au-riz_2.jpg>)

##  rice salad / mixed salad 

Hello everybody, 

a salad full of freshness, very varied, very rich that we like a lot at home, I like to put all the colors in this salad, it gives a good rendering, and as they say: a dish full of natural colors, is a very rich dish, then, we are not going for many colors:    


**rice salad / mixed salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/salade-au-riz-030_thumb.jpg)

Recipe type:  salad  portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 2 glasses of cooked white rice. 
  * a handful of pitted green olives. 
  * 2 stems of celery 
  * 1 half red pepper 
  * 1 half yellow pepper 
  * 2 fresh tomatoes 
  * babybel cheese (optional) 
  * 2 to 3 pickles (optional) 
  * lemon juice 
  * olive oil 
  * salt. 



**Realization steps**

  1. method of preparation: 
  2. clean the celery and peppers. 
  3. cut them into cubes of medium size 
  4. cut the olives in half. 
  5. cut the tomato into cubes. 
  6. place in a large bowl and pour over the rice. 
  7. prepare the vinaigrette. 
  8. pour over the mixture, and mix.   
you can decorate with the gherkin cut in thin strips, and slices of babybel ... all it remains question of taste, and here it is ready ... 



[ ![rice salad](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/salade-de-riz_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/09/salade-de-riz_2.jpg>)
