---
title: vanilla butter cream
date: '2013-03-12'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-au-beurre-vanille.CR2_1.jpg
---
![vanilla butter cream](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-au-beurre-vanille.CR2_1.jpg)

##  vanilla butter cream 

Hello everybody, 

here is a delicious butter cream with vanilla, or vanilla butter cream, which I found a great pleasure to achieve, and with which I loved decorating my birthday cake ... 

a butter cream very delicious, well perfumed. It must be said that I tried several recipes to achieve this result. A cream not too disgusting, not too sweet and not too thick, well ventilated as desired. 

This cream is ideal for decorating cupcakes, or to garnish a simple cake.   


**vanilla butter cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-au-beurre-vanille.CR2_.jpg)

**Ingredients**

  * 300 gr of soft butter 
  * 300 gr of sugar 
  * 6 egg yolks 
  * 60 ml of water 
  * 1 vanilla pod 



**Realization steps**

  1. place the egg yolks and vanilla in a bowl and whip 
  2. put the sugar and water in a saucepan, and cook on medium heat to get a viscous syrup without coloring, (or with a sugar thermometer, you get 120 ° C). 
  3. Pour the syrup into a net, before it cools, otherwise it will be hard, while continuing to whip the eggs. 
  4. When the mixture is at room temperature, add the beaten butter until the mixture becomes very smooth. 


