---
title: montecaos recipe
date: '2013-05-04'
categories:
- Algerian cuisine

image: http://idata.over-blog.com/2/42/48/75/API/2010-09/gateau-algerien-ghribiyat-ezite_thumb.jpg
---
Hello everybody, 

here is delicious, biscuits, and super melting in the mouth, an Andalusian pastry, this pastry comes from the Spanish word mantecaos, which means fat. 

Over time, this pastry has taken several aspects, and must be done with the addition of more ingredients, such as almonds, hazelnuts ... and is known in other countries under different appointments: ghoriba in Morocco, and ghribia, or ghribiya in Algeria. 

the recipe in Arabic: 

**montecaos recipe**

![](http://idata.over-blog.com/2/42/48/75/API/2010-09/gateau-algerien-ghribiyat-ezite_thumb.jpg)

Recipe type:  Algerian cake, ghoriba  portions:  10  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 1 measure overflowing with oil   
(I use as a measure a water glass of 250 ml) 
  * ½ measure of crystallized sugar   
(half icing sugar, half crystallized sugar) 
  * 3 measures of flour 



**Realization steps**

  1. in a terrine mix the overflowing oil and the sugar then leave to rest about half an hour. 
  2. after adding the three measures of flour by mixing until you get a dough not too firm but handy (if the dough does not pick up you can add a little oil) I can tell you that for me and because of my flour I still add flour, because I could not form balls, but this did not change at all the good taste of the ghribiya which was too fondant, wonderfully. 
  3. Form dumplings the size of a walnut, put in a tray and bake at 180 ° before heating.   
(you can put on a pinch of cinnamon) 
  4. once cooked, leave to cool before turning out 



follow me on: 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
