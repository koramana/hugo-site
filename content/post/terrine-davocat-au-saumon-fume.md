---
title: smoked salmon advocado terrine
date: '2013-08-24'
categories:
- cakes and cakes
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/terrine-d-avocat-au-saumon-fume.jpg
---
Hello everybody, 

a avocado mousse, covered with a beautiful layer of smoked salmon, a recipe to redo without hesitation, to present in salad or as a starter, this recipe can only please the tasters and lovers of these two ingredients, the avocado, and smoked salmon. 

you have the choice of the presentation, for my part I use a mold for mini cake, to have fun mouths of small sizes.   


**smoked salmon advocado terrine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/terrine-d-avocat-au-saumon-fume.jpg)

portions:  8  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 2 average mature lawyers 
  * a few slices of smoked salmon, to cover the avocado mousse. 
  * 80 ml of fresh cream. 
  * 6 g of gelatin in foil. 
  * salt pepper 
  * lemon 



**Realization steps**

  1. cover the inside a mini cake mold with a food film. 
  2. Blend the mold with the slices of smoked salmon, letting them overflow. 
  3. Cut the avocados in half, remove the kernel and put the flesh in a blinder bowl. 
  4. Add the lemon juice and mix. Add salt, a little pepper, and cream. 
  5. Mix with an electric mixer, and at this point you can adjust the seasoning by adding even more lemon juice, salt or pepper, depending on your taste. 
  6. Put the gelatin sheets for a few minutes in cold water, then heat them in a small saucepan with very little water. 
  7. Add the melted gelatin to the mixture and beat again. Pour into the salmon-covered cake pan. 
  8. When the mold is filled with the mixture, cover with the salmon slices and fold the food film over. 
  9. Put the terrine in the refrigerator overnight, serve the next day. 



![smoked salmon advocado terrine](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/terrine-d-avocat-au-saumon-fume.jpg)
