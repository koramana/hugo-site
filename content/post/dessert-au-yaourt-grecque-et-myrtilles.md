---
title: dessert with Greek yoghurt and blueberries
date: '2013-02-16'
categories:
- Buns and pastries

---
Hello everybody, 

here is a dessert that I find a lot of happiness to present to my little family ... 

my kids loved it a lot because it was as melting as an ice cream (obviously it came out of the freezer) and my husband liked the creamy and creamy side of a good yoghurt. 

you can prepare this dessert with frozen blueberries, the result will be perfect, I used fresh blueberries, but in the second layer I did not have, this beautiful color, which I had before, when I I had prepared the same dessert with frozen blueberries, because the latter bursting into mascarpone puree prepared, give a beautiful purple color absolutely sublime. 

for my part I had to add dye (I even put more than enough to be honest, hihihihi) 

ingredients: 

Greek yoghurt layer: 

  * 750 g Greek yoghurt at room temperature 
  * 1 C. vanilla extract 
  * 130 g icing sugar, sifted 
  * 10 g of gelatin 
  * 200 ml thick cream 
  * 350 g fresh or frozen blueberries 



layer of mascarpone 

  * 125 g of mascarpone 
  * 2 tbsp. blueberries 
  * 2 tbsp. tablespoon of crystallized sugar 
  * optional purple dye 



method of preparation: 

  1. In a large bowl, mix Greek yogurt with vanilla extract and sugar. Put aside. 
  2. dip the gelatin in cold water, then dilute it in a little cream in a bain-marie, and add it to the sugar yoghurt mixture. 
  3. whip the remaining crème fraiche into whipped cream on the farm. introduce it to the preceding mixture, 
  4. add the blueberries and pour the mixture into a mold with removable base, lined with cling film. 
  5. place in the fridge time to prepare the mascarpone layer. 
  6. Now mix all the ingredients of the mascarpone layer in a food processor 
  7. Spread on the cheese cake. Keep in the freezer. 
  8. remove the cake at least 30 minutes from the freezer before presenting it (if it's in the summer 10 minutes will be largely sufficient) 


