---
title: Strawberry magpie, candied strawberry tart
date: '2017-04-22'
categories:
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pie-aux-fraises1.jpg
---
![pie-in-fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pie-aux-fraises1.jpg)

##  Strawberry magpie, candied strawberry tart 

Hello everybody, 

here are some strawberry pies, which Lunetoiles has been preparing for some time, but I have not had the chance to publish them ... so much were still full of beautiful recipes that were queuing up. 

these tartlets are just a marvel, especially with this strawberry cooking juice, which gracefully drips small cuts covering a beautiful layer of strawberries with a very subtle taste.  strawberry pie strawberry pie 

![Magpies aux strawberries - tart-to-fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pies-aux-fraises-tartelettes-aux-fraises1.jpg)

**Strawberry magpie, candied strawberry tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-aux-fraises-Lunetoiles1.jpg)

portions:  4 

**Ingredients**

  * 1 puff pastry 
  * 20 medium sized strawberries 
  * 125 gr of almonds or hazelnuts 
  * 100 g brown sugar 
  * 1 egg yolk or a little milk to brown 
  * butter and flour for mussels 



**Realization steps**

  1. method of preparation: 
  2. Butter and flour four tartlets about 10 cm in diameter. 
  3. Drizzle puff pastry and set aside for the refrigerator. 
  4. Preheat the oven to 200 ° C th 7. 
  5. In the remaining puff pastry, detail straps 3mm wide. 
  6. Wash the strawberries and plant them, then cut them in half. 
  7. Remove the puff pastry molds from the refrigerator and prick the dough with a fork. 
  8. Cover the bottom with almond powder, then arrange the strawberries harmoniously. 
  9. Dust the brown sugar strawberries, then put the strips of pasta in a nice grid. 
  10. Brush the puff pastry with egg yolk diluted in a little water or milk, then dust the brown sugar tartlets. 
  11. Bake and cook the tartlets for 20 minutes at 200 ° C until golden brown. 
  12. Taste warm or cold. 



![tarte aux strawberries - pie-in-fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-aux-fraises-pie-aux-fraises1.jpg) ![strawberry-pie - pie-in-fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/strawberry-pie-pie-au-fraises1.jpg)
