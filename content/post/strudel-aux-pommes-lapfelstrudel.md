---
title: apple strudel, The Apfelstrudel
date: '2016-05-16'
categories:
- gateaux, et cakes
- sweet recipes
tags:
- Ramadan 2016
- To taste
- Easy cooking
- Cakes
- Breakfast
- desserts
- Apple pie

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-21.jpg
---
[ ![apple strudel, The Apfelstrudel](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-21.jpg>)

##  apple strudel, The Apfelstrudel 

Hello everybody, 

There is no easier than an apple strudel or Apfelstrudel, especially when using the commercial puff pastry, which was my case. After spending 3 hours standing in the kitchen with the mhadjebs, my children ask me the question: what are we going to have to taste it? what?????? I'm tired myself! 

I rested for a few minutes, reflecting on the snack, and remembered that I had a roll of puff pastry and lots of apples. I had a hesitation between an apple tart tatin and an apple strudel, but my husband does not like apple pie, so the choice turns to an apple strudel. I am not satisfied with the photos because it was at 16:00, and frankly I do not like the pictures of the evening. 

There are several varieties of this apple strudel or Apfelstrudel which sometimes is based on filo paste, or even broken pasta, not to mention the different filling, where sometimes you can add torrified almonds, it's just a Delight this dessert. 

[ ![apple strudel, The Apfelstrudel 12](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-12.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-12.jpg>)   


**apple strudel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-20.jpg)

portions:  10  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients**

  * 4 apples 
  * 80 gr of butter 
  * 50 gr of raisins 
  * 40 gr of powdered sugar 
  * 1 sachet of vanilla sugar 
  * 2 tbsp. honey coffee 
  * half a c. cinnamon 
  * half a c. four-spice coffee 
  * 1 puff pastry 
  * decoration: 
  * 1 egg yolk 
  * sugar crystals   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-18.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-18.jpg>)



**Realization steps**

  1. macerate the raisins in a little boiling tea for at least an hour. 
  2. drain in a Chinese and book. 
  3. Peel the apples, hollow out them and cut into cubes. 
  4. Melt the butter in a large skillet. Fry the cubes the apples over low heat for about 5 minutes, stirring occasionally. 
  5. Add honey, powdered sugar, vanilla sugar, cinnamon and allspice. 
  6. Continue cooking while stirring with a wooden spoon until the cooking water reduces and caramelizes slightly. 
  7. Pour the mixture into a bowl and let cool. 
  8. Once the mixture cools, stir in the raisins. 
  9. Unroll the puff pastry on a work surface. Divide the apple filling exactly in the middle of the rectangle. 
  10. Fold both sides over the stuffing, and with a well-cut knife cut the dough until you can see the stuffing to allow the steam to escape. 
  11. Brush the beaten egg dough with a brush and sprinkle the top with sugar crystals.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes.jpg>)
  12. Gently place the apple strudel on a baking sheet lined with baking paper. 
  13. Bake in a preheated oven at 160 ° C (I place my tray in the middle of the oven) 
  14. let cool a little and then gently place on a rack to cool. 



[ ![apple strudel, The Apfelstrudel 13](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-13.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/strudel-aux-pommes-13.jpg>)
