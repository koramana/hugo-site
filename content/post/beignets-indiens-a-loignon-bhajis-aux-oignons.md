---
title: Indian donuts with onion bhajis with onions
date: '2014-12-12'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
tags:
- Spices
- inputs
- Amuse bouche
- aperitif
- Aperitif
- Buffet
- Tapas

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/beignets-indien-a-loignon-bhajis-aux-oignons.jpg
---
[ ![Indian donuts with onion: bhajis with onions](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/beignets-indien-a-loignon-bhajis-aux-oignons.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/beignets-indien-a-loignon-bhajis-aux-oignons.jpg>)

##  Indian donuts with onion bhajis with onions 

Hello everybody, 

Indian onion donuts, or as they are called onion bhajis, are delicious tangy donuts and rich in taste. This is a recipe shared with us, Ahdila Cook a Pakistani reader and friend who liked sharing her recipe on my blog. 

She says that traditionally it's donuts are made with coarsely chopped onions, but as her kids claim they do not like big chunks of onions, she has them chopped a little bit, and cooks them well, so that they do not realize that there is ... Nice tip. 

Thank you Ahdila cook, for this beautiful recipe, and these beautiful pictures. and I hope you enjoy the recipe. As you say Ahdila, do not hesitate to reduce the amount of spiciness. but if you serve them with a [ cucumber and mint ](<https://www.amourdecuisine.fr/article-raita-de-concombre-et-menthe-en-video.html> "Raita with cucumber and mint video") , this last will well balance the taste. 

**Indian donuts with onion: bhajis with onions**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/beignets-indien-a-loignons-bhajis-aux-oignons-1.jpg)

portions:  25-30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 3 large onions cut and finely chopped, 
  * 200 g chickpea flour 
  * 2 tablespoons of rice flour 
  * 1 teaspoon of baking powder 
  * 1 C. coffee turmeric 
  * 1 teaspoon of hot pepper powder 
  * ½ teaspoon black mustard seed powder 
  * ½ teaspoon powdered cumin 
  * 4 tablespoons fresh cilantro chopped 
  * 1 pinch of salt 
  * 100 ml of water 
  * vegetable oil for frying 



**Realization steps**

  1. Prepare the dough in a large deep bowl. place in chickpea flour, rice flour, baking powder, turmeric, chilli, black mustard seed powder, cumin, fresh coriander and salt. 
  2. Add the water in a small amount so as not to have a drip flowing 
  3. With your hands picking up a small amount of chopped onions, dip it in a little of the dough and pick it up nicely in your hands to give it the shape of a ball. 
  4. Continue this way until no more onions are left 
  5. Let your bhajis sit in a refrigerator tray for an hour to keep their shape. 
  6. minutes before removing the bhajis from the fridge, heat the oil in a wok or deep-bottomed pan. 
  7. place the bhajis in hot oil one at a time and leave well golden. 
  8. Remove the bhajis and place them in a kitchen towel to reduce the excess oil. 
  9. To keep your bhajis hot while frying, keep them in the preheated oven at 50 ° C. 
  10. Serve immediately with [ Cucumber Raita ](<https://www.amourdecuisine.fr/article-raita-de-concombre-et-menthe-en-video.html> "Raita with cucumber and mint video")



![Indian donuts with onion: bhajis with onions](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/beignets-indien-a-loignon-bhajis-aux-oignons-2.jpg)
