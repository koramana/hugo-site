---
title: berkoukes bel osbane
date: '2015-05-02'
categories:
- cuisine algerienne
tags:
- Algeria
- Morocco
- Tunisia
- giblets
- dishes
- Full Dish
- Vegetables

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/berkoukes-bel-osbane-de-beda.jpg
---
[ ![berkoukes bel osbane de beda](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/berkoukes-bel-osbane-de-beda.jpg) ](<https://www.amourdecuisine.fr/article-berkoukes-bel-osbane.html/berkoukes-bel-osbane-de-beda>)

##  Berkoukes bel osbane, 

Hello everybody, 

I do not know about you my friends, but for me berkoukes remains the Algerian dish that I like most and especially during the winter and cold days. I eat it all the sauces, and I like to know all the possible recipes of Berkoukes. 

This recipe is a **Oum Ali,** when she shared it on my Facebook group, I liked it so much that I went to ask the right woman for the recipe and she was very generous, and she even liked to share it on the band. A recipe from the wilaya of **Bayadh** , made with berkoukes rolled with herbs from the region, of course you can still make the recipe with natural berkoukes. 

**berkoukes bel osbane**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/berkoukes-bel-osbane-de-beda.jpg)

portions:  5  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * berkoukes rolled with dried herbs 

for the sauce: 
  * 1 onion 
  * 1 whole tomato 
  * 2 cloves garlic 
  * 1 tablespoon of canned tomato 
  * 1 carrot, diced 
  * 1 diced potato 
  * 1 slice of pumpkin diced 
  * 1 diced turnip 
  * 1 handful of chickpea moulli at night 
  * 1 handful of dry beans 
  * Klila 
  * farms (special dried apricots) 
  * a branch of celery 
  * a bunch of coriander 
  * salt, black pepper, ras el hanout 

for the stuffed belly, Osbane: 
  * a piece of lamb heart 
  * a piece of the lung of the lamb 
  * a piece of the rumen 
  * casings 
  * minced meat 
  * 2 tablespoons of fine bulgur 
  * coriander seeds in powder form 
  * garlic, ras el hanout, salt and black pepper. 
  * and a big pieces of think to stuff. 



**Realization steps**

  1. prepare the stuffed paunch: 
  2. Clean the guts and guts. 
  3. keep a nice piece of the belly to be able to make a beautiful pocket that you will stuff just now. 
  4. then cut the falls, lungs, and intestines into small pieces 
  5. boil them in slightly salty water for 5 minutes 
  6. then pass all the ingredients in a chopper with chopped meat and spices. 
  7. add towards the end the end bulgur. 
  8. sew the piece of the belly that you keep to make a pocket, and fill it with the stuffing. then close it at the seam. poke the osbane at the fork. 

prepare the sauce: 
  1. fry the onion in a little oil. 
  2. add the canned tomato, then the rest of the ingredients and the stuffed paunch, 
  3. cover with water and let cook. 
  4. Preparation of berkoukes: 
  5. wash the berkoukes with abundant water. 
  6. place a large amount of water in the bottom of the couscous to pass the berkoukes steam. 
  7. drain the berkoukes well, then steam in the top of the couscous pot. 
  8. remove, sprinkle with a little water and steam 
  9. at the 3rd cooking add salt to the berkoukes. and made still steamed. 
  10. at the end of the cooking, water the berkoukes with the sauce, let it absorb the sauce well and enjoy. 



[ ![berkoukes bel 3osbane](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/berkoukes-bel-3osbane.jpg) ](<https://www.amourdecuisine.fr/article-berkoukes-bel-osbane.html/berkoukes-bel-osbane-de-beda>)
