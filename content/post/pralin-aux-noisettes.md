---
title: praline with hazelnuts
date: '2014-03-25'
categories:
- appetizer, tapas, appetizer
- crepes, waffles, fritters
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/pralin-aux-noisettes.CR2_.jpg
---
![praline with hazelnuts.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/pralin-aux-noisettes.CR2_.jpg)

##  praline with hazelnuts 

Hello everybody, 

before sharing the recipe for this praline with hazelnuts talking a little praline: the praline is a basic ingredient that can be used in several recipes, as was the case for me, when I use it to perfume the cream mousseline to decorate my son's birthday cake, and the cream was really a real treat. 

I prepared this praline, again, yesterday, when I went to my Senegalese friend, to make the birthday cake of her son, and I enjoy to change the old photos. 

My friend and I prepared a delicious dish of Senegal, which I will publish to you during the day, so wait for my newsletter. 

A simple and delicious recipe which I pass you the ingredients:   


**praline with hazelnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/pralin-aux-noisettes.CR2_.jpg)

Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 100g caster sugar 
  * 100g of hazelnuts 



**Realization steps**

  1. In a heavy saucepan, heat the two ingredients over low heat, stirring regularly until the sugar melts completely and coats the hazelnuts. (It will be to your taste that you want it to be a little caramelized, or caramelized well as for me) 
  2. pour into a slightly oiled tray and let cool. 
  3. Break roughly and then finely mix everything in the bowl of the blender. 
  4. keep the powder obtained in an airtight container, which you can keep for a week. 



![praline with hazelnuts.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/pralin-aux-noisettes.CR2-001.jpg)
