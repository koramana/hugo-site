---
title: mhawer constantinois couscous with minced meat
date: '2015-11-07'
categories:
- couscous
- Algerian cuisine
- recipes of feculents
tags:
- Algeria
- Morocco
- Tunisia
- Semolina
- Holidays
- dishes
- Chicken

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mhawer-couscous-aux-boulettes-de-viande-hach%C3%A9e1.jpg
---
[ ![mhawer couscous with minced meatballs](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mhawer-couscous-aux-boulettes-de-viande-hach%C3%A9e1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mhawer-couscous-aux-boulettes-de-viande-hach%C3%A9e1.jpg>)

##  mhawer constantinois couscous with minced meat 

Hello everybody, 

Mhawer constantinois couscous with minced meat: Here is a recipe Constantine well distinguished compared to other Couscous Constantinois. Unlike other couscous dishes, the Mhawer is a couscous made from fine semolina, presented with a white sauce and garnished with minced meatballs and boiled eggs. 

I must admit you that although this recipe, a classic at my family in Constantine, but pity, it was not the weak point of my father, so my mother did not realize at home, and married with my husband who hates everything that is based on white sauce, I did not have the chance to achieve this dish yet so much appreciated by myself (well I must say that I am not a difficult person, I can easily love any recipe, hihihih).   


**mhawer constantinois couscous with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mhawer-couscous-constantinois2.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 kg of couscous rolled end 
  * 1 onion 
  * salt and black pepper 
  * 2 to 3 tablespoons of smen (ghee) 
  * 1 handful of chickpeas soaked the day before (if not chickpeas in box) 
  * 1 kg of mutton (pieces of thigh if possible) 

For the minced meatballs: 
  * 500 gr of minced meat 
  * 1 whole egg 
  * salt, black pepper, cilantro powder, cumin 
  * 2 to 3 tablespoons of breadcrumbs 
  * ½ chopped onion. 



**Realization steps**

  1. place the meat in pieces in a casserole, 
  2. add the grated onion, salt, pepper and smen, 
  3. Cover everything with water and add the chickpeas soaked the day before and cook   
If it is chickpeas in box, leave at the last minute). 
  4. place the couscous in a large bowl, rinse with water and drain to remove any dirt or dust when drying the couscous. 
  5. place the couscous in a large bowl, add the equivalent of a tablespoon of salt, and let dry well. 
  6. oil the couscous seeds with two tablespoons of oil and sand them between your hands to separate and aerate them. 
  7. place the couscous in the top of the couscous and put it on the couscoussier filled with boiling water 
  8. Cook for 20 minutes after the steam has escaped, pour the couscous into the terrine, air with a wooden spoon and then wet again with the water. 
  9. Return the couscous again to steaming 
  10. Remove after almost 20 minutes, aerate the grains gently between your fingers, sprinkle with a few pieces of butter, and let stand. 
  11. now prepare the minced meatballs, mixing all the ingredients 
  12. dip them in the sauce and cook. 
  13. to serve, place a large amount in a lightly deep dish, garnish with minced meatballs, hard-boiled eggs, chickpeas and pieces of meat. 
  14. sprinkle sauce according to your taste.   
In the holidays, we always make the Chinese sauce, not to have the pieces of onions, or sometimes of bone. 



[ ![mhawer, couscous constantinois](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mhawer-couscous-constantinois3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mhawer-couscous-constantinois3.jpg>)
