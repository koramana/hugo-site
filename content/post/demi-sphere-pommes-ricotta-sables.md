---
title: Half sphere sanded ricotta apples
date: '2012-09-30'
categories:
- dessert, crumbles et barres

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/demi-sphere-pomme-ricotta-sable-011.CR2_thumb1.jpg
---
##  Half sphere sanded ricotta apples 

Hello everybody, 

a very delicious dessert with caramelized apples, huuuuuuuuum we like it is not it? then imagine a beautiful layer of caramelized apples, on a light mousse of ricotta, and a shortbread ... .. I see you already crack before this delight .... but it does not stop there, because in addition to that, we water this dessert with a sweet fillet of toffee ... 

Yes, this recipe was supposed to be in the "recipe around an ingredient" round that I launched last week, but I think my blogger friends were not ready for this round yet, and the principle was not understood by everyone. 

it's a shame, this time, but if you have apple recipes, as expected, I can integrate your links at the end of this article, and you too, you can integrate the link of this recipe at you. 

otherwise we come back to the recipe? 

**Half sphere sanded ricotta apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/demi-sphere-pomme-ricotta-sable-011.CR2_thumb1.jpg)

**Ingredients** for caramelized apples 

  * 1 apple 
  * 2 tablespoons sugar 
  * 1 tablespoon of butter 

for ricotta mousse: 
  * 100 gr ricotta 
  * 2 tablespoons sugar 
  * 100 ml thick cream1 
  * [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale-100020029.html>)
  * [ shortbread ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale-100020029.html>)



**Realization steps**

  1. peel the apple and remove the seeds, cut it into a small cube 
  2. in a saucepan, caramelise the sugar without burning it, introduce the cubes of apple, and the butter. 
  3. cook while stirring so that apple pieces do not burn. 
  4. line the half-sphere of your mold of food film, so that then the dessert clings to the walls. 
  5. fill the half-sphere with caramelized apples, and place in the fridge. 
  6. whip the heavy cream until it has a good hold. 
  7. Now whip the ricotta cheese with the 2 tablespoons of sugar, then slowly add the heavy cream. 
  8. cover the caramelized apples with this mousse, and put on a shortbread. 
  9. return to the fridge until serving time. 
  10. present the dessert with a drizzle of salted butter caramel, and enjoy 


