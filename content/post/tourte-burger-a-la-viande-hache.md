---
title: burger pie with minced meat
date: '2011-08-03'
categories:
- recipe for red meat (halal)

---
if you like the recipe vote: 20b here hello everyone, here is once a candle, a recipe participating in the contest that I announce: Ramadan entries: this time it is from another of my loyal readers Nacy , the recipe comes from the blog of drissia, I thank both for the recipe: Ingredients: for the dough: 300gr between flour and fine semolina 2 teaspoons of baker's yeast 1 tsp of sugar 1 tsp of salt 1 glass of warm water ( + or - depending on the amount of flour absorption) for stuffing: 300gr ground meat 2 chopped onions (& hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.55  (  1  ratings)  0 

if you like the recipe vote: 20b [ right here ](<https://www.amourdecuisine.fr/article-concours-les-entrees-ramadanesques-le-vote-83357324.html>)

Hello everybody, 

here is once a candle, a recipe participating in the contest that I announce: 

[ Ramadanic entrees ](<https://www.amourdecuisine.fr/article-prolongation-du-concours-entrees-ramadanesques-80041938.html>) : 

![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x389/2/42/48/75/Images/)

ingredients: 

for the dough: 

  * 300gr between flour and semolina 
  * 2 tablespoons baking yeast 
  * 1 teaspoon of sugar 
  * 1 teaspoon of salt 
  * 1 glass of warm water (+ or - depending on the amount of flour absorption) 



for the stuffing: 

  * 300gr of minced meat 
  * 2 chopped onions (I put one) 
  * 2 grated carrots 
  * 1 grated tomato (I put two) 
  * 1 clove garlic minced 
  * 2 tablespoons of olive oil 
  * ginger, black pepper and cumin, I put it on fire 
  * 1 tb harissa 
  * 3 tablespoon chopped parsley 
  * 1/2 teaspoon of salt 
  * 2 boiled eggs + 50gr of grated cheese 



for gilding: 

  * an egg yolk beaten with 2 tablespoons of milk 
  * a handful of sesame seeds 



the method of preparation: 

start by preparing the dough: in a bowl put the flour, fine semolina, salt and yeast diluted in a little water, mix and add a little water to obtain a soft dough, knead well by adding some water if necessary, for 15min, cover with a clean cloth and let rest 30min. 

during this time, prepare the stuffing: pour the oil into a pan, and fry the onions over low heat, add the carrots, garlic without stirring, then add the minced meat, crush well using of a wooden spoon, add grated tomato, spices and salt, continue cooking for a few more minutes, remove from heat and add chopped parsley, let cool. 

once the dough is lifted, degas it and cut into two balls, spread the first thin ball and put in a dish covered with parchment paper, pour the stuffing on top, add the peeled and shredded hard-boiled eggs then the grated cheese, fold the edges of dough, spread the second part of dough, then cover with, well solder the edges and flatten with the hand. let rise another time for about 30min. preheat the oven to 200 °. 

once the bread is lifted brush with the egg yolk, sprinkle with sesame seeds, prick with a fork, and bake until well browned. 

thank you to Nacy for this recipe, and if you too, you want to participate in the contest, I wait your participations until August 30, 

bonne journée et Saha ramdankoum 
