---
title: makroute
date: '2012-11-19'
categories:
- amuse bouche, tapas, mise en bouche
- couscous
- cuisine algerienne
- Cuisine par pays
- idee, recette de fete, aperitif apero dinatoire
- recette de ramadan
- recettes de feculents
- recettes salées
tags:
- Algerian cakes
- Algeria
- Cakes
- delicacies
- Pastry
- Oriental pastry
- desserts
- Holidays

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/makrout1_thumb.jpg
---
Hello everybody, 

voicin one of [ Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) that my husband likes the most, makoute, or makroud of course after the [ baklawa ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-aux-amandes-et-cacahuettes-61018034.html>) . the date makrout baked, but not the [ fried makrout ](<https://www.amourdecuisine.fr/article-makrout-frit-makrout-el-maqla-ou-el-makla-72046810.html>) that's for me. 

to prepare it, I am the method of my grandmother, of Constantine, who does it marvelously well, besides in the weddings, one always asks my grandmother to make them Makrout el koucha (in the oven), 

  
and its ingredients   
are: (for a small tray of 40 x 50 cm) 

  * 1200 gr of coarse semolina (not too much fat ok) 
  * 400 gr of ghee (smen, or clarified butter) 
  * 1 little salt 
  * Orange tree Flower water 
  * Water 



for the stuffing: 

  * 500 gr of date paste 
  * 1/2 cac of cinnamon 
  * 1/2 cac of ground cloves 
  * 2 pieces of butter 
  * 2cc of jam (optional, I had put it, because I had a paste of date of poor quality, and so I was afraid that the stuffing becomes hard when cooking, I added the jam of fig, which will make the dough soft) 
  * Orange tree Flower water. 



![https://www.amourdecuisine.fr/wp-content/uploads/2012/08/makrout1_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/makrout1_thumb.jpg)

Work the date paste with the ingredients previously given until you have a soft ball. Leave aside. 

In a large bowl, pour the semolina, the salt. Mix. Make a fountain and pour the melted clarified butter. Mix well so that the butter is well absorbed by the semolina. Let it rest. After, wet with orange blossom water and water, and mix without working with your fingertips and without kneading. Once you have a compact ball, leave aside, and cover well with a cling film. 

take a good amount of the dough, shape a pudding. With your index finger, make a channel in the center in the direction of the length. 

Roll a little roll of date paste and put it in the slot. Reassemble the edges of the dough on the dates to cover it all. Ride gently again to have a roll of a width of 2 cm in height. With an impression mold, press the top of the pudding and flatten slightly to get the pattern on the makroud. Cut the lozenge into lozenges and set aside in a baking dish. 

Renew this operation until the dough is used up. The diamonds are arranged tightly and touch each other (to prevent the date paste from getting burned)   
Preheat the oven th. 175 ° and put the tray to cook for about 30 min but watch the cooking because it must have a beautiful golden color above and below. So be sure to turn off the bottom of the oven and turn on the top at the right time. 

for the presentation, You can soak them in honey. 

These cakes are kept in a hermetic box for a good duration (if you resist of course) 

bonne dégustation. 
