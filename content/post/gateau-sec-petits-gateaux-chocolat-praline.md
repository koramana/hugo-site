---
title: dry cake / praline chocolate cupcakes
date: '2012-12-15'
categories:
- appetizer, tapas, appetizer
- crepes, waffles, fritters
- Tunisian cuisine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateau-au-pralin-et-chocolat-4-_thumb1.jpg
---
##  dry cake / praline chocolate cupcakes 

Hello everybody, 

Do you like dry cakes? so you'll probably love these dry cakes / praline chocolate cakes. 

This dry cakes / praline chocolate cakes is an achievement of our dear Lunetoiles, whom I thank for passing, for all these delights, and all its sharing without counterpart.   


**dry cake / praline chocolate cupcakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateau-au-pralin-et-chocolat-4-_thumb1.jpg)

portions:  40 

**Ingredients**

  * 125 gr of butter 
  * 125 gr of sugar 
  * 1 egg 
  * 220 g flour 
  * 1 teaspoon of baking powder 
  * 3 C. Praline paste soup 
  * Pistoles of dark chocolate 



**Realization steps**

  1. Preheat the oven to 180 ° C (th.6) 
  2. Mix the soft butter with the sugar, beat until a creamy mixture is obtained. 
  3. Add the egg then incorporate the flour, yeast and mix well. 
  4. Add the praline paste and mix well until the mixture forms a ball of non-sticky dough. 
  5. Form small dumplings and place them on a baking sheet covered with baking paper, spacing them. 
  6. Finally, place on each ball, a pistole of dark chocolate by flattening it a little. 
  7. Cook in a preheated oven at 180 ° C for 10 minutes. 
  8. At the end of the oven, allow to cool completely before taking off. 
  9. Finish by decorating your cupcakes if you wish with zigzag of melted chocolate or sprinkling with icing sugar. ( I did not do it ) 



source: magazine Gazelle 
