---
title: Couscous with a sheep's necklace
date: '2015-07-12'
categories:
- couscous
- Algerian cuisine
- Moroccan cuisine
- Tunisian cuisine
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/collier-de-mouton-au-couscous.CR2_.jpg
---
[ ![necklace of mutton with couscous.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/collier-de-mouton-au-couscous.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/collier-de-mouton-au-couscous.CR2_.jpg>)

##  Couscous with a sheep's necklace 

Hello everybody, 

What do I like about this part of the sheep, the collar of the sheep, or as it is called in the East of Algeria krouma, at home and at the feast of Aid el adha, we always prepare or couscous with the necklace of the sheep "couscous bel krouma" in the East we say rather "t3am bel krouma" or "chakhchoukhat eddfar bel krouma" a dish that I wanted to make this Aid, but I did not have much chakhchoukhat eddfar at home, I have to prepare my reserve for this winter, because it's a dish we like a lot. 

So, as here in England, we have the sheep well cut on the day of the Aid, (in Algeria, we let the sheep dry one night before cutting it), my husband asked me to prepare couscous at the neck of the sheep. I prepared this dish for dinner, and took the photos in the light of the bulb, hence the poor quality of light on the photos. 

Still, this couscous is a delight, with these pieces of mutton meat melting in the mouth, rich in protein, a complete and well-balanced dish, especially with the vegetables that garnish it. 

**Couscous with a sheep's necklace**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/couscous-au-collier-du-mouton-1.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * [ Couscous steamed ](<https://www.amourdecuisine.fr/article-comment-cuire-le-couscous-111575933.html> "how to cook couscous")
  * 5 to 6 pieces of the sheep collar 
  * a handful of chickpeas soak the day before. 
  * 3 large carrots. 
  * some potatoes (optional) 
  * 2 zucchini 
  * 1 big onions 
  * 2 tablespoons tomato concentrate 
  * 1 tablespoon smen or butter, 
  * 2 tablespoons of oil 
  * paprika, black pepper, salt and ras el hanout 



**Realization steps**

  1. start by grating the onions, 
  2. place them in the bottom of the couscoussier, add the spices, the meat and the oil, 
  3. let it simmer a little, add the carrots peeled, and cut according to your taste, 
  4. cover with a little water and let it cook a little, the meat will take a color, and reject its water. 
  5. add the tomato, and the chickpeas and cover with water. 
  6. once the meat is almost cooked add zucchini cut to taste and cook. 
  7. Once everything is ready to serve in a hollow dish arrange vegetables, meat and chickpeas, sprinkle sauce, and enjoy 



![couscous with lamb necklace 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/couscous-au-colier-du-agneau2.jpg)

Regalez vous, et encore une fois, saha aidkoum 
