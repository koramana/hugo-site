---
title: cream chiffon praline
date: '2014-11-19'
categories:
- basic pastry recipes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/creme-mousseline-pralin%C3%A9e-005.jpg
---
[ ![creme chiffon praline 005](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/creme-mousseline-pralin%C3%A9e-005.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/creme-mousseline-pralin%C3%A9e-005.jpg>)

##  Creme chiffon praline 

Hello everybody, 

This is not the first time I realize the **praline chiffon cream** , it must be said that it is one of my favorite creams, that I use to garnish my [ birthday cakes ](<https://www.amourdecuisine.fr/article-biscuit-fin-base-pour-gateau-danniversaire.html> "Biscuit end / base for birthday cake") , to poke my **pasta** and **religious** I also do it to stuff a delicious [ **Paris Brest** ](<https://www.amourdecuisine.fr/article-paris-brest.html> "Paris Brest") or a **Brioche** **tropézienne** ... 

I like [ homemade hazelnut praline ](<https://www.amourdecuisine.fr/article-pralin-aux-noisettes.html> "praline with hazelnuts") that I do not find on sale here in England, and I look for the slightest opportunity to prepare it and prepare with a cream chiffon praline ... 

So this time, I had the chance to take pictures, this delicious cream chiffon praline, to share the recipe with you, you have probably seen many times on my blog. 

![chiffon cream praline 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/creme-mousseline-pralinee-1.jpg)

**cream chiffon praline**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/Creme-mousseline-pralinee.jpg)

portions:  8 to 10  Prep time:  40 mins  cooking:  10 mins  total:  50 mins 

**Ingredients**

  * 500 ml of milk 
  * 4 egg yolks 
  * 100 gr of sugar 
  * 60 grs of maizena 
  * 2 tablespoons vanilla extract 
  * 130 gr of butter 
  * 150 [ gr of praline with hazelnuts ](<https://www.amourdecuisine.fr/article-pralin-aux-noisettes.html> "praline with hazelnuts")



**Realization steps**

  1. Pour the milk into a saucepan and heat to a boil. 
  2. place the egg yolks and sugar in a bowl. Whip up whitening 
  3. Add vanilla extract and cornflour and whisk. 
  4. Add the warm milk gently to the previous mixture while whisking 
  5. place the preparation in a saucepan rinsed with cold water so that after the cream does not stick 
  6. put on medium heat and mix with a wooden spoon without stopping. 
  7. The cream will start to thicken. 
  8. remove from heat, place in a bowl and cover with a cling film to prevent skin from forming and allow to cool completely to room temperature. 
  9. When the custard is cold, add the praline paste and whisk. 
  10. Place this cream in a bowl or the bowl of a baker and start whipping at maximum speed. 
  11. Add the ointment butter in a small piece. 
  12. Continue to introduce all the butter, the cream will become very light. 
  13. Use the cream immediately or keep it in the refrigerator. 



[ ![chiffon cream praline-001](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/creme-mousseline-pralinee-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/creme-mousseline-pralinee-001.jpg>)
