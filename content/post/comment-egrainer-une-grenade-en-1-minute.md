---
title: How to grind a grenade in 1 minute
date: '2015-12-06'
categories:
- basic pastry recipes
tags:
- Red fruits
- Fruits
- To taste
- desserts
- Pastry
- Cakes
- sauces

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/egrainer-une-grenade.jpg
---
[ ![to grind a pomegranate](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/egrainer-une-grenade.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/egrainer-une-grenade.jpg>)

##  How to grind a grenade in 1 minute 

Hello everybody, 

Do you like grenades? Of course you will say yes ... But who does the biggest job ???? to grind the pomegranate? 

Personally, not me, no no, because I have hands that are colored very quickly in black, whether it is pomegranates, artichokes, cardoons ... I must just after wash my hands well, and make them a good bath of lemon juice. So this little job is not mine for a long time. 

But the misfortune, when you are at the mercy of others, do not expect to eat pomegranate grains at will. Sometimes the beautiful fruits are there for days and days until they lose their freshness. 

But thank god, since I saw this method of grinding a grenade, I do not get tired, it goes so fast, that the juice will not impregnate my hands quickly. 

**How to grind a grenade in 1 minute**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/egrainer-une-grenade-1.jpg)

portions:  1  Prep time:  1 min  total:  1 min 

**Ingredients**

  * A grenade 

equipment: 
  * a plate 
  * a knife 
  * a wooden spoon 



**Realization steps**

  1. wash the pomegranate 
  2. cut it in half with a knife in 
  3. gently tap with a wooden spoon 
  4. enjoy! 


