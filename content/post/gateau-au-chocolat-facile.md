---
title: easy chocolate cake
date: '2017-04-21'
categories:
- Chocolate cake
- birthday cake, party, celebrations
- cakes and cakes
- sweet recipes
tags:
- To taste
- Cake
- Algeria
- desserts
- Soft
- Algerian cakes
- chocolate cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gateau-au-chocolat-facile.jpg
---
![gateau au chocolat-easy](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gateau-au-chocolat-facile.jpg)

##  easy chocolate cake 

Hello everybody, 

You want a recipe for **easy chocolate cake** ? do not go further because this recipe will please you a lot, it is a recipe that my friend Samy Aya (rather known on my blog for its continuous achievements from my blog, under the name Fleur DZ). 

Do not wait to make this recipe easy chocolate cake and leave your opinion on the recipe, because on Facebook this recipe has made its success. 

and here is the video recipe for you: 

{{< youtube CoEAR2dqUNI >}} 

A message from our friend Samy Aya: 

You have a visitor unexpectedly or you lack ideas to taste it? Well ! I propose for you this time a simple recipe, easy to prepare and especially economical, with ingredients that are always within our reach. What's good about this easy chocolate cake recipe is that with the same realization, you have the chocolate cake, and its icing! is not it beautiful? 

![gateau au chocolat-easy-5](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gateau-au-chocolat-facile-5.jpg)

**easy chocolate cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gateau-au-chocolat-facile-3.jpg)

portions:  8  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients** Measure used: a glass of 100ml. 

  * A glass of milk. 
  * A glass of crystallized sugar 
  * A glass of table oil. 
  * A glass of cocoa 
  * A glass of flour 
  * 02 eggs. 
  * A sachet of baking powder. 
  * 3 to 4 tablespoons homemade syrup 
  * grated chocolate for decoration 



**Realization steps** step one: 

  1. In your food processor (ognion chopper) put the milk and sugar, turn the robot on, the goal here is to dissolve the sugar in the milk is an important step to make your cake. 
  2. Always the robot in use add the oil and let mix well. 
  3. Stir in the cocoa now. 
  4. When the icing becomes smooth and creamy, remove a 60 ml glass and reserve for the end. 

step two: 
  1. Add two eggs to the remaining dough in the robot and mix well. 
  2. Add the flour mixed with the baking powder by small amounts in the robot that is still running (the robot must be in operation, do not stop it). 
  3. Pour the dough into a pie dish or a well-buttered 22 cm cake. 
  4. Bake in a preheated oven at 180 ° C. 
  5. Let cool and water with 3 to 4 tablespoons of syrup to soak the cake then ice with the icing reserved and decorate at your leisure. 
  6. you can add nuts, candied fruits in your dough 



![gateau au chocolat-easy-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gateau-au-chocolat-facile-2.jpg)
