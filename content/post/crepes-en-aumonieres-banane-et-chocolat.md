---
title: Crepes in aumonieres banana and chocolate
date: '2015-10-22'
categories:
- crepes, waffles, fritters
tags:
- Pastry
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/aumoniere-de-crepe-au-nutella-et-banane.jpg
---
[ ![aumoniere of crepe with nutella and banana](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/aumoniere-de-crepe-au-nutella-et-banane.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/aumoniere-de-crepe-au-nutella-et-banane.jpg>)

##  Crepes in aumonieres banana and chocolate 

Hello everybody, 

The easiest snack to make and the most appreciated by the children is the pancakes, especially since there are thousands and one way to present the pancakes, and it is always a pleasure for the children. 

Today my friend **Samia Z** , we pass her recipe for pancakes, which she found on the site Regilait ... Pancakes made from milk powder ... and to present them to her children, she made some beautiful and pretty chaplains with a nice surprise to the inside for children .... If you make these pancakes, what could be the pardon that you will present them? 

**Crepes in aumonieres banana and chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/aumoniere-de-crepe-au-nutella-et-banane-001.jpg)

portions:  10  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients** for crepes: 

  * 60 g of powdered milk (skimmed, semi-skimmed or whole) 
  * 250g of flour 
  * 2 eggs 
  * 30 g caster sugar 
  * 1 pinch of fine salt 
  * ½ liter of water 

for garnish: 
  * Nutella 
  * banana 



**Realization steps**

  1. Mix all the dry ingredients together in a bowl. 
  2. Pour the water slowly, stirring gently. 
  3. Finally, add the two beaten eggs. 
  4. Stir and let stand a few minutes. 
  5. Cook in a pancake pan 
  6. during the presentation, spread out the crepe, place the equivalent of a large tablespoon of chocolate, and decorate with banana   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/aumoniere-banane-et-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/aumoniere-banane-et-chocolat.jpg>)
  7. close the crepe to make a chaplain. 


