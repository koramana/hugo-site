---
title: potato gratin with cream of spinach
date: '2013-08-30'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg
---
##  potato gratin with cream of spinach 

Hello everybody, 

yum yum a wonderful gratin of potatoes with spinach cream! I love spinach, and I'm always looking for new recipes to make with it. 

And here is a recipe that I saw on an Arab forum, I vaguely remember the recipe, but I tried to make potato gratin with spinach cream according to my taste, while keeping the principle of recipe. In the end, we have a very melting gratin, and well perfumed. 

Do not forget if you made one of my recipes to send me the picture on this email: 

![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg)

**potato gratin with cream of spinach**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gratin-de-pommes-de-terre-a-la-creme-d-epinards-044.jpg)

**Ingredients**

  * 5 potatoes of medium size. 
  * 250 gr of spinach 
  * béchamel sauce 
  * ½ onion 
  * grated cheese. 
  * 1 tablespoon of oil 
  * salt and black pepper. 



**Realization steps**

  1. Peel the potatoes, wash them and dry them. 
  2. Cut them into 2 cm cubes. 
  3. salt and pepper, and pass them by steam. 
  4. grate the onion, and sauté in oil until you have a nice translucent color. book 
  5. Wash and cut the spinach, and cook in slices in a pan with a little oil. 
  6. add the onions. 
  7. and add this mixture to the bechamel sauce.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gratin-de-pomme-de-terre-a-la-creme-d-epinards-2_thumb1.jpg)
  8. in a buttered baking tin, place the baked potato. 
  9. cover with a nice layer of cream of spinach (bechamel + spinach) 
  10. then cover with grated cheese. 
  11. bake in a preheated oven at 180 degrees. for 15 minutes. 
  12. enjoy with a fresh salad. 


