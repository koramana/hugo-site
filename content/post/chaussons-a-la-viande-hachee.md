---
title: minced meat turnovers
date: '2015-07-25'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- Tunisian cuisine
tags:
- Amuse bouche
- inputs
- Aperitif
- Appetizers
- Algeria
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/chausson-a-la-viande-hach%C3%A9e.jpg
---
[ ![slipper with minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/chausson-a-la-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/chausson-a-la-viande-hach%C3%A9e.jpg>)

##  minced meat turnovers 

Hello everybody, 

During the summer, we always want to eat light, or prepare a good thing that can be enjoyed easily with a salad. Slippers like that with minced meat are more than welcome, just organize to prepare a delicious dough, and a good joke, and why not pass the hand to the children to make the salad, children still love it. My daughter loves washing lettuce leaves a lot, she also likes to make carrot salad, but I'm always scared when she uses the rape, so I stay in front of her, so she does not hurt herself, but the kids like those little spots a lot. 

Today, I share with you the recipe for the minced meat slippers of my dear **Wamani M,** I especially liked the dough prepared with a little paprika, it's a very good idea. 

**minced meat turnovers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/chausson-a-la-viande-hach%C3%A9e.jpg)

portions:  6  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 250g of flour 
  * 125 g of butter 
  * Salt pepper A little paprika 
  * A bit of milk 
  * A little water to pick up the dough 

Prank call : 
  * 1 small onion 
  * 1 clove of garlic 
  * 200 gr of minced meat 
  * salt, black pepper, spice according to your taste 
  * olive cut into slices 
  * melted cheese 



**Realization steps** Prepare the dough: 

  1. put the flour in a bowl, make a fountain, add the butter melted and tiedi and sand 
  2. add salt, black pepper and paprika, and pick up with a little milk and water, until you get a paste. 
  3. Wrap with a cling film and let the dough rest in a cool place for 1 hour. 

Prepare the stuffing: 
  1. In a skillet, pour a splash of olive oil. Add chopped onion, and grated garlic 
  2. Let it come back until the onion is translucent. 
  3. Add the minced meat, salt and pepper and add the spices 
  4. Simmer on low heat. 
  5. At the end of cooking add the sliced ​​olives and the pieces of cheese. 
  6. let cool. 

To train the slippers: 
  1. Divide the dough into several pieces. 
  2. Spread each piece of dough on a floured work surface, cut with a round cookie cutter of 12 cm diameter, and put in the center of each round stuffing and fold into slippers. 
  3. Weld each slipper with a fork, pressing on the edges 
  4. Place the slippers on the baking sheet lined with parchment paper. 
  5. Brush with an egg brush and bake in a preheated oven at 170 ° C for 20 to 25 minutes. 



[ ![minced meat turnovers](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/chaussons-a-la-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/chaussons-a-la-viande-hach%C3%A9e.jpg>)
