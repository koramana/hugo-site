---
title: Bourek chicken with bechamel
date: '2012-06-27'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/bourak-au-poulet-a-la-bechamel_thumb.jpg
---
##  Bourek chicken with bechamel 

Hello everybody, 

this vourek chicken bechamel is one of those small entries that I prepared with my friend for the buffet on the occasion of the circumcision of his son. 

These bechamel chicken bourek have made their success, my friend was very proud because it did not remain a crumb. I remember that the first time I tasted these delicious chicken bechamel bourek, I was barely 10 years old. My aunt did it for guests and I was only entitled to one piece as a reward for my help (because I had made the frying) ... The taste was engraved for life in my head, and I realize this little entry whenever I have the opportunity.   


**Bourek chicken with bechamel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/bourak-au-poulet-a-la-bechamel_thumb.jpg)

portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 1 chicken breast 
  * a handful of pitted green olives 
  * [ Bechamel ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video") quite thick 
  * a few sprigs of parsley 
  * grated cheese 
  * olive oil 
  * salt pepper 
  * brick sheets 



**Realization steps**

  1. Clean the chicken breast, cut into small pieces and fry in a pan with oil, salt and pepper. 
  2. prepare a thick béchamel. 
  3. mix, chicken pieces, olives and chopped parsley, bechamel, reserve. 
  4. when everything is at room temperature, add the grated cheese. 
  5. fill the leaves with bricks, and fold them into cigars 
  6. fry in a bath of hot oil, until you have a nice color. 



enjoy with a salad, or with a good  [ chorba at frik ](<https://www.amourdecuisine.fr/article-25345575.html>) au ramadan. 
