---
title: Provencal sauce
date: '2012-04-07'
categories:
- sweet recipes
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/riz-sauce-provencale11.jpg
---
![Provencal sauce](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/riz-sauce-provencale11.jpg)

##  Provencal sauce 

Hello everybody, 

Provencal rice sauce we love the rice at home, and for this time I realized rice sauce of Provence, a [ vegetarian recipe ](<https://www.amourdecuisine.fr/categorie-12348553.html>) , that I prepare, when I have a little bit of **cooked rice** , and that I do not want to throw. 

the Provencal sauce rice recipe is very simple and delicious, and do not take much time, yum yum with rice, pasta or just with a piece of bread.   


**Provencal sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/riz-sauce-provencale.jpg)

**Ingredients**

  * cooked rice 
  * tomatoes peeled and seeded 
  * 1 red and / or green pepper 
  * 1 onion 
  * 1 tbsp with olive oil 
  * 1 tbsp tomato paste 
  * basil 
  * parsley 
  * 1 handful of pitted olives 
  * ½ tablespoon garlic 
  * salt pepper 



**Realization steps**

  1. In a frying pan, sauté the chopped onion, red pepper and green pepper. in olive oil. 
  2. add the tomatoes, the tomato paste diluted in a little water, the garlic, a little basil and parsley, the olives pitted, cut into slices, 
  3. add salt and pepper. 
  4. Simmer on low heat. 
  5. after reducing the sauce, remove from heat. 
  6. Arrange the rice on a plate and pour the provancal sauce over it. 



![rice-sauce-provencal-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/riz-sauce-provencale-11.jpg)
