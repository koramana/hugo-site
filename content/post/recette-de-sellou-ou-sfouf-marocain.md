---
title: recipe of sellou or Moroccan sfouf
date: '2018-05-17'
categories:
- Moroccan cuisine
- Cuisine by country
- dessert, crumbles and bars
- sweet recipes
tags:
- Ramadan 2018
- Honey
- Ramadan
- Chamia
- Kalb el louz
- Cakes
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/05/sellou-marocain-3-683x1024.jpg
---
![recipe of sellou or Moroccan sfouf](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/sellou-marocain-3-683x1024.jpg)

##  recipe of sellou or Moroccan sfouf 

Hello everybody, 

It is the Ramadan of the year 2018, and it is the first recipe that I realize for this month, the Moroccan sellou, or selou or the Moroccan sfouf. An ideal recipe for the evening with a good mint tea. 

I opted for a Moroccan sellou recipe of any simplicity and that I adopted the way is the result is just succulent. If you need more recipes for Ramadan evenings, why not see the recipe from [ Mesfouf with raisins ](<https://www.amourdecuisine.fr/article-couscous-sucre-seffa-mesfouf.html>) , the [ seffa with coconut ](<https://www.amourdecuisine.fr/article-seffa-special-khotba-couscous-a-la-noix-de-coco.html>) , the [ rfiss constantinois ](<https://www.amourdecuisine.fr/article-rfiss-constantinois-recette-en-video.html>) , the [ zlabia ](<https://www.amourdecuisine.fr/article-zlabia-reussie-ramadan-2017.html>) , the [ Griwech ](<https://www.amourdecuisine.fr/article-griwech-gateau-algerien-en-video.html>) and also the [ samsa ](<https://www.amourdecuisine.fr/article-samsa-ou-triangles-farcis-damandes.html>)

Otherwise, I made you a video of the preparation of the Moroccan sellou recipe: 

**recipe of sellou or Moroccan sfouf**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/sellou-marocain-5.jpg)

**Ingredients**

  * 400 gr of grilled flour 
  * 200 gr grilled sesame seeds (grind 170 g and leave 30 gr whole) 
  * 200 gr toasted almonds (grind 170 gr in paste and leave 30 gr coarsely crushed) 
  * ½ c. coffee gum arabic 
  * 1 C. cinnamon powder 
  * 1 C. powdered fennel 
  * 1 C. powdered anise 
  * honey 
  * clarified butter or butter as needed 



**Realization steps**

  1. Start by grilling the flour and sift it. 
  2. roast the almonds in the oven, grind a quantity until you obtain an almond paste 
  3. and grind another amount to have roughly crushed almonds 
  4. roast the sesame seeds in the oven or pan, then grind until you have a dough of sesame seeds. leave a quantity without grinding it (which will give crunch to sellou) 
  5. grind the grains of fennel, and the grains of anise, and sift them into a fine powder 
  6. melt butter or clarified butter (you can even add olive oil if you like the taste) 
  7. in a large salad bowl, place all the ingredients and mix to have a dough that picks up 
  8. Add the butter and honey according to your taste to have a sellou that crumbles a bit like mine, or to have a sellou paste. 



![recipe of sellou or Moroccan sfouf 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/sellou-marocain-4.jpg)
