---
title: Algerian patisserie Basboussa in video بسبوسة سهلة
date: '2013-07-11'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-0081.jpg
---
![Algerian patisserie Basboussa in video](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-0081.jpg)

##  Algerian patisserie Basboussa in video بسبوسة سهلة 

Hello everybody, 

my delicious Algerian pastry Basboussa in video. Yes yes, basboussa video just for you, and I promise you something, try this recipe you will not be disappointed, it replaces super good our delicious [ qalb elouz ](<https://www.amourdecuisine.fr/article-35768313.html>) finally, when we can not succeed. 

In any case why I give pride of place to this Algerian pastry recipe, Basboussa video, because every time I do it, it is a huge success, and where I take it, I am asked the   
recipe, I tell them: she is on the blog, I'm told we can not find her. so I put you some new pictures, do not forget to always visit the [ index of Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) you will find my recipes with the   
photos, so you will not get lost, and also do not forget to go to the search window to find any article, it's at the top of the blog. 

![Algerian patisserie Basboussa in video](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-11.jpg)

**algerian patisserie, Basboussa in video بسبوسة سهلة**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-3-007-300x225.jpg)

Recipe type:  Algerian cake, dessert  portions:  12  Prep time:  15 mins  cooking:  60 mins  total:  1 hour 15 mins 

**Ingredients** for the basboussa: 

  * 4 eggs 
  * 1 glass of sugar 
  * 1 glass of milk 
  * 1 glass of oil 
  * 2 bags of yeast 
  * 1 glass and ½ semolina 
  * 1 glass of breadcrumbs (or bread that you simply crush) 

for the syrup: 
  * 1 kilo of sugar 
  * 1 liter of water 



**Realization steps** for the syrup: 

  1. let everything cook on a low heat, you can perfume it according to your taste, 

for the basboussa: 
  1. mix all the ingredients (apart from the syrup), 
  2. butter mold 25 x 40cm (mine is Pyrex) 
  3. place in a preheated oven at 180 degrees, cook for 30 to 45 minutes or until the tip of a knife inserted in the cake is dry. 
  4. at the end of the basboussa of the oven, sprinkle with warm syrup, little by little, until it absorbs everything (not to oblige to put all the syrup prepared) 



Note a small remark:   
  
I reduce the amount of sugar in the cake, sometimes I do not even put it, but usually I put ½ glass. 

method of preparation: 

a small remark: 

I decrease the amount of sugar in the cake, sometimes I do not even put it, but usually I put 1/2 glass.   


![Algerian pastry, Basboussa in video](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-3-0071.jpg)

suivez moi sur: 
