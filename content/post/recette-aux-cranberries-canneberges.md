---
title: cranberry / cranberry recipe
date: '2013-01-07'
categories:
- Algerian cuisine
- diverse cuisine

---
Hello everybody, 

Cranberries or cranberries do not contain fat, they are cholesterol free and contain very little sodium. Whole cranberries are a good source of dietary fiber, and all cranberry products contain flavonoids and polyphenols, natural compounds that offer a wide range of health benefits. 

There are many research studies that help explain how cranberries can help maintain heart health. 

today, I post you a recipe with this Fruit that Lunetoiles has passed me for a while, now, and that I did not have time to post you. so here is the opportunity: 

For a mold 18 to 20 cm in diameter 

ingredients 

  * 140 g flour 
  * 120 g butter or margarine melted and warmed 
  * 140 g of sugar 
  * 3 eggs 
  * 1/2 teaspoon baking powder 
  * lemon zest 
  * 70 g raisins (for me dried cranberries) 
  * vanilla 



Preparation 

  1. Soften the raisins (or cranberry) in boiling water. 
  2. In a bowl, beat the sugar and butter with an electric mixer. 
  3. Add the eggs one by one, beating well between each addition to obtain a homogeneous mixture. 
  4. Add the zest of lemon, yeast, vanilla and sifted flour stirring with a wooden spoon. 
  5. Dry the grapes (or cranberry) and add to the preparation. 
  6. Pour the dough into a silicone mold or else a buttered and floured mold. 
  7. Bake in Th.5 oven for 30-35 min. 
  8. Unmold on a rack and let the cake cool. 
  9. At serving time sprinkle with icing sugar. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
