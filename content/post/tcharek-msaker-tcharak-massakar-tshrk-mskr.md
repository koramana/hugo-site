---
title: Tcharek m'saker Tcharak massakar, تشارك مسكر
date: '2011-10-31'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tcharek-msaker11.jpg
---
![msaker1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tcharek-msaker11.jpg)

##  Tcharek m'saker Tcharak massakar, تشارك مسكر 

Hello everyone, 

Tcharek m'saker Tcharak massakar, تشارك مسكر is another version of cakes charek or horn of gazelle. 

Thanks to our dear Lunetoiles who loved to share with us the photos and the recipe of Tcharek m'saker Tcharak massakar, تشارك مسكر as she did at home. I really like the chak m'sakar, but when I prepare it, I'm the only one to eat it, because my husband hates sugar-coated cakes ... and my children, they lick the sugar and leave the cake like that, hihihihihih ... And me the kilos do not spare me the small mistake, hahahahaha 

![tcharek msakar-1](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/tcharek-msakar-1_thumb1.jpg)

**Tcharek m'saker Tcharak massakar, تشارك مسكر**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/tcharek-messakar-12_thumb1.jpg)

**Ingredients**

  * Ingredients for the dough: 
  * 3 measures of flour (500 gr) 
  * 1 measure of melted fat smen or margarine (166 gr) 
  * 1 pinch of salt 
  * 2 tablespoons sifted icing sugar 
  * 1 C. dessert vanilla extract 
  * 1 teaspoon of orange blossom water 
  * water needed 

Ingredients for the stuffing: 
  * 2 measures of finely ground almonds (200 gr) 
  * 1 measure sifted icing sugar (100 gr) 
  * 1 C. melted fat 
  * 1 C. dessert vanilla extract 
  * Orange tree Flower water 
  * Ingredients for the syrup: 
  * ½ liter of water 
  * 1 teaspoon of orange blossom water 
  * 250 gr of crystallized sugar 



**Realization steps**

  1. Put the sifted flour with the icing sugar, vanilla extract and salt. 
  2. Make a fountain and add the melted and cooled fat. 
  3. Rub well with the palms of your hands to penetrate the fat into the flour. 
  4. Moisten with orange blossom water and the necessary water by kneading well until soft and homogeneous. 
  5. Divide it into small balls the size of a small tangerine, place in a tray, cover with food paper and let stand. 
  6. Meanwhile prepare the stuffing with the ingredients given above and in the order of presentation. 
  7. Shape small sausages 8 cm long and half a centimeter in diameter. 
  8. Place them on a dish sprinkled with icing sugar. 
  9. On a floured work surface, lower the balls of pasta, cut circles about 10 cm, and spread the circles more finely in a slightly oval shape.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/faconnage-tcharek-msaker.jpg)
  10. Place the stuffing pudding on the edge of the circle, roll while pressing slightly while giving the shape of a crescent. 
  11. Place the cakes on the baking sheet and bake in the oven (180 ° C). 
  12. They must have a golden color. 
  13. Remove from the oven and let cool. 
  14. Prepare the syrup with the given ingredients. Boil the syrup for 15 minutes and until it thickens. 
  15. Dip the cakes in the cooled syrup, drain slightly and roll in icing sugar. 
  16. Let dry. The next day, roll the cake again in the icing sugar. 
  17. Present in boxes. 


