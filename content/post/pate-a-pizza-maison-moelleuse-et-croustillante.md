---
title: crispy homemade pizza dough
date: '2013-06-03'
categories:
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/pizza-012_thumb-300x178.jpg
---
##  Homemade and crispy homemade pizza dough 

Hello everybody, 

home-made pizza is always more appreciated at home, because everyone loves its filling, "without cheese for me" will say one, "without olives for me" will say another, "with pizzazz for me" will say another. so not better than the homemade pizza. 

and here, I give you my pizza pie very soft, crispy, a happiness to each mouth.   


**crispy homemade pizza dough**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/pizza-012_thumb-300x178.jpg)

portions:  4  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 3 glasses of flour (220 ml each) 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 tablespoon instant baker's yeast 
  * 1 teaspoon of baking powder 
  * 3 tablespoons of oil 
  * some water 



**Realization steps**

  1. I make my pizza pie in the bread machine, but you can easily make this pasta by hand, and with the robot. 
  2. put the ingredients in the order recommended for the bread machine. add the water according to the flour and its absorption. start program only, program number 8 for this device, 



at the end of the program, remove the dough from the machine, place it in an oil tray, flatten the dough and let it rise again 

garnish the dough according to your taste 

good tasting, because I swear it was a real delight, at least my father really love 

do not forget to subscribe to the newsletter if you want to be up to date with all my new publications 

merci 
