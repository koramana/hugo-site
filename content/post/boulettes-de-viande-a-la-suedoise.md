---
title: Swedish meatballs
date: '2018-01-02'
categories:
- diverse cuisine
tags:
- Beef balls
- Algeria
- dishes
- Full Dish
- sauces
- Meat
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-viande-a-la-suedoise-1.jpg
---
[ ![meatballs in Sweden 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-viande-a-la-suedoise-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-viande-a-la-suedoise-1.jpg>)

##  Swedish meatballs 

Hello everybody, 

I really like to go to Ikea on weekends with children, firstly because the children play and wander around without losing sight of them, and secondly because I like to see all these beautiful kitchens that make me dream every time. 

And when you go to Ikea, it's for most of the day, so you eat lunch at noon. We always ask for fish and chips, I really like the tartar sauce that accompanies them. My children say to me: Mom the meatballs look good too, but I never order them because it's not Halal. 

To make them happy, I made this recipe for Swedish meatballs that I found on an American blog. We loved it too much, but I can not guarantee you that it is the authentic recipe, because I have never tasted it. 

[ ![meatballs in Sweden 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-viande-a-la-suedoise-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-viande-a-la-suedoise-2.jpg>)   


**Swedish meatballs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-viande-a-la-suedoise-3.jpg)

portions:  5  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * for the dumplings: 
  * 300 gr of ground beef 
  * 1 grated raw potato (a touch of my hand instead of breadcrumbs, which gives very tender meatballs) 
  * 1 small chopped onion 
  * 1 clove garlic minced 
  * 1 C. chopped oregano 
  * 1 egg 
  * 1 small chopped parsley 
  * for the sauce: 
  * 5 c. butter 
  * 3 c. flour 
  * 400 ml beef broth (warmed) 
  * 150 ml thick cream 
  * 1 C. coffee Dijon mustard 
  * salt and black pepper according to taste 



**Realization steps**

  1. In a large skillet, heat 1 tbsp. olive oil with 1 tablespoon butter, over medium heat. 
  2. Add onion and fry until translucent, about 5 minutes, add garlic and oregano and brown for 1-2 minutes. 
  3. In a large bowl, combine ground beef, sauteed onion, garlic, grated potato and oregano. 
  4. add salt, pepper, chopped parsley and egg 
  5. shape dumplings of the same size. 
  6. fry the meatballs in a little oil and butter over medium heat, 
  7. Transfer the meatballs to a plate, and keep warm 

Prepare the sauce 
  1. Add 4 tablespoons the butter in the same pan where you have cooked the meatballs. When melted add the flour and stir until golden brown. 
  2. Stir in the heated beef broth slowly and cook on medium heat. 
  3. Add the cream, Dijon mustard, and simmer until the sauce thickens. 
  4. put the meatballs in sauce, cover and simmer on low heat for about 10 minutes. 
  5. Serve with pasta, or a [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html>) . It's too good. 



[ ![meatballs in Sweden](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-viande-a-la-suedoise.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-viande-a-la-suedoise.jpg>)
