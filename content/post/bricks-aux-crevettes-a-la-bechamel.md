---
title: shrimp brigs with bechamel
date: '2017-04-27'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- ramadan recipe
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame1.jpg
---
[ ![shrimp brigs with bechamel](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame1.jpg>)

##  Bread shrimp with bechamel 

Hello everybody, 

These bechamel shrimp bricks are a recipe that I highly recommend, shrimp bricks that can accompany a chorba, a soup, or just a salad. 

These bechamel shrimp bricks are so light that you will not even be able to count how many pieces you've eaten ... lol. 

I already made these bechamel shrimp bricks in another version, the [ chicken bourks with bechamel ](<https://www.amourdecuisine.fr/article-bourek-au-poulet-a-la-bechamel.html>) , and I guarantee you all the yum yum of the world. When I told my husband that I made shrimp bricks with bechamel, he told me, why not [ shrimp and potato boureks ](<https://www.amourdecuisine.fr/article-boureks-aux-crevettes-et-pommes-de-terre.html> "shrimp and potato boureks") , as you used to do, but when he tasted ... He left nothing! 

{{< youtube Weub9CYxerM >}} 

I used for this recipe flour leaves bricks that are sold here in the oriental shops, they are in lengths, ideal for this form of bricks (samossa), Frankly this pasta adds a lot of taste to these bricks with bechamel shrimps.   


**shrimp brigs with bechamel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame3-600x400.jpg)

portions:  6  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * bricks leaves 
  * 250 g shelled and cleaned shrimps 
  * a handful of pitted green olives 
  * béchamel quite thick 
  * a few sprigs of parsley 
  * grated cheese 
  * olive oil 
  * salt pepper 



**Realization steps**

  1. Clean the shrimp and fry in a pan with oil, salt and pepper. 
  2. prepare a thick béchamel, and let cool well. 
  3. mix shrimp, sliced ​​olives, chopped parsley and bechamel. 
  4. add the grated cheese. 
  5. fill the leaves with bricks, and fold them into triangles 
  6. fry in a bath of hot oil, until you have a nice color. 



[ ![shrimp brigs with bechamel](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame2.jpg>)
