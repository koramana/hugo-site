---
title: Algerian Frita
date: '2014-07-02'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- idea, party recipe, aperitif aperitif
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/frita-algerienne-3.CR2_.jpg
---
[ ![Algerian frite 3.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/frita-algerienne-3.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/frita-algerienne-3.CR2_.jpg>)

##  Algerian Frita 

Hello everybody, 

The frita is a dish made with peppers and tomatoes, it looks a bit like hmiss, but normally the fried is realized without the addition of garlic ... But I'll be frank with you, we like the taste of the Garlic at home, which I put a little inside, without crushing it finely. In fact, I put in my Algerian frite, whole piece of garlic, just to have that incomparable flavor of garlic ... but you can do without it ok ... 

The recipe I printed it a while ago from a site I can not find anymore. So, if this recipe is from your site, or blog, let me know, so I add your link. 

**Algerian Frita**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/frita-algerienne-1.CR2_.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 2 to 3 red peppers according to their sizes 
  * 4 fresh tomatoes 
  * 3 tablespoons of olive oil 
  * 2 cloves garlic 
  * salt 
  * pepper 



**Realization steps**

  1. grill the peppers, and especially watch for not too much burn them 
  2. then put them in a plastic bag for 15 minutes, to pound them easily. 
  3. Peel and seed the tomatoes, and cut them into quarters. 
  4. Peel the garlic and cut it in half lengthwise. 
  5. Now clean the peppers, remove all the black skin, and remove the pips 
  6. Cut the peppers into large strips. 
  7. Heat 2 tablespoons of oil in a thick bottomed pot. 
  8. Put the garlic back just to smell, then add the tomatoes and peppers. 
  9. Salt, pepper and add the remaining oil and stir a little. 
  10. Simmer uncovered, stirring occasionally, until the water completely disappears, and you see oil on the surface. 
  11. Let cool and serve with a good [ homemade bread ](<https://www.amourdecuisine.fr/article-khobz-dar-pain-maison.html> "khobz dar: homemade bread")



[ ![Algerian frita.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/frita-algerienne.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/frita-algerienne.CR2_.jpg>)
