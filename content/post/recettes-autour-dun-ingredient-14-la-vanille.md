---
title: 'Recipes around an ingredient # 14 vanilla'
date: '2016-01-06'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
[ ![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg>)

source: Pinterest 

Hello everybody, 

Well now that you know everything, I'm sure you're wondering what I was able to choose as an ingredient, it's true that January is not the most prolific month in the garden ^^, and then I told myself that a little tour under the heaviest weather would make us happy, we all need a little sweetness, after the euphoria of the holidays the beginning of the year is often a little sad, especially if we have taken as a resolution to go on a diet after the excesses of the end of the year (but hey that's another story: p), all that to tell you that I chose LA VANILLE.   
[ ![vanilla-copy](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/vanille-copie.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/vanille-copie.jpg>)

Just a note: Helene is hosted on blogspot, and to leave a comment, you do not have to leave an email, out so that helene can contact you in order to pass you the list of participants, it is better, to write your email, in the comment box. 

Thank you girls, and here is the rule of our round: 

_**_Principle of the game_ ** : _

  1. _Choose a seasonal ingredient and accessible to all over the world_
  2. _The godmother will publish this article on her blog between the 5th and the 8th of the month, citing the links of the initiators of this game._
  3. _Announce the ingredient that will be the star of the current month._
  4. _Bloggers who want to participate, will contact directly the animator of the game so that she can establish the list of the participants in the game._
  5. _On the 1st of the following month (at the latest), the participants in the game will have to send the titles of their recipes and the link of their blog to the godmother._
  6. _The sponsor will publish the final list of participating blogs and their recipes, which she will send by mail to all participants._
  7. _All the recipes must be put on line the 3rd of the month, with at the bottom the list sent by the godmother of the game._
  8. _The sponsor will then choose among the participants, a blogger who will take over and announce the new ingredient for the following month._



_**_New since edition # 12 ..._ ** _

_A Facebook page has been created so that participants can submit the url of their blog and the title of their recipe, to simplify the task of the godmother and participants.  
As soon as your recipe is ready, tell us its title here:   
_

_**_How to participate :_ ** _ _**Les rondes précédentes :** _
