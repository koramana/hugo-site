---
title: fougasse of Asmaa
date: '2011-07-11'
categories:
- gateaux, et cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/Concours-ramadan-chez-Soulef_thumb2.jpg
---
hello everyone, today by opening my email, I find these beautiful hot photos, huuuuuuuuuuuuuuum, well perfume, a recipe sent by one of my reader, she signed Omo-Ziyad, so be welcome my dear, on my blog, and your recipe too, hihihihi so with great pleasure I share with you: ingredients for the dough: 600g of flour 200g of water 2 c with dry yeast 2 c with oil of d olive a little sugar (I forgot) a little salt + a little oregano Ingredients for the stuffing: 3 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

today by opening my email, I find these beautiful pictures hot, huuuuuuuuuuuuuuum, well perfumees, a recipe sent by one of my reader, she signed Omo-Ziyad, so be welcome my dear, on my blog, and your recipe too, hihihihi 

so with great pleasure I share with you: 

** ingredients for the dough:   
**   
600g of flour   
200g of water   
2 tbsp dry yeast   
2 tablespoons olive oil   
a little sugar (I forgot)   
a little salt   
\+ some oregano   
** Ingredients for the stuffing:  **

3 minced onions   
homemade tomato sauce   
anchovy fillets (or tuna) 

** Preparation:   
**

Put all the ingredients in order in the bowl of the bread maker and start the program "Dough leavened". 

Melt the onions in a pan with the olive oil, without coloring them.   
Once the dough ready, spread out there in a large area, spread on half tomato sauce, sprinkle with onions, sprinkle with grated cheese.   
Close the fougasse weld the edges, and cut the notches.   
sprinkle with grated gryere and anchovy fillets (or tuna)   
Bake 15 to 20 min depending on the size of the fougasse. 

thank you my dear for sharing 

and thank you to everyone for your visits and comments. 

have a good day. 

![https://www.amourdecuisine.fr/wp-content/uploads/2011/07/Concours-ramadan-chez-Soulef_thumb2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/Concours-ramadan-chez-Soulef_thumb2.jpg)
