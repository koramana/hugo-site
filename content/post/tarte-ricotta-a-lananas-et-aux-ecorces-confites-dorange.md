---
title: Ricotta pie with pineapple and candied orange peel
date: '2010-04-29'
categories:
- amuse bouche, tapas, mise en bouche
- Bourek, brick, samoussa, chaussons
- Algerian cuisine
- Moroccan cuisine
- ramadan recipe

---
I bring you this delicious recipe that I made a long time ago. so before the recipe, I tell you my story with this cake, that I had prepared a week ago, but that I had not yet post: Me in the kitchen: Rayan, what are you doing? I do not like this silence .......... Him in the living room: Nooooooooooooooooooo (he likes to say that my son) Me: I'm comming (I'm coming) Him: Noooooooooooooo rayan photographer Me (very concerned in the kitchen): it's good you want to become a photographer, you'll take pictures of mom? Him: Winkiwinki (his teletubbies) Thomas (a & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

I bring you this delicious recipe that I made a long time ago. 

so before the recipe, I tell you my story with this cake, that I had prepared a week ago, but that I had not yet post: 

** Me  ** in the kitchen: Rayan, what are you doing? I do not like this silence .......... 

** Him  ** in the living room: Nooooooooooooooooooo (he likes to say that my son) 

** Me  ** : I'm coming 

** Him:  ** Noooooooooooooo rayan photographer 

Me (very concerned about the kitchen): it's good you want to become a photographer, you'll take pictures of mom? 

Him: Winkiwinki (his teletubbies) Thomas (a train he loves a lot and who goes on TV) 

I finish what I have on hand, and I go back to the salon, and rayan gave me the back, I did not pay attention, then he said: 

Kesserta (so be careful, my son speaks Arabic, French and English, with his childish language) and he means I broke it. 

I come to see and it was my camera in his hands, I do not panic, because he used to touch it, so yesterday I just put the pictures on the pc, and I find that it is missing, eh ben: "kesserta" by rayan that was it, 

so the result, a cake without cooking ......... .euh I mean, today I post a recipe for a cake before it is baked .......... 

Well, the photos after cooking ............... ..Kesserta as said rayan. 

yes, sorry for that, but the cake was very good, and I have to wait for another opportunity to do it, because it was for a small invitation, at home, and since my husband does not eat the cheese cakes, I'm looking for always opportunities to do something. 

Ingredients: 

  * 250 gr of ricotta 
  * 3 eggs 
  * 1 bulging case of flour 
  * 50 gr of powdered sugar 
  * 2 cases of candied orange peel 
  * pineapple slices in syrup 
  * Optional icing sugar 



Casser 2 oeufs en séparant les blancs des jaunes. Dans un plat creux, écraser la ricotta à la fourchette, ajouter les deux jaunes et l’oeuf entier, bien mélanger. Ajouter la farine en la tamisant, le sucre, l’écore d’orange confite. Monter les blancs en neige avec une pincée de sel et les incorporer délicatement à la préparation à la ricotta. Verser la pâte dans un moule à manqué préalablement garni d’une feuille de papier sulfurisé, le mien était en silicone, donc pas besoin de ça.. Ajouter les rondelles d’ananas sur le dessus et parsemer de sucre en poudre. Faire cuire 35 min  & laisser tiédir. Poudrer de sucre glace et déguster. 
