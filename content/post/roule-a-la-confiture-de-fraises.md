---
title: Rolled with strawberry jam
date: '2017-03-12'
categories:
- cakes and cakes
- sweet recipes
tags:
- Based
- Soft
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/roul%C3%A9-%C3%A0-la-confiture-de-fraises-2.jpg
---
![rolled with strawberry jam 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/roul%C3%A9-%C3%A0-la-confiture-de-fraises-2.jpg)

##  Rolled with strawberry jam 

Hello everybody, 

I do not think I met a person who does not like biscuit rolled with strawberry jam. It is a cake that always finds its place in the tables of taste, or for a coffee between friends ... 

Today this strawberry jam roll comes directly from the kitchen of my friend Samy Aya, whom you have always known on my blog as fleurDz. Its recipes always figured in my articles: My recipes at home ... what happiness for me, that one of my faithful readers becomes over time, one of the publicators on my blog. 

You can find on my blog, my recipe for [ rolled with jam ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture.html>) It must be said that our methods are a little different, but the result is the same a very delicious and super mellow. 

Besides, here is Samy Aya's video recipe, and do not forget this opportunity to subscribe to my youtube channel: 

**Rolled with strawberry jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/roul%C3%A9-%C3%A0-la-confiture-de-fraises-1.jpg)

**Ingredients**

  * 6 separate eggs (yellow + white) 
  * 1 sachet of baking powder 
  * 1 C. vanilla coffee 
  * 120 gr of sugar 
  * 150 gr of flour 
  * chocolate vermicelli (optional) 
  * decoration: 
  * Strawberry jam. 
  * Orange tree Flower water 



**Realization steps**

  1. start by whipping the egg yolks with the vanilla. 
  2. add the sugar in a small amount and continue whisking until you have a whitish mixture. 
  3. add the sifted flour gently with a spatula. the mixture will become a little tight. 
  4. whip the egg whites into snow. 
  5. have the flour dough softened with a little egg white in the snow. 
  6. add baking powder and chocolate vermicelli (optional) 
  7. introduce these two ingredients, then add to the mixture the egg white gradually while introducing it without breaking it, to have a dough well ventilated and homogeneous. 
  8. pour the mixture into a tray about 30 x 40 cm, lined with baking paper. 
  9. place in a preheated oven at 180 ° C for 10 to 12 minutes. 
  10. on leaving the oven, remove the baking paper, and roll the biscuit into another sheet. and let it rest for at least 5 minutes so that the roll is in shape 
  11. dilute the jam with a little orange blossom. 
  12. unroll the biscuit and garnish with the strawberry jam. 
  13. roll again, let rest and then cut slices of almost 2 cm wide. 



![rolled with strawberry jam](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/roul%C3%A9-%C3%A0-la-confiture-de-fraises.jpg)
