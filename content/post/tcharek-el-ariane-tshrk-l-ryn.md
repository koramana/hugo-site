---
title: Tcharek el Ariane تشاراك العريان
date: '2017-06-17'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/06/tcharek-el-ariane-683x1024.jpg
---
![tcharek el ariane تشاراك العريان](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/tcharek-el-ariane-683x1024.jpg)

##  Tcharek el Ariane تشاراك العريان 

Hello everybody, 

Tcharek el Ariane تشاراك العريان, is one of the most famous Algerian cakes specilatés. These cakes are too good, not too sweet and easy to make. This recipe comes to me from Lunetoiles who first published the recipe on my blog in 2011/2012. 

The recipe for this Tcharek el Ariane has been very successful near my readers who have tried, so do part and tell me your opinion. 

Me too, since I tried the recipe, I adopted it and here I realize it this time for you and on video, so jump on my youtube channel, and do not forget to subscribe: 

{{< youtube ZVMfrNHL5SA >}} 

This recipe is from the book Gateaux oriental El Djazeiria by Mrs. Grine Fatima 

**Tcharek el Ariane تشاراك العريان**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/tcharek-el-ariane-1.jpg)

**Ingredients** Ingredients for the dough: 

  * 3 measures of flour 
  * 1 measure of melted margarine 
  * 1 pinch of salt 
  * ¾ measure of icing sugar 
  * 1 egg 
  * ½ cuil. coffee baking powder 
  * vanilla extract (1 teaspoon) 
  * orange blossom water (5 tablespoons) 
  * water 

Prank call : 
  * 2 measures of finely ground almonds 
  * 1 measure icing sugar 
  * 3 tablespoons melted margarine 
  * vanilla extract, 1 tbsp. coffee 
  * Orange tree Flower water 



**Realization steps**

  1. Preparation: 
  2. In a bowl, mix the sifted flour with the pinch of salt, icing sugar and yeast. 
  3. Make a fountain, add egg, melted and cooled margarine as well as vanilla extract. 
  4. Work with the apple of the hand, sprinkle with orange blossom water and a little water, necessary by kneading until the dough is smooth. 
  5. Divide the dough into small balls the size of a tangerine. Let stand about 30 minutes. Meanwhile prepare the stuffing. 
  6. On a floured worktop, lower each triangle ball 10 cm base and 15 cm sides. 
  7. Place a small almond paste on the base of each triangle and roll it in the shape of croissants then brush with the beaten egg and sprinkle with flaked almonds.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/tcharel-ariane.jpg)
  8. Bake at 180 ° C thermostat 6, until they are golden brown. 
  9. When they are cold, sprinkle with icing sugar to decorate. 



![tcharek el ariane 2 تشاراك العريان](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/tcharek-el-ariane-2.jpg)
