---
title: Kitchen love recipes tested and approved 85
date: '2015-11-29'
categories:
- the tests of my readers
tags:
- desserts
- Cakes
- Algeria
- Morocco
- dishes
- tajine
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg>)

Hello everybody, 

Another article containing the photos of your recipes from my blog. It is long again, and I still have not done all the photos. If you can not find your photo on this article, [ Article 82 ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-82.html>) , [ Article 81 ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-81.html>) , and [ Article 83 ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-83.html>) so she's going to be in section 85, so wait, she'll be in line as soon as possible. 

If you are not a member of this group, join me on it, otherwise if you do not have a facebook account, keep sending me the pictures on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

[ ![lemon pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tarte-au-citron.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tarte-au-citron.jpg>)

[ lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee.html>) at Nesrine Sissou 

[ ![dyoul sali house](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/dyoul-maison-sali1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/dyoul-maison-sali1.jpg>)

[ homemade dyouls ](<https://www.amourdecuisine.fr/article-dyouls-ou-feuilles-de-brick-fait-maison.html>) at Sali N. 

[ ![apricot pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tarte-aux-abricots.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tarte-aux-abricots.jpg>)

[ apricot pie ](<https://www.amourdecuisine.fr/article-tarte-dabricots-creme-patissiere.html>) at Sali N. 

[ ![far breton.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/far-breton.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/far-breton.bmp.jpg>)

[ far breton ](<https://www.amourdecuisine.fr/article-far-breton-aux-pruneaux.html>) at Samy Aya 

[ ![burger pie wamani.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tourte-burger-wamani.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tourte-burger-wamani.bmp.jpg>)

[ burger pie ](<https://www.amourdecuisine.fr/article-tourte-burger-a-la-viande-hache.html>) at Wamani M 

[ ![danette sanaa cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gateau-danette-sanaa.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gateau-danette-sanaa.jpg>)

[ cake in the water ](<https://www.amourdecuisine.fr/article-gateau-la-danette-au-chocolat.html>) at Sanaa C 

[ ![pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/pate-bris%C3%A9e-younes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/pate-bris%C3%A9e-younes.jpg>)

[ pastry ](<https://www.amourdecuisine.fr/article-recette-pate-brisee-maison-facile.html>) at Oum Younes L. 

[ ![hachi parmentier.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/hachi-parmentier.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/hachi-parmentier.bmp.jpg>)

[ shepherd's pie ](<https://www.amourdecuisine.fr/article-hachis-parmentier.html>) at Fleur Dz 

[ ![focaccia](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/focaccia.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/focaccia.jpg>)

[ focaccia ](<https://www.amourdecuisine.fr/article-une-focaccia-au-romarin-et-oignon.html>) at Fleur Dz 

[ ![potato gratin with spinach](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gratin-de-pommes-de-terre-aux-epinards.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gratin-de-pommes-de-terre-aux-epinards.jpg>)

[ ![cigars with dates and coconut](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cigares-aux-dattes-et-noix-de-coco.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cigares-aux-dattes-et-noix-de-coco.jpg>)

[ cigars with dates and coconut ](<https://www.amourdecuisine.fr/article-cigares-aux-dattes-noix-de-coco.html>) at Ben Fati 

[ ![Turkish crepes .bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/crepes-turques-.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/crepes-turques-.bmp.jpg>)

[ Turkish crepes ](<https://www.amourdecuisine.fr/article-crepes-turques-a-la-viande-hachee-gozleme.html>) at Carina Dams 

[ ![matlou3 el koucha](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/matlou3-el-koucha.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/matlou3-el-koucha.jpg>)

[ matlou3 in the oven ](<https://www.amourdecuisine.fr/article-khobz-matloua-fel-koucha-matlou3-au-four.html>) at N-Houda Bouamama 

[ ![chinese nadia.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chinois-nadia.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chinois-nadia.bmp.jpg>)

the [ Chinese ](<https://www.amourdecuisine.fr/article-le-chinois.html>) at Fleur Dz 

[ ![tajine zitoune with minced meat.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-zitoune-a-la-viande-hach%C3%A9e.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-zitoune-a-la-viande-hach%C3%A9e.bmp.jpg>)

[ tajine zitoune with minced meatball ](<https://www.amourdecuisine.fr/article-tajine-boulettes-de-viande-hachee-et-olives-kefta-be-zitoune.html>) at Oum Younes London 

[ ![zroudia mchermla.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/zroudia-mchermla.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/zroudia-mchermla.bmp.jpg>)

[ carrot salad with chermoula ](<https://www.amourdecuisine.fr/article-salade-de-carottes-a-la-chermoula.html>) at Oum younes London 

[ ![semolina bread 1.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/pain-de-semoule-1.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/pain-de-semoule-1.bmp.jpg>)

[ semolina bread ](<https://www.amourdecuisine.fr/article-pain-de-semoule.html>) at Malika Oum Abdallah Azza. 

[ ![madeleine 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/madeleine-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/madeleine-1.jpg>)

[ Madeleines ](<https://www.amourdecuisine.fr/article-28402966.html>) at Naziha Strass 

[ ![biscuit rolled 1.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/biscuit-roul%C3%A9-1.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/biscuit-roul%C3%A9-1.bmp.jpg>)

[ Biscuit Roll ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture.html>) at Karima benbrahim Daas 

[ ![bread without filling](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/pain-sans-petrissage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/pain-sans-petrissage.jpg>)

[ bread without kneading ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) at Oumabdallah Larfi 

[ ![butter cake naima](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cake-pur-beurre-naima.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cake-pur-beurre-naima.jpg>)

[ four quarters pure butter ](<https://www.amourdecuisine.fr/article-recette-quatre-quart-pur-beurre-barre-bretonne.html>) at Naima Zidane 

[ ![Chicken muffin with chicken at Renales](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/muffin-sal%C3%A9-au-poulet-chez-Renales.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/muffin-sal%C3%A9-au-poulet-chez-Renales.jpg>)

[ Chicken muffins with chicken ](<https://www.amourdecuisine.fr/article-muffins-sales-au-blanc-de-poulet.html>) at Rénales Sélanir 

[ ![castles](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/castels.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/castels.jpg>)

[ Castels ](<https://www.amourdecuisine.fr/article-le-gateau-russe-methode-simplifiee-chez-les-patissiers-algeriens.html>) at Oumabdallah Larfi 

[ ![far breton 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/far-breton-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/far-breton-1.jpg>)

[ far breton ](<https://www.amourdecuisine.fr/article-far-breton-aux-pruneaux.html>) at Rinales Selanir 

[ ![tajine jben](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-jben.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-jben.jpg>)

[ tajine jben ](<https://www.amourdecuisine.fr/article-tajine-jben-tajine-de-fromage.html>) at Oumou Hamza 

[ ![tajine cauliflower and minced meatballs](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-choux-fleur-et-boulettes-de-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-choux-fleur-et-boulettes-de-viande-hach%C3%A9e.jpg>)

[ tajine with cauliflower and minced meat ](<https://www.amourdecuisine.fr/article-tajine-de-chou-fleur-et-boulettes-de-viande-hachee.html>) chez Elworoud blida 
