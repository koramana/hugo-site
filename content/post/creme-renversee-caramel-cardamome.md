---
title: Crème renversée caramel cardamome
date: '2015-08-28'
categories:
- panna cotta, flan, and yoghurt

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/creme-renversee-009a_thumb1.jpg
---
##  Crème renversée caramel cardamome 

Hello everybody, 

It is not an invention this cream spilled, but in truth, I wanted to make a panna cotta to the taste of cardamom, I infused the seeds of cardamom in hot milk, then I went to get my agar agar in my Famous closet, but impossible to get a hold of, well, I'm looking for nothing, in any sense, nothing ... 

So will I throw this milk to infuse? ... of course not, it's not my habit, I go to the fridge, and I find this plate of eggs waiting in the fridge, expiration date very close, so immediately the idea of ​​a creme brulee or so spilled cream came into my head, these two cream, love the eggs, but no need to put the egg white in the freezer, I already have plenty .... then we go for an inverted cream with cardamom aroma: 

**Crème renversée caramel cardamome**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/creme-renversee-009a_thumb1.jpg)

portions:  4  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients** ingredients for 4 ramekins: 

  * 500 ml of milk 
  * 4 eggs 
  * 100 gr of sugar 
  * 3 grains of cardamom 
  * 1 pinch of salt 

the caramel: 
  * 50 gr of sugar 
  * a few drops of lemon 



**Realization steps**

  1. heat the milk 
  2. place the cardamom seeds in it, and let it steep 
  3. Prepare the caramel, Melt the sugar moistened with water and a few drops of lemon juice, until it becomes caramel. 
  4. remove the cardamom from the milk, and heat again 
  5. Beat eggs and sugar in omelette and stir in milk, turning regularly. 
  6. cover individual mussels, or a large caramel mold 
  7. Pour the cream into the mold 
  8. cook in a bain-marie at 160 ° C. 
  9. The cream is cooked when it shakes slightly, 
  10. let cool, then put in the fridge 



merci pour vos commentaires et visites 
