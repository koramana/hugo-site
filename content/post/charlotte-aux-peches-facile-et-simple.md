---
title: charlotte with peaches easy and simple
date: '2017-09-03'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- sweet recipes
tags:
- To taste
- Cakes
- Algerian cakes
- Spoon biscuits
- Easy cooking
- Pastry
- Inratable cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/08/charlotte-aux-peches-1.jpg
---
![charlotte with the fishing 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/08/charlotte-aux-peches-1.jpg)

##  charlotte with peaches easy and simple 

Hello everybody, 

At home, my children take charlottes and bavarois, and I was surprised that my elder this year asked me a charlotte as a cake for his birthday. I certainly took advantage of the seasonal fruit to make the charlotte, and I made this beautiful and delicious peach charlotte which was not only a great success on the visual side, it was also a real delight. 

I would have liked to make the boudoirs at home, but as I was on vacation, I used the boudoirs of commerce, and it did not lose its charm to my beautiful charlotte. 

If you want to make this charlotte with [ boudoirs or biscuits with homemade spoon ](<https://www.amourdecuisine.fr/article-biscuits-a-la-cuillere-facile.html>) do not hesitate the result will only be better. 

If not before going to the recipe, I give you the list of all the previous godmothers and the star ingredients of our previous rounds: 

**charlotte with peaches easy and simple**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/08/charlotte-aux-peches-2.jpg)

**Ingredients**

  * biscuits with a spoon as needed 
  * 1 box of canned peaches. 
  * 4 egg yolks 
  * 120 g caster sugar 
  * 250 ml of milk 
  * 250 ml whole liquid cream 
  * 4 sheets of gelatin (8 gr) 

decoration: 
  * 3 to 4 peaches by size. 
  * jelly (optional) 



**Realization steps**

  1. Put the gelatin sheets to soften in a bowl of cold water. 
  2. In a saucepan, bring the milk to a boil. 
  3. In a bowl, whisk the egg yolks with the sugar until the mixture whitens. Mix with the hot milk while whisking. Pour into the pan. 
  4. Make thicken over very low heat without stirring or boiling. 
  5. remove the cream from the heat and add the drained gelatin. stir to dissolve it well. Let cool. 
  6. Whip the liquid cream very cold in chantilly (without adding the sugar). 
  7. Drain the peaches, cut in small dice. Stir them together with the whipped cream but still liquid. 
  8. Line the bottom and sides of a charlotte pan with biscuits (here I used a pastry circle). 
  9. wet with the peach syrup (canned peach) using a brush to soften the biscuits a little. 
  10. Fill it halfway with the peach cream. 
  11. Add another layer of biscuits to the spoon. and pour the second half of peach cream, and soften a little with the peach juice. 
  12. refrigerate for at least 6 hours (overnight for me).   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/charlotte-aux-peches-3.jpg)
  13. Unmount the charlotte when serving 
  14. decorate with cut peaches and jelly. 



![charlotte aux peches_](https://www.amourdecuisine.fr/wp-content/uploads/2017/08/charlotte-aux-peches_.jpg)

et voila la liste des participantes: 
