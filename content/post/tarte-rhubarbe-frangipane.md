---
title: Rhubarb tart / Frangipane
date: '2010-10-09'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- sweet recipes

---
yesterday in Lidl, I saw rhubarb fresh, and not at all expensive, 1 book, while before that was 3 pounds or more. so, without delay I jump on the occasion, and immediately, has the kitchen to prepare with that a beautiful cake. the choice was great to make a cake, so I opt for a tart very simple, but very delicious, puff pastry, covered with a very good stewed rhubarb, and covered with a delicious layer of Frangipane cream. ingredients: 1 roll of & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 1.95  (  2  ratings)  0 

yesterday in Lidl, I saw rhubarb fresh, and not at all expensive, 1 book, while before that was 3 pounds or more. 

so, without delay I jump on the occasion, and immediately, has the kitchen to prepare with that a beautiful cake. 

the choice was great to make a cake, so I opt for a tart very simple, but very delicious, puff pastry, covered with a very good stewed rhubarb, and covered with a delicious layer of Frangipane cream. 

ingredients: 

  * 1 roll of puff pastry 
  * 500gr of rhubarb 
  * 100 gr of sugar (for rhubarb) 
  * 75 gr of brown sugar (also for rhubarb) 
  * 1 baking soda (for rhubarb too) 
  * 100 gr of butter 
  * 100g of sugar 
  * 125 g of almond powder 
  * 2 eggs 
  * 2 tablespoons flour 



Preheat your oven to 180 ° (therm 6). 

knead butter with almond powder, 100 g sugar, flour and whole eggs until creamy.   
Garnish a mold with puff pastry. Prick the bottom with a fork and place in a cool place.   
Peel and cut the rhubarb in sections and plunge them 1 minute in boiling water and drain them. 

macerate the rhubarb for 1 hour in sugar (100gr) and a little baking soda, and drain   
Put the rhubarb in a saucepan and sprinkle with 75 gr of brown sugar. Place the pan over low heat and cook for 5 minutes, stirring. Remove from heat and let cool.   
Pour the mixture on the bottom of the dough, cover with almond cream and cook for about 35 minutes.   
Take the tart out of the oven and let it cool before enjoying it 

merci 
