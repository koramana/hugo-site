---
title: how to make homemade bread
date: '2018-01-22'
categories:
- Mina el forn
- bread, traditional bread, cake
tags:
- ramadan 2015
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-semoule-026.CR2_thumb1.jpg
---
##  how to make homemade bread 

Hello everybody, 

the delicious bread with semolina, a traditional bread, very tasty, despite the simple ingredients that constitute it. 

a very light bread especially to accompany a beautiful winter soup, or just to saucer. My children at least loved their bread with strawberry jam and butter in it, and for dinner, they had asked for their burgers with this bread. 

I see that it surprised me a lot, but I was very happy.   


**how to make homemade bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-semoule-026.CR2_thumb1.jpg)

Recipe type:  bakery  portions:  6  Prep time:  45 mins  cooking:  20 mins  total:  1 hour 5 mins 

**Ingredients** a 240 ml glass as a measure 

  * 1 glass of flour 
  * 1 glass and a half fine semolina 
  * 3 tablespoons milk powder 
  * 4 tablespoons of oil 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * coffee of baking powder. 
  * 1 tablespoon instant baker's yeast. 
  * between 1 glass and a half and 2 glasses of water depending on the absorption of dry products 

semolina for decoration. 

**Realization steps** if you have a bread machine or a kneader, just mix all the ingredients, and knead, otherwise: 

  1. pick up all the ingredients in a terrine gradually with water, add in small amounts. 
  2. knead the dough well for about 15 minutes. 
  3. let it rest until the dough doubles in volume. 
  4. degas it and divide it into a ball according to your choice, I made 6 of almost 70 gr each. 
  5. roll the balls in semolina. 
  6. flatten each 1.5 cm high and cut with a sharp knife. 
  7. cover and let rise for almost 45 minutes or depending on the heat of the room. 
  8. cook at 180 ° C for 20 to 25 minutes or until golden brown. 


