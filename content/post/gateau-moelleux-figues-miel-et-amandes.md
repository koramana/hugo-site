---
title: fluffy cake with figs, honey and almonds
date: '2016-08-09'
categories:
- cakes and cakes
tags:
- Cake
- fondant
- Genoise cake
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/moelleux-aux-figues-et-amandes-5_thumb11.jpg
---
##  Fluffy cake with figs, honey and almonds 

Hello everybody, 

Always with the delicious figs, but this time and again .... a Lunetoiles recipe. Imagine that this recipe is on my email for almost a year, and that I had completely forgotten, fortunately my friend Lunetoiles made me call back. 

A delicious sweet cake covering beautiful pieces of figs, garnished with a crispy layer of almonds. 

**fluffy cake with figs, honey and almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/moelleux-aux-figues-et-amandes-5_thumb11.jpg)

portions:  8  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 180 gr of soft butter 
  * 200 gr of almond powder 
  * 80 gr of cane sugar Integral Bio 
  * 80 gr of warm honey 
  * 30 gr of flour 
  * 5 eggs 
  * 1 sachet of baking powder 
  * 5 tbsp. chopped almonds 
  * 8 fresh black figs cut in quarters 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. Beat the eggs with the sugar until the mixture whitens. Add the melted and warm honey and mix well. 
  3. Then add the flour, the yeast, the almond powder and the soft butter. 
  4. Mix until a smooth and homogeneous mixture is obtained. 
  5. Arrange the quarters of figs in the bottom of a square dish well buttered or covered with a sheet of baking paper. 
  6. Pour the mixture previously obtained on the figs. Sprinkle top with chopped almonds. 
  7. Place in the oven and cook for 40 minutes. 
  8. Let cool before unmolding. 


