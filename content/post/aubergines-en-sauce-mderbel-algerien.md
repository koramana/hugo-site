---
title: aubergines in sauce, Algerian mderbel
date: '2013-07-17'
categories:
- bakery
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mayonaise-et-mderbel-059.CR2_thumb1.jpg
---
##  aubergines in sauce, Algerian mderbel 

Hello everybody, 

here is a very delicious dish of Algerian cuisine, a mderbel, or aubergines in sauce, or as we always call home at home **Ragout Badendjel** . 

and as we love eggplant at home, I was happy to prepare it, because my husband did not claim that the sauce is white and not red as he wants for all these dishes.   


**aubergines in sauce, Algerian mderbel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mayonaise-et-mderbel-059.CR2_thumb1.jpg)

portions:  4  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients**

  * Preparation time: 30 minutes Cooking time: 35 minutes 
  * The ingredients for 4 people) 
  * 500 gr of eggplants 
  * 250g of lamb meat 
  * 3 cloves of garlic 
  * 1 tablespoon of oil 
  * 1 handful of chickpeas soak the day before 
  * ½ cup of caraway coffee. 
  * salt and black pepper 
  * 1 egg 
  * 2 tablespoons of breadcrumbs. 



**Realization steps**

  1. cut the aubergines into slices, salt and let them drain from their water. 
  2. fry in a pan. and let frying oil drain on paper. 
  3. In a pressure cooker, put the cut meat in pieces, add the grated garlic, the caraway, the salt (not much because the eggplants are salted) and the black pepper, 
  4. cook for a few minutes, then add 2 glasses of water, add the chickpeas. 
  5. Cook until the meat becomes tender. 
  6. let reduce the sauce a little according to your taste, if you like a lot of sauce or just a little. 
  7. remove a ladle from the sauce, and let it cool. 
  8. Add the fried aubergine slices. 
  9. add to the sauce you took an egg and breadcrumbs. 
  10. pour this mixture over your sauce, let simmer for a few minutes and extinguish the heat. 
  11. Serve with good homemade semolina bread. 


