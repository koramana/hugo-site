---
title: two-tone croquettes with jam and hazelnuts
date: '2011-07-30'
categories:
- salads, salty verrines

---
it was a long time since I had eaten these crunchy, but at the time I did not ask the question: can I have the recipe? but yesterday I had a strong desire to make them, no recipe, it was necessary to improvise, and fortunately for me it was a real delight, cakes sweet, tasty, "light" as said my husband, and especially very beautiful , hihihihihi. as I do not like to waste I went for a small amount, I could be inspired by my usual crunch, which make their success where I present them. then we go to hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

it was a long time since I had eaten these crunchy, but at the time I did not ask the question: can I have the recipe? but yesterday I had a strong desire to make them, no recipe, it was necessary to improvise, and fortunately for me it was a real delight, cakes sweet, tasty, "light" as said my husband, and especially very beautiful , hihihihihi. 

as I do not like to waste I went for a small amount, I could be inspired by my usual crunch, which make their success where I present them. 

then we go to the recipe: 

my ingredients were: 

  * 3 eggs 
  * 175 grams of sugar 
  * 200 ml of oil 
  * zest of 1 orange 
  * 2 tablespoons of cocoa 
  * flour 
  * 1 tablespoon of baking powder 
  * 1 cup of vanilla coffee 
  * jam according to taste 
  * hazelnut, or nuts or almonds, or peanuts roasted and roughly crushed. 



for gilding: 

  * 1 egg yolk 
  * 1 cup of milk 
  * 1 cup of vanilla coffee 
  * a little sugar to sprinkle 



with a beater, whisk whole eggs with a pinch of salt, add sugar while whisking, then oil, orange peel and vanilla. 

add the yeast and a spoonful of flour, then divide the dough in half (according to your taste, you can make them equal or one larger than the other) 

add to one of the parts, 2 spoons of cocoa, then add the flour until you have a nice malleable paste. 

for my part, I go gently with the flour, even if the dough remains sticky, because the more we add flour more the cake may be hard, so gently with the flour, it must be workable, but not to the point of to become a concrete. 

do the same with the white dough, add the flour until you have a nice malleable consistency. 

on an aluminum foil, or a baking paper or even a clean bag, oil, take a little of the white paste, flatten it into a rectangle of almost 15 cm wide, the length depends on your tray. 

garnish this dough with jam and hazelnuts (or dried fruits of your tastes), now take the cocoa paste and spread it over the white dough. 

for my part I take in small piece I placed them gradually on the jam and I joined the pieces to cover the jam well. 

using your bag or baking paper, roll the white dough over the colored dough, and continue rolling to get a baguette, place in your oil pan, while using the baking paper to transport the baguette to the mold because as I told you, the dough is not too strong, but very soft. 

garnish your crunchy baguettes with the gilding of egg yolk, milk and vanilla, draw with a fork, and sprinkle with a little crystallized sugar, 

cook in an oven preheated to 180 degrees C, and watch for cooking from below, then from above. 

a la sortie du four, coupez tout de suite en large losange, et bonne dégustation 
