---
title: chorba dishes and entrees menu and ramadan recipes 2016
date: '2016-05-29'
categories:
- Algerian cuisine
- houriyat el matbakh- fatafeat tv
- ramadan recipe
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/recettes-ramadan-2016.jpg
---
**chorba dishes and entrees menu and ramadan recipes 2016**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/recettes-ramadan-2016.jpg)

Prep time:  60 mins  cooking:  60 mins  total:  2 hours 

**Ingredients** as soup: 

  * Chorba frik (jari frik) 
  * Oran hrira 
  * Moroccan Harira 
  * algerian chorba 
  * chorba beida 

as cold input: 
  * tuna bricks 
  * shrimp bourek 
  * bourek with minced meat 



**Realization steps** As a main dish: 

  1. tajine zitoune 
  2. eggplant maqlouba 
  3. tajine el khoukh 


