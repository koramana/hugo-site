---
title: tajine of potatoes
date: '2015-06-15'
categories:
- Algerian cuisine
tags:
- Full Dish
- Ramadan 2015
- Ramadan
- Ramadhan
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-el-batata-2.jpg
---
[ ![tajine el batata 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-el-batata-2.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-pomme-de-terre.html/tajine-el-batata-2>)

##  tajine of potatoes 

Hello everybody, 

Here is a hearty dish, rich in food and vegetables, chicken tajine with chicken in white sauce. it's a dish of my friend **Flower Dz.** In this dish Fleur Dz uses the same ingredients found in the famous dish "tajine Bounarine" but while lightening it (without eggs or cheese), with a white sauce. 

You can however, use any other fried vegetables next to the potato, such as zucchini. In any case, I hope that this very simple dish will please you, and do not forget, if you have one of the recipes of this blog, send the picture on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

![tajine el batata 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-el-batata-1.jpg)

**tajine of potatoes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-el-batata-3.jpg)

portions:  4-5  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients**

  * Pieces of chicken (two legs for me) 
  * Five beautiful potatoes 
  * A glass of 200 ml of chickpeas soaked 
  * One glass 200 ml green peas (crushed) 
  * Onion, finely chopped 
  * Two cloves of crushed garlic 
  * Fresh chopped parsley and coriander 
  * Table oil 
  * Salt and spices (pepper, rasse el hanout and turmeric). 
  * A handful of pitted green olives (optional I am not put). 
  * Two hard boiled eggs and a lemon (for decoration). 



**Realization steps**

  1. Clean the chicken pieces and put them in a pot. 
  2. Add the oil, the finely chopped onion, and the crushed garlic, let it return for a few minutes on a low heat 
  3. Add a small amount of parsley, and coriander, mix well and add the spices and salt to taste. 
  4. Add the chickpeas and peas and cover with about three cups of water and cook. 
  5. In the meantime, peel and clean the potatoes, then cut them into cubes, salt and fry the cubes. 
  6. When your chicken is well cooked and the sauce is reduced by about two cups, at this stage you can add the fried potatoes. 
  7. Let it simmer for no more than two minutes, just so the potatoes absorb the sauce and are well scented. 
  8. Serve your dish well decorated with sliced ​​lemons and boiled eggs, 



[ ![tajine el batata](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tajine-el-batata.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-pomme-de-terre.html/tajine-el-batata>)
