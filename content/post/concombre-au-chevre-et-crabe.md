---
title: cucumber with goat cheese and crab
date: '2017-08-19'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tandoori-006.CR2_2.jpg
---
##  cucumber with goat cheese and crab 

Hello everybody, 

Here are amusements very easy to make, very fresh, and especially super delicious, especially with the crunchy cucumbers ... 

**cucumber with goat cheese and crab**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tandoori-006.CR2_2.jpg)

portions:  4 

**Ingredients**

  * ingredients: for 4 people 
  * 1 large cucumber (I use only half because one is two) 
  * ½ piece of goat cheese 
  * 3 tablespoons fresh cream 
  * salt, pepper and lemon juice (according to your taste) 
  * ½ box of canned crab 
  * some branches of chives 
  * paprika 



**Realization steps**

  1. clean and peel cucumbers, I prefer to draw strips, but you can peel cucumbers completely. 
  2. cut slices of almost 2 cm long 
  3. make a pit inside with a spoon. 
  4. in a salad bowl, crush the goat cheese, and add the fresh cream. 
  5. introduce chopped crab, salt, pepper and lemon juice. 
  6. mix everything, and fill the cucumber wells. 
  7. garnish with chopped chives and paprika 
  8. Keep chilled until serving 



small cucumber, crab and goat cheese 

and for other recipes of tapas, appetizers, and small mouths, click on the photos:   
  
<table>  
<tr>  
<td>

[ ![amuse bouche](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/fleurs-a-la-creme-d-artichaut_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-fleurs-a-la-creme-d-artichauts-tapas-et-amuse-bouche-113751782.html>) 
</td>  
<td>

[ ![homemade breadsticks](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gressins-au-basilic_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-gressins-tres-facile-113765720.html>) 
</td>  
<td>

[ ![crab and parmesan cheese 021](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bouchees-au-crabe-et-parmesan-021_thumb5.jpg) ](<https://www.amourdecuisine.fr/article-bouchees-de-crabe-au-parmesan-101566392.html>) 
</td> </tr>  
<tr>  
<td>

[ ![amuse mouth artichokes goats 088](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/artichauts-chevres-088_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-chevre-dore-sur-fond-d-artichaut-100098265.html>) 
</td>  
<td>

[ ![appetizer with aubergines](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-028_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-roules-aux-aubergines-113337954.html>) 
</td>  
<td>

[ ![tomato nests with eggs 028](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/nids-de-tomates-aux-oeufs-028_thumb.jpg) ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs-101484090.html>) 
</td> </tr>  
<tr>  
<td>

[ ![stuffed olives 040 a](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/olives-farcis-040-a_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-brochettes-d-olives-farcies-102948239.html>) 
</td>  
<td>

[ ![cornet with boulghour 005a](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cornet-au-boulghour-005a_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-cornets-au-taboule-110333711.html>) 
</td>  
<td>

[ ![smoked salmon roulade](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/roulade-au-saumon-fume_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-roulades-de-saumon-fume-au-mascarpone-et-fenouil-101257027.html>) 
</td> </tr>  
<tr>  
<td>

[ ![tuna rice mouths](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/amuses-bouches-au-riz-thon_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-boulettes-de-riz-au-thon-97137268.html>) 
</td>  
<td>

[ ![TARTARE OF SMOKED SALMON](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/TARTARE-DE-SAUMON-FUMEE_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tartare-de-saumon-fumee-en-cornets-de-crepes-100294277.html>) 
</td>  
<td>

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/hasselback-potatoes-hasselbackpotatis_thumb.jpg) ](<https://www.amourdecuisine.fr/article-pommes-de-terre-a-la-suedoise-hasselbackpotatis-108541271.html>) 
</td> </tr>  
<tr>  
<td>

[ ![scallop walnut 009](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/noix-de-saint-jacques-009_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-noix-de-saint-jacques-bouchee-a-la-creme-de-persil-97454414.html>) 
</td>  
<td>

[ ![Pepper rolls 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/Roules-aux-poivrons-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-roules-aux-poivrons-97975273.html>) 
</td>  
<td>

[ ![dumplings with surimi amuse](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/boulettes-au-surimi_thumb3.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-boulettes-au-surimi-113306581.html>) 
</td> </tr> </table>
