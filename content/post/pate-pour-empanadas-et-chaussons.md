---
title: Pate for empanadas and slippers
date: '2016-05-20'
categories:
- Bourek, brick, samoussa, slippers
tags:
- inputs
- Algeria
- la France
- Easy cooking
- Fast Food
- Ramadan 2016
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-pour-empanadas-et-chaussons-1.jpg
---
[ ![paste for empanadas and slippers 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-pour-empanadas-et-chaussons-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-pour-empanadas-et-chaussons-1.jpg>)

##  Pate for empanadas and slippers 

Hello everybody, 

At home, a soup must always be accompanied by a small entrance, bricks, slippers, buns, empanadas. My husband prefers these small appetizers more than a homemade bread, hihihih. Of course it's more work, but it's still a delight. 

To have a successful entry, always choose the right recipe. and I'm the kind that does not change the basic recipes easily, I prefer to stick to the recipe that I know not to miss just like that, besides on the door of my fridge, I have my recipes printed: [ the mellow pizza dough ](<https://www.amourdecuisine.fr/article-pate-a-pizza-maison-moelleuse-et-croustillante.html>) , [ the shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>) , [ salt pie ](<https://www.amourdecuisine.fr/article-pate-a-tarte-salee.html>) , [ the broken pasta ](<https://www.amourdecuisine.fr/article-pate-brisee.html>) , and [ the pancake dough ](<https://www.amourdecuisine.fr/article-pate-a-crepe-facile-et-rapide.html>) . Like that, as soon as I want to make a recipe based on these pasta, they are there before my eyes. 

This dough for empanadas and slippers does not leave me for years, and I can not change to another recipe. Not only it is easy to achieve, but also the result is unavoidable, try it and tell me your opinion.   


**Pate for empanadas and slippers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-pour-empanadas-et-chaussons-3.jpg)

portions:  8  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 500 gr of flour 
  * 1 egg yolk 
  * ⅛ of coffee to salt 
  * 160 ml of oil 
  * 170 ml of water 



**Realization steps**

  1. place the flour in a large salad bowl, 
  2. make a fountain and add the egg yolk and the oil, sand well with your hands, 
  3. add salt and water until you have a dough. (depending on your flour you can add more or less than 170 ml of water) 
  4. Wrap with a cling film and let the dough rest in a cool place for 1 hour. 



[ ![dough for empanadas and slippers 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-pour-empanadas-et-chaussons-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-pour-empanadas-et-chaussons-4.jpg>)
