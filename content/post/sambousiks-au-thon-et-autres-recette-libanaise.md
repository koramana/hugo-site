---
title: Sambousiks with tuna and others (Lebanese recipe)
date: '2011-04-21'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/04/217150001.jpg
---
& Nbsp; as always, we go to Mouni, I find a recipe that makes me winks, impossible not to give in to temptation .............. this time I fell under the charm of his sambousiks. the dough is great, so I had no minced meat, I did with what I had, a tomato sauce with onion, to which I added tuna, y'avais that were stuffed only with tuna, and was sublime. recipe in Arabic we go to the recipe: the dough: 300gr of leavening flour 1 / 2cc of salt 2cs of melted butter or oil 20cl of water (la ... 

##  Overview of tests 

####  please vote 

**User Rating:** 1.78  (  8  ratings)  0 

the dough is great, so I had no minced meat, I did with what I had, a tomato sauce with onion, to which I added tuna, y'avais that were stuffed only with tuna, and was sublime. 

![sambousik](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/217150001.jpg)

we go to the recipe: 

dough: 

  * 300gr of leavening flour 
  * 1 / 2cc of salt 
  * 2s of melted butter or oil 
  * 20cl of water (next time I will put less, because the dough became sticky when I add the rest of the water) 



![dough](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/217146061.jpg)

mix all the ingredients, knead the dough a little until it becomes smooth, cover with a film of food paper and put in the refrigerator for 30 minutes to 1 hour. 

the joke as I said, I did what I had in hand, and in express; 

![prank call](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/217146351.jpg)

a little onion cut into small ones that are made to come back in the oil then tomato is added, and seasoned with salt, black pepper, let it simmer a little, and after cooking and cooling there is adds crumbled tuna. 

spread the dough finely (about 3mm) and cut rounds with the piece, 

![SHAPING](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/217150271.jpg)

put a little stuffing and close in crescent pinching the edges to weld them. 

![faconnage3](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/217150371.jpg)

![faconnage1](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/217150531.jpg) ![faconnage4](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/217150811.jpg)

fry in a hot oil bath, and serve. 

![samb](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/21715100.jpg)

Enjoy your meal 

![sambousik](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/217150001.jpg)
