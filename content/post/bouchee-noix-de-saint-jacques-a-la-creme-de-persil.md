---
title: morsel Walnuts with parsley cream
date: '2014-12-16'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- la France

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/noix-de-saint-jacques-009_thumb.jpg
---
##  morsel Walnuts with parsley cream 

Hello everybody, 

Here is a recipe for small appetizers bringing the finesse of the pan-fried scallops in a crispy bite on a bed of parsley cream. I really liked these bites, it's my first recipe for Saint Jacques nuts, and it's a delight, 

I tasted this small entry with my two friends to accompany a tasty [ soup (cream) of spinach ](<https://www.amourdecuisine.fr/article-creme-d-epinard.html> "Cream of spinach") and my friends had loved it so much that they had told me that they felt like they were invited to a high class restaurant, I am not at this great level of the great chefs restaurateurs, but this testimony put to angels, lol.  so without further ado, I give you this delicious recipe:   


**morsel Walnuts with parsley cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/noix-de-saint-jacques-009_thumb.jpg)

Recipe type:  appetizer, aperitif  portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 6 scallops (frozen for me) 
  * 1 shallot, 
  * ½ cloves of garlic, 
  * 2 tbsp. chopped parsley 
  * 30 g of butter, 
  * 2 portions of cow cheese that laughs 
  * 1 to 2 butter, 
  * Salt, 
  * Mill pepper, 
  * 6 flights to the wind, 



**Realization steps**

  1. Finely chop the shallots, garlic and parsley in the bowl of a mini-blinder 
  2. mix with butter and cheese and season to taste with salt and pepper. 
  3. In a frying pan, heat a knob of butter and fry the scallops for 1 minute on each side until they are cooked. 
  4. remove the nuts and set aside on a hot plate, 
  5. Garnish the bites with the parsley cream. and place the nut on top 
  6. Heat the bites in medium oven 175 ° C, about 10 minutes. 
  7. hot. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
