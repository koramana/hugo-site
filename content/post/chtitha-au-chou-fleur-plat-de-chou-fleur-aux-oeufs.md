---
title: chtitha with cauliflower / dish of cauliflower with eggs
date: '2015-05-12'
categories:
- cuisine algerienne
- recette de ramadan
tags:
- inputs
- Vegetarian cuisine
- Healthy cuisine
- Easy cooking
- Economy Cuisine
- Ramadan
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/chtitha-chou-fleur.jpg
---
[ ![chtitha cauliflower](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/chtitha-chou-fleur.jpg) ](<https://www.amourdecuisine.fr/article-chtitha-au-chou-fleur-plat-de-chou-fleur-aux-oeufs.html/chtitha-chou-fleur>)

##  Chtitha with cauliflower / eggplant with eggs 

Hello everybody, 

You like cabbage flowers, here is a very nice recipe for you, a recipe super easy to make, and especially good, between reduced sauce and egg cake, this dish is very tasty. 

A recipe from one of my readers **Rosa mokrane** who really enjoyed sharing our recipe with us, so let's go for this delicious dish. 

**chtitha with cauliflower**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/chtitha-chou-fleur-225x300.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * a cauliflower 
  * 1 head of garlic 
  * salt and black pepper 
  * paprika 
  * cinnamon 
  * dry red pepper 
  * table oil 
  * 3 eggs 
  * parsley 



**Realization steps**

  1. boil the cauliflower in a little salt water 

prepare the garlic derssa: 
  1. Stack garlic, salt, black pepper, red pepper, a little cinnamon, a red pepper in a mortar 
  2. put this garlic paste in a pot with half a glass of oil 
  3. return well, add two glasses of water 
  4. to boil add the cauliflower cut into a bouquet and cook over medium heat 
  5. when the sauce is reduced enough, add three beaten eggs and decorate with chopped parsley 


