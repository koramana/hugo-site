---
title: Eggplant za3louk, Moroccan eggplant caviar
date: '2012-12-01'
categories:
- Café amour de cuisine
- gateaux d'anniversaires, de fetes, celebrations
- recettes sucrees

---
Hello everybody, 

I really like eggplant, and this time I made a delicious eggplant purée, better known as Zaalouk ...... it's a delicious Moroccan recipe that I prepared in a slightly different way, is an express method, 20 minutes and it's ready: 

  
Ingredients   
: for 4 people 

  * 1 eggplant (large) 
  * 1 medium zucchini 
  * 1 half red pepper 
  * 1 half yellow pepper (you can replace both with a green pepper) 
  * 2 medium tomatoes 
  * 2 cloves garlic 
  * salt, black pepper, cumin and paprika 
  * olive oil 



method of preparation: 

  1. clean and peel the zucchini and eggplant 
  2. seed the peppers. 
  3. cut everything into pieces, and put in a pressure cooker. 
  4. add half a glass of water, garlic, and spices. 
  5. close the casserole and cook 15 minutes 
  6. open the pressure cooker, add the olive oil, and with a crushed potato, mash the whole 
  7. cook until the water is completely evaporated, 
  8. adjust the salt, and enjoy with a good [ matlou3 ](<https://www.amourdecuisine.fr/article-25345316.html>) . 


