---
title: Horn of Gazelle with nekkash, with pincer قرن الغزال
date: '2012-03-09'
categories:
- cuisine algerienne
- houriyat el matbakh- fatafeat tv
- recette de ramadan
- rice

---
Gazelle horn mankouche and me3assel, قرن الغزال منقوش و معسل 

an old recipe, which I prepared at the Aid el Adha El Aid Festival last year and that I never forgot to post 

so having to find the photos of this delicious Algerian cake, a beautiful oriental pastry, and because a person asked me for the recipe, here I mail you: 

**For the dough:**

  * 3 measures of flour 
  * 1 measure of cooled melted butter 
  * Vanilla extract 
  * 1 pinch of salt 
  * Orange tree Flower water. 



**For the stuffing:**

  * 3 measures of ground almonds 
  * 1 measure of powdered sugar 
  * 1 good c to c cinnamon powder 
  * Orange tree Flower water 



for decoration: 

a pincer, nekkache 

honey 

shiny food 

Prepare the dough with the given ingredients, pick up with orange blossom water and let stand a few minutes. 

In the meantime, prepare the stuffing by mixing all the ingredients. 

prepare balls of around 30 gr 

form a hollow in the ball and fill it with almond stuffing. 

enclose the ball and form a pudding, its length will be equal to your 6 fingers of both hands together. 

form a corn with it, and pinch it to your taste with the nekkache pliers, place your cakes on a baking sheet. 

to make leather in a preheated oven, and at the exit of the cakes of the oven, put them in hot honey, place in boxes and decorate with a little bright food 

this is my old birthday party last year. 

thank you for your comments, and thank you for subscribing to my newsletter. 

au prochain billet. 
