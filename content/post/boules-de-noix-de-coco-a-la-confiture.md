---
title: coconut balls with jam
date: '2017-11-10'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
- sweet recipes
tags:
- To taste
- Algerian cakes
- Cookies
- biscuits
- Dry Cake
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/11/boules-de-noix-de-coco-%C3%A0-la-confiture-1.jpg
---
![coconut balls with jam 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/boules-de-noix-de-coco-%C3%A0-la-confiture-1.jpg)

##  **coconut balls with jam**

Hello everybody, 

I take my old recipe of coconut balls with jam to publish it again with new photo. This recipe of biscuits is just a treat. My husband likes it very much because he finds it just very sweet, neither too much nor less. 

Me too I love these delicious and very presentable coconut balls with jam because I think they are super easy to prepare and fast. 

today, I prepared them quickly before going to my friend's house, I had nothing at home to make a good dessert, and I had only 2 eggs in the fridge, so quickly I prepared 30 pieces that I I packed in a pretty box, and frankly, I was very happy when my friend's guests tasted the cake and asked for the recipe. it did not please my husband because I left him only a few pieces to taste it, hihihih. 

**coconut balls with jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/boules-de-noix-de-coco-%C3%A0-la-confiture.jpg)

**Ingredients**

  * 2 eggs 
  * 175 ml of oil 
  * 160 gr of sugar 
  * 1 sachet of baking powder 
  * zest of a lemon 
  * +/- 350 gr of flour 

decoration: 
  * apricot jam 
  * Orange tree Flower water 
  * coconut 



**Realization steps**

  1. In a large salad bowl, beat the eggs and the sugar, 
  2. add the oil while whisking, as well as the lemon zest, 
  3. sift the baking powder and the flour and introduce them to the mixture to have a dough which picks up well and which is very soft and smooth, 
  4. line a baking sheet with baking paper and preheat oven to 180 ° C. 
  5. Form very small balls of almost 16 gr each 
  6. Place them spaced on the baking tray as they spread a little and swell slightly when cooked. 
  7. Bake well and watch the cooking because cakes do not have much color to preserve their fondant and mellow. 
  8. dilute the jam with a little orange blossom water and heat in the microwave to liquefy it. 
  9. Dip the hot-lukewarm balls in the jam for a few minutes so that the balls are well impregnated then stick two pieces and roll them in grated coconut so that the balls are well covered. 
  10. Let them dry completely and put them in boxes. 
  11. These coconut balls can easily be kept for several days in a closed hermetic iron box. 



![coconut balls with jam 3](https://www.amourdecuisine.fr/wp-content/uploads/2017/11/boules-de-noix-de-coco-%C3%A0-la-confiture-3.jpg)
