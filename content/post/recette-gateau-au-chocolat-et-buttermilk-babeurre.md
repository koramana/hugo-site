---
title: chocolate cake and buttermilk / buttermilk recipe
date: '2010-12-06'
categories:
- panna cotta, flan, and yoghurt

---
& Nbsp; buttermilk: buttermilk, or beaten milk. this easy and delicious recipe I found on the blog of Melissami couscous and puddings, since the day I had seen, I was looking for the opportunity to do it. this cake does not contain chocolate, but I swear the taste makes dream an easy recipe, but I had to estimate the quantities because it was going to give a huge cake, so if you want it to be at the base of a birthday cake the original recipe will be good enough. so here is the recipe: 240 gr of flour 220 gr of sugar 1 c & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.55  (  1  ratings)  0 

buttermilk: buttermilk, or beaten milk. 

this cake does not contain chocolate, but I swear the taste makes you dream 

an easy recipe, but I had to estimate the quantities because it would give a huge cake, so if you want it to be the basis of a birthday cake the original recipe will be enough. 

so here is the recipe: 

  * 240 gr of flour 
  * 220 gr of sugar 
  * 1 teaspoon of baking soda 
  * 1 teaspoon of baking powder 
  * 2 tablespoons cocoa powder 
  * 90 gr of melted butter 
  * 300 ml of buttermilk 



Sift the flour with the dry ingredients into a bowl. Mix thoroughly until the mixture has a uniform color 

Incorporate the melted butter, then the beaten milk. Mix well, pour into a cake pan of 20 cm x 30 cm 

Bake in a preheated oven at 180 º C / 4 gas for 45-60 min or until a skewer inserted in the center of the cake comes out clean 

When the cake is cooked, remove it from the oven and let it cool in the mold for 15 minutes. 

decorate to your taste 

good tasting 

[ Add this site to your favorites ](<javascript:if\(window.sidebar\)%7Bwindow.sidebar.addPanel\('amour%20de%20cuisine','https://www.amourdecuisine.fr',''\);%7Delse%20if\(window.external\)%7Bwindow.external.AddFavorite\('https://www.amourdecuisine.fr','amour%20de%20cuisine'\);%7D>)

you like this blog, subscribe to the newsletter to be up to date with all my new recipes. 

un commentaire est toujours le bienvenu 
