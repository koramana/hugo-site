---
title: Contest »express cooking» the result
date: '2012-07-07'
categories:
- appetizer, tapas, appetizer
- salades, verrines salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/panique-en-cuisine.bmp_thumb1.jpg
---
![panic in kitchen.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/panique-en-cuisine.bmp_thumb1.jpg)

Hello everybody, 

Voila, the vote for the contest "Cuisine Express" had come to an end on July 5, 2012 at 00h00. 

the votes were serie, and many of you had to vote for the recipe you liked, or the recipe of your friends (poor person who has no friends, it is without a vote!) 

in any case, as I told you before, I'm going to elect a jury member, and that's what I did, I chose some of my faithful readers, and blogger friends, and that's how we arrive at the final result: 

  * the jury member had to decide between the 5 votes that had the most votes, they were 6, and me the 7th. 
  * each of them had to make the classification of the recipes according to her choice, and what she likes most, therefore: 



\- 1st ranked recipe will have 5 points 

\- the 2nd ranked recipe will have 4 points. 

\- the recipe classified 3 th will have 3 points 

\- the recipe classified 4 th will have 2 points 

\- the recipe ranked 5th will have 1 points 

we sum the points for each recipe, the one with the most points, will be the winner. 

the following table shows you the result of the vote of jury members. 

I am so happy to announce the winner: 

Lady D with her recipe: [ Peanut butter mousse ](<https://www.amourdecuisine.fr/article-mousse-au-beurre-de-cacahuete-106872400.html>)

![peanut butter mousse](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/mousse-au-beurre-de-cacahuete_thumb1.jpg)

you're getting rid of Lady D and what she said: 

" **we are three French students in America, and each of us is responsible for a special task in the kitchen, mine being desserts, and as we are students and we also work part-time, we usually come back tired to our apartment, and suddenly, the kitchen express is always present at us ....**

**I like to present my recipe "peanut butter mousse and Oreos" as a participant in the small competition, and we would like to win the kenwood, because we need one as quickly as possible ... I make the recipe at random because since the time that I prepare it ... and it's a real treat, and I'm sure that you'll love this dessert ... "**

very happy for you my dear, I wait for your coordinates, to buy the robot immediately on the internet, and send it directly to your address. 

now I mail you the votes of the jury members: 

_I propose to you my vote:_

**_1) soft chocolate and praliné_ **

_**2)[ Asparagus with chive cream ](<https://www.amourdecuisine.fr/article-asperges-a-la-creme-au-ciboulette-106895194.html>) ** _

**_4) fries or almost in tomato sauce_ **

**_5) peanut butter mousse_ **

2 - the vote of Chahira, a faithful reader to the blog: 

  1. Peanut butter mousse N1, 
  2. Fluffy chocolate and praline n2, 
  3. Fricadelles or almost in tomato sauce n3, 
  4. Asparagus with cream chives N4, 
  5. Lasagne with Penne rigate N5. 



3- the vote of Elisa2newyork (Elisabeth) 

**_ 1st:  _ ** Lady D Peanut Butter Mousse **27**

**_2nd:_ ** Lasagne with Mirinda penne rigate **24**

**_3rd:_ ** Fricadelles or almost in tomato sauce at Samar **20**

**_4th:_ ** Fluffy chocolate and pralinoise at Aux delices du Palais **26**

**_5th:_ ** Asparagus with cream chives Marie H **1**

**4-** the vote of dalila, a faithful reader: 

  1. Peanut butter mousse. 
  2. Lasagne with penne rigate. 
  3. Asparagus with chive cream. 
  4. Fluffy chocolate and praline n2, 
  5. Fricadelles or almost in tomato sauce n3, 



5- The vote of Saida, a reader and who helps me in the translation of my recipes in Arabic: 

fluffy chocolate and praliné at Aux delices du Palais **26 (1st choice 5 points)**

Lady D Peanut Butter Mousse **27 (4 points)**

fries or almost in tomato sauce at Samar **20 (2 points)**

[ Asparagus with chive cream ](<https://www.amourdecuisine.fr/article-asperges-a-la-creme-au-ciboulette-106895194.html>) from Marie H **1 (1points)**

6- the vote of Celeste, my faithful reader of Peru: 

  * mellow chocolate and praline 
  * then comes: Peanut butter mousse 
  * Lasagne with penne rigate paste 
  * Asparagus with chive cream 
  * patties 



and in the final my choice: 

  1. Peanut butter mousse. 
  2. Asparagus with chive cream. 
  3. Fricadelles or almost in tomato sauce. 
  4. Moelleux with chocolate and pralinoise. 
  5. Lasagne with penne rigate. 


