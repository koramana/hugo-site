---
title: Mascarpone brioche
date: '2018-03-15'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/brioche-au-mascarpone-1.jpg
---
[ ![mascarpone brioche 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/brioche-au-mascarpone-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/brioche-au-mascarpone-1.jpg>)

##  Mascarpone brioche 

Hello everybody, 

Since when I did not realize a [ homemade brioche ](<https://www.amourdecuisine.fr/article-brioche-fait-maison.html> "Home bun") ? It's been a long time. It must be said that following the "best pastry chef" the week they had prepared their brioches tropéziennes, I was tempted to knead to see these little bits of buttered dough pushed. But I did not know what [ Brioche ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=brioche&sa=Rechercher>) I was going to realize? 

I come back to the brioche, for my part I had a box of mascarpone in my fridge, I was going to realize a tiramisu as a dessert for guests, but in the end they could not come, and this box was waiting wisely, but not for long, because it is with this box that I realized this sublime **mascarpone brioche,** a brioche very soft with a very fast crumb, a brioche that will really please you. 

I changed the recipe a little, because we started kneading, and even after a while, the dough was a little hard, so I had to add another egg yolk and some butter, I do not do not know is it because of the mascarpone. But in any case in the end I got a good result. 

If you do not have a mascarpone, at the local grocer's, you can see my recipe from [ homemade mascarpone ](<https://www.amourdecuisine.fr/article-mascarpone-fait-maison-comment-faire.html> "homemade mascarpone, how to do?")

[ ![mascarpone brioche 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/brioche-au-mascarpone-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/brioche-au-mascarpone-2.jpg>)   


**Mascarpone brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/brioche-au-mascarpone-3.jpg)

portions:  8  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * 500 g of flour 
  * 250 g of mascarpone 
  * 4 tablespoons warm milk 
  * 1 tablespoon instant yeast 
  * 1 egg + 3 egg yolks 
  * 100 g caster sugar 
  * 1 tablespoon of milk powder 
  * 1 teaspoon of salt 
  * 20 gr of butter 
  * 1 egg yolk mixed with a little milk for the painting 



**Realization steps**

  1. Place the flour in the bowl of the kneader. 
  2. Add salt, sugar, instant yeast and milk powder. 
  3. start the trouble at the first gear 
  4. whisk together egg and egg yolks 
  5. add this mixture to the flour while continuing to knead but speed 4 this time. 
  6. add warm milk and cheese, continue kneading at 6 or 7 speed for at least 15 minutes, for the mascarpone to relax, and the dough warms up a little, to grow faster. 
  7. the dough will be very supple and elastic, add the butter in pieces, and continue to knead until the butter melts completely, and is completely absorbed by the dough. 
  8. Cover the dough with a cloth and let stand for 1 hour 30 minutes at room temperature. 
  9. line with the baking paper a mold about 23 cm long and 15 cm wide. Book. 
  10. Degas the dough gently. and cut it into pieces each weighing between 80 and 90. 
  11. make balls with these pieces and place 8 balls in the prepared pan. 
  12. with the rest of the dough you can make rolls. 
  13. Let the dough grow longer than it takes.   
to be honest, I do not count the time, because it will depend on the ambient temperature ... Do not squeeze your brioche, for my part I began to knead my brioche at 11am, and it came out of my oven at 17h, because I like to give time to the dough to rise, if you are in a hurry, place each time your dough in a hot oven but off to accelerate the lifting. 
  14. Preheat the oven to 180 degrees C. 
  15. Brush the brioche with the egg yolk and milk mixture 
  16. and cook for 15 to 20 minutes, or depending on your oven. 
  17. At the end of cooking, remove the bun from the oven, and let cool a little before unmolding. 



[ ![mascarpone brioche 4](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/brioche-au-mascarpone-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/brioche-au-mascarpone-4.jpg>)
