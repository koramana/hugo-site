---
title: tahboult - mchewcha-galette with eggs
date: '2012-04-21'
categories:
- cuisine algerienne
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/mchawcha_thumb1.jpg
---
##  tahboult ntmelaline, mchewcha, egg pancake 

Hello everybody, 

This is tahboult - mchewcha-galette aux oeufs, a little delicacy of Kabyle cuisine, [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) : tahboult n'tmelaline, or egg pancake. This cake also known as mchewcha or mchoucha in other regions of Algeria, It was prepared today to celebrate the small arrival of my niece, and who comes to visit us today. a recipe prepared by the care of my mother rabbi yahfedha. 

This mchewcha recipe is ideal for a quick and express taste, very rich in eggs, so this recipe is ideal for children. It is a recipe usually prepared for women who have just given birth and it helps especially for breastfeeding because it is a cake rich in protein. 

I do not tell you, also very easy to do. You can also see the video preparation of this recipe. On the video I prepare the tahboult - mchewcha-galette with the whisk eggs for a super-well-ventilated mchewcha. But originally the recipe is prepared quickly at the foruchette, and it's always a success. 

{{< youtube PW >}} 

**tahboult - mchewcha-galette with eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/mchawcha_thumb1.jpg)

**Ingredients**

  * 8 eggs 
  * 8 tablespoons flour / 160 gr flour 
  * 3 tablespoons fine sugar 
  * ¼ teaspoon of salt 
  * ½ teaspoon vanilla powder 
  * 1 sachet of baking powder 
  * 1 glass of oil for frying 
  * liquid honey 



**Realization steps**

  1. Beat the eggs with the whip, add the sugar, vanilla powder, salt, flour and baking powder. 
  2. Stir well until the mixture becomes homogeneous. 
  3. In a heavy-bottomed pan, heat the oil and pour the dough, 
  4. Put some oil on the surface, lower the heat and let it cook slowly. 
  5. invert the cake on the other side to cook the other side 
  6. After cooking, put it on a paper towel to eliminate excess fat. 
  7. Heat the liquid honey and water the tahboult still hot. 



bonne dégustation. 
