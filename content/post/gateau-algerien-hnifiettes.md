---
title: Algerian cake hnifiettes
date: '2012-06-30'
categories:
- cuisine algerienne

---
the video here is the recipe in Arabic here a delight that I looked for some time, and it was only before yesterday that with the help of my friend Lila, and my friend Ratiba that I came across the recipe, she was hiding in the blog of a girl I do not know, it's here so, I make you a copy / paste: 1 k of flour 250 g of melted butter 3 bags of yeast 2 whole eggs 1 teaspoon Vanilla Pick all with milk. Let stand for half an hour. Make balls of 3 to 4 cm & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.68  (  2  ratings)  0 

so, I make you a copy / paste: 

  * 1 k of flour 
  * 250 g melted butter 
  * 3 bags of yeast 
  * 2 whole eggs 
  * 1 teaspoon of vanilla 
  * Pick it up with milk. 



Let stand for half an hour. 

Make balls 3 to 4 cm in diameter. Roll out the dough, then pass to the dough machine at number 4, then to numbers 7 and 9. Sprinkle the dough with corn starch. 

Roll like a cigar. Cut pudding about 4 cm (for my part I prefer 2 cm, because for cooking the better it is small) 

Pinch with your thumb in the direction of the diagonal of the square. 

Then give a roll from the center outwards on the two triangles on either side of the pinch. 

Dip them in a warm oil, and cook on a very low heat, so that the cakes cook inside. 

Put the cakes on paper towels, then immerse them in honey. Sprinkle with sesame seeds. 

These cakes can be enjoyed very well with a mint tea. 

la recette est super bien detaillee dans la video 
