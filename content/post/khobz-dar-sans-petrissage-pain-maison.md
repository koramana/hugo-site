---
title: khobz dar without food / homemade bread
date: '2017-01-10'
categories:
- cuisine algerienne
- Mina el forn
- pain, pain traditionnel, galette
- recette de ramadan
tags:
- ramadan 2016
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/khobz-dar-sans-petrissage-1.jpg
---
[ ![khobz-dar-sans-kneading-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/khobz-dar-sans-petrissage-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/khobz-dar-pain-maison-sans-petrissage-003.jpg>)

##  khobz dar without food / homemade bread 

Hello everybody, 

here is a delicious **khobz eddar or homemade bread without kneading** well ventilated, which is prepared in no time, and very very light. A recipe from my sister-in-law, the nanny cooking blog, which I had the pleasure of tasting at my sisters and my mother's. This bread is just beautiful and will not leave your table when you see how easy it is. 

This recipe of khobz dar without kneading is the "save day" especially in the month of Ramadan, if you are too busy or tired and you want to taste your [ chorba Frik ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) with a homemade bread worthy of his name! Who said that the best recipes must always be difficult? 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/khobz-dar-sans-petrissage-019_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/khobz-dar-sans-petrissage-019.jpg>)

khobz dar without petrissage 2012 

{{< youtube nsOEzjC94jY >}} 

**homemade bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/khobz-dar-sans-petrissage-3.jpg)

**Ingredients**

  * 7 cup of semolina. 
  * 1 tablespoon of sugar. 
  * 1 teaspoon of salt 
  * 1 tablespoon of instant yeast. 
  * 1 tablespoon of grain and nigels. 
  * 1 tablespoon of sesame seeds. 
  * 1 egg + 1 egg white. 
  * 4 tablespoons of oil. 
  * 250 ml warm milk. 
  * 3 glasses of flour. 
  * 1 sachet of baking powder 
  * egg yolk + 1 c milk soup + nigella seeds and sesame seeds for decoration. 



**Realization steps**

  1. In a large bowl, pour 7 tablespoons of semolina, salt, sugar, yeast and seeds. 
  2. Mix everything and add the oil and eggs. 
  3. Mix them with a whisk and then add the lukewarm milk 
  4. incorporate everything well to have a liquid paste. 
  5. leave the preparation 30 min 
  6. degas the mixture, then incorporate the flour in small quantities until you get a fairly sticky dough let stand 10 minutes. 
  7. coat your hands with oil to handle the dough more easily. just to form a beautiful ball 
  8. oil a tray and flatten the dough, which you can then decorate with the roulette wheel or simply with a knife. 
  9. Cover with a clean, slightly damp cloth and allow the dough to double out of drafts (my trick to go faster is to heat the oven, just a little, turn it off, and put the bread inside, it will double in size in max 5 minutes) 
  10. Preheat your oven to 190 ° C. 
  11. mix the egg yolk with a little milk and garnish the whole surface without degassing the bread. 
  12. decorate with seeds of Nigel and sesame and bake. 
  13. Let cool and enjoy! 



![khobz-dar-without-kneading](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/khobz-dar-sans-petrissage.jpg)
