---
title: Eggplant and chicken maklouba
date: '2017-09-26'
categories:
- Algerian cuisine
- diverse cuisine
tags:
- Healthy cuisine
- Easy cooking
- Full Dish
- dishes
- Algeria
- Ramadan 2015
- Rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/maklouba-aux-aubergines-et-poulet-1.jpg
---
[ ![aubergine and chicken maklouba](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/maklouba-aux-aubergines-et-poulet-1.jpg) ](<https://www.amourdecuisine.fr/article-maklouba-aux-aubergines-et-poulet.html/maklouba-aux-aubergines-et-poulet-1>)

##  Maklouba aubergines and chicken, 

Hello everybody, 

El maklouba, makluba, or maqlouba is a delicious dish of Middle Eastern cuisine that I had the chance to taste on different occasions, different presentations, and different ingredients. I confirm that with my Sudanese, Egyptian or Syrian friends, no dish was like another, and no dish had the taste of another. It was a treat every time, and a discovery at every invitation. 

Last time on my kitchen group on facebook, Nora from the culinary blog Délice de Constantine (which I can not find anymore online) came to share with us, a very nice version of the maqlouba with aubergine and minced meat that is I liked Nora's presentation, and what her maklouba made me want, and I promised her to make her recipe at the first opportunity. 

But here is the D-day, I am aware that I did not have enough ground meat, so I had to do with the means of board. Hamdoullah I had a piece of chicken breast in the freezer, and here I am for a maklouba aubergines and chicken. Try this recipe you will like it. 

**Eggplant and chicken maklouba**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/maqlouba-au-poulet.CR2_.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 beautiful aubergine 
  * 2 to 3 peeled potatoes 

for chicken in tomato sauce: 
  * 300 gr of chicken breast 
  * 2 boxes of tomatoes in pieces, or 1 kg of fresh red tomatoes. 
  * ¼ bunch of coriander 
  * ¼ onion 
  * 1 clove of garlic 
  * some small branches of thyme 
  * 1 bay leaf 
  * salt, black pepper, coriander powder 

for rice: 
  * 2 glasses of rice (200 ml 
  * salt and black pepper 
  * 1 clove of garlic 
  * ¼ of an onion 
  * 1 bay leaf 
  * 2 branches of thyme 
  * 3 seeds of cardamom 
  * some cubebe seeds 
  * olive oil 
  * water. 



**Realization steps** start by preparing the rice: 

  1. rinse the rice placed in a Chinese, under abundant water, until the water becomes clear. 
  2. in a deep-bottomed saucepan, sauté the chopped onion and crushed garlic in a little oil, 
  3. add bay leaf, cardamom, cubebe and thyme, then rice. 
  4. stir a little with a wooden spoon. 
  5. to add salt and black pepper and cover with water, the water must be higher than the 1 cm sweetbread 
  6. cover the pot with aluminum foil, and place the lid 
  7. cook for 10 minutes or less on medium heat, watch, if there is no more water on the surface ke rice is cooked 

Prepare the chicken with tomato sauce: 
  1. cut the chicken into pieces and fry in a little oil 
  2. when the chicken turns a beautiful color remove it and prepare the tomato sauce. 
  3. fry some garlic and chopped onion in the oil 
  4. add the tomato, and the rest of the ingredients and let reduce 
  5. towards the end put the pieces of chicken back, cook for another 5 minutes and remove from heat. 
  6. cut the aubergine into a slice, salt and let it drain from the water. 
  7. fry the floured aubergine in a little hot oil and let drain on paper towels. 
  8. cut the potato into a slice, salt a little, and fry in a hot oil bath. 

montage of the maklouba: 
  1. in a round mold, covered with aluminum foil, make a layer with eggplant slices 
  2. make a second layer with the fried potato rings 
  3. cover with a thin layer of rice 
  4. then cover with a nice layer of chicken in sauce, you do not have to put all the sauce. what remains will be served after with the dish. 
  5. cover once more with a layer of rice. 
  6. Tamp a little so that it is solder well. 
  7. place in a preheated oven at 180 degrees for at least 20 minutes. 
  8. remove from heat and let stand at least 5 minutes before unmolding. 



[ ![aubergine and chicken maklouba](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/maklouba-aux-aubergines-et-poulet.jpg) ](<https://www.amourdecuisine.fr/article-maklouba-aux-aubergines-et-poulet.html/maklouba-aux-aubergines-et-poulet>)
