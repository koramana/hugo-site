---
title: Felfel mssayer Tunisian cuisine
date: '2014-08-13'
categories:
- Tunisian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/falfel-msayer-010_thumb1.jpg
---
##  Felfel mssayer / Tunisian cuisine 

Hello everybody 

a delicious [ Tunisian recipe ](<https://www.amourdecuisine.fr/cuisine-tunisienne>) , that my husband loves a lot, this time he asked for a dinner with friends (sir more precisely), my husband took with him this **tangy salad** , of [ Matlou3 ](<https://www.amourdecuisine.fr/article-41774164.html>) , and [ chicken skewers ](<https://www.amourdecuisine.fr/article-brochettes-de-blanc-de-poulet-97966938.html>) (which I did not take in **pics** because they made their **grill** once laba). 

so, as I had put the [ salad ](<https://www.amourdecuisine.fr/categorie-10678933.html>) in small individual dishes, I have not much garnished, but if you present this salad in a large dish, it will be more beautiful, if you crumble them **eggs** Then you decorate the heart with a beautiful layer of egg white, and put in the middle the egg yolk. you can also add some **caper** (my husband does not like caper, so I did not want to wear it) 

**Felfel mssayer / Tunisian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/falfel-msayer-010_thumb1.jpg)

Recipe type:  salad  portions:  3  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 2 red peppers 
  * 2 hot peppers (optional, if you do not like spiciness) 
  * 2 eggs 
  * ½ onion 
  * 4 cloves of garlic 
  * 150 gr of tuna 
  * 1 teaspoon of ground caraway 
  * 1 Pinch of ground coriander 
  * salt, olive oil 



**Realization steps**

  1. Broil vegetables in the oven. 
  2. cook the eggs in salted water. 
  3. place everything in a sachet and let "sweat" for a few minutes. 
  4. Peel the vegetables and crush them roughly with a grinder or a mortar. or like me with a chisel. 
  5. add half of the tuna to the mixture and incorporate well. 
  6. Salis, and add your spices. 
  7. sprinkle liberally with olive oil, and decorate with the remaining tuna, and the eggs. 
  8. Enjoy this warm or cold salad. with good [ matlou3 ](<https://www.amourdecuisine.fr/article-25345316.html> "Matlouh, matlou3, matlouh - home-made tajine bread") . 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
