---
title: banana smoothie and apple juice
date: '2015-11-19'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-jus-de-pomme-1.jpg
---
[ ![banana smoothie apple juice 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-jus-de-pomme-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-jus-de-pomme-1.jpg>)

##  banana smoothie and apple juice 

Hello everybody, 

Another smoothie recipe! Yes, at home it is always a special smoothie for my daughter, another for my son, another for me and another for my husband ... So with the fruits have great feast at home. Moreover for the realization of smoothies, I invested myself well, it is not my habits, but because we like the smoothies, I bought a centrifuge: Sage, the equivalent of Reviera & Bar in France, and a blinder or smoothie maker with which we do not risk to have pieces especially vegetables a bit hard as carrots. 

Yes, if like me it's going to be smoothies at least 4 times a day, you have to choose good material. 

Usually, and with smoothies I was too lazy to take out my camera, place the tripod, and start looking for the most beautiful picture, and you have to be honest, the smoothies is not as photogenic as that , hihihihi. By sharing the photos on my cooking group, I noticed that people are interested in having different ideas, so why should I deprive you. I do not use my camera to be frank, but I quickly and quickly make photos with my mobile phone, and voila. 

**banana smoothie and apple juice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-et-jus-de-pommes.jpg)

Recipe type:  smoothie  portions:  2  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 red apples galas 
  * 1 pear 
  * 2 medium bananas 
  * 1 tablespoon of peanut butter 
  * 150 gr of fresh cream (replace by Actimel for me) 
  * cinnamon powder 



**Realization steps**

  1. If you have a juicer, you do not need to peel apples and pears, especially if they are organic. Just remove the glitches. 
  2. Get the juice of apples and pears. 
  3. in the blinder bowl, place the banana pieces, peanut butter, crème fraîche, and apple and pear juice.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/ingredients-smoothie.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/ingredients-smoothie.jpg>)
  4. Shield until it becomes velvety. 
  5. During the presentation, sprinkle with a little cinnamon powder and enjoy. 



Note You can use canned apple juice, almost 300 ml.   
If you do not have a juicer, shield the apples and pears without the skin. the smoothie may be more dense, so either reduce the amount of banana, or add more fresh cream. [ ![banana smoothie apple juice](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-jus-de-pomme.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-banane-jus-de-pomme.jpg>)
