---
title: Flan with figs
date: '2014-09-10'
categories:
- dessert, crumbles and bars
- panna cotta, flan, and yoghurt

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-aux-figues-1.jpg
---
[ ![flan with figs](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-aux-figues-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-aux-figues-1.jpg>)

##  Flan with figs 

Hello everybody, 

I will be frank with you my friends ... This summer I eat figs, as I have never done in my life, I take full advantage of the trees of the neighbor, who each day passed us a beautiful bucket well filled fleshy, unctuous and ripe figs. 

I did not have too much trouble making fig recipes, apart from realizing a nice reserve of [ FIG jam ](<https://www.amourdecuisine.fr/article-confiture-de-figues-en-video.html> "Jam of figs in video") , and repeatedly a [ easy fig pie ](<https://www.amourdecuisine.fr/article-tarte-aux-figues.html> "fig tart") as we love at home. 

Now, I'm jealous of Lunetoiles when she gave me this recipe, I imagine a good fig flan, to accompany a good tea on the terrace with my parents and my brothers and their small families, it would have is the pleasure of big and small ... 

Insha'Allah, this is to be done next year, because now I have the recipe for this delicious Flan with figs, I hope too, that I'm going to come across some nice figs here in England, to be able to try and taste this recipe… 

**Flan with figs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-aux-figues.jpg)

portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 50 cl of milk 
  * 1 vanilla pod 
  * 100 g sugar (preferably unrefined) 
  * 6 egg yolks 
  * 40 g of Maïzena 
  * 12 figs 
  * Of honey 



**Realization steps**

  1. Bring milk and vanilla to a boil. 
  2. Let infuse a few minutes. 
  3. In a bowl, whisk the yolks and sugar until you have a frothy mixture. 
  4. Add the cornflour and mix. 
  5. Pour the boiling milk over the yolks, gradually and whisking. 
  6. Pour everything into the pan and let thicken over low heat while stirring briskly. 
  7. Put the cream in a bowl and film on contact. 
  8. Let cool. 
  9. Butter 4 tartlet mold 10 cm in diameter. 
  10. Wash the figs, or peel them if you do not like there is the skin, cut them in half. 
  11. Spread the cream in the small molds. 
  12. Arrange figs cut in half, curved sides down. 
  13. Place the mussels in the fridge for an hour. 
  14. Preheat the oven to 170 ° C. 
  15. Take the mussels out of the fridge, sprinkle the figs with a drizzle of honey. 
  16. Cook for about 30 minutes. 
  17. Let cool. 
  18. Reserve in the fridge before tasting. 
  19. Profitez 🙂 



[ ![flan with figs 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flans-aux-figues.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flans-aux-figues.jpg>)
