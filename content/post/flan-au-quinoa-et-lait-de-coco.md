---
title: Flan with quinoa and coconut milk
date: '2013-07-16'
categories:
- diverse cuisine
- Cuisine by country
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-coco-et-quinoa1.jpg
---
![custard-coconut-and-quinoa.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-coco-et-quinoa1.jpg)

##  Flan with quinoa and coconut milk 

Hello everybody, 

Here is a delicious flan that shares with us my dear Lunetoiles, it's a recipe that was shelved in the archives of my blog, at the time I was doing the weight watchers diet, and that Lunetoiles has to reiterate, and despite that It's been a long time since I did not realize this flan for a while, but the taste of this melting custard that closes in his heart an endless crunch of quinoa, never left me 

**Flan with quinoa and coconut milk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-au-Quinoa-et-lait-de-coco.jpg)

**Ingredients**

  * 100 ml of water 
  * 50 gr of Quinoa 
  * 3 eggs 
  * 4 cases of cane sugar (you do not make the diet, add sugar according to your taste) 
  * 400 ml of coconut milk 
  * a little coconut 



**Realization steps**

  1. place the quinoa grains in the water and bring them to a boil. Reduce the heat and cook (all water must be reduced) 
  2. beat the eggs with the sugar, add the coconut milk. 
  3. after cooling the quinoa, it is distributed in ramekins 
  4. Pour over the egg mixture and coconut milk, place in a bain-marie. 
  5. Sprinkle a little coconut on it, and bake in preheated oven at 190 degrees, for 35 minutes 



![custard-coconut-quinoa.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-coco-quinoa1.jpg)
