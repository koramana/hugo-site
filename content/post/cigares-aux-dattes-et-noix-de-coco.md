---
title: Cigars with dates and coconut
date: '2015-07-27'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-aux-dattes-et-noix-de-coco-4.jpg
---
[ ![cigars with dates and coconut 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-aux-dattes-et-noix-de-coco-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-aux-dattes-et-noix-de-coco-4.jpg>)

##  Cigars with dates and coconut 

Hello everybody, 

a recipe from my reader and friend: **As Sou,** cigars with dates and coconuts she liked to share with us through the lines of my blog: 

Azul everyone, I present you a traditional Algerian cake that my dear Mother prepared for each Eid, until then everything is normal. But know that this famous cake hides a story, hum suspense hihihi ... Without delay, I share with you this little story. Formerly in my early childhood, this cake was part of the "dislike" lol, because I would not tolerate the coconut. But oddly bizarre, years passed and here I am big, again everything is normal, because growth is a natural fact, but the continuation promises to be surprising! As we say, time does things well. Here I am great, without my knowing how, I fall in love with the coconut to the point where and I love everything that is done with (Raffaello, ...) suddenly these cigars became my favorite cakes which I can not to do without them to make them with each Eid. The funny thing is that this has become the case of the whole family, same thing for the relatives, who wait impatiently in the exchange boxes at each Eid. It seems that it is contagious hihihi 

Note: that those who love them just honey, think has the pinches to have a nice decor. 

![cigars with dates and coconut 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-aux-dattes-et-noix-de-coco-1.jpg)

**Cigars with dates and coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-aux-dattes-et-noix-de-coco.jpg)

portions:  36  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** Dough 

  * 3 measures of flour; 
  * 1 measure minus one soft finger (ghee or clarified butter) 
  * 2 teaspoons icing sugar (optional); 
  * The pinch of salt; 
  * 1 teaspoon of vanilla; 
  * Orange tree Flower water 
  * 1 measure of water. 

Prank call 
  * 500 g of Ghars (date paste); 
  * Oil (about 50 ml or more); 
  * Vanilla or cinnamon. 

Decoration 
  * Industrial honey (brand "Teddy" for my part); 
  * Coconut. 



**Realization steps** Dough 

  1. In a bowl, mix sifted flour, icing sugar, salt and vanilla. 
  2. Incorporate the soft pen and sand the dough. 
  3. Pick up the dough with the mixture of water / water of orange blossom 
  4. Shoot in touch and let cool for about 2 hours. 

The joke 
  1. Perfume according to your taste the guy (date paste) with cinnamon or vanilla. 
  2. Add the oil (as needed) and work it while removing the impurities. 
  3. Form balls. 
  4. Cover them with foil, and leave in the fridge for 2 hours. 

Preparation of Cigars el-Guars with Coconut 
  1. Take the stuffing and dough out of the fridge 1 hour before use. 
  2. Form with stuffing sausages about 2 cm wide. 
  3. Spread the dough with a roll on a floured board. 
  4. Put the pudding at the end, then roll and cut once the stuffing is covered with dough. 
  5. Pinch the dough with your fingers to prevent it from taking off in the oven. 
  6. Cut cigars about 6 cm in length with a knife (I used personal roulette griwech to give a nice shape to my cigars). 
  7. Do the same for all the remaining stuffing, and put the cigars in a floured baking sheet. 
  8. Bake at 160 ° C for about 30 minutes or more (depending on each oven). 

Decoration 
  1. Plunge hot cigars into industrial honey. 
  2. Let drain a few minutes on a rack. 
  3. Then roll your cigars into the coconut and let it dry. 



Note PS:   
Present them in boxes.   
Serve with coffee with milk (coffee spilled) or with tea.   
Good tasting. 
