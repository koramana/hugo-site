---
title: Bradj ... is this spring Algerian cake
date: '2011-10-31'
categories:
- sweet recipes
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/bradj-1.jpg
---
& Nbsp; hello everyone, is it spring, or not yet? it must be said that with this climate, so cold, we do not know, we are in what season? in any case, even if it's cold, we do not forget our traditions, and at home in Algeria, we have the dradj, Mbardja, lbradj, and I do not know if there are other nominations, but this They are beautiful shortbread cookies, and stuffed with well-seasoned date paste of cinnamon powder, and clove powder, a delight, I do not tell you and ca & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.02  (  3  ratings)  0 

Hello everybody, 

is it spring, or not yet? 

it must be said that with this climate, so cold, we do not know, we are in what season? in any case, even if it's cold, we do not forget our traditions, and at home in Algeria, we have the dradj, Mbardja, lbradj, and I do not know if there are other nominations, but this are gorgeous shortbread cookies, and stuffed with well-seasoned date paste of cinnamon powder, and clove powder, a delight, I do not tell you and it makes its success, every time I prepare them and I share that with my neighbors. 

so yesterday it was Bradj's day for me, especially since many of my readers were asking me for details about the recipe, so yesterday the urge was great, especially since I had some date paste already ready, and that the day was still beautiful. 

![bradj-.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/bradj-1.jpg)

the recipe is already on the blog, I recommend the recipe of these [ Bradjs ](<https://www.amourdecuisine.fr/article-25345463.html>) you will not be disappointed at all, but do you have some advice: 

  * when I say semolina medium, I say semolina, and not couscous, because I was surprised that some of my readers, during recipes of [ Mhadjebs ](<https://www.amourdecuisine.fr/article-mhadjebs-algeriens-msemens-farcis-crepes-farcis-mahdjouba-62744653.html>) , [ Makrout ](<https://www.amourdecuisine.fr/article-26001222.html>) , and [ Qalb elouz ](<https://www.amourdecuisine.fr/article-35768313.html>) , these people have used couscous, because reading, semolina average, or fat, the reflection they had was "Couscous" so I said, it's good semolina for bread. 
  * when I say Measure, in any recipe, I'm talking about a kitchen utensil that is going to be a measuring device, like for yogurt cake, the yoghurt pot will become the measure for the other ingredients, so when I say measure, you can use: a bowl, a box of empty margarine, a cup ... .. so this is according to the quantity you want to get from your cake 
  * the most important thing too, the cooking, it must be on low heat, and watch for it not that it does not burn, and especially let cook for not having uncooked cakes in the middle is semolina , it's on a tagine on the ground, or on a crepe maker, take your time when cooking 



thank you for your visits and comments that I validate very slowly, to answer any questions 

Do not forget also if you have tried one of my recipes, to send me the pictures of your essays on my email: 

[ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

I also have a request to one of my readers, who once sent me the recipe that her mother has prepared during mawlid nabawi charif, I can not get my hands on this recipe and the photos that she sent me, so if you read my dear, send me the email, I can not find it 

merci a tout ceux qui continuent de s’abonner a ma newsletter, pour être a jour avec toute nouvelle publication. 
