---
title: red velvet birthday cake
date: '2013-03-10'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-d-anniversaire-red-velvet-057.CR2_3.jpg
---
![cake-birthday-red-velvet-057.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-d-anniversaire-red-velvet-057.CR2_3.jpg)

##  red velvet birthday cake 

Hello everybody, 

I share with you a piece of my birthday cake red velvet ... Yes today the 10th of March I celebrated a year more than last year, hihihi ... .. 

I realized a cake very simple and very easy, I hope that you will like it   


**red velvet birthday cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-d-anniversaire-red-velvet-055.CR2_.jpg)

**Ingredients** for the sponge cake: (3 genoise for me)   
for each round biscuit 25 cm in diameter (I used a 20 cm mold to have high cakes) 

  * 2 eggs 
  * 60 gr of sugar 
  * 60 gr (flour + 1 teaspoon cocoa) 
  * 1 pinch of baking powder 
  * pink dye   
for the 3 genoise, the ingredients are multiplied by 3. 

syrup: 
  * 200 gr of sugar 
  * 300 ml of water 
  * vanilla 
  * decoration: 
  * [ cream with vanilla butter. ](<https://www.amourdecuisine.fr/article-la-creme-au-beurre-facile-112774625.html>)



**Realization steps**

  1. first prepare the syrup, mix all the ingredients, and place on medium heat, at the first boiling turn off the heat, and let cool. 
  2. beat the eggs thoroughly with the sugar until the mixture is white and very airy. 
  3. add the flour, cocoa and baking powder, 
  4. cook in a hot oven for 15 to 20 minutes (depending on the capacity of your oven) 
  5. prepare the cream with vanilla butter. 
  6. place the first layer of the biscuit, and soak it a little (not too much) with the prepared syrup. 
  7. cover the top of the biscuit with the butter cream, using a pastry bag, and then standardize well with a spatula. 
  8. place the second biscuit on top, soak it generously with the syrup, and cover once more butter cream. 
  9. then decorate as you wish. 


