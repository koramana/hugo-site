---
title: ghribia has chickpea flour / dry and melting cake
date: '2017-06-18'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/ghribiya-a-la-farine-de-pois-chiche-1.jpg
---
#  [ ![ghribia has chickpea flour / dry and melting cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/ghribiya-a-la-farine-de-pois-chiche-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/ghribiya-a-la-farine-de-pois-chiche-1.jpg>)

##  ghribia has chickpea flour / dry and melting cake 

Hello everybody, 

when I left in **Algeria** , I took with me **smen** , the **clarified butter** because I like the smen **Algerian** , I think it is a thousand times honeyed that the smen from here that melts immediately and gives water, and suddenly it does not succeed too much cakes such as the [ ghribiya ](<https://www.amourdecuisine.fr/categorie-12344750.html>) , for which we prefer that, the smen remains creamy to give a beautiful mousse, 

and the first thing I did with it was ghribiya to chickpea flour, as my mother prepares it ...   


**ghribia has chickpea flour**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/ghribia-a-la-farine-de-pois-chiche.CR2_.jpg)

portions:  40  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 1 kg of flour (almost 1 kg) 
  * 350 gr of smen 
  * 180 gr of icing sugar 
  * 150 gr chickpeas grilled in powder 
  * 150 ml of table oil 



**Realization steps**

  1. Sift white flour and chickpea flour 
  2. beat the smen and the sugar to have a creamy mousse 
  3. add the oil and whip again 
  4. add the chickpea flour while whisking. 
  5. pick up the dough with flour. 
  6. form a pudding, and cut small lozenges, which you place on a floured tray in the oven 
  7. Bake in preheated oven at 180 ° C for 10-15 minutes depending on oven capacity 
  8. keep in a hermitic box. and enjoy with a good tea 



{{< youtube fe2FMVVnVZk >}} 
