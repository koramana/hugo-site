---
title: Chicken soup with chicken
date: '2015-06-23'
categories:
- Algerian cuisine
- Dishes and salty recipes
- chicken meat recipes (halal)
- soups and velvets
tags:
- Vegetable soup
- Vegetables
- Healthy cuisine
- Easy cooking
- Algeria
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/soupe-de-poulet-038.CR2_.jpg
---
[ ![Chicken soup with chicken](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/soupe-de-poulet-038.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/soupe-de-poulet-038.CR2_.jpg>)

##  Chicken soup with chicken 

Hello everybody, 

I ask you this question before giving you the recipe for this soup velvety chicken: It happens to you to be taken by the household and suddenly you realize that it is almost noon, your husband will return in a few minutes , and oops ... I did not eat anything ???? 

Well this was my case this morning, I had appointment at the doctor, back it was cleaning, and I did not pay attention when I noticed that it was already 11:30 ... My god, my god what am I going to do, besides I did not even have the rest of the dinner, or eggs to make an omelette quickly made, something that my husband likes ... 

I open the fridge, I have a piece of chicken breast .... 2 potatoes, 1 half onion .... Yes I did not shop this week, I had guests, so missed my weekend shopping. 

Immediately, an improvised recipe, a chicken soup, but I do not tell you a real delight .... 

###### 

###  Chicken soup 

Ingredients 

  * 1 piece of chicken breast 
  * 2 potatoes 
  * 2 celery 
  * ½ onion 
  * 2 tablespoons olive oil 
  * 3 soft cheese portions 
  * black pepper and salt. 
  * water. 

preparation  حساء الدجاج  المكونات 

1 قطعة من صدر الدجاج *    
2 حبتين بطاطس حجم متوسط ​​*    
2 الكرفس *    
½ البصل *    
2 ملاعق طعام من زيت الزيتون *    
3 قطع جبن البقرة الضاحكة *    
الفلفل الأسود والملح *    
الماء * 

طريقة التحضير 

قشر البطاطس ونظيفيه *    
افعلي الشيء نفسه مع البصل *    
يغسل جيدا ويقطع الكرفس إلى قطع *    
في وعاء, يضاف الدجاج والبصل مع قليل من الزيت *    
عندما يأخذ البصل لونا جميلا, تضاف المكونات الأخرى) (وليس الجبن    
وتغطي مع الماء والسماح ليطهى على حرارة متوسطة *    
عندما ينضج الدجاج, أزيلي جرء منه لتزيين الحساء *    
على مزيج, أعيفي الجبن وأخلطي مرة أخرى ضعي بقية الدجاج مع المكونات الأخرى في خلاط كهربائ حتى تتحصلي    
يمكنك قلي قطع الدجاج في قليل من الزيت * 

[ ![chicken soup 043.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/soupe-de-poulet-043.CR2-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/soupe-de-poulet-043.CR2-001.jpg>)
