---
title: My Algerian cakes Aid el fitr 2014
date: '2014-07-28'
categories:
- Algerian cakes with icing
- Algerian cakes with honey
- fried Algerian cakes
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/harissa-el-louz-gateau-aid-el-fitr-2014.jpg
---
[ ![harissa el louz, cake help el fitr 2014](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/harissa-el-louz-gateau-aid-el-fitr-2014.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/arayeche-gateaux-algeriens-aid-el-fitr-2014.CR2_.jpg>)

##  My Algerian cakes Aid el fitr 2014 

Hello everybody, 

Once again, happy feast of aid el fitr 2014, I hope that you spent a beautiful morning, and I hope that the children have feasted with the cakes that you could realize. 

I share with you my dear, and as usual, photos Algerian cakes of aid el fitr 2014, I hope you will like the photos, it was taken quickly, before the arrival of men of the mosque. 

This year, I did not have the chance to prepare a lot of cakes, because of the baby, and because of my trip to Algeria. I arrived just 5 days ago, just after unpacking the bags, and already at the neighborhood grocer's shop to buy the ingredients needed to make my Algerian cakes for l'aid el fitr 2014. 

Then the picture from above, and that of Algerian cakes without cooking [ harissat el louz, or harissa el louz ](<https://www.amourdecuisine.fr/article-harissa-aux-amandes-harissat-el-louz.html> "harissa with almonds or harissat el louz") (recipe to come) 

[ ![arayeche algerian cakes aid el fitr 2014.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/arayeche-gateaux-algeriens-aid-el-fitr-2014.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/arayeche-gateaux-algeriens-aid-el-fitr-2014.CR2_.jpg>)

[ Ice chill ](<https://www.amourdecuisine.fr/article-arayeches-gateaux-algeriens.html> "arayeches, Algerian cakes") "So girly" at the request of my daughter. For my daughter there is no Aid without "araycha" as she calls them. 

[ ![cake ezhar cakes of the aid 2014 031.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-ezhar-gateaux-de-laid-2014-031.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateau-ezhar-gateaux-de-laid-2014-031.CR2_.jpg>)

[ ![2014 cakes of the aid 013.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateaux-de-laid-2014-013.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateaux-de-laid-2014-013.CR2_.jpg>)

Shortbread with Nestlé and Nutella, recipe to come. 

[ ![Ghribia has the peanut cake of the aid 2014.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/ghribia-a-la-cacahuete-gateaux-de-laid-2014.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/ghribia-a-la-cacahuete-gateaux-de-laid-2014.CR2_.jpg>)

Ghribia with peanuts "recipes to come" [ ![griwech cakes from the aid 2014 .CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/griwech-gateaux-de-laid-2014-.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/griwech-gateaux-de-laid-2014-.CR2_.jpg>) [ Griwech ](<https://www.amourdecuisine.fr/article-griwech-gateau-algerien-en-video.html> "Griwech Algerian cake in video") , recipe that can never miss my help table at the request of my father. 

[ ![sands with jam cakes from the aid 2014.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/sables-a-la-confiture-gateaux-de-laid-2014.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/sables-a-la-confiture-gateaux-de-laid-2014.CR2_.jpg>)

[ Shortbread with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-d-abricots.html> "shortbread biscuits / shortbread biscuits") , very fondant I promise you. 

[ ![tcharek ice cream cake of the aid 2014 .CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/tcharek-glace-gateaux-de-laid-2014-.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/tcharek-glace-gateaux-de-laid-2014-.CR2_.jpg>)

[ Icy charek ](<https://www.amourdecuisine.fr/article-corne-de-gazelle-tcharek-glace-gateau-algerien.html> "horn of gazelle, frozen charek, Algerian cake") , super fondant too 

[ ![cakes the stages, gingerbread.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateaux-les-etages-pain-depices.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/gateaux-les-etages-pain-depices.CR2_.jpg>)

![Gingerbread with jam cakes from the 2014 2014 027.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/pain-depice-a-la-confiture-gateaux-de-laid-2014-027.CR2_.jpg)

cakes the floors, or [ chocolate gingerbread ](<https://www.amourdecuisine.fr/article-pain-depice-au-chocolat.html> "chocolate gingerbread") recipe already on the blog, but here is the revisited version, which I will post soon 

[ ![mderbel cake with jam cakes from the aid 2014.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mderbel-gateau-a-la-confiture-gateaux-de-laid-2014.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mderbel-gateau-a-la-confiture-gateaux-de-laid-2014.CR2_.jpg>) [ Mderbel, cakes with jam ](<https://www.amourdecuisine.fr/article-mderbel-facile-et-bon.html> "mderbel - Algerian cake with jam") . here decorated with flaked almonds 

[ ![baklawa cake of the aid 2014 042.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/baklawa-gateaux-de-laid-2014-042.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/baklawa-gateaux-de-laid-2014-042.CR2_.jpg>)

[ Baklawa constantinoise ](<https://www.amourdecuisine.fr/article-baklawa-constantinoise.html> "baklawa constantinoise") , with almond. 

[ ![makrout el koucha at the nakache cake of the aid 2014 021.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/makrout-el-koucha-au-nakache-gateaux-de-laid-2014-021.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/makrout-el-koucha-au-nakache-gateaux-de-laid-2014-021.CR2_.jpg>)

[ Makrout el Koucha ](<https://www.amourdecuisine.fr/article-makrout-el-koucha-en-video-gateau-de-semoule-et-dattes.html> "makrout el koucha in video / semolina cake and dates") Mankouche.   


**My Algerian cakes Aid el fitr 2014**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/harissa-el-louz-gateau-aid-el-fitr-2014.jpg)

portions:  50  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * Griwech 
  * Makrout 
  * arayeche 
  * ghribia 
  * mderbel 
  * baklawa 
  * tchareck 
  * Harissa 



**Realization steps**

  1. dry cake 
  2. honey cake 
  3. cake without cooking 
  4. Algerian cake 


