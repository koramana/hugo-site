---
title: easy homemade honey syrup
date: '2016-06-07'
categories:
- Algerian cakes with honey
- basic pastry recipes
tags:
- Algeria
- Easy recipe
- Ramadan 2016
- Morocco
- Algerian cakes
- desserts
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/miel-fait-maison-1.jpg
---
![easy homemade honey syrup](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/miel-fait-maison-1.jpg)

##  easy homemade honey syrup 

Hello everybody, 

On the first day of Ramadan, a little habit I learned from my mother is to prepare a nice amount of homemade honey syrup. This honey syrup is used a lot to coat cakes that are prepared during this holy month, such as [ griwechs ](<https://www.amourdecuisine.fr/article-griwech-gateau-algerien-en-video.html>) , the zlabia, the [ makrout el koucha ](<https://www.amourdecuisine.fr/article-makrout-au-four-makroud-gateau-algerien-%d9%85%d9%82%d8%b1%d9%88%d8%b7-%d8%a7%d9%84%d9%83%d9%88%d8%b4%d8%a9.html>) and full of other recipes .... 

In Algeria it is easy to find the 3sila (or honey syrup) trade, but despite everything my mother has always made homemade honey syrup well scented with lemon, besides when I was little, I waited I can not wait for Mum to take the slices of lemon candied in this syrup of honey so that I suck them like a super sweet and melting candy (today I only took the pictures of these beautiful slices of lemon, by what I did my **homemade honey syrup** in full swing, hihihihihi). 

The recipe in Arabic: 

**easy homemade honey syrup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/miel-fait-maison-2.jpg)

Prep time:  5 mins  cooking:  55 mins  total:  1 hour 

**Ingredients**

  * 2 kg of sugar 
  * 1 liter of water 
  * slices of lemon 
  * 1 tablespoon honey. 
  * 1 glass of water 



**Realization steps**

  1. In a heavy saucepan, place the sugar and pour the water over it. 
  2. add the slices of lemon and cook on low heat for almost 30 minutes. or until the honey is well reduced and has a beautiful color. 
  3. Add the tablespoon of honey and the glass of water 
  4. let it boil and reduce between 15 and 20. 
  5. the honey will be light as long as it is hot but it will splice on cooling. 



![easy homemade honey syrup](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/miel-fait-maison.jpg)
