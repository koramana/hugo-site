---
title: Greek salad
date: '2012-11-24'
categories:
- dessert, crumbles et barres
- recettes sucrees

---
Hello everybody, 

it is not yet summer, but what is nice to bring more colors and flavors to your table, a Greek salad very very tasty, very rich, and really very good. 

so if like me you like salads, the one will make you happy. 

without delay, and for this salad to be ready for the next meal, here are the   
ingredients: 

  * 2 or 3 fresh tomatoes 
  * 1 red onion. 
  * 1 small green pepper (I did not have one). 
  * 150 grams of feta cheese 
  * 1 handful of black olives 
  * Oregano 
  * olive oil 
  * black pepper, salt 
  * vinegar (I use lemon juice) 



Preparation: 

  1. Cut the tomatoes into slices or in half then in half-moons 
  2. cut the onion into thin slices, 
  3. green pepper in strips 
  4. and the cucumber in thin slices. 
  5. Mix in a bowl of feta cheese, cut into cubes. 
  6. Add olives, salt and pepper 
  7. sprinkle with olive oil and vinegar 
  8. Sprinkle with oregano, and voila, ready to serve. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
