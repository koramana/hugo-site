---
title: Algerian cake, almond knots
date: '2013-01-01'
categories:
- bakery
- Bourek, brick, samoussa, slippers
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/noeud2_thumb1.jpg
---
& Nbsp; hello everyone, and here is the recipe for these little delights, this delicacy that I found in a book Houria Amirouchi, "Cakes Houria 1 Decoration" delicious Algerian cakes, very presentable, very beautiful, and too good. It must be said, that the recipe was not explained too much on the book, but when one breaks the head, one arrives at the good result. and here are the ingredients: for the dough: - 3 measures of flour - 1/2 measure of smen, ghee - 1 pinch of salt - 1/2 egg - 1/2 measure of water and 1/2 measure of water from & hellip ; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

[ ![Algerian cake 2013](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/noeud2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-36513347.html>)

Hello everybody, 

and here is the recipe for these **little delights** , of this **greed** that I had found in a book of **Houria Amirouchi** , " **Houria Cakes 1 Decoration** " 

of **delicious** [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , very presentable, very beautiful, and too good. It must be said, that the recipe was not explained too much on the book, but when one breaks the head, one arrives at the good result. 

and here are the ingredients: 

for the dough: 

\- 3 measures of flour 

\- 1/2 measure of smen, ghee 

\- 1 pinch of salt 

\- 1/2 egg 

\- 1/2 measure of water and 1/2 measure of orange blossom water, mixed. 

\- white food coloring 

or if you do not like the pasta with the measures, I recommend this pasta: 

250 gr of flour 

75 gr of margarine 

1 pinch of salt 

1 teaspoon of vanilla 

1 teaspoon of white dye 

1 cup of dye 

Orange blossom water + water 

for the stuffing: 

\- 1 measure of medium ground almond (finely ground for me) 

\- 1 measure icing sugar 

\- 2 teaspoons of vanilla 

\- 2 teaspoons of lemon zest 

\- 2 eggs 

\- Orange tree Flower water 

\- green food coloring. 

[ ![Algerian cake 2013](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/noeud1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-36513347.html>)

Preparation: 

mix the ingredients of the dough to have a soft and malleable paste, form balls and let rest. 

mix the ingredients of the almond stuffing, to have a paste not too sticky. 

now, if you use the machine, pass the dough to Numbers 4, 6, and 8 and then cut it with a number 7 mold, I still do not know how to use the machine, hihihihihi, I use the baker's roll until a fine paste, and I cut with a cookie cutter 7 cm in diameter. 

spread the almond stuffing with the roll until you get a height of 5 cm, and cut pucks 6 cm in diameter, 

place these stuffing rings on the slices of dough, and cover them with another slice of 6 cm diameter. 

decorate with a knife this dough, then put a little stuffing in the middle, wet two wedges of the dough with a little egg white, and pick them up to the middle to form your knot, and decorate with an almond. 

put in the oven at 180 degrees, and at the end immerse them in lightened honey. 

do not forget to subscribe to my newsletter to be aware of the update of my articles. 

[ ![Node3](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/noeud3_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-36513347.html>)

bisous 
