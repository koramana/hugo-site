---
title: Kitchen love recipes tested and approved 56
date: '2015-02-26'
categories:
- the tests of my readers
tags:
- desserts
- Cakes
- Algeria
- Morocco
- dishes
- tajine
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg)

Hello everybody, 

Another article gathering my recipes tested and approved by you, and each time the list is longer than the previous time, which is really sublime. You are very nice, all of you who try my recipes and send me pictures on mail: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

Do not deprive me of this happiness my friends, and the more you are many to try my recipes and to leave your comments, the more I have the volenteé to make even more delights ... I continue with your encouragement. 

and we start: 

[ ![pate a speculoos](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pate-a-speculoos.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/pate-a-speculoos>)

[ pate a speculoos ](<https://www.amourdecuisine.fr/article-la-pate-a-speculoos-maison-hommage-a-micky-63142494.html> "pate of speculoos, homemade recipe") at hafred fati 

[ ![muffins with lemon and poppy seeds, at DZ flower](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/muffins-au-citron-et-grains-de-pavot-chez-fleur-DZ.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/muffins-au-citron-et-grains-de-pavot-chez-fleur-dz>)

[ Poppy seed and lemon muffins ](<https://www.amourdecuisine.fr/article-muffins-au-citron-et-graines-de-pavot.html> "Lemon muffins and poppy seeds") at flower Dz 

[ ![salt brioche base](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/base-a-brioche-sal%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/base-a-brioche-salee>)

[ Salt brioche ](<https://www.amourdecuisine.fr/article-pain-brioche-a-la-ciboulette.html> "Brioche bread with chives") revisited at Nicole Gozzi 

[ ![pastries with cream pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/patits-pains-a-la-creme-patissiere.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/patits-pains-a-la-creme-patissiere>)

[ bread rolls with pastry cream ](<https://www.amourdecuisine.fr/article-cream-de-parisienne-moelleux.html> "fluffy Parisian cream") at Lwiza Nsa 

[ ![brioche salee at Nicole's](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/brioche-salee-chez-Nicole.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/brioche-salee-chez-nicole>)

[ Salt brioche ](<https://www.amourdecuisine.fr/article-pain-brioche-a-la-ciboulette.html> "Brioche bread with chives") at Nicole gozzi 

[ ![tuna buns Radia](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/petits-pains-au-thon-Radia.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/petits-pains-au-thon-radia>)

[ tuna buns ](<https://www.amourdecuisine.fr/article-petits-pains-au-thon.html> "tuna buns") at Radia Mhd 

[ ![mguergchate at Lyna Hydra](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mguergchate-chez-Lyna-Hydra.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/mguergchate-chez-lyna-hydra>)

[ mguergchates ](<https://www.amourdecuisine.fr/article-oreillettes-algeriennes-croustillantes-mguergchates-el-warda-en-video.html> "crispy Algerian atria-mguergchates el warda on Video") at Lyna hydra. 

[ ![brick sheet at Said Hadouche](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/feuille-de-brick-chez-Said-hadouche.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/feuille-de-brick-chez-said-hadouche>)

[ Homemade brick pastry ](<https://www.amourdecuisine.fr/article-dyouls-ou-feuilles-de-brick-fait-maison.html> "dyouls or homemade Brick leaves") at Saida gadouche 

[ ![home rechta at hafred fati](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/rechta-maison-chez-hafred-fati.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/rechta-maison-chez-hafred-fati>)

[ Homemade rechta ](<https://www.amourdecuisine.fr/article-rechta-fait-maison-pour-l-aid-el-kbir.html> "rechta - homemade") at Hafred fati 

[ ![khadudja bundt cake Kbk](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/bundt-cake-de-khadudja-Kbk.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-56.html/bundt-cake-de-khadudja-kbk>)

[ Bundt chocolate cake and peanut butter ](<https://www.amourdecuisine.fr/article-bundt-cake-au-chocolat-et-beurre-de-cacahuetes.html> "Bundt cake with chocolate and peanut butter") at Khadidja Kbk 

His opinion: 

le seul changement c’est que le café que j’ai utilisé est Nescafé instantané au gout noisette…et beurre d’arachide croquant au lieu de crémeux.. aussi pour la ganache et le glaçage de cacahuète j’ai pas utilisé de mesures hihih , c’est pour ça que l’apparence n’est pas comme je la souhaitais .. mais le gout est tous simplement sublime.. 
