---
title: 'Recipes around an ingredient # 1'
date: '2015-01-02'
categories:
- Coffee love of cooking
tags:
- Pastry
- desserts
- Algerian cakes
- Cakes
- dishes
- Vegetables
- inputs
- sauces

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
[ ![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg>)

source: Pinterest 

**Recipes around an ingredient  
** **Principle of the game** 1  – Choisir un ingrédient de saison et accessible aux quatre coins du monde 🙂  2- The sponsor will publish this article on her blog with the links of the two bloggers who initiated this game  3 - Announce the ingredient that will be the star of the current month.  4 - The bloggers who want to participate, will contact directly the animator of the game so that she can already establish the list of the participants in the game.  5 - Before the 1st of the following month, the participants in the game must send the titles of their recipes and the link of their blogs to the godmother  6- The godmother of the game must then establish a final list of participating blogs and their recipes, which she will send by e-mail to all the participants.  7- All the recipes must be put on line the 3 of the month with down the list sent by the godmother of the game.  3\. The godmother of the game will then choose among the participants a blogger who will take over, and announce the new ingredient of the following month. 

**How to participate**

ingredient elected this month not Soulef and Samar is the  orange  . For a start the game was closed, the contact was established by email. The participants have already made their recipes. But if you think you can make your recipe before midnight tomorrow (02/01/2014 before 23:59) send me the link of your blog, and the name of the recipe, so that I send you the list. 

Les recettes des participantes qui ont réalisé leurs recettes a base de l’Orange, seront en ligne le 3 Janvier. 
