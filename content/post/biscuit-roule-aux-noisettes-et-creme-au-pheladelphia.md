---
title: biscuit rolled with hazelnuts and cream at philadelphia
date: '2018-04-17'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Fruits
- desserts
- To taste
- Breakfast
- Ramadan
- Algeria
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-noisettes-et-pheladelphia-2.jpg
---
[ ![rolled hazelnuts and pheladelphia 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-noisettes-et-pheladelphia-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-noisettes-et-pheladelphia-2.jpg>)

##  biscuit rolled with hazelnuts and cream at philadelphia 

Hello everybody, 

If there is a cake that everyone loves at home and without any protest it is a biscuit rolled, whatever the cream with which I will stuff it, regardless of the composition of the biscuit ... It is " the best cake ever "at home ... we all love without exception ... 

It's always based on the recipe of [ Biscuit Roll ](<https://www.amourdecuisine.fr/article-roule-mascarpone-fraise.html>) that I always do ... but this time I had a little hazelnut powder, which I add to the dough ... and the cream is with the cheese phéladelphia ... And as at home we love strawberries, well, I put some pieces in pieces before rolling the biscuit. 

[ ![hazelnut rolled pheladelphia-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-noisettes-pheladelphia-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-noisettes-pheladelphia.jpg>)

You can see the recipe for a very easy rolled biscuit with strawberry coulis: 

**biscuit rolled with hazelnuts and cream at philadelphia**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-aux-nousettes-et-creme-pheladelphia.jpg)

portions:  12  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 5 eggs (5 large if not 6 small for a tray of 30 cm by 40cm) 
  * 140 grams of sugar 
  * 100 grams of flour 
  * 50 gr of roasted hazelnuts 
  * 1 cup of vanilla coffee 
  * 1 cup of baking powder 
  * 1 pinch of salt 
  * 2 drops of vinegar (to avoid the smell of eggs) 

for the cream: 
  * 200 gr of Philadelphia cheese 
  * 100 gr of liquid cream 
  * 4 tablespoons of sugar (you can add more, at home they do not like when it's too sweet) 
  * some strawberries in pieces 

for garnish: 
  * sweet chantilly cream 
  * some strawberries 



**Realization steps**

  1. prepare your ingredients, and start by beating the eggs and the foam sugar, to have a good foamy mixture, if you use the whip of a kneader, it will give a beautiful foam, otherwise if you use an electric whisk like mine , tip the whisk to air the mixture well, make it for 8 to 10 min, until the egg sugar mixture, triple volume. 
  2. add vanilla and vinegar, 
  3. then add the flour mixture hazelnuts and baking powder, very slowly spoon per spoon, without breaking the foamy egg mixture, 
  4. mix with a spatula while aerating the mixture. 
  5. cover the tray with a baking paper, oil it a little, and slowly pour your mixture. 
  6. bake in a hot oven, 180 degrees for 10 minutes, or depending on the capacity of your oven. (the biscuit must be golden brown) 
  7. remove the biscuit from the oven and let cool completely (to have a good roll do not let too much cook) 
  8. Roll the cake right away in clean tea towel, a sheet of foil, or a sheet of baking paper, and let cool. 
  9. prepare the filling by whisking phéladelphia cheese, liquid cream and sugar. 
  10. cut the strawberries into pieces and set aside. 
  11. roll out the cake, cover it with the pheladelphia cream and scatter the strawberries over it, roll the cake again, and set aside. 
  12. let take a little, decorate the cake with a little whipped cream. 
  13. put back in the fridge and during the presentation cut into pieces of 2 cm and 2,5 cm 



[ ![rolled hazelnuts philadelphia](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-noisettes-pheladelphia.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-noisettes-pheladelphia.jpg>)
