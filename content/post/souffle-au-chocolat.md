---
title: chocolate soufflé
date: '2015-01-04'
categories:
- dessert, crumbles and bars
- cakes and cakes
- ramadan recipe
- sweet recipes
tags:
- Pastry
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/souffle-au-chocolat-019_thumb_1.jpg
---
[ ![chocolate souffle 019](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/souffle-au-chocolat-019_thumb_1.jpg) ](<https://www.amourdecuisine.fr/article-souffle-au-chocolat-2.html>)

##  chocolate souffle recipe 

Hello everybody, 

I never blown, because I always knew that it was difficult to succeed, but as we must try to really know if it will be difficult or no, I embarked on the adventure. I must say that I prefer to miss the puffs than to miss macaroons. 

a recipe that I found on youtube, which really seduced me, I do not tell you how they were the final puffs in the video, no relation with mine, hihihihi, but in any case they are too good, 

I will share with you the recipe, waiting for you to pass an even more successful recipe: 

**chocolate soufflé**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/souffle-au-chocolat-009_thumb.jpg)

Recipe type:  dessert  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for 4 small ramekins 

  * 100 g dark chocolate 70% 
  * the finely chopped zest of an orange 
  * 2 big egg whites 
  * a big egg yolk 
  * 125 g whole milk 
  * 30 g of sugar 
  * 6 g flour (1 cup of coffee a little rounded) 

for ramekins: 
  * a little soft butter, 
  * Granulated sugar 



**Realization steps**

  1. Butter the ramekins generously with a brush from bottom to top. 
  2. Sprinkle each with a tablespoon of sugar and turn the ramekin to cover the bottom and sides, (this is how you will have a beautiful puff well inflated that does not stick to the walls) 
  3. tap to remove the superfluous. 
  4. Blanch the yolk with 10 g sugar then add the flour and mix well. 
  5. Bring the milk and zests to a boil and pour over the previous mixture. 
  6. Put back on the fire a few seconds and thicken without stopping whipping. Pour this cream on the chocolate axes 
  7. mix gently to obtain a homogeneous apparatus. 
  8. Beat the whites not too firm with the remaining 20 g sugar. 
  9. gently stir in the crème in the chocolate cream, 
  10. then pour this mixture over the egg white, being careful not to drop the mixture. 
  11. Divide the preparation into the ramekins filling to the brim, 
  12. smooth the top with a paddle or spatula. 
  13. Bake for 15 min or even less depending on your oven, preheated to 180 ° C. (I made them come out as they have risen, if you leave more, the edge of the soufflé will fall, because you have to put in the head is already cooked, the chocolate cream, so the soufflé is only a half-cooked mousse, with a heart very fondant) 
  14. At the end of the oven, sprinkle with icing sugar and serve immediately 



#  chocolate soufflé 

Ingredients to make 4 ramekins 

  * 100 g dark chocolate 70% 
  * zest of an orange 
  * 2 large egg whites 
  * an egg yolk 
  * 125 g of whole milk 
  * 30g sugar 
  * 6 g of flour (teaspoon full) 

for the ramekins: 
  * soft butter 
  * beaver sugar 

preparation 
  1. Brush 4 ramekins with generous butter, then coat with sugar 
  2. Beat egg yolk with sugar, add the flour and mix well. 
  3. boil the milk with orange zest and over the mixture. 
  4. for every thing in the saucepan, cook while stirring until it gets thicker 
  5. For this cream over the chopped chocolate. Gently mix until you have a smooth mixture. 
  6. Beat egg whites with the remaining sugar (20 g) you do not need to get stiff. 
  7. Gently stir in, a third of this mixture to the chocolate cream, 
  8. then for all this mixture over the egg whites, 
  9. spoon the mixture into the ramekins filling them to the top, 
  10. Take a pallet knife and pull it over the top of each ramekin so the mixture is completely flat. 
  11. Bake for 15 minutes or less on a preheated oven at 180 ° C. The puffs should have risen by their original height and jiggle when moved, but be set on top 
  12. when taking it out of the oven, sprinkle it with icing sugar and serve immediately 



[ ![chocolate souffle 001](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/souffle-au-chocolat-001_thumb.jpg) ](<https://www.amourdecuisine.fr/article-souffle-au-chocolat-2.html>)

thank you for all your comments and your visits that make me very warm in the heart. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
