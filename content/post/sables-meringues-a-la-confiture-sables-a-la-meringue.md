---
title: Shortbread meringue with jam / meringue sands
date: '2014-10-02'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours
tags:
- Dry Cake
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/sables-meringues2.jpg
---
![sand-meringues.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/sables-meringues2.jpg)

##  Shortbread meringue with jam / meringue sands 

Hello everybody, 

Super fondant shortbread with meringue and jam that shares with us my friend lunetoiles, it's a super easy recipe to make ... 

Lunetoiles gave you detailed photos, and I made you a nice video editing, I hope you like it ... I tell you good preparation if you will make this cake for help .... 

**Shortbread meringue with jam / meringue sands**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/sables-meringues1-300x233.jpg)

Recipe type:  sand  portions:  30  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients** the sand pate: 

  * 600 g all-purpose flour 
  * 330 g unsalted butter, cold and cubed 
  * 80 g of sugar 
  * 3 to 4 egg yolks 
  * 2 teaspoons of vanilla extract 
  * pinch of salt 
  * vanilla 

Meringue : 
  * 2 egg whites, preferably at room temperature. 
  * 130 gr of icing sugar 
  * a pinch of salt and a few drops of lemon juice 
  * ¼ c of vanilla extract 
  * apricot jam or any other jam you prefer. 



**Realization steps** make shortbread cookies: 

  1. sift together the flour, sugar and salt. 
  2. add the butter and rub the mixture until lumpy. 
  3. add vanilla and yolks one by one until you form a paste. 
  4. cover with plastic wrap and refrigerate for about 30 minutes. 
  5. preheat the oven to 160 ° c. 
  6. spread the dough on a floured surface until about ¼ cm thick. 
  7. cut out the biscuits, preferably using a flower-shaped cookie cutter. 
  8. place on an ungreased cookie sheet and refrigerate. 

make the meringue: 
  1. roll up the egg whites with the pinch of salt and a few drops of lemon juice until you get a sweet spike. 
  2. add the sugar gradually until the meringue is shiny. 
  3. add the vanilla. 
  4. fill a pastry bag fitted with the meringue and set aside. 
  5. remove the fridge plate and using the piping bag, put meringue tips all around each cookie. 
  6. cook until the meringue is shiny and crisp, about 15 minutes. 
  7. if you notice that the meringue starts to brown, lower the temperature to 145 ° C. 
  8. the cookies should be very tender, melt in your mouth and the crispy meringue on the top. 
  9. once cooled, fill each cookie with the jam of your choice. 


