---
title: apple tart
date: '2013-12-03'
categories:
- diverse cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-fine-aux-pommes-41.jpg
---
![pie-fine-with-potatoes-4.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-fine-aux-pommes-41.jpg)

##  apple tart 

Hello everybody, 

More delicious and easier to make than a thin apple pie, I do not know. Already we all fall in front of an apple pie, it's always too good. And when it comes to just spreading its puff pastry decorate it with a little compote and fruit to choose from, well, there is nothing more to say. 

Of course there are people who still take the trouble to make a broken pasta for even more crisp. But me when I have fleme a little puff pastry does the job at home. So this thin apple pie is made by Lunetoiles for her family and for you. 

![pie-fine-with-apples-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-fine-aux-pommes-11.jpg)

**apple tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-fine-aux-pommes-31.jpg)

Recipe type:  pie dessert  portions:  8  Prep time:  10 mins  cooking:  35 mins  total:  45 mins 

**Ingredients**

  * 1 puff pastry 
  * 4 or 5 apples 
  * 200 g of unsweetened applesauce 
  * 40 g of butter 
  * 50 g of sugar 
  * 1 sachet of vanilla sugar 



**Realization steps**

  1. Cover the oven tray with parchment paper and place the puff pastry. 
  2. Stir the puff pastry with a fork. 
  3. Peel the apples, sift them and cut them into slices. 
  4. Preheat your oven to 210 ° C th.7. 
  5. Spread the applesauce over the dough evenly. 
  6. Arrange apples harmoniously by overlapping them. 
  7. Melt the butter in a saucepan over low heat and once the butter is melted and lukewarm, add the sugar. 
  8. With a brush, brush the apples with the butter / sugar mixture. 
  9. Then sprinkle the pie with the vanilla sugar. 
  10. Bake the pie for 35 minutes at the bottom of the oven. 


