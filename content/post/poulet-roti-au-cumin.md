---
title: roasted chicken with cumin
date: '2011-05-14'
categories:
- cakes and cakes
- recettes sucrees

---
& Nbsp; hello everyone, here is a delicious roti, that I really like, when I eat it at Sihem (during the barbecue evening) I usually make roast chicken, but cumin, gives a flavor other, it gives a plus, that I really like, and that everyone too to love at home, so without delay I give you the recipe: a chicken of almost 1.5 kilo 4 to 5 ca soup table oil (extra virgin olive oil for me) 1 ca cumin coffee 1/2 ca paprika coffee & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

I used to make roast chicken, but cumin, gives a different flavor, it gives a plus, that I really like, and that everyone also love at home, so without delay I get you pass the recipe: 

  * a chicken of almost 1.5 kilos 
  * 4 to 5 tablespoons of table oil (extra virgin olive oil for me) 
  * 1 teaspoon of cumin 
  * 1/2 teaspoon of paprika 
  * 1/2 teaspoon of black pepper 
  * 1/2 teaspoon garlic powder 
  * 1/2 teaspoon coriander powder 
  * salt according to taste 



clean the chicken well 

mix all the spices and the salt in the oil, then with a brush, cover all the chicken (inside and outside) 

marinate all night, or prepare in the morning to roast in the evening. 

do not worry but the oil will cover the chicken well, and allows the spice to penetrate the skin 

I use my oven with the roasting spit, but you can put on a rack 

place over a large mold with water and it allows the chicken to cook without being too dry. 

cook the chicken for at least 1h30. 

at the end of the cooking, recover the juice in the large pan (the water would have evaporated well) add a little water to have a good juice 

serve with a mashed potato (it was for me and my husband) 

or with fries (spices for kids) 

or according to your taste. 

thank you for your visits and comments 

bonne journee 
