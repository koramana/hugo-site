---
title: Sea cicada in tomato sauce
date: '2015-02-18'
categories:
- cuisine algerienne
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
tags:
- Healthy cuisine
- Easy cooking
- Algeria
- accompaniment
- inputs
- Morocco
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/cigale-de-mer-en-sauce-tomate.jpg
---
[ ![sea ​​cicada in tomato sauce](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/cigale-de-mer-en-sauce-tomate.jpg) ](<https://www.amourdecuisine.fr/article-cigale-de-mer-en-sauce-tomate.html/cigale-de-mer-1>)

##  Sea cicada in tomato sauce 

Hello everybody, 

Here is a recipe based on crustacean flesh fleshy and rich in taste, which we know as the Sea Cicada. I do not find these shellfish here in England, that's why I take full advantage when I go to Algeria. 

Recently, one of my readers asked me how to cook these crustaceans, I came on the blog to give her the recipe, and here I found that I never post it, old photos that date from 2011, hihihihi, but I confirm, despite the quality of the photos, this dish is of incomparable delight. 

We can always remove the entire shell of sea cicadas to cook only the flesh, but I personally find that the carapace gives a particular taste to the dish. 

**Sea cicada in tomato sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/cigale-de-mer-en-sauce-tomate.jpg)

portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 600 gr of sea cicadas 
  * 450 gr of crushed fresh tomatoes 
  * 3 tablespoons of extra virgin olive oil 
  * salt, black pepper. 
  * ½ teaspoon of cumin powder 
  * 2 cloves garlic 
  * 2 bay leaves 
  * 1 tbsp of harissa (optional) 
  * some branches of thyme 
  * ½ glass of water as needed 
  * a few sprigs of chopped parsley 



**Realization steps**

  1. Start by cleaning the cicadas of sea, for that you equip kitchen scissors 
  2. take off the tail   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/nettoyage-cigale-de-mer.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40570>)
  3. then remove the pasta from above and from below   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/cigale-de-mer-2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40565>)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/cigale-de-mer-2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40565>)
  4. rinse the sea cicadas well and set aside   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/cigale-de-mer-netoyee.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40569>)
  5. place the sea cicadas in a frying pan on medium heat with a little oil well heated. cook in small amounts on both sides   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/cigale-de-mer-frit.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40568>)
  6. remove the sea cicada and place aside. 
  7. place the tomato in the same pan now, add all the remaining ingredients, and cook. 
  8. when the sauce boils well, put the sea cicadas in the sauce, if you think that your sauce is a little dry, you can add a little water. 
  9. let reduce a little, while keeping a juicy sauce. 
  10. present with rice, or pasta 



[ ![sea ​​cicada in tomato sauce 1-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/cigale-de-mer-en-sauce-tomate-1-001.jpg) ](<https://www.amourdecuisine.fr/article-cigale-de-mer-en-sauce-tomate.html/cigale-de-mer-en-sauce-tomate-1-001>)
