---
title: infinitely pistachio tart
date: '2014-04-23'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-1.1.jpg
---
[ ![infinitely pistachio tart](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-1.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-1.1.jpg>)

Hello everybody, 

do you like pistachios? surely, what do you say then of a frangipane pie with pistachio, not almonds !!! it tempts you, so we go for this recipe for lunetoiles, and she called the pie infinitely pistachio. 

A name that sounds great, and a taste that will surely delight our taste buds. This taste will not stop at the sweet and melting pistachio cream, but you will be surprised by these juicy and tart raspberries that will break this sweet taste, and will push you to bite again and again in this little piece of cake ... 

the recipe in details is here: [ Recipe for Raspberry Pistachio Pie ](<https://www.amourdecuisine.fr/article-tarte-pistaches-framboises.html>)

[ ![infinitely pistachio tart](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-4.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-4.1.jpg>)

[ ![infinitely pistachio tart](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-b.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-b.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
