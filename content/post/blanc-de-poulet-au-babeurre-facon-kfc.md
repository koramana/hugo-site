---
title: KFC buttermilk chicken breast
date: '2016-02-21'
categories:
- diverse cuisine
- rice
tags:
- Ramadan
- Easy cooking
- dishes
- Algeria
- Oven
- la France
- Roast chicken

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/blanc-de-poulet-fa%C3%A7on-kfc-1-001.jpg
---
[ ![kfc style chicken breast 1-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/blanc-de-poulet-fa%C3%A7on-kfc-1-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/blanc-de-poulet-fa%C3%A7on-kfc-1-001.jpg>)

##  KFC buttermilk chicken breast 

Hello everybody, 

**"The** **forgotten recipes of our grandmothers "**

[ ![Charlotte, Andalusian, palm tree, prime rib 003](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Charlotte-andalouse-palmier-c%C3%B4te-de-boeuf-003.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Charlotte-andalouse-palmier-c%C3%B4te-de-boeuf-003.jpg>)

I had a great desire to participate, because I have to say that I have a very old cookbook (that I went to find in my little loft (a closet that picks up everything and nothing, hihihihih). short time because I had to deposit my stake today before midnight ... grrrr 

I began to leaf through my notebook delicately, to find a family recipe, not a recipe found in a newspaper, or seen on TV (besides I have plenty of those, to come can be !!!). 

And here I come across a recipe, from my aunt **Josiane** may God have his soul. I did not have the chance to meet this woman too much, because she lived in France, she came to visit my grandmother in summer who lived in Algeria (it was the wife of my grandmother's brother ) and it must be said that when she came, she was the queen of the kitchen, moreover it is she who introduced me indirectly to the French cuisine, because my tents and my mother learned a lot of recipes with her. I can tell you that 20% of the recipes of this blog, were recipes of my aunt **Josiane** (at the time I was writing her name: Josee-anne). 

![my cookbook](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mon-carnet-de-cuisine.jpg)

So, looking at the notebook I came across his recipe for chicken breast breadcrumbs and Lben (buttermilk, or milk ribot), I had everything on hand, great, me who just wanted to make tortillas to children, and who wanted to make pancakes, well at least the ingredients have been used for something else. 

At first I was going to make the recipe macaroons coffee (by the way it will be a recipe to program), but then I did not know what to do for lunch, I preferred to make chicken with fries. A recipe that I do not regret having realized, because my son as soon as he tasted, he told me KFC !!! and it's true the chicken pieces really had the taste of KFC chicken, next time I'll try to do that with the chicken drumsticks and see if it will give the same taste. 

[ ![kfc-style chicken breast 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/blanc-de-poulet-fa%C3%A7on-kfc-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mon-carnet-de-cuisine.jpg>)

Without delay I give you the recipe of my aunt Josiane and if you try it, tell me your opinion! 

ingredients: 

  * chicken breast cut into pieces of 10 by 10 cm 
  * 1 glass of flour 
  * 1 glass of bread crumbs (just crushed to give a nice effect, not a fine powder) 
  * 2 c. salt 
  * 3 c. black pepper 
  * 2 c. paprika 
  * 2 tbsp. coffee of oregano 
  * 3 tablespoons of Espelette chilli powder. 
  * butter (melted) for the baking dish 
  * 2 glasses of buttermilk, or ribot milk 
  * oil bomb to water the chicken every time 



![kfc-style chicken breast](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/blanc-de-poulet-fa%C3%A7on-kfc.jpg)

Method of preparation: 

  1. pre-heat the oven to 220 ° C. 
  2. In a bowl, mix flour, bread crumbs, salt, 1 teaspoon and a half black pepper, paprika, Espelette pepper and oregano. 
  3. In another large plate, pour into ribot milk. 
  4. Pour 1/2 cup melted butter into a baking dish. 
  5. Take the chicken pieces and dip them in the ribot milk. 
  6. put the pieces back in the flour mixture, thus covering both sides. 
  7. place in the dish, once all the chicken is placed in the dish, sprinkle with black pepper. 
  8. Put in preheated oven and bake for 15 minutes. Open the oven and spray the pieces. 
  9. cook for another 10 minutes, and still spray with oil, continue to make the oil, to have a tender chicken. 
  10. remove from the oven and put on paper towels. Serve with fries and a nice salad. 



[ ![kfc style chicken 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-fa%C3%A7on-kfc-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/poulet-fa%C3%A7on-kfc-2.jpg>)

**KFC buttermilk chicken breast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/blanc-de-poulet-fa%C3%A7on-kfc-1.jpg)

portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * chicken breast cut into pieces of 10 by 10 cm 
  * 1 glass of flour 
  * 1 glass of bread crumbs (just crushed to give a nice effect, not a fine powder) 
  * 2 tbsp. salt 
  * 3 c. black pepper 
  * 2 tbsp. paprika 
  * 2 tbsp. coffee of oregano 
  * 3 tablespoons of Espelette chilli powder. 
  * butter (melted) for the baking dish 
  * 2 glasses of buttermilk, or ribot milk 
  * oil bomb to water the chicken every time 



**Realization steps**

  1. Method of preparation: 
  2. pre-heat the oven to 220 ° C. 
  3. In a bowl, mix flour, bread crumbs, salt, 1 teaspoon and a half black pepper, paprika, Espelette pepper and oregano. 
  4. In another large plate, pour into ribot milk. 
  5. Pour ½ cup melted butter into a baking dish. 
  6. Take the chicken pieces and dip them in the ribot milk. 
  7. put the pieces back in the flour mixture, thus covering both sides. 
  8. place in the dish, once all the chicken is placed in the dish, sprinkle with black pepper. 
  9. Put in preheated oven and bake for 15 minutes. Open the oven and spray the pieces. 
  10. cook for another 10 minutes, and still spray with oil, continue to make the oil, to have a tender chicken. 
  11. remove from the oven and put on paper towels. Serve with fries and a nice salad. 


