---
title: pear and ginger jam
date: '2016-10-25'
categories:
- jams and spreads
tags:
- desserts
- Pastry
- To taste
- Easy cooking
- Based
- Toasts
- Apple

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/confiture-poire-gingembre-066_thumb1.jpg
---
![ginger pear jam 066](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/confiture-poire-gingembre-066_thumb1.jpg)

##  pear and ginger jam 

Hello everybody, 

A [ jam ](<https://www.amourdecuisine.fr/categorie-11700642.html>) prepared by us is always a real pleasure. Nothing but **haunting smell** that smells the house when cooking a **jam** ... This **sugar** who **caramelized** gently while cooking these **fruits** ... adding to that the invading smell of **ginger** you are only waiting impatiently for your jam to be ready to taste it ... and this was the case with this delicious pear jam **creamy** and **perfumed** with ginger. 

**pear and ginger jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/confiture-poire-gingembre-069_thumb1.jpg)

**Ingredients**

  * 650 gr of pear / apple mixture (2 apples) peeled. 
  * 350 gr of sugar for jam 
  * 15 gr of ginger 



**Realization steps**

  1. clean, peel and seed the fruits. 
  2. cut them into pieces. 
  3. peel and cut the ginger into pieces. 
  4. place in a heavy saucepan. 
  5. add the sugar, and let the mixture rest between 1 hour and 1 hour 30 minutes, stirring each time, until the sugar melts completely. 
  6. place the pan on low heat now, and cook for about 45 minutes. 
  7. skim the white foam that forms as you cook the jam. 
  8. at the end of the cooking process, mash the whole and place in sterilized jam jars immediately, and turn them until the jam is completely chilled. 


