---
title: mushroom with breadcrumbs
date: '2016-10-20'
categories:
- cakes and cakes
- sweet recipes
tags:
- desserts
- Algerian cakes
- To taste
- Chocolate
- Cake
- Easy recipe
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mouskoutchou-a-la-chapelure-2.jpg
---
![mushroom with breadcrumbs](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mouskoutchou-a-la-chapelure-2.jpg)

##  mushroom with breadcrumbs 

Hello everybody, 

Have you ever tried making a mushroom breadcrumb? Frankly I never thought about it, and this recipe is a great discovery for me. 

The recipe of this delicious mushroom breadcrumb is to Amoula A. a faithful reader and good friend on facebook. Amoula A, is very nice to share her delight with us, I hope that the recipe will please you. Personally I will try to do it as soon as I come out my molds and utensils of my cartons ... The work in the kitchen are finished, but as it is the holidays it will be a nightmare for me to put everything back in order.   


**mushroom with breadcrumbs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/mouskoutchou-a-la-chapelure-1.jpg)

portions:  8  Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients**

  * 3 eggs 
  * 1 glass of sugar (200 ml glass) 
  * 1 packet of vanilla 
  * 1 glass of oil 
  * 1 glass of milk 
  * 1 C. cornflour 
  * 2 tbsp. non-bitter cocoa 
  * 2 bags of baking powder 
  * 3 glasses of bread crumbs 

decoration: optional 
  * chocolate sauce (a little melted chocolate dissolve in a little water) 
  * crushed dried fruits 



**Realization steps**

  1. preheat the oven to 180 ° C 
  2. mix the cocoa and baking powder and sieve them. 
  3. beat the eggs and sugar to a whitish mixture. add vanilla 
  4. beat again and introduce the oil and milk. 
  5. mix cornstarch, cocoa and yeast mixture, and bread crumbs. 
  6. introduce this mixture gradually to the liquid mixture. 
  7. pour the mixture into a savarin dish (or baking tin) and place in the oven for almost 35 minutes. 
  8. towards the end of the cooking turn on the oven from above for 1 to 2 minutes just to give a nice color to the cake. 
  9. you can garnish the cake with a thin layer of chocolate sauce and crushed dried fruit. 



![mushroom with breadcrumbs](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/Mouskoutchou-a-la-chapelure.jpg)
