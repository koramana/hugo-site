---
title: 2 layer chocolate cake
date: '2012-03-02'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, chaussons
- Algerian cuisine
- vegetarian dishes
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/208525431.jpg
---
& Nbsp; hello everyone, here is a recipe that I go back from my archives, a nice chocolate cake, and it was very easy to do, and also, very good, I did not have the chance to take the details of the cake. photo, because my camera was discharged (batteries), and we were treated to a photo after husband took a nice piece of the cake: the ingredients: for the cake: 150 gr of flour 1 ca yeast coffee chemical 40 gr of cocoa 175 gr of margarine 175 gr of crystallized sugar 3 eggs 1 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![gat](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/208525431.jpg)

Hello everybody, 

here is a recipe that I go back from my archives, a beautiful chocolate cake, and it was very easy to do, and also, very good, I did not have the chance to take the details of the cake in photo, because my device Photo was discharged (batteries), and we were treated to a photo after husband took a nice piece of cake: 

Ingredients: 

for the cake: 

  * 150 gr of flour 
  * 1 cup of baking powder 
  * 40 gr of cocoa 
  * 175 gr of margarine 
  * 175 gr of crystallized sugar 
  * 3 eggs 
  * 1 case of instant coffee 



garnish 

  * 75 gr of butter 
  * 250 ml of evaporated milk 
  * 50gr of crystallized sugar 
  * 150 gr of chocolate 
  * grated or rolled chocolate (I bought ready) 



preheat the oven to 190 degrees C, oil 2 small molds of 20 cm diameter each. 

cover the base with sulfur paper 

mix the flour, baking powder, cocoa in a large bowl, add the margarine, the crystallized sugar, and the egg, mix well with the mixer, 

dissolve the coffee in 2 tablespoons of boiling water, add to the mixture and mix until creamy. 

pour this dough equally in the two molds, arrange the surface with a spatula, because the dough is not liquid it is a little hard so it remains like a soft block in the mold. 

put in the oven for 20 minutes, leave to cool for 10 minutes before unmolding, and leave to cool on a wire rack. 

now prepare the cream, putting in a pan, butter, evaporated milk, sugar and chocolate, bring to the heat and mix until all the ingredients mix well, remove from heat, let cool, and then beat the mixture well until it becomes thick and creamy. 

cover the first cake with the cream, put on the second cake, and cover well the whole surface, and finally decorate with chocolate. 

and good appetite, it's a real delight, so for evaporated milk it's a non-sugar condensed milk 

bon appétit. 
