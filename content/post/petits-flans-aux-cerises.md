---
title: Small flans with cherries
date: '2015-05-29'
categories:
- dessert, crumbles et barres
- panna cotta, flan, et yaourt
- recette de ramadan
- recettes sucrees
tags:
- Ramadan 2015
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/petits-flans-aux-cerises-2.jpg
---
[ ![small custard pudding 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/petits-flans-aux-cerises-2.jpg) ](<https://www.amourdecuisine.fr/article-petits-flans-aux-cerises.html/petits-flans-aux-cerises-2>)

##  Small flans with cherries 

Hello everybody, 

It's the cherry season, so let's take the opportunity to make these beautiful and delicious little cherry custards. A Lunetoiles recipe that dates from 2013 on the archive of my email, and I thank Lunetoiles for reminding me this morning. 

Personally, I thought it was the season of apricots (because I start to find on the étales la !!!). I apologize if I am not up to date with the fruits and vegetables of the season, but here in England, it is easily confused, because all fruits and vegetables are always present at supermarkets. 

Of course, it will not have the same taste of vegetables and seasonal fruits, but over time, it will confuse so much, we do not know is the strawberry season, cherries or loquats! You imagine that before yesterday I found very fresh figs, but to know well on the quality and the taste. 

So yesterday I asked Lunetoiles, to find out what the season is like, how fruitful now, and she tells me we're in the middle of the cherry season. I thank her for this reminder, for this recipe of small custard pudding .... Do not hesitate to enjoy this season. 

**Small flans with cherries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/petits-flans-aux-cerises-1.jpg)

portions:  6-8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 5 egg yolks 
  * 150 g caster sugar 
  * 1 sachet of vanilla sugar 
  * 75 flour 
  * 75 cl of milk 
  * pitted cherries 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. In a bowl, mix the egg yolks with the sugar and vanilla sugar until the mixture is white. Then add the flour and mix again. 
  3. Mix with the milk while whisking. 
  4. Divide 6 to 8 cherries in each glass jar and pour in the mixture. 
  5. Bake at 180 ° C for 30 minutes. 
  6. Let cool completely and chill before serving. 


