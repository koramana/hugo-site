---
title: inverted cake apricot acacia almonds
date: '2014-06-07'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/06/gateau-abricot-018_thumb1.jpg
---
![apricot cake 018](https://www.amourdecuisine.fr/wp-content/uploads/2010/06/gateau-abricot-018_thumb1.jpg)

##  inverted cake apricot acacia almonds 

Hello everybody, 

I go back this recipe of the archives of my blog, it dates from June 2010, and sometimes it is necessary to dust the good recipes is not it? So this inverted apricot almond acacia cake is in a nutshell a killer ... 

M  Husband has already eat half of this big cake despite everything, fortunately he has not seen it before I take the pictures lol ... 

A recipe that I will surely redo, but this time I will try with fresh apricots to see the result. 

**inverted cake apricot acacia almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/06/gateau-abricot-016-300x225.jpg)

portions:  12  Prep time:  25 mins  cooking:  60 mins  total:  1 hour 25 mins 

**Ingredients**

  * 2 tbsp. brown sugar 
  * 25 grams of flaked almonds 
  * 400 grs of apricot in box cut in half 
  * 225 gr of butter 
  * 225 gr of sugar 
  * 4 eggs 
  * 200 grams of self-raising flour, or add 1 tsp. coffee baking powder 
  * 25 gr of ground almonds 
  * ½ c. a coffee of almond extract 
  * 3 tablespoons of honey (acacia for me) 
  * 3 tablespoons roasted flaked almonds 



**Realization steps**

  1. preheat the oven to 180 degrees C. and cover a baking oven-baking tin 
  2. a square mold of 20.5 cm on one side, or a rectangular one like mine of 15x25 cm 
  3. sprinkle the mold with brown sugar, then flaked almonds and arrange the apricot moities one in front of the others. 
  4. whisk the butter and sugar to get a light mixture, add the eggs one by one, between one egg and another you can add a tablespoon of flour. 
  5. then add the remaining flour while whisking with the help of your mixer, add the almond powder, and the almond extract. 
  6. pour the mixture on the mold garnished with apricot, try to leave this mixture on the whole surface without moving them from their places. 
  7. place your pan in the oven and cook for 1 hour, or according to your oven. 
  8. at the end of the oven let cool a little before unmolding, brush the top of your cake with honey and garnish with flaked almonds. 



régalez vous bien car c’est un réel délice 
