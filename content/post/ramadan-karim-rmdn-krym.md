---
title: ramadan karim رمضان كريم
date: '2011-07-31'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/ramadan11.jpg
---
![https://www.amourdecuisine.fr/wp-content/uploads/2011/07/ramadan11.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/ramadan11.jpg)

Insha'Allah tomorrow it's going to be Ramadan, 

I address all Muslims, from the bottom of my heart to wish them a peaceful Ramadan, and full of belief insha'Allah. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/ramadan2.jpg)

May ALLAH approve of our fasting, strengthen our faith and make us and our descendants of pious Muslims. 

A deep thought to the sick that Allah will heal them, and give courage to their families and entourage 

and a great thought to all the people who spend Ramadan away from the family, good luck to you dear ones 

and welcoming Ramadan with our big hearts and our great faith 

تقبل الله منا و منكم 
