---
title: tandoori chicken
date: '2014-07-02'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cheesecakes and tiramisus
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/chicken-tandoori-poulet-tandoori-1_thumb1.jpg
---
[ ![chicken tandoori chicken tandoori 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/chicken-tandoori-poulet-tandoori-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-poulet-tandoori-85541703.html> "tandoori chicken")

##  Tandoori chicken 

Hello everybody, 

I really like chicken tandoori, and even though I cook chicken in different ways: 

[ KFC CHICKEN ](<https://www.amourdecuisine.fr/article-26058908.html>) , the [ CHICKEN WITH MUSTARD ](<https://www.amourdecuisine.fr/article-poulet-a-la-moutarde-60630249.html>) , the [ ROASTED CHICKEN WITH LEMON AND HONEY ](<https://www.amourdecuisine.fr/article-33181249.html>) , the [ CHICKEN BREAKING IN THE OVEN ](<https://www.amourdecuisine.fr/article-chilli-hot-chiken-poulet-piquant-au-four-44410988.html>) , and many other recipes that you will find on the [ category chicken recipes ](<https://www.amourdecuisine.fr/categorie-12364208.html>) . 

this time if I left for an even greater taste flavor, a well-known Indian spice flavor, the tandoori masala spice. 

many people want to know the tandoori masala, which is really a mixture of several spices: coriander, fenugreek, salt, cinnamon, caraway, pepper, onion, ginger, clove, garlic, bay leaf, nutmeg, celery, oil lemon, citric acid, which is finely ground with a mortar. 

this delicious chicken I accompany it often with a delicious [ Indian rice ](<https://www.amourdecuisine.fr/article-un-riz-indien-tres-delicieux-48188385.html>) but as before yesterday I was preparing rice, I preferred to present it light, with fries and a fresh salad. 

so to you this [ Indian recipe ](<https://www.amourdecuisine.fr/article-plats-et-recettes-indiennes-102329306.html>) :   


**tandoori chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/chicken-tandoori-poulet-tandoori-1_thumb1.jpg)

portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * chicken legs 
  * 6 tablespoon natural yogurt (depending on the amount of chicken) 
  * 3 cloves of garlic 
  * 1 piece of fresh ginger 
  * Salt 
  * 3 c a s full of tandoori powder 
  * the juice of a lemon. 



**Realization steps**

  1. Remove the skin from the chicken 
  2. Cut the chickens all over the place so that they absorb the marinade well 
  3. Mix in a large bowl all the yogurts and spices. 
  4. add the yogurt. 
  5. add crushed garlic and ginger. 
  6. marinate the chicken pieces in this mixture of spices. 
  7. Allow to absorb a whole night or half a day 
  8. When grilling in the oven, drizzle with ghee (smen) 
  9. Renew the operation as soon as they leave the oven to give them a little sparkle. 



and the, a video that I realized for you, not a montage photo, hihihihi my voice is rusty, because I was very sick .... 

thank you for your visits. 

have a good day 

et pour le passage une belle chanson indienne que j’aimerai partager avec vous: 
