---
title: Kofta hassan pasha "كفته حسن باشا"
date: '2018-01-28'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/kofta-hassan-pacha-1-683x1024.jpg
---
![kofta hassan pacha 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/kofta-hassan-pacha-1-683x1024.jpg)

##  Kofta hassan pasha "كفته حسن باشا" 

Hello everybody, 

Here is a kefta recipe totally different from what we are used to, it is the recipe of the Kofta hassan pasha, or basha (ah for the amateurs of "Harime ossoltane", we have not seen yet , hassan pasha, there is that Ibrahim Pasha, but I'm sure you prefer a dish called 'bali beck', it must be a delicious dish, hihihihi 

You can watch the video on my youtube channel: 

**Kofta hassan pasha "كفته حسن باشا"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/kofta-hassan-pacha-2.jpg)

Recipe type:  dish  portions:  4  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** layer of minced meat: 

  * 300 gr of minced meat 
  * 1 egg 
  * an onion chopped 
  * 1 case of bread crumbs 
  * salt, pepper, spices of your choice (I added '2 cases of parsley ax', my spice coriander / garlic) 

potato layer: 
  * ½ kg of potatoes 
  * ¼ cup of milk (I did not put too much) 
  * salt pepper 
  * 3 cases of grated cheese or any other kind. 



**Realization steps**

  1. mix the meat with the rest of the ingredients from the first layer. 
  2. take balls, dig them in the middle to make a kind of cavity. 
  3. to go quickly, it is better to put the meatballs in the baking tin and shape them by digging them with the thumb and forefinger. 
  4. cook the potatoes with water, crush them with a fork, and add the rest of the ingredients. 
  5. make dumplings and put them in the cavity of the meat. 
  6. pour over smen and cheese (I put olive oil and breadcrumbs)   
(we can shape them with a sleeve pocket to make them prettier) 
  7. pour some water or tomato sauce at the bottom of the dish.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/Recently-Updated8_thumb1-300x187.jpg)
  8. for my part, I preferred to put tomato sauce, mixing a tablespoon of tomato paste with a little water, a drizzle of oil and a quarter of a cube of chicken broth) 
  9. cook for 20 minutes in a hot oven. then return the dish to the top of the oven to brown the potato dumplings. 



![kofta hassan pacha_](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/kofta-hassan-pacha_.jpg)

Bonne dégustation, car la recette est un délice… 
