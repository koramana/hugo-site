---
title: couscous with cardoons, couscous bel khorchefs
date: '2014-10-01'
categories:
- couscous
- Algerian cuisine
- Dishes and salty recipes
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/couscous-au-cardon.CR2-001_thumb1.jpg
---
##  couscous with cardoons, couscous bel khorchefs 

Hello everybody, 

The couscous with the cardoons, or couscous beautiful khorchefs (with khorchefs) is a dish which I like very much, and with the meat of the sheep, this couscous is just a delight. 

I always complained that it was hard for me to find cardoons here in Bristol, but last year, volunteering at my children's school, planting some vegetables, things that are done every season, I I realized that the gardener at the school had planted cardoons. 

I was so moved that I went to talk to him, to tell him that we cook with this vegetable, and that I do not find him anywhere in Bristol ... The gardener was so nice, he gave me some branches, very fresh and especially super delicious, you could eat them even without cooking, and he told me, whatever you want, just ask, I'll pick you the beautiful pieces .... 

I took my pretty cardoon branches home, and immediately I thought about making the couscous cardoon that I love so much. and what was good with these freshly picked cardoons, even the children had to eat (they had eaten them raw, after washing them well and peeling, and also those cooked in the sauce) a very nice surprise for me… 

so without delay, I give you this recipe and I tell you, have a nice day. 

method of preparation: 

cook the couscous with steam in this way: 

**couscous with cardoons, couscous bel khorchefs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/couscous-au-cardon.CR2-001_thumb1.jpg)

portions:  6  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * [ Couscous steamed ](<https://www.amourdecuisine.fr/article-cuisson-du-couscous-a-la-vapeur.html> "Cooking couscous with steam")
  * 1 kg of meat 
  * a handful of chickpeas soak the day before. 
  * 3 large carrots. 
  * some potatoes 
  * 1 card boot 
  * 1 big onions 
  * 2 tablespoons tomato concentrate 
  * 1 tbsp of smen or butter, 
  * 2 tablespoons of oil 
  * paprika, black pepper, salt and ras el hanout 



**Realization steps**

  1. clean the cards and cook them separately with a slice of lemon to remove the bitterness (whiten the cardoons) 
  2. start by grating the onions, 
  3. place them in the bottom of the couscoussier, add the spices, the meat and the oil, 
  4. let it simmer a little, add the carrots peeled, and cut according to your taste, 
  5. cover with a little water and let it cook a little, the meat will take a color, and reject its water. 
  6. add the tomato, and the chickpeas and cover with water. 
  7. once the meat is almost cooked add the cardes and cook. 
  8. Once everything is ready to serve in a hollow dish arrange vegetables, meat and chickpeas, sprinkle sauce, and enjoy 



![couscous with 1.CR2 cardoons](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/couscous-aux-cardons-1.CR2_thumb1.jpg)
