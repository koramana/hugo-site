---
title: blackberry pie with lemon cream
date: '2011-01-14'
categories:
- cuisine tunisienne

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/2294907111.jpg
---
& Nbsp; when I was at the supermarket this week, my eyes flashed on these blackberries, because it awoke in me the memory of my childhood. when we were leaving the weekend of Setif in Constantine at my grandparents, more precisely in the district named mulberry. and my grandparents had this big tree that was more than 70 cm in diameter (trunk). and donations at the time of the blackberries my uncles went up to the tree to move the branches, and to make fall the ripe blackberries. I can tell you that I ate more than & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/2294907111.jpg) ](<https://www.amourdecuisine.fr/article-25345357.htm>)

when I was at the supermarket this week, my eyes flashed on these blackberries, because it awoke in me the memory of my childhood. 

when we were leaving the weekend of Setif in Constantine at my grandparents, more precisely in the district named mulberry. 

and my grandparents had this big tree that was more than 70 cm in diameter (trunk). 

and donations at the time of the blackberries my uncles went up to the tree to move the branches, and to make fall the ripe blackberries. 

I can tell you that I ate more than I picked up, so it was good. 

and when I bought these blackberries, I thought of that taste, so I was waiting to come home, to wash them, and eat them in one bite, but ............ 

it was very acidic, and no taste, I confess ....... 

so to hide the taste, I thought of a pie ......... ..ou find a good pie with a good cream 

So, I had not prepare the dough, but I made with a puff pastry that I had to buy. 

Ingredients: 

  * 1 puff pastry 
  * 2 lemons 
  * 80 gr of sugar 
  * 1 egg 
  * 100 gr of butter 
  * 200 gr of blackberries 
  * icing sugar to garnish the top. 



Take the zest of the washed lemons and chop them. Squeeze the lemons to obtain 50 ml of juice. 

In a saucepan, mix sugar, zest and add egg and lemon juice while whisking.   
Put the pan in a simmering tide and let heat whipping continuously until the cream thickens. 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/2294907611.jpg) ](<https://www.amourdecuisine.fr/article-25345357.htm>)

Remove from the tide and let it whisper while whisking, then add the diced butter while continuing to whisk. Allow the cream to cool completely. 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/2294908211.jpg) ](<https://www.amourdecuisine.fr/article-25345357.htm>)

# pour this cream over the puff pastry already cooked 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/2294908711.jpg) ](<https://www.amourdecuisine.fr/article-25345357.htm>)

garnish washed blackberries 

Sprinkle with icing sugar 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/2294909711.jpg) ](<https://www.amourdecuisine.fr/article-25345357.htm>)

I can assure you this is the best cream I have ever drop 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/2294910211.jpg) ](<https://www.amourdecuisine.fr/article-25345357.htm>)

so the pie was very good with a crisp puff pastry, but the cream was a delight apart. 

I advise you to do it absolutely. 

you will not be disappointed. 

bon appétit 
