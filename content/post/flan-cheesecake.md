---
title: Flan cheesecake
date: '2016-12-26'
categories:
- cheesecakes and tiramisus
- panna cotta, flan, et yaourt

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-cheesecake-1.CR2_.jpg
---
[ ![cheesecake flan 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-cheesecake-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-cheesecake-1.CR2_.jpg>)

##  Flan cheesecake 

Hello everybody, 

Je suis sure que déjà la nom de la recette vous fait le clin d’oeil? Un flan cheesecake??? Personnellement dés que j’ai vu la recette a la télé, je ne voulais que l’essayer, je voulais savoir le gout de ce flan cheesecake et sa texture. C’est pour ça, que je l’ai essayé le lendemain, surtout que j’ai toujours du fromage non salé Philadelphia a la maison, je l’aime beaucoup comme tartine, on garde ça entre nous, Ok 😉 

In any case, this cheesecake flan is really to test, it has the taste and sweetness of a custard, as we have the density and smoothness of a cheesecake. A recipe super easy to make, and really worth it. It is a very hearty dessert, it goes well with a very light meal. Promised, it's a treat. 

We love the blanks at home, like this [ flan at Nestlé ](<https://www.amourdecuisine.fr/article-flan-au-lait-concentre-nestle-106499948.html> "concentrated milk flan / Nestlé") , this [ coconut flan ](<https://www.amourdecuisine.fr/article-flan-coco-2.html> "coconut flan") , this [ egg flan ](<https://www.amourdecuisine.fr/article-flan-aux-oeufs.html> "Egg flan") or so [ custard pastry ](<https://www.amourdecuisine.fr/article-flan-patissier.html> "Custard pastry") . As we like cheesecakes, as this [ cheesecake without chocolate baking ](<https://www.amourdecuisine.fr/article-cheesecake-sans-cuisson-au-chocolat.html> "cheesecake without chocolate baking") , this [ cheesecake with strawberry coulis ](<https://www.amourdecuisine.fr/article-cheesecake-au-coulis-de-fraise.html> "cheesecake with strawberry coulis") , or [ cheesecake white chocolate / almonds ](<https://www.amourdecuisine.fr/article-cheesecake-chocolat-blanc-amandes.html> "Cheese cake white chocolate / almonds") . 

And what is your favorite flan, or cheesecake? or the mixture of the two tempts you well?   


**Flan cheesecake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/fLAN-CHEESECAKE.jpg)

portions:  8  Prep time:  10 mins  cooking:  50 mins  total:  1 hour 

**Ingredients**

  * ½ glass with granulated sugar water 
  * 200 Philadelphia cheese, at room temperature 
  * 5 eggs, at room temperature 
  * 390 g sweetened condensed milk 
  * 400 ml of milk 
  * 1 teaspoon of vanilla extract 



**Realization steps**

  1. Preheat the oven to 150 ° C. 
  2. Melt the sugar in a small, heavy-bottomed saucepan over medium-low heat and cook until golden brown. 
  3. Stir frequently, otherwise the sugar may burn. 
  4. Pour this hot caramel into a 22 cm round pan or a 20 x 20 cm square, tip to cover all sides and the bottom of the pan. 
  5. Beat the cheese until smooth. 
  6. Stir in the eggs, one at a time, until it is well mixed, 
  7. Then stir in both milk and vanilla whisking until smooth. Pour into the mold covered with caramel. 
  8. Line a wider plate with a damp towel. 
  9. Place the flan dish in the plate, put in the oven. 
  10. Fill the plate with boiling water to half height of the flan mold. 
  11. lower the temperature to 120 degrees C and cook for 50 to 60 minutes, until the center is well. 
  12. let cool on a rack, then refrigerate for at least 8 hours. 
  13. To serve, pass a knife around the edges of the mold and return to a serving dish. 



[ ![2.CR2 cheesecake flan](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-cheesecake-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-cheesecake-2.CR2_.jpg>)
