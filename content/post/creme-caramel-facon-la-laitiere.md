---
title: Milk caramel cream
date: '2014-03-21'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/creme-caramel-009a_thumb.jpg
---
[ ![caramel cream 009a](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/creme-caramel-009a_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/01/creme-caramel-009a_2.jpg>)

Hello everyone, 

this dairy caramel cream, and a delight to consume at any time of the day, a super easy recipe, a smooth cream, nicely garnished. 

so here is this very good recipe: 

[ ![milk caramel cream](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/creme-caramel-laitiere_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/01/creme-caramel-laitiere_2.jpg>)

**Milk caramel cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-caramel-004-a_thumb.jpg)

portions:  6  Prep time:  15 mins  cooking:  50 mins  total:  1 hour 5 mins 

**Ingredients**

  * 4 egg yolks + 1 whole egg 
  * 50 cl of ½ skimmed milk (or whole milk) 
  * 40 g of sugar 

caramel: 
  * 100 g of sugar 
  * 2 tablespoons water 



**Realization steps**

  1. method of preparation: 
  2. Preheat the oven to 150 ° C. 
  3. Mix the yolks and the whole egg with the sugar until the unit turns white. 
  4. Make a caramel with the sugar and water until you get a beautiful blonde color (be careful if the caramel is too dark it becomes bitter). 
  5. Add the milk slowly while stirring without stopping (watch out for any projections). The caramel will temporarily harden but it does not matter, it's even normal! Continue stirring and bring to a boil. The caramel will dissolve in warm milk. 
  6. Pour the milk over the egg / sugar mixture very slowly, stirring well with a wooden spatula so that the egg proteins do not coagulate. Do not use the whisk, otherwise it will foam too much and remove the foam before potting. 
  7. Pour the preparation in small pots, ramekins (or in yoghurt pots like me, but mine were a little big). 
  8. Put in the bain marie (the water of the bain-marie must be boiling and arrive at half-height of the pots) 
  9. Cook at 150 ° C for 50 minutes. 
  10. [ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/creme-caramel-1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/01/creme-caramel-1_thumb.jpg>)
  11. Remove from the oven and let cool (out of the bain-marie) 
  12. Refrigerate for at least 2 hours before eating 



I highly recommend this dessert, it is beautifully delicious 

and here are the photos of the realization of lunetoiles, which tempted me to make the recipe. 

[ ![creme-caramel-lunetoiles_2](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/creme-caramel-lunetoiles_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/01/creme-caramel-lunetoiles_2.jpg>)

merci pour vos visites, et tout vos commentaires. 
