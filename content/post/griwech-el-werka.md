---
title: griwech el werka
date: '2015-01-05'
categories:
- Algerian cakes with honey
- fried Algerian cakes
- Algerian cakes- oriental- modern- fete aid
- sweet recipes
tags:
- Algerian cakes
- Frying
- Algerian patisserie
- Oriental pastry
- Algeria
- Cakes
- delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/griwech-el-warka-004.jpg
---
[ ![griwech el werka](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/griwech-el-warka-004.jpg) ](<https://www.amourdecuisine.fr/article-griwech-el-werka.html/griwech-el-warka-004>)

##  griwech el werka 

Hello everybody, 

In any case, to make these griwech el werka or griweches that I named griwech el werka (the leaf), I used the recipe of Oum Amani of the forum el djelfa. 

Griwech el werka is a delight, except that for the realization, you will need a lot of time, and also it consumes a lot of dough. Yes they are super beautiful to present, but not really economical, when you want to have a large amount, hihihihi. 

[ ![griwech el werka](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/griwech-el-warka-023.jpg) ](<https://www.amourdecuisine.fr/article-griwech-el-werka.html/griwech-el-warka-023>)   


**griwech el werka**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/griwech-el-warka-006.jpg)

portions:  30 to 40  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 3 measures of flour (1 measure = 1 glass of 250 ml) 
  * ½ measure of maizena 
  * ½ measure of smen (ghee) melted and cooled 
  * 1 egg yolk 
  * 1 pinch of salt 
  * 1 cup of vanilla coffee 
  * 1 cup of baking powder 
  * water + orange blossom water 
  * oil for frying 
  * honey and sesame seeds for garnish 



**Realization steps**

  1. Mix the flour, cornflour, salt, vanilla and yeast in a large bowl 
  2. add the melted smen, and sand well between your hands. 
  3. incorporate the egg yolk and sanded again. 
  4. pass the mixture through the sieve, it allows to have a more homogeneous mixture. 
  5. Now pick up the mixture with the water and orange blossom water to have a well-rounded paste. 
  6. Form balls and let them rest covered with a cling film. 
  7. After resting take a ball and spread it finely using a baking roller or by hand with corn starch. 
  8. fold the dough, and lay it out again, fold and spread again, to have some crispy and crispy pawpaws in the mouth. 
  9. using a roulette, shape a rectangle almost 12 cm high, 
  10. draw 24 lines spacing them by almost 8 mm. 
  11. fold the dough on itself every 4 lines. 
  12. open the middle delicately (ie separate the 4 rows of line in half to form an "O" 
  13. turn the top of the rectangle on itself, to make it penetrate the opening (as if you are going to make a knot) 
  14. Arrange the strips gently by hand to give a nice fluff to your griwechs. 
  15. Continue until all the dough is gone. 
  16. Heat an oil bath, then reduce the heat. 
  17. Cook the griwechs by monitoring them and turning them over for even cooking. 
  18. Dip the griwechs in honey, and garnish with grilled sesame seeds. 



{{< youtube pATU0BEWoFU >}} 

[ ![griwech el werka](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/griwech-el-warka-017-001.jpg) ](<https://www.amourdecuisine.fr/article-griwech-el-werka.html/griwech-el-warka-017-001>)
