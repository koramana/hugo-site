---
title: Chocolate and peanut butter fondant
date: '2018-04-20'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Fondant-au-chocolat-et-au-beurre-de-cacahu%C3%A8tes-3.jpg
---
[ ![Chocolate and peanut butter fondant](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Fondant-au-chocolat-et-au-beurre-de-cacahu%C3%A8tes-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Fondant-au-chocolat-et-au-beurre-de-cacahu%C3%A8tes-3.jpg>)

##  Chocolate and peanut butter fondant 

Hello everybody, 

It tells you a piece of this delicious f  waving chocolate and peanut butter? in any case me, I will not stop at one piece, yum yum ... Nothing that the photos really give desire, and we thank Lunetoiles for this delight. 

Personally, I will not linger to realize it very soon this  chocolate and peanut butter fondant  as soon as I finish unpacking my bags. And yes the holidays are over, and I'm back home, and my kitchen. 

It must be said that after the Ramadan, then the realization of the cakes of the aid, then the birthday cake of my son, I took a small pose of my cooking in Algeria, the only thing that I concocted, was salads, hmiss, sardines frittes, spaghetti .... finally the cooking of all the days of a hot summer ... 

In any case, I come back to this f  fondant chocolate and peanut butter Lunetoiles to pass you his recipe, and if you realize the recipe, do not forget to send me pictures of your achievement. 

[ ![Chocolate and peanut butter fondant](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Fondant-au-chocolat-et-au-beurre-de-cacahu%C3%A8tes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Fondant-au-chocolat-et-au-beurre-de-cacahu%C3%A8tes.jpg>)

**Chocolate and peanut butter fondant**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Fondant-au-chocolat-et-au-beurre-de-cacahu%C3%A8tes-002.jpg)

portions:  16  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 200 g dark chocolate with dessert 
  * 150 g of sugar 
  * 150 g of butter 
  * 85 g of peanut butter 
  * 50 g of flour 
  * 80 g whole unsalted and roasted peanuts 
  * 3 eggs 



**Realization steps**

  1. Melt dark chocolate with butter, in a bain-marie, or in the microwave. 
  2. Then add the peanut butter, using a low speed electric mixer. 
  3. Stir in the sugar and then the eggs and flour, mixing well between each addition. 
  4. Add 40 g whole unsalted peanuts to the dough and mix quickly. 
  5. Pour your preparation in a square baking pan, put in the fridge for 1 hour. 
  6. After one hour, preheat the oven to 150 ° C. 
  7. Sprinkle top with remaining whole peanuts, spread evenly. 
  8. Bake at 150 ° C for 20 min no more! 



[ ![Chocolate and peanut butter fondant](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Fondant-au-chocolat-et-au-beurre-de-cacahu%C3%A8tes-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Fondant-au-chocolat-et-au-beurre-de-cacahu%C3%A8tes-001.jpg>)

Thank you my friend for your comments, and your passage on the blog, 

Bisous et a la prochaine recette. 
