---
title: short crumbly biscuits with jam
date: '2016-06-23'
categories:
- Algerian cakes- oriental- modern- fete aid
- gâteaux sablés, ghribiya
tags:
- Ramadan
- Algeria
- Oriental pastry
- Ramadan 2016
- Eid
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/biscuits-sabl%C3%A9s-friable-double-a-la-confiture-1-754x1024.jpg
---
![Shortbread biscuits friable with jam 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/biscuits-sabl%C3%A9s-friable-double-a-la-confiture-1-754x1024.jpg)

##  short crumbly biscuits with jam 

Hello everybody, 

Thanks to my dear Lunetoiles who reminded me that I did not share with you his recipe for **crumbly biscuits double with jam,** that she had made in September 2015, but that I did not have the chance to publish. 

It's never too late to do well, is it? 

It is shortbread all melting and super good made by sticking 3 pieces of shortbread. The idea I like well, in addition if Lunetoiles says it's friable, so it must be super good. 

![crumbly biscuits double with jam 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/biscuits-sabl%C3%A9s-friable-double-a-la-confiture-3-739x1024.jpg)

**short crumbly biscuits with jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/biscuits-sabl%C3%A9s-friable-double-a-la-confiture-4.jpg)

portions:  30  Prep time:  20 mins  cooking:  9 mins  total:  29 mins 

**Ingredients**

  * 250 gr of soft butter 
  * 140 gr of icing sugar 
  * 2 bags of vanilla sugar 
  * 4 egg yolks at room temperature 
  * 200 gr of cornflour 
  * 300 gr of flour 
  * ½ sachet of baking powder 
  * jam (to choose) 
  * icing sugar to sprinkle 



**Realization steps**

  1. Beat the softened butter with the icing sugar until the mixture whitens. 
  2. Add the egg yolks one by one while continuing to beat. 
  3. Stir in vanilla sugar, yeast, cornflour and flour. 
  4. Mix well with your hand to pick up the dough. 
  5. Let your dough rest in the fridge for 1 hour. 
  6. Spread the dough on a floured work surface. 
  7. Cut with a serrated round punch. Make a hole in the center of every third room. 
  8. Put the pieces on a baking sheet lined with baking paper 
  9. Gather the dough falls and repeat the operation. 
  10. Put in the oven at 160 degrees for 9 minutes (they should remain white). 
  11. Remove the oven plates and place them on a rack.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/biscuits-sabl%C3%A9s-friable-double-a-la-confiture.jpg)
  12. Let the biscuits cool for 15 minutes - then transfer them to the rack until cool. 
  13. To assemble the biscuits: garnish with jam the middle of a simple biscuit, and place another simple biscuit above, garnish again the jam medium and cover with a biscuit the one with a hole in the center and press a little, you get a double-layer shortbread. 



![short crumbly biscuits with jam 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/biscuits-sabl%C3%A9s-friable-double-a-la-confiture-2-799x1024.jpg)
