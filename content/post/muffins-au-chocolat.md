---
title: Chocolate muffins
date: '2013-03-03'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

---
Hello everybody  , 

finally I gave in to temptation, and I made delicious [ muffins ](<https://www.amourdecuisine.fr/categorie-11410973.html>) chocolate, not any chocolate, it's the dark chocolate of my husband, the proof on the photo ... .. 

I can not hide my crime, but the muffins with this bitter chocolate were just delicious, and what do I like about them? [ chocolate cake ](<https://www.amourdecuisine.fr/article-fondant-chocolat-99664287.html>) . 

My temptation did not stop there, I also touched the white chocolate of the children, to have white chocolate chips in the interieurm, and the children loved these muffins, hum !!! 

I stop talking about that and I give you this little recipe, because I did not abuse, I only made 7 muffins, ok 

ingredients: 

  * 50 gr dark chocolate 
  * 50 gr of sugar 
  * 25 gr of melted butter 
  * 125 ml of milk at room temperature 
  * 1/2 teaspoons of vanilla 
  * 1 beaten egg 
  * 150 gr of flour 
  * 1 cup of baking powder 
  * 1 pinch of salt 
  * 75 gr of white chocolate in chunk 
  * 2 teaspoons icing sugar for decoration (optional) 



method of preparation: 

  1. Melt the chocolate in a bain-marie. 
  2. add the sugar and the melted butter, mix  a little. 
  3. add milk, vanilla, egg,  Mix  a little, 
  4. sift the flour the yeast and the salt, 
  5. Mix  all, by hand, just to mix, do not worry too much  mix  . 
  6. add the white chocolate, and fill the boxes with muffins  already  to prepare  . 
  7. cook in an oven  PREHEATED  a 180  degrees C,  for 20 to 25 minutes, or until the top of the muffins is a little dry to the touch. 
  8. at the exit of the oven,  Decorate  with a little icing sugar, leave in the muffin pan for a few minutes, then put on a rack until it is completely cool. 



bonne degustation, 
