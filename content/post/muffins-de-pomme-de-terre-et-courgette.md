---
title: Potato and zucchini muffins
date: '2008-01-05'
categories:
- couscous
- Algerian cuisine
- Dishes and salty recipes
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/01/207004051.jpg
---
![Bonj](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/206822081.gif)

my 2-year-old child is very difficult, to make him eat I really suffer, a friend recently told me about a book by Annabel Karmel, I was able to get it, and I made this recipe, because had the ingredients, and also the recipe attracted me a lot. 

I can tell you, I had 12 muffins in the end, and Rayan and Lyna fell on them like hungry, rayan to eat 4 and a half, it's a roccord for him who never finish his ass, or so who after 3 spoons usually he leaves the table. 

so to all the moms, or if you have nuns, they will like this: 

  * 2 potatoes peeled and grated 
  * 1 zucchini rapee 
  * 1 onion rape 
  * 1 beaten egg 
  * 2 cases of flour 
  * 50 gr of melted butter 
  * salt and black pepper 



drain the potato, zucchini and onion, add the beaten egg, salt and black pepper, melted butter and flour. 

put a case in boxes in a muffin pan, and put in a crazy oven already heated. 

![S7301896](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/207004051.jpg)

cook for 35 minutes, or depending on the capacity of your oven. 

serve warm, for what not with a little mayonnaise, like eating Lyna. 

![S7301897](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/207004641.jpg)

good appetite to our children. 

![th_bye_bye](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/206822421.gif)
