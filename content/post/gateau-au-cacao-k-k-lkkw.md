---
title: cocoa cake كعك الكاكاو
date: '2013-03-16'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

---
Hello everybody, 

here is the recipe (copy / paste) 

  * 100g of butter 
  * 100g of sugar 
  * 3 cs of  ****Cocoa** powder Ethiquable (cocoa vanhouten) **
  * 200ml of lukewarm milk 
  * 2 eggs 
  * 250g of flour with yeast incorporated  **(flour + a packet of baking powder)**



Preheat your oven to 180 ° C.    
In a bowl, mix the sugar with the softened butter. Add, the eggs one by one. Add the small flour, small, stirring well **(I used the electric mixer)** In a bowl, mix the cocoa with the warm milk. Pour the cocoa mixture with the other device.    
Stir everything until a homogeneous consistency.    
Butter a cake tin, sprinkle the bottom with sugar and bake until the blade of a knife comes out dry. 

bonne Degustation. 
