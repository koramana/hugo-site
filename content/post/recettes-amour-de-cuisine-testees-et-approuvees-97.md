---
title: 'kitchen love recipes tested and approved # 97'
date: '2016-09-16'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg>)

##  kitchen love recipes tested and approved # 97 

Hello everybody, 

I'm back with the photos of your achievements that you found on my blog, thank you to my dear readers to be faithful to my blog and realize my recipes. Thanks also to those who leave me their comments after trying the recipe, even if I do not have the photo of your realization, I am always happy for your success. 

If you are not a member of this group, join me on it, otherwise if you do not have a facebook account, keep sending me the pictures on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

![clafoutisfigues1](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Clafoutisfigues1.jpg)

![Samia-tarts](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/tartelettes-samia.jpg)

[ tartlets with dried fruits ](<https://www.amourdecuisine.fr/article-tartelettes-aux-fruits-secs-caramel-beurre-sale.html>) at Samia L 

![sand-Wamani](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/sabl%C3%A9-wamani.jpg)

[ Shortbread with strawberry jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-fraises.html>) at Wamani Merou 

![14212224_1029673837149997_6722910962231136264_n](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/14212224_1029673837149997_6722910962231136264_n.jpg)

[ Crunchy raisins ](<https://www.amourdecuisine.fr/article-croquant-aux-raisins-secs-ou-croquets-gateaux-algeriens.html>) at Wamani Merou. 

![madeleine-naziha](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/madeleine-naziha.jpg)

[ chocolate madeleines ](<https://www.amourdecuisine.fr/article-les-madeleines-recette-inratable.html>) at Naziha stras 

![sand-myra-ben](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/sabl%C3%A9-myra-ben.jpg)

[ Shortbreads fondant jam of apricots and almonds ](<https://www.amourdecuisine.fr/article-sables-fondants-abricot-amandes.html>) at Myra Ben 

![kaak-Nakache](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/kaak-nakache.jpg)

[ Kaak nakache with dates ](<https://www.amourdecuisine.fr/article-kaak-nakache-aux-dattes.html>) at Fleur Dz 

![flan-caramel-spill](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/flan-caramel-renvers%C3%A9.jpg)

[ Crème caramel inversé ](<https://www.amourdecuisine.fr/article-creme-renversee-caramel-cardamome.html>) at Fleur Dz 

![brioche au speculoos](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/brioche-au-speculoos.jpg)

[ Puff pastry bun with speculoos ](<https://www.amourdecuisine.fr/article-brioche-feuilletee-la-pate-au-speculoos.html>) at Naziha Stras 

![juice, figs de barbarism](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/jus-figues-de-barbarie.jpg)

[ Juice of prickly pears ](<https://www.amourdecuisine.fr/article-jus-figue-barbarie.html>) at Caroline's place 

![Tomato soup](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/soupe-de-tomates-2.jpg)

![ghribia-louiza](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/ghribia-louiza.jpg)

[ Ghribia with almonds ](<https://www.amourdecuisine.fr/article-ghriba-aux-amandes-gateau-sec-aux-amandes-facile.html>) at Lessine louiza 

![pineapple cake](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/gateau-dananas.jpg)

[ pineapple cake ](<https://www.amourdecuisine.fr/article-gateau-renverse-lananas-facile.html>) at Dhal 

![panyandu pineapple spilled cake](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/gateau-renvers%C3%A9-ananas-panyandu.jpg)

![Tunisian malsouka](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/malsouka-tunsienne.jpg)

![donuts](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Lenovo_A1000_IMG_20160716_184745.jpg)

[ homemade donuts ](<https://www.amourdecuisine.fr/article-donuts-maison-faciles-et-delicieux.html>) chez Yamyam. 
