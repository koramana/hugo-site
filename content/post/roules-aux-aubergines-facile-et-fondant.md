---
title: eggplant rolls easy and melting
date: '2013-08-11'
categories:
- Algerian cuisine
- Cuisine by country
- salty recipes
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/aubergines-rolls-028_thumb.jpg
---
##  eggplant rolls easy and melting 

Hello everybody, 

I tell you one thing these easy and melting aubergine rolls are just a wonder, to introduce as [ appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) or as a small entrance. 

I did not imagine that the [ basil pesto ](<https://www.amourdecuisine.fr/article-pesto-au-basilic-fait-maison-101909328.html>) with melting mozzarella, was going to add all this taste to these [ eggplant ](<https://www.amourdecuisine.fr/article-que-faire-avec-aubergines-102011214.html>) grilled and rolled   


**aubergine rolls / easy and melting**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/aubergines-rolls-028_thumb.jpg)

Recipe type:  Appetizer, aperitif  portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 2 eggplants finely cut into strips 
  * 75 ml of olive oil 
  * 1 crushed garlic head 
  * 60 ml of basil pesto 
  * 175 grams of mozzarella 
  * salt and pepper 
  * basil leaves for decoration. 



**Realization steps**

  1. dirty the eggplant, to remove the bitterness, and reduce some of its water. 
  2. rinse the eggplants well with the salt, and place them on sopalain paper. 
  3. fry in a little hot oil, and let drain well. 
  4. cover each eggplant leaf with pesto, then mozzarella, 
  5. and roll it on itself, then prick it with a toothpick. 
  6. place the eggplant rolls in a lightly oiled gratin dish, 
  7. cook in a preheated oven for 8 to 10 minutes at 180 degrees C until the mozzarella melts well. 
  8. present while decorating with chopped basil. 



suivez moi sur: 
