---
title: Four quarter pure Breton butter bar recipe
date: '2014-12-09'
categories:
- cakes and cakes
- sweet recipes
tags:
- Cake
- Cakes
- To taste
- Algerian cakes
- Easy cooking
- Mellow cake
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quatre-quart-pur-beurre-2.jpg
---
[ ![Four quarter pure Breton butter bar recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quatre-quart-pur-beurre-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quatre-quart-pur-beurre-2.jpg>)

##  Four quarter pure butter or Breton bar recipe 

Hello everybody, 

**Four quarter pure butter recipe: Breton bar:** yes I know what you will tell me: a quarter of pure butter, it is a four quarter rich in calories, but it must be very frank and frank, it is a pure delight this quarter, it is the recipe We had eaten so much during our childhood, at tea time, after a hard day at school, it was this four quarter pure butter that comforted us, and made us forget all the fatigue of the day ... 

I remember I sipped my hot milk by biting generously in my four-quarter share. At the time I did not care about the size or calorie intake, and it's not now that I'm going to take my kids away from this fun, just to keep the line, and also, take a last this quarter, really took me years backwards, in those years when we did not care about anything, we played, we ate, we fell, we cried, but we forgot. 

So, what are you going to do like me, you feast for once, without looking at the scales and torturing your mind? 

The recipe I pass to you as I learned from my mother who had learned at a young age of his cousin by marriage "Josiane", so the recipe is without measure, but it is 100% successful at all times . One note, my cake mold is rather wide, hence the fact that I do not get a cake very high.   


**Four quarter pure butter recipe: Breton bar**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quatre-quarts-au-pur-beurre-1.jpg)

portions:  12  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 4 eggs 
  * butter half salt (the same weight as the 4 eggs) 
  * flour (the same weight as the 4 eggs) 
  * sugar (the same weight as the 4 eggs) 
  * 1 sachet of baking powder 
  * 1 packet of vanilla 
  * 1 pinch of salt 



**Realization steps**

  1. Melt the butter in a bain-marie, 
  2. add the sugar and beat all with a wooden spoon. 
  3. Add eggs, vanilla 
  4. sift together the flour and yeast and add it gently to the mixture 
  5. Butter a cake tin and pour the dough into it. 
  6. Bake at 180 ° for about 40 minutes or depending on your oven. 
  7. Check the cooking with a toothpick, the tip should come out dry. 
  8. Let cool before eating 



[ ![Four quarter pure Breton butter bar recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quatre-qurt-pur-beurre.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quatre-qurt-pur-beurre.jpg>)
