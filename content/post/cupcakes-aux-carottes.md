---
title: carrot cupcakes
date: '2017-03-04'
categories:
- Cupcakes, macarons, et autres pâtisseries
- gateaux, et cakes
- recettes sucrees
tags:
- Easy cooking
- desserts
- Pastry
- Algerian cakes
- Inratable cooking
- To taste
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cupcakes-aux-carottes.jpg
---
[ ![carrot cupcakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cupcakes-aux-carottes.jpg) ](<https://www.amourdecuisine.fr/article-cupcakes-aux-carottes.html/cupcakes-aux-carottes-1>)

##  carrot cupcakes 

Hello everybody, 

A great treat for afternoon tea, beautiful cupcakes with carrots that are not only beautiful, they are also good. 

It's a recipe from a friend of mine, known as Wamani Merou, on facebook. So today it is generously that she shares with us these pretty cupcakes too beautiful carrots .. Easy to make, and yumi to taste ... 

**Carrot cupcakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cupcakes-aux-carottes-1.jpg)

portions:  15  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 300 gr of flour 
  * ½ teaspoon cinnamon 
  * ½ cup of baking soda 
  * 1 packet of dry yeast 
  * 200 gr of brown sugar 
  * 4 eggs 
  * 230 ml of oil 
  * 200 gr of carrots rappées 
  * 150 g crushed walnuts 
  * Zest of an Orange 

decoraction 
  * 200 gr of mascarpone 
  * 90 gr iced sugar 
  * almond paste in orange 
  * almond paste in green 



**Realization steps**

  1. Preheat the oven to 180 °. 
  2. Peel and grate the carrots. 
  3. Beat the eggs and sugar until blanching, 
  4. stir in the oil and grated carrots. 
  5. mix dry products: flour, yeast, baking soda and cinnamon. 
  6. Add the crushed walnuts and the orange zest towards the end. 
  7. Bake between 20 and 25 minutes 
  8. Prepare the cream, whipping the mascarpone generously to aerate, add the sugar while whisking. 
  9. when the cupcakes have cooled down well, decorate them with this mascarpone cream. and marzipan in the form of carrots. 



[ ![carrot cupcakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cupcakes-aux-carottes-2.jpg) ](<https://www.amourdecuisine.fr/article-cupcakes-aux-carottes.html/cupcakes-aux-carottes-2>)
