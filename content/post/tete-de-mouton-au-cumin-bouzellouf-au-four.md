---
title: head of sheep with cumin, baked bouzellouf
date: '2014-09-30'
categories:
- Algerian cuisine
- Moroccan cuisine
- Tunisian cuisine
- recipe for red meat (halal)
tags:
- Algeria
- giblets
- Lamb
- Aid
- Morocco
- Full Dish
- tajine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/tete-de-mouton-au-cumin-023_thumb1.jpg
---
![head of sheep with cumin 023](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/tete-de-mouton-au-cumin-023_thumb1.jpg)

##  head of sheep with cumin, baked bouzellouf 

Hello everyone, 

At the approach of the aid el kebir "Aid el kbir" or the festival of the sheep, one always seeks new recipes, to take advantage of the sheep of Aid. Of course, there are traditional recipes, like: [ kebda mchermla ](<https://www.amourdecuisine.fr/article-recette-kebda-mchermla.html> "kebda mchermla") , [ brains of lamb in tomato sauce ](<https://www.amourdecuisine.fr/article-cervelle-d-agneau-en-sauce-tomate-89450259.html> "brains of lamb in tomato sauce - chtitha mokh") , [ bakbouka ](<https://www.amourdecuisine.fr/article-bakbouka-tajine-de-tripes-de-mouton-cuisine-algerienne.html> "bakbouka tajine of sheep guts-Algerian cuisine") , [ couscous bel osbane ](<https://www.amourdecuisine.fr/article-couscous-bel-osbane.html> "Couscous beautiful osbane كسكسي بالعصبان") , [ loubia bel ker3ine (white bean cassoulet with sheep's feet) ](<https://www.amourdecuisine.fr/article-cassoulet-d-haricots-blanc-aux-pieds-de-mouton-loubia-bel-ker3ine.html> "White bean cassoulet with lamb's feet, loubia bel ker3ine") [ mesrane mahchi (stuffed gut) ](<https://www.amourdecuisine.fr/article-mesrane-mahchi-boyau-farci-recettes-aid-el-kebir-2012.html> "Mesrane mahchi, stuffed gut, recipes help el kebir 2012") and many others that women prefer to prepare, because because of the dearness of life, sometimes we have these parts of the sheep, only during the feast of the Aid. 

But, there are families, who are with family members a little capricious, who do not like such or such dish, and it is here that opts for new recipes that change the classics. and here is a very good recipe, prepare with the head of the lamb or mutton as we prefer to say, A recipe that pass me a reader. 

I thank you very much Nissa, for this delicious recipe, to eat without moderation, hihihihi 

**head of sheep with cumin, baked**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tete-de-mouton-au-cumin-au-four-041_thumb.jpg)

portions:  2  Prep time:  30 mins  cooking:  60 mins  total:  1 hour 30 mins 

**Ingredients**

  * sheep's head   
cleaned and cooked in salted water, with a little bay leaf, thyme, an onion head cut in 4, a clove of garlic, pepper and cumin 
  * 4 cloves of garlic 
  * a bunch of chopped parsley 
  * pepper and cumin 
  * 2 glasses of bouillon sauce of cooking of the sheep's head 
  * a handful of pitted olives, and boiled in water in water (to minimize salt) 



**Realization steps**

  1. bend your head, and cut into pieces 
  2. add crushed garlic, chopped parsley 
  3. add pepper and cumin 
  4. mix well, place in the fridge for 2 to 3 hours to marinate well 
  5. add the olives, and the broth of the cooking water 
  6. and place in the oven for 10 to 15 minutes 
  7. then let it cook from above to take a nice color 
  8. serve this delicious dish, with fries, or then rice, or even simply with a good salad 



merci pour vos commentaires, et merci pour vos visites 
