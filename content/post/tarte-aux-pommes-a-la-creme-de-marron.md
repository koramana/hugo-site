---
title: apple pie with brown cream
date: '2017-12-05'
categories:
- cakes and cakes
- sweet recipes
- pies and tarts
tags:
- Based
- Pastry
- desserts
- creams
- To taste
- Golden
- Vanilla

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/tarte-aux-pommes-a-la-creme-de-marrons.jpg
---
![apple pie with chestnut cream](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/tarte-aux-pommes-a-la-creme-de-marrons.jpg)

##  apple pie with brown cream 

Hello everybody, 

When I made the cream of brown the last time, Lunetoiles gave me a recipe from Ranouza who is a member of the **forum Small happiness patissier.** I tried to find the blog Ranouza online, but it is inaccessible, I would have liked to thank for this delicious recipe for apple pie with chestnut cream. 

In any case the recipe is super easy to achieve, and the taste is just terribly good ... 

**apple pie with brown cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/tarte-aux-pommes-a-la-creme-de-marron-1.jpg)

portions:  8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** the shortbread dough: 

  * 250 gr of flour 
  * 125 gr of butter 
  * 1 tablespoon icing sugar 
  * 1 egg yolk 
  * 2 or 3 tablespoons liquid cream 

Cream pastry cream with chestnut: 
  * ½l of milk 
  * ½ vanilla pod 
  * 3 egg yolks 
  * 40 gr of sugar 
  * 70 gr of flour 
  * 200 gr of [ Chestnut cream ](<https://www.amourdecuisine.fr/article-recette-de-la-creme-de-marrons-maison-113097294.html>)

Garnish: 
  * 2 or 3 apples 
  * Sugar 
  * Topping (side) 



**Realization steps** Start by preparing the pie dough. 

  1. In a bowl, pour the flour. Add the icing sugar and mix. 
  2. Add the soft butter (almost melted), mix, 
  3. add the egg yolk and mix by sanding the dough with your fingers. 
  4. Finish by adding the cream and mix the dough so as to obtain a beautiful smooth and homogeneous ball. Shoot and book cool! 

Prepare the pastry cream: 
  1. In a saucepan, heat the milk over low heat. With the tip of a knife split the vanilla pod, take the seeds and put them in the milk. 
  2. Heat gently so that the milk is infused with vanilla. 
  3. In addition, whip the egg yolks and sugar until the mixture whitens and becomes foamy. Add the flour, mix well. 
  4. As soon as the milk begins to boil, remove it from the heat. Pour the boiling milk over the egg-sugar flour mixture while mixing. 
  5. Mix well and pour the cream into the pan and put to thicken over low heat, stir constantly scraping well bottom for at least 7 - 10 minutes. 
  6. To the first bouillons, turn off the cream which will have thickened. When the cream begins to warm up. Add the chestnut cream, mix well to obtain a uniform cream.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/creme-patissiere-a-la-creme-de-marron.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/creme-patissiere-a-la-creme-de-marron.jpg>)
  7. Take the dough out of the fridge. Spread it on a sulphurous paper, flouring lightly. The darker in the mold with the sulfide, which will facilitate the demolding. Pre-cook for a few minutes at 180 ° C. (I had prepared and cooked my dough at night) 
  8. Once the bottom precooked, remove it, add the pastry cream with chestnut cream.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/remplissage-de-la-creme.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/remplissage-de-la-creme.jpg>)
  9. Peel the apples, cut them into 4 quarters and remove the stalks. Cut into thin slices. Arrange the slices of apples harmoniously in a rosette by superimposing them a little.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/garniture-aux-pommes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/garniture-aux-pommes.jpg>)
  10. Sprinkle the tart with sugar and bake at 180-200 ° C until the apples are cooked and begin to caramelize. 
  11. Let cool before unmolding and put on a dish. (for the photo I cut the pie before it cools) 
  12. Heat 1 large tablespoon of topping. With a brush brush the pie to make it shiny. Book fresh. 
  13. Good tasting 



[ ![apple pie with brown cream 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/tarte-aux-pommes-a-la-creme-de-marron-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/tarte-aux-pommes-a-la-creme-de-marron-2.jpg>)
