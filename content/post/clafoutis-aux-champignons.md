---
title: clafoutis with mushrooms
date: '2012-09-11'
categories:
- gateaux algeriens sans Cuisson
- gateaux algeriens- orientales- modernes- fete aid

---
hello everyone, a clafoutis mushroom, first participation in the contest "cuisine express" (contest ended now) a recipe of Denis Beshier, thank you for the recipe, and good luck in the contest. Quick recipe idea, easy and inexpensive. Personally, I do this recipe quite often, as you come home from work, races, in 15 minutes everything is done, and the result is satisfactory. You can also make this clafoutis with leftover vegetables from a previous meal. The result is the same: zucchini, carrots, Brussels sprouts, cauliflower (also extra). I let you choose you will be filled ONE CLAFOUTIS & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

a  **mushroom clafoutis** , first participation in [ "express kitchen" contest ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>) (contest ended now) a recipe from Denis Beshier, thanks for the recipe, and good luck in the contest. 

** Quick recipe idea, easy and inexpensive  . **

Personally, I do this recipe quite often, just like you  returning from work, races, in 15 minutes everything is done, and the result  is satisfactory. 

You can also make this clafoutis with leftovers of vegetables from a  previous meal. 

The result is the same: zucchini, carrots, Brussels sprouts, cabbage  flower (extra also).    
I let you choose you will be satisfied 

**A CLAFOUTIS WITH MUSHROOMS**

ingredients 

* 200 g of Paris mushrooms or different mixed mushrooms    
* 1 shallot    
* 3 eggs    
* 15 cl of milk    
* 10 cl of fresh cream    
* 1 tablespoon cornflour    
* 15 g of butter    
* 20 g of Parmesan or Gruyère grater    
* 1 tablespoon of basil or herbes de provence    
* salt    
* pepper. 

Preparation 

Peel and mince the shallot. Wash and cut the mushrooms into thin  lamellae. 

In a skillet, melt the butter and brown the shallot 5  minutes. Add the mushrooms and simmer for 10 minutes. 

In a bowl, beat the eggs. Add cornflour, parmesan cheese or  gruyere, basil or herbes de provence, milk and cream. Good  mix with a whisk. Salt and pepper. I put a touch of caraway  powder to give more taste and that is also very good for the  digestion. 

Add the mushrooms and shallot to the mixture and pour it all  in a dish previously buttered and floured. 

Bake 35 minutes th 6 (180 °). 

This quick dish can be accompanied by a green salad, a salad of  tomatoes or salad of your choice 

Bon appétit à tous !!! 
