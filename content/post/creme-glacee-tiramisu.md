---
title: Tiramisu ice cream
date: '2017-07-12'
categories:
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/creme-glacee-tiramisu-001_thumb1.jpg
---
##  Tiramisu ice cream 

Hello everybody, 

Since my husband bought me an ice cream machine on my birthday, last year, I almost did not buy any **ice cream** because I find that the ice cream trade here in England too sweet, may not be everyone will be with me on this subject, but I swear, I feel the sugar more than any other taste. 

I'm not going to lie, I was doing the [ easy ice cream ](<https://www.amourdecuisine.fr/creme-glacee-et-sorbet>) , with a whipped cream, and fruits of all kinds, but I did not have time to make the real good ice cream. 

My friend Ouhabia author of the recipe [ spinach slippers ](<https://www.amourdecuisine.fr/article-chausson-aux-epinards-73387059.html>) on my blog, which has done courses in this area, I passed his recipe, which I immediately liked to test, of course after, it's up to you to choose the flavor you want to add. 

This cream I called tiramisu because with the taste of coffee mixed with [ the custard ](<https://www.amourdecuisine.fr/article-creme-anglaise-custard.html>) , and fresh cream, this the delight of [ Tiramisu ](<https://www.amourdecuisine.fr/article-tiramisu-avec-du-mascarpone-fait-maison.html>) who will score you the most. 

**Tiramisu ice cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/creme-glacee-tiramisu-001_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 4 egg yolks 
  * 150 gr of sugar 
  * 300 ml whole milk 
  * 200 ml of fresh cream 
  * Vanilla (in liquid pod or powder but not vanilla sugar) 
  * 4 boudoirs 
  * 1 glass of coffee 



**Realization steps**

  1. dip the boudoirs in the coffee is put in the freezer (unsweetened coffee) 
  2. Prepare a custard: mix the egg yolks, sugar and vanilla to have a voluminous and white mixture 
  3. add on it hot boiled milk 
  4. put back on the fire without stirring until the cream laps the back of the spoon when you draw a line on the back of the spoon with the finger the line remains clean, do not let too much boil if the yellow of egg will coagulate, and the cream may turn, no matter if your cream turns, mix right away with a foot mixer, and your cream will be perfect. 
  5. immediately remove from heat and let cool 
  6. Once cold add the cream 
  7. mix and put in the freezer until it is very cold it should not become an ice cube but a very cold liquid makes it easier for the ice cream maker, 
  8. remove the boudoirs of the freezer is cut them in 
  9. put in the ice cream maker on the way and pour the cream into it. 
  10. in operation add boudoirs to pieces. 
  11. When ready, put the ice back in the freezer. 



Un réel délice, et la prochaine fois je vais mettre du mascarpone avec la crème fraiche, et je vous dirai le résultat. 
