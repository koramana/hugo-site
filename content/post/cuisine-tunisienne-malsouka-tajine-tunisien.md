---
title: Tunisian cuisine Malsouka- Tunisian tajine
date: '2014-01-20'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/malsouka-024_thumb.jpg
---
[ ![Tunisian cuisine Malsouka- Tunisian tajine](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/malsouka-024_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/malsouka-024.jpg>)

##  Tunisian cuisine: Tunisian Malsouka-tajine 

Hello everybody, 

I really like the [ Tunisian cuisine ](<https://www.amourdecuisine.fr/categorie-12359410.html>) because at my young age, our neighbor was a **Tunisian** , and what did I like about cooking, even though I did not eat **piquancy** I cried, I finished the bottle of water, but I do not let anyone eat my part, now and despite that I prepare these [ Tunisian dishes ](<https://www.amourdecuisine.fr/categorie-12359410.html>) for my taste, remains that the spicy, really gives the fair value to **Tunisian dish** . 

and here is a very delicious **Tunisian tajine** that I prepared last night, for my little family, and that we enjoyed, the **recipe** is very **easy** , and too good, but you will need time to do it, do not like me, I started at 18:30, we eat at 20:30, hihihiihih 

The recipe in Arabic: 

so you get the recipe, and start early: 

**Tunisian cuisine: Tunisian Malsouka-tajine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/malsouka-025_thumb.jpg)

Recipe type:  Tunisian dish  portions:  8  Prep time:  60 mins  cooking:  45 mins  total:  1 hour 45 mins 

**Ingredients**

  * A chicken breast, 
  * 10 to 12 sheets of bricks 
  * Onion, finely chopped 
  * 100gr grated cheese (for me Mozzarela mix) 
  * 1 handful of green olives, pitted and sliced 
  * 2 boiled eggs 
  * 3 eggs 
  * 1 tablespoon of oil 
  * Chopped parsley 
  * Canned mushrooms 
  * Salt, pepper, coriander powder 



**Realization steps**

  1. grate the onion in a pot, add the oil and mushrooms, and fry for 5 minutes over medium heat. 
  2. As soon as the onion becomes translucent, add the chicken pieces, brown a little 
  3. add olives, spices, stirring occasionally. 
  4. Then cover with hot water and cook for 10 to 15 minutes, until the chicken is completely cooked and the sauce is reduced, and then almost 8 cups of sauce, which will be used to brush the leaves of the Brik, 
  5. let cool. 
  6. Take about 7 tablespoons of sauce, add a little melted butter and set aside. 
  7. Crumble the chicken, put it back in the sauce, add the cheese, egg yolks, chopped mushrooms, parsley and boiled eggs. 
  8. Beat the egg whites and gently stir them in without breaking them 
  9. Butter or oil copiously a round pan. 
  10. Brush the leaves with the butter-sauce mixture, arrange them in the mold, overlapping them and leaving a little outside the mold. 
  11. Put one to two leaves in the center for it to hold. 
  12. Pour the tajine stuffing and fold over the leaves, cover with remaining brik leaves. 
  13. You can brown the last leaf of some butter nuts. 
  14. Prick with a fork to prevent it from swelling and breaking during cooking. 
  15. Put in a medium oven (180 degrees) for 20 to 30 minutes. 
  16. delicately induce the blade of a knife in the tajine, to check the cooking. 
  17. Remove when the stuffing is well cooked and the surface is golden brown. 



[ ![Tunisian cuisine Malsouka 022](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/malsouka-022_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/malsouka-022.jpg>)
