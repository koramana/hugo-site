---
title: Flan tart
date: '2013-08-13'
categories:
- diverse cuisine
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-au-flan-2-copie-1.jpg
---
![pie-in-blank-2-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-au-flan-2-copie-1.jpg)

##  Flan tart 

Hello everybody, 

the flan pie, or the flaky pastry custard is one of the most delicious pie we love at home. it's even the favorite with my husband after the [ lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee.html>) . 

Personally when he asks for a dessert, I pie him instead of lemon pie, because it takes me less time, hihihihih.   


**Flan tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-au-flan-2-copie-1.jpg)

Recipe type:  pastry, dessert  portions:  12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * puff pastry 
  * 1 L of semi-skimmed milk 
  * 3 eggs 
  * 160 g of sugar 
  * 100 g cornflour 
  * 2 bags of vanilla sugar 
  * 3 teaspoons of vanilla extract 
  * ½ teaspoon vanilla powder (or a vanilla bean) 



**Realization steps**

  1. With an electric whisk, beat the eggs with cornstarch, vanilla extract and 125 ml of milk (taken from the liter of milk of the recipe) in a bowl. 
  2. Pour the remaining milk into a saucepan, with the vanilla sugar, sugar, vanilla powder (or the vanilla pod split in half lengthwise and scraped). 
  3. Bring to a boil. 
  4. Pour boiling vanilla sweet milk over egg mixture while beating (whisk). 
  5. Pour everything back into the pan 
  6. return to very low heat, stirring constantly with a wooden spatula. 
  7. To simmer a few seconds, no more, it is necessary that the preparation is slightly thickened. 
  8. Pour the mixture over the dough (removing the vanilla pod if you put one) 
  9. stuck in the mold and bake for 40 minutes at 180 ° C. Cover with aluminum foil if necessary the last 5 minutes. 
  10. Allow to cool completely before unmolding and cutting. Keep refrigerated. 



![tarte au flan-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-au-Flan-1.jpg)

Merci pour vos commentaires, et merci pour vos visites, 
