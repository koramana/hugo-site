---
title: fig gratin with almonds
date: '2013-08-20'
categories:
- Cupcakes, macaroons, and other pastries
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Petits-gratins-aux-figues-amandes-et-speculoos-1_thumb1.jpg
---
# 

Hello everybody, 

**fig gratin with almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Petits-gratins-aux-figues-amandes-et-speculoos-1_thumb1.jpg)

Recipe type:  dessert  portions:  8  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients**

  * 10 fresh figs 
  * 60 g of almond powder 
  * 1 eggs 
  * 50g of sugar 
  * 100 ml whole liquid cream 
  * 10 speculoos 



**Realization steps**

  1. Whisk the eggs with the sugar. 
  2. When the mixture is frothy add the almond powder and the cream. Beat again. 
  3. Arrange figs quartered at the bottom of each ramekin and cover with almond cream. 
  4. Reduce the powdered speculoos and sprinkle on each ramekin. 
  5. Put in the oven for 20-25 min at 160 ° C while watching the cooking. or do it with your thermomix 


