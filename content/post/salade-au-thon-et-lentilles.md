---
title: Tuna and lentil salad
date: '2013-08-13'
categories:
- panna cotta, flan, and yoghurt
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/25241728.jpg
---
![Tuna and lentil salad](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/25241728.jpg)

##  Tuna and lentil salad 

Hello everybody, 

I put you back one of my old recipe: Tuna and lentil salad, I have to think to redo it to put a new picture, it's a salad that we like at home, in addition to having a very special taste she is very rich, so she can have a very complete meal. 

**Tuna and lentil salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/25241728.jpg)

Recipe type:  salad  portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 3 cases of olive oil 
  * 1 case of lemon juice press 
  * 1 cup of mustard grain 
  * 1 garlic clove finely cut 
  * ½ cac of powdered cumin 
  * ½ cac of ground coriander 
  * 1 small red onion 
  * 2 tomatoes cut into cubes 
  * 400g lentils, cooked and drained 
  * 185 gr of tuna (in drained tin) 
  * 2 cases of chopped coriander 
  * black pepper 



**Realization steps**

  1. prepare your sauce by mixing, olive oil, lemon juice, mustard seeds, garlic, caraway powder, salt according to taste   
(leave at the last minute because the tuna is salty, and the lentils too) 
  2. mix, the tomatoes cut in, the onion cut in, the lentils, add the crumbled tuna, and the coriander chiseled. 
  3. sprinkle with your already prepared sauce. 
  4. your salad is ready, very good, a real treat. 


