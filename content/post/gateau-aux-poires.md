---
title: Pear cake
date: '2013-08-29'
categories:
- crepes, donuts, donuts, sweet waffles
- Algerian cuisine
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gatpoire_thumb.jpg
---
[ ![gatpoire](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gatpoire_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gatpoire_2.jpg>)

Hello everybody, 

I'm the type who always likes my morning milk is well accompanied, but differently to accustom him, I found myself today with "nothing" 

So immediately at my closet, before the awakening of my husband and children (a mom does not rest even the weekend!)  and I'm already starting to see what I can prepare, 

No time to turn on the pc, to see a recipe here or there, and improvisation does its job  in any case I have not been on it, nor everyone at home, because it was a super delicious cake. 

**Pear cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gatpoire3_thumb.jpg)

portions:  8  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients** Caramel: 

  * a little sugar (at random) 
  * a little butter 

for the cake 
  * 3 eggs 
  * 120 gr of sugar 
  * the zest of a lemon 
  * 180 gr of flour 
  * 1 sachet of baking powder 
  * 50 gr of melted butter. 
  * and a box of pear syrup 



**Realization steps**

  1. in a saucepan caramelize a little sugar, while stirring, and add a little butter and pour everything into your mold   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gatpoire1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gatpoire1_thumb.jpg>)
  2. garnish the mold with pears cut into large pieces 
  3. beat the eggs with the sugar, add the zest of a whole lemon, then incorporate the flour and baking powder. 
  4. melt the butter and add it gently while mixing with a spatula. 
  5. pour this mixture over the pears, and bake in a preheated oven at 180 degrees 



[ ![gatpoire2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gatpoire2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/gatpoire2_2.jpg>)
