---
title: Algerian cakes at my readers
date: '2012-08-18'
categories:
- Coffee love of cooking

---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

many of you have visited my blog recently to be able to make and cook Algerian cakes for the 2012 Aid Day. 

and it is with pleasure that I share with you today, the cakes you have chosen, and which you have beautifully done well. 

I am happy to share with you this joy of presenting a beautiful little table, topped with beautiful cakes for this magnificent and wonderful festival of the Aid, 

Sahha Aidkoum. and I am waiting for the pictures of your trays for the Aid on this email: vosessais@amourdecuisine.fr 

[ griweche ](<https://www.amourdecuisine.fr/article-25345475.html>) at Aurelie 

[ griweche ](<https://www.amourdecuisine.fr/article-25345475.html>) at Saida Z 

the [ Mkhabez ](<https://www.amourdecuisine.fr/article-mkhabez-109018263.html>) at Soraya 

[ Horn of gazelle pincer ](<https://www.amourdecuisine.fr/article-corne-de-gazelle-bicolore-pincee-au-miel-60151822.html>) , [ the braid ](<https://www.amourdecuisine.fr/article-la-tresse-61395654.html>) , [ ghribiya has oil ](<https://www.amourdecuisine.fr/article-ghribiya-ta3-ezzit-ghribiya-a-l-huile-57051198.html>) at Hassina 

[ Mchewek ](<https://www.amourdecuisine.fr/article-mchewek-gateaux-algeriens-aux-amandes-88045847.html>) at Saida Z 

[ Griweche ](<https://www.amourdecuisine.fr/article-25345475.html>) at Narimen S 

[ the macaroons ](<https://www.amourdecuisine.fr/article-macarons-a-la-ganache-au-chocolat-blanc-68463799.html>) at Amina's 

[ Arayeches ](<https://www.amourdecuisine.fr/article-25345469.html>) chez Amina 
