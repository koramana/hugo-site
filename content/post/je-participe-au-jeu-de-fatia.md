---
title: I participate in the game of Fatia
date: '2010-05-11'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/05/52938948_p1.jpg
---
![concours_20bis_1_](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/52938948_p1.jpg)

and so I decided to participate with my two following recipes: 

![jardin2](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/jardin2_thumb1.jpg)

the recipe is  [ right here  ](<https://www.amourdecuisine.fr/article-la-recette-du-jardin-fleuri-mon-gateau-d-anniversaire-46564330.html>)

and also this cake 

![sun 023](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/soleil-0231.jpg)

recipe  [ right here  ](<https://www.amourdecuisine.fr/article-36727369.html>)

so here I do not forget the girls when the game will start 

OK 
