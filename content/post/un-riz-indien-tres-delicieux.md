---
title: a very delicious Indian rice
date: '2011-09-30'
categories:
- boissons jus et cocktail sans alcool
- Cuisine détox
- Cuisine saine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/09/biryani-002_thumb.jpg
---
[ ![biryani 002](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/biryani-002_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/09/biryani-002.jpg>)

##  a very delicious Indian rice 

Hello everybody, 

A photo not level .... I know it, but this super good dish I prepared them the day I invited nawel home, must I tell you that this is the photo I did just at the end of cooking the last touch before to put the dish on the table, I did not dare before my guests to play the great photographer, hihihihihi 

I pass you a few small pictures well before they arrive, and well before setting the table, just to share with you what I did: 

[ ![my table 003](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/ma-table-003_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/09/ma-table-003.jpg>)

a very simple salad, but very delicious (I forget the mayonnaise) 

[ ![my table 002](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/ma-table-002_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/09/ma-table-002.jpg>)

#  the tandoori chicken with which I accompany the rice, [ recipe ](<https://www.amourdecuisine.fr/article-poulet-tandoori-85541703.html>)

[ ![tuna fish](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/poisson-au-thon_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/09/poisson-au-thon_2.jpg>)

#  fish-shaped bread with tuna, it's the famous 8, recipe [ right here ](<https://www.amourdecuisine.fr/article-ach-47652304.html>) , to accompany the chorba. 

so all her pictures were taken before the arrival of my guests, so sorry for the quality and the background, hihihihih when we are in a hurry .... 

So, we come back to Indian rice, the recipe comes from a very very good friend, very talented in the kitchen .... she recognizes herself, hihihihih   


**a very delicious Indian rice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/biryani-002_thumb.jpg)

Recipe type:  Indian dish  portions:  6  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * 4 large onions 
  * 3 to 4 chicken legs 
  * 4 fresh tomatoes 
  * 4 cloves of garlic 
  * salt, ½ tsp. a coffee of coriander grain, ½ ca coffee of black peppercorns, 7 grains of green cardamom, and 1 grain of black cardamom, 5 cloves, 1 stick of cinnamon, 3 to 4 bay leaves, ½ ca chopped fresh ginger, ½ cup of cumin in grain. 
  * 150 to 200 ml natural yoghurt 
  * a little bunch of fresh coriander 
  * 6 tablespoons of oil 



**Realization steps**

  1. then you have noticed that I use the grain spices for not having too spicy rice, but if you like your spicy, you can grind the grains. 
  2. cut the onions in pieces and put them in a pot in the oil, and cover, sauté on low heat until the onion is well cooked and transparent. 
  3. add the chicken legs and salt, and cook again while stirring until the chicken turns white. 
  4. add the crushed garlic and let it simmer a little, then add the chopped tomato, and the spices and let it cook always in a closed pot, and stirring from time to time. 
  5. When you start to see the oil rising a little to the surface, add the yoghurt, and stir constantly so that it does not stick to the bottom, and towards the end of the cooking add the fresh coriander, chopped.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/riz-indien_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/09/riz-indien_2.jpg>)
  6. now we return to the rice, take the desired quantity, and rinse it several times with water until the water becomes clear, then put the rice in a pot of cold water, and leave the cheat for almost 30 minutes. 
  7. put another pot on the fire until the water boils, add the salt and pour your rice drained in, when the rice starts to boil, count 2 to 3 min, and remove your rice from the heat, and drained another time. 
  8. take a baking tin, butter well, place rice dedans in it, cover with crumbled chicken, pour over prepared sauce, then cover with rice and take some turmeric or saffron, or even yellow dye in a little water and pour on the top of the rice, leaving white places, cover with aluminum foil and cook for 30 min. 
  9. at the end of the oven, gently stir the rice, to bring up the sauce a little, and serve hot with a little sauce if you have any leftover 



[ ![biryani 003](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/biryani-003_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/09/biryani-003.jpg>)

et bon appétit 
