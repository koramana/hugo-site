---
title: lemon cake
date: '2014-09-03'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-citron-2_thumb.jpg
---
[ ![lemon cake 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-citron-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-citron-2_2.jpg>)

##  lemon cake 

Hello everybody, 

a very easy recipe, frankly, I read a lot of recipes, but it's the easiest of them all. 

so I'll give you the recipe, and I'll put my changes: 

**lemon cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Cake-au-citron-2.CR2_thumb.jpg)

Recipe type:  cake  portions:  8  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 4 eggs 
  * 250 g caster sugar 
  * 1 pinch of fine salt 
  * 100 g of liquid cream. 
  * the zest of 2 lemons 
  * 190 g of flour 
  * 4 g of baking powder (I put ½ teaspoon of baking powder) 
  * 70 g cold melted butter. 
  * frosting: (for my part I made half of the ingredients, because my husband does not like frosting) 
  * 40g of water 
  * 180g sifted icing sugar 
  * 10 g rum, lemon juice 



**Realization steps**

  1. Preheat your oven to 170 ° C. 
  2. Melt the butter in a saucepan and set aside. 
  3. Sift the flour with the baking powder and then zest the lemons. 
  4. With the mixer, put together the eggs and the caster sugar until the mixture doubles in volume. 
  5. Then add the salt, liquid cream, lemon zest, sifted flour with yeast and finally cold melted butter by gently mixing with a whisk. 
  6. Butter and flour a cake mold (19cm long, 8 cm wide and 7 cm high) and garnish with the cake dough. 
  7. Bake and cook for about 1 hour (check the cooking by pressing the blade of a knife, it must come out slightly wet) 
  8. Prepare the ice-cream, Mix the icing sugar with the water and the lemon juice with a whisk. 
  9. When the cake is lukewarm, cover it completely with ice and pour it over it. 
  10. Enjoy this tepid or cold cake. 



[ ![lemon cake by christophe felder.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-cirton-de-christophe-felder.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-cirton-de-christophe-felder.CR2_2.jpg>)
