---
title: Pasta gratin with Bolognese Express
date: '2016-01-25'
categories:
- Algerian cuisine
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/gratin-de-pate-a-la-bolognaise-express-2.jpg
---
[ ![Pasta gratin with Bolognese Express](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/gratin-de-pate-a-la-bolognaise-express-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/gratin-de-pate-a-la-bolognaise-express-2.jpg>)

##  Pasta gratin with Bolognese Express 

Hello everybody, 

pasta gratin with bolognese express, This recipe was not intended to be published on my blog, it was an express recipe that I made with the rest of a [ bolognaise sauce ](<https://www.amourdecuisine.fr/article-sauce-bolognaise-maison-halal-et-sans-vin.html>) that I had in the freezer, and a little bit of [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html>) remained after the preparation of the dinner of the day before. But when I posted the photos of our lunch my son and I, I was surprised that many people were asking for the recipe ... In addition the photos were quickly made with my cellphone. In any case, for those who love express recipes, this pasta gratin with bolognese express is for you. 

**gratin of pate with bolognese express**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/gratin-de-pate-a-la-bolognaise-express-001.jpg)

portions:  4  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * pasta cooked aldenté in salt water 
  * [ bolognaise sauce ](<https://www.amourdecuisine.fr/article-sauce-bolognaise-maison-halal-et-sans-vin.html>)
  * [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html>)
  * mozzarella as needed 



**Realization steps**

  1. mix the pasta with the Bolognese sauce in a salad bowl (do not like me, I mixed in the baking pan or the disastrous state in the picture, hihihihi) 
  2. place the mixture in a gratin dish with a little buttered 
  3. cover the surface with a little béchamel sauce (I had the equivalent of 3 tablespoons) 
  4. then cover the surface with a generous layer of mozzarella and bake for at least 20 minutes. 



[ ![gratin of pate with bolognese express](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/gratin-de-pate-a-la-bolognaise-express.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/gratin-de-pate-a-la-bolognaise-express.jpg>)
