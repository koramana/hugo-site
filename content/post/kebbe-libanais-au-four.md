---
title: Baked kebbé in the oven
date: '2017-07-27'
categories:
- diverse cuisine
tags:
- Lebanese cuisine
- Full Dish
- Ground beef
- Oven cooking
- kebbe
- Oriental cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbe-libanais-au-four-3.jpg
---
![Lebanese kebbe baked 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbe-libanais-au-four-3.jpg)

##  Baked kebbé in the oven 

Hello everybody, 

The first time I had eaten the Lebanese kebbé was just a week after I arrived here in England, my husband's friend invited us for dinner, and his Lebanese wife made some wonderful things for us. I admit that at the sight of the table super well presented, I only waited for the moment to eat everything (not to say devour). 

She had prepared three kinds of kébbé, the fried kébbé, the kébbé in the oven, and the kébbé niyeh (without cooking), so for the last one, which is like a kind of tartare, I succumbed ... I who always liked to eat a little [ merguez ](<https://www.amourdecuisine.fr/article-recette-de-merguez.html>) uncooked, and my mother screaming at me, I can tell you that with the kébbé niyeh, I feasted. 

![Lebanese kebbe baked 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbe-libanais-au-four-5.jpg)

**Baked kebbé in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbe-libanais-au-four-1.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** For the stuffing: 

  * 1 C. tablespoon of ghee 
  * 1 onion, finely chopped 
  * 1 nice handle of pine nuts 
  * 170 gr of minced meat 
  * 1 C. mixed spice coffee (boharat) 
  * 1 C. ground cinnamon 
  * ½ c. ground cardamom 
  * ¼ c. ground nutmeg 
  * ¼ cup raisins / sultanas 
  * 3-4 c. pine nuts, plus extra to decorate the surface 

For the crust: 
  * 160 gr bourgoul (bulgur) 
  * 1 onion, in quarters 
  * 330 gr of minced meat 
  * 2 tbsp. salt 
  * 1½ c. mixed spice coffee (boharat) 
  * 1 C. ground cinnamon 
  * ½ c. ground cardamom 
  * ¼ c. ground nutmeg 
  * water with ice cubes 



**Realization steps**

  1. Wash the bulgur, drain it keeping a bottom of water that will allow the seeds of bulgur to inflate and soften. 
  2. salt, pepper and set aside in a large salad bowl.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulgour-fin.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulgour-fin.jpg>)

prepare the meat stuffing: 
  1. melt the ghee in a skillet over medium heat. 
  2. Add the onion and cook lightly, add the pine nuts and watch the cooking. 
  3. Add the beef and cook, breaking the lumps with the spoon until it starts to change color. 
  4. Add spices (mixture of spices / boharat, cinnamon, cardamom, nutmeg) and season with salt. Continue cooking until the beef is golden brown, about 10 minutes. 
  5. Let cool completely before using.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbe-bil-seniyah.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbe-bil-seniyah.jpg>)

Prepare the crust: 
  1. In a food processor, reduce the onion puree. 
  2. add the minced meat and mix again. 
  3. Add the salt and the rest of the spices, then introduce the bulgur slowly by spoon. 
  4. If the mixture is a little dry, add the ice water, and continue with the addition of bulgur. the paste obtained must stick a little to your hands 
  5. continue mixing until you obtain a homogeneous and supple paste. 

mounting: 
  1. Brush a baking dish generously with olive oil 
  2. Spread the dough at the bottom of this dish, the thickness of Kebbé must not exceed 1 cm. 
  3. Smooth the surface of the Kebbé with a wet hand 
  4. Add the stuffing and spread it over the bottom crust. 
  5. Cover with another layer of kébbé, and leave with cold water, it will allow you to have a sweet kebb in the mouth, and a lighter color. 
  6. Decorate by drawing patterns using a fork or the tip of a knife (optional but traditional step).   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbeh-libanais-au-four.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbeh-libanais-au-four.jpg>)
  7. Sprinkle with olive oil and bake at 200 ° C until golden brown. 
  8. Cut in shares like a pizza and enjoy hot or cold with raw vegetables, a salad, or a [ Lebanese Tabbouleh ](<https://www.amourdecuisine.fr/article-taboule-libanais-salade-variee-au-boulgour.html>) . or serve with [ tzatziki ](<https://www.amourdecuisine.fr/article-tzatziki.html>)



[ ![Lebanese kebbe baked](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbe-libanais-au-four.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/kebbe-libanais-au-four.jpg>)
