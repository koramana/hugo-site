---
title: makrout el koucha in video / semolina cake and dates
date: '2016-08-16'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/makrout-Aid-2016.jpg
---
![makrout Aid 2016](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/makrout-Aid-2016.jpg)

##  makrout el koucha in video / semolina cake and dates 

Hello everybody, 

Makrout el koucha, or oven-baked makrout, which means the oven-shaped diamond cake is an Algerian specialty that keeps its place despite all the other delicious Algerian cakes that pierce well with their beautiful decorations during Algerian festivals. weddings, Eid, circumcision or others. Makrout el koucha remains the favorite, and must be present especially in the beautiful tables of eastern Algeria ... 

Makrout el koucha did not stay in eastern Algeria alone, he found his place gracefully throughout the Algerian territory, and he becomes the pride of the woman who prepares it correctly to have crisp makrouts melt in the mouth stuffed with a generous date stuffing with a cinnamon flavor. 

You were a good number to ask me the recipe for this delicious cake, and here it is done, I share with you this video that I realized hard, because I still do not have a cameraman, or camera women and as I left my camera in England, my little camera drags everywhere to make you a little movie, hihihihi 

**Baked Makrout, Makroud Algerian Cake "مقروط الكوشة"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/makrout-el-koucha.jpg)

Recipe type:  Algerian cake  portions:  100  Prep time:  45 mins  cooking:  60 mins  total:  1 hour 45 mins 

**Ingredients**

  * and its ingredients are: (for a small tray of 40 x 50 cm) 
  * 1200 gr of coarse semolina (not too much fat ok) 
  * 400 gr of ghee (smen, or clarified butter) 
  * 1 little salt 
  * Orange tree Flower water 
  * Water 
  * for the stuffing: 
  * 500 gr of date paste 
  * ½ teaspoons cinnamon 
  * ½ teaspoon of ground cloves 
  * 2 tablespoons of butter 
  * 2 cafe jam (optional, I had put it, because I had a paste of date of poor quality, and so I was afraid that the stuffing becomes hard cooking, I added the fig jam, which will make the dough soft) 
  * Orange tree Flower water. 



**Realization steps**

  1. Work the date paste with the ingredients previously given, until you get a soft ball. Leave aside. 
  2. In a large bowl, pour the semolina, the salt. Mix. 
  3. Make a fountain and pour the melted clarified butter. Mix well so that the butter is well absorbed by the semolina. Let it rest. 
  4. Wet with orange blossom water and water, and mix without working with your fingertips and without kneading. Once you have a compact ball, leave out, and cover well with a cling film. 
  5. take a good amount of the dough, shape a pudding. With your index finger, make a channel in the center in the direction of the length. 
  6. Roll a little roll of date paste and put it in the slot. 
  7. Reassemble the edges of the dough on the dates to cover it all. 
  8. Ride gently again to have a roll of a width of 2 cm in height. 
  9. With an impression mold, press the top of the pudding and flatten slightly to get the pattern on the makroud. 
  10. Cut the lozenge into lozenges and set aside in a baking dish. 



![makrout el koucha 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/makrout-el-koucha-1.jpg)
