---
title: the Mandises
date: '2011-08-22'
categories:
- Cupcakes, macaroons, and other pastries
- cakes and cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/dsc098111.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/dsc098111.jpg)

Hello everybody, 

the days are getting more and more busy, I do not even have time to blog, and you know why? it is the period of preparation of the cakes of the aid, and between the cakes, the preparation of the manger, to take care of the children. 

so, thank you very much my dear Lunetoiles who sent me this recipe for a while and that I did not have the opportunity to publish, so here is the recipe: 

Ingredients for 12 cakes:    


  * 2 eggs 
  * 125 grams of butter 
  * 125 grams of sugar 
  * 1 teaspoon of pure liquid vanilla extract, 
  * 1 teaspoon of pure vanilla extract in beans 
  * 100 ml unsweetened condensed milk 
  * 250 grams of flour 
  * 1 packet of dry yeast 
  * 1 natural yogurt or cottage cheese (I used cottage cheese) 
  * 12 x 4 square of Pralinoise 
  * 100 grams of chocolate chips 

![mandises.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x449/2/42/48/75/Images/)   


** Preparation:  **

Preheat the oven to Th.6 or 180 ° C. 

In a bowl, beat the eggs with the sugar. 

Add the melted butter, then the milk, the yoghurt, the vanilla, the flour, the yeast and half of the chocolate chips. Mix briefly, just to assemble everything. 

Fill the muffin cups previously lined with paper boxes until 3/4, place 4 squares of Pralinoise in the center, cover with the remaining dough and place the remaining chocolate chips. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/dsc098101.jpg)

Bake for 20 minutes. 

Enjoy tepid! 

source: 
