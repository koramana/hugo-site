---
title: hazelnut mchewek
date: '2014-11-08'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
- oriental delicacies, oriental pastries
- houriyat el matbakh- fatafeat tv
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/mchewek-aux-noisettes-1.jpg
---
[ ![hazelnut mchewek](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/mchewek-aux-noisettes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/mchewek-aux-noisettes-1.jpg>)

##  hazelnut mchewek 

Hello everybody, 

The mchewek is one of the most delicious Algerian cakes without gluten, yes you read well without gluten. Usually the mchewek is prepared with almonds, you can see the original recipe of [ almond mchewek ](<https://www.amourdecuisine.fr/article-mchewek-aux-amandes-gateau-algerien.html> "mchewek with almonds, Algerian cake") . 

But this delicious cake was revisited several times, which gave for example: [ mchewek has cashew ](<https://www.amourdecuisine.fr/article-mchewek-a-la-noix-de-cajou.html> "Algerian cake Mchewek with cashew nuts") , [ mchewek has coconut ](<https://www.amourdecuisine.fr/article-mchewek-a-la-noix-de-coco-gateau-sec-algerien.html> "Mchewek has coconut / Algerian dry cakes") , or [ mchewek with sesame seeds ](<https://www.amourdecuisine.fr/article-mchewek-aux-grains-de-sesame-gateau-algerien-economique.html> "Mchewek Sesame Grain / Algerian Cake Economic Video") . By cons against this last recipe contains gluten, ok. 

So we come back to the recipe of the hazel mchewek made by our dear Lunetoiles during one of the celebrations of the Aid ... I hope that the recipe will please you, and do not forget if you realized one of the recipes of my blog , send me the picture or the link on my email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)   


**hazelnut mchewek**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/mchewek-aux-noisettes-2.jpg)

portions:  30 to 40  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 800 gr of finely ground hazelnuts 
  * 400 gr sifted icing sugar 
  * egg white 
  * 1 C. coffee vanilla extract 

For the decoration: 
  * 250 gr of crushed almonds 
  * 1 beaten egg 
  * whole hazelnuts 



**Realization steps**

  1. In a terrine, mix the hazelnuts in powder, icing sugar and vanilla extract. 
  2. Wet with the egg whites until 'to obtain a firm dough. 
  3. Spread the dough into small balls the size of a walnut that you must pass in a beaten egg, then roll them in crushed almonds. 
  4. Garnish the center of the m'cheweks with a whole hazelnut. 
  5. Place the mécheweks in a buttered and floured plate, or covered with baking paper. 
  6. Bake in preheated oven halfway up th. (4-5) 160 ° C, about twenty minutes. 
  7. Place them in boxes after cooling. 



[ ![hazelnut mchewek](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/mchewek-aux-noisettes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/mchewek-aux-noisettes.jpg>)
