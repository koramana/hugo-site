---
title: Mediterranean dish with chicken and shrimps
date: '2016-09-21'
categories:
- Algerian cuisine
- cuisine diverse
- chicken meat recipes (halal)
- fish and seafood recipes
- rice
tags:
- Olive
- Feta cheese
- Greek cuisine
- tomatoes
- sauces
- Tomato sauce
- Fish

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/plat-mediterraneen-au-poulet-et-crevettes-078.jpg
---
[ ![mediterranean dish with chicken and shrimps 078](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/plat-mediterraneen-au-poulet-et-crevettes-078.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/plat-mediterraneen-au-poulet-et-crevettes-078.jpg>)

##  Mediterranean dish with chicken and shrimps 

Hello everybody, 

A little return to summer, especially since there is sunshine today, to share with you this delicious dish that we like a lot at home. This Mediterranean dish with chicken and shrimps, a dish enhanced with the acid and salty taste of [ homemade lemons ](<https://www.amourdecuisine.fr/article-citron-confit.html> "lemon confit salty") , and feta cheese that I like a lot, perfumed during its presentation of some fresh leaves of basil and parsley. 

It's hard for me to take these pictures, because everyone behind me was waiting to be served, and frankly when I'm stressed like that, I can not take pictures that will do justice to the dish. 

You can present this delicious sauce with pasta, fries or potatoes in the oven as you always do at home, or with rice as I did this time. 

And then what with recipes based on shrimp, I want to invite you to participate in our round: 

We are waiting for you on this round my beautiful! 

more 

![mediterranean dish with chicken and shrimp 049](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/plat-mediterraneen-au-poulet-et-crevettes-049.jpg)

**Mediterranean dish with chicken and shrimps**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/plat-mediterraneen-au-poulet-et-crevettes-081.jpg)

portions:  4 to 5  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * 3 c. extra virgin olive soup 
  * 4 to 5 legs of chicken preferable with the skin. 
  * 1 medium onion, finely chopped 
  * 6 medium garlic cloves, finely chopped 
  * 2 boxes of peeled and chopped tomatoes 
  * 1 C. dried oregano 
  * 1 C. dried basil 
  * 1½ tablespoons of salt 
  * ½ lemon confit finely sliced 
  * ½ cup of pitted black olives 
  * 100 gr raw shrimps, shelled and deveined 
  * 20 gr of feta cheese (you can not put it, or put more than 20 gr) 
  * fresh parsley to garnish 
  * 6-8 small fresh basil leaves, to garnish 



**Realization steps**

  1. Season the chicken with salt and freshly ground black pepper. 
  2. In a large baking pan afterwards, put the olive oil to heat. 
  3. Add chicken and cook for 10 to 15 minutes, watching or until golden brown.   
(The chicken will not be fully cooked at this time, but will be finished later in the oven.) 
  4. remove the chicken pieces on a plate, cover and refrigerate for later. 
  5. In the same pan, add chopped onions and cook until tender and translucent, about 6-8 minutes. Add the garlic and cook for a minute. 
  6. Add the tomatoes, dried oregano, dried basil and salt, and cook for 30 minutes until the sauce is thick and fragrant. 
  7. Heat the oven to 200 degrees C. 
  8. Place the chicken pieces, shrimp and lemon pieces in the sauce. 
  9. add pieces of feta cheese and place them between the shrimp and the chicken. 
  10. sprinkle olives on top and bake for 15 to 20 minutes or until sauce is bubbling and chicken is cooked through. 
  11. garnish with parsley and chopped basil leaves. Serve this hot sauce 



![mediterranean dish with chicken and shrimps 056](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/plat-mediterraneen-au-poulet-et-crevettes-056.jpg)

Thank you for your visits and comments. 

Bonne journée. 
