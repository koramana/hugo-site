---
title: Chicken nuggets and rice in white sauce
date: '2015-11-24'
categories:
- Algerian cuisine
- rice
tags:
- Easy recipe
- Algeria
- dishes
- Full Dish
- ramadan 2015
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/Croquettes-de-poulet-et-riz-en-sauce-blanche.jpg
---
[ ![Chicken nuggets and rice in white sauce](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/Croquettes-de-poulet-et-riz-en-sauce-blanche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/Croquettes-de-poulet-et-riz-en-sauce-blanche.jpg>)

##  Chicken nuggets and rice in white sauce 

Hello everybody, 

But when I saw that Nadia posted it, all these memories of a Ramadan in the middle of spring, of my return home after a tiring day at the university, and all the frying that awaited me, hihihihi . Yes, you have to say that in Ramadan you get too much fried food and dishes that go through the frying before you are completely ready. 

My mother left me this stain of frying, because she and my sisters would have prepared all the day, and then what I was all day outside, so it's up to me to do the last minute stains, and especially especially the dishes! !!! I hear the violins of Alfred Hitchcock's movies, hihihihih, yes my friends I did the dishes 30 days out of 30. 

**Chicken nuggets and rice in white sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/Croquettes-de-poulet-et-riz-en-sauce-blanche-001.jpg)

portions:  4  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients**

  * 2 chicken breast 
  * 1 onion 
  * 1 glass of rice 
  * 1 nice handful of green olives 
  * Salt, black pepper, cinnamon and oil 
  * Chopped parsley 
  * 1 teaspoon cornflour 
  * 1 beaten egg 

For breading: 
  * 2 eggs 
  * Flour 
  * breadcrumbs 
  * Oil for frying 



**Realization steps**

  1. Cook chicken breast in hot oil with grated onion, salt and spices, add parsley and let simmer 
  2. Sprinkle with warm water Cover and cook 
  3. In the meantime, cook the rice in salted water and allow to exceed a little of the daily life, to have seeds of rice a little pasty to facilitate the formation of the croquettes 
  4. Boil the olives three times to desalt them 
  5. Cut half of the olives and throw the rest in the sauce 
  6. Dilute the cornflour in a little cold water, then pour it in the sauce to bind it 
  7. remove the chicken from the sauce, and crumble it, add the rice and olives in small pieces, add chopped parsley too and ½ beaten egg 
  8. Form croquettes, pass them through flour, then beaten egg and breadcrumbs. Keep them in the fridge for 1 hour then fry them for 2 minutes in a hot oil bath 
  9. In the serving plate, pour the hot sauce and dress over the croquettes. Decorate with chopped parsley 



[ ![Chicken nuggets and rice in white sauce-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/Croquettes-de-poulet-et-riz-en-sauce-blanche-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/Croquettes-de-poulet-et-riz-en-sauce-blanche-001.jpg>)
