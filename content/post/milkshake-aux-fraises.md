---
title: strawberry milkshake
date: '2016-07-19'
categories:
- juice and cocktail drinks without alcohol
tags:
- Juice
- Drinks
- Milk
- Algeria
- smoothies
- Organic cuisine
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/milk-shake-aux-fraises.jpg
---
[ ![strawberry milkshake](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/milk-shake-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/article-milk-shake-aux-fraises.html/milk-shake-aux-fraises-3>)

##  Strawberry milkshake 

Hello everybody, 

It happens sometimes to wake up early in the morning, not having the desire to take my coffee with milk, especially avoid taking my breakfast with a piece of cake stuffed with calorie ... My solution is always a milk shake, or also called mlikshake, and to make this delight I have fun to empty the fridge, or so to put everything that seems good in the bowl of the blender. 

This time, it is with this pretty box of strawberries a little sour which dragged for a while in the fridge. I also had 1 small handful of cherries in the freezer that I added, which gave the color a little dark milk shake. 

**strawberry milk shake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/milk-shake-aux-fraises-2-294x300.jpg)

portions:  3  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 250 gr of strawberries 
  * 2 glasses of milk (500 ml) 
  * 1 handful of frozen cherries (optional) 
  * 1 tablespoon of fresh cream 
  * honey according to taste. (I prefer it to sugar, but you can add sugar instead) 



**Realization steps**

  1. Wash, plant and mix the strawberries in the blinder bowl 
  2. introduce milk, honey, fresh cream spoon and cherries 
  3. mix again well. Reserve in the refrigerator until use. 
  4. Serve chilled in individual glasses. 



[ ![strawberry milkshake 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/milk-shake-aux-fraises-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-milk-shake-aux-fraises.html/milk-shake-aux-fraises-1-cr2>)
