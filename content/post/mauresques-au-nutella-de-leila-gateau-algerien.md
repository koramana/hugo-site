---
title: Moorish Nutella from Leila Algerian Cake
date: '2011-10-31'
categories:
- salads, salty verrines

---
& Nbsp; today I'm posting you the delicious recipe from one of my adorable readers, leila Saaidia. a very nice recipe she likes to share with you through my blog, thank you very much leila, and I pass you the ingredients: 2 measures of ground almonds 1 measure icing sugar white syrup (250 gr of sugar + 1/2 liter of water) for the stuffing: 1 packet of biscuits small butters 1 large handful of peanut Nutella & nbsp; method: mix ground almonds and sugar add white dye pick up with syrup put side dough hell 

##  Overview of tests 

####  please vote 

**User Rating:** 2.97  (  6  ratings)  0 

today I'm posting you the delicious recipe from one of my adorable readers, leila Saaidia. 

a very nice recipe she likes to share with you through my blog, thank you very much leila, and I pass you the ingredients: 

  * 2 measures of ground almonds 
  * 1 measure icing sugar 
  * white dye 
  * syrup (250 gr of sugar + 1/2 liter of water) 



for the stuffing: 

  * 1 pack of biscuits small butters 
  * 1 big handful of peanuts 
  * Nutella 



the method  : 

  1. mix ground almonds and sugar 
  2. add the white dye 
  3. pick up with the syrup 
  4. put the dice aside 
  5. for stuffing mix crushed cookies and peanuts 
  6. pick it up with Nutella 
  7. keep a ball of almond paste and a ball of Nutella stuffing for decoration 
  8. spread the almond paste and using a cookie cutter shape your cakes 
  9. do the same thing with Nutella stuffing, 
  10. take the cakes, spread a little Nutella, then place the Nutella stuffing on top and cover with another almond paste covered with a little Nutella (so that it sticks well) 
  11. using a flower-shaped cookie cutter, shape two flowers of each color 
  12. stick them with Nutella on your Moorish 
  13. decorate with silvery food flakes 



thank you very much leila for this very beautiful and surely very good recipe 

thank you to everyone for your comments 

thank you for all your visits 

thank you to those who subscribe to the newsletter 

et merci a ceux qui cliquent sur les liens sponsorisés 
