---
title: appetizer / crab bites with parmesan cheese
date: '2013-12-17'
categories:
- diverse cuisine
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bouchees-au-crabe-et-parmesan-021_thumb4.jpg
---
Hello everybody, 

then we come back to the crab !!! ??? hihihiih 

so I had this little idea fridge-free and cupboard, and I made super delicious bites.   


**appetizer / crab bites with parmesan cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bouchees-au-crabe-et-parmesan-021_thumb4.jpg)

Recipe type:  appetizer, aperitif  portions:  24  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 175 gr canned crab drained from its water 
  * 150 ml milk or cream. 
  * 1 small onion 
  * 1 clove of garlic 
  * a little water 
  * 2 eggs 
  * 55 gr grated Parmesan cheese 
  * 2 tablespoons finely chopped parsley 
  * 1 pinch of grated nutmeg 
  * salt and black pepper 
  * 1 tablespoon of olive oil 
  * 24 uncooked flight pieces. 



**Realization steps**

  1. preheat the oven to 150 degrees C 
  2. chop the onion finely and fry in a little oil, do not brown. 
  3. add the crushed garlic, and sauté for 30 seconds, then add the equivalent of 2 tbsp water, and cook well. 
  4. beat the eggs, add the cream or milk, mix 
  5. then add the piece of crab, the chopped parsley, the onion and garlic mixture, the grated parmesan cheese. 
  6. season with salt, pepper and nutmeg. 
  7. place the uncooked wind flights on a baking tray. 
  8. fill them with this mixture. 
  9. bake for 20 to 30 minutes until the dough swells well, and the mixture turns a beautiful color. 



You can see the video of preparation: 

{{< youtube teRF8H8T6eM >}} 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
