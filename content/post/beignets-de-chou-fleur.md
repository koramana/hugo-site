---
title: cauliflower donuts
date: '2011-12-01'
categories:
- cuisine indienne
- Cuisine par pays
- Plats et recettes salees
- recettes a la viande de poulet ( halal)
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/beignet-de-chou-fleur-1_thumb.jpg
---
[ ![cauliflower donut 1](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/beignet-de-chou-fleur-1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/beignet-de-chou-fleur-1.jpg>)

#  cauliflower donut 

Hello everybody, 

my husband loves cauliflower donuts a lot, it's not my case at all, nor children's, I prefer them in white sauce: [ cauliflower in white sauce ](<https://www.amourdecuisine.fr/article-chou-fleur-en-sauce-blanche.html>) . 

in any case the donuts is very very easy to do, it is just enough to tolerate a little smell at home (my children, did not stop to claim: but what's that mom? it smells yukkkkkkkkkkkkkkki .... hihihihi 

I present you the simplified recipe that my husband loves, I always told you that it is difficult, well donuts, it will be without flour .... eh yes.   


**cauliflower donuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/beignet-de-chou-fleur-1_thumb.jpg)

Recipe type:  appetizer  portions:  4  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * A cauliflower 
  * thyme 
  * vinegar 
  * 2 eggs 
  * 1 clove of garlic 
  * pepper powder, salt, coriander powder 
  * oil for frying 



**Realization steps**

  1. clean the cauliflower, and cut it into bunches. 
  2. Then dip them in a pan of cold water, in which the branch of thyme is stripped and add the salt and the vinegar (it absorbs a little the smell) 
  3. Bake for 8 to 10 minutes over medium heat. 
  4. Drain and let cool. 
  5. meanwhile, beat the eggs with the spices mentioned 
  6. add the garlic crushed 
  7. Heat oil in a skillet. 
  8. Dip the bouquets of cauliflower in the eggs, remove and fry until they are golden brown. 
  9. Serve cauliflower fritters with fries or a sauce of your choice. 



[ ![cauliflower donut](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/beignet-de-chou-fleur_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/12/beignet-de-chou-fleur.jpg>)
