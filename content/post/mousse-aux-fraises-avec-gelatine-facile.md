---
title: strawberry mousse with gelatin, easy
date: '2015-04-10'
categories:
- dessert, crumbles et barres
- verrines sucrees
tags:
- Bavarian
- desserts
- Chantilly
- verrines
- Desserts And Sweets
- healthy
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/mousse-aux-fraises_2.jpg
---
##  strawberry mousse with gelatin, easy 

Hello everybody, 

As it is the season of strawberries, I share with you this nice recipe of foam with strawberry creamy, frothy, light, and super melting in the mouth mouth, with this layer of jelly which gives a little crunch and which enhances the taste strawberries, this fresh strawberry mousse and easy to make, is a real treat. 

so without delay we go to the recipe  strawberry mousse 

**strawberry mousse with gelatin, easy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/mousse-aux-fraises_2.jpg)

Recipe type:  sweet verinne  portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 400 gr of strawberries 
  * 120 gr of icing sugar 
  * 2 sheets of gelatin 
  * 50 cl of very cold liquid cream 
  * 1 cup of vanilla coffee 
  * 1 box of strawberry jelly 
  * 5 strawberries cut in slices 



**Realization steps**

  1. Rinse and cut strawberries 
  2. Mix with half of the icing sugar and vanilla 
  3. Soften the gelatin in a bowl of cold water for 10 minutes and wring it in your hands 
  4. Put it in a saucepan with 2 spoonfuls of strawberry purée 
  5. Melt over low heat and add this mixture to the remaining mashed potatoes. 
  6. Whip the cream very cold in chantilly with the remaining sugar and stir in the strawberry purée, mix well together 
  7. Put in 6 small verrines or ramekins 
  8. prepare the jelly as indicated on the jelly box 
  9. decorate your verrines with strawberries cut in a slice, pour over the jelly 
  10. and put them in the refrigerator 


