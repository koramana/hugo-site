---
title: Tuna bricks, easy and quick recipe
date: '2015-06-20'
categories:
- Algerian cuisine
- Dishes and salty recipes
tags:
- Ramadan 2015
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/bricks-au-thon-009.CR2_.jpg
---
![brigs tuna au-009.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/bricks-au-thon-009.CR2_.jpg)

##  Tuna bricks, easy and quick recipe 

Hello everybody, 

These tuna bricks are the easiest and quickest recipe you can make, it's the bricks I like most. Why? because no need to complicate life, we open the tuna box, we put in a bowl, we add the other ingredients, we roll in the leaves of bricks, we fry, and voila, a recipe made in almost 30 minutes .... 

you like, so keep this recipe under your elbow, it will serve you a lot, when you are in a hurry and want to impress people around you, presenting a well-stocked table. 

the recipe in Arabic: 

**Tuna bricks, easy and quick recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/bricks-au-thon-021.CR2_1.jpg)

portions:  4  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * Brick leaves, I had the special samossa sheets, so already shaped long rectangle) 
  * 1 can of tuna in oil. 
  * A few sprigs of chopped parsley. 
  * 1 egg. 
  * Grated Emmental or any other cheese. 
  * 1 handful of pitted green olives. 
  * 1 pinch of salt. 
  * ¼ cup of black pepper. 
  * ¼ teaspoon ginger powder. 
  * Oil for frying. 



**Realization steps**

  1. place the drained tuna in its oil in a salad bowl. 
  2. add chopped parsley, sliced ​​green olives, grated cheese, and egg. 
  3. Sprinkle with salt, black pepper, and ginger. 
  4. incorporate everything together 
  5. If you have round bricks, cut them in half, and bend one of the sides to have a kind of long rectangle. 
  6. place some of the tuna stuffing in the end of the rectangle, and fold to form a beautiful triangle. 
  7. paste the tip with a little egg white 
  8. continue until you have used the joke. 
  9. heat the oil in a shallow pan or fryer. 
  10. plunge the bricks in and cook, until they turn a beautiful golden color 
  11. place the cooked bricks on sopalin papers to reduce the cooking oil. 
  12. serve with fillet of lemon juice, if you like this tangy taste. 


