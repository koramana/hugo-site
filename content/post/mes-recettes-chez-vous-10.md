---
title: my recipes at home 10
date: '2011-05-28'
categories:
- juice and cocktail drinks without alcohol
- Cuisine saine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/05/SAM_09251.jpg
---
![https://www.amourdecuisine.fr/wp-content/uploads/2011/05/SAM_09251.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/SAM_09251.jpg)

![https://www.amourdecuisine.fr/wp-content/uploads/2011/05/648876361.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/648876361.jpg)

[ apple tartlets ](<https://www.amourdecuisine.fr/article-35889018.html>) in [ oumsheerazade ](<http://pointe2douceur.canalblog.com/archives/2011/05/21/17966018.html>)

[ donuts ](<https://www.amourdecuisine.fr/article-donuts-faciles-et-delicieux-68158130.html>) at Naima B 

[ stuffed chicken ](<https://www.amourdecuisine.fr/article-poulet-farci-62609585.html>) at Samia M 

[ Indian style chicken ](<https://www.amourdecuisine.fr/article-puree-au-poulet-a-la-sauce-indienne-64105296.html>) at Samia 

[ spice rice ](<https://www.amourdecuisine.fr/article-riz-epice-aux-petits-pois-et-poulet-70463803.html>) at Samia M 

[ lemon squares ](<https://www.amourdecuisine.fr/article-carrees-citronnes-46031497.html>) at Samia M 

[ Brie quiche ](<https://www.amourdecuisine.fr/article-25720989.html>) at Hassana E 

[ lemon squares ](<https://www.amourdecuisine.fr/article-carrees-citronnes-46031497.html>) at Hassana E 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/Photo-2147-copie-11.jpg)

[ Yogurt cake ](<https://www.amourdecuisine.fr/article-ben-10-le-gateau-d-anniversaire-de-rayan-54691985.html>) in [ Mounia ](<http://cuisinenounou.over-blog.com/article-joyeux-anniversaire-ismail-74277777.html>)

![](https://www.amourdecuisine.fr//import/http://storage.canalblog.com/49/95/820201/)

[ Chicken couscous ](<https://www.amourdecuisine.fr/article-38522177.html>) in [ Sissi ](<http://chezssissi.canalblog.com/archives/2011/02/08/20337871.html>)

![](https://www.amourdecuisine.fr//import/http://storage.canalblog.com/71/36/820201/)

![](https://www.amourdecuisine.fr//import/http://storage.canalblog.com/43/63/725423/)

So, if you too, like these beautiful readers and bloggers, you have tried one of my recipes, do not forget to send me the picture of the recipe 

on this email: 

amour2cuisine@gmail.com 

good night 
