---
title: homemade tahini
date: '2017-05-02'
categories:
- diverse cuisine
- Cuisine by country
- dips and sauces
- basic pastry recipes
tags:
- Based
- Fast Food
- salads
- sauces
- spread
- Dips
- Cooking on video

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/tahini-fait-maison-1.jpg
---
![homemade tahini 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/tahini-fait-maison-1.jpg)

##  homemade tahini 

Hello everybody, 

here is the simple and easy homemade tahini recipe that many of you would ask me. It is true that I often use tahini full of my recipes and sauces, as in the recipe of [ homemade hummus ](<https://www.amourdecuisine.fr/article-houmous-fait-maison.html>) . 

Generally I use tahini trade, especially that I find organic for not too expensive. But when sometimes I need tahini for a recipe and I do not have time to buy it, I make this recipe super simple and easy. 

On this recipe I use the most simplified version with oil of table (sunflower here) not too strong in smell and in taste, it is always preferable to use a neutral oil to savor the distinct taste of the seeds roasted sesame seeds. 

**homemade tahini**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/tahini-fait-maison.jpg)

**Ingredients**

  * 280 gr of sesame seeds 
  * 4 to 5 c. of neutral oil (I use refined and odorless sunflower oil) 
  * 1 pinch of salt. 



**Realization steps**

  1. roast the sesame seeds over medium heat, to give them a beautiful golden color. 
  2. let cool, then place the sesame seeds in the blinder bowl. 
  3. reduce to a fine powder. 
  4. add the oil and mix again to have a liquid dough. 
  5. keep in a jar tightly closed until use 


