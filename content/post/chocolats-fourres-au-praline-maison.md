---
title: chocolates stuffed with homemade praline
date: '2015-12-09'
categories:
- dessert, crumbles and bars
- basic pastry recipes
- recettes sucrees
tags:
- Confectionery
- Gourmet Gifts
- Delicacies
- delicacies
- Easy recipe
- Without cooking
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-fourr%C3%A9-a-la-pralin%C3%A9.jpg
---
[ ![chocolate stuffed with praline](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-fourr%C3%A9-a-la-pralin%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-fourr%C3%A9-a-la-pate-de-pralin%C3%A9.jpg>)

##  chocolates stuffed with homemade praline 

Hello everybody, 

Another recipe of stuffed chocolates, frankly I am under the spell of this shiny shell ... I thank Tema for these shares and these explanations, because if you want to have a chocolate as bright as the one, we must respect the [ chocolate tempering ](<https://www.amourdecuisine.fr/article-comment-temperer-le-chocolat.html>) I already have to think about buying a new thermometer probe, if I want to make these little chocolates stuffed with homemade praline paste ... 

**chocolates stuffed with homemade praline**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-temp%C3%A9r%C3%A9.jpg)

portions:  25  Prep time:  60 mins  cooking:  10 mins  total:  1 hour 10 mins 

**Ingredients**

  * 300 g of grated chocolate (chocolate with black choices, milk, white ...) 
  * 1 beautiful amount of [ homemade praline paste ](<https://www.amourdecuisine.fr/article-pate-de-praline-maison.html>)
  * small silicone molds. 



**Realization steps**

  1. First, melt the chocolate in a bain marrie, respecting the [ tempering curves. ](<https://www.amourdecuisine.fr/article-comment-temperer-le-chocolat.html>)

to make the hulls there are two ways: 
  1. either fill the molds with chocolate and then return them to a grid to empty the excess; scrape with a spatula and reserve in a cool place. 
  2. either brush the walls of the patterns with a brush, allow to harden. Put another layer to strengthen them 
  3. Using a spoon or pocket fill the chocolate with homemade praline paste without going to the edge.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pate-pralin%C3%A9-amande.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pate-pralin%C3%A9-amande.jpg>)
  4. Cover with the remaining chocolate and gently pat the pan to remove air bubbles and smooth the surface. Scrape another time with the spatula and return to the refrigerator. 
  5. After cooling, and after having removed the filled chocolates from the molds, file the periphery of the chocolates with the rasp to have perfectly sharp edges. 



[ ![chocolate filled with praline paste](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-fourr%C3%A9-a-la-pate-de-pralin%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chocolat-fourr%C3%A9-a-la-pate-de-pralin%C3%A9.jpg>)
