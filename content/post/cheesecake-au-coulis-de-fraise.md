---
title: cheesecake with strawberry coulis
date: '2011-07-09'
categories:
- juice and cocktail drinks without alcohol
- Cuisine saine

---
hello everyone, I really forget when I made this recipe, according to the date on my pc, I did it around March 17, 2011, well, I totally forgot about it the recipe, fortunately it was taken from my favorite book, otherwise I will not have you post that photos. in any case happy that I have not put my book in my luggage, if not, the recipe would have remained for still how long before being posted: so the ingredients: 125 gr of biscuits (digestive for me) 100 grs candied fruit (orange & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

I really forgot when I made this recipe, according to the date on my pc, I did it around March 17, 2011, well, I totally forgot the recipe, fortunately she was taken from my favorite book, otherwise I will only post you the pictures. 

in any case happy that I have not yet put my book in my luggage, otherwise, the recipe would have remained for still how long before being posted: 

so the ingredients: 

  * 125 gr of biscuits (digestive for me) 
  * 100 grams of candied fruit (orange for me) 
  * 65 gr of melted butter 
  * 150 ml of fresh cream 
  * 575 gr of ricotta cheese (replace with any other cheese) 
  * 100 gr of sugar 
  * vanilla 
  * 2 large eggs 
  * 225 gr strawberries 
  * 25 to 50 grams of sugar 
  * zest and juice of an orange 



method of preparation: 

  1. preheat the oven to 170 degrees C. 
  2. cover the bottom of a mold with a removable base of 20 cm diameter, with baking paper 
  3. place the crushed cookies and the candied fruit in the blinder, add the 50 grs of melted butter and mix 
  4. pour it all into the mold and pile up gently (do not over-pile or the base will be too hard) 
  5. always in the blinder, mix the cream, the ricotta cheese, the sugar, the vanilla and the eggs 
  6. add the remaining melted butter, and mix again for a few seconds. 
  7. pour the mixture on the basis of biscuits 
  8. place all in the preheated oven, and cook for almost 1 hour. until the cake's turn is golden, but the heart of the cake remains quivering. 
  9. turn off the oven, and let the cheese cake cool in it. 
  10. place in the fridge for at least 8 hours 
  11. wash the strawberries, place them in the blinder with 25 grs of sugar, the juice and the orange zest 
  12. all well, add the rest of the sugar, according to your taste, not to oblige all the quantity. 
  13. go to a Chinese to remove the seeds, and put in the fridge 
  14. present your cheese cake with this delicious coulis, and enjoy 



thank you for your visits and comments, and remember, if you have tried one of my recipes, send me your photos on my email: [ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

and if this is your first visit on my blog, subscribe to the newsletter, to be aware of any new publication 

merci 
