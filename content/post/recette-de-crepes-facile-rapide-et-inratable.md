---
title: easy, fast and unbreakable pancake recipe
date: '2014-01-01'
categories:
- bakery
- Unclassified
- bread, traditional bread, cake
- pizzas / quiches / pies and sandwiches
- salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/crepes-aux-oranges-2_thumb.jpg
---
[ ![pancakes-with-orange-2_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/crepes-aux-oranges-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/crepes-aux-oranges-2_thumb.jpg>)

##  easy, fast and unbreakable pancake recipe 

Hello everybody, 

You want an easy, fast and unbreakable pancake recipe? Here is one! my children do not stop asking me to make them pancakes this week, and by this cold duck, and this wind impossible, and because today everything is closed, and we can not go anywhere, eh Well to comfort us we enjoyed these delicious pancakes with orange taste at breakfast, and yummmmmmmmmmmmmmi, it was good. 

a simple and easy recipe here: 

**easy, fast and unbreakable pancake recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/crepes-a-l-orange_thumb.jpg)

Recipe type:  basic recipe  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 2 eggs 
  * 35 g of sugar 
  * 150 g flour 
  * 400 ml of milk 
  * 50 ml of orange juice 
  * the zest of an orange 
  * 50 g of butter 
  * 2 tablespoons of oil 



**Realization steps**

  1. In a salad bowl put the eggs with the sugar 
  2. Add flour, orange peel, and butter to pieces 
  3. mix well and add the milk gradually 
  4. then add the orange juice (if it's canned juice, reduce the sugar) 
  5. add oil, mix again. 
  6. cover with plastic wrap and let stand for 2 hours. 
  7. Cook the pancakes in an anti-adhesive pan. 



[ ![Crepes-a-l-orange-2_thumb_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/crepes-a-l-orange-2_thumb_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/crepes-a-l-orange-2_thumb_1.jpg>)

enjoy with a spread, such as [ spread with gingerbread ](<https://www.amourdecuisine.fr/article-pate-a-tartiner-au-pain-d-epice-98437586.html>) , or with [ applesauce ](<https://www.amourdecuisine.fr/article-compote-de-pomme-98542533.html>) , or bake a cake with these delicious pancakes 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
