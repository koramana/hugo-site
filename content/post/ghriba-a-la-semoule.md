---
title: ghriba with semolina
date: '2017-01-25'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya
tags:
- To taste
- desserts
- Ramadan
- Gateau Aid
- delicacies
- Algerian cakes
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/01/ghriba-%C3%A0-la-semoule-2.jpg
---
![ghriba with semolina 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/ghriba-%C3%A0-la-semoule-2.jpg)

##  ghriba with semolina 

Hello everybody, 

I remember that the first time I had made the ghriba semolina, or as we call it home: the ghribia semolina, I was just 16 years old. I had found the recipe on a newspaper that my father had bought. 

La recette était un réel délice, j’avais coupé le bout du journal pour coller la recette sur mon carnet de recette. On avait refait ensuite la recette à plusieurs reprises, c’était un délice à chaque fois. Récemment une de mes lectrices m’avait demandé la recette, je savais que j’avais la recette, mais impossible de retrouver mon carnet de recette! Alors je suis allée chercher sur youtube pour trouver la recette qui ressemblait le plus à notre recette… et j’ai trouvé une recette qui y ressemble un peu, seulement je me souviens que notre recette ne contenait pas de noix de coco… En tout cas, attendez la recette sans noix de coco, dès que je retrouve mon petit carnet 😉 

For you the video I made: 

**ghriba with semolina**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/ghriba-%C3%A0-la-semoule.jpg)

portions:  20  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 200 gr of fine semolina 
  * 50 gr of coconut 
  * 2 eggs 
  * 80 gr of powdered sugar 
  * 80 gr of flour 
  * 100 ml of table oil 
  * 1 C. coffee baking powder 
  * 1 C. vanilla sugar 

decoration: 
  * 2 tbsp. tablespoons sugar 
  * 2 tbsp. coconut 
  * 2 tbsp. orange blossom water 
  * almonds 



**Realization steps**

  1. whisk eggs, sugar and vanilla sugar with a manual whisk 
  2. add the oil and continue whisking. 
  3. introduce the semolina, then the coconut and continue to mix 
  4. add the flour and the yeast, mix again you will have a paste a little sticky, let rest at least 10 minutes to allow the semolina to absorb liquids. 
  5. take the dough after resting, and shape small balls the size of a walnut (wet your hands with orange blossom water so that the dough does not stick) 
  6. coat them in the sugar and coconut mixture. 
  7. garnish with an almond. 
  8. place to cook in a preheated oven at 180 ° C between 10 and 15 minutes (watch) 
  9. keep the cookies in an airtight container 



![ghriba with semolina 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/ghriba-%C3%A0-la-semoule-1.jpg)
