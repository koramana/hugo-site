---
title: ghribia, leblabi de houriat el matbakh
date: '2013-11-08'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/ghribia-a-la-farine-de-pois-chiche.jpg
---
[ ![ghribia-a-la-flour-de-chickpea](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/ghribia-a-la-farine-de-pois-chiche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/ghribia-a-la-farine-de-pois-chiche.jpg>)

##  ghribia, leblabi de houriat el matbakh 

Hello everybody, 

here is the most delicious ghribia you can prepare, a chickpea ghribia a [ dry cake ](<https://www.amourdecuisine.fr/categorie-11814728.html>) and [ sand ](<https://www.amourdecuisine.fr/categorie-12344749.html>) at the same time.When I went to Algeria, I took with me **smen** , clarified butter, because I like the Algerian smen, I think it is a thousand times better than the smen from here that melts immediately and gives water, and suddenly it did not succeed too much cakes such as ghribia, who likes the smen remains creamy to give a nice mousse, and the first thing I did with it is the ghribia of [ Houriyat el matbakh ](<https://www.amourdecuisine.fr/categorie-11131465.html>) . 

**ghribia, leblabi de houriat el matbakh**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ghribia-a-la-farine-de-pois-chiche_thumb.jpg)

Recipe type:  Algerian cake  portions:  50  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 130 g of soft butter (I put some smen) 
  * 80 g caster sugar 
  * 60 g of coconut (not put) 
  * 50 g of cornstarch 
  * 1 eggs 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of yeast 
  * ¼ teaspoon of salt 
  * 170 gr of flour 
  * 80 g grilled chickpea flour 



**Realization steps**

  1. Sift white flour and chickpea flour with starch, salt and baking powder 
  2. whip smen and sugar with vanilla for a creamy mousse 
  3. Add the egg and continue whisking 
  4. Add the coconut and then sift dry ingredients 
  5. pick up to form the dough 
  6. form a boudin, use a pincer to decorate it well, 
  7. cut small squares, which you place on a floured tray in the oven 
  8. Bake in preheated oven at 180 ° C for 15-20 minutes depending on the type of oven 
  9. you can decorate with icing sugar 
  10. keep in a hermitic box. and enjoy with a good tea 



[ ![ghoriba, ghriba, love of cooking, dry cakes 2012](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ghoriba-ghriba-amour-de-cuisine-gateau-sec-2012_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ghoriba-ghriba-amour-de-cuisine-gateau-sec-2012_2.jpg>)

they are good the [ Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages.html>) . discover even more recipes from the [ Algerian cuisine ](<https://www.amourdecuisine.fr/article-la-cuisine-algerienne-en-photos.html>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et cochez les deux cases. 
