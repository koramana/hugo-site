---
title: strawberry pudding as a strawberry
date: '2015-06-03'
categories:
- dessert, crumbles and bars
- panna cotta, flan, et yaourt
- recettes sucrees
- sweet verrines
tags:
- verrines
- Pastry
- Cakes
- desserts
- Ramadan 2015
- Ramadan
- flan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/pudding-aux-fraises-faux-fraisier.jpg
---
[ ![strawberry pudding - false strawberry](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/pudding-aux-fraises-faux-fraisier.jpg) ](<https://www.amourdecuisine.fr/article-pudding-aux-fraises-comme-un-fraisier.html/pudding-aux-fraises-faux-fraisier>)

##  Strawberry pudding as a strawberry 

Hello everybody, 

I love strawberries, and when Sarah from the blog, sweet salty has launched the theme of the 6th round of the little game: [ recipes around an ingredient # 6: strawberries ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-6-la-fraise.html>) I did not hesitate to make several recipes ... I did not know which to choose for the game ... and in fact it is my daughter who made the choice in the end ... She preferred strawberry pudding verrines, that I 'call the false strawberry ... 

To be honest, I'm not very happy with the photos, because the day I made them, was so rainy and cloudy, that trasportant photos of my camera to the pc, I was all enragee the quality of photos ... all dull and all gray .... pity these photos do not heal too much this delight. Because these verrines strawberry pudding as a strawberry are to fall. 

[ ![strawberry pudding as a strawberry-a](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/pudding-de-fraises-comme-un-fraisier-a.jpg) ](<https://www.amourdecuisine.fr/article-pudding-aux-fraises-comme-un-fraisier.html/pudding-de-fraises-comme-un-fraisier-a>)   


**strawberry pudding as a strawberry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/pudding-aux-fraises-21.jpg)

portions:  6  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-inratable.html>) homemade 

for pastry cream: 
  * 300 ml of milk 
  * 4 to 5 cases of sugar 
  * 30 gr of cornflour 
  * 2 eggs 
  * strawberry aroma 
  * strawberries 
  * Orange juice 
  * almond paste 



**Realization steps**

  1. prepare the sponge cake and after cooling, cut in 3 according to the height, and make circles according to the diameter of your verrines 

Prepare the strawberry pudding: (you can replace it with the one of the trade) 
  1. In a bowl, mix the eggs, sugar and strawberry aroma. 
  2. Add the cornstarch and mix well. 
  3. Boil the milk and pour over the egg / sugar / cornflour mixture. 
  4. put the mixture back in the pan, and cook over low heat while stirring until thickened, let cool a little, the time to prepare the verrines 

mounting: 
  1. place in the bottom of the verrine a disc of sponge cake 
  2. soak it with orange juice 
  3. decorate the round strawberry verrines, washed hulled and cut in half 
  4. gently pour the pudding over it. 
  5. cover with another genoise disc, soak it with orange juice 
  6. cover with a disc of almond paste colored green 
  7. decorate according to your taste. 



Note I let the verrines cool before covering with the disc of marzipan, and make the decoration. [ ![strawberry pudding verrines with almond paste -a](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/verrines-de-pudding-aux-fraises-a-la-pate-damande-a.jpg) ](<https://www.amourdecuisine.fr/article-pudding-aux-fraises-comme-un-fraisier.html/verrines-de-pudding-aux-fraises-a-la-pate-damande-a>)

[ ![strawberry pudding 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/pudding-aux-fraises-3.jpg) ](<https://www.amourdecuisine.fr/article-pudding-aux-fraises-comme-un-fraisier.html/pudding-aux-fraises-3>)

Previous rounds  : 

Les participantes au jeu: 
