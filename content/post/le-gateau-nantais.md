---
title: the Nantes cake
date: '2015-11-05'
categories:
- cakes and cakes
- sweet recipes
tags:
- Fruits
- desserts
- To taste
- Breakfast
- Soft
- Almond
- Fluffy cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gateau-nantais.jpg
---
[ ![Nantais cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gateau-nantais.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gateau-nantais-2.CR2_.jpg>)

##  the Nantes cake 

Hello everybody, 

The almond cakes are always appreciated at home, and especially at my husband's, all cake with almonds, he will eat the big part ... The cake Nantes is the cake he asks me most often, when he do not ask for a mouskoutchou, or a rolled cake. 

But he will insist on a very important thing: "do not put me fondant over" ... And on this point he is not alone, my two boys also join him, except for my daughter who has not problem with the cakes of his mom, hihihih ... I admit that it is one of the causes that gave me the chance to publish the recipe of the cake nantais before part on my blog ... by the way this time if I begged him so that I could take photos and publish the cake, he then allowed me to put the fondant based on the sweetener .... Obviously I found myself short of sweetener, and to hide this little lack I sprinkled my cake of icing sugar, to give a more beautiful appearance ... 

In any case, in my opinion, even without this layer of fondant, this cake is super delicious .... and comparing my recipe to the one I found on the net, I think they put sugar in this cake !!!!, we do not need all this sugar, is it not? Finally, I do not know the recipe has orgine contains rum, is this alcohol will minimize the taste of sugar ... I do not know too much ... for my part I do without this drink, and my cake is super delicious.   


**the Nantes cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gateau-nantais-2.CR2_.jpg)

portions:  12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 280 g caster sugar 
  * 200 g of butter 
  * 1 pinch of salt 
  * 6 whole eggs 
  * 200 g of almond powder 
  * ½ cup of almond extract 
  * 50 g of flour 
  * 1 tablespoon of lemon marmalade 

basis: 
  * 100 gr of icing sugar (sweetener for me) 
  * 1 teaspoon of lemon juice 
  * 1 little orange blossom water 



**Realization steps**

  1. Preheat the oven to 180 ° 
  2. beat the sugar with the butter at room temperature using a whisk to obtain a creamy mixture. 
  3. Add the eggs one at a time, being careful to whip between each addition. 
  4. Add almond powder, almond extract and lemon marmalade 
  5. Whisk again for at least 5 minutes to aerate the mixture. 
  6. Stir the flour into a smooth and even mixture. 
  7. Pour into a nearly 22 cm baking pan lined with baking paper. 
  8. Bake for at least 40 min (done the dry tip test) 
  9. unmold and cool on a rack 
  10. Prepare your icing and cover the cake with it. 
  11. leave the frosting well dried before eating 



[ ![Nantes cake 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gateau-nantais-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/gateau-nantais-2.CR2_.jpg>)
