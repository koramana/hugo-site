---
title: Inratable genoise ultra soft
date: '2016-11-13'
categories:
- basic pastry recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/genoise-facile-poids-de-plume_thumb1.jpg
---
[ ![Genoese-easy-to-weight-plume_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/genoise-facile-poids-de-plume_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/genoise-facile-poids-de-plume_thumb1.jpg>)

##  Inratable genoise ultra soft 

Hello everybody, 

a genesis unratable ultra soft for you, are you starters? This cake always appreciated to be the base of [ birthday cake ](<https://www.amourdecuisine.fr/categorie-11700499.html>) , or just a [ cake ](<https://www.amourdecuisine.fr/categorie-10678925.html>) that we can then decorate according to our tastes and desires. 

for my part, I always used the method that I had seen on blogs and in some books for the realization of my [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-creme-mousseline-103155968.html>) inratable ultra soft. 

But last time I saw, another method on the blog of **appetizers** . and I really like this version. Who says love, said try, so here I am at the act and I really love this method of preparing the **Genoese** . And you what is your method?   


**Inratable genoise ultra soft**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/genoise-poids-de-plume-080_thumb1.jpg)

Recipe type:  basic recipe  portions:  8  Prep time:  20 mins  cooking:  16 mins  total:  36 mins 

**Ingredients**

  * 4 eggs 
  * 120 g of sugar 
  * 120 g of flour 
  * ½ sachet of baking powder 



**Realization steps**

  1. Separate whites from yellows, 
  2. mix the yeast with the flour. 
  3. In the bowl of the robot (for me it was with a normal whisk in a bowl), place the egg whites and put them in snow. 
  4. When they start to froth, add the sugar and whip until a shiny and firm meringue is obtained. 
  5. Immediately add the egg yolks and the flour. Do not let the robot run too long. 
  6. Pour the dough into a buttered and floured baking tin or silicone (I used a removable base mold, it was 25 cm in diameter, next time I will use a mold of 20 cm in diameter to have a cake more above) 
  7. put in the oven for 20 minutes at 180 ° C (th.6) (cooking in my oven has only lasted 16 minutes, so check the cooking time according to the capacity of your oven). 
  8. Unmold and let cool on a wire rack. 
  9. to finish the cake, I cut in two (the cake was not too high, but a little wide anyway) I stuffed the cake with a [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline.html> "muslin cream \(ideal for strawberry\)") , and decorate with a [ chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat.html> "chocolate ganache") . 


