---
title: potato and zucchini pie
date: '2012-09-02'
categories:
- cuisine algerienne
- ramadan recipe
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/tarte-courgette-et-pomme-de-terre-2_thumb.jpg
---
I really liked the recipe, but I could not make it because I was too lazy to make puff pastry, and I did not find the commercial puff pastry I'm used to. So I thank Lunetoiles who made the recipe and shared these photos with us. 

**potato and zucchini pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/tarte-courgette-et-pomme-de-terre-2_thumb.jpg)

**Ingredients**

  * A small zucchini 
  * 3 small firm-flesh potatoes 
  * 200 gr of fresh goat cheese with pasteurized milk 
  * 2 cloves garlic 
  * Thyme 
  * Salt, pepper 
  * A puff pastry 
  * Comté cheese 



**Realization steps**

  1. Cut the zucchini into 3 large pieces. 
  2. Cut each section in half lengthwise, then cut into thin slices. 
  3. Wash and peel the potatoes, cut in four lengthwise and cut thin slices. 
  4. Put your dough in a pie pan (if necessary butter and flour, but in my case, I cooked with the baking paper). 
  5. On the puff pastry, place the pieces of zucchini and pieces of potatoes. 
  6. Peel the two cloves of garlic, remove the seeds and cut into small cubes, the finest possible. 
  7. Sprinkle evenly with garlic on potatoes and zucchini. 
  8. Crumble the fresh goat cheese by trying to spread it over the ingredients. 
  9. Season with salt and pepper. 
  10. Add thyme on the whole surface of the pie. 
  11. Then cut out Comté matches and spread them evenly over the top. 
  12. Bake thermostat 200 ° C for a good half hour. The pie must be golden brown on the top. 


