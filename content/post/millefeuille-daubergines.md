---
title: eggplant millefeuille
date: '2016-04-01'
categories:
- amuse bouche, tapas, mise en bouche
- Algerian cuisine
- diverse cuisine
- Healthy cuisine
tags:
- Tomato sauce
- Easy cooking
- Grilled eggplants
- dishes
- Ramadan
- Algeria
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/12/millefeuille-daubergines.jpg
---
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/millefeuille-daubergines.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/12/millefeuille-daubergines.jpg>)

##  eggplant millefeuille 

Hello everybody, 

I do not tell you my weakness for eggplant, amazing when I remember that being small, I ate this vegetable in the grilled pepper salad. I also ate the fried eggplant, yes it's too good, but with time and when we start to realize that it's not because a recipe is good, so it's good for health, it's where we start looking for alternatives. In any case for me, it's been a long time since I tried to avoid frying at home, moreover the fries it is hardly that we eat 1 time per 15 days. 

Today I'm putting you an old recipe that I prepared before with slices of fried eggplant. To avoid grease and cooking oil, I opted with the time to cook aubergines in the oven with just a small trickle of olive oil. Despite this small change, this dish has kept its flavor and delight, I often do for my children and for me, we enjoy each time. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/millefeuille-daubergines-1.jpg)

**eggplant millefeuille**

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/millefeuille-daubergines-1.jpg)

portions:  6  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 4 medium eggplants 
  * homemade tomato sauce 
  * mozarilla 
  * salt and black pepper 
  * olive oil 



**Realization steps**

  1. wash the eggplants, remove the peduncles and the bulging sides 
  2. cut in 4 or 5 slices in length. salt and let them out of the water. 
  3. Heat the oven, wrap a baking tin with baking paper. 
  4. wipe the eggplants with paper, put them on the plate, and garnish them with a little drizzle of olive oil. 
  5. bake until the eggplant slices turn a nice color. 
  6. remove from the oven and let it cool to prepare the tomato sauce   
watch out for salt! 
  7. in an oiled dish, assemble the aubergine millefeuille, alternating eggplant slices and tomato sauce.   
I do not like a thousand leaves too high, so I make 4 slices of eggplant. 
  8. decorate the last layer of tomato with mozzarella cheese 
  9. prick with toothpicks to keep the shape of the millefeuilles 
  10. bake at 180 ° C just the time to brown the thousand leaves (no more than 12 minutes) 
  11. let cool a little before introducing, and remember when serving to remove the toothpicks. 
  12. Enjoy your meal. 



[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/millefeuille-daubergines-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/12/millefeuille-daubergines-2.jpg>)
