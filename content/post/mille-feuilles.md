---
title: Thousand sheets
date: '2007-11-11'
categories:
- fish and seafood recipes

---
  * Puff pastry 
  * pastry cream buttered 
  * white fondant 
  * coffee extract 



... and quickly prick it with a fork or spades quickly 

Place it between two greased baking trays. In this way it will remain flat and even during cooking while maintaining a tight puff. 

Bake in a hot oven, 180 ° C for 20 to 25 minutes 

Make a vanilla pastry cream. 

Once done, beat it with an electric whisk, preferably to cool it completely. Then add 150 to 200 g of ointment butter per kilogram of custard. 

Arrange the first plate on the worktop. 

Position a second puff pastry plate on top. Press with your hand to make sure to adhere. 

Spread a second layer of custard 

Spread with a spatula evenly. 

Finally, arrange the last drop of cooked dough 

A mille-feuilles always consists of 3 layers of puff pastry. 

Take a small portion and color with coffee extract. 

Mix. 

Arrange the mille-feuilles on a rack. 

Pour the warm white fondant over the mille-feuilles 

Smooth using a curved metal spatula ... 

... going to the edges. 

Fill a decorated cornet with colored fondant and draw parallel lines. 

Scratch the surface in one direction then the other with the tip of a knife to perform the marbling. 

This work must be done quickly, before the white flux cools and hardens. 

To trim the sides of the mille-feuilles ... 

... with a saw knife to make it presentable. 

Réserver au frais jusqu’au moment de le servir. 
