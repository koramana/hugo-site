---
title: Kitchen love took on a new look
date: '2011-03-12'
categories:
- Café amour de cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/03/look1.jpg
---
Hello everybody, 

many of you have noticed my new banner, in truth I wanted to completely change the look of my blog to a color "clear" 

Caroline did a great job for me, starting with the banner you saw, I went to a "light shrimp" color, and caroline started to make me full of color and drawing, but when I wanted to apply the new design on my blog, I had a big problem, with golden lines, butterflies, and most importantly the writing of articles that was to remain "yellow or orange" on a bottom Creuvette, disaster, all that must ask for a big, big job, so with 690 published articles, all in orange, I must pass on article, after article to put the appropriate color, because I could not do that in the script. 

![look.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/look1.jpg)

so I asked Caroline to stop everything, and I just put the banner she made me kindly. 

but also I made a little change, I went to a blog color, even darker, did you notice? 

![soulef3 banner](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/banniere-soulef31.jpg)

look at the color difference 

the main thing, I always have the idea to relook my blog to a light color, but will have to find the trick to put a sudden writing. 

in any case, this is just to tell you, that I like that the visit on my blog is comfortable for you, I know it's not the look that is needed more for a blog, but rather what we find on a blog. 

and if you know a nice person who can break the head with me to revamp my blog, hihihihi I will not say no 

bisous 
