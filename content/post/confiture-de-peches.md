---
title: jam of peaches
date: '2016-09-05'
categories:
- jams and spreads
tags:
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-de-peche4.jpg
---
![jam-de-fishing](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-de-peche4.jpg)

##  jam of peaches 

Hello everybody, 

This is what I prepared with the 5 kilos of peaches that my husband has brought so called season and too good, but here is practically inedible, I do not tell you my disappointment, me who loves peach !!! 

For the recipe and the ingredients, I just cleaned and pitted the 5 kilos of peaches ... .. I think you'd rather have the recipe detailed, but I'm going to divide the proportions for just a small amount 

**jam of peaches**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-de-peche3.jpg)

Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 1.5 kg of peaches 
  * 800 gr of sugar 
  * 1 lemon 



**Realization steps**

  1. carry 1 liter of water to a boil. 
  2. plunge the peaches in for almost 2 min 
  3. dip the peaches directly into a cool water that will help you peel them 
  4. cut them into quarters and pit the 
  5. sprinkle peach pieces with lemon juice 
  6. melt the sugar in 200 ml of water 
  7. bring to a boil in a special jam pan, add the peaches 
  8. occasionally foam the mousse when cooking and cook on high heat. 
  9. Check the cooking of the jam by pouring some snacks on a plate. If the liquid freezes after a while, then it is well cooked 
  10. fill the sterilized jars, close and place them upside down. 



![jam-de-peches.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-de-peches1.jpg)

thank you for all your comments, and thank you for your visits that make me really happy 

Maybe you will like: 

![https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-de-tomate-007_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/confiture-de-tomate-007_thumb.jpg) [ confiture a la tomate ](<https://www.amourdecuisine.fr/article-confiture-a-la-tomate.html> "jam with tomato")
