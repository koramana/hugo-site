---
title: Baklawa baqlawa has the filo dough for the aid party
date: '2017-06-11'
categories:
- gateaux algeriens au glaçage
- gateaux algeriens- orientales- modernes- fete aid
tags:
- Ramadan
- Ramdhan
- Ramadan 2017
- Algerian cakes
- Cakes
- desserts
- stuffed

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-%C3%A0-la-pate-filo-741x1024.jpg
---
![baklawa with filo paste](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-%C3%A0-la-pate-filo-741x1024.jpg)

##  Baklawa baqlawa has the filo dough for the aid party 

Hello everybody, 

On the occasion of the holiday of aid, I tell you all Aid moubarek, and for the occasion I prepared some cupcakes, to be frank, not much time, to do what I wanted. 

the baqlawa, is the favorite cake of my dear husband, and that, I was a little tired, I opted for the easiest preparation, that based on filo paste. and like that, the baklawa can become easy to do for everyone, no risk of failure. 

The recipe in Arabic: 

the ingredients: (for a large tray) 

  * 2 boxes of filo pastry 
  * 1 kg of dried and ground hulled almonds 
  * sugar crystallized according to your taste (for the stuffing of almonds) 
  * 1 cac of cinnamon 
  * Orange tree Flower water 
  * Ghee or clarified butter or smen. 
  * 500 gr of honey (plan more according to your plate) 



![baklawa with filo paste 1](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-%C3%A0-la-pate-filo-1.jpg)

starting by preparing the stuffing, mixing our ground almonds but not powdered, with the crystallized sugar and cinnamon, wet with orange blossom water or rose, to have a nice non sticky mixture. 

now, we move to the setting tray, which becomes very easy with the filo paste. brush a flat with well-brimmed edges, using a brush, ghee (smen), put a sheet of filo pastry, to cover the whole tray, if necessary 2 or cut to have the exact size. brush the surface of the ghee leaf, put another leaf, then the ghee, and so on, until you have a good layer of foliage. 

now cover the filo pastry surface with the almond stuffing, and again put on your filo pastry again, always brush with ghee. until you finish your foliage. 

now we can go to the most difficult stage, the cutting of the baqlwa form. as below 

may be for certain, it's easy to do, and maybe some of you, prefer to argyle, I like the rosette, otherwise I do not feel it's baqlawa. 

the method is very clear in the photo; so **c** oupez the baklawa in two equal parts then in four. Each quarter will be divided in two. Cut strips parallel to the quarter cut line. Then cut parallel strips to the 8th. You will get your first diamonds. We continue with the other part of the quarter always cutting strips parallel to the black and blue line. 

![baklawa editing](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-montage-683x1024.jpg)

Stitch each diamond with a hulled almond. Sprinkle generously with ghee and bake in preheated oven at 180 °. Cook for 45 minutes or until the top is golden brown (or the dough separates from the side of the tray, the cooking time depends on the oven, I have an electric oven that chuaffe very badly). Remove the tray and immediately sprinkle hot honey mixed with orange blossom water. 

if you can resist, leave the tray as it is for 24 hours or longer, so that the dough absorbs the honey well. 

![baklawa cooked](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-cuite.jpg)

good tasting, 

for [ the Constantine baklawa ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-aux-amandes-et-cacahuettes-61018034.html>) : 

![baklava, cake-algerien1](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/baklawa-gateau-algerien1.jpg)

for [ the Algerian baklawa ](<https://www.amourdecuisine.fr/article-baklawa-aux-amandes-de-lunetoiles-56933001.html>) : 

[ ![baklawa9](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa91.jpg) ](<https://www.amourdecuisine.fr/article-baklawa-aux-amandes-de-lunetoiles-56933001.html>)

and for the Turkish Baklawa: 

{{< youtube bU_3sXgeLaQ >}} 

**Baklawa baqlawa has the filo dough for the aid party**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-%C3%A0-la-pate-filo-1.jpg)

**Ingredients**

  * 2 boxes of filo pastry 
  * 1 kg of dried and ground hulled almonds 
  * sugar crystallized according to your taste (for the stuffing of almonds) 
  * 1 cac of cinnamon 
  * Orange tree Flower water 
  * Ghee or clarified butter or smen. 
  * 500 gr of honey (plan more according to your plate) 



**Realization steps**

  1. starting by preparing the stuffing, mixing our ground almonds but not powdered, with the crystallized sugar and cinnamon, wet with orange blossom water or rose, to have a nice non sticky mixture. 
  2. now, we move to the setting tray, which becomes very easy with the filo paste. brush a flat with well-brimmed edges, using a brush, ghee (smen), put a sheet of filo pastry, to cover the whole tray, if necessary 2 or cut to have the exact size. brush the surface of the ghee leaf, put another leaf, then the ghee, and so on, until you have a good layer of foliage. 
  3. now cover the filo pastry surface with the almond stuffing, and again put on your filo pastry again, always brush with ghee. until you finish your foliage. 
  4. now we can go to the most difficult stage, the cutting of the baqlwa form. as below 
  5. may be for certain, it's easy to do, and maybe some of you, prefer to argyle, I like the rosette, otherwise I do not feel it's baqlawa. 
  6. the method is very clear in the photo; so cut the baklawa into two equal parts and then four. Each quarter will be divided in two. Cut strips parallel to the quarter cut line. Then cut parallel strips to the 8th. You will get your first diamonds. We continue with the other part of the quarter always cutting strips parallel to the black and blue line. 
  7. Stitch each diamond with a hulled almond. Sprinkle generously with ghee and bake in preheated oven at 180 °. Cook for 45 minutes or until the top is golden brown (or the dough separates from the side of the tray, the cooking time depends on the oven, I have an electric oven that heats very badly). Remove the tray and immediately sprinkle hot honey mixed with orange blossom water. 
  8. if you can resist, leave the tray as it is for 24 hours or longer, so that the dough absorbs the honey well. 



![baklawa with filo paste 3](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-%C3%A0-la-pate-filo-3.jpg)
