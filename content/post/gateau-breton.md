---
title: Breton cake
date: '2016-11-03'
categories:
- dessert, crumbles et barres
- gateaux, et cakes
- recettes sucrees
tags:
- Cookies
- biscuits
- desserts
- To taste
- Easy cooking
- Cake
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/gateau-breton-1.jpg
---
![cake-Breton-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/gateau-breton-1.jpg)

##  Breton cake 

Hello everybody, 

When a friend calls you to say she's going to see you in the afternoon, and when all your pretty dishes are in the boxes because the work in the kitchen will begin in a few days, it's a great panic. Especially when this friend is very precious and you have not seen in a while, you do your best to have a good time around a good tea. 

I had thought of doing a simple [ mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouscoutchou.html>) , but I remembered that I packed all my molds in the boxes, I had only a small mold with removable bottom that dragged in the bottom of the cupboard ... What could I really do ??? A quick recipe that may be ready to eat in less than two hours, but which ???? 

![cake-Breton-6](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/gateau-breton-6.jpg)

I started looking at my cake books to see what I could do in the blink of an eye, a cake that would not need decoration or too much work, because at the same time I had to arrange a little House. 

And that's where I saw the picture of **Breton cake** , yeahhhhhhhhhhhhhh not only this cake is too good, too fondant in the mouth and too beautiful, it is super easy to prepare ... Especially if we make out the butter a little more from the fridge, not even need to bring out his robot! 

Quickly, I made my cake and baked time to arrange everything .... Remains that I did not have the dishes for tea, hihihihihih, so we did with what I had on hand. The most important was having a good time in front of our delicious Breton cake, talking about our summer holidays, our families, and especially the new schools of our children, because our children are no longer together in the same high school. 

I spoke too much, so I quickly deliver the recipe for Breton cake super successful and super good.   


**Breton cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/gateau-breton-2.jpg)

portions:  6  Prep time:  10 mins  cooking:  50 mins  total:  1 hour 

**Ingredients**

  * 4 egg yolks (+1 to brown the cake) 
  * 140 g caster sugar 
  * ½ c. vanilla coffee 
  * 165 g of ointment butter 
  * 1 pinch of salt 
  * a small pinch of baking powder 
  * 220 g flour 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. Mix the egg yolks, salt and sugar without much whipping. 
  3. Add the butter in the ointment while whisking for a creamy mixture. 
  4. Add the sifted flour with the baking powder and mix well to obtain a homogeneous mixture 
  5. line a removable base of 18 cm diameter. and pour in the dough. 
  6. trim the surface with a spatula and then brush with the egg yolk. 
  7. draw braces with a fork, and bake the cake for 40 to 50 minutes depending on your oven. 
  8. At the end of cooking, remove from oven, turn out and let cool on a wire rack. 



![cake-Breton-4](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/gateau-breton-4.jpg)
