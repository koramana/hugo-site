---
title: Strawberries and Cream Bagatelles
date: '2015-05-08'
categories:
- dessert, crumbles et barres
- recettes sucrees
- verrines sucrees
tags:
- Fruits
- desserts
- verrines
- Ramadan
- Ramadan 2015
- Easy cooking
- Trifles

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Fraises-et-Cr%C3%A8me-Bagatelles-1.jpg
---
[ ![Strawberries and Cream Bagatelles](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Fraises-et-Cr%C3%A8me-Bagatelles-1.jpg) ](<https://www.amourdecuisine.fr/article-fraises-et-creme-bagatelles.html/sony-dsc-287>)

##  Strawberries and Cream Bagatelles 

Hello everybody, 

I forget recipes in my archives, and here is a recipe Lunetoiles that date from 2013. Thank you to Lunetoiles who reminded me of this recipe at the right time, the strawberry season. So why not enjoy, and make these beautiful verrines of strawberries and cream bagatelles? 

A trifle with summer fruits is a quick and easy dessert to make. the recipe is made with sponge cake, cake, or any other sponge cake, custard or custard cream, homemade strawberry jam or sugar strawberries. So we can say that trifles are very similar to English trifles. 

[ ![Strawberries and Cream Bagatelles 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Fraises-et-Cr%C3%A8me-Bagatelles-2.jpg) ](<https://www.amourdecuisine.fr/article-fraises-et-creme-bagatelles.html/fraises-et-creme-bagatelles-2>)

**Strawberries and Cream Bagatelles**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Fraises-et-Cr%C3%A8me-Bagatelles.jpg)

portions:  6  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients** for pastry cream 

  * 60 cl of whole milk 
  * 1 vanilla pod scraped 
  * 4 egg yolks 
  * 50 g caster sugar 
  * 2 tablespoons flour (30 g) 
  * 4 tablespoons corn flour (60 g) 
  * 1 pinch of salt 
  * 100 g of mascarpone 

for strawberries 
  * 800 g strawberries, sliced 
  * 3 tablespoons caster sugar 

for editing and decorating 
  * cake, sponge cake, biscuit, sponge cake ... (sliced ​​into pieces) 
  * sliced ​​strawberries 
  * whipped cream 
  * icing sugar 



**Realization steps**

  1. To make pastry cream, heat the milk and vanilla in a saucepan. 
  2. Meanwhile, mix egg yolks, sugar, flours and a pinch of salt until the mixture is smooth and thick. 
  3. When the milk is hot, pour it slowly into the egg yolk mixture while continuing to mix. 
  4. When all the milk has been incorporated, transfer the liquid mixture to the saucepan over low heat, cook and thicken, stirring constantly. 
  5. Remove from heat and place in a bowl and put a piece of plastic film directly on the surface of the cream to prevent skin from forming. 
  6. Place in the refrigerator and let cool. When it's totally cold, beat the cream with the mascarpone cheese. 
  7. To make the strawberries, mix the sliced ​​strawberries with the powdered sugar and mix well. Let stand for 20 minutes until the strawberries have released a lot of their juice. 
  8. To assemble trifles: layered cake layer with macerated strawberries and custard. 
  9. Garnish with whipped cream, sliced ​​strawberries and sprinkle with icing sugar. 



[ ![Strawberries and Cream Bagatelles](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/Fraises-et-Cr%C3%A8me-Bagatelles-3.jpg) ](<https://www.amourdecuisine.fr/article-fraises-et-creme-bagatelles.html/sony-dsc-288>)
