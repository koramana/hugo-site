---
title: Olive chicken
date: '2014-09-07'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tajine-de-poulet-aux-olives_thumb.jpg
---
##  Olive chicken 

Hello everybody, 

a beautiful photo is not it? this delicious chicken tagine with olives, prepared with lemon zest and dried tomato, a recipe that comes from the sister of my friend Lunetoiles, and frankly, myself I ask him the recipe right now that I have saw the photos, because I had a great desire to taste this dish. 

a tagine very delicious, with a smooth sauce, so immediately the recipe for you and me ...   


**Olive chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tajine-de-poulet-aux-olives_thumb.jpg)

Recipe type:  Tagine dish  portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 2 small farm chickens cut into pieces or in the absence of chicken thighs that you will take care of cut in half, on one side of pestle, on the other the thigh. 
  * 2 big onions 
  * 3 cloves of garlic 
  * 1 lemon 
  * 1 tablespoon sweet pepper 
  * 1 tablespoon of tagine spice or failing: 1 tbsp. coffee of cumin, 1c of curry coffee or turmeric. 
  * 300 g of green olives (which can be natural or candied according to your taste) 
  * 300 g dried tomatoes 
  * 2 tablespoons chopped coriander 
  * 2 tablespoons chopped parsley 
  * 3 tablespoons of olive oil *** 
  * 1 tablespoon of butter 
  * salt pepper 



**Realization steps**

  1. Peel and chop the onions that you cut into thin slices and then into small cubes, 
  2. put them on a plate. 
  3. Peel and crush the cloves of garlic, and put them in a bowl. Rinse the lemon and using a grated board or zester, extract the bark. 
  4. Then, cut the lemon, and recover the juice (with a good juice press), mix this in the bowl, adding the parsley, the coriander that you took care of chopped, the dried tomatoes cut in slices and 1 c to olive oil soup. In the bowl, add 2 pinches of salt and 2 pinches of pepper. 
  5. Melt the onions over low heat in a pan with 100 gr of butter, when they start to brown, place the chickens and add the 2 tablespoons of olive oil. Brown chickens over medium heat, turning often; Pour 35 cl of water as well as the sweet pepper and spices and bring to a boil. When the water decreases, add the container of your bowl and add the olives. Lower the heat, cover and cook for about 30 minutes. Do not hesitate to stir often, prick the chicken for better cooking, add a little water if the sauce is too dry. 
  6. Serve chicken tajine with olives, zesty lemon and dried tomatoes very hot, in a tajine dish, decorated with cut lemons. 



Note *** If your green olives are under vacuum, boil 1 liter of water in a saucepan with 2 tablespoons salt or 1 tablespoon coarse salt, curry and chopped parsley, to remove bitterness related to preservatives. 
