---
title: makrout sniwa or makrout way baklawa
date: '2015-03-18'
categories:
- gateaux algeriens au miel
- gateaux algeriens- orientales- modernes- fete aid
tags:
- Oriental pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa_.jpg
---
[ ![makrout sniwa_](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa_.jpg>)

##  makrout sniwa or makrout way baklawa 

Hello everybody, 

Before starting this article, I must express my pride of the Algerian woman, this innovative woman and creator of dishes and delights just as beautiful and good. Like this new version of Makrout, called Makrut sniwa, ie the makrout in the tray, or makrout way baklawa, because of the method of realization of this delight to the version of the baklawa. 

It's a recipe that comes from eastern Algeria, an easier version of [ makrout el koucha ](<https://www.amourdecuisine.fr/article-makrout-au-four-makroud-gateau-algerien-%d9%85%d9%82%d8%b1%d9%88%d8%b7-%d8%a7%d9%84%d9%83%d9%88%d8%b4%d8%a9.html>) , Which is one of the delicious cakes that take a lot of time to realize, but with this method of makrout sniwa preparation, we will have fun making trays and trays (sniwa) without getting too tired. 

[ ![makrout sniwa 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa-4.jpg>)

**makrout sniwa or makrout way baklawa**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa-3-1.jpg)

portions:  60  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * 3 and a half measures of semolina 
  * 1 measure of melted butter 
  * salt 
  * 1 sachet of baking powder 
  * orange blossom water + water to pick up the dough 
  * for the stuffing: 
  * 3 measure peanuts or crushed almonds 
  * 1 measure of sugar 
  * Orange tree Flower water 



**Realization steps**

  1. preparation of the dough: 
  2. mix the semolina and melted butter to thoroughly shatter the semolina, let stand overnight if possible.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/semoule-sabl%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/semoule-sabl%C3%A9e.jpg>)
  3. add over baking powder and salt, mix 
  4. pick up the dough with the orange blossom water and a little water. 
  5. divide the dough in half and let it rest (try to do it equally, I have not weighed down so the lower layer is a bit thinner than the top layer) 
  6. Now prepare the stuffing by mixing almonds, sugar and a little orange blossom water. 
  7. spread the first semolina ball in a mold to a thickness of almost 5 mm 
  8. cover this semolina layer with an even layer of stuffing 
  9. now spread the second ball of semolina on baking paper, to a thickness of 5 mm too, and then pour it on the stuffing. 
  10. press gently to adhere the dough to the stuffing.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/montage-makrout-sniwa.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/montage-makrout-sniwa.jpg>)
  11. Cut makrouts of equal size, using a ruler. 
  12. Using a pair of pliers, decorate the makrouts according to your taste. 
  13. you can also decorate with a whole almond   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trassage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/trassage.jpg>)
  14. cook the cake in a preheated mold at 200 degrees C until the cake turns a beautiful golden color. 
  15. As soon as you leave the makrout tray of the oven, sprinkle with cold syrup and let it absorb well.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cuisson-et-miel.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cuisson-et-miel.jpg>)



[ ![makrout sniwa 5](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa-5-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/makrout-sniwa-5-1.jpg>)
