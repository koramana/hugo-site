---
title: roast beef in the oven
date: '2013-08-12'
categories:
- bakery
- crepes, donuts, donuts, sweet waffles
- diverse cuisine
- Cuisine by country
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/roti-de-boeuf_thumb1.jpg
---
##  roast beef in the oven 

Hello everybody, 

Yum yum a roast fillet of beef baked! This is my favorite recipe, I realized this for a while and I forgot to publish. 

it's a very easy recipe, you can accommodate it in any sauce, so I hope you like it.   


**roast beef in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/roti-de-boeuf_thumb1.jpg)

Recipe type:  dish  portions:  6  Prep time:  10 mins  cooking:  60 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 kilo of beef 
  * 1 onion 
  * coriander and parsley 
  * salt and black pepper 
  * 1 bay leaf 
  * olive oil 



**Realization steps**

  1. cut the piece of meat sideways, to have a nice uniform piece. 
  2. sprinkle on top, chopped onion, chopped parsley. 
  3. season with salt and pepper. 
  4. roll the meat to enclose all the ingredients 
  5. celerate well with a string, and place the bay leaf over 
  6. place the roll of meat on a sheet of aluminum, poured over a slice of olive oil, and cover well with the sheet of allu. 
  7. cook in preheated oven for at least an hour. 
  8. serve with sautéed potatoes, puree, or just a small salad. 


