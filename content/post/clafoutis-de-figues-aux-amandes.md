---
title: Clafoutis of figs with almonds
date: '2016-09-07'
categories:
- dessert, crumbles et barres
- sweet recipes
tags:
- To taste
- The start of the school year
- Algeria
- desserts
- Ramadan
- flan
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Clafoutis-de-figues-aux-amandes-1.jpg
---
![clafoutis-of-figs-to-almond-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Clafoutis-de-figues-aux-amandes-1.jpg)

##  Clafoutis of figs with almonds 

Hello everybody, 

Yes, I really enjoy the fig season, here in England, in a few days it will disappear from the stalls, so I try to do my best to make all the delicious recipes that I have detected here and for a while. 

Among these delicacies is the recipe for this clafoutis of figs with almonds that I spotted in the magazine: Cuisine Actuelle. This clafoutis is just too good, and I promise you that you will not stop at a single spoon, especially accompanying this delicious clafoutis figs almonds hot with a scoop of vanilla ice cream .... It's just a delusion, oupst !!! I meant a delight, hihihihi. 

**Clafoutis of figs with almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Clafoutis-de-figues-aux-amandes_.jpg)

portions:  2  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * 250 g fresh figs 
  * 3 eggs 
  * 75 g flour 
  * 350 ml of milk 
  * 1 nice handful of flaked almonds 
  * 100 g caster sugar 
  * butter for the mold 



**Realization steps**

  1. Preheat the oven to 200 ° C. 
  2. Beat the eggs and the sugar, then add the flour. 
  3. Stir in the milk while whisking. 
  4. Wash and dry the figs. Cut them into 2, then cut into large cubes.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/clafoutis-de-figues.jpg)
  5. Pour the dough into a buttered mold and add the pieces of figs. 
  6. Sprinkle with almonds. 
  7. Bake 30 minutes while watching. Serve warm or cold. 



![clafoutis-of-figs-to-almond-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/Clafoutis-de-figues-aux-amandes-2.jpg)
