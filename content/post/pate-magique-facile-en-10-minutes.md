---
title: easy magic pate in 10 minutes
date: '2016-12-16'
categories:
- cuisine algerienne
- cuisine diverse
- Mina el forn
- pizzas / quiches / tartes salees et sandwichs
- recipes of feculents
- basic pastry recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-de-10-minutes.jpg
---
[ ![10 minutes magic paste](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-de-10-minutes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-a-pizza.jpg>)

##  easy magic pate in 10 minutes 

Hello everybody, 

Do not you have a great desire to make pizza, salty slippers, or any other dough-based delights, but you are too lazy to make the dough and wait 40 minutes between lifting and degassing, or even sometimes, we do not have time to make a pizza while there are only 30 minutes left for dinner time ... so here is the best magic dough recipe you can do. 

A magic dough of 10 minutes no more, you just have to be quick to make the topping of your pizza. So it's not magic for nothing, hihihi. 

I thank my dear Pipo Bijoux, for this recipe, and I confess to you, that Pipo gave me the courage to make this recipe soon, I must confess that I have my [ Pizza dough ](<https://www.amourdecuisine.fr/article-pate-a-pizza-maison-recette-facile.html>) I've been doing it for over 8 years now ... I did not want to change it, but I'll do this recipe as soon as possible, and I'll let you know, though I'm sure many of you we have already realized. 

**easy magic pate in 10 minutes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-a-pizza.jpg)

portions:  3  Prep time:  10 mins  total:  10 mins 

**Ingredients** for leaven: 

  * 1 glass of warm water (220) 
  * 1 glass of flour 
  * 1 teaspoon of salt 
  * 1 tablespoon instant yeast (baker's yeast) 
  * 1 tablespoon of sugar 
  * 1 tablespoon of milk powder (optional) 

for the pasta: 
  * ¼ glass of oil 
  * enough flour to have a dough that picks up 



**Realization steps**

  1. Mix all the leavening ingredients 
  2. let the volume double in a bowl and cover with cling film.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-.jpg>)
  3. When the dough has doubled the volume add the oil and dry with flour until you have a smooth and tender dough   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-2.jpg>)
  4. You can use this pasta as a base for pizza, slipper, cornet, or even donuts express. 



Note With these quantities, you can have three beautiful pizza patties. 

[ ![magic pasta for pizza](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-pour-pizza.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/pate-magique-pour-pizza.jpg>)
