---
title: Panna cotta with strawberries / ramadan dessert
date: '2013-07-23'
categories:
- bakery
- Buns and pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/panna-cotta-aux-fraises1.jpg
---
Hello everyone, I'm a little absent from my blog these days, but I made you some videos that I hope you will like, find these videos on my channel youtube, tell me your opinion, especially encourage me with your jesus love under the videos ..... I must thank by the way, my friend Lunetoiles who takes care of the blog in my absence, and who also helps me by sending me these beautiful achievements ... and here is one: & nbsp; Preparation time: 30 min Cooking time: 35 min 110 g caster sugar 35 cl water 1 tsp. powdered gelatin & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.65  (  1  ratings)  0 

![panna cotta aux fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/panna-cotta-aux-fraises1.jpg)

Hello everybody, 

I must thank by the way, my friend Lunetoiles who takes care of the blog in my absence, and who also helps me by sending me these beautiful achievements ... and here is one: 

**Preparation time: 30 min** **Cooking time: 35 min**

  * 110 g caster sugar 
  * 35 cl of water 
  * 1 C. powdered gelatin (or 3 sheets of gelatin) 
  * Strawberries washed and cut in half 



For panna cotta: 

  * 60 cl of liquid cream 
  * 30 cl of milk 
  * 2 tbsp. powdered gelatin (or 7 sheets of gelatin) 
  * 150 g icing sugar 
  * 1 vanilla pod 



![terrine de panna cotta aux fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/terrine-de-panna-cotta-aux-fraises1.jpg)

method of preparation: 

  1. Pour 110 g caster sugar and 35 cl water into a saucepan, put over medium heat, stir to simmer 3 to 4 min. 
  2. Pour the gelatin into the syrup, stir to dissolve, simmer 2 min. 
  3. Pour the syrup into a bowl, let cool. 
  4. Place the strawberries in a cake tin (26 x 8 x 7.5 cm) or missed buttered non-stick, the cut portion towards the bottom. 
  5. Cover with cooled syrup and place 2 hours in the refrigerator. 
  6. Meanwhile, prepare the panna cotta: Split the vanilla bean in the length and scratch the black seeds it contains over a saucepan. 
  7. Add the cream, the milk, the icing sugar, the pod and the gelatin. 
  8. Put on the heat and let shiver on low heat for 3 minutes, stirring. 
  9. Remove the pod and let the panna cotta cool to room temperature. 
  10. Remove the pan from the refrigerator, pour over the cooled panna cotta and return to the fridge for 6 hours. 
  11. When serving, dip the pan in a dish of hot water and return to a serving dish. 



![panna cotta-with-strawberries - desserts de ramadan.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/panna-cotta-aux-fraises-dessert-de-ramadan1.jpg)
