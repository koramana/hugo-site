---
title: Far Breton with candied kumquats
date: '2014-04-07'
categories:
- dessert, crumbles and bars
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Far-breton-aux-kumquats-confits-1.1.jpg
---
[ ![Far Breton with candied kumquats](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Far-breton-aux-kumquats-confits-1.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Far-breton-aux-kumquats-confits-1.1.jpg>)

Hello everybody, 

A recipe to fall from our dear Lunetoiles this far Breton candied kumquats, yumi. 

On the blog you will find the recipe of [ prune far Breton ](<https://www.amourdecuisine.fr/article-far-breton.html> "Far breton, prune far فلان بالبرقوق") , a delight. I can not tell you anything about this recipe, the pictures speak for themselves, a creamy and rich layer that covers well these delicious candied fruits, the kumquats in this recipe. 

In any case my friends, I will try in the days to come to post the recipes of Lunetoiles who queued in my mailbox, there are more than 50 recipes, I did not have time to post since the beginning of my pregnancy, and fatigue throughout this period ... I will try to catch up with you, and publish them as soon as possible. 

**Far Breton with candied kumquats**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Far-breton-aux-kumquats-confits.1.jpg)

portions:  6-8  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients** Candied vanilla kumquats: 

  * 400 g of kumquats 
  * 265 g of sugar 
  * 1 vanilla pod 
  * 30 Cl of water 

For far breton: 
  * 75 cl of whole milk 
  * 50g of soft butter or soft half-salt (at room temperature) 
  * 130 g caster sugar 
  * 5 eggs 
  * 1 sachet of vanilla sugar 
  * 220 g flour 



**Realization steps**

  1. Preheat the oven to 180 ° (th 6) and butter a mold to miss. 
  2. Prepare the vanilla candied kumquats: 
  3. Bring a pot of water to a boil. 
  4. Add the kumquats and let them blanch for about 4 minutes. 
  5. Drain the kumquats and refresh them. 
  6. In a large saucepan, put kumquats, sugar, water and scraped vanilla bean. 
  7. Bring to a boil and cook for 5 minutes. 
  8. Then bring everything to a low heat and cook in small broth for about 15 minutes. 
  9. Check that it does not caramelize too much. 
  10. Let cool and drain the kumquats. 
  11. Break the eggs into a bowl, add the caster sugar and the vanilla sugar. Whisk (preferably with electric mixer) adding, little by little, the flour, then the milk and finally the very soft butter. 
  12. Continue whisking until the dough is smooth. 
  13. Arrange the kumquats in the buttered mold, pour the dough, bake for about 40 minutes. 
  14. Continue cooking for a further ten minutes if necessary. 
  15. Prick with a knife to check the cooking. The blade should barely come out wet. 
  16. Let cool before eating. 



[ ![Far Breton with candied kumquats](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Far-breton-aux-kumquats-confits-2.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/Far-breton-aux-kumquats-confits-2.1.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
