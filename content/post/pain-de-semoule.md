---
title: semolina bread
date: '2015-10-20'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-semoule-026.CR2_thumb2.jpg
---
##  semolina bread 

Hello everybody, 

I go back after all your requests the recipe of **semolina bread** , a bread that has made its success on my facebook cooking group. The girls on the group do not stop doing it and redo, besides I share with you at the bottom of this article all the photos of their tests. 

This bread is super delicious semolina bread, which despite is based on flour and semolina, but this beautiful layer of crisp semolina that covers it, he owes this appointment, a traditional bread homemade, very tasty, despite the ingredients all simple that constitutes it. 

A very light bread especially to accompany a beautiful winter soup, or just to saucer. My children at least loved their bread with strawberry jam and butter in it, and for dinner, they had asked for their burgers with this bread. 

I see that it surprised me a lot, but I was very happy. 

**semolina bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-semoule-026.CR2_thumb2.jpg)

portions:  6  Prep time:  40 mins  cooking:  45 mins  total:  1 hour 25 mins 

**Ingredients** a 240 ml glass as a measure 

  * 1 glass of flour 
  * 1 glass and a half fine semolina 
  * 3 tablespoons milk powder 
  * 4 tablespoons of oil 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 cup of baking powder. 
  * 1 tablespoon instant baker's yeast. 
  * between 1 glass and a half and 2 glasses of water depending on the absorption of dry products 
  * semolina for decoration. 



**Realization steps**

  1. method of preparation: 
  2. if you have a bread machine or a kneader, just mix all the ingredients, and knead, otherwise: 
  3. pick up all the ingredients in a terrine gradually with water, add in small amounts. 
  4. knead the dough well for about 15 minutes. 
  5. let it rest until the dough doubles in volume. 
  6. degas it and divide it into a ball according to your choice, I made 6 of almost 70 gr each. 
  7. roll the balls in semolina. 
  8. flatten each 1.5 cm high and cut with a sharp knife. 
  9. cover and let rise for almost 45 minutes or depending on the heat of the room. 
  10. cook at 180 ° C for 20 to 25 minutes or until golden brown. 



recipe tested and approved:   
  
<table>  
<tr>  
<td>

[ ![semolina bread at Isabelle Lgd's](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-chez-Isabelle-Lgd-150x113.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-chez-Isabelle-Lgd.jpg>) At Isabelle Lgd's place 
</td>  
<td>

[ ![semolina bread samia](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-samia1-150x113.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-samia1.jpg>) at Samia Z 
</td>  
<td>

[ ![nesrine semolina bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-semoule-nesrine-110x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-semoule-nesrine.jpg>) at Nesserine Sissou 
</td> </tr>  
<tr>  
<td>

[ ![moufida semolina bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-moufida1-113x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-moufida1.jpg>) at Moufida hadid 
</td>  
<td>

[ ![amina semolina bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-amina-150x90.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-amina.jpg>) at Amina Dadou 
</td>  
<td>

[ ![Rawane semolina bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-de-Rawane-150x95.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-de-Rawane.jpg>) at Rawane Abden 
</td> </tr>  
<tr>  
<td>

[ ![semolina bread nadia](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-nadia-150x84.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-nadia.jpg>) at Felur Dz 
</td>  
<td>

[ ![semolina bread samia](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-samia-113x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-samia.jpg>) at Samia Z 
</td>  
<td>

[ ![semolina bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-a-la-semoule-150x112.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pain-a-la-semoule.jpg>) at Rachida Rach 
</td> </tr> </table>
