---
title: kentucky chicken
date: '2012-04-15'
categories:
- juice and cocktail drinks without alcohol

---
& Nbsp; hello everyone, here is a very delicious chicken recipe, the homemade kfc chicken recipe. I repaired it again at noon today, and I did not fail to make new photos. it was during a walk on the blogosphere that I saw this recipe at Kaouther. I already prepared the chicken tandoori, I already prepare the spicy chicken mexican, this time I wanted to try the famous kfc and I assure you the recipe was a real delight. To make this recipe, you need to marinate the chicken first: chicken marinade: chicken in pieces salt pepper & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.48  (  1  ratings)  0 

Hello everybody, 

here is a very delicious recipe of [ chicken ](<https://www.amourdecuisine.fr/categorie-12364208.html>) , that of  homemade kfc chicken. I repaired it again at noon today, and I did not fail to make new photos. 

I have already prepared the [ tandoori chicken ](<https://www.amourdecuisine.fr/article-poulet-tandoori-85541703.html>) , I already have to prepare [ Mexican spicy chicken ](<https://www.amourdecuisine.fr/article-chilli-hot-chiken-poulet-piquant-au-four-44410988.html>) This time I wanted to try the famous KFC and I assure you the recipe was a real treat. 

to make this recipe, you need to marinate the chicken: 

chicken marinade: 

  * chicken in pieces 
  * salt 
  * black pepper 
  * paprika 
  * Hrissa (red pepper paste, for lovers of tangy taste) 
  * Chinese salt (I did not have) 
  * 1 onion 
  * 2 to 3 cloves of garlic 
  * 1 small piece of ginger 
  * the juice of a lemon 
  * 1 pinch of spices (ras el hanout) 



put the chicken pieces in a large bowl, add salt, black pepper, paprika, hrissa, MSG, sliced ​​onion, crushed garlic, crushed ginger, a lemon wedge, a pinch of spices .    
mix everything well. cover the bowl with foil, and marinate all night. 

for breading: 

  * seasoned flour 
  * 2 slightly salted scrambled eggs 
  * breadcrumbs 



Flour milling: 3/4 cup flour for one kilogram of chicken) 

  * 2 cups flour 
  * 4 tablespoons of paprika 
  * 1 tablespoon of MSG (you will find it in Asian stores) 
  * 1/2 tablespoon of salt 
  * 1/2 tablespoon of oregano 
  * 2 tablespoons garlic powder 
  * 2 tablespoons of onion powder 
  * 1 tablespoon of mustard seeds 
  * 1 tablespoon of black pepper 
  * 1 tablespoon of baking powder. 


  1. put all these ingredients in the juice mixer and beat for a few seconds until a reddish powder is obtained. 
  2. drip the flour at the end of the tongue, and rectify the salt if necessary. 
  3. take the chicken pieces, dip them in the eggs first, then the breadcrumbs and finally the flour. 
  4. well oil a baking dish. place the pieces of chicken, taking care to separate them. 
  5. cover with foil and cook for about thirty to forty minutes. 
  6. check the cooking by inserting a knife inside the chicken, if the liquid that emerges is clear, your chicken is cooked. 
  7. remove the foil and let roast for a few minutes. 
  8. dive into a hot fry for a few seconds to give it that typical crisp chicken kfc. 



bonne dégustation 
