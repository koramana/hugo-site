---
title: Eton blueberry mess
date: '2015-12-22'
categories:
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/eton-mess-aux-myrtilles.CR2_1.jpg
---
![andthe-mess-aux-myrtilles.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/eton-mess-aux-myrtilles.CR2_1.jpg)

##  Eton blueberry mess 

Hello everybody, 

a dessert that I like very much, the easiest in the world, and the most delicious, in addition we can do it with all the fruits possible, the Eton mess. 

the eton mess is an English dessert, which usually was made from strawberries, meringue and cream ... there is a great story around this dessert, but apparently it holds its college name: "Eton College" ... and mess means in English: »bazaar» .... 

a little story on wikipedia tells: that the Eton mess, was actually a [ Pavlova ](<https://www.amourdecuisine.fr/article-recette-de-pavlova-au-kiwi-et-cranberry-116017619.html>) which was to be served at a picnic at Eton College, but the dessert was spilled accidentally, and what was saved from the cake, was served, and was well appreciated ...   


**Eton blueberry mess**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/eton-mess-Myrtilles-023.CR2_1.jpg)

portions:  4  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * [ meringues ](<https://www.amourdecuisine.fr/article-meringue-a-l-ancienne-recette-tres-facile-114688303.html>)
  * liquid cream 
  * sugar (just for liquid cream) 
  * blueberries 



**Realization steps** yes my friends, the recipe is done at random, according to your taste, more sugar, less sugar, a lot of fruit, and voila it is done. 

  1. whip the cream very cold with a little sugar to have a beautiful whipped cream. 
  2. crush the meringue roughly. 
  3. share the whipped cream on two, and gently mix one of the moities with blueberries, to have beautiful mottling. 
  4. place a layer of cream in your verrines, then a layer of meringue, a layer of blueberry cream. and so on, until you fill your verrines well. 
  5. decorate with blueberries, and serve immediately. 



![andthe-mess-blueberries-023.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/eton-mess-Myrtilles-023.CR2_1.jpg)
