---
title: samboussek with minced meat
date: '2017-09-09'
categories:
- Bourek, brick, samoussa, slippers
- diverse cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
- ramadan recipe
tags:
- Algeria
- inputs
- Easy cooking
- Ramadan
- Aperitif
- Summer kitchen
- slippers

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/09/samboussek-%C3%A0-la-viande-hach%C3%A9e-2.jpg
---
![samboussek with minced meat 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/samboussek-%C3%A0-la-viande-hach%C3%A9e-2.jpg)

##  samboussek with minced meat 

Hello everybody, 

At the request of my husband, I made these samboussek with minced meat, a very delicious starter that we like at home and that resembles bourek laajin (recipe of Algerian cuisine). 

This tasty appetizer will easily find room on your summer tables, Ramadan tables, large party buffets or for a picnic outing. When it comes to pranks, you can play with your imagination, with the stuffing you prefer the most: a stuffing with chicken, merguez, a stuffing with vegetables, a stuffing with cheese, the choice is great! 

So we go this time for Samboussek with minced meat, and soon I will share with you other varieties. 

{{< youtube 3wrv4jmaltM >}} 

**samboussek with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/samboussek-%C3%A0-la-viande-hach%C3%A9e-3.jpg)

**Ingredients** for the pasta: 

  * 4 glasses of flour (240 ml glass) 
  * ¾ glass of table oil 
  * 1 C. salt 
  * ½ c. sugar coffee 
  * water to pick up the dough. 

for the stuffing: 
  * 400 gr of minced meat 
  * 2 medium onions 
  * 1 clove of garlic 
  * 1 crushed tomato crushed 
  * 2-3 tbsp. extra virgin olive oil. 
  * salt, black pepper, paprika, cumin, cinnamon, 
  * a few sprigs of chopped parsley. 



**Realization steps** preparation of the dough: 

  1. in a large bowl, place the flour, sugar, salt and mix everything. 
  2. add the oil and sand well with your hands to incorporate the fat. 
  3. pick up the dough with the water until it becomes soft, smooth and supple. It must not be sticky. 
  4. cover the dough with cling film and let stand for at least 1 hour. 

preparation of the stuffing: 
  1. fry the chopped onion in the olive oil. 
  2. when the onion becomes translucent, add the minced meat. 
  3. cook the meat well, then add the crushed tomato, salt and remaining spices. 
  4. towards the end, introduce garlic and chopped parsley. Let simmer a little, then remove from heat and let cool. 
  5. preparation of samboussek with minced meat: 
  6. take the dough and divide it into 4 parts. 
  7. spread the ball to a thickness of 2 to 3 mm. 
  8. cut washers between 9 and 10 cm depending on the size you want to get from your samboussek. 
  9. place the equivalent of 1 teaspoon of stuffing in and close the circle out of two to have a half moon. 
  10. do so until all the dough is used up. 
  11. fry samboussek with minced meat in a hot oil bath. 
  12. drain on paper towels. And enjoy tepid. Enjoy! 



![samboussek with minced meat 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/samboussek-%C3%A0-la-viande-hach%C3%A9e-1.jpg)
