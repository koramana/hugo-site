---
title: Strawberry and kiwi smoothie
date: '2015-12-04'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-kiwi-fraises-001.jpg
---
[ ![strawberry kiwi smoothie-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-kiwi-fraises-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-kiwi-fraises-001.jpg>)

##  Strawberry and kiwi smoothie 

Hello everybody, 

I believe you are now living on my smoothies every morning, hihihihi. One thing is sure it's great to wake up every day, and to consume one thing that has never been consumed before, to discover each time a new flavor, a new association ... For me now it's a must, like a ritual. 

Today is a simple smoothie, I had to make a pie with these fruits, but I was more greedy, I preferred to taste all my fruits in one cup, hihihi. So here's a super simple recipe of smoothie made from kiwi and strawberries. 

**Strawberry and kiwi smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-kiwi-fraises.jpg)

portions:  2  Prep time:  3 mins  total:  3 mins 

**Ingredients**

  * 3 kiwis 
  * 1 banana 
  * 1 glass of frozen strawberries 
  * 1 glass of almond milk (you do not have to put all the glass) 
  * 1 strawberry yoghurt 0% 



**Realization steps**

  1. peel the kiwis 
  2. cut a few thin slices and stick them in the glasses in which you will serve the smoothie. 
  3. place the rest in the blinder bowl 
  4. wash a fresh strawberry cut in fine pieces and garnish the glasses with 
  5. place the frozen strawberries in the blinder bowl 
  6. add the remaining ingredients and mix. 
  7. Serve imediately 



[ ![kiwis strawberry smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-fraises-kiwis-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-kiwi-fraises-001.jpg>)
