---
title: homemade raffaello easy recipe
date: '2016-12-20'
categories:
- Algerian cakes without cooking
- gateaux algeriens- orientales- modernes- fete aid
tags:
- Algerian cakes
- Coconut
- delicacies
- Cakes
- desserts
- truffles
- Chocolate

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/Raffaello-fait-maison.jpg
---
[ ![homemade raffaello easy recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/Raffaello-fait-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/raffaello-facon-maison-2.jpg>)

##  Easy recipe homemade raffaello 

Hello everybody, 

Who does not like Raffaello? personally I'm crazy, I'm looking for the first opportunity to buy, my children love ferrero rock, I like of course, but I say to myself, saying that it's less caloric, lol .... 

Today Lunetoiles will share with you the delicious recipe easy homemade raffaello ... very simple, and super good. So here we have the choice, there are raffaello stuffed with creamy wafers, or with a crispy hazelnut in it, well, it must be crunchy in the mouth. 

You can also see the [ snowballs ](<https://www.amourdecuisine.fr/article-gateau-algerien-boulettes-a-la-noix-de-coco.html> "Algerian cake / coconut dumplings") that I realized a moment ago, I assure you the recipe is a killer with that touch of Nutella in it. 

The recipe that Lunetoiles passes us can give almost 30 pieces of raffaello house, but of course it depends on the size of the **coconut dumplings** that you will do. 

[ ![homemade raffaello easy recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/raffaello-facon-maison-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/raffaello-facon-maison-2.jpg>)   


**homemade raffaello easy recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/raffaello-fait-maison-1.jpg)

portions:  30  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 1 can of sweetened condensed milk (397 g) 
  * 300g grated coconut (for me 200g crushed fresh coconut and 100g grated dry coconut) 
  * whole roasted hazelnuts 
  * 100 gr vanilla wafers crumbled 

For the coating: 
  * 300 g of white chocolate (optional) 
  * 100 g grated coconut 
  * 90 g vanilla wafers crumbled (not too fine, it must not be powder!) 



**Realization steps**

  1. The day before: Pour the grated coconut and sweetened condensed milk and stir with a spatula. Book in the refrigerator overnight, or best for 24 hours. 
  2. The next day, take the salad bowl out of the fridge. 
  3. Add the crumbled vanilla wafers and mix everything together. 
  4. Take some pieces and put a roasted hazelnut in the center and roll into a ball between your hands. 
  5. Do the same thing until you are exhausted. 
  6. Then put all your balls, one after the other in a bowl with crumbled vanilla wafers, and then pass them to finish in grated coconut. 
  7. Reserve your coconut balls in the fridge. 

If you want a crisp shell: 
  1. Melt the white chocolate in a bain-marie or on a very low heat (it should not exceed 40 ° C) 
  2. Dip a ball of coconut in the melted white chocolate, pick it up with a fork and remove the excess chocolate by tapping 
  3. Then put the coconut ball in a bowl where there is grated coconut and roll it quickly. 
  4. Let cool for 2 hours. 
  5. Store in the refrigerator. 



[ ![homemade raffaello easy recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/raffaello-facon-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/raffaello-facon-maison.jpg>)
