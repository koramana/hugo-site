---
title: Portuguese salad with tuna and chickpeas
date: '2017-07-02'
categories:
- salads, salty verrines
tags:
- Potato salad
- Healthy cuisine
- Easy cooking
- Algeria
- inputs
- accompaniment
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/salade-portugaise-au-thon-et-pois-chiche.jpg
---
[ ![Portuguese salad with tuna and chickpeas](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/salade-portugaise-au-thon-et-pois-chiche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/salade-portugaise-au-thon-et-pois-chiche.jpg>)

##  Portuguese salad with tuna and chickpeas 

Hello everybody, 

Do you like potato and tuna salads? This Portuguese tuna and chickpea salad is ideal for you! For me it is the empty salad fridge and empty cupboard, especially when it's the end of the week, I'm not yet out to do the amp, and I do not know what to do with 3 potatoes that drag in the crate, or a box of tuna already started, or a box of chickpeas that I must consume as much as possible. This Portuguese salad with tuna and chickpea is the solution for that, and you will not be disappointed with the result that will please everyone. 

Of course, it's a recipe, because it's an empty cupboard recipe. It's a salad that I ate at school during "international food day", a day where moms of different nationalities come to share recipes from their country, and my friend very close **Anna** who is Portuguese of origin shared several recipes and delights, and this salad has me a lot more, especially since it is with red onion that I like a lot. What I liked the most is the addition of chickpeas in the salad, for me it was a first, and it was a super rich and nutritious salad, so surely I kept the recipe and that I have prepared it for every occasion. 

{{< youtube DepphcZBB6s >}} 

And here is the part of my husband, he took with him for his lunch at work, hihihihi 

[ ![Portuguese salad with tuna and chickpeas 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/salade-portugaise-au-thon-et-pois-chiche-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/salade-portugaise-au-thon-et-pois-chiche-5.jpg>)   


**Portuguese salad with tuna and chickpeas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/salade-portugaise-au-thon-et-pois-chiche-2.jpg)

portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 3 potatoes (or according to the number of people) 
  * 1 can of tuna in oil 
  * 1 red onion 
  * 1 glass of chickpeas cooked well 
  * a few sprigs of parsley (I really like parsley in this salad, but I did not have many) 
  * ½ glass of green olives 
  * 2 to 3 boiled eggs 
  * salt, extra virgin olive oil 
  * lemon juice (or vinegar of your choice, my Portuguese friend tells me that it is usually with red wine vinegar) 



**Realization steps**

  1. clean and peel the potatoes, then cut into medium cubes 
  2. salt and steam. 
  3. when the potatoes are cooked, put them in a salad bowl, 
  4. add the drained tuna from the oil, the chopped onion, the chickpeas, the chopped parsley, the olives and the sliced ​​eggs. 
  5. add a little salt, olive oil, and lemon juice, 
  6. mix gently and enjoy warm or cold, according to your taste. 



[ ![Portuguese salad with tuna and chickpea 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/salade-portugaise-au-thon-et-pois-chiche-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/salade-portugaise-au-thon-et-pois-chiche-3.jpg>)
