---
title: Cookies with peanut butter
date: '2008-03-04'
categories:
- Algerian cuisine
- diverse cuisine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/03/228376011.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/228376011.jpg)

Hello everybody 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/228376031.jpg)

Ingredients: 

  * 80 grams of butter 
  * 70 gr of peanut butter 
  * 70 gr of brown sugar 
  * 1 egg 
  * 2 cases of coarsely ground peanuts 
  * 1 packet of vanilla 
  * 1/2 teaspoon baking powder 
  * flour 



![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/228376091.jpg)

I mix the ingredients one by one to have a paste mole, then I add the flour little by little, to have a malleable paste, I have shaped several forms, balls, circles and blood sausages to toothless entwined. 

cook in a preheated oven for about 20 minutes 

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/03/228376171.jpg)

the result was delicious 

a refaire a la première occasion avec le tahina 
