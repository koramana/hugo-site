---
title: Mixed salad with rice and tuna / salad for ramadan 2017
date: '2017-05-11'
categories:
- ramadan recipe
- rice
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/salade-composee-au-riz-pour-ramadan-2013.CR2_1.jpg
---
![Mixed salad with rice and tuna / salad for ramadan 2017](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/salade-composee-au-riz-pour-ramadan-2013.CR2_1.jpg)

##  Mixed salad with rice and tuna / salad for ramadan 2017 

Hello everybody, 

When summer comes, my husband just asks me for salads for meals, and when it becomes a salad every day, we must vary and make it an almost complete meal ... 

This salad is made from rice, and peas with a good amount of tuna, to which is added a beet salad and another of carrot ... 

For more salad recipe for this ramadan, visit this article: 

###  [ menu of ramadan 2017 salads ](<https://www.amourdecuisine.fr/article-menu-du-ramadan-2017-les-salades.html>)

**Mixed salad with rice and tuna / salad for ramadan 2017**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/salade-de-ramadan-au-riz.CR2-copie-11.jpg)

**Ingredients**

  * 1 glass of pre-cooked rice in salt water 
  * ½ glass of peas steamed 
  * 100 gr of tuna 
  * 2 to 3 beets cooked in salt water 
  * 2 to 3 grated carrots 
  * green salad washed and drained. 
  * ½ glass of corn 
  * some pickles 
  * pitted green olives 
  * cherry tomatoes 
  * 3 to 4 tablespoons mayonnaise 
  * 1 cup of biker coffee. 
  * salt 
  * lemon juice 
  * olive oil. 



**Realization steps**

  1. prepare the sauce by mixing the mayonnaise, the biker, the lemon juice, the oil and the salt, beat well. 
  2. add in the rice, the peas, and the tuna, mix and put apart. 
  3. cut the beetroot into some, and season with lemon juice, oil and salt. 
  4. grated carrots, and season them also with lemon juice, oil and salt, usually I add a little garlic, according to the taste. 
  5. put the green salad in a large presentation dish. 
  6. beat the rice salad in a bowl and turn it in the middle. 
  7. decorate according to your taste, with beet salad, carrot salad, corn, tomato and pickles. 
  8. decorate with olives, boiled eggs if you want, and a little parsley and mayonnaise. 
  9. introduce the fresh salad. 


