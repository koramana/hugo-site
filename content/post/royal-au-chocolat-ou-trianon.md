---
title: Royal chocolate or trianon
date: '2014-12-23'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cakes and cakes
- sweet recipes
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/royal-au-chocolat-001_thumb1.jpg
---
![Royal chocolate or trianon](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/royal-au-chocolat-001_thumb1.jpg)

##  Royal chocolate or trianon 

Hello everybody, 

a delicious chocolate dessert all good, and all beautiful this Royal chocolate or trianon, directed by Lunetoiles, and she shared with us almost 2 years ago, I go back to the request of many of my readers and readers who were looking for this recipe, but to find a page not found, thank you my friends for mentioning it to me. 

So you have this delicious Royal chocolate or trianon, and thanks to Lunetoiles for the recipe. 

Note: 

I took my missing mold 28 cm in diameter, I removed the bottom, that's all, it served as a circle   


**Royal chocolate or trianon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/royal-au-chocolat-2_thumb1.jpg)

portions:  12  Prep time:  60 mins  cooking:  15 mins  total:  1 hour 15 mins 

**Ingredients** Chocolate mousse : 

  * 4 yellow 
  * 300 g of dark chocolate 
  * 550 g whole cream (35% fat) 
  * 40 g of sugar 
  * 3 c. tablespoons water 

Flaky Praliné: 
  * Praliné paste 2 tablespoon (about 200 g) 
  * 200 g of feuillantine or pancakes gavottes crumbled 

Chocolate biscuit (x2): 
  * 4 eggs 
  * 100 g of sugar 
  * 100g of flour 
  * 80 g of dark chocolate 
  * 20 g of butter 

Chocolate icing : 
  * 103 g of sugar 
  * 37 g of water 
  * 35 g unsweetened cocoa powder (Barry or VanHoutten) 
  * 71 g of liquid cream (35% of MG) 
  * 2 sheets of gelatin (I put 1) 



**Realization steps** Chocolate biscuits: 

  1. Melt chocolate and butter in the microwave. Separate the white from the yellows. Beating egg whites. 
  2. Beat the egg yolks with the sugar. Add the chocolate-melted butter mixture. Then gently whites in snow and finish with sifted flour. 
  3. Place it in the mold that will serve you for your Royal chocolate. Bake for 8 to 12 minutes at 180 ° C. 
  4. After leaving the oven, remove with the fingers the layer that formed on top. 
  5. To realize the day before and to place it in the expenses: it will be more unctuous. 

Flaky praliné: 
  1. Mix the two ingredients gently. 

Chocolate mousse : 
  1. Melt the chocolate in a bain-marie. 
  2. Beat the cream with a mixer. Reserve cool. 
  3. In a saucepan, bring the water and sugar to a boil. 
  4. Remove and pour on the yolks little by little while whisking vigorously until the mixture has doubled. 
  5. Add the chocolate and finish with the cream, going up little by little.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/royal-au-chocolat-2_thumb1.jpg)

Montage of royal chocolate: 
  1. Take a dish and put a film of food on it. Then place your stainless steel frame or circle on it. 
  2. Cut the outline of the biscuit. Put one of the two cookies to the bottom. Add the praline laminated. Spread well with the maryse. 
  3. Add the chocolate mousse, (keep a little for the top) and make sure it flows well on the sides by the space left by the biscuit whose edges have been cut. 
  4. Add the second biscuit and do not hesitate to push it a little. 
  5. Pour the remaining chocolate mousse and smooth. 
  6. Place the two good hours in the freezer at a minimum (one night in the ideal). 
  7. With a blowtorch or hair dryer, heat the outline of your mold to easily unmold and return to the freezer, time to make your frosting. 
  8. For the frosting, place your royal chocolate on a high container and put a dish on the bottom, so that it does not run on the table! 
  9. Pour in your icing and smooth slightly. Let the icing flow. Place the chocolate royal on a dish and put it back in the freezer so that the frosting is well fixed. 

Frosting: 
  1. Soak the gelatin in cold water. Bring to the boil water and sugar and leave approx. 3 minutes. In another saucepan, heat the cream. 
  2. Remove from heat and add the cocoa powder and mix. 
  3. Pour the cream before boiling and mix again. Add the gelatin, softened and well wrung. 
  4. Before icing, reheat for a few seconds in the microwave. Used at a temperature of 30 ° C max. 



Pas facile peut être à suivre toutes ces étapes, alors je partage avec vous la video que Lunetoiles a suivi pour réaliser ce gateau: 
