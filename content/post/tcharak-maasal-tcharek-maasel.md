---
title: Tcharak Maasal - Tcharek Maasel
date: '2012-04-14'
categories:
- boissons jus et cocktail sans alcool

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/corn-de-gazelle-au-nekkache-011_thumb-300x224.jpg
---
##  Tcharak Maasal - Tcharek Maasel 

Hello everybody, 

always in the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , here is a delight of the [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) , a [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) Honey, **tcharak maasal** , which exists in form without frosting: [ tcharek elariane ](<https://www.amourdecuisine.fr/article-tcharek-el-ariane-53899937.html>) , or covered with a nice layer of sugar: [ tcharek msakar ](<https://www.amourdecuisine.fr/article-tcharak-msakar-103050711.html>) , or a nice frosting with egg whites, [ iced tcharek ](<https://www.amourdecuisine.fr/article-soltan-el-meida-tcharek-glace-corne-de-gazel-avec-glacage-55370825.html>) this cake looks a lot like [ kaak Nakache ](<https://www.amourdecuisine.fr/article-kaak-nekkache-aux-amandes-82530128.html>) but the form differs, and the addition of honey completely changes its appearance. 

and if you like them [ Algerian cakes with honey ](<https://www.amourdecuisine.fr/categorie-12344741.html>) Visit the category. 

**Tcharak Maasal - Tcharek Maasel**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/corn-de-gazelle-au-nekkache-011_thumb-300x224.jpg)

Recipe type:  Algerian cake  portions:  30  Prep time:  30 mins  cooking:  15 mins  total:  45 mins  **Ingredients** For the dough: 

  * 3 measures of flour 
  * 1 measure of cooled melted butter 
  * Vanilla extract 
  * 1 pinch of salt 
  * Orange tree Flower water. 

For the stuffing: 
  * 3 measures of ground almonds 
  * 1 measure of powdered sugar 
  * 1 good c to c cinnamon powder 
  * Orange tree Flower water 

for decoration: 
  * a pincer, nekkache 
  * honey 
  * shiny food 



Prepare the dough with the given ingredients, pick up with orange blossom water and let stand a few minutes. 

In the meantime, prepare the stuffing by mixing all the ingredients. 

prepare balls of around 30 gr 

form a hollow in the ball and fill it with almond stuffing. 

enclose the ball and form a pudding, its length will be equal to your 6 fingers of both hands together. 

form a corn with it, and pinch it to your taste with the nekkache pliers, place your cakes on a baking sheet. 

to make leather in a preheated oven, and at the exit of the cakes of the oven, put them in hot honey, place in boxes and decorate with a little bright food 

merci pour vos commentaires, et bonne journee. 
