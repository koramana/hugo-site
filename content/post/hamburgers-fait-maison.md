---
title: homemade burgers
date: '2012-11-12'
categories:
- boulange
- pizzas / quiches / tartes salees et sandwichs

---
Hello everybody, 

at home, the kids keep asking for hamburgers, but so much I always do it fast, I never had the chance to take my hamburgers in photos. 

In any case, even this time, the time I prepare the hamburgers, I turn and I think they left me the smallest hamburger, hihihihi, we can barely see the slice of ground meat, so I am really sorry for the photo quality. 

but I wanted to share with you my homemade burgers, which I made with homemade hamburger buns, of which I already share the recipe with you. 

Ingredients: 

  *     * 4 [ buns for burger ](<https://www.amourdecuisine.fr/article-pain-pour-hamburgers-buns-extra-legers-112050290.html>)
    * 250 g minced meat (or depending on the number of people) 
  *     * 2 tbsp. chopped fresh parsley, 
    * 1 clove of chopped garlic 
    * 2 fresh tomatoes, 
    * 4 slices of cheese for burger 
    * 1 onion, finely chopped, 
    * 4 tablespoons mayonnaise, mustard, or melted cheese (according to your taste) 
    * a little olive oil for frying, 
    * green salad. 



method of preparation: 

  1. slice the tomatoes 
  2. cut the onion into thin slice 
  3. clean the salad, wash it and let it drain. 
  4. Mix the minced meat, parsley, crushed garlic, salt and black pepper. 
  5. divide the resulting mixture into four and form pucks. 
  6. Heat nonstick skillet over medium heat, and fry pucks for 5 minutes on each side until cooked through. 
  7. Cut the rolls in half. 
  8. spread with mayonnaise, and mix your burger, cheese, meat, sliced ​​tomato, salad. 
  9. Close the roll, repeat the same with the other rolls and serve immediately with fries. 


