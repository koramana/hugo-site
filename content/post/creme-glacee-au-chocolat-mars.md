---
title: Mars chocolate ice cream
date: '2017-08-27'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/06/creme-glacee-chocolat_thumb.jpg
---
##  Mars chocolate ice cream 

Hello everybody, 

with this infernal heat, nothing more refreshing than an ice cream .... 2 chocolates, chocolate, and chocolate mars. 

I give you a recipe for my friend Ouahiba (I hope you're fine my dear) 

The ingredients are: 

  * 4 egg yolks 
  * 150gr of sugar 
  * 300 ml of whole milk (it gives a soft ice even when it comes out of the freezer) 
  * 200ml of liquid cream 
  * Vanilla (in liquid pod or powder but not vanilla sugar) 
  * cocoa or melted chocolate 
  * one or two bars of chocolate Mars. 



Prepare a custard: 

  1. mix egg yolks, sugar and vanilla to have a voluminous and white mixture 
  2. add on it hot boiled milk 
  3. put back on the fire without stirring until the cream laps the back of the spoon when you draw a line on the back of the spoon with the finger the line remains clean (usually when the milk has boiled it takes 3 minutes maxi to cook), 
  4. remove immediately from the heat and allow to cool. (if you have put the vanilla in pods it is necessary to let rest one night the mixture to well infuse and the following day you withdraw the pod and you finish the preparation) 
  5. Once cold add the fresh cream mixes and put in the freezer until it is very cold it should not become an ice cube but a very cold liquid that facilitates the work for the ice cream maker, 
  6. you put the ice cream maker on the road and pour in the cream. 
  7. Once ready, put the ice in the freezer. 
  8. frozen MARS bars, spray with the blender and added to the vanilla ice cream, 



thanks for your comments, and messages 

have a good day   


**Mars chocolate ice cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/creme-glacee-chocolat_thumb.jpg)

**Ingredients**

  * 4 egg yolks 
  * 150gr of sugar 
  * 300 ml of whole milk (it gives a soft ice even when it comes out of the freezer) 
  * 200ml of liquid cream 
  * Vanilla (in liquid pod or powder but not vanilla sugar) 
  * cocoa or melted chocolate 
  * one or two bars of chocolate Mars. 



**Realization steps**

  1. Prepare a custard: 
  2. mix egg yolks, sugar and vanilla to have a voluminous and white mixture 
  3. add on it hot boiled milk 
  4. put back on the fire without stirring until the cream laps the back of the spoon when you draw a line on the back of the spoon with the finger the line remains clean (usually when the milk has boiled it takes 3 minutes maxi to cook), 
  5. remove immediately from the heat and allow to cool. (if you have put the vanilla in pods it is necessary to let rest one night the mixture to well infuse and the following day you withdraw the pod and you finish the preparation) 
  6. Once cold add the fresh cream mixes and put in the freezer until it is very cold it should not become an ice cube but a very cold liquid makes it easier for the ice cream maker, 
  7. you put the ice cream maker on the road and pour in the cream. 
  8. Once ready, put the ice in the freezer. 
  9. frozen MARS bars, spray with the blender and added to the vanilla ice cream, 


