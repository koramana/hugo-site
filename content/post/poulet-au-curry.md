---
title: chicken curry
date: '2016-11-08'
categories:
- Indian cuisine
- Cuisine par pays
tags:
- dishes
- Meat
- Ramadan
- Today's special
- Healthy cuisine
- Easy cooking
- Full Dish
- India

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/poulet-au-curry.jpg
---
![chicken curry](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/poulet-au-curry.jpg)

##  chicken curry 

Hello everybody, 

Want new flavors, want a different touch to your kitchen, want a culinary escape by the cold .... Direction India for a chicken curry well piquant that will give you sweat in this cold 

The first time I made this dish years ago, I was waiting for my husband's reaction with my eyes and ears wide open. He who loves chicken curry and who often ate dishes from his favorite restaurant, he liked the recipe so much that since then he no longer bought chicken curry, he even gave me a good grade by saying to me in addition your curry and a thousand times more good, because it is not sweet like that of the restaurant .... ouiiiiiiiiiiiiiiiiiii. 

To serve this delicious chicken curry with [ homemade pita breads ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html>) or some [ Naan bread ](<https://www.amourdecuisine.fr/article-les-naans-a-la-coriandre.html>) . 

![Chicken au curry-2](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/poulet-au-curry-2.jpg)

**chicken curry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/poulet-au-curry-2.jpg)

portions:  4  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients** for the marinade: 

  * Chicken pieces of your choice 
  * 150 ml of natural yoghurt 
  * 1 C. vinegar 
  * 1 C. grated fresh ginger 
  * 1 C. grated garlic 
  * 1 C. tablespoons of massala powder (coriander powder, spicy powder, grains of fennel powder, cardamom powder, caraway powder, fenugreek powder, cloves powder, black pepper powder) 

for the dish: 
  * 1 medium onion, finely chopped 
  * 1 C. chopped garlic 
  * 1 C. grated ginger 
  * 4 crushed cardamoms 
  * 1 Bay leaf 
  * 1 cinnamon stick 
  * 1 C. tomato paste 
  * 500 ml of water 
  * 1 C. coffee salt and black pepper (according to taste) 
  * 3 small red peppers dried, soaked in water and then crushed 



**Realization steps**

  1. prepare marinade one night before or at least 6 hours before cooking. 
  2. Wash the chicken without the skin, place the pieces in a large bag or large bowl with the following ingredients: yogurt, ginger, crushed garlic, massala powder and vinegar. 
  3. massage the chicken pieces well with the marinade cover and place in the refrigerator. 
  4. The next day, in a large saucepan over medium heat, sauté the onions for 2 minutes in a little olive oil. 
  5. then add ginger, bay leaf, cardamom, cinnamon, salt and pepper and cook for at least 3 minutes. 
  6. Add chicken and crushed red peppers and mix well for about 5 minutes. 
  7. Now add the tomato paste and the garlic paste, then cook until you start to see the juice and the sauce begins to thicken, then add the water. 
  8. simmer over low heat for 45 to 50 minutes (add water as needed without too much water). 
  9. Serve the dish with homemade naans 



![chicken-curry-in-1](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/poulet-au-curry-1.jpg)
