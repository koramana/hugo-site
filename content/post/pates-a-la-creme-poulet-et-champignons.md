---
title: Pasta with chicken and mushroom cream
date: '2018-02-06'
categories:
- diverse cuisine
- Cuisine by country
tags:
- Healthy cuisine
- Italy
- dishes
- Full Dish
- Easy cooking
- Dinner
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/02/P%C3%A2tes-%C3%A0-la-cr%C3%A8me-poulet-et-champignons.jpg
---
![Pasta with chicken and mushroom cream](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/P%C3%A2tes-%C3%A0-la-cr%C3%A8me-poulet-et-champignons.jpg)

##  Pasta with chicken and mushroom cream 

Hello everybody, 

Here is the type of dish I like most, pasta cream, today is the version of pasta cream with chicken and mushrooms, or pasta with alfredo sauce. Personally that's what I like to prepare the most for noon. You can see elsewhere [ pasta with smoked salmon ](<https://www.amourdecuisine.fr/article-pates-au-saumon-fume-facile-et-rapide.html>) . 

It is a very easy dish to prepare and very fast, especially when you are in a hurry, but what is sure is that you will put a good gourmet dish on your table in less than 30 minutes. 

So I pass you this easy recipe pasta with creamy alfredo sauce with chicken and mushrooms. And you can see the version: tagliatelle with chicken and mushrooms cream on my youtube channel: 

**Pasta with chicken and mushroom cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/pates-au-poulet-et-champignons-%C3%A0-la-sauce-cr%C3%A9meuse-2.jpg)

portions:  2 

**Ingredients**

  * 1 chicken breast cut into pieces 
  * 2 cloves garlic 
  * 1 glass of liquid cream 
  * 100 gr of fresh mushrooms or ½ box of mushrooms 
  * Salt pepper 
  * 300 gr of vices dough 
  * 1 handfuls of grated cheese 
  * 1 C. butter 
  * 1 C. tablespoon of olive oil 
  * chopped parsley 
  * chopped chives. 



**Realization steps**

  1. place the pasta in boiling salted water and cook as recommended on the package. 
  2. Start by sautéing the garlic in the butter and oil 
  3. add the chicken breasts and sauté until you have a nice color. 
  4. add the mushrooms cut into strips. 
  5. fry until the sauce is completely reduced. 
  6. add salt and pepper. 
  7. remove this mixture from the stove, and pour the cream into the pan to degrease all the juice of the cooking. 
  8. put the chicken and mushrooms in this cream and add salt, chopped parsley and chopped chives. 
  9. let back just a moment to remove the stove 
  10. enjoy while it's still hot. 



![pasta with chicken and mushrooms in a creamy sauce](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/pates-au-poulet-et-champignons-%C3%A0-la-sauce-cr%C3%A9meuse.jpg)
