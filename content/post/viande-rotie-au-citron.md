---
title: roasted meat with lemon
date: '2011-11-18'
categories:
- diverse cuisine
- Healthy cuisine
- Dishes and salty recipes
- ramadan recipe
- riz

---
Hello everybody, 

a way of cooking the leg of meat, very quickly, and have a tender meat to wish, 

in addition a cooking that will not take more than 30 minutes. 

I strongly recommend this method to have a meat too good: 

ingredients: 

  * 4 to 5 pieces of medium sized leg of meat cut 
  * the juice of a lemon (more or less, according to your taste for lemon) 
  * salt 
  * black pepper 
  * 1/4 cup of cardamom coffee powder 
  * 1/4 teaspoon cinnamon 
  * 2 cloves garlic crushed 
  * 1/2 teaspoon coriander powder 
  * 4 tablespoons of oil 



method: 

  1. marinate the meat in the sauce prepared with the ingredients at the top 
  2. leave almost 1 hour 
  3. in a pot, over low heat, put the oil 
  4. put on the meat and cover 
  5. try each time to turn the meat, at the beginning the meat will reject its water, always leave covered, after, this sauce will be reduced, and the meat will begin to roast, 
  6. watch for a nice color. 



puis que c’est une journee de recette facile, je l’ai presenter pour mes enfants avec des frittes, et pour mon epoux et moi d’une creme piquante au beurre de cacahuettes, recette dans l’article prochain. 
