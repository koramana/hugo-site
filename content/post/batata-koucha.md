---
title: batata koucha
date: '2013-01-22'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours

---
Hello everybody 

very easy to do, and very delicious,    
Ingredients:   


\- potatoes 

\- 1 onion 

\- 1 clove 

\- 1 handful of chickpeas, or more according to your taste 

\- parsley 

\- oil 

\- salt and black pepper 

clean the chicken, or chicken pieces, and put them in a pot, add the oil, the finely chopped onion, and the crushed garlic, cook over low heat, add a little parsley, cover all about 1 liter of water, and cook. 

now, peel and clean the potato, cut it in length, a little wider than the normal fries, salt it a little bit. and go to the frying. 

when the chicken is cooked, take it out of the sauce, take an oven-baking tin, place in the frits, sprinkle with half of the sauce, cover the pieces of chicken, and place in a hot oven, to give a beautiful color. 

for people who love eggs, you can beat an egg and pour it on the surface, otherwise, it's like my husband, leave like that. 

before serving, sprinkle each dish with a little sauce, and garnish with chickpeas and parsley. 

bonne dégustation. 
