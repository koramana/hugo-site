---
title: flights to the bolognese wind "طاير في الريح"
date: '2010-04-24'
categories:
- cuisine algerienne
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/04/231747351.jpg
---
sometimes when we start the interminable cleaning in the morning, and between the showers of the children, and the linen to tender, we forget to prepare lunch, we see the time and it is already 11:30, Mister R will be here minute, and I have not done anything yet ........... oops I open the fridge, my god that I'll do, even with a pressure cooker, it's never going to be ready. I have a little puff pastry, ah I have bolognese, in my freezer since the last time, I think a little, and I tell myself flights to the wind with & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.5  (  1  ratings)  0  ![](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/231747351.jpg)

sometimes when we start the interminable cleaning in the morning, and between the showers of the children, and the linen to tender, we forget to prepare lunch, we see the time and it is already 11:30, Mister R will be here minute, and I have not done anything yet ......... .. 

oops I open the fridge, my god that I'll do, even with a pressure cooker, it's never going to be ready. 

I have a little puff pastry, ah I have  [ the Bolognese  ](<https://www.amourdecuisine.fr/article-25345436.html>) In my freezer since the last time, I think a little, and I think the wind blowing with a salad will please my husband a lot. 

and so I shape my flight to the wind with the puff pastry and hop in the oven already preheated to 180 degrees (I had no time to take pictures, sorry) 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/231747301.jpg)

the time it's cooking. 

I take out my bolognese I take the quantity I need 

when cooking my flights to the wind I stuff them with Bolognese, garnish them with grated Gruyere, and again, for 5 minutes or less in the oven, just to melt Gruyere cheese, and let the Bolognese be a good temperature 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/04/231747351.jpg)

I serve this with a good green salad 

and my husband was very happy. 

I too, because they were very very delicious. 

each his taste in the farce of flights to the wind ... 

And you, which is yours??? 

I would like to find recipes like that good and simple 

to face the last minute ......... 

bon Appétit 
