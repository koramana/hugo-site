---
title: Successful zlabia for Ramadan 2018
date: '2018-05-18'
categories:
- Algerian cuisine
- Cuisine by country
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries
tags:
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/zlabia-r%C3%A9ussie-pour-ramadan-2017-a.jpg
---
![Successful zlabia for Ramadan 2018](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/zlabia-r%C3%A9ussie-pour-ramadan-2017-a.jpg)

##  Successful zlabia for Ramadan 2018 

Hello everybody, 

And here is the zlabia recipe that's been 100% successful for this Ramadan 2018. I admit that since I've been here in England, and that I do not find the magazines that sell zlabia, I always try to make a small amount for ramadan. 

I tried lots and lots of recipes, which I found here and there on the internet or youtube, but I was never satisfied with the result until yesterday, when I came across on a girl's recipe: **Minou basmala** , who shared a recipe very different from the one I used to see. 

a zlabia with yoghurt, I told myself that maybe with the yoghurt in the dough, we will feel the good taste of fermentation that we have in the real zlabia, or the one we are used to. This zlabia is just top, I will try to find the next time a tip to have zlabia less fine, but otherwise with the small amount of flour that I used, I had a good rendering of zlabia, I given to neighbors and friends, the children ate at will (and me too) and there is a little left. 

**zlabia successful for ramadan 2017**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/zlabia-r%C3%A9ussie-pour-ramadan-2017-c.jpg)

**Ingredients**

  * 2 glasses minus 2 tablespoons all-purpose flour (one glass of 250 ml) 
  * 2 tablespoons cornflour 
  * 1 C. baking yeast. 
  * 1 C. tablespoons sugar 
  * 1 C. tablespoon natural yoghurt 
  * 1 C. orange blossom water 
  * 1 pinch of salt 
  * 1 glass and a half of warm water (+/- depending on the quality of the flour) 
  * food coloring according to taste (red or orange) 



**Realization steps**

  1. all the ingredients are mixed, and the water is introduced in a small quantity. 
  2. when the dough is at the right consistency, cover it and let stand at least 1 hour. 
  3. preheat a bath of oil not too deep 9 of almost 1 cm deep no more) 
  4. place the dough in a bottle having an opening of maximum 8 mm no more. 
  5. shape dough swirls from the center to the end and close by drawing a line on the diameter of the circle formed so that the swirls hold up. 
  6. cook the zlabia on both sides until you have a nice color that is hard on the surface. 
  7. dive directly into honey and let drain on a dish. 
  8. You can keep the zlabia in a hermetically sealed box for 3 days. 



![zlabia successful for ramadan 2017 b](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/zlabia-r%C3%A9ussie-pour-ramadan-2017-b-710x1024.jpg)
