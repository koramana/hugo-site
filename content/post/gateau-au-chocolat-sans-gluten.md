---
title: gluten-free chocolate cake
date: '2014-12-10'
categories:
- birthday cake, party, celebrations
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/gateau-d-anniversaire-au-chocolat-7_thumb.jpg
---
##  gluten-free chocolate cake 

Hello everybody, 

here is a very nice cake all **gluten-free chocolate** , that Lunetoiles has prepared for a special occasion at home and that she loved to share with us, a **fluffy chocolate cake without gluten** , a fondant all chocolate without gluten, then treat yourself even if you are **gluten intolerant** , so you have a **gluten allergy** there are plenty of [ gluten-free recipes ](<https://www.amourdecuisine.fr/article-tag/sans-gluten>)

This gluten-free chocolate cake is too good, and I'm sure too good too, and so I liked the cake, I asked Lunetoiles to do my part, it's the one downstairs, so nobody do not touch .. ok !! 

Personally when I saw the pictures of the cake, I thought it was a **[ Genoese ](<https://www.amourdecuisine.fr/article-genoise-inratable.html> "unbreakable genesis") in chocolate ** decorated with a chocolate mousse, but in fact, the cake is a kind of [ chocolate mousse ](<https://www.amourdecuisine.fr/article-recette-mousse-au-chocolat-facile.html> "easy chocolate mousse recipe") Eggs this time, so you imagine the fondant. I think I'll eat this cake alone in 10 minutes, which is better? lol, ah ... a treat, is not it? 

**gluten free chocolate cake / birthday cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/gateau-d-anniversaire-au-chocolat-7_thumb.jpg)

Recipe type:  gluten free  portions:  8  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 250 g of chocolate 
  * 250 g of sugar 
  * 250 g of butter 
  * 9 egg yolks 
  * 5 whites 



**Realization steps**

  1. Preheat the oven to 170 ° / 180 ° C (th.6). 
  2. Butter a 24 cm hinged mold. 
  3. Melt the chocolate in a bain-marie. And add the melted chocolate butter. Mix well. 
  4. Whisk the sugar and egg yolks until the mixture whitens and doubles. 
  5. Add the butter plus melted chocolate to the sugar mixture, yellow. 
  6. Beat the egg whites and add 1 pinch of salt. 
  7. Add a spoon of these whites to the chocolate mixture without precaution to relax the mixture. 
  8. Then incorporate the rest of the whites gently. 
  9. Pour half of the mixture into the mold. 
  10. Bake 45 minutes (watching) 
  11. Allow the cake to cool well before covering it with the remainder of the preparation still raw. 
  12. Reserve at least 12 hours. 
  13. Décercler. 
  14. Sprinkle with bitter cocoa and surround the cake with small chocolate biscuits. (for me chocolate lace pancakes) 


