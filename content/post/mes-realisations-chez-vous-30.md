---
title: my achievements at home 30
date: '2012-11-25'
categories:
- cakes and cakes

image: http://amour-de-cuisine.com/wp-content/uploads/2012/11/Photo-1758.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

I pass the apron to my faithful readers in this article, to publish their recipes prepared from my blog. 

it's really beautiful to see all these people taking inspiration from my small achievements, 

And if you too were tempted by one of my recipes, send me the picture on this email: 

[ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)

[ Apple cake ](<https://www.amourdecuisine.fr/article-30944674.html>) at Malika's. 

[ Norman apple pie ](<https://www.amourdecuisine.fr/article-tarte-normande-aux-pommes-111845453.html>) at Mira M 

![](https://www.amourdecuisine.fr//import/http://i49.servimg.com/u/f49/11/39/90/42/)

[ hamburger bread ](<https://www.amourdecuisine.fr/article-pain-pour-hamburgers-buns-extra-legers-112050290.html>) at malika 

Bravo my dear soulef I made the buns this morning is a killer, I made big hamburger hummmmm. A delight for children and even dads is very good 

Malika. 

[ homemade hamburger ](<https://www.amourdecuisine.fr/article-hamburgers-fait-maison-112343938.html>) at malika (malika made this recipe well before mine) 

[ magic cake ](<https://www.amourdecuisine.fr/article-gateau-magique-au-chocolat-et-creme-caramel-107207656.html>) at malika 

![Picture 1758](http://amour-de-cuisine.com/wp-content/uploads/2012/11/Photo-1758.jpg)

![DSCN0731](http://amour-de-cuisine.com/wp-content/uploads/2012/11/81146766.jpg)

[ Arayech with honey ](<https://www.amourdecuisine.fr/article-arayech-gateau-algerien-au-miel-112520776.html>) in [ Ranouza ](<http://ranouzarecettes.canalblog.com>)

![](https://www.amourdecuisine.fr//import/http://3.bp.blogspot.com/-KIkEgJWNdos/UK8vrZbiX0I/AAAAAAAANqc/jktLd0Nm6t8/s1600/)

[ mkhabez ](<https://www.amourdecuisine.fr/article-mkhabez-109018263.html>) at Moha E 

[ chocolate cupcakes ](<https://www.amourdecuisine.fr/article-cupcakes-spartacus-112250629.html>) at Assia K 

[ mini mouskoutchou ](<https://www.amourdecuisine.fr/article-minis-mouskoutchou-a-l-orange-112244829.html>) at Oumira 

[ snowballs ](<https://www.amourdecuisine.fr/article-gateaux-algeriens-a-la-noix-de-coco-aid-el-kebir-2012-111495668.html>) at Charlotte 

[ makrout elouz ](<https://www.amourdecuisine.fr/article-makrout-el-louz-111801152.html>) at Charlotte 

[ chocolate fingers without cooking ](<https://www.amourdecuisine.fr/artic%0Ale-gateau-algerien-gateau-sans-cuisson-doigts-au-chocolat-107284273.html>) at charlotte 

[ the mkhidettes ](<https://www.amourdecuisine.fr/article-mkhedette-laaroussa-l-oreille-de-la-mariee-57147864.html>) chez Charlotte 
