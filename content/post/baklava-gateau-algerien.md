---
title: baklava cake Algerian
date: '2013-03-17'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/baklawa-aux-noix-nobles1_thumb1.jpg
---
##  baklava cake Algerian 

Hello everybody, 

baklava cake Algerian, or as it is pronounced in Arabic baklawa is a delicious heritage of Algerian cuisine, which finds its success on the party tables. 

At home in Constantine, if the baklava or baklawa is not present in a wedding party, or in the cake tray of Eid is that you missed a big point of the family tradition, moreover at home if there are only three days left for Eid and I would not have done the baklawa, my husband will start to make head, hihihihi 

Today for this baklava Lunetoiles liked to make the recipe of Mouni. 

![baklava](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/baklawa-aux-noix-nobles1_thumb1.jpg)

**baklava cake Algerian**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/baklawa-aux-noix-nobles2.jpg)

**Ingredients** Pate: use a measure, the same all along the recipe, for example a 300ml bowl or a 250ml glass (the larger the container is and the larger the proportions will be) 

  * 5 measures of flour 
  * 1 measure of melted butter 
  * ½cc of salt 
  * 1 egg 
  * water (possibly with added orange blossom water) 

Prank call: 
  * 3 measures of coarsely ground walnuts: or noble walnuts: cashews, walnuts, almonds and hazelnuts 
  * a little less than one measure of powdered sugar 
  * 2 teaspoons of cinnamon (see more according to taste) 
  * 3 - 4 tablespoons of melted butter 
  * Orange tree Flower water 

Assembly: 
  * Melted butter 

Finish: 
  * Melted honey with orange blossom water (about 1kg of honey) 



**Realization steps**

  1. Start by making the dough, for that mix all the ingredients by adding little by little water to collect the dough, 
  2. divide it into 12-14 small balls, half will serve for the lower and upper layer of dough, cover them with cling film and let them rest minimum 30min, ideally 1h. 
  3. meanwhile prepare the stuffing by mixing all the ingredients by adding a little orange blossom, reserve. 
  4. take a ball of dough and start to spread it with a roll, the dough must be so thin that you have to see the worktop through it, in a previously buttered tray, gently put it down, then brush it with butter . 
  5. Repeat this operation with the other 5 sheets (or 6 if you have chosen the "14 balls" option), taking care to butter between each sheet and chasing any air bubbles with a curing agent. tooth 
  6. then arrange the filling with a light pressure and then cover with the rest of the leaves as for the first layer   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/2010-11-15-baklawa_thumb.jpg)
  7. then cut into lozenges and plant half almonds (or whole, one pinion) in each diamond, bake in oven preheated to 175 ° for about 45min-1h depending on the size of the tray, it is necessary to monitor and adapt according to the power of the oven and the thickness of the baklawa, 
  8. As soon as the top is golden brown, remove the tray from the oven and gently brush with honey syrup before putting it back in the oven, ideally let the baklawa rest for 12 hours before cutting it 


