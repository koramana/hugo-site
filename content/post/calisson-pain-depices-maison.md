---
title: calisson homemade gingerbread
date: '2015-12-24'
categories:
- Cupcakes, macaroons, and other pastries
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid
- sweet recipes
tags:
- Confectionery
- Melon
- biscuits
- Almond paste

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/calissons-au-pain-depices-1.jpg
---
###  [ ![calisson homemade gingerbread](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/calissons-au-pain-depices-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/calissons-au-pain-depices-1.jpg>)

##  calisson homemade gingerbread 

Hello everybody, 

**calisson homemade gingerbread** : Before you pass the recipe for these delicious calisson homemade gingerbread, I tell you my story with the calissons. The first and only time o _ù_ I ate calissons d'Aix, was when I went to Strasbourg for my studies of Magister in chemistry, I remember that I arrived there during the Christmas season, and one of my colleagues The time gave me a box of calissons, I must say that it was the first time that I received a Christmas gift, because with us we do not celebrate. I could not say no, especially not for a box of cakes that I have never tasted, um hum. 

The taste has remained in my mouth to this day, and I must thank this time Lunetoiles who sent me a while ago, a nice gift box has calissons, containing a book with lots of recipes various sweet and salty calissons  Calissons Maison Calves' Sweet  & Aperitifs "by José Maréchal  , 4 pieces of different sizes, a spatula, and a mini pastry roll. That's not to stop here, but it also sent me candied fruits, such as candied melon, candied kiwi, candied kumquats, ... .the list is long to name anything. I can not find candied fruits here at home. 

The most important thing is that these Lunetoiles gifts have allowed me to make calissons for all tastes, and above all, to make **homemade calissons from Aix.** Moreover, since my daughter has eaten, now she does not stop to claim calissons, and in addition, it is she who chooses the recipe from the photos on the book, before she asked them much [ arayeches ](<https://www.amourdecuisine.fr/article-arayeche-larayeche-aux-pistaches-gateau-algerien.html> "arayeche, pistachio larch with Algerian cake") . 

One important thing that I have to tell you, I do not use the unleavened leaf to make my calissons, I use rather rice leaves, I'm not yet falling on rice leaves of good quality because they are a little thick, so making calissons is a little hard, and the other thing, I do not know what is called the unleavened leaf in English to buy, lol. 

Now we go to the recipe of these homemade gingerbread calissons, which are super good, and I'm sure you've already met, for my part, I made these delights with [ homemade gingerbread ](<https://www.amourdecuisine.fr/article-pain-d-epice-tres-moelleux-et-trop-bon.html> "fluffy gingerbread") because here in England I can not find gingerbread. 

###  [ ![calisson homemade gingerbread](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/calissons-au-pain-depices-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/calissons-au-pain-depices-4.jpg>)

[ ![calisson homemade gingerbread](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/calissons-au-pain-depices-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/calissons-au-pain-depices-2.jpg>)
