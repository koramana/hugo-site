---
title: Arayech, Algerian honey cake
date: '2012-11-16'
categories:
- boulange
- crepes, beignets, donuts, gauffres sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-au-miel-2_thumb1.jpg
---
Hello everybody, 

here is a super nice Algerian cake, arayechs with honey,  the younger sister of  [ arayechs at the glacages ](<https://www.amourdecuisine.fr/article-25345469.html>) . 

the pasta is very easy to make, and above all, very very good. The recipe for the pasta is from the book: Oriental Cakes, Step by Step 

the ingredients without delay: 

250 gr of flour 

75 gr of margarine 

1 pinch of salt 

1 teaspoon of vanilla 

1 teaspoon of white dye 

1 cup of dye (color of your choice, here red, white and blue) 

Orange blossom water + water 

the joke: 

250 gr of ground almonds 

125 grams of sugar 

1 C. a vanilla coffee 

1 egg 

garnish: 

honey 

colorful pearl 

Pour the flour in a bowl, add the margarine, vanilla, salt. 

Mix well with your fingertips 

divide the dough into two parts 

for the one you are going to color in white add the dye, then pick up the dough with flower water, add a little water to pick up, continue to knead a few seconds. 

for the other color, take the other half of the shortbread, add the dyes to the orange water until the desired color is obtained, and start to collect the dough with this water, add the water if you need, and knead a few seconds. 

cover your dough and let it sit on the side. 

for stuffing mix almonds and dry ingredients, collect with egg and set aside. 

lower the dough and pass it to the machine from number 3 to number 7, cut two circles of dough, the first of the dark color with a cookie cutter 8 cm in diameter. 

the second of the white color with a 9 cm diameter cookie cutter. 

then superimpose them, the big one on the small 

take a ball of 25 grams of the almond stuffing, form a star, place it on the circle of the dough, and pick up the sides of the dough to coat the almond stuffing, without covering it completely, press well so that the dough is well stick to the stuffing, and form well the ends of the star. 

decorate with small flowers 

and cook for 10 minutes in a preheated oven at 170 degrees C. 

![arayeche with honey 2](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-au-miel-2_thumb1.jpg)

take it out of the oven, let cool a little, and emerge your cakes in a little hot honey. 

now decorate the small flowers with pearls. 

enjoy. 

the model of this cake is from the book: Cakes and Decoration 1 by Mrs. Lila Bounazo. 

merci pour vos visites, merci pour tout vos commentaires, et merci pour vos abonnements de plus en plus intense a ma newsletter, ca me fait réellement plaisir. 
