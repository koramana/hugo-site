---
title: chicken skewers tikka massala
date: '2017-07-30'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/chikken-tikka1.jpg
---
[ ![chicken-tikka 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/chikken-tikka1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/chikken-tikka1.jpg>)

##  chicken skewers tikka massala 

Hello everybody, 

a traditional recipe from India's spice country. Like most Indian recipes, the chicken is spicy but not spicy, a recipe that prepares in two three strokes, and after a good time in the marinade, you will enjoy grilled kebabs, and succumb to each bite. 

**Chicken Tikka Massala**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/poulet-tikka1.jpg)

portions:  4  Prep time:  20 mins  cooking:  5 mins  total:  25 mins 

**Ingredients**

  * 750 g of chicken breast 
  * 150 g plain yoghurt 
  * 1 tablespoon grated ginger 
  * 2 crushed garlic cloves 
  * ½ teaspoon of cayenne pepper 
  * 1 tablespoon coriandremoulus beans 
  * ½ teaspoon of salt 
  * the juice of a lemon 
  * 2 tablespoons of oil 



**Realization steps**

  1. With the exception of chicken, mix all the ingredients in a bowl. 
  2. Cut the chicken into cubes that you will soak in the previous melenge. 
  3. Cover and marinate overnight in the refrigerator. 
  4. Divide the chicken over 4 skewers and cook for 5 to 6 minutes under the grill, turning often. 
  5. Arrange the skewers on the plates, decorate with lemon, parsley and serve, or with a good salad 



![2012-02-11-chicken-tikka.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/2012-02-11-chikken-tikka1.jpg)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/visitecomm3.png)
