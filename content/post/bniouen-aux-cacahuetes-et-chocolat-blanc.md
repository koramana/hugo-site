---
title: Benuouen with peanuts and white chocolate
date: '2017-06-29'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian cakes
- Algeria
- Algerian patisserie
- delicacies
- Aid
- Ramadan 2017
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/06/Bniouen-aux-cacahu%C3%A8tes-et-chocolat-blanc-1.jpg
---
![Benuouen with peanuts and white chocolate 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/Bniouen-aux-cacahu%C3%A8tes-et-chocolat-blanc-1.jpg)

##  Benuouen with peanuts and white chocolate 

Hello everybody, 

Here is the recipe for bnouien peanuts and chocolate that I forgot to post for more than a year. I made this delicious cake without cooking last year, but forgot to put the recipe online! 

At the request of the recipe by a large number of my readers during the preparations for Eid el fitr 2017, I took the opportunity to redo the recipe for this peanut and white chocolate bniouen, video. 

Generally I make this recipe at random, but here I am going to give you the measures, while advising you all the same to adjust the ingredients according to your taste, it is a cake without cooking so you can taste while mixing and know what lack or what is more. 

**Benuouen with peanuts and white chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/Bniouen-aux-cacahu%C3%A8tes-et-chocolat-blanc-2.jpg)

**Ingredients**

  * 1 package of dry biscuits (digestive chocolate for me 500 gr) reduced to powder 
  * \+ or - 3 c. curved butter at room temperature 
  * 2 tbsp. curved Turkish halwa soup 
  * 2 tbsp. cocoa powder 
  * \+ or - 2 tbsp. honey 
  * 200 gr roasted and crushed peanuts 
  * white chocolate to garnish cakes 



**Realization steps**

  1. we are going to place the biscuit powder in a large salad bowl. 
  2. Peanuts, Turkish halwa and powdered cocoa are added. 
  3. mix well by hand, then introduce the butter and introduce it by hand, 
  4. the honey is also added, and we continue to crush with the hand to homogenize all the mixture, if necessary if the mixture does not collect, we add butter or honey, according to the taste. 
  5. the paste is placed in the cavities of a silicone mold, and it is kept cool for 15 minutes to harden. 
  6. we remove the expense, and we melt the white chocolate in a water bath or microwave. 
  7. the pieces of cakes are removed from the mold, and the bottom of the cavities is covered with a little chocolate. 
  8. directly we put the cakes in the molds, so that the white chocolate sticks. 
  9. we put in the fridge for another 15 minutes. 
  10. the cakes are removed, placed in boxes, and placed in a hermetic box, the layers of cakes can be separated, not the food film. 
  11. we put the box of cakes in the fridge, because these cakes that quickly become very soft. 



![Benuouen with peanuts and white chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/Bniouen-aux-cacahu%C3%A8tes-et-chocolat-blanc-683x1024.jpg)
