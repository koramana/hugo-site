---
title: Cream of Brown ............ .house
date: '2009-06-21'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/cremem81.jpg
---
It must be said that by reading so much recipe with brown cream, I really wanted to taste this delight, and why not try these beautiful recipes. 

Here in England, there is no cream of chestnut or maybe I have a bad search .................. with the translation must say it's not easy. 

So all I could find is the mashed chestnut, not serious it's the case as long as there is the recipe. 

So, I'm sure there will be a lot of girls here in England who are going to be happy, so we go to the recipe   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/cremem81.jpg)

Put the sugar in a large pot and dilute with a little water to obtain a thick syrup of sugar.   
Cook on high heat until you reach a large bowl (120 degrees C) 

Add the chestnut puree. Stir quickly. Continue to cook on high heat, the cream of brown will thicken and darken after about 15 minutes. 

To test the thickness of the chestnut cream, it is possible to put a ladle in a plate or dish and turn it on its side. If it stays in place, it is ready. 

Stop the fire, then fill the glass jars initially prepared. Fill them to the brim, close them immediately and fill them upside down. This avoids any risk of mold. 

Keep in a cupboard and go out for any occasion. Serve with white cheese or cream, spread on a slice of bread for breakfast 

for those who do not have mashed chestnut it is necessary to put the chestnuts in a big pressure cooker.   
Fill it with water up to the chestnuts above (without covering them). Put the vanilla pod, then close the casserole. Put it on high heat and then heat softly when the casserole begins to whistle. Cooking takes about 10 to 15 minutes. 

Using a blender, purée the chestnuts. It is then necessary to weigh this purée to determine the exact weight of sugar which one will need, knowing that one needs 750 g of sugar per 1 kg of mashed chestnuts (3/4 of the weight of puree). 

en tout cas je vous le dit, ne perdez pas l’occasion de goutter a ça. 
