---
title: The Hnifiettes delicious Algerian cake حنيفيات
date: '2011-11-03'
categories:
- dessert, crumbles et barres
- recettes sucrees
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/hnif-1_thumb1.jpg
---
##  The Hnifiettes delicious Algerian cake حنيفيات 

**The Hnifiettes delicious Algerian cake حنيفيات**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/hnif-1_thumb1.jpg)

portions:  30  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 1 k of flour 
  * 250 g melted butter 
  * 3 bags of yeast 
  * 2 whole eggs 
  * 1 teaspoon of vanilla 
  * Pick it up with milk. 



**Realization steps**

  1. Let stand for half an hour. 
  2. Make balls 3 to 4 cm in diameter. Roll out the dough, then pass to the dough machine at number 4, then to numbers 7 and 9. Sprinkle the dough with corn starch. 
  3. Roll like a cigar. Cut pudding about 4 cm (for my part I prefer 2 cm, because for cooking the better it is small) 
  4. Pinch with your thumb in the direction of the diagonal of the square. 
  5. Then give a roll from the center outwards on the two triangles on either side of the pinch. 
  6. Dip them in a warm oil, and cook on a very low heat, so that the cakes cook inside. 
  7. Put the cakes on paper towels, then immerse them in honey. Sprinkle with sesame seeds. 
  8. These cakes can be enjoyed very well with a mint tea. 



the recipe is super well detailed in the video 

{{< youtube EJrdz0D2QZ4 >}} 
