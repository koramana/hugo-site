---
title: arayeches, Algerian cakes
date: '2017-03-01'
categories:
- gateaux algeriens au glaçage
- gateaux algeriens- orientales- modernes- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/arayeches-gateau-algerien.jpg
---
![arayeches Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/arayeches-gateau-algerien.jpg)

##  arayeches, Algerian cakes 

Hello everybody, 

Here is the recipe for arayeches, Algerian cakes well known for its presence in the most beautiful tables of Eid, wedding, engagement, circumcision and birth celebrations. 

These beautiful Arayeches beautifully garnished and well presented are the realization of my friend Mina Minnano, who liked to share with us his wonders, and his recipe for arayeches, which still differs from my [ larayeche recipe ](<https://www.amourdecuisine.fr/article-arayeche-ou-larayeche.html>) , or [ the arayeche of Lunetoiles ](<https://www.amourdecuisine.fr/article-gateau-algerien-arayeche.html>) . 

If you want, you can see also the [ arayeches with honey ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-60064269.html>) , and the [ arayeches with coconut ](<https://www.amourdecuisine.fr/article-arayeche-au-miel-amande-et-noix-de-coco-61897731.html>) , or the [ pistachio arayeches ](<https://www.amourdecuisine.fr/article-arayeche-larayeche-aux-pistaches-gateau-algerien.html>) .   


**arayeches, Algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/arayeches-gateau-algerien-1.jpg)

**Ingredients** For the dough: 

  * 3 measures of flour 
  * 1 part butter softened (at room temperature) 
  * Vanilla extract 
  * Orange blossom water + water 

For the stuffing: 
  * 3 measures of almonds (or even peanuts) 
  * 1 measure of powdered sugar 
  * zest of 1 lemon 
  * vanilla and vanilla extract 
  * Orange tree Flower water 

For frosting; 
  * 2 egg whites (more or less depending on the amount of cakes) 
  * 3 tablespoons lemon juice 
  * 5 tablespoons of orange blossom water 
  * 1 C. honey, or table oil for shine 
  * white + red dye to adjust to have the right color. 
  * between 500 and 700 gr of icing sugar 



**Realization steps**

  1. Prepare the dough with the given ingredients, 
  2. pick up with the orange blossom water and leave for a few minutes. 
  3. In the meantime, prepare the stuffing by mixing all the ingredients. 
  4. prepare balls of 30 gr around, 
  5. and lower each ball to have a small circle, 
  6. put the stuffing in the shape of a "Y". 
  7. fold over all 3 sides, 
  8. put the cake on the palm of your hand and with the other hand, give it the shape of a star with curved angles, as if it floated in the water (a beautiful starfish). 
  9. Bake at 180 ° C and allow to cool completely before icing. 
  10. Prepare the icing with the ingredients mentioned above, it must not be flowing or too thick, 
  11. always test on a cake and grind by adding icing sugar until you get the desired texture. 
  12. decorate according to your taste, here I have fashioned green leaves with sugar dough, pink flowers also with sugar dough, and small artificial flowers. 



![arayeches algerian cake 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/arayeches-gateau-algerien-2.jpg)
