---
title: Couscous with green beans
date: '2015-08-06'
categories:
- couscous
- Algerian cuisine
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/08/couscous-aux-haricots-vert-1.jpg
---
[ ![couscous with green beans 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/couscous-aux-haricots-vert-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/couscous-aux-haricots-vert-1.jpg>)

##  Couscous with green beans 

Hello everybody, 

Here is the couscous I often eat that I go to my family in Tizi Ouzou in Algeria, a couscous with vegetables and green beans, cooked and prepared with a creamy sauce, very different from the one I personally realize. And I do not tell you how good this couscous is. 

I was happy to see this recipe of my dear **Saadia Smail** she shares with us through the lines of my blog ... Now that I have the recipe, I will not hesitate to make this couscous with green beans as I can. 

**Couscous with green beans**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/couscous-aux-haricots-vert.jpg)

portions:  6  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * Couscous steamed 
  * chickpea moullis the day before 
  * 1 onion 
  * 2 to 3 carrots 
  * black pepper, cumin, cinnamon, ras el hanout, salt, paprika, 
  * oil 
  * 2 diced potatoes 
  * 2 zucchini, diced 
  * 1 piece of chicken 
  * 300 gr of green beans or according to taste 
  * 1 tablespoon of tomato paste 



**Realization steps**

  1. Boil the chickpeas in the water 
  2. add the chopped onion and the carrots cut in slices and let cook well 
  3. meanwhile prepare the [ couscous steamed ](<https://www.amourdecuisine.fr/article-comment-cuire-le-couscous-111575933.html>)
  4. at the first steaming of the couscous add to the sauce black pepper, cumin, cinnamon, ras el hanout, salt, paprika, oil, potato, zucchini, chicken, green beans and concentrated tomato 
  5. let boil well and return the couscous to continue steaming. 
  6. let it cook, 
  7. air the couscous well cooked with a little butter, or olive oil according to your taste 
  8. serve in dishes and sprinkle with sauce, and garnish with vegetables, you can roast the pieces of chicken to give them a beautiful color. 



[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/couscous-aux-haricots-vert.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/couscous-aux-haricots-vert.jpg>)
