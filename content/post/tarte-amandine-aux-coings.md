---
title: almond tart with quince
date: '2016-12-04'
categories:
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-frangipane-aux-coings-010.CR2_thumb1.jpg
---
##  almond tart with quince 

Hello everybody, 

Another recipe with quince, let's enjoy the season ... this amandine pie with quince, was really a real delight, very easy to prepare and too good, like all amandine pies of course. 

I tested this recipe, in my mini heart-shaped tartlet molds, and my daughter liked it so much, the quantities gave me 6 pretty tartlets. 

you can make the recipe in a large pie pan of almost 22 cm in diameter, it is preferable that it is a mold with removable base. 

what I advise you also with pies made with a shortbread, you can preserve the pie in the refrigerator, but try to get out the pie at least 30 minutes before serving, and put at room temperature, so that the butter in the dough softened a little, and that the dough is not too hard. in this way, the dough will be softer, and easy to cut ... 

you imagine in front of your guests, cut the pie with the knife, and find yourself pressing it with all your strength, when in fact the pie melts in the mouth ... it's just that the butter at the exit of the fridge gives this effect hard.   


**almond tart with quince / quince recipes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-frangipane-aux-coings-010.CR2_thumb1.jpg)

Recipe type:  pie dessert  portions:  8  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast")

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

frangipane cream, or almond cream: 
  * 100 gr of butter 
  * 80 gr of sugar 
  * 1 tablespoon cornflour 
  * 2 eggs 
  * 125 gr of ground almonds 

the quinces: 
  * 2 quinces 
  * 20 gr of sugar 
  * 1 liter of water 

topping: 
  * just a little honey to give shine, otherwise it's not obligatory. 



**Realization steps** start with the quinces: 

  1. clean the quinces with running water, peel them. 
  2. cut in quarters, remove the hard part and the seeds. 
  3. cut again to have beautiful slats. 
  4. put them in the water with a little sugar, until the pieces of quince become tender, and you can prick them easily with a fork. 
  5. drain and let cool 

prepare the amandine cream: 
  1. Mix the softened butter with the sugar. 
  2. Add the eggs, then the almonds and the cornflour. 
  3. Stir well. 

prepare the pie: 
  1. Lower the dough, place it in the pie pan. 
  2. Garnish the bottom of the pie with the frangipane cream. Put the slices of quince 
  3. cook in preheated oven th. 170 ° approximately 30 to 40 min. When the top is golden. Remove and let cool. 
  4. you can dip with a little honey, otherwise taste as. 



I share here with you the video of the preparation of the pate sande: 

and for the video of preparation of this pie, you can see this video: 

{{< youtube Pa0L9ugIs1Y >}}  {{< youtube D6MZpkEBbsE >}} 
