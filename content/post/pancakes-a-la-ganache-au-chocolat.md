---
title: pancakes with chocolate ganache
date: '2018-03-29'
categories:
- crepes, donuts, donuts, sweet waffles

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pancake-a-la-ganache-au-chocolat-078.CR2_thumb1.jpg
---
##  pancakes with chocolate ganache 

Hello everybody, 

a recipe that my kids call me all the time, pancakes with chocolate ganache, and even I love it, this softness of the pancakes that mixes with the endless fondant of the chocolate ganache, and if in the middle, to crack fruit, I do not tell you, 

a recipe that I highly recommend, I was lucky to garnish my pancakes with these delicious fruits, which may not be in season at home, but here in England, we never miss, last week I even found apricots, it was not as ripe as apricots, but even so I made a tart apricot amandine, super delicious. 

Ingredients: 

  * 1 glass and 1/2 flour 
  * 3 tablespoons sugar 
  * 1 teaspoon of baking powder 
  * 1/2 c. a bicarbonate coffee 
  * 1/2 c. coffee salt 
  * the zest of a lemon 
  * 1 glass and 1/4 buttermilk 
  * 60 gr of melted butter 
  * 2 eggs 
  * 1 C. vanilla 
  * [ chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat-115568486.html>)
  * seasonal fruits (optional) 



method of preparation: 

  1. sift flour, sugar, baking powder, baking soda, salt and lemon peel. 
  2. Whip together buttermilk, melted butter, eggs and vanilla. 
  3. Stir in dry ingredients until combined. 
  4. Heat a pan with a touch of butter. 
  5. Use a ladle by filling it halfway. (for me I told you that I buy a very small pan pancake, it helps me a lot to have pancakes of the same size) 
  6. Pour the dough into the pan. 
  7. Cook about 2 minutes, to see if your pancakes are cooked on the stove side, bubbles will form on the surface. 


  1. prepare the chocolate ganache 
  2. for assembly, remove the ganache from the fridge, whip it a little 
  3. place a pancake, cover with chocolate ganache, place another pancake on top, then ganache, and so on, until you reach the desired height of the cake. 
  4. decorate with fruit, or just a thin layer of ganache 



**pancakes with chocolate ganache**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pancake-a-la-ganache-au-chocolat-078.CR2_thumb1.jpg)

portions:  6  Prep time:  10 mins  cooking:  2 mins  total:  12 mins 

**Ingredients**

  * 1 glass and ½ flour 
  * 3 tablespoons sugar 
  * 1 teaspoon of baking powder 
  * ½ c. a bicarbonate coffee 
  * ½ c. coffee salt 
  * the zest of a lemon 
  * 1 glass and ¼ buttermilk 
  * 60 gr of melted butter 
  * 2 eggs 
  * 1 C. vanilla 
  * chocolate ganache 
  * seasonal fruits (optional) 



**Realization steps**

  1. sift flour, sugar, baking powder, baking soda, salt and lemon peel. 
  2. Whip together buttermilk, melted butter, eggs and vanilla. 
  3. Stir in dry ingredients until combined. 
  4. Heat a pan with a touch of butter. 
  5. Use a ladle by filling it halfway. (for me I told you that I buy a very small pan pancake, it helps me a lot to have pancakes of the same size) 
  6. Pour the dough into the pan. 
  7. Cook about 2 minutes, to see if your pancakes are cooked on the stove side, bubbles will form on the surface. 


