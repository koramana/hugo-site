---
title: briochettes, small raisin buns
date: '2011-01-27'
categories:
- Café amour de cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/briochette_thumba1.jpg
---
![briochette_thumba.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/briochette_thumba1.jpg)

##  briochettes, small raisin buns 

who of us do not like buns? and who does not like to make them home? yes we like it, but it's still not easy to succeed, but we still continue to do and redo until we fall on **Tea** **Recipe** . 

I will not tell you that I find the recipe, but I assure you this is the first time I am proud of myself after making a brioche. 

a very very good recipe, which gave me delicious bricks, all light, with a beautifully airy crumb, I swear, a real happiness. 

![brioches au raisins.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/briochettes-au-raisins1.jpg)

I give you this delicious recipe, I did not even need my bread machine to make it 

and here are my ingredients: 

  * 225 gr of flour 
  * 3 tablespoons of sugar 
  * 7 grams of instant dry yeast 
  * 2 beaten eggs 
  * 50 grs of melted butter 
  * 2 tablespoons warm water 
  * 1 tablespoon of milk powder 
  * 1/2 teaspoon of salt 



to poke: 

  * a handful of raisins 
  * 2 tablespoons orange blossom 


  1. in a salad bowl, sift the flour and the salt, 
  2. add the sugar, the milk powder and the yeast, 
  3. mix, make a well in the middle and add the beaten eggs 
  4. mix with a wooden spoon, then add the melted butter and water. 
  5. knead your dough on a floured surface, 
  6. add the flour gently if it sticks (the flour has the work surface, not the dough), although it is not going to be too sticky dough. 
  7. knead for 5 min until smooth and elastic. 
  8. put your dough in a buttered bowl, and cover with a cling film, and let rise for an hour, or until it doubles in volume 
  9. in the meantime, wash the grapes and let them fool in the orange blossom water. 



![briochettes-to-grape-2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/briochettes-aux-raisins-21.jpg)

  1. after emergence, degass again your dough, kneading for 1 min to two, 
  2. divide this pasta with 12 equal balls. 
  3. each ball take a little bit, the 1/4 of the ball, 
  4. flatten 3/4 of the dough and stuff with the deceived grapes. and close 
  5. place these balls in a muffin pan, so that the face is closed at the bottom. 
  6. form a small ball with the 1/4 of the dough and put it on top. 
  7. go with your finger this piece of dough to stick it with the other ball. 
  8. cover your skewers with food film and let stand and rise for 20 to 30 minutes 
  9. preheated the oven to 220 degrees C. 
  10. after the basting brush the skewers with a little egg yolk to which you add a little yeast and sugar 
  11. sprinkle with a little sugar. 
  12. cook for about 10 to 12 minutes or until you get the desired color. 



![briochettes-to-grape-3.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/briochettes-aux-raisins-31.jpg)

I swear to you a true delight.   


**briochettes, small raisin buns**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/briochette_thumba.jpg)

portions:  12  Prep time:  30 mins  cooking:  12 mins  total:  42 mins 

**Ingredients**

  * 225 gr of flour 
  * 3 tablespoons of sugar 
  * 7 grams of instant dry yeast 
  * 2 beaten eggs 
  * 50 grs of melted butter 
  * 2 tablespoons warm water 
  * 1 tablespoon of milk powder 
  * ½ teaspoon of salt 

to poke: 
  * a handful of raisins 
  * 2 tablespoons orange blossom 



**Realization steps**

  1. in a salad bowl, sift the flour and the salt, 
  2. add the sugar, the milk powder and the yeast, 
  3. mix, make a well in the middle and add the beaten eggs 
  4. mix with a wooden spoon, then add the melted butter and water. 
  5. knead your dough on a floured surface, 
  6. add the flour gently if it sticks (the flour has the work surface, not the dough), although it is not going to be too sticky dough. 
  7. knead for 5 min until smooth and elastic. 
  8. put your dough in a buttered bowl, and cover with a cling film, and let rise for an hour, or until it doubles in volume 
  9. in the meantime, wash the grapes and let them fool in the orange blossom water. 
  10. briochettes-to-grape-2.jpg 
  11. after emergence, degass again your dough, kneading for 1 min to two, 
  12. divide this pasta with 12 equal balls. 
  13. each ball take a little bit, the ¼ of the ball, 
  14. flatten ¾ of the dough and stuff with the deceived grapes. and close 
  15. place these balls in a muffin pan, so that the face is closed at the bottom. 
  16. form a small ball with the ¼ of the dough and put it on top. 
  17. go with your finger this piece of dough to stick it with the other ball. 
  18. cover your skewers with food film and let stand and rise for 20 to 30 minutes 
  19. preheated the oven to 220 degrees C. 
  20. after the basting brush the skewers with a little egg yolk to which you add a little yeast and sugar 
  21. sprinkle with a little sugar. 
  22. cook for about 10 to 12 minutes or until you get the desired color. 


