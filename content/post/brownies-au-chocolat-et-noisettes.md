---
title: chocolate brownies and hazelnuts
date: '2014-12-22'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Delicacies
- Gourmet Gifts
- To taste
- Breakfast
- Cakes
- Pastry
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/brownies-au-chocolat-et-noisettes-1.jpg
---
[ ![chocolate brownies and hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/brownies-au-chocolat-et-noisettes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/brownies-au-chocolat-et-noisettes-1.jpg>)

##  chocolate brownies and hazelnuts 

Hello everybody, You like them **brownies** ? You are like me, it is a dessert that I appreciate very much, I prepare it quite often, especially when I want to go visit a friend of mine, and I'm too lazy to stay long in cooking, brownie is my solution, because it is prepared in the blink of an eye, and cooked super fast. Do you really like this dessert, I invite you to see also: [ peanut brownie ](<https://www.amourdecuisine.fr/article-brownies-aux-cacahuetes.html> "Brownies with peanuts") , [ brownie cheesecake ](<https://www.amourdecuisine.fr/article-cheesebrownie-cafe-chocolat-cheesecake-brownie.html> "cheesebrownie cafe chocolate-cheesecake brownie") , the [ brownie with nutella ](<https://www.amourdecuisine.fr/article-brownie-a-la-nutella.html> "brownie with nutella") and for what not a [ spread with brownies ](<https://www.amourdecuisine.fr/article-pate-a-tartiner-au-brownie-2.html> "brownie spread") . Apparently I'm not the only one to love this dessert, Lunetoiles is just like me, and it's not the first time she shares brownie recipes on the blog. This time, she chose brown hazelnut brownies for you. hope the recipe will please you, and if you try it, do not forget to tell me your opinion. 

![chocolate brownies and hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/brownies-au-chocolat-et-noisettes-3.jpg)

**chocolate brownies and hazelnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/brownies-au-chocolat-et-noisettes-2.jpg)

portions:  12  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 3 eggs 
  * 100 gr of sugar 
  * 200 gr of dark chocolate 52% cocoa 
  * 100 gr of butter 
  * 60 gr of hazelnut powder 
  * 50 gr of almond powder 
  * 80 gr of gold gruel meal 



**Realization steps**

  1. Preheat oven to 180 ° C TH 6 
  2. Melt the butter and chocolate in a bain-marie. 
  3. Meanwhile beat the eggs with the sugar. 
  4. Add the flour and the hazelnut powder, mix gently. 
  5. To finish add the chocolate / butter mixture, mix with the spatula preferably. 
  6. Pour into a baking tin. 
  7. Cook for 10 to 15 minutes at 180 ° C. 
  8. Let cool, detail the brownies in small square. 



[ ![chocolate brownies and hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/brownies-au-chocolat-et-noisettes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/brownies-au-chocolat-et-noisettes.jpg>)
