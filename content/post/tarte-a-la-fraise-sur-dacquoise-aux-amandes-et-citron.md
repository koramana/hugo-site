---
title: strawberry pie on dacquoise with almonds and lemon
date: '2013-05-09'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-aux-fraises-sur-dacquoise-aux-amandes-et-citron1.jpg
---
![tart-with-strawberries-on-Dax-to-almond-and-citron.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-aux-fraises-sur-dacquoise-aux-amandes-et-citron1.jpg)

Hello everybody, 

our dear Lunetoiles takes advantage of the strawberry season, and here is a sublime strawberry tart on a dacquoise with almonds and lemon, huuuuuuum. 

just to change pies based on [ sweet dough ](<https://www.amourdecuisine.fr/article-tarte-flan-aux-pommes-117274851.html>) , from [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>) , or [ pastry ](<https://www.amourdecuisine.fr/article-pate-brisee-116283085.html>) ... 

so we go for this nice version very enticing. 

for a circle 24 cm in diameter 

ingredients 

For dacquoise with almonds and lemon: 

  * 3 egg whites 
  * 1 pinch of salt 
  * 80 g caster sugar 
  * 80 g icing sugar 
  * 65 g of whole amades reduced to powder 
  * 2 lemons 



For strawberry whipped cream: 

  * 300 g whole liquid cream (place in the fridge since the old one) 
  * 30 g of mascarpone 
  * 115 g smoothed pureed strawberries 



500 g strawberries 

Lemon zest 

![Dax-to-strawberries - tarte aux fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/dacquoise-aux-fraises-tarte-aux-fraises1.jpg)

Prepare the dacquoise: 

1\. Whisk the egg whites with a pinch of salt, and when the whites start to froth, pour the powdered sugar in several times. 

2\. Stop the whisk and gently mix with a maryse the sifted icing sugar and the zest of 2 lemons, and the almonds reduced to a fine powder. 

3\. In a circle of 24 cm diameter buttered and sweet on a baking sheet also buttered and sweet, draw the dacquoise. 

4\. Bake at 150 ° C for about 30 minutes. 

Prepare the strawberry whipped cream: 

1\. Whisk the cream with the mascarpone. 

2\. When the cream is set, add the strawberry puree. 

Mounting : 

1\. Cut the strawberries in half. 

2\. Garnish generously in the center of the dacquoise, the whipped cream with strawberries. 

3\. Arrange the strawberries harmoniously. 

4\. Sprinkle with icing sugar and lemon peel. 

![pie-Dax-to-fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/tarte-dacquoise-aux-fraises1.jpg)
