---
title: Qalb el louz kalb elouz cake Almond heart قلب اللوز
date: '2014-06-20'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-elouz-ou-kalb-el-louz-.jpg
---
[ ![Qalb el louz kalb elouz cake Almond heart](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-elouz-ou-kalb-el-louz-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-elouz-ou-kalb-el-louz-.jpg>)

[ ![Qalb el louz kalb elouz cake Almond heart](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/qlb-005_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35768313.html>)

##  Qalb el louz kalb elouz cake Almond heart قلب اللوز 

Hello everybody, 

Qalb el louz kalb elouz cake Heart of almonds, I bring you this delicious recipe at the request of my readers, who can not find it, it is classify in the eastern pastry category is western, do not forget to to see the recipes of my Algerian cakes: choose the category at the top: [ petits fours, Algerian party cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) (for party cakes and honey cakes) 

[ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) : for cakes without honey, sands and petits fours 

[ cakes without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) .  Or higher up you'll see the [ index of Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) , and above you will find lots of pictures and you have the choice, do not forget if you are sure to have seen a recipe on my blog and you can not find it, contact me, and I will pass you the link all of after. 

[ ![Qalb el louz kalb elouz cake Almond heart](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-el-louz.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-el-louz.CR2_.jpg>)

So because it's his recipe, I copy / paste: 

for my part I have everything in two   


**Qalb el louz kalb elouz cake Almond heart قلب اللوز**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-el-louz-kalb-elouz.jpg)

portions:  16  Prep time:  60 mins  cooking:  25 mins  total:  1 hour 25 mins 

**Ingredients**

  * 500g of semolina 
  * 250g of sugar (next time I will reduce the quantity, because it was a bit sweet for me) 
  * 125g of melted butter 
  * 125 ml of liquid: 65 ml of orange blossom water and 60 ml of water 

for the syrup: 
  * 1 liter of water 
  * 500 g of sugar 
  * juice of lemon 
  * a glass of orange blossom water 

the joke: 
  * 1 measure of almonds 
  * ¼ measure of sugar, 
  * 2 cups of melted butter, 
  * 2 teaspoons cinnamon. 



**Realization steps**

  1. the day before: mix semolina and sugar 
  2. add the melted butter, and mix with your fingertips, especially do not knead 
  3. finally wet with half of the liquid (reserved the rest for the next day) and work as when you wet the couscous. 
  4. Cover and chill all night. (important step) 

the same day: 
  1. Leave the bowl at room temperature for about 30 minutes, gently shred the semolina grains, and wet with the remaining liquid, which is kept. 
  2. butter a baking dish, the dimensions of the one I used are 33 x 23 cm (13 x 9 inches) 

prepare the stuffing: this step is optional, for my part, I did not stuff my qalb el louz. 
  1. Arrange small piles of dough with your fingertips in the mold,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/Recently-Updated_thumb1-300x187.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/Recently-Updated_thumb1.jpg>)
  2. when it is uniform, spread the stuffing on top and then cover the rest of the dough in the same way. 
  3. if you do the unfilled version: take bigger piles from the beginning and do only one layer 
  4. always with your fingertips to flatten so that it is homogeneous, 
  5. then cut out portions with a large knife, making the knife stand out each time, do not slide it. 
  6. Brush with melted butter (100g) (here a little accident with the butter, I try to spread more evenly when it began to cool, we see the traces of the brush, but it does not matter!) 
  7. Decorate each serving with an almond 
  8. Cook at 200 C for 1 hour, or until it is golden brown. During cooking prepare the syrup 
  9. add orange blossom water 
  10. at the end Water at the oven exit, using a ladle. 
  11. you may not use all your syrup, because it depends on the quality of the semolina, but it is better to have more than not enough. 
  12. you can check with the tip of a knife if it is well soaked or not ..... let cool and then cut .... 



[ ![Qalb el louz kalb elouz cake Almond heart](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/kalb-el-louz.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/kalb-el-louz.CR2_.jpg>)

[ ![Qalb el louz kalb elouz cake Almond heart](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/qlb-012_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35768313.html>)

and good tasting 

yet another recipe of qalb el louz: 

![qalb elouz 020](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/qalb-elouz-020_thumb1.jpg) [ qalb el louz, ou chamia ](<https://www.amourdecuisine.fr/article-qalb-el-louz-ou-chamia-86960968.html> "qalb el louz, or chamia")
