---
title: cauliflower cake / carambar cake
date: '2015-09-02'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-au-carambar-096.CR2_1.jpg
---
![cake au caramel-096.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-au-carambar-096.CR2_1.jpg)

##  cauliflower cake / carambar cake 

Hello everybody, 

When I come back from Algeria, I always take with me a good bag of Caprices, these sweet candies melting to the taste of caramel, huuuuuuuuuuum ... 

I say to my husband it's for children, but in fact it's me who sucks every day 2 to 3 whims ... this time I decided to make a recipe carambar cake, but instead I put quirks, and ohhhhhh it's delicious .... 

I relied on the recipe of [ carambar cake ](<https://www.amourdecuisine.fr/article-gateau-fondant-aux-carambars-85029025.html>) from Lunetoiles 

**cauliflower cake / carambar cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-au-carambar-084.CR2_1.jpg)

portions:  12  Prep time:  15 mins  cooking:  50 mins  total:  1 hour 5 mins 

**Ingredients**

  * 3 eggs 
  * 120 g of sugar 
  * 160 g flour + ½ cc baking powder 
  * 120 g of butter 
  * 50 whims / or then 25 carambars. (capricious pictures below) 
  * 120 ml of milk 



**Realization steps**

  1. Preheat the oven to 160 ° C. 
  2. In a saucepan, melt butter with milk and carambars over low heat. 
  3. In a bowl whisk the eggs and sugar until the mixture whitens. 
  4. Stir in the flour mixed with the yeast, beat with the whisk and finally add the preparation to the carambars whisking well. 
  5. Pour the dough into a buttered and floured mold or silicone. 
  6. Bake for 50 minutes, check the cooking with the tip of a knife and let cool. 



![cake au caramel-071.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-au-carambar-071.CR2_1.jpg)
