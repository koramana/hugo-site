---
title: pie bread, turkish bread
date: '2018-02-02'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/164947857391.jpg
---
![pie bread, turkish bread](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/164947857391.jpg)

##  pie bread, turkish bread 

Hello everybody, 

beautiful photos, that's what I said when yesterday, my dear Lunetoiles me emvoyer a lot of recipes, yes sometimes, she sends me and I do not publish, because I have recipes in my archives, but, she like me, we want to share with you, our little know-how, so when I saw his [ bread ](<https://www.amourdecuisine.fr/categorie-10678924.html>) Turkish pide, I told myself that I must post it as soon as possible, and voila, thing promised, thing done. 

in the category of Turkish recipes, I recommend the recipe of the [ Turkish pizza - lahmacun ](<https://www.amourdecuisine.fr/article-lahmacun-pizza-turque-90286084.html>)

and if not you can see another version in video: 

**pie bread, turkish bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/16494664031.jpg)

Recipe type:  bread, boulnge, bread  portions:  8  Prep time:  90 mins  cooking:  20 mins  total:  1 hour 50 mins 

**Ingredients**

  * 500 g white flour 
  * 1 sachet of dry yeast 
  * 22 cl of lukewarm water 
  * 1 teaspoon of sugar 
  * 1 teaspoon of salt 
  * 2 tablespoons olive oil 
  * 1 natural yoghurt 



**Realization steps** Make the yeast rise: 

  1. mix in a small bowl the yeast with the sugar and a little water (taken from the 22 cl) 
  2. let rise 15 min. 

At the same time : 
  1. put in the bowl of the kneading robot, flour, salt, olive oil, yoghurt and lukewarm water. 
  2. Mix and add the yeast and knead at medium speed for 6 minutes.   
The dough must be homogeneous and easily peel off the walls of the robot. 
  3. Let the ball of dough rise in a warm place, away from drafts for 1h30. 
  4. The dough has now doubled in volume, degas it and knead a few moments on a floured worktop. 
  5. Divide the dough into two parts and form two balls of the same size and let stand 10 min. 
  6. With the palm of your hand, flatten the balls about 20 cm in diameter. 
  7. With the handle of a wooden spoon, make holes on all the bread, and let it again, rest for 15 min. 
  8. Brush the surface of the patties with a beaten egg. 
  9. Sprinkle seeds of nigella, sesame, and oat flake. 
  10. Let stand still, the time to preheat your oven to 190 ° C, 
  11. in the oven place a dish, filled with water.   
(so that there is steam in the oven) 
  12. Bake for about 20 minutes, on a grid covered with baking paper. 
  13. The breads must be golden brown. 



![pide bread, turkish bread 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/164945415701.jpg)

thank you for all your visits and comments 

bonne journée 
