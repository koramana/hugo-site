---
title: dry cakes with almonds and walnuts
date: '2012-12-01'
categories:
- cuisine algerienne
- diverse cuisine
- cuisine marocaine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- recette de ramadan
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/05/dsc040271.jpg
---
![dry cakes with almonds and walnuts](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/dsc040271.jpg)

##  dry cakes with almonds and walnuts 

Hello everybody, 

These dry cakes with almonds and nuts are a real delight, a texture very close to those of old-fashioned macaroons. 

A recipe from my friend Lunetoiles that she found on Nadia Jouhri's book "La pastry marocaine". A nice little treat, so do not hesitate to make these little delights.   


**dry cakes with almonds and walnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/dsc04031.jpg)

**Ingredients** For about sixty cupcakes: 

  * 250g roasted nuts 
  * 250g whole almonds with roasted skin 
  * 250g caster sugar 
  * 1 sachet of baking powder or 2 teaspoons 
  * 2 eggs 
  * Ice sugar for coating 
  * Small paper cups for baking cakes ("truffle" format) 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. In an electric chopper, grind the nuts with the almonds and sugar until a powder is obtained. 
  3. Put this mixture in a large bowl and add the baking powder and the eggs. 
  4. Mix and form small balls the size of a chocolate truffle or a large olive approximately. 
  5. Roll in the icing sugar and place in the small boxes. 
  6. Place on a baking sheet and bake. 
  7. When the cakes are nicely browned, cracked and slightly puffed, remove them from the oven. 
  8. Let the cupcakes cool on a wire rack. 
  9. They keep very well in an iron box. 



![dry cakes with almonds and walnuts](https://www.amourdecuisine.fr/wp-content/uploads/2011/05/dsc040311.jpg)

do not forget your comments always make me happy 

and if you like my recipes, and recipes from my blog, subscribe to the newsletter 

merci pour votre passage 
