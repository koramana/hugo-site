---
title: Tea cake
date: '2017-04-01'
categories:
- cakes and cakes
tags:
- To taste
- Cakes
- desserts
- Easy cooking
- Mellow cake
- Soft
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/cake-au-the-5.jpg
---
[ ![cake with the 5](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/cake-au-the-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/cake-au-the-5.jpg>)

##  Tea cake 

Hello everybody, 

It tempts you a cake with tea, it is not a cake to accompany your tea, it is a cake with "tea". I'm not going to tell you, you have to use green tea, or red tea, Twinings tea, or Ty-poo tea .... No, make your choice, let yourself be inspired by the scent of your taste. One thing is sure, the result, will be the top for you, a cake super soft, super melting in the mouth, and especially super fragrant, with that sweet touch of orange peel that blends so well with the taste of bursts of tea to awaken all your taste buds, and titillate your palate. 

But before you pass the recipe for this delicious tea cake, I tell you what happened with me. While I had prepared all my ingredients, and that I had started to whip eggs and sugar, I realized that I did not have any cream, and the recipe required 150 gr of cream still ... I can not go out now to buy fresh cream, especially since baby is sleeping ... 

Will I replace cream with milk? in this way, I risk adding even more flour, and find myself with a thicker texture! What should I do? even the glass of buttermilk I had in the fridge, my husband had tasted it with a few dates and [ kesra rekhssis ](<https://www.amourdecuisine.fr/article-kesra-rekhsis.html> "kesra rekhsis / cake with Algerian oil") (Which he likes a lot). The buttermilk would replace the cream in my recipe, it would give a good cake punch, an acid taste more, hum! damage, damage, damage ... 

During my reflection, I saw the Philadelphia cheese box at the end of the shelf of the fridge, you remember, I already told you in the recipe of the [ cheesecake flan ](<https://www.amourdecuisine.fr/article-flan-cheesecake.html> "Flan cheesecake") recently published, that Philadelphia cheese, will never risk being away from home, lol. I told myself then, that replacing the cream, with unsalted cheese spread, will only give more to the texture of the cake, and the end result was the proof. 

A tea cake (which I will not call cake cheese, which will look a lot like the "cheese cake", lol,) super rich in taste, and soft texture and fondant wish. 

[ ![cake with the 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/cake-au-the-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/cake-au-the-1.jpg>)   


**Tea cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/cake-au-the.jpg)

portions:  12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 1 C. soup and ½ of tea. 
  * 250 gr of crystallized sugar 
  * 4 eggs 
  * 200g sifted flour 
  * the zest of an orange 
  * 1 C. coffee baking powder 
  * 150g of cream   
replaced by 150 gr of philadelphia cheese 
  * 50 gr of melted butter 
  * 1 tablespoon softened butter 



**Realization steps**

  1. Preheat the oven to 180 ° C. Butter and flour a cake tin. 
  2. In a mortar, crush the tea leaves with 50 gr of sugar until you have a fine sweet scented powder of tea. 
  3. Place the mixture in a large bowl with the eggs and the remaining sugar. Whisk until well blended, light and creamy.   
(about 5 minutes) 
  4. Mix the flour, orange zest and baking powder in a separate bowl. 
  5. mix the dry ingredients gently with a spatula to the mixture of eggs and sugar, to incorporate everything. 
  6. In another bowl, mix the cream and melted butter (cheese and butter in my recipe). 
  7. Mix half of the egg mixture in the cream mixture. Then transfer this device to the remaining egg mixture and sugar dry ingredients. 
  8. Stir it gently with a spatula in a manner to air the mixture. 
  9. Pour everything into the cake tin. 
  10. stir now the remaining tablespoon of softened butter, and shape with a small line in the middle of the cake, this is what will allow you to have the nice slit after baking the cake. 
  11. Bake at 180 ° C for 5 minutes, then reduce the oven temperature to 170 ° C for 10 minutes. And again reduce the temperature to 160 ° C and cook for 25-35 minutes or until the cake is golden brown and a toothpick inserted comes out clean.   
this cooking time is of course indicative, it is necessary to follow the cooking time of your oven, if it is necessary to prolong the cooking, do not hesitate to do it. 
  12. Let cool on a rack before unmolding the cake. 



[ ![cake the the 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/cake-au-the-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/cake-au-the-3.CR2_.jpg>)
