---
title: appetizer / appetizer at Surimi
date: '2012-12-23'
categories:
- Algerian cuisine
- Cuisine by country

---
Hello everybody, 

Here is a very simple entry to make for fans of surimi like me, who is always at home, and who always wants to present it differently, each time on my table. 

A recipe very simple, pif, and too good. 

ingredients: 

  * 10 crumbled surimi sticks 
  * 1 hard egg 
  * 3 portions of cheese the laughing cow. 
  * 1 tablespoon of lemon juice 
  * a few sprigs of parsley 
  * 1 pinch of salt 
  * 1 pinch of cumin 



method of preparation: 

  1. crush the cheese with a fork 
  2. crush on the hard egg 
  3. add the surimi crumbs 
  4. season with the juice, chopped parsley, salt and cumin. 
  5. mix to have a paste 
  6. shape dumplings. 
  7. book fresh for 1 hour before presenting with a fresh salad. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
