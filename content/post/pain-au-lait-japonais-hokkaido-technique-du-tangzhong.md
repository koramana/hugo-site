---
title: hokkaido-technique japanese milk bread from tangzhong
date: '2015-12-01'
categories:
- Buns and pastries
- Mina el forn
tags:
- Pastries
- Boulange
- Bakery
- To taste
- Bread maker
- buns

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-au-lait-japonais-hokkaido.jpg
---
[ ![hokkaido japanese milk bread 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-au-lait-japonais-hokkaido.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-au-lait-japonais-hokkaido-1.jpg>)

##  hokkaido-technique japanese milk bread from tangzhong 

Hello everybody, 

Who does not like to make a brioche, and have a super-fast crumb, a brioche very soft and melting in the mouth? Sometimes it's not easy, but once you get your first bun, once you understand how the texture should be, how the bun is to be lifted ... That's where you get it, and your buns go to be a success each time. 

This is with Lunetoiles that we will discover the tricks to succeed this brioche, and voila what she tells us: 

In fact, it is a Japanese milk bread that is called hokkaido with the Japanese technique of tangzhong, that I too wanted to test it. Well, the result is bluffing, a crumb and the brioche is all soft, it looks like a cushion. it's very easy to do and the result is a brioche too good. 

[ ![hokkaido japanese milk bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-au-lait-japonais-hokkaido-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-au-lait-japonais-hokkaido-1.jpg>)   


**hokkaido-technique japanese milk bread from tangzhong**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-au-lait-japonais-hokkaido-2.jpg)

portions:  10  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients** For the Tangzhong: 

  * 50 g of flour 
  * 125 ml of water 
  * 125 ml of milk 

For the dough: 
  * 540 g of flour 
  * 85 g caster sugar 
  * 10 g of milk powder (I have not put any) 
  * 8 g of salt 
  * 1 sachet Briochin dehydrated baker's yeast (or 20 g fresh baker's yeast) 
  * 60 g whole liquid cream 30% 
  * 2 eggs at room temperature 
  * 55 g lukewarm milk 
  * 50 g butter cut into pieces 
  * 200 g of tangzhong (I put everything) 

For the painting: 
  * 1 egg yolk 
  * a little bit of milk 



**Realization steps** preparation of Tangzhong: 

  1. In a saucepan, mix with the whisk (manual), the flour with the water and the milk. Heat to a temperature not exceeding 65 ° while mixing. The mixture will thicken and look like a porridge. Once the temperature is reached ... remove from the heat. Book until it warms up. 
  2. In the bowl of your mess, pour 440 g flour, salt, sugar, milk powder. Mix. Then add the liquid cream, the two eggs and the tangzhong. 
  3. Lightly warm your milk and melt your yeast. Pour over the mixture in the bowl and operate your kneading on medium speed. Pour the yeast-milk mixture into a net. Let the robot work until you get a ball of dough. After about 10 minutes knead then add the butter in small pieces. When the butter is well incorporated, add your remaining flour in small amounts. Little by little you will see a ball of dough forming and peeling off the walls of the bowl. The dough must be homogeneous and a little sticky. 
  4. Cover and let stand near a radiator or heat source for one hour, or until the dough doubles in volume. 
  5. At the end of this hour, degas your dough on a work surface and divide it into four equal parts. Work the four pieces of dough between your hands to get pretty balls that you will place in your cake pan (26.5 cm) buttered. 
  6. Cover again with a cloth and allow to rest for one hour at an hour and a half near a radiator or a source of heat. 
  7. mn before the end of the rest period, preheat your oven to 180 ° C. 
  8. Brush your buns with an egg yolk mixed with a little milk. 
  9. Bake for about thirty minutes. 
  10. At 180 ° C for the first ten minutes of cooking and lower the oven temperature to 160 ° C for the rest of the cooking. 



[ ![japanese hokkaido milk bread 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-au-lait-japonais-hokkaido-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-au-lait-japonais-hokkaido-3.jpg>)
