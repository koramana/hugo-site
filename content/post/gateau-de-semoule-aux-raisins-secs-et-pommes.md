---
title: semolina cake with raisins and apples
date: '2017-08-22'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- desserts
- Pastry
- To taste
- Algeria
- Easy cooking
- eggs
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-de-semoule-aux-raisins-et-pommes-5.jpg
---
[ ![semolina cake with raisins and apples 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-de-semoule-aux-raisins-et-pommes-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-de-semoule-aux-raisins-et-pommes-5.jpg>)

##  semolina cake with raisins and apples 

Hello everybody, 

[ ![logo-serving hatch-between-amis1-300x300](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/logo-passe-plats-entre-amis1-300x300.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/logo-passe-plats-entre-amis1-300x300.png>)

It was necessary to find a recipe that makes me return to childhood, but also you have to dig into a friend blog !! It was a bit difficult for me because my childhood recipes I learned from my mother. But when luck comes in our favor we can find what we are looking for without even looking for it. I wandered by chance on Pinterest, when I came across a nice picture of a semolina cake, which for a few seconds made me come back to those spring days, when my mother with the little way and the little of ingredients we prepared his delicious cake super greedy, with this nice caramel flowing in all directions. 

![semolina cake with raisins and apples 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-de-semoule-aux-raisins-et-pommes-2.jpg)

**semolina cake with raisins and apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-de-semoule-aux-raisins-et-pommes-1.jpg)

portions:  6  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** For caramel 

  * 100 gr of sugar 
  * 20 ml of water 

For the cake 
  * 500 ml of milk 
  * 50 gr of sugar 
  * 1 pinch of salt 
  * 15 gr of butter 
  * 2 eggs (which I added as my mother did) 
  * 60 gr of fine semolina 
  * 1 teaspoon vanilla extract 
  * 100 gr of [ applesauce ](<https://www.amourdecuisine.fr/article-cupcakes-aux-pommes-et-mascarpone.html>) with pieces 
  * 50 gr of raisins 



**Realization steps**

  1. Dip the raisins in hot water for 30 minutes and drain. 

Prepare the caramel: 
  1. place the sugar in a saucepan with the water, 
  2. put on low heat, let the sugar melt and gradually take a golden color, without it burning. 
  3. Spread the caramel in your mold. (mine is very small, almost 22 cm in diameter, but it is a little narrow) 

Prepare the cake: 
  1. Put in a pan butter, sugar and milk 
  2. bring the whole to a boil then add the semolina. 
  3. Cook while mixing with a wooden spatula, until thickened. 
  4. remove from the heat and let cool. 
  5. introduce the whipped eggs while mixing. 
  6. Add vanilla extract, raisins and applesauce 
  7. mix gently and then divide the whole into the mold. 
  8. place the mold in a water bath, and cook for 15 to 20 minutes at 180 ° C. 
  9. at the end of cooking, remove from heat, allow to cool, and cool. 
  10. Carefully unmold the cake, and enjoy, it's a delight! 



[ ![semolina cake with raisins and apples 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-de-semoule-aux-raisins-et-pommes-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-de-semoule-aux-raisins-et-pommes-4.jpg>)

Ah! Before I forget, are you waiting for the picture my daughter took? 

Well if you are reading this message, what are you looking for the photo, hihihihih 

[ ![the part of ussa semolina cake](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/la-part-de-noussa-gateau-de-semoule.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/la-part-de-noussa-gateau-de-semoule.jpg>)

Ils sont jolis les petits morceaux de pommes!!! 
