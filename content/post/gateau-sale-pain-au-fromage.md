---
title: Salted cake / cheese bread
date: '2013-01-17'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

---
Hello everybody, 

a delicious and super mellow cheese bread, which I prefer to call salty cake, because it is a bread without baker's yeast, without kneading, without a lot of water, but rather a lot of eggs .... so in your opinion is not it a salty cake? 

a salty cake that will go well with your soups, and be a base of amuse bouches or as appetizers ... 

  * 3 cups of flour 
  * 2 teaspoons of baking powder 
  * 1 1/4 teaspoon of salt 
  * 1 glass of grated parmesan cheese (240 ml) 
  * 1 glass of grated cheddar cheese, mozzarella or any other cheese of your choice 
  * 4 tablespoons softened butter 
  * 1/2 glass chopped chives (fresh if possible, for me it was dried chives) 
  * 1/2 glass of chopped tomatoes, 
  * 1/2 glass of green olives pitted in slices. 
  * 3 large cloves of crushed garlic. 
  * 4 big eggs 
  * 3/4 glass of milk 



method of preparation: 

  1. preheat the oven to 180 ° C. 
  2. oil a baking pyrex mold, or then line 
  3. mix all the dry ingredients with the soft butter. 
  4. introduce herbs and olives. 
  5. whisk eggs and milk 
  6. remove 2 tablespoons of this mixture (to decorate the bread) 
  7. add the liquid mixture to the dry products 
  8. pour this mixture into your mold, it's a bit sticky, try to arrange it with your hands just a little wet. 
  9. decorate with the milk egg mixture that you have to keep 
  10. sprinkle over the herbs of your choice (for me it was: dried parsley, oregano, dried chives, and a little seed of nigella) 
  11. cook between 35 to 40 minutes 



methode de preparation detaillee sut cette video: 
