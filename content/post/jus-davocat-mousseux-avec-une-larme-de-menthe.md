---
title: SPICY AVOCADO JUICE WITH A TEA OF MINT
date: '2007-11-13'
categories:
- birthday cake, party, celebrations

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/11/191150791.jpg
---
here are the ingredients for a very very good juice 

  * 750 ml of cold milk 
  * one or two lawyers 
  * sugar according to taste 



![S7301317](https://www.amourdecuisine.fr/wp-content/uploads/2007/11/191150791.jpg)   
shaker until you have a nice mousse. 

Add a drizzle of mint syrup, the flavor of the avocado will be enhanced 

![S7301318](https://www.amourdecuisine.fr/wp-content/uploads/2007/11/1911514911.jpg)

I hope you will enjoy it 

et consommez sans attendre! 
