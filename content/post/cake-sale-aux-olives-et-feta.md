---
title: salty cake with olives and feta
date: '2017-10-28'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- accompaniment
- Aperitif
- inputs
- Cocktail dinner
- Cheese
- Easy cooking
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-sal%C3%A9-aux-olives-et-f%C3%A9ta_.jpg
---
![salty cake with olives and feta](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-sal%C3%A9-aux-olives-et-f%C3%A9ta_.jpg)

##  salty cake with olives and feta 

Hello everybody, 

Here is a delicious cake salted with olives and Feta super mellow and very easy to make. Rich in taste, and well perfumed, this recipe comes from [ Lunetoiles (olive cake) ](<https://www.amourdecuisine.fr/article-cake-aux-olives.html>) she was in the dungeon of my blog, and yet it is a recipe that must at least taste once in his life, lol ... 

Personally, this salty cake with olives and feta is a must at least every month at home, the children find it super delicious, and beautiful instead of tasting a few times. 

**salty cake with olives and feta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-sal%C3%A9-aux-olives-et-f%C3%A9ta-2.jpg)

portions:  8  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients**

  * 200 g flour 
  * 3 eggs 
  * 100 ml of milk 
  * 100 ml of olive oil or sunflower oil 
  * 1 packet of dry yeast 
  * 150 g of feta 
  * 75 g of pitted black olives 
  * mill pepper 
  * butter for the mold 
  * no salt (feta and olives are quite salty) 



**Realization steps**

  1. Preheat the oven to 180 ° C, 
  2. butter a cake tin. 
  3. Cut the feta and the olives in small dice, 
  4. Flour and shake well. 
  5. Mix flour, yeast and beaten eggs, a little pepper. 
  6. Add the oil, milk and diced feta and olives. 
  7. Pour the mixture into the pan and bake for about 45 minutes. 
  8. Check the cooking, then let cool a few minutes before unmolding. 
  9. Serve warm or cold. 



![salty cake with olives and feta 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cake-sal%C3%A9-aux-olives-et-f%C3%A9ta-1.jpg)
