---
title: Beetroot and raspberry smoothie
date: '2016-05-27'
categories:
- juice and cocktail drinks without alcohol
tags:
- Juice
- Fast Food
- Healthy cuisine
- Drinks
- Summer kitchen
- Ramadan 2016
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-betterave-et-framboises-2.jpg
---
[ ![beet and raspberry smoothie 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-betterave-et-framboises-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-betterave-et-framboises-2.jpg>)

Hello everybody, 

When I saw that the new theme of our round: [ recipes around an ingredient # 18 ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient18-betterave.html>) was the beet, I started looking for recipes on the internet, and I promise my friends, the choice is vast, besides I did not stop at the realization of a single recipe based beetroot, so wait for future recipes. 

[ ![logo-serving hatch-between-amis1-300x300](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/logo-passe-plats-entre-amis1-300x300.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/logo-passe-plats-entre-amis1-300x300.png>)

The theme of this round is the  Red  , Cook red, red and more red .... In salted with tomatoes, beets, radishes, cabbage, pepper etc ... or sweetened with strawberries, raspberries, currants etc ....   
The choice is vast and varied ... So  Put some red in your plate  ...... 

**Beetroot and raspberry smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-betterave-et-framboises-1.jpg)

portions:  3  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 3 medium beets, cooked and peeled 
  * 1 green apple, peeled 
  * 2 cups of unsweetened coconut water 
  * 1 ½ cup frozen raspberries 
  * The juice of half an orange 

decoration: 
  * whipped cream 
  * pieces of raspberries. 



**Realization steps**

  1. place all the ingredients in the blender jar and reduce to a smoothie. 
  2. Decorate with a little whipped cream and raspberry and serve immediately. 



[ ![beet and raspberry smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-betterave-et-framboises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/smoothie-betterave-et-framboises.jpg>)
