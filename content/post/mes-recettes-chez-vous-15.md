---
title: my recipes at home 15
date: '2011-12-19'
categories:
- cuisine algerienne
- Cuisine saine
- rice

image: http://amour-de-cuisine.com/wp-content/uploads/2011/12/sables.jpg
---
![2011-09-04 for the moment tout1](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

I share with you, your impressions, your comments, and recipes from my blog that some of my visitors have tried. 

on the [ traditional apple tart ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-anciennes-65550114-comments.html#comment93462351>)

good evening soulef, yesterday I made this recipe it was very successful, we made I had a basket full of apples, I propose to my children to choose between an apple cake or a pie, they chose the pie, I roll up my sleeves and I go on the net to find the easiest recipe, and of course you had to have broken pasta, I told myself no time to search, and yes the solution quickly found in your blog and I was delighted 

![La tresse.JPG](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x200/2/42/48/75/Images/)

the braid at [ Mahdia ](<https://www.amourdecuisine.fr/article-la-tresse-61395654.html>)

Merci beaucoup chere madame 😉    
  
Your recipe I applied to the letter, and it is a success, this cake was unanimous    
It is above all easy (but it takes time) presentable, beautiful and delicious    
I send you here the photo of my dish    
  
thanks again 

![truffes.JPG](http://img.over-blog.com/300x225/2/42/48/75/Images/truffes.JPG)

[ truffles ](<https://www.amourdecuisine.fr/article-truffes-au-chocolat-et-biscuits-65846143.html>) at pelusa (a very good friend) 

Hello Huge: How are you? the children? Yesterday I prepared your truffle recipe. They were excellent 

![sables.jpg](http://amour-de-cuisine.com/wp-content/uploads/2011/12/sables.jpg)

[ sands with strawberry jam ](<https://www.amourdecuisine.fr/article-25345485.html>) at Y Sabrina 

Hi soulef, how are you? I tried your recohaette shortbread and she beautiful. thank my beautiful  ![;\) Wink](https://www.amourdecuisine.fr//import/http://mail.yimg.com/ok/u/assets/img/emoticons/)

![croissants brioches.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/300x225/2/42/48/75/Images/)

[ croissants buns ](<https://www.amourdecuisine.fr/article-35847545.html>) at Anais Moha 

Salam my beautiful, since your blog inspires me so much it is beautiful, I tried the brioche croissant, a pure treat thank you. big kiss 

and if you too have tried one of my recipes, do not forget to send me your photos on this email: 

amour2cuisine@gmail.com 
