---
title: salmon tartare - with crepes
date: '2014-02-07'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/TARTARE-DE-SAUMON-FUMEE_thumb.jpg
---
[ ![TARTARE OF SMOKED SALMON](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/TARTARE-DE-SAUMON-FUMEE_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/02/TARTARE-DE-SAUMON-FUMEE_2.jpg>)

Hello everybody, 

delicious [ appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) , or even to taste as an entree, if you like them **Tartar** . So here is a very good recipe to taste, these delicious [ pancakes ](<https://www.amourdecuisine.fr/categorie-12348217.html>) stuffed with a very good **salmon tartare** , scented with dill, presented in cornets, just a delight    


**salmon tartare - with crepes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/TARTARE-DE-SAUMON-FUMEE_thumb.jpg)

Recipe type:  Entrance  portions:  8  Prep time:  30 mins  cooking:  5 mins  total:  35 mins 

**Ingredients** For pancakes: 

  * 250 g buckwheat flour 
  * 2 eggs 
  * 2 tablespoons of oil 
  * 400 ml of water 
  * 1 pinch of salt. 

for the tartar: 
  * smoked salmon fillets 
  * the juice of half a lemon 
  * olive oil 
  * sprigs of dill 
  * salt and black pepper 



**Realization steps**

  1. In a bowl, form a well with the dry elements, break the eggs in the center and mix vigorously with the whisk by adding the milk gradually. Let the dough rest for 1 hour at room temperature. 
  2. Heat a skillet or crepe maker and lightly oiled. Pour a ladle of dough by spreading and cook each pancake on both sides. Return as soon as the coloring. 
  3. cut the salmon fillets into pieces, add the lemon, and the olive oil 
  4. add chopped dill, season with salt and pepper to taste. marinate in a cool place for 15 minutes 
  5. cut each pancake in half, and form cornets, fill them with the salmon tartare, and prick with a toothpick so that it does not open. 



[ ![smoked salmon tartar](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/tartare-de-saumon-fume_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/02/tartare-de-saumon-fume_2.jpg>)

good tasting, and the next recipe 

if you have tried one of my recipes, send me the picture here: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

and if you have a recipe for you that you want to publish on my blog, you can put it online here: 

[ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
