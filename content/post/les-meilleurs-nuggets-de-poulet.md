---
title: the best chicken nuggets
date: '2017-11-14'
categories:
- appetizer, tapas, appetizer
- idea, party recipe, aperitif aperitif
- salty recipes
tags:
- Nuggets House
- Picnic
- Easy cooking
- accompaniment
- inputs
- Amuse bouche
- Aperitif Aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/les-meilleurs-nuggets-de-poulet.jpg
---
![the-best-of-chicken nuggets,](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/les-meilleurs-nuggets-de-poulet.jpg)

##  the best chicken nuggets 

Hello everybody, 

Here are the best chicken nuggets you can make! This is my children's favorite recipe, chicken nuggets can be on our table at least 2 times a week. 

As I told you, it's the best chicken nuggets that you will taste, try the recipe and tell me your opinion, scented with desire, super melting in the mouth with a crispy touch of breadcrumbs (panko in this recipe, because was what I had on hand). 

I made you a complete video, which you can see on this link: 

**the best chicken nuggets**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/les-meilleurs-nuggets-de-poulet-1.jpg)

portions:  4  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 2 chicken breasts 
  * ½ onion 
  * chopped chives 
  * 1 egg 
  * salt, black pepper, cumin, garlic / coriander powder 

breading: 
  * flour 
  * 1 egg 
  * breadcrumbs. 

spicy dip with natural yoghurt: 
  * 2-3 tbsp. plain yoghurt 
  * chopped pepper 
  * chopped chives 
  * chopped parsley 
  * salt and black pepper 
  * pressed lemon juice. 



**Realization steps** Prepare the chicken nuggets: 

  1. chop the chicken breasts into pieces. 
  2. add the egg, chopped chives, salt, black pepper, cumin and garlic / coriander powder 
  3. mix well to have a homogeneous stuffing. 
  4. cover and place in a cool place. 

prepare the yogurt dip: 
  1. in a bowl mix yoghurt, chopped parsley, chives, chopped pepper, salt and pepper 
  2. add the squeezed lemon juice. 
  3. mix well and season to taste 

cooking chicken nuggets: 
  1. shape nuggets according to taste, and place them on a floured work surface 
  2. put the oil to heat 
  3. dip the nuggets in the flour, then in the beaten egg and finally in the breadcrumbs. 
  4. cook in the oil bath while watching so that it cooks well inside and out. 
  5. drizzle on paper towels, and enjoy with the yogurt dip. 



![the-best-of-nuggets chicken-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/les-meilleurs-nuggets-de-poulet-2.jpg)
