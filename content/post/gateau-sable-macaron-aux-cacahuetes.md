---
title: shortbread cake macaron with peanuts
date: '2015-06-13'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
tags:
- Algerian cakes
- Cakes
- Cookies
- biscuits
- Pastry
- Ramadan
- Ramadan 2015

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-sable-macaron-aux-cacahu%C3%A8tes.jpg
---
![macaroon sand cake with peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-sable-macaron-aux-cacahu%C3%A8tes.jpg)

##  shortbread cake macaron with peanuts 

Hello everybody, 

besides, that's what she told me about this recipe: 

__ * I did not put apricot jam between the two layers at all. 

* For decoration, I opted for marbling with chocolate 

and his opinion about this cake  : 

In any case, thank you again for sharing this recipe with me, I like it, because it is simple, easy to achieve, you spend too much time preparing it, it's fast, for a great performance, I have not counted, but more than 60 pieces, even 80, I can fill two tray with what I got, and most importantly the taste of course, they are very fondant, soft and melting sand paste in mouth, we feel the taste of peanut hummm, and it goes wonderfully well with chocolate, it fits perfectly 

[ ![macaroon sand cake with peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-sable-macaron-aux-cacahu%C3%A8tes-1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-sable-macaron-aux-cacahuetes.html/gateau-sable-macaron-aux-cacahuetes-2>)

**shortbread cake macaron with peanuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-sable-macaron-aux-cacahu%C3%A8tes-3.jpg)

portions:  60-80 pieces  Prep time:  25 mins  cooking:  30 mins  total:  55 mins 

**Ingredients** For a tray (40cm x 30cm), you will need: For the shortbread dough: 

  * 250 gr of soft butter 
  * 150 gr of powdered sugar 
  * 2 eggs 
  * 3 c. vanilla flavored flan powder 
  * Flour (as needed) 

For the macaroon layer: 
  * 3 glasses of roasted and ground peanuts 
  * 1 glass of sugar 
  * 3 c. vanilla flavored flan powder 
  * 1 C. coffee baking powder 
  * 6 egg whites 

Decoration: 
  * 400 gr of white chocolate 
  * 40 to 50 gr of dark chocolate 



**Realization steps** For the preparation:   
you start with the shortbread dough: 

  1. Beat the butter until you make it ointment. 
  2. Add the sugar little by little, always beating. 
  3. Add the eggs and the vanilla custard powder. 
  4. Dry with flour until soft, smooth dough. 
  5. Spread this dough on the tray to the edge.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/la-couche-sabl%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42838>)

you go to the 2nd layer: 
  1. Preheat the oven to 180 ° C 
  2. In a salad bowl, mix ground peanuts, sugar, vanilla custard powder and baking powder. 
  3. Wet with the egg whites (the white is not mounted in snow) and pour the mixture immediately on your first layer. 
  4. Bake in a preheated oven at 180 ° C for almost 30 minutes, watching. 
  5. After cooking, let the cake cool completely.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-sable-macaron-aux-cacahu%C3%A8tes6.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42836>)

you go to the decor: 
  1. You will proceed as for the marbling of the mille-feuille: 
  2. Melt the white chocolate in a bain-marie and then, in another bain-marie, the dark chocolate. 
  3. Cover the cake cooled with white chocolate and spread evenly over the entire surface of the cake. 
  4. Then put the dark chocolate in a pastry bag, or a cone made of parchment paper, or a freezer bag that you will pierce with a toothpick (be careful that the chocolate is not too hot because otherwise, it will melt the plastic, if it is still hot wait for it to go down in temperature) and make parallel strokes, all in the same direction, not too fast to make beautiful regular features but enough not to leave fondant and chocolate time to harden.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/to-print.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42839>)
  5. Then make the marbling marks with the point of the knife in one direction, then in the opposite direction and continue until the end 
  6. Leave the several hours in a cold room, until the chocolate hardens. 
  7. Cut out in rectangle, or square or triangle as you wish. 



[ ![macaroon sand cake with peanuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-sable-macaron-aux-cacahu%C3%A8tes-2.jpg) ](<https://www.amourdecuisine.fr/article-gateau-sable-macaron-aux-cacahuetes.html/gateau-sable-macaron-aux-cacahuetes-1>)
