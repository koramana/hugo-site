---
title: Pasta stuffed with spinach and ricotta
date: '2013-07-02'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/Conchiglioni-farcies-pates-farcies-recette-de-ramadan-2011.jpg
---
![Conchiglioni-stuffed - pasta-stuffed - Recipe of Ramadan-201.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/Conchiglioni-farcies-pates-farcies-recette-de-ramadan-2011.jpg)

##  Pasta stuffed with spinach and ricotta 

Hello everybody, 

Here is a recipe Conchiglioni (it took me an hour to read this word correctly, and I'm not sure of the pronunciation ... hihihihi) or giant pasta shaped shells stuffed with a cream of spinach and ricotta, deposited on a bed of tomato sauce .... a recipe very easy to make, and very delicious at the same time. 

**Pasta stuffed with spinach and ricotta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-zitoune-041.CR2_.jpg)

portions:  4  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients** For the tomato sauce: 

  * 1 tablespoon olive oil 
  * ½ large onion cut into small cubes. 
  * 1 large clove garlic minced. 
  * 1 kg of peeled tomatoes, or 2 boxes of tomatoes cut into slices. 
  * ¼ teaspoon of sugar (optional, unless your tomato is very acidic) 
  * salt and freshly ground black pepper. 
  * handful of fresh basil leaves. 
  * handful of fresh oregano leaves 

Pasta and filling: 
  * 16 shells (6cm each) or giant egg-shaped shells 
  * 1 tablespoon of olive oil 
  * 1 clove garlic minced 
  * 100 gr frozen spinach (thawed) 
  * salt and freshly ground black pepper 
  * pinch of nutmeg 
  * 1 ¼ cup fresh ricotta 
  * ¼ cup grated Parmesan cheese 
  * handful of parsley leaves, finely chopped 



**Realization steps**

  1. Prepare the sauce: In a medium saucepan, heat the olive oil over medium heat. 
  2. Add the onions and cook until it becomes translucent. 
  3. Add garlic, cook for 1-2 minutes, then add tomatoes and sugar. 
  4. add a little water to the pan. Season with salt and pepper, crush the large pieces of tomatoes with a wooden spoon and cook on medium heat, stirring occasionally, for about 20 minutes or until the mixture slightly thickens. 
  5. Add basil and oregano, mix well, cover and remove from heat. 
  6. Cook the shells in a large pot of salted boiling water until al dente. drain and let them cool. 
  7. Preheat oven to 180 ° C / 350 ° F. 

Garnish: 
  1. in a small saucepan, heat the olive oil over medium heat. Add garlic, cook for about 1 minute or until fragrant. 
  2. Add the spinach and cook until thawed, stirring with a wooden spoon. 
  3. Season with salt, pepper and nutmeg and remove from heat. Let cool a little. 
  4. In a large bowl, mix ricotta, parmesan, spinach and parsley. 
  5. in a baking dish, make a layer of tomato sauce. 
  6. Generously fill with a spoon, the shells with cream spinach ricotta 
  7. put them in the mold, on the sauce. Cover with foil and cook until boiling, about 30 minutes. 



Note * Some shells could break in boiling water, so it's a good idea to cook more shells than you really need ![pasta-stuffed-with-spinach-and-ricotta.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/pates-farcies-aux-epinards-et-ricotta.CR2_1.jpg)

Thank you for your visits, and for your comments 

Et a la prochaine recette. 
