---
title: shortbread peanuts and chocolate
date: '2015-07-15'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- sweet recipes
tags:
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9-cacahuetes-chocolat-2.jpg
---
[ ![shortbread chocolate peanuts 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9-cacahuetes-chocolat-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9-cacahuetes-chocolat-2.jpg>)

##  shortbread peanuts and chocolate 

Hello everybody, 

Yet another sweet and well-scented shortbread recipe that will not leave you indifferent, a crunchy peanut-flavored shortbread with every bite, and covered with a thin layer of dark chocolate, which adds even more sweetness to this cake so much enjoy on the tables and trays of the cakes of the Aid. Besides, I do not think there is a plateau of the Aid, or will not find some [ shortbread with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-fraises.html>) , or shortbread decorated with 1000 and one way. 

And here is a beautiful presentation of these shortcakes with peanuts and chocolate, sometimes called this cake » **kahlouchi** «, A recipe from my friend **Halima Ahmed** that I thank a lot of passage, and who always shares beautiful recipes, I hope we will have the chance to see these delights together. 

do not forget you too can share your recipes on my blog, by following this link: [ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette>)

[ ![shortbread chocolate peanuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9s-cacahuetes-chocolat-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9s-cacahuetes-chocolat-1.jpg>)   


**shortbread peanuts and chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9s-cacahuetes-et-chocolat-4.jpg)

portions:  40  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 500 g softened 
  * 1 egg 
  * 1 egg yolk 
  * 1 sachet of baking powder 
  * 1 glass of cornflour (a glass of 250 ml) 
  * 1 glass of icing sugar 
  * 4 tbsp cocoa 
  * 1 glass of peanuts 
  * Flour to pick up the dough 
  * decoration: 
  * dark chocolate 
  * apricot jam 
  * roasted hazelnuts, nuts, or roasted peanuts. 



**Realization steps**

  1. whip the butter with the sugar until you have a nice cream. 
  2. add the egg yolk and the complete egg and whisk well once more 
  3. introduce the yeast, the cocoa, and the maïzena while mixing. 
  4. add the glass of roasted and crushed peanuts 
  5. add the flour in a small amount until you have a paste that is collected. 
  6. wrap the dough in cling film, and let stand for 30 minutes 
  7. spread the dough in a small quantity on a lightly floured work surface and cut small cupcakes with the shortbread mold of your choice. 
  8. place the cakes as you go on a lightly floured baking tray. 
  9. bake in a preheated oven at 180 degrees for almost 15 minutes, you have to watch the cooking, because it can cook faster. 
  10. Once all the shortbreads are cooked and cooled, assemble them by two with jam 
  11. melt the dark chocolate in a bain marie 
  12. place the shortbread on a rack, and cover the chocolate with a spoon. 
  13. You can garnish shortbreads with toasted hazelnuts, nuts or roasted peanuts. 



[ ![sanded 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sabl%C3%A9-1.jpg>)
