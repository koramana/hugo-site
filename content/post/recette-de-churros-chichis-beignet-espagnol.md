---
title: recipe of Churros / Chichis, Spanish donut
date: '2013-02-12'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/22421539.jpg
---
![churros](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/22421539.jpg)   
Hello everybody, 

here is a very good recipe for [ donuts ](<https://www.amourdecuisine.fr/categorie-11700496.html>) Spanish, which looks like our **zlabia** called bled **Sbae laeroussa** , a delightful [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) . 

in any case, these churros that taste great with [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale-100020029.html>) (which I prefer), nutella, or [ white chocolate spread ](<https://www.amourdecuisine.fr/article-pate-a-tartiner-au-chocolat-blanc-99781140.html>) (what my children prefer) 

my husband is just with a little icing sugar. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/224215201.jpg)

**recipe of Churros / Chichis, Spanish donut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/22421539.jpg)

Recipe type:  donut  portions:  40  Prep time:  30 mins  cooking:  5 mins  total:  35 mins 

**Ingredients**

  * 500 g of flour 
  * 500 ml of water 
  * 1 teaspoon of salt 
  * 1 teaspoon vanilla 
  * 1 teaspoon of sugar (it's not in the original recipe, but add because I did not want to brush mine in sugar) 
  * 1 oil bath for frying 
  * sugar to sprinkle 



**Realization steps**

  1. Boil the water and salt in a nonstick pot of preferences. 
  2. Pour the flour at once into the pot, remove from heat and mix vigorously with a wooden spoon,   
the dough should have the consistency of a thick choux pastry, depending on the brand of the flour you need more water then add hot water if the dough is too dry. Here is the consistency that it must have:   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/224215211.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/224215211.jpg>)
  3. To put the dough in a churros apparatus with the biggest end, or then in a pocket of sleeve also with a big end, I used to do it with my apparatus for cream 
  4. the mouthpiece was a little small, and it was tiring to make the dough come out, and in addition, the result of the churros a little lean   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/224215311.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/224215311.jpg>)
  5. Put on top of the frying bath and drop in the churros dough by making them stand out from the appliance and cut it with a knife of the dough stick of 15 cm long. 
  6. Let them brown the churros and drain them, then sprinkle the sugar. 



![churros](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/22421539.jpg)

I can assure you that it was very good 

Bon Appétit 
