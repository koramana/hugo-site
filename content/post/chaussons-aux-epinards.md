---
title: spinach slippers
date: '2016-05-30'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, chaussons
- Algerian cuisine
- plats vegetariens
- ramadan recipe
tags:
- Ramadan 2016
- Ramadan
- Easy cooking
- Healthy cuisine
- Ramadan 2015
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chaussons-epinards-champignons-021.CR2_thumb.jpg
---
##  spinach slippers 

Hello everybody, 

Frankly, there's no more delicious than these spinach spinach turnovers and super melting at the same time, with spinach and mushrooms sautéed and well seasoned covered with a generous layer of melted cheese and then wrapped quickly in puff pastry. It's child's play this recipe. 

These spinach slippers are my express recipe and especially during Ramadan, when I go out all day and I come home tired and no time to make my husband boureks to accompany his [ jari frik ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) . usually it's either spinach turnovers, because spinach cooks quickly, or [ coca with puff pastry ](<https://www.amourdecuisine.fr/article-coca-a-la-pate-feuilletee.html>) , especially the latter, after having prepared a good [ tchektchouka ](<https://www.amourdecuisine.fr/article-tchektchouka-cuisine-algerienne.html>) , it is placed between two puff pastries and baked hop ... .. 

The recipe in Arabic: 

**spinach slippers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chaussons-epinards-champignons-021.CR2_thumb.jpg)

Recipe type:  entrance, amuses mouths  portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 1 bunch of fresh chopped spinach. 
  * 7 - 8 mushrooms cut into small pieces. 
  * 2 garlic cloves, finely chopped. 
  * 1 tsp black pepper 
  * squares of cheese special burger. 
  * 2 rolls of dough feuilletees (for 12 small slippers) 
  * Olive oil 
  * Salt 



**Realization steps**

  1. In a pot. Heat the olive oil. 
  2. Add the garlic and sauté. 
  3. Add the chopped spinach, mushrooms, salt, black pepper. Mix everything well. 
  4. Cook for 20 minutes. 
  5. Thaw and cut your puff pastry sheets into the desired shape (rectangle 12 cm by 5 cm for me). 
  6. Fill with about 2 tablespoons of the stuffing. cover with cheese and close carefully. Be careful not to overfill your dough.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chaussons-epinards-champignons_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chaussons-epinards-champignons_thumb.jpg>)
  7. brush the top of your egg yolk slippers, to have a nice color towards the end. 
  8. cook in an oven preheated to 150 degrees C, until the slippers swell well then increase the oven to 180 degrees C to brown the slippers. 



serve with a fresh salad and enjoy 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
