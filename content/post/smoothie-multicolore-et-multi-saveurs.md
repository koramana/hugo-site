---
title: Multicolored and multi-flavored smoothie
date: '2017-07-31'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-multicolore-et-multi-saveurs-1.jpg
---
[ ![Multicolored smoothie and multi flavors 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-multicolore-et-multi-saveurs-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-multicolore-et-multi-saveurs-1.jpg>)

##  Multicolored and multi-flavored smoothie 

Hello everybody, 

I always continue with my saga of smoothies, but not to be fed up, you have to vary, you have to change, you always need a new taste, a new color. It happens to me even to redo one of my smoothies especially when it is good, not too much caloric and which stalls well the stomach until the midday meal, hihihihi. 

Today, it is a multicolored smoothie multi flavors that will surprise you with each sip. It takes a little time, because you have to put in the freezer for 10 minutes between each layer of smoothie, but the result will make you forget all this waiting time.   


**Multicolored and multi-flavored smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-multicolore-multi-saveurs-2.jpg)

portions:  4  Prep time:  15 mins  total:  15 mins 

**Ingredients** For the banana base: 

  * 8 ice cubes 
  * 1 ripe banana 
  * 220 ml of natural yoghurt 
  * 300 ml orange squeezed juice 

For the second layer: 
  * ⅓ of the remaining banana base 
  * 1 mango cube 
  * Orange juice according to the desired consistency of the smoothie 

For the 3rd layer: 
  * ⅓ of the banana base 
  * 1 tablespoon bitter cocoa 
  * Orange juice in the desired consistency 

For the 4th layer: 
  * ⅓ of the remaining banana base 
  * 1 glass of frozen raspberries 
  * Orange juice in the desired consistency 



**Realization steps**

  1. to speed up the cooling process more quickly, try to work with the fruit cut into small cubes and put in the freezer in advance. 
  2. empty a place in your freezer and place the glasses in which you will present your smoothie 
  3. Prepare the banana base by mixing the ingredients well for layer 1 
  4. pour in 4 glasses filling the ¼ of the verrine. 
  5. place in the freezer for 10 minutes the time to prepare the second layer 
  6. prepare the second layer with the given ingredients, add the quantity of freshly squeezed orange juice according to your taste 
  7. make sure that the first base layer holds well before pouring on the second layer. 
  8. put back in the freezer for another 10 minutes 
  9. continue in the same way with the two remaining layers. 
  10. place the smoothies now in a cool place, and remove from the refrigerator at least 10 minutes before introducing. 



[ ![multicolored smoothie multi flavors](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-multicolore-multi-saveurs.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-multicolore-multi-saveurs.jpg>)
