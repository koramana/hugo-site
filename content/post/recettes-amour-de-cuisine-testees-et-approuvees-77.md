---
title: Kitchen love recipes tested and approved 77
date: '2015-07-31'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-58.html/dyouls-de-kari-ben>)

Hello everybody, 

I lingered to share again this article of my recipes tested and approved by your care, because I'm on vacation in Algeria for 2 weeks, and I admit having a very bad connection where I am. 

As a result, it is difficult for me to insert the photos in my articles. Nevertheless, I try to share with you the minimum of the photos that I could join ... if you do not see your photo on this article, look in the next, because it's the max of photos that I managed to insert. 

I thank you all for your fidelity and your comments, I try to answer as many as possible. Thank you to you who realize my recipes with confidence, happy for you to have succeeded, and even more happy to see the pictures of your achievements. 

Do not hesitate too who have not yet sent the photos of your achievements, share them with me, or by email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

[ ![growech Rima](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/growech-Rima.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/growech-Rima.jpg>)

[ Griwech ](<https://www.amourdecuisine.fr/article-griwech-gateau-algerien-en-video.html>) at Rima 

[ ![batbout.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/batbout.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/batbout.bmp.jpg>)

[ batbout ](<https://www.amourdecuisine.fr/article-batbout-marocain.html>) at Assia K 

[ ![lemon rings](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/anneaux-citronn%C3%A9s.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/anneaux-citronn%C3%A9s.jpg>)

[ Lemon rings ](<https://www.amourdecuisine.fr/article-anneaux-citronnes-aux-amandes-effiles.html>) at Claire Kob 

[ ![pyramids with almonds](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pyramides-aux-amandes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/pyramides-aux-amandes.jpg>)

[ pyramids with almonds ](<https://www.amourdecuisine.fr/article-les-pyramides-aux-amandes-gateaux-algeriens-2015.html>) at Claire Kob 

[ ![djawzia Ishak tart](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tartelette-djawzia-Ishak.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tartelette-djawzia-Ishak.jpg>)

[ tartlets with djawzia cream ](<https://www.amourdecuisine.fr/article-tartelettes-a-la-creme-djouzia-djawzia.html>) at Ishak Ghadj 

[ ![Ishak dates truffles](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/truffes-aux-dattes-Ishak.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/truffes-aux-dattes-Ishak.jpg>)

[ chocolate truffles and dates ](<https://www.amourdecuisine.fr/article-truffes-chocolat-dattes.html>) at Ishak Ghadj 

[ ![ghribia has chickpea flour at ishak](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/ghribia-a-la-farine-de-pois-chiche-chez-ishak.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/ghribia-a-la-farine-de-pois-chiche-chez-ishak.jpg>)

[ Ghribia has chickpea flour ](<https://www.amourdecuisine.fr/article-ghribia-a-la-farine-de-pois-chiche-gateau-sec-et-fondant.html>) at Ishak Ghadj 

[ ![gsablés sesame grains at ishak](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gsabl%C3%A9s-aux-grains-de-sesames-chez-ishak.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gsabl%C3%A9s-aux-grains-de-sesames-chez-ishak.jpg>)

[ sesame seed shortbread ](<https://www.amourdecuisine.fr/article-sables-aux-grains-de-sesames.html>) at Ishak ghadj 

[ ![cakes at Djami Bou's](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/les-gateaux-chez-Djami-Bou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/les-gateaux-chez-Djami-Bou.jpg>)

[ halwat tabaa ](<https://www.amourdecuisine.fr/article-halwat-tabaa-gateau-algerien-sec.html>) , [ shortbread with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture.html>) , [ Nutella shortbread ](<https://www.amourdecuisine.fr/article-sables-au-nutella.html>) at Djami Bou's place 

[ ![maamoul arwa](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/maamoul-arwa.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/maamoul-arwa.jpg>)

[ maamoul to the Turkish halwa ](<https://www.amourdecuisine.fr/article-maamoule-a-la-halwa-turc-gateau-sec.html>) at Arwa Naili 

[ ![katia ben bow ties](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/les-noeuds-papillons-katia-ben.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/les-noeuds-papillons-katia-ben.jpg>)

[ Cakes bow ties ](<https://www.amourdecuisine.fr/article-gateau-algerien-noeuds-papillons.html>) at Katia Ben 

[ ![flan cheesecake samy aya](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/flan-cheesecake-samy-aya.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/flan-cheesecake-samy-aya.jpg>)

[ Flan cheesecake ](<https://www.amourdecuisine.fr/article-flan-cheesecake.html>) at Fleur Dz 

[ ![fluffy cake with strawberries](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateau-moelleux-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateau-moelleux-aux-fraises.jpg>)

[ fluffy cake with strawberries ](<https://www.amourdecuisine.fr/article-tarte-moelleuse-aux-fraises-et-amandes.html>) at Fleur Dz 

[ ![Ziati matloue](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-Ziati.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/matloue-Ziati.jpg>)

[ matloue ](<https://www.amourdecuisine.fr/article-pain-arabe-matloue.html>) chez Saida Ziati 
