---
title: Moroccan Chebakia Mkherka
date: '2016-10-23'
categories:
- Algerian cakes with honey
- fried Algerian cakes
- gateaux algeriens- orientales- modernes- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/chebakia-marocaine1.jpg
---
![Moroccan Chebakia Mkherka](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/chebakia-marocaine1.jpg)

##  Moroccan Chebakia Mkherka 

Hello everybody, 

I share with you today a delight of Moroccan cuisine, which I always like to realize, but I lingered to make the decision to take action ... 

**Moroccan Chebakia (Mkherka)**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/chebakia-marocaine1.jpg)

Recipe type:  cake  portions:  10  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * 1 kg of flour, 
  * 190 g of grilled and ground sesame seeds, 
  * 60 g of almond powder, 
  * 100 g melted butter, 
  * 100 g of extra virgin olive oil, 
  * 2 tbsp. vinegar, 
  * 1 teaspoon of orange blossom water, 
  * 2 tbsp. cinnamon, 
  * 2 tablespoons anise powder, 
  * 1 sachet of baking powder, 
  * a pinch of salt, 
  * 1 C. baking yeast, 
  * some saffron pistils, 
  * food coloring (saffron, I did not have any) 
  * water or orange blossom water to pick up the dough. 
  * oil for frying, 
  * 1 kg of honey to coat the cakes, 
  * golden sesame seeds to decorate. 



**Realization steps**

  1. In a large container, put all ingredients except flour and baking powder. 
  2. Mix well and then add the sifted flour with the yeast, 
  3. add the amount of water needed to have a compact paste. work well using the palm of your hand to have a homogeneous paste, smooth but not very flexible. 
  4. Divide the dough into several pieces, form balls and wrap them in plastic and let it cool for 15 minutes. 
  5. Take the dough out of the fridge and spread it out finely with a rolling pin. 
  6. Using a roulette, cut rectangles of 5 cm, in each rectangle made cuts 4 or 5 (depending on the size you want to give to your chebakia), then fold the chebakia to have the shape of a flower . 
  7. Dip the chebakia in a hot oil bath and brown on both sides, 
  8. dip them immediately in honey heated in a bain-marie and flavored with a c. orange blossom. 
  9. Then sauté in a colander and decorate with sesame seeds. 
  10. Once cooled, keep the chebakia in an airtight container. 



###  Moroccan chebakia (Mkherka) 

Ingredients 

  * 1 kg flour, 
  * 190 g of sesame toasted and grounded 
  * 60 g almonds powder, 
  * 100 g melted butter, 
  * 100 g extra virgin olive oil 
  * 2 vinegar tbs, 
  * 1/4 cup orange blossom water, 
  * 2 tsp ground cinnamon, 
  * 2 tbs ground anise 
  * 1 tbs baking powder, 
  * a pinch of salt, 
  * 1 tsp dried yeast, 
  * some saffron threads, 
  * food coloring 
  * water or orange blossom water for the dough. 
  * oil, for frying 
  * 1 kg of honey, 
  * golden unhulled sesame seeds, toasted, for decorating. 

preparation 
  1. In a large bowl, place all the ingredients except flour and baking powder. 
  2. Mix well and then add the sifted flour and baking powder, 
  3. addenough water to have a tender dough. 
  4. Knead the dough by hand for seven to eight minutes or in a mixer with dough hook for four to five minutes. 
  5. Divide the dough into four portions, shape each into a smooth ball, and place the dough in a plastic bag to rest for 10 to 15 minutes. 
  6. Take one ball, and roll it out to the thickness of a thin piece of cardboard. Lightly flour your work surface if necessary. 
  7. Using a pastry cutter, cut rectanglesapproximately the size of your palm 
  8. Make evenly spaced cuts lengthwise in each rectangle. These cuts should be almost the length of the rectangle, but should not be cut to the edges of the dough. The resulting rectangle will have five strips of attached dough. 
  9. then bend chebakia to have the shape of a flower take a look at the video above. 
  10. Heat oil in a large deep frying pan over medium heat, cook the chebakia in batches. Adjust the heat as slowly as possible to a medium brown color 
  11. When the chebakia are cooked, use a strain to transfer them from the oil directly to the hot honey. 
  12. When the chebakia has finished soaking, remove them from the honey to a strainer or colander, and allow them to drain for a few minutes and garnish with sesame seeds. 
  13. Once cooled, store in an airtight container chebakia. 



Video de préparation: 
