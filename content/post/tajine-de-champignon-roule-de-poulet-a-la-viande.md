---
title: mushroom tajine rolled chicken with meat
date: '2018-02-10'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- Mina el forn
- recipe for red meat (halal)
- ramadan recipe
- chicken meat recipes (halal)
tags:
- Ramadan 2015
- Ramadan
- Algeria
- Minced meat
- dishes
- sauces
- Olive

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-champign-et-roul%C3%A9-de-poulet-a-la-viande.jpg
---
[ ![mushroom tajine rolled chicken with meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-champign-et-roul%C3%A9-de-poulet-a-la-viande.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-champignon-roule-de-poulet-a-la-viande.html/tajine-de-champign-et-roule-de-poulet-a-la-viande>)

##  mushroom tajine-rolled chicken with meat 

Hello everybody, 

Here's a recipe for mushroom tajine rolled chicken meat that really deserves to be tested. This dish is to die for, it is so good that my husband and my dad blamed me for not having done much (and yet I made a very large pot, gourmands!). A dish to enjoy without moderation so much that it's good. 

This mushroom meatball tajine recipe is inspired by the talented **Umm amani** El djelfa forum ... A woman so generous who gives always successful recipes. Except that Umm amani in his recipe used merguez, and since I did not have a merguez, I made with minced meat. 

Even if this meat chicken meat mushroom tajine recipe seems to you full of meat, in my humble opinion, it is not, because if you use merguez, 5 or 6 pieces will suffice (for 5 or 6 people ), in addition to half a chicken breast, and for the sauce, according to your taste, you can add pieces of chicken, as you can do without. For my part, the chicken legs were a plus, just to have a better picture, hihihihi   


**mushroom tajine rolled chicken with meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-champignon-et-son-roul%C3%A9-de-poulet-farci-a-la-viande-.jpg)

portions:  4  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients** for the sauce: 

  * 4 chicken legs (otherwise 2 will be enough, because it will be more, personal, I put just for a nice picture) 
  * 1 medium onion 
  * 2 cloves garlic 
  * ½ bunch of parsley 
  * 1 tablespoon of smen, 
  * salt and black pepper 
  * 300 gr mushrooms in a box 
  * 1 handful of pitted green olives, or + or - according to the desire. 

for chicken rolls: 
  * the half of a chicken breast 
  * 200 to 300 gr of minced meat 
  * salt, black pepper, caraway powder 
  * ½ bunch of parsley 
  * 1 egg yolk 
  * 1 little bread crumb dipped in milk 
  * 1 clove of garlic 



**Realization steps** Prepare the sauce 

  1. fry the onion and garlic crushes in the smen, 
  2. add the pieces of meat, and the chopped parsley, or in small bundle tied according to the taste. 
  3. add water, salt and pepper, and cook (not too much salt especially) 

prepare chicken rolls stuffed with minced meat: 
  1. Begin by preparing the meat stuffing, adding the chopped parsley, chopped garlic, salt, black pepper, cumin, egg yolk, and the crumb of bread drained from its milk. 
  2. Combine it to season all the meat well. 
  3. Prepare sausages of almost 12 cm on 2   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/boudin-de-viande-hach%C3%A9e-480x462.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42133>)
  4. just flour a little, and fry in a little oil 
  5. let cool on paper towels. 
  6. chop the piece of chicken breast 
  7. add salt, black pepper, chopped garlic and chopped parsley 
  8. mix everything well, then spread it in a rectangle of almost 5 mm in height. 
  9. cover each pudding well with the chicken breast (if the chicken meat sticks in your hand, wet your hands a little)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/roul%C3%A9-de-blanc-de-poulet-a-la-viande-hach%C3%A9e-5120x3413.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42134>)
  10. fry in the same frying oil, just to give a nice color to the little chicken rolls. 
  11. let drain a little on paper towels. 
  12. in the same oil, fry the sliced ​​mushrooms. 
  13. add them to the white sauce and add the chicken rolls. 
  14. add the pitted olives previously boiled to desalt them a little. 
  15. let simmer a little, so that the sauce is a little reduced, check the salt and remove fire. 
  16. You can fry the chicken legs just to give them a little color. 
  17. during the presentation, remove the rolls of chicken with minced meat, and cut them into slices but at an angle (ie knife inclined), 
  18. garnish the dish, and cover with mushroom sauce. 



[ ![mushroom tajine rolled chicken with meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-champignon-et-roul%C3%A9-de-poulet-a-la-viande-1-.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-champignon-roule-de-poulet-a-la-viande.html/tajine-de-champignon-et-roule-de-poulet-a-la-viande-1>)
