---
title: shrimp in tomato sauce
date: '2015-02-18'
categories:
- Algerian cuisine
- Moroccan cuisine
- Tunisian cuisine
tags:
- Healthy cuisine
- Easy cooking
- Algeria
- accompaniment
- inputs
- Morocco
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Crevettes-en-sauce-tomates.CR2_.jpg
---
[ ![shrimps in tomato sauce](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Crevettes-en-sauce-tomates.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-crevettes-en-sauce-tomate.html/crevettes-en-sauce-tomate-2>)

Hello everybody, 

Here's a recipe I really care about, shrimp in tomato sauce, but it's not just any shrimp, it's fresh shrimp from the Mediterranean. A double delight, a double freshness, a double intense and incomparable taste of this white meat, very crunchy and sweet on the palate. 

This recipe is on my PC since Ramadan 2013, and I completely forgot to post it, until yesterday one of my readers ask me how to cook the sea cicadas (you know you?) I got the chance to taste these delicious crustaceans every time I go to Algeria, we like a lot at home. So I came looking for the recipe on the blog to pass the link to the girl, but I did find recipes. 

I went to get the photos, and that's when I came across the photos of these shrimps in tomato sauce, and it awakens all my memories and all the tastes, especially that this dish is prepared with tomato of the small my mother's garden, so 100% organic. 

**shrimp in tomato sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/crevettes-en-sauce-tomate.CR2_.jpg)

portions:  6  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 1 kg of cleaned shrimp 
  * 800 gr of crushed fresh tomatoes 
  * 3 tablespoons of extra virgin olive oil 
  * salt, black pepper. 
  * ½ teaspoon powdered ginger 
  * ½ teaspoon of cumin powder 
  * 2 cloves garlic 
  * 2 bay leaves 
  * 1 tbsp of harissa (optional) 
  * some branches of thyme 
  * ½ glass of water as needed 
  * a few sprigs of chopped parsley 



**Realization steps**

  1. Clean the shrimp and remove the head and pasta, you can leave the tail. 
  2. place the pan over medium heat with a little oil. 
  3. when the oil is hot, add the shrimp, sauté well and cover. 
  4. let the shrimp cook a little 
  5. remove the shrimp with a slotted spoon. 
  6. place the tomato in the same pan now, add all the remaining ingredients, and cook. 
  7. When the sauce boils well, put the shrimp in the sauce, if you think that your sauce is a little dry, you can add a little water. 
  8. let reduce a little, while keeping a juicy sauce. 



[ ![shrimps in tomato sauce](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/crevettes-en-sauce-tomate-2.jpg) ](<https://www.amourdecuisine.fr/article-crevettes-en-sauce-tomate.html/crevettes-en-sauce-tomate-2>)
