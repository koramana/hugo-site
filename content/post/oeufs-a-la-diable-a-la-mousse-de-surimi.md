---
title: Devil eggs with surimi mousse
date: '2016-03-01'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- Healthy cuisine
- salads, salty verrines
tags:
- Garni eggs
- Mimosa
- Mayonnaise
- inputs
- Aperitif
- Buffet
- salads

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/oeufs-a-la-diable-a-la-mousse-de-surimi-4.jpg
---
[ ![devil eggs with surimi mousse 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/oeufs-a-la-diable-a-la-mousse-de-surimi-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/oeufs-a-la-diable-a-la-mousse-de-surimi-4.jpg>)

##  Devil eggs with surimi mousse 

Hello everybody, 

I'm sure you know and love the mimosa eggs, or the devil's, or simply the stuffed eggs. At home, we like a lot, besides my children prefer to eat stuffed eggs instead of eating them hard, because they say that the egg yolk suffocates them, of course children can give you all the excuses of the world, hihihihi, and with mine, that's not missing! 

Today, I share with you the recipe of the eggs facris to the mousse of surimi, I had to add a little anchovies in the foam, to give this small side spicy and salty, but I had forgotten that I had used them in the pizza the day before, hihihih.   


**Devil eggs with surimi mousse**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/oeufs-a-la-diable-a-la-mousse-de-surimi-2.jpg)

portions:  10  Prep time:  5 mins  cooking:  8 mins  total:  13 mins 

**Ingredients**

  * 5 eggs 
  * 3 anchovy fillets (I did not have any, but the anchovies give a superb taste to this mousse) 
  * 4 surimi stick 
  * 2-3 c. tablespoons [ homemade mayonnaise ](<https://www.amourdecuisine.fr/article-mayonnaise-fait-maison-rapide-et-facile.html>)   
I prefer homemade mayonnaise, because it has no sugar in it, it has a little garlic, it is more acidic, so it adds good flavor to this mousse) 



**Realization steps**

  1. First, cook the eggs, and let cool. 
  2. cut the eggs out of two and remove the egg yolk. 
  3. wash the anchovies for desalting a little and place them on paper towels. 
  4. In the bowl of the blender, place cubed surimi sticks, chopped anchovy fillets, mayonnaise and hard-boiled egg yolks. 
  5. mix to have a light and frothy consistency (if necessary add more mayonnaise). 
  6. fill a pastry bag, and shape flowers to fill the egg whites 
  7. Keep chilled until serving 



[ ![devil eggs with surimi mousse 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/oeufs-a-la-diable-a-la-mousse-de-surimi-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/oeufs-a-la-diable-a-la-mousse-de-surimi-3.jpg>)
