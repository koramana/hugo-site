---
title: Bavarian mascarpone / toblerone
date: '2011-02-05'
categories:
- gateaux, et cakes
- recettes sucrees
- pies and tarts

---
A delight that I prepared this week, and that we really liked, my children and I, is a Bavarian, made of a mascarpone-based mousse (you know how much I love mascarpone ) So the marriage of these two beautiful mousses to give a taste really delicious, and it will be a dessert to redo with great pleasure. for the ingredients: Genoise: 2 eggs 60 gr of sugar 50 gr of flour 1/2 teaspoon of baking powder 10 gr of almonds Preheat the oven to 180 °. Separate the whites from the yolks. Fit the blanks & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

A delight that I prepared this week, and that we really liked, my children and I, is a Bavarian, made of a mascarpone-based mousse (you know how much I love mascarpone )   


so the marriage of these two beautiful mousses to give a taste really delicious, and it will be a dessert to redo with great pleasure. 

for the ingredients: 

Genoese: 

  * 2 eggs 
  * 60 gr of sugar 
  * 50 gr of flour 
  * 1/2 teaspoon baking powder 
  * 10 gr of almonds 


  1. Preheat the oven to 180 °. 
  2. Separate the whites from the yolks. 
  3. Beat the egg whites until stiff, adding the pinch of salt. 
  4. When they are firm, gradually add the sugar and continue whisking. 
  5. Suddenly, add the egg yolks. Beat again. 
  6. Add the flour in the rain while continuing to whisk with the mixer. 
  7. Add the almond. 
  8. Once the dough is homogeneous, pour it into a mold butter and flour or line with baking paper and bake for 20 minutes. 
  9. the sponge cake will be colored, it is cooked when the edges retract the walls of the mold. 
  10. remove the cooked layer and sprinkle the sponge cake with an orange juice (you can sprinkle with a syrup that you have already prepared) 



I garnished my sponge cake with some pitted Cherries 

Ingredient of mascarpone mousse: 

  * 250 gr of mascarpone 
  * 2 eggs 
  * 60 gr of sugar 
  * 5 gr of gelatin 
  * 2 tablespoons icing sugar 
  * 35 gr of almond powder 


  1. In a salad bowl, beat the yolks and sugar well, 
  2. add the cold mascarpone, whisk again. 
  3. Heat the 2 tablespoons of water in a small bowl in the microwave (without boiling!), Dissolve the gelatin, 
  4. mix until a liquid is obtained (there should not be a piece of gelatin left). 
  5. Stir in mascarpone, whisk to homogenize everything. 
  6. Beat the egg whites in very firm snow. 
  7. When they are mounted, gradually incorporate the icing sugar while whisking until a smooth, firm and shiny meringue is obtained. 
  8. Whisk the mascarpone whites very gently with a spatula, lifting the mass well. This step can take a long time because the mascarpone preparation is thick. 
  9. Add the almond powder, and pour on the sponge cake. spawn for at least 1 hour. 



Chocolate mousse ingredients: 

  * 50 gr of black Toblerone 
  * 50 gr of white chocolate 
  * 30 gr of butter 
  * 2 eggs 
  * 10g of sugar 



Method of preparation: 

  1. beat the egg white in firm snow, 
  2. and whip the egg yolk with the sugar. 
  3. in a saucepan melt the toblerone with half of the butter, 
  4. and in another saucepan melt the white chocolate with the other half of the butter. 
  5. add half of the egg yolk / sugar mixture and then add half of the egg white just gently to form a mousse. 
  6. do the same thing with white chocolate, ie add the egg yolk / sugar mixture, then add the egg white very slowly. 
  7. take your two chocolate mousses, pour them on the first mascarpone mousse, then form a marbling to your taste, 
  8. cool, for 2 hours or more. 



Bonne dégustation 
