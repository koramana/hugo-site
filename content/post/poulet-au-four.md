---
title: Baked Chicken
date: '2013-11-19'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- Mina el forn
- recipe for red meat (halal)
- ramadan recipe
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/poulet-au-four_thumb.jpg
---
##  Baked Chicken 

Hello everybody, 

here is a delicious roast chicken, which I like to prepare for my little family, when I have a chicken not cut yet, and I want a quick meal. 

A chicken that I marinate at the beginning of the day, and 3 hours before dinner, I cook in the oven, until the chicken is well roti. 

recipe in Arabic: 

**Baked Chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/poulet-au-four_thumb.jpg)

Recipe type:  dish, party recipe  portions:  6  Prep time:  20 mins  cooking:  90 mins  total:  1 hour 50 mins 

**Ingredients**

  * a chicken of almost 1.5 kilos 
  * 4 to 5 tablespoons of table oil (extra virgin olive oil for me) 
  * 1 teaspoon of cumin 
  * ½ teaspoon of paprika 
  * ½ teaspoon of black pepper 
  * ½ teaspoon garlic powder 
  * ½ teaspoon coriander powder 
  * salt according to taste 



**Realization steps**

  1. clean the chicken well 
  2. mix all the spices and salt in the oil, then with a brush, cover all the chicken (inside and outside) 
  3. marinate all night, or prepare in the morning for roasting at night. 
  4. do not worry but the oil will cover the chicken well, allowing the spices to penetrate the skin 
  5. I use my oven with the roasting pin, but you can put on a rack 
  6. place over a large mold with water in it, it allows the chicken to cook without being too dry. 
  7. cook the chicken for at least 1h30. 
  8. at the end of the cooking, recover the juice in the large pan (the water would have evaporated well) add a little water to have a good juice 
  9. serve with a mashed potato (it was for me and my husband) 
  10. or with fries (with spices for children), or according to your taste. 


