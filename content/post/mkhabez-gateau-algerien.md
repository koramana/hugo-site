---
title: mkhabez, Algerian cake
date: '2013-10-31'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/mkhabez-el-louz-1.jpg
---
##  mkhabez, Algerian cake 

Hello everybody, 

Who is who does not know the mkhabez? this Algerian cake all flavored almond-based flavored with fresh lemon zest, and topped with a melting layer of well-stocked ice cream that does not leave you indifferent. 

The recipe of mkhabez [ Algerian cake ](<https://www.amourdecuisine.fr/gateaux-algeriens-orientales-modernes-fete-aid>) that I share with you today is the recipe of lunetoiles that I tasted with pleasure, following his beautiful package that she had sent me, a recipe that I will share with you, we say "gouty and approved" hihihihihihi. 

His recipe is not so different from mine, except side frosting that I realize with a touch of milk that adds more fondant to this sweetness 

my recipe [ mkhabez almond cake ](<https://www.amourdecuisine.fr/article-mkhabez-gateaux-aux-amandes.html>) and its very melting icing: 

![mkhabez el louz 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/mkhabez-el-louz-1.jpg)

**mkhabez, Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/mkhabez4_thumb.jpg)

Recipe type:  Algerian cake  portions:  10  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** ingredients: 

  * 500 g finely ground almonds 
  * 300 g sifted icing sugar 
  * 2 tbsp. to c. of lemon zest 
  * 1 C. to c. of vanilla extract 
  * 2 whole eggs + 1 yellow by size 

Icing: 
  * 3 egg whites 
  * 3 c. to s. of lemon juice 
  * 3 c. to s. of orange blossom water 
  * sifted icing sugar 
  * 2 tbsp. to s. of table oil 
  * blue and green dye / sweet almond flavor 
  * pink dye / strawberry aroma 
  * purple dye / blackcurrant flavor 



**Realization steps**

  1. Mix the almonds, the icing sugar, the zest of 
  2. lemon, vanilla extract and wet with the eggs to get a 
  3. manageable and firm dough. 
  4. On a floured work plan lower the dough on a 
  5. thickness of 3 cm and cut rounds of 4 cm in diameter using a 
  6. cutter. 
  7. Place them on a greased and floured plate, cook for 20 to 30 minutes at 140 °. 
  8. Cool on a baking rack. 

Preparation of the icing: 
  1. Prepare the icing with the given ingredients, add the oil last and mix. 
  2. Proceed with coloring by adapting to each color its aroma. 
  3. Check the icing by testing a cake. 
  4. Ice the cakes. Let them dry completely, remove the excess frosting. 
  5. Pass the silvery food gloss on the surface using a food sponge. Decorate the pieces by adapting to each color its decoration. 
  6. Use perforated freezer bags with a toothpick in a corner, to make dots, scallops, branches, etc. 
  7. Garnish the center of each cakes with a white daisy, a bouquet of three daisies degraded in pink and white, a flower by raising the petals, small roses and green leaves made with green icing. 



**thank you to lunetoiles, and thank you for all your comments, do not forget to subscribe to my newsletter if you want to be with any update articles and recipes of my blog.**

bisous 
