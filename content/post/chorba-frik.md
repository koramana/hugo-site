---
title: chorba Frik
date: '2015-06-18'
categories:
- Algerian cuisine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-2-006.CR2_1.jpg
---
[ ![chorba-frik-2-006.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-2-006.CR2_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-2-006.CR2_1.jpg>)

##  chorba Frik 

Hello everybody, 

I prepare the chorba frik almost once a week, but I noticed that I did not have a nice picture of this delicious dish on my blog, so I go back my old recipe, and I put you new photos. 

this is a traditional recipe that marks the table Algerian especially in the holy month of Ramadan, here my husband often asks, almost every week, and I have a friend of the yemen who loves him very much and always asks me the recipe, so I give it to him, and certainly there are many women who make it, each one his ingredients, there are who likes it with vegetables, others without, and each the taste of his family, to accompany this chorba frik I advise you these [ input recipes ](<https://www.amourdecuisine.fr/categorie-10678929.html>)

The recipe in Arabic is here: 

you can see here: [ Ramadan dishes ](<https://www.amourdecuisine.fr/article-plats-du-ramadan-2014.html>)

and here [ bourak and briks for ramadan ](<https://www.amourdecuisine.fr/article-recettes-speciales-ramadan-2014-les-boureks-et-bricks.html>)

other [ soups for ramadan ](<https://www.amourdecuisine.fr/article-menu-ramadan-2014-les-soupes.html>)

[ Breads and cakes for Ramadan ](<https://www.amourdecuisine.fr/pain-pain-traditionnel-galette>)

different [ Ramadan salads ](<https://www.amourdecuisine.fr/article-menu-du-ramadan-2014-les-salades.html>)

full of [ desserts, juices and flans for Ramadan ](<https://www.amourdecuisine.fr/article-index-de-flan-desserts-jus-et-boisson-ramadan-2014.html>)

and for more recipes you can see my [ ramadan recipes 2014 ](<https://www.amourdecuisine.fr/article-tag/recettes-de-ramadan-2014>)

so without delay I go to 

**chorba Frik**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/huit-0111.jpg)

portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * my ingredients 
  * chicken or meat 
  * 1 onion 
  * oil 
  * 1 and ½ cases of canned tomato 
  * 1 handful of chickpeas soaked the day before 
  * 1 bunch of coriander 
  * 1 small celery stalk 
  * 1 glass of wheat water crushed green (frik) 
  * salt, coriander powder, black pepper 



**Realization steps**

  1. cut the meat into cubes or the chicken into pieces, 
  2. place the meat in a pot, add the oil and chopped onion, chopped coriander, 
  3. add the celery stalk, salt, black pepper, and coriander powder, 
  4. let it simmer a little over low heat, the meat will drop from its water, and the onion will become very translucent 
  5. add the preserved tomato diluted in the water, let it return a few more minutes, and add the water, until you cover the meat 
  6. half-cooking the meat pour the green wheat crushed in the broth,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frick-300x248.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frick.jpg>)
  7. to cook and towards the end, also add the chickpeas in box, 
  8. let simmer for a few minutes, and remove the pot from the heat 
  9. at the end of cooking, sprinkle with chopped coriander and serve hot. 



bon appétit 
