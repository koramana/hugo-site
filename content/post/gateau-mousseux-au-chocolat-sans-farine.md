---
title: flourless chocolate cake without flour
date: '2017-02-21'
categories:
- dessert, crumbles et barres

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-mousseux-au-chocolat-sans-farine1.jpg
---
![cake-sparkling-au-chocolate-in-farine.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-mousseux-au-chocolat-sans-farine1.jpg)

##  flourless chocolate cake without flour 

Hello everybody, 

to your forks and spoons to enjoy this flourless chocolate mousse cake. A recipe for non-tolerant gluten, and for everyone, you can always afford everything. 

Thank you to Lunetoiles who shares with us these delights and these little culinary discoveries, it is a pleasure for me to receive these recipes on my email. 

This flourless chocolate flour cake recipe is a super easy recipe to make and the result is to lick the chops .... 

![Sparkling cake-au-chocolate no-flour, no-gluten.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-mousseux-au-chocolat-sans-farine-sans-gluten2.jpg)

**flourless chocolate cake without flour**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-mousseux-au-chocolat-sans-farine-sans-gluten11.jpg)

**Ingredients**

  * 200 g of good dark chocolate 
  * 6 organic eggs 
  * 120 g of half-salted butter 
  * 80 g caster sugar 
  * 1 cup of very tight coffee 



**Realization steps**

  1. Preheat the oven th. 6-7 (200 ° C). 
  2. Separate the egg yolks from the whites. Beat the yolks with the sugar until you obtain a homogeneous mixture. 
  3. Beat the whites in firm snow. 
  4. Melt the butter with the chocolate and the coffee very tight. 
  5. Stir in the yellow-sugar mixture delicately with the chocolate / butter / coffee mixture and then add the whites to the snow gently. 
  6. pour everything into a buttered and floured baking tin. 
  7. Bake and cook for 15 minutes. 
  8. Take the cake out of the oven, the heart must be flowing. Let cool completely, then put in the fridge for several hours before eating. 
  9. To serve sprinkle with icing sugar and serve with a scented chantilly. 



![cake-sparkling-au-chocolate-in-gluten.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-mousseux-au-chocolat-sans-gluten1.jpg)
