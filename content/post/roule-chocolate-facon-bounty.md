---
title: Bounty chocolate roll
date: '2017-12-29'
categories:
- dessert, crumbles and bars
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- desserts
- To taste
- Breakfast
- Ramadan
- Algeria
- Algerian cakes
- delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-chocolat%C3%A9-facon-bounty-1.jpg
---
[ ![bounty chocolate way bounty 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-chocolat%C3%A9-facon-bounty-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-chocolat%C3%A9-facon-bounty-1.jpg>)

##  Bounty chocolate roll 

Hello everybody, 

The biscuits rolled and despite the fact that people think that it is cakes that take a lot of time, and ask to have the hand, in fact it is a cake very easy to realize, and we do not need too much decorating to have a presentable cake, sometimes a little cocoa, or icing sugar is your cake is ready. 

Today, the rolled biscuit is declined to the flavors of a [ Bounty ](<https://www.amourdecuisine.fr/article-bounty-maison.html>) , it's small chocolates that hide a generous stuffing with coconut ... So you're interested to have the recipe of this biscuit chocolate rolled way bounty of our dear **Exotic flower** ? 

**Bounty chocolate roll**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-chocolat%C3%A9-facon-bounty.jpg)

portions:  8  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * for the biscuit: 
  * 5 eggs 
  * 120 gr of sugar 
  * 1 pinch of salt 
  * 1 cup of vanilla sugar 
  * 4 tablespoons of cocoa 
  * ½ sachet of baking powder 
  * 150 gr of flour 

syrup: 
  * 100 ml of water 
  * 100 gr of sugar 
  * 1 to 2 tablespoons of nescafé coffee (according to your taste) 

prank call: 
  * coconut 
  * sweetened condensed milk 

decoration: 
  * [ chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat.html>)
  * cocoa 



**Realization steps**

  1. Beat eggs, salt, sugar and vanilla until blanched 
  2. add the cocoa, and the mixture of flour and baking powder 
  3. bake in preheated oven at 180 C for 15-20 min 
  4. meanwhile, prepare a syrup by cooking sugar in water for 10 minutes, then add nescafe, 
  5. as soon as the biscuit is cooked, soak it with the syrup 
  6. spread the mixture of coconut and sweetened condensed milk prepared with the screen, 
  7. roll the biscuit and let it go. 
  8. decorate with a little chocolate ganache and sprinkle cocoa over it 
  9. you can serve with vanilla custard. 



[ ![chocolate bounty roll 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-chocolat%C3%A9-facon-bounty-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/roul%C3%A9-chocolat%C3%A9-facon-bounty-2.jpg>)
