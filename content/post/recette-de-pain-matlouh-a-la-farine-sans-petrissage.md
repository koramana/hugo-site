---
title: Recipe for matlouh flour bread without kneading
date: '2017-10-27'
categories:
- bakery
- Algerian cuisine
- Cuisine by country
- Unclassified
- bread, traditional bread, cake
tags:
- Ramadan 2016
- Boulange
- Ramadan
- Bread
- Ramadan 2017
- Algeria
- Easy cooking
- Sandwich

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/10/Recette-de-pain-matlouh-%C3%A0-la-farine-sans-p%C3%A9trissage-1.jpg
---
![Recipe bread matlouh flour without kneading 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/Recette-de-pain-matlouh-%C3%A0-la-farine-sans-p%C3%A9trissage-1.jpg)

##  Recipe for matlouh flour bread without kneading 

Hello everybody, 

Here is a simple recipe bread matlouh flour and without kneading. This recipe certainly requires a little tweaking when working with the dough because it is too sticky, but the result is a bread matlouh well ventilated, very soft which has the texture of a cotton. 

On my blog you will find several matlouh recipes, [ matlou3 with semolina ](<https://www.amourdecuisine.fr/article-matlou3-recette-de-matlou-khobz-tajine.html>) , [ matlou3 with semolina and flour, ](<https://www.amourdecuisine.fr/article-matlouh-a-la-semoule-et-farine.html>) and [ matloua in the oven or khobz el couche ](<https://www.amourdecuisine.fr/article-khobz-matloua-fel-koucha-matlou3-au-four.html>) , then to you the choice! 

This version is very simple, and you only need a good wooden spoon to pick up the dough, put to lift, shape your cakes and here is a delicious matlouh flour without kneading ready for consumption, yeeeeeeeeeee! 

You can see this video recipe made by my friend Lynda Akdader: 

**Recipe for matlouh flour bread without kneading**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/Recette-de-pain-matlouh-%C3%A0-la-farine-sans-p%C3%A9trissage-2.jpg)

**Ingredients**

  * 500 g flour 
  * 350 ml warm water 
  * 1 C. baking yeast 
  * 1 C. tablespoons sugar 
  * 1 C. salt coffee 
  * 5 c. vegetable oil 



**Realization steps**

  1. in a large salad bowl or bowl of the mess, place the water very warm. 
  2. add the baker's yeast, sugar, salt and oil, mix with a spoon. 
  3. add the flour and pick up the dough with a wooden spoon, to have a homogeneous mixture. 
  4. let the dough rise by covering it and keeping it free from drafts. 
  5. When the dough has risen, place it on a floured work surface, and divide it into 3 balls. 
  6. let it rise again, then lower the balls to have patties. 
  7. cover and let rise again. 
  8. cook on a tagine slotted or in the ground, on both sides to have patties well gilded. 



![Recipe for matlouh flour bread without kneading](https://www.amourdecuisine.fr/wp-content/uploads/2017/10/Recette-de-pain-matlouh-%C3%A0-la-farine-sans-p%C3%A9trissage.jpg)
