---
title: cake with lemon and raisins
date: '2012-04-14'
categories:
- Algerian cuisine
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/2010-08-11-1233_21.jpg
---
& Nbsp; hello everyone, when I went to our dear Ratiba, and I had to eat his delicious cake, I only sought the opportunity to try his recipe, I must tell you, that since I'm in Algeria, and that I buy my wonderful cook Eniem, I am very happy with the gas oven, and especially the good cooking. In England, my stove is electric, and the settings of the oven make me see all the colors, sometimes my cakes cram down without cooking in it. so since I'm here, & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

In England, my stove is electric, and the settings of the oven make me see all the colors, sometimes my cakes cram down without cooking in it. 

so since I've been here, I'm enjoying redoing and trying the recipes that always have me, long live the gas, and cooking in the gas ovens. 

finally, I come back to the cake. 

the ratiba recipe is as follows: 

ingredients: 

  * 250g of butter 
  * 250 grams of sugar 
  * 5 eggs 
  * 350gr of flour 
  * 1 packet of dry yeast 
  * vanilla (I put lemon juice 1 tbsp soup) 
  * 100GR of dried figs (I put some raisins) 



![2010-08-11-1233 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/2010-08-11-1233_21.jpg)

preparation: 

  1. beat the butter and sugar until the mixture becomes foamy, in an electric mixer, not with a whip, it will not give the same effect. 
  2. add the eggs one by one, beating between each egg 
  3. Add the lemon juice 
  4. make the raisins trickle into a little water when it drains well from the water and into the flour. 
  5. add the flour and yeast to the butter and sugar mixture, then add the raisins 
  6. butter a cake mold bake at 200 ° C preheated for 5 minutes then lower the oven to 180 ° C 
  7. cook until the cake swells and takes a nice golden color 



let cool down before unmolding 

and enjoy, it must be said that this cake and even better, the 2nd day, if you can resist this delight 

thank you for your visits, if you like my blog, and you want to be always up to date with my publications, subscribe to the newsletter, and especially do not forget to validate the registration on your email. 

merci 
