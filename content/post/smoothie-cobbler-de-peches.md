---
title: Peach cobbler smoothie
date: '2015-11-21'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-cobbler-de-peche.jpg
---
[ ![fishing cobbler smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-cobbler-de-peche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-cobbler-de-peche.jpg>)

##  Peach cobbler smoothie 

Hello everybody, 

You will still say a smoothie !!! Well yes, I take a smoothie every morning for a long time now. If I do not make a smoothie, I make a porridge to the sound of oats, because to be honest, a sweet bean that I am, if I take a latte in the morning, it's with 2 or 3 biscuits, a big piece of cake or a good piece of bread butter and jam ... Super greedy, then my solution to avoid all this sugar intake in the morning, a smoothie with natural fruit sugar, even if I put you honey as an ingredient of my recipe, personally I do not add it. 

This time, my morning started with. a super smoothie smoothie taste of a peach cobbler ... I had two peaches that I cut into pieces and put in the freezer at night, so my smoothie was so refreshing, I put myself under my quilt again a time, and I watched TV while sipping my smoothie, yum yum ... 

**Peach cobbler smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-cobbler-de-peches-1.jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * ½ glass of oatmeal 
  * 2 peaches cut into strips and frozen. 
  * 1 glass of vanilla yogurt, otherwise 2 Actimel 
  * ½ glass of milk 
  * 1 tablespoon of honey 
  * ¼ teaspoon of vanilla extract 
  * ½ teaspoon of cinnamon powder.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cobbler.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cobbler.jpg>)



**Realization steps**

  1. start by reducing the oatmeal powder in the blinder bowl.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/son-davoine.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/son-davoine.jpg>)
  2. place the rest of the ingredients now, and turn on the blinder. It will take time, do not hesitate to use a wooden spoon to mix the powder of oat bran that sticks to the edge of the blinder. 
  3. mix again. and enjoy this delicious smoothie the rather than possible. 



[ ![peach cobbler smoothie 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-cobbler-de-peches-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-cobbler-de-peches-2.jpg>)
