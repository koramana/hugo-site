---
title: khobz dar with nigelle (with technique of tangzhong)
date: '2016-01-10'
categories:
- Mina el forn
- bread, traditional bread, cake
tags:
- Boulange
- Bakery
- To taste
- Ramadan
- Algerian bread
- Bread
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/hobz-dar-aux-nigelle-avec-tehnique-du-tangzhong-1.jpg
---
[ ![khobz dar with nigelle \(with technique of tangzhong\) 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/hobz-dar-aux-nigelle-avec-tehnique-du-tangzhong-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/hobz-dar-aux-nigelle-avec-tehnique-du-tangzhong-1.jpg>)

##  khobz dar with nigelle (with technique of tangzhong) 

Hello everybody, 

Today, I share with you this nice homemade bread recipe khobz dar that you certainly will not miss, it's a Lunetoiles recipe, it uses the technique of tangzhong. 

Frankly this technique tempts me a lot, I think I'll get started because after seeing the [ Polish brioches ](<https://www.amourdecuisine.fr/article-brioches-polonaises.html>) , the [ japanese milk bread ](<https://www.amourdecuisine.fr/article-pain-au-lait-japonais-hokkaido.html>) and now this recipe, I think there is no longer any doubt about this technique, and here is what tells you **Lunetoiles** : 

Salam aleykum wa rahmatullah wa barakatuh, 

Since I realized that the tangzhong was something incredible that works, and I understood that it could be incorporated into any dough, I wanted to try to see what would be result with the homemade bread that we used to prepare our moms, and what he could bring more to this bread. It has been appreciated by all, incomparable soft, a crumb like cotton, it makes me want to do almost every day. 

The recipe in Arabic: 

[ ![khobz dar with nigelle \(with tangzhong technique\) 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/hobz-dar-aux-nigelle-avec-tehnique-du-tangzhong-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/hobz-dar-aux-nigelle-avec-tehnique-du-tangzhong-4.jpg>)   


**khobz dar with nigelle (with technique of tangzhong)**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/hobz-dar-aux-nigelle-avec-tehnique-du-tangzhong-2.jpg)

Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * 300 g of extra fine (or fine) semolina 
  * 200 g of organic flour 
  * 50 g butter at room temperature 
  * 1 tbsp. salt 
  * 1 tbsp. tablespoons sugar 
  * 3 tbsp. black seed 
  * lukewarm water 
  * 1 egg at room temperature 
  * 1/2 cube of fresh baker's yeast (20g) or 1 sachet of briochin alsa yeast 

For the top of the bread: 
  * egg yolk to brown 
  * a little water 
  * seeds of Nigel to sprinkle 

Tangzhong: 
  * 50 gr of flour 
  * 125 ml of milk 
  * 125 ml of water 



**Realization steps** Start with the tangzhong: 

  1. In a saucepan, mix with the whisk (manual), the flour with the water and the milk. Heat to a temperature not exceeding 65 ° while mixing. The mixture will thicken and look like a porridge. Once the temperature is reached ... remove from the heat. Book until it warms up. 

For the bread dough: 
  1. To raise the yeast, mix in a small bowl the yeast with the sugar and a little warm water and let rise 15 min. 
  2. Put the flour, the semolina, the salt, the nigella seeds, the egg, then the yeast in the tank of your robot, add warm water until it forms a paste and knead at medium speed. 
  3. Add the tangzhong and if your dough is too soft, add semolina (I do not give precise weight, I made the eye) 
  4. Then knead for 10 minutes at medium speed. 
  5. The longer it is kneaded, the result will be better. 
  6. And incorporate the butter cut into small pieces and knead again until complete penetration of the butter into the dough. 
  7. After a long kneading, cover with a cloth and let rise for 1 hour in a warm, draft-free place. 
  8. Once raised, flour well your hands and degas the dough and divide in a baking dish covered with baking paper. 
  9. And let it rise for another hour in a warm room away from drafts. I place it near a radiator. 
  10. Preheat the oven to 170 ° C 
  11. Brush with diluted egg yolk with a little water 
  12. Sprinkle black seed and bake in the hot oven in the middle of the oven. 
  13. And let it cook for about 40 minutes, you can increase the temperature of the oven after 30 minutes of cooking up to 180 ° C for a nice golden crust.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/khobz-dar-aux-nigelle-avec-tehnique-du-tangzhong.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/khobz-dar-aux-nigelle-avec-tehnique-du-tangzhong.jpg>)



Note Watch how much cooking depends on your oven and the size of your bread, if you have done it rather thin or thick.   
For me 40 minutes was enough starting with cooking at 170 ° C the first thirty minutes and I was growing just at the end, because it was not very golden. ![khobz dar with nigelle \(with tangzhong technique\) 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/hobz-dar-aux-nigelle-avec-tehnique-du-tangzhong-5.jpg)
