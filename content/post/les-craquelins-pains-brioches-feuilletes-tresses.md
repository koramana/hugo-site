---
title: Crackers, braided brioche rolls braided
date: '2018-01-26'
categories:
- Buns and pastries
- Mina el forn
tags:
- Puff pastry
- Boulange
- Breakfast
- To taste
- Braided bun

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-011.jpg
---
![Crackers, braided brioche rolls braided](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-011.jpg)

##  Crackers, braided brioche rolls braided 

Hello everybody, 

But when I went to Algeria, with my big family, I found that it was the ideal moment to do that, I had a little heat, because I had to handle the dough quickly while the butter is always cold. In the end, and for the first time, I was very satisfied with my result, they were so good that I did not even have the chance to take pictures. what I present to you is the photos of the crackers right out of the oven, hihihihih. Ah, the greedy ones!   


**The crackers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-013_thumb_1.jpg)

**Ingredients** For 8 braided brioche brioche crackers: o) 

  * 450 g flour 
  * 1 teaspoon dehydrated yeast 
  * 1 cc of salt 
  * 60 g of sugar 
  * 260 ml of milk 
  * 15 g of butter 
  * 150 g butter at room temperature for lamination 
  * 50 g of sugar 
  * water 



**Realization steps**

  1.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-013_thumb_11.jpg) ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-008_thumb.jpg)
  2.   
I copy you paste of the recipe: 
  3. In the bowl of your robot put the flour, salt, yeast and sugar. Mix. Put the milk and butter in a bowl, heat for 1 minute in the microwave and pour on the flour. Knead until smooth and even. Let rise 1 hour in a temperate place (you can preheat your oven to 80 ° C 10 minutes earlier and leave the bowl of your robot in with a clean cloth on top (oven off)). When the dough doubled in volume, put it in the refrigerator 30 minutes to firm it, so it will be easier to work. 
  4. Take the dough out of the fridge, spread it in a rectangle, put the butter in the middle of this rectangle   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-003_thumb.jpg)
  5. and fold both sides on it. Put in the fridge 10 minutes.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-004_thumb.jpg)
  6. Repeat the process by turning ¼ turn, put in the fridge for 10 minutes. 
  7. Repeat for the last time (3 rounds in all) and leave in the fridge for 10 minutes. 
  8. Remove the dough from the fridge, spread it in a rectangle and cut strips about 15 mm wide. Braid these strips and drop them on a silpat. Let stand 1 hour.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-009.jpg)
  9. Preheat the oven to 220 ° C. 
  10. Bake the crackers, lower the T ° immediately to 180 ° C and cook for 15 to 20 minutes under supervision. You will have to make 2 batches. While the crackers are cooking, add a little water to the sugar in a bowl to liquefy it, prepare your brush.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-014.jpg)
  11. As soon as you get out of the oven crackers with sugar, you have to be fast because it creates a slight crispy caramelization of the most appreciable ... Let them cool on a rack.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-013_1.jpg)



![the crackers 6](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/brrrrrrrrrr-013_1.jpg)
