---
title: kesra rekhsis / cake with Algerian oil
date: '2013-06-24'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine

---
I like the cake, and I enjoy since I am in Algeria, to make these delights, because in England I have only my crepiere, and it does not give the same cake. rakhssiss is a super delicious, mellow, crunchy galette at the same time, and frankly I love to try the kouky method, it had to give measures, and ingredients, that I really wanted to test it, and I swear, 6 pancakes, did not last long & nbsp; Preparation time: 30 minutes Cooking time: 15 minutes Ingredients for 6 cookies (copy / paste at kouky) & nbsp; 4 bowls & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.55  (  5  ratings)  0 

_ I like the cake, and I enjoy since I am in Algeria, to make these delights, because in England I have only my crepiere, and it does not give the same cake.  _

**Preparation time : 30 min  ** **Cooking time : 15 min  **

_ Ingredients for 6 cakes (copy / paste at kouky)  _

_ 4 bowls of fine semolina (1bol = 400ml)  _

_ 1 glass of oil or melted smen (1 glass = 250ml) next time I decrease the fat 200 ml will be enough  _

_ 3 tablespoons of powdered milk  _

_ 1 tablespoon of instant dry yeast  _

_ 1 tablespoon of sugar  _

_ 1 tablespoon of salt  _

_ 3 tablespoons of sesame seeds, not put  _

_ 1cas orange blossom water or rose (optional) not put  _

_ 2 bowls of lukewarm water (+ or- depending on the quality of the semolina)  _

_ \- Put the sifted semolina in a gas3a or a large dish, add all the dry ingredients and mix  _

_ \- Add the oil or melted smen and work with the palms of the hands so that the semolina absorbs the fat  _

_ \- Wet gradually with lukewarm water while working with fingertips to obtain a firm paste  _

_ \- Gather the dough without too much kneading and let it rest for a moment covered so that the semolina absorbs the water and becomes more manageable  _

_ \- Resume the dough, work it so that it becomes smooth and homogeneous, but not too much so that it remains melting  _

_ \- Shape 6 balls, place them on a tray spacing them a little and cover with a clean cloth  _

_ \- Light the fire and heat the tagine  _

_ \- Roll the first ball to make a thin slab, pierce it and place it on a clean cloth. Do the same for other patties. They rest the time that the tagine heats.  _

_ \- Cook them on a hot tagine on both sides  _

_ \- Let cool a little and cut and regale you  _

_ Thank you for your feedback.  _
