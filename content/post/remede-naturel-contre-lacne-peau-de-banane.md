---
title: 'natural remedy for acne: banana peel'
date: '2013-02-13'
categories:
- diverse cuisine
- Tunisian cuisine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/acne-bouton-naturel1.jpg
---
![treatment against acne pimples](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/acne-bouton-naturel1.jpg)

source of the photo: oh my dolls. 

Hello everybody, 

a natural remedy for acne, that's what my nephew asked me last time, I searched on google, and this was the first article I came across. 

I gave him the recipe he applied, and here he reminds me, to tell me, that the recipe works for him ... 

So, I find myself sharing with you this recipe, sorry I have no photos, because my nephew did not think to make some (even I did not ask him) 

but, I pass you the photos and the recipe of the blog that I took in reference, for this recipe: 

![remedy against acne pimples](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/banana-peels-acne-treatment1.png)

bananas help a lot in the fight against acne because of its capacity for potassium and antioxidants found in the skin. 

They have the ability to significantly reduce skin irritation, swelling and redness, as well as reduce the visibility of scars. 

how it works? 

  1. Eat a ripe banana, 
  2. Cut the skin into small pieces (about 2 cm). 
  3. Rub the inside of the skin gently over the irritated area. Rub for about 5 minutes or until the inside of the skin turns brown. 
  4. let the banana dry on the irritated area, or with acne to make its magic. 
  5. Repeat at least 3 times a day. 



Apply daily to visibly reduce your acne. You will be surprised by the quick and glorious results and the clear skin. 

In fact, you will see the first results overnight. 

Bonne chance 
