---
title: ktayefs or angel hair with almonds
date: '2015-06-24'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian cakes
- Algeria
- Algerian patisserie
- delicacies
- Aid
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/ktayefs-aux-amandes-2.jpg
---
[ ![ktayefs with almonds 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/ktayefs-aux-amandes-2.jpg) ](<https://www.amourdecuisine.fr/article-ktayefs-ou-cheveux-dange-aux-amandes.html/ktayefs-aux-amandes-2>)

##  ktayefs or angel hair with almonds 

Hello everybody, 

The **ktayefs** , this sublime Algerian pastry with honey, competing in the first degree of the famous recipe: **the Baklawa** Because these two honey pastries are similar in taste and preparation, but the ktayefs are by far easier to prepare than the baklawa, and also the slightest problem is to be able to find the dough at ktayefs .... On the Algerian, one finds easily, in Constantine too .... But there are a few places where it is almost impossible to find this paste so soft to touch, like angel hair. I remember at the time where I lived in Sétif, we could not find the ktaifs, besides my mother was filling up on Algiers or Constantine ... Maybe now it is easier to acquire this dough here and there. 

Personally, here in England, I find the pate has ktaifs in the trade, but because it is a preparation of the countries of the Middle East, I find that the dough is a little salty .... So, I always do my shopping in Algeria, and I put in the freezer, to prepare this delight in the month of Ramadan. 

This time, it's a recipe that comes from the heart of Algiers, (the North West of Algiers more precisely) of **Kolea** , a recipe from a friend and reader of the blog, whom I thank in passing: Mrs. **Amel Benmouffok Haddag** , she gave this nice look to qtayefs with almonds (ktaifs or angels' hair with almonds), I think that the mini version of this pastry gives a lot of charm to this little delicacy well honeyed and wonderfully scented with orange blossom . My dear **Amel** insists that I mention that the recipe was sent to him by **his mom** May God protect her and give her good health ... A big thank you to all the mothers who are doing their best to transmit our wonderful culinary heritage that we are sharing with you. 

**ktayefs or angel hair with almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/ktayefs-ou-cheveux-dange-aux-amandes.jpg)

portions:  8  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients**

  * 1 packet of pasta with ktayefs (400 gr) 
  * 100 gr of butter + smen 

prank call: 
  * 250 gr of almonds 
  * ¼ cup of cinnamon powder 
  * orange blossom water to pick up 
  * and a few tablespoons of sugar (not a lot you will sprinkle with syrup thereafter) 
  * 50 gr of butter in pieces for cooking 
  * Syrup: 
  * 500 g of sugar 
  * ½ l of water, 
  * orange blossom water according to taste 
  * 2 to 3 tablespoons of honey 



**Realization steps**

  1. air the ktayefs as for the rechta, then add the mixture of butter and melted smen 
  2. Rub gently between your hands to absorb the fat in the ktayefs. 
  3. Butter a mold about 30 cm in diameter, or muffin molds a little wide. 
  4. put a first layer of ktayefs, well compact especially. 
  5. cover this first layer with the almond stuffing. 
  6. Then cover the almond stuffing with another layer of ktayefs 
  7. Butter the top with pieces of butter and bake in a preheated oven at 180 degrees C for almost 25 min, if it's a large pan, and less, if it's in the muffin pan 
  8. Meanwhile prepare the syrup with sugar and water over low heat, after boiling it should be boiled for about 15 to 20 minutes. 
  9. At the end add a little orange blossom water and a few spoons of honey 
  10. Sprinkle the potatoes a little warm, with the syrup a bit hot or even lukewarm to obtain soft and crispy ktayefs on the outside. 
  11. Or sprinkle them with cold syrup and hot katayefs for crispy ktayefs. 



[ ![qtayefs with almonds 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/qtayefs-aux-amandes-1.jpg) ](<https://www.amourdecuisine.fr/article-ktayefs-ou-cheveux-dange-aux-amandes.html/qtayefs-aux-amandes-1>)
