---
title: Milk rice
date: '2013-01-07'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/riz-au-lait.jpg
---
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/riz-au-lait.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/riz-au-lait.jpg>)

Hello everybody, 

who does not like rice pudding, or who has not eaten this delight at least once in his life, at least not me, because I do not tell you sometimes when I'm short of ideas for a drip, or sometimes a dessert after a very light meal, well that's the recipe that is needed at home, hihihihih 

in any case, children love him very much. 

simple recipe is easy, you can vary the flavors to infinity. 

**Milk rice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/riz-au-lait-1-300x225.jpg)

portions:  2  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * ingredients: 
  * 1/2 liter of milk 
  * 1 glass of round grain rice 
  * 4 tablespoons sugar 
  * 1 cinnamon stick 
  * the lemon peel 
  * some grains of anise. 
  * method: 



**Realization steps**

  1. pour the milk, add the cinnamon stick, the sugar, the lemon zest and the anise 
  2. Leave until this mixture reaches the boiling temperature 
  3. pass the mixture in Chinese and add the rice 
  4. Stir constantly with a wooden spoon until creamy. 
  5. Remove from heat and let cool 
  6. present by decorating with cinnamon powder 



[ ![rice pudding 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/riz-au-lait-11.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/riz-au-lait-11.jpg>)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
