---
title: plum and almond tart / plum almond
date: '2013-09-06'
categories:
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tarte-aux-prunes-et-amandes1.jpg
---
#  ![tarte aux prunes-and-amandes.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tarte-aux-prunes-et-amandes1.jpg)

Hello everybody, 

As we are still in the plum season, we must take full advantage of it, and with beautiful plums, delicious and acidic at the same time, Lunetoiles has prepared us this beautiful plum almond tart ...   


**plum and almond tart / plum almond**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tarte-amandine-aux-prunes1.jpg)

Recipe type:  pie dessert  portions:  8  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** For a pie circle 28cm in diameter For the shortbread dough: 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

Almond cream: 
  * 120 g of almond powder 
  * 2 eggs 
  * 100g of sugar 
  * 200 ml whole liquid cream 
  * fresh plums 
  * 3 tbsp. plum jam 

Decoration: 
  * Icing sugar 



**Realization steps** The shortbread dough: 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a paste is removed from the sides of the bowl and is non-sticky. 
  4. Let it rest. 
  5. Spread the dough with the roll between 2 sheets of baking paper. 
  6. Darken a pie circle 28 cm in diameter. 
  7. Stir the batter stock with a fork and chill for the time to prepare the filling. 

Almond cream: 
  1. In the bowl of the beater this time equipped with the whisk, beat the eggs with the sugar. 
  2. When the mixture is frothy add the almond powder and the cream. Beat again. 
  3. Garnish the plum jam tart. 
  4. Arrange the cut plums and cover with almond cream. 
  5. Put in the oven for 30 minutes at 180 ° C (th.6). 
  6. Take the pie out of the oven. 
  7. Let the pie cool on a rack and take a lid smaller in diameter than the pie, put it on top, and sprinkle just the edge with icing sugar, 
  8. Remove the lid and present the pie on a serving platter. 



![amandine-pie-with-plum-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tarte-amandine-aux-prunes-copie-11.jpg)
