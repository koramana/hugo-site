---
title: the braid الظفيرة حلوى جزائرية Algerian cakes
date: '2012-06-01'
categories:
- recette a la viande rouge ( halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/la-tresse-2_111.jpg
---
& Nbsp; today I received the visit of some friends, and when I was about to serve the last pieces of this delicious cake, I remembered that I did not make a final photo to present it on the blog, so with my camera and in the kitchen cluttered and a little dark, well I do not know why my little braids have changed color ... it's beautiful purple sweet and clear, an Algerian cake recipe easy and especially too much good. but before I give you this recipe, I share with you these & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.78  (  8  ratings)  0 

![Algerian cake braid](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/la-tresse-2_111.jpg)

today I received the visit of some friends, and when I was about to serve the last pieces of this delicious cake, I remembered that I did not make a final photo to present it on the blog, so with my camera and in the kitchen cluttered and a little dark, well I do not know why my little braids have changed color ... it's beautiful purple soft and clear, 

an Algerian cake recipe very easy and above all too good. 

but before I give you this recipe, I share with you these links: 

[ ![Arayeche, pistachio larch with Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/arayeche-larayeche-aux-pistaches-gateau-algerien.160x12011.jpg) ](<https://www.amourdecuisine.fr/article-arayeche-larayeche-aux-pistaches-gateau-algerien-87986224.html> "Arayeche, larayeche with pistachios, Algerian cake of love of cuisine at soulef")

[ Arayeche, pistachio larch with Algerian cake ](<https://www.amourdecuisine.fr/article-arayeche-larayeche-aux-pistaches-gateau-algerien-87986224.html> "Arayeche, larayeche with pistachios, Algerian cake of love of cuisine at soulef")

[ ![Maamoul, cake stuffed with dates Algerian cakes](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/maamoul-gateau-farci-aux-dattes-gateaux-algeriens-155.160x1201.jpg) ](<https://www.amourdecuisine.fr/article-maamoul-gateau-farci-aux-dattes-64583472.html> "Maamoul, cake stuffed with dates Algerian cakes of love of cooking at soulef") [ Maamoul, cake stuffed with dates Algerian cakes ](<https://www.amourdecuisine.fr/article-maamoul-gateau-farci-aux-dattes-64583472.html> "Maamoul, cake stuffed with dates Algerian cakes of love of cooking at soulef") [ ![Cakes with thousand-leaf Algerian cakes](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateaux-a-etage-mille-feuille-gateaux-algeriens-154.160x1201.jpg) ](<https://www.amourdecuisine.fr/article-gateaux-a-etage-mille-feuille-64491761.html> "Cakes with thousand-leaf Algerian cakes of love of cuisine at soulef") [ Cakes with thousand-leaf Algerian cakes ](<https://www.amourdecuisine.fr/article-gateaux-a-etage-mille-feuille-64491761.html> "Cakes with thousand-leaf Algerian cakes of love of cuisine at soulef") [ ![Arayeches with honey, Stars with almonds and honey Algerian cakes](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/arayeches-au-miel-etoiles-aux-amandes-et-miel-gateaux-algeriens-151.160x12011.jpg) ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-60064269.html> "Arayeches with honey, Stars with almonds and honey Algerian cakes of love of cuisine at soulef") [ Arayeches with honey, Stars with almonds and honey Algerian cakes ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-60064269.html> "Arayeches with honey, Stars with almonds and honey Algerian cakes of love of cuisine at soulef") [ ![Two-tone gazelle horn pinches honey Algerian cakes](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/corne-de-gazelle-bicolore-pincee-au-miel-gateaux-algeriens.160x12011.jpg) Two-tone gazelle horn pinches honey Algerian cakes ](<https://www.amourdecuisine.fr/article-corne-de-gazelle-bicolore-pincee-au-miel-60151822.html> "Two-tone gazelle horn pinches honey Algerian cuisine love cakes at soulef") and for more: 

##  [ INDEX OF ALGERIAN, ALGERIAN, FESTIVAL AND WEDDING CAKES ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html> "Index of Algerian cakes, Algerians, parties and weddings")

for the recipe I prefer to work with the pasta that I already use for two-tone cornets and larayeches with honey, because I find the final result beautiful, this dough does not harden, and even if the short work you find it a little dry, you can add the water and repaint it, the dough remains a very tender delight. 

dough: 

250 gr of flour 

75 gr softened margarine 

1 pinch of salt 

1 C. white dye coffee 

1 C. purple dye coffee 

flower water 

water 

the joke: 

250 gr of almonds 

125 gr of sugar 

1 C. a vanilla coffee 

1 egg 

garnish: 

honey 

Mix well with your fingertips 

divide the dough into two parts 

for the one you are going to color in white add the dye, then pick up the dough with flower water, add a little water to pick up, continue to knead a few seconds. 

for the other color, take the other half of the shortbread, add the dyes to the orange water until the desired color is obtained, and start to collect the dough with this water, add the water if you need, and knead a few seconds. 

cover your dough and let it sit on the side. 

for stuffing mix almonds and dry ingredients, collect with egg and set aside. 

Spread both pasta, this time I did it with a baking roll a little tiring, but faster than the machine dough especially when I use it alone I do catas ... .hihihihi 

cut the two pasta into rectangles each 10 cm wide, put one in front of the other, and superimpose the ends to have a rectangle with two colors of 19cm wide still pass the roll so that the two pasta stick well 

place the stuffing pudding along the length of the dough, it should be approximately 1.5 to 2 cm high, and almost 3 cm wide. 

then with a knife form strips of 1 cm on both sides, then make the braids to cover all the stuffing. 

then cut your cakes by almost 4 cm long. 

let rest overnight and the next day bake in a preheated oven at 150 degrees, bake between 10 and 15 minutes 

at the end of the oven, dip in hot honey and decorate according to your taste 

Thank you for your feedback 

and for all your visits that always make me happy. 

and thanks to all those who try my recipes, thank you to trust the recipe that I post to you. 

merci 
