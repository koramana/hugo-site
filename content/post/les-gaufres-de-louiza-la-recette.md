---
title: the waffles of louiza, the recipe.
date: '2009-09-29'
categories:
- panna cotta, flan, and yoghurt

---
and as promised here is the recipe, and I thank louiza for this sharing 

  * 300g of flour 
  * 3 big eggs 
  * 1 salt salt 
  * 75g of butter 
  * 1/2 sachet of yeast 
  * 150ml milk 
  * 150ml water or sparkling water 
  * 60g of sugar 
  * 2 vanilla sachets 
  * 2c has flower water 



I do not put yeast in my waffles, because I think it gives an acid taste and an unpleasant feeling to taste. then I put the egg whites beaten in the snow instead.   
I mix my dough with the egg yolks and last I add the whites in firm snow   
and I mix gently, and I cook in a waffle iron. 
