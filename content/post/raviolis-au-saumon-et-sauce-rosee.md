---
title: salmon ravioli and rosé sauce
date: '2016-03-03'
categories:
- diverse cuisine
- Healthy cuisine
tags:
- Algeria
- dishes
- Healthy cuisine
- Vegetarian cuisine
- Easy cooking
- Full Dish
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/raviolis-au-saumon-et-sauce-ros%C3%A9e-3.jpg
---
[ ![salmon ravioli with rosé sauce 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/raviolis-au-saumon-et-sauce-ros%C3%A9e-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/raviolis-au-saumon-et-sauce-ros%C3%A9e-1.jpg>)

##  salmon ravioli and rosé sauce 

Hello everybody, 

A recipe for fish, that's not what I missed in recent days, I made a lot of fish recipes, I could not decide with what I was going to participate, but in any case all the recipes were as good as each other. 

In the end it was the ravioli with salmon and pink sauce that pleased my children that I chose for this round, I hope you will try the recipe and give me your opinion!   


**salmon ravioli and rosé sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/raviolis-au-saumon-et-sauce-ros%C3%A9e-2.jpg)

portions:  6  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * [ ravioli pasta ](<https://www.amourdecuisine.fr/article-raviolis-fait-maison-pate-a-ravioles.html>)

salmon stuffing: 
  * 2 salmon fillets 
  * 1 cheese rolled Boursein style, or ricotta (my cheese is flavored with garlic and herbs) 
  * ½ cup of rose berry crushed with mortar 

dew sauce: 
  * 1 onion 
  * 1 green pepper 
  * 2 cloves garlic 
  * 1 tablespoon of extra virgin olive oil 
  * 3 crushed fresh tomatoes 
  * 500 ml of vegetable broth (otherwise a cubic broth diluted in 500 ml of hot water) 
  * 200 ml of liquid cream 
  * 1 C. pink berry coffee. 



**Realization steps** prepare the salmon stuffing: 

  1. salt and pepper the salmon, and cook in a little oil on both sides. 
  2. place the pieces of salmon on paper towels and let cool. 
  3. crush the salmon with a fork, and add the cheese in quantity until you have the consistency of your choice, and the taste that suits you the most. 
  4. add the crushed pink berry, and season your stuffing to taste   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/farce-au-saumon.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/farce-au-saumon.jpg>)
  5. leave the stuffing aside, and prepare the [ ravioli pasta ](<https://www.amourdecuisine.fr/article-raviolis-fait-maison-pate-a-ravioles.html>) as shown on the link 
  6. spread the pasta finely and fill the fingerprints with a teaspoon of the salmon stuffing   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/farcir-les-raviolis-au-saumon.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/farcir-les-raviolis-au-saumon.jpg>)
  7. add the egg white all around the stuffing, cover with another layer of dough and weld well before forming your ravioli with a special mold, or just the roulette. 
  8. let it dry a little while preparing the sauce 

prepare the rosé sauce: 
  1. fry chopped onion, crushed garlic and green pepper in a little olive oil 
  2. When everything is well cooked add the grated tomatoes. 
  3. let it come back a little then add the vegetable broth and the 5 minutes, the vegetable broth and the pink berry powder 
  4. when the sauce has reduced (partially) reduce the well-blender 
  5. add the cream, let it come back a little, season to taste, and remove from the heat. 
  6. Cook pasta for 3 minutes in salted water. immediately plog them in cold water to stop cooking, 
  7. drain and place in the sauce. put everything on medium heat for a few minutes and serve immediately. 



[ ![salmon ravioli and rosé sauce 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/raviolis-au-saumon-et-sauce-ros%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/raviolis-au-saumon-et-sauce-ros%C3%A9e-1.jpg>)

Previous rounds: 

and here is the list of participants: 

*****La liste des participantes****** 
