---
title: the number of points you need
date: '2008-04-30'
categories:
- Buns and pastries
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- cakes and cakes
- Mina el forn
- sweet recipes

---
Following a few requests I chose to summarize here the "great principles of WW". 

First of all, we must find the number of points we are allowed to eat per day. 

**CALCULATION OF POINTS:** add the numbers to get the daily quota. 

1) You are: 

1 woman: 7 

1 man: 15   
2) How old are you? 

18/20 years: 5 

21/35 years: 4 

36/50 years: 3 

51/65 years: 2 

over 65: 1   
3) How much do you weigh? Note as a result the figure of ten corresponding to your weight   
Ex: from 70 to 79 kg = 7 points   
4) How much do you measure? : 

less than 1.60 m: 1 

1.60 m and over: 2   
5) Daily activity: 

Work exclusively seated: 0 

Mostly seated but sometimes standing with displacements: 2 

Essentially standing: 4 

Very physical work 6   
6) You want? 

Lose weight: 0 

Stabilize your weight 4 

THEN:   
Each food represents a number of points, otherwise for all dishes prepared here is the formula to apply to find the number of points: Number of calories / 60 + fat / 9 

It is possible to save up to 4 points per day to use them during an outing or for the weekend but they can not refer from one week to another.   
Consuming from time to time which is nice is important for dieting. But foods rich in sugar (pastry, confectionery ...) and alcohol, are limited to 14 points per week, or 2 per day. 

You can also "earn" BONUS POINTS through physical activity. Either you eat them more, or you notice them to activate even more weight loss.   
Here are some examples :   
Bike ride, gardening = 20 minutes: 1 point / 30 minutes: 2 points   
Jogging, brisk walking or swimming = 20 minutes: 2 points / 30 minutes: 4 points 

VOILA for the explanation of the number of points. still apply 6 basic principles to balance your days:   
\- drink 1.5 liters of water a day (easy to do like me, I drink a glass of 200 ml in the morning, 200 after breakfast, 200 ml before breakfast, 200 ml after, 200 ml to drip, 200 ml before the dinner, 200 ml after the dinner, and I have 1 liter.4 a day)   
\- move !!!!   
\- eat at least 3 servings of vegetables, 2 servings of fruit a day   
\- eat 2 servings of calcium (dairy products, cheeses) a day.   
\- eat 2 points of fat (for skin and transit) per day.   
\- vary your menus !! 

I hope it helps you a little, do not hesitate to ask your questions. 
