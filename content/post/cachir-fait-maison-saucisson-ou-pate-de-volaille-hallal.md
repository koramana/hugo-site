---
title: Homemade Sage, Sausage or Hallal Poultry Pie
date: '2012-12-02'
categories:
- Algerian cuisine
- houriyat el matbakh- fatafeat tv
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/pate-de-volailles-1_thumb1.jpg
---
##  Homemade Sage, Sausage or Hallal Poultry Pie 

Hello everybody, 

Homemade cachir is a recipe that we all try to make, but we do not get it easily, we always have something missing. 

today, one of my readers "Malika" and who has practically realized all the recipes of my blog, hihihihi ... to like to share with us the recipe of his home made cachir (poultry paste or halal poultry sausages) 

a little research on wikipedia gives me this little introduction: 

The **cachir** is a type of sausage of Algerian origin that we meet especially in Algeria and also by influence in the border areas of this country. 

This traditional sausage has the appearance and many taste similarities with a European sausage is made from beef or chicken, but never pork; it is often used to garnish sandwiches, or to stuff slippers.   


**Homemade Sage, Sausage or Hallal Poultry Pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/pate-de-volailles-1_thumb1.jpg)

**Ingredients**

  * Ingredients: 
  * 5oeuf 
  * 5 tablespoons fine semolina 
  * 2 portion of kiri cow cheese 
  * 2 cloves garlic 
  * 1 knorr cube (or any other brand) with poultry 
  * Half a glass of oil 
  * 200 g of raw chicken breast. 
  * A parsley stalk 
  * A pinch of paprika 
  * Cumin, black pepper 
  * ½ teaspoon of salt 
  * a handful of pitted green olives.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/moule-a-cachir_thumb1.jpg)



**Realization steps**

  1. put all the ingredients (without the olives) in the blender. 
  2. puree. 
  3. then add a handful of green olives or black slices, 
  4. put in a pot to seal that closes, or fail to put in a bag of spaghetti and close with a string. 
  5. put in the bottom of a couscoussier or a tall container, and let simmer for 1 hour, 
  6. check the cooking with the blade of a knife, it must come out dry, 
  7. put in the fridge for a few hours or overnight. 
  8. Slice and taste .... 



![cachir-house-to-chicken-2_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cachir-maison-au-poulet-2_thumb1.jpg)
