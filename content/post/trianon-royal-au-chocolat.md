---
title: Trianon - royal chocolate
date: '2012-01-30'
categories:
- boissons jus et cocktail sans alcool

---
hello everyone, the trianon, also known as royal chocolate, is a very refined entremet based on a biscuit with almonds covered with a beautiful crisp praline layer, all under a very smooth unctuous layer with chocolate. and on the occasion of the birthday of her brother, lunetoiles prepared this Trianon, and she liked to share her recipe with you the recipe it, is taken from the delicious blog of Isabelle or puceblue here I do not hide you the Truth, I would love to eat a piece, but it's a shame I have only the photos, so without delay I share & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

the trianon, also known as royal chocolate, is a very refined dessert based on an almond biscuit covered with a beautiful crisp praline layer, all under a very smooth unctuous chocolate layer. and  the occasion of her brother's birthday, lunetoiles prepared this Trianon, and she enjoyed sharing her recipe with you 

I do not hide the truth, I would love to eat a piece, but it's a shame I have only the photos, so without delay I share with you the recipe for lunetoiles. 

First of all, make homemade praline the day before: 

  * 500 g of almonds 
  * 500 g of hazelnuts 
  * 500 g sugar  _(I use red sugar, but you can also take a sugar semolina)_
  * 12 cl of water 



In a large skillet, roast the nuts for about 10 minutes, stirring occasionally. Book. 

In another pan (large enough), pour in the sugar and wet with water. Cook without touching until the mixture is liquefied and lightly colored. 

Add the roasted dried fruit mix and mix quickly: the caramel whitens and recrystallizes; it is said that it "sand" or that it "mass". 

Cook over medium heat and caramelize for a few more minutes: the darker the caramel, the more the praline will be strong in taste. 

Once the desired color has been obtained, remove from heat and allow to cool completely. 

Place the mixture in the mixer bowl. 

** for grains, mix quickly 

** a little more, and we get a powder 

** mix even more, and you get a dough 

Store the praline grains and powder in an airtight container upside down at room temperature. 

The dough itself is kept in the refrigerator. 

Dacquois with almonds  : Ingredients: 

  * 4 egg whites 
  * 100 g of sugar 
  * 100 g ground almonds 
  * 15 g of flour 



Prepare all the ingredients. 

Beat the egg whites until stiff and meringue with the caster sugar. 

Mix with almond powder and flour, using a spatula. 

Pour the mixture into a greased circle and smooth to the desired height (about 1 cm). 

Bake in a mild oven (150 °, thermostat 3/4) for about 30 minutes. 

Remove from the oven and let cool. 

Place the sapwood bottom inside a circle. 

Crunchy praline chocolate  : Ingredients: 

  * 195 g homemade praline paste (prepared the day before) 
  * 30 g of  pralinoise  (chocolate bar with fondant praline from Cote d'Or brand or Poulain 1848) 
  * 105 g of  feuilletine  (Gavotte lace crêpe crumbled roughly) 



Put the chocolate in a small bowl and melt in the microwave 2 x 30 seconds. 

Relax the praline paste with the robot until you get a very smooth machine. 

Add the melted chocolate and continue to whisk for a few seconds. 

Gently fold the foil and mix by hand with a spoon to coat well. 

Pour on the dacquois and equalize using the back of a tablespoon. 

Chocolate mousse  : Ingredients: 

  * 225  gr  cover chocolate 
  * 1/2 liter of whole cream 



Melt the chocolate in a bain-marie. 

Let the chocolate cool down. 

Whip the whipped cream. 

Then mix the mounted cream with the chocolate to obtain a homogeneous mixture. 

Put the chocolate mousse obtained on the praline chocolate crunch. 

Smooth and froth in refrigerator. 

Decenter the entremet once the mousse "well taken". 

Sprinkle with cocoa before serving. 

Decorate at your leisure. 

thank you very much for this delicious recipe, happy birthday to your brother 

et merci a vos mes lecteurs et lectrices pour tout vos messages, emails et commentaires, et merci a tous ceux qui continuent a s’abonnez a ma newsletter, gros bisous 
