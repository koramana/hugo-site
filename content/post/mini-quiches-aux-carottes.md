---
title: mini quiches with carrots
date: '2012-04-17'
categories:
- cakes and cakes

---
hello everyone, how are you? I hope so, today I share with a super delicious carrot quiche recipe, I recommend this recipe because it's really a real pleasure, I swear to you that my husband who does not like quiches at all really love, and said to me: you did only 4, well I did not know he was going to eat it, I told myself at least I eat 2, and the children one each, better than to do 8, and eat all, all alone. it's a recipe inspired by the talented Houriyat & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.65  (  2  ratings)  0 

Hello everybody, 

how are you? I hope so, today I share with a super delicious recipe of [ quiche ](<https://www.amourdecuisine.fr/categorie-12354939.html>) with carrots, I recommend this recipe because it is really a real pleasure, I swear to you that my husband who does not like at all quiches really like, and said to me: you did only 4, Well I did not know he was going to eat it, I told myself at least I eat 2, and the children one each, better than 8, and eat all, all alone. 

it's a recipe inspired by the talented [ Houriyat el matbakh ](<https://www.amourdecuisine.fr/categorie-11131465.html>) , 

ingredients for 4 mini quiches (in tartlets) 

for the pasta: 

  * 250 grams of flour 
  * 120 grams of butter in pieces 
  * 1 egg yolk 
  * 1/2 teaspoons of sugar 
  * just a little sprig of parsley 
  * 1/2 teaspoon of kernel powder. 
  * 1/2 teaspoon of salt 
  * 50 ml of cold water (+ or - as needed) 



for the stuffing: 

  * 1 carrot of medium size 
  * 1 small onion 
  * 1 coffee powder of fennel 
  * 1 clove of garlic 
  * 3 tablespoons fresh cream 
  * 1 tablespoon of milk 
  * 1 egg 
  * salt and black pepper 
  * a little gruyere 
  * 1 tablespoon of olive oil 



method of preparation: 

  1. in the bowl of a blender, put the flour and butter in pieces, mix 
  2. add the egg yolk, sugar, salt, and the chopped parsley, the fennel powder mix again 
  3. add the water in small quantities and mix until the dough forms a ball (go gently with the water) 
  4. cover the dough ball with cling film, and leave in the fridge for 30 minutes 
  5. cut the finely chopped onions into thin slices, sauté the garlic 
  6. brown in olive oil until translucent 
  7. add the grated carrots, and stir to avoid burning 
  8. season with just a little salt and black pepper 
  9. remove from the fire, and add the fennel powder. 
  10. in a salad bowl, beat the egg with a fork, with just a little salt and black pepper, add the fresh cream and the milk, whip a little. 
  11. add the cooled carrot mixture 
  12. add the grated cheese 
  13. take the dough out of the fridge 
  14. cut the dough into 4 balls, and spread in small circles then fill the tartelletes molds, fill the bottom with white bean pies and cook for 15 minutes in a preheated oven 
  15. remove the white beans, and cook well. 
  16. remove from the oven, add the mixture to it, and cook again until the mixture freezes. 
  17. at the end of the oven, garnish with gruyere grated 



Thank you for your visits and your comments 

bonne journee 
