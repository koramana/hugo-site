---
title: Squishy Yogurt Cake
date: '2010-02-15'
categories:
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/02/sponge-cake_thumb_11.jpg
---
& Nbsp; Celeste is a very good friend, and faithful reader, who come all the time on my blog, she lives very far away, she is in Peru, it is very beautiful home now, I think I will go to her for this holiday , hihihihi yesterday she sent me this yogurt sponge cake recipe and I like to share it with you Ingredients: 250g butter 250g all-purpose flour 1 tbsp. coffee baking powder 250 g sugar 2 teaspoon cinnamon 4 eggs 1 cup whole milk yogurt & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![sponge cake](https://www.amourdecuisine.fr/wp-content/uploads/2010/02/sponge-cake_thumb_11.jpg)

Celeste is a very good friend, and faithful reader, who come all the time on my blog, she lives very far away, she is in Peru, it is very beautiful home now, I think I will go to her for this holiday , hihihihi 

yesterday she sent me this yogurt sponge cake recipe and I like to share it with you 

ingredients: 

  * 250g of butter 
  * 250g all-purpose flour 
  * 1 C. coffee baking powder 
  * 250 g of sugar 
  * 2 teaspoons cinnamon 
  * 4 eggs 
  * 1 cup whole milk yogurt 
  * Icing sugar 



  
Preparation: 

Preheat the oven to 170 º C. Butter and flour the mold.   
Melt the butter and let cool.   
sift the flour with the baking powder and cinnamon.   
In a bowl whisk the warm butter with the sugar, then add the eggs one by one. 

Add the dry ingredients and yoghurt. Do not mix too much   
mettre dans le moule préparé et cuire au four pendant 20-25 minutes. a la sortie du four laisser refroidir un peu, demouler et saupoudrer d’un peu de sucre glace 
