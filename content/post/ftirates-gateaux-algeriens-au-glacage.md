---
title: ftirates, Algerian cakes with icing
date: '2016-04-11'
categories:
- Algerian cakes with icing
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya
- Gateaux Secs algeriens, petits fours
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/ftirate-gateau-algerien_thumb1.jpg
---
##  ftirates, Algerian cakes with icing 

Hello everybody, 

Here is a very delicious Algerian cake the ftirate (ftiyrate, ftirates, ftirate) ... and I'll be very frank, it's a cake that I did not know at all, until one of my readers comes for me ask the recipe, I told him then, yes I'll get it, and at the same time, try to know more about this Algerian cake ... 

**ftirates, Algerian cakes with icing, Christmas biscuit**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/ftirate-gateau-algerien_thumb1.jpg)

Recipe type:  cake biscuit  portions:  30  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients** for cookies: 

  * 3 measures of flour (I used a 160 ml glass) 
  * 1 measure lukewarm melted butter. 
  * almost 1 measure of the mixture water + water of orange blossom 
  * lemon zest 

Icing 
  * 1 egg white 
  * 1 tablespoon orange blossom water 
  * 1 teaspoon lemon juice 
  * 1 teaspoon of oil 
  * dye 
  * icing sugar 



**Realization steps** Cookies preparation 

  1. In a bowl, sift the flour and add the measure of melted and cooled butter. 
  2. Rub with your hands so that the flour absorbs the butter, and then sieve for the mixture to be well incorporated. 
  3. Gradually add the mixture of water and orange blossom water and collect the dough into a ball without kneading. 
  4. Cover and let stand about twenty minutes. 
  5. Roll out the dough using a rolling pin and use a cookie cutter of your choice to cut the shape of your choice (my daughter at this stage had fun). 
  6. with another little cookie cutter, make a hole in the cupcake 
  7. cook for 10 to 15 minutes in a preheated oven   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateau-algerien_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateau-algerien_thumb1.jpg>)

icing: 
  1. place the egg white in a large bowl and mix gently with a fork 
  2. add orange blossom water, lemon juice and oil. 
  3. stir in the icing sugar gently with a tablespoon while mixing until you get a thick cream. 
  4. Test with a cake, if the glaze is too thick, add a little orange blossom water and if it is too liquid add icing sugar. 
  5. glaze all your cakes and let them dry completely in the open air before placing them in an airtight container. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
