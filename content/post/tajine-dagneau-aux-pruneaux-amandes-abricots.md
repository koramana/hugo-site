---
title: Lamb tajine with apricot almond prunes
date: '2012-07-22'
categories:
- boissons jus et cocktail sans alcool
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tajine-hlou-004_thumb.jpg
---
##  Lamb tajine with apricot almond prunes 

Hello everybody, 

Lamb tajine with apricot almond prunes is a very tasty dish. This marriage of sweet and salty in a dish that does not consume much at home, but I personally like to enjoy with a good homemade bread. 

This salty sweet tajine never leaves the first day of the month of Ramadan on our tables, there are even people who like it to be always on their table during the whole holy month of Ramadan. 

In any case, at home, I can say that I am the only one who will eat it, not serious about the fact that this dish can be well preserved in the refrigerator. And here is my little recipe, as my mother always did 

**Lamb tajine with apricot almond prunes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tajine-hlou-004_thumb.jpg)

**Ingredients**

  * Lamb meat 
  * dried prunes (for apricot mix, prune apples) 
  * some dried raisins 
  * ½ onion 
  * some butter or smen 
  * 3 c. tablespoon of crystallized sugar 
  * 2 tbsp. orange blossom water 
  * 1 pinch or 1 cinnamon stick 
  * 1 little saffron or dye 
  * 1 pinch of salt 



**Realization steps**

  1. arrange all the cut meat, butter or smen, chopped onion, dye, cinnamon and salt. 
  2. cover with about ½ liter of water and cook. 
  3. in the meantime, dip the prunes in the water for a few moments, as well as the raisins. 
  4. remove them, dip them in the sauce, add the sugar and orange blossom water, cook on low heat, until the prunes swell. 
  5. reduce the sauce 
  6. you can sprinkle your dish with some blanched and roasted almonds. 



good tasting . 

for a tajine hlou recipe without meat: 

![apple tajin](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajin-hlou-11.jpg) [ TAJINE HLOU AUX POMMES ET ANIS ÉTOILÉ, SANS VIANDE ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html> "tajine hlou with apples and star anise, without meat")
