---
title: the pyramids with almonds, Algerian cakes 2016
date: '2016-07-04'
categories:
- gateaux algeriens- orientales- modernes- fete aid
tags:
- Oriental pastry
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/les-gateaux-algeriens-Aid-2012-les-pyramides-300x225.jpg
---
##  the pyramids with almonds, Algerian cakes 2016 

Hello everybody, 

Well late, and as promised, here are the recipes of my Algerian cakes that I made for the feast of Aid el fitr, there were [ arayeches ](<https://www.amourdecuisine.fr/article-gateaux-algeriens-arayeche-101510145.html>) , laaroussa sbaa (recipe to come), ftimates (recipe to come), ghribia peanuts (recipe to come), the "S" (recipe to come), the [ Crunchy or Raisin Croquettes ](<https://www.amourdecuisine.fr/article-gateaux-secs-les-croquants-croquets-107572938.html>) , the [ griweches ](<https://www.amourdecuisine.fr/article-25345475.html>) , and the peanut baklawa (recipe coming soon) 

Today I share with you the recipe for almond pyramids, because I already have on the blog the [ pyramids without cooking ](<https://www.amourdecuisine.fr/article-gateau-sans-cuisson-les-pyramides-104662225.html>) , and this at the request of one of my readers, Samia, who really needs it, so good realization my dear. 

**the pyramids with almonds, Algerian cakes 2015**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/les-gateaux-algeriens-Aid-2012-les-pyramides-300x225.jpg)

portions:  30  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients** for the pasta: 

  * 250 gr of flour 
  * 75 gr of margarine 
  * 1 pinch of salt 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of white dye 
  * 1 cup of dye (color of your choice, here red, white and blue) 
  * Orange blossom water + water 

the joke: 
  * 250 gr of ground almonds 
  * 125 gr of sugar 
  * 1 C. a caramel aroma coffee 
  * 1 egg 

garnish: 
  * honey 



**Realization steps**

  1. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  2. Mix well with your fingertips. 
  3. divide the dough into two parts 
  4. for the one you are going to color in white add the dye, then pick up the dough with flower water, add a little water to pick up, continue to knead a few seconds. 
  5. for the other color, take the other half of the shortbread, add the dyes to the orange water until the desired color is obtained, and start to collect the dough with this water, add the water if you need, and knead a few seconds. 
  6. cover your dough and let it rest on the side. 
  7. for stuffing mix almonds and dry ingredients, collect with egg and set aside. 
  8. first prepare the pyramids with almond stuffing, if you have a mold to do it, it's great, otherwise start to make small cubes of 2.5 x2.5 cm and a height of 1 cm. 
  9. place a small ball of almond stuffing on top, and merge the two shapes manually to have the pyramid, continue until the almond stuffing has run out. 
  10. lower the white dough and pass it to the machine from number 3 to number 7, personally I do it with a baker's roll, I handle it better than the dough machine. 
  11. place the pyramids on it, and draw triangles in front of each side of the pyramid, the triangle must be a little smaller than the face of the pyramid, so in the end, your pyramid will be placed on a Stars with 4 arms. 
  12. brush the egg white pyramid, and raise the dough triangles to cover the pyramid. 
  13. do the same thing, with the second colored dough, but drawing a 4-arm star even smaller than the white star. 
  14. when the pyramids are fashioned, let them rest a whole night.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateaux-algeriens-2012-les-pyramides_thumb_11.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gateaux-algeriens-2012-les-pyramides_thumb_11.jpg>)
  15. then bake in preheated oven for 10 to 15 minutes 
  16. dip the hot pyramids, at the end of the oven, in lukewarm honey, perfumed with orange blossom. 
  17. present in boxes. 
  18. you can keep this cake in a hermetic box between 15 days and a month. but they must be put back in honey before presenting them. 



you can watch the stages of realization of this cake on this video: 

{{< youtube gDQ3dvaWbRg >}} 

j’espère que je n’ai pas oublier de détails dans la description du façonnage de ce gâteau. 
