---
title: Tlitli constantinois for the year hijir 1438
date: '2016-10-01'
categories:
- cuisine algerienne
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-constantinois-043.CR2_thumb.jpg
---
[ ![tlitli constantinois](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-constantinois-043.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-constantinois-043.CR2_2.jpg>)

##  Tlitli constantinois for the year hijir 1438 

Hello everybody, 

Here is a good Tlitli Constantinois for the year hijir 1438, so on the occasion of the new year Hijirien (hejirien): Awal Mouharem, I address all Muslims with my best wishes for joy, prosperity and good health 

and on this occasion, I prepared a delicious Tlitli Constantinois, in white sauce, as my grandmother and my mother always prepared. 

[ ![tlitli constantinois 040.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-constantinois-040.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-constantinois-040.CR2_2.jpg>)

_It's a recipe a little long to achieve, but it's worth it, because it's a treat ...._   


**Tlitli constantinois for the year hijir 1438**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-constantinois-031.CR2_thumb.jpg)

Recipe type:  dish, Algerian cuisine  portions:  6  Prep time:  40 mins  cooking:  45 mins  total:  1 hour 25 mins 

**Ingredients**

  * Ingredients: 
  * 500 gr of tlitli. 
  * 1 medium onion. 
  * chicken legs. 
  * 250 minced meat 
  * 2 chickpea grips soaked the day before 
  * ½ c s of smen 
  * 1 tablespoon of oil 
  * salt pepper 
  * parsley 
  * boiled eggs to accompany the Tlitli. 



**Realization steps**

  1. Grill the Tlitli in a pan on a low heat until it turns a beautiful golden color (this is optional, but I like to have golden tlitli) 
  2. Once the dough has been grilled, coat it with oil, 
  3. Meanwhile, in the bottom of the couscous, prepare the sauce: chop the onion (use half for the sauce and the rest for the meatballs). 
  4. add the chicken, smen, salt and pepper and fry. 
  5. add the chickpeas and water and cook. 
  6. put the tlitli in a couscoussier and steam. 
  7. Prepare minced meatballs, add ½ of remaining onion, salt, pepper and chopped parsley, and form small balls, 
  8. when the steam begins to clear from the top of the couscous, count for 15 minutes and remove the tlitli. 
  9. place it in a large bowl (gas3a, gassaa), and sprinkle with a little sauce. 
  10. let it absorb well, put it back in the couscous pot, and let it cook again. 
  11. Repeat this opportunity until the tlitli becomes tender. 
  12. now add the chopped meatballs, and cook. 
  13. towards the end, in a frying pan over low heat, place the tlitli and sprinkle with the sauce and allow to absorb the sauce well. 
  14. present with hard-boiled eggs .. 



[ ![tlitli constantinois 046.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-constantinois-046.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/tlitli-constantinois-046.CR2_2.jpg>)
