---
title: Carrot cake smoothie
date: '2017-08-18'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-carrot-cake1.jpg
---
[ ![carrot cake smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-carrot-cake1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-carrot-cake1.jpg>)

##  Carrot cake smoothie 

Hello everybody, 

It keeps asking me the question: what's the secret of your smoothies everyday? There's no secret my friends, the smoothies I have been making without stopping for over 3 years, my kids love it a lot, and me early in the morning is my beautiful magic cup that will boost me all day long. The only thing is that now I take pictures, thanks to my little laptop, no need to unpack my camera every morning. 

I hope that just like me, you will love these recipes smoothies all different from each other ... We have fun making smoothies. Personally, I'm never the recipe to the letter, I take the idea, and I do with what I have in my fridge. Today is a carrot cake that you will sip, so that tells you? 

**Carrot cake smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-carrot-cake..jpg)

portions:  2  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 1 banana sliced ​​and frozen 
  * 2 carrots cleaned and cut into small cubes. 
  * 1 glass of [ almond milk ](<https://www.amourdecuisine.fr/article-lait-d-amande-maison.html>)
  * 2 nuts 
  * ¼ teaspoons of cinnamon 
  * 1 pinch of ginger powder 
  * 1 pinch of nutmeg 

for the decoration: 
  * coconut 
  * crushed nuts 



**Realization steps**

  1. place all the ingredients in the bowl of a blender. 
  2. mix until reduced to a smooth smoothie. 
  3. enjoy everything fresh. 


