---
title: Couscous with vegetables
date: '2016-03-19'
categories:
- couscous
- Algerian cuisine
- Moroccan cuisine
- recipes of feculents
tags:
- Oriental cuisine
- Algeria
- dishes
- Algerian dish
- Ramadan 2016
- Healthy cuisine
- Morocco

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Couscous-aux-l%C3%A9gumes.jpg
---
[ ![Couscous with vegetables](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Couscous-aux-l%C3%A9gumes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Couscous-aux-l%C3%A9gumes.jpg>)

##  Couscous with vegetables 

Hello everybody, 

At home we love couscous, and as is the custom, couscous is present once a week on our table, especially on Friday. I do not know why this habit, but in any case, it is always to taste a good couscous rich in vegetables. 

The couscous with vegetables of my mother is a feast of vegetables, she puts everything and sauce is just a treat. At home it's not easy because everyone has their favorite vegetable, my two oldest they like carrots, my little one likes zucchini, just like me, my husband loves potatoes and chickpeas, I like everything vegetables (surprising because I did not like all these vegetables when I was small !!!). 

I would have liked to add turnips as my mother did, but no one eats it at home. You can of course add meat or chicken to this couscous, but I prefer it like that, just tasty in the cooking juices delicious fresh vegetables. 

[ ![Couscous with vegetables 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Couscous-aux-l%C3%A9gumes-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Couscous-aux-l%C3%A9gumes-3.jpg>)   


**Couscous with vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Couscous-aux-l%C3%A9gumes-2.jpg)

portions:  6  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 1 kg of [ Couscous steamed ](<https://www.amourdecuisine.fr/article-cuisson-du-couscous-a-la-vapeur.html>)
  * 1 to 2 onions depending on the size 
  * 2 to 3 carrots (you can put more at least) 
  * 2 to 3 turnips (I have not put) 
  * 2 to 3 zucchini (I personally like it a lot) 
  * 1 to 2 peeled potatoes 
  * 1 to 2 small aubergines 
  * 1 handful of chickpeas in a box (or more if you like, put more) 
  * 2 to 3 tablespoons canned tomato 
  * salt, pepper, ras el hanout, paprika 
  * 2 peppers 
  * oil 



**Realization steps**

  1. pass the onion to the robot and put in a pot (bottom couscousier) 
  2. add the oil, paprika, and a little water put on low heat, 
  3. add the sliced ​​turnips, as well as the carrots, 
  4. add the canned tomato, cover with water and cook. 
  5. When the carrots and turnips are a little tender, add the quarter cut potatoes, and the aubergines cut in half. 
  6. Add the chickpeas (if they are moullis the day before, they must be put at the beginning of the cooking) 
  7. add the 2 peppers, and cook, adjust seasoning according to your taste 
  8. Garnish the couscous with the vegetables and chickpeas and sprinkle with the hot sauce when serving. 



[ ![Couscous with vegetables 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Couscous-aux-l%C3%A9gumes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Couscous-aux-l%C3%A9gumes-1.jpg>)
