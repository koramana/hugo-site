---
title: Pasta gratin with tuna
date: '2017-02-13'
categories:
- Plats et recettes salees
- recettes aux poissons et fruits de mer

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gratin-de-pates-au-thon-1.jpg
---
![gratin pasta with tuna 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gratin-de-pates-au-thon-1.jpg)

##  Pasta gratin with tuna 

Hello everybody, 

here is a tuna pasta gratin recipe that a friend showed me, and that from the first try was a great success, it is a simple and easy recipe, but above all too good. 

last time I was talking to my sister, and she was telling me that she does not know what she will do at dinner, I told her about this recipe, I wanted to pass her the link, and to my surprise I see that I have never posted this recipe of this delicious gratin of pasta with tuna! So, this time as soon as I made it, I tried to take the most pictures for you. 

{{< youtube HAozHT >}} 

**Pasta gratin with tuna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gratin-de-pates-au-thon.jpg)

Recipe type:  gratin, pasta, dishes  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 500 grs of dough in the shape of butterflies for me 
  * a box of tomato in piece, otherwise 4 fresh tomatoes 
  * 2 cloves garlic 
  * 1 can of tuna in the oil 
  * gruyère as needed 
  * salt, black pepper, thyme 
  * table oil, or olive oil 
  * mozzarella to garnish. 



**Realization steps**

  1. cook the pasta in salted water, until it becomes soft 
  2. prepare the tomato sauce, in a pan with chopped tomatoes, crushed garlic, salt pepper, thyme and oil.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gratin-de-pates-au-thon-2.jpg)
  3. in a baking tin, a little buttered, mix pasta and tomato sauce 
  4. add the tuna drain from its oil (usually it is in this oil that I make my tomato sauce 
  5. add grated Gruyère cheese 
  6. decorate with a little mozzarella 
  7. bake in a preheated oven until the cheese is melted completely 
  8. TASTE 



![gratin pasta with tuna 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gratin-de-pates-au-thon-3.jpg)

c’est une recette que je vous conseille vivement, vous allez vraiment aimer 
