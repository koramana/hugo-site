---
title: chicken tikka massala pizza
date: '2016-03-11'
categories:
- pizzas / quiches / pies and sandwiches
tags:
- Pizza dough
- Aperitif
- inputs
- Algeria
- Italy
- Easy cooking
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pizza-au-poulet-tikka-massala-1.jpg
---
[ ![chicken pizza tikka massala 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pizza-au-poulet-tikka-massala-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pizza-au-poulet-tikka-massala-1.jpg>)

##  chicken tikka massala pizza 

Hello everybody, 

After the [ chicken skewers tikka massala ](<https://www.amourdecuisine.fr/article-brochettes-de-poulet-tikka-massala.html>) , and the delicious [ tikka massala chicken sauce ](<https://www.amourdecuisine.fr/article-recette-poulet-tikka-massala.html>) , I share with you today a pizza chicken tikka massala. I do not tell you my surprise, despite the chicken was super hot, my children did not leave anything, the pizza was swallowed in the blink of an eye ... 

the recipe is super simple, you just have to go a little early to marinate the chicken breasts. The pizza pie is still a choice for you, you can use the recipe of the [ mellow pizza pie ](<https://www.amourdecuisine.fr/article-pate-a-pizza-maison-moelleuse-et-croustillante.html>) (that's the one I used), or [ the magic dough ](<https://www.amourdecuisine.fr/article-pate-magique-facile-en-10-minutes.html>) . 

[ ![chicken tikka massala pizza](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pizza-au-poulet-tikka-massala.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pizza-au-poulet-tikka-massala-2.jpg>)   


**chicken tikka massala pizza**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pizza-au-poulet-tikka-massala-3.jpg)

portions:  6  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * [ mellow pizza pie ](<https://www.amourdecuisine.fr/article-pate-a-pizza-maison-moelleuse-et-croustillante.html>)
  * 200 gr of chicken breast in 2 cm cube 
  * 1 tablespoon of butter 
  * 2 cloves garlic crushed 
  * ½ seedless pepper 
  * salt 
  * 1 kg of fresh tomatoes or 2 tins of crushed tomatoes 
  * 1 glass of mozzarella 
  * 1 onion 
  * some branches of chopped coriander 
  * olive oil 

marinade: 
  * ½ teaspoon of cumin powder 
  * ½ teaspoon of chilli powder (chili) 
  * ½ cup of garam massala 
  * ½ teaspoon grated fresh ginger 
  * ¼ teaspoon of salt 
  * ¼ cup of black pepper 
  * 1 tablespoon of lemon juice 
  * 2 tablespoons natural yoghurt 
  * 1 pinch of cinnamon powder 



**Realization steps**

  1. In a bowl, place chicken pieces, add marinade ingredients and refrigerate for at least an hour. 
  2. After this time, line a baking dish with a layer of aluminum foil. 
  3. Divide the marinated chicken in a single layer (without the marinade) 
  4. Grill the chicken at 200 ° C for 6-7 minutes, turning in the meantime.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/poulet-tikka-marinade.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/poulet-tikka-marinade.jpg>)
  5. While the chicken is cooking, prepare the tomato sauce, Heat a small saucepan and melt the butter. 
  6. Brown the garlic and chilli, add the crushed tomato. season with a little salt, garam massala. 
  7. let the sauce simmer until thickened, another 2 minutes. Turn off the heat and add the chicken pieces in the sauce and mix well. 
  8. Preheat the oven to 200 ° C. spread the pizza dough (I got 3 small circles) 
  9. decorate it with a little sauce.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pr%C3%A9paration-de-la-pizza.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pr%C3%A9paration-de-la-pizza.jpg>)
  10. garnish with a little mozzarella. Arrange chicken on pizza and onion strips 
  11. Add the chopped coriander and the rest of the cheese. 
  12. Cook the pizza according to your oven. At the presentation garnish with a little chopped coriander. 



[ ![chicken pizza tikka massala 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pizza-au-poulet-tikka-massala-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pizza-au-poulet-tikka-massala-2.jpg>)
