---
title: 'Croquet with jam: dry cakes with jam'
date: '2015-04-26'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- Gateaux Secs algeriens, petits fours
tags:
- Algerian cakes
- Algeria
- biscuits
- To taste
- Dry Cake
- Aid
- Shortbread

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/biscuits-croquant-a-la-confiture.jpg
---
[ ![Croquet with jam: dry cakes with jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/biscuits-croquant-a-la-confiture.jpg) ](<https://www.amourdecuisine.fr/article-croquet-a-la-confiture-gateaux-secs-a-la-confiture.html/biscuits-croquant-a-la-confiture>)

##  Croquet with jam: dry cakes with jam 

Hello everybody, 

But here it is, this biscuit which is basically the biscuit [ croquet ](<https://www.amourdecuisine.fr/article-gateaux-secs-les-croquants-croquets.html> "croquets, crunchy recipe, dry cakes") well known in Algerian cuisine, revived by Oum Amani, has resurfaced this year on social networks, and especially facebook, it was even named the cake virus, a delicious culinary contagion, which eventually touch me. 

A dry biscuit, well melted and perfumed, which will be perfect when tasting or just to accompany a mint tea. I strongly advise you to use your favorite recipe of croquet, to be sure not to miss it. For my part and this time, I preferred to make the recipe of Umm Amani to the letter, because it was the source of my love at first sight. 

[ ![croquet with jam, dry cakes with jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/croquet-a-la-confiture-gateau-sec-a-la-confiture.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-croquet-a-la-confiture-gateaux-secs-a-la-confiture.html/croquet-a-la-confiture-gateau-sec-a-la-confiture-cr2>)   


**Croquet with jam: dry cakes with jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/gateau-sec-a-la-confiture-.jpg)

portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 2 eggs 
  * ½ glass of powdered sugar (1 glass of 220 ml) 
  * ½ glass of table oil 
  * 1 packet of vanilla 
  * the zest of a lemon 
  * 1 teaspoon of baking powder 
  * flour to pick up the dough 
  * decoration: 
  * 1 egg white 
  * jam according to your taste 
  * ½ teaspoon agar agar (or jelly for Oum Amani) 
  * flaked almonds 
  * icing sugar 



**Realization steps**

  1. Whisk eggs and sugar until blanching 
  2. add the oil while whisking. 
  3. perfume with the freshly grated zest of a lemon and vanilla 
  4. Now introduce the baking powder and the flour slowly with a spoon, until you have a paste that is homogeneous and picks up well. 
  5. line a baking tray with baking paper and form long chopsticks 
  6. trace with your finger a slot in the middle of the chopsticks 
  7. brush the sides with egg white and decorate with flaked almonds 
  8. Bake in a preheated oven at 180 degrees C for 12 to 17 minutes   
If during cooking you notice that the slot swells and disappears, do not hesitate to remove the cake and trace the slot again. 
  9. place the jam in a saucepan over medium heat to make it a little fluid 
  10. add agar or jelly, mix and remove from heat. 
  11. Remove the cakes from the oven, and fill the slot gently with the jam 
  12. decorate the sides with icing sugar. 
  13. let cool and make sure the jam is frozen before cutting the cakes. 



[ ![croquet with jam, dry cakes with jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/croquet-a-la-confiture.jpg) ](<https://www.amourdecuisine.fr/article-croquet-a-la-confiture-gateaux-secs-a-la-confiture.html/croquet-a-la-confiture>)
