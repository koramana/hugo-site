---
title: lemkhidettes, mkhedette laaroussa, the pillow of the Algerian cake bride
date: '2011-10-31'
categories:
- Healthy cuisine
- soups and velvets

---
Hi everyone, I lingered to post you this recipe: Lemkhidettes. especially now that I am in full preparation to return to England, I return tomorrow inchallah. but there are so many girls who ask me the recipe, especially since I had to publish the photos just after my return from setif in August, and after I did it especially because of the very high demand for this nice recipe very presentable, and it was beautifully presented on my plateau of the 2010 aid, and especially in the plateau of the sadaka at the mosquee, that i have hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.63  (  2  ratings)  0 

_** Hi everyone, I lingered to post you this recipe: Lemkhidettes. especially now that I am in full preparation to return to England, I return tomorrow inchallah.  ** _

_** but there are so many girls who ask me the recipe, especially since I had published the [ photos ](<https://www.amourdecuisine.fr/article-une-belle-selection-de-gateaux-algeriens-54958638.html>) just after my return from setif in august, and after i did it especially because of the very high demand for this nice recipe very presentable, and she was nicely presented on [ my plateau of aid 2010 ](<https://www.amourdecuisine.fr/article-gateaux-algeriens-pour-l-aid-2010-56806564.html>) and especially in the plateau of the sadaka at the mosque, which I have failed to publish to you.  ** _

_** it's a simple recipe, but it needs a little time to shape the cupcakes, I have to say that because I made big cakes, to braid and shape a piece, it took me 10 minutes ...... so it you have to be a good group to make this recipe, one slice the dough, another the slice cut, another braid, hihihihihihi  ** _

_** so we go to the recipe:  ** _

_** dough:  ** _

_** 3 measures of flour  ** _

_** 1/2 measure of butter  ** _

_** 1 pinch of salt  ** _

_** 1 teaspoon vanilla  ** _

_** a mixture of water of orange blossom + Water  ** _

_** food coloring according to taste  ** _

_** the joke :  ** _

_** 3 measures of coconut (pass a little to the blender)  ** _

_** 1 measure of sugar  ** _

_** the zest of a lemon  ** _

_** 1 teaspoon vanilla  ** _

_** 1 to 2 eggs   
** _

_** the sirup :  ** _

_** Of honey  ** _

_** Orange tree Flower water  ** _

_** In a bowl, put the flour, butter, vanilla, salt and orange blossom water.  ** _

_** Wet with water and knead to a firm, smooth dough.  ** _

_** Cover and let rest.  ** _

_** Divide the dough in half. Color half with food coloring.  ** _

_** Now prepare the stuffing by mixing coconut, sugar, vanilla, lemon peel and eggs.  ** _

_** spread the stuffing on a work surface, at a height of 8 mm to 1 cm and form squares of 5 cm side.  ** _

_** form balls with the dough, and start spreading the dough using the dough machine, pass the dough at No. 1, then No. 4, then No. 6. Then pass the dough in the tagliatelle part.  ** _

_** Place 5 white strips vertically and 5 strips of the other color horizontally.  ** _

_** for my part, I did not have the square piece of 5 cm, but another larger, so I had big cakes, and I use 7 strips not 5.  ** _

_** Put a square of stuffing in the center. Bring a white strip from top to bottom to cover the stuffing. Take a strip of the second color from right to left, Take back a white strip from top to bottom.  ** _

_** Take a strip from right to left and so on  _** until you cover the stuffing well. Cut the surplus. you can brush the strips of egg white so that they stick well on top of each other.  ** _ ** _

_** Bake in preheated oven for about 10 to 15 minutes. The dough must remain clear.  ** _

_** Let cool and dip after the cakes in honey. Drain.  ** _

_** merci a tout le monde, et j’attends toujours la 1500 abonnée pour qu’elle me contacte a fin de prendre ses coordonnées pour que je lui envois son cadeau.  ** _
