---
title: Bsissa the tamina of eastern Algeria
date: '2014-12-15'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries
- houriyat el matbakh- fatafeat tv
tags:
- Algerian cakes
- Algeria
- desserts
- Algerian patisserie
- Easy cooking
- Oriental pastry
- Delicacies
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/bsissa-de-lest-algerien-rouina-ou-zrira.CR2_.jpg
---
[ ![Bsissa the tamina of eastern Algeria](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/bsissa-de-lest-algerien-rouina-ou-zrira.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/bsissa-de-lest-algerien-rouina-ou-zrira.CR2_.jpg>)

##  Bsissa the tamina of eastern Algeria 

Hello everybody, 

**Bsissa the tamina of eastern Algeria** : Ah! I did not share with you the bsissa of my delivery, lol, my baby is already 10 months old, but I did not have the chance to invite you to taste this bsissa, or tamina of eastern Algeria. 

I am not going to tell you that it is the bsissa of Constantine, although it is my hometown, and although it is a family heritage. My grandmother on the father's side, prepared it herself, she grilled her own grains and took them to the mill neighborhood, to make the reserve of the year. For my grandmother it was grilled durum wheat and grilled chickpea too. 

But here, I was lucky enough to live in several cities in eastern Algeria, and Bsissa was present here and there, from Annaba to Taref, from Skikda to Mila, from Chelghoum el Aid to Setif, under several nominations, and with different compositions. The Bsissa the tamina of eastern Algeria becomes then, rummage here, or zrira by the. It can contain the malted grains, lentils, barley seeds, namely the tastes and desires of these women who carefully grind these grains so that it does not burn, then past them to the fine powder mill, for then be protected from moisture. 

![Bsissa the tamina of eastern Algeria](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/bsissa.jpg)

One thing is sure, despite the high caloric intake of this sweetness of the East of Algeria, remains that it is very rich in vitamins, fiber and minerals, which is why it is often prepared for the woman after childbirth, she l help to recover faster, and also for breastfeeding.   


**Bsissa: the tamina of eastern Algeria**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/bsissa-de-lest-algerien-zrira-ou-rouina-.jpg)

portions:  4  Prep time:  10 mins  cooking:  5 mins  total:  15 mins 

**Ingredients**

  * 300 gr of bsissa 
  * 220 gr of butter (more or less) 
  * 180 gr of honey (more or less according to taste) 



**Realization steps**

  1. Melt honey and butter in a microwave or saucepan over medium heat   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/zrira-bsissa-ou-tamina.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/zrira-bsissa-ou-tamina.jpg>)
  2. add ⅓ of the amount of bsissa and taste, if you think you need more butter, or more honey, do not hesitate to add one or the other.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/bsissa-rouina-ou-zrira.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/bsissa-rouina-ou-zrira.jpg>)
  3. Continue to add the bsissa to the spoon while mixing until you have the consistency you want, it should not be too liquid or too hard, because it will harden on cooling butter. 
  4. serve the bsissa in dishes, it is eaten with a spoon, and can be enjoyed with a good latte. 



![Bsissa the tamina of eastern Algeria](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/bsissa-de-lest-algerien.jpg)
