---
title: Aid moubarek 2013
date: '2013-10-15'
categories:
- bakery
- Buns and pastries
- sweet recipes
- pies and tarts

---
![6fdkzjrcyz30j9i8cp9.gif](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/6fdkzjrcyz30j9i8cp9.gif)

salam Alaykoum, 

On the occasion of the aid el adha, or as it is called in Algeria, Aid el kbir, I address to all the Muslims, of the whole world, to wish them to spend a beautiful party ... 

May the good God accept our sacrifice, and the pilgrimage of the people who are at the Meque ... 

I hope that you will spend a good and beautiful day in your family, and with your loved ones .. 

Pardonnez autrui, Allah est grand et Miséricordieux… 
