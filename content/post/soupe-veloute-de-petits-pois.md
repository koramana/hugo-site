---
title: Velvety soup with peas
date: '2017-06-13'
categories:
- soups and velvets
tags:
- Algeria
- Ramadan
- Ramadan 2017
- dishes
- Vegetables
- Fast Food
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/04/soupe-ou-velout%C3%A9-de-petits-pois-2.jpg
---
![soup or velvety peas 2](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/soupe-ou-velout%C3%A9-de-petits-pois-2.jpg)

##  Velvety soup with peas 

Hello everybody, 

During Ramadan, I do not eat that chorba and hrira, I even prefer to make soups of varied and tasty vegetables, and soup or velvety peas and one of the velvety that I like most. 

I love this sweet and salty flavor of this velvety, and it goes very well with a good bourek, or [ tuna rolls ](<https://www.amourdecuisine.fr/article-les-roules-au-thon.html>) . 

![soup or velvety peas](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/soupe-ou-velout%C3%A9-de-petits-pois.jpg)

**Velvety soup with peas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/soupe-ou-velout%C3%A9-de-petits-pois.jpg)

**Ingredients** for 2 people 

  * 400 gr of peas 
  * 1 to 2 carrots 
  * 2 cloves garlic 
  * 1 medium-sized onion 
  * 3 glasses of chicken broth water, or chicken bouillon cube 
  * 2 tablespoons of oil 
  * 30 grams of parmesan cheese (I had no, replace with cheddar cheese) 
  * black pepper, salt. 
  * lemon juice (not put, forget) 



**Realization steps**

  1. fry the garlic cut in large pieces in the oil, add the garlic also cut in chunk, salt and black pepper, add the diced carrots, fry again a little, add the peas, cover with chicken broth, and cook on medium heat. 
  2. When it's cooked, mash it by blending it, add Parmesan cheese, always coat, then mix with Chinese, to get rid of large pieces. 
  3. to present this velouté garnished with slice of lemon, or then with the fresh cream as it was the case with me, I accompanied the velouté of mini triangle stuffed, recipe to come very soon. 



![soup or velvety peas 1](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/soupe-ou-velout%C3%A9-de-petits-pois-1-660x1024.jpg)
