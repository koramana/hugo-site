---
title: Fish curry
date: '2017-02-05'
categories:
- cuisine indienne
- Cuisine par pays
- recettes aux poissons et fruits de mer
tags:
- Full Dish
- India
- Ramadan
- dishes
- Healthy cuisine
- Ramadan 2107
- Easy cooking
- sauces

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/curry-de-poissons.jpg
---
![fish curry](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/curry-de-poissons.jpg)

##  Fish curry 

Hello everybody, 

If you have never eaten a fish curry, I highly recommend this recipe. Personally and for lunch, I often do curry, because my children are at school, and I'm just for two, a curry very spicy, and when I say spicy, it's very spicy to have tears, finally, between pleasure and burns, hihihihi. 

**Fish curry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/curry-de-poissons-1.jpg)

portions:  4  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients**

  * 400 gr of white fish (cod) 
  * 2 tbsp. tablespoon of table oil 
  * 2 cloves garlic 
  * 3 shallots (onion here) 
  * ½ tsp (tea spoon, teaspoon) mustard seeds 
  * 1 cm chopped fresh ginger 
  * ½ green pepper 
  * 1 C. powdered curry 
  * 200 gr of coconut milk 
  * 200 gr of fresh tomato 
  * fresh coriander 
  * Salt 
  * 10 fresh curry leaves (which I did not have), also called "kaloupilé" (they are indeed "curry" leaves, you need fresh ones, the dry ones have no taste) 



**Realization steps**

  1. Start by thinly slicing the shallots (I used onions because I did not have any), and garlic. Grate the ginger, 
  2. Cut the fish into pieces. 
  3. Heat the oil over medium heat in a pan and add the mustard seeds.   
you have to cover with an anti-projection lid because they will jump like popcorn! Lower the fire as soon as they start to "pop", and remove the lid: 
  4. Add the shallots, garlic, ginger, chilli and kaloupile leaves. 
  5. Brown over medium heat for 5 minutes, stirring regularly. 
  6. Mix the curry powder with a little water, and pour into the pan, mix and fry on high heat for 1 minute stirring well: 
  7. Pour the coconut milk, and the crushed tomatoes. 
  8. Mix, salt, and bring to a boil. 
  9. As soon as it boils, lower the heat, add the fish, and simmer in small broths for about 1 hour uncovered without stirring. This cooking time will vary depending on the fish chosen, the size of the pieces, your cooking utensil etc. You have to taste: if the fish is rubbery it is not cooked enough. 
  10. Shred fresh coriander just before serving. 



![fish curry 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/curry-de-poissons-2.jpg)
