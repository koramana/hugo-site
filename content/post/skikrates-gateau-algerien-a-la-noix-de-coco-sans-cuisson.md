---
title: Skikrates Algerian cake with coconut without cooking
date: '2017-01-19'
categories:
- gateaux algeriens sans Cuisson
- gateaux algeriens- orientales- modernes- fete aid
tags:
- Algeria
- Pastry
- To taste
- desserts
- Ramadan
- Gateau Aid
- delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikrate-006_211.jpg
---
[ ![Skikrates Algerian cake with coconut without cooking](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikrate-006_211.jpg) ](<https://www.amourdecuisine.fr/article-25345462.html>)

##  Skikrates Algerian cake with coconut without cooking 

Hello everybody, 

Algerian cake skikrates with no cooking coconut is a recipe that I learned from my sister in law. She had made this recipe for Eid, and frankly I loved this delicious cake super perfumed with coconut. 

I really like this little joke that gives the appearance of [ Kefta (Algerian cake without cooking) ](<https://www.amourdecuisine.fr/article-kefta-gateau-algerien-sans-cuisson.html>) but with coconut, the recipe will please you a lot, because not only are they very easy to make these skikrates, we can give them pretty colors, and even make them in the shape of balls filled. 

![skikrates](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/skikrates.jpg)

realization of readers 

**Skikrates Algerian cake with coconut without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikrate-006_2-275x300.jpg)

**Ingredients**

  * 3 measures of coconut 
  * 1 measure icing sugar. 
  * 1 measure of milk powder. 
  * 1 tablespoon melted butter 
  * dyes according to your taste 
  * orange blossom water to water. 

for the stuffing: 
  * cookies 
  * jam. 

garnish: 
  * cristalized sugar 



**Realization steps**

  1. we start with the stuffing, you crush the biscuits, you mix with jam to your taste. 
  2. the dough should be easy to form 1 cm thick rolls. 
  3. you now mix the coconut, the icing sugar, the milk powder, you add the spoon of butter, and you wet with water of orange blossom, it is a paste easy to make and very handy, you can divide your dough and add different colors, you make as for the makrout, except that here cakes in the final must not exceed 2 cm thick. after stuffing them with the biscuit pudding + jam.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikratecor_thumb1.jpg)
  4. then you form sausages 2 cm thick and you cut into lozenges of 2 cm.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skikrate-007_21.jpg)
  5. put in boxes. 



[ ![Skikrates Algerian cake with coconut without cooking](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/skik-001_thumb_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-25345462.html>)

régalez vous. 
