---
title: tzatziki
date: '2013-01-19'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tzatziki-300x225.jpg
---
Hello everybody, 

you all know the **tzatziki** , where the **yoghurt cucumber** , a delight to be savored simply with a good [ bread ](<https://www.amourdecuisine.fr/categorie-10678924.html>) or **pita bread** .... or use it as a sauce. 

and when you have half a cucumber lying in the fridge, and a yoghurt pot that will expire at the latest tomorrow, well that's the solution, and it's a good opportunity to make a tzatziki. 

**tzatziki**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tzatziki-300x225.jpg)

**Ingredients**

  * 500 g Greek yoghurt 
  * 4 to 5 cloves of garlic. 
  * 1 cucumber. 
  * 4 tablespoons of olive oil. 
  * 2 tablespoons of vinegar. 
  * some mint leaves (here it's a choice, so it's optional for you) 
  * salt pepper 



**Realization steps**

  1. method of preparation: 
  2. Grate the washed cucumber into the big hole grater (I do not remove all the skin because I like the small pieces that it gives in the tzatziki) 
  3. Put the slices in a colander and sprinkle with salt. 
  4. Let it dry 2 to 3 hours for a better result 
  5. then squeeze them in your hands so that all the juice flows, or if you can, through a muslin, it will be even better. 
  6. grate the garlic very finely. 
  7. place the yogurt in a bowl. incorporate the drained cucumber and garlic. 
  8. Mix well together. 
  9. add the vinegar, the olive oil then the salt and the pepper. 
  10. place it in a salad bowl, cover with a cool leave until serving 


