---
title: fluffy tart with strawberries and almonds
date: '2015-06-04'
categories:
- dessert, crumbles et barres
- sweet recipes
tags:
- desserts
- Cakes
- To taste
- Algerian cakes
- Pastry
- Ramadan
- Ramadan 2015

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tarte-moelleuse-aux-fraises-et-amandes-1.jpg
---
[ ![fluffy tart with strawberries and almonds 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tarte-moelleuse-aux-fraises-et-amandes-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42678>)

##  Fluffy tart with strawberries and almonds 

Hello everybody, 

This mellow pie is the basis of a **fluffy cake with almonds** well-founded, of a **custard** rise in muslin with a little mascarpone, and decorate with strawberries ... I must say that I love this beautiful decoration of **Lunetoiles** . 

[ ![fluffy tart with strawberries and almonds 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tarte-moelleuse-aux-fraises-et-amandes-2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42679>)   


**fluffy tart with strawberries and almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tarte-moelleuse-aux-fraises-et-amandes-11.jpg)

portions:  8  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients** mellow almonds: 

  * 3 eggs 
  * 90 g of sugar 
  * 150 g white almond powder 
  * 1 vanilla pod 
  * 50 g melted butter, plus a little bit for the mold 

Cream: 
  * 210 g of milk 
  * 20 g cornflour 
  * 2 egg yolks 
  * 40 g of sugar 
  * 50 g of butter 
  * 1 tablespoon of mascarpone (or for lack of good thick cream) 

Dressage: 
  * gariguette strawberries 
  * 2, 3 beautiful handful of crushed toasted almonds 
  * Strawberry jam 
  * icing sugar 



**Realization steps** mellow almonds: 

  1. Preheat your oven to 175C. 
  2. Mix the eggs and the sugar energetically, 
  3. stir in the almond powder, mix until you obtain a homogeneous mixture. 
  4. Add the melted butter and mix one last time until the butter is well incorporated. 
  5. place the preparation in a mold of 22 cm diameter buttered and floured. 
  6. You can bake it for 45 minutes of cooking. 
  7. Remember to plant a skewer (iron) or the tip of a knife in the cake a few seconds, it must come out clean when the cake is cooked. 
  8. While the mellow bakes prepare the cream: 
  9. Heat the milk in a saucepan. 
  10. In a bowl whiten (whisk vigorously until you get a light white color) egg yolks with sugar, add cornstarch and mix until a homogeneous mixture is obtained. 
  11. Pour the warm milk over twice, mixing well. Put back on medium heat and cook the cream at 85 ° no more. If you do not have a thermometer stay alert, if the cream starts to take too quickly do not hesitate to take the pan out of the fire (to lower the temperature) and put it back while stirring well. 
  12. Once the cream is done, let the temperature of the cream go down to 40 ° (you have to be able to put your finger without getting burned) and add the cold butter to pieces while stirring well. Mix with a dipping blender to have an ideal texture and incorporate the mascarpone. Put it in a flat film to the touch until completely cool, at room temperature. 

Dressage: 
  1. Brush the edges of the cooled cake with heated strawberry jam, and roll in chopped toasted almonds. 
  2. Place the cake on a serving platter. 
  3. On the cold cake, arrange the cold cream with a spatula. 
  4. Arrange the strawberries according to your taste. 
  5. Sprinkle with a few pinches of chopped almonds and sprinkle with icing sugar. 
  6. Serve well fresh! 



[ ![fluffy tart with strawberries and almonds 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tarte-moelleuse-aux-fraises-et-amandes-12.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42681>)
