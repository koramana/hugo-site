---
title: khobz tounes with nuts
date: '2015-04-28'
categories:
- gateaux algeriens au miel
- gateaux algeriens- orientales- modernes- fete aid
- gateaux, et cakes
- gourmandises orientales, patisseries orientales
- recettes sucrees
tags:
- Pastry
- Algeria
- Aid
- Ramadan
- Algerian patisserie
- desserts
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/khobz-tounes-aux-noix-1.jpg
---
[ ![khobz tounes with nuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/khobz-tounes-aux-noix-1.jpg) ](<https://www.amourdecuisine.fr/article-khobz-tounes-a-la-noix.html/khobz-tounes-aux-noix-1>)

##  khobz tounes with nuts 

Hello everybody, 

A recipe from the Algerian culinary drawer that retains its originality and charm. [ Khobz tounes ](<https://www.amourdecuisine.fr/article-khobz-tounes.html> "khobz tounes") also known as khobz el bey, an Algerian pastry that was made with almonds and bread crumbs to avoid the wasting of hard bread at the time. 

This time we will see a different version of khobz nut tunes and ground biscuits, I love this version of Wamani merou, so do not miss the recipe, which is a breeze.  khobz tounes with nuts 

**khobz turn on the nut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/khobz-tounez-aux-noix.jpg)

Recipe type:  Algerian pastry  portions:  12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 1 measure of eggs (3 to 4) 
  * 1 measure of sugar 
  * 1 measure of oil 
  * ½ measure of milk 
  * 1 measure chopped walnuts 
  * 1 measure of ground biscuits 
  * 1 measure of medium semolina 
  * 1 sachet of chemical yeast 
  * 1 packet of vanilla 
  * Syrup: 
  * 3 water measurements 
  * 1 and a half measure of sugar 



**Realization steps**

  1. beat the eggs, sugar, vanilla and add the oil 
  2. add the milk and the rest of the ingredients to make a smooth dough. 
  3. generously butter a mold and pour the mixture into it 
  4. cook in an oven preheated to 180 degrees between 40 and 45 minutes until the cake is golden brown. 
  5. Cut out the cake according to your taste or in rhombuses 
  6. water the cake well warm with the warm syrup prepared well before. 
  7. Decorate according to taste and enjoy with a good mint tea 



khobz tounes a la noix 
