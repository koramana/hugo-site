---
title: Barley salad with butternut butternut squash
date: '2015-12-05'
categories:
- salads, salty verrines
tags:
- Medley
- inputs
- Fast Food
- Easy cooking
- Healthy cuisine
- Grenade
- Vinaigrette

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-dorge-et-courge-musqu%C3%A9e-1.jpg
---
[ ![barley salad and butternut squash 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-dorge-et-courge-musqu%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-dorge-et-courge-musqu%C3%A9e-1.jpg>)

##  Barley salad with butternut butternut squash 

Hello everybody, 

Always on the quill of delicious Ebly, here in England, but I never find. This time, when I went out with my husband, I found barley seeds quick cooking, it looked a lot like Ebly, I told myself that I have nothing to lose ... And frankly I do not was not disappointed, a barley salad is a real delight, the taste was not too far from that of the Ebly, I even had fun making several variations of this salad. 

This recipe was for me and my daughter, especially since my daughter loves pomegranate seeds, she feasted on enjoying this salad with me. 

[ ![barley and butternut squash salad, butternut squash](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-dorge-et-courge-musqu%C3%A9e-butternut-squash.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/salade-dorge-et-courge-musqu%C3%A9e-butternut-squash.jpg>)
