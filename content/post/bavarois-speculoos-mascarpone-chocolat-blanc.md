---
title: Bavarian speculoos mascarpone white chocolate
date: '2011-12-19'
categories:
- appetizer, tapas, appetizer
- cuisine diverse
- Cuisine saine
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/bavspec1_thumb1.jpg
---
##  Bavarian speculoos mascarpone white chocolate 

Hello everybody, 

For my husband's birthday, July 31, I prepared this Bavarian speculoos mascarpone white chocolate, which was a great success, and that everyone loves. 

I like the Bavarians a lot, despite the fact that it takes a lot of time for the realization, especially if we venture to more than one layer and a mirror, we must make sure that a layer has taken well before starting to realize the second. 

But the final result of the Bavarian is something else, because with the usual birthday cakes, we know in advance the taste, but a Bavarian, we can do an infinity of wedding flavors. 

**Bavarian speculoos mascarpone white chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/bavspec1_thumb1.jpg)

**Ingredients** this Bavarian was composed by: 

  * a chocolate biscuit. 
  * a mascarpone mousse / white chocolate. 
  * a speculoos mousse. 
  * a chocolate mirror   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/bavspec6_thumb1-150x150.jpg)

the chocolate biscuit: 
  * 50g of flour 
  * 150g of dark chocolate 
  * 60g of sugar 
  * 100g of butter 
  * 3 eggs 
  * 2 tablespoons almond powder (which you can replace with grated coconut) 
  * A pinch of salt 
  * Half a cup of baking powder 

mascarpone mousse / white chocolate: 
  * 140 ml of milk 
  * 2 egg yolks 
  * 30 gr of powdered sugar 
  * 140 gr of white chocolate 
  * 250 gr of mascarpone. 
  * 2 g of gelatin powder (1 cac, my gelatin is a vegetable gelatin)   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/bavspec8_thumb1-150x150.jpg)

Foam speculoos: 
  * 200 ml of fresh cream 
  * 100 gr of speculoos paste, recipe here 
  * 1 C. gelatin 

chocolate mirror: 
  * 50 g of chocolate 
  * 50 g of fluid cream 
  * 50 g icing sugar   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/bavspec3_thumb1-150x150.jpg)



**Realization steps** chocolate biscuit: 

  1. Melt the chocolate in the microwave or in a bain-marie 
  2. Add the butter and mix until it melts in the chocolate 
  3. Beat the eggs with the manual whisk with the sugar, before adding them to the choco mixture 
  4. Add the almond powder 
  5. Finally, add the flour and the yeast little by little and pour the paste obtained into a mold 24 buttered 
  6. Bake 15 to 20 minutes at 180 ° C 
  7. At the exit of the oven let cool   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/bavspec10-150x150.jpg)

mascarpone mousse / white chocolate: 
  1. In a saucepan, warm the milk. 
  2. In a bowl, beat the egg yolks with the sugar until the mixture whitens and becomes creamy. 
  3. Pour the milk gently and mix. 
  4. Put everything in the pan and cook the custard without boiling (the cream is cooked when it is thick and it laps the spoon). 
  5. Crush the chocolate and pour the cream on it. 
  6. Mix well to melt the chocolate. 
  7. Add the gelatin, whip and let cool. 
  8. beat the mascarpone, and gently add it to the previous mixture. 
  9. place the biscuit in a pastry circle, prepare the speculoos all around, you can put your biscuits to the desired size. 
  10. and pour the first mousse, put in the fridge for this mousse to take well. 

Foam speculoos: 
  1. dissolve the gelatin in a little water, pass it to a tide bath so that it becomes liquid, add it to the speculoos paste. 
  2. put the cream in whipped cream, gently mix the dough with speculoos, and pour this foam, on the first apparatus. 

chocolate mirror 
  1. melt on a low heat chocolate with sugar and cream. 
  2. Pour over the entremets and allow to harden 
  3. garnish after according to your taste. 



bonne dégustation 
