---
title: Avocado mimosa-avocado egg
date: '2014-03-29'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/oeuf-mimosa-amuse-bouche.CR2_thumb.jpg
---
[ ![Avocado mimosa-avocado egg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/oeuf-mimosa-amuse-bouche.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/oeuf-mimosa-amuse-bouche.CR2_2.jpg>)

##  Avocado mimosa-avocado egg 

Hello everybody, 

Egg mimosa-avocado cream. Here is delicious mouth-watering, mimosa eggs, an aperitif very easy to make, very presentable, and that can be made in all sauces, and with a lot of decoration .... 

I have several versions of these little delights, but I prefer those, with the avocado cream, which I like very much. 

Generally, I prepare these eggs with a tuna cream .... a delight. but this time, I did it with the lawyer, do you like lawyers? I do not speak of these lawyers of justice, leaving them apart, they like my cooking, but I hope to know a lawyer only by friendship, not for business, lol. 

So I'm talking about avocados, this fleshy vegetable, melting rich in omega 3, and mono-unsaturated acids that have beneficial effects on cardiovascular function. 

Yes now, you know what lawyer I'm talking about, so we're back to this delicious starter.   


**Avocado mimosa-avocado egg**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/oeuf-mimosa-019.CR2_thumb_1.jpg)

portions:  4  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * hard boiled eggs 
  * a few spoons of mayonnaise. 
  * 1 lawyer 
  * salt, paprika. 



**Realization steps**

  1. Cook the eggs in water and cut them in half. 
  2. remove the egg yolk delicately. 
  3. Mix the avocado with mayonnaise and egg yolk. add the salt to the mixture. 
  4. replace the cavities of the egg whites, decorate with paprika, and present. 



[ ![mimosa egg, aperitifs with avocado.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/oeuf-mimosa-aperitifs-a-l-avocat.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/oeuf-mimosa-aperitifs-a-l-avocat.CR2_2.jpg>)
