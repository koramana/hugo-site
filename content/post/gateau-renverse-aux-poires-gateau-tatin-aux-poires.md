---
title: pie cake with pears
date: '2011-10-31'
categories:
- panna cotta, flan, and yoghurt
- recettes sucrees

---
I'm the type who always likes my morning milk is well accompanied, but differently to accustom him, I found myself today with "nothing" so immediately at my closet, before the awakening of my husband and children (a mom does not rest even the weekend !!!) and I start to see what I can prepare, no time to turn on the pc, to see a recipe here or there, and improvisation in any case I have not been on it, nor everyone at home, because 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

I'm the type who always likes my morning milk to be well accompanied, but differently to accustom him, I found myself today with "nothing" 

so right now at my closet, before the awakening of my husband and children (a mom does not rest even the weekend!) 

and I start to see what I can prepare, no time to turn on the pc, to see a recipe here or there, and improvisation does its part 

in any case I have not been on it, nor everyone at home, because it was a super delicious cake. 

so my ingredients were: 

for caramel 

  * a little sugar (at random) 
  * a little butter 



for the cake 

  * 3 eggs 
  * 120 grams of sugar 
  * the zest of a lemon 
  * 180 grams of flour 
  * 1 sachet of baking powder 
  * 50 grs of melted butter. 
  * and a box of pear syrup 



in a saucepan caramelize a little sugar, while stirring, and add a little butter and pour everything into your mold 

garnish the mold with pears cut into large pieces 

beat the eggs with the sugar, add the zest of a whole lemon, then incorporate the flour and baking powder. 

melt the butter and add it gently while mixing with a spatula. 

pour this mixture over the pears, and bake in a preheated oven at 180 degrees. 

dégustez!!!! 
