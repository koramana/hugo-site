---
title: Maamoul with dates
date: '2011-10-31'
categories:
- dessert, crumbles et barres
- cakes and cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/maamoul-71_thumb.jpg
---
Maamoul with dates 

Hello everyone, 

This Maamoul with dates was made by pure accident ... I tell you the story, when I wanted to do [ the makrout ](<https://www.amourdecuisine.fr/article-26001222.html>) My old Libra, who was already beginning to give me wrong figures, went around weighing the clarified butter! I found myself (to have the consistency of the makrout paste) with a ton of well-greased pastry, so I took a quantity and put it aside. 

After a few days, I took again the dough to which I added a little flour, and I prepared this maamoul, the date maamouls were good and delicious, that I made them again at the request of my children and here I share the recipe with you. 

![maamoul 71](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/maamoul-71_thumb.jpg)

**Maamoul with dates**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/maamoul-2_thumb.jpg)

**Ingredients**

  * 3 glasses of fine semolina (a glass of 250ml) 
  * half a glass of flour 
  * a glass and a quarter of melted butter 
  * 2 salt tongs 
  * half a glass of icing sugar 
  * a teaspoon of baking powder 
  * \+ or - a glass of milk all meets the absorption of semolina 

for the farce 
  * the date paw about 200 gr 
  * 2 teaspoons of cinnamon powder 
  * 3 tablespoons melted butter 
  * 6 tablespoons water 



**Realization steps**

  1. in a large salad bowl mix the semolina and the flour 
  2. add salt, sugar and baking powder and mix well 
  3. add the melted butter and squeeze well between the hands 
  4. let stand a minimum of 4 hours 
  5. after the rest water your mix with milk in a small amount mixing with the ends of the fingers (be careful not to work too much dough)   
![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/2010-11-13-maamoul-7_thumb.jpg)
  6. pick up your mixture into a ball of dough to cover and let rest for an hour longer 
  7. meanwhile prepare your stuffing 
  8. mix all ingredients together and divide into 50 pellets cover and set aside for later 
  9. divide your dough into 50 small balls 
  10. flatten a dough dumpling put in the middle a stuffing ball go up the edges of the dough and close 
  11. continue with the rest of the balls until the pellets are completely used up. 
  12. put a ball in the mold has slightly floured maamoul lightly press the ball and 
  13. reverse the mold slightly hitting the top of the mold to pull out the ball 
  14. transfer to a baking tray and bake at 180 degrees until the maamoul is lightly browned 
  15. let cool and sprinkle with icing sugar 



_merci_
