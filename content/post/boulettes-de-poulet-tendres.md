---
title: Tender chicken meatballs
date: '2016-11-11'
categories:
- appetizer, tapas, appetizer
- idea, party recipe, aperitif aperitif
tags:
- Tapas
- Apero
- Aperitif
- Amuse bouche
- Entrees
- Holidays
- meatballs

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/croquettes-de-poulet-009_thumb.jpg
---
##  Tender chicken meatballs 

Hello everybody, 

You want a super easy and successful recipe for tender chicken meatballs? here is a recipe that I strongly recommend. 

These chicken balls are super tender and successful every time, ideal to present to your guests as an appetizer, buffet or a super nice party table. These **Tender chicken meatballs** have everything to please and big and small ... So treat yourself and surprise your little tribe or your guests with this little delight. 

**Tender chicken meatballs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/croquettes-de-poulet-009_thumb.jpg)

**Ingredients** for 20 balls of 26 gr each 

  * half of a chicken breast 
  * 2 hard boiled eggs 
  * 2 potatoes 
  * 2 portions of the cheese the laughing cow 
  * salt, pepper, garlic powder 

for breading: 
  * flour 
  * egg + 1 teaspoon of oil 
  * breadcrumbs 



**Realization steps**

  1. cut the chicken breast into pieces, salt, pepper and cook in a little oil 
  2. peel the potatoes, cut into cubes, salt a little, and steam 
  3. pass the chicken breasts to the blender. 
  4. crush the mashed potatoes. 
  5. add the crushed hard eggs,   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/2012-06-21-croquettes-de-poulet_thumb.jpg)
  6. stir in the cheese portions, and mix well by hand 
  7. prepare meatballs of almost 26 gr each 
  8. roll these dumplings into the flour, then into the egg and oil mixture. 
  9. then continue the breading of these dumplings in breadcrumbs 
  10. place in the refrigerator for at least 1 hour.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-de-poulet-recette-du-ramadan_thumb1.jpg)
  11. put the meatballs in a hot oil bath for 5 minutes, place on paper towels (although it will not be too fat). 



Present with a beautiful [ tomato sauce ](<https://www.amourdecuisine.fr/article-sauce-tomate-fait-maison-facile.html>) , or with a [ piquant butter with peanut butter ](<https://www.amourdecuisine.fr/article-creme-piquante-au-beurre-de-cacahuete-89052515.html>) , or even with [ tzatziki ](<https://www.amourdecuisine.fr/article-tzatziki-105328008.html>) . 
