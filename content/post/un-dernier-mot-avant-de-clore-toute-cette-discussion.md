---
title: one last word before closing this whole discussion
date: '2010-01-24'
categories:
- amuse bouche, tapas, mise en bouche
- ramadan recipe
- recettes de feculents
- salads, salty verrines

---
yesterday, I almost spent my whole day on the forum already mentioned, to find my stolen recipes and the recipes of my other blogger friends.   
at first there was an incredible anger that blinded my eyes, especially when I saw that they dared to steal my daughter's birthday cake recipe, frankly and honestly, I had tears in my eyes because I make all my recipes with passion, and the birthday cake of my daughter did it with great love, I remember having to watch until 3 o'clock in the morning, to be able to put it on foot, and a person comes take it like that, without even saying "I like this cake, and I want to share it with other people"   
in any case, I was looking for recipes, and the list got longer and longer, at the beginning we do not understand what happens, but after we do not know what to do, judge this person, or let it do this judge a day ... .. this day will come sooner or later, everyone will take with him, only what he has done in this life.   
I come back to this person, she comes a first time on your blog, she sees a recipe, she thinks, oh it's not going to hurt to take it and put it on a forum in my name, yes between us it's true that it does not matter .... she has put it on, and the praises begin, she catches the madness of grandeursa happens to us too, so we do not judge.   
she comes back again, no one has noticed that she has taken a recipe or a picture, she is going to serve herself again, and the glory goes on, no one realizes, no one sees it, that's what she says to herself, and she continues every day, she finds another blog, another source, she continues, in a few days, she finds herself with about thirty recipes, people love them and ask even more, in her head, she does service, but .... is this really the reality ????? in a small period, just to follow the glory, she finds herself led to take a good that is not hers, and to commit a sin, we cut the hand for a sin like this in our religion. but the misfortune in all that, she does not realize what she did.   
I may not have seen it, you have not seen it, the others have not seen it, but ...... the good god he sees everything, she will tell him what the day of the judgment? ???? I was happy to others ????? we needed my help ????? no it was not a serious thing ?????   
what is she going to say ?????   
she commits the sin of stealing, and not only that, the people they deceive? she is not going to be judging on that ???? What do you think????   
if she will read that, she will understand the harm she did, or else she will always find excuses deep inside herself ????   
I am not here to judge anyone, I do not have the right to do it.   
in any case, me this person, and any other person (because must say that it is a clan that grows) did not hurt me, she hurt herself.   
we get tired making a recipe, take photos, write a recipe, try to explain as much as possible, give everything without hiding anything, without cheating, sometimes it's a 3-day job, including I even do it for a whole week, and a person comes to take it, without even reading it sometimes, because sometimes when I read what she copies, it makes me laugh.   
she puts the article in her name, she accepts the thanks, and she even answers the questions asked, courage, daring, intelligence, I do not know how I should qualify this person.   
I empty my bag, I put an end to this discussion, I do not want to torture me and hurt me every time I find one of my recipes on a forum, I leave this person between her and her conscience and if I do not see what she's doing, the good god sees everything.   
you like this article, you want to put it on your blog, serve you, like that, if this person does not read it at home, she will read it at home.   
Merci pour votre solidarité, portez vous bien 
