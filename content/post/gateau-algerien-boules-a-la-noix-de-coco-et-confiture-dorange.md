---
title: Algerian cake, coconut balls and orange jam
date: '2012-05-01'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid
- sweet recipes

---
cakes without cooking, Algerian cake hello everybody, a very good Algerian cake that my step sister has prepared, and whose idea came from a failure of a Mouskoutchou. it's always great to be able to catch up. a recipe very simple, but also very delicious, I feasted with a good cup of tea. the ingredients: 10 eggs 200 grs of sugar 8 ca soup of oil 120 ml of milk 1 sachet of baking powder (10gr) 250 grs of flour 2 ca cocoa soup 100 grs of raisins for filling: water orange blossom 500 grs & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.88  (  3  ratings)  0 

#  [ cakes without cooking, Algerian cakes ](<https://www.amourdecuisine.fr/categorie-11745248.html>)

Hello everybody, 

a very good **Algerian cake** that my sister-in-law prepared, and whose idea came from a failure of a [ Mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouskoutchou-mouchete-87564683.html>) . it's always great to be able to catch up. 

a **very simple recipe** but also very delicious, I enjoyed a good cup of tea. 

Ingredients: 

  * 10 eggs 
  * 200 grams of sugar 
  * 8 tablespoons of oil 
  * 120 ml of milk 
  * 1 sachet of baking powder (10gr) 
  * 250 grams of flour 
  * 2 tablespoons of cocoa 
  * 100 grams of raisins 



for garnish: 

  * orange blossom water 
  * 500 grams of coconut 
  * Orange jelly 



start by making the cake, beat the foam eggs, then add the oil and sugar, continue beating, add the milk, then add the yeast and the flour slowly, add the 2 tablespoons of cocoa, and mix once again. 

pour everything into a round tray 40 cm in diameter, place in a preheated oven at 180 degrees C, until cooking 

after cooking, crush the cake in more or less small pieces, then wet with the orange blossom water until you have a soft and malleable paste. 

add after 4 tbsp. soup of jam and 9 tablespoons of coconut, if the dough does not curl up, add a little orange blossom. 

form balls. wrap these balls in jam diluted with a little orange blossom water, and then coat in coconut. 

place in boxes and enjoy. 

merci pour vos commentaires, et merci de vous abonnez a la newsletter si vous voulez etre au courant de toute nouvelle mise a jour. 
