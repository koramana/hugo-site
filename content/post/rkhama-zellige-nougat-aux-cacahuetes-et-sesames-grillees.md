---
title: Rkhama Zellige, Nougat with peanuts and grilled sesame
date: '2011-12-27'
categories:
- Chocolate cake
- birthday cake, party, celebrations
- cakes and cakes
- recipes of feculents
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/rkhama1.jpg
---
![rkhama.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/rkhama1.jpg)

##  Rkhama Zellige, Nougat with peanuts and grilled sesame 

Hello everybody, 

I come back to the sweet recipes of my friend Lunetoiles, and always with the cakes of the aid, today I deliver you its recipe of Zellige زلليج Rkhama رخامة, or Nougat with peanuts and grilled sesame, already nothing that the name gives me really desire. 

so a copy / paste of the recipe with the small modifications of Lunetoiles:   


**Rkhama Zellige, Nougat with peanuts and grilled sesame**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/Zellige11.jpg)

portions:  25  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 1 kg roasted and mixed peanuts 
  * 1 bowl of grilled sesame seeds 
  * 1 teaspoon cinnamon 
  * 400g of white chocolate (dark chocolate here) 
  * 400 g of milk chocolate 
  * honey (to pick up the peanut paste) 



**Realization steps**

  1. Melt dark chocolate in a bain-marie Mix peanuts, sesame, cinnamon, and honey until you can form a ball, 
  2. cover a worktop with a sheet of food film, 
  3. put your ball on it, then cover it with another sheet of food film, 
  4. spread out with a baking roll, then put this dough in a mold (a tray, a fried noodle, depending on the width and length obtained) 
  5. remove the first sheet of the cling film, and apply the first layer of melted chocolate 
  6. To go faster, let this chocolate layer in the refrigerator, 
  7. during this time, melt the milk chocolate in a bain marie, and a little dark chocolate aside, and fill a cone for decoration 
  8. cover the other side with milk chocolate, and start decorating 
  9. With dark chocolate, draw parallel lines on the layer of milk chocolate, 
  10. then with the help of a knife draw lines perpendicular to the first lines drawn, to make mottles 
  11. let take, then cut the cupcakes according to the desired size and shape 



![nougat-to-peanuts-and-sesames-grillees.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/nougat-aux-cacahuetes-et-sesames-grillees1.jpg)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
