---
title: Couscous with cabbage
date: '2017-07-21'
categories:
- couscous
- Algerian cuisine
- recipes of feculents
tags:
- Full Dish
- Semolina
- Algerian couscous
- beans

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/couscous-au-chou-1.jpg
---
[ ![couscous with cabbages](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/couscous-au-chou-1.jpg) ](<https://www.amourdecuisine.fr/article-couscous-au-choux.html/couscous-au-chou-1>)

##  Couscous with cabbage 

Hello everybody, 

To be honest, and although I had the chance to taste a lot of food Laghouat prepared by the care of an old neighbor of the same city, this dish: couscous cabbage is a dish that I have never tasted. 

This delicious dish of cabbage couscous is a recipe from one of my readers: **Ninna A.** In Laghouat this dish is known as mesfouf, a salted mesfouf, but in the version of Ninna A, there is the addition of chicken white sauce to this salty mesfouf with cabbage and beans, one thing it learned from her beautiful family, and she appreciates a lot because the mesfouf becomes even more delicious. This recipe in any case has had its success on our kitchen group on facebook.   


**Couscous with cabbage**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/couscous-au-chou-2.jpg)

portions:  5-6  Prep time:  25 mins  cooking:  40 mins  total:  1 hour 5 mins 

**Ingredients**

  * 1 medium-sized cabbage 
  * 500 gr of fresh beans. 
  * 1 chopped onion 
  * 1 kg of medium couscous 

for the white sauce: 
  * chicken legs with skin 
  * 1 tablespoon smen (clarified butter) 42 tablespoons of table oil 
  * 1 onion grated 
  * ½ fresh tomato (for the taste) 
  * salt and black pepper 
  * water 



**Realization steps**

  1. chop the cabbage, salt and steam in the top of the couscous maker. 
  2. clean the beans and cut them into pieces of almost 1 cm, salt and place steam in another couscous top. 
  3. halfway through baking add on top of grated onion   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/couscous-au-chou-et-feves1.jpg) ](<https://www.amourdecuisine.fr/article-couscous-au-choux.html/couscous-au-chou-et-feves-2>)
  4. when cooking these vegetables, place them in a large salad bowl. 
  5. proceed to [ cooking couscous with steam ](<https://www.amourdecuisine.fr/article-cuisson-du-couscous-a-la-vapeur.html> "Cooking couscous with steam")
  6. when cooking couscous, pour everything hot on the vegetables and mix   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/couscous-au-chou-6.jpg) ](<https://www.amourdecuisine.fr/article-couscous-au-choux.html/couscous-au-chou-6>)
  7. add over a little butter so that it melts in the mixture, or then olive oil, or pure smen. 
  8. in a skillet, fry chicken legs in oil and smen, to give it a nice golden color. 
  9. pour it into a pot, add over the grated onion, half tomato, salt and black pepper. 
  10. cover with a little water and cook.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/couscous-au-chou-5.jpg) ](<https://www.amourdecuisine.fr/article-couscous-au-choux.html/couscous-au-chou-5>)
  11. at the end of cooking, you can roast the chicken pieces, in a little oil, for the nice color 
  12. during the presentation, sprinkle the couscous with the sauce, garnish with the chicken pieces ,. and some hard boiled eggs (optional) 



[ ![couscous with cabbage](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/couscous-au-chou.jpg) ](<https://www.amourdecuisine.fr/article-couscous-au-choux.html/couscous-au-chou>)
