---
title: ghribia-leblabi - houriat el matbakh
date: '2012-04-19'
categories:
- crème glacée, et sorbet

---
Ghribia hello everyone, here is the most delicious ghribia you can prepare, a chickpea ghribia a dry and sanded cake at the same time.When I went to Algeria, I took with me Smen, clarified butter, because I love the Algerian smen, I think it is a thousand times better than the smen from here that melts immediately and gives water, and suddenly it does not succeed too much cakes such as ghribia who likes that the smen stays creamy to give a nice moss, and the first thing I did was hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.6  (  1  ratings)  0 

#  Ghribia 

Hello everybody, 

here is the most delicious ghribia you can prepare, a chickpea ghribia a [ dry cake ](<https://www.amourdecuisine.fr/categorie-11814728.html>) and [ sand ](<https://www.amourdecuisine.fr/categorie-12344749.html>) at the same time.When I went to Algeria, I took with me **smen** , clarified butter, because I like the Algerian smen, I think it is a thousand times better than the smen from here that melts immediately and gives water, and suddenly it did not succeed too much cakes such as ghribia, who likes the smen remains creamy to give a nice mousse, and the first thing I did with it is the ghribia of [ Houriyat el matbakh ](<https://www.amourdecuisine.fr/categorie-11131465.html>) . 

Ingredients: 

  * 130 g of soft butter  (I put some smen) 
  * 80 g caster sugar 
  * 60 g of coconut  (not put) 
  * 50 g of cornstarch 
  * 1 eggs 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of yeast 
  * 1/4 cup of salt 
  * 170 gr of flour 
  * 80 g grilled chickpea flour 



Method of preparation 

  1. Sift white flour and chickpea flour with starch, salt and baking powder 
  2. whip smen and sugar with vanilla for a creamy mousse 
  3. Add the egg and continue whisking 
  4. Add the coconut and then sift dry ingredients 
  5. pick up to form the dough 
  6. form a boudin, use a pincer to decorate it well, 
  7. cut small squares, which you place on a floured tray in the oven 
  8. Bake in preheated oven at 180 ° C for 15-20 minutes depending on the type of oven 
  9. you can decorate with icing sugar 
  10. keep in a hermitic box. and enjoy with a good tea 



they are good the [ Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) . discover even more recipes from the [ Algerian cuisine ](<https://www.amourdecuisine.fr/article-la-cuisine-algerienne-en-photos-103600770.html>)

# 
