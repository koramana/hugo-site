---
title: blueberry chicken
date: '2012-03-07'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- Algerian dry cakes, petits fours
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/myrtilles-048_thumb1.jpg
---
![blueberries 048](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/myrtilles-048_thumb1.jpg)

##  blueberry chicken 

Hello everybody, 

Delicious roasted chicken legs cooked with onions and garlic and flavored with thyme and cinnamon. 

To top it off, a few scattered blueberries are added towards the end of the cooking to give a touch of pepper ... a sweet, tangy touch that adds a lot to these blueberry chicken legs.   


**blueberry chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/poulet-aux-myrtilles-062_thumb.jpg)

**Ingredients**

  * 4 chicken legs 
  * 1 bottle of fresh blueberries 
  * 1 big onion 
  * 2 cloves garlic 
  * a few sprigs of thyme 
  * 1 pinch of cinnamon powder 
  * 3 tablespoons of olive oil. 



**Realization steps**

  1. Preheat the oven to 180 ° C (thermostat 6). 
  2. clean the chickens, remove the skin if you do not want your dish to be too fat. 
  3. In a baking dish, arrange chicken, coarsely chopped onions, chopped garlic and sprigs of thyme. 
  4. Sprinkle with a little cinnamon, salt, pepper, sprinkle with oil, cover the dish with a sheet of light and bake 
  5. Flip the thighs after 30 min and refit for 20 min. 
  6. Arrange the blueberries all around the thighs and put back 10 min. 
  7. serve with gratin dauphinois, or with rice. 


