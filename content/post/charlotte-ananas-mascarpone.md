---
title: Charlotte pineapple mascarpone
date: '2015-07-19'
categories:
- bavarois, mousses, charlottes, agar agar recipe
tags:
- desserts
- creams
- Cakes
- mosses
- Chantilly
- Fruits
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/charlotte-ananas-mascarpone1.jpg
---
![charlotte-pineapple-mascarpone.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/charlotte-ananas-mascarpone1.jpg)

##  Charlotte pineapple mascarpone 

Hello everybody,   
Another one of my recipes classified in the dungeons, sometimes I make recipes, I take them in photographs, I do others, I take photos, when I send these photos on pc, I give the name of one of the recipes in the file, I publish then the recipe which take the name of the file, and I forget the others, and now, because I am in full cleaning of my pc, to empty all this stock of photos, I find myself with recipes, which I forgot completely. 

same story, between this [ charlotte ](<https://www.amourdecuisine.fr/categorie-12125855.html>) , and [ kiwis mascarpone verrines ](<https://www.amourdecuisine.fr/article-verrines-sucrees-kiwis-mascarpone-64934521.html>) , do you figure that I made the two recipes in the same day, but I forgot to publish that one. 

in any case I catch up and I post the recipe. 

![charlotte-pineapple-mascarpone-067-.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/charlotte-ananas-mascarpone-067-1.jpg)

**Charlotte pineapple mascarpone**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/charlotte-ananas-mascarpone-top1.jpg)

portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients** for a mini charlotte 15 cm in diameter 

  * thirty boudoirs 
  * 250 gr of mascarpone 
  * 90 gr of sugar 
  * 200 to 250 ml pineapple juice 
  * pineapple in box (I use the juice of this box) 



**Realization steps**

  1. whip the mascarpone 
  2. add the sugar and 110 ml pineapple juice 
  3. reserve aside 
  4. dip boudoirs one by one in pineapple juice 
  5. line your mold to charlotte with these boudoirs 
  6. pour the ⅓ of the mascarpone mixture over 
  7. cover it with a layer of pineapple 
  8. add another third of the mascarpone mixture 
  9. cover it with a layer of boudoirs dipped in pineapple juice 
  10. pour over the remaining mascarpone mixture   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/charlotte-ananas-mascarpone-11.jpg)
  11. and finally cover with another layer of boudoirs 
  12. pack well and cover your dessert with a film of food and place in the fridge for at least 2 hours of preferable, and leave it overnight, to have a delicious dessert to the good taste of pineapple. 
  13. I am writing this article, and I really want to go immediately prepare another charlotte. 



![charlotte-pineapple-mascarpone-050-.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/charlotte-ananas-mascarpone-050-1.jpg)

thank you for your visits and comments 

thanks to all those who subscribe to the article publication notification (click on newsletter to find this) 

bonne journée 
