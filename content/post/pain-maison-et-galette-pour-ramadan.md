---
title: Homemade bread and cake for Ramadan
date: '2014-06-13'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-a-la-farine-027_thumb11.jpg
---
![matloue with flour 027](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-a-la-farine-027_thumb11.jpg)

##  Homemade bread and cake for Ramadan 

I share with you today the Index of Homemade Bread and Cake for Ramadan, my recipes of homemade breads and cakes easy to make to accompany your dishes and salads during the holy month of Ramadan, and even just to accompany a fresh salad in a hot day ... 

I know that in a hot day, we do not even want to light the fire ... personally this is my case, I can not bear to light the stove, the tripod or the oven .... But because we like home-baked breads, such as cakes matloue or rakhssis, I organize myself to prepare them for happiness, so that maximum 10 am, my bread is ready ... 

In Algeria, my father especially loves bread khobz dar .... because the bread is very soft and he likes it to accompany his morning coffee, and sometimes, even to accompany some pieces of watermelons, lol ... So I always make this bread, of happiness, and to keep the bread like just out of the oven, cooling, I cut the bread in pieces and hop in a bag in the freezer, before serving, make out the pieces of bread to let defrost at temperature, and here is the bread, just out of the oven ...   
  
<table>  
<tr>  
<td>

[ ![matloue-a-la-flour-027_thumb11](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-a-la-farine-027_thumb11-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-a-la-farine-027_thumb11.jpg>)

[ matlou3 or matlou ](<https://www.amourdecuisine.fr/article-matloue-matlou3-matlou-pain-maison-au-tadjine.html>)


</td>  
<td>

[ ![khobz-dar-bread-home-without-kneading-003_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/khobz-dar-pain-maison-sans-petrissage-003_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/khobz-dar-pain-maison-sans-petrissage-003_thumb1.jpg>)

[ khobz dar without fluffing ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>)


</td>  
<td>

[ ![khobz-8_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/khobz-8_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/10/khobz-8_thumb1.jpg>) 
</td> </tr>  
<tr>  
<td>

[ ![Moroccan batbout or Moroccan homemade bread](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/batbout-marocain-pain-maison-marocain.CR2-001-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/batbout-marocain-pain-maison-marocain.CR2-001.jpg>) [ Moroccan batbout ](<https://www.amourdecuisine.fr/article-batbout-marocain.html> "Moroccan Batbout "بطبوط مغربي"") 
</td>  
<td>

[ ![Bread-for-hamburgers.CR2_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/pain-pour-hamburgers.CR2_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/pain-pour-hamburgers.CR2_thumb1.jpg>) [ homemade hamburger bread ](<https://www.amourdecuisine.fr/article-pain-humburger-maison.html>) 
</td>  
<td>



[ ![Bread-of-semolina-026.CR2_thumb2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-semoule-026.CR2_thumb2-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-semoule-026.CR2_thumb2.jpg>) [ semolina bread ](<https://www.amourdecuisine.fr/article-pain-de-semoule.html> "semolina bread")


</td> </tr>  
<tr>  
<td>

[ ![rakhsiss2_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/rakhsiss2_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/rakhsiss2_thumb1.jpg>) [ kesra rekhsis ](<https://www.amourdecuisine.fr/article-kesra-rakhsiss-galette-rakhsiss.html>) 
</td>  
<td>

[ ![Pit 111_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/pit-111_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/04/pit-111_thumb1.jpg>) [ oil cake, kesra mbessa ](<https://www.amourdecuisine.fr/article-galette-a-lhuile-kessra-mebessessa.html>) 
</td>  
<td>



[ ![matloue-a-la-flour-027_thumb11](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-a-la-farine-027_thumb11-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-a-la-farine-027_thumb11.jpg>) [ mullet with semolina and flour ](<https://www.amourdecuisine.fr/article-matloue-a-la-farine-et-la-semoule-un-delice.html>)


</td> </tr>  
<tr>  
<td>

[ ![pie bread](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/16494785739-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/16494785739.jpg>) [ pie bread or Turkish bread ](<https://www.amourdecuisine.fr/article-pain-pide-pain-turc.html>) 
</td>  
<td>

[ ![Lebanese bread, pita bread.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/Pain-libanais-pain-pita.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/Pain-libanais-pain-pita.CR2_.jpg>) [ pita bread or Lebanese bread ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html>) 
</td>  
<td>

[ ![focaccia-parmesan-and-rosemary-030.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/focaccia-parmesan-et-rosemary-030.CR2_1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/focaccia-parmesan-et-rosemary-030.CR2_1.jpg>) [ focaccia with parmesan and rosemary ](<https://www.amourdecuisine.fr/article-focaccia-au-parmesan-et-romarin-117804271.html>) 
</td> </tr>  
<tr>  
<td>

[ ![wafer-chives-2_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/galette-ciboulettes-2_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/galette-ciboulettes-2_thumb1.jpg>)

[ pancakes with onions, chilli and oregano ](<https://www.amourdecuisine.fr/article-galette-aux-oignons-piment-et-origan.html> "onion galette, chilli and oregano")


</td>  
<td>

[ ![tortilla-a-la-farine_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tortillas-a-la-farine_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tortillas-a-la-farine_thumb1.jpg>) [ flour tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html> "Flour tortillas") 
</td>  
<td>

[ ![Tourton Nantes](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tourton-nantais-1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tourton-nantais-1.jpg>) [ Tourton Nantes ](<https://www.amourdecuisine.fr/article-tourton-nantais-pain-sucre-de-nantes.html>) 
</td> </tr>  
<tr>  
<td>



[ ![khobz-eddar-to-3-grains_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/khobz-eddar-aux-3-grains_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/khobz-eddar-aux-3-grains_thumb1.jpg>) [ khobz eddar with 3 grains ](<https://www.amourdecuisine.fr/article-khobz-eddar-aux-3-grains.html>)


</td>  
<td>

[ ![focaccia-004_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/focaccia-004_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/focaccia-004_thumb1.jpg>) [ focaccia with onions, onion bread ](<https://www.amourdecuisine.fr/article-khobz-aux-oignons-pains-aux-oignons.html>) 
</td>  
<td>

[ ![bread in the olive-012_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/pain-aux-olives-012_thumb-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/pain-aux-olives-012_thumb.jpg>) [ olive bread ](<https://www.amourdecuisine.fr/article-pains-aux-olives-cuisine-algerienne.html>) 
</td> </tr>  
<tr>  
<td>

[ ![cheese dripping bread and mushroom 3.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pain-degoulinant-au-fromage-et-champignon-3.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/pain-degoulinant-au-fromage-et-champignon-3.CR2_.jpg>) [ dripping bread with cheese and mushroom ](<https://www.amourdecuisine.fr/article-pain-degoulinant-au-fromage-champignon.html>) 
</td>  
<td>

[ ![dripping bread with two cheeses](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/pain-degoulinant-au-deux-fromages-1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/pain-degoulinant-au-deux-fromages-1.jpg>) [ dripping bread with two cheeses ](<https://www.amourdecuisine.fr/article-pain-degoulinant-aux-deux-fromages.html>) 
</td>  
<td>

[ ![buns-to-the-meat-hachee_thumb11](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/buns-a-la-viande-hachee_thumb11-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/buns-a-la-viande-hachee_thumb11.jpg>) [ breads stuffed with minced meat ](<https://www.amourdecuisine.fr/article-recette-de-pains-farcis.html>) 
</td> </tr>  
<tr>  
<td>

[ ![Bread-crumb-of-homemade-021.CR2_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-mie-fait-maison-021.CR2_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-mie-fait-maison-021.CR2_thumb1.jpg>) [ home-made bread ](<https://www.amourdecuisine.fr/article-pain-de-mie-maison.html>) 
</td>  
<td>

[ ![bread-to-cheese-and-olive-040.CR2_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-au-fromage-et-olives-040.CR2_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-au-fromage-et-olives-040.CR2_thumb1.jpg>) [ cheese bread ](<https://www.amourdecuisine.fr/article-gateau-sale-pain-au-fromage.html>) 
</td>  
<td>

[ ![Bread-a-la-chive-081.CR2_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/pain-a-la-ciboulette-081.CR2_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/pain-a-la-ciboulette-081.CR2_thumb1.jpg>) chive brioche with chives 
</td> </tr> </table>

you can see here: [ Ramadan dishes ](<https://www.amourdecuisine.fr/article-plats-du-ramadan-2014.html>)

and here [ bourak and briks for ramadan ](<https://www.amourdecuisine.fr/article-recettes-speciales-ramadan-2014-les-boureks-et-bricks.html>)

other [ soups for ramadan ](<https://www.amourdecuisine.fr/article-menu-ramadan-2014-les-soupes.html>)

different  [ Ramadan salads ](<https://www.amourdecuisine.fr/article-menu-du-ramadan-2014-les-salades.html>)

full of [ desserts, juices and flans for Ramadan ](<https://www.amourdecuisine.fr/article-index-de-flan-desserts-jus-et-boisson-ramadan-2014.html>)

and for more recipes you can see my [ ramadan recipes 2014 ](<https://www.amourdecuisine.fr/article-tag/recettes-de-ramadan-2014>)   


**Homemade bread and cake for Ramadan 2014**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-a-la-farine-027_thumb11.jpg)

**Ingredients**

  * flour 
  * semolina 
  * baker's yeast 
  * baking powder 
  * salt 
  * sugar 
  * milk 



**Realization steps**

  1. use a bread machine. 
  2. you can knead by hand 
  3. I prefer to use the mess 


