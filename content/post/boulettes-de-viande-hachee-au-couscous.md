---
title: minced meatballs with couscous
date: '2017-03-16'
categories:
- appetizer, tapas, appetizer
- couscous
- cuisine algerienne
- Cuisine par pays
- idee, recette de fete, aperitif apero dinatoire
- ramadan recipe
- recipes of feculents
- recettes salées
tags:
- Picnic
- accompaniment
- Cocktail dinner
- Amuse bouche
- Aperitif
- Buffet
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/croquettes-de-viande-hach%C3%A9e-au-couscous-1.jpg
---
![minced meatballs with couscous 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/croquettes-de-viande-hach%C3%A9e-au-couscous-1.jpg)

##  minced meatballs with couscous 

Hello everybody, 

this minced meat couscous recipe will surely please you! it's a recipe from my friend Mina Minnano, that you must know since the time she posts her recipes with us on the blog. 

If you make couscous for a dinner, and the next day you have some cooked couscous left, without the sauce, why not make these meatballs with couscous? It's super easy to achieve and surely it's too good ... I'll wait the next time I'm doing couscous to make ... croquettes very rich in taste! 

You can see the video here: 

**minced meatballs with couscous**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/croquettes-de-viande-hach%C3%A9e-au-couscous.jpg)

**Ingredients**

  * 200 gr precooked couscous 
  * ½ chopped onion 
  * 1 nice amount of chopped parsley 
  * 100 gr of grated fromafe 
  * 1 egg 
  * 250 gr of minced meat 
  * salt, hrour, cumin, sweet paprika 

To coat: 
  * 1 beaten egg 
  * breadcrumbs 



**Realization steps**

  1. Mix the ingredients to have a homogeneous paste. 
  2. form dumplings of almost 25 to 30 gr 
  3. coat them in beaten egg and bread crumbs 
  4. cook in a bath of hot dien oil until the meatballs turn a beautiful golden color. 
  5. present in a bunch on a fresh salad. 


