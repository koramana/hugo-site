---
title: Financial marbled strawberry
date: '2014-04-06'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/financiers-marbres-aux-fraises-1.1.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/financiers-marbres-aux-fraises-1.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/financiers-marbres-aux-fraises-1.1.jpg>)

Hello everybody, 

Another beautiful recipe strawberry, which comes to us from the kitchen of Lunetoiles. Today she shares with us a recipe for strawberry marbled financiers, and what must be good. 

Already the financiers are a dessert very tasty and melting in the mouth, if you add a nice touch of strawberry coulis, it can only be an explosion of flavors. 

On the blog you will find a nice selection of financial recipes, so just click on this link: 

[ the financiers ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=financiers&sa=Rechercher&siteurl=www.amourdecuisine.fr%2F&ref=&ss=>)

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/financiers-marbres-aux-fraises-3.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/financiers-marbres-aux-fraises-3.1.jpg>)

so here we go with this nice recipe:   


**Financial marbled strawberry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/financiers-marbres-aux-fraises.1.jpg)

Recipe type:  Dessert  portions:  12  Prep time:  10 mins  cooking:  13 mins  total:  23 mins 

**Ingredients**

  * 150 g icing sugar 
  * 5 egg whites 
  * 2 grams of yeast 
  * 80 g of butter 
  * 60 gr of flour 
  * 120 g powdered ground almonds 

Strawberry sauce : 
  * 200 gr of strawberries 
  * 50 g of sugar 



**Realization steps**

  1. Mix the strawberries with the sugar, until liquid consistency, reserve. 
  2. Preheat the oven to 190 ° C 
  3. Melt the butter gently until you get a nutty butter. 
  4. Mix the icing sugar, almond powder, flour and yeast in a bowl. 
  5. Add the egg whites, then continue mixing by incorporating the nutty butter. 
  6. Pour into financial molds, otherwise use a muffin cup, butter the mold cavities well, or silicone molds. 
  7. With a teaspoon take a little grout and spread it over the financiers, then with a toothpick, or a knife, make a marbling. 
  8. Bake for 13 minutes at 190 ° C. 
  9. Watch for the cooking, if necessary, cook another 5 minutes. 
  10. Check the cooking using a toothpick inserted, the heart of a financial, it must come out dry and with a few crumbs. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/financiers-marbres-aux-fraises-2.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/financiers-marbres-aux-fraises-2.1.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
