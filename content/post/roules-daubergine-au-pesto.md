---
title: rolled eggplant with pesto
date: '2017-05-30'
categories:
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-033_thumb.jpg
---
[ ![eggplant rolls 033](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-033_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-033.jpg>)

##  rolled eggplant with pesto 

Hello everybody, 

present as [ appetizers ](<https://www.amourdecuisine.fr/recettes-salees/amuse-bouche-tapas-mise-en-bouche>) or as a small appetizer, here are delicious rolls of [ eggplant ](<https://www.amourdecuisine.fr/article-que-faire-avec-aubergines-102011214.html>) to rise with the delicious taste of [ basil pesto ](<https://www.amourdecuisine.fr/article-pesto-au-basilic-fait-maison.html> "basil pesto - homemade") , and sweet with melting mozzarella that flows with every bite.    


**rolled eggplant with pesto**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-028_thumb.jpg)

Recipe type:  appetizer  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 2 eggplants finely cut into strips 
  * 75 ml of olive oil 
  * 1 cloves of crushed garlic 
  * 60 ml of basil pesto 
  * 175 grams of mozzarella 
  * salt and pepper 
  * basil leaves for decoration. 



**Realization steps**

  1. dirty the eggplant, to remove the bitterness, and reduce some of its water. 
  2. rinse the eggplants well with the salt, and place them on sopalain paper. 
  3. fry in a little hot oil, and let drain well. 
  4. cover each eggplant leaf with pesto, then mozzarella, 
  5. and roll it on itself, then prick it with a toothpick. 
  6. place the eggplant rolls in a lightly oiled gratin dish, 
  7. cook in a preheated oven for 8 to 10 minutes at 180 degrees C until the mozzarella melts well. 
  8. present while decorating with chopped basil. 



[ ![2012-03-19 eggplant rolls](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/2012-03-19-aubergines-rolls_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/2012-03-19-aubergines-rolls_2.jpg>)
