---
title: old-fashioned apple cake
date: '2014-10-28'
categories:
- cakes and cakes
- sweet recipes
tags:
- Fruits
- Cakes
- desserts
- Four quarters
- Pastry
- Algerian cakes
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-aux-pommes-a-lancienne-1.jpg
---
[ ![old apple cake 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-aux-pommes-a-lancienne-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-aux-pommes-a-lancienne-1.jpg>)

##  Old-fashioned apple cake 

Hello everybody, 

Another delicious cake from Lunetoiles, after the [ Cake with apples ](<https://www.amourdecuisine.fr/article-cake-aux-pommes.html> "Cake with apples") , voice the old-fashioned apple cake recipe, a super cinnamon-flavored cake. A recipe super easy and fast to realize that will make the happiness of big and small. 

You will find your happiness on my blog with the [ apple recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=pommes&sa=Rechercher>) , and do not forget if you have realized one of my recipes, send me the pictures of your achievements on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

[ ![old apple cake 4](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-aux-pommes-a-lancienne-4-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-aux-pommes-a-lancienne-4-001.jpg>)   


**old-fashioned apple cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-aux-pommes-a-lancienne-5.jpg)

portions:  12  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 250g of flour 
  * 3 eggs 
  * 150g melted butter 
  * 1 vanilla pod 
  * 100g of sugar 
  * 80g of vergeoise / or brown sugar 
  * 1 tbsp. cinnamon powder 
  * salt 
  * ½ sachet of baking powder 
  * 3 apples 



**Realization steps**

  1. Preheat your oven to 180 ° C 
  2. Beat the eggs, vanilla seeds and sugar to obtain a sparkling mixture 
  3. Separately, put 80g of vergeoise or brown sugar with 1 tbsp. with cinnamon soup, and 4 tbsp. of melted butter (taken from 150 gr), mix to obtain a fairly smooth paste. Put aside 
  4. Add the flour and the yeast to the mixture of eggs and caster sugar, then add the melted butter and salt until you obtain a smooth paste. 
  5. Prepare the apples, peel them and cut into thin slices. 
  6. In the buttered and floured cake mold, place a pastry dough base, arrange the apple slices on top, cover with sugar / cinnamon / butter mixture, and cover with dough again, then start again with a row of apples and cinnamon. 
  7. Bake for 40 - 45 minutes. 
  8. Take the cake out of the oven, let it rest for several minutes on a cooling rack, and unmold. 



[ ![old-fashioned apple cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-aux-pommes-a-lancienne.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-aux-pommes-a-lancienne.jpg>)
