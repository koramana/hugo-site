---
title: Spaghetti with chicken / chicken spaghetti
date: '2011-01-08'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/spaguetti-au-poulet-2-2-003.jpg
---
![Spaghetti with chicken / chicken spaghetti](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/spaguetti-au-poulet-2-2-003.jpg)

##  Spaghetti with chicken / chicken spaghetti 

Hello everybody, 

Here is a recipe very very simple and easy Chicken spaghetti / chicken spaghetti, that the vast majority of you know, and may be his own method of preparing it, the recipe is not for you, hihihiih. 

she is for the friend of my husband, who came to us unexpectedly at the time, and to find that at dinner, he has to eat and to love, and as he is single, and lives alone, he to ask my husband the recipe. 

of course, my husband can not give him the recipe, otherwise it will be a disaster, so I told him I will put the recipe on the blog, and insha'Allah, he will succeed   


**Spaghetti with chicken / chicken spaghetti**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/spaguetti-au-poulet-2-2-003.jpg)

**Ingredients**

  * in300 gr of spaghetti 
  * 4 pieces of chicken 
  * 2 tablespoons tomato concentrate 
  * 1 onion 
  * 4 cloves of garlic 
  * 2 bay leaves 
  * salt, black pepper, paprika. 
  * 3 tablespoons of table oil (or virgin olive oil) 
  * grated gruyere for garnish 
  * a green pepper (optional) 



**Realization steps**

  1. Cook the pasta in a large volume of boiling salted water, with 2 tablespoons of oil. 
  2. in another pot, prepare your red sauce: 
  3. cut onion and garlic into very thin cube 
  4. fry them in a little oil 
  5. add the chicken pieces 
  6. add the tomato sauce, simmer a little while stirring 
  7. when onions are translucent, add salt and other spices, bay leaves 
  8. cover the chicken well with a little water (at most 1 liter of water and let it cook 
  9. when the spaghetti is cooked, drain, and rinse with a little cold water 
  10. remove the cooked chicken from the red sauce, it must be well reduced, and it remains almost 500 ml, or a little less 
  11. put spaghetti in this sauce, and put back on low heat, cook while simmering, so that the spaghetti do not stick to the bottom. 
  12. when the sauce is well reduced, your dish is ready 
  13. serve with a nice layer of grated cheese. 



by the way, I greet one of my readers, who did [ the Mhadjebs ](<https://www.amourdecuisine.fr/article-mhadjebs-algeriens-msemens-farcis-crepes-farcis-mahdjouba-62744653.html>) and who was very happy with his result, he left me a nice comment, I was told that it was a long time ago that he did not eat that, and that he was happy to try the recipe on my blog, and he was even happier to have succeeded. 

juste pour dire, que nos blogs ne sont pas que pour les femmes, et meme la recette la plus simple a nos yeux, est tres utiles pour d’autres. 
