---
title: semolina batbout rolls stuffed with minced meat and spinach
date: '2018-03-26'
categories:
- bakery
- Unclassified
- bread, traditional bread, cake
- pizzas / quiches / pies and sandwiches
- salty recipes
tags:
- inputs
- Boulange
- Bakery
- Ramadan
- accompaniment
- Algeria
- Morocco

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/03/pains-batbout-%C3%A0-la-semoule-farcis-%C3%A0-la-viande-hach%C3%A9e-et-%C3%A9pinards-1.jpg
---
![semolina batbout rolls stuffed with minced meat and spinach 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/pains-batbout-%C3%A0-la-semoule-farcis-%C3%A0-la-viande-hach%C3%A9e-et-%C3%A9pinards-1.jpg)

##  semolina batbout rolls stuffed with minced meat and spinach 

Hello everybody, 

Stuffed bread is always good, and always know how to make a good joke, and especially try to introduce the vegetables that children eat with difficulty, and that's what I did with these batbout breads at the semolina stuffed with minced meat and spinach, I accompanied a good chorba frik, and my children had eaten their bread, without leaving a crumb, and I am sure that it is the happiness of each mother! 

For the recipe, I made semolina batbouts, a super easy recipe, like stuffed matlouh, and of course the stuffing everyone's taste, and everyone he likes to put in his semolina breads. 

**semolina batbout rolls stuffed with minced meat and spinach**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/pains-batbout-%C3%A0-la-semoule-farcis-%C3%A0-la-viande-hach%C3%A9e-et-%C3%A9pinards.jpg)

**Ingredients** the batter to batbout: 

  * 500 gr of fine semolina 
  * 1 C. tablespoons sugar 
  * 1 C. baking yeast 
  * 1 C. salt 
  * 1 C. tablespoon milk powder 
  * 1 C. coffee baking powder 
  * 420 ml warm water to pick up the dough 

the joke: 
  * 200 gr of minced meat 
  * 150 gr of spinach 
  * 1 medium onion 
  * 2 cloves garlic 
  * 1 C. concentrated coffee 
  * salt, black pepper, garlic powder and coriander 
  * olive oil 



**Realization steps** We start by preparing the batter to batbout. You can do that by hand or in a mess, 

  1. mix the dry ingredients, without putting the salt in front of the yeast. 
  2. pick up the dough with warm water, to have a nice paton. 
  3. start the kneading now, introducing the water gently to the dough until you have a very nice dough very soft. 
  4. place the dough in a clean salad bowl, and let stand and double in size. 

now prepare the stuffing: 
  1. fry the chopped onion in a little oil until you have a nice golden color. 
  2. add the minced meat, and cook well. 
  3. then introduce spinach, spices and tomato paste. 
  4. When everything is cooked, remove from heat and let cool. 
  5. take the dough and degas it on a lightly floured space, and divide it into 4 balls. 
  6. take again each ball, spread a little to put the stuffing in and close it, and put to rest. 
  7. then pick up ball by ball, spread as finely as possible, brush the surface with a little oil, and cook on a skillet or crepe pan by placing it on the oiled side. 
  8. before turning the bread on the second side oil this face and turn. 
  9. when the bread turns a beautiful color, remove from the heat and it is ready for tasting. 



![semolina batbout rolls stuffed with minced meat and spinach 2](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/pains-batbout-%C3%A0-la-semoule-farcis-%C3%A0-la-viande-hach%C3%A9e-et-%C3%A9pinards-2.jpg)
