---
title: Brownies with peanuts
date: '2014-04-25'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/DPP_0003-1024x768.jpg
---
[ ![peanut brownie](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/DPP_0003-1024x768.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/01/DPP_0003.jpg>)

##  Brownies with peanuts 

Hello everybody, 

Want chocolate? Here is a recipe too good peanut brownies rich in chocolate, super easy, and above all ... too good. 

In truth I have prepared these brownies for another recipe, but I think I have to do it again, because the children have left nothing, so it's delicious, mellow, and chocolatéeeeeeeeeeeeeeeeeeeeee. so let's go for this delicious recipe:   


**Brownies with peanuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/DPP_0002-1024x838.jpg)

portions:  16  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 3 eggs 
  * 125 g caster sugar 
  * 60 g flour 
  * 250 g of dark chocolate pastry 
  * 150 g of butter 
  * 100 g of peanuts 



**Realization steps**

  1. preheat the oven to 180 ° C 
  2. whisk the eggs with the sugar and 
  3. incorporate the flour 
  4. Melt the chocolate with the butter in a bain-marie, while mixing. 
  5. add the melted chocolate to the preparation then the dried fruits (I preferred not to mix the dried fruits but to sprinkle them on the surface of the cake) 
  6. pour the mixture into the buttered and floured mold or lined with baking paper. 
  7. put in the oven for 25 to 30 minutes 
  8. let cool before turning out 


