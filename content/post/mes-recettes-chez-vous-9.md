---
title: my recipes at home 9
date: '2011-04-27'
categories:
- cuisine algerienne
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/04/cake-marbre1.jpg
---
![https://www.amourdecuisine.fr/wp-content/uploads/2011/04/cake-marbre1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/cake-marbre1.jpg)

Hello everyone, 

here, I'm posting the recipes of my faithful friends, who have tried my recipes, and whom I thank very much for having trusted my recipes, having tried them, and having wanted to share their essays with us 

Do not forget, too, if you have tried one of my recipes, send me the pictures on this email: 

[ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

your photos will be published in the next article, my recipes at home 

[ braid cakes ](<https://www.amourdecuisine.fr/article-la-tresse-61395654.html>) at Nina Amelia 

[ el yasmina ](<https://www.amourdecuisine.fr/article-el-yasmina-doublee-gateau-algerien-aux-amandes-67607039.html>) at Nina Amelia 

[ lmikhdate ](<https://www.amourdecuisine.fr/article-mkhedette-laaroussa-l-oreille-de-la-mariee-57147864.html>) at Nina Amelia 

[ Dress with coconut ](<https://www.amourdecuisine.fr/article-m-khabez-a-la-noix-de-coco-64789506.html>) at Existence destinee 

![](https://www.amourdecuisine.fr//import/http://4.bp.blogspot.com/-4Z9nwe2j_8A/TaOtbXduU3I/AAAAAAAABkw/FuEPdefNsNI/s400/)

[ Flan with coconut milk ](<https://www.amourdecuisine.fr/article-flan-au-lait-de-noix-de-coco-58241026.html>) in [ Meriem ](<http://cuisinedespigeonsvoyageurs.blogspot.com>)

[ Makrout el Koucha ](<https://www.amourdecuisine.fr/article-26001222.html>) at Choulot 

[ Focaccia rosemary / onion ](<https://www.amourdecuisine.fr/article-focaccia-au-romarin-et-oignon-62123009.html>) at Imen B A 

[ frangipane pie ](<https://www.amourdecuisine.fr/article-tarte-frangipane-poire-45915918.html>) with apples at Imene B A 

[ Donuts ](<https://www.amourdecuisine.fr/article-donuts-faciles-et-delicieux-68158130.html>) at Imene B A 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/les-roses-de-printemps-21.jpg)

[ el Warda ](<https://www.amourdecuisine.fr/article-el-warda-gateau-algerien-71563711.html>) in [ Lynda ](<http://deliceorientale.centerblog.net>)

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/709b5b2f1.jpg)

[ Stuffed chicken pancakes ](<https://www.amourdecuisine.fr/article-crepes-farcies-au-poulet-55641173.html>) in [ lynda ](<http://deliceorientale.centerblog.net>)

![](https://www.amourdecuisine.fr//import/http://storage.canalblog.com/02/88/637278/)

![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/375x500/3/75/79/67/beghrir/)

![](https://www.amourdecuisine.fr//import/http://idata.over-blog.com/4/30/74/66//)

[ the farts of none ](<https://www.amourdecuisine.fr/article-pets-de-none-69086545.html>) at Christelle's place 

[ el yasmina ](<https://www.amourdecuisine.fr/article-el-yasmina-doublee-gateau-algerien-aux-amandes-67607039.html>) at Najet 

[ halwat lambout ](<https://www.amourdecuisine.fr/article-halwat-lambout-70875141.html>) at Ouahiba 

[ bradj ](<https://www.amourdecuisine.fr/article-bradj-est-ce-le-printemps-69206724.html>) at Ouahiba 

![https://www.amourdecuisine.fr/wp-content/uploads/2011/04/100_12451.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/04/100_12451.jpg)
