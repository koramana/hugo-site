---
title: chocolate trifles
date: '2017-09-24'
categories:
- dessert, crumbles and bars
- sweet verrines
tags:
- Chocolate mousse
- verrines
- mosses
- desserts
- Ramadan
- Easy cooking
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/trifles-au-chocolat-1.jpg
---
[ ![chocolate trifles](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/trifles-au-chocolat-1.jpg) ](<https://www.amourdecuisine.fr/article-trifles-au-chocolat.html/trifles-au-chocolat-1>)

##  chocolate trifles 

Hello everybody, 

I'm sure you're always up and going for trifles, the dessert that always turns heads. 

So chocolate trifles, surely you join? I take again, again and again. These chocolate trifles all beautiful, all melting in the mouth and super well presented by our dear **Wamani Merou** . Except that for this trifle recipe, there is no layer of cakes, because these trifles are super rich in calories. Still, you can always put a thin layer of chocolate genoise, or brownies. 

**chocolate trifles**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/trifles-au-chocolat.jpg)

portions:  4  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * For the chocolate mousse: 
  * 120 gr of dark chocolate 
  * 4 tablespoons powdered sugar (if your chocolate is too bitter, add ½ teaspoon) 
  * 3 egg white 
  * 2 egg yolk 
  * whipped cream: 
  * 250 gr of fresh liquid cream 
  * 4 tablespoons caster sugar 



**Realization steps**

  1. melt the chocolate in a bain-marie 
  2. transfer it to another bowl and let cool a little 
  3. Beat the egg white with the sugar to get a shiny meringue 
  4. using a tablespoon, add the chocolate egg yolks while mixing, until it becomes homogeneous. 
  5. add the meringue slowly to this mixture, and in small amounts until you have a nice mousse 
  6. Whip the fresh cream with the sugar to make a nice whipped cream. 
  7. make the assembly of your verrines according to your taste. 



[ ![chocolate trifles](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/trifles-au-chocolat-2.jpg) ](<https://www.amourdecuisine.fr/article-trifles-au-chocolat.html/trifles-au-chocolat-2>)
