---
title: Banana bread-Cake with banana
date: '2014-04-05'
categories:
- cakes and cakes
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/banana-bread-1.1.jpg
---
[ ![Banana bread-Cake with banana](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/banana-bread-1.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/banana-bread-1.1.jpg>)

Hello everybody, 

You have two bananas that have walled too much for your taste, and you want to throw ??? stop, you do not need to do that anymore, because here is the recipe that will save you the bet, lol the "banana bread" I will not call that, banana bread, but rather banana cake, because is the texture of a cake that we have here. 

This recipe comes from our dear Lunetoiles, and the photos are sublime, I did not know which to choose to accompany the recipe .... For my part, I have two or three recipes a little different from that, and that I will post you with time. So if you want to follow the blog, do not forget to subscribe to my feedburner feed, and to be sure of the good registration and confirmation of the email you would receive on your box. 

**Banana bread-Cake with banana**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/banana-bread-2.1.jpg)

Cooked:  English  Recipe type:  Dessert  portions:  12  Prep time:  15 mins  cooking:  60 mins  total:  1 hour 15 mins 

**Ingredients**

  * 2 cup flour (300 g flour) 
  * 1 teaspoon of baking soda 
  * 1/4 teaspoon of salt 
  * 1/2 cup soft butter (125 g butter) 
  * 3/4 cup brown sugar (150 g brown sugar) 
  * 2 eggs, beaten 
  * 2 1/3 cup ripe bananas, mashed (475 g ripe bananas, mashed) 



**Realization steps**

  1. Preheat oven to 175 degrees C 
  2. Lightly grease a cake tin. 
  3. In a large bowl, combine the flour, baking soda and salt. 
  4. In another bowl, beat the cream butter together with the brown sugar. 
  5. Stir in the eggs and bananas until smooth. 
  6. Stir in the banana mixture in the flour mixture just enough to moisten. 
  7. Pour the dough into the prepared baking pan. 
  8. Bake in preheated oven for 60 to 65 minutes, until toothpick inserted in center comes out clean. 
  9. Let cool in the mold for 10 minutes, before unmolding on a rack. 
  10. You can sprinkle or serve with salted butter caramel sauce. 



[ ![Banana bread-Cake with banana](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/banana-bread.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/banana-bread.1.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
