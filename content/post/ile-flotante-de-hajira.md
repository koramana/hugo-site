---
title: Floating island of Hajira
date: '2011-01-31'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/ile-flotante1.jpg
---
![ile-flotante.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/ile-flotante1.jpg)

Hello everyone 

I'm in Setif, it's a hellish heat, but it's too much for me to walk this morning in the city where I grew up, I confess that I walked and looked for familiar faces, but I did not get the chance to find my friends. 

Finally, so I did not bring my computer with me, so I could not post my recipes, which gives me the opportunity to post your recipes. 

I post a recipe for Hajar, I already post one of my recipes that she had to try it [ right here ](<https://www.amourdecuisine.fr/article-mon-gateau-superpose-chez-hajar-53706359.html>) . 

today I'm posting you the floating island she did and she loved to share with you 

here are his ingredients: 

\- 400 ml of milk    
\- 3 eggs    
\- 90g of sugar    
\- 1 vanilla bean (I'm not put)    
for caramel: 

\- 60g of sugar 

\- 4 tbsp. soup of water    
THE ENGLISH CREAM: 

in a saucepan, put the milk to heat, beat the egg yolks with 60g of sugar until blanching, pour the hot milk little by little by mixing and then put the preparation back into the pan and wash over low heat, stirring then put in a salad bowl, and let cool.    
WHITES IN SNOW: 

whisk the whites into firm snow with the remaining sugar (30g) to cook them: microwave cooking. 

butter cups and sprinkle with sugar then fill them, cover with a film and cook for 20 seconds in the microwave, pour the custard in cups and put the snow white and warm caramel. 
