---
title: buns with tuna and chakchouka
date: '2017-02-15'
categories:
- boulange
- Bourek, brick, samoussa, chaussons
- idee, recette de fete, aperitif apero dinatoire
- pain, pain traditionnel, galette
tags:
- Healthy cuisine
- Ramadan
- Ramadan 2017
- Easy recipe
- Aperitif
- Economy Cuisine
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/petits-pains-au-thon-et-chakchouka.jpg
---
![buns with tuna and chakchouka](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/petits-pains-au-thon-et-chakchouka.jpg)

##  buns with tuna and chakchouka 

Hello everybody, 

Here are delicious delicious rolls with tuna and chakchouka that we like to accompany with a good soup, or chorba frik ... These tuna buns are just a delight and for the farce, it's up to you to play your imagination! 

On the other hand, I am a bit disappointed for my first loaves, which I could not let up, because it was noon, and like my husband, like the children, they were there to create their hungers, that I hurried to bake my bread without giving them time to get up (a rising time of almost 20 minutes for a much nicer mie. 

With the second and the third plateau the result was better, because the tuna buns and chakchouka had rested and lifted before cooking, my husband liked it so much that he took the rest to his friends at work, to taste it. But I assure you that usually these breads have a spongy crumb wish. 

By the way, it's the same dough I use in my [ tuna rolls ](<https://www.amourdecuisine.fr/article-les-roules-au-thon.html>) , that many of my readers have tried and succeeded many times. 

**buns with tuna and chakchouka**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/petits-pains-au-thon-et-chakchouka-2.jpg)

portions:  12  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** For 12 rolls: 

  * 3 cups of flour 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 tablespoon baker's yeast 
  * 1 teaspoon of baking powder 
  * 4 to 5 tablespoons of oil 

for the farce 
  * 1 onion 
  * 2 fresh tomatoes 
  * 1 shredded garlic clove 
  * 1 can of tuna 
  * salt and black pepper 
  * grated cheese 
  * bread sprouts 

for decoration: 
  * egg yolk + milk 
  * Nigella seeds. 



**Realization steps** prepare the pasta: 

  1. Mix all these ingredients and add enough lukewarm water until soft, soft and easy to work. 
  2. Knead a few minutes. Form the dough into a ball and let it rise under cover, protected from drafts until it has doubled in volume. 

prepare the stuffing: 
  1. fry the cubed onion in a little olive oil 
  2. add chopped garlic and pine nuts. 
  3. then add the tomato cube and season with salt, black pepper, and spices of your choice. 
  4. let reduce the sauce and keep aside to cool. 

prepare the rolls: 
  1. take again the paste which will have doubled of volume, degas it a little, and divide it in 12 equal parts. 
  2. place the dumplings formed on a floured space, and cover. 
  3. Spread each dumpling with an oval shaped baking roll. 
  4. put some stuffing on it, cover with some tuna and cheese. 
  5. weld the edges of the dough that you can wet slightly with a little water (it will stick more easily) 
  6. place the rolls in a tray lined with baking paper, cover and let stand 20 minutes 
  7. when you see that the loaves have developed well, brush with the yellow mixture of egg and milk, and decorate with a little seed of nigella. 



![buns with tuna and chakchouka 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/petits-pains-au-thon-et-chakchouka-1.jpg)
