---
title: shortbreads with chestnut cream
date: '2016-12-03'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
tags:
- Algerian cakes
- Algeria
- Cakes
- Cookies
- Aid
- biscuits
- Little butter

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/sabl%C3%A9s-%C3%A0-la-cr%C3%A8me-de-marrons.jpg
---
![sands-a-la-creme-de-Brown](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/sabl%C3%A9s-%C3%A0-la-cr%C3%A8me-de-marrons.jpg)

##  shortbreads with chestnut cream 

Hello everybody, 

it's been a while since I made the recipe for these shortbreads with chestnut cream! Super-sweet shortbread that we found too good with the delicious layer of chestnut cream and the thin layer of crunchy chocolate well colored. 

I really enjoyed myself and my daughter decorating these shortbreads with chestnut cream with chocolate and working with the chocolate transfer sheets, it was really fun to see the bright eyes of my daughter every time she removed the transfer sheets and saw these beautiful drawings on chocolate. 

I leave you in any case with the recipe of these pretty shortbreads with chestnut cream, and the list of the godmothers of this game since its beginning, as well as the name of our next godmother: 

**shortbreads with chestnut cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/sabl%C3%A9s-%C3%A0-la-cr%C3%A8me-de-marrons-1.jpg)

portions:  6  Prep time:  30 mins  cooking:  12 mins  total:  42 mins 

**Ingredients**

  * 220 g of butter, at room temperature 
  * 100 g icing sugar 
  * 2 egg yolks 
  * 300 g of flour 
  * 50 g cornflour 
  * 1 C. coffee vanilla extract 
  * pinch of salt 

decoration: 
  * Chestnut cream 
  * dark chocolate (50%) 
  * chocolate transfer sheet (optional) 



**Realization steps**

  1. Place the butter in a bowl and whip it to become creamy 
  2. Add the sugar, at once and whip again to have a nice airy cream. 
  3. Add the egg yolks, vanilla extract, and salt, whip again. 
  4. incorporate the cornflour delicately 
  5. pick up the dough with the flour, to have a nice smooth dough. 
  6. shape your cupcakes, according to the cookie cutter of your choice, I used a cookie cut square 4x4 cm 
  7. cook in preheated oven at 170 ° C for 15 to 20 minutes. 
  8. let cool then pick up each two pieces with the chestnut cream. 
  9. take it well before starting the chocolate icing. 
  10. Melt the chocolate in a bain marie, do not over cook the chocolate otherwise it will crystallize. 
  11. remove from the water bath, and stir to homogenize the temperature and melt the remaining chocolate that has not yet melted.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/d%C3%A9coration-des-sabl%C3%A9s.jpg)
  12. place the shortbread on a rack, place the rack on a clean tray, 
  13. cut the chocolate transfer sheets into squares the size of the shortbread. 
  14. Pour the melted chocolate over the biscuits (I use the spoon to cover the biscuits evenly on all sides. 
  15. I work piece by piece, every time I cover a piece, I quickly deposit the square of chocolate transfer sheet over (as long as the chocolate is hot, so that the patterns are permeated) 
  16. allow the chocolate to set and harden before removing the transfer sheet. 



![sands-a-la-creme-de-brown-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/sabl%C3%A9s-%C3%A0-la-cr%C3%A8me-de-marrons-2.jpg)

la liste des participantes: 
