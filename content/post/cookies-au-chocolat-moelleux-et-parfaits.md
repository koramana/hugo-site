---
title: fluffy and perfect chocolate cookies
date: '2016-10-06'
categories:
- Chocolate cake
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/cookies-aux-pepites-de-chocolat_thumb1.jpg
---
##  fluffy and perfect chocolate cookies 

Hello everybody, 

who does not dream of dipping a soft cookie in a glass of hot milk, and see these chocolate chips that begin to melt, on the top of the cookie, like a thin layer of Nutella ... and then in a moment of silence, we put this cookie in the mouth, and there we forget everything around us, and we continue to dream to eat another cookie. 

your dream come true with these delicious extra-smooth and perfect chocolate cookies as Lunetoiles, who prepared this recipe, says, and gives it a 5-star ... 

**fluffy and perfect chocolate cookies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/cookies-aux-pepites-de-chocolat_thumb1.jpg)

portions:  16  Prep time:  15 mins  cooking:  8 mins  total:  23 mins 

**Ingredients**

  * 375 g all-purpose flour 
  * ½ teaspoon baking powder 
  * A pinch of salt 
  * 180 g unsalted butter, melted 
  * 250 ml of brown sugar (weighing 220 gr) 
  * 30 ml of sugar (2 tablespoons) 
  * 1 egg 
  * 1 egg yolk 
  * 195 g coarsely chopped milk chocolate (half dark chocolate / half milk chocolate) 



**Realization steps**

  1. In a bowl, mix the flour, yeast and salt. Book. 
  2. In another bowl, mix the butter with the brown sugar and the sugar with the wooden spoon. 
  3. Add the eggs and mix until the mixture is homogeneous. 
  4. Stir in dry ingredients and chocolate. Cover and refrigerate 1 hour. 
  5. Place the rack in the center of the oven. Preheat the oven to 190 ° C. Line two or three baking sheets with parchment paper. 
  6. Using an ice cream scoop, shape each cookie with 45 ml (3 tablespoons) of dough. 
  7. Drop five to six balls of dough per plate, spacing them. 
  8. Bake 8 to 9 minutes or until cookies are lightly browned all around. 
  9. They will still be very soft in the center. Let cool completely on the plate. 



méthode de préparation: 
