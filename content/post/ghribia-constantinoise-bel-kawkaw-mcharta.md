---
title: ghribia constantinoise beautiful kawkaw mcharta
date: '2016-06-22'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya
tags:
- Ramadan
- Algeria
- Oriental pastry
- Ramadan 2016
- Eid
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-constantinoise-bel-kawkaw-mcharta-1.jpg
---
![ghribia constantinoise beautiful kawkaw mcharta 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-constantinoise-bel-kawkaw-mcharta-1.jpg)

##  ghribia constantinoise beautiful kawkaw mcharta 

Hello everybody, 

Back with this delight, the ghribia constantinoise or [ ghribia el warka ](<https://www.amourdecuisine.fr/article-ghribia-el-warka-gateau-algerien-2016.html>) but this time with another form. I remember a moment ago, there were pictures on facebook of a cake known as **m'cherta** which we enjoyed very much at Lunetoiles and me. There was the recipe on the photo that Lunetoiles had tried at the time but the result was not satisfactory. 

So when I saw the ghribia el warka recipe, I told Lunetoiles that she should try the same recipe with the shape of the **M'cherta** . That's what she did, and I must admit that the result is more than beautiful. In Morocco, the mcherta I saw on the internet is based on date paste, and there are two forms, this form that Lunetoiles has made today, and another form that I will try to share with you as soon as possible. as possible. 

At the request of my readers who want to know what it is like as a smen that I use in my kitchen: I use the smen Medina of Algeria: 

![smen Medina](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/smen-medina.jpg)

![ghribia constantinoise beautiful kawkaw mcharta 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-constantinoise-bel-kawkaw-mcharta-2.jpg)

**ghribia constantinoise beautiful kawkaw mcharta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-constantinoise-bel-kawkaw-mcharta-4.jpg)

portions:  60  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients** Dough : 

  * 3 measures of flour 
  * 1 measure of melted smen 
  * orange blossom water and water 
  * 1 pinch of salt 

Prank call : 
  * 3 measure of ground and roasted peanuts 
  * 1 measure icing sugar 
  * peanut oil or neutral oil 

more : 
  * 1 egg for shaping   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-constantinoise-bel-kawkaw-mcharta-3.jpg)



**Realization steps**

  1. Put the flour in the bowl of the kneader then the melted smen, the pinch of salt. 
  2. Activate the robot with the "K" mixer (the paddle) and mix on medium speed until a mixture like sand is obtained. 
  3. Pick up the dough with the orange blossom water and a little water. 
  4. Make a ball of dough and wrap it in cellophane paper to let it rest. 

Prepare the stuffing: 
  1. grind using a robot with blades (ideally a powerful chopper or electric coffee grinder) 
  2. grind roasted peanuts until dough and transfer to a large salad bowl.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/mcherta.jpg)

ghribia el kawkaw mcherta 

  3. Add the icing sugar and knead by adding oil until you obtain a non-sticky peanut paste that forms a ball. 
  4. Book. 
  5. Sprinkle the work plan with flour. 
  6. Cut the dough into two equal pieces, roll out each piece one after the other in a rectangle and then overlay the 2 rectangles by flouring well between the two above and below, and spread both at the same time, until you get two large rectangles, the dough is very fine. 
  7. Cut the edges to get two large rectangle. 
  8. This technique allows to spread the two pieces of dough at the same time, on the same thickness, very fine each and the same dimensions. 
  9. Roll the first rectangle of dough on itself and cover it to prevent the dough from drying out. 
  10. Take the peanut paste, roll it into several long rolls, the same length as the pastry rectangle. 
  11. Break an egg into a small bowl, using a brush to pass the egg on the edges, then put a peanut stuffing pudding, roll and put the egg for welding. Cut the dough and stick well by rolling the pudding. 
  12. Slice lozenges of the same size.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/mcherta-2.jpg)

ghribia 

  13. pre-drill each diamond, and make 3 cuts on the top. 
  14. take the piece of cake and press the sides between your fingers to give a shape defined as you see in the photo   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/mcherta-3.jpg)
  15. place the pieces of cake as you go in a baking tray 
  16. take the pasta rectangle spread and rolled again (step 9). Unroll it, and do the same operation until the dough is used up. 
  17. Bake at 160 ° C for about 15 minutes, the dough should remain white and should not color. 
  18. Allow to cool completely before storing the cakes in an airtight box. 



![ghribia constantinoise beautiful kawkaw mcharta](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-constantinoise-bel-kawkaw-mcharta.jpg)
