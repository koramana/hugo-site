---
title: seffa, sweet couscous at the Turkish halwa
date: '2018-02-09'
categories:
- couscous
- Algerian cuisine
- Moroccan cuisine
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/seffa-a-la-halwa-turc-2.jpg
---
[ ![seffa, sweet couscous at the Turkish halwa](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/seffa-a-la-halwa-turc-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/seffa-a-la-halwa-turc-1.jpg>)

##  seffa, sweet couscous at the Turkish halwa 

Hello everybody, 

Here is a dish very popular in the Algerian cuisine, the seffa, a sweet couscous garnished with scents according to the tastes and desires. 

Today, it is with my friend and reader: Ferdadou G, that we will taste this seffa with raisins and halwa Turkish, it lacks a good bowl of Lben "milk ribot" to eat with teeth, hihihihi. 

In any case, you can see the most simplified recipe of the [ raisins mesfouf ](<https://www.amourdecuisine.fr/article-couscous-sucre-seffa-mesfouf.html>) , then question of decoration, as I told you, it's up to you to choose and decline this dish according to your desires. 

**seffa at the Turkish halwa**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/seffa-a-la-halwa-turc.jpg)

portions:  4  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients**

  * 450 g of couscous close 
  * 1 pinch of salt 
  * 300 ml warm water 
  * 150 g raisins. 
  * 120 g of butter 
  * 1 tbsp sugar 

Decoration: 
  * good honeyed dates 
  * Butter 
  * Turkish halwa 
  * hard boiled eggs 



**Realization steps**

  1. put the grapes to swell in a little warm water. 
  2. prepare the couscous with steam. 
  3. and before putting the couscous to cook for the last time, first place the drained raisins with their water, cover and cook for 8 to 10 minutes by steaming. 
  4. remove the lid, and cover the raisins with couscous 
  5. let the couscous cook for another 15 minutes 
  6. pour everything into a large bowl, add the butter and stir in the mixture couscous and raisins. 
  7. add the sugar according to your taste 
  8. garnish with buttered dates, hard-boiled eggs and crumbled Turkish halwa. 
  9. you can present with a little honey, so that everyone can water the seffa according to his taste, 
  10. serve hot with lben or buttermilk 



[ ![seffa, sweet couscous at the Turkish halwa](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/seffa-a-la-halwa-turc-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/seffa-a-la-halwa-turc-1.jpg>)

and here is the video that explains how to cook couscous: 

{{< youtube n1A2FGkyjq8 >}} 
