---
title: crumble and strawberry muffins
date: '2017-10-14'
categories:
- Cupcakes, macaroons, and other pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/muffins-crumble-et-fraises.1.jpg
---
[ ![crumble and strawberry muffins](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/muffins-crumble-et-fraises.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/muffins-crumble-et-fraises.1.jpg>)

##  crumble and strawberry muffins 

Hello everybody, 

We always enjoy the strawberry season, and today it's crumble and strawberry muffins, a recipe shared with us Lunetoiles. 

These muffins are garnished with crispy brown sugar crumble and filled with strawberry sauce and frozen in the recipe. 

If you like crumble muffins, you also have a recipe for [ blueberry crumble muffins ](<https://www.amourdecuisine.fr/article-muffins-aux-myrtilles-recette-de-muffins-extra-moelleux.html> "blueberry muffins / extra muffin recipe") , and you can see as many [ muffin recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=muffin&sa=Rechercher&siteurl=www.amourdecuisine.fr%2F&ref=&ss=1202j347034j6>) on the blog. and as it's strawberry season, here's a variety of [ strawberry recipe ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=fraises&sa=Rechercher&siteurl=www.amourdecuisine.fr%2F&ref=&ss=1176j422726j7>) . 

So without delay, I'll give you the recipe for these delicious crumble muffins stuffed with strawberry coulis.   


**crumble and strawberry muffins**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/muffins-au-crumble-et-fourr%C3%A9s-au-coulis-de-fraises.jpg)

Recipe type:  Dessert  portions:  12  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** for muffins: 

  * 250g of flour 
  * 150 g of sugar 
  * 2 eggs 
  * 100 g half-salted butter 
  * 150 ml of milk 
  * ½ sachet of baking powder 

for strawberry coulis: 
  * 250 g strawberries 
  * 50 g caster sugar 

for the crumble: 
  * 25 gr of brown sugar 
  * 25 gr of half-salted butter 
  * 25 gr of flour 



**Realization steps** Prepare the strawberry coulis the day before: 

  1. In a blender, put the strawberries, the sugar. 
  2. Mix until you get a smooth grout, you can switch to Chinese to remove the pips. 
  3. Fill the cavities with a silicone mold such as a chocolate mold, for example.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC02977-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC02977.jpg>)
  4. Put in the freezer, for several hours. 

Preheat the oven to 180 ° C. 
  1. Prepare the crumble: quickly mix all the ingredients with your fingertips. 
  2. Melt the butter in a small saucepan, and let cool. 
  3. Sift together the dry ingredients: (flour, sugar, yeast). 
  4. In another bowl, beat eggs with butter and milk. 
  5. Mix the two preparations quickly, the mixture must remain lumpy.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC02973-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC02973.jpg>)
  6. Fill the molds halfway with this preparation. Add a piece of frozen strawberry coulis,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC02982-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC02982.jpg>)
  7. then cover with remaining dough. Sprinkle with crumble. 
  8. Bake for fifteen to twenty minutes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC02994-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC02994.jpg>)
  9. To enjoy warm, and before 2 days to keep a soft texture.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC03065-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/DSC03065.jpg>)



[ ![crumble and strawberry muffins](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/muffins-crumble-et-fraises-001.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/muffins-crumble-et-fraises-001.1.jpg>)

merci 
