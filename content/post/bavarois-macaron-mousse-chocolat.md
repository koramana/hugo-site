---
title: Bavarian macaroon / chocolate mousse
date: '2011-12-19'
categories:
- cuisine marocaine

---
macaroons, I miss all the time, so not to throw that, I try to do things with, today, after a nice missed macaroons with pistachios, I made myself a Bavarian to camouflage this failure . So here are my ingredients: sponge cake recipe here mousse à l'orange macaroon with pistachio mousse au Toblerone here. Mousse with orange: 1 zest of lemon 1 zest of orange 1 case of Orange juice 1 case of juice Lemon 1 case of gelatin 2 eggs white cheese 250 gr 100 gr of sugar 1 honey cube separate the yellows from the white of 'eggs, well & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.3  (  1  ratings)  0 

macaroons, I miss all the time, so not to throw that, I try to do things with, today, after a nice missed macaroons with pistachios, I made myself a Bavarian to camouflage this failure . 

So here are my ingredients: 

  * sponge cake recipe  [ right here  ](<https://www.amourdecuisine.fr/article-26736625.html>)
  * orange mousse 
  * pistachio macaroon 
  * Toblerone mousse  [ right here  ](<https://www.amourdecuisine.fr/article-26736625.html>) . 



Foam with orange: 

  * 1 zest of lemon 
  * 1 orange zest 
  * 1 case of Orange juice 
  * 1 case of Lemon juice 
  * 1 case of gelatin 
  * 2 eggs 
  * white cheese 250 gr 
  * 100 gr of sugar 
  * 1 honey cube 



separate the yolks from the egg whites, beat the egg yolks with the sugar, add the cottage cheese, then add the honey, orange / lemon zest. and the juice. 

boil 2 tablespoons water, add the gelatin, then add to the previous mixture. mix well. 

whisk the egg white into the snow, slowly add it to the already prepared mixture, and you have your citrus mousse. 

take the sponge cake, soak it with orange juice, pour over half of the citrus mousse, place your macaroons, cover with the rest of the mousse, place in the fridge until it catches. 

pour over the tablerone mousse, let still take, then garnish to your taste, for my par, I sprinkle the top with bitter cocoa, and garnished the middle with grated white chocolate. 

merci de votre visite, 
