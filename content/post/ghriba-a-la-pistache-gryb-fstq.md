---
title: pistachio ghriba غريبة فستق
date: '2012-01-15'
categories:
- boissons jus et cocktail sans alcool

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ghribiya-au-pistache-006_thumb.jpg
---
#  Ghribia with pistachios 

##  pistachio ghriba غريبة فستق 

Hello everyone, 

I forgot that I did not post the recipe for these delicious melange cakes (halawiyat), which are very similar to monticos, and shortbread cookies 

and hop for a copy / paste of her home 

for the ingredients: 

  * a glass and a half of smen, ghee (a glass of 250 ml) 
  * a glass of icing sugar 
  * \+ or - 3 ½ cups of flour (depending on the absorption of the flour) 
  * a glass of finely ground pistachio 
  * green dye 
  * a few drops of pistachio extract 


  1. Beat the smen and icing sugar with an electric mixer for at least 20 minutes until the mixture is sparkling (this is the secret of success). 
  2. Add a little green dye and pistachio extract. 
  3. add the pistachios reduced to powder 
  4. Stir the flour in a small amount and mix gently to collect the dough. 
  5. Add some flour if you see that the dough is sticking to your hands. 
  6. Preheat the oven to 160 degrees. 
  7. take a little of the dough, and put it in a mold Maamoul well flour 
  8. put in a lightly floured tray, continue until the dough has run out 
  9. Bake your ghribia for about 25 to 30 minutes until the cakes are lightly browned. 



Thank you for your feedback   


**pistachio ghriba غريبة فستق**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ghribiya-au-pistache-006_thumb.jpg)

portions:  30  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients**

  * a glass and a half of smen, ghee (a glass of 250 ml) 
  * a glass of icing sugar 
  * \+ or - 3 ½ cups of flour (depending on the absorption of the flour) 
  * a glass of finely ground pistachio 
  * green dye 
  * a few drops of pistachio extract 



**Realization steps**

  1. Beat the smen and icing sugar with an electric mixer for at least 20 minutes until the mixture is sparkling (this is the secret of success). 
  2. Add a little green dye and pistachio extract. 
  3. add the pistachios reduced to powder 
  4. Stir the flour in a small amount and mix gently to collect the dough. 
  5. Add some flour if you see that the dough is sticking to your hands. 
  6. Preheat the oven to 160 degrees. 
  7. take a little of the dough, and put it in a mold Maamoul well flour 
  8. put in a lightly floured tray, continue until the dough has run out 
  9. Bake your ghribia for about 25 to 30 minutes until the cakes are lightly browned. 


