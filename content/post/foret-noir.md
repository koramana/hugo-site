---
title: Black Forest
date: '2012-03-20'
categories:
- Café amour de cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/260043211.jpg
---
a delice is not it? This is what Louiza has prepared this time here is the recipe: genoise: 4 eggs; 120 g caster sugar; 1 pinch of salt; a pinch of baking powder; 60 g of cocoa; 60g of cantilly cream flour for rosettes: 1 dl of creme fraiche thick at 30% 2c a sugar cream to stuff and mask the cake: 350 cl cream 30% thick 30% rise in whipped cream mixed with 150ml cream pastry cooled cherries in syrup well drained while recovering 150ml of juice auqel one will have & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

  
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/260043211.jpg)

a delice is not it? 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/260043421.jpg)

This is what Louiza has prepared this time 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/260043601.jpg)

here is the recipe:   
Genoese: 

  * 4 eggs; 
  * 120 g caster sugar; 
  * 1 pinch of salt; 
  * a pinch of baking powder; 
  * 60 g of cocoa; 
  * 60g of flour 



cantilly cream for rosettes  : 

  * 1 dl of creme fraiche thick at 30% 
  * 2c with sugar 



cream to cram and hide the cake: 

  * 350 cl of creme fraiche thick at 30% rise in whipped cream 
  * mixed with 150ml of cold pastry cream 



cherries in well-drained syrup while recovering 150ml of juice to which one will have dissolved 40g of sugar   
chocolate shavings that will be taken with the aid of an economical knife   
8 cherries for rosettes   
Once the genoise is cooked the cut in 3 in the sense of the horizons, the syrup puncher   
put a layer of creme cherry cream, and so on, mask all the cake with the cream, put the chocolate chips on the cake and on the sides using a bag with cinnamon socket. 

make 16 rosettes and topped with cherries. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/260043721.jpg)

book in the fridge until serving 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/260043841.jpg)

bon Appetit. 
