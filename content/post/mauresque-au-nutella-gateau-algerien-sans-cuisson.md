---
title: Moorish Nutella - Algerian Cake without cooking
date: '2012-04-15'
categories:
- salades, verrines salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/maresque-au-nutella-300x226.jpg
---
Hello everybody, 

today I'm posting you the delicious recipe from one of my adorable readers, leila Saaidia. 

a very nice recipe [ Algerian cake without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) that she likes to share with you through my blog, 

you can also find 

the [ Moorish with coconut ](<https://www.amourdecuisine.fr/article-mauresque-a-la-noix-de-coco-gateau-algerien-101325541.html>)

the [ Moorish kefta, from lunetoiles ](<https://www.amourdecuisine.fr/article-les-mauresques-kefta-57994918.html>)

**Moorish Nutella - Algerian Cake without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/maresque-au-nutella-300x226.jpg)

portions:  40  Prep time:  20 mins  total:  20 mins 

**Ingredients** I thank you very much leila, and I pass you the ingredients: 

  * 2 measures of ground almonds 
  * 1 measure icing sugar 
  * white dye 
  * syrup (250 gr of sugar + ½ liter of water) 

for the stuffing: 
  * 1 pack of biscuits small butters 
  * 1 big handful of peanuts 
  * Nutella 



**Realization steps**

  1. mix ground almonds and sugar 
  2. add the white dye 
  3. pick up with the syrup 
  4. put the dice aside 
  5. for stuffing mix crushed cookies and peanuts 
  6. pick it up with Nutella 
  7. keep a ball of almond paste and a ball of Nutella stuffing for decoration 
  8. spread the almond paste and using a cookie cutter shape your cakes 
  9. do the same thing with Nutella stuffing, 
  10. take the cakes, spread a little Nutella, then place the Nutella stuffing on top and cover with another almond paste covered with a little Nutella (so that it sticks well) 
  11. using a flower-shaped cookie cutter, shape two flowers of each color 
  12. stick them with Nutella on your Moorish 
  13. decorate with silvery food flakes 



thank you very much leila for this very beautiful and surely very good recipe 

thank you to everyone for your comments 

thank you for all your visits 

merci a ceux qui s’abonnent a la newsletter 
