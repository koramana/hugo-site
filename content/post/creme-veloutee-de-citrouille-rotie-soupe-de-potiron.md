---
title: Cream of pumpkin soup with roasted pumpkin soup
date: '2017-10-24'
categories:
- Algerian cuisine
- diverse cuisine
- soups and velvets
tags:
- Soup
- Pumpkin soup
- Ginger
- Velvety
- Algeria
- la France
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/creme-de-citrouille-rotie-1.jpg
---
[ ![Cream of pumpkin soup with roasted pumpkin soup](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/creme-de-citrouille-rotie-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/creme-de-citrouille-rotie-1.jpg>)

##  Cream of pumpkin soup with roasted pumpkin soup 

Hello everybody, 

Velvety pumpkin cream rotie pumpkin soup: Yum! A roasted pumpkin cream ... There is not a fall or a winter that passes where I do not cook the pumpkin soup or pumpkin soup (pumpkin cream roasted cream soup), moreover you will find varieties on my blog: [ the surprise pumpkin soup ](<https://www.amourdecuisine.fr/article-soupe-de-potiron-surprise.html> "surprise pumpkin soup") , or the [ velvety pumpkin ](<https://www.amourdecuisine.fr/article-soupe-de-potiron.html> "pumpkin soup") , or even [ pumpkin jam ](<https://www.amourdecuisine.fr/article-confiture-de-citrouille.html> "Pumpkin jam") if you like. But I would like you to pass this recipe for soup or pumpkin cream rotie, I assure you having to pass the pieces of pumpkins in the oven, gives another dimension to the soup, a taste that you can not be never explored, a recipe that is worth testing. 

Then to you the recipe for this velvety pumpkin cream roasted pumpkin soup:   


**Cream of pumpkin soup rotie-pumpkin soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Creme-de-citrouille-rotie.jpg)

portions:  4-5  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** Ingredients for the soup: 

  * 1 large pumpkin peeled and cut into cubes 
  * 1 medium onion, peeled and cut into strips. 
  * 1 medium carrot, peeled and cut into pieces 
  * 1 sweet potato peeled and cut into cubes 
  * 2 tablespoons extra virgin olive oil 
  * 1 teaspoon of sea salt 
  * ¼ teaspoon freshly ground black pepper 
  * 1 C. butter 
  * 1 C. extra virgin olive oil 
  * 4 cloves of garlic, minced 
  * 1 tablespoon grated fresh ginger, 
  * 2 teaspoons ground coriander 
  * 1 teaspoon of cumin 
  * 1 liter of chicken broth (or a cube of knorr poultry diluted in 1 liter of water) 
  * A medium potato, cut into pieces about 1 ½ inches 
  * ½ cup coriander, packed 
  * 1 tablespoon of honey 
  * 150 ml of fresh cream 
  * 1 teaspoon of sea salt, more if necessary 
  * ½ teaspoon freshly ground black pepper 
  * roasted pine nuts to garnish 
  * fresh parsley to garnish 

Ingredients for Ginger Cream: 
  * 50 ml of fresh cream 
  * 3 tablespoons milk 
  * 1 teaspoon of honey 
  * 2 teaspoons grated fresh ginger 
  * a pinch of sea salt 
  * 1 pumpkin to use as a soup tureen (optional) 



**Realization steps**

  1. Preheat the oven to 200 ° C. Line a baking sheet with foil or baking paper. 
  2. Place the pumpkin cubes, onion and carrot and sweet potato pieces on the plate. 
  3. Sprinkle with olive oil and sprinkle with salt and pepper. Mix slightly. 
  4. roast for 20 minutes or until vegetables begin to brown, stirring occasionally.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/potiron-et-legumes-rotis-225x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/potiron-et-legumes-rotis-225x300.jpg>)
  5. While vegetables are roasting, melt butter and 1 tablespoon olive oil in a pot over medium heat. When butter is melted Add crushed garlic, grated ginger, coriander, and cumin. 
  6. Cook for 30 seconds, stirring continuously. 
  7. Add chicken broth, potato, coriander, honey and grilled vegetables. Bring to a boil, then lower the steady fire. Simmer uncovered for 15 to 20 minutes or until potatoes are tender. 
  8. puree with a blender foot until you have a silky cream. 
  9. Taste and season, if necessary with more sea salt and pepper, 
  10. add the fresh cream and mix again 
  11. Serve with a nice swirl of ginger cream (see below), grilled pine seeds and a little bit of parsley. 

For the ginger cream: 
  1. mix all the ingredients. and decorate the cream with a net and go back and forth with a toothpick for a nice pattern. 

prepare the pumpkin soup 
  1. To prepare the pumpkin for use as a soup bowl, cut the cap or lid using the top of the pumpkin. 
  2. Preheat the oven to 200 degrees C. Line a baking sheet with aluminum foil and lightly grease with olive oil. 
  3. Rub the pumpkin skin with olive oil, and even the lid, 
  4. place the pumpkin on the open part on the plate.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/citrouille-rotie-198x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/citrouille-rotie-198x300.jpg>)
  5. roast on the lower rack of the oven for 15 minutes (it cooks quickly) 
  6. after cooking and cooling, empty the inside leaving an edge of ½ cm. You can use a round cookie cutter for a round, smooth edge, if desired.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/potiron-roti-287x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/potiron-roti-287x300.jpg>)



[ ![Cream of pumpkin soup with roasted pumpkin soup](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/creme-de-potiron-roti.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/creme-de-potiron-roti.jpg>)
