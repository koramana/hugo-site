---
title: rolled pizza recipe with tuna
date: '2017-10-22'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/rayan-birthday-cake-090_thumb1.jpg
---
##  rolled pizza recipe with tuna 

Hello everybody, 

In search of a simple but good entry recipe, a delight you can adopt for [ ramadan recipes ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>) Here is a recipe for pizza tuna rolled is a delicacy, melting and mellow, in addition to very tasty with this tuna stuffing, try it, anyone who has tried this recipe really liked it. 

the dough is always that of my pizza, a recipe that I can not change or try another in its place, but if you are in a hurry, there is the [ magic dough in 10 minutes ](<https://www.amourdecuisine.fr/article-pate-magique-facile-en-10-minutes.html>) . The stuffing, I did with what I had on hand, anyway, this tuna rolled pizza recipe went in the blink of an eye, I did not even have time to make some pictures as I like for the blog !!!! 

![rayan birthday cake 090](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/rayan-birthday-cake-090_thumb1.jpg)

**rolled pizza recipe with tuna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/roules-au-thon3_thumb1.jpg)

**Ingredients** For the dough 

  * 3 cups of flour 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 tablespoon baker's yeast 
  * 1 teaspoon of baking powder 
  * 4 to 5 tablespoons of oil 

for the farce 
  * 1 onion 
  * 2 fresh tomatoes 
  * 1 can of tuna 
  * 1 handful of pitted olives 
  * salt, black pepper. 



**Realization steps**

  1. Mix all these ingredients and add enough lukewarm water until soft, soft and easy to work. 
  2. Knead a few minutes. Form the dough into a ball and let it rise under cover, protected from drafts until it has doubled in volume. 
  3. open the can of tuna, put its oil in a pan. 
  4. cut the onions, and return them in the oil, add after the cut tomato, and season according to your taste. 
  5. let cool.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/roules-au-thon2_thumb1.jpg)
  6. now that the dough has doubled in volume, degas it, and spread it into a large rectangle, decorate the onion stuffing, sprinkle over the tuna in pieces, and the olives cut into small slices. 
  7. roll the dough on itself, do not forget to brush the last side of the dough with a little egg so that it does not open. 
  8. cut pieces of 2 cm wide, it's a bit difficult, because the stuffing may come out, but you can arrange it 
  9. place your rolls on an oiled plate, and let rise a little. 
  10. before placing in the oven, brush the rolls with a little egg yolk. 
  11. cook in a hot oven. 
  12. and enjoy the delight 



**_ _ **
