---
title: chicken potato puree
date: '2009-08-05'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/08/209434041.jpg
---
![bonj4](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/209484281.gif)

Since I had a problem with my oven making a roast chicken, I got used to cooking my chicken this way, so you need: 

  * chicken legs (my husband only likes that, and I do not say no) 
  * 2 bay leaves 
  * salt, black pepper, thyme 
  * garlic crushed 
  * oil and water 



in a done everything, put the chicken leg wash, and add all the ingredients, cover and cook on low heat. 

![S7301842](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/209434041.jpg)

meanwhile, I prepare my potato for puree: 

  * potato peel, wash and cut into 

  * 2 cloves garlic 

  * salt and black pepper 




in a pot, put the potato with the ingredients and cook 

![S7301844](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/209436731.jpg)

at the end of the cooking of the potato, crush it with a whip beater, yes it is as if you whip eggs, but we whip the potato, please only if there is is not too much water otherwise you risk getting a boiling. 

add, warm milk, butter while whisking, 

![S7301861](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/209437611.jpg)

back to the thigh, yes they have to be cooked right now, and the water has almost completely evaporated, 

so I put the legs on a rack in the oven, just to brown them, because the expensive must remain juicy, and not become hard when it comes out of the oven. 

the sauce left by the thighs, it is delicious, if you add a little water, and a little butter, you put on the fire 2 min, you'll have a nice sauce with which we put the purree, and c is beautiful and delicious. 

![table1](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/209454171.jpg)

have a good meal 

![th_groetjes27](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/209485781.gif)
