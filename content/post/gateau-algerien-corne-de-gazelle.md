---
title: Algerian cake / gazelle horn
date: '2016-10-30'
categories:
- Algerian cakes- oriental- modern- fete aid
- Gateaux Secs algeriens, petits fours
- gourmandises orientales, patisseries orientales

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cornes-de-gazelle-aux-noisettes-2_thumb2.jpg
---
##  Algerian cake / gazelle horn 

Hello everybody, 

I still do not publish the [ Algerian cakes ](<https://www.amourdecuisine.fr/article-bonne-fete-d-aid-kbir-et-mes-gateaux-algeriens-88105516.html>) that I prepared during the Eid party spent ?? Today I wanted to put the ghribia recipe with peanut butter, and guess what? I only had one picture, hihihih yes only the one I took for the help ??? 

I do not know what happened, or where are the photos but I will redo this very very soon and you will do this time without missing a lot of photos. So, because it put me in all my states, especially that the recipe is already written! I preferred to put a recipe still ready for Lunetoiles so your ingredients! 

**Algerian cake / gazelle horn**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cornes-de-gazelle-aux-noisettes-2_thumb2.jpg)

Recipe type:  Algerian cake  portions:  10  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** Dough: 

  * 1kg of flour 
  * 400g of melted butter 
  * The first pinch of salt 
  * 2 eggs 
  * dozens of tablespoons of orange blossom 
  * a little vanilla if you like) 
  * water to pick up the dough 

Almond stuffing: 
  * 500g coarsely ground hazelnuts 
  * 240gr of powdered sugar 
  * 2 teaspoons cinnamon 
  * 50-60 gr of melted butter 
  * Orange Blossom 



**Realization steps**

  1. Begin by preparing the stuffing by mixing all the ingredients mentioned, add a little orange blossom while working the stuffing that must be easily held, 
  2. take small balls of 20 grams (I have weighed each time) 
  3. to lengthen them in pudding then in croissants, you can help yourself with a glass to form a pretty croissant, I advise you to form all the croissants before going to the shaping of the horns of gazelle 
  4. To prepare the dough, start by working the flour, salt and melted butter, 
  5. then add the rest of the ingredients. 
  6. Add water slowly to obtain a smooth paste that is easy to work with. 
  7. Take a little dough and roll it out finely, 
  8. arrange a croissant of almond and cover with dough by marrying the shape of the croissant by pressing slightly to make adhere the paste 
  9. use a notched wheel to cut off the surplus dough to form a beautiful gazelle horn, 
  10. with the help of a Nekkache, to form motives on the whole of the horn, I made them rather discrete but if you wish them more marked be careful not to tear the dough 
  11. Bake at 180 ° for 15 minutes, you must monitor the horns of gazelles that should not brown too much, leaving the oven to cool and ready! 


