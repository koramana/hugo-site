---
title: orange pudding
date: '2015-12-08'
categories:
- cakes and cakes
- sweet recipes
tags:
- Cakes
- To taste
- desserts
- Pastry
- Soft
- la France
- Baba

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pudding-a-lorange-1.jpg
---
[ ![pudding with orange 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pudding-a-lorange-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pudding-a-lorange-1.jpg>)

##  orange pudding 

Hello everybody, 

This is the orange season, so take advantage of this juicy and vitamin C-rich fruit that will boost your energy, and help your immune system to protect you from all these climate changes, and winter viruses. 

Today, orange is well invested in this delicious orange pudding, there is orange in it, orange on it, and orange juice to imbibe it. Thanks to my friend **Hichem Nihed** for this beautiful recipe she had from her aunt, and she shares with us through my blog, so prepare your pens to note the recipe. 

**orange pudding**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pudding-a-lorange.jpg)

portions:  8  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 5 eggs 
  * 100 gr of sugar 
  * 100 gr of flour 
  * 1 packet of dry yeast 
  * lemon zest 

Syrup to soak: 
  * 250 ml orange juice 
  * 200 gr of sugar 

decoration: 
  * orange supremes 



**Realization steps**

  1. Blanch eggs and sugar 
  2. add the lemon zest, and the flour and yeast mixture 
  3. butter and flour mini savarin mussels 
  4. gently pour some of the mixture into it, without overfilling the molds 
  5. place 2 to 3 supreme on top,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pudding-a-lorange-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pudding-a-lorange-2.jpg>)
  6. cook for 10 to 15 minutes, in a preheated oven at 180 degrees (the cooking time depends on the oven and the size of the molds) 
  7. prepare the syrup: cook the mixture orange juice and sugar for almost 10 minutes (if your oranges are very sweet, reduce the sugar) 
  8. let the syrup cool 
  9. soak your cakes with the syrup, and let it absorb well. 



[ ![pudding with orange 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pudding-a-lorange-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pudding-a-lorange-4.jpg>)
