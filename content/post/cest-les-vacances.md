---
title: It's the holidays
date: '2010-02-13'
categories:
- amuse bouche, tapas, mise en bouche
- cuisine algerienne
- ramadan recipe
- salads, salty verrines

---
Yes, it's the holidays, a week with the children, maybe a week of sleep (I do not believe in any case with my children) they like to wake up early).   
no matter, the main point I wanted to tell you my dear (es) that I risk being absent on your, and also not immediately answer the comments, and questions.   
maybe I have to wait for the night, when the children are in bed, and I will try to make a jump to your home, otherwise excuse my absence.   
I will still try to post recipes, if I find the time.   
because in truth, I want to enjoy a little with my children, especially my son who is always at school. here in England the child is at school from 8:45 until 15:15.   
So we must say that all this is time away from them.   
go, I leave you there, I have small projects to do with them (especially as work continues in the house)   
bisous. 
