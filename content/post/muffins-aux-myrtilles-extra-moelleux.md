---
title: extra-sweet blueberry muffins
date: '2018-01-30'
categories:
- Cupcakes, macaroons, and other pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/muffins-aux-myrtilles-038.CR2_.jpg
---
![extra-sweet blueberry muffins](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/muffins-aux-myrtilles-038.CR2_.jpg)

##  extra-sweet blueberry muffins 

Hello everybody, 

when I arrived in England, my husband always bought extra mellow blueberry muffins, and I do not tell you how beautiful and good they are with a huge hat .... 

I tried to make the recipe, and to succeed like the one he bought, well ... well. No recipe was successful, I missed about twenty recipes of blueberry muffins that I would have liked extra mellow, until I came across this special muffin book. I tried the recipe with a bit of "frustration of failure" especially when I saw that the dough was a bit thick, trying to put it in the boxes .... 

As a result, the most successful extra-smooth blueberry muffins I've ever tried, not only are they beautiful, they are super-soft, they do not flow or overflow when cooked ... I'm so pleased that I think again the recipe to all the sauces, and with many other fruits. 

![extra-sweet blueberry muffins](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/muffins-aux-myrtilles-030.CR2_.jpg)

**blueberry muffins / extra muffin recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/muffins-aux-myrtilles-021.CR2_.jpg)

Recipe type:  muffins  portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 1 for the crumble: 
  * 45 gr of flour 
  * 1 tablespoon brown sugar 
  * 1 tablespoon of sugar 
  * 2 tablespoons melted butter 
  * vanilla extract 
  * a pinch of salt 

for muffins: 
  * 125 gr of flour 
  * 1 cup of baking powder 
  * ½ cup of baking soda 
  * 1 pinch of salt 
  * 1 egg 
  * 60 ml of buttermilk (ribot milk) 
  * 50 gr of brown sugar 
  * ½ teaspoon of vanilla extract 
  * the zest of 1/2 lemon 
  * 55 gr of melted butter 
  * 50 gr of blueberries. 



**Realization steps**

  1. prepare the crumble, mix all the ingredients with the fork, and let rest on the side. 
  2. preheat the oven to 200 degrees C, and place the trays in the muffin cups 
  3. in a bowl, sift flour, baking powder, baking soda and salt. 
  4. in a salad bowl, beat the egg, add the sugar and mix. 
  5. incorporate buttermilk, lemon zest, and vanilla extract. 
  6. then add the melted butter. 
  7. introduce the flour mixture 3 times into the liquid mixture. 
  8. add the blueberries, just to scatter them in the muffin dough. 
  9. fill the boxes. 
  10. bake for 10 minutes at 200 degrees C, lower the temperature to 180 degrees, and cook another 15 minutes, you can check the cooking with a toothpick, if you think you need 5 minutes more, do it. 


