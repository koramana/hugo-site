---
title: Raisin cake
date: '2013-09-04'
categories:
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/123-0781.jpg
---
[ ![Raisin cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/123-0781.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/123-078.jpg>)

Hello everybody, 

In England, my stove is electric, and the settings of the oven make me see all the colors, sometimes my cakes cram down without cooking in it. 

so since I've been here, I'm enjoying redoing and trying the recipes that always have me, long live the gas, and cooking in the gas ovens. 

finally, I come back to the cake. 

[ ![Raisin cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/2010-08-11-1235_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/2010-08-11-1233_2.jpg>)

the ratiba recipe is as follows:    


**Raisin cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/123-0781.jpg)

portions:  12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 250 gr of butter 
  * 250 grams of sugar 
  * 5 eggs 
  * 350 gr of flour 
  * 1 packet of dry yeast 
  * vanilla (I put lemon juice 2 tbsp soup) 
  * 100 gr of dried figs (I put some raisins) 
  * the zest of 2 lemons 



**Realization steps**

  1. beat the butter and sugar until the mixture becomes foamy, in an electric mixer, not with a whip, it will not give the same effect. 
  2. add the eggs one by one, beating between each egg 
  3. add the lemon juice, as well as the zest. 
  4. make the raisins trickle into a little water when it drains well from the water and into the flour.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/2010-08-11-1233_21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/2010-08-11-1233_21.jpg>)
  5. add the flour and yeast to the butter and sugar mixture, then add the raisins 
  6. butter a cake mold bake at 200 ° C preheated for 5 minutes then lower the oven to 180 ° C 
  7. cook until the cake swells and takes a nice golden color 



[ ![Raisin cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/2010-08-11-1234_21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/2010-08-11-1234_2.jpg>)

let cool down before unmolding 

[ ![123 081](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/123-0811.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/123-081.jpg>)

and enjoy, it must be said that this cake and even better, the 2nd day, if you can resist this delight 

thank you for your visits, if you like my blog, and you want to be always up to date with my publications, subscribe to the newsletter, and especially do not forget to validate the registration on your email. 

thank you 
