---
title: Spiderman cake, rayan's birthday cake
date: '2013-03-17'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/rayan-birthday-cake-015_thumb-300x224.jpg
---
##  Spiderman cake, rayan's birthday cake 

Hello everybody, 

I lingered to post Rayan's birthday cake recipe, a simple but delicious cake, and despite the fact that there were two cakes for Rayan's birthday, I did not have time to take a cup of spiderman cake. 

in any case at the base of the cake I chose a cake frangipane peaches, a real delight, melting in the mouth, covered with a thin layer of almond paste colored according to the tastes of the prince of the party, my little Rayan, yes, last year it was [ Ben 10 ](<https://www.amourdecuisine.fr/article-ben-10-le-gateau-d-anniversaire-de-rayan-54691985.html>) and this time sipderman, I wonder what it's going to be next year bi idn Aallah. 

so here is the recipe:   


**Spiderman cake, rayan's birthday cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/rayan-birthday-cake-015_thumb-300x224.jpg)

portions:  12 +  Prep time:  60 mins  cooking:  25 mins  total:  1 hour 25 mins 

**Ingredients**

  * 250 gr of butter at room temperature 
  * 250 grams of sugar 
  * 1 cup of almond extract 
  * 200 gr of flour 
  * 120 grs ground almonds 
  * 2 tablespoons of baking powder 
  * 4 eggs 
  * peaches in pieces 
  * 3 tablespoons of apricot jam 

for decoration: 
  * white almond paste 
  * almond paste colored in red 
  * dark chocolate 



**Realization steps**

  1. preheat the oven to 180 degrees. 
  2. butter a 20 to 23 cm oven-proof baking pan or lined with baking paper, a removable base mold is the most appropriate 
  3. whip the butter and sugar, until you have a nice creamy consistency 
  4. add almond extract and eggs one by one while whisking constantly. 
  5. in another container, mix the flour, almonds and baking powder, then add this mixture gently to the butter cream. 
  6. stir in peaches cut into pieces in this mixture 
  7. pour this mixture into your mold, trying to give it a uniform surface with the back of a spoon 
  8. cook for about 35 minutes or until golden brown. 
  9. let cool in the mold for 10 minutes, then remove from the mold and let cool for another 10 minutes on a rack 
  10. heat the apricot jam with 2 teaspoons water, then garnish your cake with this jam 
  11. spread the white almond paste on a sheet of food film 
  12. cover the cake with. 
  13. spread out the red almond paste, and shape the spiderman face 
  14. shape eyes with white almond paste 
  15. Melt the chocolate, and place in a bag that you perforate, so that it makes it easier for you to trace the web of the spider 
  16. and here is my happy birthday again to my Rayan 



thank you for your visit, and your comments 

bonne journée . 
