---
title: Basboussa stuffed with coconut
date: '2017-07-24'
categories:
- cakes and cakes
- sweet recipes
tags:
- Ramadan
- Algerian cakes
- Algeria
- Semolina Cake
- Oriental pastry
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/Basboussa-fourr%C3%A9e-%C3%A0-la-noix-de-coco-001.jpg
---
[ ![Basboussa stuffed with coconut](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/Basboussa-fourr%C3%A9e-%C3%A0-la-noix-de-coco-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/Basboussa-fourr%C3%A9e-%C3%A0-la-noix-de-coco-001.jpg>)

##  Basboussa stuffed with coconut 

Hello everybody, 

Lunetoiles shared with me yesterday the photos of his **Basboussa stuffed with coconut** , and frankly I could only share his basque recipe with you as soon as possible. A nice version of the **Egyptian basboussa** , and it must be said that in Egypt, we enjoy very well with different basboussa, different ingredients, and different pranks! Yes, when I went to Egypt last year, I was surprised by all varieties of basboussa. I believe it is the number one Egyptian pastry par excellence. 

And that's what Lunetoiles tells us: 

In fact, I tried to reproduce a cake that my brother took home, a basboussa filled with coconut that came straight from Egypt! A cake that is made from semolina as one like a [ qalb ellouz ](<https://www.amourdecuisine.fr/article-qalb-el-louz-au-lben-babeurre-recette-inratable.html>) especially in the way of preparation, because it is a cake of semolina baked and then soaked with syrup. This basboussa was stuffed with coconut, raisins, almonds and whole hazelnuts. She was too good.   
My brother wanted us to try to do it at home. I tried the recipe inspired by a recipe that I saw on the forum ptit happiness pastry: a basboussa cream and that's what it gave. I do not know if I managed to get closer to the original, but in any case, it was still very good my basboussa! 

**Basboussa stuffed with coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/Basboussa-fourr%C3%A9e-%C3%A0-la-noix-de-coco-2.jpg)

portions:  12  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** Note: 1 cup = 250 ml For the basboussa: 

  * 2 cup of semolina 
  * 1 cup of oil 
  * ¼ cup of sugar 
  * 6 eggs 
  * ½ packet of baking powder 
  * 280 g whole liquid cream 

For garnish : 
  * 200 g grated coconut 
  * ¾ cup of sugar 
  * 1 can of unsweetened condensed milk 
  * 100 g blond raisins 
  * 55 g whole almonds 
  * 55 g whole hazelnuts 

For the syrup: 
  * 1 can of unsweetened condensed milk 
  * 35 cl of water 
  * 300 g of sugar 



**Realization steps**

  1. Preheat oven to 180 ° C - thermostat 6 
  2. In a salad bowl, mix the eggs with the sugar. 
  3. Add the oil and mix. 
  4. Add the cream and mix. 
  5. Then finish with the sifted yeast and the medium semolina, mix well until a homogeneous mixture. 
  6. Butter a rectangle dish in the oven.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/basboussa-pr%C3%A9paration.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/basboussa-pr%C3%A9paration.jpg>)
  7. Pour half of the mixture into the dish and bake for 10 minutes. 
  8. Meanwhile, prepare the filling: 
  9. In a salad bowl put the sugar, grated coconut, unsweetened condensed milk and mix well. 
  10. Then add raisins, almonds and hazelnuts, mix.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/farce-de-basboussa.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/farce-de-basboussa.jpg>)
  11. Pour the mixture over the cooked basboussa, spread evenly and smooth the surface. 
  12. Pour over the second half of the basque dough. 
  13. Bake until golden brown. 
  14. In the meantime, prepare the syrup in a saucepan, bring the water and sugar to the boil, then cook for about 10 minutes on a low heat. 
  15. As soon as the oven leaves, cut the basboussa square. 
  16. Then pour the syrup mixed with the condensed milk, on the latter still very hot.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/finalisation-basboussa.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/finalisation-basboussa.jpg>)
  17. Let cool, then put in the fridge for several hours or overnight. 
  18. The basboussa can be kept in the refrigerator because it contains concentrated milk, do not leave it too long at room temperature. 



[ ![Basboussa stuffed with coconut 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/Basboussa-fourr%C3%A9e-%C3%A0-la-noix-de-coco-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/Basboussa-fourr%C3%A9e-%C3%A0-la-noix-de-coco-1.jpg>)
