---
title: Panna cotta with apricots
date: '2016-07-10'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Panna-cotta-aux-abricots1.jpg
---
[ ![Panna cotta with apricots](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Panna-cotta-aux-abricots1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Panna-cotta-aux-abricots1.jpg>)

##  Panna cotta with apricots 

Hello everybody, 

Is this the apricot season at home already ??? Here in Bristol, I have not yet crossed this delicious fleshy fruit, rich in fiber and vitamin B ... 

To start the season in style, Lunetoiles shares with us apricot panna cotta recipe, very nice verrines to eat without moderation ... 

There are plenty of apricot-based recipes on the blog, and inshAllah full to come, so watch my newsletter alerts for more recipes, you can choose to sign up on feedburner to get the blog's daily newsletter, or then, choose the subscription on wisija, to have occasional or weekly newsletter ...   


**Panna cotta with apricots**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Panna-cotta-aux-abricots-001.jpg)

portions:  4  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 4 beautiful apricots, washed and cut in half 
  * 110 g caster sugar 
  * 35 cl of water 
  * 1 tbsp. tablespoon of gelatin powder (or 3 sheets of gelatin) 

For panna cotta: 
  * 60 cl of liquid cream 
  * 30 cl of milk 
  * 2 tbsp. powdered gelatin (or 6 sheets of gelatin) 
  * 150 g icing sugar 
  * 1 vanilla pod 



**Realization steps**

  1. Prepare the panna cotta: 
  2. Split the vanilla bean lengthwise and scrape the black seeds it contains over a saucepan. 
  3. Add the cream, the milk, the icing sugar, the pod and the gelatin. Put on the heat and bring to a boil over low heat for 3 minutes, stirring. 
  4. Remove the pod and let the panna cotta cool to room temperature. 
  5. Pour the panna cotta into the glasses and cool. 
  6. Meanwhile, pour 110 g caster sugar and 35 cl water into a saucepan, put over medium heat, stir when the syrup begins to simmer, then dip the apricots. 
  7. Let confuse 3 to 4 min, drain the apricots and pour the gelatin in the syrup, stir 2 min to dissolve it. Pour the syrup into a bowl, let cool. 
  8. When the panna cotta has taken (minimum 2 hours in the fridge), put the apricot mumps on the panna cotta. Cover with syrup and place 6 hours in the refrigerator. 



[ ![Panna cotta with apricots](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Panna-cotta-aux-abricots.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Panna-cotta-aux-abricots.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
