---
title: tamina from mouloud-mawlid nabawi
date: '2017-12-01'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries
tags:
- Algeria
- Algerian cakes
- Holidays
- Honey
- Easy cooking
- Fast Food
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/tamina-du-mawlid-nabawi-5.jpg
---
![tamina from mawlid nabawi 5](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/tamina-du-mawlid-nabawi-5.jpg)

##  tamina from mouloud-mawlid nabawi 

Hello everybody, 

At home, my children are used to [ bsissa or the Constantinian tamina ](<https://www.amourdecuisine.fr/article-bsissa-la-tamina-de-l-est-algerien.html>) but this morning they woke up to the smell of grilled semolina that scented the whole house. 

Yes, it is the tradition, the tamina of mouloud, or mawlid nabawi charif, as my mother did for us. In any case, it was not only the good smell, I made them a beautiful decoration too, hihihih. I did not do too much to tell the truth, because it was early, I had to grill the semolina, because I had not done that in advance ... 

**tamina from mouloud-mawlid nabawi**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/tamina-du-mawlid-nabawi.jpg)

portions:  4  Prep time:  30 mins  cooking:  5 mins  total:  35 mins 

**Ingredients**

  * 2 measures of grilled semolina 
  * 1 measure of honey 
  * 1 measure of butter 

decoration: 
  * cinnamon 
  * coconut 
  * pistachios 
  * pecan nuts 
  * flaked almonds 
  * colorful beads 
  * raisins 



**Realization steps**

  1. grill the semolina over a low heat in a pan until it turns a nice golden color. 
  2. let it cool a little 
  3. melt the butter and add the honey 
  4. pour over the semolina grilled in rain, while mixing (the dough must be neither too firm nor too light, so you have to go slowly with the semolina) 
  5. pour into a beautiful dish and decorate according to your taste 



![tamina from mouloud mawlid nabawi](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/tamina-du-mouloud-mawlid-nabawi.jpg)
