---
title: chicken with mustard
date: '2011-09-12'
categories:
- gateaux, et cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/poulet-a-la-moutarde-3_thumb.jpg
---
##  **chicken with mustard**

Hello everybody, 

a long day waiting for me, a lot of things to do, so no time to be in front of the pc, so I go back to this delicious recipe that I do very often when I have no time. 

baked chicken in the cooking bag, what easier than that? 

not even a 100-degree baking dish to make it as clean as ever, hihihihihi 

a very easy recipe is very good, you can use chicken without the skin, or with the skin, you can do the cooking, in special baking bags, as I did on the picture above, or then in a cooking pot with baking lid (if you want to do more dishes, hihihihi) 

Ingredients: 

  * chicken thighs 
  * 2 tbsp. mustard soup 
  * 2 tablespoons of olive oil 
  * salt, black pepper, ground coriander seeds 
  * 2 cloves of garlic 
  * 1/2 onion 
  * 2 bay leaves 
  * a little thyme 
  * 1 coffee of paprika 



in a baking pan, mix all your ingredients, after cleaning the chicken chunks, mix them in the mixture, let go for a little time, if you use the cooking bags add, 2 ca soup of water, if you use a casserole, add half a glass of water, then bake for 45 minutes to 1 hour, depending on your oven. 

at the end, remove the chicken pieces and roast a little to get a nice color. 

serve with fries, the sauce is a real delight, it can even be served with potato purée. 

thank you for all your comments, and good day 

kisses   


**chicken with mustard**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/poulet-a-la-moutarde-3_thumb.jpg)

**Ingredients**

  * chicken thighs 
  * 2 tbsp. mustard soup 
  * 2 tablespoons of olive oil 
  * salt, black pepper, ground coriander seeds 
  * 2 cloves of garlic 
  * ½ onion 
  * 2 bay leaves 
  * a little thyme 
  * 1 coffee of paprika 



**Realization steps**

  1. in a baking pan, mix all your ingredients, after cleaning the chicken chunks, mix them in the mixture, let go for a little time, if you use the cooking bags add, 2 ca soup of water, if you use a casserole, add half a glass of water, then bake for 45 minutes to 1 hour, depending on your oven. 
  2. at the end, remove the chicken pieces and roast a little to get a nice color. 
  3. serve with fries, the sauce is a real delight, it can even be served with potato purée. 


