---
title: easy homemade chocolate custard
date: '2013-07-26'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cheesecakes and tiramisus
- idea, party recipe, aperitif aperitif
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-au-chocolat-046.CR2_1.jpg
---
![easy homemade chocolate custard](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-au-chocolat-046.CR2_1.jpg)

##  easy homemade chocolate custard 

Hello everybody, 

my children love everything that contains chocolate, as everyone presumes, and as I prefer to offer homemade desserts, I prepared them a delicious chocolate flan ... 

it's a recipe that I often do, but I've never had the chance to take pictures, because my children do not even let them cool ... 

yes, they like the texture you see in these two photos ... I prefer the texture of the picture from below, because I like when the flan melts in the mouth gently, and we can detect the bitterness of dark chocolate, before the sweet taste of custard .... what do I like to take my time with each spoon ... 

I do not eat the chocolate custard just for dessert, I eat it to enjoy it .... in the true sense of the word.   


**easy homemade chocolate custard**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-au-chocolat-033.CR2_2.jpg)

**Ingredients**

  * 50 g of milk 
  * 120 g of whipping cream 
  * 120 g of 60% chocolate 
  * 12 g of butter 
  * 2 small eggs 



**Realization steps**

  1. Preheat the oven to 100 ° C. 
  2. Crush the chocolate in a bowl. 
  3. In a saucepan, boil the milk, butter and cream. 
  4. Pour this mixture on the chocolate. Mix gently, making sure not to make bubbles. 
  5. Beat the eggs lightly, then add them to the mixture. 
  6. Pour this mixture into ramekins. 
  7. cook in a bain-marie for 15 minutes 



![easy homemade chocolate custard](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/flan-au-chocolat-019.CR2_1.jpg)
