---
title: cheesecake oreo without cooking
date: '2016-11-21'
categories:
- cheesecakes et tiramisus
- recettes sucrees
tags:
- biscuits
- To taste
- desserts
- Algerian cakes
- Cakes
- Cake
- Breakfast

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/cheesecake-oreo-sans-cuisson_.jpg
---
![cheesecake-oreo-without-cuisson_](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/cheesecake-oreo-sans-cuisson_.jpg)

##  cheesecake oreo without cooking 

Hello everybody, 

By dint of doing quite often this cheesecake oreo without cooking, I did not pay attention that I had never published the recipe on my blog. 

This is the favorite cheesecake of my children because they like the Oreos, frankly me oréos trade are the last biscuit I think to buy! however, I really liked [ homemade oréos ](<https://www.amourdecuisine.fr/article-biscuit-oreo-fait-maison.html>) I did not realize it personally because it was Lunetoiles who posted the recipe that is on my blog, but I tasted the cookies at my sister's house and I was surprised when my sister told me that it is the recipe on my blog! 

When this cheesecake oreo without cooking, it's a breeze, I had kept a few pieces to garnish my cheesecake at the end, in the end I found the package empty (probably that my children had found the hiding place) . 

![cheesecake-oreo-in-kitchen-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/cheesecake-oreo-sans-cuisine-1.jpg)

**cheesecake oreo without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/cheesecake-oreo-sans-cuisson-2.jpg)

portions:  8  Prep time:  30 mins  total:  30 mins 

**Ingredients** for a 20 cm biscuit background 

  * 14 Oreos, finely ground in a food processor 
  * 2 tbsp. unsalted butter, melted 

for cheese mousse 
  * 125 gr of mascarpone 
  * 100 gr of Philadelphia cheese 
  * 2 tablespoons granulated sugar 
  * 220 ml of cold cream complete well cold 
  * 7 Oreos crushed roughly 

Decoration 
  * 7 Oreos reduced to powder 
  * Red fruits 



**Realization steps**

  1. crushed the oreos without the cream in the food processor. 
  2. introduce the melted butter, make a few strokes and remove. 
  3. tamp the mixture well in a mold with a removable bottom, or in a pastry circle. 
  4. put the time to prepare the cheese mousse. 
  5. whip together the mascarpone and Philadelphia and set aside. 
  6. whip the cold cream very cold with sugar cream chantilly, and gently introduce the mass of cheese without breaking it. 
  7. pour half of this cream on the biscuit base, sprinkle crushed Oesos on top and cover with the rest of the cream. 
  8. Decorate in the end with the oreos reduced in fine powder and some red fruits. 
  9. cool down for at least 3 hours. 


