---
title: top 5 favorite articles January 2012
date: '2012-02-02'
categories:
- Bourek, brick, samoussa, chaussons

---
Hello everybody, 

I come back to the update of this category, which I have not done for a while now, just for lack of time, 

and today, I publish you the top of the most consulted articles, in January 2012, 

the first place was at: 

[ SWISS ROLL MARBLE WITH LEMON CREAM ](<https://www.amourdecuisine.fr/article-37115347.html>)

the second place and delicious and good Algerian cake with which I inaugurate the new year 2012 

and it's : 

[ TRIANGLE SKANDRANIETTE WITH HAZELNUT ](<https://www.amourdecuisine.fr/article-skandraniette-en-triangle-aux-noisettes-gateau-algerien-2012-96576788.html>)

and for more Algerian cakes, you have: 

[ the index of Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) , (I try to update the ones I can not do) 

another delicious sweet, good and beautiful, and it is: 

[ WHITE CHOCOLATE NOUGAT ](<https://www.amourdecuisine.fr/article-nougat-au-chocolat-blanc-gateau-sans-cuisson-96636940.html>)

and it is not only the cats, you also love these delicious: 

[ SMALL BREADS WITH AX MEAT ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee-96050112.html>) E 

and the, the good ones: 

[ CROQUETTES OF CHOU-FLEUR ](<https://www.amourdecuisine.fr/article-croquettes-de-chou-fleur-94202829.html>) R 

merci a vous tout pour visites, et vos messages, 
