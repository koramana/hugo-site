---
title: shortbread cake dry cake
date: '2013-03-14'
categories:
- diverse cuisine
- Healthy cuisine
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/sables-pistaches-abricots-013_thumb.jpg
---
##  shortbread cake dry cake 

Hello everybody, 

Here again is a recipe for [ Petit fours ](<https://www.amourdecuisine.fr/categorie-11814728.html>) that I prepared during the year 2011, [ delicious shortbread cookies ](<https://www.amourdecuisine.fr/categorie-12344749.html>) and at once crunchy with the little chips of grilled pistachios in it. 

[ index of Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)

The recipe in Arabic: 

so without me lingering I pass you this very very good recipe: 

  * 250 gr of butter at room temperature 
  * 100 gr of maizena 
  * 125 gr of icing sugar 
  * 50 gr grilled pistachios and coarsely crushed 
  * 2 egg yolks 
  * vanilla, or pistachio aroma. 
  * 1 sachet of baking powder 
  * flour according to your need 



decoration: 

  * apricot jam 
  * crushed pistachios (almost 50 grs) 
  * coconut 


  1. in a container, beat the butter and sugar until the mixture doubles in volume and is very creamy. 
  2. add the egg yolks and continue beating. 
  3. stir in maizena mixed with baking powder and vanilla 
  4. dry the mixture with the flour, to see a beautiful soft and malleable dough. 
  5. spread the dough on a floured work surface, has a thickness of 4 mm 
  6. cut with a piece of your taste, for me it was a piece square 
  7. place your cakes delicately on a floured baking sheet 
  8. cook 10 to 15 minutes at 180 degrees in a preheated oven 
  9. Take out of the oven and let cool down 
  10. decorate the pieces with the hole at the top, with the help of a brush, with deliue jam with a little orange blossom water. 
  11. sprinkle top with a little coconut and pistachios 
  12. assemble each 2 pieces with jam 
  13. fill the cavity with jam, and leave for at least half a day 
  14. can be well preserved in a hermitic box (if you will resist) 



Thank you for your visits and your comments 

do not forget to subscribe to my newsletter if you want to be always up to date with my publication (the link for the newsletter is just up front in front of you, and do not forget you have to check at least one of the boxes, otherwise the registration is incorrect)   


**shortbread cake, dry cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/sables-pistaches-abricots-013_thumb.jpg)

portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 250 gr of butter at room temperature 
  * 100 gr of maizena 
  * 125 gr of icing sugar 
  * 50 gr grilled pistachios and coarsely crushed 
  * 2 egg yolks 
  * vanilla, or pistachio aroma. 
  * 1 sachet of baking powder 
  * flour according to your need 

decoration: 
  * apricot jam 
  * crushed pistachios (almost 50 grs) 
  * coconut 



**Realization steps**

  1. in a container, beat the butter and sugar until the mixture doubles in volume and is very creamy. 
  2. add the egg yolks and continue beating. 
  3. stir in maizena mixed with baking powder and vanilla 
  4. dry the mixture with the flour, to see a beautiful soft and malleable dough. 
  5. spread the dough on a floured work surface, has a thickness of 4 mm 
  6. cut with a piece of your taste, for me it was a piece square 
  7. place your cakes delicately on a floured baking sheet 
  8. cook 10 to 15 minutes at 180 degrees in a preheated oven 
  9. Take out of the oven and let cool down 
  10. decorate the pieces with the hole at the top, with the help of a brush, with deliue jam with a little orange blossom water. 
  11. sprinkle top with a little coconut and pistachios 
  12. assemble each 2 pieces with jam 
  13. fill the cavity with jam, and leave for at least half a day 
  14. can be well preserved in a hermitic box (if you will resist) 



bonne journee. 
