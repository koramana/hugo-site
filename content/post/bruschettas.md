---
title: Bruschetta
date: '2012-05-23'
categories:
- amuse bouche, tapas, mise en bouche
- Bourek, brick, samoussa, chaussons
- cuisine algerienne
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/bruschettas-tomate-ail-poivrons-thon-feta-olives-gruyere-11-300x288.jpg
---
##  Bruschetta 

Hello everybody, 

here are Bruschettas, another recipe that participates in [ "Express cooking" contest ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>) and it is again, Samia oum Serine, and it must be said that she has ideas, to make delights in less than 15 mins .... 

So this recipe Samia oum Serine sharing with you, I already write the recipe, and I think about going immediately to my dinner, children have already had a good cassoulet lentils. 

then we go, even if we do not have much time, to realize a delight of the italian cooking the Italian Bruschettas:   


**Bruschetta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/bruschettas-tomate-ail-poivrons-thon-feta-olives-gruyere-11-300x288.jpg)

**Ingredients**

  * slices of bread wholemeal (it is normal according to the tastes) 
  * tomato coulis (or fresh cream in base according to the tastes still) 
  * sliced ​​fresh tomatoes 
  * olives 
  * mushrooms 
  * tuna 
  * cheese (anyway: camembert, gruyere, goat cheese, mozzarella etc.) 
  * finely chopped peppers 
  * Provencal herbs (or thyme, oregano or others) 



**Realization steps**

  1. garnish the slice of bread with your ingredients 
  2. put in a few minutes, 
  3. decorate with a few drops of olive oil at the oven exit 
  4. enjoy with a good salad! 



Oum Sirine et Sayaline 
