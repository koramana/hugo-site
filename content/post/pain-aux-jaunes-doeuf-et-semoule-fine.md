---
title: bread with egg yolks and fine semolina
date: '2016-03-12'
categories:
- Mina el forn
- bread, traditional bread, cake
tags:
- Algeria
- To taste
- Bakery
- Breakfast
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-aux-jaunes-doeuf-et-semoule-fine-1.jpg
---
[ ![bread with egg yolks and fine semolina 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-aux-jaunes-doeuf-et-semoule-fine-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-aux-jaunes-doeuf-et-semoule-fine-1.jpg>)

##  bread with egg yolks and fine semolina 

Hello everybody, 

it's been a while since I shared a recipe for bread and baking on my blog, and it's good that Samia my friend liked to share this recipe with us. A delicious loaf, very tasty and very airy. Personally I want to prepare it right now, and I'm sure it will not be long. 

**bread with egg yolks and fine semolina**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-aux-jaunes-doeuf-et-semoule-fine-2.jpg)

portions:  6  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 850 gr of fine semolina 
  * 42 grams of fresh baker's yeast cubes (20 grams of instant dry yeast) 
  * warm water to pick up the dough 
  * 1 cup of sugar 
  * 4 egg yolks 
  * 1 tablespoon of salt 



**Realization steps**

  1. Deluge yeast and sugar in lukewarm water, and let rise 
  2. add to the semolina 
  3. mix the egg yolks with a little warm water and add them to the semolina 
  4. knead by hand and add water gradually until you have a very malleable paste. 
  5. knead for 10 minutes by hand while adding water gradually. 
  6. add the salt with the water and continue kneading for another 5min, you must have a paste that sticks a little to the hands (but not too much) 
  7. Divide the dough into three equal balls. 
  8. Let the balls rest for a few minutes. 
  9. take round baking dishes lined with baking paper 
  10. butter a little and place the balls in the dishes and flatten with a little soft butter in your hands. (the thickness neither too thin nor too thick) 
  11. cover the patties and let rise. 
  12. preheat your oven to 180 ° C and bake the bread first by lighting the oven from below, and once the bread is cooked and well inflated, light the oven from above so that the bread takes a nice golden color   
Good tasting 



[ ![bread with egg yolks and fine semolina 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-aux-jaunes-doeuf-et-semoule-fine-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pain-aux-jaunes-doeuf-et-semoule-fine-3.jpg>)
