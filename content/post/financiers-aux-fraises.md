---
title: Strawberry financiers
date: '2016-09-27'
categories:
- Cupcakes, macarons, et autres pâtisseries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/financiers-aux-fraises1.jpg
---
![Financial aux fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/financiers-aux-fraises1.jpg)

##  Strawberry financiers 

Hello everybody, 

Here is a good dessert to taste it, hum .... Strawberry financiers, or we can also call Strawberry Visitandines, a recipe prepared by the care of our Lunetoiles pastry fairy. 

If you are an amateur of [ strawberry recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=fraises&sa=Rechercher>) do not go further, you'll find lots of recipes on the blog, and if you like them [ financial revenue ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=financiers&sa=Rechercher>) , also jump on the link, it will really please you ...   


**Strawberry financiers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/DSC004431.jpg)

portions:  12  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 5 egg whites almost 175 g 
  * 125 g ground almonds 
  * 75 g flour 
  * 125 g butter or margarine melted and warmed 
  * 125 g caster sugar 
  * 1 packet of vanilla sugar 

Garnish: 
  * 12 strawberries 



**Realization steps**

  1. method of preparation: 
  2. Preheat oven to th.6 180 ° C 
  3. In a bowl beat half of the whites until stiff and add 50 g caster sugar. 
  4. In another bowl, beat the almonds, the rest of the caster sugar, the vanilla sugar, the flour, the other half of the egg whites (not beaten) and the butter. 
  5. Gently stir in the whites to the previous mixture. 
  6. Spread in muffin pans filled with paper boxes. 
  7. Add a strawberry in each visitandine. 
  8. Bake at 200 ° C for 15 to 20 minutes: the visitandines should be lightly browned   
(sting with a wooden toothpick to check the cooking) 



![Financial-to-strawberry-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/financiers-aux-fraises-11.jpg)
