---
title: Menu first day of ramadan / dish, soup, salad, brick, chhiwat ramadan
date: '2013-07-10'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg
---
[ ![tajine hlou lham lahlou](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg>)

##  Menu first day of ramadan / dish, soup, salad, brick, chhiwat ramadan 

####  Atbak Ramadan 2016 

Hello everybody, 

Ramadan moubarek Said, Insha Allah syam maqboul and dhanb maghfour. 

yes First day of Ramadan: I know that just like me, you are already thinking about what you will do on this first day of Ramadan ??? So for you help a little, I pass you this small menu, which may well guide you in your choices: 

For the beginning and the first day of Ramadan, the tradition is that we prepare a sweet dish, so that Ramadan is sweet, hihihihi and the month passes pleasantly smoothly, (photo above) 

[ lham l Ahlou, sweet dish, tajine lahlou. ](<https://www.amourdecuisine.fr/article-lham-l-ahlou-plat-sucree-ou-tajine-lahlou.html>)

if not:   
  
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajin-hlou-11-272x125.jpg) [ tagine with apples, without meat ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/chbah-essafra2-272x125.jpg) [ Algerian cuisine, Chbah essafra ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-chbah-essafra.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-de-nefles-aux-amandes-076.CR2_2-272x125.jpg) [ tajine of stuffed loquat with almonds ](<https://www.amourdecuisine.fr/article-tajine-de-nefles-farcies-aux-amandes-117638636.html>) 
</td> </tr> </table>

Who says sweet tajine, says dip his piece of bread in it to imbibe it with scented perfumed sauce, and for that, we will have to prepare: 

[ ![matloue-013_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-013_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/matloue-013_thumb1.jpg>)

[ galette Matlou3 - مطلوع Algerian Arabic bread ](<https://www.amourdecuisine.fr/article-matlou3-recette-de-matlou-khobz-tajine.html>)

as you can prepare:   
  
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/khobz-dar-pain-maison-sans-petrissage-003_thumb1-272x125.jpg) [ khobz dar without food / homemade bread ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/khobz-eddar-aux-3-grains_thumb1-272x125.jpg) [ Khobz eddar with 3 grains - easy homemade bread ](<https://www.amourdecuisine.fr/article-khobz-eddar-aux-3-grains-80811114.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/khobz-dar-4.CR2_-272x125.jpg) [ khobz dar ](<https://www.amourdecuisine.fr/article-khobz-dar-108029095.html>) 
</td> </tr> </table>

and now, the kings or the queen of the table: chorba, Jari, harira 

![chorba-frik-012.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-0122.jpg) [ chorba Frik ](<https://www.amourdecuisine.fr/article-chorba-frik.html>)

or:   
  
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-bida-1_thumb1-272x125.jpg) [ chorba bayda, white soup with vermicelli and chicken "شربة بيضاء" ](<https://www.amourdecuisine.fr/article-chorba-beida.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/hrira-aux-legumes-272x125.jpg) [ harira / soup for ramadan ](<https://www.amourdecuisine.fr/article-hrira-aux-legumes.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-vermicelle-007_thumb-272x125.jpg) [ Chorba vermicelli, soup for ramadan ](<https://www.amourdecuisine.fr/article-chorba-vermicelle-soupe-pour-le-ramadan.html>) 
</td> </tr> </table>

to accompany this beautiful soup, there is no more delicious than the bourek or the bricks: 

![bourek-to-apples-to-earth-and-fromage.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourek-aux-pommes-de-terre-et-fromage.CR2_1.jpg) [ bourek with potatoes and cheese / entree for ramadan ](<https://www.amourdecuisine.fr/article-bourek-aux-pommes-de-terre-et-fromage-entree-pour-ramadan.html>)

the bourek has the potato, economical, and delicious remains my favorite, but you can still realize:   
  
<table>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/bricks-a-la-viande-hachee-recettes-du-ramadan_thumb_1-272x125.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briouates-a-la-viande-hachee-1.jpg>)   
[ **bricks with chopped meat** ](<https://www.amourdecuisine.fr/article-bricks-a-la-viande-hachee-108968133.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bricks-au-thon-009.CR2_-272x125.jpg) **[ Tuna bricks ](<https://www.amourdecuisine.fr/article-bricks-au-thon-recette-facile-et-rapide-117943659.html>) ** 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bourak-au-poulet-a-la-bechamel_thumb-272x125.jpg) [ **Bourek chicken with béchamel** ](<https://www.amourdecuisine.fr/article-bourek-au-poulet-a-la-bechamel.html>) 
</td> </tr> </table>

and also for more freshness on your tables, we like salads: 

![salad-composed-at-rice-to-Ramadan-2013.CR2.jpg](http://amour-de-cuisine.com/wp-content/uploads/2009/08/salade-composee-au-riz-pour-ramadan-2013.CR2_.jpg) [ Mixed salad with rice and tuna / salad for ramadan 2013 ](<https://www.amourdecuisine.fr/article-salade-composee-au-riz-et-thon-salade-pour-ramadan-2013.html>)

or:   
  
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/salade-carottes-016_thumb1-272x125.jpg) **[ Carrot puree salad ](<https://www.amourdecuisine.fr/article-salade-puree-de-carottes.html>) ** 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/salade-008a_thumb1-272x125.jpg) **[ Varied salad with Boulgour ](<https://www.amourdecuisine.fr/article-taboule-libanais-salade-variee-au-boulgour.html>) ** 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/hmiss-salade-de-poivrons-entrees-speciales-ramadan_thumb1-272x125.jpg) hmiss, pepper salad ](<https://www.amourdecuisine.fr/article-hmiss-salade-de-poivrons.html>) 
</td> </tr> </table>

and as main course, I still like: 

[ minced meat recipe with olives, minced meat tagine with olives ](<https://www.amourdecuisine.fr/article-tajine-de-viande-hachee-aux-olives.html>)

or else:   
  
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/tajine-de-kefta-aux-oeufs-013.CR2_thumb1-272x125.jpg) [ **tagine of kefta with eggs** ](<https://www.amourdecuisine.fr/article-tajine-de-kefta-aux-oeufs.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/gratin-d-aubergines-026.CR2_1-272x125.jpg) ** [ **Eggplant Gratin On** ](<https://www.amourdecuisine.fr/article-recette-de-gratin-daubergines.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/foie-d-agneau-en-sauce-004_thumb2-272x125.jpg) [ **kebda mchermla** ](<https://www.amourdecuisine.fr/article-recette-kebda-mchermla.html>) 
</td> </tr> </table>

always a refreshing on the table and it will be for me in these days very hot: 

![Syrup de menthe.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/sirop-de-menthe1.jpg) [ mint syrup / refreshing drink ](<https://www.amourdecuisine.fr/article-sirop-a-la-menthe-boisson-rafraichissante.html>)  
  
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/citronnade-ou-limonade-au-citron-fait-maison-272x125.jpg) [ lemonade, or lemonade with lemon ](<https://www.amourdecuisine.fr/article-citronade-limonade-au-citron-faite-maison.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/Pina-colada-sans-alcool.CR2_-272x125.jpg) [ pina colada without alcohol ](<https://www.amourdecuisine.fr/article-pina-colada-boisson-fraiche-sans-alcool.html> "pina colada: cold drink without alcohol") 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/granit%C3%A9-a-la-grenadine.CR2_-272x125.jpg) [ grenadine granita ](<https://www.amourdecuisine.fr/article-granite-a-la-grenadine-ou-granite-au-sirop-de-grenadine.html> "grenadine granita or granita with grenadine syrup") 
</td> </tr> </table>

I have other home-made juice recipes to come, that I will try to post as soon as possible, and now we go to dessert, it's mostly for children, or for the evening: 

![creme-INVERTED-au-lait-focus-sucre.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-renversee-au-lait-concentre-sucre.CR2_1.jpg) [ sweet caramel cream ](<https://www.amourdecuisine.fr/article-creme-caramel-au-lait-concentre-sucre.html>)  
  
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-047.CR2_thumb2-272x125.jpg) [ Creamy crème brûlée ](<https://www.amourdecuisine.fr/article-creme-brulee-recette-creme-brulee-115598588.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/Mahalabia-aux-abricots-034_thumb-272x125.jpg) [ Mahalabia with apricots ](<https://www.amourdecuisine.fr/article-mahalabiya-aux-abricots-mahalabiya.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi.CR2_1-272x125.jpg) Mhalbi, cream of rice, algerian dessert 
</td> </tr> </table>

and for the evening with a good tea, the kings of Ramadan cakes: 

[ ![qalb elouz](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/qlb-005_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35768313.html>)

[ kalb el louz / ramadan dessert / قلب اللوز ](<https://www.amourdecuisine.fr/article-kalb-el-louz-dessert-de-ramadan-118938674.html>)

or:   
  
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/samsa-028_thumb1-272x125.jpg) [ Samsa, or triangles stuffed with almonds "صامصة" ](<https://www.amourdecuisine.fr/article-28064835.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/gateau-algerien-griwech-057_thumb2-272x125.jpg) [ Griwech - Algerian cakes ](<https://www.amourdecuisine.fr/article-griwech-gateau-algerien-en-video.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-0082-272x125.jpg) [ Basboussa ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html>) 
</td> </tr> </table>

or even easier: 

[ ![crispy donuts](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignets-au-fer-a-beignet.CR2_.jpg>)

[ donuts with donut iron ](<https://www.amourdecuisine.fr/article-beignets-au-fer-a-beignet-gateau-algerien.html> "donuts with donut iron, Algerian cake")

**Menu first day of ramadan / dish, soup, salad, brick, chhiwat ramadan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/viande-hachee-aux-olives.CR2_2-300x203.jpg)

**Ingredients**

  * soup 
  * bread 
  * salad 
  * bourek 
  * juice 



**Realization steps**

  1. 1st day menu of Ramadan 
  2. Author: love of cooking 
  3. Prep time: 60 mins 
  4. Cook time: 60 mins 
  5. Total time: 2 hours 
  6. Serves: 4 
  7. Ingredients 
  8. chorba 
  9. tajine hlou 
  10. bourek 
  11. salad 
  12. Instructions 
  13. it must be ready before the Maghreb! lol 


