---
title: chakchouka (tomato onions) بصل بطوماطيش
date: '2012-06-14'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

---
Hello everyone, Oum Yasmine, a faithful reader, who loves to participate in my contest "express cooking", and here is his contribution with a very delicious Algerian recipe that I personally like: tchektchouka .... too good ... and that's what she tells us: & nbsp; Hello soulef, I am a big fan of your blog, I am a day to day and I would like to participate with this recipe that I personally find it very practical, very economical and at the same time very original, it reminds me of my childhood and my kids love it, accompanied by a good matlou bread or kessra i & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.15  (  2  ratings)  0 

Hello everybody, 

Oum yasmine, a faithful reader, who loves to participate in my "express cooking" contest, and here is her contribution with a very delicious Algerian recipe that I personally like: tchekchukka .... too good… 

and that's what she tells us: 

Hello soulef, 

I am a big fan of your blog, I am a day to day and I would like to participate with this recipe that I personally find it very practical, very economical and at the same time very original, it reminds me of my childhood and my children love , accompanied by a good matlou bread or kessra I do not tell you, a treat. 

So I go to the recipe that requires only ingredients. 

  * 1kg of tomatoes. 
  * 2 medium onions. 
  * a hot pepper. 
  * ras elhanout. 
  * pepper and salt. 
  * a little oil equivalent of 3c to s. 



Preparation: 

  1. cut the onion into small dices, 
  2. put in a pan, add the oil and salt then put on the heat until the onion becomes translucent, 
  3. add diced tomatoes, ras elhanoute, black pepper, and hot pepper (do not cut the pepper must leave it whole not to have a hot preparation for people who do not like the spicy, if not it's ok if it breaks during cooking, the chilli is just to give a special taste to this chakchouka) 
  4. let it reduce to have a nice sauce, and here it is ready. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
