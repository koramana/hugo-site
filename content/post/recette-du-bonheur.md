---
title: Recipe of Happiness
date: '2008-02-14'
categories:
- basic pastry recipes
- sweet recipes
- pies and tarts

---
a recipe that I try and that I find delicious: 

** ![](https://www.amourdecuisine.fr/wp-content/uploads/2008/02/16pj31.gif)   
**

** 4 cups of love  **

2 loyalty cups 

3 cups of sweetness 

1 cup of friendship 

2 strands of hope 

2 leaves of tenderness 

4 fingers of confidence 

1 good dose of understanding 

Mix love and loyalty with trust. 

To mix tenderness, sweetness and understanding. 

Add friendship and hope. 

Sprinkle abundantly with gaiety. 

Warm up the sun. 

Servir généreusement tous les jours 
