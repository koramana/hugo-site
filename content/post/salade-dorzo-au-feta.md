---
title: Orzo salad with feta cheese
date: '2017-09-07'
categories:
- diverse cuisine
- ramadan recipe
- salads, salty verrines
tags:
- Ramadan 2016
- Ramadan
- Easy cooking
- Healthy cuisine
- Algeria
- Mixed salad
- Varied salad

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-dorzo-au-feta-7.jpg
---
![orzo salad with feta 7](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-dorzo-au-feta-7.jpg)

##  Orzo salad with feta cheese 

Hello everybody, 

I love salads decorated with feta, this delicious cheese with a slightly acid taste that adds a lot of flavor and sweetness at the same time. This orzo salad with Mediterranean feta is just a treat, and I have a little history with this salad. 

Like the majority of you, I subscribe to the newsletter of English, Russian, Italian blogs (it does not mean that I can read all these languages, thanks to the google translator who helps us sometimes to decipher between the lines, because the translation is not always famous), in any case I received a newsletter from one of these blogs with the recipe for this superb salad. I passed to my husband the list of ingredients that I missed to realize it (he forgot the tomato ... hihihi). Two days later, my friend **Lunetoiles** who by coincidence is subscribed to the same blog send me the link of the recipe to tell me if I like this kind of salad, I told him that the recipe is planned for tomorrow, hihihihi. 

In any case, this salad is too good, even my husband who is not a big fan of salads made of pasta to eat to finish his plate, big surprise for me! 

![orzo salad with feta 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-dorzo-au-feta-5.jpg)

**Orzo salad with feta cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-dorzo-au-feta-8.jpg)

portions:  4  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 1 glass of uncooked orzo 
  * ½ chickpea glass cooked or in box, rinsed, drained and skins removed 
  * 1 medium tomato, seeded and chopped (I did not have one, for the touch of color I added red pepper) 
  * ½ glass of feta crumbled 
  * ½ glass of cucumber without seeds cut in cubes (I did not put any) 
  * ⅓ glass of pitted Kalamata olives cut in slices 
  * ½ can of tuna in oil, well drained 
  * ½ red onion, diced 
  * 1 C. minced fresh mint 
  * 1 C. chopped fresh basil 
  * 2 tbsp. freshly chopped dill 
  * ¼ glass of extra virgin olive oil 
  * the juice of 1 lemon juice 
  * 1 C. vinegar 
  * 1 clove garlic minced 
  * 2 tbsp. coffee Dijon mustard 
  * ¼ c. salt 
  * A pinch of black pepper   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-dorzo.jpg)



**Realization steps**

  1. cook the orzo according to the package instructions. Drain and let cool. 
  2. place the cooled orzo in a large bowl and mix with chickpeas, feta, cucumber, tomato, olives, tuna, onion, mint and dill and season lightly with a pinch of salt. 
  3. In a small bowl, add olive oil, lemon juice, vinegar, garlic, Dijon mustard, salt and pepper. 
  4. whip it well to have a dressing sauce smooth and homogeneous. 
  5. Pour the vinaigrette over the salad and mix to coat well.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-dorzo-au-feta-4.jpg)
  6. Season with salt and pepper to taste. 
  7. Refrigerate until serving. 
  8. Before serving garnish with feta and chopped herbs, if desired. 



![orzo salad with feta 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/salade-dorzo-au-feta-1.jpg)
