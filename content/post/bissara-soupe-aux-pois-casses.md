---
title: bissara broken pea soup
date: '2013-11-21'
categories:
- couscous
- Algerian cuisine
- Moroccan cuisine
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/bissara_thumb.jpg
---
##  bissara broken pea soup 

Hello everybody, 

_bissara broken pea soup: Broken peas are dry vegetables rich in fiber, protein and minerals, as they have many nutritional qualities. and as we always like to have healthy and balanced food, a **velvety split peas** will only be welcome to enjoy this vegetable. _

_I had accompanied this veloute with[ indian samoussa ](<https://www.amourdecuisine.fr/article-samoussa-indienne-97307661.html>) _   


**Bissara**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/bissara_thumb.jpg)

Recipe type:  soup, vegetarian dish  portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 2 bowl of split peas 
  * 2 cloves garlic 
  * 1 onion 
  * a cube of chicken broth. 
  * 2 portions of cheese (laughing cow) 
  * salt, black pepper, cumin and paprika 
  * olive oil 



**Realization steps**

  1. cut the onions into large pieces 
  2. bring back in the oil, to have a beautiful color 
  3. add the split peas, garlic, salt and spices 
  4. leave a little brown 
  5. add the chicken broth 
  6. water with enough water (¾ a liter) 
  7. cover and cook until the peas become tender. 
  8. go to the blinder, with the two portions of cheese 
  9. enjoy it while it's hot, with a drizzle of olive oil, or if you like the acid taste, with a squeezed lemon juice. 


