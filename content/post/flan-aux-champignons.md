---
title: mushroom custard
date: '2016-05-01'
categories:
- appetizer, tapas, appetizer
- cuisine diverse
tags:
- Easy cooking
- eggs
- Healthy cuisine
- Vegetarian cuisine
- accompaniment
- Flan Of Vegetables
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Flan-aux-champignons-1.jpg
---
[ ![Flan with mushrooms 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Flan-aux-champignons-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Flan-aux-champignons-1.jpg>)

##  mushroom custard 

Hello everybody, 

I do not remember when I realized this flan to the chaspignons, but I remember that I was talking on messenger with my friend Mina minnano who showed me the pictures of her mushroom custard just out of the oven, and I admit that at the sight of these photos, I had only one desire to make the most quickly this hot entry. 

The next day without delay, I realized my **salted puddings** , which we tasted with a good fresh salad, and a good [ tomato soup ](<https://www.amourdecuisine.fr/article-soupe-de-tomate-parfaite-et-veloutee-114752334.html>) . I did not think my husband was going to eat it, but he liked it so much that he ate 2 flans, when to my little one who was at home, he ate his share and put himself on my lap to eat my yes, that's the way he does when he wants to eat my part, hihihihi. 

**mushroom custard**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Flan-aux-champignons-4.jpg)

**Ingredients** for the mushroom mixture: 

  * 300 gr mushrooms 
  * 1 onion 
  * 1 clove of garlic 
  * salt and pepper 
  * chopped parsley 

for the blank: 
  * 1 tablespoon oil 
  * 1 tablespoon of milk 
  * 1 teaspoon cornflour 
  * 100 gr of grated cheese (Gruyère) 
  * 4 eggs 



**Realization steps**

  1. fry the onion and mushrooms in a little oil, 
  2. add chopped garlic, parsley, salt and black pepper. 
  3. Preheat the oven to 180 ° C. 
  4. In a bowl, beat the eggs with milk and oil. 
  5. Stir in Gruyère and maizena. Salt, pepper lightly and mix well. 
  6. in the bottom of a small oven-baking tin, place the mushroom mixture 
  7. pour the egg mixture over it. 
  8. cook in the oven for 20 minutes. 



[ ![Mushroom flan 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Flan-aux-champignons-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Flan-aux-champignons-2.jpg>)
