---
title: Chocolate financiers
date: '2016-11-07'
categories:
- Cupcakes, macarons, et autres pâtisseries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/financiers-au-chocolat-et-noisettes.jpg
---
![Chocolate financiers](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/financiers-au-chocolat-et-noisettes.jpg)

##  Chocolate financiers 

Hello everybody, 

tasty chocolate and hazelnut financiers for your afternoon tea for the first comment, hihihihih 

**Chocolate financiers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/financiers-chocolat-noisettes-copie-1.jpg)

Recipe type:  cake cake  portions:  6  Prep time:  10 mins  cooking:  25 mins  total:  35 mins 

**Ingredients**

  * 6 egg whites 
  * 160 g of butter 
  * 160 g caster sugar 
  * 100g of flour 
  * 100 g of hazelnut powder 
  * 90 g of chocolate chips 



**Realization steps**

  1. Melt the butter in the microwave. 
  2. Mix the butter with the flour, the caster sugar and the hazelnut powder. 
  3. Beat the egg whites until they foam and incorporate them into the previous preparation. 
  4. Add the chocolate chips and mix again. 
  5. Pour the paste into the cavities of a silicone mold and bake in a preheated oven at 180 ° C for 20 to 25 minutes. 
  6. Let cool and then unmould. 



![Chocolate financiers](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/financiers-aux-noisettes-et-chocolat.jpg)
