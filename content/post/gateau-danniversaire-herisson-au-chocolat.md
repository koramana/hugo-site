---
title: chocolate hedgehog birthday cake
date: '2016-02-08'
categories:
- Chocolate cake
- birthday cake, party, celebrations
- sweet recipes
tags:
- Algerian cakes
- To taste
- fondant
- desserts
- Pastry
- creams
- Butter cream

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gateau-danniversaire-herisson-1a.jpg
---
[ ![hedgehog birthday cake 1a](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gateau-danniversaire-herisson-1a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gateau-danniversaire-herisson-1a.jpg>)

##  chocolate hedgehog birthday cake 

Hello everybody, 

Today was the birthday of my little chick. Already 2 years, time flies ... My children, the older ones were more excited than him to eat the birthday cake, that I did not plan to do at all, because we go on a trip tomorrow morning, and like Monday is the busiest day of my week, and besides the children on vacation, so the cake was not at all in my project of the week. 

But here it is, my children claim a cake, on the pretext that we do not make a lot of birthday at home, and it's been a long time since I made a cake, and that it is not always that my little one is going to be two years old, hihihihi. In any case, they harassed me so much with their: But why mom? it's unfair mom? it's your little angel anyway ??? hahahahah, that I ended up giving in ... thing that I do not do easily, especially not a Monday. In any case, they went to look on the internet, and they chose a hedgehog cake. Frankly I liked the idea because it was super simple to do. 

Between the preparation, the cooking and the decoration, it did not take me 2 hours. A base of [ mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-meskouta.html>) , a [ chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat.html>) black, Mikado, a little almond paste colored green, and here is a good cake as they wanted.   


**chocolate hedgehog birthday cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gateau-danniversaire-herisson-2a.jpg)

portions:  12  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** for the mouskoutchou: 

  * 6 eggs 
  * 1 glass of sugar (a glass of 240 ml) 
  * 1 cup of vanilla coffee 
  * the zest of a lemon 
  * the zest of an orange 
  * 2 tablespoons of milk 
  * 2 tablespoons of table oil 
  * 1 glass of flour + 2 tablespoons. 
  * 1 cup of baking powder 

for the [ chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat.html>) : 

  * 250 gr of chocolate (dark chocolate for me) 
  * 250 ml of liquid cream 
  * 70 gr of butter 

for decoration: 
  * Makido 
  * red and black candies 
  * almond paste 
  * green dye 



**Realization steps** prepare the muskoutchou (I did the cooking in a mold for the football cake, but you can use a Pyrex salad bowl to have a round cake on one side) 

  1. in the bowl of a robot, place the eggs, start whipping at high speed, and introduce the sugar, vanilla and the two zests. 
  2. continue to whip until the mixture is triple in volume. 
  3. add the milk, and the oil while continuing to whisk. 
  4. sift the flour, and add the yeast. 
  5. lower the speed of the robot and introduce the flour and yeast with the spoon, until incorporated all the mixture. 
  6. Pour the creamy and unctuous mixture into the baking pan lined with baking paper and bake for 40 to 45 minutes, depending on your oven, in a preheated oven at 180 degrees C. 
  7. remove from the oven and place the cake on a rack 
  8. remove the bump from the cake, it will serve to make the hedgehog head. 

prepare the ganache: 
  1. Roughly chop the chocolate and melt it in a bain-marie pan. 
  2. Meanwhile, in another saucepan, bring the liquid cream to a boil. 
  3. Pour the cream in three times over the melted chocolate. 
  4. Using a spatula, gently mix in small circles, starting from the center of the container to obtain a homogeneous mixture. 
  5. Let the mixture cool for 10 minutes, then add the cut butter. 
  6. Mix until your ganache is smooth and shiny. 

Mounting: 
  1. cut the cake into three equal parts 
  2. cover each layer of ganache and glue the pieces. 
  3. cut the part of the bump into a rectangle to give the shape of a head. 
  4. stick the head to one side, and cover all the cake with the ganache. 
  5. drop the eyes and nose. 
  6. have a toothpick, make strokes on the gananche throughout the body of the hedgehog. 
  7. poke the makidos in the rounded part. 
  8. to give the effect of the grass, color the almond paste in green and lay heaps to cover the cake stand 
  9. poke the makidos to give the effect of the hedgehog's spades. 



[ ![hedgehog birthday cake 3a](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gateau-danniversaire-herisson-3a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/gateau-danniversaire-herisson-3a.jpg>)
