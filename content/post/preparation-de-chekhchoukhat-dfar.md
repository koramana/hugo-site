---
title: preparation of chekhchoukhat dfar
date: '2016-04-18'
categories:
- Algerian cuisine
tags:
- Based
- Semolina
- Boulange
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Pr%C3%A9paration-de-chekhchoukhat-dfar.jpg
---
[ ![Preparation of chekhchoukhat dfar](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Pr%C3%A9paration-de-chekhchoukhat-dfar.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/Pr%C3%A9paration-de-chekhchoukhat-dfar.jpg>)

##  preparation of chekhchoukhat dfar 

Hello everybody, 

Here is a recipe that has rocked my childhood. It is not as famous as couscous rolled, chekhchoukhat dfar is a recipe of the East Algerian, which does not require a great knowledge nor technique to realize it, different from the couscous rolled which requires by cons, of the material (between wide terrine: wooden gassaa, different sieves with different mesh) and a lot of time to have a nice quantity. 

To prepare Chkhchukhat dfar, we just need a small terrine to knead and prepare the patties, cook them just a little, not too much on a tajine cast iron, then and that's where the difficulty begins, crumble the pancakes to the hand, ie between the thumb and forefinger (I will use a lobster in this operation), after it is out of the question to hold a pen, or chatting on facebook with them girlfriends. 

When I was young, I remember that every weekend between my two sisters and my mother, we prepared 8 cakes to crumble them while watching TV. Yes at the time, there was no internet, X-box or small game consoles that eat us our free time (and after we come to claim I have no time, I have no time, hihihihih ). After a month we are sure to have a good amount of chekhchoukhat dfar that can feed more than 50 people. 

I usually buy it ready in Algeria from a woman who did it very well, but this year I did not have the chance to visit this woman to have my reserve, and I do not buy the one of trade, as long as I do not know how it was prepared. So when I recently wanted to taste the delicious dish of the Chekhchoukha Constantine, I decided to make and prepare [ chekhchoukhat dfar ](<https://www.amourdecuisine.fr/article-chekhchoukha-constantinoise-chakhchoukha-de-constantine.html>) myself (maybe you have noticed my absence from your blogs, hihihihih, now you know the reason).   


**preparation of chekhchoukhat dfar**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chekhchoukha-et-cake-au-mascarpone-014-1.jpg)

portions:  6  Prep time:  15 mins  cooking:  3 mins  total:  18 mins 

**Ingredients**

  * 500 gr of medium semolina 
  * ½ teaspoon of salt (according to taste, because we do not forget that the cooking sauce will contain salt too, so be careful at this point) 
  * water to pick up the dough 



**Realization steps**

  1. in a bowl, place the semolina and the salt 
  2. add the water slowly and knead, the dough must be malleable but not too sticky to the hands. Kneading well the dough can have a better result. 
  3. turn the dough into two balls and leave under a clean cloth. 
  4. Pre-heat a cast iron, cast iron tajine, or crepe pan, you can also use a pan, but in what make smaller patties. 
  5. spread out the first dumpling as finely as possible. 
  6. cook on the tadjine on both sides, it is necessary to cook just the time that the cake takes a whitish color, if you cook too much, the cake will be hard, and difficult to crumble. 
  7. after cooking cut the cake in 4 quarters, place in a clean cloth and directly in a plastic bag, the time to cook the rest of the patties. 
  8. crumple each quarter into small squares of almost 6 by 6 mm. 
  9. place the crumbs on a clean cloth in one layer, and let dry well 
  10. When the small squares of dough are dry, place them in an airtight box. 



[ ![chekhchoukhat dfar](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chekhchoukhat-dfar.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chekhchoukhat-dfar.jpg>)
