---
title: Charlotte revisited with apricot and almond mousse
date: '2012-12-03'
categories:
- gateaux algeriens au miel

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bavarois-charlotte.jpg
---
![charlotte Bavarian style.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bavarois-charlotte.jpg)

Hello everybody, 

a Bavarian sublime disguised as a charlotte that I had made to taste almonds, and I do not tell you what it was delicious. the idea came when I wanted to make an easy cake without staying in the stove !!!! 

I open my closet, and I have only one box of apricot, and some small quantities of dried fruits 

I still look and I saw a small bag of almond powder, then I imagined the taste of a mousse with almond 

and hop the ideas were built, and I started in the preparation of a Bavarian, and I will not tell you twice, but the wedding apricot almond is not judging, it marries wonderfully, and it awakens all your senses, that I tell you, ok. 

![charlotte revisited with apricots](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/birthday-cake-soulef_21.jpg)

ingredients:   


  * 1 box of boudoirs 
  * 1 glass of apricot juice 



for apricot mousse: 

  * 300 ml of fresh cream 
  * 200 gr of apricots (it was apricot in box, and it is the net weight of the apricot after having drained of its syrup) 
  * 60 gr of sugar 
  * 1 tablespoon of apricot jam 
  * 8 g of gelatin 
  * 1/2 teaspoon orange dye 



for almond mousse: 

  * 300 ml of fresh cream 
  * 100 gr of almond powder 
  * a few drops of almond extract 
  * 80 gr of sugar 
  * 2 tablespoons fresh cream 
  * 5 g of gelatin 



decoration: 

  * some blanched almonds 
  * some nuts 
  * some hazelnuts 
  * some dried apricots 



preparation: 

  1. starting by covering a 20 cm diameter mold with removable base with boudoirs 
  2. dip the boudoirs in the apricot juice, 
  3. cover the base and the sides (a little trick so that the boudoirs that you raise in vertical do not fall, take aluminum foil, and fold it on the walls of the mold, and cover the top of 4 or 5 cakes already erected, and continue with the other boudoirs, until you have the mold well covered) 
  4. cool 



preparation of the apricot mousse: 

  1. put the gelatin in cold water and let it swell 
  2. whip the cold cream very cold, in a bowl very cold, in whipped cream 
  3. add 30 gr of sugar, and continue to whip, put the cream in the fridge 
  4. crush the apricots roughly 
  5. add the apricot jam, and the food coloring 
  6. cook on low heat for a compote 
  7. add the sugar 
  8. let reduce a little, not to have a too liquid count, and to let the water evaporate apricots 
  9. add the dewatered gelatin and let it melt well in this mixture 
  10. put this mixture out of the fire and let cool 
  11. remove the equivalent of 4 tablespoons of apricot jelly and leave aside 
  12. mix the rest of the compote very slowly with the fresh cream, to have a homogeneous and airy mixture 
  13. pour into your mold, on the boudoirs, arrange the surface 
  14. then decorate with the apricot jelly, and place in the fridge 



preparation of the almond mousse: 

  1. put the gelatin in cold water and let it swell 
  2. whip the cream in whipped cream 
  3. add the sugar and the almond extract and whip again, then put in the fridge 
  4. toast a little almond powder and set aside 
  5. in a small saucepan, gently heat the cream 
  6. add the dewatered gelatin, and let it dissolve well 
  7. mix the grilled almond powder with whipped cream 
  8. add the cream mixture cool / warm gelatin. 
  9. mix gently to get a homogeneous mixture 
  10. and fill your mold with this last layer 



decoration: 

  1. arrange and decorate the cake according to your taste, with dried fruits 
  2. heat a little apricot jam and with a brush decorate the dried fruits to give them a little shine. 



Thanks for your visit and your comments 

thank you to all those who continue to subscribe to my newsletter, and if you too, love my articles, do not forget to subscribe to my newsletter, it's free, easy and simple, and you will have an alert on your email, whenever I publish an article 

and do not forget, if you have tried one of my recipes, send me your photos on my email: 

[ @ a vosessais ](<mailto:vosessais@gmail.com>) mourdecuisine.fr 

bonne journée 
