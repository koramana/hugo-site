---
title: Tunisian Fricassee
date: '2017-05-01'
categories:
- appetizer, tapas, appetizer
- crepes, gauffres, beignets salees
- Tunisian cuisine
- ramadan recipe
tags:
- Tunisia
- fritters
- Tuna
- inputs
- Ramadan
- Sandwich
- Algeria
- accompaniment

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fricass%C3%A9-tunisien.jpg
---
[ ![Tunisian Fricassee](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fricass%C3%A9-tunisien.jpg) ](<https://www.amourdecuisine.fr/article-fricasse-tunisien.html/fricasse-tunisien>)

##  Tunisian Fricassee 

Hello everybody, 

Do you like Tunisian Fricassés? I'm crazy, since I learned this recipe from my friends at the faculty, I never tired of doing it, especially during Ramadan. I remember those years of faculty, where we ended up sharing recipes like crazy, to try them on weekends. And the Tunisian fricassés were my first recipe to make from our friend "Rahab", a Tunisian who married an Algerian and came to study with us, what she was good at cooking her. 

To tell you, during the month of Ramadan, I devote an evening to prepare the rolls for fricassee, I make for the equivalent of 2 kg of flour ... After cooling, I put in bags and in the freezer. That way, the day I do not feel like doing some boureks, I take out some donuts, and I prepare my farce, and here is a nice entrance to accompany my chorba.   


**Tunisian Fricassee**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fricass%C3%A9-tunisien-3.jpg)

portions:  6  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients** for the dough: 

  * 500 g flour. 
  * 1 tablespoon instant yeast 
  * 3 tablespoons of oil 
  * 1 tablespoon of sugar 
  * 1 egg 
  * 2 teaspoons of salt 
  * 150 to 200 ml of warm water (as needed) 

for the farce 
  * 2 grilled green peppers 
  * 3 tomatoes, 
  * 1 clove of garlic 
  * a drizzle of olive oil, 
  * salt pepper 
  * 200 g tuna in oil 
  * 2 hard boiled eggs 
  * green or black olives (according to taste) 
  * 2 boiled potatoes 



**Realization steps** Prepare the dough 

  1. In the bowl of the petrin, place the flour, add the yeast, and turn the petrin on at medium speed 
  2. add the sugar and salt. 
  3. whip the egg, and introduce it. 
  4. Now add the warm water to have a slightly sticky dough. 
  5. oil until the dough becomes soft and does not stick to the bowl wall 
  6. Now introduce the oil slowly while petrifying, form a ball and let rise between 1 to 2 hours depending on the temperature of the room. 
  7. Degas the dough and take pieces of dough about 40 g 
  8. shape them into sticks to obtain oval buns. 
  9. place them on a floured tray spacing them. 
  10. Cover them with a clean towel and let them rise again 
  11. Heat oil deep pan and cook by two or three pieces no more   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/friture.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/friture.jpg>)
  12. let them brown about 2 to 3 minutes per side. 
  13. Remove fricassee and drain on paper towels.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fricasse.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fricasse.jpg>)

Prepare the stuffing 
  1. grill the peppers, clean them and cut them into slices 
  2. place the cut peppers in a pan with a little oil, add over the crushed tomatoes and cook. 
  3. Then add the crushed garlic, salt and black pepper, and let the sauce reduce   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/hmiss.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/hmiss.CR2_.jpg>)
  4. Cook the potato with skin cut in thirds in salted water 
  5. you can put the eggs in the same saucepan to boil them. 
  6. after cooking remove the skin, and crush the potato and boiled eggs 
  7. add the tuna crumbled over, and the green or black olives cut in a slice. 
  8. Season with salt, pepper, and a drizzle of olive oil   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/farce-pomem-de-terre-et-thon.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/farce-pomem-de-terre-et-thon.jpg>)
  9. Cut the fricassés on one side to give them the shape of a sandwich 
  10. Stuff them with a spoon of hmiss, add on top the potato, egg and tuna mixture 
  11. garnish with ½ black olive and enjoy 



[ ![Tunisian Fricassee](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fricasses-tunisien.jpg) ](<https://www.amourdecuisine.fr/article-fricasse-tunisien.html/fricasses-tunisien>)
