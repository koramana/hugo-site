---
title: fried maakouda with tuna
date: '2018-05-08'
categories:
- appetizer, tapas, appetizer
tags:
- Algeria
- Ramadan
- inputs
- Easy cooking
- accompaniment
- Aperitif
- Potato

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/maakouda-au-thon-4.jpg
---
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/maakouda-au-thon-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/maakouda-au-thon-4.jpg>)

##  fried maakouda with tuna 

Hello everybody, 

I like makouda a lot, but I'm used to baking it, like [ Baked maakouda ](<https://www.amourdecuisine.fr/article-maakouda-au-thon.html>) firstly, I avoid frying and secondly, it is faster, I place my preparation in the mold and hop in the oven until the cooking of my maakouda. 

the recipe in Arabic: 

**fried maakouda with tuna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/maakouda-au-thon-1.jpg)

portions:  6  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 1 kg of potatoes 
  * 2 tuna cans 
  * 2 eggs 
  * Chopped parsley 
  * Salt pepper 
  * Flour 
  * Oil for frying 



**Realization steps**

  1. cook the well-washed potatoes with their skin in a pot of water. 
  2. peel and mash them in a bowl, using a potato masher. 
  3. Add the drained tuna, chopped parsley, salt, pepper and two beaten eggs.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/maakouda.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/maakouda.jpg>)
  4. Mix everything well. 
  5. Take some potato balls, pass them in the flour, flatten a little to shape small patties. 
  6. Fry in a pan and place on a plate covered with paper towels. 



[ ![maakouda with tuna 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/maakouda-au-thon-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/maakouda-au-thon-2.jpg>)
