---
title: rice with the empress, technical test best pastry
date: '2015-10-27'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
- rice
tags:
- desserts
- la France
- Milk rice
- Custard
- fondant
- To taste
- Confectionery

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limp%C3%A9ratrice-1.jpg
---
[ ![rice to the empress 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limp%C3%A9ratrice-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limp%C3%A9ratrice-1.jpg>)

##  rice to the empress, technical challenge best pastry chef 

Hello everybody, 

This year, and despite that I am every week the show of the best patissier 2015, I promised not to challenge myself, and realize the proceeds of the show ... 

But here, when they announced that for the technical test the candidates of the best patissier 2015 were to realize the rice to the Empress of Mercotte, a dessert created in the 19th century in honor of the Empress Eugenie, the wife of Napoleon III. I was curious to see the recipe ... besides it is on many sites, and frankly the reading of the ingredients and the recipe, I found myself too tempted to realize it. 

A well-scented rice cream, mixed with custard, then aerated foam with whipped cream ... So I did not hesitate to buy the necessary ingredients for the recipe ... Finally I thought I had to buy everything, but here I forgot to buy candied cherries to decorate my dessert, lol. 

[ ![Rice At The Empress](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limp%C3%A9ratrice.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limp%C3%A9ratrice.jpg>)   


**rice with the empress, technical test best pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limp%C3%A9ratrice-3.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 70 g diced candied fruits 
  * 100 ml of orange blossom water 
  * 500 ml of milk 
  * 1 vanilla pod (I had vanilla extract) 
  * 1 pinch of salt 
  * 125 g of round rice (arborio, risotto for me) 
  * 13 g of butter 
  * 80 g caster sugar 
  * 200 g of [ custard ](<https://www.amourdecuisine.fr/article-creme-anglaise-custard.html>)
  * 6 g of gelatin 
  * 130 g of whipped cream 
  * 1 sachet of vanilla sugar 
  * 3 candied cherries (I did not have any) 



**Realization steps**

  1. Put the candied fruits diced and macerate in orange blossom water. 
  2. Heat the milk with vanilla, salt and butter. 
  3. Boil 1 liter of water. Pour the rice in rain in the boiling water, let it cook for 2 minutes, then drain it 
  4. place the rice in the boiling milk. Turn the heat down and cook for about 20 minutes, gently, until the rice is cooked through. 
  5. add the sugar and cook another 5 minutes. 
  6. Add and mix candied fruits and orange blossom water by removing the rice from the heat. Then let it cool down. 
  7. Put the gelatin to soak in a little cold water. 
  8. Prepare the custard and, at the end of cooking, add the drained gelatin. let cool   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limperatrice-etapes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limperatrice-etapes.jpg>)
  9. Prepare the whipped cream with vanilla sugar. 
  10. When the rice and custard are cold, mix them well. 
  11. Add the whipped cream while turning slowly. Pour into a 20 cm diameter savarin dish and refrigerate for 3 to 4 hours. 
  12. To unmold, dip the mold for a few seconds in a dish filled with boiling water and return to the serving dish. Decorate with candied cherries cut in half. 



[ ![rice with the imperatrice 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limperatrice-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-a-limperatrice-2.jpg>)
