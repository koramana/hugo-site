---
title: Striped cake, Baumkuchen
date: '2015-10-11'
categories:
- cakes and cakes
- sweet recipes
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/G%C3%A2teau-ray%C3%A9-Baumkuchen.jpg
---
[ ![Baumkuchen striped cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/G%C3%A2teau-ray%C3%A9-Baumkuchen.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/G%C3%A2teau-ray%C3%A9-Baumkuchen.jpg>)

##  Striped cake, Baumkuchen 

Hello everybody, 

This striped cake or Baumkuchen is always his turn on facebook, and on the blogosphere. I have already published the nature version: [ Schichttorte ](<https://www.amourdecuisine.fr/article-schichttorte-ou-gateau-a-etages-alsacien.html>) , and today I share with you the chocolate version. 

I love this new version, and I will certainly try it because the chocolate addicts, it is not that which is missing at home ... 

**Striped cake, Baumkuchen**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/G%C3%A2teau-ray%C3%A9-Baumkuchen-1-300x210.jpg)

portions:  12  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 250 g of very soft butter 
  * 250 g of sugar 
  * 7 medium eggs at room temperature 
  * 150 g flour 
  * 100 g cornflour 
  * 1 pinch of salt 
  * 1 tbsp. powdered vanilla 
  * 2 tablespoons of good quality pure cocoa powder 

Frosting: 
  * 200 ml of cream with 35% fat 
  * 200 g dark chocolate couverture 
  * 1 tablespoon of butter 



**Realization steps**

  1. Start preparing the cake: To do this, with an electric mixer or food processor, beat the butter with the sugar until you get a creamy consistency and the mixture whitens. 
  2. Separate the whites from the egg yolks and set aside. 
  3. Add the yolks one by one to the butter / sugar mixture. 
  4. Then add the flour mixed with the cornstarch. 
  5. Then, mount the egg whites in stiff snow with a pinch of salt. 
  6. Gently add the egg whites to the mixture, it is best to do it twice. 
  7. Divide the dough into two equal parts, and add the cocoa to one of them and the vanilla to the other. 
  8. Preheat the oven grill or rotisserie mode if you have the option on your oven (if the temperature is 200 ° C). Cover a hinged mold with 20 to 22 cm of parchment paper or greased and floured. 
  9. Started by baking the layers of cake: Take two tablespoons of white cake dough, and distributed on the mold, leaving a very thin layer, like a pancake. Bake this layer for 2 or 3 minutes in the center of the oven. 
  10. The layer of dough should be cooked and lightly browned. 
  11. Now distribute two tablespoons of chocolate paste on the previous layer. Bake ⅔ minutes to cook under the grill. 
  12. Proceed as before to finish both pasta. 
  13. Take the cake from the oven, wait 15 or 20 minutes and unmold carefully. 
  14. Cool completely. 
  15. Prepare the chocolate icing. Boil the cream, and when it boils, remove from the heat. 
  16. Add the chopped chocolate and butter, and stir gently until the chocolate is melted. Leave aside to temper the chocolate. 
  17. Put the cake upside down on a rack and cover with frosting. Let solidify and refrigerate for. 
  18. If you wish, you can decorate the sides of the cake with tri-colored chocolate balls or chocolate vermicelli or chocolate chips: stick carefully before the end of the solidification of the chocolate. 



[ ![Baumkuchen striped cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/G%C3%A2teau-ray%C3%A9-Baumkuchen-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/G%C3%A2teau-ray%C3%A9-Baumkuchen-2.jpg>)
