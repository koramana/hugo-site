---
title: Chocolate financiers vermicelli
date: '2015-08-10'
categories:
- Cupcakes, macaroons, and other pastries
- cakes and cakes
- sweet recipes
tags:
- biscuits
- Cakes
- Easy recipe
- To taste
- Algerian cakes
- desserts
- Delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/08/financier-au-chocolat.jpg
---
[ ![financial chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/financier-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/financier-au-chocolat.jpg>)

##  Chocolate financiers vermicelli 

Hello everybody, 

Ready for an easy taste, and super delicious? I'll give you this recipe of Financiers au vermicelle chocolate, a recipe from my friend As Sou, or rather the recipe of her mother, god forbid of all evil. 

Personally I like the financiers, besides you can do a little walk on my blog, to see the different easy recipes, simple and delicious, in my opinion, there is no more delicious than the financiers to taste it ... 

**Chocolate financiers vermicelli**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/financier-au-chocolat-2.jpg)

portions:  20  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * egg whites (250 g); 
  * The pinch of salt; 
  * 250 g of melted butter; 
  * 150 g of almond powder; 
  * 100g of flour ; 
  * 250 g of sugar; 
  * Vanilla sugar ; 
  * 2 tablespoons vermicelli chocolate 



**Realization steps**

  1. Beat the egg whites with a little salt. 
  2. Mix the almond powder, sugar, vanilla sugar with the sifted flour. 
  3. Bring the butter to a boil in a saucepan until you get the hazelnut color. 
  4. Gradually add the egg white mixture and stir gently. 
  5. Add the vermicelli chocolate. 
  6. Then pour the melted butter and stir again slowly. 
  7. Preheat the oven to 180 ° C 
  8. Butter and flour the silicone mold of financiers. 
  9. Pour the mixture. 
  10. Bake 20 to 25 minutes.   
PS: For the half recipe, I got 16 pieces. 

Decoration 
  1. Let cool. 
  2. Decorate with a ganache, or with melted Nutella and Pistachio if not with 
  3. jam and sprinkled with icing sugar. 



[ ![chocolate financial 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/financier-au-chocolat-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/financier-au-chocolat-1.jpg>)
