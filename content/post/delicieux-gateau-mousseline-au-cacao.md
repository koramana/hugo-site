---
title: delicious cake with cocoa muslin
date: '2010-07-05'
categories:
- panna cotta, flan, et yaourt
- ramadan recipe
- riz

---
I hope that this time the recipe will pass, hihihihiih, I'm fighting now with the connection. in any case, I pass you the recipe, which, with the ingredients, will give you a big cake, the mold that I use is a cake mold 40 cm long. the ingredients: 9 eggs 220 grs of sugar 9 ca soup of oil 100 ml of milk 300 grs of flour 2 bags of yeast (20 grs) 2 ca cocoa coffee decoration: dark chocolate coconut or according to taste separate the whites yellow eggs. mount the & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

I hope that this time the recipe will pass, hihihihiih, I'm fighting now with the connection. 

in any case, I pass you the recipe, which, with the ingredients, will give you a big cake, the mold that I use is a cake mold 40 cm long. 

Ingredients: 

  * 9 eggs 
  * 220 grams of sugar 
  * 9 tablespoons of oil 
  * 100 ml of milk 
  * 300 grams of flour 
  * 2 bags of yeast (20 grs) 
  * 2 cocoa coffee 



decoration: 

  * dark chocolate 
  * coconut 



or according to taste 

separate the egg whites from the yolks. 

turn the egg whites into snow, and slowly add half the sugar, leave aside. 

beat the egg yolk with the remaining sugar. 

add the oil, while whisking, then the milk, then incorporate a small amount of white in snow, and a small quantity of flour mix with the yeast, until exhaustion, while trying not to break the white of 'egg. 

at the end add the cocoa and slowly add it to the mixture. 

pour the mixture into a large cake tin, or otherwise in a savarin dish, and cook in a preheated oven at 180 degrees. 

at the end of cooking, melt the chocolate, cover all your cake and decorate with cocoa 

bonne degustation 
