---
title: Sachertorte recipe Austrian chocolate cake
date: '2017-12-27'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-de-sachertorte-gateau-au-chocolat-autrichien.jpg
---
![recipe of sachertorte, Austrian chocolate cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-de-sachertorte-gateau-au-chocolat-autrichien.jpg)

##  _Sachertorte_ Austrian chocolate cake recipe 

Hello everybody, 

Here is the recipe that makes the news on the blogosphere. Why? Well, because the Sacher torte or as we write it sometimes _Sachertorte_ recipe, Austrian cake was the subject of the technical test of "best pastry chef season 3". Yes I know that almost everyone follows this show, and everyone tries to make the leader, without being under the eyes of Mrs. Mercotte, whom I salute of passage, and the chief Cyril ... 

In any case, I was not to make the recipe, as it was presented by the chef ... But I went to refer to one of my cookbooks, which is almost 13 years old or older. 

An English book, the chef: Anne William: Look  & Cook, Chocolate Desserts, 1992 edition. Well, I had forgotten this book in the bottom of my closet, but last week, while I was in full paint, and sorting my cupboards, I fell on it, I flipped through it briefly, and it must be said that the sacher torte (sachertorte), gave me a little wink. I said to myself, as soon as I finish all this paperwork at home, I will do this delight for my children, I did not know that it was going to be the subject of challenge of the chefs participating in the Best Pastry Chef program. 

[ ![Sachertorte recipe Austrian chocolate cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sachertorte-1.jpg) ](<https://www.amourdecuisine.fr/article-sachertorte-recette-gateau-autrichien-au-chocolat.html/sachertorte-1>)

picture of Iphone 

Ah great minds meet, lol ... So when the recipe for this delicious sachertorte cake went into challenge, it awoke my nostalgia "lol" for this little delight, and here I am in my pots, and near my oven. 

you can see here the recipe of the sachertorte video, and I hope you will enjoy the video: 

**Sachertorte recipe, Austrian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-de-sachertorte-gateau-au-chocolat-autrichien-2.jpg)

portions:  8-10  Prep time:  35 mins  cooking:  60 mins  total:  1 hour 35 mins 

**Ingredients** the chocolate cake: 

  * 125 gr of chocolate 
  * 6 eggs 
  * 125 gr of butter at room temperature 
  * 135 g icing sugar 
  * 1 teaspoon of vanilla extract 
  * 100 gr of crystallized sugar 
  * 100 gr of flour 

icing with apricot jam: 
  * 175 gr of jam 
  * 30 ml of water 
  * chocolate icing: 
  * 250 gr of bitter dark chocolate, or over 75% cocoa 
  * 250 gr of crystallized sugar 
  * 125 ml of water   
![Sachertorte recipe, Austrian cake](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sacher-torte-018.CR2_.jpg)



**Realization steps** preparation of the chocolate cake: 

  1. Preheat the oven to 180 C (350 F) 
  2. Butter and flour generously a round mold of almost 22 cm in diameter. 
  3. Melt the chocolate in a bain marie. 
  4. mix the butter and icing sugar with a whisk until you have a well-aired cream. 
  5. separate the egg whites from the yolks, each time introducing an egg yolk to the butter mixture. 
  6. Now add the melted chocolate warm, incorporating it with a wooden spoon.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sachertorte-4-241x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sachertorte-4-241x300.jpg>)
  7. Beat the egg whites, when the whites start to froth add the granulated sugar in a small amount.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sachertorte-6-293x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sachertorte-6-293x300.jpg>)
  8. whisk at maximum speed until firm. 
  9. Now add the sifted flour and the whipped egg whites, that is to say, 2 tablespoons flour, in a small quantity, gently add with a spatula while aerating the dough, then a quantity of Whipped egg whites, always with a spatula, aerating the dough, without breaking the egg whites   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sachertorte-7-240x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sachertorte-7-240x300.jpg>)
  10. Pour this mixture into the mold and bake for almost 1 hour to 1 hour and a quarter, watch the cooking with a toothpick that should go out dry, when you insert it into the cake. 
  11. at the exit of the oven, let the cake cool a little, before unmolding it. 
  12. Divide the cake into two discs. 
  13. Heat half the amount of jam, and spread it on the first disc.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sachertorte-8-264x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/sachertorte-8-264x300.jpg>)
  14. Drop the second disc over it. 
  15. heat the other half of the jam with a little water, switch to Chinese to remove the large pieces. reheat again, to have like a light syrup. 
  16. Soak with the second disc, and let cool. 

Prepare the chocolate icing: 
  1. chop the chocolate well, and place it in a saucepan 
  2. add sugar and water 
  3. stir over medium heat, and watch the temperature, which should not exceed 90 degrees C with the sugar thermometer. 
  4. continue to mix 
  5. When the temperature is reached, remove the pan from the heat, and continue to mix to cool, you will have a very bright glaze. 
  6. pour this mixture delicately on the cake, in the middle, and help you with a flat spatula, to spread the galacage on the cake, and cover the sides. Cover all the cake. 
  7. Let cool before serving. 



![recipe of sachertorte, Austrian chocolate cake 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/recette-de-sachertorte-gateau-au-chocolat-autrichien-1.jpg)
