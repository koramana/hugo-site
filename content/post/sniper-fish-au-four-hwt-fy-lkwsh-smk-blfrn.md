---
title: Baked sniper fish حوت في الكوشة, سمك بالفرن »
date: '2010-01-12'
categories:
- dessert, crumbles and bars
- panna cotta, flan, and yoghurt

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/01/snaper-fish11.jpg
---
I'm not too expert in fish names, and here in England it does not help much, hihihihih 

so this is a big goldfish, it's up to you to find his name, hihihihihi. 

my husband had bought me this "Sniper fish" as they call it here, frozen, the day before I cleaned it well, and well marine, with: 

  * salt 
  * black pepper 
  * of salmon 
  * ginger 
  * Garlic 
  * thyme 



so let everything marinate at least 4 hours (for me it was a night in the fridge) 

take a baking dish, 

place potatoes peeled, rinsed and cut into slices (3 potatoes for me) 

slice over a long cut onion, crush over 2 garlic cloves, salt, pepper and spice according to your taste, add bay leaves, and a little thyme, beware of salt. 

put on the fish (2 fish for me). 

pour over the marinade. 

cover with aluminum foil, and place in a hot oven, until cooking fish, remove the aluminum foil, and let brown a little. 

good tasting. 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2010/01/snaper-fish11.jpg) ](<http://amour-de-cuisine.com/wp-content/uploads/2010/01/snaper-fish.jpg>)
