---
title: Algerian cake without cooking - stuffed bniouen
date: '2014-07-24'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/bniouen-farci-075_thumb1.jpg
---
##  Algerian cake without cooking - stuffed bniouen 

Hello everybody, 

Here is a delicious [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , and [ without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) , so do not bother to say I'm lazy, you'll prepare in 30 min max, but do not blame me if towards the end you will find only 2 pieces in the tray, because it's too good. 

For my part I prepared them at night, and let it dry, in the morning to take pictures, I found only 4 pieces, with my 3 little monsters, who woke up, a little before me, and ben c ' was the cute catch for them ...... 

The recipe for you without delay knowing that the stuffing is what I had left of the [ Moorish almond paste with coconut ](<https://www.amourdecuisine.fr/article-mauresque-a-la-noix-de-coco-gateau-algerien-101325541.html>) , that I had been preparing just recently. 

the recipe in Arabic: 

**Algerian cake without cooking - stuffed bniouen**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/bniouen-farci-075_thumb1.jpg)

portions:  50  Prep time:  15 mins  cooking:  1 min  total:  16 mins 

**Ingredients**

  * ingredients: 
  * 2 boxes of biscuits 
  * softened butter 
  * honey according to taste 
  * 2 tablespoons orange marmalade (it gives a sublime taste) 
  * Turkish halwat, or sesame halva. 
  * cocoa (5 to 6 tablespoons, for cocoa lovers like me, otherwise go slowly) 
  * a box of white chocolate 
  * [ almond paste from the Moorish cake ](<https://www.amourdecuisine.fr/article-mauresque-au-nutella-gateau-algerien-sans-cuisson.html> "Moorish Nutella - Algerian Cake without cooking") (for the stuffing) 



**Realization steps**

  1. method of preparation: 
  2. crush the cookies a little and place them in the bowl of the blinder 
  3. add the rest of the ingredients (except the almond paste) and mix until you have a nice homogeneous paste (if necessary add the butter and or honey, according to your taste) 
  4. take the almond paste, and form balls of 2 cm in diameter 
  5. take a quantity of the biscuit paste, put it in the hollow of your hand place the almond paste ball in the heart and enrobe. 
  6. keep going until exhaustion dough and stuffing. 
  7. melt the white chocolate in a bain-marie or in the microwave, and immerse the balls of bniouen, for my part I prefer to put the balls of bniouen on a grid and cover with the melted white chocolate. 
  8. let it dry a little, and if you want, you can decorate with a little melted dark chocolate, to give the effect of raillures. 


