---
title: how to cook rice successfully
date: '2016-02-26'
categories:
- diverse cuisine
- Healthy cuisine
- Dishes and salty recipes
- ramadan recipe
- rice
tags:
- Algeria
- dishes
- Healthy cuisine
- Vegetarian cuisine
- Easy cooking
- Full Dish
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Comment-reussir-la-cuisson-du-riz.jpg
---
[ ![How to cook rice successfully](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Comment-reussir-la-cuisson-du-riz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Comment-reussir-la-cuisson-du-riz.jpg>)

##  how to cook rice successfully 

Hello everybody, 

When I started  at  cook the rice, it  was  every time a disaster, I did everything, I read everywhere, I tried  at  each time one mode of cooking, cata, after cata, after another.  I always avoided the dishes  at  base of rice, and yet I loved them a lot ... But still, it's not  is not  possible to make a missed rice each time. 

One day I went to my Sudanese friend's house, we chatted and talked in her kitchen, and I watched her make her rice, it was child's play for her, and when she put it  at  I was more than bluish by an impeccable result. She presented her rice with a sauce based on coretian, besides you can see the recipe when I redone it here: [ Rice with the corte ](<https://www.amourdecuisine.fr/article-riz-a-la-corete-mouloukhiya.html>) . 

So, you too have this worry with your rice? follow me, and it will work well, you will see! 

**how to cook rice successfully**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Comment-reussir-la-cuisson-du-riz-1.jpg)

portions:  6  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 500 gr of rice 
  * 2 tablespoons extra virgin olive oil 
  * 1 teaspoon of coriander seeds 
  * 2 bay leaf 
  * ¼ onion 
  * 1 clove of garlic 
  * salt and black pepper 
  * 4 to 5 green cardamom 



**Realization steps**

  1. For my part, I use basmati rice, the other rice do not succeed at all. So if you want to make a success of your dish, opt for basmati rice.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/riz-bismati.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/riz-bismati.jpg>)
  2. The first and most important thing is to wash the rice under abundant water until you have a very clear water that escapes. 
  3. rub lightly between your fingers and let the starch escape from your rice.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/lavez-le-riz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/lavez-le-riz.jpg>)
  4. in a pot with a thick bottom, brown the chopped onion in small cubes. 
  5. When the onion becomes translucent, add the spices, bay leaves and crushed garlic.   
for spices you can put all you want is how do you want to flavor your rice, sometimes I add frozen peas 
  6. add the rice and stir a little, add salt and black pepper. 
  7. cover with water, not too much water, just 5 mm above the rice, it is very important to respect this point and especially do not stir.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cuisson-du-riz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cuisson-du-riz.jpg>)
  8. cover the pot with a well-sealed aluminum foil, then cover, so that the steam does not go too far. 
  9. watch as soon as you can not see the steam escape, remove the rice from the fire. 
  10. normally your rice at this point is cooked well, with a fork, gently stir to air your rice, If the rice is cooked well, do not cover it, if it misses just a little cooking, cover it, and leave until moment of serving, he will finish cooking in his steam. 



[ ![How to cook rice 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Comment-reussir-la-cuisson-du-riz-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Comment-reussir-la-cuisson-du-riz-1.jpg>)
