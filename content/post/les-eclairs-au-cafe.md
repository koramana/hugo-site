---
title: Coffee lightning
date: '2018-01-31'
categories:
- Cupcakes, macaroons, and other pastries
- basic pastry recipes
- sweet recipes
tags:
- To taste
- Chiffon cream

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/10/eclairs_thumb1.jpg
---
##  Coffee lightning 

Hello everybody, 

It must be said that yesterday we watch the show **Best pastry chef** , I drool well in front of my screen, and especially I took advantage of the tips shared by our leaders: **Mercotte** , **Cyrils Lignac** and the **Chef Philippe Rigollot.** And of course after all that, I went to eat some pieces of [ Paris Brest ](<https://www.amourdecuisine.fr/article-paris-brest.html> "Paris Brest") , yes mine was not revisited, but still it was super good especially with this creamy [ praline chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline-pralinee.html> "cream chiffon praline") who stuffed it generously. 

I also remembered that I had already made and shared with you through my blog the lightning recipe, it was very far in my archives, so I'm resting it today, and I hope it goes a lot to please you .. I know that many of my readers confessed yesterday their weakness for the **choux pastry,** and I believe that I personally start to appreciate the desserts made from pate with cabbage ... I really liked in any case my **Paris Brest** ...   


**Lightning**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/eclairs_thumb1.jpg)

portions:  20  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients** For the pasta 

  * ¼ l of water 
  * 100g of butter 
  * 10 g of sugar 
  * 1 teaspoon of salt 
  * 150g of flour 
  * 4 eggs (Medium) 

for the pastry cream: 
  * 500 ml of milk, 
  * 4 egg yolks 
  * 4 to 5 cases of sugar 
  * aroma of your choice 
  * 4 tbsp full of cornflour 



**Realization steps**

  1. Put to heat in a pan the water, the butter cut in small dice, the salt and the sugar. 
  2. Then bring to a boil stirring with a wooden spoon or spatula. 
  3. As soon as the mixture is boiling, remove from heat and pour the flour quickly at one time. Mix well. 
  4. Then put back on the heat and stir vigorously with a wooden spoon or spatula to dry the dough. Remove from heat when the dough does not stick any more with the spoon or spatula or the pan. 
  5. Add the eggs one by one, stirring each time. 
  6. using a pastry bag, form small pieces of 10 cm long, 
  7. I used the 1.8cm socket The dough should be smooth and firm at the same time.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/eclairs3_thumb1.jpg)
  8. Bake in a hot oven around 210 °. 

prepare the pastry cream 
  1. Put all these ingredients in the mixer, turn a few seconds. Pour into a saucepan and thicken, turning constantly with a wooden spoon over medium heat. After a few minutes, the cream is ready to use! 
  2. I did the filling with a special syringe, so no need to cut the lightning by two, to fill. 
  3. and to garnish, I followed the packing of 

Frosting for coffee lightning. 
  1. Heat a little milk mixed with soluble coffee. Take the icing sugar and gradually add the coffee to the milk until you reach the desired consistency. Apply on the cakes. 


