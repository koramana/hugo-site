---
title: Mesrane mahchi, stuffed gut, recipes help el kebir 2012
date: '2012-10-14'
categories:
- crepes, gauffres, beignets salees
- cuisine algerienne
- cuisine marocaine
- recettes a la viande de poulet ( halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/masrane-mahchi-1-aid-el-kbir-2012_thumb1.jpg
---
##  Mesrane mahchi, stuffed gut, recipes help el kebir 2012 

Hello everybody, 

a big thank you to Chahira, one of my readers who liked to share the recipe of Mesrane mahchi, stuffed gut, recipes aid el kebir 2012. 

She sent me the recipe more than a moment ago now, and I've never had the time to publish, but is approaching the  'Aid el Kebir  ,  Aid el Adha  ,  عيد الأضحى sheep sacrifice festival  I think it's the perfect time to share this recipe with you.   


**Mesrane mahchi, stuffed gut, recipes help el kebir 2012**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/masrane-mahchi-1-aid-el-kbir-2012_thumb1.jpg)

**Ingredients**

  * 1 hose (The big one) 
  * 500 g minced meat 
  * 1 bunch of chopped parsley 
  * 1 egg 
  * A handful of peas 
  * An onion 
  * 2 cloves of garlic 
  * A little oil 
  * A little butter 



**Realization steps**

  1. Scrape the hose on both sides, and wash well. 
  2. Mix the minced meat, grated onion, parsley, cinnamon, pepper, salt, peas, and egg to pick up the stuffing. 
  3. Fill the hose with this stuffing, to facilitate the work, use a funnel. 
  4. Tie both ends of the hose with a thread, and prick it with a needle so that it does not burst. 
  5. In a pot, grate onion, cloves of garlic, a little oil. 
  6. let simmer a little and add a little water, and spices. 
  7. Immerse the casing in the sauce, trying to wrap it in a spiral or snail, then prick with toothpicks, and cook over low heat. 
  8. Once cooked, remove the stuffed gut from the sauce, and brown it in a pan with butter to give it a golden color. 
  9. On the other hand, sauté the mushrooms with butter, 1 clove of garlic and parsley. 
  10. introduce the stuffed gut, on a nice dish garnished with salad and mushroom saute. 


