---
title: tajine lamb prunes apricot almonds
date: '2012-01-21'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours
- idee, recette de fete, aperitif apero dinatoire

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg
---
# 

#  [ ![tajine hlou12](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg>)

#  Tajine lahlou, lham lahlou 

a tasty dish, a marriage of sweet and salty in a dish to taste with a good homemade bread. 

Tajine never goes away on the first day especially of the month of Ramadan, there are even people who like it to be always on their table. 

in any case, at home, I can say that I'm the only one who will eat it, not serious, because this dish can be well preserved in the refrigerator. 

and here is my little recipe, as my mother always did 

**tajine lamb prunes apricot almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou12.jpg)

portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * mutton 
  * dried prunes 
  * dried apricots 
  * some dried raisins 
  * ½ onion 
  * some butter or smen (clarified butter) 
  * 3 tablespoons of crystallized sugar 
  * 2 tablespoons orange blossom water 
  * 1 pinch of cinnamon powder or 1 stick of cinnamon 
  * 1 little saffron or dye 
  * 1 pinch of salt 



**Realization steps**

  1. arrange in a fact all the cut meat, butter or smen, chopped onion, dye, cinnamon and salt. 
  2. cover with about ½ liter of water and cook, until the meat becomes tender 
  3. in the meantime, put the prunes and apricots in the steam, being careful not to mix them, to keep the beautiful color of each of them. 
  4. cheat the raisins in orange blossom water, so that it swells well. 
  5. towards the end of cooking meat place in punches, apricots and raisins. 
  6. Add sugar and orange blossom water, cook for just a few minutes and remove from heat. 
  7. at the moment of serving, you can garnish your dish with some blanched and grilled almonds. 



[ ![tajine hlou lham lahlou](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/tajine-hlou-lham-lahlou.jpg>)

for a tajine hlou recipe without meat: 

![Tajin-hlou-1.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x416/2/42/48/75/Images/) [ TAJINE HLOU AUX POMMES ET ANIS ÉTOILÉ, SANS VIANDE ](<https://www.amourdecuisine.fr/article-tajine-hlou-aux-pommes-et-anis-etoile-sans-viande-80614283.html> "tajine hlou with apples and star anise, without meat")
