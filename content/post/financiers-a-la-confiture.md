---
title: Financial with jam
date: '2013-03-26'
categories:
- diverse cuisine
- ramadan recipe
- salads, salty verrines

---
![Financial-a-la-jam-de-figs-012.CR2.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x369/2/42/48/75/API/2013-03/07/)

Hello everybody, 

I always have egg whites in the freezer, but every time I want to realize the financial, we like a lot at home, I have a missing ingredient ... 

in any case, not this time, or we could eat jam financiers ( [ fig and mango jam ](<https://www.amourdecuisine.fr/article-confiture-de-figues-et-mangues-111341416.html>) in this recipe). and what was soft and sweet in the mouth ... besides I did not even have the chance to take a lot of pictures, because there were hands that jumped in all directions to take a piece .... even my husband, who is generally, only takes cakes for his afternoon tea, had much to appreciate this little delight. 

![Financial-a-la-jam-de-figs-021.CR2.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x833/2/42/48/75/API/2013-03/07/)

ingredients: 

  * _90 g of butter_
  * _4 egg whites_
  * _30 g of flour_
  * _75g of almond powder_
  * _80 g of sour powder_
  * _1 tablespoon of fig and mango jam_
  * _the zest of ¼ orange_
  * _20g of tapered almonds_



![Financial-a-la-jam-de-figs-004.CR2.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x369/2/42/48/75/API/2013-03/07/)

method of preparation: 

  1. _Mix the flour, sugar, almond powder and orange peel._
  2. _Add whipped whites, melted butter, jam and mix all ingredients_
  3. _Butter the mussels at the financial ones and fill them at ¾._
  4. _Sprinkle with flaked almonds._
  5. _Bake in a preheated oven at 200 ° C for about 20 minutes._
  6. _Leave to cool, turn out and let cool before eating_



![Financial-a-la-jam-de-figs-019.CR2.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x381/2/42/48/75/API/2013-03/07/)
