---
title: Bolognese sauce recipe
date: '2016-11-22'
categories:
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/sauce-bolognaise-halal-sans-vin-034.CR2_thumb.jpg
---
![Bolognese sauce recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/sauce-bolognaise-halal-sans-vin-034.CR2_thumb.jpg)

##  Bolognese sauce recipe 

Hello everybody, 

I share with you today the recipe for my Halal and Wine-Free Homemade Bolognese Sauce recipe as I have always prepared it, and as I learned from an old neighbor, who is a real blue cordon in cooking. 

a recipe that always accompanies me, pasta, lasagna, rice, and full of other recipes. 

sometimes I introduce pitted olives, when I want my Bolognese sauce to be presented with spaghetti ... it changes some of the original Bolognese sauce recipe, but the taste of olives in it, a real treat. 

recipe in Arabic 

**Bolognese sauce recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/sauce-bolognaise-halal-sans-vin-026.CR2_thumb.jpg)

portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 1 tablespoon of butter 
  * 2 tbsp. extra virgin olive oil 
  * 1 onion, finely chopped 
  * 2 crushed garlic cloves 
  * a finely chopped celery stalks 
  * 1 carrot, finely chopped 
  * 500 gr chopped veal 
  * 1 glass of water 
  * a little freshly ground nutmeg 
  * 1 can of tomatoes in pieces, otherwise 4 to 5 fresh tomatoes 
  * salt, pepper, basil, oregano, and thyme 



**Realization steps**

  1. In a heavy-bottomed pan, melt the butter and add the oil over medium heat. 
  2. Fry the onion until it becomes translucent, 
  3. add crushed garlic, grated carrot and chopped celery, cook for about 4 minutes. 
  4. Add ground meat, salt, and a little black pepper. 
  5. cook until the meat changes color. 
  6. Add water and nutmeg and simmer 
  7. add the remaining spices and herbs, and cook until the water is completely evaporated. 
  8. Add the tomatoes now and mix well. 
  9. Reduce the heat and simmer very slowly uncovered, stirring occasionally. 
  10. Rectify the seasoning. 



![Bolognese sauce recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/sauce-bolognaise-halal-sans-vin-032.CR2_thumb.jpg)

method of preparation: 
