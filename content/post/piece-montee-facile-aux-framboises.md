---
title: easy raspberry cake
date: '2014-12-25'
categories:
- cakes and cakes
- sweet recipes
tags:
- Pastry
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-aux-framboises_thumb1.jpg
---
#  [ ![piece raspberry raspberry](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-aux-framboises_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-aux-framboises_thumb1.jpg>)

##  easy raspberry cake 

Hello everybody, 

A pretty **cake** , prepared by our talented Lunetoiles, and which I mistakenly deleted from my blog, I put it back online, and I hope you will like it. 

This piece assembled and easy to achieve because it is based on [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-inratable.html> "unbreakable genesis") glued to each other with a delicious raspberry coulis and a smooth cream pastry, and covered with a nice layer of [ white chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat-blanc.html> "white chocolate ganache") .   


**easy raspberry cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-aux-framboises_thumb1.jpg)

portions:  10  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients** For Genoese: 

  * 6 eggs 
  * 200 g of sugar 
  * 200 g flour 

For pastry cream: 
  * 500 ml whole milk 
  * 4 egg yolks 
  * 130 gr of sugar 
  * 50 g of maizena 
  * 1 vanilla pod 

For the ganache: 
  * 200 g of white chocolate 
  * 250 ml whole liquid cream 

For assembly and decoration: 
  * 200 ml raspberry coulis 
  * 400 g frozen raspberries 
  * 200 g fresh raspberries 
  * Small meringue with vanilla 



**Realization steps** The day before, prepare the ganache: 

  1. Bring the cream to a boil and pour the boiling cream on the chocolate (pistoles or chocolate broken into pieces), in three times, 
  2. mix. In this way a smooth and homogeneous ganache is obtained. 
  3. Let cool to room temperature and let rest overnight in the refrigerator. 

Prepare the sponge cake: 
  1. put the eggs and sugar in a salad bowl and place it in a bain-marie. 
  2. Whisk with an electric mixer until the mixture is white and triple in volume. It takes about 5 minutes: you have to get a very frothy preparation. 
  3. Remove the salad bowl from the bain-marie. 
  4. Pour the sifted flour in rain over the entire surface of the foam and incorporate it with a spatula, lifting the mass. You must mix a minimum, to prevent the foam from falling. 
  5. Pour the preparation into two buttered and floured molds or silicone, one of 18 cm and one of 22 cm of diameter. 
  6. Put in the oven for 20 minutes at 180 ° C (th.6). 
  7. Leave to cool for 5 minutes, then unmold on a rack. 
  8. Then cut the sponge cake in half. 
  9. Soak them with raspberry coulis diluted with 50 ml of water. 



[ ![piece raspberry raspberry](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/piece-montee-lunetoiles_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/piece-montee-lunetoiles_thumb.jpg>)

Prepare the pastry cream: 

  1. split the vanilla bean in half, scrape the seeds and put all in the milk you will bring to the boil. 
  2. In a bowl, whisk the yolks and sugar until the mixture is white. 
  3. Then add the maizena. 
  4. Pour the boiling milk over the previous mixture, then put it back into the pan. 
  5. Then make thicken, without mixing. Pour the cream into a salad bowl and cover it with food film. Thus, there will be no skin formation on the surface. 
  6. Let cool. 
  7. Divide the raspberries still frozen on the disc of sponge 18 cm and that of 22 cm and cover with pastry cream. 
  8. Put the 2nd disc of génoises to close again. 
  9. Whip the ganache. Cover the first sponge cake of 22 cm previously placed on a serving dish. 
  10. Place the second sponge 18 cm in the center, and cover with ganache. 
  11. Regularize the surface with a metal spatula. 
  12. Place in the fridge until serving, where you will decorate your cake with fresh raspberries and small meringues. 



[ ![piece raspberry raspberry](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-lunetoiles_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-lunetoiles_thumb1.jpg>)
