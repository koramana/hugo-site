---
title: Crispy chicken with doritos
date: '2012-04-05'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/poulet-rotis-doritos_thumb.jpg
---
##  Crispy chicken with doritos 

Hello everybody, 

hum, from [ roasted chicken ](<https://www.amourdecuisine.fr/categorie-12364208.html>) with a good skin **crunchy** , **tender** inside, and scented .... with doritos, yes **Doritos** , it is delicious **tortilla chips** Great **crunchy** , which forms a nice crust on the chicken ... a taste to discover, and I recommend it. 

then to the recipe: 

  * 3 to 4 legs of **chicken** complete. 
  * 3 tablespoons of olive oil. 
  * 1 teaspoon of cumin. 
  * 1 teaspoon coriander powder. 
  * 1/2 teaspoon of turmeric. 
  * 1/2 teaspoon of black pepper. 
  * salt according to taste. 
  * 2 to 3 pack of Doritos 



method of realization: 

  1. clean the chicken legs. 
  2. the day before, mix the olive oil with all the spices. 
  3. place the chicken legs in a bag and pour over this marinade. 
  4. close the bag, rotate in all directions, so that the marinade infiltrates everywhere. 
  5. leave to spawn all night. 
  6. in the morning crush the doritos slowly. 
  7. cover the chicken legs with the crushed doritos. 
  8. place the legs in a baking dish preheated to 180 degrees C 
  9. cook for 40 to 50 minutes, depending on the oven capacity. 
  10. serve with a very fresh salad. 



follow me on: 

Or on 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

**Crispy chicken with doritos**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/poulet-rotis-doritos_thumb.jpg)

**Ingredients**

  * 3 to 4 complete chicken legs. 
  * 3 tablespoons of olive oil. 
  * 1 teaspoon of cumin. 
  * 1 teaspoon coriander powder. 
  * ½ teaspoon of turmeric. 
  * ½ teaspoon of black pepper. 
  * salt according to taste. 
  * 2 to 3 pack of Doritos 



**Realization steps**

  1. clean the chicken legs. 
  2. the day before, mix the olive oil with all the spices. 
  3. place the chicken legs in a bag and pour over this marinade. 
  4. close the bag, rotate in all directions, so that the marinade infiltrates everywhere. 
  5. leave to spawn all night. 
  6. in the morning crush the doritos slowly. 
  7. cover the chicken legs with the crushed doritos. 
  8. place the legs in a baking dish preheated to 180 degrees C 
  9. cook for 40 to 50 minutes, depending on the oven capacity. 
  10. serve with a very fresh salad. 


