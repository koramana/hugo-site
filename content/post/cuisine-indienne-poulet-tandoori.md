---
title: 'Indian cuisine: tandoori chicken'
date: '2012-11-14'
categories:
- idee, recette de fete, aperitif apero dinatoire

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/chicken-tandoori-poulet-tandoori-1_thumb.jpg
---
![chicken tandoori chicken tandoori 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/chicken-tandoori-poulet-tandoori-1_thumb.jpg)

##  Indian cuisine: tandoori chicken 

Hello everybody, 

We love chicken at home, and you can find several chicken recipes on my blog: 

[ KFC CHICKEN ](<https://www.amourdecuisine.fr/article-26058908.html>) , the [ CHICKEN WITH MUSTARD ](<https://www.amourdecuisine.fr/article-poulet-a-la-moutarde-60630249.html>) , the [ ROASTED CHICKEN WITH LEMON AND HONEY ](<https://www.amourdecuisine.fr/article-33181249.html>) , the [ CHICKEN BREAKING IN THE OVEN ](<https://www.amourdecuisine.fr/article-chilli-hot-chiken-poulet-piquant-au-four-44410988.html>) , and many other recipes that you will find on the [ category chicken recipes ](<https://www.amourdecuisine.fr/categorie-12364208.html>) .   
This time, if I left for an even more flavored taste, a well-known Indian spice flavor, the tandoori masala spice. 

many people want to know the tandoori masala, which is really a mixture of several spices: coriander, fenugreek, salt, cinnamon, caraway, pepper, onion, ginger, clove, garlic, bay leaf, nutmeg, celery, oil lemon, citric acid, which is finely ground with a mortar. 

this delicious chicken I accompany it often with a delicious [ Indian rice ](<https://www.amourdecuisine.fr/article-un-riz-indien-tres-delicieux-48188385.html>) but as before yesterday I was preparing rice, I preferred to present it light, with fries and a fresh salad. 

so to you this [ Indian recipe ](<https://www.amourdecuisine.fr/article-plats-et-recettes-indiennes-102329306.html>) :   


**Indian cuisine: tandoori chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/poulet-tandoori-chicken-tandoori_thumb1.jpg)

**Ingredients**

  * chicken legs 
  * 6 tablespoon natural yogurt (depending on the amount of chicken) 
  * 3 cloves of garlic 
  * 1 piece of fresh ginger 
  * Salt 
  * 3 c a s full of tandoori powder 
  * the juice of a lemon 



**Realization steps**

  1. Remove the skin from the chicken 
  2. Cut the chickens all over the place so that they absorb the marinade well 
  3. Mix in a large bowl all the yogurts and spices. 
  4. add the yogurt. 
  5. add crushed garlic and ginger. 
  6. marinate the chicken pieces in this mixture of spices. 
  7. Allow to absorb a whole night or half a day 
  8. When grilling in the oven, drizzle with ghee (smen) 
  9. Renew the operation as soon as they leave the oven to give them a little sparkle. 



thank you for your visits. 

bonne journee 
