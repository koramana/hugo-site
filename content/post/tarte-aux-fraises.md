---
title: Strawberry tart
date: '2015-04-21'
categories:
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-aux-fraise2-a_thumb.jpg
---
##  Strawberry tart 

Hello everybody, 

I do not know about you, but here at home it is strawberries at a dream price, and not only in England, apparently even in France, and Lunetoiles did not miss the opportunity to make us this little delight, I swear, if I had not made a fruit salad for dessert today, I would make this recipe, which really makes you want ...  strawberry pie strawberry pie 

so if your strawberry tray is still in the fridge, do not hesitate to concoct this wonder ......  Strawberry tart 

Ingredients: 

[ ![tarte aux-fraise2 a_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-aux-fraise2-a_thumb.jpg) ](<https://www.amourdecuisine.fr/article-tarte-aux-fraises.html/tarte-aux-fraise2-a_thumb>)   


**Strawberry tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-aux-fraise2-a_thumb-290x195.jpg)

Recipe type:  dessert  portions:  8  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** for a pie of 24 cm shortbread : 

  * 120 gr of soft butter cut into pieces 
  * 70 gr of icing sugar 
  * 30 gr of almond powder 
  * 1 pinch of salt 
  * 1 egg 
  * 200 gr of flour 

Almond cream: 
  * 75 gr of soft butter cut into pieces 
  * 75 gr of almond powder 
  * 75 gr of sugar 
  * 5 gr of flour 
  * 2 eggs 

for decoration: 
  * 250 gr of strawberries 
  * 4 c. strawberry jam or jelly 



**Realization steps** Preparation of the dough: 

  1. Mix the butter by adding the icing sugar, then successively the almond powder, the salt, the egg and the flour. 
  2. Form a ball of dough, wrap it in a film of food and let rest at least 2 hours. 
  3. Preheat your oven to 180 ° 
  4. Roll out the dough, put it in your pan and cook for 20 minutes. 
  5. Let cool and reserve. 

Preparation of the cream 
  1. In a salad bowl, work in soft butter cream. 
  2. Add the sugar, the almond powder and the flour. Whip to get a homogeneous mixture. 
  3. Relax this preparation with the eggs that you incorporate one by one. 
  4. Pour on the bottom of cooled dough and bake for 15 minutes. 
  5. Let cool completely. 
  6. Cover with strawberries that have been previously cut and cut in half. 
  7. Melt 4 tablespoons strawberry jam in a saucepan and gently coat the fruit. 
  8. Place in the fridge for 1 hour. 



[ ![tart-with-strawberry-a_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-aux-fraises-a_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-aux-fraises-a_thumb1.jpg>)

Thanks to Lunetoiles for the recipe for this strawberry pie. 

suivez moi sur: 
