---
title: basboussa, semolina cake, Algerian pastry
date: '2012-07-14'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-0082.jpg
---
![Semolina Cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-0082.jpg)

##  basboussa, semolina cake, Algerian pastry 

Hello everybody, 

this is a recipe of my sister, I always like to basque this way, it is very very light, plus a real delight, you imagine. 

**basboussa, semolina cake, Algerian pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/basboussa-a-la-chapelure-008.jpg)

**Ingredients**

  * the composition: 
  * \- 4 eggs 
  * \- 1 glass of sugar 
  * \- 1 glass of milk 
  * \- 1 glass of oil 
  * \- 2 bags of yeast 
  * \- 1 glass and ½ semolina 
  * \- 1 glass of breadcrumbs (or bread that you simply crush) 

for the syrup I did not specify the quantities malgres that I do it visibly, but I pass you my ingredients: 
  * 1 kilo of sugar 
  * 1 liter of water 



**Realization steps**

  1. so we mix everything, and put in a butter mold (25 x 40cm) 
  2. finally, that it is not too big nor too small. 
  3. place in a hot oven and cook. 
  4. do not forget to check the cooking with the tip of a knife. 
  5. at the same time prepare a syrup, and out of the basboussa you water it all hot. the more you water, the more it takes volume. 



let everything cook on a low heat, you can perfume it according to your taste, 

![Basboussa-a-la-breadcrumbs-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/basboussa-a-la-chapelure-1.jpg)

coupez des carres ou des losanges et laisser refroidir. 
