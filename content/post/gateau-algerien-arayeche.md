---
title: algerian cake arayeche
date: '2012-06-14'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/aarayeche-gateau-algerien_thumb1.jpg
---
![aarayeche gateau Algerian](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/aarayeche-gateau-algerien_thumb1.jpg)

##  algerian cake arayeche 

Hello everybody, 

sublime jewels these pretty Algerian cakes, Arayeche, larayeche, or just Arayeche, with that sublime color of frosting that Lunetoiles superbly succeeded, is not it. 

it is a very nice Algerian traditional recipe, timeless, and always present at major festivals and ceremonies to sublimate the guests. 

**algerian cake arayeche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/gateaux-algeriens-Arayeche_thumb1.jpg)

Recipe type:  Dessert  portions:  30  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients** Dough : 

  * 3 measures of flour 
  * 1 measure of melted butter 
  * ½ c. coffee vanilla extract 
  * Water + water of orange blossoms 

The joke : 
  * 3 measures of ground almonds (250 gr) 
  * ¼ measure crystallized sugar (50 gr) 
  * 30 gr of melted butter 
  * Orange blossom water 

The frosting: 
  * 3 egg whites 
  * ½ glass of filtered lemon juice 
  * 1 C. coffee sifted vanilla extract 
  * 2 glass of flower water 
  * 1 tbsp. softened butter 
  * Sifted ice sugar 

The decoration : 
  * Almond paste 
  * Shiny food 



**Realization steps**

  1. \- Prepare the dough: mix the flour and vanilla then add the melted butter and rub well between the palms of the hands. 
  2. \- Sprinkle with water and flower water, then pick up until smooth. 
  3. \- Prepare the stuffing by mixing the ingredients mentioned. 
  4. \- On a floured work surface, lower the dough to 3mm thick, cut rounds of 10 cm diameter and put on the stuffing in the shape of "T". 
  5. -Raise the edges of the dough towards the center in the shape of the stuffing and close them well. 
  6. \- Turn over the cakes then give the shape of  < >. Arrange in a floured dish and bake. 
  7. \- After cooking, let cool and soak in the frosting. 
  8. \- Fix on each cake a flower. Let it dry for several hours. 
  9. \- When the frosting is dry, wipe with food gloss then sprinkle with bright food too. 

The frosting: 
  1. \- Using an electric mixer, beat the egg whites by gradually pouring 1 glass of water of flowers. 
  2. \- Add a little icing sugar always beating 
  3. \- In another container, put the lemon juice, remaining flower water, vanilla extract and butter. 
  4. \- Add a little icing sugar. 
  5. \- Using an electric mixer, beat this last mixture. 
  6. \- Pour this last mixture on the first preparation. 
  7. \- Using the electric mixer, beat another time. 
  8. \- Gradually add the icing sugar and mix with a wooden spoon. 
  9. \- You get a thick frosting. 
  10. \- Glaze the cake and shake it to remove the excess. 
  11. \- To obtain a yellow glaze, add a little yellow food coloring and mix well. 
  12. \- Glaze the cake and let it dry. 
  13. \- To obtain the salmon color, mix the yellow + red dye. 
  14. \- For the green color, mix the yellow + blue dye. 
  15. \- For the purple color, mix the blue + red dye. 
  16. \- For the blue color, add the blue dye. 



Note _Note :_   
to obtain a small amount of frosting, decrease the indicated measures. ![larayeche, Algerian cake in the glaze](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/larayeche-gateau-algerien-au-glacage_thumb1.jpg)
