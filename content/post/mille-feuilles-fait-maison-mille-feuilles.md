---
title: Thousand Leaves Homemade, Thousand Leaves
date: '2016-01-19'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/milles-feuilles.jpg
---
##  [ ![miles](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/milles-feuilles.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/milles-feuilles.jpg>)

##  homemade millefeuilles, mille feuilles 

Hello everybody, 

millefeuilles is a pastry that I like a lot, but unfortunately it's a pastry, which we will not find here in Bristol easily. So, I realize it at home, a thousand homemade leaves, just to find this delicious crunchy taste of the puff pastry which contains a very smooth layer of [ custard ](<https://www.amourdecuisine.fr/article-creme-patissiere.html>) .... yum yum already i just enjoy this recipe. 

[ ![thousand leaf house](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/mille-feuille-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/mille-feuille-maison.jpg>)   


**homemade millefeuilles, mille feuilles**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/mille-feuille-maison.jpg)

portions:  10  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * 3 rolls of puff pastry from the shops 
  * 1 liter of [ custard ](<https://www.amourdecuisine.fr/article-creme-patissiere.html>)
  * the fondant, for me it's sugar pie that I delude in a little water, or coffee, if I want the millefeuille to the taste of coffee.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2009/06/1000f10_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/06/1000f10_2.jpg>)



**Realization steps**

  1. Lower the puff pastry about 3 mm. 
  2. prick it with a fork, so that it does not swell. put it in the oven between two plates, at 180 ° C. 
  3. after cooking and cooling each sheet, spread the pastry cream on the first layer. 
  4. place the second layer of puff pastry on top, decorate once more with a layer of pastry cream.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/milles-feuilles-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/milles-feuilles-1.jpg>)
  5. and finish with a layer of puff pastry. 
  6. Wait until the cake is cool, and spread over the fondant. 
  7. color some of the fondant with a little cocoa. 
  8. make raillures on the cakes, and draw mottles with the tip of a knife. 
  9. put the cake in the refrigerator, and let the cream take well, and the frosting to dry well, to be able to cut the thousand leaves 



[ ![thousand leaves homemade](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/milles-feuilles-fait-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/milles-feuilles-fait-maison.jpg>)
