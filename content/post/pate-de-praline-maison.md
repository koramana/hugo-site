---
title: homemade praline paste
date: '2014-12-28'
categories:
- basic pastry recipes
- sweet recipes
tags:
- Pralin
- Almond
- Algeria
- Algerian cakes
- Cakes
- Pastry
- Based

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-aux-noisettes-maison-1.jpg
---
[ ![Homemade praline paste with hazelnuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-aux-noisettes-maison-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-aux-noisettes-maison-1.jpg>)

## 

Hello everybody, 

Today I share with you a recipe from my readers, which she posted by following the link » [ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.") «, It's the homemade praline paste recipe, or hazelnut praline paste. The recipe comes from Claudine Cook, and this is what Claudine Cook tells you: 

Hello Soulef, I am Claudine Cook, I am one of your faithful readers. I started following your blog, since I got married with "Djamel" who is of Algerian origin. Besides it is thanks to him that I know your blog, he wanted me to make a recipe, and looking for it on google, we came across your blog. Since then, it's always with you that I come to recharge my batteries. I would like to share with you the photos of my homemade praliné paste that I hold from your blog, it was in the recipe of a log trianon way, that I realize often, not for the new year, we do not celebrate my husband and me. But I find that the log is faster to make, the cut is very pretty, and especially that I have a gutter mold that I never use. Thank you for your sublime blog, there is everything in it 

Thank you my dear Claudine Cook for this nice testimony, always be welcome on the blog, and thank you for the recipe. 

**homemade praline paste**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-aux-noisettes-maison-1.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 500 g almonds (I only had crushed almonds) 
  * 500 g of hazelnuts 
  * 500 g sugar 
  * 120 ml of water 



**Realization steps**

  1. In a large skillet, roast the dried fruit for about 10 minutes, stirring occasionally. Book. 
  2. In another pan (large enough), pour in the sugar and wet with water. 
  3. Cook without touching until the mixture is liquefied and lightly colored. 
  4. Add the roasted dried fruit mix and mix quickly: the caramel whitens and recrystallizes; it is said that it "sand" or that it "mass". 
  5. Cook over medium heat and caramelize for a few more minutes: the darker the caramel, the more the praline will be strong in taste. 
  6. Once the desired color has been obtained, remove from heat and allow to cool completely.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-maison-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-maison-1.jpg>)
  7. Place the mixture in the mixer bowl.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-maison.jpg>)
  8. for grains, mix quickly 
  9. a little more, and we get a powder   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-aux-noisettes-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-aux-noisettes-maison.jpg>)
  10. mix even more, and we get a paste 
  11. Store the praline grains and powder in an airtight container upside down at room temperature. 
  12. The dough itself is kept in the refrigerator. 



[ ![hazelnut praline paste house-001](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-aux-noisettes-maison-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-de-praline-aux-noisettes-maison-001.jpg>)

For the recipe of [ pralin aux noisettes maison ](<https://www.amourdecuisine.fr/article-pralin-aux-noisettes.html> "praline with hazelnuts")
