---
title: soup butternut squash carrots sweet potato
date: '2016-11-18'
categories:
- Non classé
tags:
- Pumpkin soup
- Velvety
- Algeria
- la France
- Healthy cuisine
- Soup
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/soupe-butternut-carottes-patate-douce-1.jpg
---
![soup butternut squash carrots sweet potato](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/soupe-butternut-carottes-patate-douce-1.jpg)

##  soup butternut squash carrots sweet potato 

Hello everybody, 

The cold begins and frankly there is no better than a soup velvety to warm up the stomach ... This soup squash butternut carrots sweet potato is a delight.It must be too good. 

Thanks to Lunetoiles who reminded me that I had not posted this recipe she sent me a while ago, yes it is the season in addition to these delicious sweet sweet potatoes. This butternut (carrot butternut) soup carrots and sweet potato reminds me a lot of [ pumpkin cream ](<https://www.amourdecuisine.fr/article-creme-de-citrouille-rotie.html>) that I like to do often, because I start by baking the ingredients in the oven.   


**soup butternut squash carrots sweet potato**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/soupe-butternut-carottes-patate-douce-3.jpg)

**Ingredients**

  * 1 large butternut or 2 medium (my own garden was very big but it must fill your oven plate) 
  * 3 sweet potatoes 
  * 7 carrots of medium size 
  * 3 large potatoes 
  * 3 onions 
  * coarse salt, black pepper 
  * olive oil 
  * water 



**Realization steps**

  1. Preheat the oven to 180 ° C - 190 ° C in the preferred heat mode. 
  2. Put whole sweet potatoes without peeling them on a plate lined with baking paper and cook in the hot oven for about 1 hour, plant a knife, if they sink easily it is cooked. 
  3. Using a knife remove the skin from the butternut squash and remove the seeds. Cut into not too small pieces and transfer to a baking sheet. 
  4. Cut the onions into fairly large strips and transfer them with the butternut on the plate, sprinkle with coarse salt and sprinkle with 3 tbsp. olive oil, mix well and bake in the hot oven, at the same time as the sweet potatoes that are already in the oven, it will not be a problem, but put the butternut plate at the top of the oven and leave roast the onions and squash about 30 minutes or so, stirring occasionally. 
  5. The squash and onion slivers must be caramelized. 
  6. Remove the oven when the whole is well roasted. 
  7. In large pot, cook potatoes and carrots peeled and cut into pieces of salt water with salt. 
  8. Cook until carrots are cooked through and cut easily with a knife if squeezed. 
  9. Meanwhile remove the skin of the sweet potatoes that goes away on its own.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/soupe-butternut-carottes-patate-douce.jpg)
  10. Once the potatoes and carrots are well cooked, add all the other vegetables to the pot, butternut, onions and sweet potatoes. 
  11. And add a little more water if there was not enough, the water should be at the height of the vegetables. 
  12. At this time correct the seasoning, add a little more coarse salt, a little black pepper if you wish. 
  13. You can put branches of thyme to perfume. 
  14. Simmer for 15 to 20 minutes after boiling. 
  15. Let cool. 
  16. Using a dipping mixer, mix the soup a long time until they are creamy. 
  17. Add about 5 tablespoons of olive oil while mixing. 
  18. Serve hot. 


