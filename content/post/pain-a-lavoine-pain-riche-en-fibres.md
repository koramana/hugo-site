---
title: oatmeal bread / high fiber bread
date: '2013-03-16'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pain-d-avoine-1.jpg
---
![bread-and-oats-.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pain-d-avoine-1.jpg)

Hello everybody, 

Here is a recipe out of the ordinary, a recipe of bread, full of grains, a bread rich in fiber, it is a recipe that shares with us Lunetoiles, and I promise you, as soon as I come across all the ingredients, I will surely try this recipe, 

this bread consists of: 

Sun-flower seeds  : who are able to reduce appetite and some weight loss diets actively use it as a snack. Thus, these dietary supplements provide the necessary fats and limit fat in the diet without compromising health. 

linseed  : They have omega-3s that are polyunsaturated fatty acids, they help reduce bad cholesterol, reduce the risk of cardiovascular disease and other heart diseases (such as coronary heart disease), they prevent the risk of cancer, they play a role in the construction of the cell membrane and more. 

oatmeal  : Are a complete food, consisting of 3 major families of nutrients: low GI carbohydrates, good quality fats and incomplete aminogram proteins. It also contains fiber, vitamins and minerals. 

chia seeds  : Contain a record rate of omega 3. They are so rich in alpha-linolenic essential fatty acid that only 10 grams are enough to meet your daily needs of omega-3. Chia seeds absorb up to 14 times their weight in water, which allows them to slow the absorption of sugars in the body when they are consumed together with sweet products 

psyllium seeds  : Psyllium or ispaghul is one of the medicinal plants used by Egyptians, more than ten centuries BC, for their laxative virtues. Traditional Indian medicine uses psyllium to relieve inflammations of the urinary and digestive tract, and during acute diarrhea. It is also used in kidney disease, difficulty urinating, duodenal ulcers and hemorrhoids. Psyllium sound is also proposed as an appetite suppressant in slimming diets. 

Here is what I could find on these grains, during a small search on google. 

when at Lunetoiles, she says, that although this bread contains all these seeds, but in fact it is very mellow, and that to have the crisp effect, it will be better to grill the slices to the toaster. this bread is super delicious nature, breakfast or tasting ... 

otherwise you can use it as a sandwich with slices of chicken and salad. as it will also be very good to taste it spread of a beautiful layer of goat and honey ... most importantly, you eat two slices, and you are well stalled .... 

![bread-has-the-oats - bread-to-grains.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pain-a-l-avoine-pain-aux-grains1.jpg)

Ingredients: 

  * 135 g of sunflower seeds 
  * 90 g of flax seeds 
  * 65 g of hazelnuts or almonds 
  * 145 g oat flakes 
  * 2 tbsp. chia seeds 
  * 4 c. psyllium seeds (3 tbsp if using psyllium powder) 
  * 1/2 c. fine salt 
  * 1 C. acacia honey (for diets without sugar, use a pinch of stevia) 
  * 3 c. coconut oil or melted clarified butter 
  * 350 ml of water 



method of preparation: 

  1. In a bowl, mix all ingredients except water, stirring well. 
  2. Add to this the water and mix well until everything is completely soaked and the dough becomes very thick (if the dough is too thick to stir, add one or two teaspoons of water until the dough is manageable). 
  3. Pour everything into a cake tin, lined with parchment paper or a silicone bread pan. 
  4. let the dough rest in the mold for 2 hours or better, all day, or overnight. 
  5. Preheat the oven to 175 ° C. 
  6. Put in the oven on the middle rack and bake for 20 minutes. 
  7. Remove the bread from the parchment dish, place it upside down directly on the grill and cook for 30-40 minutes. The bread is ready when it rings hollow when you tap it. Let cool completely before slicing (difficult, but important). 
  8. Keep the bread in an airtight container for up to five days. Freezes very well too - slice before freezing for quick and easy toast. 



![bread-to-noisettes.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pain-aux-noisettes1.jpg)
