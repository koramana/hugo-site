---
title: Shortbread tart with apricot and almonds
date: '2017-06-26'
categories:
- Unclassified
tags:
- Breakfast
- desserts
- To taste
- Fruits
- Cakes
- Fruit tart
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/06/Tarte-sabl%C3%A9e-a-labricot-et-amandes-1-763x1024.jpg
---
![Shortbread tart with apricot and almonds 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/Tarte-sabl%C3%A9e-a-labricot-et-amandes-1-763x1024.jpg)

##  Shortbread tart with apricot and almonds 

Hello everybody, 

We take advantage of the apricot season to make this delicious tarte shortbread with apricot and almonds, a recipe to enjoy without moderation. 

Thank you very much to Lunetoiles for this beautiful and very good recipe that she made especially for the blog, thank you very much little sister. 

And do not forget, we take advantage of this very generous fruit rich in Vitamins A and C, not to mention the minerals: **potassium, iron, copper and magnesium** . 

I also invite you, if you have not done so, to enjoy this season to come to participate in our game: [ Recipes around an ingredient # 30 apricot ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-30-labricot.html>) which I am the godmother, you have until July 15 to participate in this round.   


**Shortbread tart with apricot and almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/Tarte-sabl%C3%A9e-a-labricot-et-amandes-3.jpg)

portions:  8  cooking:  40 mins  total:  40 mins 

**Ingredients**

  * a pastry dough shortbread or commercial 
  * fresh and ripe apricots 
  * 2 eggs 
  * 120 gr of powdered sugar 
  * 20 cl whole cream 
  * homemade marzipan 
  * a handful of golden flaked almonds 



**Realization steps**

  1. Preheat oven to 180 ° C rotating heat 
  2. Wash and gently dry the apricots. Then cut them in half, removing the core. Put aside. 
  3. Garnish your pie pan with shortcrust pastry, grate the almond paste and put a good layer on the pie shell. 
  4. Arrange the half apricots, curved side down. 
  5. In a large bowl, beat the eggs and sugar, add the liquid cream and mix well to obtain a homogeneous mixture. 
  6. Pour over the apricots, distributing the mixture well evenly. 
  7. Sprinkle with flaked almonds. 
  8. Bake for 35 to 45 minutes while watching. 
  9. The pie must be golden brown, the cream must be colored and browned and the apricots must be golden brown. 
  10. Let cool completely before placing in a cool place. 
  11. Serve well chilled, and possibly with a scoop of ice cream. 



![Shortbread tart with apricot and almonds](https://www.amourdecuisine.fr/wp-content/uploads/2017/06/Tarte-sabl%C3%A9e-a-labricot-et-amandes-774x1024.jpg)
