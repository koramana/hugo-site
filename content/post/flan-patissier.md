---
title: Custard pastry
date: '2012-12-11'
categories:
- cuisine algerienne
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-au-flan-2-copie-11.jpg
---
![custard pastry](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-au-flan-2-copie-11.jpg)

##  Custard pastry 

Hello everybody, 

who does not like this [ delicious pastry ](<https://www.amourdecuisine.fr/categorie-12347071.html>) , a flan light and seculant vanilla flavors very rich, that pastry patisserie, which we love so much at the local patissier, but you know you can do it yourself !!! Eh yes. 

**Custard pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-au-Flan-11.jpg)

portions:  12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * puff pastry 
  * 1 L of semi-skimmed milk 
  * 3 eggs 
  * 160 g of sugar 
  * 100 g cornflour 
  * 2 bags of vanilla sugar 
  * 3 tablespoons vanilla extract 
  * ½ teaspoon vanilla powder (or a vanilla bean) 



**Realization steps**

  1. With an electric whisk, beat the eggs with cornstarch, vanilla extract and 125 ml of milk (taken from the liter of milk of the recipe) in a bowl. 
  2. Pour the remaining milk into a saucepan, with the vanilla sugar, sugar, vanilla powder (or the vanilla pod split in half lengthwise and scraped). 
  3. Bring to a boil. 
  4. Pour boiling vanilla sweet milk over egg mixture while beating (whisk). 
  5. Pour everything back into the pan 
  6. return to very low heat, stirring constantly with a wooden spatula. 
  7. To simmer a few seconds, no more, it is necessary that the preparation is slightly thickened. 
  8. Pour the mixture over the dough (removing the vanilla pod if you put one) 
  9. stuck in the mold and bake for 40 minutes at 180 ° C. Cover with aluminum foil if necessary the last 5 minutes. 
  10. Allow to cool completely before unmolding and cutting. Keep refrigerated. 



![tarte au flan-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tarte-au-Flan-11.jpg) ![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
