---
title: algerian cake / makrout elouz
date: '2012-11-30'
categories:
- cakes and cakes
- recettes sucrees

---
Hello everybody, 

makrout el louz, makrout ellouz, or makrout louz, is a very delicious Algerian cake based on almonds, which I personally like a lot, but I do not really like to do it because I'm so greedy that I can eat a whole platter in one day. 

so the best I do not cook, but that does not mean that I do not share this recipe with my lovely readers, ok. 

so for the el adha this year I made this delicious cake, well melted in the mouth. 

  * 500 gr of finely ground almonds 
  * 200 gr of caster sugar 
  * a few drops of almond extract 
  * Zest of 1 lemon 
  * 3 or 4 eggs (all depends on the size of the egg, and if the dough gets well) 



for the syrup: 

  * 300g caster sugar 
  * 150 ml of water 
  * 2 tablespoons of orange blossom water 



Finishing: 

Icing sugar 

method of preparation: 

  1. Preheat the oven to 180 ° C. and cover with a sheet of baking paper. 
  2. In a large salad bowl, mix the ground almonds with the icing sugar and the lemon zest 
  3. add the aroma of almonds (I confess that I put a lot, because I like the bitter taste of the almond, covered with a layer of sugar) 
  4. Add the eggs one by one, mix well with each addition to see if the dough gets well, which should be able to roll into a ball, and be well homogeneous, without sticking too much, or being too dry. 
  5. Lightly flour the worktop. Shape a 3 cm pudding with the paste obtained and cut diamonds. 
  6. Place the diamonds on the hob and bake for about ten minutes. 
  7. Meanwhile, prepare the syrup in a small saucepan by mixing the water with the sugar. Bring the pan on the heat and allow to thicken slightly. Add the orange blossom water towards the end and remove from the heat. Let cool. 
  8. Once the cakes are cooked, let them cool and then dip them in the cold syrup. 
  9. leave the few moments in the syrup, then drain on a rack. 
  10. Place the diamonds on a tray and sprinkle with icing sugar. 
  11. then take a bag in which you put icing sugar, and dip makrouts one by one, while pressing on the cake, to coat it with the icing sugar on all sides. 
  12. then tap a little to remove the excess, and place the pieces of cake on another tray so that the sugar dries and adheres well to the cake. 
  13. to avoid imprints when presenting the cake to the guests, with the help of a brush, gently pat the surface of the cake with the hair of the brush, and you will have this side sprinkled with the cake. 
  14. Deposit in boxes before serving. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
