---
title: algerian cakes with almonds
date: '2017-06-20'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian cakes
- Cakes
- Algeria
- Aid cake
- baklava
- Ramadan 2017
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/dziriette-en-fleur-007_thumb_1.jpg
---
##  algerian cakes with almonds 

###  the Rosaces, Dziriettes el warda, دزيريات الوردة 

Hello everyone, 

again, I'm coming back for you post one of the recipes from [ my cakes ](<https://www.amourdecuisine.fr/article-gateaux-algeriens-pour-l-aid-2010-56806564.html>) that I prepared during the festival of the Aid, this time these Dziriettes in the shape of flowers, a very pretty [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) . 

the first time I saw this recipe, it was one of my readers Lila who had sent me the photos, so I absolutely wanted to try the recipe. 

for even more recipes from [ Algerian cakes, the index ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) is super well done, a click on the photo of the choice, takes you to the recipe directly. 

{{< youtube jVCjq4DWk_s >}} 

The recipe in Arabic: 

**Algerian cake / almond biscuits**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/dziriette-en-fleur-007_thumb_1.jpg)

Recipe type:  Algerian cake  portions:  50  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** for the pasta 

  * 3 measures of flour 
  * ½ measure of melted margarine 
  * 1 pinch of salt 
  * ½ C.a vanilla extract coffee 
  * 3 C.a orange blossom water soup 
  * water needed 

for the stuffing: 
  * 6 measures of finely ground almonds 
  * 2 sugar measures 
  * 1 C.a coffee full of lemon zest 
  * 2 to 3 eggs by size (or until wet stuffing) 
  * 1 teaspoon of vanilla extract 



**Realization steps** start with the pasta: 

  1. work sifted flour with melted margarine salt vanilla extract .mow with orange blossom water and necessary water. 
  2. work with the apple of your hands until you have a smooth, firm dough.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/dziriettes-en-fleur-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/dziriettes-en-fleur-.jpg>)
  3. cover with foil and leave to rest. 
  4. during this time prepare the stuffing with the indicated ingredients. 
  5. knead with your hand for 10 to 15 minutes to obtain a fairly soft and well-aerated composition. 
  6. take the dough on a floured surface and using a rolling pin spread it out very thin and then go to the machine n 1 then n 3 and then n 5 and cut out discs 9 cm in diameter using a cookie cutter. 
  7. place your dough in special boxes 
  8. put the stuffing in and take a bottle cap tilt and try to make traces to give the shape of a flower.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/dziriettes-en-fleurs_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/dziriettes-en-fleurs_thumb.jpg>)
  9. continue until the dough is finished, decorate with an almond. 
  10. place in a dish and bake in preheated oven for 15 to 20 minutes. 
  11. after cooking demold and soak them in the honey for 10 minutes then put in boxes. 


