---
title: Apple crumble pie
date: '2015-09-30'
categories:
- sweet recipes
- pies and tarts
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-pommes-crumble-1.jpg
---
[ ![apple crumble pie 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-pommes-crumble-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-pommes-crumble-1.jpg>)

##  Apple crumble pie 

Hello everybody, 

Apples are always in season, and a dessert with apples is always a delight we love to enjoy. So what do you like to do with your apples? [ Apple pie ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-au-beurre-67056341.html>) ? or one [ Apple crumble ](<https://www.amourdecuisine.fr/article-crumble-aux-pommes.html>) ? What do you say about both at the same time? 

it's like a [ apple pie ](<https://www.amourdecuisine.fr/article-apple-pie-dautomne-au-caramel-au-beurre-sale.html>) but with a crispy cover and crumbly yum yum, are we going for the apple crumble tart recipe? 

A recipe from my friend Fleur Dz from Canada, which gives the appointment: apple pie pie, for this delight, because in Canada, they use the word crisp instead of crumble   


**Apple crumble pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-aux-pommes-crumble-203x300.jpg)

portions:  8  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * A drop of [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>) for a mold 26 cm in diameter 

For the apple filling 
  * 310 ml of apple sauce. 
  * 30 g of Maïzena around two and a half tablespoons 
  * Three beautiful medium sized apples cut into slices. 
  * 120 g of caster sugar. 

for the crumble: 
  * 65 g white flour. 
  * 55 g of brown sugar. 
  * Two tablespoons of almond powder. 
  * A tablespoon almond crushed coarsely. 
  * 4 to 5 tablespoons melted butter. 



**Realization steps** for the apple filling 

  1. On a low heat boil all the ingredients of the pie shell so the first four ingredients, to melt the sugar and then set aside. 

For your crumble 
  1. In a salad bowl, mix the dry ingredients of your crumble and add the butter 
  2. until the mixture becomes moist. 

Mounting 
  1. Go through a 26-inch diameter pan with your shortcrust pastry, poke the bottom well 
  2. spread the apple mixture 
  3. cover it with the crumble. 
  4. Cook in an oven preheated to 180 degrees C, until the pie takes a nice 
  5. golden color about 45minutes. 
  6. Serve warm. 



[ ![apple pie crumble 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-pomme-crumble-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/tarte-pomme-crumble-2.jpg>)
