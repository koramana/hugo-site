---
title: strawberry jam lightened
date: '2015-04-24'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/confiture-de-fraises-allegee.CR2_1.jpg
---
![jam-de-bur allegee.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/confiture-de-fraises-allegee.CR2_1.jpg)

##  strawberry jam lightened 

Hello everybody, 

What I liked strawberries that my husband to buy this week, it was beautiful small strawberries, very very delicious and succulent .... 

We loved it so much that my husband went to buy some again, the children used it without depriving themselves, but I had the impression that the strawberries doubled as and when, hihihiih or maybe by what I do not I do not like that a whole shelf of my fridge, be dominated in this way by a bass drum of fruits ....  Strawberry jam 

![jam-de-strawberry-fat-reduced - 019.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/confiture-de-fraises-allegee-019.CR2_1.jpg)

**strawberry jam allegee**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/confiture-de-fraises-allegee-019.CR2_1.jpg)

portions:  10  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 400 gr strawberry hulled 
  * 180 gr of sugar 
  * 1 knob of butter 



**Realization steps**

  1. macerate strawberries and sugar overnight (about 12 hours). 
  2. in the morning, bring the mixture strawberry and sugar and the butter nut (it prevents the white foam is formed) to boil and maintain for 20 minutes stirring regularly. 
  3. After 20 minutes of cooking, lower the heat a little and test the cold plate to see if your jam is cooked by pouring a drop into a cold plate if it freezes immediately; your jam is ready. 
  4. preserve your jam, in jars of jam previously scalded and dried, 


