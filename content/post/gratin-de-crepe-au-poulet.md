---
title: chicken pancake gratin
date: '2017-02-09'
categories:
- crepes, gauffres, beignets salees
- cuisine algerienne
- cuisine marocaine
- recettes a la viande de poulet ( halal)
tags:
- Oven cooking
- Easy cooking
- Algeria
- Full Dish
- dishes
- Healthy cuisine
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gratin-de-cr%C3%AApes-au-poulet-1.jpg
---
[ ![chicken pancakes gratin 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gratin-de-cr%C3%AApes-au-poulet-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gratin-de-cr%C3%AApes-au-poulet-1.jpg>)

##  chicken pancake gratin 

Hello everybody, 

You like crepes and pancake recipes, a very good chicken pancake gratin that will call you for a moment. 

**chicken pancake gratin**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gratin-de-cr%C3%AApes-au-poulet-.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** For pancakes (Pierre Hermé recipe): 

  * 4 eggs 
  * 200 g flour 
  * 5 g of salt 
  * 20 g of melted butter 
  * ½ l whole milk 
  * 6 cl + 2 tablespoons of water 
  * salt and pepper 

For the [ chicken broth ](<https://www.amourdecuisine.fr/article-bouillon-de-poulet-fait-maison.html>) :   
Lunetoiles has the same recipe as mine, but she added a few brunoises of vegetables (carrot and a little celery, but we can put leek white too that will go well with this recipe) 

  * pieces of chicken breasts with bone 
  * 1 onion 
  * 1 carrot 
  * 2 branches of celery 
  * salt and pepper 
  * 1 bay leaf 
  * branches of thyme 
  * 1 liter and a half of water 

for bechamel 
  * 60g of butter (or margarine) 
  * 60 g flour 
  * salt pepper, 
  * powdered cumin 
  * 70 CL chicken broth (if not milk) 
  * one or two pieces of cheese spread (like laughing cow) 



**Realization steps** Prepare the pancakes. 

  1. Whisk the eggs in a container. 
  2. Sift the flour directly over it and mix. Add salt and melted butter. 
  3. Mix with the milk and 6 cl of water. 
  4. Let stand 2 hours at room temperature. 
  5. Once your dough is resting start preparing your pancakes, in a special pancake pan (about twenty centimeters) hot. 
  6. Pour a ladle, tilting the pan. 
  7. Once your pancakes are ready, cover them.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/crepes-de-pierre-herm%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/crepes-de-pierre-herm%C3%A9.jpg>)

crepes of stone 


prepare the chicken broth: 
  1. Put the pieces of chicken breast with the bone in a pot, with all the other ingredients of the chicken broth. 
  2. And cook boiling for about 1 hour on a low heat. 
  3. Leave to cool. 
  4. Then remove the chicken, and remove the bay leaf and thyme branches or pass everything to the Chinese. 
  5. Keep 70 cl of chicken broth to make the bechamel with, and what you have left, keep it cool in a tightly sealed jar.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bouillon-de-poulet-et-l%C3%A9gumes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bouillon-de-poulet-et-l%C3%A9gumes.jpg>)

prepare the béchamel: 
  1. Melt the butter, add the flour, salt, pepper and cumin powder. 
  2. Gradually pour in the chicken broth (or milk) until the béchamel is thick, add the last portion cheese to soften it. 

mounting: 
  1. at this point, take a bit of bechamel which will be used to cover the whole of the pancake gratin. 
  2. Crumble the chicken, mix it with bechamel. 
  3. You can add hash browned mushrooms.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/b%C3%A9chamel-au-poulet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/b%C3%A9chamel-au-poulet.jpg>)
  4. Stuff your pancakes, arrange them in a buttered oven dish, and pour a little béchamel that you will reserve, sprinkle with grated cheese,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/montage-du-gratin.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/montage-du-gratin.jpg>)
  5. then do au gratin! 
  6. let cool a little before serving, and enjoy!   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gratin-de-cr%C3%AApe-au-poulet-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gratin-de-cr%C3%AApe-au-poulet-2.jpg>)

chicken pancake gratin 2 




[ ![chicken pancake gratin 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gratin-de-cr%C3%AApe-au-poulet-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gratin-de-cr%C3%AApe-au-poulet-3.jpg>)
