---
title: Baked chicken legs with mustard
date: '2014-01-14'
categories:
- bakery
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/haricots-verts-saut%C3%A9s-%C3%A0-lail-et-persil-2.jpg
---
![sautéed green beans with garlic and parsley 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/haricots-verts-saut%C3%A9s-%C3%A0-lail-et-persil-2.jpg)

##  Baked chicken legs with mustard 

Hello everybody, 

of the  **chicken legs with mustard in the oven** ç  Are you tempted? Yes you read well, with a nice touch of mustard, but in addition to the cooking bags, so not even a dish of gratin washing 100 degrees to be as clean as ever, hihihihihi 

a very easy recipe is very good  baked chicken legs  , with a nice touch of mustard, you can use chicken without the skin, or with the skin, you can do cooking, in special baking bags, as I did on the picture above, or then in a cooking pot with baking lid (if you want to do more dishes, hihihihi)   


**Baked chicken legs with mustard**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/poulet-a-la-moutarde-3_thumb.jpg)

Recipe type:  chicken recipe  portions:  4  Prep time:  15 mins  cooking:  60 mins  total:  1 hour 15 mins 

**Ingredients**

  * chicken thighs 
  * 2 tbsp. mustard soup 
  * 2 tablespoons of olive oil 
  * salt, black pepper, ground coriander seeds 
  * 2 cloves of garlic 
  * ½ onion 
  * 2 bay leaves 
  * a little thyme 
  * 1 coffee of paprika 



**Realization steps**

  1. in the pot mix all your ingredients, 
  2. after cleaning the chickens, marinate it with this spice mixture for a short time,   
if you use the cooking bags add, 2 tablespoons water, if you use a cooking pot, add half a glass of water,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/poulet-a-la-moutarde-4_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/10/poulet-a-la-moutarde-4_thumb.jpg>)
  3. bake for 45 minutes to 1 hour, depending on your oven. 
  4. at the end, remove the chicken pieces and roast a little to get a nice color. 
  5. serve with fries, the sauce is a real delight, it can even be served with [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html> "mashed potatoes") . 



Oven baked chicken thighs with mustard  Ingredients 

  * Chicken thigh 
  * 2 tsp mustard 
  * 2 tablespoons olive oil 
  * salt, black pepper, ground coriander 
  * 2 cloves of garlic 
  * ½ onion 
  * 2 bay leaves 
  * thyme 
  * 1 teaspoon paprika 

method of preparation  أفخاد الدجاج بالماسترد في الفرن  المكونات  أفخاذ دجاج  *  ملعقتي خردل *  2 ملاعق طعام من زيت الزيتون *  الملح والفلفل أسود, وبذور الكزبرة المطحونة *  2 فصوص من الثوم *  *  ½ البصل *  2 أوراق الغار *  القليل من الزعتر *  1 ملعقة الفلفل الحلو *  طريقة التحضير 

في وعاء نخلط جميع المكونات *    
بعد تنظيف قطع الدجاج, ينقع مع هذه التوابل لمدة من الوقت *    
2ا كنت تستخدم أكياس الطبخ أضيفي 2 ملاعق طعام من الماء, وإذا كنت تستخدم وعاء, وإضافة نصف كوب من الماء *    
تخبز لمدة 1 ساعة 45 دقيقة, على حسب الفرن الخاصر بك * 

في نهاية, أزيلي قطع الدجاج وضعيها تحمر قليلا * 

[ ![Baked chicken legs with mustard](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/poulet-a-la-moutarde_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/10/poulet-a-la-moutarde_2.jpg>)

J’espère que cette recette de Cuisses de poulet a la moutarde au four vous plaise, si vous l’essayez vous me dites votre avis? 
