---
title: streusel cocoa and caramelized hazelnuts
date: '2011-08-28'
categories:
- cuisine algerienne
- cuisine tunisienne
- rice

---
& Nbsp; hello everyone, I'm in the middle of preparing cakes for the party of the aid like any woman who will help, and I do not tell you until what time I watch, it's bad, and no time to write a recipe, although I have a lot in my reserve, anyway, for today I saw a post a nice recipe for Luente, she sent me for some time, from its appearance is a very good cake, try surely: & nbsp; Streusel: 50 g of sugar 50 g of diced butter & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.4  (  1  ratings)  0 

Hello everybody, 

I am in the middle of preparing cakes for the feast of the aid like any woman who will celebrate the help, and I do not tell you until what time I watch, it's meccant ca, and no time for you write a recipe, despite that I have many in my reserve, anyway, for today I saw a post a beautiful recipe for Luente, she sent me for some time, its appearance, it is a very good cake, to try surely: 

Streusel: 

50 g of sugar 

50 g butter cut in cold dice   
50 g of almond powder 

50 g of flour   
20 g of bitter cocoa 

**NOT** caramelized birds   
80 g of sugar 

2 tbsp. to s. water   
100 g of hazelnuts 

Chocolate cake : 

160 g of dark chocolate 

125 g of butter 

4 eggs 

120 g of sugar 

90 g of almond powder 

50 g of flour 

Prepare the streusel: 

Sand all with your fingertips as for a crumble dough. Set aside the time to prepare the cake. 

Prepare the caramelized hazelnuts: 

  1. In a small skillet, pour in the sugar and water. 
  2. Cook over medium heat without touching until golden blond toffee. 
  3. Add the hazelnuts, mix to coat and pour over a sheet of parchment paper. 
  4. Let cool completely. Crush with a knife. 



Prepare the cake: 

  1. Melt chocolate and butter in a bain-marie or microwave. 
  2. Add 90 g sugar, almond powder, flour and egg yolks. 
  3. Raise the egg whites to snow, mix them with the remaining sugar and then gently add them to the dough. 



Adding the sugar to the white and incorporating a meringue into the dough, gives a crisp crust to the cake ......... at least the first hours. 

But you can put all the sugar in the dough and add the whites up with just a pinch of sugar. 

Pour the dough into a buttered baking pan or silicone. Spread over the streusel and the hazelnuts. 

Bake at 180 ° C for 15 minutes, then reduce to 160 ° C and continue cooking for 30 minutes. 

Let cool completely on a baking rack before unmolding. 

thank you my dear Lunetoiles for this nice cake: 

thank you for your comments and visits 

thank you to all those who love my blog, and subscribe to the newsletter, if you also like my blog, add it to your list of favorites, or so subscribe to the newsletter, and if you want to be up to date with my publication, tick the box "Notifications of article publication" 

merci et bonne journee. 
