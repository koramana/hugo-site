---
title: Mascarpone / strawberry roll
date: '2013-09-08'
categories:
- panna cotta, flan, and yoghurt

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/roule-mascarpone-fraise-21.jpg
---
#  ![rolls strawberry mascarpone](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/roule-mascarpone-fraise-21.jpg)

Hello everybody, 

here is a very good recipe rolled, which is going to be a good base for a beautiful log, you just have to make a small decoration with a nice butter cream, or then chocolate cream, and the trick is to play. 

this roll is super ventilated, and very very light. I highly recommend it. here is the recipe and I will try to give you the details and tips as best as I can.   


**Mascarpone / strawberry roll**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/roule-mascarpone-fraise-11.jpg)

Recipe type:  dessert, taste  portions:  12  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 5 eggs (for a tray of 30 cm by 40cm) 
  * 140 grams of sugar 
  * 150 grams of flour 
  * 1 cup of vanilla coffee 
  * 1 cup of baking powder 
  * 1 pinch of salt 
  * 2 drops of vinegar (to avoid the smell of eggs) 

garnish: 
  * 250 grs of mascarpone 
  * 100 grams of sugar 
  * 50 grs of cut strawberries (optional) 



**Realization steps**

  1. prepare your ingredients, and start by beating the eggs and the foam sugar, to have a good foamy mixture, if you use the whisk of a kneader, it will give a beautiful foam, otherwise if you use an electric whisk like mine , tip the whisk as shown in the photo to aerate the mixture well, do it for 8 to 10 min, until the egg sugar mixture, triple volume. 
  2. add vanilla and vinegar, 
  3. then add the mixture flour and baking powder, very slowly spoon per spoon, without breaking the foamy mixture of eggs, 
  4. mix with a spatula while aerating the mixture. 
  5. cover the tray with a baking paper, and oil a little, and pour gently your mixture.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/2010-06-14-rouler_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/09/2010-06-14-rouler_thumb1.jpg>)
  6. bake in a hot oven, 180 degrees for 10 minutes, or depending on the capacity of your oven. 
  7. remove from the oven and let cool completely (to have a good roll does not allow to cook too much) 
  8. prepare a piece of aluminum foil, very wide, and after total cooling of the cake roll it in aluminum. and leave out. 
  9. prepare the filling by whisking the mascarpone with the sugar. cut the strawberries into pieces and set aside. 
  10. roll out the cake, cover it with the mascarpone cream and scatter the strawberries over, roll the cake again, and set aside. 
  11. let take a little, decorate with icing sugar if you like and cut between 2 cm and 2,5cm 



good tasting. 

si vous aimez mes recettes, n’oubliez pas de vous abonner a ma newsletter. 
