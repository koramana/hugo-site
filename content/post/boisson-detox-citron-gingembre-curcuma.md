---
title: detox drink lemon ginger turmeric
date: '2017-01-13'
categories:
- boissons jus et cocktail sans alcool
- Cuisine détox
- Cuisine saine
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen
- Waters

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/boisson-detox-au-citron-et-gingembre.jpg
---
[ ![detox drink with lemon and ginger](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/boisson-detox-au-citron-et-gingembre.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/boisson-detox-au-citron-et-gingembre.jpg>)

##  detox drink lemon ginger turmeric 

Hello everybody, 

Never say that I do not need a detox drink, a detox drink is not for you to lose weight, but rather to clean the liver ... We do not stop eating dishes and recipes rich in fat whether it's good for health or bad, and there's only one organ in our body that takes care of all the flitrage, it's the liver. Know that you can do all the diets of the world, without losing weight and this because of the liver that does not work properly because it is well suffocated by all this fat. Cleanse the liver is essential even for lean people, so do not think because you are lean, that your body does not need you to take care of him. 

Lemon is an excellent source of vitamin C, and it contains flavonoid compounds that have antioxidant and anti-cancer properties. 

Ginger is a powerful anti-inflammatory that gives protection against cancer, as it gives a boost to your immune system. 

Turmeric, it is the family of ginger, it is also an excellent anti-inflammatory. It protects the cardio system, and helps lower the bad cholesterol. It protects against Alzheimer's disease, and it can help prevent certain types of cancer, and studies have shown that it inhibits the growth and metastasis of cancer cells (which means it keeps cancer from spreading ). 

**detox drink lemon ginger turmeric**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/boisson-detox-citron-et-gingembre.jpg)

portions:  2  Prep time:  5 mins  cooking:  10 mins  total:  15 mins 

**Ingredients**

  * 550 ml of boiling water 
  * 1 organic lemon cut into a slice 
  * 2 cm peeled and sliced ​​ginger 
  * ⅛ c. coffee turmeric powder 
  * 2 tbsp. honey coffee (optional) 



**Realization steps**

  1. Bring the water to a boil. 
  2. remove from heat and add lemon, ginger and turmeric. 
  3. Let infuse for 30 minutes. 
  4. Filter and drink at room temperature or warmed up if it stays for tomorrow. 
  5. to drink preferably in the morning fasting 



[ ![detox drink with lemon ginger and turmeric](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/boisson-detox-au-citron-gingembre-et-curcuma.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/boisson-detox-au-citron-gingembre-et-curcuma.jpg>)
