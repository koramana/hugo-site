---
title: Bolognese style lasagna
date: '2015-08-16'
categories:
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/lasagne-a-la-bolognaise1.jpg
---
![Bolognese style lasagna](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/lasagne-a-la-bolognaise1.jpg)

##  Bolognese style lasagna 

Hello everybody, 

Here is Lasagna a la Bolognese, a recipe that will appeal to young and old alike, even the most demanding will love lasagna ... And lasagna has homemade bolognese is the best, promise to Italian, hihihihih 

This is the dish of lasagna a la bolognaise is the favorite of my son, since he follows "Garfield" he asks me to make lasagna every time, hihihihi .... You can see the recipes from [ Bechamel ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video") , and [ bolognaise sauce ](<https://www.amourdecuisine.fr/article-sauce-bolognaise-maison-halal-et-sans-vin.html> "homemade Bolognese sauce / halal and without wine") by following the links. 

lasagna a la bolognaise is a very rich and tasty dish, hyper caloric certainly, but when you eat it once a month, better to enjoy it, so do not find the excuse that it is full of fat.   


**Bolognese style lasagna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/lasagne-a-la-bolognaise.jpg)

Recipe type:  dish  portions:  6  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * lasagna leaves (according to the mold) 
  * [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video")
  * [ bolognaise sauce ](<https://www.amourdecuisine.fr/article-sauce-bolognaise-maison-halal-et-sans-vin.html> "homemade Bolognese sauce / halal and without wine")
  * 100 gr of parmesan cheese 
  * 100 gr of emmental 



**Realization steps**

  1. Butter a dish and put a layer of béchamel there, 
  2. Deposit the lasagna then a layer of bechamel 
  3. then a Bolognese sauce. Sprinkle with grated parmesan cheese. 
  4. Repeat the process, until all the ingredients are gone, ending with the bechamel. 
  5. Finally, sprinkle with grated emmental cheese. 
  6. Bake for 45 minutes, oven preheated to 200 ° C (or following the capacity of your oven). 
  7. Let stand 10 minutes to cool lasagna before serving 



Note Ingredients for bechamel sauce:   
70 g of butter   
70 g of flour   
800 ml of milk   
Nutmeg   
Parmesan (optional)   
Salt pepper.   
ingredient with Bolognese sauce:   
1 tablespoon of butter   
2 tbsp. extra virgin olive oil   
1 onion, finely chopped   
2 crushed garlic cloves   
a finely chopped celery stalks   
1 carrot, finely chopped   
500 gr chopped veal   
1 glass of water   
a little freshly ground nutmeg   
1 can of tomatoes in pieces, otherwise 4 to 5 fresh tomatoes   
salt, pepper, basil, oregano, and thyme 
