---
title: orange cake
date: '2012-04-01'
categories:
- panna cotta, flan, and yoghurt

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320534261.jpg
---
& Nbsp; hello everyone, a cake very soft, a very airy texture, and an incomparable lightness. well scented with orange, it is a cake that will surely amaze you. so take advantage of your last oranges, to taste it, and to realize this easy and simple recipe. & Nbsp; 4 eggs 230 gr of crystallized sugar 250 gr of softened butter 230 gr of flour 1 sachet and a half of baking powder (1 1/2 CAC) 1 case of orange zest. 100 ml of orange juice for the syrup: 100 gr of sugar semolina the juice of 3 oranges in a terrine, & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.7  (  1  ratings)  0 

![cake with orange2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320534261.jpg)

![cake with orange](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320533401.jpg) ![cake with orange5](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320533451.jpg)

![cake with orange9](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320533531.jpg) ![cake with orange1](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320533601.jpg)

Hello everybody, 

a [ cake ](<https://www.amourdecuisine.fr/categorie-10678925.html>) very soft, with a very airy texture, and incomparable lightness. well perfumed **orange** it's a cake that will surely amaze you. 

so take advantage of your last oranges, to taste it, and to realize this **easy and simple recipe** . 

  * 4 eggs 
  * 230 gr of crystallized sugar 
  * 250 gr of softened butter 
  * 230 gr of flour 
  * 1 and a half sachets of baking powder (1 1/2 CAC) 
  * 1 case of orange zest. 
  * 100 ml orange juice 



for the syrup: 

  * 100 gr of caster sugar 
  * the juice of 3 oranges 



![cake with orange13](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320533661.jpg) ![cake with orange14](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320533741.jpg)

![cake with orange12](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320533791.jpg) ![cake with orange11](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320533841.jpg)

  1. in a bowl, mix the softened butter with the sugar,   

  2. add the egg yolks one by one,   

  3. then the flour then the orange juice and finally the orange peel. 
  4. stir again until a frothy mixture is obtained. 
  5. then add the beaten egg whites,   

  6. and mix gently, and finally add the baking powder. 
  7. pour the dough into a buttered baking tin and bake for 30 minutes, or until the cake is well cooked (you can test it with a slice of the hill that must come out dry if you put it in the cake) 



![cake with orange10](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320533971.jpg)

meanwhile prepare your syrup, mixing the sugar with the juice of the oranges, or you can replace it, by any other orange juice (I use tropicana) 

at the end of the oven, sprinkle your cake with the orange syrup 

![cake with orange6](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320534061.jpg) ![cake with orange4](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/320534151.jpg)

it was very very good, we liked the taste of oranges. 

Enjoy your meal. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
