---
title: Maamoul with figs, stuffed cakes
date: '2012-07-09'
categories:
- gateaux algeriens sans Cuisson
- Algerian cakes- oriental- modern- fete aid
- Chocolate cake
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gateaux-secs-2_thumb11.jpg
---
[ ![dry cakes 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gateaux-secs-2_thumb11.jpg) ](<https://www.amourdecuisine.fr/categorie-10678931.html>)

Maamoul with figs, stuffed cakes 

Hello everybody, 

a forgotten recipe, like, every time, hihihihi, that fig maamoul that I had prepared at the aid party, [ Algerian cakes from the aid ](<https://www.amourdecuisine.fr/article-bonne-fete-d-aid-kbir-et-mes-gateaux-algeriens-88105516.html>) , and that I completely forgot to post. 

I really like the [ maamoul ](<https://www.amourdecuisine.fr/article-maamoul-100449886-comments.html>) , with dates, or with any other joke, it looks a lot like [ Makrout ](<https://www.amourdecuisine.fr/article-26001222.html>) . a [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-11814728.html>) that I like a lot. 

this time so it's with a stuffing with dried figs. and I do not tell you, for fans of figs, it's a delight. 

**Maamoul with figs, stuffed cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gateaux-secs-2_thumb11.jpg)

**Ingredients** for the pasta: 

  * 250 g of fine semolina 
  * 250g of flour 
  * 250 g melted butter at room temperature 
  * 1 teaspoon vanilla 
  * 2 tablespoons sugar 
  * 1 pinch of salt 
  * ½ teaspoon of baking powder 
  * ½ teaspoon of water + flower water 

prank call: 
  * 200 gr of dried figs 
  * 25 gr of butter 
  * 3 to 4 tablespoons of honey (or according to taste, sometimes figs are not sweet at all) 
  * flower water to have a nice non-sticky stuffing. 



method of preparation: 

  * Mix the ingredients dry and add the melted butter, 
  * sand with your fingers, to incorporate the butter. 
  * Add the mixture of water + flower water gently to collect the dough 
  * form a ball and cover 
  * keep cool for 1 hour (for me it was all night) 
  * Now prepare the stuffing, mixing the cut figs with the blinder, remove the hard part. 
  * add butter and honey 
  * remove the blinder, it will be very sticky, gently add the flower water, just to have a nice stuffing. 
  * Take a small ball of dough the size of a walnut and flatten it in the hand 
  * put a small amount of stuffing in the hollow. Close the ball and place it in the special mold MAAMOUL floured, 
  * press a little, then pat a little on the table and drop the cake. 
  * Cook in a preheated oven at 180 degrees until the cake is golden brown. 
  * At the end of the oven, sprinkle with icing sugar. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to subscribe to the newsletter, just click on this image, and tick both boxes. 

the [ gateaux algeriens ](<https://www.amourdecuisine.fr/categorie-10678931.html>)
