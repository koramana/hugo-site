---
title: Couscous with acorn couscous baloute
date: '2015-01-06'
categories:
- couscous
- Algerian cuisine
- recipes of feculents
tags:
- Gluten-free cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Couscous-sans-gluten-couscous-a-la-farine-de-glands.jpg
---
##  [ ![Couscous with acorn couscous baloute](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Couscous-sans-gluten-couscous-a-la-farine-de-glands.jpg) ](<https://www.amourdecuisine.fr/article-couscous-aux-glands-couscous-bel-baloute.html/couscous-sans-gluten-couscous-a-la-farine-de-glands>)

##  Couscous with acorn couscous baloute 

Hello everybody, 

Here is the acorn couscous recipe or the oak flour couscous that I promised you when I published my article about [ dishes of mawlid nabawi charif ](<https://www.amourdecuisine.fr/article-plats-mawlid-nabawi.html> "Mawlid nabawi dishes") . 

The recipe is the ordinary recipe for durum couscous, but it's couscous that changes. 

It's couscous rolled with oak flour, it's another texture, the cooking is shorter, the taste is incredibly delicious especially if you like acorns. Personally I'm crazy, what I ate tassels roasted fire heating oil at the time. when it was a cold duck, and my mother made a small opening in the acorns one by one, to then place them on the cast iron lid of the heating, this scent embalmed the whole house, and it was wisely waited that his thigh for enjoy, the good weather ... Here in England, I do not even find acorns, finally the trees are everywhere, but apparently it is not edible ??? 

When I had this couscous with oak flour, I often ate it when I was young, especially in the region of Setif, there were women who rolled this couscous. We ate without knowing that it was a couscous without gluten, and without knowing all the virtues of acorns, we ate because it was too good. 

In time, we found more for sale, or it was rare to find couscous acorns of good quality. But here it is two years ago, I came across a range of couscous variety going to Ardis, a supermarket in Algiers, I did not believe my eyes. Of course I robbed the radius of couscous, I buy each variety a box of 1 kg ... 

And during my pregnancy, I ate this couscous, I even ate without sauce, just steamed, I ate it by hand, lol, because what is good, yum yum.   


**Couscous with acorns: couscous beautiful ballout**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/couscous-el-ballout-couscous-de-Gland-chene.jpg)

portions:  4-5  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * 500 gr Couscous with chain flour 
  * 5 to 6 pieces of mutton 
  * a handful of chickpeas dipped the day before. 
  * 3 large carrots. 
  * 2 zucchini 
  * 1 big onions 
  * 2 tablespoons tomato paste 
  * 1 tablespoon smen or butter, 
  * 2 tablespoons of oil 
  * paprika, black pepper, salt and ras el hanout 



**Realization steps** Cooking acorn couscous: 

  1. Moisten the couscous with a little water, add the salt, and sand between your fingers. 
  2. when the couscous would absorb all the water, oil it by sanding it between your hands. 
  3. Steam it by placing the couscous in the top of a couscous 
  4. when the steam begins to leak, count 10 to 15 minutes and remove the couscous. 
  5. place in a large bowl, separate the beans delicately with a wooden spoon, and add more water, the equivalent of half a glass. 
  6. when the couscous absorbs the water well, turn it on again for steaming. 
  7. if you think that the couscous is cooked (and it cooks faster than ordinary couscous) add the equivalent of a tablespoonful of butter, separate the grains and let stand. 

sauce preparation: 
  1. place the pieces of meat in the bottom of the couscous, add over the grated onion, spices, salt and oil. 
  2. let it simmer a little, add the carrots peeled, and cut according to your taste, 
  3. cover with a little water and let it cook a little, the meat will take a color, and reject its water. 
  4. add the tomato paste, and the chickpeas (if they are just deceived in the water) and cover with water. 
  5. once the meat is almost cooked, add the zucchini cut to your taste and cook. 
  6. Once everything is ready to serve in a hollow dish filled with couscous, arrange over vegetables, meat and chickpeas, sprinkle with sauce, and enjoy 



[ ![couscous el ballout](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/taam-el-ballout-aux-legumes-et-viande.jpg) ](<https://www.amourdecuisine.fr/article-couscous-aux-glands-couscous-bel-baloute.html/taam-el-ballout-aux-legumes-et-viande>)
