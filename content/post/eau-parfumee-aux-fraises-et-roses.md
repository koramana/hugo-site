---
title: scented water with strawberries and roses
date: '2017-09-12'
categories:
- juice and cocktail drinks without alcohol
- Detox kitchen
- Healthy cuisine
- recipes at least 200 Kcal
tags:
- Detox water
- Drinks
- Detox
- Breakfast
- To taste
- Easy cooking
- lemons

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/09/eau-parfume-aux-fraises-et-rose-1.jpg
---
![water with strawberry and rose fragrance 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/eau-parfume-aux-fraises-et-rose-1.jpg)

#  scented water with strawberries and roses 

Hello everybody, 

We all have to hydrate well, whether in summer or winter. You usually have to drink at least 1 liter and a half of water a day (2 liters is even better). But sometimes, we do not want to drink water, just water. 

And this is where the scented waters with fruits, or herbs come to help you drink water in quantity and without moderation! Not only this water is too good, it is also good for the body and for detox. 

For this scented water, I used frozen strawberries that I had in large quantities in my freezer, and I used roses that I had from a previous recipe and a little lime. I assure you that this drink is so good that you can not live without it.   


**scented water with strawberries and roses**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/eau-parfume-aux-fraises-et-rose-3.jpg)

**Ingredients**

  * A handful of strawberries 
  * Rose petals (no pesticide) or a nice handful of small roses 
  * 1 lime, cut into slices 
  * 1 liter of filtered water 



**Realization steps**

  1. Fill a carafe or mug with all the ingredients and place in the refrigerator for at least a few hours, preferably overnight. 
  2. you can consume this super refreshing drink at any time. it's pure happiness 



![water with strawberry and rose fragrance 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/eau-parfume-aux-fraises-et-rose-2.jpg)
