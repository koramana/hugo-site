---
title: the most consulted articles in January
date: '2011-02-01'
categories:
- dessert, crumbles and bars
- cakes and cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/hrira-oranaise-copie-12.jpg
---
![https://www.amourdecuisine.fr/wp-content/uploads/2013/07/hrira-oranaise-copie-12.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/hrira-oranaise-copie-12.jpg)

##  [ harira, soup with fresh and dried vegetables, with chicken ](<https://www.amourdecuisine.fr/article-hrira-soupe-aux-legumes-frais-et-secs-au-poulet-65270080.html> "harira, soup with fresh and dried vegetables, with chicken")

![Two-tone gazelle horns with honey 056](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/Cornes-de-Gazelle-bicolore-au-miel-056_thumb1.jpg)

##  [ Two-colored gazelle horn pinches honey ](<https://www.amourdecuisine.fr/article-corne-de-gazelle-bicolore-pincee-au-miel-60151822.html> "Two-colored gazelle horn pinches honey")

![avocado salad 026](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-d-avocat-026_thumb1.jpg)

##  [ avocado salad with tuna ](<https://www.amourdecuisine.fr/article-salade-d-avocat-58843418.html> "avocado salad with tuna")

![https://www.amourdecuisine.fr/wp-content/uploads/2012/04/mauresque-au-nutella4_thumb1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/mauresque-au-nutella4_thumb1.jpg)

##  [ Moorish with leila Nutella ](<https://www.amourdecuisine.fr/article-mauresques-au-nutella-de-leila-63006178.html> "Moorish with leila Nutella")

![mhadjeb 069](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mhadjeb-069_thumb1.jpg)

##  [ algerian mhadjebs, stuffed msemens, stuffed pancakes, mahdjouba ](<https://www.amourdecuisine.fr/article-mhadjebs-algeriens-msemens-farcis-crepes-farcis-mahdjouba-62744653.html> "algerian mhadjebs, stuffed msemens, stuffed pancakes, mahdjouba")

![gingerbread mercotte 018](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-d-epice-mercotte-018_thumb1.jpg)

##  [ gingerbread very soft, and too good ](<https://www.amourdecuisine.fr/article-pain-d-epice-tres-moelleux-et-trop-bon-49778567.html> "gingerbread very soft, and too good")

![galette des kings 2-023.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/galette-des-rois-2-0232.jpg)

##  [ Galette des Rois ](<https://www.amourdecuisine.fr/article-galette-des-rois-64172342.html> "Galette des Rois")

![chicken puree with Indian sauce 003](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/puree-au-poulet-a-la-sauce-indienne-003_thumb.jpg)

##  [ Chicken puree with Indian sauce ](<https://www.amourdecuisine.fr/article-puree-au-poulet-a-la-sauce-indienne-64105296.html> "Chicken puree with Indian sauce")

![Mascarpone kiwi verrines 043](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/Verrines-kiwi-mascarpone-043_thumb1.jpg)

##  [ Mascarpone kiwis sweet verrines ](<https://www.amourdecuisine.fr/article-verrines-sucrees-kiwis-mascarpone-64934521.html> "Mascarpone kiwis sweet verrines")

![cakes without cooking 001a](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/gateaux-sans-cuisson-001a_thumb1.jpg)

##  [ chocolate truffles and biscuits ](<https://www.amourdecuisine.fr/article-truffes-au-chocolat-et-biscuits-65846143.html> "chocolate truffles and biscuits")

![baklawa filo 10](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/baklawa-filo-10_thumb1.jpg)

##  [ Baklawa (baqlawa) with filo pastry ](<https://www.amourdecuisine.fr/article-25555887.html> "Baklawa \(baqlawa\) has filo pie for the aid party")

![donut khefaf 067 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/beignet-khefaf-067-2_thumb1.jpg)

##  [ Donuts, khfaf, or sfendj ](<https://www.amourdecuisine.fr/article-beignets-khfaf-ou-sfendj-64651536.html> "Donuts, khfaf, or sfendj")

![focaccia 004](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/focaccia-004_thumb1.jpg)

##  [ Focaccia with rosemary and onion ](<https://www.amourdecuisine.fr/article-focaccia-au-romarin-et-oignon-62123009.html> "Focaccia with rosemary and onion")

![qlb 005](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/qlb-005_thumb1.jpg)

##  [ Qalb el louz, gateau Coeur d`amandes »قلب اللوز » ](<https://www.amourdecuisine.fr/article-35768313.html> "Qalb el louz, cake Heart of almonds")
