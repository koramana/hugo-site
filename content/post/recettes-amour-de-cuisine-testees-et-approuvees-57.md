---
title: kitchen love recipes tested and approved 57
date: '2015-03-04'
categories:
- les essais de mes lecteurs
tags:
- desserts
- Cakes
- Algeria
- Morocco
- dishes
- tajine
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg)

Hello everybody, 

We start another ballet of my recipes tested and approved, by you my faithful readers. I try to publish this article as and when to forget any of the photos sent. Thanks a thousand times my friends for sharing, you are very nice, all of you who try my recipes. When to those who do not know, you can send me your photos either on mail: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

Do not deprive me of this happiness my friends, and the more you are many to try my recipes and to leave your comments, the more I have the volenteé to make even more delights ... I continue with your encouragement. 

[ ![halima egg custard ahmed](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/flan-aux-oeufs-de-halima-ahmed.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/flan-aux-oeufs-de-halima-ahmed>)

[ egg flan ](<https://www.amourdecuisine.fr/article-flan-aux-oeufs.html> "Egg flan") at Halima Ahmed 

[ ![hnifiettes at Wamani merou](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/hnifiettes-chez-Wamani-merou.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/hnifiettes-chez-wamani-merou>)

[ hnifiettes ](<https://www.amourdecuisine.fr/article-hnifiettes-gateau-algerien.html> "hnifiettes, Algerian cake") at Wamani Merou 

[ ![bourek hafred fati](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/bourek-hafred-fati.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/bourek-hafred-fati>)

[ Dyouls or brick sheets ](<https://www.amourdecuisine.fr/article-dyouls-ou-feuilles-de-brick-fait-maison.html> "dyouls or homemade Brick leaves") at Hafred Fati 

[ ![another Emmanuel's bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/un-autre-pain-de-Emmanuel-1024x576.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/un-autre-pain-de-emmanuel>)

[ Homemade bread ](<https://www.amourdecuisine.fr/article-pain-maison-facile-khobz-eddar-aux-3-grains.html> "easy homemade bread: Khobz eddar with 3 grains") at Maurizio Emmanuelle Rsc 

[ ![salted butter caramel](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/caramel-au-beurre-sal%C3%A9.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/caramel-au-beurre-sale-3>)

[ Salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html> "salted butter caramel") at Amina Zebiche 

[ ![homemade rechta, flower dz](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/rechta-fait-maison-fleur-dz.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/rechta-fait-maison-fleur-dz>)

[ Homemade rechta ](<https://www.amourdecuisine.fr/article-rechta-fait-maison-pour-l-aid-el-kbir.html> "rechta - homemade") at flower Dz 

[ ![cake with orange at nicole](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cake-a-lorange-chez-nicole.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/cake-a-lorange-chez-nicole>)

[ ![orange cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/cake-orange.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/cake-orange>)

[ Cake with orange ](<https://www.amourdecuisine.fr/article-gateau-a-l-orange-moelleux.html>) at Nicole Gozzi 

[ ![mbesses oum mohamed](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/mbesses-oum-mohamed.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/mbesses-oum-mohamed>)

[ Mbesses ](<https://www.amourdecuisine.fr/article-mbesses-sable-la-semoule.html> "Mbesses: shortbread with semolina") at Oum -Mohamed 

[ ![genoise at Fleur Dz](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/genoise-chez-Fleur-Dz.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/genoise-chez-fleur-dz>)

[ Genoese ](<https://www.amourdecuisine.fr/article-genoise-inratable.html> "unbreakable genesis") (with 2 tablespoons cocoa) at Fleur Dz 

[ ![shortbread with jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/sabl%C3%A9-a-la-confiture.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/sable-a-la-confiture>)

[ Shortbread with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture.html> "shortbread with jam") at Bakhta M 

[ ![tuna rolls](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/roul%C3%A9s-au-thon.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/roules-au-thon-2>)

[ Tuna rolls ](<https://www.amourdecuisine.fr/article-les-roules-au-thon.html> "tuna rolls") at Bakhta M 

[ ![spilled cake with pineapple](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-renvers%C3%A9-a-lananas.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/gateau-renverse-a-lananas>)

[ Spilled cake with pineapple ](<https://www.amourdecuisine.fr/article-gateau-renverse-lananas-facile.html> "Spilled cake with easy pineapple") at Bakhta M 

[ ![Spongebob](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/bob-leponge.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-57.html/bob-leponge>)

[ Spongebob ](<https://www.amourdecuisine.fr/article-gateau-bob-l-eponge.html> "cake sponge bob") chez Bakhta M 
