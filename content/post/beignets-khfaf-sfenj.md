---
title: Donuts - khfaf - sfenj
date: '2012-07-07'
categories:
- panna cotta, flan, and yoghurt

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/beignet-khefaf-060-1_thumb1.jpg
---
##  Donuts - khfaf - sfenj 

Hello everyone, 

you noticed what is on the side of the photo above, yes, my little baby started kindergarten "nursery", it started this Thursday, and for this occasion, I did to him, what we doing in our custom, donuts ... 

a great opportunity to taste this delight that I have not done for almost a year now, because of the frying, but once, is not going to be serious. 

So, I promised myself to make his recipe at the first opportunity, and here it is, a real success. 

**Donuts - khfaf - sfenj**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/beignet-khefaf-060-1_thumb1.jpg)

portions:  10  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients**

  * 500 g of flour (375 gr for me) 
  * 250 g of medium or fine semolina (180 gr for me) 
  * 1 cup instant yeast (¾ for me) 
  * ½ teaspoon salt 
  * 1 or 2 eggs (1 for me) 
  * lukewarm water 
  * I added, 1 tablespoon of sugar 
  * frying oil 



**Realization steps**

  1. In a large bowl or kneader or bread machine put the flour, semolina, yeast, salt and egg 
  2. add warm water to wet well 
  3. pick up the dough (neither too hard nor too soft) 
  4. leave a few moments 
  5. Then gently wet your dough, working with the palms of your hands until you get a very soft and elastic dough 
  6. cheat your hands in lukewarm water and take the dough with your fingertips to bend it violently in this way you will air your dough well. 
  7. Let the dough rest in a large pot 
  8. let it rise for 1 hour 
  9. Put a deep pan on the fire filled with ¾ oil, let warm (very important) 
  10. take a bowl filled with water, dip your fingers in it 
  11. take the value of a large egg dough stretched between the fingers gently and quickly to give it a length or round shape according to your choice 
  12. Put in the pan, sprinkle with hot oil, brown the first side then turn to brown the 2nd side. 
  13. do the same way until the dough is exhausted 


