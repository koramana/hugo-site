---
title: soft cake cheese and blueberries
date: '2017-10-18'
categories:
- cheesecakes and tiramisus
- dessert, crumbles and bars
- cakes and cakes
tags:
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-a-la-creme-de-fromage-et-myrtilles1-830x1024.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-a-la-creme-de-fromage-et-myrtilles1-830x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-a-la-creme-de-fromage-et-myrtilles1.jpg>)

##  soft cake cheese and blueberries 

Hello everybody, 

A soft cake rich in taste and flavor, a recipe super easy to make, with lots of delicacies. This creamy cake with creamy cheese and blueberries is a realization of Lunetoiles, and I'm sure you'll like it. 

Already nothing when watching this end right next to the fork, I want to put my in the screen, and eat a bite ... and have this moment of tasting when the cream of cheese melts in the mouth, and when the blueberries start to burst and give this sweet tangy taste, which will mix with the soft and melting tip of cake .... 

so we put on our aprons ??? 

**chewy cake with cheese and blueberries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-au-fromage-cremeux-et-mysrtilles-685x1024.jpg)

Recipe type:  cake, dessert  portions:  12  Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients** the dough of the cake 

  * 300 g of flour 
  * 2 teaspoon of baking powder 
  * 100 g sugar 
  * 1 sachet of vanilla sugar 
  * 100 g cold butter 
  * 2 eggs 

Cheese cream 
  * 400 g cream cheese (for me a mixture of bush and goat faisselle) 
  * 1 teaspoon of lemon peel or orange (I personally did not put) 
  * 100 g of sugar 
  * 1 sachet of vanilla sugar 
  * 1 bag of preparation for flan, vanilla dessert 
  * 250 ml of fresh cream of 30% to beat, and it must be very cold 

decoration: 
  * 400 gr of blueberries 



**Realization steps**

  1. butter or cover a 20 x 30 cm mold, a sheet of parchment paper 

Basic paste: 
  1. Sift the flour with the yeast, add the sugar, vanilla sugar and butter cut into pieces. 
  2. Sand all in hands for a mixture that looks like sand. 
  3. Add the eggs and pick up the dough. 
  4. You can also mix the pasta with a robot or a kitchen mixer. 
  5. Form a ball, then flatten the ball and put it in the refrigerator 30 - 60 minutes. 

Cheese cream: 
  1. In a bowl, place the cream cheese, lemon zest or orange, sugar, vanilla sugar and the powdered flan packet and mix. 
  2. In another bowl beat the cream 30% to have a stiff mousse. 
  3. Preheat the oven to 180 ° C (th.6). 
  4. Remove the dough from the refrigerator, and spread it over parchment paper (the dough may be sticky or tearing). 
  5. Then fill the mold with the dough and go up on the edges. 
  6. Gently mix the cheese mixture with the whipped cream and pour over the dough. 
  7. Sprinkle with the blueberries and bake. 
  8. Bake for 35 minutes, until the edges of the dough are golden brown. 
  9. Let cool completely and refrigerate for several hours before serving. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-au-fromage-et-myrtilles-1024x685.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-au-fromage-et-myrtilles.jpg>)
