---
title: velvety leek and parmesan soup
date: '2016-06-27'
categories:
- Algerian cuisine
- Plats et recettes salees
- soups and velvets
tags:
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/veloute-de-poireaux-soupe-aux-poireaux-026.CR2_.jpg
---
![veloute de leek - soup-to leeks 026.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/veloute-de-poireaux-soupe-aux-poireaux-026.CR2_.jpg)

##  velvety leek and parmesan soup 

Hello everybody, 

The cold does not want to leave us, we were treated to a summer week as they say the English, and here again it rains, that it sells, and that the small dishes which warm us, find again their places on our tables. 

Today it is a small cream of leeks, a very tasty leek soup, perfumed with parmesan that warmed my heart after a long day of fasting .... Hum, an endless intense taste, I just wanted to sip again and again this velvety without stopping! 

if you like leeks, here are two delicious recipes: 

[ leek and celery soup ](<https://www.amourdecuisine.fr/article-soupe-aux-poireaux-et-celeri-114070540.html>)

[ leek pie ](<https://www.amourdecuisine.fr/article-tarte-aux-poireaux-115568724.html>)

**leek soup / leek soup and parmesan cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/veloute-de-poireaux-022.CR2_.jpg)

portions:  2  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 2 tablespoons of butter 
  * 3 leeks 
  * 1 medium potato 
  * 1 cube of chicken broth. 
  * black pepper 
  * 30 gr grated Parmesan cheese. 



**Realization steps**

  1. Heat the butter in a large pot over low heat. 
  2. Add the sliced ​​leek, the chopped potato and the bouillon cube. 
  3. let it simmer over low heat, stirring occasionally, until the vegetables begin to soften, about 5-10 minutes. . 
  4. Add water and increase the temperature so that it boils a little, then reduce the heat so that the soup cooks gently. 
  5. let it simmer until the vegetables are very tender, about 15 minutes. 
  6. mix the mixture until it becomes smooth and velvety. 
  7. add a little grated Parmesan cheese to make sure it melts. 
  8. Season to taste with salt. 
  9. serve with parmesan on top, fresh or dried chives, and black pepper from the grinder. 



![veloute de leeks 022.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/veloute-de-poireaux-022.CR2_1.jpg)
