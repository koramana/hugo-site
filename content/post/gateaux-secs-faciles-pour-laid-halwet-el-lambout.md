---
title: easy dry cakes for Halwet el-lambout Aid
date: '2012-08-11'
categories:
- cuisine algerienne
- diverse cuisine
- Mina el forn
- pizzas / quiches / pies and sandwiches
- recettes de feculents
- recettes patissieres de base

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/lambout-003a_thumb_11.jpg
---
##  easy dry cakes for Halwet el-lambout Aid 

Hello everybody, 

I do not know if you're like me, but for Eid, I always start with [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) (you can see more about the link), and [ sands and ghribia ](<https://www.amourdecuisine.fr/categorie-12344749.html>) because it can be kept longer. 

I then share with you, one of the easy dry cakes for Halwet el-lambout Aid, a dry cake that I like very much, because my children eat dry cakes, or [ cakes with glaze ](<https://www.amourdecuisine.fr/categorie-12344742.html>) . 

the  halwat lambout  , or ** el mohgone cake  ** is a traditional recipe, and I believe that each person has his way or rather his ingredients to prepare it, for me here are the ingredients of my mother:   


**easy dry cakes for Halwet el-lambout Aid**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/lambout-003a_thumb_11.jpg)

**Ingredients**

  * 4 eggs 
  * 250 grams of sugar 
  * 100 ml of melted butter 
  * 150 ml of oil (you can only use oil) 
  * 1 cup of baking powder 
  * 1 cup of vanilla coffee 
  * of flour 



**Realization steps**

  1. the method of preparation is very simple: 
  2. in a large salad bowl, whisk the eggs is the sugar 
  3. add the vanilla, the oil and butter mixture 
  4. keep whipping 
  5. add the sifted flour mixture, baking powder gradually to the previous mixture until you obtain a malleable dough and you can shape using a nozzle 
  6. shape rods 7 to 8 cm, or circles. 
  7. place them directly on an oiled baking sheet 
  8. preheat the oven to 170 degrees 
  9. cook between 15 and 20 minutes 
  10. you can join each two pieces with jam of your taste, and wrap the tip of a delicious chocolate, I did not have time for that 



thank you very much for your comments and passages 

passez un agréable journée 
