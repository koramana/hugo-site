---
title: mtewem red sauce - minced garlic meatballs
date: '2011-11-09'
categories:
- recettes sucrees
- rice
- tartes et tartelettes

---
Hello everybody, 

before yesterday i received a com me asking the recipe mtewem is an Algerian dish, meat and meat chopped seasoning garlic, whose name mtewem, because "garlic" in Arabic is "toum "So I said to the girl it's on the category [ meat and fish ](<https://www.amourdecuisine.fr/categorie-10678928.html>) , she said ke do not find, I go to look, and to my surprise I had never post this dish on my blog, while I do it very often because my husband loves very much, and even children eat the meat of this way (luckily for me). 

and as I had chopped meat at home, I thought, I am preparing it and I am going to post it this time. 

Ingredients for 4 persons 

  * 300g of meat in pieces (mutton for me here) 
  * 250g minced meat 
  * 1 handful of chickpeas dipped the day before 
  * 3 tablespoons of table oil (I cook with extra virgin olive oil) 
  * 5 cloves of garlic 
  * 1 small onion 
  * salt, black pepper, cumin, red pepper 
  * 1/2 teaspoon of concentrated tomato 



method of preparation: 

  1. In a pot over low heat, sauté the pieces of meat in the oil with chopped onion 
  2. add 2 cloves garlic crushed, cumin and other spices 
  3. add the chickpea grip and wet with the water until the meat completely covers and cook 
  4. Now prepare the meatballs, in a salad bowl, put the minced meat, add the crushed garlic, salt and spices. form balls 3 cm in diameter. 
  5. Once the meat is cooked, immerse the meatballs in the sauce, 
  6. simmer a few minutes, to have a reduced sauce 



j’espere Rihem que tu vas essayer la recette, et merci d’avoir attirer mon attention sur le fait que la recette n’etait pas sur mon blog. 
