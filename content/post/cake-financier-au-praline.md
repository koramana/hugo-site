---
title: financial cake with praline
date: '2014-10-29'
categories:
- cakes and cakes
- sweet recipes
tags:
- Fruits
- Cakes
- desserts
- Four quarters
- Pastry
- Algerian cakes
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-financier-au-pralin%C3%A9-1.jpg
---
[ ![cake-financier with praline](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-financier-au-pralin%C3%A9-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-financier-au-pralin%C3%A9-1.jpg>)

##  cake finan  praline 

Hello everybody, 

So I tell you one thing, the result is just a delight, and the idea of ​​this dessert or drip is superb, baking two financial cookies in a large mold, then cut the biscuit into several rectangles, to finally superimpose one on the other by interposing a delicious layer of melting Nutella. 

it tempts you the cake, so I do not take long to pass you the recipe. 

**financial cake with praline**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-financier-au-pralin%C3%A9-2.jpg)

portions:  6 to 8  Prep time:  25 mins  cooking:  10 mins  total:  35 mins 

**Ingredients**

  * 150 g icing sugar 
  * 50 g almond powder 
  * 60 g plain flour 
  * 150 g egg whites 
  * 90 g of hazelnut butter 
  * 200 g of pralinoise (I had none, so I replace with 100 g of milk chocolate and 100 g of nutella) 



**Realization steps**

  1. Roast the almond powder by passing it for a few minutes under the oven grill (or in a dry pan over low heat). 
  2. Preheat the oven to 220 ° C. 
  3. Mix the icing sugar, flour and roasted almond powder, then add the butter and the whites. Mix well. 
  4. Bake the dough twice in a 20 x 30 cm rectangular mold (or three times in a 20 x 20 square pan!) For about 8 minutes. 
  5. Melt the Pralinoise (or like me milk chocolate with nutella). 
  6. Cut each of the 2 plates into 6 rectangles, brush each rectangle with Praline and stack them. 
  7. Eventually, finish with a layer of dark chocolate. 
  8. Leave the cake in the fridge for at least 2-3 hours before cutting it into thin slices. 



[ ![cake-financier with praline](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-financier-au-pralin%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/cake-financier-au-pralin%C3%A9.jpg>)
