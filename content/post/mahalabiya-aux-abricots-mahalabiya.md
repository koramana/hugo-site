---
title: mahalabiya with apricots, mahalabiya
date: '2013-12-07'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/Mahalabiya-aux-abricots-037-001_thumb.jpg
---
##  mahalabiya with apricots, mahalabiya 

Hello everybody, 

A delicious dessert that comes from Lebanon, the **mahalabiya, mhalabia or mohalabiya** , very light as dessert, and very tasty, it looks a little like Algerian palouza (recipe coming soon) and also [ mhalbi ](<https://www.amourdecuisine.fr/article-mhalbi-creme-dessert-au-riz-56030222.html>) except that for the mhalbi, rice powder is used. 

this time, I prepared a delicious mahalabiya, garnished with a nice layer of dried apricot cream (so do not bother saying: I'm going to wait for the apricot season, we have dried apricots all the time ...) 

**mahalabiya with apricots, mahalabiya**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/Mahalabiya-aux-abricots-037-001_thumb.jpg)

Recipe type:  sweet verrines  portions:  6  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** layer of mhalabiya: 

  * 2 glasses of milk (240 ml) 
  * 1 glass of fresh cream 
  * ½ glass of sugar 
  * ¼ glass of cornflour 
  * A pinch of salt 
  * 1 teaspoon of orange blossom water 
  * 1 tablespoon of butter 

for the cream of dried apricots: 
  * 100 gr of dried apricots 
  * water 
  * 2 tablespoons cornflour 
  * 1 teaspoon of orange blossom water 
  * sugar according to taste 
  * The juice of a lemon 



**Realization steps**

  1. Rinse the dried apricots for cleaning. Place them in a bowl and cover with water. Soak overnight or for three or four hours in the fridge 
  2. In a saucepan, combine all the dry ingredients. 
  3. Add the flower water, milk and cream. 
  4. Cook over low heat stirring constantly so lumps do not form. 
  5. The mixture should thicken well (if at a certain moment you notice that the mixture seems to turn - out of phase, pass the mixture to the blinder so that the cream becomes flexible). Continue cooking until thick enough to coat the back of a spoon. 
  6. Remove from heat and add the butter, mix well. 
  7. Pour in small verrines filling ¾ of the verrine and chill. 
  8. remove the apricots from the fridge and pass them with their water to the blinder to have 400 ml of puree (add the water to have this quantity of puree). 
  9. Skip this puree to the Chinese. 
  10. mix the apricot purée, cornflour, lemon juice and orange blossom water in the pan 
  11. add the sugar according to your taste and gently boil over low heat. 
  12. let it simmer until it thickens. Remove from the fire 
  13. using a spoon, fill the mhalabiya verrines with this cream and return to the fridge. 



merci pour vos visites et bonne journee. 
