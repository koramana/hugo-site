---
title: Steaming chicken wings in the oven
date: '2017-08-15'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- idea, party recipe, aperitif aperitif
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/ailes-de-poulets-piquants-au-four-1.CR2_.jpg
---
[ ![1.CR2 oven-roasted chicken wings](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/ailes-de-poulets-piquants-au-four-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/ailes-de-poulets-piquants-au-four-1.CR2_.jpg>)

##  Steaming chicken wings in the oven 

Hello everybody, 

Today it was a barbecue party at home, and I prepared the same recipe that was grilled. In any case, if you do not have a barbecue or the climate in your home does not help too much grilling, I recommend the oven version. 

These spicy chicken wings are just crunchy. Very melting and very pungent ... I do not tell you the delight. You can accompany these spicy chicken wings with a yogurt dip flavored with a little coriander, fresh tomato, garlic, salt and black pepper, and here is the magic, it will really please you.    


**Steaming chicken wings in the oven**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/ailes-de-poulet-piquant-au-four.CR2_.jpg)

portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 6 to 8 chicken wings 
  * 2 garlic cloves, finely chopped 
  * 1 C. coriander powder 
  * 1 C. ground cumin 
  * ½ c. hot pepper 
  * 1 C. curry coffee 
  * 2 tbsp. olive oil coffee 
  * 2 tbsp. apple vinegar coffee 
  * 1 C. soy sauce coffee 
  * ½ c.a salt coffee (or according to taste) 



**Realization steps**

  1. In a salad bowl, mix crushed garlic, coriander, cumin, hot pepper, salt and curry. 
  2. Add the oil, vinegar and soy sauce and mix. 
  3. Cut the tips of the chicken wings and divide the wings in half at the joint. 
  4. Put the chicken wings in the spice mixture by turning them around to coat them. 
  5. Cover and marinate in the refrigerator for at least 1 hour or overnight. 
  6. On an oiled baking sheet, arrange the chicken wings. Bake in preheated oven at 200 ° C until golden brown (return during cooking). 



[ ![spicy chicken wings in the oven 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/ailes-de-poulet-piquants-au-four-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/ailes-de-poulet-piquants-au-four-1.CR2_.jpg>)
