---
title: 'recipes around one ingredient # 36 Raisins'
date: '2018-01-20'
categories:
- Coffee love of cooking
tags:
- Cake
- Cakes
- Four quarters
- desserts
- To taste
- dishes
- Soup

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg)

source: Pinterest 

##  recipes around one ingredient # 36 Raisins 

Hello everybody, 

Now, every 3rd of the month, we look forward to seeing all the recipes we can make around the star ingredient and especially to see who will be the next godmother and what will be her star ingredient. 

This time, I have been paying for the relief just because the majority of the potential godmothers are on vacation, and the fact that the game must continue "smoothly" I take over, and I expect you many and many to participate in this round, for maybe, be the godmother of the next round! 

I chose after a long discussion with Samar **raisins** as the featured ingredient in Round 36 of our recipe game around one ingredient: 

![raisins recipes around an ingredient 36](https://www.amourdecuisine.fr/wp-content/uploads/2018/01/raisins-secs-recettes-autour-dun-ingr%C3%A9dient-36.jpg)

To be honest, I wanted to choose either the puff pastry or the pancakes, but Samar told me that it's not ingredients anymore, but themes ... and what is your opinion? can we consider puff pastry or pancakes as ingredients, and could they be good subjects for our upcoming rounds? 

OK! there we drowned star ingredient, so we go to the serious things, the principle of the game: 

The principle of the game: 

  * The initiators of the game: Samar and Soulef 
  * Add the **complete list of participants with the titles of their recipes** . 



Si malgré avoir voulu participer et au final vous n’êtes pas parvenue à faire votre recette, me contacter avant le jour -J- afin de vous retirer de la liste. ( Ceci reste un jeu et pas une contrainte). 
