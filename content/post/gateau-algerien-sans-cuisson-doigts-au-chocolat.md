---
title: Algerian cake without cooking chocolate fingers
date: '2012-06-22'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateau-sans-cuisson-batonets-au-chocolat_thumb.jpg
---
##  Algerian cake without cooking chocolate fingers 

Hello everybody, 

a very nice recipe **[ cake without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) ** , that I prepared often, before embarking on the adventure of the blog, and of course I did not decorate in this way, it must be said that since the blog, I learned a lot from these hundreds of books that I , and also my blogger friends. 

so I really liked sharing with you, not only is it easy to achieve, but also, it is very good, and very nice to present. 

easy ingredients, and accessible to everyone, and the most important that we can adapt them according to our tastes and what we have, at home. 

look at the link, for more [ cakes without cooking ](<https://www.amourdecuisine.fr/categorie-11745248.html>) , or [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>)   


**Algerian cake without cooking chocolate fingers**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/gateau-sans-cuisson-batonets-au-chocolat_thumb.jpg)

Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * ingredients: 
  * 1 box of biscuits 
  * 1 glass (220 ml) of coconut 
  * jam (peach jam for me) but you can one of your choice. 
  * 5 tablespoons of melted butter 
  * dark chocolate for decoration 
  * coconut for decoration 



**Realization steps**

  1. in the bowl of a blender, crush the biscuits powder 
  2. add the coconut 
  3. then stir in the butter. 
  4. pick up the dough with the jam until you have a paste that picks up. 
  5. prepare rolls or fingers 8 cm long and 2.5 cm in diameter 
  6. put in the fridge for 1 hour, so that the dough fits well 
  7. melt the chocolate in a bain-marie 
  8. place your fingers on a rack, and cover them gently with the melted chocolate, using a spoon 
  9. sprinkle the coconut on the sides, before the chocolate freezes 
  10. take the chocolate well. 
  11. melt 2 to 3 squares of chocolate, and place them in a cone of baking paper or a bag and pass over the cakes to draw scoffers. 
  12. let it freeze, and put them in boxes, in a cool place (not in the fridge otherwise you will have a point on the chocolate) 
  13. can be kept for 3 days in an airtight container in a cool place. 



اصابيع الشكولا رووعة  ا لالمقادير عجينة   
المقادير   
3 كيلات من الفرينة   
كيلة سمن   
قرصة ملح + ماء زهر  الحشو   
كوب ماء زهر نجمعهم مع بعض حتى نتحصل على كرية من الخليط نقدرو نتحكمو فيها. للتزيين   
شكولا سوداء وشكولا بنية   
بعد ماطيب نغطسعا في شربات ونخليها تنشف ونذوب الشكولا السوداء مع 3ملاعق حليب تم نطلي بيهه الاصابع ثم نخليها تنشف مليح ونعمل خطوط بالشكولا البنية 
