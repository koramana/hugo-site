---
title: mushroom with candied fruits
date: '2015-11-25'
categories:
- cakes and cakes
- sweet recipes
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mouscoutchou-aux-fruits-confits-2.jpg
---
[ ![mushroom with candied fruits 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mouscoutchou-aux-fruits-confits-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mouscoutchou-aux-fruits-confits-2.jpg>)

##  mushroom with candied fruits 

Hello everybody, 

I have to try to post Lunetoiles' recipes that have been waiting for a while in my archives, and I thank my little sister Lunetoiles for everything she shares with us without exception, despite her worry with her camera, it is not missing, and I always receive these beautiful achievements, and these beautiful photos, I admit that sometimes I'm afraid to put only 3 or 4 photos per receipt, while she sends me a beautiful album, full of more beautiful pictures than others. 

Today, I share with you his mushroom with candied fruit. Lunetoiles tells me that this mushroom has the texture of a cake, but as it cooks first slowly, it is very soft and not dry at all. What she really appreciated is that it can be kept well packaged for a long time, while keeping its softness. 

**mushroom with candied fruits**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mouscoutchou-aux-fruits-confits-1.jpg)

portions:  8  Prep time:  20 mins  cooking:  55 mins  total:  1 hour 15 mins 

**Ingredients**

  * 2 cups sifted flour (300 gr) 
  * 1 cup of fine sugar (215 gr) 
  * 1 cup of butter (150 gr) 
  * 1 sachet of baking powder 
  * 1 sachet of vanilla sugar 
  * 5 eggs 
  * ½ glass of buttermilk or whole milk 
  * 400 g mixed candied fruits (orange peel, lemon peel, melon, cherries, pears, apricots, peaches ...) 
  * icing sugar for the decor 



**Realization steps**

  1. Put in a bowl the sugar, egg yolks and vanilla sugar. 
  2. Then work this mixture with an electric whisk until it turns whitish. 
  3. Add warm melted butter, buttermilk, flour and yeast and mix well. 
  4. Beat the egg whites until very firm and add them to the dough gently. 
  5. Add to the dough the candied fruits previously floured, always mixing gently. 
  6. Pour the dough into a buttered pan sprinkled with flour. 
  7. Bake in the oven at 160 ° C for 25 minutes and reheat the oven to 18O ° C, and continue cooking for about 30 minutes. 
  8. At the end of the oven, turn out and sprinkle with icing sugar. 



[ ![mushroom with candied fruits](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mouscoutchou-aux-fruits-confits.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mouscoutchou-aux-fruits-confits.jpg>)
