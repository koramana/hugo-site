---
title: grated potato quiche
date: '2015-11-22'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-de-pomme-de-terre-rapees.jpg
---
[ ![grated potato quiche](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-de-pomme-de-terre-rapees.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-de-pomme-de-terre-rapees.jpg>)

##  grated potato quiche 

Hello everybody, 

Do you like quiches? Here is a very easy and delicious, and normally these ingredients are within reach of everyone. 

This shredded potato quiche is an achievement of my friend Kiko Benakano, well known on facebook, macha allah, it is delicious, I always like what she realizes that I want to make all these recipes. 

I hope I can find the time to realize her delights, otherwise I hope she can share them with us. 

**grated potato quiche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-de-pommes-de-terre-rap%C3%A9es.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** for the broken pasta: 

  * 300 gr of flour 
  * 150 gr of cold butter cut into cubes 
  * 1 teaspoon of salt 
  * 1 cup of sugar 
  * 8 tablespoons water to pick up the dough. 

for the stuffing: 
  * 3 large grated potatoes 
  * 2 onions cut into strips 
  * 1 little butter 
  * 1 tablespoon of oil 
  * 2 crushed garlic cloves 
  * salt and black pepper 
  * 1 can of liquid cream 
  * 3 to 4 eggs 
  * chopped parsley 
  * 1 tablespoon of mustard 
  * 1 nice amount of grated cheese 
  * slices of black olives 



**Realization steps**

  1. Start by preparing the dough without mixing too much the ingredients, to have a short dough 
  2. spread between two sheets of baking paper, and transfer to a quiche pan covered with baking paper. 
  3. put in the freezer for almost 15 minutes, so that when cooking the pie base keeps its beautiful shape. 
  4. after resting, cook the half-baked dough in an oven preheated to 180 degrees C 
  5. fry the onion with a little butter and oil from a frying pan. 
  6. add the grated potato, salt, black pepper, and garlic, stir to make sure it does not stick 
  7. in a bowl, mix the fresh cream, eggs and cheese 
  8. add the chopped parsley and the slices of olives 
  9. spread the potato mixture over the pie shell, 
  10. pour over the cream mixture. 
  11. place in the preheated oven at 180 degrees for at least 20 minutes, or until the quiche is cooked through. 



[ ![quiche with potatoes](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-pomme-de-terre.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-a-la-pomme-de-terre.jpg>)
