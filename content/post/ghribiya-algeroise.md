---
title: ghribiya algeroise
date: '2012-08-09'
categories:
- diverse cuisine
- Cuisine by country
- trucs et astuces

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/ghribiyat-ezite-ghribiya-a-l-huile-ghriba-gateau-algeri11.jpg
---
![ghribiyat ezite, ghribiya oil, ghriba cake algerian aid 2012](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/ghribiyat-ezite-ghribiya-a-l-huile-ghriba-gateau-algeri11.jpg)

##  ghribiya algeroise 

Hello everybody, 

usually, I always do my [ ghribia ](<https://www.amourdecuisine.fr/categorie-12344749.html>) to the smen, as I learned from my mother and grandmother, but a ghribia has oil, never try, and frankly, I did not expect at all this melting result. the recipe was too good, moreover, my father asked for more, so much he appreciated. 

For even more recipe from [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , visit the [ index of Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) (I thank Lunetoiles who helped me a lot in the update of the latter)   


**ghribiya algeroise**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateau-algerien-ghribiyat-ezite_thumb1.jpg)

**Ingredients**

  * 1 measure overflowing with oil (I use as measure a water glass of 250 ml) 
  * ½ measure of crystallized sugar (half icing sugar, half crystallized sugar) 
  * 3 measures of flour 



**Realization steps**

  1. in a terrine mix the overflowing oil and the sugar then leave to rest about half an hour. 
  2. after adding the three measures of flour by mixing until you get a dough not too firm but handy (if the dough does not pick up you can add a little oil) I can tell you that for me and because of my flour I still add flour, because I could not form balls, but this did not change at all the good taste of the ghribiya which was too fondant, wonderfully. 
  3. Form dumplings the size of a walnut, put in a tray and bake at 180 ° before heating. 
  4. (you can put on a pinch of cinnamon) 
  5. once cooked, leave to cool before turning out 


