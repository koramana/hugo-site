---
title: Homemade apricot juice
date: '2017-07-18'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/jus-dabricots-fait-maison.jpg
---
[ ![homemade apricot juice](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/jus-dabricots-fait-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/jus-dabricots-fait-maison.jpg>)

##  Homemade apricot juice 

Hello everybody, 

It's always refreshing a homemade fruit juice, and when it's a seasonal fruit, you enjoy it. Today it's homemade apricot juice, especially if you see your husband with 10 kilos of apricots on the pretext that the neighborhood merchant sells them at very low prices. 

This was the case **Mina Minnano,** who take advantage of these apricots to make a delicious homemade apricot juice without additives or products and dyes that we really do not need to have in our juices, but with these manufacturers, these products become so important to the manufacture of any food product, without even knowing the reason for their addition !!!! 

So you have a homemade apricot juice recipe, healthy and healthy, to drink without remorse because it's too good. 

And if you realized one of the recipes of this blog, do not forget to share the photos with me on this email: 

![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg)

**Homemade apricot juice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/jus-dabricots-fait-maison.jpg)

portions:  8  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 1 kg of apricots 
  * 2 liters of water 
  * 2 liters of cold water 
  * sugar according to taste and need 



**Realization steps**

  1. wash the apricots, remove the stones and put in a pot 
  2. add 2 liters of water, and place on medium heat 
  3. at the first broth, remove from the heat, and mix with the mixer foot 
  4. add 2 liters of cold water 
  5. sweeten to your taste, so you'll get 4 liters of apricot juice 
  6. fill glass bottles, and cool 


