---
title: Chocolate Tiramisu - chocolate ganache / mascarpone
date: '2012-04-16'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/123-059.jpg
---
##  Chocolate Tiramisu - chocolate ganache / mascarpone 

Hello everybody, 

did I tell you how much I love this [ dessert ](<https://www.amourdecuisine.fr/categorie-recettes-sucrees/dessert-crumbles-et-barres>) Italian, the delicious, the incomparable [ Tiramisu ](<https://www.amourdecuisine.fr/categorie-recettes-sucrees/cheesecakes-et-tiramisus>) . last time I was in a bookstore, I was flipping through a book of **Italian food** , and I flashed on this beautiful chocolate tiramisu, the book was a little expensive, but the recipe very easy with very simple ingredients, so I immediately keep in mind, I could not afford the book , may be next time, hihihiih 

in any case, with the delicious Italian cheese, mascarpone, we add dark chocolate ... .. frankly an irresistible taste, I highly recommend this wonderful recipe. 

for people who can not find mascarpone, here is the recipe of [ homemade mascarpone ](<https://www.amourdecuisine.fr/article-tiramisu-avec-du-mascarpone-fait-maison-55272756.html>) . 

so I share now this very very good recipe with you, and especially if you try it, tell me your opinion. 

you will need: 

300 gr of dark chocolate 

400 gr of mascarpone (trade or house) 

150 grs thick cream 

400 ml of sweet black coffee according to taste 

36 boudoirs, or long biscuit of cats 

cocoa powder for decoration 

whip the cream, until it becomes a whipped cream, stir in the mascarpone, it's a mix not easy to mix, so go slowly 

Melt the chocolate in a bain-marie, let cool a little, before incorporating it into the mascarpone / whipped cream mixture. 

for the presentation according to your taste, for my part, I put the ganache in a bag, I cut the corner. 

dip the boudoirs in the sweet coffee, not too much until it melts. 

place them in your presentation dish, cover them with a layer of chocolate cream / mascarpone whipped cream. 

cover with another layer of boudoirs soaked in coffee, then another layer of cream, until the cream has run out. 

sprinkle with some cocoa, and place in the fridge for at least 1 hour. 

and enjoy. 

I do not tell you the taste, a spoon, after another ... and another, a supreme taste of happiness 

try it and give me your opinion 

Thank you for your feedback 

and thank you to everyone who subscribes to my newsletter 

for more tiramisu recipes you have:   
  
<table>  
<tr>  
<td>

![https://www.amourdecuisine.fr/wp-content/uploads/2012/12/123-059.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/123-059.jpg) 
</td>  
<td>



##  [ Tiramisu with homemade mascarpone ](<https://www.amourdecuisine.fr/article-tiramisu-avec-du-mascarpone-fait-maison-55272756.html> "Tiramisu with homemade mascarpone")


</td> </tr>  
<tr>  
<td>

![https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tira_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tira_thumb.jpg) 
</td>  
<td>

[ Tiramisu ](<https://www.amourdecuisine.fr/article-tiramisu-vous-ai-je-dit-a-quel-point-je-l-aime--44008438.html>) 
</td> </tr>  
<tr>  
<td>

![https://www.amourdecuisine.fr/wp-content/uploads/2012/12/255168471.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/255168471.jpg) 
</td>  
<td>

[ Tiramisu light ](<https://www.amourdecuisine.fr/article-25345349.html>) 
</td> </tr> </table>
