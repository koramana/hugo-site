---
title: fine quiche with zucchini and goat cheese
date: '2015-07-08'
categories:
- pizzas / quiches / pies and sandwiches
tags:
- ramadan 2015
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-courgettes-quiche-aux-courgettes1.jpg
---
![tarte aux courgettes - quiche aux courgettes.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-courgettes-quiche-aux-courgettes1.jpg)

##  fine quiche with zucchini and goat cheese 

Hello everybody, 

a zucchini quiche, what do you think? 

Here is a delicious and easy recipe that Lunetoiles has prepared and enjoyed sharing with us. a quiche composed of a crunchy unsweetened shortbread dough, and topped with a thin layer of cream, scented with goat cheese, and these delicious slices of zucchini ... 

Are you ready for the recipe? 

![quiche-fine-with-zucchini-and-chevre.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/quiche-fine-aux-courgettes-et-chevre1.jpg)

**fine quiche with zucchini and goat cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/quiche-aux-courgettes-et-au-chevre1.jpg)

Recipe type:  quiche  portions:  8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** For the dough: 

  * 240 g flour 
  * 150 g cold butter 
  * 1 pinch of sugar 
  * 2 pinch of salt 
  * 1 egg 
  * 1 C. vinegar 
  * 3 c. chilled water 

For garnish : 
  * 1 zucchini 
  * 2 eggs 
  * 100 g of fresh goat type billy 
  * 2 tablespoons fresh cream 
  * olive oil 



**Realization steps**

  1. In the bowl of a robot, put the flour, pinch sugar, cold butter cut into pieces and 2 pinches of salt 
  2. Blend until well blasted. 
  3. Add the egg and 3 tbsp. water, vinegar, and mix again until the dough forms a ball and comes off the sides of the bowl. 
  4. The dough is a bit sticky, wrap it in plastic wrap and let rest for one hour in the refrigerator. 
  5. Preheat the oven to 200 ° C (th.7). Spread the dough on the floured work surface or between two sheets of floured parchment paper. 
  6. Garnish a pie dish or circle 28 cm in diameter. Prick the dough with a fork. Reserve in a cool place while preparing the garnish. 
  7. Wash the zucchini under cold water with a sponge. Wipe it, and cut it into thin slices with a mandolin. 
  8. Dip the zucchini slices in boiling water for 2 minutes and drain them. 
  9. Diving them immediately in a salad bowl of very cold water, and drain them. 
  10. Beat the eggs with the cream. Add the crumbled goat cheese, salt and pepper. 
  11. Pour the mixture on the bottom of the dough, put the zucchini in a rosette, overlapping and drizzle with olive oil. 
  12. Bake at 200 ° C for 15 minutes and lower the oven to 180 ° C (th.6) and continue cooking for another 15 minutes. 


