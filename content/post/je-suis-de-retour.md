---
title: I'm back
date: '2009-07-20'
categories:
- dessert, crumbles and bars
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/07/side-turquie1.jpg
---
thank you to everyone, for all your messages that make me really happy, I will try to answer one by one.   
In the meantime, I leave you with some photos of Side, the city where I spent my holidays.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/side-turquie1.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/harbour-in-side1.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/east-side-of-town-side1.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/shuttle-tractor-side1.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/march-take-a-coat-fleece1.jpg)
