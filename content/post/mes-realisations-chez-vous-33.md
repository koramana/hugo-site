---
title: my achievements at home 33
date: '2013-02-07'
categories:
- couscous
- Algerian cuisine
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/Caknut21.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

yet another article to paratger with you, the recipes of my readers and they try my blog ... and it makes me very happy. 

please continue to send me your test photos on my email: [ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

**just a clarification** : 

this email, [ vosessais@gmail.com ](<mailto:vosessais@gmail.com>) is not a contact email, if you have questions to ask me about a recipe, do it in comments, because this email, I only consult when I have time to write the article: my achievements at you, and sometimes I find urgent questions that are more than 1 month old .... 

sorry, I have the comments to answer your questions, and I have this email for the achievements, and I have another email to contact the author, but it's mostly for commercials, hihihihih 

I can not receive everything in the same email, it will be overflowing. 

so, and I'll say it again, if you have an urgent question about a recipe, make it a comment, it will allow me to respond faster, as it allows other visitors to enjoy the answer, and not to rest the question again and again. 

thank you. 

otherwise I share with you today the photos of your achievements: 

soups, breads, cakes, cookies, desserts, salads, appetizers, brioches ... 

I'm so glad you found my blog is rich in recipes, thanks again. 

so for photos of your essays send me on this email: [ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

![Caknut2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/Caknut21.jpg)

[ marble cake ](<https://www.amourdecuisine.fr/article-cake-marbre-a-la-pate-a-tartiner-114502897.html>) nutella at [ Culinary horizon ](<http://horizon-culinaire.over-blog.com>)

[ chocolate cake without egg ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-sans-oeufs-108210207.html>) in [ Assia delicacies ](<http://gourmandisesassia-91.over-blog.com>)

[ ![chocolate cake without rosa egg](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gateau-au-chocolat-sans-oeuf-de-rosa_thumb1.jpg) ](<http://amour-de-cuisine.com/wp-content/uploads/2013/02/gateau-au-chocolat-sans-oeuf-de-rosa.jpg>)

[ chocolate cake without egg ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-sans-oeufs-108210207.html>) at Nathalie's place 

[ carrot cake ](<https://www.amourdecuisine.fr/article-gateau-aux-carottes-114177990.html>) at Natalie's place 

[ Apple pie ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-au-beurre-67056341.html>) at Katia 

[ lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee-114713045.html>) at Katia 

[ cake brioche ](<https://www.amourdecuisine.fr/article-recette-couronne-des-rois-114096697.html>) at Eric's place 

![Orange Pancakes](http://art.devivre.fr/.a/6a0133f5a8e69b970b017ee7f3884d970d-800wi)

[ crown of briochee kings ](<https://www.amourdecuisine.fr/article-recette-couronne-des-rois-114096697.html>) at souhila 

[ leafed cornet ](<https://www.amourdecuisine.fr/article-cornets-sales-a-la-creme-au-thon-49023773.html>) at souhila 

[ shortbread with a robot ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>) at souhila 

[ Tunisian tajine ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks-88766924.html>) at Sylvie D 

[ tagine with olives and chicken ](<https://www.amourdecuisine.fr/article-poulet-aux-olives-111188073.html>) at Assia k 

[ ![Chahirakay Blog: Jasmine Palace., CAKE AT THE MASCARPONE.](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/1360076289981.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/1360076289981.jpg>)

[ mascarpone cake ](<https://www.amourdecuisine.fr/article-33378410.html>) at Jasmine Palace 

[ cake without cooking ](<https://www.amourdecuisine.fr/article-gateau-algerien-sans-cuisson-bniouen-farci-101810986.html>) at lila B 

[ salmon and spinach quiche ](<https://www.amourdecuisine.fr/article-quiche-aux-epinards-et-saumon-114800663.html>) at Linda B 

[ breads with olives ](<https://www.amourdecuisine.fr/article-pains-aux-olives-cuisine-algerienne-108119096.html>) at Linda B 

![022-copy-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/022-copie-11.jpg)
