---
title: chicken stuffed with mango and pistachio
date: '2018-03-06'
categories:
- diverse cuisine
- Healthy cuisine
tags:
- Easy cooking
- Healthy cuisine
- Fast Food
- dishes
- accompaniment
- Chicken breast
- creams

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/poulet-au-pistache-1024x849.jpg
---
[ ![pistachio chicken](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/poulet-au-pistache-1024x849.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/poulet-au-pistache.jpg>)

##  chicken stuffed with mango and pistachio 

Hello everybody, 

Today I give way to the recipes of my readers, and it is with one of my friends of facebook that we will discover this very nice recipe chicken stuffed with mango and pistachio, it is a recipe of **HJM.**

**chicken stuffed with mango and pistachio**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/poulet-au-pistache-1024x849.jpg)

**Ingredients**

  * chicken cutlet according to family members 
  * 4 mangoes (2 cut in julienne and 2 in small cubes) 
  * pistachios 
  * salt and pepper 
  * 2 tablespoons of oil 
  * 1 tablespoon of butter 
  * 2 tablespoons of honey 
  * 2 to 3 tablespoons liquid cream at 35% 



**Realization steps**

  1. spread the chicken schnitzel on a work surface and flatten them. 
  2. salt and pepper each piece of chicken 
  3. place the cut mangoes in julienne and the pistachios. 
  4. Roll everything tightly and hold the rolls with toothpicks. 
  5. in a pan, put a little butter and oil, put the rolls in the hot oil and brown on all sides. remove the chicken from the pan and place in a dish and put in the oven for 20 to 30 minutes. 
  6. in the meantime put the diced mangoes in a saucepan with a little water and let it cook 
  7. before putting out the fire add one or two tablespoon of honey. 
  8. Once cooked, put the mangoes in the mixer bowl. Mix until smooth purée at the end add cream. 
  9. take out the chicken cutlets and cut the roll into pieces. 
  10. Arrange on the plate. Pour the sauce over the pieces. 
  11. Suggestion; accompany the chicken with vegetables (boiled with a knorr chicken base and sear in the pan before serving)   
Enjoy your meal! 


