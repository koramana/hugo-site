---
title: chamia, kalb el louz mahchi algerian pastry
date: '2014-06-16'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/qalb-elouz-020-300x199.jpg
---
##  chamia, kalb el louz mahchi algerian pastry 

Hello everybody, 

here is a sublime sweet recipe, a [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) a classic of [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , the qalb el louz, also called Chamia in other regions, a pastry shop that is just like [ baklawa ](<https://www.amourdecuisine.fr/article-baklawa-de-l-aid-el-fitr-2011-82649365.html>) and the [ Makrout ](<https://www.amourdecuisine.fr/article-54307532.html>) remain irresistible. 

in Algeria, during the month of Ramadan, I did not do this at all, because we have a pastry chef in the neighborhood, who makes an unequal qalb el louz, and I ate myself and the children without any moderation. 

back here in England, the children still asked me for qalb el louz, this time, the pastry chef is not near, will have to get started in the furnace .... 

and ...... again another success, I followed the recipe to the letter, I controlled the cooking, especially that now I have a good oven, and the result was perfect. 

more 

Before you pass the recipe, I just want to tell you, that the recipe of qalb el louz, Kalb ellouz. and despite the simple ingredients available to everyone, it remains a recipe not easy to succeed at all, and all this, can be because of the oven, if it is not a good oven that gives a good heat, it is because of the semolina, sometimes if you water the qalb badly praise the output of the oven, so if you miss the recipe, do not blame, the recipe itself, because if you go to the pastry chef of the neighborhood it will give you the same recipe, without any gram of difference, and you can even get the recipe right the first time, and the day you want to do it again, you miss it. 

for my part, I already managed this recipe with the method of [ Radotouille ](<https://www.amourdecuisine.fr/article-35768313.html>) , as I did with Linda's method, and my children loved it a lot. 

**chamia, kalb el louz mahchi algerian pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/qalb-elouz-020-300x199.jpg)

portions:  20  Prep time:  60 mins  cooking:  60 mins  total:  2 hours 

**Ingredients** for a round mold of 40 cm (for my part it was a rectangular pyrex mold 20 by 40 cm). 

  * 1kg of coarse semolina (durum wheat) we can use the average but I advise you the big one (I did the medium and big mixture, because I had a semolina too big, and it is not the semolina of the couscous) . 
  * 600g of crystallized sugar. 
  * 100g of melted butter. 
  * ¼ liter (half water and half water orange blossom 125ml each). 
  * 1 pinch of salt. 
  * 1 teaspoon of liquid vanilla. 

For the stuffing: 
  * 100g ground almonds. 
  * 2 tablespoons of cocoa 
  * For water syrup 1.5 
  * 500g of sugar 
  * 1 glass of orange blossom water tea. 



**Realization steps**

  1. Mix the semolina with the sugar and the salt and the vanilla and add the melted butter cooled, 
  2. sand well with the apple of the hands add half of water (water + water of orange blossoms) 
  3. mix, cover and leave overnight or morning. 
  4. Prepare the syrup so that it can cool put everything in the pot with a thick bottom from the first broth put on moderate fire for 
  5. in the end get about 1.2l of syrup book it. 
  6. Take again the mixture of semolina rub with the hands 
  7. then add the remaining water and mix well, 
  8. grease your dish with butter 
  9. put half of the dough and sprinkle ground almonds top mixed with cocoa 
  10. put the other half tamp well with the hands cut deeply squares with the knife and put in the center of each square an almond 
  11. put in preheated oven at 200 ° C for 45 minutes even 1 hour all depends on the oven, 
  12. at the end pour the syrup gently with a ladle and put back in the oven off. 
  13. let cool at least 5 or 6 hours or preferably overnight and then cut with a knife that you will dip each time in hot water and 
  14. put in the boxes. 



merci a linda pour cette delicieuse recette, qu’on a beaucoup aimer 
