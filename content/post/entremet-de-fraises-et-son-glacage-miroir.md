---
title: Strawberry entremet and mirror glaze
date: '2017-03-14'
categories:
- gateaux d'anniversaires, de fetes, celebrations
- recettes sucrees
tags:
- Red fruits
- Valentine's day
- Genoese
- Chocolate cake
- Birthday cake
- desserts
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/Entremet-de-fraises-et-son-gla%C3%A7age-miroir.jpg
---
![Strawberry entremet and mirror glaze](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/Entremet-de-fraises-et-son-gla%C3%A7age-miroir.jpg)

##  Strawberry entremet and mirror glaze 

Hello everybody, 

it is sublime this Entremet of strawberries and mirror frosting! I'm very proud of it ... I really like to make Bavarian sweets to celebrate a birthday, because it happens in the blink of an eye. 

While the preparation takes a little time and requires a little patience, but to be honest, the entremet itself is super easy, and if you have good quality gelatin, the success of this dessert of strawberries and icing mirror is guaranteed without problem 

**Strawberry entremet and mirror glaze**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/mon-gateau-danniversaire.jpg)

**Ingredients** Chocolate biscuit : 

  * 2 eggs 
  * 60 gr of sugar 
  * 50 gr of flour 
  * 1 C. cocoa 

Mirror icing: 
  * 75 gr of water 
  * 5 sheets of gelatin 
  * 150 gr of sugar 
  * 150 grams of glucose 
  * 150 gr of white chocolate 
  * 100 gr of sweetened condensed milk 
  * pink dye 

Insert of strawberries: 
  * 250 gr of strawberries 
  * 2 sheets of gelatin 
  * 50 gr of sugar 

Strawberry mousse: 
  * 250 gr of strawberries 
  * 6 sheets of gelatin 
  * 300 ml of liquid cream 
  * 80 gr of powdered sugar 
  * 50 gr of icing sugar 



**Realization steps** preparation of the mirror glaze: 

  1. Dip the gelatin sheets in cold water 10 min before 
  2. In a saucepan, heat the water, glucose, and sugar until boiling (103 ° C) remove from heat. 
  3. pour this mixture over the condensed milk, add the drained gelatin. 
  4. mix so that the gelatin melts, and add this mixture on the white chocolate and the pink dye, 
  5. as soon as the white chocolate melts, mix everything in the blender foot, 
  6. place in an airtight box, film on contact with food film and reserve a whole night in the fridge, 

preparation of the strawberry insert: 
  1. Soften gelatin in cold water 10 minutes before 
  2. To go to the mill or to blender the strawberries with the sugar to have a puree of strawberry. 
  3. Place in a saucepan and heat, 
  4. remove from heat, add gelatin and let cool 

preparation of the chocolate biscuit: 
  1. beat the eggs and the sugar until blanching, and have a good foamy mixture. 
  2. then add the flour and cocoa mixture well sifted in small quantities 
  3. pour the mixture into a 20 x 20 cm baking pan lined with baking paper. 
  4. cook for 12 minutes in a preheated oven at 180 ° C 

preparation of the strawberry mousse: 
  1. Soften gelatin in cold water 10 minutes before 
  2. To go to the mill or to blender the strawberries with the sugar to have a puree of strawberry. 
  3. Place in a saucepan and heat, 
  4. remove from heat, add gelatin and let cool 
  5. put the liquid cream in whipped cream, add the strawberry mixture gently. 

Mounting: 
  1. pour half of the strawberry mousse into the mold 
  2. place the strawberry insert that you cut in the same shape as your mold 
  3. cover with another layer of strawberry mousse. 
  4. place the cut chocolate biscuit over the same shape of the mold. 
  5. place in the freezer for one night. 
  6. The next day, remove and unmount the dessert. 
  7. heat the mirror glaze to 37 ° C to liquify it. 
  8. let cool to 35 ° C and cover your dessert with. 
  9. return to the fridge until tasting. 



![mounting](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/montage.jpg)
