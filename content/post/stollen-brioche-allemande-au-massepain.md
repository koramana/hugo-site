---
title: Stollen German brioche with marzipan
date: '2014-11-23'
categories:
- Buns and pastries
- Mina el forn
- sweet recipes
tags:
- Bread
- shaping
- Boulange
- Pastries
- Boulange
- Breakfast
- Christmas

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/stollen-1.jpg
---
[ ![Stollen German Christmas buns with marzipan \(marzipan\)](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/stollen-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/stollen-1.jpg>)

##  Stollen German Christmas buns with marzipan (marzipan) 

Hello everybody, 

Before I give you the recipe for stollen d'Alsace with marzipan (marzipan), what do I do for you? 

We already feel Christmas here, especially when we go shopping, and for more than 3 weeks, where I go, I see stalls filled with **German brioche "the Stollen"** . Frankly it gave me every time a great desire to come home with his hands filled with this brioche ... But the little problem in the story, is that every time I read the ingredients in the package, I read that it contains rum, probably to macerate the raisins, suddenly, I prefer to abstain! 

But do you think I was going to let things go like this? No, I went for the recipe on my cookbooks, and it was not the stollen recipes that were missing, and there are several varieties. The one that tempted me the most is the recipe from **marzipan stollen** , **marzipan stollen** , that is to say **stollen with almond paste,** especially when we use the [ homemade almond paste ](<https://www.amourdecuisine.fr/article-pate-damande-maison-massepain.html> "homemade almond paste - marzipan")

The recipe is super easy to make, just as for any brioche, take all his preparation time, despite that we will say that the stollen, has a texture between the cake and the sweet bread ... 

In any case for me, cake, bread, or brioche, rest that the result was sublime, the children devoured everything, they even protest against the fact that I did not put a lot of almond paste in, lol. 

![recipe Stollen German Christmas bun with marzipan \(marzipan\)](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/stollen-4.jpg)

**Stollen German brioche with marzipan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/stollen-2.jpg)

portions:  12 to 15  Prep time:  40 mins  cooking:  35 mins  total:  1 hour 15 mins 

**Ingredients**

  * 250 gr of all-purpose flour 
  * 145 gr of warm milk (it's ok, ok) 
  * 7g of instant yeast or 21g of fresh yeast 
  * 5 g of salt 
  * 1 egg yolk 
  * 40 g softened butter 
  * 30 g of sugar 
  * the zest of an orange 
  * the zest of a lemon 
  * 180 g dried fruit black grapes, white grapes, and cranberries, (I buy it mixed on Lidl) 
  * 150 g of [ homemade marzipan ](<https://www.amourdecuisine.fr/article-pate-damande-maison-massepain.html> "homemade almond paste - marzipan") combined with a small yolk and a little butter for more sweetness. 
  * melted butter for garnish 
  * icing sugar to sprinkle 



**Realization steps** Before you start: 

  1. Prepare your dried fruits by soaking them in hot water for 15 minutes. 
  2. Rinse and leave in a colander over a bowl to allow excess water to drain. Set aside. 
  3. Prepare your marzipan (find [ homemade marzipan ](<https://www.amourdecuisine.fr/article-pate-damande-maison-massepain.html> "homemade almond paste - marzipan") here) by combining it with an egg yolk, add it in stages until you get the right consistency. then oil with a little butter and set aside. 

Prepare the stollen 
  1. In a bowl combine half of the flour (125 g), instant yeast, egg yolk and warm milk. Mix well with a whisk, cover and let stand for 30 minutes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-de-stollen.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-de-stollen.jpg>)
  2. Add the rest of the ingredients (except the dried fruits!) And knead for 10 to 12 minutes to have a smooth paste, supple and a little sticky   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-de-stollen-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-de-stollen-1.jpg>)
  3. at this point, add the dried fruits prepared, work a little to evenly distribute the dried fruits in the dough. 
  4. allow the dough to rest for 20 minutes or longer until doubled in size. 
  5. degas the dough a bit, and if it is too sticky, place it in a cool place so that you can give it the shape easily. 
  6. flatten the dough with a baking roll, making sure to leave the edges a little more tight and also leave the top half of the dough a little thicker than the bottom half.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/la-pate-de-stollen-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/la-pate-de-stollen-3.jpg>)
  7. roll the almond paste into a pudding of 2 cm and a half in diameter, and place it in the middle   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-de-stollen-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-de-stollen-6.jpg>)
  8. Cover the almond paste with one of the parts 
  9. Place the stollen on a baking sheet lined with baking paper and let rise   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/stollen-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/stollen-5.jpg>)
  10. If you feel that your dough has risen well, preheat the oven to 190 ° C (for me, it took by this cold almost 1 hour and a half, that's how you will have a very light brioche) 
  11. Cook the stollen in the preheated oven for 35 minutes until golden brown. If you notice that the stollen takes the color quickly, lower the temperature to 160 ° C 
  12. Let the stollen cool on a rack for 10 minutes, then brush with a little melted butter. 
  13. When it is completely chilled, you can sprinkle with the icing sugar. 



[ ![Stollen German Christmas buns with marzipan \(marzipan\)](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/stollen-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/stollen-3.jpg>)
