---
title: Thai fish fillet with tomatoes and spices
date: '2014-01-24'
categories:
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/thai.jpg
---
[ ![Thai](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/thai.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/thai.jpg>) Hello everybody, 

I am Julia, and I would like to share with you this Thai Fish Filet recipe with tomatoes and spices. It is not easy to eat while trying to lose weight. 

It is classified as highly restricted diets. All this does not prevent to feast next door. 

I propose you today a recipe in agreement with this diet, the filet of Thai fish with tomatoes and spices! 

**Thai fish fillet with tomatoes and spices**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/thai.jpg)

portions:  1  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 5 g of fresh garlic 
  * 2 teaspoons shallot 
  * 1 g of bird pepper 
  * 2 tomatoes 
  * ½ lemon 
  * 1 pinch of green pepper seed 
  * 2 teaspoons fresh coriander 
  * 1 fine eagle fillet 
  * 5 g of fish stock 



**Realization steps**

  1. Start by frying garlic, shallots and bird pepper in a pan and let cool 
  2. Peel the tomatoes, mix them with the lemon juice and add 2 teaspoons of fish stock 
  3. Stir the garlic with the peppercorns and the two spoons of coriander until you get a paste 
  4. Add remaining fish stock and mix 
  5. Once well mixed, add the mixture to the sauce previously prepared 
  6. Place fish fillets in a deep plate and cover with sauce 
  7. Smooth marinate 1 hour and cook the fish in the pan on high heat to seize 
  8. Reduce the heat and deglaze with the marinade 
  9. Decorate with Thai rice and some salad leaves 


