---
title: brains of lamb in tomato sauce - chtitha mokh
date: '2013-10-19'
categories:
- Chhiwate Choumicha

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/k-014_thumb.jpg
---
##  brains of lamb in tomato sauce - chtitha mokh 

Hello everybody, 

the photos of this recipe of lamb brains in tomato sauce - chtitha mokh come directly from Algeria, from the kitchen of my dear mother. 

My mother managed super good to present us pretty pictures, this super delicious platm is not it? so without delay I'll give you my mother's recipe:   


**brains of lamb in tomato sauce - chtitha mokh**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/k-014_thumb.jpg)

Recipe type:  dish, recipe with lamb,  portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 2 brains of lamb 
  * 2 to 3 large grated tomatoes 
  * 1 tablespoon of tomato paste 
  * parsley 
  * 2 cloves garlic 
  * pepper 
  * 3 tablespoons olive oil 
  * salt 



**Realization steps**

  1. brown the crushed garlic in the oil. 
  2. Put the grated tomato and the tomato paste in a pan, 
  3. season with salt and black pepper, 
  4. cook tomato sauce well 
  5. add the brains cut in slices, 
  6. Mix well and cook over low heat for 20 minutes. 
  7. decorate with parsley 



ça fait longtemps que je n’ai pas manger la cervelle d’agneau en sauce tomate – chtitha mokh. Ces photos me donnent une grande envie de plonger un bout de pain pour me régaler, huuuuuuuuuuuuum ! 
