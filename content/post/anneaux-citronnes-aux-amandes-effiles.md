---
title: lemon rings with slender almonds
date: '2015-03-12'
categories:
- gateaux algeriens au glaçage
- Algerian cakes- oriental- modern- fete aid
- gâteaux sablés, ghribiya
- Algerian dry cakes, petits fours
tags:
- Algerian cakes
- Oriental pastry
- Algerian patisserie
- Cookies
- Shortbread cookies
- biscuits
- To taste
- Small cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/anneaux-citronn%C3%A9s-aux-amandes-effil%C3%A9s-4.jpg
---
[ ![lemon rings with slender almonds 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/anneaux-citronn%C3%A9s-aux-amandes-effil%C3%A9s-4.jpg) ](<https://www.amourdecuisine.fr/article-anneaux-citronnes-aux-amandes-effiles.html/anneaux-citronnes-aux-amandes-effiles-4>)

##  lemon rings with slender almonds 

Hello everybody, 

The cookies are very popular because it's always easy to do, and you can decorate them with a thousand and one ways. Today one of my readers who prefers to be known as: A Moment of Pleasure, enjoyed sharing with us these delicious biscuits: Lemon rings with slender almonds. 

Very delicious shortbread crispy cakes, with a strong taste of lemon, and crunchy almonds flaked previously, for more flavor. 

Thanks to you my dear for this nice and delicious sharing, and your next recipes. If you also want to see your recipes here on my blog, you can publish it by following this link: [ suggest your recipes ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.") , and if you can not put the recipe, send it to me on my email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

[ ![shortbread with lemon](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/sabl%C3%A9s-au-citron.jpg) ](<https://www.amourdecuisine.fr/article-anneaux-citronnes-aux-amandes-effiles.html/sables-au-citron>)   


**lemon rings with slender almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/anneaux-citronn%C3%A9s-aux-amandes-effil%C3%A9s-7.jpg)

portions:  30  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 250 g flour 
  * 125 g butter 
  * 60 g icing sugar 
  * ½ sachet of baking powder 
  * 1 egg 
  * zest of 2 lemons 

For icing: 
  * Juice of a lemon 
  * 200 g icing sugar 
  * tapered almonds 



**Realization steps**

  1. Beat the butter and sugar until you get an ointment texture. 
  2. Add the flour, the baking powder, the lemon peel and then pick it up with the egg. 
  3. Flour your work plan then spread your dough on a floured work surface 
  4. Using a cookie cutter, cut out your rings 
  5. put them in a floured plate or covered with parchment paper. 
  6. Bake for 10 to 15 minutes in a preheated oven at 180 ° C. 
  7. Let the rings cool before icing, with lemon frosting   
[ ![lemon rings with slivered almonds 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/anneaux-citronn%C3%A9s-aux-amandes-effil%C3%A9s-300x300.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40962>)
  8. Decorate with tapered almonds .. 
  9. leave it dry and it's ready! 



[ ![lemon rings with slivered almonds 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/anneaux-citronn%C3%A9s-aux-amandes-effil%C3%A9s-5.jpg) ](<https://www.amourdecuisine.fr/article-anneaux-citronnes-aux-amandes-effiles.html/anneaux-citronnes-aux-amandes-effiles-5>)
