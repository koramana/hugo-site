---
title: Brownie cheesecake easy and delicious recipe
date: '2014-03-28'
categories:
- cheesecakes and tiramisus
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/03/brownie-cheesecake-022.CR2_.jpg
---
[ ![brownie cheesecake 022.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/brownie-cheesecake-022.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/brownie-cheesecake-022.CR2_.jpg>)

##  Brownie cheesecake easy and delicious recipe 

Hello everybody, 

Here is a brownie cheesecake recipe that I kept under my elbow for a long time, and between pregnancy, childbirth and the baby, I could not achieve it, plus one of my readers, who also lives in Bristol , came to visit me to see the baby and took me a nice plate of this cake ... This convinced me to make the recipe I had, because this brownie cheesecake is just a real treat. 

The only thing I will advise you is to decrease the measure of sugar ... To my taste, the layer of brownie was a little sweet, but it's my fault, because I go with the English, and I know they like many sweet recipes, and I did not decrease the measure of sugar.   


**Brownie cheesecake easy and delicious recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/brownie-cheesecake-035.CR2_.jpg)

Cooked:  American  Recipe type:  dessert  portions:  12-15  Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients** brownie layer 

  * ½ cup of unsweetened cocoa powder 
  * 1 teaspoon baking powder 
  * 1 ¼ cup flour 
  * ½ teaspoon of salt 
  * 1 teaspoon of vanilla extract 
  * ¾ cup of melted butter 
  * 2 cups of sugar   
you have to put 1 cup and a half 
  * 3 eggs 

Cheesecake layer: 
  * 220 gr of pheladelphia cheese at room temperature. 
  * ½ cup of sugar 
  * 2 tablespoons flour 
  * 1 egg 



**Realization steps**

  1. Prepare the brownie layer: 
  2. Mix cocoa, chicmic yeast, flour and salt. Put aside. 
  3. In a large bowl, combine eggs, melted butter, sugar and vanilla extract. 
  4. Beat with a wooden spoon until smooth. 
  5. Add the dry ingredients and mix. 
  6. Reserve ½ cup of brownie dough for swirling, and spread remaining dough evenly in a mold (20 by 20 cm if you want a top cake, otherwise use a wider pan) lined with cling film   
leave the tier to make the mottling over it. 

the layer of cheesecake 
  1. In the bowl of an electric mixer, combine cheese, sugar and egg on low speed until smooth. Add the flour and mix until homogeneous. 
  2. Spread layer of cheese on top of the brownie dough in a regular layer. 
  3. Using a tablespoon, put remaining brownie dough over the layer of cheese and gently mix with a knife. 
  4. Bake in preheated oven at 180 degrees for 35-36 minutes. 
  5. Cool, then refrigerate. 
  6. Once the cheesecake brownie has completely cooled down, you can carefully invert the mold and remove the brownie on a chopping board and cut into squares. Keep refrigerated. 



[ ![brownie cheesecake 014](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/brownie-cheesecake-014.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/brownie-cheesecake-014.jpg>)

if you have tried one of my recipes, send me the picture here: 

**[ Mes recettes chez vous ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **
