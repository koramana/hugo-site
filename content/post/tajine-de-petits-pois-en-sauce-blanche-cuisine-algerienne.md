---
title: Tajine of peas in white sauce / Algerian cuisine
date: '2017-04-02'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/petits-pois-a-la-sauce-blanche-033.CR2_1.jpg
---
[ ![peas-in-the-white-sauce-033.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/petits-pois-a-la-sauce-blanche-033.CR2_1.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-en-sauce-blanche-cuisine-algerienne.html/petits-pois-a-la-sauce-blanche-033-cr2_1-2>)

##  Tajine of peas in white sauce / Algerian cuisine 

Hello everybody, 

at home we really like peas, and this time, I realized this tagine peas in white sauce, a dish very very good, but which was not up to my husband (if you're faithful to my blog you must know that my husband does not like dishes in white sauce !!!) 

**Tajine of peas in white sauce / Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/petits-pois-a-la-sauce-blanche-027.CR2_1.jpg)

Recipe type:  Tagine dish  portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 300 gr of mutton 
  * 1 kg of peas. 
  * 250 gr artichokes 
  * salt 
  * black pepper 
  * 1 onion 
  * 2 tablespoons of butter 
  * 1 egg 
  * 1 lemon 
  * parsley 



**Realization steps**

  1. Scoop the peas, wash them and strain them in a colander. 
  2. In the butter, cook the chopped meat, grated onion, salt and pepper for 10 minutes. 
  3. Add the peas, sprinkle with 3 glasses of water and cook for 20 minutes. 
  4. Then plunge the cleaned artichoke hearts and continue cooking for another 20 minutes, add water if necessary. 
  5. After cooking and before serving, make a mixture of egg yolk, lemon juice and finely chopped parsley, and pour out of the heat into the sauce. 
  6. Serve this hot dish topped with lemon slices 



[ ![peas-in-the-white-sauce-031.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/petits-pois-a-la-sauce-blanche-031.CR2_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/06/petits-pois-a-la-sauce-blanche-031.CR2_1.jpg>)
