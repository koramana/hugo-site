---
title: halwat el lambout cake - mohgone
date: '2011-12-29'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/halwat-el-lambout_thumb1.jpg
---
##  halwat el lambout cake - mohgone 

Hello everybody, 

Yesterday, to escape some of the paint and these smells, I had a great desire for this cake that my mother often made us cake halwat el lambout - mohgone, as it was called in some regions, a delicious dry cake that can accompany tasty tea or coffee. 

as it is a traditional recipe I believe that each person has its way or rather its ingredients to prepare it, for me here are the ingredients of my mother:   


**halwat el lambout cake - mohgone**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/halwat-el-lambout_thumb1.jpg)

**Ingredients**

  * 4 eggs 
  * 250 grams of sugar 
  * 100 ml of melted butter 
  * 150 ml of oil (you can only use oil) 
  * 1 cup of baking powder 
  * 1 cup of vanilla coffee 
  * of flour 



**Realization steps**

  1. in a large salad bowl, whisk the eggs is the sugar 
  2. add the vanilla, the oil and butter mixture 
  3. keep whipping 
  4. add the sifted flour mixture, baking powder gradually to the previous mixture until you obtain a malleable dough and you can shape using a nozzle 
  5. shape rods 7 to 8 cm, or circles. 
  6. place them directly on an oiled baking sheet 
  7. preheat the oven to 170 degrees 
  8. cook between 15 and 20 minutes 
  9. you can join each two pieces with jam of your taste, and coat the tip of a delicious chocolate, I did not have time for it 



![halwat el lambout cake - mohgone](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/lambout-003a_thumb_1.jpg)

Thank you very much for your comments and passages. 

Passez un agréable journée. 
