---
title: kak nakache with dates
date: '2016-08-12'
categories:
- Algerian cakes- oriental- modern- fete aid
- Gateaux Secs algeriens, petits fours
tags:
- Gateau Aid
- Aid cake
- Ramadan
- Ramadan 2016
- Pastry
- delicacies
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/08/kaak-nakache-aux-dattes.jpg
---
![kak nakache with dates](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/kaak-nakache-aux-dattes.jpg)

##  kak nakache with dates 

Hello everybody, 

The nakache kaak with dates is one of my favorite cakes, this nakache kaak so melting in the mouth, this pasta with just perfect taste, covering a super flavored layer of pasta dates, I assure you that you will not regret to prepare this Delight, plus I find that the nakache date kaak recipe is one of the easiest recipes to make, in addition to being a super presentable cake. 

I have a lot of fun decorating these pretty kaak nakache with the pliers, although it's not my strong point, but I'm very proud of the result. If you do not like date stuffing in your Kaak nakache, you can see the recipe for [ kaak nakache with almonds ](<https://www.amourdecuisine.fr/article-kaak-nakache-gateaux-algeriens.html>) . 

![kaak nakache with dates 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/kaak-nakache-aux-dattes-1.jpg)

**kak nakache with dates**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/08/kaak-nakache-aux-dattes-3.jpg)

portions:  50  Prep time:  20 mins  cooking:  12 mins  total:  32 mins 

**Ingredients** for the dough: 

  * 3 measures of flour 
  * 1 measure full of smen 
  * 1 measure of water mix with orange blossom water 
  * 1 pinch of baking powder 
  * 1 teaspoon vanilla 
  * ½ measure of sugar 
  * 1 pinch of salt 

for the stuffing: 
  * 500 gr of date paste 
  * ½ teaspoon cinnamon 
  * ½ teaspoon ground cloves 
  * 2 teaspoons of butter 
  * 2 teaspoons of jam (optional, I had put some, because I had a poor date paste, and so I was afraid that the stuffing becomes hard when cooked, I added the fig jam , which will make the dough soft) 
  * Orange tree Flower water. 



**Realization steps** method of preparation: 

  1. sift the flour, salt, sugar and vanilla, 
  2. add the melted fat and cooled (it should not be hot, otherwise it will seem that there is too much fat). 
  3. mix everything by rubbing well between the hands to adhere the fat in the flour and then collect with orange blossom water to obtain a smooth and firm paste, 
  4. cover and let stand for half an hour. 

prepare the date paste: 
  1. Work the date paste with the ingredients previously given until you have a soft ball. let it rest. 

preparation of the kaak nakache: 
  1. take a quantity of the dough and lower it finely. 
  2. cut a rectangle of almost 12 cm wide, and place a roll of 1 cm in diameter, the length of the rectangle, on the end. 
  3. wrap the dough in two rounds to cover the stuffing, and glue both ends to form a rings. Glue with a little orange blossom water. 
  4. Decorate according to your taste with the nakache pliers 
  5. place the kak as you go in a baking tray. 
  6. bake in a preheated oven at 180 ° C for almost 12 minutes, until the kaak turns a beautiful golden color. 
  7. let cool before taking off these delicious cakes. 


