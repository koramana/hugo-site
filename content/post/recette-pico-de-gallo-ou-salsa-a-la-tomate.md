---
title: pico de gallo recipe or tomato salsa
date: '2016-02-23'
categories:
- appetizer, tapas, appetizer
- dips and sauces
tags:
- sauces
- Fast Food
- Mexico
- Easy cooking
- Dips
- Tacos
- nachos

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-pico-de-gallo.jpg
---
[ ![pico de gallo recipe or tomato salsa](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-pico-de-gallo.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-pico-de-gallo.jpg>)

##  pico de gallo recipe or tomato salsa 

Hello everybody, 

To make a recipe, I had to prepare this salsa with fresh tomato, and I must confess that the colors of this salsa known rather under the name of Pico de gallo, the colors I liked so much that I liked sharing the pictures with you. 

Pico de gallo is a name of Spanish origin for this Mexican salsa that wants to designate a mixture of diced tomatoes, onions and Jalapeños peppers. In any case after this salsa you will dance the "Samba" because waw, it is super high and very piquant this salsa. 

You can use this Salsa to make [ burritos ](<https://www.amourdecuisine.fr/article-paniers-de-burritos-au-poulet.html>) , or tacos, recipes to come. 

**pico de gallo recipe or tomato salsa**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-pico-de-gallo-2.jpg)

portions:  6  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 3 ripe tomatoes, cored and seeded, diced 
  * ½ red onion cut into small cubes 
  * 1 tbsp, chopped coriander 
  * 1 C. extra virgin olive oil 
  * ½ pepper finely chopped bird (optional) 
  * 1 pinch of salt 
  * 3 c. fresh lime juice 



**Realization steps**

  1. Put all the ingredients in a bowl and mix well. 
  2. Taste and adjust the seasoning according to your taste. 
  3. Cover and let stand for 30 minutes to have aromas and flavors even more pronounced. 



[ ![pico de gallo recipe or salsa with tomato 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-pico-de-gallo-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/recette-de-pico-de-gallo-1.jpg>)
