---
title: Berkoukes, cradles بركوكس
date: '2016-11-27'
categories:
- Algerian cuisine
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/berkoukes-028.CR2_thumb1.jpg
---
[ ![berkoukes 028.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/berkoukes-028.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-berkoukes-bercouces.html>)

##  Berkoukes, cradles بركوكس 

Hello everybody, 

The berkoukes, also called Ayche in eastern Algeria, is a delicious Algerian dish, which is prepared with pasta rolled by hand called "small sinkers". 

A complete and rich dish especially with the addition of vegetables in it, that we prepare with chicken, as in the recipe that I will pass you, or so, "gueddid" (dried meat ... .. a real delight). It is a dish that I do not fail to prepare often when it is cold, like these days. 

And you, what dish that warms you and comforts when it's cold? 

[ ![berkoukes with vegetables037.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/berkoukes-037.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-berkoukes-bercouces.html>)   


**Berkoukes بركوكس, burrows**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/berkoukes-019.CR2_thumb_11.jpg)

portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 2 to 3 chicken legs or any other pieces. 
  * 1 big onion. 
  * 2 cloves garlic. 
  * 1 handful of chickpeas deceived the day before. 
  * 1 carrot cut into small cubes 
  * 1 zucchini cut into small cubes 
  * 1 tablespoon and a half of tomato paste 
  * Paprika 
  * black pepper 
  * salt 
  * 1 glass and a half of small pellets. 
  * dried basil (optional) 
  * 2 to 3 tablespoons of oil 
  * knowing that you can put other vegetables, like turnips, potatoes ... 



**Realization steps**

  1. in a pot, over low heat, fry the chicken pieces in sunflower oil, with the grated onion, 
  2. add the garlic cut in 4 on length or shredded if you do not like the pieces of garlic. 
  3. then add spices, salt and diced vegetables and let them come back. 
  4. Add chickpeas and tomato paste. 
  5. saute, then water 2 to 2.5 liters. 
  6. let it cook well. 
  7. Once everything is tender, add the pasta all at once. 
  8. cook for 15 to 20 minutes while stirring a few times so that the pasta does not stick to the bottom of the pot. 
  9. The sauce will reduce slowly as the pasta cooks. 
  10. Serve hot, with a few crushed basil leaves and a drizzle of olive oil. 



[ ![berkoukes 006.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/berkoukes-006.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-berkoukes-bercouces.html>)
