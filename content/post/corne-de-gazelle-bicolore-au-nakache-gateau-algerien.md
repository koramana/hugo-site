---
title: two-tone gazelle horn with nakache / Algerian cake
date: '2013-06-18'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/corne-de-gazelle1.jpg
---
![horn-de-gazelle.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/corne-de-gazelle1.jpg)

##  two-tone gazelle horn with nakache / Algerian cake 

###  قرن لغزال ملون منقوش بالعسل 

Hello everybody, 

I really like them [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) **Gazelle horn** . my mother always made us **Gazelle horn** almonds, but I especially loved those prepared by my aunt **Gazelle horn** Ice. 

this time, I present this [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , in its modern form: **Gazelle horn** two-tone, honey and pinch, a beautiful makeover for a delicious **Algerian cake** . 

pull model of the book: **Oriental cakes** , step not step.   


**two-tone gazelle horn with nakache / Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/Cornes-de-Gazelle-bicolore-au-miel-056_thumb1.jpg)

Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients** dough: 

  * 250 gr of flour 
  * 75 gr softened margarine 
  * 1 pinch of salt 
  * 1 C. white dye coffee 
  * 1 C. a green dye coffee 
  * flower water 
  * water 

the joke: 
  * 250 gr of almonds 
  * 125 gr of sugar 
  * 1 C. a vanilla coffee 
  * 1 egg 
  * 1garniture: 
  * honey 
  * crushed pistachios. 



**Realization steps**

  1. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  2. Mix well with your fingertips 
  3. divide the dough into two parts 
  4. for the one you are going to color in white add the dye, then pick up the dough with flower water, add a little water to pick up, continue to knead a few seconds. 
  5. for the other color, take the other half of the shortbread, add the dyes to the orange water until the desired color is obtained, and start to collect the dough with this water, add the water if you need, and knead a few seconds. 
  6. cover your dough and let it sit on the side. 
  7. for stuffing mix almonds and dry ingredients, collect with egg and set aside. 
  8. spread the two pasta, pass the colored pasta in the pasta machine, side tagliatelles, place the colored strips on the white paste, trying to keep the tape distance between each strip. 
  9. iron the white dough with the colored strips in the dough machine, to stick them well, pass them to the number 6.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/corne-de-gazelle-bicolore-au-miel-au-nekache_thumb1.jpg)
  10. cut circles with the 8 cm diameter cookie cutter. 
  11. make cornets with the almond stuffing. 
  12. place them on the dough circles. roll up and give the shape of a horn, pinched with pliers. 
  13. bake for 10 minutes at 170 degrees, and after cooling soak gazelles horns in honey. 
  14. and cover the top of the cone with crushed pistachios. 



thank you for your visits 

**[ msaker gazelle horn ](<https://www.amourdecuisine.fr/article-tcharak-msakar-103050711.html>) **

**[ corne de gazelle a la pince « nakache » ](<https://www.amourdecuisine.fr/article-corn-de-gazelle-au-nekkache-a-la-pince-59752732.html>) **
