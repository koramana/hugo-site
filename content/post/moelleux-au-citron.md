---
title: In soft Citron
date: '2018-03-22'
categories:
- cakes and cakes
- sweet recipes
tags:
- Lemon cake
- Lemon cake
- Cakes
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/moelleux-au-citron-2.jpg
---
![In soft Citron](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/moelleux-au-citron-2.jpg)

##  In soft Citron 

Hello everybody, 

He has made his success at home, since that day, my husband asks for it and asks for more, especially since he has a little texture of the [ mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouscoutchou.html> "mouskoutchou / mouscoutchou") he likes a lot. He never gave me the opportunity to take pictures, hihihih. This time I took advantage that it left with the children to the swimming pool to realize it, without that it disturbs me .. 

But I had a little mouse at home, who was standing with the tripod of the camera, well again, it's hard for me to take pictures, Yes, baby is his law my friends ... 

**In soft Citron**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/moelleux-au-citron-1.jpg)

portions:  8  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients** For a mold 22 cm in diameter: 

  * 4 eggs 
  * 120 g of flour 
  * 150 g of sugar 
  * 80 g cooled melted butter 
  * 1 sachet of baking powder 
  * 1 pinch of salt 
  * 1 pinch of bicarbonate 
  * 120 ml of lemon juice (2 or 3 lemons) 
  * Grated zest of 1 beautiful lemon (or 2 small) 
  * Ice sugar to sprinkle 



**Realization steps**

  1. Preheat the oven to 180 ° C then butter and flour a 22-23 cm diameter missed pan (I used a silicone mold). 
  2. Sift together the flour, yeast, salt and baking soda. Mix. 
  3. Beat the egg yolks with the sugar until the mixture turns pale and sparkling (a good time). 
  4. Add the cooled melted butter, the finely grated lemon peel, the lemon juice (leave 2 tablespoons for the egg white). 
  5. Whip again and gradually introduce the flour mixture by whisking at low speed, without overworking the dough, just to incorporate it. 
  6. Beat the egg whites until very stiff with a pinch of salt and a drizzle of lemon juice (both soups already removed). 
  7. Slowly add the egg whites to the previous preparation using a spatula, without breaking them. 
  8. Bake at 180 ° C for 15 to 20 minutes or depending on your oven. 
  9. Let cool in the oven off and slightly ajar so that it does not sag. Once cooled, sprinkle with sifted icing sugar. 


