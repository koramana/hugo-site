---
title: croquettes dry cakes
date: '2012-06-29'
categories:
- Cupcakes, macarons, et autres pâtisseries

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquant-gateaux-secs-algeriens_thumb1.jpg
---
##  croquettes dry cakes 

Hello everybody, 

before you get the recipe: the dry cakes croquet that I prepared for the party at the school of Rayan, "Summer fair" or the summer fair, there will be games, "donkeys" toboggans , and cakes for sale, whose earnings will return to school ... 

So for this occasion, I liked to prepare a [ dry cake ](<https://www.amourdecuisine.fr/categorie-11814728.html>) "The crunchy" or croquet, which looks a bit like biscotti, I told myself at least that way we do not offer a cake too sweet for children, because I know that English moms will do a lot: brownies, cupcakes, flapejacks, and so on ... sugar shovel, hihihihi. 

Why croquet and not another Algerian cake? First, it is not a cake that will dirty, or may pick up microbes, because no cream or chocolate. Also, there are many children at my son's school who have dried fruit allergy, such as peanuts and others. 

And the most important point, as I told you this week, I'm too taken, because I will pass my exam the week ahead, this cake is very fast to prepare, and in the end, we have a large amount.   


**croquettes dry cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquant-gateaux-secs-algeriens_thumb1.jpg)

**Ingredients**

  * 6 eggs 
  * zest of a lemon 
  * 1 cup of vanilla coffee 
  * 300 grs of crystallized sugar (I do not like them too much sugar, I put 280 gr) 
  * 500 ml of table oil 
  * 2 tablespoons of baking powder 
  * 1 nice handful of raisins, which you let swell in a little water (you can put everything you like, almonds, peanuts ...) 
  * flour (allow at least 1 kg, depending on your flour) 

for gilding: 
  * 1 egg 
  * 1 cup of milk 
  * 1 cup of coffee (or a little instant coffee) 
  * 1 pinch of sugar 
  * 1 pinch of vanilla 



**Realization steps**

  1. in a large salad bowl, beat the eggs well, add the vanilla and vanilla sugar, 
  2. slowly add the sugar, then the oil, in a net. 
  3. continue to beat. 
  4. then add a little flour, then the baking powder, then add the raisins, and then continue to add your flour, until you obtain the soft and malleable paste, 
  5. do not put too much flour so that you will not have too much crunch. 
  6. even if to form the chopsticks, you find that the dough sticks to the hand, flour your hands for work, or your hands must be well oiled. 
  7. in a baking tray, place your chopsticks, spacing them well from each other. as in the photos above. 
  8. garnish your baguettes according to your taste, then brush with the egg mixture 
  9. (You can put only one egg for gilding, but I add vanilla, to no longer smell the eggs, and the other ingredients are to have a nice crunchy color) 
  10. the choice of the garnish after depends on your taste: sugar, coconut, ground almonds, peanuts, vermicelli in chocolate or in color ...... the list is very long. 
  11. cook your crisp in a pre-heated oven at 180 degrees C. 
  12. dice the release of your cake from the oven, 
  13. cut the cakes, lozenges according to the size you like, for me it is between 1.5 and 2 cm wide. 
  14. to have more crispy, you can put your crunchy, on the side, in a tray and put in the oven, for a small gilding from above and from below. 
  15. allow to cool well, then put in a hermetic box, can be preserved a long duration, and even can be frozen, for use in case of unforeseen visits. 


