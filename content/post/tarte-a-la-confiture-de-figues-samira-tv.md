---
title: pie with fig jam / samira tv
date: '2017-09-17'
categories:
- dessert, crumbles and bars
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-la-confiture-de-figues-003.CR2_-1024x757.jpg
---
![pie with fig jam / samira tv](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-la-confiture-de-figues-003.CR2_-1024x757.jpg)

##  pie with fig jam / samira tv 

Hello everybody, 

More simple and easy as this pie to achieve ??? I do not think you'll find any !! It was last week, while I was doing my housework, and the TV was on the Samira TV channel, that I saw this recipe for fig jam pie, and how much I like fig jam ... 

Especially since I brought with me a stock of homemade fig jam that I realized while I was in Algeria this summer. 

the recipe is very simple and easy, the hardest is to prepare the broken dough or the sweet dough   


**pie with fig jam / samira tv**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-la-confiture-de-figues-001.CR2_-1024x726.jpg)

Recipe type:  pie dessert  portions:  6  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * [ sweet dough ](<https://www.amourdecuisine.fr/article-pate-a-tarte-sucree.html> "Sweet pie dough") , or [ pastry ](<https://www.amourdecuisine.fr/article-pate-brisee.html> "Pastry")
  * [ FIG jam ](<https://www.amourdecuisine.fr/article-confiture-de-figues-en-video-119685090.html> "Jam of figs in video")
  * 1 egg 
  * 1 tablespoon butter at room temperature 



**Realization steps**

  1. after preparing the dough of your pie, spread it on tartlets. 
  2. pour in the equivalent of 2 teaspoons of fig jam. 
  3. cut strips and put them on the jam.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-la-confiture-de-figues.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-la-confiture-de-figues.jpg>)
  4. cook in a preheated oven at 180 degrees, until the tartlets turn a nice color from below. 
  5. during this time, in a saucepan mix the butter and the egg, bringing it on medium heat ... do not boil the butter, otherwise you will cook the egg. 
  6. remove the tartlets halfway through the oven, and pour in the equivalent of 2 tablespoons of this sauce. 
  7. put back in the oven, and let the pies take a nice golden color .. 
  8. remove from the oven, and let cool before serving, the jam keeps the heat. 



###  Fig jam pie 

preparation 

  1. After preparing your pie dough, spread on the pie mold. 
  2. for in the equivalent of 2 teaspoons of fig jam. 
  3. cut strips and layer on the jam.   
[ ![pie with fig jam / samira tv](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-la-confiture-de-figues.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-la-confiture-de-figues.jpg>)
  4. bake in a preheated oven at 180 degrees until the tarts take nice color. 
  5. mean while, in a saucepan mix butter and egg on medium heat ... do not boil butter, otherwise you will cook the egg. 
  6. remove the tarts that are halfway cooked for 2 tablespoons of the egg sauce. 
  7. put to cook again in the oven, and let the pies take a nice golden color .. 
  8. after well cooked, can not wait before serving, the jam will be really bearing !!! 



[ ![pie with fig jam / samira tv](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-la-confiture-de-figues-008.CR2_-728x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/tarte-a-la-confiture-de-figues-008.CR2_.jpg>)
