---
title: beetroot salad pie from Meriem
date: '2011-08-04'
categories:
- boissons jus et cocktail sans alcool
- Healthy cuisine

---
I share with you a recipe that one of my faithful readers to prepare, it is a recipe Meriem, and frankly I am very tempted to do it, surely what will not be on my table very soon. 

Ingredients: 

for 6 people you need 

2 beets 

2 carrots 

2 potatoes 

Vinaigrette (in sufficient quantity: oil, vinegar, salt, pepper, mustard) 

1 bowl of Mayonnaise (I prefer homemade) 

To decorate : 

Green olives, pitted and cut 

Hard beef 

**Preparation:**

Boil beetroots and peeled potatoes separately in salted water. Grate the carrots in a small bowl and let macerate in the vinaigrette, the time that the potato and beet cook. 

Once done, peel them. Start by draining and mashing the potato in a small bowl while adding the vinaigrette. Grate the cooked beets and season with the vinaigrette too. Let everything cool and prepare the presentation plate. 

Spread a little mayonnaise on the plate on which you will present your salad (square or round). Deposit the potato first (it serves as a platform), then spread mayonnaise then put on carrots, then mayonnaise (you will say hello the kilos but it's good I guarantee you) and put the layer of beetroot, also covered with mayonnaise. This last layer will be colored with the pretty beet color. Decorate with olives and boiled eggs (optional). Keep cool for a few hours (I do it the day before for the next day) .It leave 10min before being consumed. 

For those who have a problem with cholesterol, remove the mayonnaise, and grate a hard-boiled egg on the last layer of beetroot that will give a pink marbled appearance. 

thank you very much to you Meriem, thank you all for your fidelity. 

kisses 

just one thing, i just know that meriem has made his blog, which came out today and i pass you his link: 
