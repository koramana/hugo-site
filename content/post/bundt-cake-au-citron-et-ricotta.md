---
title: bundt cake with lemon and ricotta
date: '2016-04-26'
categories:
- cakes and cakes
- sweet recipes
tags:
- Cakes
- desserts
- To taste
- Ramadan 2016
- Mellow cake
- Lemon cake
- Fluffy cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta-3.jpg
---
[ ![bundt cake with lemon and ricotta 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta-1.jpg>)

##  bundt cake with lemon and ricotta 

Hello everybody, 

My husband loves cakes and cakes with lemon. The [ lemon cake ](<https://www.amourdecuisine.fr/article-cake-au-citron-extra-moelleux-et-facile-112488789.html>) is his favorite, he asks for it every time. This time I decided to change the recipe a little, without him noticing it, finally I just added ricotta to the recipe, especially that the box expired. 

In fact I wanted to make this bundt cake with lemon made from whole cream (double cream here in England) as prepared by **Chief Raymond Blanc,** I always watch his show on TV, and I do not tell you, I already like his accent when he speaks English, not to mention how he cooks, everything becomes child's play. In any case, I hope to realize its lemon cake soon, and I pass you the recipe of this **bundt cake with lemon and ricotta** which is soft at wish, super perfumed and above all too good. 

[ ![bundt cake with lemon and ricotta 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta-1.jpg>)

You can always add a spoon of poppy seeds, to have this beautiful effect in the texture, in addition the lemon marries super well with poppy seeds, so you do not risk being disappointed. 

I would have liked to add poppy seeds, but my husband does not like at all, nor my youngest by the way .... When I make a loaf and I decorate the top with poppy seeds, he removes seed by seed to eat his piece (it makes me laugh, hihihih)   


**bundt cake with lemon and ricotta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta-4.jpg)

portions:  10  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 390 gr all-purpose flour 
  * 2 tbsp. coffee baking powder 
  * 1 pinch of salt 
  * 3 large eggs at room temperature 
  * 260 gr of powdered sugar 
  * 120 ml of table oil 
  * Zest of 3 unprocessed lemons 
  * juice of 1 lemon 
  * 250 gr Ricotta cheese 
  * 2 tbsp. coffee vanilla extract 
  * 2 tablespoons apricot jam for   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta-2.jpg>)



**Realization steps**

  1. preheat the oven to 180 ° C. 
  2. Spray your savarin baking pan generously with cooking oil and set aside. 
  3. Sift the flour, baking powder and salt into a bowl and set aside. 
  4. In the bowl of the beater, beat the eggs, add the lemon juice of the oil and the sugar, beat until smooth. 
  5. Add the ricotta, lemon zest and vanilla, and whip again. 
  6. Add the flour mixture gently and mix without mixing too much, until you have a homogeneous dough 
  7. pour into prepared savarin baking tin and bake for 45-50 minutes, or until golden brown and a toothpick inserted in the center of the cake comes out clean. 
  8. Let cool in the pan for 5 to 10 minutes, then spill on a wire rack, brush with the warm jam and allow to cool completely. 



[ ![bundt cake with lemon and ricotta](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/bundt-cake-au-citron-et-ricotta.jpg>)
