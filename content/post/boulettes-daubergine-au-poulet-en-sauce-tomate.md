---
title: Eggplant dumplings with chicken in tomato sauce
date: '2015-04-04'
categories:
- amuse bouche, tapas, mise en bouche
- cuisine algerienne
- cuisine diverse
- idee, recette de fete, aperitif apero dinatoire
- Dishes and salty recipes
tags:
- Vegetables
- Algeria
- inputs
- Easy cooking
- Bouchees
- Aperitif
- aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/boulettes-daubergine-au-poulet.jpg
---
[ ![eggplant dumplings with chicken](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/boulettes-daubergine-au-poulet.jpg) ](<https://www.amourdecuisine.fr/article-boulettes-daubergine-au-poulet-en-sauce-tomate.html/boulettes-daubergine-au-poulet>)

**Eggplant dumplings with chicken on a bed of tomato sauce**

Hello everybody, 

Eggplant is a very delicious vegetable, you just have to know how to cook it, and you will have a dish that will delight all those who enjoy it. For my part, aubergines I really like, I can tell you that I make a recipe aubergines every week, whether it's a simple [ eggplant caviar ](<https://www.amourdecuisine.fr/article-caviar-d-aubergines.html> "Eggplant caviar") , a [ aubergine gratin ](<https://www.amourdecuisine.fr/article-recette-de-gratin-daubergines.html> "aubergine gratin recipe") so yumi, or [ aubergines stuffed with minced meat ](<https://www.amourdecuisine.fr/article-aubergines-farcies-2.html> "Stuffed eggplant") . 

In any case, a new recipe adds to my collection of [ eggplant recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=aubergine&sa=Rechercher>) , these eggplant dumplings with chicken on a bed of tomato sauce, a recipe made by one of my readers **Tifa Nahanis,** I hope you will like this recipe just like me. 

**Eggplant dumplings with chicken in tomato sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/boulettes-daubergine-au-poulet.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 400 g chopped chicken (about ½ large breast) 
  * 3 medium eggplants 
  * 2 tbsp. chopped fresh coriander 
  * 3 cloves of garlic cut into slices 
  * 2 tbsp. cumin 
  * salt and pepper 

For breading: 
  * flour 
  * beaten egg 
  * breadcrumbs 

For the tomato sauce: 
  * 1 can of peeled and crushed tomatoes (or grated tomatoes) 
  * 2 tablespoons of oil 
  * 1 clove of garlic 
  * salt, black pepper, paprika 
  * 1 branch of thyme 
  * dry or fresh basil 
  * fillet of olive oil 



**Realization steps**

  1. Start by preparing the tomato sauce by cooking all the ingredients over medium heat, until the sauce is reduced 
  2. at the end of cooking, season to taste, and sprinkle with a nice drizzle of olive oil, reserve. 

prepare the eggplant and chicken balls: 
  1. Make small cuts in the whole aubergines and put in the garlic, 
  2. wrap them in foil and put them in the oven until they are soft, then let them cool and drain. 
  3. Peel the eggplants, crush them and mix them with the ground chicken meat 
  4. add the spices, coriander to have a homogeneous mixture.   
Add breadcrumbs if the preparation is too soft. 
  5. Make medium sized dumplings. Pass the balls in the flour then in the egg and finally in the breadcrumbs. 
  6. Cook the meatballs in a pan with butter + oil, 
  7. drain and serve with tomato sauce. 


