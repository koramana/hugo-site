---
title: cake with sardines, salty cake
date: '2012-07-18'
categories:
- cuisine marocaine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/3097455211.jpg
---
hello everyone, I do not present on my blog, a lot of fish recipe, because here we do not find fresh fish, so when we go to Algeria, we consume it thoroughly. and this is a dish that my sister-in-law prepares as a chef. so for the ingredients we have (for 8 people): 2 kilos of fresh sardine 4 clove of garlic 1 onion 1 bunch of parsley 1 egg salt, pepper, cumin, paprika and black pepper 3 hard eggs (cooked) Start all first by cleaning the sardines, removing the stop & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.56  (  2  ratings)  0 

![S7303884](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/3097455211.jpg)

Hello everybody, 

I do not present on my blog, a lot of recipe based on fish, because here we do not find fresh fish, so when we go to Algeria, we consume it thoroughly. 

and this is a dish that my sister-in-law prepares as a chef. 

so for the ingredients we have (for 8 people): 

  * 2 kilos of fresh sardine 
  * 4 cloves of garlic 
  * 1 onion 
  * 1 bunch of parsley 
  * 1 egg 
  * salt, pepper, cumin, paprika and black pepper 
  * 3 hard eggs (cooked) 



Start by first cleaning the sardines, removing the stop and the tail. 

![S7303852](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309736371.jpg)

pass them to the robot, to have a beautiful mashed sardine. 

![S7303851](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309735971.jpg)

![S7303854](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309737131.jpg)

add the garlic, onion, parsley, salt, and the rest of the condiments 

![S7303855](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309737671.jpg)

.... and parsley 

![S7303859](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309738371.jpg)

do not forget the egg 

![S7303860](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309738711.jpg)

butter well a baking tin 

![S7303864](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309739981.jpg)

put your stuffing in it 

![S7303865](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309740471.jpg)

arrange the hard-boiled eggs 

![S7303866](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309743261.jpg)

and cover the eggs well with the whole sardine mixture, so that you have a pudding 

![S7303867](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309743901.jpg)

put butter on the surface of your pudding or cake 

![S7303868](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309744341.jpg)

and put in the oven until cooked, it is better to put a little butter on the surface, so as not to have a very hard cake. 

![S7303880](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309744711.jpg)

serve warm, and you will be amazed at the taste that it has. 

![S7303883](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/309745271.jpg)

delicious 

bon Appétit. 
