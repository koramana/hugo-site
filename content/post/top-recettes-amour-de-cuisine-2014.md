---
title: Top Cooking Love Recipes 2014
date: '2014-12-31'
categories:
- Coffee love of cooking
tags:
- Algeria
- Morocco
- Chicken
- Tunisia
- Algerian cakes
- Cakes
- Pastry
- inputs

image: http://s2.static-footeo.com/uploads/asl-ajon/news/bonne-annee-2015__nh93gc.jpg
---
![](http://s2.static-footeo.com/uploads/asl-ajon/news/bonne-annee-2015__nh93gc.jpg)

source: http://asl-ajon.footeo.com/ 

Hello everybody, 

Here is a Year that is coming to an end, I only hope that it has left some beautiful traces at home, beautiful memories and good times. For my part I say el hamdou lillah, this year we had a new member in the family, my little angel who was born in me from February. 

There were ups and downs, the event that marked us the most was the death of my mother-in-law during the last days of summer (may God have his soul). And life goes on ... It's the good god's will. 

I hope we can revise with this new year, the year 2015, I hope we can catch up with our mistakes, and make the right choices. Insha'Allah this year will be a good year for all of us, a year full of good experiences and beautiful things. 

I hope that in this new year, we can keep the good liver, act without hurting around us. I hope peace will reign everywhere in this world. Amine (Amen). 

To close this year, I'm giving you a little ranking of the recipes of the year 2014, the recipes that made the "buzz" then we start the parade: 

##  January 2014   
  
<table>  
<tr>  
<td>



###  [ Tunisian cuisine: Tunisian Malsouka-tajine ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks.html> "Permalinks to Tunisian cuisine: Tunisian Malsouka-tajine")

[ 66 reviews ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks.html#comments> "Comment on Tunisian cuisine: Tunisian Malsouka-tajine")

[ ![malsouka-025](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/malsouka-025-150x150.jpg) ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks.html> "Permalinks to Tunisian cuisine: Tunisian Malsouka-tajine") 
</td>  
<td>



###  [ spicy clementine yoghurt ](<https://www.amourdecuisine.fr/article-yaourt-aux-clementines-epicees.html> "Permalinks to yoghurt with spicy clementines")

[ 27 reviews ](<https://www.amourdecuisine.fr/article-yaourt-aux-clementines-epicees.html#comments> "Comment on spicy clementine yoghurt")

[ ![clementine yogurt with spice 005.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/yaourt-a-la-clementine-epicee-005.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/article-yaourt-aux-clementines-epicees.html> "Permalinks to yoghurt with spicy clementines") 
</td>  
<td>



###  [ Tamina ](<https://www.amourdecuisine.fr/article-tamina.html> "Permalinks to Tamina")

[ 45 reviews ](<https://www.amourdecuisine.fr/article-tamina.html#comments> "Comment on Tamina")

[ ![tamina-cake-a-la-semolina-grilled-004-a_2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tamina-gateau-a-la-semoule-grillee-004-a_2-150x150.jpg) ](<https://www.amourdecuisine.fr/article-tamina.html> "Permalinks to Tamina") 
</td> </tr> </table>

For even more recipes this month [ Archive January 2014 ](<https://www.amourdecuisine.fr/article-2014/01>)

##  February 2014   
  
<table>  
<tr>  
<td>



###  [ sweet couscous / seffa / mesfouf ](<https://www.amourdecuisine.fr/article-couscous-sucre-seffa-mesfouf.html> "Permalinks to sweet couscous / seffa / mesfouf")

[ 79 reviews ](<https://www.amourdecuisine.fr/article-couscous-sucre-seffa-mesfouf.html#comments> "Comment on sweet couscous / seffa / mesfouf")

[ ![mesfouf aux raisins, dried-kitchen-Algerian](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mesfouf-aux-raisins-secs-cuisine-algerienne-150x150.jpg) ](<https://www.amourdecuisine.fr/article-couscous-sucre-seffa-mesfouf.html> "Permalinks to sweet couscous / seffa / mesfouf") 
</td>  
<td>



###  [ Bradj semolina lozenges with dates ](<https://www.amourdecuisine.fr/article-bradj-losanges-de-semoule-aux-dattes.html> "Permalinks to Bradj semolina lozenges with dates")

[ 103 reviews ](<https://www.amourdecuisine.fr/article-bradj-losanges-de-semoule-aux-dattes.html#comments> "Comment on Bradj Semolina lozenges with dates")

[ ![bradj-002a_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/bradj-002a_thumb-150x150.jpg) ](<https://www.amourdecuisine.fr/article-bradj-losanges-de-semoule-aux-dattes.html> "Permalinks to Bradj semolina lozenges with dates") 
</td>  
<td>



###  [ Cake with orange ](<https://www.amourdecuisine.fr/article-25345557.html> "Permalinks to Gateau to orange")

[ 62 reviews ](<https://www.amourdecuisine.fr/article-25345557.html#comments> "Comment on Gateau à l'orange")

[ ![Cake-a-l-orange](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/gateau-a-l-orange-150x150.jpg) ](<https://www.amourdecuisine.fr/article-25345557.html> "Permalinks to Gateau to orange") 
</td> </tr> </table>

For even more recipes this month [ Archive February 2014 ](<https://www.amourdecuisine.fr/article-2014/02>)

##  March 2014   
  
<table>  
<tr>  
<td>



###  [ Brownie cheesecake easy and delicious recipe ](<https://www.amourdecuisine.fr/article-brownie-cheesecake-recette-facile-delicieuse.html> "Permalinks to Brownie cheesecake easy and delicious recipe")

[ 30 reviews ](<https://www.amourdecuisine.fr/article-brownie-cheesecake-recette-facile-delicieuse.html#comments> "Comment on Brownie cheesecake recipe easy and delicious")

[ ![Brownie cheesecake easy and delicious recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/brownie-cheesecake-022.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/article-brownie-cheesecake-recette-facile-delicieuse.html> "Permalinks to Brownie cheesecake easy and delicious recipe") 
</td>  
<td>



###  [ strawberry jam: "express, in the microwave" ](<https://www.amourdecuisine.fr/article-confiture-de-fraises-express-au-micro-onde.html> "Permalinks to strawberry jam: "express, microwave"")

[ 54 reviews ](<https://www.amourdecuisine.fr/article-confiture-de-fraises-express-au-micro-onde.html#comments> "Comment on strawberry jam: "express, microwave"")

[ ![jam-de-bur 024_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/confiture-de-fraises-024_thumb-150x150.jpg) ](<https://www.amourdecuisine.fr/article-confiture-de-fraises-express-au-micro-onde.html> "Permalinks to strawberry jam: "express, microwave"") 
</td>  
<td>



###  [ choumicha chicken meatballs gratin ](<https://www.amourdecuisine.fr/article-gratin-de-boulettes-de-poulet-de-choumicha.html> "Permalinks to choumicha chicken meatballs gratin")

[ 49 reviews ](<https://www.amourdecuisine.fr/article-gratin-de-boulettes-de-poulet-de-choumicha.html#comments> "Comment on choumicha chicken dumplings gratin")

[ ![au gratin balls-of-chicken-choumicha_2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-au-boules-de-poulet-choumicha_2-150x150.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-boulettes-de-poulet-de-choumicha.html> "Permalinks to choumicha chicken meatballs gratin") 
</td> </tr> </table>

For even more recipes this month [ March 2014 Archive ](<https://www.amourdecuisine.fr/article-2014/03>)

##  April 2014   
  
<table>  
<tr>  
<td>



###  [ guacamole ](<https://www.amourdecuisine.fr/article-recette-du-guacamole.html> "Permalinks to guacamole")

[ 25 reviews ](<https://www.amourdecuisine.fr/article-recette-du-guacamole.html#comments> "Comment on guacamole")

[ ![guacamole-009.CR2_](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/guacamole-009.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/article-recette-du-guacamole.html> "Permalinks to guacamole") 
</td>  
<td>



###  [ garantita - karantika قرنطيطة ](<https://www.amourdecuisine.fr/article-garantita-karantika.html> "Permalinks to garantita - karantika قرنطيطة")

[ 58 reviews ](<https://www.amourdecuisine.fr/article-garantita-karantika.html#comments> "Comment on garantita - karantika قرنطيطة")

[ ![garantita12](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/garantita12-150x150.jpg) ](<https://www.amourdecuisine.fr/article-garantita-karantika.html> "Permalinks to garantita - karantika قرنطيطة") 
</td>  
<td>



###  [ rice salad / mixed salad ](<https://www.amourdecuisine.fr/article-salade-de-riz-salade-composee.html> "Permalinks to rice salad / mixed salad")

[ 47 reviews ](<https://www.amourdecuisine.fr/article-salade-de-riz-salade-composee.html#comments> "Comment on rice salad / mixed salad")

[ ![salad-in-rice-030](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/salade-au-riz-030-150x150.jpg) ](<https://www.amourdecuisine.fr/article-salade-de-riz-salade-composee.html> "Permalinks to rice salad / mixed salad") 
</td> </tr> </table>

For even more recipes this month [ Archive April 2014 ](<https://www.amourdecuisine.fr/article-2014/04>)

##  May 2014   
  
<table>  
<tr>  
<td>



###  [ Tajine jben Tajine Cheese ](<https://www.amourdecuisine.fr/article-tajine-jben-tajine-de-fromage.html> "Permalinks to Tajine jben Tajine Cheese")

[ 72 reviews ](<https://www.amourdecuisine.fr/article-tajine-jben-tajine-de-fromage.html#comments> "Comment on Tajine jben Tajine Cheese")

[ ![tajine jben or tajine djben](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/tajine-jben-ou-tajine-djben.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/article-tajine-jben-tajine-de-fromage.html> "Permalinks to Tajine jben Tajine Cheese") 
</td>  
<td>



###  [ pasta with smoked salmon easy and fast ](<https://www.amourdecuisine.fr/article-pates-au-saumon-fume-facile-et-rapide.html> "Permalinks to pasta with smoked salmon easy and fast")

[ 20 reviews ](<https://www.amourdecuisine.fr/article-pates-au-saumon-fume-facile-et-rapide.html#comments> "Comment on pasta with smoked salmon easy and fast")

[ ![egg mimosa-and-paste-au-smoke salmon-038.CR2_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/oeuf-mimosa-et-pate-au-saumon-fume-038.CR2_thumb-150x150.jpg) ](<https://www.amourdecuisine.fr/article-pates-au-saumon-fume-facile-et-rapide.html> "Permalinks to pasta with smoked salmon easy and fast") 
</td>  
<td>



###  [ Moroccan Batbout "بطبوط مغربي" ](<https://www.amourdecuisine.fr/article-batbout-marocain.html> "Permalinks to Moroccan Batbout "بطبوط مغربي"")

[ 56 reviews ](<https://www.amourdecuisine.fr/article-batbout-marocain.html#comments> "Commentary on Moroccan Batbout "بطبوط مغربي"")

[ ![Moroccan batbout or Moroccan homemade bread](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/batbout-marocain-pain-maison-marocain.CR2-001-150x150.jpg) ](<https://www.amourdecuisine.fr/article-batbout-marocain.html> "Permalinks to Moroccan Batbout "بطبوط مغربي"") 
</td> </tr> </table>

For even more recipes this month [ Archive May 2014 ](<https://www.amourdecuisine.fr/article-2014/05>)

##  June 2014   
  
<table>  
<tr>  
<td>



###  [ shrimp and potato boureks ](<https://www.amourdecuisine.fr/article-boureks-aux-crevettes-et-pommes-de-terre.html> "Permalinks to boureks with shrimp and potatoes")

[ 58 reviews ](<https://www.amourdecuisine.fr/article-boureks-aux-crevettes-et-pommes-de-terre.html#comments> "Comment on shrimp and potato boureks")

[ ![potato shrimp shrimps](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-aux-crevettes-pommes-de-terre--150x150.jpg) ](<https://www.amourdecuisine.fr/article-boureks-aux-crevettes-et-pommes-de-terre.html> "Permalinks to boureks with shrimp and potatoes") 
</td>  
<td>



###  [ Qalb el louz: kalb elouz cake Heart of almonds قلب اللوز ](<https://www.amourdecuisine.fr/article-qalb-el-louz-kalb-elouz-gateau-coeur-damandes%d9%82%d9%84%d8%a8-%d8%a7%d9%84%d9%84%d9%88%d8%b2.html> "Permalinks to Qalb el louz: kalb elouz cake Heart of almonds قلب اللوز")

[ 95 reviews ](<https://www.amourdecuisine.fr/article-qalb-el-louz-kalb-elouz-gateau-coeur-damandes%d9%82%d9%84%d8%a8-%d8%a7%d9%84%d9%84%d9%88%d8%b2.html#comments> "Comment on Qalb el louz: kalb elouz cake Heart of almonds قلب اللوز")

[ ![qalb el louz.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/qalb-el-louz.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/article-qalb-el-louz-kalb-elouz-gateau-coeur-damandes%d9%82%d9%84%d8%a8-%d8%a7%d9%84%d9%84%d9%88%d8%b2.html> "Permalinks to Qalb el louz: kalb elouz cake Heart of almonds قلب اللوز") 
</td>  
<td>



###  [ chorba Frik ](<https://www.amourdecuisine.fr/article-chorba-frik.html> "Permalinks to chorba frik")

[ 83 reviews ](<https://www.amourdecuisine.fr/article-chorba-frik.html#comments> "Comment on chorba frik")

[ ![chorba-frik-2-006.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-2-006.CR2_1-150x150.jpg) ](<https://www.amourdecuisine.fr/article-chorba-frik.html> "Permalinks to chorba frik") 
</td> </tr> </table>

For even more recipes this month [ June 2014 Archive ](<https://www.amourdecuisine.fr/article-2014/06>)

##  July 2014   
  
<table>  
<tr>  
<td>



###  [ stuffed cabbage with chopped meat ](<https://www.amourdecuisine.fr/article-choux-farcis-a-la-viande-hachee.html> "Permalinks to cabbage stuffed with minced meat")

[ 56 reviews ](<https://www.amourdecuisine.fr/article-choux-farcis-a-la-viande-hachee.html#comments> "Comment on cabbage stuffed with chopped meat")

[ ![cabbage-stuffed-to-four-Houriat el Matbakh-kitchen-algerienn](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/choux-farcis-au-four-houriat-el-matbakh-cuisine-algerienn-150x150.jpg) ](<https://www.amourdecuisine.fr/article-choux-farcis-a-la-viande-hachee.html> "Permalinks to cabbage stuffed with minced meat") 
</td>  
<td>



###  [ Arayeches with honey, ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-gateaux-algeriens.html> "Permalinks to Arayeches with honey, Stars with almonds and honey Algerian cakes")

[ 62 reviews ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-gateaux-algeriens.html#comments> "Comment on Arayeches with honey, Stars with almonds and honey Algerian cakes")

[ ![arayeche au miel-062](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-au-miel-062-150x150.jpg) ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-gateaux-algeriens.html> "Permalinks to Arayeches with honey, Stars with almonds and honey Algerian cakes") 
</td>  
<td>



###  [ sands with milk jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-lait.html> "Permalinks to sands with milk jam")

[ 74 reviews ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-lait.html#comments> "Commentary on sands with milk jam")

[ ![sands-a-la-jam-of-milk-046a_thumb_1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/sables-a-la-confiture-de-lait-046a_thumb_1-150x150.jpg) ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-lait.html> "Permalinks to sands with milk jam") 
</td> </tr> </table>

For even more recipes this month [ July 2014 Archive ](<https://www.amourdecuisine.fr/article-2014/07>)

##  August 2014   
  
<table>  
<tr>  
<td>



###  [ Bavarian apricots almonds ](<https://www.amourdecuisine.fr/article-bavarois-abricots-amandes.html> "Permalinks to Bavarian Apricots Almonds")

[ 30 reviews ](<https://www.amourdecuisine.fr/article-bavarois-abricots-amandes.html#comments> "Comment on Bavarian Apricots Almonds")

[ ![Bavarian-charlotte](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/bavarois-charlotte-150x150.jpg) ](<https://www.amourdecuisine.fr/article-bavarois-abricots-amandes.html> "Permalinks to Bavarian Apricots Almonds") 
</td>  
<td>



###  [ marble cake ](<https://www.amourdecuisine.fr/article-cake-marbre.html> "Permalinks to marble cake")

[ 58 reviews ](<https://www.amourdecuisine.fr/article-cake-marbre.html#comments> "Comment on Marbled Cake")

[ ![marble-cake-1_2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-marbre-1_2-150x150.jpg) ](<https://www.amourdecuisine.fr/article-cake-marbre.html> "Permalinks to marble cake") 
</td>  
<td>



###  [ Meringue with old, very easy recipe ](<https://www.amourdecuisine.fr/article-meringue-a-l-ancienne-recette-tres-facile-114688303.html> "Permalinks to old-fashioned Meringue, easy recipe")

[ 42 reviews ](<https://www.amourdecuisine.fr/article-meringue-a-l-ancienne-recette-tres-facile-114688303.html#comments> "Comment on Meringue à l'ancienne, very easy recipe")

[ ![meringue-018.CR2_2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-018.CR2_2-150x150.jpg) ](<https://www.amourdecuisine.fr/article-meringue-a-l-ancienne-recette-tres-facile-114688303.html> "Permalinks to old-fashioned Meringue, easy recipe") 
</td> </tr> </table>

For even more recipes this month [ Archive August 2014 ](<https://www.amourdecuisine.fr/article-2014/08>)

##  September 2014   
  
<table>  
<tr>  
<td>



###  [ head of sheep with cumin, baked bouzellouf ](<https://www.amourdecuisine.fr/article-tete-de-mouton-au-cumin-au-four.html> "Permalinks to Cumin Sheep Head, Baked Bouzellouf")

[ 53 reviews ](<https://www.amourdecuisine.fr/article-tete-de-mouton-au-cumin-au-four.html#comments> "Comment on sheep's head with cumin, baked bouzellouf")

[ ![sheepshead head-to-cumin-to-four-041_2](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tete-de-mouton-au-cumin-au-four-041_2-150x150.jpg) ](<https://www.amourdecuisine.fr/article-tete-de-mouton-au-cumin-au-four.html> "Permalinks to Cumin Sheep Head, Baked Bouzellouf") 
</td>  
<td>



###  [ kebda mchermla ](<https://www.amourdecuisine.fr/article-recette-kebda-mchermla.html> "Permalinks to kebda mchermla")

[ 30 reviews ](<https://www.amourdecuisine.fr/article-recette-kebda-mchermla.html#comments> "Comment on kebda mchermla")

[ ![kebda-mchermla](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/kebda-mchermla-150x150.jpg) ](<https://www.amourdecuisine.fr/article-recette-kebda-mchermla.html> "Permalinks to kebda mchermla") 
</td>  
<td>



###  [ tuna slippers ](<https://www.amourdecuisine.fr/article-chaussons-au-thon.html> "Permalinks to tuna slippers")

[ 46 reviews ](<https://www.amourdecuisine.fr/article-chaussons-au-thon.html#comments> "Comment on tuna slippers")

[ ![Floor-to-the-stove-to-tuna-016.CR2_2](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chaussons-a-la-poele-au-thon-016.CR2_2-150x150.jpg) ](<https://www.amourdecuisine.fr/article-chaussons-au-thon.html> "Permalinks to tuna slippers") 
</td> </tr> </table>

For even more recipes this month [ Archive September 2014 ](<https://www.amourdecuisine.fr/article-2014/09>)

##  October 2014   
  
<table>  
<tr>  
<td>



###  [ batbouts stuffed with tuna ](<https://www.amourdecuisine.fr/article-batbouts-farcis-au-thon.html> "Permalinks to batbouts stuffed with tuna")

[ 21 reviews ](<https://www.amourdecuisine.fr/article-batbouts-farcis-au-thon.html#comments> "Comment on batbouts stuffed with tuna")

[ ![batbout stuffed with tuna](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/batbouts-farcis-au-thon-recettes-de-ramadan-2014.CR2_-150x150.jpg) ](<https://www.amourdecuisine.fr/article-batbouts-farcis-au-thon.html> "Permalinks to batbouts stuffed with tuna") 
</td>  
<td>



###  [ tajine warka, or Tunisian tagine in flaky pastry ](<https://www.amourdecuisine.fr/article-tajine-warka-ou-tajine-tunisien-en-pate-feuilletee.html> "Permalinks to tajine warka, or Tunisian tagine in flaky pastry")

[ 29 reviews ](<https://www.amourdecuisine.fr/article-tajine-warka-ou-tajine-tunisien-en-pate-feuilletee.html#comments> "Commentary on tajine warka, or Tunisian tagine in flaky pastry")

[ ![tajine warka, or Tunisian tagine in flaky pastry](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tajine-tunisien-en-croute-001-150x106.jpg) ](<https://www.amourdecuisine.fr/article-tajine-warka-ou-tajine-tunisien-en-pate-feuilletee.html> "Permalinks to tajine warka, or Tunisian tagine in flaky pastry") 
</td>  
<td>



###  [ Chicken nuggets ](<https://www.amourdecuisine.fr/article-croquettes-de-poulet.html> "Permalinks to Chicken Croquettes")

[ 77 reviews ](<https://www.amourdecuisine.fr/article-croquettes-de-poulet.html#comments> "Comment on Chicken Croquettes")

[ ![croquettes chicken-kitchen-algerienne_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/croquettes-de-poulet-cuisine-algerienne_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/article-croquettes-de-poulet.html> "Permalinks to Chicken Croquettes") 
</td> </tr> </table>

For even more recipes this month [ Archive October 2014 ](<https://www.amourdecuisine.fr/article-2014/10>)

##  November 2014   
  
<table>  
<tr>  
<td>



###  [ bolognese tortillas gratin ](<https://www.amourdecuisine.fr/article-gratin-de-tortillas-a-la-bolognaise.html> "Permalinks to bolognese tortillas gratin")

[ 21 reviews ](<https://www.amourdecuisine.fr/article-gratin-de-tortillas-a-la-bolognaise.html#comments> "Comment on bolognese tortillas gratin")

[ ![recipe gratin with bolognese tortillas](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/gratin-de-tortillas-a-la-bolognase-010-150x100.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-tortillas-a-la-bolognaise.html> "Permalinks to bolognese tortillas gratin") 
</td>  
<td>



###  [ royal charlotte technical event best pastry chef ](<https://www.amourdecuisine.fr/article-charlotte-royale-epreuve-technique-meilleur-patissier.html> "Permalinks to charlotte royale technical event best pastry chef")

[ 46 reviews ](<https://www.amourdecuisine.fr/article-charlotte-royale-epreuve-technique-meilleur-patissier.html#comments> "Comment on royal charlotte technical event best pastry chef")

[ ![royal charlotte recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/charlotte-royale-023-150x100.jpg) ](<https://www.amourdecuisine.fr/article-charlotte-royale-epreuve-technique-meilleur-patissier.html> "Permalinks to charlotte royale technical event best pastry chef") 
</td>  
<td>



###  [ Coffee lightning ](<https://www.amourdecuisine.fr/article-les-eclairs-au-cafe.html> "Permalinks to Coffee Lightning")

[ 67 reviews ](<https://www.amourdecuisine.fr/article-les-eclairs-au-cafe.html#comments> "Comment on Coffee Lightning")

[ ![eclairs_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/eclairs_thumb1-150x150.jpg) ](<https://www.amourdecuisine.fr/article-les-eclairs-au-cafe.html> "Permalinks to Coffee Lightning") 
</td> </tr> </table>

For even more recipes this month [ Archive November 2014 ](<https://www.amourdecuisine.fr/article-2014/11>)

##  December 2014   
  
<table>  
<tr>  
<td>



###  [ homemade hamburger bread ](<https://www.amourdecuisine.fr/article-pain-hamburger-maison.html> "Permalinks to homemade hamburger bread")

[ 40 reviews ](<https://www.amourdecuisine.fr/article-pain-hamburger-maison.html#comments> "Comment on homemade hamburger bread")

[ ![Bread-for-hamburgers-buns-tres-lightweight-013_2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/pain-pour-hamburgers-buns-tres-legers-013_2-150x150.jpg) ](<https://www.amourdecuisine.fr/article-pain-hamburger-maison.html> "Permalinks to homemade hamburger bread") 
</td>  
<td>



###  [ Kabyle galette stuffed with chakchouka ](<https://www.amourdecuisine.fr/article-galette-kabyle-farcie-la-chakchouka.html> "Permalinks to kabyle galette stuffed with chakchouka")

[ 63 reviews ](<https://www.amourdecuisine.fr/article-galette-kabyle-farcie-la-chakchouka.html#comments> "Comment on Kabyle galette stuffed with chakchouka")

[ ![Kabyle galette stuffed with chakchouka](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/galette-kabyle-farcie-a-la-chakchouka-1-150x102.jpg) ](<https://www.amourdecuisine.fr/article-galette-kabyle-farcie-la-chakchouka.html> "Permalinks to kabyle galette stuffed with chakchouka") 
</td>  
<td>



###  [ avocado salad with tuna ](<https://www.amourdecuisine.fr/article-salade-d-avocat-au-thon.html> "Permalinks to avocado salad with tuna")

[ 53 reviews ](<https://www.amourdecuisine.fr/article-salade-d-avocat-au-thon.html#comments> "Comment on tuna avocado salad")

[ ![salad-and-Advocate 020_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-d-avocat-020_thumb-150x150.jpg) ](<https://www.amourdecuisine.fr/article-salade-d-avocat-au-thon.html> "Permalinks to avocado salad with tuna") 
</td> </tr> </table>

For even more recipes this month [ Archive December 2014 ](<https://www.amourdecuisine.fr/article-2014/12>)

Here for my blog and my recipes. I am very happy that you liked my blog, that you also like my channel youtube: 

I hope Allah insists that you continue to follow me and be faithful to my blog. 

Merci. 
