---
title: bites with queen cod, goat and spinach
date: '2012-05-14'
categories:
- Algerian cuisine
- diverse cuisine
- chicken meat recipes (halal)
- fish and seafood recipes
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/panique-en-cuisine.bmp_thumb1.jpg
---
bites to the queen cod / goat / spinach hello everyone, again a recipe that participates in the contest "cuisine Express", and this is the second recipe of Samia orm Sirine, the first was: Asparagus fish ... so if you too you have quick recipes, you can participate in my little contest, to win a beautiful Kenwood blinder. Ingredients for 8 mouthfuls: - 550g spinach with cream (frozen preparation) - an onion - (optional grated carrot) - a cod pavé (frozen here) - a few slices of goat cheese - salt, pepper & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0  **bites with queen cod / goat / spinach** Hello everybody,  still a recipe that participates in [ "Express kitchen" contest ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>) , and this is the second recipe of Samia oum Sirine, the first was: [ Asparagus with fish ](<https://www.amourdecuisine.fr/article-asperges-au-poisson-104897914.html>) ...  so if you too have fast recipes, you can participate in my little contest, to win a beautiful Kenwood blinder.  ingredients for 8 bites around:  \- 550g spinach with cream (frozen preparation)  \- an onion  \- (optional grated carrot)  \- a pad of cod (frozen here)  \- a few slices of goat cheese  \- salt pepper  \- bites to the queen (bought all ready on the market)  \- one to two tablespoons of fresh cream  \- a little grated gruyere  1) slice the onion and put it in the pan with a little olive oil,  2) Add spinach on a low heat and let the spinach defrost  3) grated carrot and add to spinach onion, pepper and salt  4) add the cod and cook, stirring occasionally  5) Once everything is cooked, add the grated gruyere (a handful) and remove from the heat to allow the mixture to cool slightly.  6) cut slices of goat cheese and stuff the bites like this:  a spoon of spinach preparation and a slice of goat cheese and a spinach preparation spoon  7) Bake for a few minutes in a hot oven and serve with a salad  Enjoy your meal !  Oum Sirine and Sayaline 

this recipe participates in 

#####  [ contest ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>)

[ ![panic in kitchen.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/panique-en-cuisine.bmp_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>)

to win this gift: 

#####  ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/Kenwood-FP-196-01.jpg)
