---
title: Flan with coconut milk
date: '2011-02-09'
categories:
- amuse bouche, tapas, mise en bouche
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/02/flan-013_thumb.jpg
---
##  Flan with coconut milk 

Hello everyone, 

Always with the recipes I made when I was in Algeria, and I did not even have time to post during the busy month of Ramadan then my return to England and then try to regain his normal life routine after a long absence. 

I mail you this beautiful flan with coconut milk, which has not even lasted long, hence the few photos I share today. This flan is real happiness, too good and too tasty to perfection, perfect!   


**Flan with coconut milk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/flan-013_thumb.jpg)

**Ingredients**

  * a box of coconut milk (400 ml) 
  * 250 ml of milk 
  * 80 grs of sugar or according to taste (I remember to put 6 ca soups my beautiful daughter tasted each time the mixture, until you had the good taste not too sweet, so better than you test the taste, before l adding eggs of course, hihihihih) 
  * 4 eggs 
  * caramel topping (in Algeria I had Nouara flan I believe in box, I used the caramel bag that is in the box) 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. In a salad bowl, put the eggs, milk, sugar and coconut milk 
  3. Mix everything well. 
  4. Pour into a cake mold, a little caramel topping and pour the preparation of the custard. 
  5. Cook in a bain-marie for 30 to 45 minutes (or depending on your oven) 
  6. Let the flan cool in its mold 
  7. place in the fridge until serving. 
  8. to unmold it, pass the blade of a knife on the sides. 


