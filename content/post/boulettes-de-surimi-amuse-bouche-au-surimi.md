---
title: Surimi meatballs / appetizer with surimi
date: '2013-09-01'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/boulettes-au-surimi_thumb1.jpg
---
Hello everybody, 

Here is a very simple entry to make for fans of surimi like me, who is always at home, and who always wants to present it differently, each time on my table. 

A recipe very simple, pif, and too good.   


**Surimi meatballs / appetizer with surimi**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/boulettes-au-surimi_thumb1.jpg)

Recipe type:  Appetizer, appetizer  portions:  8  Prep time:  20 mins  cooking:  5 mins  total:  25 mins 

**Ingredients**

  * 10 crumbled surimi sticks 
  * 1 hard egg 
  * 3 portions of cheese the laughing cow. 
  * 1 tablespoon of lemon juice 
  * a few sprigs of parsley 
  * 1 pinch of salt 
  * 1 pinch of cumin 



**Realization steps**

  1. crush the cheese with a fork 
  2. crush on the hard egg 
  3. add the surimi crumbs 
  4. season with the juice, chopped parsley, salt and cumin. 
  5. mix to have a paste 
  6. shape dumplings. 
  7. book fresh for 1 hour before presenting with a fresh salad. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
