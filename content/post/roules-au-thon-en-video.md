---
title: tuna rolls video
date: '2013-07-23'
categories:
- dessert, crumbles and bars
- Algerian cakes without cooking
- cakes and cakes
- sweet recipes

---
hello everyone, looking for a simple but good recipe for a recipe that you can adopt for ramadan recipes, here is one, these tuna rolls, are a delight, melting and mellow, in addition very tasty with tuna stuffing, try them, anyone who has to try this recipe really like it. the dough I pricked at our dear kaouther, the stuffing, I did with what I had on hand, anyway, these rolls are passed in the blink of an eye. For the dough - 3 cups flour - 3 tablespoons milk powder - 1 teaspoon & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.73  (  2  ratings)  0 

Hello everybody, 

looking for a simple entry recipe  but good,  a delight that you can adopt for  [ ramadan recipes ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>) ,  here is one, these  wraps  tuna, are a  delight  , melting and mellow, in addition  very  appetizing  with the tuna stuffing, try them, anyone who has to try this recipe really like it. 

**_For the dough  
\- 3 cups flour   
\- 3 tablespoons milk powder   
\- 1 teaspoon of salt   
\- 1 tablespoon of sugar   
\- 1 tablespoon baker's yeast   
\- 1 teaspoon baking powder   
\- 4 to 5 tablespoons of oil _ **

Mix all these ingredients and add enough lukewarm water until soft, soft and easy to work.    
Knead a few minutes. Form the dough into a ball and let it rise under cover, protected from drafts until it has doubled in volume. 

for the farce 

  * 1 onion 
  * 2 fresh tomatoes 
  * 1 can of tuna 
  * 1 handful of pitted olives 
  * salt, black pepper. 



open the can of tuna, put its oil in a pan. 

cut the onions, and return them in the oil, add after the cut tomato, and season according to your taste. 

let cool. 

now that the dough has doubled in volume, degas it, and spread it into a large rectangle, decorate the onion stuffing, sprinkle over the tuna in pieces, and the olives cut into small slices. 

roll the dough on itself, do not forget to brush the last side of the dough with a little egg so that it does not open. 

cut pieces of 2 cm wide, it's a bit difficult, because the stuffing may come out, but you can arrange it 

place your rolls on an oiled plate, and let rise a little. 

before placing in the oven, brush the rolls with a little egg yolk. 

cook in a hot oven. 

and enjoy the delight 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

merci pour vos commentaire, et n’oubliez pas de vous abonner a ma newsletter. 
