---
title: unctuous chocolate cream with chocolate
date: '2017-05-03'
categories:
- Algerian cuisine
- Cuisine by country
- dessert, crumbles et barres
- recipes of feculents
- sweet recipes
- rice
- sweet verrines
tags:
- Ramadan 2017
- Ramadan
- Algeria
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/creme-au-riz-onctueuse-au-chocolat-1.jpg
---
![cream with creamy chocolate rice 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/creme-au-riz-onctueuse-au-chocolat-1.jpg)

##  unctuous chocolate cream with chocolate 

Hello everybody, 

This unctuous cream with chocolate rice is a little revisit with a delicate dessert of the Algerian cuisine: the [ mhalbi ](<https://www.amourdecuisine.fr/article-mhalbi-constantinois-creme-dessert-au-riz-en-video.html>) perfumed with orange blossom water distilled at home, as my late grandmother had done. 

![](http://passioncuisinedejulie.fr/wp-content/uploads/2017/04/sac-riz.jpg)

For my participation I preferred a dessert to rice, I had thought of rice in bed, but I had already full on the blog, as the [ strawberry rice pudding ](<https://www.amourdecuisine.fr/article-riz-lait-aux-fraises.html>) , or even the recipe [ pancakes stuffed with rice pudding ](<https://www.amourdecuisine.fr/article-crepes-farcies-au-riz-au-lait.html>) . And as I love chocolate, I opted for this recipe cream creamy chocolate cream, a recipe rich in taste, which can of course lighten up with fewer calories. But to be honest, for me a cream like this is just ideal during Ramadan. 

{{< youtube tvLXMajifDU >}} 

You can also see the list of all the previous godmothers in our game: 

**unctuous chocolate cream with chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/creme-au-riz-onctueuse-au-chocolat.jpg)

portions:  4 

**Ingredients**

  * 500 ml whole milk 
  * 50 ml of liquid cream 
  * 50 gr of chopped dark chocolate 
  * 100 gr of powdered sugar 
  * 1 C. powdered rice 
  * 2 tbsp. cornflour 
  * 2 tbsp. cocoa powder 
  * ½ c. coffee vanilla extract 



**Realization steps**

  1. do not start mixing together the sugar, rice powder, cornflour, and sifted cocoa powder. 
  2. add the milk in a small amount while stirring to prevent the formation of lumps 
  3. cook over medium heat, stirring constantly until cream becomes smooth 
  4. at this stage we introduce the dark chocolate, stirring 
  5. when it is well melted, add the liquid cream, then the vanilla extract 
  6. stir a little and remove from the heat. 
  7. Fill the verrines with this cream, let cool before putting in the fridge. 



![cream with creamy chocolate rice 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/creme-au-riz-onctueuse-au-chocolat-2.jpg)

et voila la liste des participantes à cette ronde, faites un tour pour voir leurs délices: 
