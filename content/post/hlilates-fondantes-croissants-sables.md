---
title: Melting hlilates, shortbread croissants
date: '2015-07-16'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- sweet recipes
tags:
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/croissants-sabl%C3%A9s-hlilates.jpg
---
[ ![croissants shortbread, hlilates](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/croissants-sabl%C3%A9s-hlilates.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/croissants-sabl%C3%A9s-hlilates.jpg>)

##  Melting hlilates, shortbread croissants 

Hello everybody, 

I always find it difficult to answer a certain question in the comments, when my readers ask me: can we replace the smen (ghee) with butter. I confess that for me, it's a trap question, because perso it happens to me to do this little replacement, but with that I change my method of working the cake. 

It should be known that ghee, clarified butter or smen, as it is called, is pure fat, it is easy to have smen from butter, by boiling the butter, skim the foam that is on the surface, and after cooling, subtract that fat ... suddenly, the butter contains liquid, and put one in the place of the other especially in the realization of a cake may be a risky party, because it can influence the final texture of the cake. 

However, here is one of my readers, who has changed my recipe for [ Qmirates ](<https://www.amourdecuisine.fr/article-qmirates-petites-lunes-aux-cacahuetes.html>) , which is a cake that contains only smen originally, she added butter, but she made some modifications to the recipe and the result is impeccable. 

This is the recipe of my friend and faithful reader of the blog, for a few years now, I am very happy to count it among my good friends » **Flower Dz** ", And I thank her for the word she left me on my email, and she asked me to publish on my blog: 

__ Hear me as a reader of the talented cooking lover I really like to make her recipes because they are always intact and correct to the letter. Among the beautiful recipes Soulef had the Qmirattes this beautiful recipe in the mouth to the taste of grilled peanuts attracted me a lot, I wanted to realize it especially as the holiday of Aid approaches, the opportunity for me forever hihihih. 

Soulef's recipe is based on Smen, and given that my smen is no longer tolerated by my children, I modified the ingredients of the recipe to meet the taste and preferences of my family. I hesitated to tell Soulef the changes made on her recipe, but she understood it all by herself after seeing the pictures on her cooking group and I was surprised by her reaction by saying congratulations my dear it's a good idea (yatik el saha) which confirms that this lady is really a chef with a big heart. 

thank you my **Flower Dz** for this word, and I want to tell you, that we learn every day, and I learned a lot and lots of things with my friends. 

**Melting hlilates, shortbread croissants**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/Hlilates-fondantes-croissants-sabl%C3%A9s.jpg)

portions:  40  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients**

  * 150 g butter at room temperature. 
  * 150 g of good quality Smen at room temperature. 
  * 300 g iced sugar. 
  * 100 g grilled sesame (and ground for me) 
  * 200 g roasted peanuts. 
  * A separate egg. 
  * flour as needed 
  * Nuts for decoration 



**Realization steps**

  1. In a large bowl mix well both fat (butter and smen), use an electric mixer is better. 
  2. Add the iced sugar and mix well you must have a good sparkling mixture because it is the trick of success for Qmirattes. 
  3. Now add the egg yolk and mix again. 
  4. In hand add the sesame and peanuts and pick up with the flour. 
  5. Using a roller, flatten the dough to a thickness of 1 cm. 
  6. Use a glass to cut moons, and put them on a baking tray. 
  7. Brush the surface of your cakes with the egg white and decorate with nuts. 
  8. baked in a preheated oven at 180 degrees for about 10 minutes. 
  9. Let cool in their trays. 
  10. NB: it is important to cool the pieces of Qmirattes in their trays because they crumble easily, 
  11. Also it is important to watch the cooking because the cakes can easily burn. 



[ ![melting hlilates, shortbread croissants](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/hlilates-fondantes-croissants-sabl%C3%A9s.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/hlilates-fondantes-croissants-sabl%C3%A9s.jpg>)
