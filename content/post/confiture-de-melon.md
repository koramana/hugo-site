---
title: melon jam
date: '2014-09-12'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/confiture-de-melon-2.CR2_thumb1.jpg
---
##  melon jam 

Hello everybody, 

It happens sometimes that we come across a melon not too sweet, like the case of these three melons that my husband has to buy, and that no one could eat .... 

Solution…. in the kitchen, we must never throw ... ok! So, for the first melon, a jam was the ideal solution .... and it is a succulent jam, delicious and well perfumed that I finally got .... 

For other melons ...... recipes to come, as I arrange to find time and organize myself .... 

**melon jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/confiture-de-melon-2.CR2_thumb1.jpg)

portions:  8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 1 melon (without pepin, and without skin): 500 grs in my recipe. 
  * sugar: 60% of the weight of the melon, 300 grs in my recipe 
  * the zest and juice of a lemon 
  * 2 tablespoons of ginger confit, otherwise 2 cm fresh ginger. 



**Realization steps**

  1. In a large bowl, mix the melon that you have peeled, and seeded, the lemon juice, the lemon zest, the ginger and the sugar. 
  2. Mix well. Let them macerate in the refrigerator for 12 hours. 
  3. drain the pulp and put it in a small bowl. 
  4. Put the juice and sugar mixture in a heavy bottom pan (or heavy saucepan of stainless steel or copper) and bring slowly to a boil. 
  5. Then reduce to low heat and simmer for 20 minutes. 
  6. Then add the melon pieces and cook another 30 minutes, or until the melon pieces become translucent and the liquid thickens. 
  7. A good way to check if the jam is ready: with a spoon pour a little jam syrup on a cold plate, if the drop freezes on cooling, the jam is ready. 



I hope that the recipe will tempt you, and that you will enjoy your melon but in another way. 

and if you like, you can also see: 

![figs jam-melon-004.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/confiture-figues-melon-004.CR2_.jpg) [ Confiture figues et melon ](<https://www.amourdecuisine.fr/article-confiture-de-figues-et-melon-119628571.html>)
