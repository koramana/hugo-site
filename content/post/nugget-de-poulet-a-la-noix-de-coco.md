---
title: Chicken nugget with coconut
date: '2017-07-26'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- idea, party recipe, aperitif aperitif
- Dishes and salty recipes
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/chicken-nugget-nuggets-de-poulet-a-la-noix-de-coco.CR2_.jpg
---
[ ![Chicken nugget with coconut](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/chicken-nugget-nuggets-de-poulet-a-la-noix-de-coco.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/chicken-nugget-nuggets-de-poulet-a-la-noix-de-coco.CR2_.jpg>)

##  Chicken nugget with coconut 

Hello everybody, 

Here is a recipe very easy and very delicious chicken nuggets that I like to realize, when I do not know what to eat, and I never noticed that I did not have the recipe on the blog. 

In any case, today I share with you the recipe for chicken nuggets with coconut, and next time I will make the original and simple version ...   


**Chicken nugget with coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/chicken-nuggets-nuggets-de-poulet.CR2_.jpg)

Recipe type:  appetizer, aperitif, entree  portions:  4  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients** for the marinade: 

  * 1 chicken breast 
  * 1 tablespoon yoghurt 
  * black pepper and salt 
  * 1 teaspoon of tomato puree 

for gilding: 
  * 1 egg 
  * 1 little flour 
  * 1 glass of bread crumbs 
  * ½ glass of unsweetened coconut. 
  * oil for frying 



**Realization steps**

  1. in a salad bowl, mix yoghurt, salt, black pepper and tomato. 
  2. clean the chicken breast and cut into medium cube. 
  3. place it in the marinade already prepared and leave at least an hour. 
  4. heat the oil well. 
  5. whisk the egg a little with a fork. 
  6. dip the chicken cubes into the flour, then into the egg, then into the coconut / breadcrumb mixture. 
  7. cook them in the hot oil, which is on medium heat   
the fire must not be high to allow the chicken pieces to cook inside. 
  8. serve with a [ hot yoghurt sauce ](<https://www.amourdecuisine.fr/article-sauce-piquante-au-yaourt.html> "Hot sauce with yoghurt") It's going to be a delight. 



[ ![chicken nuggets.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/nuggets-de-poulet.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/nuggets-de-poulet.CR2_.jpg>)
