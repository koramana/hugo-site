---
title: basquaise chicken, easy recipe
date: '2016-11-30'
categories:
- recettes a la viande de poulet ( halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/poulet-basquaise-037.CR2_thumb1.jpg
---
![basquaise chicken 037.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/poulet-basquaise-037.CR2_thumb1.jpg)

##  basquaise chicken, easy recipe 

Hello everybody, 

here is a super rich dish, and too good, that is the basquaise chicken. A dish from the Basque country, where the chicken is cooked in a sauce called "piperade" containing tomatoes, peppers, onions, garlic and Bayonne ham .... 

there are several versions of this dish, and I chose, the easiest recipe, and the fastest too, and it was super delicious. 

and I tell you one thing, while preparing the recipe, I realized, that it was exactly the same as the [ chekchouka ](<https://www.amourdecuisine.fr/article-tchektchouka-cuisine-algerienne-108479411.html>) with chicken peppers, because as I told you, I chose the simplest recipe.   


**basquaise chicken, easy recipe**

![](http://amour-de-cuisine.com/wp-content/uploads/2013/03/poulet-basquaise-035.CR2_thumb.jpg)

Recipe type:  dish, poultry  portions:  2  Prep time:  10 mins  cooking:  25 mins  total:  35 mins 

**Ingredients**

  * 2 full legs of chicken 
  * 2 onions 
  * 4 tomatoes (in box for me) 
  * 2 red peppers 
  * 3 tablespoons olive oil 
  * 3 cloves of garlic 
  * Salt pepper 



**Realization steps**

  1. heat the oil in a skillet, or cooking pot. 
  2. Brown and brown each side of the chicken legs. 
  3. Slice the onions finely, add on top, cook, stirring a few times, until the onion is translucent. 
  4. Cut the red peppers, crush the garlic and pour into the pan, add salt and black pepper. 
  5. then add the tomatoes, cover and let cook. 
  6. if necessary, add a little water and let everything cook on low heat for half an hour. 
  7. Serve with good rice, and enjoy this delight. 



{{< youtube sthG >}} 
