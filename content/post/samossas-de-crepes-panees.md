---
title: samosas of breaded pancakes
date: '2016-12-01'
categories:
- Bourek, brick, samoussa, chaussons

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/samossas-de-crepes-panees-1_thumb.jpg
---
[ ![samosas from crepes panees 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/samossas-de-crepes-panees-1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/samossas-de-crepes-panees-1_2.jpg>)

##  samosas of breaded pancakes 

Hello everybody, Many of you have been asking me for salty pancake recipes, and here is another recipe "Samosas of breaded pancakes" (I still have enough on the blog) but I would not say no to add more. 

a recipe prepared by the care of my friend Lunetoiles, and that I frankly like to achieve, but as I told you with the outings without stopping that I do to my sister-in-law, I do not tell you how I come home punctured, and I run quickly to the kitchen just to make a "little boob" that calle well belly, hihihihih 

**samosas of breaded pancakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/samossas-de-crepes-panees_thumb.jpg)

portions:  16  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 8 [ pancakes ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee.html>) (cut in half) 

The joke : 
  * 2 bouquets of boiled spinach well drained 
  * 1 chopped onion 
  * 50 gr of mushrooms 
  * 50 gr of melted cheese 
  * 50 gr of Gruyère 
  * 1 teaspoon of butter 
  * Salt and black pepper 
  * 1 bowl of bechamel sauce 

Frying 
  * breadcrumbs 
  * 2 eggs 
  * Oil 
  * 1 teaspoon of butter 



**Realization steps** Prepare the stuffing: brown onion and butter. 

  1. Add the mushrooms and let them come back. 
  2. Add the spinach, the salt and the black pepper, bring back the whole thing until total evaporation of the water. 
  3. Add the melted cheese, grated cheese and bechamel sauce, mix well. 

Train the samossas: 
  1. Put on each crepe a little stuffing and shape the samosas. 
  2. Continue the operation until exhaustion. 
  3. Dip the pancake samosas in the beaten eggs. 
  4. Sauté the samosas with stuffed pancakes in bread crumbs. 
  5. Fry both sides in oil and butter.   
Watch the video : 



[ ![samosas from crepes panees 3](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/samossas-de-crepes-panees-3_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/samossas-de-crepes-panees-3_21.jpg>)
