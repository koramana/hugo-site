---
title: Savarin has orange without rum
date: '2016-12-08'
categories:
- dessert, crumbles and bars
- gateaux, et cakes
- ramadan recipe
- sweet recipes
tags:
- Cakes
- To taste
- desserts
- Pastry
- Soft
- la France
- Baba

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Savarin-a-lorange-sans-rhum-016.CR2_.jpg
---
[ ![savarin with orange without rum](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Savarin-a-lorange-sans-rhum-016.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-savarin-lorange-sans-rhum.html/savarin-a-lorange-sans-rhum-016-cr2>)

##  Savarin has orange without rum 

Hello everybody, 

Savarin has orange without rum please! A favorite recipe at home, after the [ mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouscoutchou.html> "mouskoutchou / mouscoutchou") , the [ banana cake ](<https://www.amourdecuisine.fr/article-cake-a-la-banane-ultra-moelleux.html> "ultra moist banana cake") , and the [ Sachertorte ](<https://www.amourdecuisine.fr/article-sachertorte-recette-gateau-autrichien-au-chocolat.html> "Sachertorte recipe, Austrian chocolate cake") . We love this little delight, which looks a lot like the baba, which is preparing like a [ Brioche ](<https://www.amourdecuisine.fr/article-galette-des-rois-briochee.html> "cake brioche") , and that melts in the mouth. 

My husband is constantly asking for it, and the day I make the Savarin orange rum-less is the longest day for him, hihihiih ... Because me and the bakery is taking all the time possible, like a friend who we have not seen for a long time, we find it and we take all our time to talk about everything and nothing calmly. 

Yes that's the realization of buns, savarins and all that requires a lifting time. As for this savarin, I started at 15h, and I did the cooking at 20h. Yes my friends, do not be surprised, you can be faster than that, especially in summer, but in winter, and let the savarin grow by itself, it's almost the minimum it can take you. 

In any case, this wait is always crowned by a bluffing result, a brioche or savarin with a blameless and airy crumb. You can judge it for yourself, hihihiih. My [ first Savarin ](<https://www.amourdecuisine.fr/article-26438036.html> "A savarin at the Nawel with news of her") was not bad at all, it goes back to the beginning of my blog, but over time, I re-adapted the recipe for my taste, for an even better result. 

[ ![savarin with orange without rum](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Savarin-a-lorange-sans-rhum-003.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-savarin-lorange-sans-rhum.html/savarin-a-lorange-sans-rhum-003-cr2>)   


**Savarin has orange without rum**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Savarin-a-lorange-sans-rhum-009.jpg)

portions:  12  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * 250 gr of flour 
  * 3 eggs 
  * 1 tablespoon instant yeast 
  * 70 gr of melted butter cooled 
  * 30 gr of sugar 
  * 30 ml of milk 
  * 1 generous pinch of salt 

Syrup has orange: 
  * 200 ml orange juice (freshly squeezed oranges) 
  * 250 ml of water 
  * 300 gr of sugar (I put 225 gr of sugar because the juice of my oranges was a little sweet) 
  * 3 tablespoons of orange blossom water 



**Realization steps** for this recipe I use the petrin because at the beginning the dough is really too sticky, you can use a wooden spoon and mix, it goes well you're tired, but it's much better than doing it by hand. 

  1. place the flour, salt and sugar in the tank of a robot. Mix 
  2. now add instant yeast. 
  3. pour over the warmed milk and eggs beaten in omellette. 
  4. Whisk for at least 15 minutes, until the dough forms a good elastic band.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Savarin-petrissage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Savarin-petrissage.jpg>)
  5. Remove the cuvette from the robot, cover the dough with a food thread and let it rise at room temperature until the dough doubles in volume. 
  6. put the bowl back in the food processor, add the lukewarm butter and chill in a small amount while mixing until it is well inserted. 
  7. directly put the dough in a savarin dish (I do not have one) buttered 
  8. arrange the dough well so that it is well distributed in the mold and inflates in a balanced way.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/savarin-levee.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/savarin-levee.jpg>)
  9. Let it swell until it doubles in volume. 
  10. Preheat the oven to 180 ° C and cook savarin for almost 20 minutes or according to your oven, it cooks quickly 

Prepare the syrup in the meantime: 
  1. put the sugar the orange juice, and the water in a saucepan and bring to a boil. 
  2. when it starts to boil count 10 minutes and remove from the fire 
  3. When the savarin is cooked, turn out carefully.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/savarin-cuisson.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/savarin-cuisson.jpg>)
  4. Sprinkle with the syrup gently, by a little, until it absorbs, and water again. 
  5. Present with fruit, or just a little whipped cream. 
  6. Whisk to whip. 
  7. Put savarins in small papers (muffin papers do the trick) then fill the hole with whipped cream. 
  8. It only remains to give them a very 80's look with a half candied cherry! 



[ ![savarin with orange without rum](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/Savarin-a-lorange-sans-rhum-018.jpg) ](<https://www.amourdecuisine.fr/article-savarin-lorange-sans-rhum.html/savarin-a-lorange-sans-rhum-018>)
