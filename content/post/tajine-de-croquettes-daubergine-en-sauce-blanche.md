---
title: tajine eggplant croquettes in white sauce
date: '2017-04-30'
categories:
- cuisine algerienne
- recette de ramadan
- recettes a la viande de poulet ( halal)
tags:
- Healthy cuisine
- Easy cooking
- Full Dish
- Ramadan 2015

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-croquettes-daubergine-en-sauce-blanche-1.jpg
---
[ ![tajine eggplant croquettes in white sauce 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-croquettes-daubergine-en-sauce-blanche-1.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-croquettes-daubergine-en-sauce-blanche.html/tajine-de-croquettes-daubergine-en-sauce-blanche-1>)

##  tajine eggplant croquettes in white sauce 

Hello everybody, 

I really like eggplants, and I always look for a different way to prepare a dish or an eggplant starter. This time it was on my cooking group on facebook, that one of my friends prepared eggplant fingers with meat. A recipe that has me much more, and that I did not hesitate two seconds to make this dish, by adopting the means of edges. 

Do not hesitate to make this dish, you will not regret the deliciousness of the dish, nor the well-known benefits of eggplant, this vegetable-fruit rich in antioxidant, Manganese, Copper, vitamin B1, vitamin B6 … and many more.   


**tajine eggplant croquettes in white sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-croquettes-daubergine-en-sauce-blanche-.jpg)

portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * for eggplant croquettes: 
  * 2 large aubergines 
  * 1 tablespoon of cumin (you can decrease if you do not like it too much, but it gives a good taste to kibble) 
  * salt and black pepper 
  * 2 egg yolks 
  * 1 half bunch of chopped parsley 
  * flour to pick up the croquettes (until you can make dumplings with two spoons) 
  * oil for frying. 
  * for the sauce: 
  * chicken pieces 
  * 1 medium onion 
  * 1 clove of garlic 
  * ½ bunch of parsley (leave a few strands for decoration) 
  * salt and black pepper 
  * 1 tablespoon of smen (or butter) 
  * 1 handful of chickpeas dipped the day before 
  * chilli (according to taste) 



**Realization steps**

  1. grill the eggplants, peel and crush them with a fork 
  2. add salt, black pepper, cumin, chopped parsley, 2 egg yolks and flour to have a dough that partially picks up, not a hard dough   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/quennelles-daubergine.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41892>)
  3. form dumplings using 2 tablespoons, and dip them directly into a hot frying bath. 
  4. after frying place them on paper towels. 

Prepare a white sauce: 
  1. fry the chopped onion and crushed garlic in the melted smen. 
  2. add the chicken pieces and let them come back a little. 
  3. add the half bunch of parsley, salt and black pepper. 
  4. add the chickpeas 
  5. water with 2 glasses of water and let it cook, if the chicken pieces without removing them, and let the chickpeas cook well 
  6. When serving, place the eggplant croquettes on the serving platter and sprinkle with sauce. 
  7. You can pass the pieces of chicken to fry to give it a beautiful color. 
  8. If you have prepared the croquettes early, you can preheat them in the oven. 



[ ![tajine eggplant croquettes in white sauce 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tajine-de-croquettes-daubergines-en-sauce-blanche-2.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-croquettes-daubergine-en-sauce-blanche.html/tajine-de-croquettes-daubergines-en-sauce-blanche-2>)
