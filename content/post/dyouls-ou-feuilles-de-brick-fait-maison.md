---
title: dyouls or homemade Brick leaves
date: '2018-04-12'
categories:
- crepes, waffles, fritters

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/dyouls-faits-maison-1.jpg
---
##  [ ![dyouls-homemade-1](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/dyouls-faits-maison-1.jpg) ](<https://www.amourdecuisine.fr/article-dyouls-ou-feuilles-de-brick-fait-maison.html/dyouls-faits-maison-1-2>)

##  dyouls or homemade Brick leaves 

Hello everybody, 

I did not change the recipe except I did half of the ingredients, because I worked on a pancake pan quite small, and I did not want to have a lot of dyouls .. 

The problem is not in the quantity, because these leaves keep well in the fridge, or in the freezer, but I did not want to spend the whole afternoon at the kitchen standing in front of my stove, 1and let the baby cry, lol 

the recipe in Arabic: 

**diouls or homemade Brick leaves**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/dyouls-faits-maison-1.jpg)

portions:  30  Prep time:  15 mins  cooking:  5 mins  total:  20 mins 

**Ingredients**

  * 400ml of warm water 
  * 250g of flour 
  * a little salt 
  * 2 tablespoons fine semolina 



**Realization steps**

  1. Mix all the dry ingredients 
  2. add the water gradually, while mixing with a whisk, to have a very liquid paste 
  3. let it sit a little 
  4. place a stove on a bain-marie 
  5. use a brush to brush the dough on the stove evenly 
  6. cook, until the dough comes off the edges of the pan 
  7. place the leaves of bricks on a clean cloth 
  8. lightly oil the surface, and cover the leaves with dyouls so they do not dry 



![](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/brrrrrrrrrr-0181.jpg)

recipes based on bricks leaves:   
  
<table>  
<tr>  
<td>

![flowers with artichoke cream](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/fleurs-a-la-creme-d-artichaut_thumb.jpg) [ **Fried flowers stuffed with artichoke cream** ](<https://www.amourdecuisine.fr/article-fleurs-frites-farcies-a-la-creme-d-artichauts-102277133.html>) 
</td>  
<td>

![brig-of-the-chicken-curry - Recipe of Ramadan-2013.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/brick-de-poulet-au-curry-recette-de-ramadan-2013.CR2_.jpg) [ **Chicken Brick with Curry** ](<https://www.amourdecuisine.fr/article-brick-de-poulet-au-curry-118763982.html>) 
</td>  
<td>

![brigs tuna au-009.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bricks-au-thon-009.CR2_.jpg) **[ Tuna bricks ](<https://www.amourdecuisine.fr/article-bricks-au-thon-recette-facile-et-rapide-117943659.html>) ** 
</td> </tr>  
<tr>  
<td>

![brick-a-la-ricotta-016.CR2-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/brick-a-la-ricotta-016.CR2-copie-1.jpg) [ **Bricks with ricotta** ](<https://www.amourdecuisine.fr/article-recette-de-ramadan-brick-a-la-ricotta-118717423.html>) 
</td>  
<td>

![bourak, ramadan recipes](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bourak_thumb.jpg) [ **Bourak with tuna and cheese** ](<https://www.amourdecuisine.fr/article-bourak-au-thon-et-fromage-80735244.html>) 
</td>  
<td>

![samosas from crepes panees](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/samossas-de-crepes-panees_thumb.jpg) [ **samosas of breaded pancakes** ](<https://www.amourdecuisine.fr/article-samossas-de-crepes-panes-114970535.html>) 
</td> </tr>  
<tr>  
<td>

[ ![shrimp brigs and bechamel](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame3-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame3.jpg>) [ shrimp and bechamel brigs ](<https://www.amourdecuisine.fr/article-bricks-aux-crevettes-a-la-bechamel.html> "shrimp brigs with bechamel") 
</td>  
<td>

[ ![potato shrimp shrimps](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-aux-crevettes-pommes-de-terre--150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-aux-crevettes-pommes-de-terre-.jpg>) [ shrimp and potato boureks ](<https://www.amourdecuisine.fr/article-boureks-aux-crevettes-et-pommes-de-terre.html> "shrimp and potato boureks") 
</td>  
<td>

[ ![bourek-to-Ramadan-a-la-apple-Earth-bourek-batata.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourek-pour-ramadan-a-la-pomme-de-terre-bourek-batata.CR2_1-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourek-pour-ramadan-a-la-pomme-de-terre-bourek-batata.CR2_1.jpg>) [ boureks potatoes and cheese ](<https://www.amourdecuisine.fr/article-bourek-aux-pommes-de-terre-et-fromage-entree-pour-ramadan.html>) 
</td> </tr>  
<tr>  
<td>

![indian samoussa 046](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/samoussa-indiennes-046_thumb.jpg) [ **indian samoussa** ](<https://www.amourdecuisine.fr/article-samoussa-indienne-97307661.html>) 
</td>  
<td>

![bourak with spinach and tuna 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bourak-aux-epinards-et-au-thon-1_thumb.jpg) [ **Boureks with spinach and tuna** ](<https://www.amourdecuisine.fr/article-boureks-aux-epinards-et-thon-107386648.html>) 
</td>  
<td>

![samoussa has chopped meat](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samoussa-a-la-viande-hachee_thumb.jpg) [ **Samoussa with Ground Meat** ](<https://www.amourdecuisine.fr/article-samoussa-a-la-viande-hachee-samoussas-88499027.html>) 
</td> </tr>  
<tr>  
<td>

[ **![bricks-a-la-minced meat-box-of-ramadan_thumb_11](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/bricks-a-la-viande-hachee-recettes-du-ramadan_thumb_11-150x150.jpg) bricks with chopped meat ** ](<https://www.amourdecuisine.fr/article-bricks-a-%3Cbr%20/%3E%3Cbr%20/%3E%3Cbr%20/%3E%20la-viande-hachee-108968133.html>) 
</td>  
<td>

![smoked salmon tapas.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-de-saumon-fume.CR2_thumb.jpg) [ brigs with smoked salmon ](<https://www.amourdecuisine.fr/article-amuse-bouche-au-saumon-fume-et-feuilles-de-bricks-113825045.html>) 
</td>  
<td>

[ ![malsouka-024_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/malsouka-024_thumb-150x150.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/malsouka-024_thumb.jpg>) [ tajine malsouka ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks.html> "Tunisian cuisine: Tunisian Malsouka-tajine") 
</td> </tr>  
<tr>  
<td>

[ **![chicken bourak with bechamel](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bourak-au-poulet-a-la-bechamel_thumb.jpg) ** ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bourak-au-poulet-a-la-bechamel_2.jpg>) [ **Bourek chicken with béchamel** ](<https://www.amourdecuisine.fr/article-bourek-au-poulet-a-la-bechamel-107496092.html>) 
</td>  
<td>

**![cheese bourek](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/menu-044_thumb.jpg) ** [ **cheese bourak** ](<https://www.amourdecuisine.fr/article-55595087.html>) 
</td>  
<td>

**![brick-to-apples-to-earth-and-tuna-entree de ramadan.CR2_1](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/brick-aux-pommes-de-terre-et-thon-entree-de-ramadan.CR2_1-150x150.jpg) [ potato and tuna bricks ](<https://www.amourdecuisine.fr/article-bricks-aux-pommes-de-terre-et-thon-entree-du-ramadan-119072877.html>) ** 
</td> </tr> </table>
