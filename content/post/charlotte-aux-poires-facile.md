---
title: charlotte with pears easy
date: '2016-09-09'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles and bars
- sweet recipes
tags:
- To taste
- Cakes
- Algerian cakes
- Spoon biscuits
- Easy cooking
- Pastry
- Inratable cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/09/charlotte-aux-poires-4.jpg
---
![charlotte-to-pear-4](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/charlotte-aux-poires-4.jpg)

##  charlotte with pears easy 

Hello everybody, 

I really like charlotte, and today I took advantage of [ spoon biscuits ](<https://www.amourdecuisine.fr/article-biscuits-a-cuillere-facile.html>) I prepared yesterday and I shared the recipe with you to make a delicious charlotte with pears. 

Frankly, and as I told you yesterday, with homemade sponge cookies the pear charlotte has made it a hit at home, I did not expect my kids to ask for more ... It was just perfect, melting in the mouth, super tasty, a recipe that I would do and would do with pleasure, especially since I made my halal gelatin stock high range in Algeria. 

![charlotte-to-pear-6](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/charlotte-aux-poires-6.jpg)

**charlotte with pears**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/charlotte-aux-poires-2.jpg)

portions:  10  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * biscuits with a spoon as needed 
  * 6 half pears in syrup for stuffing 
  * 4 egg yolks 
  * 120 g caster sugar 
  * 250 ml of milk 
  * 250 ml whole liquid cream 
  * 4 sheets of gelatin (8 gr) 

decoration: 
  * 6 half pears with syrup 
  * dark chocolate (optional) 
  * jelly (optional) 



**Realization steps**

  1. Put the gelatin sheets to soften in a bowl of cold water. 
  2. In a saucepan, bring the milk to a boil. 
  3. In a bowl, whisk the egg yolks with the sugar until the mixture whitens. Mix with the hot milk while whisking. Pour into the pan. 
  4. Make thicken over very low heat without stirring or boiling. 
  5. remove the cream from the heat and add the drained gelatin. stir to dissolve it well. Let cool.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/pour-la-charlotte-aux-poires.jpg)
  6. Whip the liquid cream very cold in chantilly (without adding the sugar). 
  7. Drain the pears, cut in small dice. Stir them together with the whipped cream but still liquid. 
  8. Line the bottom and sides of a charlotte pan with biscuits (here I used a pastry circle). 
  9. Fill it halfway with the pear cream. 
  10. Add another layer of biscuits to the spoon. and pour the second half of pear cream 
  11. refrigerate for at least 6 hours (overnight for me).   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/charlotte-aux-poires-5.jpg)
  12. Unmount the charlotte when serving 
  13. decorate with the cut pears and chocolate. 



![charlotte-to-pear-7](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/charlotte-aux-poires-7.jpg)
