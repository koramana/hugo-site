---
title: Samsa well delicious, Algerian cake
date: '2012-04-16'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa-laadjine.jpg
---
![samsa laadjine](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa-laadjine.jpg)

##  Samsa well delicious, Algerian cake 

Hello everybody, 

_Yesterday I had this crazy desire to eat samsa a delicious[ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) . a [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) very refined and as I had all my ingredients at hand, well it was a question of a little will. _

_this[ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , has different ways of preparing, the best known and the one with bricks leaves, there is also the method of preparing a dough and roll the stuffing in, but the one I like is the ravioli method, hihihihi Well yes we do not get tired too much. _

**Samsa well delicious, Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/samsa-028_thumb1.jpg)

portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** dough: 

  * 3 bowls of flour (3 measures of flour, so whatever the measure you will use) 
  * 1 bowl of butter 

1 pinch of salt 
  * for the stuffing: 
  * 2 bowls of almonds 
  * 1 bowl of crystallized sugar 
  * cinnamon 
  * Orange tree Flower water 

oil for frying. 

**Realization steps**

  1. prepare the dough, mixing the ingredients, to have a malleable paste, not to knead too much, not to give elasticity to the dough, because it will lose otherwise its crispness. 
  2. share the dough with medium sized balls. let it rest 
  3. prepare the stuffing that should not be too sticky or too liquid. 
  4. spread out a ball, place it on your little tool, roll it over this dough, just to have the drawing of the mold, put a spoon of stuffing in each triangle, spread another ball, place it on the first layer and pass the roll, and you have your little triangles, ready to go into the frying. 
  5. if you are afraid that your triangle will open when frying, brush the contact areas of both pasta with a little egg yolk. 



after frying, you can dip your Samsas in honey, and book in a large hermetic box. 

_and that it was boooooooooooooon_

_I made you many photos, well explained, and I made you a beautiful video demonstrative_

_Enjoy_
