---
title: onion soup
date: '2015-12-29'
categories:
- soups and velvets
tags:
- Toasts
- Bread
- Healthy cuisine
- Algeria
- Easy cooking
- Ramadan
- la France

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-a-loignon_.jpg
---
[ ![onion soup](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-a-loignon_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-a-loignon_.jpg>)

##  onion soup 

Hello everybody, 

You like the onion soup, this unmatched recipe of French cuisine that you have to know how to prepare well as it should, to be able to enjoy it at its fair taste ... It's not onions that we will cook in the water and here it is ready. Even if the recipe is easy to make, but you must respect the good preparation steps. 

I often enjoy it every time we go to London, in a small French Bistro, my husband and my children always ask me how I can love this soup ... But I like it a lot. So, and since I know their opinion on the recipe, I never tried to do it at home. 

**onion soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-a-loignon-21.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 kg and a half of onion 
  * 3 tablespoons of butter 
  * 3 tablespoons flour 
  * 2 liters of chicken broth 
  * salt and black pepper 
  * 1 pinch of nutmeg 
  * 1 baguette 
  * 150 gr of cheese 
  * [ homemade chicken broth ](<https://www.amourdecuisine.fr/article-bouillon-de-poulet-fait-maison.html>)



**Realization steps**

  1. in a pot, melt the butter, 
  2. add the onion cut into thin slices 
  3. stir and watch the onion, which should turn a beautiful golden color without burning. 
  4. add the flour to the rain, stirring 
  5. Pour over chicken broth, add salt, black pepper and nutmeg. 
  6. increase the heat and cook for at least 30 minutes. 
  7. Serve the onion soup with slices of cheese bread placed on top   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-gratin%C3%A9-au-fromage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-gratin%C3%A9-au-fromage.jpg>)

prepare slices of cheese bread: 
  1. cut the baguette into a slice of almost 1cm and a half 
  2. sprinkle the top of each slice of cheese, and bake in the oven at medium temperature until golden brown. 



[ ![onion soup](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-a-loignon-11.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-a-loignon-11.jpg>)
