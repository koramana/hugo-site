---
title: béchamel chicken waffle
date: '2016-10-29'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- accompaniment
- inputs
- Amuse bouche
- Aperitif Aperitif
- Algeria
- Ramadan
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/feuillete-tresse-au-poulet.jpg
---
##  béchamel chicken waffle 

Hello everybody, 

Here is a nice recipe of feuilletees braid stuffed with chicken breast and bechamel. A delicious recipe is super easy to make, that I share with you today, and I hope you enjoy it ... 

sometimes I make other varieties of this recipe with pieces of salmon, or just sautéed vegetables, and frankly it's a treat this entry. 

For the recipe of the [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html>) , just click on the link   


**béchamel chicken waffle**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/feuillete-tresse-au-poulet.jpg)

**Ingredients**

  * commercial puff pastry 
  * 300 grs of chicken breast 
  * a handful of pitted green olives 
  * 1 small box of mushrooms (optional) 
  * olive oil 
  * salt pepper 
  * thick béchamel 
  * grated cheese 
  * sesame seeds 
  * 2 egg yolks 



**Realization steps**

  1. cut the chicken in small cubes. 
  2. Fry your chicken in a pan with oil, salt and pepper. 
  3. add the mushrooms cut, make return well 
  4. Add the olives, book 
  5. Add the bechamel and grated cheese and let cool 
  6. spread out the dough, in a large rectangle, then draw two parallel lines to store the stuffing 
  7. cut diagonal strips 2 cm from the edge of the dough to the lines of the rectangle. 
  8. drop the stuffing in the middle 
  9. cross the strips to obtain a braid taking care to tighten them well to obtain a good result. 
  10. brush with egg yolk and sprinkle with sesame 
  11. cook in a hot oven at medium temperature (140 ° C) for a good puff pastry. 
  12. present with a nice salad well seasoned 


