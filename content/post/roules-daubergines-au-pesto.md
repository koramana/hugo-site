---
title: rolled eggplants with pesto
date: '2015-09-04'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-028_thumb1.jpg
---
##  rolled eggplants with pesto 

Hello everybody, 

present as [ appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) or as a small appetizer, here are delicious rolls of [ eggplant ](<https://www.amourdecuisine.fr/article-que-faire-avec-aubergines-102011214.html>) to rise with the delicious taste of [ basil pesto ](<https://www.amourdecuisine.fr/article-pesto-au-basilic-fait-maison-101909328.html>) , and sweet with melting mozzarella that flows with every bite. 

**rolled eggplants with pesto**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-028_thumb1.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 2 eggplants finely cut into strips 
  * 75 ml of olive oil 
  * 1 crushed garlic head 
  * 60 ml of basil pesto 
  * 175 grams of mozzarella 
  * salt and pepper 
  * basil leaves for decoration. 



**Realization steps**

  1. preparation: 
  2. dirty the eggplant, to remove the bitterness, and reduce some of its water. 
  3. rinse the eggplants well with the salt, and place them on sopalain paper. 
  4. fry in a little hot oil, and let drain well. 
  5. cover each eggplant leaf with pesto, then mozzarella, 
  6. and roll it on itself, then prick it with a toothpick. 
  7. place the eggplant rolls in a lightly oiled gratin dish, 
  8. cook in a preheated oven for 8 to 10 minutes at 180 degrees C until the mozzarella melts well. 
  9. present while decorating with chopped basil. 



follow me on: 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
