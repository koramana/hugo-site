---
title: Mudukhiya, or mloukhia with beef
date: '2015-02-16'
categories:
- Algerian cuisine
- Tunisian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mloukhiya-.jpg
---
[ ![Moulukhiya or beef mukhiya](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mloukhiya-.jpg) ](<https://www.amourdecuisine.fr/article-mouloukhiya-ou-mloukhia-au-boeuf.html/mloukhia-au-boeuf>)

##  Mudukhiya, or mloukhia with beef 

Hello everybody, 

If you are regulars and readers of my blog, you probably had the chance to read here and in my old articles, read passages on the recipes that I like most, and mloukhia, mloukhiya or mouloukhia figure always at the top of my list. 

This dish rocked my childhood, I remember that at the beginning when our neighbor of "Souq ahras" at the time passed a dish to my mother, she was the only one to eat it. My mother knew the dish well and often eaten it because she had an old Tunisian neighbor who also shared this recipe with my grandmother ... 

Finally what I want to tell you about Mouloukhia, is that it is not a dish that we will prepare without knowledge, and present it to his family, to tell them: "you must finish your plate !!! ", No it does not work like that with the Mouloukhia, it's a whimsical dish and a dish of connoisseurs, we do not eat the mloukhia for the first time and say, waw a delight, it's a dish that learns to taste first, then one begins to judge, then wants to taste again, to know what was not detected the first time, to ask why people like this dish so much? And then, a mouthful becomes two, then with time three, and after 5 or 6 mouthfuls, and finally we become hooked ... Yes it is the laws of the millukhia, she knows how to put in value, hihihi. It took my father over 10 years to start asking my mother to make him mukhukah. 

And its history does not stop there, this dish very capricious that takes time to be appreciated, also requires a lot of time to be cooked ... As for example this time, it took me more than 6 hours of cooking ... Yes the friends, the people who know this recipe, know these hours to watch her pot, to go back and forth so that the foam overflows, or so not that its glue at the bottom of the pot. In any case, in my eyes it's all worth it, because in my humble opinion, there is no more delicious than a hot Moulukhia with a very melting meat, accompanied by a [ matlou3 ](<https://www.amourdecuisine.fr/article-matloue-matlou3-matlou-pain-maison-au-tadjine.html> "Matlouh, matlou3, matlouh - home-made tajine bread") well spongy. 

[ ![matlou3](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/matlou3.jpg) ](<https://www.amourdecuisine.fr/article-mouloukhiya-ou-mloukhia-au-boeuf.html/matlou3>)

Matlou3 on tajine Electric stone 

A small remark: 

The mouloukhia of Eastern Algeria, and the Tunisian Mouloukhia, in no way resembles the Egyptian Mouloukhia, I had the chance to taste this Egyptian recipe which is actually prepared with fresh coret leaves, and which requires less at the cooking time, and I was lucky enough to have our Musskhia dish taste to my Egyptian and Sudanese friends, who were very seduced by our version, and who find that the taste of our mussoukhia is very different from theirs. 

besides you can see a way of the preparation of the Egyptian mukhia: [ rice with corn ](<https://www.amourdecuisine.fr/article-riz-a-la-corete-mouloukhiya.html>) it's the same sauce they get and they eat it with Egyptian reghif. 

And you, what do you think? 

[ ![Moulukhiya or beef mukhiya](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mloukhia-au-boeuf.jpg) ](<https://www.amourdecuisine.fr/article-mouloukhiya-ou-mloukhia-au-boeuf.html/mloukhia-au-boeuf>)   


**Mudukhiya, or mloukhia with beef**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mloukhia-2.jpg)

portions:  4 to 5  Prep time:  30 mins  cooking:  360 mins  total:  6 hours 30 mins 

**Ingredients**

  * 5 tablespoons of Moulukhia powder (Corete)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mouloukhia-en-poudre.jpg) ](<https://www.amourdecuisine.fr/article-mouloukhiya-ou-mloukhia-au-boeuf.html/mouloukhia-en-poudre>)
  * 200 ml of table oil not less 
  * 3 l of water 
  * 500 g beef 
  * 1 tablespoon coriander seeds 
  * 1 carvi coffee 
  * 4 crushed garlic cloves 
  * 1 chopped onion 
  * 1 peeled tomato 
  * ½ cup canned tomato 
  * 2 bay leaves 
  * ½ teaspoon salt (take care, despite the 3 liters of water the sauce will be completely reduced towards the end, and may be too salty) 
  * 1 teaspoon of harissa (optional for spicy fans especially) 

for the finalization: 
  * 1 clove of garlic 
  * 1 tablespoon of oil 



**Realization steps**

  1. In a high saucepan, sauté the chopped onion, the crushed garlic and the meat in a little oil, add the spices and salt, and let it simmer. 
  2. add the two tomatoes (fresh and canned), and the bay leaves 
  3. In a small bowl, mix the powdered corn with the 200 ml of oil, until you have a homogeneous paste. 
  4. Pour this mixture over the meat and let it simmer a little, stir with a wooden spatula so that it does not stick to the bottom. 
  5. cover everything with boiling water and let it cook   
Because my cocote is not too high, I put at the beginning 2 liter of very hot water, and the reduction of water, I add the other liter of water. 
  6. Leave a wooden spoon in the pot so that the foam does not overflow. 
  7. turn down the heat, the mudukhia cooked over low heat, for 4 to 6 hours 
  8. let the sauce reduce uncovered, until the oil begins to appear on the surface, it is the secret of the success of the mukukhiya, otherwise you have to add more boiling water. 
  9. If you want to know if your dish is ready, sauce with a piece of bread, if it is just wet, so it's not cooked yet. 
  10. The mllukhiya is not eaten with a spoon, it must be sapped with good pieces of bread crispy, or with good matlou3. 



[ ![Moulukhiya or beef mukhiya](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/mouloukhiya-au-boeuf.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-mouloukhiya-ou-mloukhia-au-boeuf.html/mouloukhiya-au-boeuf-cr2>)
