---
title: thanks to you i'm at the top
date: '2010-08-21'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/08/au-top6.jpg
---
![in-top.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/au-top6.jpg)

Hi my dear readers, today no recipes, although there are, but I wanted to give you a little recap of the evolution of my blog. 

and it's a real pleasure to see you like my blog, I hope I can do more, and I hope to live up to your expectations. 

at first I thank you because I'm bitch at the top of cooking blogs thanks to your visits, thank you very much. 

and I will publish to you the most visited and loved recipes: 

![in-top1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/au-top11.jpg)

[ Quick menu of a day of Ramadan ](<https://www.amourdecuisine.fr/article-menu-rapide-d-une-journee-du-ramadan-55412808.html>)

![in-top2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/au-top21.jpg)

[ Bavarian peach / cream mousse pastry ](<https://www.amourdecuisine.fr/article-bavarois-peche-mousse-de-creme-patissiere-55462997.html>)

![in-top3.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/au-top31.jpg)

[ cheese bourak ](<https://www.amourdecuisine.fr/article-55595087.html>)

![in-top4.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/au-top41.jpg)

[ stuffed crepes with chicken ](<https://www.amourdecuisine.fr/article-crepes-farcies-au-poulet-55641173.html>)

![in-top5.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/au-top51.jpg)

[ pepper rolls ](<https://www.amourdecuisine.fr/article-roules-aux-poivrons-55641009.html>)

![the-top hello.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/08/au-top-hello1.jpg)

merci pour tout ce que vous faites pour moi, vous m’aidez beaucoup a faire plus et a continuez le chemin de mon blog. 
