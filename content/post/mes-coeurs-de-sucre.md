---
title: my sugar hearts
date: '2012-02-14'
categories:
- boissons jus et cocktail sans alcool

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/02/sucre-decoratifs-020_thumb1.jpg
---
![decorative sugar 020](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/sucre-decoratifs-020_thumb1.jpg)

##  my sugar hearts 

Hello everybody, 

We are used to pieces of sugar rectangular or square, but we can present them differently and it is very easy to make homemade ... 

when I was invited last time to a friend's house, I had seen sugar for decoration, which had sugar cubes of all shapes in it, I absolutely loved, and she told me that she brought it when of one of his travels, and that it was edible, 

so from my arrival at home, I made a small amount just to see if it will work, 

and…. it was sublime, there will be other shapes and colors, I promise you, hihihiih   


**my sugar hearts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/sucre-decoratifs-004.jpg)

**Ingredients**

  * sugar 
  * just a little water 
  * dye according to your taste 
  * A sachet 
  * chocolate molds 



**Realization steps**

  1. place the sugar in a bag 
  2. add the dye (gel for me) 
  3. mix well until color becomes uniform 
  4. now add just a little water to moisten the sugar, not to wet it completely 
  5. mix again in the bag so that all the sugar is moistened 
  6. heap well in chocolate molds, 
  7. let dry well in the open air, overnight, or more depending on the heat of the room. 
  8. unmold and voila your small heart-shaped sugars, or any other form, to present when you are going to have guests, you can always have your reserve at home now 



![sugar-decoratifs.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/sucre-decoratifs1.jpg)
