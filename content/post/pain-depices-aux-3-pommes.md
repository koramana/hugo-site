---
title: gingerbread with 3 apples
date: '2016-11-15'
categories:
- cakes and cakes
- sweet recipes
tags:
- biscuits
- To taste
- desserts
- Algerian cakes
- Cakes
- Cake
- Breakfast

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/11/pains-depices-aux-pommes-2.jpg
---
![loaves dEpices-to-apples-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/pains-depices-aux-pommes-2.jpg)

##  gingerbread with 3 apples 

Hello everybody, 

When Lunetoiles redid this Marie Chioca recipe, she thought that the recipe lacked some sugar, so here you have the recipe with the modifications made by Lunetoiles. 

![loaves dEpices-to-apples-3](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/pains-depices-aux-pommes-3.jpg)

**gingerbread with 3 apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/pains-depices-aux-pommes-1.jpg)

portions:  8  Prep time:  10 mins  cooking:  45 mins  total:  55 mins 

**Ingredients**

  * 3 untreated Royal-Gala apples 
  * 250 g of acacia honey 
  * 220g of wheat flour or large spelled T110 
  * 3 eggs 
  * 1 bag of baking powder 
  * 10 cl of vegetable oil of neutral flavor (sunflower for example ...) is 85 gr approx 
  * 1 C. thinly sliced ​​cinnamon 
  * 5cl of orange juice 
  * 1 good pinch of salt 



**Realization steps**

  1. Preheat oven to 180 ° C, static heat. 
  2. Wash and wipe the apples, place them in a cake pan about 25cm long by 12cm wide, covered with baking paper. 
  3. Thoroughly whisk flour, salt, cinnamon and baking powder, make a well. Pour honey, oil, orange juice, eggs into the well and whisk well. 
  4. Pour around the apples and bake for 45 minutes or 1 hour, lowering to 160 ° C after 45 minutes, for the apples to finish cooking. 
  5. Cool completely before cutting. 



![loaves dEpices-to-apples](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/pains-depices-aux-pommes.jpg)
