---
title: Biryani chicken with Iranian rice
date: '2015-10-17'
categories:
- diverse cuisine
- Cuisine by country
- rice
tags:
- tajine
- Oriental cuisine
- Algeria
- dishes
- sauces
- Syria
- Iran

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/poulet-biryani-iranien.jpg
---
[ ![Iranian biryani chicken](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/poulet-biryani-iranien.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/poulet-biryani-iranien.jpg>)

##  Biryani chicken with Iranian rice 

Hello everybody, 

After the [ Indian biryani chicken ](<https://www.amourdecuisine.fr/article-biryani-au-poulet.html>) Here we are today with Iranian biryani chicken. According to the two recipes the difference is especially in the preparation of the sauce, because in the Iranian biryani chicken, there is neither tomato nor natural yoghurt, and it is not necessary to put in the oven. In Iranian biryani chicken, once the rice and sauce are ready, it can be used. 

And you, do you have another different recipe for biryani chicken? there are recipes I assure you ... 

**Iranian biryani chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/riz-iranien-poulet-biriyani.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 500 gr of chicken (legs, or whites depending on choice) 
  * 4 cloves powder 
  * 1 star anise (star anise) powder 
  * 4 green cardamom powders 
  * 1 teaspoon of powdered fennel seeds 
  * 1 cinnamon stick 
  * ½ teaspoon powdered ginger 
  * 1 bay leaf 
  * 1 teaspoon coriander powder 
  * 2 thin onions 

for rice: 
  * 2 glasses of rice (220 ml) 
  * 1 tablespoon of oil 
  * 1 to 2 sliced ​​potatoes 



**Realization steps**

  1. Start by marinating the chicken in the spices mentioned above 
  2. then in a pot, put some oil and chopped onions in cubes, 
  3. when the onion is translucent, add the chicken with its spices. and cook well under medium heat. 
  4. when the chicken is cooked and the sauce is reduced, your sauce is ready. 

prepare the rice now: 
  1. place the rice in cold water for 1 hour 
  2. drain the rice, and cook the water in a little water, until it is cooked moderately (Aldenté) so do not cook the rice completely. 
  3. in a pot with a thick bottom, put some oil, place the slices of potatoes. 
  4. place the rice on top, cover with a clean tea towel, and place the lid of the pot. 
  5. cook on low heat in the steam. 
  6. you can turn the rice kernels with a thin skewer just to stir it. 
  7. add pieces of butter to the rice, it will give a taste of more 
  8. to give some color to your rice, dilute some saffron in a little water, and moisten the rice with this dye. 
  9. serve this rice with the chicken and some of the sauce. 



[ ![Iranian biryani chicken-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/poulet-biryani-iranien-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/poulet-biryani-iranien-001.jpg>)
