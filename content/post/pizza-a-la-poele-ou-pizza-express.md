---
title: pizza with the stove or pizza express
date: '2010-11-03'
categories:
- bavarois, mousses, charlottes, recette l'agar agar

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/11/piz-023_thumb.jpg
---
##  pizza with the stove or pizza express 

Hello everybody, 

yumi for this super delicious pizza in the pan or pizza express that I had found in a site, I do not know which one, because I realized this recipe well before starting to blog. 

I admit that since I started making this pizza or pizza pizza recipe I have not been disappointed, of course it's not like the crisp pizza in the oven, but it's still role anyway when you want a quick pizza made and well done. The dough is very soft and my son calls it " **pizza cake** " 

**pizza with the stove or pizza express**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/piz-023_thumb.jpg)

**Ingredients**

  * 150 grams of flour 
  * 150 ml of milk 
  * 2 eggs 
  * 1 C. coffee baking powder 
  * 2 tbsp. oil soup 
  * 1 pinch of salt 
  * and for the garnish it's according to your taste, for me it was a nice tomato sauce, garnished with olive, tuna and grated cheese. 



**Realization steps**

  1. mix all the ingredients to have a dough a little runny, heat a frying pan with a non-sticky surface (anti adhesive) grease it with a little oil, pour the dough (here it's up to you to see if you like a dough thick, or fine for me I made two small pizzas. 
  2. the pan should be very hot, pour a little of your stuffing, cook on low heat, and monitor, as the dough takes a little color, invert and cook the other side, and start to garnish the face gilding (not too brown) recover the pizza, let continue cooking and at the end of cooking place it on a rack. 
  3. enjoy the hot. 


