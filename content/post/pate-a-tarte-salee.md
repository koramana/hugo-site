---
title: salty pie crust
date: '2016-05-10'
categories:
- pizzas / quiches / tartes salees et sandwichs
tags:
- Vegetables
- inputs
- Algeria
- la France
- Easy cooking
- Fast Food
- quiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-a-tarte-sal%C3%A9e-1.jpg
---
[ ![salty pie dough 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-a-tarte-sal%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-a-tarte-sal%C3%A9e-1.jpg>)

##  salty pie crust 

Hello everybody, 

At the request of many of you, I share today the basic recipe that I use all the time for my savory pies, or even my pies. A very short pastry that gives a taste to your pies salty that will not leave those who taste your delicacies based on this dough indifferent. 

This is a recipe that even my friend Lunetoiles often uses in these pies and quiches that she often shares on my blog (I even think that I learned from her home). Sometimes I add spices or herbs, it's always super greedy as a recipe. 

You can prepare this dough by hand, as you can place all your ingredients in the bowl of a large blinder and mix, as soon as the dough begins to pick up, remove it, pick it up by hand without kneading too much, form a ball that you cover with a film of food and in the refrigerator until the moment of use. 

**salty pie crust**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-a-tarte-sal%C3%A9e-2.jpg)

portions:  6  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 250g of flour 
  * 30 ml of cold water 
  * 1 egg 
  * salt 
  * 125 gr of ointment butter 



**Realization steps**

  1. Pour the flour on a work surface and add the butter ointment with your fingertips, merging the mixture. You will get a sandy mix like a crumble dough. 
  2. Make a well and add the egg, water and salt while kneading. 
  3. When a ball begins to form, pour it on the worktop. 
  4. Stretch the dough 5 to 6 times with the palm of your hand on the work surface, form a ball. 
  5. Wrap it in plastic wrap and store in the refrigerator for 2 hours before using. 



Note you can put all the ingredients in the bowl of a blinder, and mix to have a ball, remove, pick up and cover in food film. [ ![salt pie](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-a-tarte-sal%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/pate-a-tarte-sal%C3%A9e.jpg>)
