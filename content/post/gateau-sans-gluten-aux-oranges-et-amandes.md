---
title: gluten-free cake with oranges and almonds
date: '2015-10-16'
categories:
- cakes and cakes
tags:
- desserts
- Algerian cakes
- To taste
- Pastry
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-sans-gluten-aux-oranges-et-amandes-005_thumb1.jpg
---
##  gluten-free cake with oranges and almonds 

Hello everybody, 

A very delicious recipe, which has been appreciated at home only by "me" yesiiiiiiiiiiiii, we can not satisfy all tastes, my husband does not even try to drip, he does not like them [ cakes ](<https://www.amourdecuisine.fr/categorie-10678925.html>) who "appear muddy" as he says, go understand what he means ???? 

My daughter is fine, she still drips, but because she was sick, I do not like to give her too much, so my son does not even bother to try, it's an Indian movie that we show him the spoon??? 

finally, myself, I really like, because I like the taste of the [ **orange marmalade** ](<https://www.amourdecuisine.fr/article-marmelade-d-oranges-non-amere-117420888.html> "non-bitter orange marmalade") , and the **orange peel** so I could only appreciate the cake.   


**gluten-free cake with oranges and almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-sans-gluten-aux-oranges-et-amandes-005_thumb1.jpg)

portions:  10  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients** for a mold of 20 to 22 cm 

  * 2 oranges 
  * 4 eggs 
  * 1 cup of honey (a glass of 240 ml) (330 gr) 
  * 2 cups of almond powder 
  * 1 teaspoon of baking soda 
  * 1/2 teaspoon of salt. 



**Realization steps**

  1. Place the two oranges in a saucepan with enough water to cover them, even if the oranges are floating, but they should be largely covered 
  2. Cover the pan. and cook for about 1 1/2 to 2 hours. When you can easily slip a toothpick or fork in, they are ready. You can add water when cooking, if necessary. 
  3. remove and let cool the oranges for a few minutes, cut into quarters and remove the stones or the inedible parts 
  4. mix the oranges until you have a smooth, lump-free dough. 
  5. Preheat the oven to 170 degrees C. 
  6. In a bowl, whisk the egg yolks add honey and dry ingredients (baking soda, salt and almond powder). 
  7. Add the orange paste to the mixture and mix well. 
  8. put the egg whites in snow with a little salt, gently stir in the mixture without breaking them 
  9. butter a mold with high edges and removable base or then wrap one to the baking paper 
  10. Pour the batter into the mold and bake for about 1 hour or until a toothpick inserted in the center comes out clean. 
  11. remove from oven, leave 5 min and unmold cake 


