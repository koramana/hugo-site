---
title: Martha stewart chocolate cookies
date: '2011-05-22'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles et barres
- gateaux, et cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/cookies-chocolat71.jpg
---
English hello everyone, always with a new recipe, in any case, it's not the recipes and the photos that are missing, but it's rather the time that is missing, frankly, I do not know anymore, is it that I am badly organized, or there are things to do at the same time. sometimes I do not hide the truth my dear, the desire to stop the blog trots my mind, and I try to distance myself as much as possible so that the stress gobbled me, because frankly, sometimes I tell myself that I spend a lot of & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.6  (  2  ratings)  0 

![Cookies-chocolat7.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/cookies-chocolat71.jpg)

Hello everybody, 

always with a new recipe, in any case, it's not the recipes and the photos that are missing, but it's rather the time that is lacking, frankly, I do not know anymore, is it me that is badly organized or there are a lot of things to do at the same time. 

sometimes I do not hide the truth my dear, the desire to stop the blog trots my mind, and I try to distance myself as much as possible so that the stress gobbled me, because frankly, sometimes I tell myself that I spend a lot of time on it, and that after I start to strangle myself to do this and that at the same time, and it puts me in all my states. 

and I will be very frank with you, I continue my blog, because I like to share what I can do, but also by that it helps me to make a little money in my pocket, something that everyone love, do not waste your time for nothing. 

but lately, these pennies have fallen so much that it does not buy a candy to children, hihihihihiih 

So, get tired of you for nothing ????? 

and really, everything we do on the blog, and all this time to prepare, to take the photos, to write the article, to arrange the photos, is it really worth it ??? ??? 

do not blame my dear friends, I am a human being, just like you, and in this life, we never do things without receiving another, we will say to myself: we do things for fun .... 

yes I am with you, but the fun becomes a fardo. 

I stop, otherwise it will become a subject without end, and I pass you this recipe of our talented Lunetoiles, whom I thank very much in passing, for his confidence, and for sharing with me all these delights: 

preparation time: 20 minutes 

the ingredients will give: 2 plates of 12 cookies: 

  * 220 g of dark chocolate of very good quality 
  * 4 tbsp. to s. of butter 
  * 2/3 cup of flour (86 gr) 
  * 1/2 cuil. to c. of baking powder 
  * 1/2 cuil. to c. salt 
  * 2 big eggs 
  * 3/4 cup of brown sugar (170 gr) 
  * 1 tbsp. to c. pure vanilla extract 
  * 250 gr (340 gr normally) of  nuggets of  chocolate 


  1. Preheat the oven to 180 ° C. 
  2. Heat chocolate and butter in a bain-marie stirring until melted. 
  3. In a bowl,  whisk together the flour, baking powder and salt. 
  4. In a bowl, beat eggs with brown sugar, vanilla extract and at high speed until light and fluffy. 
  5. Reduce speed to low and beat by adding chocolate  molten. 
  6. I  add the flour mixture without mixing too much. 
  7. Stir in chocolate pieces (chocolate chips or chunks). 
  8. Using an ice cream scoop, arrange 2 to 3 cm intervals between baking trays on baking trays. 
  9. bake for 12 minutes, until the cookies are shiny and crunchy but soft in the center 
  10. let cool  on the baking sheets for 10 minutes, then transfer them to a rack so that they cool completely. 

"Do not over cook cookies, they are meant to be soft and fluffy. "  ![Downloads1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/Downloads11.jpg) thank you for your comments and your passages  bonne journee 
