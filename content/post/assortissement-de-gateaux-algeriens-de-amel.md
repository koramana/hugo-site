---
title: Assorted Algerian cakes from Amel
date: '2010-03-18'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/03/arayeche11.jpg
---
Amel is a faithful visitor on my blog, always very nice to leave me comments, which has given rise to a small relationship of friendship between us. 

A month ago, Amel's friend gave birth, and Amel prepared some Algerian cakes, worthy of their names. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/arayeche11.jpg)

[ The Arayeches ](<https://www.amourdecuisine.fr/article-25345469.html>)

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/arayeche21.jpg)

professional work is not it? 

recipe [ right here ](<https://www.amourdecuisine.fr/article-25345469.html>)

![DSC02502](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/267049501.jpg)

The Mchaweks 

recipe [ right here ](<https://www.amourdecuisine.fr/article-25345484.html>)

![DSC02523](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/267049891.jpg)

the baqlawa 

![DSC02512](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/267050061.jpg)

the recipe is [ right here ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-aux-amandes-et-cacahuettes-61018034.html>)

![DSC02507](https://www.amourdecuisine.fr/wp-content/uploads/2010/03/267050461.jpg)

[ Tcharek mekkar ](<https://www.amourdecuisine.fr/article-tcharek-m-saker-tcharak-massakar-61311016.html>)

a big thank you to Amel 

and really it's delights 

and that happiness for the eyes. 
