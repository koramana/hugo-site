---
title: flowers with artichoke / tapas cream and appetizer
date: '2012-12-24'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe

---
Hello everybody, 

you want to impress your guests with [ appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) and [ appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) different from what you're used to concocting ... 

here are sublime fried roses **bricks leaves** or won your Chinese wraps stuffed with a cream of artichokes, really a crunchy delight and melting in the mouth. 

I will see this recipe among the [ ramadan recipes ](<https://www.amourdecuisine.fr/article-recette-du-ramadan-102488687.html>)

for 6 fried roses: 

  * 3 leaves of bricks, or 12 won your Chinese wraps 
  * 1/2 jar of heart of grilled artichokes marinated in oils 
  * 3 cloves of garlic 
  * 20 gr of parmesan 
  * 2 portions of cheese 
  * black pepper and salt. 
  * egg white for decoration 
  * 2 pieces of 8.5 cm and 4 cm 
  * oil for frying. 



method of preparation: 

  1. place cloves of garlic in aluminum foil, with a little oil, close and grill in the oven for 10 min. 
  2. in the bowl of the blinder, put the hearts of artichokes, grilled garlic after removing the skin, 
  3. add the parmesan, the cheese, the pepper, and mix to have a cream. 


  1. cut 6 large circles with the 8.5 cm cookie cutter, and between 30 and 36 small circles with the 3.5 cm cookie cutter. 
  2. place a 2 cm diameter nut in the big circle 
  3. brush the edges with egg white, and close it, 
  4. brush again with egg white, and start to place the petals 
  5. for each rose, you can stick between 5 and 6 small circles. 
  6. cook in a well preheated oil bath. 
  7. present as an aperitif with a fresh salad. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
