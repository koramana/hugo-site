---
title: love heart dessert icing mirror red
date: '2017-02-16'
categories:
- bavarois, mousses, charlottes, recette l'agar agar
- gateaux d'anniversaires, de fetes, celebrations
- recettes sucrees
tags:
- Red fruits
- Valentine's day
- Genoese
- Chocolate cake
- Birthday cake
- desserts
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/entremet-coeur-damour-gla%C3%A7age-miroir-rouge-2.jpg
---
![love heart dessert frosting red mirror 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/entremet-coeur-damour-gla%C3%A7age-miroir-rouge-2.jpg)

##  love heart dessert icing mirror red 

Hello everybody, 

With the quantities of the ingredients given below, she realized a dessert for a silicone mold of almost 21 cm in the form of heart, another entrance also in form of heart, in smaller, almost 18 cm, and 5 small hearts of almost 10 cm each ... so expect a great dessert with the quantities given. 

![love heart dessert icing mirror red 4](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/entremet-coeur-damour-gla%C3%A7age-miroir-rouge-4.jpg)

**love heart dessert icing mirror red**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/entremet-coeur-damour-gla%C3%A7age-miroir-rouge-1.jpg)

portions:  12 

**Ingredients** Chocolate biscuit : 

  * 4 eggs 
  * 125gr of sugar 
  * 30 gr of cornflour 
  * 80 gr of flour 
  * 25 gr of cocoa   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/m%C3%A9lange-biscuit-chocolat-296x300.jpg)

Lemon mousse: 
  * 4 sheets of gelatin 
  * 100 fr of sugar 
  * 1 sachet of vanilla sugar 
  * juice of 2 lemons 
  * 250 gr of mascarpone 
  * 40 cl of liquid cream. 

Strawberry mousse: 
  * 300 gr of strawberry 
  * 6 sheets of gelatin 
  * 150 gr of sugar 
  * 40 cl liquid cream   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/ingr%C3%A9dient-mousse-fraises.jpg)

Mirror icing: 
  * 75 gr of water 
  * 5 sheets of gelatin 
  * 150 gr of sugar 
  * 150 grams of glucose 
  * 150 gr of white chocolate 
  * 100 gr of sweetened condensed milk 
  * red dye 
  * 75 gr of water 



**Realization steps** preparation of the chocolate biscuit: 

  1. beat the eggs and the sugar until blanching, and have a good foamy mixture. 
  2. then add in small quantities the mixture: maize, flour and cocoa well sifted 
  3. pour the mixture into a 30 x 45 cm baking pan lined with baking paper. 
  4. cook for 12 minutes in a preheated oven at 180 ° C   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/biscuit-chocolat.jpg)

preparation of the lemon mousse: 
  1. soften the gelatin sheets in cold water 10 min before, 
  2. place the lemon juice and the zest and vanilla in a saucepan with the sugar and let heat, 
  3. remove from heat, add gelatin and mix well, let cool 
  4. Whip cream, add mascarpone and 2 tablespoons icing sugar 
  5. whisk and add to the lemon mixture, put in a mold and freeze 1 h   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/mousse-au-citron-300x270.jpg)

preparation of the strawberry mousse: 
  1. Soften gelatin in cold water 10 minutes before 
  2. To go to the mill or to blender the strawberries with the sugar to have a puree of strawberry. 
  3. Place in a saucepan and heat, 
  4. remove from heat, add gelatin and let cool 
  5. put the liquid cream in whipped cream, add the strawberry mixture gently. 
  6. remove the mold from the freezer and add the strawberry mousse to the lemon mousse 
  7. place a biscuit disc cut in the same shape as the mold and freeze overnight.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/entremet-coeur-damour-gla%C3%A7age-miroir-rouge.jpg)

preparation of the red mirror glaze:   
![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/gla%C3%A7age-miroir-rouge-300x281.jpg)

  1. Dip the gelatin sheets in cold water 10 min before 
  2. In a saucepan, heat the water, glucose, and sugar until boiling (103 ° C) remove from heat. 
  3. pour this mixture over the condensed milk, add the drained gelatin. 
  4. mix for the gelatin to melt, and add this mixture on the white chocolate and the red dye, 
  5. as soon as the white chocolate melts, mix everything in the blender foot, 
  6. place in an airtight box, film on contact with food film and reserve a whole night in the fridge, 
  7. the next day take the dessert from the freezer, unmold. 
  8. heat the icing at 35 ° C and glaze your cake. 



![love heart dessert icing mirror red 3](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/entremet-coeur-damour-gla%C3%A7age-miroir-rouge-3.jpg)
