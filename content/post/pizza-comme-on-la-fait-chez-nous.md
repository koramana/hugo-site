---
title: pizza as we did at home
date: '2008-01-01'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205552531.jpg
---
once again a happy new year, full of joy and happiness so I did not make the log no, no, .... no but I made a pizza, we enjoyed family well then at the recipe, whatever I know that many do it, but not everyone knows so for the pasta: 1 measure of fine semolina 2 measure of flour 2 cac of instant yeast salt oil water one mixes well the flour and the first semol with salt, then add oil and rub well with hands add warm water and 

##  Overview of tests 

####  please vote 

**User Rating:** 0.68  (  2  ratings)  0 

![th_Happy_New_Year_by_clwoods](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205552531.jpg)

once again a happy new year, full of joy and happiness 

so I did not make the log 

no no no 

but I made a pizza, which we enjoyed with family 

![S7301794](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205561371.jpg)

so to the recipe, whatever I know that many do, but not everyone knows 

so for the pasta: 

  * 1 measure of fine semolina 
  * 2 measure of flour 
  * 2 cac of instant yeast 
  * salt 
  * oil 
  * water 



![S7301784](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205563761.jpg)

first mix the flour and semolina well with the salt, then add the oil and rub well with your hands 

![S7301786](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205567071.jpg)

add the warm water and work the dough a little until you have a nice ball 

![S7301788](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205590901.jpg)

let rest and then rework the dough while adding water until you have a nice dough, very supple 

![S7301789](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205591131.jpg)

we let this paste rest in a place at average temperature, and we go to stuffing, 

this paste can be prepared in a MAP. 

for the garnish my ingredients were: 

  * red tomato almost 1 kg (I made 2 large trays) 
  * 2 cloves of garlic 
  * coriander 
  * salt and black pepper 
  * oil 
  * thyme 
  * olives 
  * enchois 
  * frommage 



Peel and cut half of the tomato into small ones. Blend the other half of the tomato with the garlic. 

add this tomato puree to the cut tomato and season with your condiments. 

spread the dough in an oil tray, prick with the fork, and adjust your filling, and of course add the enchois and olives (you can add mushrooms, shrimps, chilli, ... according to your taste) and cheese shoots. 

![S7301792](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205598071.jpg)

bake in an oven preheat 

serve hot: 

![S7301793](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205598631.jpg)

![S7301795](https://www.amourdecuisine.fr/wp-content/uploads/2008/01/205598851.jpg)

bonne appetit 
