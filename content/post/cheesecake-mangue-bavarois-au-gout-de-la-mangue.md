---
title: Mango / bavarian cheesecake to taste mango
date: '2013-09-26'
categories:
- dips and sauces
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/bavarois-mangue_thumb1.jpg
---
Hello everybody, 

Today is a busy day for me, before taking the kids for swimming lessons, I'm putting you back an old recipe. 

This cheesecake, Bavarian style, that we devoured, but the stupidity that I had done, is that when I took the detailed photos to make a beautiful video, and because the memory card of my camera was a new card, as I plugged it to my computer, it asked me to format the memory card, I tried everything to recover all the pictures made, but I could not do anything, so I formatted the card memory, I lost all the photos, do, but fortunately there was still a big piece of this delicious cake, and that I could share with you 

the recipe is very simple is super good delicious:   


**Mango / bavarian cheesecake to taste mango**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/bavarois-mangue_thumb1.jpg)

Recipe type:  Bavarian dessert  portions:  8  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients** for the base: 

  * 50 grams of coconut 
  * 150 grams of biscuits 
  * 70 grs of melted butter 

cheese mousse: 
  * 250 ml thick cream 
  * 200 grams of unsalted cheese 
  * 75 grams of sugar 
  * 6 grams of powdered gelatin or foil. 
  * mango or peach cut into pieces 
  * the juice of a mango or peach + 2 grams of gelatin for the mirror 



**Realization steps** prepare the database: 

  1. grill the coconut, just a few minutes because it burns quickly. 
  2. in a blender, mix the biscuits with the coconut, to have a fine mixture, then add the melted butter and to mix again. 
  3. pour this mixture into a mold with removable base, or then in a pastry circle place on a plate, and heap well with the back of a spoon, place in the refrigerator. during the preparation of the cheese mousse. 

prepare the cheese mousse: 
  1. whip the cream with 30 grams of sugar to make a nice mousse. 
  2. whip the cheese with the remaining sugar, add the mango cut in pieces or the peaches (according to your taste) 
  3. Melt the gelatin in a little water on a bain-marie, add the melted gelatine to the cheese mixture, and slowly add the whipped cream without breaking it, pour this mixture on the biscuit base, and return to the fridge. 
  4. Now prepare the mango mirror, placing the mango juice with a little gelatin in a water bath, until the mixture is completely dissolved. 
  5. and pour it gently on your cheese-cake. 
  6. leave it cool, and enjoy 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
