---
title: Potato salad
date: '2016-11-16'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-pomme-de-terre-radis-et-son-coulis-020_thumb.jpg
---
##  Potato salad 

Hello everybody, 

Here is a very very tasty [ salad ](<https://www.amourdecuisine.fr/categorie-10678933.html>) of potato that I prepared for me today. The children are at school, my husband does not come back from work so no need to cook. 

**This potato salad,** with a delicious potato coulis, instead of overdoing it with the fat sauces, and it was a real treat! Try it and give me your opinion. I really like the crunchy radishes add to this salad, if you do not just add an ingredient that you like the most.   


**Potato salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/salade-pomme-de-terre-radis-et-son-coulis-020_thumb.jpg)

portions:  2 

**Ingredients**

  * 2 medium sized potatoes 
  * 2 medium sized shallots 
  * 8 radishes 
  * some lettuce leaves 
  * 1 cup of mustard 
  * the juice of 1/2 lemon 
  * 2 tablespoons of olive oil 
  * salt pepper 
  * 1 coffee of oregano 



**Realization steps**

  1. peel the potatoes and cut them into cubes of medium size 
  2. boil them in salt water 
  3. when the potatoes are tender, drain and keep their water. 
  4. in the bowl of a blender, take the equivalent of half a potato (cooked cubes) 
  5. add mustard, olive oil, salt, lemon juice, black pepper, oregano, and mix. 
  6. add the water of the potato, gently to this mixture while mixing, to have a smooth sauce. 
  7. cut radishes and onions into thin slices 
  8. cut lettuce 
  9. mix everything in a salad bowl 
  10. and decorate with the potato coulis. 



Thank you for your visits and comments 

Bonne journee 
