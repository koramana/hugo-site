---
title: Old apple pie
date: '2011-01-23'
categories:
- les essais de mes lecteurs

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/tarte-aux-pommes-ancienne16a1.jpg
---
الوصفة بالعربية هنا & nbsp; hello everyone, another recipe of lunetoiles, an apple pie she kindly like to share with you, and frankly I'll wait for the first opportunity, to make this recipe, because his photos really give me a great want to put my hand in the screen and take a piece, greedy that I am so this recipe, lunetoiles has it at home: Fafa so I pass you the ingredients: & nbsp; Ingredients: & nbsp; For the dough: - 250 g flour - 125 g butter - 80 g sugar - 1 egg - & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![tart-with-apples-ancienne16a.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/tarte-aux-pommes-ancienne16a1.jpg)

Hello everybody, 

yet another recipe of lunetoiles, an apple pie she kindly like to share with you, and frankly I'll wait for the first opportunity, to make this recipe, because her photos really give me a great desire to put my hand in the screen and take a piece, greedy that I am 

so I pass you the ingredients: 

** Ingredients:  **

**For the dough:**

\- 250g of flour 

\- 125 g of butter 

\- 80 g of sugar 

\- 1 egg 

\- 1 pinch of salt 

**For garnish :**

\- 3 medium-sized apples 

\- the juice of a lemon 

\- Sugar and cinnamon to sprinkle the pie 

\- 5 tbsp apricot jam (or other), to give a shine to the pie 

**For applesauce:**

\- 3 medium-sized apples 

\- 1 tbsp honey 

![tart-with-apples-ancienne.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/tarte-aux-pommes-ancienne1.jpg)

**Preparation:**

**for the pasta**

  1. Take a salad bowl put in the softened butter, 
  2. add the sugar, the flour and the pinch of salt, 
  3. crush well with your fingers to get a nice sand pate. 
  4. Add the egg, gather everything quickly and form a ball. 
  5. Cover with a film and leave in the fridge for 30 minutes. 



**For the features:**

  1. Peel, wash and then cut the apples into small pieces. 
  2. Cook them for 15 minutes in 1/2 liter of water (or more) with the tablespoon of honey. 
  3. Mix everything to obtain a compote. 



**For garnish :**

  1. Peel the apples, cut in half, 
  2. remove the heart with the seeds before slicing 
  3. sprinkle with lemon juice to prevent them from oxidizing. 


  1. Take a baking sheet, put on baking paper 
  2. spread the dough, prick with a fork 
  3. pour the apple cake. 
  4. Arrange the apple slices according to your taste 
  5. Sprinkle with a mixture of sugar and cinnamon before baking 
  6. Bake at 180 - 200 ° C for 30 (or more depending on the power of your oven) 
  7. After cooking, heat in a pan about 5 tablespoon apricot jam and spread it evenly with a brush on the pie to give it a shiny appearance 



![tart-with-apples-ancienne17-a.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/tarte-aux-pommes-ancienne17-a1.jpg)   

