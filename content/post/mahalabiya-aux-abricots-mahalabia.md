---
title: mahalabiya with apricots / mahalabia
date: '2012-05-30'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya

---
Hello everybody, 

a delicious dessert that comes from Lebanon, the ** mahalabiya, mhalabia or mohalabiya  ** , very light as dessert, and very tasty, it looks a little like Algerian palouza (recipe coming soon) and also [ mhalbi ](<https://www.amourdecuisine.fr/article-mhalbi-creme-dessert-au-riz-56030222.html>) except that for the mhalbi, rice powder is used. 

this time, I prepared a delicious  mahalabiya  , garnished with a beautiful layer of  dried apricot cream  (So ​​do not bother saying: I'm going to wait for the apricot season, we have dried apricots all the time ...) 

the ingredients: (for 5 people, verrines of 200 ml) 

layer of mhalabiya: 

  * 2 glasses of milk  (240 ml) 
  * 1 glass of fresh cream 
  * 1/2 glass of sugar 
  * 1/4 glass of cornflour 
  * A pinch of salt 
  * 1 teaspoon of orange blossom water 
  * 1 tablespoon of butter 



for the cream of dried apricots: 

  * 100 gr  dried apricots 
  * water 
  * 2 tablespoons cornflour 
  * 1 teaspoon of orange blossom water 
  * sugar according to taste 
  * The juice of a lemon 



method of preparation: 

  1. Rinse the dried apricots for cleaning.  Place them in a bowl and cover with water.  Soak overnight or for three or four hours in the fridge 
  2. In a saucepan, combine all the dry ingredients. 
  3. Add the flower water, milk and cream. 
  4. Cook over low heat stirring constantly so lumps do not form. 
  5. The mixture should thicken well (if at a certain moment you notice that the mixture seems to turn - out of phase, pass the mixture to the blinder so that the cream becomes flexible).  Continue cooking until thick enough to coat the back of a spoon. 
  6. Remove from heat and add the butter, mix well. 
  7. Pour into small verrines filling the 3/4 of the verrine and  refrigerate. 
  8. remove the apricots from the fridge and pass them with their water to the blinder to have 400 ml of  puree (add water to have this amount of puree). 
  9. Skip this puree to the Chinese. 
  10. mix the apricot purée, cornflour, lemon juice and orange blossom water in the pan 
  11. add the sugar according to your taste and gently boil over low heat. 
  12. let it simmer until it thickens.  Remove from the fire 
  13. using a spoon, fill the mhalabiya verrines with this cream and return to the fridge. 



thank you for your visits and good day. 
