---
title: milk bread and 3 years on the blogosphere
date: '2010-11-07'
categories:
- amuse bouche, tapas, mise en bouche
- Algerian cuisine
- rice

---
hi everyone, already 3 years on the blogosphere, I never knew that this adventure that I started when Imane from the blog amounamazyouna started to push me to create a blog, I thank you very much Imane, you were and you are always a very good friend, very very nice and very attentive and very helpful especially with everyone. This blog is because you pushed me to do it, I do not think that without you I would think at any time to create one, big kisses to you my dear and dear friend. 3 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 1.25  (  2  ratings)  0 

Hello everyone, 

3 years of blogging, and I have 1693 subscribers to my newsletter, 591 articles published, 20553 comments, and almost an average of 3000 visitors a day, a number that I never think to achieve. 

thank you very much to you because you help me always go forward, you are my beloved readers. 

a beautiful crumb is shooting desire, nothing more to say, a very melting bread very delicious, pleasant and sweet, even after freezing, his rolls have lost none of their flavors or their textures. 

to do again and again without stopping, so do not be surprised if I do not publish too much recipe, it means that I redo his loaves again and again, hihihihihiih ... .. 

so I made you a copy paste of the recipe, although I did half the quantities and I had 12 rolls. 

Ingredients: 

  * 1kg of flour 
  * 18g of salt 
  * 100g of sugar ( _I put 165 divide for two, for half of each ingredient)_
  * 30g of fresh baker's yeast (3.5 cc of dry yeast) 
  * 380 to 400g of milk (or soya milk) 
  * 4 eggs (200g) 
  * 250g softened butter (or margarine) 



gilding: 

an egg yolk 

vanilla 

pearl sugar. 

for my part I worked in my map, I put all my ingredients in the order recommended for my bread machine, I started the kneading program, and I let the bread machine do its job, I let the dough rest and well triple volume. 

on a work plan flour, I guess the ball dough, each weighing almost 80 grams. 

and then I shape my rolls (must I tell you, that during the shaping, my batteries of the camera were flat, and I could not take you the photos in detail, the ones that I have try to do with my laptop ... .. a zero pointed) 

after shaping the rolls, place the dough pieces on the prepared baking trays spaced at least 5 cm apart. Cover with a clean, dry towel and let rest 1h30 to 2h depending on the temperature of the room, or as I did it in my oven without covering them. 

Preheat the oven th. 6-7 (200 ° C). Gently brown the puffed dough with the yolk of the egg yolk to which you add vanilla. Sprinkle with sugar and possibly cut the surface with sharp scissors soaked in gilding to form small pimples. 

Bake and cook for about fifteen minutes. Cool on a rack. Enjoy your meal! 

thank you for all your comments (there is that I still have not validate, because I want to respond to his wonderful comments) 

and thank you for wanting to subscribe to my newsletter, I will never say it's enough, the more you are, the more it makes me happy, hihihihihi 

gros bisous a vous tous 
