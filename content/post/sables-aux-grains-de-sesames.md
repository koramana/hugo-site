---
title: shortbreads with sesame seeds
date: '2015-05-25'
categories:
- gâteaux sablés, ghribiya
tags:
- Algerian cakes
- Algeria
- Cakes
- Cookies
- Aid
- biscuits
- Little butter

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-aux-grains-de-sesames-2.jpg
---
[ ![sesame seed biscuits 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-aux-grains-de-sesames-2.jpg) ](<https://www.amourdecuisine.fr/article-sables-aux-grains-de-sesames.html/sables-aux-grains-de-sesames-2>)

##  shortbreads with sesame seeds 

Hello everybody, 

**Jeljlaniya** : Always with a different way of presenting the [ shortbread cake ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-fraises.html> "sands with strawberry jam") , another cooking in a special silicone mold that I bought in Algeria more than 2 years ago, when the recipe of **Ms. Benbrim:** the heart sesame seed cakes (jeljlaniya) made its buzz ... But one thing that I could not realize the recipe, and my silicone molds are well forgotten at the bottom of my closet (you know my closet magic which contains all at once, but when we look for, we find nothing !!! ???) for those who follow my blog, this closet is very famous, is not it? 

So miraculously, this time, we wanted to look for something from my closet, my little mold to jump in my arms, I do not know who was happy to find us the other, in any case, it was the big reunion, especially that the jar of sesame seeds was just there, in sight, ready to use. What more could you ask for, besides following the ingredients to make my cake. 

At first, I wanted to make the recipe of Ms. Benbrim, but not to go to get the book, who knows where it hides, I preferred to make my shortbread recipe, a recipe that we appreciate a lot, and to which the taste and the crunchiness of the sesame seeds, is only going to be a spark of sweetness to these shortbreads.   


**shortbreads with sesame seeds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-aux-grains-de-sesames.jpg)

portions:  30  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 500 grams of butter (at room temperature) 
  * 2 bags of vanilla sugar 
  * 250 grams of icing sugar 
  * 4 egg yolks (the whites I do the mcheweks with) 
  * 2 bags of yeast 
  * 1 kg of flour 
  * decoration: 
  * Strawberry jam and lemon marmalade 



**Realization steps**

  1. beat the butter with the sugar and the vanilla until you have a nice cream. 
  2. add the egg yolks and whisk well once more 
  3. introduce the yeast and the flour (I do not put all the flour of the times, to know its quality) 
  4. wrap the dough in cling film, and let stand for 30 minutes 
  5. take pellets of almost 30 gr, coat them with sesame seeds and pack them in silicone molds with floured fingers to escape any air ball 
  6. filling the molds 
  7. bake in a preheated oven at 180 degrees for almost 20 minutes, watch the cooking well. 
  8. for a large quantity of this shortbread, you have to have at least 3 of these molds, each mold makes 15 pieces of shortbread 
  9. let the shortbreads cool, then carefully remove it from the silicone molds. 
  10. Repeat the operation with the rest of the dough until exhausted 
  11. Fill the imprint with a little jam and let it dry 



[ ![shortbread with sesame seeds-001](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-aux-grains-de-s%C3%A9sames-001.jpg) ](<https://www.amourdecuisine.fr/article-sables-aux-grains-de-sesames.html/sables-aux-grains-de-sesames-001>)
