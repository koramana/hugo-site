---
title: poppy buns - Scones with poppy seeds
date: '2012-01-12'
categories:
- salads, salty verrines

---
hello everyone, it's rare that I make recipes with poppy seeds, because my husband does not like at all, but when I was at the dentist last time, I flipped through a magazine, when I I saw this recipe, and I really like it, right away I took a picture of the recipe, with my little cell phone (it's better than writing it, or looking for a piece of paper, and a pen all over the bottom of the purse, and between wipes for child, a few packets of chewing gum, car parking tickets & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 1.3  (  3  ratings)  0 

Hello everybody, 

it's rare for me to make poppy seed recipes because my husband does not like it at all, but when I was at the dentist last time, I was flipping through a magazine when I saw this recipe , and I really like, right away I took a picture of the recipe, with my little cell phone (it's better than writing it, or looking for a piece of paper, and a pen all over the place the bottom of the handbag, and between wipes for child, some packets of chewing gum, car parking tickets (I do not have a car, but I do not know what it does in my bag), without talk about the receipts of purchases of such or such magazins ... .. finally all that to say to you, that it is genial the technology which allows us to keep an important information in a minute what we did before in 30 min .... 

then the recipe for you without you looking in your purse ![Winking smile](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/wlEmoticon-winkingsmile_21.png)

Ingredients for 20 small rolls: (I made half of the ingredients) 

  * 500 g of flour 
  * 2 tablespoons of soft butter 
  * 2 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 3 tablespoons poppy 
  * 1.5 teaspoons dry yeast 
  * 4 tablespoons of honey 
  * 275 ml of water 
  * 1 egg + a spoonful of milk 



method of preparation: 

  1. Sift the flour into a terrine, 
  2. add the butter and incorporate until all is crumble. 
  3. Add milk powder, salt, poppy seeds and yeast. 
  4. Pour the honey and water, to form a soft and elastic dough (knead for 5 minutes). 
  5. Place the dough in a bowl, cover and leave in a warm place until doubled in size (about 1 hour). 
  6. put the dough on a lightly floured surface, and form 20 equal balls. 
  7. place on the lightly floured baking tray, cover and let stand for 30 minutes. 
  8. Just before cooking, brush the scones with a beaten egg mixed with a tablespoon of milk. 
  9. Bake for about 12 - 15 minutes at 200 ° C. 
  10. put on a rack and let cool. 



thank you for your visit and have a nice day 

et si vous voulez suivre mon blog, inscrivez vous sur la newsletter de mon blog, et veillez SVP cochez les deux cases, sinon l’inscription n’est pas valide. 
