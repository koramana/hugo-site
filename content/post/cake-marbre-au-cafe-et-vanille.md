---
title: marbled cake with coffee and vanilla
date: '2015-11-15'
categories:
- cakes and cakes
- sweet recipes
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cake-marbr%C3%A9-caf%C3%A9-et-vanille-1.jpg
---
[ ![marbled cake coffee and vanilla 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cake-marbr%C3%A9-caf%C3%A9-et-vanille-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cake-marbr%C3%A9-caf%C3%A9-et-vanille-1.jpg>)

##  marbled cake with coffee and vanilla 

Hello everybody, 

You'll see that it's the same recipe from [ coffee cake ](<https://www.amourdecuisine.fr/article-cake-extra-moelleux-au-cafe.html>) , but a little twist of the recipe has given a nice result 

**marbled cake with coffee and vanilla**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cake-marbr%C3%A9-caf%C3%A9-et-vanille.jpg)

portions:  12  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 3 eggs 
  * 200 g of sugar 
  * 120 g of soft butter 
  * 200 g flour 
  * 1 tablespoon and a half of good quality coffee under the water (1.5 tablespoons hot water in which I dissolve 1.5 teaspoons of nescafe) 
  * 1 tablespoon and a half of cocoa powder 
  * 1 cup of vanilla coffee 
  * A pinch of salt 
  * Two level teaspoons of baking powder 



**Realization steps**

  1. Preheat the oven to medium temperature (180 degrees C0. 
  2. Start by whipping the sugar and the butter with an electric whisk. 
  3. continue beating for two minutes then add the eggs one by one while continuing to beat. 
  4. divide the dough in half, add to one coffee and cocoa, and add to the other half vanilla 
  5. In a bowl, mix the flour and yeast and divide the mixture in half. 
  6. Add the first half to the coffee mixture, little by little and stir with a wooden spatula until a homogeneous mixture is obtained. 
  7. do the same with the vanilla blend 
  8. Butter a cake mold, sprinkle with flour, shake the mold to remove the extra and pour both pasta according to your choice. 
  9. Bake for 35 to 40 minutes, check the cooking with a knife, 
  10. remove from the oven and allow to cool before unmolding. 
  11. Sprinkle with icing sugar when serving and savor. 



[ ![marbled cake coffee and vanilla 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cake-marbr%C3%A9-caf%C3%A9-et-vanille-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cake-marbr%C3%A9-caf%C3%A9-et-vanille-2.jpg>)
