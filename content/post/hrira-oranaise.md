---
title: Hrira Oranaise
date: '2015-03-01'
categories:
- Algerian cuisine
- ramadan recipe
tags:
- Healthy cuisine
- Easy cooking
- Velvety
- Algeria
- Vegetables
- Soup
- Meat

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/hrira-oranaise.jpg
---
[ ![Oran hrira](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/hrira-oranaise.jpg) ](<https://www.amourdecuisine.fr/article-hrira-oranaise.html/hrira-oranaise>)

##  Hrira Oranaise 

Hello everybody, 

I love the hrira, but at home, never leaven, my husband does not like, besides you can see, the [ hrira ](<https://www.amourdecuisine.fr/article-hrira-soupe-du-ramadan-aux-legumes.html> "hrira, ramadan soup with vegetables") from my mother, just vegetables to give it smoothness. 

As I have already tried [ Moroccan Harira ](<https://www.amourdecuisine.fr/article-harira-soupe-marocaine-du-ramadan-2014.html> "harira Moroccan soup of Ramadan 2014") , a harira of a softness and perfume And today it is a harira oranaise that I share with you, a harira that comes directly from Ireland, from my friend Nawel Zellouf. This harira is leavened, and well scented with caraway and kebaba (cubèbe). 

**Hrira Oranaise**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/hrira-oranaise.jpg)

portions:  4-5  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * 500g of lamb. 
  * 4 medium sized carrots cut into cubes. 
  * 2 Turnips cut into cubes. 
  * 1 large onion. 
  * 1 potato. 
  * 2 large grated tomatoes. 
  * 2 tablespoons of vegetable oil. 
  * 1 tablespoon of Smen. 
  * a bowl of chickpea. 
  * 2 tablespoons concentrated tomato. 
  * a Jumbo cube taste beef. 
  * a bunch of coriander. 
  * some branches of fresh mint. 
  * spices: salt, black pepper, ginger, saffron or food coloring, kebaba (4 spices), cinnamon, caraway, ras el hanout 

LEAVEN: 
  * 3 tablespoons flour, 
  * 1 tea glass of fresh water 



**Realization steps**

  1. In a pot, put the oil, the smen, the onion and the meat to come back a little 
  2. add the vegetables, coriander, and a little mint and the spices apart from the caraway and rass el hanout, and let it come back a little then add the water. 
  3. after meat and vegetables are cooked, filter and leave aside 
  4. take the broth and put back on the heat, add 1+ ½ tablespoon of caraway and 1 teaspoon of rass el hanout, coriander, and menth let it boil then with the help of a Chinese add the leaven while stirring without a stop on a gentle fire for 12 minutes. 
  5. after cooking refine the broth using a very fine Chinese to get rid of the coarsely ground spices. 
  6. Put the meat and vegetables back in, 
  7. Enjoy the hrira with lemon fillet and a little coriander and bouraka ...... 



[ ![feast of Nawel Zallouf](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/festin-de-Nawel-Zallouf1.jpg) ](<https://www.amourdecuisine.fr/article-hrira-oranaise.html/festin-de-nawel-zallouf-2>)
