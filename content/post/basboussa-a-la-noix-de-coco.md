---
title: Basboussa with coconut
date: '2017-04-16'
categories:
- gourmandises orientales, patisseries orientales
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/basboussa-a-la-noix-de-coco-032.jpg
---
##  Basboussa with coconut 

Hello everybody, 

The Coconut Basboussa is a honey cake that I really like, and frankly I have a recipe that I never change, because it's an inescapable recipe: [ basboussa with breadcrumbs ](<https://www.amourdecuisine.fr/article-basboussa-gateau-de-semoule-patisserie-algerienne-108142909.html>) , but yesterday I really wanted to change in the recipe, and by making a recipe at random, I do not tell you, this basboussa has the coconut has made success at home, I'm too proud. 

we do not worry, it's a snap, but I keep the measures for you and me.   


**Basboussa with coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/basboussa-a-la-noix-de-coco-032.jpg)

portions:  10  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 4 eggs 
  * 1 glass of coconut powder (1 glass of 250 ml) 
  * 1 glass of breadcrumbs 
  * ½ glass of semolina. 
  * ½ glass of sugar. 
  * ½ glass of oil 
  * 1 glass of milk 
  * 1 sachet of baking powder 
  * lemon zest 
  * zest of orange (I thank my mother, who froze me the zest of orange during the season of the oranges) 
  * 1 cup of vanilla sugar 

syrup: 
  * 300 ml of water 
  * 300 gr of sugar 
  * vanilla 



**Realization steps**

  1. first prepare the syrup and let cool. 
  2. mix dry ingredients 
  3. whisk the eggs, milk, oil and zest with the help of a manual whisk. 
  4. add the liquid mixture to the dry ingredients and pour the mixture into a buttered pyrex mold 22cm x 32 cm 
  5. cook in a preheated oven at 180 degrees C for 30 to 40 minutes, depending on your oven. 
  6. at the end of the oven, sprinkle gently with the vanilla syrup. and leave well absorbed. 
  7. cut in rhombus or square, and decorate with a little coconut. 



and if you have a facebook account, follow me on my page: 
