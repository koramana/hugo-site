---
title: gratin of potatoes and cheese
date: '2018-02-19'
categories:
- diverse cuisine
- Cuisine by country
tags:
- Algeria
- Ramadan 2018
- Ramadan
- Economy Cuisine
- Vegetarian cuisine
- Full Dish
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/02/gratin-de-pommes-de-terre-au-fromage-1.jpg
---
![potato gratin with cheese 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/gratin-de-pommes-de-terre-au-fromage-1.jpg)

##  gratin of potatoes and cheese 

Hello everybody, 

it tells you a gratin of all simplicity, very tasty and easy to do? So here you are the gratin of cheese potatoes, which we love a lot at home and that my children ask as soon as I ask the famous question: what do you want at dinner ???? 

Well here, I just had to ask the question, and my kids directly asked for the cheese potato gratin. It was for dinner, but all the same, I said to myself I'm going to make a video of this simple dish to share it with you. I know everyone makes gratin cheese potato, and everyone has their own family recipe, but no big deal, I still share with you. 

**gratin of potatoes and cheese**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/gratin-de-pommes-de-terre-au-fromage-3.jpg)

**Ingredients**

  * 5 medium potatoes 
  * salt, black pepper, nutmeg 
  * 2 cloves garlic 
  * butter for the mold, and some hazelnuts for the gratin 
  * ½ liter of milk 
  * 6 c. fresh cream 
  * cheese to taste: Emmental, Gruyere, Parmesan, 



**Realization steps**

  1. cut the washed and peeled potatoes into thin slices 
  2. butter a baking tin of almost 22 cm. 
  3. sprinkle some grated garlic (optional) 
  4. cover with a layer of potato slices, salt and pepper. 
  5. Sprinkle some nutmeg on it, 
  6. add ⅓ of the mixture milk and fresh cream. 
  7. garnish with grated cheese and garlic and do the operation 3 more times until all the slices of potatoes are gone. 
  8. cook in a preheated oven at 180 ° C for at least 1 hour (extend the cooking if necessary) 



![gratin with potatoes and cheese 2](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/gratin-de-pommes-de-terre-au-fromage-2.jpg)
