---
title: grated carrot pie
date: '2015-11-16'
categories:
- pizzas / quiches / pies and sandwiches
tags:
- quiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-aux-carottes-1.jpg
---
[ ![carrot pie 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-aux-carottes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-aux-carottes-1.jpg>)

##  grated carrot pie 

Hello everybody, 

Anyway, I really like this carrot-based salty pie, moreover you can find another version on my blog, [ the carrot quiche ](<https://www.amourdecuisine.fr/article-quiche-aux-carottes-samira-tv-119190687.html>) . 

The recipe in Arabic: 

**grated carrot quiche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-aux-carottes-300x196.jpg)

portions:  6  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 1 puff pastry or use a homemade dough 
  * 7 to 10 small carrots 
  * 1 onion 
  * salt pepper, 
  * spices (raz el hanout and cumin powder) 
  * 3 eggs 
  * 20 cl of liquid cream 
  * mustard (optional) 
  * grated cheese 



**Realization steps**

  1. Grate the carrots, reserve. Sweat the chopped onion with a drizzle of oil, add the carrots, season (I put ras el hanout and cumin). Cook for 10 minutes, stirring regularly, then turn off the heat and let cool. 
  2. In the meantime, mix the eggs and the cream. 
  3. Dip the dough in a dish, brush it with mustard (optional) and sprinkle with a little grated emmental cheese. Then divide the cooked carrots and pour the egg / cream mixture over the whole surface, finish with grated cheese. 
  4. Bake until fully gilded. 



[ ![carrot pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-aux-carottes-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-aux-carottes-2.jpg>) [ ![carrot quiche](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-aux-carottes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/quiche-aux-carottes.jpg>)
