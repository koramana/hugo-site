---
title: avocado cream / olives / smoked salmon tapas
date: '2012-12-27'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tandoori-006.CR2_thumb1.jpg
---
Hello everybody, 

you like lawyers, at home at home, it never fails, but sometimes I forget it, and I think it's too old to be presented in a salad, the solution a cream of avocado, and with avocado cream, I can make a lot of tapas, amuse bouche, and verrines .... 

today with my avocado cream, I presented two appetizers super delicious, and very easy to achieve. 

[ cherry tomatoes with avocado cream ](<https://www.amourdecuisine.fr/article-tapas-a-la-creme-d-avocat-tomate-cerises-farcies-113829637.html>)

and olives with avocado cream and smoked salmon. 

Ingredients: 

  * some pitted black olives 
  * 1 mature lawyer 
  * 1 cup of fresh cream 
  * 1 teaspoon of lemon (or according to your taste) 
  * 1/4 spoon of curry (optional, but I like the taste) 
  * two slices of onion. 
  * salt 
  * smoked salmon 



method of preparation: 

  1. in the bowl of a blinder, place the avocado slice, salt, curry, lemon juice. onion and cream 
  2. mix to get a cream. 
  3. put this cream in a piping bag. 
  4. fill the black olives with the avocado cream ... 
  5. roll in smoked salmon 
  6. prick them with a toothpick ... and present. 

  
<table>  
<tr>  
<td>

[ ![cucumber crab and goat](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tandoori-006.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tapas-du-reveillon-concombre-au-chevre-et-crabe-113788582.html>) 
</td>  
<td>

[ ![smoked salmon tapas.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-de-saumon-fume.CR2_thumb2.jpg) ](<https://www.amourdecuisine.fr/article-amuse-bouche-au-saumon-fume-et-feuilles-de-bricks-113825045.html>) 
</td>  
<td>

[ ![olive and goat tapas 021.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-olive-et-chevre-021.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-amuse-bouche-olive-et-chevre-113801503.html>) 
</td> </tr>  
<tr>  
<td>

[ ![amuse bouche](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/fleurs-a-la-creme-d-artichaut_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-fleurs-a-la-creme-d-artichauts-tapas-et-amuse-bouche-113751782.html>) 
</td>  
<td>

[ ![homemade breadsticks](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gressins-au-basilic_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-gressins-tres-facile-113765720.html>) 
</td>  
<td>

[ ![crab and parmesan cheese 021](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bouchees-au-crabe-et-parmesan-021_thumb5.jpg) ](<https://www.amourdecuisine.fr/article-bouchees-de-crabe-au-parmesan-101566392.html>) 
</td> </tr>  
<tr>  
<td>

[ ![amuse mouth artichokes goats 088](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/artichauts-chevres-088_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-chevre-dore-sur-fond-d-artichaut-100098265.html>) 
</td>  
<td>

[ ![appetizer with aubergines](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-028_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-roules-aux-aubergines-113337954.html>) 
</td>  
<td>

[ ![tomato nests with eggs 028](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/nids-de-tomates-aux-oeufs-028_thumb.jpg) ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs-101484090.html>) 
</td> </tr>  
<tr>  
<td>

![Olive-Stuffed-038-a_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/olives-farcis-038-a_thumb1-150x150.jpg) 
</td>  
<td>

[ ![cornet with boulghour 005a](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cornet-au-boulghour-005a_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-cornets-au-taboule-110333711.html>) 
</td>  
<td>

[ ![smoked salmon roulade](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/roulade-au-saumon-fume_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-roulades-de-saumon-fume-au-mascarpone-et-fenouil-101257027.html>) 
</td> </tr>  
<tr>  
<td>

[ ![tuna rice mouths](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/amuses-bouches-au-riz-thon_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-boulettes-de-riz-au-thon-97137268.html>) 
</td>  
<td>

[ ![TARTARE OF SMOKED SALMON](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/TARTARE-DE-SAUMON-FUMEE_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tartare-de-saumon-fumee-en-cornets-de-crepes-100294277.html>) 
</td>  
<td>

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/hasselback-potatoes-hasselbackpotatis_thumb.jpg) ](<https://www.amourdecuisine.fr/article-pommes-de-terre-a-la-suedoise-hasselbackpotatis-108541271.html>) 
</td> </tr>  
<tr>  
<td>

[ ![scallop walnut 009](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/noix-de-saint-jacques-009_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-noix-de-saint-jacques-bouchee-a-la-creme-de-persil-97454414.html>) 
</td>  
<td>

[ ![Pepper rolls 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/Roules-aux-poivrons-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-roules-aux-poivrons-97975273.html>) 
</td>  
<td>

[ ![dumplings with surimi amuse](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/boulettes-au-surimi_thumb3.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-boulettes-au-surimi-113306581.html>) 
</td> </tr> </table>
