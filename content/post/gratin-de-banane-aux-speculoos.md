---
title: banana gratin with speculoos
date: '2018-04-28'
categories:
- dessert, crumbles and bars
- sweet recipes
tags:
- desserts
- Algerian cakes
- To taste
- Chocolate
- biscuits
- Cake
- Easy recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gratin-de-banane-aux-speculoos.jpg
---
![gratin de banana-aux-spiced](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gratin-de-banane-aux-speculoos.jpg)

##  banana gratin with speculoos 

Hello everybody, 

I'm sure this delicious banana gratin with speculoos will disappear quickly when you make the recipe. 

I had spotted and realized this recipe a while ago, and I did not notice that I never posted it on my blog. This banana gratin with speculoos is just too good, plus we can have variations of these little gratins nothing that change the fruit. 

You can use apples, pears, strawberries ... yum yum the choices is yours. 

**banana gratin with speculoos**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gratin-de-banane-aux-speculoos-1.jpg)

portions:  2  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 1 banana 
  * 2 eggs, 
  * 4 speculoos 
  * 3 to 4 tbsp. fresh cream 
  * vanilla sugar 
  * chocolate chips 



**Realization steps**

  1. crush the speculoos and place them in the bottom of two ramekins 
  2. add over the banana slices 
  3. sprinkle chocolate chips 
  4. whisk eggs, cream and vanilla sugar 
  5. fill the ramekins with this mixture   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gratin-de-banane-au-speculoos-4.jpg)
  6. and in a preheated oven at 180 ° C until the small gratins turn a beautiful golden color. 
  7. enjoy a little warm with a scoop of vanilla ice cream. 



![gratin de-spiced banana-to-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gratin-de-banane-aux-speculoos-2.jpg)
