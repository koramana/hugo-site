---
title: Smooth flowing in the heart of blueberries, fast and easy
date: '2014-10-26'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/moelleux-au-coeur-en-myrtilles-052.CR2_11.jpg
---
![soft-to-core-in-blueberries-052.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/moelleux-au-coeur-en-myrtilles-052.CR2_11.jpg)

##  Smooth flowing in the heart of blueberries, fast and easy 

Hello everybody, 

here is a delicious mellow fondant in the heart of blueberries, a cake to fall, we do not even know how to eat it, so it's melting .... 

and blueberries all honey that crunch a mouth ... frankly a recipe to redo as soon as possible. 

**Smooth flowing in the heart of blueberries, fast and easy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/moelleux-au-coeur-en-myrtilles-039.CR2_11.jpg)

portions:  4  Prep time:  10 mins  cooking:  7 mins  total:  17 mins 

**Ingredients**

  * 2 eggs 
  * 60 brown sugar 
  * 1 tablespoon of flour 
  * 1 tablespoon cornflour 
  * 140 gr of dark chocolate 
  * 110 gr of butter 
  * 3 tablespoons of whipping cream. 
  * blueberry jam as needed 



**Realization steps**

  1. preheat the oven to 180 degrees C 
  2. flour the silicone molds, otherwise individual aluminum molds, and it gives a better result. 
  3. beat the eggs and sugar until the mixture turns pale and smooth. 
  4. add the sifted flour and the maïzena and mix. 
  5. melt chocolate, butter and whipped cream in a bain-marie. 
  6. add the melted chocolate mixture, a little warm on the egg mixture. 
  7. pour this mixture into the molds, filling them in ¾. 
  8. add the equivalent of a tablespoon of blueberry jam 
  9. cook the cake between 6 to 7 minutes. the cakes must be very soft to the touch. 


