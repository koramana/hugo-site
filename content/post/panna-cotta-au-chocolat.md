---
title: chocolate panna cotta
date: '2013-09-28'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/panna-cotta-au-chocolat-facile1.jpg
---
![panna cotta au chocolate-facile.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/panna-cotta-au-chocolat-facile1.jpg)

Hello everybody, 

Who wants a delicious dessert all chocolate, well melted in the mouth, and of an infine sweetness ???? so I share with you today the recipe for this delicious panna cotta, very easy to make, and with very easy ingredients and the scope of everyone.   


**chocolate panna cotta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/panna-cotta-au-chocolat2.jpg)

Recipe type:  dessert, sweet verrines  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 250 ml of fresh cream. 
  * 50 gr of dark chocolate 
  * 40 gr of sugar 
  * 1 tablespoon of bitter cocoa 
  * 4 gr of gelatin   
Agar-agar can replace gelatin in all recipes, just know that 1 coffee agar-agar (which corresponds to about 4g) is equivalent to 6 sheets of gelatin, it takes between 2 and 4g agar per liter and boil in a liquid for 2 minutes. 



**Realization steps**

  1. Soak the gelatin sheets in cold water. 
  2. In a saucepan, heat the cream. 
  3. Break the chocolate into pieces, add it to the cream, along with the sugar and bitter cocoa. Stir constantly until the chocolate is melted. 
  4. Continue stirring to the first broth and out of the heat. 
  5. Squeeze the gelatin sheets and place them in the previous mixture to melt them. 
  6. Pour into small individual pots and put in the fridge for 2 or 3 hours 



{{< youtube yUrWnT6ks_c >}} 
