---
title: Horn of gazelle / cake for the aid 2013
date: '2013-08-03'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cornes-de-gazelle-aux-noisettes-2_thumb1.jpg
---
Hello everybody, 

I still do not publish the [ Algerian cakes ](<https://www.amourdecuisine.fr/article-bonne-fete-d-aid-kbir-et-mes-gateaux-algeriens-88105516.html>) that I prepared at the past aid party, today I wanted to put the ghribia recipe with peanut butter, and guess what? I only had one picture, hihihiih yes only the one I took for the help ??? 

I do not know what will happen, neither where are the photos but I will redo this delight very soon and make you this time without missing, full of photos. 

so, because it put me in all my states, especially that the recipe is already written ... I preferred to put a recipe still ready for Lunetoiles so your ingredients: 

**Horn of gazelle / cake for the aid 2013**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cornes-de-gazelle-aux-noisettes-2_thumb1.jpg)

Recipe type:  Algerian cake  portions:  100  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** Dough: 

  * 1 kg of flour 
  * 400 gr of melted butter 
  * 1 nice pinch of salt 
  * 2 eggs 
  * dozens of tablespoons of orange blossom 
  * a little vanilla if you like) 
  * water to pick up the dough 

Almond stuffing: 
  * 500 gr of coarsely ground hazelnuts 
  * 240 gr of powdered sugar 
  * 2 teaspoons cinnamon 
  * 50-60 gr of melted butter 
  * Orange Blossom 



**Realization steps**

  1. Begin by preparing the stuffing by mixing all the ingredients mentioned, add a little orange blossom while working the stuffing that must be easily held, 
  2. take small balls of 20 grams (I have weighed each time) 
  3. to lengthen them in pudding then in croissants, you can help yourself with a glass to form a pretty croissant, I advise you to form all the croissants before going to the shaping of the horns of gazelle 
  4. To prepare the dough, start by working the flour, salt and melted butter, 
  5. then add the rest of the ingredients. 
  6. Add water slowly to obtain a smooth paste that is easy to work with. 
  7. Take a little dough and roll it out finely, 
  8. arrange a croissant of almond and cover with dough by marrying the shape of the croissant by pressing slightly to make adhere the paste 
  9. use a notched wheel to cut off the surplus dough to form a beautiful gazelle horn, 
  10. with the help of a Nekkache, to form motives on the whole of the horn, I made them rather discrete but if you wish them more marked be careful not to tear the dough 
  11. Bake at 180 ° for 15 minutes, you must monitor the horns of gazelles that should not brown too much, leaving the oven to cool and ready! 


