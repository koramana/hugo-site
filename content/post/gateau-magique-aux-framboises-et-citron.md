---
title: magic cake with raspberries and lemon
date: '2016-04-03'
categories:
- Not rated @en
- sweet recipes
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-magique-aux-framboises-6.jpg
---
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-magique-aux-framboises-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-magique-aux-framboises-6.jpg>)

##  magic cake with raspberries and lemon 

Hello everybody, 

We love the magic cake at home, because its texture can satisfy everyone, my children who love the flan side, my husband who loves the creamy side, and me who loves everything, hihihihi. 

During my research, I came across the recipe of the magic cake with fruits, besides I made 2 magic cakes, one with blueberries (recipe to come) and this one; a super delicious cake. I should have put more fruit, but I was afraid of the final texture, because these fruits reject a lot of water, surely I will try again with more fruits to see what it will give. 

Because the children are on vacation, I do not take pictures in the light of day, and that's why we can not see the three different textures, but they are there, we really feel them and especially when you eat this fresh cake, the creamy texture is just intensely melting. You really have to try this cake to understand what I'm talking about. 

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-magique-aux-framboises-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-magique-aux-framboises-3.jpg>)   


**magic cake with raspberries and lemon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-magique-aux-framboises-4.jpg)

portions:  10  Prep time:  15 mins  cooking:  50 mins  total:  1 hour 5 mins 

**Ingredients** for a mold 22 cm in diameter 

  * 4 medium eggs at room temperature, 
  * 150 gr of sugar, 
  * 1 C. water, 
  * 125 gr of butter, 
  * 115 grams of flour, 
  * 1 pinch of salt, 
  * 500 ml whole milk, 
  * the zest of 1 lemon organic or untreated, 
  * the juice of 1 half lemon 
  * 125 gr of fresh raspberries. 



**Realization steps**

  1. Melt the butter and let cool. 
  2. Warm the milk and preheat the oven to 150 ° C. 
  3. Separate the yellows and whites. Climb the whites to firm snow with a pinch of salt. Book. 
  4. Beat the egg yolks, sugar and water for a few minutes. Then add the melted butter, the juice of the half lemon, the lemon peel and continue beating. 
  5. Sift on the mixture the flour and mix quickly just to introduce the flour. always to the drummer. Add the warm milk gradually and beat to mix everything well.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pr%C3%A9paration-du-gateau-magique.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/pr%C3%A9paration-du-gateau-magique.jpg>)
  6. gently fold the whites into the dough a few times using a hand whisk. Place the raspberries in the bottom of the pan and pour the mixture on top. (I tried this cake several times, I advise you to pour the dough in the mold (it is super liquid and after adding the fruits, if you place as I did in the photo, the fruit then pour the dough , the fruits will disperse towards the edges of the cake)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-magique-aux-framboise-8.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-magique-aux-framboise-8.jpg>)
  7. use a mold with removable base, because you can not remove the cake from the mold is like a blank, and in addition you can not spill it because you will crush the layer of sponge cake from above. Stitch your removable mold with cooking paper, so that the liquid does not come out from below. 
  8. Bake for about 50 minutes. Check the cooking: the cake must be firm to the touch. If it still seems too liquid, add a few minutes. 



![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-magique-aux-framboises-5.jpg)

and here is the list of participants in this round: 

et voila la liste des marraines de toutes les rondes précedentes: 
