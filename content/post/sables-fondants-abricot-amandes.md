---
title: shortbreads apricot almonds
date: '2014-09-28'
categories:
- Unclassified
tags:
- Algerian cakes
- Aid cake
- Gateau Aid
- Oriental pastry
- Algerian patisserie
- Dry Cake
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sabl%C3%A9s-fondants-abricot-amandes-3.jpg
---
[ ![shortbreads apricot almonds](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sabl%C3%A9s-fondants-abricot-amandes-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sabl%C3%A9s-fondants-abricot-amandes-1.jpg>)

##  shortbreads apricot almonds 

Hello everybody, 

They are beautiful almond biscuits that Lunetoiles had prepared during the feast of Aid el Fitr 2014, and I did not have the chance to post the recipe for this delight. 

I make up for it, to give you the recipe for this delight she found on the forum el Djelfa, a recipe that returns to Oum Amani (this woman is a fairy of Algerian pastry), like that at least you have a recipe for cookies easy to prepare for the holiday of Aid el kbir 2014. 

A beautiful presentation of a shortbread biscuit easy to prepare, garnished with flaked almonds sprinkled on a thin layer of apricot jam, oh what a delight these shortbreads apricot crudely-melting almonds. 

![shortbreads apricot almonds](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sabl%C3%A9s-fondants-abricot-amandes-2.jpg)

**shortbreads apricot almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sabl%C3%A9s-fondants-abricot-amandes-1.jpg)

portions:  35  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 250 g Margarine 
  * 125 g icing sugar 
  * 1 glass of maizena water 
  * 1 egg, 
  * 1 sachet of baking powder 
  * 1 packet of vanilla 
  * flour 

For the decoration: 
  * Flaked almonds roasted 
  * icing sugar 
  * apricot jam 



**Realization steps**

  1. beat the margarine and the sugar with the help of an electric mixer. 
  2. add egg and vanilla, continue to whip 
  3. incorporate cornflour and yeast delicately. 
  4. pick up the dough with flour 
  5. spread the dough using a roll, then just pass with a baking roll with decoration. 
  6. shape squares with a shortbread mold 
  7. place them on a baking tray floured or lined with baking paper 
  8. still shape squares, but this time cut them in half diagonally. 
  9. cook the cakes without browning them, they must remain a light color. 
  10. after cooking and cooling the cakes, cover the face of the complete squares with a thin layer of jam. 
  11. stick over the half square, and decorate the uncovered face with flaked almonds. 
  12. sprinkle with a thin layer of icing sugar. 



[ ![shortbreads apricot almonds](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sabl%C3%A9s-fondants-abricot-amandes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sabl%C3%A9s-fondants-abricot-amandes.jpg>)
