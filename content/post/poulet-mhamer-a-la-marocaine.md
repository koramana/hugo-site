---
title: moroccan chicken mhamed
date: '2018-03-10'
categories:
- Moroccan cuisine
- Cuisine by country
tags:
- dishes
- Ramadan
- Algeria
- Easy recipe
- tajine
- Full Dish
- Olive

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Poulet-mhamer-a-la-marocaine-1.jpg
---
![Chicken-mhamer-a-la-Moroccan-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Poulet-mhamer-a-la-marocaine-1.jpg)

##  moroccan chicken mhamed 

Hello everybody, 

frankly I watched the video and I had only one desire, put my hand in the screen to take a piece, what they are greedy these pieces of roasted chicken (mhamer). 

A few days later I receive this little album of my friend Lunetoiles who also saw the video, and better than me went to the kitchen to make this delicious dish very greedy. 

I share with you the video of sousou kitchen, so you can make the recipe without any problem: 

**moroccan chicken mhamed**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Poulet-mhamer-a-la-marocaine-2.jpg)

portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 8-10 pieces of chicken 
  * 1 gr.c smen, olive oil or butter 
  * half of a candied lemon 
  * 120 gr green olives 
  * 1 p.c thyme 

For the marinade 
  * 1 onion 
  * 3-4 cloves of garlic 
  * 1 gr.c fresh ginger 
  * 2 g coriander fresh 
  * a pinch of saffron 
  * juice of a lemon 
  * 1 p.c salt 
  * 3-4 gr. Olive oil 
  * salt pepper 



**Realization steps** the method is well explained in video 

  1. Prepare the marinade by mixing all the ingredients together. 
  2. marinate each piece of chicken, try to coat it well with marinade 
  3. let the chicken pieces rest for at least two hours in the marinade. 
  4. then fry the chicken pieces in a little olive oil, in a pot with a thick bottom. 
  5. add the smen and lemon confit cut into pieces. 
  6. add the rest of the marinade and cover with water. 
  7. cover and cook until sauce is reduced. 
  8. take the pieces of chicken and place them in a baking tin. 
  9. brown the chicken for a few minutes in the oven. 
  10. put the sauce on the fire, add the olives, thyme, and adjust the taste of the sauce with salt and black pepper. 
  11. let reduce, then present the roasted chicken pieces with the delicious creamy olive sauce. 



![Chicken-mhamer-a-la-Moroccan](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Poulet-mhamer-a-la-marocaine.jpg)
