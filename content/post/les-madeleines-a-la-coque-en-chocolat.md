---
title: madeleines with chocolate shell
date: '2016-01-22'
categories:
- Cupcakes, macaroons, and other pastries
- Chocolate cake
tags:
- Soft
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/12/madeleines-avec-coque-chocolat-1.jpg
---
[ ![madeleines with chocolate shell](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/madeleines-avec-coque-chocolat-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2010/12/madeleines-avec-coque-chocolat-1.jpg>)

##  madeleines with chocolate shell 

Hello everybody, 

After trying several times my recipe for [ Madeleines unbreakable ](<https://www.amourdecuisine.fr/article-les-madeleines-recette-inratable.html>) , why not try this Lunetoiles recipe. I went to see the difference between my recipe and this recipe, and there is the addition of glucose syrup on this recipe, so if you have difficulties to get some, try the recipe for [ unbreakable madeleines ](<https://www.amourdecuisine.fr/article-les-madeleines-recette-inratable.html>) for the same result. 

and now, here is the little story of Lunetoiles: 

Salam aleykum wa rahmatullah wa barakatuh, 

I'm currently in a period where I do a lot of madeleines, I arrived to find the recipe that satisfies me completely with the pretty little bump, they are very bright and extra mellow, a delight. 

I chose to make them simple just with a chocolate back, but you can vary the tastes by adding chocolate chips, or by putting lemon zest or orange, or by marbling ... 

[ ![madeleine with chocolate shell](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/madeleines-avec-coque-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2010/12/madeleines-avec-coque-chocolat.jpg>)   


**madeleines with chocolate shell**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/madeleines-avec-coque-chocolat-2.jpg)

**Ingredients** For about 40 madeleines 

  * 220 g of soft butter or half-salt 
  * 4 eggs 
  * 110 g caster sugar 
  * 110 g of glucose / fructose syrup 
  * 220 g of organic flour 
  * 1 sachet of baking powder (10 g) 
  * 3 sachets of vanilla sugar or the seeds of a vanilla pod for example 

For the chocolate shell: 
  * 300 gr of quality dark chocolate 



**Realization steps**

  1. Start by melting the butter and let it warm. 
  2. In a salad bowl put the eggs, caster sugar, syrup and vanilla sugar, and whip in the hand. 
  3. Add the flour, and the baking powder, whisk. 
  4. Then stir in the cold melted butter and whisk until well incorporated in the butter. 
  5. Put the resulting dough in a pastry bag and put the two hours in the refrigerator. 
  6. After two hours, preheat your oven to 220 ° C. 
  7. Butter the cavities of silicone molds. 
  8. Put them on a perforated plate. 
  9. Using the pastry bag, fill the pastry cavities, stop at 1 cm from the edge. 
  10. Bake for 2 minutes at 220 ° C and lower the thermostat to 160 ° C, and cook for about 10 minutes.   

  11. While the first batch is cooking, put the bag back in the fridge! 
  12. Continue for the other batches: 
  13. Reassemble the oven temperature to 220 ° C, fill the cavities again and bake for 2 minutes at 220 ° C and lower to 160 ° C for 10 minutes. 
  14. Let them cool. 
  15. Melt the chocolate in a bain-marie, put a teaspoon in the cavities of the silicone madeleines molds and replace the madeleines by pressing to distribute the chocolate evenly. 
  16. Refrigerate until chocolate hardens. 
  17. Once the chocolate has hardened, carefully remove the molds and store them in an airtight container. 



[ ![madeleine chocolate shell 1](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/madeleines-avec-coque-chocolat-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2010/12/madeleines-avec-coque-chocolat-3.jpg>)
