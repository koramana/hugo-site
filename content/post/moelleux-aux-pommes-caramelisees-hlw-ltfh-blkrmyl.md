---
title: fluffy with caramelized apples »حلوى التفاح بالكراميل»
date: '2010-05-10'
categories:
- les essais de mes lecteurs

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/05/farine_thumb1.jpg
---
a very nice cake that I had spotted at jaclyne, then find again in the agenda a recipe, full of sweets, we love at home. 3 large apples 3 eggs 125g flour 1 sachet of yeast 125g of sugar 1 sachet of vanilla sugar 3 hazelnuts of butter Orange blossom water. Fry peeled apples, cut into strips, with butter and vanilla sugar. Meanwhile mix, eggs, sugar, 2 tablespoons rum, flour and yeast. Add the caramelized apples, and mix everything together. In a & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

a recipe, full of sweets, that we love at home. 

3 big apples   
3 eggs   
125g of flour   
1 packet of dry yeast   
125g of sugar   
1 sachet of vanilla sugar   
3 hazelnuts butter   
Orange blossom water. 

Fry peeled apples, cut into strips, with butter and vanilla sugar.   
Meanwhile mix, eggs, sugar, 2 tablespoons rum, flour and yeast.   
Add the caramelized apples, and mix everything together.   
In a buttered mold, cook at 130-140 ° C for 40 minutes. At the exit of the oven water the cake with orange blossom water 

good tasting 

do not forget to participate in the game: 

cook the brioche: 

![flour](https://www.amourdecuisine.fr/wp-content/uploads/2010/05/farine_thumb1.jpg)

the rules of the game is [ ici ](<https://www.amourdecuisine.fr/article-briochettes-petites-brioches-aux-raisins-avec-un-jeu-49990012.html>)
