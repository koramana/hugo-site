---
title: hot coffee smoothie-mocha
date: '2017-09-05'
categories:
- juice and cocktail drinks without alcohol
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-chaud-au-caf%C3%A9-1.jpg
---
[ ![hot coffee smoothie 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-chaud-au-caf%C3%A9-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-chaud-au-caf%C3%A9-1.jpg>)

##  hot coffee smoothie-mocha 

Hello everybody, 

Yesterday for my breakfast, I wanted a little coffee, but still in my saga of smoothies. I went to get on google, and found this hot coffee smoothie recipe, mocha smoothie ... 

I really liked the recipe, especially since I had all the ingredients on hand, I really like that, hihihih. 

**hot coffee smoothie-mocha**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-chaud-au-caf%C3%A9-2.jpg)

portions:  2  Prep time:  5 mins  cooking:  1 min  total:  6 mins 

**Ingredients**

  * 15g of chopped dark chocolate (I had a chocolate with hazelnuts, I removed the hazelnuts to use them in this same recipe) 
  * 240 ml of milk (regular milk or almond) 
  * 1 teaspoon of instant coffee, dissolved in 2 tablespoons hot water. 
  * 4 c. tablespoons oat bran 
  * ½ ripe banana 
  * 10 hazelnuts 
  * 1 C. coffee chia seeds 
  * 2 tablespoons cold water 



**Realization steps**

  1. place the dark chocolate in a microwave-safe bowl 
  2. pour over the milk and coffee. 
  3. put in the microwave until the mixture is hot and the chocolate has melted 
  4. Place the oat bran, hazelnuts, and chia seeds in the blinder and shield to get a fine powder. 
  5. add the pieces of bananas, and the milk to shield if you feel that you need more milk, add according to your taste. 
  6. Now add the hot chocolate mixture gently while checking the heat resistance of your blinder. 
  7. Mix very well and check the consistency for your smoothie. 
  8. Enjoy it right away, and treat yourself. 



[ ![hot coffee smoothie 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-chaud-au-caf%C3%A9-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-chaud-au-caf%C3%A9-3.jpg>)
