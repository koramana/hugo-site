---
title: Tango waffles
date: '2011-04-18'
categories:
- gateaux algeriens au miel
- Algerian cakes- oriental- modern- fete aid
- sweet recipes

---
Hello everyone, 

So, I give you his recipe: 

ingredients: 

  * 300g of flour 
  * 3 big eggs 
  * 1 salt salt 
  * 75g of butter 
  * 1/2 sachet of yeast 
  * 150 ml milk 
  * 150 ml water or sparkling water (Orange tango) 
  * 60 g of sugar 
  * 2 vanilla sachets 
  * 2 c a s flower water 



start by mixing the dry ingredients, and also mix the liquid ingredients, then mix everything together to have a liquid that flows a little thick (not too much) and is cooked in a waffle iron. 

Thank you for your visit 

have a good day 
