---
title: mushroom with peanuts
date: '2016-03-22'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Algerian cakes
- Algeria
- Cake
- Genoese
- Chocolate cake
- Pastry
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/mouskoutchou-aux-cacahuetes-1.jpg
---
[ ![mushroom with peanuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/mouskoutchou-aux-cacahuetes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/mouskoutchou-aux-cacahuetes-1.jpg>)

##  mushroom with peanuts 

Hello everybody, 

Another mouskoutchou that shares with us Lunetoiles. It should be expected, she likes the mouskoutchou, moreover I pass you there the link of a beautiful selection of mouskoutchou recipes: [ marbled mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-marbre.html>) , [ honey muskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-au-miel.html>) , [ chocolate mouskoutchou ](<https://www.amourdecuisine.fr/article-le-mouskoutchou-au-chocolat.html>) , [ muskoutchou with orange ](<https://www.amourdecuisine.fr/article-minis-mouskoutchou-a-l-orange.html>) , [ speckled mouskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouskoutchou-mouchete.html>) , and of course the [ nature's muskoutchou ](<https://www.amourdecuisine.fr/article-mouskoutchou-meskouta.html>) . 

For the little story of this peanut mushroom, here is what Lunetoiles tells us: 

> The other time I was talking to my mom about cooking, and she told me that she was preparing the coconut muskoutchou, she was putting on top of it too, so that the coconut was cooking and grilling to give a good flavor to the mouskoutchou. 
> 
> I imagined that! 
> 
> And then she also told me that she also made the peanut mouskoutchou, so I immediately started thinking about it seriously. There are still other recipes she told me ... But me I had only one desire, it is to taste with teeth in this peanut mouskoutchou, so here is the recipe: 

**mushroom with peanuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/mouskoutchou-aux-cacahuetes-2.jpg)

portions:  8  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * 4 eggs 
  * 130 g of sugar 
  * 130 g of glucose / fructose syrup 
  * 1 tbsp. powdered vanilla 
  * 260 ml of fermented milk (buttermilk) 
  * 200 ml of neutral oil 
  * 1 sachet of baking powder 
  * 330 g flour 
  * 2 glass of whole peanuts without skin 
  * 2 tbsp. tablespoons sugar 

decoration: 
  * 40 gr of melted dark chocolate 
  * darling (caramelized peanuts) roughly crushed 



**Realization steps**

  1. Preheat the oven to 160 ° C 
  2. Roast the peanuts in a hot oven on a baking tray for about 15 minutes, they should be golden brown. 
  3. Let them cool before reducing them to powder. Book. 
  4. Increase the oven to 180 ° C. Butter a savarin dish and flour well. 
  5. Beat the eggs with the 130 gr of sugar and vanilla until the sugar is dissolved. 
  6. Add the glucose syrup and mix. 
  7. Add the fermented milk and the oil and mix well. 
  8. Stir in yeast and sifted flour and whisk until smooth and evenly prepared.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pr%C3%A9paration-du-mouskoutchou-aux-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/pr%C3%A9paration-du-mouskoutchou-aux-cacahuetes.jpg>)
  9. Pour half of the dough into another bowl, add the powder of roasted peanuts and add the 2 tbsp. sugar and mix well.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/mouskoutchou-aux-cacahuetes-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/mouskoutchou-aux-cacahuetes-5.jpg>)
  10. Pour the two dough alternately into the buttered and floured mold.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/marbrage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/marbrage.jpg>)
  11. Bake for about 20 minutes at 180 ° C then lower the temperature to 160 ° C and continue cooking for 30 minutes. 
  12. The cake should be well inflated and golden. 
  13. Monitor the cooking, check if the cake is cooked by inserting a toothpick, if it comes out clean, it's cooked. 
  14. Let it cool on a rack. 
  15. Then using a pastry bag or a cone make zigzag dark chocolate melted on the mouskoutchou and sprinkle with crushed caramelized peanuts (scrunchies). 



[ ![peanut mushroom](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/mouscoutchou-aux-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/mouscoutchou-aux-cacahuetes.jpg>)
