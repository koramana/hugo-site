---
title: mini mushroom with orange
date: '2013-06-06'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/mini-mouskoutchou-a-l-orange-6_thumb_11.jpg
---
##  mini mushroom with orange 

Hello everybody, 

back to the recipes of lunetoiles, and here are sublime small cakes, minis mouskoutchou with orange, that it must be light and well scented, huuuuuum 

so I share with you the recipe for lunetoiles:   


**mini mushroom with orange**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/mini-mouskoutchou-a-l-orange-6_thumb_11.jpg)

portions:  6  Prep time:  10 mins  cooking:  35 mins  total:  45 mins 

**Ingredients**

  * 3 eggs 
  * 1 glass of sugar 
  * ½ glass of oil 
  * ½ glass of whole milk 
  * 180 g flour mixed with a bag of baking powder 
  * zest of an orange 
  * 100 gr of candied orange (keep a handle for the decoration) 

Decoration: 
  * icing sugar 
  * candied orange (a handful) 



**Realization steps**

  1. In a bowl, put the eggs and sugar and beat until the mixture whitens and has doubled in size. 
  2. Add the oil and beat. Add the milk. 
  3. Add the flour / yeast mix and beat until smooth and smooth. Add candied orange and orange zest. 
  4. Butter and lightly flour the mussels. 
  5. Pour into ⅔ of the mold. 
  6. Bake in th.5 or 150 ° C oven. 
  7. Check the cooking with a toothpick, if it comes out clean, they are cooked. 
  8. Remove from oven, let cool. Sprinkle with icing sugar and decorate with the candied oranges. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
