---
title: Bavarian Explosion of Flavors / Bavarian Pineapple, Chocolate and Mango
date: '2014-01-28'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/2014-01-25-11.38.40.jpg
---
[ ![chocolate pineapple bavarois](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/2014-01-25-11.38.40.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/2014-01-25-11.38.40.jpg>)

Hello everyone, 

I propose today a dessert extremely good and easy to achieve. 

Hello OumBil, thank you for sharing and it's beautiful photos, I am really charmed by the recipe, thank you for sharing it with us. 

OumBil used the form: [ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette>) So do not forget too, you can suggest your recipe, using this form.   


**Bavarian explosion of flavors**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/2014-01-25-11.56.13.jpg)

Recipe type:  Dessert  portions:  4  Prep time:  40 mins  cooking:  5 mins  total:  45 mins 

**Ingredients** the base : 

  * shortbread biscuits or speculoos 
  * 40g of melted butter. 

Pineapple mousse: 
  * 1 can of liquid cream 30% of 20cl. 
  * 2 tablespoons mascarpone. 
  * pineapple in box 
  * 25 g of sugar 
  * 1 leaf and a half of gelatin 

Chocolate mousse : 
  * 150 g of chocolate (to choose) 
  * ½ can of liquid cream (10cl) 
  * 1 tablespoon mascarpone. 

Mashed mango passion. 

**Realization steps** Prepare the base: 

  1. Blend the shortbread biscuits with the melted butter and place in individual molds.   
(For my part I have plastic molds as shown in the photo).   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/2014-01-25-11.41.54.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/2014-01-25-11.41.54.jpg>)
  2. For the pineapple mousse: 
  3. Purée the pineapple in a blender 
  4. Heat the puree with the sugar, remove at the first boil 
  5. Out of the fire add the gelatin previously softened in cold water. 
  6. Let cool. 
  7. Mix the liquid cream with the mascarpone then add whipped cream. 
  8. Add the sugar little by little. 
  9. Stir in the mashed potatoes gently and reserve in the fridge. 

prepare the chocolate mousse: 
  1. Melt the chocolate in a bain-marie. 
  2. Add whipped cream with mascarpone and sugar. 
  3. Stir in the chocolate (not too hot or too cold) delicately with the whipped cream. 

Mounting : 
  1. Arrange the pineapple mousse on the shortbread base and spread a thin layer of passion mango puree, then the chocolate mousse and freeze. 
  2. Prepare the chocolate plates for the top: 
  3. Melt 150 g chocolate, spread a thin layer on a special chocolate plate or on baking paper, then let cool, cut into the desired shape with a hot knife to avoid breaking the chocolate. 



Very good tasting. 

[ ![2014-01-25 11.56.13](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/2014-01-25-11.56.13.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/2014-01-25-11.56.13.jpg>)

Again, thank you my dear Oumbil for the recipe, and next time maybe, I will not say no. 

thank you: 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
