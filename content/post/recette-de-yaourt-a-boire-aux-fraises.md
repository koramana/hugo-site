---
title: strawberry yoghurt recipe
date: '2017-04-24'
categories:
- juice and cocktail drinks without alcohol
- recettes sucrees
tags:
- Ramadan
- Juice
- Drinks
- Healthy cuisine
- Easy cooking
- Breakfast
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/04/yaourt-%C3%A0-boire-aux-fraises-11.jpg
---
![strawberry yogurt drink 11](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/yaourt-%C3%A0-boire-aux-fraises-11.jpg)

##  strawberry yoghurt recipe 

Hello everybody, 

I really like to prepare this drink at home, because I think it's more economical, and I know what's in the drink I give to my children. This yoghurt drink is super refreshing, too good and rich in nutrients ... Importantly, this drink can be kept 3 days cool. 

**strawberry yoghurt recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/yaourt-%C3%A0-boire-aux-fraises.jpg)

**Ingredients**

  * 500 ml whole milk 
  * 115 gr of natural yoghurt 
  * 2 tbsp. honey or more depending on the taste 
  * 150 gr of strawberries in pieces (you can use frozen strawberries) 



**Realization steps**

  1. place all the ingredients in the blender bowl 
  2. mix well until you have a smooth drink 
  3. Keep chilled until serving. 



Note 1- shake the bottle a little before consuming the yoghurt to drink   
2- this drink can be kept cool for 3 days. ![Strawberry yogurt drink 12](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/yaourt-%C3%A0-boire-aux-fraises-12.jpg)
