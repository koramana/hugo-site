---
title: Brussels sprouts and rotten butternut squash
date: '2015-12-13'
categories:
- salads, salty verrines
tags:
- inputs
- Aperitif
- aperitif
- Easy cooking
- Medley
- Fast Food
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-de-bruxelles-avec-courge-musqu%C3%A9e-et-noix-de-p%C3%A9can.jpg
---
[ ![Brussels sprouts with butternut squash and pecan](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-de-bruxelles-avec-courge-musqu%C3%A9e-et-noix-de-p%C3%A9can.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-de-bruxelles-avec-courge-musqu%C3%A9e-et-noix-de-p%C3%A9can.jpg>)

##  Brussels sprouts and rotten butternut squash 

Hello everybody, 

One thing I like about my children's school canteen is that my children have learned to eat vegetables without too much complication. I must admit that I was surprised when we went out to shop with my children, my eldest son asks me a very strange question: Mum why you do not cook for us brussel sprouts ???? (Mom why you do not do for us Brussels sprouts ???) and my daughter joins him !!! 

Astonished, well I was more than surprised, I told them: but I'm afraid you're eating it ... and they tell me, but we eat it often at school and we like it a lot ... Well, it's a first for me, I immediately bought a package ... Before they change their minds, hihihih. 

Having never cooked brussels sprouts before, I preferred that they tell me how they used to eat them ... I had a little idea of ​​the recipe, but I still made a new little research on the net to have this recipe, which apparently and according to their say, it is not going to be the last that I will prepared this delicious entry. 

**Brussels sprouts and grilled butternut squash**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-de-bruxelles-avec-courge-musqu%C3%A9e-et-noix-de-p%C3%A9can-1.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** Roasted Brussels Sprouts: 

  * 2 cups Brussels sprouts, hulled, yellow leaves removed 
  * 2 tablespoons olive oil 
  * Salt, to taste 

Butternut squash: (butternut) 
  * 1 cup butternut squash, peeled, seeded and cubed 
  * 1 C. tablespoon of olive oil 
  * 2 tbsp. tablespoon maple syrup 
  * ½ c. ground cinnamon 

Other ingredients: 
  * 1 cup pecan halves 
  * ½ cup dried cranberries 
  * 2 to 3 tbsp. maple syrup (optional) 



**Realization steps** Roasted Brussels Sprouts: 

  1. Preheat the oven to 180 degrees C. 
  2. Lightly grease the baking sheet with 1 tablespoon of olive oil. 
  3. cut Brussels sprouts in half. In a medium bowl, place Brussels sprouts 2 tablespoons olive oil, salt (to taste), and mix. 
  4. put on the plate cut side down, and bake for about 20-25 minutes. During the last 5-10 minutes, return them for even cooking. 

roasted butternut squash 
  1. In the same preheated oven at 180 degrees C. Lightly grease the baking sheet with 1 tablespoon of olive oil. 
  2. In a medium bowl, mix cubed butternut squash (peeled and seeded), 1 tablespoon olive oil, maple syrup and cinnamon and mix well. 
  3. Place butternut squash in a single layer on the baking sheet. Bake for 20-25 minutes, turning once halfway until softened. 

presentation:   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-de-bruxelles.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/choux-de-bruxelles.jpg>)

  1. In a large bowl, combine roasted Brussels sprouts, butternut squash, pecans and cranberries. 

(Optional): For extra sweetness, add 2 or 3 tablespoons of maple syrup, if desired 
  1. do not add all the maple syrup at once, start with 1 tablespoon, then add, if desired, and mix with the ingredients of the salad to combine. 


