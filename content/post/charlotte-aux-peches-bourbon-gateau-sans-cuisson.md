---
title: Charlotte with bourbon peach (cake without cooking)
date: '2011-02-16'
categories:
- cuisine diverse
- fish and seafood recipes
- riz

---
hello everyone, today I bring you back one of my recipes classified in the oubliettes, it's my first charlotte, I still like cakes without cooking, cakes that can be done in two minutes, so I share with you this little delice very fondant. my first charlotte, and it was delicious, the kids did not even wait for the custard to take. then for the ingredients: 1 pack of boudoir pastry cream recipe: 300 ml of milk 4 to 5 cases of sugar 30 gr of cornstarch 2 eggs zest of lemon a few cupcakes (Bourbon & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

today I bring you one of my recipes classified in the oubliettes, it's my first charlotte, I still like cakes without cooking, cakes that can be made in two minutes, so I share with you this little delice very melting. 

my first charlotte, and it was delicious, the kids did not even wait for the custard to take. 

then for the ingredients: 

  * 1 boudoir package 
  * pastry cream recipe: 
  *     * 300 ml of milk 
    * 4 to 5 cases of sugar 
    * 30 gr of cornflour 
    * 2 eggs 
    * lemon zest 
  * some cupcakes (Bourbon for me) 
  * 1 box of peach in his syrup 
  * some strawberries for garnish 
  * and whipped cream. 



drain the peaches, and prepare a syrup a little thick with the juice, to which you add a little sugar (according to your taste) 

prepare the pastry cream, 

In a bowl, mix eggs, sugar and lemon zest. Add the cornstarch and mix well. Boil the milk and pour over the egg / sugar / cornflour mixture. Then put back on low heat and allow to thicken 

in a salad bowl, start by setting up the boudoirs that you have deceived in the syrup of the peach juice. 

When your bowl is well garnished, pour in half of the pastry cream, cover with a nice layer of diced peach, or half moon, cover everything with bourbon, you also have a little trick in the syrup cover with pastry cream, then peach, and finally cover well with the rest of the boudoir, 

and place everything in the refrigerator, to take it well. 

merci pour vos visites 
