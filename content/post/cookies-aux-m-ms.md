---
title: M & M'S cookies
date: '2015-08-12'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours
- sweet recipes
tags:
- Cookies
- United States
- Algerian cakes
- To taste
- cracked
- biscuits
- Chocolate

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/biscuits-aux-MMS-1.jpg
---
[ ![Soft cookies at m & ms](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/biscuits-aux-MMS-1.jpg) ](<https://www.amourdecuisine.fr/article-cookies-moelleux-aux-mms.html/sony-dsc-252>)

##  M & M'S cookies 

Hello everybody, 

Cookies that will surely please your little bit of cabbage. These extra fluffy cookies at M & M's are besides being beautiful, too good, super delicious and crunchy-soft on the palate. 

Un sublime goûter pour accueillir vos enfants a leurs retour de école, et ne vous en faites pas si après quelques minutes, il n’en restera rien. Alors je vous conseille, goûtez ces cookies avant le retour des enfants de lécole et ne dites pas que je ne vous ai pas prévenu 😉 lol. 

A recipe for cookies at the m & m's that is generously shared with us, our dear Lunetoiles. The quantities of ingredients will give almost 18 cookies, so treat yourself! 

[ ![M & M'S cookies](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cookies-aux-mms.jpg) ](<https://www.amourdecuisine.fr/article-cookies-moelleux-aux-mms.html/sony-dsc-255>)   


**M & M'S cookies **

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/cookies-aux-mms-1.jpg)

portions:  18  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 80 g of a mixture of walnuts, hazelnuts, ground almonds 
  * 50 g of brown sugar 
  * 65 g of butter 
  * 65 g of flour 
  * M  & M's 



**Realization steps**

  1. In a salad bowl, work the butter in ointment. 
  2. Add the sugar and mix. 
  3. Add the almond powder and mix. 
  4. Add the flour and a pinch of salt. 
  5. Mix with a spoon. 
  6. Form a ball of dough. Take pieces of dough the size of a large walnut. Make balls that will flatten you like a cookie. Place the cookies on a baking sheet covered with baking paper. 
  7. Fucking M  & M's, 3 or 4 by cookies. According to your taste. 
  8. Bake the cookies for 15 to 20 minutes in an oven preheated to 180 ° C. 



[ ![Soft cookies at m & ms](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/biscuits-aux-mms.jpg) ](<https://www.amourdecuisine.fr/article-cookies-moelleux-aux-mms.html/sony-dsc-253>)
