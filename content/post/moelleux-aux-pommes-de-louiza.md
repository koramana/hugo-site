---
title: mellow with Louiza apples
date: '2009-03-13'
categories:
- dessert, crumbles and bars
- panna cotta, flan, and yoghurt
- ramadan recipe

---
a mellow of my dear Louiza as she wrote it for you (thank you my angel) Here is a cake very soft and melting, its texture is wet with a very delicate taste. the contrast of flaked almonds and the fondant of apples flatters the taste buds. In a word, it is very light and ideal for children's snacks. without further ado here is the recipe: & nbsp; this is the slightly modified yogurt cake recipe: 4 eggs 1 jar of plain yogurt 2 jars of sugar 1/2 jar of sunflower oil 2 jars of flour 1 jar of maizena 1 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

a mellow of my dear Louiza as she wrote it for you (thank you my angel) 

Here is a cake very soft and melting, its texture is wet with a very delicate taste. the contrast of flaked almonds and the fondant of apples flatters the taste buds. In a word, it is very light and ideal for children's snacks. without further ado here is the recipe: 

this is the slightly modified yogurt cake recipe: 

4 eggs 

1 jar of plain yoghurt 

2 jars of sugar 

1/2 pot of sunflower oil 

2 pots of flour 

1 jar of maizena 

1 sachet of baking powder 

2 tbsp aroma of bitter almonds 

2 large apples, peeled and diced 

Tapered almonds 

In a bowl beat the eggs and sugar until the mixture doubles in volume. Add the oil, mix. then add the yogurt and the aroma. Finally the maizena and sifted flour with the baking powder mix gently. At this stage add the apples and coat well with the dough. pour into a silicone mold 24 cm in diameter, otherwise in a mold of your choice buttered and floured. Put on the surface of the flaked almonds. Bake in the preheated oven for 35 to 45 minutes at 180 °, depending on your oven. After cooking, let cool a little and unmold on a rack and let cool. 

Look at me this part! is not it a real treat! at your furnaces, you will tell me news. 

Je vous souhaite bonne degustations. 
