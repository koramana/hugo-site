---
title: Cheesecake strawberry smoothie
date: '2014-05-05'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-fraise-cheesecake-2.jpg
---
[ ![Cheesecake strawberry smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-fraise-cheesecake-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-fraise-cheesecake-2.jpg>)

##  Smoothie cheesecake strawberries. 

Hello everybody, 

Cheesecake Strawberry Smoothie: Another fresh and rich drink recipe that comes from our beloved Lunetoiles, I believe it is in smoothie mood, so let's enjoy its delicacies. 

This time it's a cheesecake mix everything and blinder, present in glasses in smoothie version, I like the idea, and that's what I like in smoothies, it is that we leave our imagination do your magic trick. 

What is also good with smoothies is that we do not need fresh fruit, on the contrary it is better to use frozen fruits, to have a very fresh and refreshing drink, which is prepared in one wink, that can be well presented at parties and parties, or as a small healthy snack for large and small. 

The most difficult part of this preparation is to remember to take these fruits out of the freezer for at least 2 minutes before starting the preparation. Since it is better to prepare it for direct presentation, I prefer to take the smoothies while they are still sparkling, and well ventilated, if it rests in the fridge, the ingredients may separate, to make a decantation, and the liquids come back up, which will make the smoothie lose its flavor. 

Before posting this recipe, I wanted to apologize to my readers and blogger friends who left me comments and I have not had time to answer them yet. Do not worry, my dear, I'll do that, slowly but surely, lol. 

So let's go back to the recipe for this cheesecake strawberry smoothie:   


**Cheesecake strawberry smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothie-fraises-cheesecake-1-290x195.jpg)

portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 2 tablespoons of PHILADELPHIA cream cheese 
  * 1 lean natural yoghurt 
  * 375 ml of milk 
  * 3 tablespoons sugar (or agave syrup, to taste) 
  * 1 + ½ teaspoons of liquid vanilla 
  * 6 cookies 
  * 2 cups frozen strawberries, slightly thawed 

To serve : 
  * 1 biscuit 
  * plain lean yoghurt 



**Realization steps**

  1. In a blender put the cream cheese, milk, 3 tbsp. sugar, vanilla and 6 biscuits and blend until smooth.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/STRAWBERRY-CHEESECAKE-SMOOTHIES-300x201.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/STRAWBERRY-CHEESECAKE-SMOOTHIES-300x201.jpg>)
  2. Add the strawberries to the blender and blend until smooth. 
  3. Pour into glasses. 
  4. Put yogurt gently on strawberry smoothies in glasses. 
  5. Reduce 1 crumble biscuit; sprinkle over the smoothies. 
  6. Serve immediately. 



[ my recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous>)

And if like Lunetoiles, you want to share one of your recipes, you can [ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
