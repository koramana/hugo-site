---
title: sliced ​​turnovers with minced meat
date: '2015-12-12'
categories:
- Bourek, brick, samoussa, slippers
tags:
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chaussons-feuillet%C3%A9s-a-la-viande-hach%C3%A9e.jpg
---
[ ![sliced ​​turnovers with minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chaussons-feuillet%C3%A9s-a-la-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chaussons-feuillet%C3%A9s-a-la-viande-hach%C3%A9e.jpg>)

##  sliced ​​turnovers with minced meat 

Hello everybody, 

Sometimes we want to feast without much time for hours to concoct an entry or any recipe. 

This is the case with this express recipe of puff pastry, you can of course put the stuffing of your choice, but today you will have the recipe for sliced ​​lamb slices.   


**sliced ​​turnovers with minced meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chaussons-feuillet%C3%A9s-a-la-viande-hach%C3%A9e.jpg)

portions:  6  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * puff pastry. 

prank call: 
  * minced meat 
  * sliced ​​onion 
  * salt and black pepper 
  * cheese and olives 
  * gilding: 
  * 1 beaten egg 



**Realization steps**

  1. fry the onion in a little oil. 
  2. Add the minced meat and fry until cooked 
  3. add salt and pepper. and leave aside. 
  4. preheated the oven to 180 degrees C 
  5. flour the worktop and spread the puff pastry with a roller. 
  6. cut pucks with a cookie cutter   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chaussons-feuillet%C3%A9s-a-la-viande-hach%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chaussons-feuillet%C3%A9s-a-la-viande-hach%C3%A9e-1.jpg>)
  7. Put the stuffing: minced meat, cheese and olives.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chaussons-feuillet%C3%A9s-a-la-viande-hach%C3%A9e-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/chaussons-feuillet%C3%A9s-a-la-viande-hach%C3%A9e-2.jpg>)
  8. close the slippers. 
  9. brush with the egg and cook until the slippers turn to a beautiful golden color. 


