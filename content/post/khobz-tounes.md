---
title: khobz tounes
date: '2018-04-09'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/khobz-tounes-1.jpg
---
#  [ ![khobz tounes 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/khobz-tounes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/khobz-tounes-1.jpg>)

##  Khobz tounes 

Hello everybody, 

at the request of one of my readers, who wanted to know if the [ besboussa breadcrumbs ](<https://www.amourdecuisine.fr/article-besboussa-gateau-algerien-108816163.html>) is khobz tounes, I rewrite this recipe which was well ranked in the archives of my blog the authentic recipe of khobz tounes or khobz el bey almonds. Just for information, many people think that this recipe is named after the city of Tounes in Tunisia and think that this recipe is of Tunisian origin, in fact the recipe is named after an Algerian woman named Tounes , who made this recipe, which becomes popular afterwards. 

A mellow and fluffy cake, soaked in syrup perfumed with orange blossom water, and much enjoyed in this Ramadan, with a good mint tea. A recipe super easy to make, I even made a roasted peanut version, and it was a treat. 

الوصفة بالعربية 

and here I pass you another version of khoubz tounes or khobz el bey with almonds in video: 

{{< youtube mPg0zfvIKfw >}} 

**khobz tounes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/khobz-tounes-2-300x200.jpg)

portions:  10  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients** (250 ml bowl for a mold 30 cm in diameter) 

  * 1 bowl of almonds, sliced ​​and roughly chopped 
  * 1 bowl of eggs (between 5 and 6) 
  * ¾ bowl of sugar 
  * ½ bowl of butter or even good quality oil 
  * 2 bowls of bread crumbs 
  * 1 sachet of baking powder 
  * zest of lemon and vanilla 
  * 3 good spoons of orange blossom water 

Syrup: 
  * 2 glasses of water 
  * 1 glass of sugar 
  * 2 tablespoons of orange blossom water 



**Realization steps**

  1. mix all the ingredients of the dough, 
  2. pour the dough in a mold of almost 30 cm in diameter, otherwise in individual molds 
  3. cook in a preheated oven at 200 degrees C until the cake turns a nice color. 
  4. during cooking, prepare the syrup, boil the sugar with water for almost 20 minutes, at the end of cooking, add orange blossom water 
  5. at the end of the oven cake, cut edges or lozenges and sprinkle with syrup. 
  6. Decorate according to your means and your taste. 



[ ![picture](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/image.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/image.jpg>)

et bonne dégustation. 
