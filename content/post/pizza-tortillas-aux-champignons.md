---
title: Pizza mushroom tortillas
date: '2013-05-10'
categories:
- dessert, crumbles and bars
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pizza-de-tortilla-aux-champignons-010.CR2_1.jpg
---
![pizza-de-tortilla-the-mushroom-010.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pizza-de-tortilla-aux-champignons-010.CR2_1.jpg)

##  Pizza mushroom tortillas 

Hello everybody, 

Easier than that, there is no, hihihihi ... .., well apart from preparing [ flour tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine-106300512.html>) For my part I had prepared the tortillas two days before for another recipe, there remained 3 ideal for this Pizza mushroom tortillas. 

And since it was home cleaning day, it was almost noon, and I had not done anything yet, I went to see in my fridge, and I found only fresh mushrooms, a meatball. mozarella, and just a little red cheese. 

At first, I thought of sautéed mushrooms, which I personally like a lot, then I remembered a pizza with a tortilla base that I saw on fatafeat tv ....   


**Pizza mushroom tortillas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pizza-de-tortilla-aux-champignons-008.CR2_.jpg)

**Ingredients**

  * flour tortillas 
  * olive oil 
  * mushrooms, cleaned and sliced 
  * crushed garlic 
  * salt and black pepper 
  * Mozzarella 
  * Emmental 
  * oregano 



**Realization steps**

  1. method of preparation: 
  2. Preheat the oven to 180 ° C 
  3. place the tortillas one by one if they are large, on a mold lined with baking paper. 
  4. coat them with a little oil, and cook just to have a soft golden color. 
  5. Heat the oil in a large skillet. Add the mushrooms, garlic, salt and pepper and cook until it changes color. 
  6. garnish the tortillas with these sautéed mushrooms, 
  7. add over slices of mozzarella, and a little grated emmental, or a lot depending on your taste. 
  8. sprinkle with oregano, and bake for 10 minutes, until the cheese melts. 
  9. serve directly with a good salad, 



![pizza-de-tortilla-the-mushroom-005.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pizza-de-tortilla-aux-champignons-005.CR2_1.jpg)

Pour ma part je n’ai même pas eu le temps de prendre des photos, tellement à la maison ils ont beaucoup aimé, ces pizza bien chaudes. 
