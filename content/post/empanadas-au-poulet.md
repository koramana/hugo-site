---
title: chicken empanadas
date: '2016-05-20'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- Algerian cuisine
tags:
- inputs
- Algeria
- la France
- Easy cooking
- Fast Food
- Ramadan 2016
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet_.jpg
---
[ ![empanadas with chicken_](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet_.jpg>)

##  chicken empanadas 

Hello everybody, 

I shared with you yesterday the recipe of my [ paste for empanadas or slippers ](<https://www.amourdecuisine.fr/article-pate-empanadas-chaussons.html>) , the dough I use all the time to make my slippers and small appetizers that we like a lot at home. 

I think once you have the right recipe for the pasta, the stuffing becomes child's play. Sometimes for a joke we use just what we have at home. As for these chicken empanadas, or as they are called in Algeria chicken slippers, I just used what came to hand. I added a touch of curry, I really like the flavor of this spice with chicken and especially in a tomato sauce, it's just perfect. 

Sometimes by fégniantise, I make these chicken slippers with commercial puff pastry, but to be honest, I prefer them with this paste that melts directly in the mouth without crumbling to have more crumbs on the floor than in the mouth as with the puff pastry. 

**chicken empanadas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet-7.jpg)

portions:  6  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * [ paste for empanada ](<https://www.amourdecuisine.fr/article-pate-empanadas-chaussons.html>) ( click on the link) 
  * 1 chicken breast cut into pieces 
  * 4 to 5 fresh tomatoes 
  * ½ onion 
  * some branches of thyme 
  * 1 small clove of garlic 
  * salt and black pepper 
  * ¼ teaspoon curry powder. 
  * 2 tablespoons of olive oil 
  * 1 handful of green pitted olives cut into slices. 
  * 1 egg white to paste the dough 
  * 1 egg yolk for gilding 
  * thyme, or seeds of nigelle to decorate. 



**Realization steps**

  1. fry chicken cubes in oil until golden brown, remove and set aside. 
  2. in the same oil fry the chopped onion and garlic to obtain a translucent color. 
  3. Add the garlic, thyme and chopped tomato, then season with salt, black pepper and curry, cook a little and add the chicken cubes to cook in the sauce, until the sauce is reduced. 
  4. let the sauce cool down, then add the green olives. 
  5. take the dough again, cut it in half and spread it in a layer of almost 4 mm thick. 
  6. cut slices of almost 8.5 cm. 
  7. place each washer in a slipper mold and fill with stuffing   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet-1.jpg>)
  8. brush the round of the dough with a little egg white, and stuff the stuffing by pressing well.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet-2.jpg>)
  9. Continue until the dough is used up (if dough is difficult to handle, add a little water, a little bit of water while tearing the dough, let stand and resume shaping. 
  10. place the empanadas as you go on a baking dish. 
  11. brush the top with egg yolk and decorate with a little dried thyme or black seed. 
  12. cook in a preheated oven at 180 ° C for 10 to 15 minutes, until you have a nice color. 
  13. enjoy with a good soup, or just a very fresh salad. 



[ ![chicken empanadas 6](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/empanadas-au-poulet-6.jpg>)
