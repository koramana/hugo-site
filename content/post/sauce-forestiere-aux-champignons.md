---
title: mushroom mushroom sauce
date: '2018-01-29'
categories:
- diverse cuisine
- Cuisine by country
- dips and sauces
tags:
- White sauce
- Mushrooms
- creams
- Chicken
- accompaniment
- Meat
- Holidays

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/12/sauce-foresti%C3%A8re.jpg
---
![Ranger Sauce](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/sauce-foresti%C3%A8re.jpg)

##  mushroom mushroom sauce 

Hello everybody, 

This is a sauce that I do not often to be frank, but that does not mean that I do not like, on the contrary, I love the forest mushroom sauce, as I like many other sauce to accompany my little lunchtime dishes, like here I made this mushroom forest sauce to accompany chicken steaks ... A delight. 

I come back to my forest mushroom sauce, which I did to go fast and shoot even faster, just to taste my chicken steaks, because I was really hungry, I who arrived at noon I thought I had my [ velvety chestnut ](<https://www.amourdecuisine.fr/article-soupe-de-chataigne.html>) that I had prepared 2 days before and I tasted at noon to warm me by the cold ... But here I think my husband to throw what was left, hihihihih.   


**mushroom mushroom sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/sauce-foresti%C3%A8re-aux-champignons-1.jpg)

portions:  2  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 2 cutlets 
  * 150 ml chicken broth 
  * 100 g mushrooms from paris 
  * 1 shallot 
  * ½ c. mustard 
  * salt and black pepper 
  * 100 ml liquid cream 
  * Butter 



**Realization steps**

  1. Heat the butter in a pan. Fry the salted and peppered cutlets on both sides until golden brown. 
  2. Remove the cutlets and set aside. 
  3. Add the shallot to the pan and sauté for a few minutes. 
  4. Deglaze with 50 ml chicken broth. 
  5. Add the mushrooms rinsed and finely chopped. Fry for a few minutes until they become tender. 
  6. Add the remaining broth and let reduce by half. 
  7. Add the cream and rub the bottom of the pan to remove the juice. 
  8. Put the chicken cutlets back in the pan and let the sauce reduce slowly. 
  9. Finally baking add the spoon of mustard and mix well. 
  10. add a little black pepper from the mill and season the seasoning. 
  11. Serve scallops with mushroom mushroom sauce. 



![sauce-forestry-to-mushroom-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/sauce-foresti%C3%A8re-aux-champignons-2.jpg)
