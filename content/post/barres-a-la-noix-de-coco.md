---
title: coconut bars
date: '2012-12-17'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/barre-confiture-noix-de-coco-027-a_2.jpg
---
##  coconut bars 

Hello company, 

Here are some very delicious **coconut bars** , specifically coconut jam, a very easy recipe that I found on an English magazine, and that I do not hesitate to do, plus the children have a great love, and it has me really happy. 

You can also see the [ strawberry bars ](<https://www.amourdecuisine.fr/article-barres-aux-fraises---crumble-aux-fraises-103847602.html>) from Luentoiles. 

the only stupidity I made, and that I did not cool well before unmolding, and as it is a shortbread, it crumbled a lot, so I did not succeed in the shape of the bars.   
ingredients: for a mold of 20 by 20 cm 

  * 220 gr of flour. 
  * 200 gr of butter. 
  * 1 tablespoon of sugar 
  * 1 egg. 
  * 1 tablespoon of milk. 
  * 1 cup of baking powder. 
  * pinch of salt 
  * 200 gr of apricot jam 



for garnish: 

  * 1 egg 
  * 80 gr of sugar 
  * 20 gr of melted butter 
  * 120 coconut 



method of preparation: 

  1. Heat the oven to 170 degrees 
  2. in the bowl of a blender, mix the butter into pieces, flour, salt, sugar and baking powder. 
  3. beat the egg with the milk and add to the previous mixture, then shield again. 
  4. pour this mixture into your mold lined with baking paper, and spread the dough over the entire surface, it does not run the dough, so you must leave it well. 
  5. then cover the jam dough. 
  6. Now mix the remaining egg with the sugar, melted butter and coconut. 
  7. spread over the jam evenly 
  8. Bake for 35 to 45 minutes, depending on your oven, or until the surface turns golden. 
  9. out of the oven, and let cool, before unmolding, and if you have an electric knife, it will be ideal for cutting, and have nice bars. 



And the method in video: 

**coconut bars**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/barre-confiture-noix-de-coco-027-a_2.jpg)

Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 220 gr of flour. 
  * 200 gr of butter. 
  * 1 tablespoon of sugar 
  * 1 egg. 
  * 1 tablespoon of milk. 
  * 1 cup of baking powder. 
  * pinch of salt 
  * 200 gr of apricot jam 

for garnish: 
  * 1 egg 
  * 80 gr of sugar 
  * 20 gr of melted butter 
  * 120 coconut 



**Realization steps**

  1. Heat the oven to 170 degrees 
  2. in the bowl of a blender, mix the butter into pieces, flour, salt, sugar and baking powder. 
  3. beat the egg with the milk and add to the previous mixture, then shield again. 
  4. pour this mixture into your mold lined with baking paper, and spread the dough over the entire surface, it does not run the dough, so you must leave it well. 
  5. then cover the jam dough. 
  6. Now mix the remaining egg with the sugar, melted butter and coconut. 
  7. spread over the jam evenly 
  8. Bake for 35 to 45 minutes, depending on your oven, or until the surface turns golden. 
  9. out of the oven, and let cool, before unmolding, and if you have an electric knife, it will be ideal for cutting, and have nice bars. 



bonne dégustation, et a la prochaine recette 
