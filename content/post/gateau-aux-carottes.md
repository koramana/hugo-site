---
title: carrot cake
date: '2015-03-13'
categories:
- gateaux, et cakes
tags:
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/gateau-aux-carottes.CR2_.jpg
---
[ ![carrot cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/gateau-aux-carottes.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-gateau-aux-carottes.html/gateau-aux-carottes-cr2>)

##  carrot cake 

Hello everybody, 

There are several varieties of "carrot cake" or the carrot cake, but this recipe is the best for me. It's a carrot cake recipe that's always good. 

This time, the carrot cake was the base of my birthday cake, and it's a super good base, because the carrot cake is very soft and melting in the mouth, with a smooth cream, the cake is not is more beautiful. I did not dwell too much on the decoration, made me had the pleasure to deposit tagada strawberries on top, according to his taste, to make me the surprise, hihihi 

[ ![carrot cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/gateau-aux-carottes-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-gateau-aux-carottes.html/gateau-aux-carottes-1-cr2>)   


**carrot cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/gateau-aux-carottes-1.CR2_.jpg)

portions:  10-12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * for 2 cakes of 23 cm 
  * 350 gr flour 
  * 240 gr of sugar 
  * 400 gr grated carrots 
  * 350 ml of oil 
  * 150 gr of crushed walnuts 
  * 6 eggs 
  * the zest of an orange (I did not, replace with lemon zest) 
  * 1 C. a coffee of cinnamon 
  * ½ teaspoon nutmeg 
  * 2 teaspoons of baking powder 
  * 2 teaspoons baking soda 
  * a pinch of salt 

Mascarpone cream: 
  * 1 box of mascarpone (200 ml) 
  * 1 box of thick cream (double cream here in England) 
  * sugar according to taste 



**Realization steps**

  1. preheat the oven to 170 ° C 
  2. butter and flour two 25 cm molds. 
  3. In a bowl mix the sugar, the oil and the eggs, 
  4. add the peeled and grated carrots, the crushed nuts and the orange zest. 
  5. In another bowl mix remaining dry ingredients (flour, yeast, baking soda, salt and spices). 
  6. Add the dry ingredients to the first mixture, divide the preparation on both molds in a preheated oven at 170 degrees C, about 40 minutes (depending on the oven). 
  7. let cool down and then unmould 
  8. prepare the cream by whisking the mascarpone and the thick cream, 
  9. add the sugar until you have the taste that you like the most. 
  10. cover the first cake with a layer of cream, 
  11. put the second cake and decorate the cake according to your taste. 



[ ![carrot cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/gateau-aux-carottes-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-gateau-aux-carottes.html/gateau-aux-carottes-2-cr2>)
