---
title: dry cakes with maizena
date: '2015-07-13'
categories:
- birthday cake, party, celebrations
- Algerian dry cakes, petits fours
- sweet recipes
tags:
- Algeria
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateaux-secs-a-la-maizena-1.jpg
---
[ ![dry cakes with maizena 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateaux-secs-a-la-maizena-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateaux-secs-a-la-maizena-1.jpg>)

##  dry cakes with maizena 

Hello everybody, 

Are you looking for simple cakes, dry cakes, light, crispy fondant in the mouth, and super easy to make? do not go far, because here is the recipe for a light cake with maizena, cakes lambout super easy to make. 

These maizena dry cakes are an achievement of my friend **Malika Bouaskeur.** These dry cakes can easily find their place in your cake tray, and I'm sure it will please many. 

A cake that looks a lot like [ Lambout cake ](<https://www.amourdecuisine.fr/article-gateau-lambout-gateau-mohgone.html>) , yum yum I like it a lot. 

[ ![dry cakes with maizena 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateaux-secs-a-la-maizena-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateaux-secs-a-la-maizena-2.jpg>)   


**dry cakes with maizena**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateaux-secs-a-la-maizena.jpg)

portions:  30  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 250 g softened margarine 
  * 1 glass of icing sugar 
  * 2 egg whites 
  * 1 sachet baking powder 
  * 1 C. vanilla coffee 
  * 500 g of maizina 
  * Flour as needed (I added 3 tablespoons) 
  * jam and coconut to assemble the pieces 



**Realization steps**

  1. In a bowl, mix margarine and icing sugar 
  2. add egg whites, vanilla and baking powder, mix well 
  3. then add the maizina if the dough sticks gradually add the flour must have a firm and soft dough. 
  4. With a lambout (cornet), form bracelets and sticks 
  5. bake in a preheated oven at 180 degrees until the cakes turn a light golden color. 
  6. Glue sticks 2 to 2 with jam and decorate with coconut 



[ ![dry cakes with maizena 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateaux-secs-a-la-maizena-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/gateaux-secs-a-la-maizena-3.jpg>)
