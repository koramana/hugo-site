---
title: Frangipane cake with apricots
date: '2011-06-24'
categories:
- Algerian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/06/frangipane-abricot-028_thumb1.jpg
---
##  Frangipane cake with apricots 

Hello everyone, 

I liked to start this new Muslim year, with a little gluttony, a beautiful cake very fondant, very very delicious, which is only the base of the delicious cream almond or Frangipane. 

In any case, you are lucky to see these pictures of this Apricot Frangipane Cake, because there is no more a crumb, everyone has eaten at will (must say that with us, it has never been the case, my children do not like to eat the cakes, but that the, well, they did not resist, and must say that it's really good, it melts in the mouth .................. ..hum 

you will need:   


**Frangipane cake with apricots**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/frangipane-abricot-028_thumb1.jpg)

**Ingredients**

  * 125 gr of butter at room temperature 
  * 125 gr of sugar 
  * ½ teaspoon of almond extract 
  * 100 gr of flour 
  * 60 grs ground almonds 
  * 1 teaspoon of baking powder 
  * 2 eggs 
  * 8 apricots fresh, or as for me in box 
  * 2 tablespoons of apricot jam 
  * 30 gr of almonds 



**Realization steps**

  1. preheat the oven to 180 degrees. 
  2. butter an oven-baking tin 20 to 23 cm (mine was 21 cm, and I preferred to chemise it with baking paper, and it was a mold with a removable base) 
  3. whip the butter and sugar until you have a nice creamy consistency 
  4. add almond extract and eggs one by one while whisking constantly. 
  5. in another container, mix the flour, almonds and baking powder, then add this mixture gently to the butter cream. 
  6. pour this mixture into your mold, trying to give it a uniform surface with the back of a spoon 
  7. take the apricots, cut them in half, and go through them in the almond cream, cut surface and denoyautee upwards 
  8. cook for almost 35 minutes or until golden brown. 
  9. leave it in the mold for 10 minutes, then remove from the mold and leave to soften for 10 minutes on a rack 
  10. at this time, heat the apricot jam with 2 teaspoons of water, then garnish your cake with this jam, and sprinkle with the tapered almonds 
  11. TASTE ... .. 



je vous conseille vivement cette recette qui est trop bonne et aussi pas trop coûteuse. 
