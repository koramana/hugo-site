---
title: thousand leaves to the broken dough
date: '2015-06-04'
categories:
- dessert, crumbles and bars
- cakes and cakes
- recettes sucrees
tags:
- Algerian cakes
- Cakes
- Cookies
- biscuits
- Pastry
- Ramadan
- Ramadan 2015

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/milles-feuilles-a-la-pate-bris%C3%A9e-1.jpg
---
[ ![thousand leaves to the broken pasta 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/milles-feuilles-a-la-pate-bris%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42673>)

##  Thousand leafs with broken pasta, 

Hello everybody, 

Another recipe from one of my readers, a recipe that I really liked ... I admit that I never thought about this version has the broken pasta of thousand sheets, plus the presentation is just top. 

[ ![thousand leaves with broken pasta](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/milles-feuilles-a-la-pate-bris%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42671>)   


**thousand leaves to the broken dough**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/milles-feuilles-a-la-pate-bris%C3%A9e-3.jpg)

portions:  4-6  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** original broken dough: 

  * 200 g of soft butter 
  * 250g of flour 
  * 1 tablespoon of icing sugar 
  * a little water 

for the muslin cream: 
  * [ regular pastry cream ](<https://www.amourdecuisine.fr/article-creme-patissiere.html>) prepare with ½ liter of milk 
  * 1 tablespoon softened butter 
  * 1 tablespoon of icing sugar 

decoration: 
  * icing sugar 
  * dark chocolate 



**Realization steps**

  1. prepare the broken dough by mixing all the ingredients then pick up with the dough, until you have a soft dough. 
  2. let the dough rest. 
  3. shape triangles and bake in a preheated oven at 180 degrees, until you have a golden dough. 

Prepare the pastry cream 
  1. after cooling, work with a beater, add 1 tablespoon butter softened while whisking and 1 tablespoon icing sugar. 

Mounting: 
  1. make the assembly by filling the pastry with the muslin cream using a pastry bag. 
  2. each cake will have 3 layers of broken dough. 
  3. decorate with a little icing sugar and dark chocolate 



[ ![thousand leaves with broken pasta 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/milles-feuilles-a-la-pate-bris%C3%A9e-2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42670>)
