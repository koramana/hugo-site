---
title: Beef wok fried with noodles
date: '2012-05-05'
categories:
- amuse bouche, tapas, mise en bouche
- diverse cuisine
- pizzas / quiches / tartes salees et sandwichs

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/panique-en-cuisine.bmp_2-300x260.jpg
---
Hello everybody, 

another recipe from a reader: **Beef wok fried with noodles** , a recipe that participates in the contest "express cooking" that I launched just recently, and I'm still waiting for your entries. 

see more detail on the link, [ contest »express cooking" ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>) to win a very nice gift. 

ingredients: 

  * 2 slices of beef tenderloin 
  * 1 packet of noodles 
  * mix of frozen vegetables (you can find special vegetable mixes wok, I use a classic mixture: peas, carrots, green beans) 
  * 2 tablespoons soy sauce 
  * 2 teaspoon of cornflour (I used a spoonful of flour I did not have cornflour) 
  * 2 tablespoons honey 
  * 1 half glass of tea of ​​bouillon cube. 
  * 1 half teaspoon garlic powder. 
  * 2 pinches of ginger 
  * 1 onion 
  * 1 teaspoon of sugar 



preparation 

  1. boil some water for the noodles, 
  2. for a while, cut the beef fillets into thin slices, then pour a tablespoon of soy sauce, a tablespoon of honey, 1 tablespoon of flour or cornflour and mix everything together. 
  3. leave in the fridge to prepare the noodles 
  4. prepare noodles as indicated on the package (usually 3 minutes) in boiling water and drain 
  5. pour a tablespoon of honey and mix so that it does not stick ... 
  6. put 2 tablespoons of olive oil or neutral oil in a wok or in a rather hollow pan. 
  7. puis mettre l’oignon et mettre le bœuf mariné a feu vif, en remuant sans s’arrêter… (faites attention a ce que ca ne colle pas et par la suite avoir du mal a nettoyer votre wok! 😀 ) 
  8. cook for 1 minute and reserve the meat and its cooking juices ... 
  9. wipe your wok, put some oil, then put 1 half teaspoon garlic powder and 2 pinches ginger for 30 seconds over high heat (be careful that it does not burn, I was inattentive for a second, and I found garlic and black ginger lol) 
  10. then pour the frozen vegetables, the half teaspoon of cube broth, 1 tablespoon of soy and the teaspoon of sugar. 
  11. simmer over medium heat for 3-4 minutes ... 
  12. pour noodles and meat and mix over high heat for about 2 minutes (I give half a glass of tea bouillon cube) 
  13. Note: it looks complex but c very easy and very fast especially but of course it is important to respect the steps of the recipe! 



this recipe participates in [ "express kitchen" contest ](<https://www.amourdecuisine.fr/article-concours-recettes-indienne-ma-participation-104476288.html>) : 

#  ![panic-in-cuisine.bmp_2](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/panique-en-cuisine.bmp_2-300x260.jpg)
