---
title: Green beans with onions and chicken
date: '2011-02-23'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/02/haricots-vert-oignons_11.jpg
---
![bean-green-oignons_1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/haricots-vert-oignons_11.jpg)

##  Green beans with onions and chicken 

Hello everybody, 

Green beans with onions and chicken: today I put you a recipe that has been well classified in the dungeons of my blog, a delicious and very rich dish, I really like to introduce vegetables to my children, and I'm happy that they do not say no to that, you'll laugh if I tell you, my kids do not like to eat fries, eggs on a plate, pizzas, and quick meals, I always have to cook dishes in sauce or stew, ca, he eats it ???? 

in any case this dish is very fast, and very delicious, and especially very practical for hot days. (yes I had done the summer or rather two years ago ... .. hihihihi, it is for that I go back the recipe)   


**Green beans with onions and chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/haricots-vert-oignons_11.jpg)

**Ingredients**

  * Ingredients: 
  * pieces of chickens 
  * green beans 
  * 3 onions 
  * 4 fresh tomatoes, or canned tomatoes, in pieces 
  * 2 cloves garlic 
  * salt, black pepper, olive oil 



**Realization steps**

  1. in a pressure cooker, sauté the onion cut into thin slices in length, 
  2. add the chicken pieces, salt and pepper, 
  3. let go well until the onion becomes very tender. 
  4. add the garlic and tomato cut into pieces, 
  5. then add the beans, well cleaned. 
  6. close the pressure cooker, and cook. 
  7. and here is your dish is ready to be tasted with a delicious bread, or a good matlou 



![bean-green-oignons1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/haricot-vert-oignons11.jpg)

good Appetite, and thank you for your visits 

bonne journee 
