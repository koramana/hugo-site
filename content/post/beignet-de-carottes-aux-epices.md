---
title: carrot donut with spices
date: '2012-07-10'
categories:
- Algerian cuisine
- Plats et recettes salees

---
& Nbsp; hello everyone, finally the time to publish these delicious donuts spiced with carrots, it must be said that although it is with spices, but I really like to taste them after wrapping them in crystallized sugar, they were a fall. in any case, I give you the recipe, and you can eat them according to your desires, with a salty sauce, such as tzatziki, or a hot sauce, like peanut butter cream that I already made on my blog, Or do like me, with a thin layer of sugar, huuuuuuuuuuum Ingredients (for 20 donuts almost) & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.75  (  1  ratings)  0 

Hello everybody, 

finally the time to publish these delicious donuts spiced with carrots, it must be said that although it is with spices, but I really like to enjoy them after wrapping them in crystallized sugar, they were to fall. 

in any case, I pass you the recipe, and you can eat them according to your desires, with a salty sauce, like tzatziki, or a hot sauce, like [ peanut butter cream ](<https://www.amourdecuisine.fr/article-creme-piquante-au-beurre-de-cacahuete-89052515.html>) that I already did on my blog, or then do like me, with a thin layer of sugar, huuuuuuuuuuum 

Ingredients (for 20 donuts almost): 

  * 2 big carrots 
  * a nice handful of raisins 
  * curry 
  * cumin 
  * coriander 
  * turmeric 
  * salt 



For the dough: 

  * 100g of flour 
  * 2 whole eggs 
  * 1/2 sachet of baking powder 
  * 100 ml of milk 
  * some hot water 



method of preparation: 

  1. Prepare the donut dough by mixing all the ingredients and let stand while you prepare the rest. 
  2. Swell the raisins with a little hot water in a bowl and add the spices. 
  3. Cut carrots into small cubes, cook for about 15 minutes in boiling water. 
  4. add the carrots to the donut dough. 
  5. Add the grapes and the juice of infusions of spices, just to have a nice dough not too flowing. 
  6. Mix and fry the donuts in a hot oil bath. 


