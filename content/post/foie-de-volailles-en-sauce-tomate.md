---
title: Poultry liver with tomato sauce
date: '2013-07-03'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/foie-de-volailles-mchermla-recette-ramadan-2013.CR2_1.jpg
---
![liver-of-poultry-mchermla - recipe-Ramadan-2013.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/foie-de-volailles-mchermla-recette-ramadan-2013.CR2_1.jpg)

##  Poultry liver with tomato sauce 

Hello everybody, 

Ramadan 2013 is fast approaching, and I want to do the big cleaning on my pc, as I did at home, I will try to publish all the recipes I have before the day "J" to empty all the photos, and can try to put new recipes .... 

So I post you this poultry liver recipe in tomato sauce, or kebda mchermla poultry, a recipe easy to make and very tasty ... 

**Poultry liver with tomato sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/kebda-mchermla-de-volailles-ramadan-2013.CR2_1.jpg)

portions:  4  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 350 g of poultry liver. 
  * 2 cloves garlic minced. 
  * a few sprigs of coriander. 
  * salt, black pepper, coriander powder 
  * thyme 
  * 2 tablespoons olive oil. 
  * 1 teaspoon of cumin. 
  * 1 can of tomato in pieces 



**Realization steps**

  1. clean the liver of poultry and cut it into 
  2. In a heavy-bottomed frying pan, sauté the garlic and the diced liver in the oil for 2 minutes. 
  3. add the spices, salt and pepper. 
  4. Add tomatoes, thyme, coriander and spices and salt. 
  5. cover and cook 
  6. rectify the seasoning if necessary. 
  7. Serve in a dish and sprinkle with chopped coriander. 



![liver-of-poultry-with-sauce-tomates.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/foie-de-volailles-en-sauce-tomates.CR2_1.jpg)

method of preparation: 

{{< youtube 35SKRuD4Rlc >}} 
