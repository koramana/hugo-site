---
title: marbled chocolate / orange cake
date: '2012-11-19'
categories:
- gateaux d'anniversaires, de fetes, celebrations
- recettes sucrees

---
Hello everybody 

a delicious marbled with the good taste of orange, we take advantage of it since it is the period, so imagine that we add to that the taste of chocolate, a marvel 

without delay we go to the recipe: 

  
ingredients: 

  * 50 grams of dark chocolate 
  * 125 grams of butter at room temperature 
  * 120 grams of sugar 
  * zest of an orange 
  * 2 medium eggs, beaten 
  * 125 grams of flour 
  * 1 C. coffee baking powder 
  * 2 c.a orange juice coffee 
  * 1 C. cocoa powder 


  1. preheat your oven to 180 degrees C 
  2. cover a cake tin with baking paper, or butter and flour. 
  3. put the chocolate to melt in a bain-marie, or like me in the microwave, while monitoring and stirring every 5 seconds.  put aside. 
  4. beat the butter and sugar to have a creamy mixture 
  5. add the orange zest, always beat 
  6. add the eggs, without stopping still beating. 
  7. add the flour, and mix with a spatula, going up and down to have an airy mixture, add the orange juice, 
  8. divide the mixture in half, and add to one of the parts, the cocoa powder, and the chocolate 



with a spoon, pour the mixture into the mold, inserting it, then with the tip of the knife, stir gently to make a small marbling 

and cook for almost 40 min. 

faire sortir du four, et laissez refroidir 5 min avant de le retirer du moule 
