---
title: Tart with tuna
date: '2010-11-03'
categories:
- gateaux, et cakes
- ramadan recipe
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/11/malsouka-tajine-tunisien-aux-feuilles-de-bricks.160x1201.jpg
---
hello everyone I always told you that I often take advantage of the absence of my husband to do different things in the kitchen every day, because my husband is not one who easily accepts the different recipes of those he's always used to eat, and especially not to present him something that looks like pizza and tell him, here is a salty pie ....... hihihihihiih but before I go to the recipe I pass you from other link for entrees, pizzaz and others: Malsouka, Tunisian tajine, with leaves of bricks & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody 

I always told you, that I often take advantage of the absence of my husband to do different things in the kitchen every day, because my husband is not one who easily accepts the different recipes of those he always has The habit of eating, and especially not to present him a thing that looks like pizza and tell him, here is a salty pie ....... hihihihihiih 

but before I go to the recipe I pass you another link for entrees, pizzas and others: 

[ ![Malsouka, Tunisian tagine, with leaves of bricks](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/malsouka-tajine-tunisien-aux-feuilles-de-bricks.160x1201.jpg) Malsouka, Tunisian tagine, with leaves of bricks  ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks-88766924.html> "Malsouka, Tunisian tagine, with leaves of cooking love bricks at soulef")

[ ![Samoussa has chopped meat, Samoussas](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/samoussa-a-la-viande-hachee-samoussas.160x1201.jpg) Samoussa has chopped meat, Samoussas  ](<https://www.amourdecuisine.fr/article-samoussa-a-la-viande-hachee-samoussas-88499027.html> "Samoussa has chopped meat, Samoussas of cooking love at soulef")

[ ![Salmon tart spinach](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/tarte-saumon-epinards.160x1201.jpg) Salmon tart spinach  ](<https://www.amourdecuisine.fr/article-tarte-saumon-epinards-87479529.html> "Salmon tart of love of cooking at soulef")

[ ![Pepper pizza](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/pizza-aux-poivrons.160x1201.jpg) Pepper pizza  ](<https://www.amourdecuisine.fr/article-pizza-aux-poivrons-87367397.html> "Pizza with sweet peppers at soulef")

and even more here: 

[ INDEX OF INPUTS SPECIAL RAMADAN, PIZZAS AND QUICHES ](<https://www.amourdecuisine.fr/article-index-des-entrees-special-ramadan-pizzas-et-quiches-54310798.html> "Index of special entries ramadan, pizzas and quiches")

Ingredients: 

a short pastry: 

and at random I put my ingredients: 

\- flour almost 500 grs 

\- olive oil, or other if you do not like almost 70 ml or according to your taste of the texture of your dough (quite crispy, or not) 

\- 1 egg yolk 

\- 1 teaspoon baker's yeast 

\- 1 case of powdered milk 

\- some water to wet 

so mix the ingredients and pick them up with water to get a nice dough, let them rest and prepare your stuffing: 

\- a can of tomato cut in pieces, or 3 to 4 fresh tomatoes 

\- 2 cloves garlic 

\- Gruyère 

\- mozarilla 

\- cheese kiri cow 

\- tuna 

\- black olives 

spread the dough, garnish the corners with kiri cow cheese, then fold the dough on itself, and pour stuffing on, garnish to your taste, and cook in a hot oven. 

bonne dégustation. 
