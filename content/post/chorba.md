---
title: chorba
date: '2013-02-17'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- idea, party recipe, aperitif aperitif
- Dishes and salty recipes
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/huit-01111.jpg
---
![chorba](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/huit-01111.jpg)

##  chorba 

Hello everybody, 

the chorba is imperatively present on our table almost once a week, but I noticed, that I did not have a beautiful picture of this delicious dish on my blog, so I go back my old recipe, and I put you again pictures. 

this is a traditional recipe that marks the table Algerian especially in the holy month of Ramadan, here my husband often asks, almost every week, and I have a friend of the yemen who loves him very much and always asks me the recipe, so I give it to him, and certainly there are many women who make it, each one his ingredients, there are who likes it with vegetables, others without, and each the taste of his family, to accompany this chorba I advise you these [ input recipes ](<https://www.amourdecuisine.fr/categorie-10678929.html>)   


**Chorba, Algerian frik soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/chorba-frik-0121.jpg)

portions:  8  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * pieces of mutton meat 
  * 1 onion 
  * oil 
  * 1 and ½ cases of canned tomato 
  * 1 handful of chickpeas soaked the day before 
  * 1 bunch of coriander 
  * 1 small branch of celery 
  * 1 green water glass crushed (frik) 
  * salt, coriander powder, black pepper 



**Realization steps**

  1. therefore, cut the meat into cubes, put in a pot, and add the oil and onion to the blender with the branches of coriander, add the branch of celery, and the leaves of coriander cut well, l oil, salt, black pepper, and coriander powder, let it simmer a little over low heat, wet with ½ glass of water, and cook a little over low heat. 
  2. add after the canned tomato diluted in water, let it return a few more minutes, and add the water, half-cooking the meat pour the green wheat crushed in the broth, 
  3. cook and add chickpeas as cooked or half-cooked, so if cooked, add a few minutes before putting out the heat. 
  4. at the end of cooking, sprinkle with chopped coriander and serve hot. 



![chorba](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/chorba-frick1.jpg)

Do not forget, if you have tried one of my recipes, send me your essays on the page: [ my recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home")

bon appetit 
