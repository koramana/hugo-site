---
title: Apricot tart macaroon
date: '2013-06-14'
categories:
- cheesecakes and tiramisus
- dessert, crumbles and bars
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/Tarte-abricot-macaron1.jpg
---
Hello everyone, a sublime apricot pie with a nice layer of macaroons that Lunetoiles, it's a recipe a bit similar to the recipe for strawberry pie macaron ... which is already on the blog, but with a nice variation which has the place of the cream pastry, it has a beautiful layer of jam ... For the shortbread dough Preparation time: 30 min Cooking time: 30 min 240 g flour 150 g butter 90 g sugar 30 g almond powder 1 sachet vanilla sugar 1 egg yolk 1 pinch of salt & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.1  (  1  ratings)  0 

![Tart apricot-macaron.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/Tarte-abricot-macaron1.jpg)

Hello everybody, 

a sublime apricot tart with a nice layer of macaroons that Lunetoiles, it's a recipe a bit similar to the recipe of [ macaron strawberry pie ](<https://www.amourdecuisine.fr/article-tarte-fraises-macaron-117152632.html>) ... which is already on the blog, but with a nice variation that has the place of the cream pastry, we have a nice layer of jam ... 

For shortbread dough 

**Preparation time : 30 min  ** **Cooking time : 30 min  **

  * 240 g flour 
  * 150 g of butter 
  * 90 g of sugar 
  * 30 g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 



Cover macaroon: 

  * 3 egg whites 
  * 125 g icing sugar 
  * 125 g of almond powder 



Garnish : 

  * Jam of apricots (250 gr) 
  * Dry apricots (soaked in water for 24 hours) 
  * Tapered or coarsely chopped almonds 



![Tart apricot macaroon-2-copy-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/Tarte-abricot-macaron-2-copie-11.jpg)

method of preparation: 

  * The day before, put the dried apricots in a bowl, cover them with water, let them rehydrate for 24 hours. 



For the shortbread dough: 

  1. Mix the flour, sugar, vanilla sugar and salt. Add the diced butter and mix with the whisk K until you get a big shortbread. 
  2. Add the egg yolk and knead until a smooth, non-sticky paste is obtained. 
  3. Allow the time to cool in food film. 
  4. Spread the dough between 1 baking sheet. Cover a pie circle 24 cm in diameter after carefully buttering. 
  5. Prick with a fork and cool to prepare the macaroon machine. 



For the macaron device: 

  1. Whisk egg whites until stiff, add sugar halfway through. 
  2. Gently stir in the almond powder. 
  3. Place the apricot jam on the bottom of the pie, pour the macaroon machine and place the apricots, drained with the almonds. 
  4. Cook for 30 minutes at 180 ° C (th.6). Let cool. 
  5. Enjoy tepid or cold, accompanied by an apricot sorbet. 



![Tart apricot macaroon-1-copy-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/Tarte-abricot-macaron-1-copie-11.jpg)

PS: 

If you have a facebook page, and you like my recipes, do not make a full copy of my recipe on your pages, it is an infringement of the royalties .... you like my recipe, share the recipe link ... .. do not enrich your pages with the work of others .... 
