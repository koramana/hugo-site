---
title: almond and nut cones - Algerian cakes
date: '2014-10-02'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
tags:
- Dry Cake
- Aid cake
- Algeria
- Cookies
- Oriental pastry
- Algerian patisserie
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/cornets-bicolore-noix-amandes-019_thumb1.jpg
---
![almond cones with almonds 019](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/cornets-bicolore-noix-amandes-019_thumb1.jpg)

##  almond and nut cones - Algerian cakes 

Hello everybody, 

The almond cones, a nice Algerian cake that I revisited to give it a new look, remains that this cake is always a delight, especially with this thin layer of crispy and honey paste, which coats a super fondant cornet with flavors. almonds, nuts and / or any other dry fruit of your choice. 

it's one of [ Algerian cakes ](<https://www.amourdecuisine.fr/gateaux-algeriens-orientales-modernes-fete-aid>) I love prepared for any occasion, because it's super presentable in addition to being ultra delicious. An Algerian cake recipe easy to make, and you can do in a simple way in a plain color, it's a matter of taste.   


**almond and nut cones - Algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/cornets-bicolore-amandes-et-nois-2_thumb1.jpg)

portions:  40  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients** dough: 

  * 250 gr of flour 
  * 75 gr softened margarine 
  * 1 pinch of salt 
  * 1 C. white dye coffee 
  * 1 C. a pink dye coffee, and red 
  * flower water 
  * water 

the joke: 
  * 100 gr of almonds 
  * 150 gr of nuts 
  * 125 gr of sugar 
  * 1 C. a vanilla coffee 
  * 1 egg 

garnish: 
  * honey 
  * silver beads 
  * shiny food 



**Realization steps** prepare the pasta: 

  1. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  2. Mix well with your fingertips 
  3. divide the dough into two parts 
  4. for the one you are going to color in white add the dye in the flower water, then pick up the dough with this water, add a little if necessary to pick up, continue to knead a few seconds. 
  5. for the other color, take the other half of the shortbread mixture, add the dyes to the orange blossom water until the desired color is obtained, and start to collect the dough with this water, add the water if you need, and knead a few seconds. 
  6. cover your dough and let it rest on the side. 

for the stuffing: 
  1. for stuffing mix almonds, nuts and dry ingredients, pick up with egg and set aside. 
  2. spread both pasta, machine or baking roll 
  3. shape with the knife a right side for both pasta 
  4. put one in front of the other, and superimpose the straight ends to have a two-color paste 
  5. pass the roll again so that the two pasta stick well. 
  6. cut circles with the 8 cm diameter cookie cutter. 
  7. make cornets with the almond stuffing. 
  8. place them on the circles of dough. and cover with the dough 
  9. with flower-shaped pieces, make flowers with which you decorate your cone 
  10. let rest all night 
  11. bake for 10 minutes at 170 degrees, 
  12. just at the end of the oven soak the cornets in honey at normal temperature 
  13. garnish to your taste with pearls and shiny food 



and join me on facebook 

merci. 
