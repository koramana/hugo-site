---
title: chocolate ice cream and nestle
date: '2015-06-16'
categories:
- ice cream and sorbet
tags:
- Ramadan 2015
- ice cream
- Summer
- Easy recipe
- Without ice cream maker
- Fast Food
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/creme-glacee-chocolat-nestle-1.jpg
---
[ ![chocolate ice cream nestle 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/creme-glacee-chocolat-nestle-1.jpg) ](<https://www.amourdecuisine.fr/article-creme-glacee-au-chocolat-et-nestle.html/creme-glacee-chocolat-nestle-1>)

##  Chocolate ice cream and Nestlé 

Hello everybody, 

Does it tell you a delicious chocolate ice cream? This recipe for chocolate ice cream and Nestle is a super easy recipe to make, and you only need three ingredients to make it ... And most importantly, you do not need to use an ice cream maker ... Are you interested then? 

It's a recipe from our cooking group, made by my friend **Wamani M,** and who was repeating, not once, not two, and not three by the members of the group ... So why do not you try your turn? 

**chocolate ice cream and nestle**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/creme-glac%C3%A9e-chocolat-nestl%C3%A9-2-001.jpg)

portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 100 gr of sweetened condensed milk 
  * 2 to 3 tablespoons of cocoa (+/- according to your taste) 
  * orange zest (optional) 
  * 250 ml of fresh cream 



**Realization steps**

  1. mix the condensed milk with the cocoa, to have a very homogeneous mixture. 
  2. stir in the orange zest if you are going to use it 
  3. of another recipent, go up in chantilly the creme fraiche very cold 
  4. stir in the first whipped cream mixture gently 
  5. and place in the freezer 
  6. at the moment serve, you can decorate with flaked almonds, or broken praline, and a drizzle of chocolate sauce 



[ ![nestle chocolate ice cream](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/creme-glacee-chocolat-nestl%C3%A9.jpg) ](<https://www.amourdecuisine.fr/article-creme-glacee-au-chocolat-et-nestle.html/creme-glacee-chocolat-nestle>)
