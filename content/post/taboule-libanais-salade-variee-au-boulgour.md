---
title: Lebanese tabouleh / mixed salad at Boulgour
date: '2014-07-10'
categories:
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/salade-008a_thumb1.jpg
---
Hello everybody, 

yesterday a beautiful sunny day that did not want to go into the kitchen to simmer something, especially since the children had a little white beans in red sauce, a dish they like a lot, so a little late, the idea of ​​a salad full of freshness tempted me for a light dinner. 

but it's not only a light dinner, it's a very tasty denier, this salad really makes all the senses come to life, so all the tastes are mixed in the mouth ... 

this salad is a delight to present on your tables of Ramadan, and on this article I draw you an index of [ ramadan recipe ](<https://www.amourdecuisine.fr/article-menu-ramadan-plats-chorbas-et-entrees-55268353.html>) . 

**Lebanese tabouleh / mixed salad at Boulgour**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/salade-008a_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * without delay I pass you the ingredients: 
  * 3 tablespoons of bulgur (or instead crushed wheat) 
  * ½ pepper 
  * ½ cucumber 
  * 2 fresh medium tomatoes 
  * 1 handful of pitted green olives 
  * ½ onion 
  * 10 strands of parsley 
  * the pressed juice of a lemon (or according to taste) 
  * 2 to 3 tablespoons of olive oil 
  * salt 



**Realization steps**

  1. method of preparation: 
  2. place the bulgur in a bowl, add the boiling water over 
  3. let stand 5 min 
  4. drain and boil in moderately salted water 
  5. when it's soft, drain and let cool 
  6. wash your vegetables, and cut them into small ones 
  7. cut the olives into slices 
  8. mix all your ingredients in a salad bowl 
  9. season to taste, with salt, olive oil, and lemon juice 



enjoy this salad, with bread, or without bread, present it as a starter, or for hot days, present it with a barbecue, this salad is a real delight. 

to eat without moderation, hihihihihiihih 

thank you for your visits and comments 

thank you to all those who continue to subscribe to my newsletter, it makes me really happy, all those people who want to be up to date with any new publication at home. 
