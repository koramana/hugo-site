---
title: Pomegranate smoothie
date: '2015-12-07'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-de-grenade-1.jpg
---
[ ![Pomegranate smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-de-grenade-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-de-grenade-1.jpg>)

##  Pomegranate smoothie 

Hello everybody, 

for the story of this Pomegranate Smoothie, after having [ to grind a pretty pomegranate ](<https://www.amourdecuisine.fr/article-comment-egrainer-une-grenade-en-1-minute.html>) , I made myself a super delicious smoothie smooth, super delicious ... My cup of happiness in the morning. 

With smoothies, you do not have to follow the recipe to the letter, as for me, this recipe for Pomegranate Smoothie that was to be based on pomegranate and strawberries (because I like the acidity of strawberries), but there was more, so I did with what I had: frozen cherries ... So do not hesitate to put the fruit that you like in your cup of smoothie. 

To your armor, the time I sip my pomegranate Smoothie. 

**Pomegranate smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-de-grenade.jpg)

portions:  2  Prep time:  2 mins  total:  2 mins 

**Ingredients**

  * 1 grenade 
  * ½ glass of frozen cherries 
  * 1 banana 
  * 1 glass of almond milk 



**Realization steps**

  1. grind the pomegranate 
  2. place all the ingredients in the blender bowl 
  3. you do not have to add all the glass of almond milk 
  4. pass the smoothie to the Chinese, to avoid the hard pieces of seeds 
  5. sip! 


