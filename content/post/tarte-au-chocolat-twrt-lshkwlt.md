---
title: chocolate pie تورتة الشكولاطة
date: '2017-09-06'
categories:
- Chocolate cake
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-au-chocolat-028.CR2_.jpg
---
#  ![chocolate pie 028.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-au-chocolat-028.CR2_.jpg)

##  chocolate pie 

Hello everybody, 

and here is my favorite pie in the world, my cute sin, the one and only, tadaaaa: **The chocolate pie** , a timeless classic, a recipe with incomparable flavors, an irresistible fondant ... 

I do not want to do it often, because, I tell you and it's a secret between us: "I can eat all the pie, all alone in one day ..." I'm supposedly: "the conscious", and I cut a small piece, then I come back to say, yes just another little piece, and so on ... .. until I open the fridge to find only the crumbs ... 

With **the chocolate pie** I become a choco-addict, hihihihi. So this time, I was super happy to realize it, on the pretext that we received friends of my husband .... 

For once, by making the chocolate pie, I eat only one small piece .... hihihihi. 

![chocolate tart-012.CR2 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-au-chocolat-012.CR2_2.jpg)

To prepare this delicious **chocolate pie** as a base I preferred my [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast") , that I can not easily replace with another dough, and for the filling, I adopted the recipe of Frederic Anton.  **chocolate pie**

What I like in the realization of this chef, is that after the white cooking of the shortcrust pastry, we prepare the filling, we heat the oven at 170 degrees C, pour the filling on the bottom of the pie, we put in the oven, and at this time, we turn off the oven, so the filling will cook with the heat of the oven, which will cool very slowly (do not forget it is off), suddenly the filling will not boil , and we're going to have a bubble-free chocolate pie, and with a smooth texture, as you can see in the photos.   


**chocolate pie تورتة الشكولاطة**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-au-chocolat-029.CR2_2.jpg)

portions:  8  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast") : 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

garnish: 
  * 50 g of milk 
  * 120 g of whipping cream 
  * 120 g of 60% chocolate 
  * 12 g of butter 
  * 2 small eggs 



**Realization steps**

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a dough is detached from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Darken a pie circle 28-30 cm in diameter. 
  6. Prick the bottom of the dough with a fork. 
  7. cover with a baking sheet, and fill with dry vegetables (for me it was glass tablets for vase ornament) and put in the oven 20 min at 180 ° C, then remove the legumes and the leaf and continue the cooking 10 minutes. 

for garnish: 
  1. Preheat the oven to 170 ° C. 
  2. Crush the chocolate in a bowl. 
  3. In a saucepan, boil the milk, butter and cream. 
  4. Pour this mixture on the chocolate. Mix gently, making sure not to make bubbles. 
  5. Beat the eggs lightly, then add them to the mixture. 
  6. Pour this mixture into the bottom of the pie before cooking. 
  7. turn off the oven and cook for 15 minutes 
  8. Check the cooking by tapping lightly, the pie should be slightly flickering. And otherwise you can leave it even more. 



![chocolate tart-020.CR2 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-au-chocolat-020.CR2_2.jpg)
