---
title: ghribia with peanuts, algerian cake / ghoriba with peanuts
date: '2013-11-15'
categories:
- couscous
- Algerian cuisine
- Moroccan cuisine
- recipes of feculents

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/ghribiya-aux-cacahuetes_thumb.jpg
---
![ghribia with peanuts, algerian cake / ghoriba with peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/ghribiya-aux-cacahuetes_thumb.jpg)

##  ghribia with peanuts, algerian cake / ghoriba with peanuts 

Hello everybody, 

kouky took with her a nice box filled with delicious cakes, and there was a succulent ghribia in it .... it must be said that among all the ghribiya recipes that I tested and tried, the one, remains among the most delicious and melting, besides there is not a person who drip and did not ask me the recipe. 

**ghribia with peanuts, algerian cake / ghoriba with peanuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/les-gateaux-algeriens-Aid-el-fitr-2012-ghribia-aux-cacahu1.jpg)

Recipe type:  Algerian cake, dry cakes  portions:  10  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * A measure of grilled and ground peanuts 
  * A measure of sifted chickpea flour 
  * One measure of sieved icing sugar 
  * Two measures of grilled flour very lightly and sifted 
  * Vanilla 
  * ½ teaspoon of baking powder 
  * A measure of melted smen (vegetable fat) 
  * A pinch of salt 



**Realization steps**

  1. Mix all the dry elements and water with melted smen by rubbing with the palms of the hands until obtaining a soft paste that can be spread out. 
  2. Cool for an hour or more depending on the season. 
  3. Spread on a slightly floured plan, to a thickness of 2 cm. 
  4. Cut with a cookie cutter of your choice (crescent moon, rose hole in the middle). 
  5. Place on a tray covered with baking paper, return to the fridge. 
  6. Bake in a preheated soft oven for about 10 to 15mm. 
  7. Allow to cool completely before unmolding. 



Note The measure I used was a 220ml glass for all the ingredients. For a larger container, the amount of yeast should be increased [ ![algerian cakes Aid el fitr 2012 ghribia with peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/les-gateaux-algeriens-Aid-el-fitr-2012-ghribia-aux-cacahu1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/les-gateaux-algeriens-Aid-el-fitr-2012-ghribia-aux-cacahuet.jpg>)
