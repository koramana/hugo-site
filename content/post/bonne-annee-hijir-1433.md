---
title: happy new year hijir 1433
date: '2011-12-26'
categories:
- boissons jus et cocktail sans alcool

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/couscous-611_thumb.jpg
---
السلام عليكم 

On the occasion of the new Year hijri, I address to all the Muslims of the whole world, to wish them a good year, full of love, peace, solidarity, fraternity, and especially full of faith in the volente of the good god. 

دعاء آخر السنة:   
اللهم ما عملت في هذه السنة مما نهينتي عنه فلم أتب منه و لم ترضه و نسيته و لم تنسه و حلمت علي بعد قردتك على عقوبتي و دعوتني إلى التوبة منه عد جراءتي على معصيتك فإني أستغفرك فاغفر لي و ما عملت فيها بما ترضاه و وعدتني عليه الثواب فاسألك اللهم يا ذا الجلال و الإكرام أن تتقبله من و لا تقطع رجائي منك ياكريم ..   
\-----   
دعاء أول السنة:   
اللهم أنت الأبدي القديم الأول. و على فضلك العظيم و جودك المعول, و هذا عام جديد قد أقبل نسألك العصمة فيه من الشيطان و أوليائه و جنوده, و العون على هذه النفس الأمارة بالسوء, والاشتغال بما يقربني إليك زلفى يا ذا الجلال و الإكرام ..   
اللهم .. آمين ..آمين ..آمين ..   
و صلى الله على سيدنا محمد صلى الله عليه و سلم و على آله و صحبه أجمعين .. 

**************** during this magnificent, we have the custom to prepare super delicious dishes, such as: [ Couscous ](<https://www.amourdecuisine.fr/article-25345446.html>) ![https://www.amourdecuisine.fr/wp-content/uploads/2013/11/couscous-611_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/couscous-611_thumb.jpg) [ Chakhchukhat eddfar ](<https://www.amourdecuisine.fr/article-26090486.html>) ![https://www.amourdecuisine.fr/wp-content/uploads/2013/11/rus-007_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/rus-007_thumb.jpg) [ Trida ](<https://www.amourdecuisine.fr/article-25345529.html>) ![https://www.amourdecuisine.fr/wp-content/uploads/2012/04/trida_thumb_12.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/trida_thumb_12.jpg) [ tlitli red sauce ](<https://www.amourdecuisine.fr/article-tlitli-langues-d-oiseaux-sauce-rouge-69679639.html>) ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tlitli-2_thumb_1.jpg)

![https://www.amourdecuisine.fr/wp-content/uploads/2011/12/0912170911079492321.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/0912170911079492321.jpg)
