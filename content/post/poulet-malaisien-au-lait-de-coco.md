---
title: Malaysian chicken with coconut milk
date: '2016-06-30'
categories:
- diverse cuisine
- cuisine indienne
- Cuisine by country
- riz
tags:
- Oven cooking
- Spice
- Exotic cuisine
- Rice
- Curry
- Ginger
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/poulet-malaisien-au-lait-de-coco.jpg
---
[ ![Malaysian chicken with coconut milk](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/poulet-malaisien-au-lait-de-coco.jpg) ](<https://www.amourdecuisine.fr/article-poulet-malaisien-au-lait-de-coco.html/ingredient-pour-poulet-malaisien>)

##  **Malaysian chicken with coconut milk**

Hello everybody, 

On the kitchen group on facebook, the girls had asked that I make them a challenge, and as we had already the game: recipe around an ingredient 3 and that had for theme, the coconut milk. I chose the same theme on the band, and there were many nice recipes, among them, the recipe of Nawel Zellouf: CLAY-POT CHICKEN 

and this is what Nawel tells us: 

__ It is a very old dish of the Malaysian Cuisine. At the time they cooked the sauce in a pot (clay pot) and at the end of cooking the meat obtained is very tender! yum yum ... 

**Malaysian chicken with coconut milk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/poulet-malaisien-au-lait-de-coco-5.jpg)

portions:  5  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 1 kg of chicken. 
  * 45 g grated fresh coconut. 
  * 2 shallots chopped finely. 
  * 30 ml of vegetable oil. 
  * 2 cloves garlic. 
  * 20 g fresh ginger minced.   
[ ![Malaysian chicken with coconut milk](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/ingredient-pour-poulet-malaisien.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40740>)
  * Zest of a lemon. 
  * 2 green peppers optional. 
  * 400 g of coconut milk. 
  * 300 ml of chicken broth. 
  * 15 g of sugar. 
  * 15 ml of vinegar. 
  * ½ cube of knorr taste fish. 
  * A little coriander for decoration. 
  * ½ onion grilled for decoration. 



**Realization steps**

  1. Thoroughly wash chicken slices, remove all skin and preserve. 
  2. In a Wok, put the coconut and let it brown, 
  3. add the oil, scallops, garlic, ginger, chilli, and let it come back for 2-4 min then add the chicken slices and let cook 2-3min.   
[ ![Malaysian chicken with coconut milk](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/poulet-malaisien-au-lait-de-coco-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40741>)
  4. take the coconut milk out of the liquid and add it to the chicken and boil for 2 minutes then add the vinegar and the sugar. 
  5. Put it all in a baking pot (I used a clay but you can use a Pyrex), then put in the oven at 180 * for 50 minutes. After 50 minutes open the pot stir a little and put it in the oven for 10 minutes.   
[ ![Malaysian chicken with coconut milk](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/poulet-malaisien-au-lait-de-coco-4.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40743>)
  6. In a very hot wok put the oil and the sliced ​​onion and leave caramelized. 
  7. Serve with a good basmati rice and garnish with minced coriander and caramelized onion. 



[ ![Malaysian chicken with coconut milk](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/poulet-malaisien-au-lait-de-coco-5.jpg) ](<https://www.amourdecuisine.fr/article-poulet-malaisien-au-lait-de-coco.html/poulet-malaisien-au-lait-de-coco-5>)
