---
title: easy and fast eggplant donuts
date: '2015-09-01'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/beignets-d-aubergines-au-basilic3_thumb1.jpg
---
##  Easy and fast eggplant donuts 

Hello everybody, 

An aubergine that has been waiting for the time in the fridge, so that I realize a recipe .... Well, here is an expectation that has given a good result ... 

If you taste these donuts, the aubergine will not leave your shopping list, with the captivating scent of basil, and the softness of this beautiful layer of scented dough, which generously covers a thin melting eggplant, a simple and easy recipe but really delicious. 

in any case, me personally, I really like vegetable donuts, or fruit, because there is always a surprise inside, and these aubergine donuts were really a delight, even my husband who is very difficult, he really loved, I was surprised to see that there was no more. 

**easy and fast eggplant donuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/beignets-d-aubergines-au-basilic3_thumb1.jpg)

portions:  6  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 1 eggplant 
  * 1 egg 
  * 150 g flour 
  * 150 ml of water 
  * 1 teaspoon of yeast 
  * basil 
  * salt and pepper 



**Realization steps**

  1. Prepare the dough, Separate the egg yolk and white. 
  2. In a bowl, mix the flour, egg yolk, water and baking powder until smooth. 
  3. Beat the egg white and gently add it to the dough, salt, pepper and add the basil. 
  4. let stand about 30 min. 
  5. Cut the eggplant into thin slices (about 3 mm thick, no more). 
  6. Heat the oil, coat the aubergine slices in the dough, and fry until golden brown. 
  7. Arrange on paper towels to reduce excess fat. 
  8. serve with a good salad, or dip. 



![eggplant donuts with basil 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/beignets-d-aubergines-au-basilic-2_thumb1.jpg) ![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
