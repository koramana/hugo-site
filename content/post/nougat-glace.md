---
title: Iced nougat
date: '2017-11-12'
categories:
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/08/nougat-glac%C3%A9-6.jpg
---
##  Iced nougat 

Hello everybody, 

The frozen nougat, here is a dessert that will seduce you, and that you can prepare well in advance, put in verrines, or in a cake or baking mold, in the fridge and let out a few minutes before serving. 

An iced Nougat recipe shared with us, my friend Lunetoiles, whom I thank from the bottom of my heart. It must be said that I did not remain insensitive to this delight, because reading the recipe, I remembered the jawzia foam, or nut mousse that we realized my mother when we were small. 

My mother's recipe was a glass froth that was just chilled, differently to the frozen nougat that was frozen in the freezer. 

The Lunetoile recipe comes from _Isabel Brancq_ Lepage, and what I like about this recipe is that egg yolks and egg whites are used, unlike many recipes that only use egg whites. Personally, it helps me better to use whole eggs, because for me it's easy to preserve the egg whites (in the freezer) when I only use the egg yolks in a recipe, but to do it. contrary, does not work, because egg yolks are very poor. 

preparation 30 min 

cooking 3 min 

freezing   
10 hours for a large mold   
4 hours for verrines 

more 

I realized for you the video recipe, only in my recipe, I added a little glucose so that the honey does not crystallize, because I did not have a good honey at home, in the video I I've also used the sugar thermometer, but it's optional because you just need the honey to boil. 

{{< youtube jDCWwAyoxdA >}} 

**Iced nougat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/nougat-glac%C3%A9-6.jpg)

portions:  6  Prep time:  30 mins  cooking:  3 mins  total:  33 mins 

**Ingredients**

  * 2 cold eggs 
  * 20 cl of cold whole liquid cream 
  * 70 g of sugar 
  * 1 + ½ tablespoons acacia liquid honey 
  * 80 g of a mixture of flaked almonds, pine nuts and pistachios 



**Realization steps** First, caramelize the dried fruits: 

  1. In a non-fat frying pan, toast almonds, pine nuts and pistachios. 
  2. When the pan is hot, sprinkle with 40 g sugar, mix. 
  3. Let the dry caramelize, stirring to separate if they do not form a compact pile. 
  4. Remove from heat and place the caramelized dried fruit on a sheet of baking paper to allow them to cool. 

Then prepare the ice cream: 
  1. Heat the honey. 
  2. Separate whites from yellows. Mix the yolks and 30 g of sugar in a bowl. Beat the egg whites in a large bowl and add the boiling honey, continuing to whisk. Stir in this mixture gradually to egg yolks. 
  3. In a small bowl that you have put in the refrigerator while starting the recipe, whip the whole cream liquid whipped cream. 
  4. Then mix it gently, using a wooden spoon, with the preparation (white in snow, honey, yellow, sugar). 
  5. Now add the almonds, pistachios and caramelised pine nuts. 



Note I decided to present the nougats glazed in verrine (4h in the freezer), I could put them in a cake-type dish (10 / 12h in the freezer). The important thing is to respect the freezer time. The bigger your dish, the longer you have to leave it.   
  
To help you easily unmould a dish if you want to present the nougat in a log. Cover the inside of the dish with cling film or parchment paper before pouring the mixture into it.   
In any case, cover with a sheet of food or a lid the verrines or the dish and leave in the freezer.   
  
Turn out at the last minute and serve this iced nougat with raspberries and strawberries.   
  
Prepare this nougat with all kinds of candied fruits and dried (pecan nuts, caramelized peanuts, hazelnuts, dried apricots, dates, grapes, prunes) and candied fruits. ![frozen nougat 4](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/nougat-glac%C3%A9-4.jpg)
