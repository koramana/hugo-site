---
title: the bakewell pie
date: '2014-10-25'
categories:
- basic pastry recipes
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-bakewell-4.jpg
---
[ ![bakewell pie 4](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-bakewell-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-bakewell-4.jpg>)

##  the bakewell pie 

Hello everybody, 

Maybe a few of you are intrigued by this title, but it's not going to last too long, lol, because the Bakewell pie, it's just the British version of the frangipane pie, with a nice layer of jam, that goes settle between the shortbread and the frangipane cream .... I see, "is that all ??? Yes that's all ... nothing more difficult, you see! 

On the blog, I had the chance to realize, me and my girlfriend Lunetoiles several version of the pie bakewell, but we did not dwell on the English nomination, hihihihi ... 

So you can see for example [ plum bakewell tart ](<https://www.amourdecuisine.fr/article-tarte-aux-prunes-et-amandes.html>) , and we did the revisited version, replacing the raspberry jam. 

in any case, a little stroll on my [ pie recipes ](<https://www.amourdecuisine.fr/tartes-et-tartelettes>) and you will find what will please you. 

[ ![1.CR2 bakewell pie](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-bakewell-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-bakewell-1.CR2_.jpg>)

Now the little story with this pie, I was thrown into the preparation, I was sure to have all the ingredients, I prepared the dough, super everything went well, a nice dough, in the fridge for 15 minutes, I I spread it again in the fridge, I darkened it in the mold, I was going to attack the realization of the frangipane cream, and go find the powder of almonds in the cupboard !!! I cluttered the cupboard in camber, to finally find it just on the workspace, because I had removed when I started to prepare the shortcrust pastry, well! 

No problem, I prepare the cream, a children's game this part. I remove the pie shell from the fridge, I cover it with a thin layer of raspberry jam, I spread the frangipane cream using the pastry bag ... I put it all in the oven for 25 minutes, I wash it all the dishes that drags, then I say to myself it's time to sprinkle the flaked almonds over the pie, so that they are just golden brown ... 

I'm looking in the closet, on the desk, I'm sure to have crossed when I was looking for almonds powder ??? 

Well can not fall on the bag ... .I was carried a little, but I told myself that this is not what will spoil the recipe .... is not it?   


**the bakewell pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-bakewell-3.jpg)

portions:  8  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** shortbread: 

  * 240 g flour 
  * 150 g of butter 
  * 80 g of sugar 
  * 30 g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

garnish: 
  * 130 gr of unsalted butter, softened 
  * 120 gr of icing sugar 
  * 3 eggs 
  * ½ tablespoon almond extract 
  * 130 gr of ground almonds 
  * 30 gr of flour 
  * 1 pinch of baking powder 
  * raspberry jam as needed 



**Realization steps** preparation of the shortbread dough: 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a dough is detached from the sides of the bowl and is non-sticky. 
  4. pick up and put in the fridge for 15 min. 

preparation of the filling: 
  1. Beat the butter and sugar for about a minute or until the mixture becomes creamy. 
  2. Scrape the wall of the bowl and add the eggs, one at a time, beating well after each addition. The cream may seem like it has curdled. Do not panic. It will become very creamy after adding all the eggs. 
  3. Add the almond extract and whip for about 30 seconds and scrape the walls again. 
  4. Then add the almonds and the flour slowly. Mix well to incorporate everything 

Montage of the pie: 
  1. Spread the dough with the roll between 2 sheets of baking paper. always start from the middle, and roll the baking roll towards you, raise the baking roller, turn the dough 45 degrees. Place the pastry roll in the middle again and roll it again towards you, this will prevent the dough from torn or sticking to the baking roll. 
  2. When the dough is well spread on a thickness of almost 7 mm, put it in the fridge, for a few minutes 
  3. Go for a pie circle 28-30 cm in diameter. 
  4. Prick the bottom of the dough with a fork and place it in the fridge for at least another 30 minutes 
  5. remove from the fridge and spread over the raspberry jam, smooth to have an even surface   
Does not it remind you of all that? the alcazar cake of the best pastry chef, for those who saw the show? thin layer, or a lot of jam ???? a question everyone was asking. I will tell you according to your taste, if you like a little sweet or not, because the raspberry jam is not acidic like the apricot count in the cake alcazar 
  6. Pour on the frangipane jam you can use the pastry bag for a perfect result. 
  7. Bake in a preheated oven at 180 degrees for 30 minutes 
  8. Five minutes before the end of cooking, remove the tart from the oven and sprinkle with flaked almonds on the top and return to the oven until the almonds turn a nice color. 
  9. Cool completely before cutting. 



[ ![bakewell pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-bakewell-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/tarte-bakewell-2.jpg>)

Merci pour vos visitez et commentaires, et a la prochaine recette. 
