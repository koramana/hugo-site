---
title: Chocolate flan
date: '2016-06-16'
categories:
- panna cotta, flan, and yoghurt
tags:
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-au-chocolat-046.CR2_1.jpg
---
#  ![flan au chocolat-046.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-au-chocolat-046.CR2_1.jpg)

##  Chocolate flan 

Hello everybody, 

My children love everything that contains chocolate, as everyone presumes, and as I prefer to offer homemade desserts, I prepared them a delicious chocolate flan ... 

it's a recipe that I often do, but I've never had the chance to take pictures, because my children do not even let them cool ... 

yes, they like the texture you see in these two photos ... I prefer the texture of the picture from below, because I like when the flan melts in the mouth gently, and we can detect the bitterness of dark chocolate, before the sweet taste of custard .... what do I like to take my time with each spoon ... 

I do not eat the chocolate custard just for dessert, I eat it to enjoy it .... in the true sense of the word. 

![flan au chocolat-033.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-au-chocolat-033.CR2_1.jpg)

**Chocolate flan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-au-chocolat-019.CR2_1.jpg)

portions:  4  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients**

  * 50 g of milk 
  * 120 g of whipping cream 
  * 120 g of 60% chocolate 
  * 12 g of butter 
  * 2 small eggs 



**Realization steps**

  1. Preheat the oven to 160 ° C. 
  2. Crush the chocolate in a bowl. 
  3. In a saucepan, boil the milk, butter and cream. 
  4. Pour this mixture on the chocolate. Mix gently, making sure not to make bubbles. 
  5. Beat the eggs lightly, then add them to the mixture. 
  6. Pour this mixture into ramekins. 
  7. cook in a bain-marie for 25 to 30 minutes 


