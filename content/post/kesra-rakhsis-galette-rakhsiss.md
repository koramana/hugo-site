---
title: kesra rakhsis - galette rakhsiss
date: '2018-04-22'
categories:
- Mina el forn
- bread, traditional bread, cake
tags:
- Boulange

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/kesra-rakhsiss-1.jpg
---
![kesra rakhsis 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/kesra-rakhsiss-1.jpg)

##  kesra rakhsis - galette rakhsiss 

Hello everybody, 

_I like the galette rakhssis or kesra rakhsis, but I never liked cooking on my electric crepiere because I have no gas at home in England. even it is the kesra is too good, I found that the cooking was not uniform until inside. But I confess my friends that I bought myself the electric tagine with the special stone Matlou3, and since my kesra rakhsis or rekhsis is perfect since then as successful as those cooked on gas._

_the recipe in Arabic:_

_Ingredients for 6 cakes (copy / paste at kouky)_

**kesra rakhsis - galette rakhsiss**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/kesra-rakhsiss.jpg)

portions:  6  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients** for 6 patties: 

  * 4 bowls of fine semolina (1bol = 400ml) 
  * 1 glass of oil or melted smen (1 glass = 250ml) me I put only 180 ml of table oil. 
  * 3 tablespoons milk powder 
  * 1 tablespoon instant dry yeast 
  * 1 tablespoon of sugar 
  * 1 tablespoon salt 
  * 3 tablespoons sesame seeds, not put 
  * 1 tbsp of orange blossom water or rose (optional) not put 
  * 2 bowls of lukewarm water (+ or- depending on the quality of the semolina) 



**Realization steps**

  1. Put the sieved semolina in a gas3a or a large dish, add all the dry ingredients and mix 
  2. Add the melted oil or smen and work with the palms of the hands so that the semolina absorbs the fat 
  3. Gradually wet with lukewarm water by working with fingertips to obtain a firm paste 
  4. Collect the dough without unduly kneading it and let it rest for a moment covered so that the semolina absorbs the water and becomes more manageable 
  5. Resume the dough, work it so that it becomes smooth and homogeneous, but not too much so that it remains melting   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/rakhssis_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/rakhssis_thumb1.jpg>)
  6. Shape 6 balls, place them on a tray spacing them a little and cover with a clean cloth 
  7. Light the fire and heat the tagine 
  8. Roll out the first ball to make a thin slab, pierce it and place it on a clean cloth. Do the same for other patties. They rest the time that the tagine heats. 
  9. Cook them on a hot tagine on both sides   
![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/kesra-rakhsiss-3.jpg)
  10. Let cool a little and cut and regale you 



![kesra rakhsis - galette rakhsiss](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/kesra-rakhsiss-2.jpg) _merci pour vos commentaires._
