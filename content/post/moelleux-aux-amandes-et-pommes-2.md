---
title: Fluffy with almonds and apples (2)
date: '2011-06-04'
categories:
- boissons jus et cocktail sans alcool
- Cuisine saine

---
yes there is already such a recipe on my blog, that of Louiza, that you will find it. when the one I took it from lakbira, going to visit the blog agenda (this blog is sublime because it is a summary to all the delights post by the best bloggers), the difference between the two I think it's cream in the place of yoghurt, in any case, it is a delight that I strongly advise you, so much so that it is very very bonnnnnnnnnnnnn. the recipe copy / paste with all my respect for our dear & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.43  (  2  ratings)  0 

the recipe copy / paste with all my respect for our dear Lakbira: 

** 1 briquette of fresh liquid cream (20 cl, I think I made more or the texture even softer, but delicious)  **

** \- 2 glasses of powdered sugar (a glass = 150 ml of capacity, ie a water glass!)  **

** \- 4 eggs  **

** \- 2 glasses of flour  **

** \- 1 sachet of 11 g of baking powder  **

** \- 1 glass of ground almonds  **

** \- a few drops of bitter almond extract (I do not tell you the taste it gives to the cake)  **

** \- 1/2 glass of oil  **

** \- To decorate the cake: a small handful of flaked almonds  **

** \- a little apricot jam to make it shine (optional)  **

** \- 3 big apples (I put 2) cut in small dice.  **

** PREPARATION:  ** (fast and without fatigue!)   
** In the bowl of a robot, put the cream, sugar and eggs and whip.  **

** Then add the almond powder and the bitter almond extract. Whisk.  **

**   
Then add, little by little, whipping, the flour and the yeast.  **

** Then add the oil. Whisk well, add the diced apples, stir and pour the dough into a buttered and floured baking tin, decorate the top with the flaked almonds!  **

** Cook for about 1 hour, watch for the cooking!  **

** As soon as it is cooked, wait for it to cool before unmolding. With a brush, brush with a little apricot jam if you want!  **

** merci pour vos commentaires et merci de bien vouloir vous abonnez a ma newsletter  **
