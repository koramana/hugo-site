---
title: Dacquoise cake and chocolate mousse
date: '2016-12-25'
categories:
- dessert, crumbles et barres
- cakes and cakes
- sweet recipes
tags:
- Mousse cake
- Dessert
- Pastry
- desserts
- Algerian cakes
- Confectionery

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/buche-dacquoise-et-mousse-de-chocolat.jpg
---
[ ![buche dacquoise and chocolate mousse](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/buche-dacquoise-et-mousse-de-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/buche-dacquoise-et-mousse-de-chocolat.jpg>)

##  Dacquoise cake and chocolate mousse 

Hello everybody, 

Dacquoise cake and chocolate mousse: A very nice dacquoise cake with hazelnuts, and chocolate mousse in the shape of a log. It's not a recipe for me, but a recipe from Claudine Cook, who shared with us her recipe. It's a pleasure to receive another recipe, and this is what Claudine Cook tells you. 

Hello Soulef, 

Finally the recipe that I wanted to share with you, the dacquoise cake with hazelnuts, and its chocolate mousse. This recipe is also coming from your blog, but I can not find it again. I realize this cake all the time, but this time it was to give it to my mother, who has been in the hospital for a while. There was a little party recently, and it was one of my contributions, I knew it was not a recipe I could have missed. 

According to my mother, the cake had a big effect, and everyone asked for the recipe, so STP Soulef, can you put the recipe on your blog, so that I pass the link to people who want the recipe. 

Note: I take these photos with my Samsung galaxy, hence the quality not at the top of my photos 

So this dessert of Claudine is a version of the royal chocolate, then to you the recipe: 

[ ![trianon](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/buche-trianon.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/buche-trianon.jpg>)   


**Dacquoise cake and chocolate mousse**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gateaun-dacquoise-mousse-au-chocolat.jpg)

portions:  12  Prep time:  60 mins  cooking:  15 mins  total:  1 hour 15 mins 

**Ingredients**

  * [ Dacquoise with hazelnuts ](<https://www.amourdecuisine.fr/article-dacquoise-aux-noisettes.html> "Dacquoise with hazelnuts")

for the cereal crunch 
  * 200 gr of [ homemade praline paste ](<https://www.amourdecuisine.fr/article-pate-de-praline-maison.html> "homemade praline paste")
  * 40 gr of dark chocolate 
  * 90 gr Cereals cornflakes 

for chocolate mousse: 
  * 225 gr of 70% dark chocolate 
  * 500 ml whole cream 



**Realization steps**

  1. Start by preparing the praline paste 
  2. prepare the biscuit [ dacquoise with hazelnuts ](<https://www.amourdecuisine.fr/article-dacquoise-aux-noisettes.html> "Dacquoise with hazelnuts")

the cereal crunch: 
  1. place the chocolate in a bowl and melt in the microwave while watching and stirring. 
  2. pass the praline paste to the robot so that it becomes even more liquid. 
  3. add the melted chocolate and continue to whisk for a few seconds.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-praline-et-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/pate-praline-et-chocolat.jpg>)
  4. Gently stir in the cereals and mix with a spoon to coat well.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/cereal-et-praline.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/cereal-et-praline.jpg>)

prepare the chocolate mousse: 
  1. Melt the chocolate in a bain marie. Let it warm up. 
  2. Put the cream in whipped cream. 
  3. mix the cream with the chocolate to obtain a homogeneous mixture. 
  4. Assembly of the cake: 
  5. using a pastry bag containing the chocolate mousse, fill the ⅓ of your mold (almost half of the chocolate mousse) 
  6. pour over the crunchy cereal and adjust with the back of a spoon. 
  7. cover again with the chocolate mousse. 
  8. Add the size of the dacquoise biscuit to cover the chocolate mousse. 
  9. remove the dacquoise, press lightly to adhere well. Reserve in the refrigerator. 
  10. Unmount after several hours in the fridge, sprinkle with cocoa, decorate according to your taste. 



[ ![Dacquoise dessert and chocolate mousse](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dessert-Dacquoise-et-mousse-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dessert-Dacquoise-et-mousse-au-chocolat.jpg>)
