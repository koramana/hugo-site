---
title: honey almond yogurt cake, financial way
date: '2015-06-28'
categories:
- cakes and cakes
- sweet recipes
tags:
- Algerian cakes
- Pastry
- Ramadan
- Ramadan 2015
- To taste
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-au-yaourt-amandes-miel-fa%C3%A7on-financier-2.jpg
---
[ ![almond yogurt cake honey way financial 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-au-yaourt-amandes-miel-fa%C3%A7on-financier-2.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-amandes-miel-facon-financier.html/gateau-au-yaourt-amandes-miel-facon-financier-2>)

##  honey almond yogurt cake, financial way 

Hello everybody, 

You like yoghurt cakes, is not it, it's always a pleasure to achieve a [ Yogurt cake ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-moelleux.html>) , easy ingredients, and success all the time. 

I hope this recipe will please you, and here are some other yoghurt cake recipes: 

[ cake with strawberry yoghurt ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-aux-fraises.html>) , [ yogurt cake with cherries and almonds ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-cerises-amarena-et-amandes.html>) , [ Chocolate Yogurt Cake ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-au-chocolat.html>) , [ chocolate yoghurt cake without egg ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-sans-oeufs-recette-delicieuse-108236853.html>) .... 

[ ![almond yogurt cake honey way financial 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-au-yaourt-amandes-miel-fa%C3%A7on-financier-1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-amandes-miel-facon-financier.html/gateau-au-yaourt-amandes-miel-facon-financier-2>)   


**honey almond yogurt cake, financial way**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-au-yaourt-amandes-miel-fa%C3%A7on-financier-3.jpg)

**Ingredients**

  * 165 g egg whites 
  * 125 g half-salted butter 
  * 2 jars of sugar (250 g) 
  * 2 pots of flour (preferably organic) 
  * 1 tbsp. coffee baking powder 
  * 2 jars of almond powder (125 g) 
  * 1 jar of 125 g of organic whole milk yoghurt 
  * 3 tablespoons melted honey and warmed 

Decor : 
  * 60 g chopped almonds 
  * 4 tablespoons of honey 



**Realization steps**

  1. Preheat the oven to 170 ° C. 
  2. Melt the butter in a saucepan and let cool. 
  3. Separate egg whites ⅓ - ⅔ from their weight. 
  4. Mount ⅔ of whites in snow. 
  5. Add 1 jar of sugar and meringue. Book. 
  6. In another bowl, mix the 2nd jar of sugar, honey, remaining whites, melted butter and yoghurt. 
  7. Add flour (sifted is better), baking powder and almond powder. 
  8. Pour over the meringue whites and mix with a spatula. 
  9. You get a nice mousse. 
  10. Line your baking pan with parchment paper (or butter and flour). 
  11. Distribute the preparation evenly. 
  12. Sprinkle evenly with chopped almonds. 
  13. Bake for 1 hour. 
  14. minutes before the end of cooking, heat the 4 tablespoons. honey, and brush the surface of the cake with a brush, and put back in the hot oven for 5 minutes. 



[ ![yogurt cake almonds honey way financial_](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/g%C3%A2teau-au-yaourt-amandes-miel-fa%C3%A7on-financier_.jpg) ](<https://www.amourdecuisine.fr/article-gateau-au-yaourt-amandes-miel-facon-financier.html/gateau-au-yaourt-amandes-miel-facon-financier_>)
