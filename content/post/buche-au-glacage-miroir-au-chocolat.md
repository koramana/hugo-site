---
title: beehive with glaze chocolate mirror
date: '2012-12-22'
categories:
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/biscuit-roule-buche-de-noel-au-chocolat.CR2_211.jpg
---
![biscuit-rolls - christmas-noel-au-chocolat.CR2 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/biscuit-roule-buche-de-noel-au-chocolat.CR2_211.jpg)

Hello everybody, 

here is a chocolate log that I realized for a friend ... .. 

the recipe to come with a video as soon as possible. 

![buche-au-chocolat.CR2-001 thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/buche-au-chocolat.CR2-001_thumb4.jpg)

recipe: [ rolled biscuit / Christmas log with chocolate frosting ](<https://www.amourdecuisine.fr/article-biscuit-roule-buche-de-noel-au-glacage-au-chocolat-113700698.html> "rolled biscuit / Christmas log with chocolate frosting")
