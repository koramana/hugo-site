---
title: smoothie has the avocado
date: '2015-11-24'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-davocat-.jpg
---
[ ![smoothie has the avocado](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-davocat-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-davocat-.jpg>)

##  Avocado smoothie 

Hello everybody, 

a delicious avocado smoothie For my breakfast, I do not find more smooth than a smoothie, and not to fall into the routine, I realize every morning a different smoothie ... This time, it is by looking this little lawyer in my fridge, that I decided that my smoothie will be based on avocado ... 

a little research in one of my books, and here I am with a recipe for which I have all the ingredients. (I always look for a smoothie recipe at night, so that in the morning, I get up, my ingredients ready, if I need to put one of the ingredients in the freezer, I do it at night) and frankly, take a smoothie the morning for me, it's the best thing I can take. 

I know it's easier to make a morning coffee, a juice, then a smoothie ... But for me, it's my little moment of pleasure, especially if I do that, before the children wake up. This is my moment to me in front of all this creaminess and sweetness and this smoothie has the lawyer is just too good. 

**smoothie has the avocado**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-davocat-1.jpg)

portions:  1  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 1 Lawyer 
  * [ pear and orange juice ](<https://www.amourdecuisine.fr/article-jus-de-poires-a-la-centrifugeuse.html>) . 



**Realization steps**

  1. in the blinder bowl, place the avocado in pieces, then add pear juice in small amounts 
  2. Shield, or mix and add the juice, until you have the consistency for your smoothie. 



[ ![smoothie has the avocado](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-davocat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-davocat.jpg>)
