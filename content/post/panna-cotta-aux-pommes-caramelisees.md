---
title: panna cotta with caramelized apples
date: '2016-01-28'
categories:
- panna cotta, flan, and yoghurt
- sweet recipes
tags:
- verrines
- Agar Agar
- creams
- Easy cooking
- Caramel

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/panna-cotta-aux-pommes-caram%C3%A9lis%C3%A9es-1-001.jpg
---
[ ![caramelized apple panna cotta 1-001](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/panna-cotta-aux-pommes-caram%C3%A9lis%C3%A9es-1-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/panna-cotta-aux-pommes-caram%C3%A9lis%C3%A9es-1-001.jpg>)

##  panna cotta with caramelized apples 

Hello everybody, 

Since my children discovered panna cotta, they keep asking me and asking for this dessert again. They prefer it by far to the Flans, 

it must never be missed in the fridge. I love to do what they like, but I also like to present their favorite dessert differently each time. You can see from elsewhere [ chocolate panna cotta ](<https://www.amourdecuisine.fr/article-panna-cotta-au-chocolat.html>) that I like very much, the [ coffee panna cotta ](<https://www.amourdecuisine.fr/article-panna-cotta-au-cafe-a-l-agar-agar.html>) the favorites of my husband, the [ panna cotta with chestnut cream ](<https://www.amourdecuisine.fr/article-panna-cotta-a-la-creme-de-marron.html>) , panna cotta an orange (that I have not posted yet because the photos are trapped in my hard drive that I still have not recovered) ... There are still plenty of [ panna cotta recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=panna+cotta&sa=Rechercher>) if you like, and there will be future recipes. 

[ ![panna cotta caramelized apple](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/panna-cotta-pomme-caramelisee.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/panna-cotta-pomme-caramelisee.jpg>)   


**panna cotta with caramelized apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/panna-cotta-aux-pommes-caramelis%C3%A9es-e.jpg)

portions:  8  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 600 ml whipping cream 
  * 320 ml of milk 
  * 100 g of sugar 
  * 2 gr of agar-agar (or 2 sheets of gelatin) 
  * 1 teaspoon powdered vanilla 

for caramelized apples: 
  * 4 apples 
  * 50 gr of brown sugar 
  * 50 gr of butter 
  * 1 lemon 



**Realization steps**

  1. In a saucepan, mix the milk cream, sugar and vanilla. 
  2. Heat 2 minutes and add the agar-agar mixed with a little powdered sugar. Boil 1 min. 
  3. Pour into cups and let cool to room temperature then put in the fridge. 

caramelized apples: 
  1. Peel, seed the apples and cut into cubes. 
  2. Cook them in the pan with the butter, sprinkle with brown sugar and let caramelise. 
  3. Remove the apples, deglaze the pan with lemon juice and pour over the apples. 



[ ![panna cotta with caramelized apples](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/panna-cotta-aux-pommes-caramelis%C3%A9es-a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/panna-cotta-aux-pommes-caramelis%C3%A9es-a.jpg>)
