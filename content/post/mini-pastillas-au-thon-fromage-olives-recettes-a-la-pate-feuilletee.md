---
title: mini pastillas with tuna cheese olives recipes with puff pastry
date: '2013-01-28'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mini-pasty6_thumb1.jpg
---
##  mini pastillas with tuna cheese olives recipes with puff pastry 

Hello everybody, 

It's super easy to make these mini olive tuna pastillas, that's what I often prepare to go fast with a flaky pastry of commerce, when I want to present an easy and simple entry to the menu. 

The result is always satisfying because these mini tuna pastillas with olive tuna are just beautifully delicious. You can even present them with a simple salad, or a varied salad and it will stall your guests. 

The recipe in Arabic: 

**mini pastillas with tuna cheese olives recipes with puff pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mini-pasty6_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 1 rolls of puff pastry 
  * 1 half onion 
  * 1 clove 
  * 1 to 2 fresh tomatoes 
  * some tuna 
  * grated cheese 
  * pitted green olives 



**Realization steps**

  1. fry the onion, wash and slice in a little oil, add the crushed garlic, add the chopped tomato, and cook until the sauce is reduced.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mini-pasty1_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mini-pasty1_thumb1.jpg>)
  2. cut the puff pastry into small squares, add a little tomato onion sauce, add a little tuna, grated cheese, and olives cut in slices.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mini-pasty2_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mini-pasty2_thumb1.jpg>)
  3. close the dough on itself, decorate the ends with a fork, brush with egg yolk, and sprinkle with a little seed of nigella (optional)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mini-pasty4_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/mini-pasty4_thumb1.jpg>)
  4. cook in a hot oven. 



bonne dégustation. 
