---
title: homemade pesto
date: '2012-11-24'
categories:
- cuisine algerienne
- cuisine diverse
tags:
- Christmas

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/pesto-au-basilic-300x225.jpg
---
##  homemade pesto 

Hello everybody, 

A homemade pesto recipe that I often use in my recipes, and I noticed that I did it on my blog, so quickly to your armor. In Italian cuisine, pesto is used in pasta dishes or with grilled vegetables. Abroad, it can be used on pizzas or with steaks. 

In the province of Catania, in the place of basilica, _pistacchio verde_ . 

To prepare the _pesto_ , just work all the ingredients in a mortar or mixer. 

Basil should be freshly picked. The gables can be replaced by walnut kernels. Some prefer to use the _pecorino romano_ (strong and dry sheep cheese) Parmesan cheese. Pistou is a cousin of Provence _pesto_ , usually prepared without gables. It is the essential ingredient of the pesto soup. 

There are many other varieties of _pesto_ : _pesto Rosso_ (with fresh or dried tomato, also called _portofino_ ) _pesto_ winter (where basil is replaced by spinach) 

You can in any case see the delights made with this pesto: [ eggplant rolled with pesto ](<https://www.amourdecuisine.fr/article-roules-daubergines-au-pesto.html>) or [ palms with pesto ](<https://www.amourdecuisine.fr/article-palmiers-au-pesto.html>)

source: Wikipedia. 

**homemade pesto**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/pesto-au-basilic-300x225.jpg)

**Ingredients**

  * a bouquet of basil 
  * 27 gr of pine nuts 
  * 1 clove of garlic 
  * 25 g freshly grated Parmesan cheese 
  * 3 tablespoons olive oil 
  * salt pepper 



**Realization steps**

  1. in the bowl of the blinder, mix the basil leaves washed and drained with the garlic, the pine nuts and the olive oil and the Parmesan cheese. 
  2. Do not mix too much to reduce mashed pine nuts. 
  3. Add the oil to obtain the desired consistency, then season with salt and pepper.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/2012-03-19-pesto-au-basilic_thumb1.jpg)


