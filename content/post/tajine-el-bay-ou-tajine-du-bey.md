---
title: tajine el bay, or tajine of the bey
date: '2017-10-13'
categories:
- Tunisian cuisine
tags:
- ramadan 2017
- Ramadan
- Ramadhan
- Minced meat
- Express cuisine
- Tunisia

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tajine-el-bey-cuisine-tunisienne-1.jpg
---
![tajine el bay, or tajine of the bey](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tajine-el-bey-cuisine-tunisienne-1.jpg)

##  tajine el bay, or tajine of the bey 

Hello everybody, 

Here is a very beautiful and delicious Tunisian tajine recipe, tajine el bay, or tajine of the bey "tajine elbey". Who says Tunisian tajine, said dish with eggs, recipe totally different from the Moroccan tajine not to be confused. 

This tajine based on minced meat, spinach and ricotta is presented in three distinct layers, very pleasant visually, and a delight! We're not going to stop at one room, whatever it's really good, so great during Ramadan! 

**tajine el bay, or tajine of the bey**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tajine-el-bey-cuisine-tunisienne-2.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** layer of meat: 

  * 300 gr of minced meat 
  * 3 eggs 
  * 1 nice handful of grated cheese 
  * 1 onion 
  * 2 cloves garlic 
  * salt, black pepper, cumin and tabel 
  * olive oil 

layer of spinach: 
  * 2 spinach bunches (or frozen spinach 250 gr) 
  * 1 C. butter 
  * 2 egg yolk + 1 whole egg 
  * 1 handful of grated cheese 
  * black pepper and tabel. 

layer of ricotta: 
  * 250 g ricotta 
  * 1 handful of grated cheese 
  * 2 egg whites 
  * black pepper 



**Realization steps** prepare the first layer: 

  1. Brown the minced meat in a little oil, with the onion and crushed garlic. Salt and pepper, add cumin and tabel, mix well. 
  2. let the meat cook, remove and reserve. 
  3. Add 3 tablespoons of grated cheese and 3 to 4 eggs to the meat. 
  4. Butter a dish, pour the stuffing with the minced meat, 
  5. even out the surface with a spoon. Bake for 10 minutes at 180 ° C touch the surface should be strong enough, 
  6. In a skillet, fry the finely chopped spinach with a little butter until all the liquid has evaporated. 
  7. let cool then add 2 egg yolks and 1 whole egg, spices and grated cheese. 
  8. remove from the oven and add the spinach stuffing, cook for another 5 to 10 minutes. 
  9. Prepare the cheese filling by mixing the ricotta cheese, the grated cheese and the 2 egg whites. 
  10. then add this cheese stuffing on the layer of spinach and cook for another 10 minutes. 
  11. Let cool and then cut into diamonds or according to the shape you want 



![tajine el bey, Tunisian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tajine-el-bey-cuisine-tunisienne.jpg)
