---
title: tartlet with apples and turrón
date: '2016-10-24'
categories:
- sweet recipes
- tartes et tartelettes
tags:
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tartelette-aux-pommes-et-turr%C3%B3n-2-763x1024.jpg
---
![tart-with-apples-and-nougat-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tartelette-aux-pommes-et-turr%C3%B3n-2-763x1024.jpg)

##  tartlet with apples and turrón 

Hello everybody, 

Yum yum to these pretty apple tart and turrón! You will tell me but what is turrón? The turrón (in Spanish) or the turrón is a Spanish confectionery made with honey, sugar, egg white and whole or crushed almonds. In simpler is the nougat! 

I programmed you this lunetoiles recipe that she had shared with me in July 2013 (yes again a recipe forgotten in my archives) because I'm on vacation, neither the head to cook or to write recipes. I will be able to respond to your comments upon my return. 

Thank you for your visits and comments. 

![tart-with-apples-and-nougat-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tartelette-aux-pommes-et-turr%C3%B3n-1-758x1024.jpg)

**tartlet with apples and turrón**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tartelette-aux-pommes-et-turr%C3%B3n-3.jpg)

portions:  6  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * For 6 tarts: 
  * 300 g puff pastry 
  * 100 ml of milk 
  * a package of turrón of jijona (the soft and friable) so 150 gr 
  * 3 apples 
  * a little sugar and butter (about 2 tablespoons sugar and a knob of butter per tartlet) 

Finish: 
  * cracked alicante 
  * apricot jelly to glaze apples after cooking 



**Realization steps**

  1. In a saucepan, pour the milk and half of the turrón to gently melt on low heat until it melts, then add the other half and mix well until it becomes creamy, remove from heat and let cool. 
  2. Meanwhile garnish your dough molds, put in the fridge 10 min. 
  3. Garnish each bottom with turrón cream paste, peel the apples with a peeler and form a small flower with the peel and put it on the cream on the side, then cut strips of apples not too thin and garnish the tartlets. 
  4. Sprinkle with a little sugar and drop some of the butter, bake at 180 ° C for about 20 minutes, until the tarts are golden brown and the apples are lightly caramelized. 
  5. At the end of the oven, top with apricot jelly (very little) to bring a little shine, as long as the tartlets are hot, sprinkle with alicante turrón (the hard) coarsely crushed and serve lukewarm. 



![tart-with-apples-and-nougat](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tartelette-aux-pommes-et-turr%C3%B3n.jpg)
