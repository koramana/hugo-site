---
title: Coconut flan
date: '2017-11-29'
categories:
- panna cotta, flan, and yoghurt
tags:
- Healthy cuisine
- Ramadan
- Caramel

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-au-lait-de-coco-1-773x1024.jpg
---
![flan with coconut milk 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-au-lait-de-coco-1-773x1024.jpg)

##  Coconut flan 

Hello everyone, 

For a beautiful coconut milk flan, I absolutely recommend this recipe, especially with the thin layer of caramel. My children love this coconut flan, and claim it every time! Fortunately for me, that the recipe is very simple. 

Well yes, I would like that all recipes are as easy and simple as a caramel coconut flan, simple and fast, just take the time, and wait for it to cool before tasting. 

When I do it for my children, I usually present it in ramekins, so do not bother to unmold! Finally, this is not the case for everyone, my daughter always loves her flan on the plate, she begins to sip all the caramel flowing, then eat all the caramelized layer, then enjoy the rest! 

For people who like video recipes, here is the filmed recipe for this coconut flan: 

**Coconut flan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/flan-au-lait-de-coco-2.jpg)

**Ingredients**

  * a box of coconut milk (400 ml) 
  * 250 ml of milk 
  * 80 grs of sugar or according to taste (I remember to put 6 ca soups my beautiful daughter tasted each time the mixture, until you had the good taste not too sweet, so better than you test the taste, before l adding eggs of course, hihihihih) 
  * 4 eggs 
  * topping caramel ready, or 150 gr converted into caramel. 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. In a salad bowl, put the eggs, milk, sugar and coconut milk 
  3. Mix everything well. 
  4. Pour into a cake mold, a little caramel topping and pour the preparation of the custard. 
  5. Cook in a bain-marie for 30 to 45 minutes (or depending on your oven) 
  6. Let the flan cool in its mold 
  7. place in the fridge until serving. 
  8. to unmold it, pass the blade of a knife on the sides. 


