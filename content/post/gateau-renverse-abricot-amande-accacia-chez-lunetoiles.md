---
title: inverted cake apricot almond accacia at lunetoiles
date: '2010-07-19'
categories:
- cuisine algerienne
- cuisine diverse
- pizzas / quiches / pies and sandwiches

---
hello everyone, I love to start this new Muslim year, with a little greed, a beautiful cake fondant, very very delicious, which is only the base of the delicious cream almond or Frangipane 

in any case, you are lucky to see it's photo, because there is not a single end, everyone has to eat volente (must say that with us, it has never been the case, my children do not like eating cakes, but the one, well, they do not resist, and must say that it is really good, it melts in the mouth .................. ..hum 

you will need: 

125 gr of butter at room temperature 

125 gr of sugar 

1/2 teaspoon of almond extract 

100 gr of flour 

60 grs ground almonds 

1 teaspoon of baking powder 

2 eggs 

8 apricots fresh, or as for me in box 

2 tablespoons of apricot jam 

30 gr of almonds 

preheat the oven to 180 degrees. 

butter an oven-baking tin 20 to 23 cm (mine was 21 cm, and I preferred to chemise it with baking paper, and it was a mold with a removable base) 

whip the butter and sugar until you have a nice creamy consistency 

add almond extract and eggs one by one while whisking constantly. 

in another container, mix the flour, almonds and baking powder, then add this mixture gently to the butter cream. 

pour this mixture into your mold, trying to give it a uniform surface with the back of a spoon 

take the apricots, cut them in half, and go through them in the almond cream, cut surface and denoyautee upwards 

cook for almost 35 minutes or until golden brown. 

leave it in the mold for 10 minutes, then remove from the mold and leave to soften for 10 minutes on a rack 

at this time, heat the apricot jam with 2 teaspoons of water, then garnish your cake with this jam, and sprinkle with the tapered almonds 

TASTE ... .. 

I highly recommend this recipe is very good and not too expensive. 

thank you for your comments and thank you to everyone who subscribed to my newsletter, and all those who continue to subscribe. 

et si vous aimez cette recette votez pour elle sur hellocoton, ( en tout cas c’est un vote juste pour fan, donc c’est pas un jeu ou une competition) 
