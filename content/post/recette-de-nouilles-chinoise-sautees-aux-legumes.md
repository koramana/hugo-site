---
title: recipe of Chinese noodles sauteed with vegetables
date: '2018-02-28'
categories:
- diverse cuisine
- Cuisine by country
tags:
- Chinese New Year Challenge
- China
- Algeria
- dishes
- Wok
- Healthy cuisine
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/02/Recette-nouilles-chinoise-saut%C3%A9es-aux-l%C3%A9gumes-1.jpg
---
![Recipe Chinese noodles sauteed with vegetables 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/Recette-nouilles-chinoise-saut%C3%A9es-aux-l%C3%A9gumes-1.jpg)

##  recipe of Chinese noodles sauteed with vegetables 

Hello everybody, 

So I will not say more, but if you want to enjoy a good balanced dish that can be ready in less than 20 minutes, I recommend this recipe of Chinese noodles sauteed with vegetables! 

Yes, yes, it must be tried to see that it is ready, sometimes when I do not have fresh vegetables and I use frozen vegetables all cut in julienne, well, the dish of Asian noodles is vegetarian ready in 10 minutes. 

I was surprised when I realized my dish of Chinese noodles sautéed with some broccoli flowers, which my husband and son had eaten without making any remark. Usually I always make these stir-fried noodles with some nice shrimp or pieces of chicken breast, but this vegetable-based version surprised me with my little family. 

**recipe of Chinese noodles sauteed with vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/Recette-nouilles-chinoise-saut%C3%A9es-aux-l%C3%A9gumes-2.jpg)

**Ingredients**

  * 250 grams of Chinese noodles 
  * ¼ red pepper 
  * ¼ green pepper 
  * ¼ yellow bell pepper 
  * 1 carrot 
  * 1 clove of garlic 
  * some broccoli flowers 
  * 5 c. soup of soy sauce 
  * salt (according to your tastes) 
  * olive oil 



**Realization steps**

  1. Dip the Chinese noodles in boiling water for about 5 minutes (or depending on the method of preparation on your package) then drain them. 
  2. In a Wok or a deep pan, brown the peppers, carrot cut in julienne and the broccoli flowers in a little olive oil. 
  3. Cook over medium heat for a few minutes, 
  4. add salt and garlic, and let it come back for a few minutes 
  5. add the drained Chinese noodles, mix well, sprinkle with soy and cook for a few minutes. 



![Recipe Chinese noodles sauteed with vegetables 3](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/Recette-nouilles-chinoise-saut%C3%A9es-aux-l%C3%A9gumes-3.jpg)
