---
title: Sfenj - Algerian patisserie
date: '2012-04-12'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/khffaf-beignet-0321.jpg
---
& Nbsp; & Nbsp; hello everyone here is a sublime Algerian pastry, that I like to prepare when I have the bitterness of the wheat, a smell that I love when cooking these donuts, every time I make this recipe of a different way, for example I share with you also the recipe of Algerian sfenj flour, as I like American donuts, the famous donuts. not to mention also the pastry cream pastries, and you can have more choices on the donuts category and I go to the ingredients now and according to the recipe of my mother: 500 gr semolina warm water 1 and a half cents & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![khffaf-donut-032.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/khffaf-beignet-0321.jpg)

Hello everybody 

here is a sublime [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) , that I like to prepare when I have the bitterness of the wheat, an odor that I like when cooking these donuts, 

every time I make this recipe in a different way, for example I share with you also the recipe of [ Algerian sfenj ](<https://www.amourdecuisine.fr/article-beignets-khfaf-ou-sfendj-64651536.html>) to flour, as I like them [ american donuts ](<https://www.amourdecuisine.fr/article-donuts-faciles-et-delicieux-68158130.html>) , the famous [ donuts ](<https://www.amourdecuisine.fr/article-donuts-faciles-et-delicieux-68158130.html>) . not to mention also [ donuts with cream pastry ](<https://www.amourdecuisine.fr/article-27186708.html>) , and you can have more choices on the category [ donuts ](<https://www.amourdecuisine.fr/categorie-11700496.html>)   
and I go to the ingredients now and according to my mother's recipe: 

  * 500 gr of semolina 
  * lukewarm water 
  * 1 and a half instant yeast 
  * warm milk 
  * 1cc of salt (see less, you can test after it is the dough is good or needs more salt) 



mix semolina with salt and warm water, stir to have a handy dough and let stand. 

![S7302055](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/212177221.jpg) ![S7302056](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/212177481.jpg)

go back to our pasta and add water as you go along. 

![S7302057](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/212177771.jpg) ![S7302058](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/212178201.jpg)

add the yeast deluee in a little water to your dough, 

and then add the milk so the dough should be very soft and easy to handle 

![S7302060](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/212178351.jpg)

let the dough rise, and after heating a bath of oil. 

wet your doights lightly, and take a small ball of dough to shape it in a way to have a circle, and dip it in the oil. 

![S7302064](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/212179091.jpg) ![S7302063](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/212179371.jpg)

fry on both sides, drain your khefaf. 

to serve, you can cover them with icing sugar or crystallize, or even put them in honey, you can even consume them as simple, 

![khffaf-donut-029.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/khffaf-beignet-0291.jpg)

good tasting 

for another recipe for flour donuts 

I advise you: 

[ khfaf with flour ](<https://www.amourdecuisine.fr/article-beignets-khfaf-ou-sfendj-64651536.html>)

![donut-khefaf-060-1 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/beignet-khefaf-060-1_211.jpg)
