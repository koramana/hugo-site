---
title: Chocolate vanilla flan
date: '2014-02-10'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-vanille-chocolat-023_thumb.jpg
---
[ ![chocolate vanilla flan 023](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-vanille-chocolat-023_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-vanille-chocolat-023.jpg>)

Hello everybody, 

[ ![chocolate vanilla flan 010](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-vanille-chocolat-010_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-vanille-chocolat-010.jpg>)

**Chocolate vanilla flan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-vanille-chocolat-010_thumb.jpg)

Cooked:  Francaise  Recipe type:  Dessert  portions:  4  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients** first layer: 

  * 500 ml of milk 
  * 1 box of vanilla custard flan 
  * 1 cup of vanilla coffee 
  * 1 tablespoon of sugar 
  * 1 tablespoon of maizena 

2nd layer: 
  * 500 ml of milk 
  * 1 box of vanilla chocolate flavor 
  * 2 tablespoons sugar 
  * 2 tablespoons of cocoa 
  * 1 tablespoon of maizena 



**Realization steps**

  1. preparation: 
  2. put in a saucepan the ½ liter of milk vanilla flan powder, vanilla sugar, tablespoons of sugar and the tablespoon of cornflour. 
  3. bring to a boil over low heat, stirring. 
  4. Fill the cups half, with vanilla flan and let cool a few minutes ...: 
  5. prepare the second layer by putting ½ liter of milk with chocolate flan powder, cocoa, sugar and cornflour. 
  6. bring to a boil over low heat while stirring. 
  7. pour the chocolate custard over the first layer of vanilla flan and allow a few minutes to cool. 



[ ![chocolate vanilla flan 013](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-vanille-chocolat-013_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/flan-vanille-chocolat-013.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
