---
title: surprise pumpkin soup
date: '2012-11-18'
categories:
- amuse bouche, tapas, mise en bouche
- idee, recette de fete, aperitif apero dinatoire
- recettes salées

---
Hello everybody. 

Winter is already here, and today I do not tell you the cold that can be felt as soon as you tip your nose out, the best thing is to stay at home by the fire, and why not, concoct a nice soup, huuuuuuuuuuuuuum! 

This time I would like to stay in front of the fire and have this nice soup prepared by the care of our talented Lunetoiles, and in addition no need to dirty the tureens, hihihihihi 

I had already prepared a [ pumpkin soup ](<https://www.amourdecuisine.fr/article-soupe-de-potiron-58516712.html>) but I did not show it that way, hihihihi 

  
Ingredients: 

  * 1 whole pumpkin (about 1kg 500) 
  * 4 potatoes 
  * 1 onion 
  * 1 bouillon cube 
  * salt pepper 
  * water 
  * 20 cl of semi-thick cream 



Method of preparation: 

  1. Cut the pumpkin hat with a knife. Keep it. 
  2. Remove the seeds with a tablespoon. 
  3. Recover the pulpit of the pumpkin by scraping with a tablespoon, leave 1 cm of pulpit almost, it must stand because it will be used as a soup tureen. 
  4. In a casserole, put the flesh, the onion cut into slices and the potatoes cut into cubes. 
  5. Cover with water. Add salt, pepper and a bouillon cube. 
  6. Cook for 30 minutes. Let cool 15 to 20 min, then mix with the cream. 
  7. Pour into the pumpkin emptied. To serve! 



Thank you my dear for this beautiful presentation and delicious recipe. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
