---
title: sardines in tomato sauce
date: '2011-04-30'
categories:
- juice and cocktail drinks without alcohol
- Cuisine saine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sardine-en-sauce-tomate-031_thumb_111.jpg
---
![sardine in tomato sauce 031](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sardine-en-sauce-tomate-031_thumb_111.jpg)

##  sardines in tomato sauce 

Hello everybody, 

When I go to Algeria, I take full advantage of the sardine of the Mediterranean. I tried to cook sardines in England in all its sauces, but it never had the same taste as our pretty little sardine. 

The sardine in tomato sauce is one of my favorite dishes, my mother does it so well that I do not try to make the recipe myself, do not you notice that the recipes of our moms are always good ???   


**sardines in tomato sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sardine-en-sauce-tomate-032_thumb3.jpg)

**Ingredients**

  * 500 grs of sardines 
  * 4 cloves of garlic 
  * 1 small onion (very small or a quarter of a large onion) 
  * 4 beautiful tomatoes 
  * ½ tablespoon of tomato paste 
  * 1 bay leaf 
  * 1 little thyme 
  * black pepper 
  * cumin 
  * ginger 
  * salt 
  * 3 tablespoons oil 
  * ½ glass of water 



**Realization steps**

  1. put the oil in a small pot. 
  2. in a pestle crush garlic, onion, cumin, salt and spices, 
  3. fry this mixture in the oil. 
  4. crush and cut tomato, add to this meal and cook a little, add tomato paste, bay leaf and thyme. 
  5. cook a little add ½ glass of water. 
  6. clean the sardines, remove the head and stop it, if you can not stand too, remove the tail. 
  7. and start putting the sardines in the sauce, placing 3 to 5 sardines on top of each other. when the sauce starts to boil. 
  8. cook for a maximum of 15 minutes over low heat. 
  9. and garnish with a little parsley, and why not a lemon juice. 



regalez vous et à bientot, bisous 
