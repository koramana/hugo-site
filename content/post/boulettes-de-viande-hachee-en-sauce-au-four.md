---
title: meatballs chopped in sauce, baked
date: '2013-03-19'
categories:
- jams and spreads
- basic pastry recipes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/riz-aux-boulettes-de-viande-hachee-010.CR2_thumb1.jpg
---
##  meatballs chopped in sauce, baked 

Hello everybody, 

Delicious and tender meatballs in sauce, which I bake to accompany my [ rice (how to cook rice) ](<https://www.amourdecuisine.fr/article-comment-reussir-la-cuisson-du-riz.html>) or just to eat with a nice little piece of bread. 

**meatballs chopped in sauce, baked**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/riz-aux-boulettes-de-viande-hachee-010.CR2_thumb1.jpg)

**Ingredients**

  * 500 gr of minced meat 
  * 50 gr bread crumbs dipped in milk (which will give the tender side to minced meat) 
  * 2 crushed garlic cloves 
  * salt, black pepper. 
  * 1 pinch of cumin 
  * 1 onion 
  * 1 green pepper 
  * 1 red pepper (not spicy for me, but if you like spicy, put in a little) 
  * 1 can of chopped tomatoes, otherwise 4 fresh tomatoes 
  * olive oil 
  * ½ glass of water. 
  * cooked rice 



**Realization steps**

  1. We start with the meatballs, we mix the minced meat, garlic, crumb of bread drained of its milk, spices, and cumin. 
  2. form medium meatballs and leave aside. 
  3. in a baking pyrex mold, preferably rectangular, put a little oil. 
  4. cut the onion into strips, the peppers too, and put everything in the mold. 
  5. add the chopped tomato, crushed garlic clove, salt and black pepper. 
  6. mix well and garnish with the meatballs. 
  7. Pour on half the glass of water (about 120 ml). 
  8. cover the pyrex molds with aluminum foil, and bake in preheated oven at 200 degrees C, for 20 to 30 minutes, or depending on the capacity of your oven 
  9. if you like to have a lot of sauce to accompany your rice, get out of the oven. otherwise if you like a creamy sauce, remove the aluminum foil, and let reduce the sauce. 
  10. serve hot with a little rice, or even mashed potatoes. 


