---
title: Boureks with spinach and tuna
date: '2012-06-25'
categories:
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/bourak-aux-epinards-et-thon-a_thumb1.jpg
---
##  Boureks with spinach and tuna 

Hello everybody, 

This week I run the risk of being overwhelmed, and being less present on my blog, and suddenly on your blogs, and also I can not answer your comments because I have a review to pass. 

At least, I have prepared some recipes, waiting for my return .... 

And I start with a few entries, that I prepared last week with my friend, for the circumcision of his son, we had prepared a nice buffet, but pity the battery of my device was discharged at the right time (as it always has to happen, and at those moments, forget it, either the second battery or the charger, and in my case it was both, hihihihi) 

Fortunately, I had photos during the preparation of these beautiful entries. 

For a start, I'm mailing you these delicious spinach and tuna boureks, well, the guests really liked, because they never ate spinach bouraks, besides, they were amazed every time I told them than those with spinach and tuna.   


**Boureks with spinach and tuna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/bourak-aux-epinards-et-thon-a_thumb1.jpg)

portions:  4  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients**

  * Brick leaves 
  * spinach (frozen in this recipe) 
  * A can of tuna with oil 
  * a handful of pitted green olives 
  * portioned cheeses (according to taste) 
  * 1 onion 
  * A pinch of black pepper 
  * A pinch of powdered cumin 
  * A pinch of salt (I did not put it because the taste was ideal) 



**Realization steps**

  1. take the spinach out of the freezer at least 2 hours before. 
  2. cut the onion into thin strips and sauté in the oil (which I drain from the can of tuna) 
  3. when the onions become translucent, add the spinach, and let sweat well. 
  4. add cumin and black pepper. 
  5. let cool, and add the cheese in pieces, the olives cut, and the tuna. 
  6. shuffle a little and fill in the brick leaves, to have triangles, or if you want cigars. 
  7. paste the end with a little egg white 
  8. cook in a bath of hot oil, until the bouraks take a nice color. 


