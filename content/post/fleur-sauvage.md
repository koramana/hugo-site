---
title: wild flower
date: '2011-09-02'
categories:
- Café amour de cuisine

---
Hello everybody, 

here again is a recipe that participates in the contest: Ramadan entries, recipe received before the help from Elihalger, but also that I did not have time to publish. 

so thank you elihalger, and good luck at the contest   


**wild flower**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/image_thumb1.png)

Recipe type:  appetizer, aperitifs  portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * Puff pastry, 
  * Tomatoes cut into slices, 
  * Oregon, salt, pepper, 
  * Onion cut in strips, 
  * Slices of cheese (I used Gouda), 
  * Anchovy, 
  * olives 
  * capers, 
  * egg. 



**Realization steps** The shaping is very simple and easy: 

  1. Preheat your oven to 190 degrees C. 
  2. Spread the dough and cut the squares. 
  3. On each laminated square, order the above ingredients. 
  4. Fold the dough like an envelope but without rubbing the dough between it. 
  5. Brush the surface with an egg sprinkle with sesame seeds. 
  6. Bake until golden brown. 



merci pour vos visites et commentaires, et bonne journée. 
