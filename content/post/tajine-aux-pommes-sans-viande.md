---
title: tagine with apples, without meat
date: '2013-01-20'
categories:
- gateaux algeriens au glaçage
- gateaux algeriens- orientales- modernes- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tajin-hlou-11.jpg
---
![apple tajine](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tajin-hlou-11.jpg)

Hello everybody, 

[ a vegetarian dish ](<https://www.amourdecuisine.fr/categorie-12348553.html>) , which will really please you, a great apple tajine, without chicken, without meat, just perfumed with the good taste of the star anise, a delight to taste with a good [ homemade bread ](<https://www.amourdecuisine.fr/article-comment-faire-son-pain-maison-114167181.html>)

ingredients: 

  * 2 apples cut in quarters 
  * 1 handful of raisins 
  * 4 tablespoon of sugar 
  * 2 tablespoons orange blossom water 
  * 250 ml of water 
  * 1 sachet of vanilla sugar 
  * 1 cinnamon stick 
  * 3 to 4 anise stars 
  * 1 teaspoon cinnamon powder 
  * 1 little saffron 
  * 50 g of butter 
  * some whole almonds, sliced ​​and fried (I did not have any) 



method of preparation: 

  1. in a Dutch oven to warm the butter, add, water, cinnamon, sugar, vanilla sugar, orange blossom water 
  2. put the raisins in a little water to inflate 
  3. boil then add the apple wedges 
  4. then add the well-filled grapes. 
  5. leave until the apples are melted and the sauce is creamy 
  6. add a little water if necessary and rectify the sugar if you like sweet 
  7. before serving add some whole toasted almonds or fries 
  8. enjoy with a good homemade bread 



et saha ftourkoum 
