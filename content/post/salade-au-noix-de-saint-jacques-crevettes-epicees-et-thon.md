---
title: scallop and spicy shrimp salad with tuna
date: '2013-12-09'
categories:
- diverse cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/proposez-votre-recette.bmp-1024x299.jpg
---
Hello everybody, Here is a recipe from a faithful reader of my blog Celeste, who used the new form, which I posted: [ ![suggest your recipe.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/proposez-votre-recette.bmp-1024x299.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/proposez-votre-recette.bmp.jpg>) it's very simple, just put your name, your email, go to the next page, and write the recipe, it's very simple, three of the readers have already used the form, but they find a little difficulty to insert the photo. It's super simple, just click on this icon: [ ![insert image.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/inserer-image.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/inserer-image.bmp.jpg>) It will be nice also to leave me a message with your email, so that I can contact you, because the first email you leave, it is not visible at home. We return to the recipe of Celeste, this salad with scallop, spicy shrimp and tuna. 

**scallop and spicy shrimp salad with tuna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/delicious_thumb21.jpg)

Recipe type:  salad, entree  portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * tuna 
  * 200 gr or 1 cup of Shells St. Jacques or scallops. 
  * 300 or 400 gr of shrimp. 
  * 1 large chopped onion 
  * 1 spoonful of oil 
  * 1 clove garlic chopped. 
  * 4- 6 slices of bread 
  * 1 cup of milk + 1 cup of water 
  * Salt pepper 
  * Cumin and a pinch of oregano 
  * 2 tablespoons parmesan 
  * 4-6 tablespoons chilli   
(or harissa) 



**Realization steps**

  1. In the bowl of a blender, mix the bread, milk and water. set aside   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/milkbread_thumb21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/milkbread_thumb21.jpg>)
  2. in a skillet with a little oil, fry the onion, crushed garlic, 
  3. add oregano, cumin and chilli and fry. 
  4. Then add the rest of the ingredients: raw shrimp, scallop, canned tuna, parmesan cheese and continue cooking for 4-5 minutes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/rawingrediennts_thumb21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/rawingrediennts_thumb21.jpg>)
  5. present with rice as we did, or with pasta. or as a flight to wind. 


