---
title: eggplant salad / oriental salad video
date: '2016-08-01'
categories:
- appetizer, tapas, appetizer
- Chhiwate Choumicha
- cuisine diverse
- dips and sauces
- idea, party recipe, aperitif aperitif
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/coco-crevette-et-riz-007.CR2_-1024x682.jpg
---
[ ![coconut shrimp and rice 007.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/coco-crevette-et-riz-007.CR2_-1024x682.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/coco-crevette-et-riz-007.CR2_.jpg>)

##  eggplant salad / oriental salad video 

Hello everybody, 

I share with you today this delicious recipe, which I realized a moment ago, but that I did not have the chance to publish ... 

it's never too late for you to have this delicious aubergine salad, very easy to make, and very rich in taste ... give it a try and tell me your opinion ... 

for the moment, I pass you the video of the recipe, and as soon as possible I put you the complete recipe ....    


**eggplant salad / oriental salad video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/coco-crevette-et-riz-007.CR2_-1024x682.jpg)

Recipe type:  salad, entree  portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 1 eggplant 
  * * a green pepper (if not ¼ of 3 multicolored peppers) 
  * taste 
  * * 1 garlic crushed 
  * * 1 teaspoons of tahini 
  * * 1 tablespoon of peanut butter 
  * * 2 tablespoons natural yogurt (if not according to your 
  * * salt to season. 



**Realization steps**

  1. the recipe is super detailed in the video below 



{{< youtube UHTy5frm56A >}} 

You can taste this salad as a sandwich with [ pita bread (Lebanese bread) ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html> "Lebanese bread, pita bread video") , or [ matlouh (matlou, matlou3) ](<https://www.amourdecuisine.fr/article-41774164.html> "galette Matlou3 - مطلوع Algerian Arabic bread")

J’attends vos commentaires et vos avis. 
