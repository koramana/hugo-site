---
title: briouates with chopped meat
date: '2015-06-19'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- Moroccan cuisine
- ramadan recipe
tags:
- Ramadan 2015

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/briouates-a-la-viande-hachee-1.CR2_.jpg
---
##  [ ![brioche with chopped meat 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/briouates-a-la-viande-hachee-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/briouates-a-la-viande-hachee-1.CR2_.jpg>)

##  briouates with chopped meat 

Hello everybody, 

Here briouates has minced meat, easy to make and above all too good, these briouates or as we like to call them bourek, boureq or bricks has minced meat passes very quickly with a beautiful [ harira ](<https://www.amourdecuisine.fr/article-harira-soupe-marocaine-du-ramadan-2014.html> "harira Moroccan soup of Ramadan 2014")

A recipe that I prepare often, when the weather is nice, to accompany a salad, or that never leaves our tables of Ramadan that is approaching at a great pace ... 

a recipe very easy to make, a crisp endless every time you crumble a bourek to arrive at this sweet generous layer of minced meat, very fragrant and delicious ....   


**briouates with chopped meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/bricks-a-la-viande-hachee.CR2_.jpg)

portions:  6  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * brick sheets 
  * 500g minced meat 
  * 1 chopped onion 
  * 1 bunch of parsley 
  * a few portions of cheese 
  * garlic powder, coriander powder 
  * Salt pepper 
  * 100 ml of water 
  * of table oil 
  * cooking oil 



**Realization steps**

  1. Brown the chopped onion in a little oil until it becomes translucent. 
  2. add the minced meat, the spices, the salt, and using a wooden spoon turn the meat so that it is well separated. 
  3. add the water if you think that the meat is not yet well cooked and cook until it is completely reduced. 
  4. off the heat, add the parsley and let cool 
  5. cut the bricks leaves in half. 
  6. place on each half of the leaf, the equivalent of a spoon of minced meat, and place on half of a portion of cheese. 
  7. fold the leaves on the stuffing, to have a triangle 
  8. cook in a warm oil bath on both sides, and place the cooked bricks on paper towels, let drain well. 



[ ![briouates with chopped meat.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/briouates-a-la-viande-hachee.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/briouates-a-la-viande-hachee.CR2_.jpg>)
