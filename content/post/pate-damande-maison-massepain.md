---
title: homemade almond paste - marzipan
date: '2014-11-22'
categories:
- basic pastry recipes
- sweet recipes
tags:
- Cakes
- Algerian cakes
- Pastry
- Easy cooking
- Without cooking
- Based

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/Pate-damande-maison.jpg
---
[ ![Homemade almond paste](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/Pate-damande-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/massepain-pate-damandes-fait-maison.jpg>)

##  How to make a homemade almond paste - marzipan / marzipan 

Hello everybody, 

I always made homemade marzipan, or as it is still called marzipan, to make my Algerian cakes as in the recipe [ honey pyramids ](<https://www.amourdecuisine.fr/article-les-pyramides-aux-amandes-gateaux-algeriens-2015.html> "the pyramids with almonds, Algerian cakes 2015") , [ the esses ](<https://www.amourdecuisine.fr/article-les-esses-gateaux-aux-amandes-gateau-algerien-2015.html> "Esses, cakes with almonds, Algerian cake 2015") , the [ mkhabez ](<https://www.amourdecuisine.fr/article-mkhabez-gateau-algerien.html> "mkhabez, Algerian cake") and many more [ Algerian cakes ](<https://www.amourdecuisine.fr/article-gateau-algerien-et-gateaux-orientals-pour-laid-el-fitr-2014.html>) . 

But what you do not know, and that same marzipan and the one that is also used for decorating birthday cakes, sometimes just to reduce the almond powder even more finely. And here is the recipe for homemade almond paste knowing that here I did not grind the almonds too finely, I used it as I bought it because simply this almond paste that I prepared does not will not be to decorate a cake, or so fill a delicious [ Stollen ](<https://www.amourdecuisine.fr/article-stollen-brioche-allemande-au-massepain.html> "Stollen German brioche with marzipan") . 

**homemade almond paste - marzipan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-damande-fait-maison.jpg)

portions:  covers a big cake  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 250 gr of ground almonds 
  * 210 gr of crystallized sugar 
  * 1 to 2 egg white 
  * a few drops of almond extract 



**Realization steps**

  1. place the almonds and the sugar in a salad bowl 
  2. add the first egg white with almond extract   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-damande-maison-massepain-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-damande-maison-massepain-1.jpg>)
  3. petrify by hand 
  4. If you think the dough is hard, add more egg white, you have to go slowly. 
  5. If you think that the dough is liquid by cons, add a little more almond, it will not change the taste or texture of marzipan. 
  6. roll in pudding and cover with cling film, and place in the fridge.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/massepain-fait-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/massepain-fait-maison.jpg>)
  7. This dough can be kept very well for 3 or 4 months in the fridge 


