---
title: cherry amandine pie
date: '2017-06-12'
categories:
- sweet recipes
- pies and tarts
tags:
- Algerian cakes
- Pastry
- Ramadan
- Ramadan 2015
- To taste
- desserts
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tarte-amandine-aux-cerises-1.jpg
---
[ ![cherry amandine pie 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tarte-amandine-aux-cerises-1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-cerises.html/tarte-amandine-aux-cerises-1>)

##  cherry amandine pie 

Hello everybody, 

I took advantage of the cherry season this year, we had the chance to find a merchant, who sold cherry cakes of varying quality, and at very reasonable prices. We ate fresh crunchy cherries in the mouth, a little juicy and sweet as it should, as we could find cherries of a taste bland, or even without taste. 

I had the chance to prepare delicious [ Cherry clafoutis ](<https://www.amourdecuisine.fr/article-clafoutis-aux-cerises-facile-et-rapide.html>) , a delicious treat [ cherry jam ](<https://www.amourdecuisine.fr/article-confiture-de-cerises.html>) and this amandine pie with cherries, which we liked a lot at home, was a success. 

With this recipe, I participated 2 years ago in our game: [ recipe around an ingredient # 7 the cherry ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-7-la-cerise.html>) this time, the theme of the round of which I am godmother is the [ apricot ](<https://www.amourdecuisine.fr/article-recettes-autour-dun-ingredient-30-labricot.html>) , and I'm still waiting for your registration !! 

kisses 

[ ![cherry amandine pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/tarte-amandine-aux-cerises.jpg) ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-cerises.html/tarte-amandine-aux-cerises-2>)   


**cherry amandine pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tarte-amandine-aux-cerises-3.jpg)

portions:  8  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html>)

for garnish: 
  * 3 teaspoons warmed [ cherry jam ](<https://www.amourdecuisine.fr/article-confiture-de-cerises.html>)
  * 250 g of pitted cherries cut in half 
  * 125 g of butter 
  * 125 g of sugar 
  * 2 eggs 
  * 125 g ground almonds 
  * 2 tablespoons flour. 
  * 4 tablespoons of milk 



**Realization steps**

  1. Start by making the dough, let it rest in the fridge for at least 20 minutes. 
  2. Spread the dough on baking paper, then go for it in a round pie dish of almost 22 cm in diameter, or a rectangular mold of almost 12 by 38 cm. 
  3. let the dough sit cool for almost 20 minutes. 
  4. Then cook in a preheated oven at 180 degrees for almost 15 minutes 
  5. Prepare the filling: 
  6. Beat the butter and sugar until lightly whipped cream 
  7. Add the beaten eggs one at a time. 
  8. then add the almond powder and the flour. 
  9. Add milk slowly while mixing 
  10. Divide the warmed jam over the base of the pie shell. 
  11. Veresez the almond cream mixture on top. 
  12. Arrange the half cherries on the surface. Do not overload them in the cream. 
  13. Bake the pie at 180 degrees C for 25 - 30 minutes. 
  14. Let cool before turning out on a dish. 
  15. Make yourself a good cup of tea and enjoy a slice of your pie, at least that's what I did. 



[ ![cherry almond tart 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/tarte-amandine-aux-cerises-4.jpg) ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-cerises.html/tarte-amandine-aux-cerises-4>)
