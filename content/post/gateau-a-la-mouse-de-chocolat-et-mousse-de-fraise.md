---
title: cake with chocolate mouse and strawberry mousse
date: '2007-12-13'
categories:
- crepes, donuts, donuts, sweet waffles

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200487721.jpg
---
![girl](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/197166931.gif)

Genoese : 

4 eggs   
120g caster sugar   
80g flour   
40g coconut   
a pinch of salt   
Beat the eggs with the caster sugar until the mixture doubles in volume and add the other ingredients. 

Put in a plate and bake 10min. 

Syrup: 

Prepare 250ml of syrup with water and sugar and divide in half 

Make the sponge cake with the first one and add 1càs of nescafé to the second and re-aroser the sponge cake 

![S7301578](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200487721.jpg)

Orange chocolate mousse: 

250ml fresh cream   
150g white cheese   
250g dark chocolate   
250ml orange juice   
2 egg yolks   
50g caster sugar   
3cas icing sugar   
1.5 c in powdered gelatin   
1 càs nescafé   
Add the crème fraîche in whipped cream with 2 tablespoons of icing sugar 

beat the white cheese with 1 tablespoon icing sugar 

beat the egg yolks with the caster sugar 

Dissolve the gelatin in 4 tables of cold water 

Put the orange juice on fire and let it boil, add the gelatine and the dark chocolate and the nescafe until they melt and add this mixture on the eggs while beating at the maximum speed, add the cheese white and continue beating, add the cream and beat. 

![S7301579](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200488901.jpg)

Put the mixture on the sponge cake and let it in the refrigerator. 

Strawberry mousse: 

  * 100g of cottage cheese 
  * 200g strawberries (I use the boxed strawberry) 
  * 100 ml of fresh cream 
  * 3 cases of icing sugar (or then according to your taste) 
  * 1.5 cases of vegetable gelatin dissolved in strawberry juice and boiled. 



start beating the white cheese and half of the galette sugar, then beat the fresh cream with the other half of the sugar, mix the two together, add the strawberry mix well, and beat all together, then add the gelly. pour all on the mousse, and put in the refrigerator. 

![S7301581](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200492831.jpg)

c’est le gateaux d’anniversaire de lyna 
