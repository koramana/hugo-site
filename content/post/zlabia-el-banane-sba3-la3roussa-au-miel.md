---
title: zlabia el banane sba3 la3roussa with honey
date: '2016-06-08'
categories:
- gateaux algeriens au miel
- fried Algerian cakes
- gateaux algeriens- orientales- modernes- fete aid
- recette de ramadan
tags:
- Algeria
- Ramadan 2016
- Algerian cakes
- desserts
- Ramadan
- Easy cooking
- Honey

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/zlabia-el-banane.jpg
---
![zlabia el banane](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/zlabia-el-banane.jpg)

##  zlabia el banane sba3 la3roussa with honey 

Hello everybody, 

During the holy month of Ramadan, zlabia is always present on our tables. The zlabia this pretty golden sweet fries fried in a hot oil then deceived in the [ homemade honey ](<https://www.amourdecuisine.fr/article-miel-maison-facile.html>) . No one can abstain and resist this delight. 

Today my friend Wamani Merou liked to share with us one of the varieties of Zlabia, Zlabia el banane, or saba3 la3roussa, or honey churros. But to be sharper, zlabia el banane does not look too much texture churros. The churros are donuts, while the zlabia el banane has a crunchy texture on the outside and melting in the heart, and with the honey that coats it, this mignardise accompanies well a mint tea well scented for a Ramadan night. 

**zlabia el banane sba3 la3roussa with honey**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/zlabia-el-banane-ou-sba3-la3roussa-au-miel.jpg)

Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 2 glasses of water 
  * ¼ glass of oil 
  * 1 pinch of salt 
  * 2 tablespoons of honey 
  * 1 packet of vanilla 
  * 2 glasses of flour 
  * oil for frying 
  * [ homemade honey ](<https://www.amourdecuisine.fr/article-miel-maison-facile.html>) to coat it 
  * crushed peanuts for garnish (optional) 



**Realization steps**

  1. place the water, oil, salt and honey in a saucepan over low heat. 
  2. when the mixture is homogeneous, introduce the flour into the rain and the vanilla. 
  3. mix over low heat to have a nice dough malleable that stands out well from the walls of the pan, 
  4. place the dough as long as it is hot in a piping bag and form a bead of almost 12 cm. 
  5. let the sausages cool before starting to fry. 
  6. coat the zlabia el banane directly in hot honey while they are hot. 
  7. sprinkle some crushed peanuts over it. 



![zlabia el banane 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/zlabia-el-banane-1.jpg)
