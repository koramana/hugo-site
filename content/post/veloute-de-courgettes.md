---
title: Cream Of Zucchini
date: '2012-04-25'
categories:
- Gateaux Secs algeriens, petits fours
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/veloute-de-courgettes1.jpg
---
**_![Cream Of Zucchini](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/veloute-de-courgettes1.jpg) _ **

##  Cream Of Zucchini 

Hello everybody, 

zucchini velvet, or zucchini cream, a recipe that I draw from my forgotten archives, and finally I'll post it, I do not give this recipe its true value, because I swear to you that it made the happiness of all the people around me. 

this velvet really deserves to be tasted. 

for more [ soups and velvets ](<https://www.amourdecuisine.fr/categorie-10678936.html>) .   


**Cream Of Zucchini**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/Veloute-de-Courgettes-1.jpg)

Recipe type:  soup, vegetarian cuisine  portions:  4  Prep time:  10 mins  cooking:  30 mins  total:  40 mins 

**Ingredients**

  * 500g of zucchini 
  * 1 large onion 
  * 1 potato 
  * 1 cube of chicken broth 
  * 1 tbsp. Butter 
  * water 
  * Pepper and just a little salt (depending on the bouillon cube) 
  * Fresh cream 



**Realization steps**

  1. In a casserole, brown the chopped veggies in a spoonful of butter. Lower the heat, and let it sweat slowly 
  2. Add the chicken stock cubes and cover the vegetables with hot water 
  3. Close the casserole and cook for ten minutes 
  4. Put the vegetables in a mixing bowl with some cooking water 
  5. Mix and add the cooking water if necessary, until you obtain a creamy creamy soup 
  6. Serve hot with a drizzle of fresh cream and freshly ground pepper 


