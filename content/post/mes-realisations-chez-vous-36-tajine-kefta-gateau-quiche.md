---
title: my achievements at home 36, tajine, kefta, cake, quiche
date: '2013-09-25'
categories:
- diverse cuisine
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

I put you, a system much faster so that you can send me your photos, just go on this page: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll like ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link of your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)

So we start our beautiful ride. 

![P1080812](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/87069649_p1.jpg)

![creme caramel au-au-butter-sale.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/creme-au-caramel-au-beurre-sale1.jpg)

![002](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/85514192_p1.jpg)

![https://www.amourdecuisine.fr/wp-content/uploads/2013/09/DSC_05571.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/DSC_05571.jpg)

![https://www.amourdecuisine.fr/wp-content/uploads/2013/09/87004892_p1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/87004892_p1.jpg)

![http://www.amour-de-cuisine.com/wp-content/uploads/2013/09/flan+au+lait+concentr%C3%A9+1.jpg](http://www.amour-de-cuisine.com/wp-content/uploads/2013/09/flan+au+lait+concentr%C3%A9+1.jpg)

![https://www.amourdecuisine.fr/wp-content/uploads/2013/09/88343699_p1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/88343699_p1.jpg)

![zitoune au poulet.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/zitoune-au-poulet1.jpg)

[ tajine with chicken balls and olives ](<https://www.amourdecuisine.fr/article-tajine-aux-boulettes-de-poulet-et-olives-plat-de-ramadan-2013-118668736.html>) at Ghania H (share on facebook) 

![kesra-rekhssis.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/kesra-rekhssis1.jpg)

[ kesra rakhsiss - galette rakhsiss ](<https://www.amourdecuisine.fr/article-kesra-rakhsiss-galette-rakhsiss-54714485.html>) at Ghania H (share on facebook) 

![basboussa.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/basboussa1.jpg)

[ Basboussa in video بسبوسة سهلة ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html>) at Habiba M 

![cake-magique.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/gateau-magique1.jpg)

[ magic cake with chocolate and caramel cream ](<https://www.amourdecuisine.fr/article-gateau-magique-au-chocolat-et-creme-caramel-107207656.html>) at Samira B 

![tajine-kefta.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tajine-kefta1.jpg)

![bavarois.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/bavarois1.jpg)

[ Bavarian speculoos strawberries ](<https://www.amourdecuisine.fr/article-bavarois-fraises-speculoos-70074240.html>) at Aziza D 

![Cake-a-la-pate-a-sucre.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/gateau-a-la-pate-a-sucre1.jpg)

[ the recipe for the cake with sugar dough ](<https://www.amourdecuisine.fr/article-37650983.html>) at Aziza D 

![arayeche modern-au miel.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/arayeche-moderne-au-miel1.jpg)

[ Arayeches with honey, Stars with almonds and honey Algerian cakes ](<https://www.amourdecuisine.fr/article-arayeches-au-miel-etoiles-aux-amandes-et-miel-60064269.html>) at Assia R 

![Bavarian-slitting speculoos.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/bavarois-fraises-speculoos1.jpg)

[ Bavarian speculoos strawberries ](<https://www.amourdecuisine.fr/article-bavarois-fraises-spec<br%20/><br%20/><br%20/><br%20/>%20uloos-70074240.html>) at Mimi D 

![kofta tagine-k-Assia-copy-1.jpeg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/tajine-kefta-Assia-k-copie-11.jpeg)

[ tagine of kefta with eggs ](<https://www.amourdecuisine.fr/article-tajine-de-kefta-aux-oeufs-112750436.html>) at Assia K 

![bouchee-crabe.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/bouchee-crabe1.jpg)

[ appetizer / crab bites with parmesan cheese ](<https://www.amourdecuisine.fr/article-bouchees-de-crabe-au-parmesan-101566392.html>) at Linda T 

![donut-of-cabbage-fleur.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/beignet-de-chou-fleur1.jpg)

[ cauliflower donuts ](<https://www.amourdecuisine.fr/article-beignets-de-chou-fleur-90899510.html>) r at Assia K 

It's not all my friends, I'm going to write the second article, I'll publish quickly later in the day, but if you do not want me to forget you completely, you can always send me your photo and the link to the recipe. 

Merci 
