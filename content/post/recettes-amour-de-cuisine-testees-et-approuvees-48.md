---
title: Kitchen love recipes tested and approved 48
date: '2015-01-17'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

Many of you have made my recipes. This article summarizes only a small minority among you, who took their pictures, thank you very much for your sharing, and thank you very much for having made my recipes ... Your comments are the proof ... It makes me a lot of pleasure every time that one of you comes to me to leave a comment saying that you have succeeded the recipe, and that it rained at your place. 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") . **

You can send the photo of your achievement with the link of the recipe you have tried (I am talking about the link from my blog) on ​​this email address: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

If you are a blogger, it will be nice to leave the link to your blog too, and if you are not a blogger, your name will be good enough. Also if you have tested and approved a recipe from my blog, and you have no photos, just leave a comment in the recipe in question. your opinion will surely help people who want to try the recipe. 

[ ![birthday cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-danniversaire-e1420371927601.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/gateau-danniversaire.jpg>)

[ Bavarian with vanilla mousse ](<https://www.amourdecuisine.fr/article-bavarois-biscuit-joconde-mousse-a-la-creme-custard.html> "bavarian biscuit joconde mousse with cream custard") (birthday cake) at Oumou Amir (facebook) 

[ ![hazelnut basboussa](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/basboussa-aux-noisettes-e1420371712386.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/basboussa-aux-noisettes.jpg>)

[ Hazelnut and orange basboussa ](<https://www.amourdecuisine.fr/article-basboussa-aux-noisettes-et-oranges.html> "Hazelnut and orange basboussa") at Jocelyne Berny (facebook) 

[ ![bread stuffed with minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pain-farcie-a-la-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pain-farcie-a-la-viande-hach%C3%A9e.jpg>)

[ breads stuffed with minced meat ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee.html> "buns with chopped meat") , at Arwa Naili (facebook) 

[ ![apple pie with butter](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tarte-aux-pommes-au-beurre.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tarte-aux-pommes-au-beurre.jpg>)

[ apple pie with butter ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-au-beurre-67056341.html> "Butter apple pie") at Najil Redjedal (facebook) 

[ ![butter tart shahira](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tarte-au-beurre-chahira.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/tarte-au-beurre-chahira.jpg>)

[ ![khobz edar without fluffing](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/khobz-edar-sans-petrissage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/khobz-edar-sans-petrissage.jpg>)

[ ![olive breads at Nicole's](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pains-aux-olives-chez-Nicole.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/01/pains-aux-olives-chez-Nicole.jpg>)

[ breads with olives ](<https://www.amourdecuisine.fr/article-pain-aux-olives.html> "olive bread") chez Nicole Gozzi ( facebook) 
