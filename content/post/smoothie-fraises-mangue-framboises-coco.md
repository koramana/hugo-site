---
title: coconut raspberry mango strawberry smoothie
date: '2014-06-20'
categories:
- juice and cocktail drinks without alcohol
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-mangue-framboises-coco-1.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-mangue-framboises-coco-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-mangue-framboises-coco-1.jpg>)

##  coconut raspberry mango strawberry smoothie 

Hello everybody, 

A recipe that I programmed to publish rather, but that I completely forgot, a raspberry and coconut mango strawberry smoothie, that shares with us Lunetoiles ... 

A rich and refreshing smoothie ... for my part, I already have a beautiful stock of strawberries, mangoes, raspberries, and seasonal fruits that disappear very quickly from our stalls ... so I have a good stock of these fruits in my freezer that way, this Ramadan, I will make a lot of smoothie rich in vitamins, to take the evening, to make up for the fasting of the day, lol ...   


**coconut raspberry mango strawberry smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-mangue-framboises-coco-2.jpg)

portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 1 box of mango syrup of about 500 gr 
  * About fifteen fresh strawberries washed, hulled cut into pieces 
  * 1 brick of 100% original coconut milk (250 ml) 
  * A big handful of frozen raspberries 
  * Optional ice cubes 



**Realization steps**

  1. Pour the mango box in the syrup into the blender with its juice. 
  2. Add the strawberries, frozen raspberries and ice cubes. 
  3. Pour about half of the coconut milk and mix. 
  4. Add as much coconut milk as you like. 
  5. Pour into glasses and decorate with grated coconut. 
  6. Enjoy! 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-mangue-framboises-coco.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/smoothie-fraises-mangue-framboises-coco.jpg>)

I hope you'll be lucky to try this delicious smoothie. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
