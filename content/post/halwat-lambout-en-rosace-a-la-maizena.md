---
title: halwat lambout in rosette with maizena
date: '2015-11-11'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
tags:
- Algeria
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/halwat-lambout-a-la-maizena-21.jpg
---
[ ![halwat lambut with maizena 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/halwat-lambout-a-la-maizena-21.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/halwat-lambout-a-la-maizena-21.jpg>)

##  halwat lambout in rosette with maizena 

Hello everybody, 

The lambout cake, halwat el lambout, or the petits fours à la pouch are cakes that have marked my childhood. My mother often made us to taste it ... Because it's a cake super easy to make, and it's an economic cake too ... 

But, I do not remember that my mother did it at the cornflower at the time .... Generally Algerian cakes were always flour-based ... And I confess that the first time I ate lambout cake with corn starch ... I did not really like it, the cake was too dry in the mouth. The neighbor then told me that she put half flour, half maïzena ... in my opinion it's too much corn, and even if the cake is going to be super fondant, but it will also be too dry in the mouth. 

By cons this recipe of my friend **Souma** I like it well, because there is only one glass of maïzena and the rest flour ... so we will have a cake super fondant, without being dry. 

**halwat lambout in rosette with maizena**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/halwat-lambout-a-la-maizena-11.jpg)

portions:  30  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 3 eggs 
  * 250 gr of icing sugar 
  * 250 ml of oil 
  * 1 glass of maizena (220 ml) 
  * 1 packet of baking powder 
  * 1 teaspoon of vanilla sugar 
  * flour to pick up the dough 

decoration: 
  * Strawberry jam 
  * pistachios powder. 



**Realization steps**

  1. whisk eggs, icing sugar, and vanilla sugar. 
  2. add the oil and continue to whip 
  3. then add cornflour delicately. 
  4. then the yeast and the flour little by little to have soft paste, which can be shaped with the lambout. 
  5. make rosettes directly on a baking tray, line with baking paper. 
  6. cook in a preheated oven at 180 degrees between 10 and 15 minutes, or until the cakes turn a light golden color. 
  7. after cooling decorate the cake with a little jam, and pistachio powder. 
  8. you can present the cakes in serrated boxes 


