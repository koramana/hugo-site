---
title: Chocolates filled with milk jam
date: '2015-12-09'
categories:
- dessert, crumbles and bars
- sweet recipes
tags:
- Confectionery
- Gourmet Gifts
- Delicacies
- delicacies
- Easy recipe
- Without cooking
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chocolat-four%C3%A9e-a-la-confiture-de-lait.jpg
---
[ ![chocolate stuffed with milk jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chocolat-four%C3%A9e-a-la-confiture-de-lait.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chocolat-four%C3%A9e-a-la-confiture-de-lait.jpg>)

##  Chocolates filled with milk jam 

Hello everybody, 

Today I share with you a recipe from my friend Tema from the **Tema's kitchen,** she has prepared chocolates filled with [ milk jam ](<https://www.amourdecuisine.fr/article-confiture-de-lait-dulce-de-leche.html>) , or caramelized Nestle. and this is what she tells us: 

Salem my greedy! I hope you're fine! .. recently I made chocolate hulls filled with caramelized Nestle or [ milk jam ](<https://www.amourdecuisine.fr/article-confiture-de-lait-dulce-de-leche.html>) . They were really succulent and sucking ...   
Without delay I go to the recipe: 

**Chocolates filled with milk jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chocolat-fouree-a-la-confiture-de-lait.jpg)

portions:  25  Prep time:  40 mins  cooking:  10 mins  total:  50 mins 

**Ingredients**

  * 300 g of grated chocolate (chocolate with black choices, milk, white ...) 
  * 1 box of caramelized Nestle, or [ homemade milk jam ](<https://www.amourdecuisine.fr/article-confiture-de-lait-dulce-de-leche.html>)
  * small silicone molds. 



**Realization steps**

  1. First, melt the chocolate in a bain marrie, respecting the [ tempering curves ](<https://www.amourdecuisine.fr/article-comment-temperer-le-chocolat.html>) . 

to make the hulls there are two ways: 
  1. either fill the molds with chocolate and then return them to a grid to empty the excess; scrape with a spatula and reserve in a cool place. 
  2. either brush the walls of the patterns with a brush, allow to harden. Put another layer to strengthen them 
  3. Using a spoon or pocket fill the chocolate with Nestle caramelized without going to the edge.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chocolat-four%C3%A9e-a-la-confiture-de-lait-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chocolat-four%C3%A9e-a-la-confiture-de-lait-1.jpg>)
  4. Cover with the remaining chocolate and gently pat the pan to remove air bubbles and smooth the surface. Scrape another time with the spatula and return to the refrigerator. 
  5. After cooling, and after having removed the filled chocolates from the molds, file the periphery of the chocolates with the rasp to have perfectly sharp edges. 



[ ![chocolate stuffed with milk jam 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chocolat-four%C3%A9e-a-la-confiture-de-lait-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/chocolat-four%C3%A9e-a-la-confiture-de-lait-2.jpg>)
