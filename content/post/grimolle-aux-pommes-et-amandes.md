---
title: grimolle with apples and almonds
date: '2018-04-24'
categories:
- cakes and cakes
- sweet recipes
tags:
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/grimolle-aux-pommes-et-amandes-1.jpg
---
![grimolle-to-apples-and-almond-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/grimolle-aux-pommes-et-amandes-1.jpg)

##  Grimolle with apples and almonds 

Hello everybody, 

Grimolle with apples and almonds, or grimole or grimollées with apples is a recipe that I discover for the first time. 

When Lunetoiles had given me the pictures of this apple and almond grimolle almost a year ago, I had forgotten the recipe in my archives. And here is Lunetoiles back today to remind me that I had not posted the recipe and especially that it is the season of this delicious fruit. 

She passed me the recipe under the name Grimolée aux pommes, I went to see on google, and there was actually three appointment to this delicious cake. The grimolle Poitevin is a recipe that comes from Poitou and was once cooked in a cabbage leaf. 

The Lunetoiles remark: 

> This is actually an old cake, which is a little forgotten in my opinion, because never seen before, it's an apple cake with a caramelized almond crust on top. It is very greedy, very easy to do and still not very expensive either. 
> 
> The recipe is given for a mold of 30 cm, but I made it in a mold of 22 cm and it was perfect. It is very very good and fluffy, I loved it. 

If you like apple recipes, you can try [ Apple pie ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-au-beurre-67056341.html>)

![grimolle-to-apples-and-almond-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/grimolle-aux-pommes-et-amandes-2.jpg)

**grimolle with apples and almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/grimolle-aux-pommes-et-amandes-5.jpg)

portions:  8  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients** Ingredients for a 22 cm pie plate 

  * 100g of flour 
  * 70 g brown sugar + 1 tsp. to s. 
  * ½ sachet of yeast 
  * 1 C. to c. vanilla sugar 
  * 1 egg 
  * 200 g of milk 
  * A little butter for the mold 
  * 4-5 apples (depending on the size) 

Ingredients for almond crust 
  * 50 g of half-salted butter 
  * 50 g of brown sugar 
  * 1 egg 
  * 125 g slivered almonds 



**Realization steps**

  1. Cut a round of parchment paper into the diameter of the pie plate. Place it in the bottom of the mold, butter lightly and sprinkle with 1 tablespoon of brown sugar. 
  2. Preheat the oven to 180 ° C. 
  3. In the bowl of the robot, combine the flour, sugar, yeast, vanilla sugar, egg, milk. 
  4. Mix a few seconds at medium speed. 
  5. Cut, peel and seed the apples. 
  6. Detail them in thin strips. 
  7. Put them in the previous mixture and mix. 
  8. Pour everything into the jacketed mold. 
  9. Smooth the surface with a spatula. 
  10. Bake the mold for 20 minutes. 
  11. Rinse the bowl quickly. 
  12. Put the butter to melt in a small saucepan. 
  13. Out of the heat add the sugar, beat the beaten egg and mix. 
  14. Pour the flaked almonds and mix for 10 seconds. 
  15. Take the cake out of the oven. The apples started to brown.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/grimolle-aux-pommes-et-amandes.jpg)
  16. Spread the almond mixture on the surface, spreading it over the whole cake. Smooth with a spatula. 
  17. Re bake for 12 to 15 minutes. The almonds should start to brown. 
  18. Let cool a bit before unmolding. 



![grimolle-to-apples-and-almond-4](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/grimolle-aux-pommes-et-amandes-4.jpg)
