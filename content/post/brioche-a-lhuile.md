---
title: Brioche with oil
date: '2015-03-23'
categories:
- Brioches et viennoiseries
tags:
- Viennese bread
- Algeria
- Bread
- Brioche bread
- Algerian cakes
- Boulange
- Bakery

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/brioche-a-lhuile-1.jpg
---
[ ![bun with oil 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/brioche-a-lhuile-1.jpg) ](<https://www.amourdecuisine.fr/article-brioche-a-lhuile.html/brioche-a-lhuile-1>)

##  Brioche with oil 

Hello everybody, 

Personally, I have never made a brioche oil, for me a brioche must always be butter ... But the recipe of my friend "My passion my cooking" will change my little ideas because at the sight of this magnificent me, gives me the desire to go immediately realize this beautiful wonder ... 

In any case after "My passion my cooking" this brioche and so light is rich in taste it disappears as soon as it comes out of the oven "and I do not doubt for a second. I hope you too will be trying to make this brioche, keep me informed if you do, you can send me the pictures of your essays on this email: 

Thank you to all those who do their best to share their delights on my blog, I am very honored ... And do not worry if you have not yet seen your recipe on my blog, I have a nice list of wait, I validate your recipes as you go, because I make small arrangements each time you post your recipes. but be sure, your recipes will be online very soon. 

Thank you.   


**Brioche with oil**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/brioche-a-lhuile.jpg)

portions:  10-12  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 4 eggs 
  * 1 glass of crystallized sugar 
  * 2 and ½ glass of warm water 
  * 1 and ½ glass of oil 
  * 1 glass of hot milk 
  * 1 tablespoon baker's yeast 
  * 1 packet of baking powder 
  * vanilla 
  * lemon zest 
  * Orange zest 
  * flour as needed 
  * a pinch of salt 



**Realization steps**

  1. mix all the ingredients except the flour let froth 10 min. 
  2. add the flour, the dough should stick between the fingers, let rise. 
  3. shape balls, then give them the look of a donut   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/brioche-a-lhuile-4-300x221.jpg) ](<https://www.amourdecuisine.fr/article-brioche-a-lhuile.html/brioche-a-lhuile-4>)
  4. arrange them in a baking tray 
  5. let stand 2 hours or more until it swells well 
  6. brush with (egg + coffee + orange blossom water) 
  7. sprinkle with sugar and bake in a preheated oven at 180 degrees C for 20 to 25 min, depending on your oven 



[ ![oil brioche](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/brioche-a-lhuile.jpg) ](<https://www.amourdecuisine.fr/article-brioche-a-l-huile.html/brioche-a-lhuile>)
