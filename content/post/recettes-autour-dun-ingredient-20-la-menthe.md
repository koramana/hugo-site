---
title: 'Recipes around an ingredient # 20: Mint'
date: '2016-07-23'
categories:
- Café amour de cuisine
tags:
- Thu
- Healthy cuisine
- salads
- Juice

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg)

source: Pinterest 

##  Recipes around an ingredient # 20: Mint 

Hello everybody, 

We decided Samar and myself to take the game again: recipes around an ingredient as godmothers during this summer vacation, so as not to clutter you with the game knowing that it takes a lot of your time to contact them. bloggers, write the article to choose the theme etc .... 

The duration of the game will also be longer 45 days instead of 30 days which gives us more time to prepare the recipe. 

The choice of the ingredient for this month was a little difficult to choose knowing that I wanted to choose one accessible in the four corners of the world:) .... so my choice was on **Mint** in all its states: fresh, mint extract, dried mint, sweet recipes salty through sweet-salty! 

![Fresh mint in closeup](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/la-menthe-1.jpg)

The principle of the game: 

  * The initiators of the game: Samar and Soulef 
  * Add the complete list of participants with the titles of their recipes. 



If despite having wanted to participate and in the end you have not managed to make your recipe, contact the godmother before the day -J- to ask him to remove you from the list. (This remains a game and not a constraint). 

Les rondes précédentes: 
