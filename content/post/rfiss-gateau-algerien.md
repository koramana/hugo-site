---
title: Rfiss Algerian cake
date: '2014-10-02'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries
tags:
- Confectionery
- Pastry
- desserts
- Algeria
- Dry Cake
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/rfiss-038_thumb1.jpg
---
##  Rfiss Algerian cake 

Hello everybody, 

Easier than the Rfis Algerian cake? I do not think you're going to find any more super delicious ... 

This is what I like in the rfiss tounsi, as my husband asks me, I never tell him: I have no time, because not even 30 minutes and it's ready. 

frankly, me this recipe I do it at random, but I tried this time to do my measurements for you: 

**Rfiss Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/rfiss-038_thumb1.jpg)

portions:  50  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 300 gr of medium semolina 
  * 350 gr of date paste 
  * 100 to 150 gr of soft butter 



**Realization steps**

  1. grill the semolina, according to your taste, put in a terrine, butter and date paste, add the semolina all hot on it, and try to mix all together. 
  2. you can start using a spatula, until you can stand the heat, and then knead with your hands, the date paste should be well incorporated in the semolina. 
  3. then shape to your taste. 
  4. for me, I made small sausages, which I then decorated with a mold in Makrout, and cut in rhombus, to make my cupcakes   
simple recipe, and any easy 


