---
title: harissa with almonds or harissat el louz
date: '2014-08-04'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/08/harissa-el-louz-harissat-el-louz.CR2_.jpg
---
[ ![harissa el louz, harissat el louz.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/harissa-el-louz-harissat-el-louz.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/harissa-el-louz-harissat-el-louz.CR2_.jpg>)

##  harissa with almonds or harissat el louz 

Hello everybody, 

here is a delicious cake that is really worth trying, harissat el louz, or harissa with almonds. This Algerian cake is so melting in the mouth, that you do not even realize that you have already eat 2 or 3 pieces. 

Contrary to what you may think, this cake is super light to the taste buds, because the seeds of almonds powder after cooking swell, which gives an airy effect to harissat el louz. 

It has nothing to do with commercial almond paste, and you have to really try to find out. 

**harissa with almonds or harissat el louz**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/harissa-el-louz-harissat-el-louz.CR2_.jpg)

portions:  15  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 2 measures of finely ground almonds 
  * 1 measure of crystallized sugar 
  * ½ measure of melted butter 
  * 1 teaspoon of rose water 
  * 1 tablespoon vanilla powder 
  * Pink and green food dyes 



**Realization steps**

  1. In a saucepan over low heat, boil the sugar, rose water and melted butter. 
  2. Add vanilla and ground almonds 
  3. stir with a wooden spoon until the paste comes off the walls. Then remove from the fire. 
  4. let the dough cool down and divide it into 3 parts, a large one that you color in pink, another smaller in green, and the last one stays nature. 
  5. On a non-stick worktop, lower the pink dough to a thickness of 1 cm 
  6. cut small cakes with a cutter of your choice.   
you can decorate the cake for more effect (see the video) 
  7. spread another ball of pink paste, in a thickness of 3 mm 
  8. cut the same shape of the cake 
  9. stick the two cakes with water, on one end of the cake 
  10. fold the other end to give the effect of a turned sheet 
  11. cut some leaves with the green paste, and stick them at the other end of the cake 
  12. cut flowers with the natural dough, and stick them on the green leaves 
  13. decorate their centers with a silver bead 
  14. the cake is ready, and can be kept 4 to 5 days or even a week in an airtight box. 



{{< youtube ozJj4x >}} 

![harissat el louz.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/harissat-el-louz.CR2_.jpg)
