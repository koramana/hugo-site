---
title: Vegetable pie
date: '2014-09-20'
categories:
- pizzas / quiches / pies and sandwiches
tags:
- Fast Food
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-de-legumes-bio_thumb1.jpg
---
##  Vegetable pie 

Hello everybody, a nice starter, a delicious salty pie, with good fresh vegetables and organic, full of vitamins and wealth, what does it say ??? oh yes for me, a very nice recipe that Lunetoiles loved to share with us, and it is with great pleasure that I share it with you too. 

So if you cook organic, here is a recipe for you, and if you do not have the chance to find organic products where you are, well, we will be satisfied with the products of the super market. 

**Vegetable pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/tarte-de-legumes-bio_thumb1.jpg)

portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 organic puff pastry 
  * 750 gr of organic vegetable mix (frozen for me) containing cubed carrots, peas, leek, celery, white cabbage, cauliflower, corn, parsley 
  * 3 organic eggs (for me the eggs of our hens raised in the open air) 
  * 200 ml of organic milk 
  * 200 ml of thick organic cream 
  * Sea salt with organic vegetables 
  * pepper 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. Unroll the puff pastry in a tart pan with a removable bottom of 30 cm diameter. 
  3. To prick with a toothpick. Reserve in the fridge while waiting to prepare the filling. 
  4. Cook the vegetables, pour the vegetables in boiling salted water, when the boil is over, count for 10 minutes. Drain and let cool. 
  5. In a large bowl beat eggs with vegetable salt and pepper, add milk and cream, beat again. 
  6. Pour the cooled vegetables into the egg / milk / cream mixture. Mix well. 
  7. Pour the filling on the puff pastry base. Bake and cook for 45 to 50 minutes. 
  8. Remove the pie from the oven, let cool for 10 to 15 minutes, before turning out. 



merci ma chère Lunetoiles pour cette belle tarte salée. 
