---
title: chocolate and coconut basboussa
date: '2017-05-08'
categories:
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Algeria
- Algerian cakes
- Ramadan
- Easy cooking
- desserts
- Semolina cake
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/basboussa-au-chocolat-et-noix-de-coco-2.jpg
---
![chocolate and coconut basboussa 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/basboussa-au-chocolat-et-noix-de-coco-2.jpg)

##  chocolate and coconut basboussa 

Hello everybody, 

This recipe of chocolate and coconut basboussa is just a cloud in the mouth, so light, so melting that you will not know how the tray you have prepared would have disappeared while you take pictures ??? 

Yes, it was the case with me, my children and my husband, as soon as I cut pieces of this chocolate and coconut basboussa, and made the staging to take pictures of this good and delicious Basked that all that remained in the tray just disappeared. 

This recipe is ideal during Ramadan, as it can be a super fast recipe to make when you do not know what to do to taste it, and if you have guests unexpectedly, because it prepares at a glance , and even the cooking is fast (I divided my dough on 2 mussels of 2 cm each, each cake cooked in less than 25 minutes. 

**chocolate and coconut basboussa**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/basboussa-au-chocolat-et-noix-de-coco.jpg)

**Ingredients**

  * 4 eggs 
  * vanilla extract 
  * 1 C. coffee baking powder 
  * ½ glass of sugar (glass = 200 ml) 
  * ½ glass of oil 
  * 1 glass of milk ribot (lben) 
  * 1 glass of coconut 
  * 1 glass minus 1 finger of fine semolina 
  * 2 tbsp. cocoa powder 
  * zest of lemon and orange zest 

Syrup: 
  * 2 glasses of sugar 
  * 2 glasses of water 
  * slices of lemon 



**Realization steps**

  1. We start by preparing the syrup, we bring the mixture sugar, water and lemon to boiling and after 10 minutes of appearance of the first bubbles, we extinguish the fire. 
  2. let cool and proceed to the preparation of the basboussa. 
  3. feast the eggs with the zest of lemon and orange, add the vanilla. 
  4. whisk well then add sugar, then oil and ribot milk. 
  5. add sifted cocoa, flour and baking powder, 
  6. whip a little and introduce the coconut. 
  7. place the mixture in a mold of almost 40 by 40 cm, otherwise like me in 2 molds of 22 dm in diameter. 
  8. cook in a preheated oven at 180 ° C for 40 minutes or depending on the oven capacity. 
  9. at the end of the oven, sprinkle the basboussa with the syrup to soak it well 
  10. decorate it with coconut. 
  11. Let cool a little before cutting the cake and go to the tasting. 



![chocolate basboussa and coconut 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/basboussa-au-chocolat-et-noix-de-coco-1.jpg)
