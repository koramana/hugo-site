---
title: Rice with the moulukhiya corn
date: '2016-08-20'
categories:
- recette a la viande rouge ( halal)
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/riz-a-la-mouloukhiya-014_thumb1.jpg
---
##  Rice with the moulukhiya corn 

Hello everybody, 

This recipe **Rice with the moulukhiya corn** that I ate for the first time at my friend Sudanese was so good, I realized it quickly to share it with you. 

So this recipe is my first attempt, I did not want to publish it, but I said to myself, any recipe is a discovery, not everyone knows the koruna, and generally, it is found in powder in Algeria and Tunisia, but here in England, we arrive at the found in frozen sheet. 

a small passage on wikipedia and I found it for you: 

The **vegetable coret** ( _Corchorus olitorius_ L.) is a plant of the family Tiliaceae native of India. 

####  Usages: 

It is grown in southern Europe as a textile plant, especially for the manufacture of burlap (stem), and as food (leaves). 

#####  Food 

Its leaves are used in cooking (Tunisian specialties (Mloukhiya), Egyptian, Syrian and Lebanese). 

Now, I go to this recipe that I had to eat when I was invited to my friend Chirine the Sudanese, and as usual I'm crazy about mloukhiya, I succumbed to this new method of cooking, the corette 

Ingredients for 4 persons:   


**Rice with the moulukhiya corn**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/riz-a-la-mouloukhiya-014_thumb1.jpg)

portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** the sauce: 

  * 4 pieces of medium-sized mutton 
  * 2 onions of medium size 
  * 200 grs of frozen corette 
  * 2 fresh tomatoes 
  * 6 cloves of garlic 
  * table oil 
  * salt, pepper, coriander powder, a cinnamon stalk. 

rice: 
  * 2 glasses of rice (250 ml) 
  * 4 glasses of water 
  * olive oil 
  * 1 small onion 
  * salt pepper. 



**Realization steps** first of all we will prepare the rice: 

  1. wash the rice, and let stand in hot water for 30 minutes 
  2. In a pot, fry the chopped onion until it becomes translucent. 
  3. drain the rice, and put it with the onions, let simmer a little while stirring with a wooden spoon 
  4. add salt, and black pepper, then water. 
  5. cover the pot with aluminum foil, then place your lid 
  6. cook for 20 to 25 minutes over medium heat 
  7. if you notice that there is no more steam that escapes, it means that the rice is cooked well, so remove from heat and let rest, until serving. 

now we prepare the sauce 
  1. in another pot, add the oil and onion and let it simmer until it becomes translucent 
  2. add the meat, and the cinnamon and let it simmer, cover so that the meat is tender under soft fire 
  3. add tomatoes cut in pieces or puree, add a little water (1 glass of tea) 
  4. leave until it becomes a thick sauce. 
  5. add salt, black pepper, and coriander, 
  6. now add the 3 head garlic rapee and mouloukhia 
  7. let simmer a little on low heat. 
  8. when the mukhukah takes a beautiful color, remove from the fire 
  9. now grate the remaining 3 heads of garlic, and cook in a little hot oil 
  10. pour this mixture on the sauce of Mouloukhiya. 
  11. and garnish your rice with this sauce 


