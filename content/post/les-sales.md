---
title: the salted
date: '2013-01-17'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cornet-au-boulghour-005a_thumb1.jpg
---
![cornet with boulghour 005a](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cornet-au-boulghour-005a_thumb1.jpg)

##  the salted 

**Hello everybody,**

**Here is a small index of** **good and delicious salty entrees, which can be accompanied by a good salad for a light meal, or to accompany a dish.**

You only have vegetable-based recipes, such as egg-free recipes, chicken meat recipes, or red meat recipes. 

If you find that one of the links is not accessible, or fall on another recipe, please contact me. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to subscribe to the newsletter, just click on this image, and tick both boxes. 

**the salted**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/cornets-au-poulet-028_thumb1.jpg)

portions:  6  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * bourek 
  * pizza 
  * quiche 
  * puff pastry 



**Realization steps**

  1. fried 
  2. baked 


