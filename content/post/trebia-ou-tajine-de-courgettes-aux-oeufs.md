---
title: Trebia or tajine of zucchini with eggs
date: '2012-03-25'
categories:
- gateaux algeriens au miel
- Algerian cakes- oriental- modern- fete aid

---
& Nbsp; & Nbsp; hello everyone, a delicious Algerian dish, a vegetarian dish that I often ate when I was little, and that I enjoy sharing with you, because it's a good summer dish, very simple. the name trebia, which comes from the origin of the word merbia, comes from the fact that this dish always gives small quantities, even if we make a lot of zucchini, well the result is a small dish, do not say that I do not have not warned, hihihihihihi the ingredients for 4 people: 1 kilos of zucchini 1 fresh tomato 1/2 ca soup of & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

a delicious [ Algerian dish ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , a dish [ vegetarian ](<https://www.amourdecuisine.fr/categorie-12348553.html>) that I ate often when I was little, and that I take advantage to share with you, because it is a good dish of summer, very simple. 

the name trebia, which comes from the origin of the word merbia, comes from the fact that this dish always gives small quantities, even if we make a lot of zucchini, well the result is a small dish, do not say that I do not have not warned, hihihihihihi 

The ingredients for 4 people: 

  * 1 kilos of zucchini 
  * 1 fresh tomato 
  * 1/2 tablespoon of tomato paste 
  * 1 onion 
  * 2 cloves garlic 
  * 2 tablespoons of oil 
  * salt, black pepper, paprika, spices according to your taste 
  * 1/2 bunches of coriander 
  * 4 eggs 


  1. clean and cut the sliced ​​zucchini 
  2. place it in the pot, with the oil 
  3. add tomato cut into pieces, and tomato concentrate 
  4. add the onion and the well chopped garlic 
  5. let it all brown, add the salt (be careful the zucchini tends to be a little salty and condiment 
  6. add the chopped coriander. 
  7. let it simmer on a low heat 
  8. add a small amount of water just to help in cooking the zucchini, not to submerge the dish. 
  9. leave until the zucchini is tender, 
  10. towards the end of cooking, break the eggs over, let it cook a little, and put out the fire 



serve hot or tepid according to your taste, and have a good appetite 

et saha ramdankoum 
