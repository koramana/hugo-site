---
title: wheat salad
date: '2015-08-13'
categories:
- salads, salty verrines
tags:
- Defi Salads
- Medley
- Summer
- inputs
- Fast Food
- Easy cooking
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/08/salade-dEbly-1.jpg
---
[ ![wheat salad](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/salade-dEbly-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/salade-dEbly-1.jpg>)

##  Wheat salad 

Hello everybody, 

In summer, there is no more light and easier than a salad easy to make, simple, rich in color and delicious. For lunch and with [ grilled sardines in the oven ](<https://www.amourdecuisine.fr/article-sardines-a-la-farce-et-l-huile-d-olive-au-four.html>) I added this wheat salad, which we really liked. 

To make this salad, I used wheat from the brand Ebly, which is easy to cook, 10 to 15 minutes of cooking in boiling water, but if you do not have this wheat at your disposal, you can use natural durum, soaking it all night in a little water (you can add some baking soda if you're in a hurry), and at the time of cooking, drain the wheat from its water, and cook in a little salty water to have tender wheat in the mouth. 

**wheat salad**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/salade-dEbly-2.jpg)

**Ingredients**

  * 2 bags of ebly (250 gr) 
  * 1 tablespoon of mustard 
  * 1 tablespoon of mayonnaise (you can adjust the quantity according to your taste) 
  * sunflower oil 
  * vinegar 
  * salt 
  * tuna with oil 
  * pickles 
  * but in box 



**Realization steps**

  1. boil Ebly wheat in salt water for 10 to 15 minutes to get a tender wheat 
  2. drain and let cool 
  3. in a salad bowl, put the cooked wheat, add the mayonnaise, mustard, vinegar, oil, salt, and mix to have the ideal taste for you. 
  4. add tuna in pieces, corn, and gherkins cut into slices 



[ ![Ebly salad 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/salade-dEbly-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/salade-dEbly-3.jpg>)
