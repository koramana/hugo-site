---
title: orange flavored pancakes
date: '2015-01-30'
categories:
- crepes, donuts, donuts, sweet waffles
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-aux-oranges-2_thumb.jpg
---
[ ![orange flavored pancakes](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-aux-oranges-2_thumb.jpg) ](<https://www.amourdecuisine.fr/article-crepes-aromatisees-a-l-orange.html>)

##  orange flavored pancakes 

Hello everybody, 

At home, pancakes to taste it is every weekend, my children love, and if it's not pancakes, it must be waffles. These orange-flavored pancakes are what they like the most, they love everything that is scented with orange, and me too. 

In any case if you like pancakes, you will enjoy following these links: 

[ Nutella pancake pasta ](<https://www.amourdecuisine.fr/article-pate-a-crepe-au-nutella-crepe-au-chocolat-facile-et-rapide.html> "Nutella pancake batter / chocolate crepe, easy and fast")

[ Crepes with coconut milk ](<https://www.amourdecuisine.fr/article-crepes-au-lait-de-coco.html> "pancakes with coconut milk")

[ plain pancakes ](<https://www.amourdecuisine.fr/article-crepes-natures-pour-garniture-sucree-ou-salee.html> "plain pancakes, for sweet or savory garnish") for sweet or savory toppings 

[ salted butter caramel crepes ](<https://www.amourdecuisine.fr/article-crepes-au-caramel-au-beurre-sale.html> "salted butter caramel pancakes")

[ crepe cake with lemon curd ](<https://www.amourdecuisine.fr/article-gateau-de-crepes-au-lemon-curd.html> "pancake cake with lemon curd")

In salty version, you can see: 

[ gratin of crepes with shrimps ](<https://www.amourdecuisine.fr/article-gratin-de-crepes-aux-crevettes.html> "gratin pancakes with shrimps")

[ gratin crepes with florentine ](<https://www.amourdecuisine.fr/article-gratin-de-crepes-a-la-florentine.html> "gratin pancakes with florentine")

[ gratin with chicken crepes ](<https://www.amourdecuisine.fr/article-crepes-farcies-au-poulet.html> "stuffed crepes with chicken")

[ mushroom and turkey crepes ](<https://www.amourdecuisine.fr/article-crepes-aux-champignons-dinde.html> "mushroom and turkey crepes")

So to you a super delicious recipe of orange-flavored pancakes, a simple and easy recipe:   


**orange flavored pancakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-a-l-orange_thumb.jpg)

portions:  20  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 2 eggs 
  * 35 g of sugar 
  * 150 g flour 
  * 400 ml of milk 
  * 50 ml of orange juice 
  * the zest of an orange 
  * 50 g of butter 
  * 2 tablespoons of oil 



**Realization steps**

  1. In a salad bowl put the eggs with the sugar 
  2. Add flour, orange peel, and butter to pieces 
  3. mix well and add the milk gradually 
  4. then add the orange juice (if it's canned juice, reduce the sugar) 
  5. add oil, mix again. 
  6. cover with plastic wrap and let stand for 2 hours. 
  7. Cook the pancakes in a non-stick skillet on each side to get a nice color. 



[ ![orange flavored pancakes](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-a-l-orange-2_thumb_1.jpg) ](<https://www.amourdecuisine.fr/article-crepes-aromatisees-a-l-orange.html>)

enjoy with a spread, such as [ spread with gingerbread ](<https://www.amourdecuisine.fr/article-pate-a-tartiner-au-pain-d-epice-98437586.html>) , or with [ applesauce ](<https://www.amourdecuisine.fr/article-compote-de-pomme-98542533.html>) , ou préparez un gâteau avec ces délicieuses crêpes. 
