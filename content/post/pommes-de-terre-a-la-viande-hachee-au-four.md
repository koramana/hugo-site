---
title: mashed potatoes with baked meat
date: '2009-07-12'
categories:
- recipe for red meat (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/07/218179891.jpg
---
![PT_au_four](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/218179891.jpg)

sometimes we have everything at home, but we have no more ideas, and we begin to stumble here and there, and look for: what am I going to do at dinner. 

then, Eureka, the ideas begin to come, and one has a little brain, a good dish that will please everyone. 

it was my case tonight, finally it's the first time I do that, maybe there are women who do it every time, but to be honest, for me it's an invention ............ ..hum, editor's right ... ..lol 

Any way, I started by preparing a little stuffing of minced meat, after simmering a little onion and a little garlic in oil, I add after minced meat, season with salt, and black pepper, and sprinkle with a little parsley ......... .. it feels boooooooooooon 

![pt](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/218186181.jpg)

Afterwards, I made some onion come back, in the oil, and to which I added a little fresh tomato and 1 case of canned tomato, and I seasoned with salt and black pepper, and a bit of an Indian condiment based on coriander and garlic (an incredibly good smell, long live Indian spices) 

![pt1](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/218189211.jpg)

At the same time, I had 4 to 5 potatoes peeled and sliced ​​seasoned with salt and pepper, steamed, (all of which was very careful with salt, because everything is seasoned apart, but putting them together, you risk having a salty dish) 

![pt2](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/218190121.jpg)

everything was ready, so I baste the bottom of my baking dish with a little butter (mine is lighter) and I put the slices of the potato, so as to cover all my dish of low and sides. I covered with my red sauce, and then with a layer of minced meat, I sprinkled with a little butter, and I renewed the operation, potato, red sauce, minced meat. and finally covered with a layer of potato, and then I still sprinkle with butter, I wanted to scratch on a little gruillere, but the mouse is in the fridge. 

I remember having to hide a small part by the ............ .. mischievous my mouse ?????? 

so I see a cow laughing all the same in my fridge (not the cow but the cheese box) 

![pt3](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/218195251.jpg)

so I put some cheese on the last layer, and in the oven already preheated. 

everything is cooked, so just the time to have a beautiful gilding at the bottom and top of my dish. 

![pt4](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/218196511.jpg)

he was very good, of course, of course, how a mixture of potato and minced meat is not going to be good, you're going to tell me. 

![PT_au_four](https://www.amourdecuisine.fr/wp-content/uploads/2009/07/218179891.jpg)

bonne appétit 
