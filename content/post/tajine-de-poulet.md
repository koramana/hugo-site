---
title: chicken tagine
date: '2012-06-18'
categories:
- Buns and pastries
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/poulet-palestinien-51.jpg
---
& Nbsp; hello everyone, here is a delicious chicken tagine recipe, a recipe I saw at Nadia's, I immediately pulled the chicken legs out of the freezer, the original recipe was supposed to contain fresh chicken, but not Serious, it did the trick, and frankly, it was really a treat, despite the absence of Saumac (which I replaced by lemon). Ingredients: Chicken legs 2 to 3 onions, depending on the size (you can put more, we make you after cooking it is a sublime taste) Salt black pepper Turmeric Paprika Cinnamon Saumac & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.75  (  1  ratings)  0 

![Palestinian chicken, chicken tajine](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/poulet-palestinien-51.jpg)

Hello everybody, 

![chicken tagine](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/poulet-palestinien11.jpg)

Ingredients: 

  * Chicken legs 
  * 2 to 3 onions, depending on the size (you can put more, we make you after cooking it is a sublime taste) 
  * Salt 
  * black pepper 
  * Turmeric 
  * Paprika 
  * Cinnamon 
  * Saumac (replaced at home by the juice of a whole lemon, huuuuuuuuuuuum) 
  * olive oil 



![Baked Chicken](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/poulet-palestinien21.jpg)

![chicken tagine](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/poulet-palestinien31.jpg)

  1. cut a chicken into pieces, coat with salt, pepper, turmeric, paprika, cinnamon and **sumach** , or like me the juice of a lemon 
  2. slice 2 to 3 onions, put them in your baking tray / pan and cover them with olive oil, 
  3. place on top, the pieces of chicken and mix with onions. 
  4. cover with foil and cook for 1 to 1 1/4 hours, taking care to return them from time to time. 
  5. remove the aluminum and roast for about ten minutes. it's a very simple way that gives a very delicious result and very melting in the mouth 



![chicken tagine](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/poulet-palestinien-51.jpg)

I swear it was very very good, I served this chicken with a baked potato dish, 

bonne dégustation 
