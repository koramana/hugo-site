---
title: brownie spread
date: '2015-08-21'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/02/pate-a-tartiner-au-brownie-038_thumb.jpg
---
[ ![brownie spread 038](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/pate-a-tartiner-au-brownie-038_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/02/pate-a-tartiner-au-brownie-038.jpg>)

##  brownie spread 

Hello everybody, 

a very good spread to the taste of brownie, a very creamy dough, very tasty, especially if you like the taste of cocoa (something I really like), a true delight, I prepared it to accompany my pancakes, but the children ate it with a spoon.    


**brownie spread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/pate-a-tartiner-au-brownie-038_thumb.jpg)

portions:  4 

**Ingredients**

  * 250 gr [ brownie ](<https://www.amourdecuisine.fr/article-brownies-recette-facile.html> "brownies, easy brownies recipe")
  * 50g of soft butter 
  * 6 tablespoons of condensed milk 
  * 2 tablespoons fresh cream 



**Realization steps**

  1. Emit the brownies in small pieces 
  2. Place in the bowl of the blinder with the soft butter and the cream 
  3. Mix and add the condensed milk gradually, until you have a light and unctuous dough, if you think you want to put more milk concentrate, do not hesitate 
  4. Fill your jars with this paste and place in the fridge 



[ ![brownie spread](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/pate-a-tartiner-au-brownie_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/02/pate-a-tartiner-au-brownie_2.jpg>)

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

and if you have a recipe for you that you want to publish on my blog, you can put it online here: 

[ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
