---
title: 'Mhalbi constantinois: rice dessert cream video'
date: '2015-06-25'
categories:
- panna cotta, flan, and yoghurt
- ramadan recipe
- rice
tags:
- Ramadan 2015
- Ramadan
- Algeria
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi-de-pipo.jpg
---
[ ![mhalbi de pipo](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi-de-pipo.jpg) ](<https://www.amourdecuisine.fr/article-mhalbi-constantinois-creme-dessert-au-riz-en-video.html/mhalbi-de-pipo>)

![https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi-0182.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi-0182.jpg)

##  Mhalbi constantinois: rice dessert cream video 

Hello everybody, 

It's the **I halbi** from my mother, inescapable, incomparable, and always a delight ... Well yes I take advantage of my presence with my mother to ask the delights with which I grew up, and yesterday she made us her delicious mhlabi perfumed with orange blossom . 

a delicious [ dessert ](<https://www.amourdecuisine.fr/categorie-12347071.html>) of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) . in the genre, you can see the recipe for the [ mhalabia ](<https://www.amourdecuisine.fr/article-m-halabiya-tricolore-citron-et-fruits-81792006.html>) , and of [ Indian rice cream ](<https://www.amourdecuisine.fr/article-creme-de-riz-indienne-59532236.html>) . 

The photo above, is that of one of my friends, who made the Constantinois mhalbi, and the cocoa garnished, so I share the photo just to give you an idea of ​​decoration. 

![https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi_21.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi_21.jpg)

**Mhalbi constantinois: rice dessert cream video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi.CR2_1.jpg)

portions:  6 to 8  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 1 liter of milk 
  * 5 tablespoons of rice reduced to a fine powder 
  * 1 tablespoon of maizina 
  * 3 tablespoons of orange blossom water 
  * 5 tablespoons of sugar (or according to your taste) 
  * cinnamon for decoration. 



**Realization steps**

  1. take half a glass of milk from the liter, and make the 
  2. maizina in it. 
  3. mix the sugar and the rice with the milk 
  4. and start stirring over low heat 
  5. When the mixture begins to bubble, add the maizina to the remaining milk and orange blossom water. 
  6. continue stirring, when the mixture is well chopped (not too much anyway) pour in ramekins or in dishes for dessert. 
  7. decorate with cinnamon and let cool. 



![https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi-0231.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/mhalbi-0231.jpg)

degustez le bien frais, huuuuuuuuuuuuuum un delice 
