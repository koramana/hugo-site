---
title: super sweet chocolate chip cookies
date: '2017-09-18'
categories:
- Algerian dry cakes, petits fours
tags:
- Chocolate cake
- Soft
- biscuits

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/recette-de-cookies-sans-gluten-1.jpg
---
[ ![super sweet chocolate chip cookies](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/recette-de-cookies-sans-gluten-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/recette-de-cookies-sans-gluten-1.jpg>)

##  super sweet chocolate chip cookies 

Hello everybody, 

Here are cookies with chocolate chips super moist rich in taste, crisp at the first bite, soft to the second, and melting at the third ... 

I have to tell you one thing, I'm the number 1 reader of Lunetoiles, she does not have a blog, she likes to share her recipes on my blog, and I thank her a lot for that ... And I often test her recipes that I succeed without problem ... 

Like this recipe for cookies, which I realized this weekend with my children, they were all happy to put their hand in the dough, but I do not tell you the state of the show after this nice adventure ... Yes my friends, for those who know my apartment, my kitchen is really small, very very small, it is not even 1 m by 2 and a half, the work plan, with the mico-wave on, the robot, the baby bottle sterilizer I only have 50cm by 50cm left to do the work ... so when the kids liked to cook with me, we had to work in the living room. It took me 2 hours of cleaning afterwards to put the living room in order ... 

The cookies of my children were too good, but aesthetic side ... not worth that I show you that, but my children were very proud of themselves that everyone has to prepare a box for his teacher at school ... they will have a nice surprise on Monday, lol 

I come back to Cookies Lunetoiles which are on the other hand too beautiful. and if as Lunetoiles you want to share with us one of your recipes, you can send on my email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)   


**super sweet chocolate chip cookies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-2.jpg)

portions:  40  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 350 g flour 
  * ¾ c. coffee baking soda 
  * ¼ c. fine salt 
  * 225 g softened butter 
  * 180 g of whole sugar   
(in the bio section) or brown sugar (but it's really better with the whole sugar!) 
  * 150 g white sugar 
  * 1 C. vanilla extract powder or vanilla powder 
  * 2 temperate eggs 
  * 160 g of nuts   
(pecan, macadamia, or almonds, hazelnuts, or whatever you want!) 
  * 300 g coarsely chopped dark chocolate 
  * optional: fleur de sel 



**Realization steps**

  1. Preheat the oven th. (150 ° C). 
  2. Spread the nuts on a baking sheet and roast in the oven for about 10 minutes. Cool, then chop roughly and set aside. 
  3. In a medium bowl, whisk the flour with the baking soda and salt to distribute the ingredients. 
  4. In a large bowl or the bowl of a robot equipped with the sheet (K beater), place the softened butter cut into pieces and work it a few moments to give it an ointment consistency. 
  5. Add the white sugar, whole sugar and vanilla and mix vigorously until a smooth cream is obtained. 
  6. Pour the eggs one by one, beating energetically after each addition so that the egg is completely incorporated. 
  7. Add the flour and mix with the wooden spoon or the leaf just enough so that it is amalgamated then add the chopped roasted nuts and the coarsely chopped chocolate and incorporate them without working the dough excessively (the best is to finish at the hand). 
  8. Transfer the dough to a lightly floured work surface, form a homogeneous ball and divide into 4 pieces of the same weight. 
  9. Shape each piece into a regular pudding about 23 cm long (about 4 cm in diameter) 
  10. Wrap the wraps tightly and tightly in stretch film and place in the refrigerator for the time needed for the wraps to be hard enough to slice (a few hours minimum) or preferably for 24 hours.   
[ ![super sweet chocolate chip cookies](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-300x201.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-300x201.jpg>)
  11. Preheat the oven th. 6 (175 ° C). 
  12. Garnish one or more baking trays with parchment paper. 
  13. Remove the food film from a roll of dough and cut out regular slices about 1.5 cm thick (more for less fluffy cookies for crisper cookies). If the slices are deformed, no worries: just reform them! 
  14. Place the slices as you go on the baking trays spaced at least 6 cm apart. You can put 8 by plates. 
  15. Bake the plate in the middle of the oven and cook for 8 to 10 minutes. 
  16. Be careful to cook only one plate at a time! 
  17. The cookies must be very lightly colored but it is imperative to remove them from the oven still soft or undercooked (they continue cooking on the plate off the oven) so that they remain moist at heart. 
  18. Leave the cookies on the hotplate for about 10 minutes until they can be transferred with a spatula on a cooling rack to cool completely. 
  19. Enjoy! 



[ ![super sweet chocolate chip cookies](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-et-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-et-chocolat.jpg>)
