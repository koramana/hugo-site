---
title: chicken stuffed batbout
date: '2015-08-26'
categories:
- recette de ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/kitchen-mini-batbout_2-300x225.jpg
---
##  chicken stuffed batbout 

Hello everybody, 

Here is a good recipe of mini stuffed batbouts, which comes from one of my readers "kitchen 2010", delicious small batbouts very light and airy, stuffed with salad with chicken breast ... 

you can find the recipe in Arabic here: 

**chicken stuffed batbout**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/kitchen-mini-batbout_2-300x225.jpg)

portions:  6  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients** Dough: 

  * 500g flour, 
  * 1 teaspoon salt 
  * 25g of fresh beer yeast, 
  * 1 teaspoon of seed Nigel, 
  * warm water as needed. 

Prank call: 
  * green salad leaves, 
  * fresh tomatoes, 
  * Pitted green olives cut into slices, 
  * Chicken breast cooked and crumbled, 
  * Mayonnaise, and hot sauce. 



**Realization steps**

  1. Mix the flour, salt, and black seed, add the diluted yeast in a little warm water, mix with the fingers, add the necessary lukewarm water gradually, in order to obtain a soft dough. knead it, and let it rise for 15 minutes (cover well). 
  2. Take half of the dough, open it with a rolling pin to a thickness of 0.50 cm and with a glass or a round cookie cut small circles, place them on a cloth already sprinkled with flour and cover them, repeat until all the dough is gone. let rise a second time. 
  3. Heat a heavy bottom skillet on low heat, and start cooking the batbouts, as they swell you back on the other side. 
  4. In a salad bowl: mix the crumbled chicken with the green slices of green olives, mayonnaise and hot sauces. 
  5. Open each batbout halfway (like a wallet), put in a little salad, stuffing chicken, and a small piece of tomato. 
  6. present on a bed of green salad. 



Note Note: if you do not like the taste of spice, you can always replace the spicy sauce with ketchup, it will be more enjoyable for children too. 
