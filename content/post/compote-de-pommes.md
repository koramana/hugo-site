---
title: applesauce
date: '2016-10-17'
categories:
- confitures et pate a tartiner
- basic pastry recipes
- recipes for babies 4 to 6 months
tags:
- desserts
- Pastry
- To taste
- Easy cooking
- Based
- Apple pie
- pies

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/compote-de-pommes_thumb.jpg
---
##  applesauce 

Hello everybody, 

A recipe of applesauce very simple, but above all too good, ideal to taste simply with a spoon, or to garnish and decorate a cake or a pie shell. 

This applesauce is almost always present at home, my daughter claims it all the time. and even I use it in many of my recipes. 

This time I prepared this account for a super delicious [ cake mounted while pancakes ](<https://www.amourdecuisine.fr/article-gateau-de-crepes-a-la-compote-de-pommes.html>) . I also use this applesauce in my semolina cake recipe with raisins and apples, or to make a [ apple tart ](<https://www.amourdecuisine.fr/article-tarte-fine-aux-pommes.html>) .... Try you will really like it.   


**applesauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/compote-de-pommes_thumb.jpg)

**Ingredients**

  * 1 kg 500 of apples 
  * ½ lemon juice 
  * 1 cup of vanilla coffee 
  * 5 c. generous soups of sugar 



**Realization steps**

  1. Peel the apples and seed them. 
  2. Cut them into cubes. Put them in a saucepan over low heat adding the lemon juice, and vanilla. 
  3. cover and cook on fire for about 15 minutes, stirring regularly with a wooden spoon. 
  4. To finish, add the sugar and pass the compote to the mill. 
  5. enjoy it with a spoon, it is too good, and as you see, I did not put water in it, it was cooked in its own steam. 


