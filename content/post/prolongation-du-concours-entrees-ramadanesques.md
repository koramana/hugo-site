---
title: extension of the contest "Ramadan entries"
date: '2011-07-23'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/zlabia-r%C3%A9ussie-pour-ramadan-2017-a-300x200.jpg
---
![zlabia successful for ramadan 2017 a](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/zlabia-r%C3%A9ussie-pour-ramadan-2017-a-300x200.jpg)

Hello everybody, 

following connection problems, here in Algeria, and because sometimes I can not access your blogs to take the photo of the participating recipe, and also, because I can not easily update all the participations, 

I am obliged to extend the date of the end of the contest, from August 10 to August 30, so once again I give you the rules but with the new dates: 

  1. you can participate with up to 4 recipes. 
  2. if you have a blog: put this logo at home, talk a little about this contest and send me your links * of participating recipes, on this email: ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-150x34.jpg)
  3. or leave a comment on this link. 
  4. if you are without a blog, send me the photo and the recipe ** on this email: ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-150x34.jpg)
  5. the entries will start from the 10th of July until the 30th of August. 
  6. a summary will be published on September 1, and you will have until September 5, to contact me in case you forget about me validation of a recipe. 
  7. the vote on the recipe you like will start from the 5th of September. 
  8. you can vote for the recipe you like the most, on the summary article, leaving a comment. 
  9. a member to swear will ultimately decide the winner, among the top 10 recipes ***. 
  10. expect more precision about the gift and the result. 



* If your photos are protected by the right click, send me on the email, the link of the photo, or the photo. 

** the recipe will publish on my blog as soon as possible. 

*** in the 10 best recipes chosen by the readers, will only be valid for one recipe per participant, ie, if a contestant has 2 recipes ranked in the top 10, only one recipe will be chosen, to give the chance to a other participants. 

for any information or suggestions contact me on my email: 

vosessais@gmail.com 

merci 
