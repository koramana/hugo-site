---
title: batbouts with avocado and potatoes
date: '2017-04-05'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/batbout-a-l-avocat-et-pommes-de-terre-4.jpg
---
##  batbouts with avocado and potatoes 

Hello everybody, 

Here is some delicious home-made sandwiches, that we like to share with us one of my loyal readers "malika" she has already shared with us her recipe for poultry keg. 

today it's stuffed with avocado and potatoes:   


**batbouts with avocado and potatoes** avocado and potato bats Author: Love Cooking bats with avocado and potatoes Author: Cooking Love 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/batbout-a-l-avocat-et-pommes-de-terre-4.jpg)

**Ingredients**

  * For the dough 
  * 200g of flour 
  * 200g of very fine semolina 
  * 1 tablespoon of salt 
  * 1 tablespoon of baker's yeast 
  * Half a bowl of water or more Depending on the absorption of the flour 
  * The joke: 
  * A lawyer 
  * 2 potatoes 
  * A box of natural tuna 
  * 100 g green or black olives 
  * 2 boiled eggs 
  * 2 tablespoons mayonnaise 
  * 2 tablespoons of olive oil 
  * 1 teaspoon of salt 
  * 1 cup of pepper 
  * A little onions 
  * 1 tablespoon of fresh parsley 



**Realization steps**

  1. In a bowl, or a large salad bowl, put all the ingredients to make a beautiful dough. 
  2. Knead for 10 minutes, until the dough becomes smooth. 
  3. let stand for 30 minutes in a warm place 
  4. in the meantime, cook the potatoes during. 30 min in salt water, check with a knife 
  5. let cool, then peel it. 
  6. cut the potatoes into cubes, 
  7. peel avocado, egg and cut into small cubes, 
  8. grate the onions on the potatoes, put the avocado, the egg, the oil, the mayonnaise, the spices, the parsley, the olives, the tuna, mix well and reserve. 
  9. after lifting the dough, make balls, between 10 to 12 balls the size of an orange. 
  10. put them on a tray and allow to swell 25 min, 
  11. take a pan or a pancake or tajine. heat and cook the mini batbouts. 
  12. brown them on each side. 
  13. After cooking, open them halfway and fill them with stuffing, 
  14. enjoy. It's ready .. and super good 



Ingredients:   
For the pasta 

  * 200g of flour 
  * 200g of very fine semolina 
  * 1 tablespoon of salt 
  * 1 tablespoon of baker's yeast 
  * Half a bowl of water or more Depending on the absorption of the flour 



The joke: 

  * A lawyer 
  * 2 potatoes 
  * A box of natural tuna 
  * 100 g green or black olives 
  * 2 boiled eggs 
  * 2 tablespoons mayonnaise 
  * 2 tablespoons of olive oil 
  * 1 teaspoon of salt 
  * 1 cup of pepper 
  * A little onions 
  * 1 tablespoon chopped fresh parsley 


  1. In a bowl, or a large salad bowl, put all the ingredients to make a beautiful dough. 
  2. Knead for 10 minutes, until the dough becomes smooth. 
  3. let stand for 30 minutes in a warm place 
  4. in the meantime, cook the potatoes during. 30 min in salt water, check with a knife 
  5. let cool, then peel it. 
  6. cut the potatoes into cubes, 
  7. peel avocado, egg and cut into small cubes, 
  8. grate the onions on the potatoes, put the avocado, the egg, the oil, the mayonnaise, the spices, the parsley, the olives, the tuna, mix well and reserve. 
  9. after lifting the dough, make balls, between 10 to 12 balls the size of an orange. 
  10. put them on a tray and allow to swell 25 min, 
  11. take a pan or a pancake or tajine. heat and cook the mini batbouts. 
  12. brown them on each side. 
  13. After cooking, open them halfway and fill them with stuffing, 
  14. enjoy. It's ready .. and super good. 


