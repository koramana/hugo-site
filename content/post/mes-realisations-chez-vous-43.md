---
title: my achievements at home 43
date: '2014-01-16'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

I am very happy to share with you your achievements and recipes that you like on my blog. A nice list, and successful tests, so thank you all for your loyalty. 

For some of you that do not save it, I put you, a much faster system so that you can send me your photos, just go on this page: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll like ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link of your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)

and here are your recipes my friends: 

![Pie conversation](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/ob_d09609_conversation-22.jpg)

[ Chocolate cookies ](<https://www.amourdecuisine.fr/article-biscuit-de-noel-au-chocolat.html> "Chocolate biscuit") revived in Isma (a reader, and I love your help Isma, it is true that it is more practical in these molds fingerprints) 

![image fcdf68edc14e9291d976ed7293298ef672d76fdc](https://www.amourdecuisine.fr/wp-content/gallery/1/image-fcdf68edc14e9291d976ed7293298ef672d76fdc.jpg)

[ How to make homemade bread ](<https://www.amourdecuisine.fr/article-comment-faire-son-pain-maison.html> "how to make homemade bread") at Naziha's place 

![Nadia-004-abcecb0191658123a007b61170b14a69627f93d4](https://www.amourdecuisine.fr/wp-content/gallery/1/nadia-004-abcecb0191658123a007b61170b14a69627f93d4.jpg)

[ Khobz dar ](<https://www.amourdecuisine.fr/article-25345314.html> "khobz eddar - homemade bread") at Fleur DZ 

![Nadia-011-1103e6625bf3027156cc5048af278692653d8ba0](https://www.amourdecuisine.fr/wp-content/gallery/1/nadia-011-1103e6625bf3027156cc5048af278692653d8ba0.jpg)

[ Brioche with chive cream ](<https://www.amourdecuisine.fr/article-pain-brioche-a-la-ciboulette.html> "Brioche bread with chives") at Fleur DZ 

![Nadia-034-28ff8c308cf5b4aebe22d1118707278d8f199c72](https://www.amourdecuisine.fr/wp-content/gallery/1/nadia-034-28ff8c308cf5b4aebe22d1118707278d8f199c72.jpg)

[ Basboussa with coconut ](<https://www.amourdecuisine.fr/article-basboussa-a-la-noix-de-coco.html> "Basboussa with coconut") at Fleur DZ 

![013-373ee2fbd27686eaf27b08236d88d02c3cb5a17e](https://www.amourdecuisine.fr/wp-content/gallery/1/013-373ee2fbd27686eaf27b08236d88d02c3cb5a17e.jpg)

[ Oreilletes or khechkhach ](<https://www.amourdecuisine.fr/article-khechkhach-ou-khochkhach-oreillettes-gateau-algerien.html> "khechkhach or khochkhach / Algerian cake earplugs") at Fleur DZ 

![92579910_o1-ee27f3bbd584e7a18bfaefcb2b31b73377cfbb8c](https://www.amourdecuisine.fr/wp-content/gallery/1/92579910_o1-ee27f3bbd584e7a18bfaefcb2b31b73377cfbb8c.jpg)

and it's a recipe from a reader who did not mention her name, I think she wanted to come up with her recipe ... 

I tell you my dear goes on this link, and send your entire recipe: 

[ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

Bisous 
