---
title: the gruffalo
date: '2012-03-01'
categories:
- gateaux algeriens- orientales- modernes- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/591340751.jpg
---
Hello everybody, 

no it's not a recipe, but the character of a children's story book by Julia Donaldson, an English children's story writer, and like today, they celebrate the day of the book at school from my son, they put the spotlight on this writer, and asked the children to come to school to dress everyone as her favorite character. 

my son chose him: The gruffalo: 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/591340751.jpg)

a very beautiful story of a mouse looking in the forest what to eat, and during her search, she meets fierce animals: the owls, the jackal, and the snake, but each time the mouse, and intelligently s' escaped between the teeth of these animals, they are told that she will meet a horrible monster, for dinner, this monster is the Gruffalo, and she begins to describe it in a way to scare these animals. 

finally, during her walk, she really meets the gruffalo .... 

a very beautiful story, there is even a film about this story, which I advise you to see, if you have not already seen, because it's really sublime as a story for children, I'll be frank, myself I really like the movie, and the story, and I read it all the time to my kids. 

so for the past two days, I have become a dressmaker, to do to my son: the gruffalo, so no time to cook. 

Rayan to move in these photos, and as I had not put the option "children" on my device, well they are a little vague, I will catch up on his return from school, for the moment I pass you these a few poses that I could take right outside the house, he was very excited to go show his costume to his classmates. 

and here is a little video: 

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/trans2.gif)
