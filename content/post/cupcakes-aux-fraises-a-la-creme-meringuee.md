---
title: Strawberry cupcakes with meringuee cream
date: '2012-01-10'
categories:
- juice and cocktail drinks without alcohol
- recettes a moins 200 Kcal

---
Hello everybody, 

I am still with the problem of pc, so I apologize if I can not immediately visit you, or that I can not immediately validate your comments, but I swear, I can not stand being in front of the pc, so a page must stay at least 20 minutes to load, so frankly, I do not need all this stress, when the pages go up quickly, I try to immediately visit you, or answer the question but if not when it reams, I leave the pc to do something else, so thank you to all who are still there, to support me. 

so today I share with you this recipe that comes from our dear Lunetoiles, that's what she could send me as photos, because what she had done, her beautiful sister took with her, suddenly, she could not do more photos, at least the recipe, her sister-in-law did not take it, hihihiih so I share it with you: 

Here is the recipe divided for a yield of 15 cupcakes: 

  * 157 gr all-purpose flour 
  * 35 gr of cake flour 
  * 1/2 tablespoon baking powder 
  * 1 pinch of salt 
  * 113 g unsalted butter at room temperature 
  * 253 grams of sugar (I only put 150 grams) 
  * 1 tbsp. coffee vanilla extract 
  * 1 large egg (or 2 small) + 1 egg white at room temperature 
  * 1 glass of milk (100 ml) at room temperature 
  * 140 gr of fresh strawberries, finely chopped 



Frosting: 

  * 112 gr strawberries smoothed puree 
  * 2 big white eggs 
  * 140 gr of sugar 
  * 170 gr unsalted butter at room temperature (diced) 
  * \+ Strawberries for decoration 


  1. Preheat the oven to 180 ° C 
  2. Garnish a cupcake tin with paper boxes. 
  3. In a bowl, mix together all-purpose flour, cake flour, baking powder and salt. 
  4. In the bowl of a whisk-beater, cream butter and sugar and vanilla at medium-high speed until light and fluffy, about 3 to 4 minutes. 
  5. Add eggs and egg white, one at a time, beating well after each addition, stop and scrape the sides of the bowl as needed. 
  6. Reduce the speed to low-medium and add the flour mixture in half, alternating with the milk. Mix until everything is well incorporated. 
  7. Gently stir in the chopped strawberries with a rubber spatula. 
  8. Fill each well with the 3/4 cupcake pan. 
  9. Bake for 25-30 minutes or until the cakes are golden, and check for baking: a toothpick inserted in the center of a cake should come out clean. 
  10. Remove the baking sheet from the oven and let cool for 5-10 minutes. Then take the cakes out of the molds and let them cool on a rack, to cool completely. 



For icing: 

  1. Add the egg whites and sugar to the mixer bowl and put on a pan of simmering water. Heat, stirring constantly, until sugar is dissolved. Test with your fingers, if the mixture is granular, it is not ready yet. 
  2. Fix the bowl on your mixer and equip it with the whisk. 
  3. Beat on medium-high medium speed until stiff peaks form and the mixture has cooled completely to room temperature for about 10 minutes (the bowl will be cool to the touch). 
  4. Reduce the speed to medium-low and add the butter, 2 tbsp. at a time. 
  5. That each one is completely incorporated before adding the next one. Scrape the sides of the bowl, if necessary. (The mixture may seem to "curdle", continue to beat, it will come together) 
  6. Scrape the sides of the bowl, and add the strawberry puree. Beat until a homogeneous mixture. The mix will probably look "curdled" again when you add the strawberry purée, that's normal, keep beating. 
  7. Put the cream in a piping bag with a star socket, and place on the cupcakes. 
  8. Garnish with additional strawberries for the decor, if desired. 



thank you for your comments and visits 
