---
title: fluffy cake with lemon and poppy seeds
date: '2017-03-27'
categories:
- cakes and cakes
- sweet recipes
tags:
- Fluffy cake
- Algerian cakes
- To taste
- desserts
- fondant
- Breakfast
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/cake-moelleux-au-citron-et-graines-de-pavot-1-683x1024.jpg
---
![fluffy cake with lemon and poppy seeds 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/cake-moelleux-au-citron-et-graines-de-pavot-1-683x1024.jpg)

##  fluffy cake with lemon and poppy seeds 

Hello everybody, 

To you this wonderful recipe for a fluffy cake with lemon and poppy seeds. This cake is just a pleasure to taste, and it's been a long time since I realized it. And when my neighbor invited me for a coffee with neighbors, I directly thought to make this delicious cake. 

The presence of poppy seeds was a little surprise for a few, but in the end it did not remain a crumb of this cake mellow with lemon and poppy seeds. At home to be frank, I do not need poppy seeds, because my children do not like too much, and I will advise you the same if your children are like mine, this will not change the beautiful taste of this cake. 

**fluffy cake with lemon and poppy seeds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/cake-moelleux-au-citron-et-graines-de-pavot.jpg)

**Ingredients**

  * 60 gr of butter at room temperature 
  * 85 gr of powdered sugar 
  * 2 tbsp. lemon zest 
  * 2 whole eggs 
  * 200 ml of milk 
  * the juice of 1 lemon and a half 
  * 1 C. coffee vanilla extract 
  * 280 gr of all-purpose flour 
  * 1 C. coffee baking powder 
  * ½ c. coffee baking soda 
  * 2 tbsp. poppy seeds (optional) 

icing: 
  * 100 gr of icing sugar 
  * lemon juice 
  * lemon zest. 



**Realization steps**

  1. Preheat the oven to 180 °. 
  2. In a salad bowl whip the butter and sugar until a slightly creamy mixture is obtained. Then add eggs 1 to 1 while whisking 
  3. introduce milk, lemon juice and vanilla, whip again. 
  4. add sifted flour, baking powder and baking soda. 
  5. Introduce everything well, and add the poppy seeds and the lemon zest. 
  6. Pour the mixture into a cake pan lined with baking paper and bake for 55 minutes or until the blade of a knife stuck in the cake comes out clean. 
  7. let cool before unmolding. 
  8. prepare the frosting by gently adding the lemon juice to the icing sugar while stirring with a spoon. 
  9. has the desired texture, garnish the cake with this icing and a little lemon peel 



![fluffy cake with lemon and poppy seeds 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/cake-moelleux-au-citron-et-graines-de-pavot-2-771x1024.jpg)
