---
title: shrimp and potato boureks
date: '2014-06-27'
categories:
- Bourek, brick, samoussa, slippers

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-aux-crevettes-pommes-de-terre-2.jpg
---
[ ![potato prawn boureks 2](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-aux-crevettes-pommes-de-terre-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-aux-crevettes-pommes-de-terre-2.jpg>)

##  shrimp and potato boureks 

Hello everybody, 

recipes of shrimp and potato boureks is the recipe for boureks at home, especially for my husband, imagine that this recipe must be on the table of ramadan for 30 days ... without stop ... 

my husband tells me, make me 3 boureks, and for you do what you want ??? lol ... Men do their best in Ramadan so we do not go out of the kitchen, hihihih. Anyway, these boureks I prepare them with the recipe of [ homemade dyouls ](<https://www.amourdecuisine.fr/article-dyouls-ou-feuilles-de-brik-fait-maison.html> "diouls or homemade Brick leaves") ... and they were crispy at wish. 

I do not hide the truth, it's true that I like these boureks, but I prefer by far the [ shrimp brigs with bechamel ](<https://www.amourdecuisine.fr/article-bricks-aux-crevettes-a-la-bechamel.html> "shrimp brigs with bechamel") ....   


**shrimp and potato boureks**

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-aux-crevettes-pommes-de-terre-.jpg)

portions:  6  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * potatoes 
  * parsley 
  * onion 
  * garlic 
  * grated or portioned cheese 
  * olive green olives. 
  * salt, black pepper, garlic / coriander mixture (spice) 
  * shrimp 



**Realization steps**

  1. boil the potato peeled and cut into cubes, in a slightly salty water. 
  2. crush after cooking, add parsley, spices, 
  3. in a frying pan, fry shrimp cleaned and peeled with garlic crushed 
  4. Open the dyoul leaf, make a thin line of finely chopped onion 
  5. put over the parsley potato 
  6. add over a layer of sliced ​​olives 
  7. now add the sautéed shrimp, and garnish with cheese. 
  8. cover the stuffing with the dyoul leaf 
  9. fry in a warm oil bath, and drain on paper towels. 



[ ![boureks shrimp potatoes](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-crevettes-pommes-de-terre.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-crevettes-pommes-de-terre.jpg>)

bon appétit et saha ftourek. 
