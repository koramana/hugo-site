---
title: fluffy chocolate cake
date: '2015-01-14'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-chocolat-053.CR2_thumb.jpg
---
[ ![chocolate cake 053.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-chocolat-053.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/article-cake-au-chocolat-moelleux.html>)

##  fluffy chocolate cake 

Hello everybody, 

you want to make the recipe  chocolate cake  , to which nobody will resist, well I'll give you mine, a recipe that will not be dethroned at home. 

besides being a  very easy chocolate cake  to realize, in not even 15 minutes, the cake will be already in the oven to cook, this cake is also a true delight, very melting in mouth, with the brilliant flavors in mouth .... a happiness at the end of the fork .... 

it's difficult for me to make the pictures, because I did it for the four o'clock, (the light was not famous, suddenly, the photos are unfair, and do not give true value to this magnificent cake). 

and there was nothing left to redo the photos in the morning ... despite everything, it does not prevent me from sharing with you this delicious cake, you will be conquered after having realized, so your aprons. it's irresistibly melting and mellow. 

[ ![chocolate cake 041.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-chocolat-041.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/article-cake-au-chocolat-moelleux.html>)   


**fluffy chocolate cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-chocolat-009.CR2_thumb.jpg)

Recipe type:  cake  portions:  12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * Ingredients: 
  * 200 gr of butter 
  * 100 gr of chocolate 
  * 150 gr of sugar 
  * 4 eggs 
  * 130 gr of flour 
  * 50 gr maïzena 
  * 1 tablespoon cocoa powder 
  * ½ sachet of baking powder 
  * icing sugar for the decoration, otherwise you can make a [ chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat.html> "chocolate ganache") . 



**Realization steps**

  1. turn on the oven at 180 degrees C 
  2. line a cake tin with baking paper 
  3. melt the chocolate in pieces and butter in the microwave, otherwise in the bain-marie 
  4. beat the eggs and sugar until the mixture is white. 
  5. add cocoa, yeast, maizena and flour 
  6. introduce the butter and chocolate mixture 
  7. pour the mixture into the molded pan. 
  8. cook between 40 and 45 minutes 



[ ![chocolate cake 073.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-au-chocolat-073.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/article-cake-au-chocolat-moelleux.html>)

method of preparation: 

Source:  Magazine feminin Elle a table 
