---
title: truffles with cereals
date: '2012-12-21'
categories:
- Moroccan cuisine
- rice

---
Hello everybody, 

delicious sweets, for the coffee break ca tells you ??? truffles cereals very easy to achieve, really to fall, between the melting of the chocolate cream and the crunchy cereals, these truffles bright, to the delight of your taste buds. 

truffles that you can present at parties, for a coffee, or to present as a gourmet gift, everyone will appreciate ...   
Ingredients:   


  * 200 grs of pastry chocolate 
  * 80 grams of butter 
  * 150 ml of milk 
  * 2 glasses and a half of icing sugar 
  * 2 glasses of grated coconut 
  * 2 teaspoons of almonds, grilled and crushed 
  * 3 beaten eggs 
  * the lemon peel 
  * 1 teaspoon vanilla 
  * Cereals as needed 
  * coconut for decoration 


