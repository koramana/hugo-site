---
title: SPICE BREAD WITH FIGS & DRY APRICOTS
date: '2007-11-13'
categories:
- jams and spreads

---
_10cl of milk   
_ 250g of honey   
_ 100g of butter 

I heat up all that so that it melts   
I pour into a bowl or in the tank for the map 

and I add a beaten egg then 30g of brown sugar 

Then I add: 

_ 200g of flour   
_1 / 2 tbsp of bicarbonate   
_ 2cc of 4 spices   
_ a sachet of baking powder 

and some dried figs and dried apricots diced 

and bake for 20 minutes, thermostat 4/5 

be careful, it turns brown quickly, take care to cover the top with an aluminum foil if you bake in the oven 

for the map: sweet pastry program, 750g and the lowest degree of color 

it is even better after 2 or 3 days, finally if it remains lol 

ah oui j’oublais !!!! j’ai parsemé le dessus de sucre perlé..; Bien sûre c’est facultatif 
