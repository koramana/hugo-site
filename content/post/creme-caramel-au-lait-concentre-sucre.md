---
title: sweet caramel cream
date: '2016-09-30'
categories:
- panna cotta, flan, and yoghurt

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/creme-caramel-renversee-au-lait-concentre-sucre.CR2_.jpg
---
![creme-caramel-milk-INVERTED-focuses-sucre.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/creme-caramel-renversee-au-lait-concentre-sucre.CR2_.jpg)

##  sweet caramel cream 

Hello everybody, 

a very delicious dessert, a tasty and unctuous custard, which my children claim every time. A very easy recipe to make, this caramel spilled cream, which makes it just a cream with eggs, a flan with eggs with sweetened condensed milk. 

**sweet caramel cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/creme-renversee-au-lait-concentre-sucre.CR2_.jpg)

portions:  6 

**Ingredients**

  * 1 box of sweetened condensed milk 
  * 3 eggs 
  * 370 ml of milk 
  * 1 teaspoon of vanilla extract 

Caramel: 
  * 100 gr of sugar 
  * 1 tablespoon of water 



**Realization steps**

  1. heat the oven to 100 degrees C 
  2. prepare the caramel by cooking the sugar with a little water. 
  3. cover your ramekins, otherwise a mold with this caramel. 
  4. in a salad bowl, or a bowl for sweetened condensed milk. 
  5. add the eggs, whip a little, with a whisk or an electric whisk. 
  6. add milk and vanilla extract. 
  7. fill the ramekins. 
  8. take a baking tin, stainless steel, or in a pyrex mold, place in the ramekins 
  9. put the mold with the ramekins in the oven, and at this time pour in some hot water if possible (I tell you to avoid the water spills when trying to place the mold and the ramekins in the oven, it risks to 'be heavy for you). 
  10. close the oven, and let it cook, until, and trying to move the ramekins, you will notice that only the heart of the custard flakes delicately, so everything will depend on the size of your ramekins, but generally it will take between 1 hour, and 1 hour 15, do not worry if it will take more, the result will be perfect and ideal cooking. 
  11. let cool before serving. 



![creme-caramel-milk-concentrate-sucre.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/creme-caramel-au-lait-concentre-sucre.CR2_.jpg)

pour d’autres recettes de Flans, cliquez sur cette photo: 
