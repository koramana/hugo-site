---
title: Tiramisu with homemade mascarpone
date: '2010-11-13'
categories:
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/123-059.jpg
---
hello everyone, I tell you this recipe that I made in the month of Ramadan, because I noticed that photos have disappeared, I do not know why? So I put them back, and I redelivre this delicious recipe, in any case if you notice on my blog that there are missing photos in one article or another, please do not miss to thank me for everything. My first dessert for the sacred month of Ramadan, I wanted to eat tiramisu, but as I can not find mascarpone here in Boudouaou, and even if there 

##  Overview of tests 

####  please vote 

**User Rating:** 4.05  (  2  ratings)  0 

![123-059](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/123-059.jpg)

Hello everybody, 

I tell you this recipe that I did in the month of Ramadan, because I noticed that photos have disappeared, I do not know why? so I put them back, and I redelivre this delicious recipe, in any case if you notice on my blog that there are missing pictures in one article or another, please do not miss to prevent me  thank you for everything. 

My first dessert for the sacred month of Ramadan, I wanted to eat tiramisu, but as I can not find a mascarpone here in Boudouaou, and even if there is, I was told that it is too expensive, and I can not imagine wasting more than 1000 DA for a small dessert to be enjoyed with family, especially since the waste is not a principle for me. 

the main thing, I remembered that there was more than 6 months I came across a homemade mascarpone recipe, but I swear I could not fall back on this recipe, that the good blogger who does apologize for that. 

and here is the recipe: 

500ml of 35% cream (450ml for me) 

1 tablespoon of lemon juice 

prepare a bain-marie: 

put 4 cm of water in a saucepan and bring to a boil, 

when the water boils, reduce the fire  Mix the cream well in a deep bowl before putting it in a bain-marie (do not let the bottom of the bowl touch the water) 

Heat by stirring regularly a good groin of minutes, then add the lemon juice and mix again. 

When the cream laps the top of a spoon, it's ready. 

Cool, a groin of minutes, then put in a colander lined with etamine fold in 4 layers (dressing) slightly wet (a square folded in 4 will do) 

Cover with plastic film; and put in the refrigerator 24 hours. 

Flipping on a plate, it's ready to use 

![2010-08-11-1231 2](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/2010-08-11-1231_21.jpg) now I go to tiramisu recipe: 

\- 3 big eggs + 1 white that I had in the freezer 

\- 110 g of sugar 

\- 1 sachet of vanilla sugar 

\- 250 g of mascarpone 

-a small piece of gelatine in sheet (2cm by 2cm) just to have a nice texture, because I did not know how was going to be the result with homemade mascarpone 

\- 2 tbsp. soup of milk to dissolve gelatin 

\- 24 biscuits with spoon 

\- 1/2 liter of black coffee a little sweet 

\- 30 g bitter cocoa powder 

![2010-08-11-1232 2](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/2010-08-11-1232_21.jpg)

first put the gelatin in a little water so that it swells (at least 15 min before use) 

Separate the whites from the yolks.  Mix the yolks + sugar + vanilla sugar. 

Add the mascarpone to the whisk. 

in a bain-marie, put the gelatin in the milk and dissolve  Whisk in the egg whites and gently toss with the spatula to the previous mixture of egg yolk. 

Prepare black coffee.  Wet the biscuits in the coffee. 

![123-058](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/123-0581.jpg)

Line the bottom of the mold with the cookies. 

Cover with a layer of cream, egg, sugar, mascarpone. 

Alternate cookies and cream. 

Finish with a layer of cream. Sprinkle with cocoa. 

and now you have no more excuses, I assure you that the taste was there, only the texture of the mascarpone I got was a bit like that of the boursin, but in spite of everything the tiramisu was a great delight.  ![123-060](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/123-0601.jpg)

thank you for your visits, 

thank you for your comments, and sorry if I can not answer you because of the bad, impossible connection  thank you for always being faithful to my blog, and if you like it do not forget to subscribe to the newsletter to be up to date with any new publication.  merci 
