---
title: magic cake with blueberries
date: '2016-04-15'
categories:
- dessert, crumbles and bars
- gateaux, et cakes
- recettes sucrees
tags:
- desserts
- Pastry
- To taste
- Algeria
- Easy cooking
- eggs
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-magique-aux-myrtilles-1.jpg
---
[ ![magic cake with blueberries 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-magique-aux-myrtilles-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-magique-aux-myrtilles-1.jpg>)

##  magic cake with blueberries 

Hello everybody, 

I had forgotten this recipe, and yet I realized it when I made the [ magic cake with raspberries and lemon ](<https://www.amourdecuisine.fr/article-gateau-magique-aux-framboises-citron.html>) yes, two birds with one stone. My children like blueberries, and I prefer cakes with slightly acidic fruits, to break the sweet taste. So we prepared the recipe, I saw the dough out of two, and I made a cake with blueberries for them, and another with raspberries for me and my husband. 

To be honest, the magic cake with blueberries is a recipe to try imperatively! It's too good. You will not realize the number of rooms you will eat, because it passes quickly. I do not tell you how much I have eaten just enough time to take the pictures, so do not ask the question why I did not post beautiful pieces, which contained the most fruit, hihihihihi. 

![magic cake with blueberries](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-magique-aux-myrtilles.jpg)

**magic cake with blueberries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-magique-aux-myrtilles-3.jpg)

portions:  16  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients** for a mold 22 cm in diameter 

  * 4 medium eggs at room temperature, 
  * 150 gr of sugar, 
  * 1 C. water, 
  * 125 gr of butter, 
  * 115 grams of flour, 
  * 1 pinch of salt, 
  * 500 ml whole milk, 
  * the zest of 1 lemon organic or untreated, 
  * the juice of 1 half lemon 
  * 125 gr of fresh blueberries. 



**Realization steps**

  1. Melt the butter and let cool. 
  2. Warm the milk and preheat the oven to 150 ° C. 
  3. Separate the yellows and whites. Climb the whites to firm snow with a pinch of salt. Book. 
  4. Beat the egg yolks, sugar and water for a few minutes. Then add the melted butter, the juice of the half lemon, the lemon peel and continue beating. 
  5. Sift on the mixture the flour and mix quickly just to introduce the flour. always to the drummer. Add the warm milk gradually and beat to mix everything well. 
  6. gently fold the whites into the dough a few times using a hand whisk. 
  7. pour the dough into the removable base mold well lined with baking paper, and add the blueberries on top.   
use a mold with removable base, because you can not remove the cake from the mold is like a blank, and in addition you can not spill it because you will crush the layer of sponge cake from above. Stitch your removable mold with cooking paper, so that the liquid does not come out from below. 
  8. Bake for about 50 minutes. Check the cooking: the cake must be firm to the touch. If it still seems too liquid, add a few minutes. 



[ ![magic cake with blueberries 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-magique-aux-myrtilles-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/gateau-magique-aux-myrtilles-2.jpg>)
