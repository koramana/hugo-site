---
title: Crème brûlée, crème brûlée recipe
date: '2014-03-09'
categories:
- appetizer, tapas, appetizer

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-047.CR2_thumb.jpg
---
[ ![creme brulee 047.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-047.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-047.CR2_2.jpg>)

Hello everybody, 

a creamy crème brûlée ... the best I could achieve, until today .. 

I'm going to be frank with you my friends, I've always made homemade creams and custards, but I've never had that creamy texture, smooth, soft and melting in the mouth, as I had during my realization of this creme brulee ... 

the secret ... the cooking of these creams is very delicate, we always say: but why we put in a bain-marie? the secret is that we do not want the cream to boil, and that there are bubbles of air in the cream, which will not give us a uniform texture, and smooth with this cream. 

then the most important point, to avoid all this, is to achieve the cooking at a low temperature, 100 degrees C, it will appear for certain, that it is really low, but it is the ideal temperature to have a result perfect. 

some say, cook at this temperature for 45 minutes, in fact, it's up to you to see, it will depend on the size of the ramekins, and also the volume of the oven. 

In the end, I had a really creamy cream wish. 

in any case, this recipe is the version of the creme brulee based sweetened condensed milk, and what creamy I had, a tasty cream and sweet on the palate ...   
  


**Crème brûlée, crème brûlée recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-021.CR2_thumb.jpg)

portions:  6  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 1 box of sweetened condensed milk 
  * 3 eggs 
  * 370 ml of milk 
  * 1 teaspoon of vanilla extract 
  * 40 gr brown sugar (for decoration) 

you will need a blowtorch, if possible. 

**Realization steps**

  1. heat the oven to 100 degrees C 
  2. in a salad bowl, or a bowl for sweetened condensed milk. 
  3. add the eggs, whip a little, with a whisk or an electric whisk. 
  4. add milk and vanilla extract. 
  5. fill the ramekins.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-004.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-004.CR2_thumb.jpg>)
  6. take a baking tin, stainless steel, or in a pyrex mold, place in the ramekins 
  7. put the mold with the ramekins in the oven, and at this moment pour in some hot water if possible (I tell you to avoid that the spills when trying to place the mold and ramekins in the oven, it is likely to be heavy for you). 
  8. close the oven, and let it cook, until, when trying to move the ramekins, you will notice that only the heart of the custard flakes delicately, so everything will depend on the size of your ramekins, but generally it will take between 1 hour, and 1 hour 15, do not worry if it will take more, the result will be perfect and ideal cooking 
  9. remove from the oven, and let cool, when serving, sprinkle with a little sugar brown sugar,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-015.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/creme-brulee-015.CR2_thumb.jpg>)
  10. and caramelize with a blowtorch, if you do not have a blowtorch, light the oven from above, when it is red, place the ramekins all ready for this fire, leave between 2 to 3 minutes and voila it is caramelized 


