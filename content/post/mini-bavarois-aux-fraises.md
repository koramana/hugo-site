---
title: Bavarian strawberry mini
date: '2018-02-22'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- sweet recipes
tags:
- Dessert
- Algerian cakes
- To taste
- Cakes
- desserts
- Pastry
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mina-1-bavarois-aux-fraises.jpg
---
[ ![mina 1 Bavarian strawberry](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mina-1-bavarois-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mina-1-bavarois-aux-fraises.jpg>)

##  Bavarian strawberry mini 

Hello everybody, 

Thank you darling for this recipe, and these beautiful photos, and here I pass you the recipe of Mina without delay: 

![mini bavarois with strawberries from mina 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mini-bavarois-aux-fraises-de-mina-1.jpg)

**Bavarian strawberry mini**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mini-bavarois-aux-fraises-de-mina-2.jpg)

portions:  8  Prep time:  30 mins  cooking:  12 mins  total:  42 mins 

**Ingredients** for the biscuit 

  * 4 eggs 
  * 125 gr of sugar 
  * 125 gr of flour 

for the foam: 
  * 300 gr of strawberries 
  * 60 gr of sugar 
  * 3 and a half gelatine leaves 
  * 200 ml liquid whole cream 



**Realization steps** Prepare the biscuit: 

  1. beat the eggs and the sugar, until it triples in volume. 
  2. introduce the flour gently. 
  3. pour the dough into a baking pan lined with baking paper, and bake in a preheated oven at 180 ° C for 10 to 12 minutes (or depending on the capacity of your oven) 
  4. remove the cake after cooking and let cool on a wire rack. 

Prepare the strawberry bavarois: 
  1. mix the strawberries with the sugar and a few drops of lemon juice 
  2. take a few tablespoons of this preparation and put to heat, without boiling 
  3. add the gelatine already ramoli in cold water and you will have pressed well between the fingers. 
  4. then add this mixture in the remaining strawberry coulis, mix well and leave in the fridge for 1 hour 
  5. drip the cream very cold in chantilly and incorporate the delicatement with strawberry coulis 
  6. pour this preparation into mini molds of the shape of your choice. 
  7. Cut biscuit slices of the same size as the mussels and press to adhere the biscuit to the mousse. 
  8. stay cool for 4 hours 

for the topping, I kept a small amount of the strawberry jelly that I prepared before, and decorated it with. 

[ ![mini Bavarian strawberry mina](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mini-bavarois-aux-fraises-de-mina1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mini-bavarois-aux-fraises-de-mina1.jpg>)
