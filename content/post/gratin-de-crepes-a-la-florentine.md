---
title: gratin pancakes with florentine
date: '2015-02-02'
categories:
- vegetarian dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gratin-de-crepes-florentines-068a.jpg
---
[ ![gratin pancakes with florentine](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gratin-de-crepes-florentines-068a.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-crepes-a-la-florentine.html>)

##  gratin pancakes with florentine 

Hello everybody, 

Here is a very good recipe for pancakes, a real treat, that I concocted this week, and that everyone loved at home, and most importantly, they ate the spinach without detecting it, I really like recipes like that. 

In any case to prepare this gratin of pancakes to the Florentine, you have to go a little in advance, because you must first make the pancakes, then the sauce, and after the oven ... So do not do not say that I did not warn you !!!   


**gratin pancakes with florentine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gratin-de-crepes-florentines-068a.jpg)

portions:  4  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients** For pancakes: 

  * 250 g buckwheat flour 
  * 2 eggs 
  * 2 tablespoons of oil 
  * 400 ml of water 
  * 1 pinch of salt. 
  * [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video")

For the stuffing: 
  * 400 g spinach 
  * 400 g ricotta 
  * 200 g of parmesan cheese 
  * 2 eggs 
  * 6 tablespoons tomato coulis 
  * salt pepper 



**Realization steps** prepare the pancakes: 

  1. in a bowl, form a well with the dry elements, 
  2. break the eggs in the center and mix vigorously with the whisk, adding the milk gradually. 
  3. To be sure that there are no more lumps, pass the preparation in a colander or blender. 
  4. Let the dough rest for 1 hour at room temperature. 
  5. heat a pan or pancake and oily. 
  6. Pour a ladle of dough by spreading it and cook each pancake on both sides. Return as soon as the coloring.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/2012-02-10-gratin-de-crepes-florentines.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/02/2012-02-10-gratin-de-crepes-florentines.jpg>)

the joke: 
  1. cook the spinach with a little water, wring out and leave to cool 
  2. mix with ricotta and whipped eggs. add the Parmesan cheese. 
  3. spread each pancake with this stuffing, roll the stretches, 
  4. cut each roll into lozenges of the same size. 
  5. Preheat the oven to 210 degrees C 
  6. Place the pancake lozenges in a baking tin. 
  7. Top them with béchamel, and garnish with tomato coulis, sprinkle with a little parmesan cheese. 
  8. make gratin between 10 to 15 min 



[ ![gratin-of-pancakes-Florentine-059.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/gratin-de-crepes-florentines-059.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-crepes-a-la-florentine.html>)
