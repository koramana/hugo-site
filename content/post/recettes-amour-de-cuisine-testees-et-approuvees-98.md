---
title: 'kitchen love recipes tested and approved # 98'
date: '2016-11-02'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg
---
![2011-09-04-to-the-moment tout1_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/2011-09-04-pour-le-moment-tout1_thumb1.jpg)

##  kitchen love recipes tested and approved # 98 

Hello everybody, 

Back with your recipes tested and approved from my blog. It is with great pleasure that I highlight your very successful and fabulous achievements. 

Your comments also make me very happy, especially when a person tells me the effect that his recipe has made around her. Thank you very much for keeping me informed each time. 

So I'm not going to talk too much and I'm going to your wonderful recipes, and do not forget you can always pass me pictures of your achievements from my blog on this email: 

![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg)

![Paris Brest](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/paris-brest.jpg)

[ Paris brest with homemade praline ](<https://www.amourdecuisine.fr/article-recette-paris-brest.html>) at Mathilde Gérard 

![Makrout El Luz](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/makrout-el-louz.jpg)

[ Makrout El Luz ](<https://www.amourdecuisine.fr/article-makrout-el-louz.html>) at andgie esthetique 

![terrine](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/terrine.jpg)

[ Chicken liver terrine ](<https://www.amourdecuisine.fr/article-terrine-de-foies-de-volaille.html>) at Celine Luciani 

![brioche, homemade](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/brioche-fait-maison.jpg)

[ Home bun ](<https://www.amourdecuisine.fr/article-brioche-fait-maison.html>) at Céline's place 

![blank-nestle](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/flan-nestl%C3%A9.jpg)

[ concentrated caramel cream ](<https://www.amourdecuisine.fr/article-creme-caramel-au-lait-concentre-sucre.html>) at Sadjia 

![Biscuit Roll](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/biscuit-roul%C3%A9.jpg)

[ Biscuit Roll ](<https://www.amourdecuisine.fr/article-genoise-facile-pour-biscuit-roule.html>) at Assia K. 

![chocolate cake](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/gateau-au-chocolat.jpg)

[ Fluffy chocolate cake ](<https://www.amourdecuisine.fr/article-cake-au-chocolat-moelleux.html>) at Lillia's 

![tajine-hlou](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/tajine-hlou.jpg)

[ Lamb tajine with prunes and almonds ](<https://www.amourdecuisine.fr/article-tajine-d-agneau-aux-pruneaux-amandes-abricots.html>) at Lillia's 

![bun-house-nadia](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/brioche-maison-nadia.jpg)

[ Homemade brioche ](<https://www.amourdecuisine.fr/article-brioche-fait-maison.html>) at Samy aya (flower DZ) 

![croquet-a-la-jam](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/croquet-a-la-confiture.jpg)

[ Croquets with jam ](<https://www.amourdecuisine.fr/article-croquet-a-la-confiture-gateaux-secs-a-la-confiture.html>) at Ya Rahman's. 

![rolls au chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/roul%C3%A9-au-chocolat.jpg)

[ Biscuit rolled with hazelnuts ](<https://www.amourdecuisine.fr/article-biscuit-roule-aux-noisettes-et-creme-au-pheladelphia.html>) at Samia L. (I replaced hazelnuts with 30 g cocoa and 20 g flour) 

![grimole-to-apples](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/grimole-aux-pommes.jpg)

[ Grimolle with apples ](<https://www.amourdecuisine.fr/article-grimolle-aux-pommes-amandes.html>) at Malika Bedrani 

![madeleine-home fouzia](https://www.amourdecuisine.fr/wp-content/uploads/2016/11/madeleine-chez-fouzia.jpg)

[ Madeleines ](<https://www.amourdecuisine.fr/article-les-madeleines-recette-inratable.html>) chez Samia G. S 
