---
title: holiday cakes / pineapple bavarois and crepes
date: '2012-12-23'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- ramadan recipe
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bavarois-ananas-crepes-023_thumb.jpg
---
Hello everybody, 

a **creamy cream** and **frothy** to the taste of the **pineapple** interspersed with delicious [ pancakes ](<https://www.amourdecuisine.fr/article-recette-de-crepes-faciles-et-delicieuses.html> "recipe for easy and delicious pancakes") **fondant** , to form a very good [ Bavarian ](<https://www.amourdecuisine.fr/bavarois-mousses-charlottes-recette-lagar-agar>) , a dessert to present to its guests with pride. 

a recipe in my archives for 2 months now, hihihihi, and that I have the pleasure to share with you, and here is more [ Bavarian recipes ](<https://www.amourdecuisine.fr/bavarois-mousses-charlottes-recette-lagar-agar>)

**holiday cakes / pineapple bavarois and crepes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bavarois-ananas-crepes-023_thumb.jpg)

Recipe type:  dessert, bavarian  portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * of the [ pancakes ](<https://www.amourdecuisine.fr/article-recette-de-crepes-faciles-et-delicieuses.html> "recipe for easy and delicious pancakes")
  * 100 gr of canned pineapple 
  * 200 ml thick cream 
  * 6 grams of gelatin 
  * pineapple aroma 
  * 50 gr of sugar 
  * dark chocolate for decoration 



**Realization steps**

  1. In a salad bowl put the eggs with the sugar. Add the flour, vanilla and butter in pieces. mix well and add the milk gradually. add oil, mix again. cover with plastic wrap and let stand for 2 hours. Cook the pancakes in a non-stick pan. 
  2. soften the gelatin in a little water. whip the cream in whipped cream and stir in the sugar. Melt the gelatin with a tablespoon of fresh cream over low heat. add it to the whipped cream. 
  3. cut the pineapple into a piece, add it to the mousse, add the pineapple aroma, and mix gently. 
  4. you can use the pancakes as they are in a big pastry circle, or you can prepare mini bavarois, and in this case the, use the small pastry circles as a cutter, to form mini pancake discs. 
  5. start the cake assembly by placing a pancake disk at the bottom, pour a little pineapple mousse, cover with two pancake disks. then cover with pineapple mousse, then two more pancake discs, and finally with a final layer of pineapple mousse. 
  6. place the cake, or mini cakes in the fridge for 2 to 3 hours, then let out and decorate with a nice layer of melted chocolate in a bain-marie 
  7. let the chocolate take a little and serve. 



{{< youtube jDyzikCZC2o >}} 

thank you for your visits and comments 

passez une belle journée. 
