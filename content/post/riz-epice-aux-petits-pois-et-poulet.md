---
title: spicy rice with peas and chicken
date: '2011-03-29'
categories:
- dessert, crumbles et barres
- gateaux, et cakes
- recettes sucrees
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/03/riz-aux-petits-pois-6_2.jpg
---
##  spicy rice with peas and chicken 

Hello everybody, 

Monday is always a hard day for me, especially since now I'm going to remake, myself the paint paper, in addition to the paints, so after paints the wall (I work wall by wall, not to say that I do it in the blink of an eye) I went to the laundromat, because Monday is my laundry day (from 16:00 to 18:00) so lift up flat. 

Ideas escaped my head, but what should I do to eat! I went to the kitchen, to look here and there, then I said, we go for a rice. it's a recipe totally improvised, not too far from a delicious Indian rice, must say that I had all the spices out of my cupboards, so do not worry if the list is a bit long, in anyway he was too good.   


**spicy rice with peas and chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/riz-aux-petits-pois-6_2.jpg)

**Ingredients**

  * Ingredients: 
  * 4 pieces of chicken (you can use either the thighs or then the chest in pieces) 
  * 3 onions 
  * 2 heads of garlic 
  * 4 tablespoons of table or olive oil 
  * 1 nice handful of peas 
  * 2 bay leaves 
  * 2 fresh tomatoes cut into pieces 
  * 1 tablespoon tomato concentrate 
  * 10 sprigs of parsley 
  * salt 
  * 3 glasses of rice (200 ml the glass) 
  * ½ carrot 
  * water 

spices: 
  * 1 clove, 
  * ½ teaspoon coriander powder 
  * 4 green cardamom seeds, 
  * ½ teaspoon of turmeric, 
  * ½ cup of garam masala, 
  * 1 pinch of grated nutmeg, 
  * 8 black pepper seeds, 
  * 8 coriander seeds, 
  * 1 pinch of cinnamon, 
  * ½ teaspoon of black pepper. 



**Realization steps**

  1. cut the onion into pieces 
  2. place it in a pot with 3 tablespoons of oil 
  3. make well gilded turning 
  4. add the chicken pieces 
  5. add spices, and salt, brown well 
  6. add the crushed garlic, and the tomato in piece 
  7. made to simmer, the onion must become translucent 
  8. add the tablespoon of tomato paste and the peas 
  9. mix a little and add just a little water. 
  10. add the bay leaves, and chopped parsley 
  11. add to the max 7 glasses of water (200 ml) and let it cook 
  12. in a frying pan, add the tablespoon of olive oil, and brown the previously rinsed rice. 
  13. grated over the half carrot 
  14. mix until the rice turns a nice color 
  15. sprinkle sauce of small weights with the vegetables in it (it must be: the sauce without vegetables in 6 glasses of 200 ml each: 1 glass of rice, for 2 glasses of water) so if it lacks sauce, add a little water) if we like red sauces you can dilute a tomato concentrate in the water you add more. and grind the salt 
  16. cook on low heat 
  17. you can fry chicken pieces (if it's thighs) 



![rice-spicy-to-peas-.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/03/riz-epice-aux-petits-pois-1.jpg)
