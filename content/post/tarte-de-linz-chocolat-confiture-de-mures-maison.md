---
title: Linz chocolate tart / homemade blackberry jam
date: '2015-08-23'
categories:
- cakes and cakes
tags:
- Algerian cakes
- Pastry
- desserts
- Shortbread
- Fruits
- Cakes
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/linz-chocolat10_thumb1.jpg
---
##  Linz chocolate tart / homemade blackberry jam 

Hello everybody, 

here is one of the delights of my dear Lunetoiles, this time, it's a very good pie, judge for yourself photos, and do not forget, if you also want to share your delicious recipes at home, it will make me great pleasure, here is the link to follow: [ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette>) . 

So without too much delay, we go to the recipe: 

**Linz chocolate tart / homemade blackberry jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/linz-chocolat10_thumb1.jpg)

portions:  8  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients** The base of shortbread with chocolate: 

  * 175 gr of flour 
  * 55 gr of cocoa (Van Houten) 
  * 230 gr of butter at room temperature 
  * 115 gr of sugar 
  * 4 tbsp. finely ground espresso (optional) 
  * 1 pinch of salt 

The blackberry jam: 
  * 650 gr of blackberries 
  * 300 gr of sugar (150 gr of fructose for me) 
  * juice and zest of a yellow lemon 



**Realization steps** Preparation of the blackberry jam: 

  1. Put the blackberries in a large saucepan, with the sugar and the juice and zest of the lemon. 
  2. Bring to a boil and boil approx. 10 to 15 min. Until it reduces and the jam takes. 
  3. Book. 

Prepare the chocolate shortbread dough: 
  1. In a medium bowl, mix flour and cocoa together. 
  2. In a large bowl, cream butter and sugar until smooth. 
  3. Stir in espresso and salt until smooth. 
  4. Add the flour / cocoa mixture and mix well. 
  5. The dough can be very stiff. Divide the dough into two balls: ⅔ and ⅓ 
  6. Reserve the smallest ball of dough (⅓) in the refrigerator. 
  7. Preheat the oven to 180 ° C. 
  8. With your fingers, press the ball of dough (⅔) into a buttered round mold 20 to 24 cm in diameter. 
  9. Press with your fingers, going up a little to form edges. Prick with a fork. 
  10. Spread the blackberry jam evenly over the dough. 
  11. Take out the little ball of dough from the refrigerator (⅓) and roll it out with a roller. 
  12. Cut out parallel strips using a notched kitchen wheel. 
  13. Place strips of pasta on the pie, spacing a few centimeters. 
  14. And do the same in the other direction to do like a grid. ( see the pictures ) 
  15. Bake for 30 to 35 minutes. Cool in the pan and peel off the edges and remove from the pan. 



merci pour vos commentaires, et visites 
