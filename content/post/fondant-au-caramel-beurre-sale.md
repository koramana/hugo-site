---
title: Salted butter caramel fondant
date: '2017-09-10'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Chocolate fondant
- Cakes
- Chocolate
- Cake
- To taste
- desserts
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fondant-au-caramel-beurre-sal%C3%A9.jpg
---
[ ![salted butter caramel](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fondant-au-caramel-beurre-sal%C3%A9.jpg) ](<https://www.amourdecuisine.fr/article-fondant-au-caramel-beurre-sale.html/fondant-au-caramel-beurre-sale>)

##  Salted butter caramel fondant 

Hello everybody, 

A recipe love at first sight, yes, when you come to a fondant caramel salted butter, we do not have much to say apart, I fonnnnnnnnnnnnnds! 

So today is the recipe of a friend and reader Oum Nour, a recipe she found on the site of Fashion cooking, it tells you something? Well this is the blog of our beautiful Anne Sophie the winner of the best pastry season 2014, yes yes. 

But in any case our dear Oum Nour has revived a little recipe in its own way, because it did not have soft caramel candies for salted butter caramel   


**Salted butter caramel fondant**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fondant-au-caramel-beurre-sal%C3%A9.jpg)

Cooked:  French  Recipe type:  cake, taste  portions:  8  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 200 gr of dark chocolate 
  * 100 gr of sweet butter 
  * 100 gr of brown sugar 
  * 4 eggs 
  * 1 tablespoon flour 
  * 100 gr of half-salted butter (with salt crystals) 
  * 3 tablespoons of [ salted butter caramel ](<https://www.amourdecuisine.fr/article-caramel-au-beurre-sale.html> "salted butter caramel")



**Realization steps**

  1. Preheat the oven to 200 °. 
  2. Break the chocolate into pieces and melt it in a bain-marie with the butter and salted butter caramel 
  3. Pour the melted chocolate mixture into a salad bowl. Add the brown sugar and mix until a smooth mixture is obtained. 
  4. Add the eggs one by one then the flour. 
  5. Pour the dough into a buttered mold about 20 cm in diameter, then put in the oven at 200 ° for 5min. 
  6. After 5 minutes, lower the temperature to 120 ° and cook for about 10-12 minutes. Monitor cooking very often with the tip of a knife (this is the secret of fondant). The environment must still be a little liquid. 
  7. Remove from the oven and let cool for 15 minutes before serving. Serve immediately warm and flowing or reserve a cool night for a more CLOSED texture. 


