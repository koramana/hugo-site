---
title: appetizer olive and goat
date: '2017-11-20'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- Tapas
- Aperitif
- Apero
- Mouthing
- Christmas

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/amuse-gueule-petite-bouchee.CR2_thumb1.jpg
---
##  appetizer olive and goat 

Hello everybody, 

Here is a quick recipe, easy and delicious of appetizers, that you can present in aperitif and aperitif, especially for the people who will celebrate the reveillon and the celebrations of Christmas .... 

A recipe that is done in two or three movements, and here it is ready .... 

I put you the video just to see what is it tip top, hihihihihih 

**appetizer olive and goat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/amuse-gueule-petite-bouchee.CR2_thumb1.jpg)

**Ingredients**

  * flaky pasta cut into a slice, otherwise wind 
  * a handful of black olives 
  * 2 to 3 slices of goat cheese (you can increase the dose depending on the number of people) 
  * 2 tablespoons pheladelphia cheese, otherwise 3 cheeses 
  * cherry tomatoes for decoration 
  * basil leaves for decoration. 



**Realization steps**

  1. cook the pie slices in a preheated oven at 160 degrees for 10 to 15 minutes, depending on your oven. 
  2. in the bowl of the blinder, put the olives, the two cheeses, 
  3. do 2 to 3 strokes to have a homogeneous mixture, to you to see the texture that you want to obtain, I like to feel the crunch of the olives for my part. 
  4. decorate the slices cooked with this olive goat mixture 
  5. garnish with chopped tomatoes and basil leaves 
  6. it's ready, enjoy ... .. 



and for even more recipes of tapas, amuse bouche, aperitif, apero and amuse gueulle, click on the photos:   
  
<table>  
<tr>  
<td>

[ ![amuse bouche](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/fleurs-a-la-creme-d-artichaut_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-fleurs-a-la-creme-d-artichauts-tapas-et-amuse-bouche-113751782.html>) 
</td>  
<td>

[ ![cucumber crab and goat](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tandoori-006.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tapas-du-reveillon-concombre-au-chevre-et-crabe-113788582.html>) 
</td>  
<td>

[ ![crab and parmesan cheese 021](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bouchees-au-crabe-et-parmesan-021_thumb5.jpg) ](<https://www.amourdecuisine.fr/article-bouchees-de-crabe-au-parmesan-101566392.html>) 
</td> </tr>  
<tr>  
<td>

[ ![amuse mouth artichokes goats 088](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/artichauts-chevres-088_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-chevre-dore-sur-fond-d-artichaut-100098265.html>) 
</td>  
<td>

[ ![appetizer with aubergines](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/aubergines-rolls-028_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-roules-aux-aubergines-113337954.html>) 
</td>  
<td>

[ ![tomato nests with eggs 028](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/nids-de-tomates-aux-oeufs-028_thumb.jpg) ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs-101484090.html>) 
</td> </tr>  
<tr>  
<td>

[ ![stuffed olives 040 a](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/olives-farcis-040-a_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-brochettes-d-olives-farcies-102948239.html>) 
</td>  
<td>

[ ![homemade breadsticks](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gressins-au-basilic_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-gressins-tres-facile-113765720.html>) 
</td>  
<td>

[ ![smoked salmon roulade](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/roulade-au-saumon-fume_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-roulades-de-saumon-fume-au-mascarpone-et-fenouil-101257027.html>) 
</td> </tr>  
<tr>  
<td>

[ ![tuna rice mouths](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/amuses-bouches-au-riz-thon_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-boulettes-de-riz-au-thon-97137268.html>) 
</td>  
<td>

[ ![TARTARE OF SMOKED SALMON](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/TARTARE-DE-SAUMON-FUMEE_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tartare-de-saumon-fumee-en-cornets-de-crepes-100294277.html>) 
</td>  
<td>

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/hasselback-potatoes-hasselbackpotatis_thumb.jpg) ](<https://www.amourdecuisine.fr/article-pommes-de-terre-a-la-suedoise-hasselbackpotatis-108541271.html>) 
</td> </tr>  
<tr>  
<td>

[ ![scallop walnut 009](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/noix-de-saint-jacques-009_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-noix-de-saint-jacques-bouchee-a-la-creme-de-persil-97454414.html>) 
</td>  
<td>

[ ![Pepper rolls 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/Roules-aux-poivrons-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-roules-aux-poivrons-97975273.html>) 
</td>  
<td>

[ ![dumplings with surimi amuse](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/boulettes-au-surimi_thumb3.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-boulettes-au-surimi-113306581.html>) 
</td> </tr> </table>
