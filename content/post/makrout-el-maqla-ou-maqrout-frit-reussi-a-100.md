---
title: makrout el maqla or fried maqrout 100% successful
date: '2017-04-26'
categories:
- fried Algerian cakes
- Algerian cakes- oriental- modern- fete aid
tags:
- Aid
- Oriental pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/04/makrout-el-maqla_.jpg
---
![makrout el maqla or fried maqrout 100% successful](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/makrout-el-maqla_.jpg)

##  makrout el maqla or fried maqrout 100% successful 

Hello everybody, 

And here is the recipe for 100% makrout el makla or fried maqrout. You were many and many to ask the trick to achieve this delight. 

And here is for you today the makrout el makla recipe or fried maqrout video made by my friend Mina minnano, just for you. This recipe is super simple, the ingredients are for everyone, the recipe is very simple, just follow the video to make the makrout el makla. 

the video is also in Arabic. 

**makrout el maqla or fried maqrout 100% successful**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/makrout-el-maqla.jpg)

**Ingredients**

  * 3 measures of medium semolina 
  * 1 C. Cubèbe powder 
  * 1 C. cinnamon powder 
  * 1 C. vanilla sugar 
  * water + orange blossom water. 
  * a pinch of salt. 
  * 1 fat measure: both for smen and oil 

for the stuffing: 
  * paste of dates 
  * a little cinnamon 
  * a little ground cloves 
  * some butter butter 
  * Orange tree Flower water. 



**Realization steps**

  1. Work the date paste with the ingredients previously given until you have a soft ball. 
  2. form small balls of almost 10 gr and set aside. 
  3. In a large bowl, pour semolina, salt, cinnamon, vanilla and cubèbe. Mix. 
  4. Make a fountain and pour the melted fat. Mix well so that the butter is well absorbed by the semolina. Let stand overnight. 
  5. the next day, take the dough, sand again and pick it up with a mixture of water and orange blossom water. 
  6. form pasta balls of almost 28 gr 
  7. stuff it with the dumpling of dates, and stick in the moon cake mold. 
  8. let stand and dry makrout between 1 am and 5 pm depending on the temperature of the room. 
  9. fry in a hot oil bath. 
  10. lower the heat when cooking. 
  11. when the makrut are well fried, soak them in honey perfumed with orange blossom, and place in small boxes. 
  12. you can also keep makrout without putting them in honey, in a hermetically sealed box for 15 days. 



![makrout el maqla or fried maqrout](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/makrout-el-maqla-ou-maqrout-frit.jpg)
