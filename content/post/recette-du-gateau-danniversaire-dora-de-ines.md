---
title: birthday cake recipe Dora de Ines
date: '2012-02-01'
categories:
- salades, verrines salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/02/dora-ines-033-300x225.jpg
---
##  birthday cake recipe Dora de Ines 

Hello friends, 

I go back this cake at the request of one of my readers, who finds a difficulty in finding recipes on my blog, in any case I try to put indexes as and when I have the time, and there is a category for [ birthday cakes ](<https://www.amourdecuisine.fr/categorie-11700499.html>) , and on this occasion, I tell my nephew Salah eddine, birthday jewels, because it's his birthday today. 

so without delay I give you the recipe: 

so my basic cake was a savoy cake, cut into three discs and stuffed with muslin cream, and pieces of peach, covered with a beautiful layer of royal cream, and garnish around grated almonds, and at the top of a paste of sugar paste with a beautiful drawing of Dora. 

for the cake I made the savoy cake because my husband loved the cake, recipe [ right here ](<https://www.amourdecuisine.fr/article-37650983.html>)   


**birthday cake recipe Dora de Ines**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/dora-ines-033-300x225.jpg)

portions:  12  Prep time:  60 mins  cooking:  40 mins  total:  1 hour 40 mins 

**Ingredients** for the muslin cream: 

  * 500 ml of milk 
  * 150 g of sugar 
  * 2 bags of vanilla sugar 
  * 4 egg yolks 
  * 60 g cornflour 
  * 200 g soft butter or margarine 



**Realization steps** better start with the cream muslin, to give it time to rest and cool 

  1. Remove the butter from the refrigerator so that it is at room temperature. 
  2. Mix the egg yolks, cornflour, flour and sugars in a saucepan and whisk well. 
  3. Pour the hot milk over, mix at the same time, and put back on medium heat while stirring constantly with a wooden spoon until the mixture thickens (about 4-5 minutes). 
  4. Out of the fire add half of the butter. 
  5. Let cool, add the other half of the butter in the cooled cream and whisk well with an electric mixer if possible until a light and foamy mass that must be almost white. 



cover a cake disc with the muslin cream, and decorate with peach cut into pieces (or other fruit of your choice) 

cover with the other disc and make parreil, covering the cake with cream muslin, and fishing. 

for my cake I made half of these ingredients. 

  * 500g icing sugar 
  * 2white eggs and a half 
  * juice of half a lemon 
  * food coloring (I put red to have a beautiful pink, but it did not appear too much, hihihi not serious) 



Work 3/4 of the sugar with the whites and the lemon juice for 15 minutes (by hand) 8 minutes (in the blender) 

When it's homogeneous, clear, white and light add the rest of the sugar and whisk another 2 minutes 

if it's not firm enough add a little icing sugar 

try not to whip in the blender so that there are as few air pockets as possible 

once ready, cover with damp cloth so that it does not dry out 

Beat vigorously before using 

It can be kept in the fridge for one week provided it is covered with 2 plastic films: the first directly against the surface: to avoid a crust 

Finally can make it thicker or thinner as needed. 

knowing that I find that the royal cream is lighter than the cream in order to garnish a cake, and less sweet too. 

allow time to dry a little, and garnish the sides with roasted almonds. 

and let it dry in the open air, to begin the preparation of the drawing Dora. 

for that I printed the picture of Dora, who would be the size of the cake. 

take the white sugar pie and spread it well and cut a circle: 

it's your support, put this disc on a paper towel, and start your work, 

first take a little bit of the sugar paste that you have colored in blue, (the sugar dough can always be replaced by the almond paste but in this case it must immediately put your record on the Cake and work on the cake) not like the sugar paste that will harden and that is going to be a solid support that you can then deposit on the cake without problem. 

So take the blue sugar paste, finely spread it (1 mm if possible), but you have to work on a surface sprinkle well with icing sugar so that the paste does not stick to your work surface, shape a half circle equal to white circle, to have the part of the sky on your image. 

do the same with the green sugar paste. 

to paste the pasta to each other to pass a brush slightly wet, to stick one face to another: 

now color a part of the sugar paste (it is necessary to color each small part of the sugar paste, so as not to have a lot of rest, but also it is necessary to make a sufficient quantity, to do such or such part), therefore color the dough with a color that can be very close to the color of the dora skin, and take the print drawing that you cut out according to the features of your drawing, place it on the dough and cut   
st. 

place dora on the drawing already made (stick with the brush delicately wet. 

and start to cut out each part of different color of dora (the hair, the eyes, the knitting, the pantallon, the shoes, which I forget) 

make adequate colors of the sugar paste, place your cut pieces on each colored paste, and cut, and dress dora. 

Desollee no pictures, because I did this work at 3am, I was squeezed and tired, so no head to take pictures. 

garnir le gateau comme vous le voulez apres. 
