---
title: Biscuit rolled with lemon mousse
date: '2017-12-06'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Fruits
- desserts
- To taste
- Breakfast
- Ramadan
- Algeria
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/biscuit-roul%C3%A9-a-la-mousse-de-citron-.CR2_.jpg
---
[ ![biscuit rolled with lemon mousse .CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/biscuit-roul%C3%A9-a-la-mousse-de-citron-.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-mousse-de-citron.html/biscuit-roule-a-la-mousse-de-citron-cr2>)

##  Biscuit rolled with lemon mousse 

Hello everybody, 

If you like the taste of lemon, and the fondant of a genoise well ventilated, this biscuit rolled with lemon mousse is for you. This biscuit is of an incomparable fudge, I promise you that you can finish this cake by yourself in the blink of an eye, you really have to control yourself, hihihihi. 

For the rolled biscuit recipe, I keep my basic recipe that I succeed at all times .... and the result is a soft biscuit, all melting, and above all without smell of egg. 

If not for the decoration and the stuffing of this delight, I opted for a lemon mousse without gelatin, neither too sweet, nor too tart, a taste that takes you to the clouds with this biscuit rolled with lemon mousse all cloudy , hihihih.   


**Biscuit rolled with lemon mousse**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/biscuit-roul%C3%A9-a-la-mousse-de-citron-1.jpg)

portions:  8  Prep time:  50 mins  cooking:  15 mins  total:  1 hour 5 mins 

**Ingredients** for the rolled biscuit: 

  * 5 eggs (for a tray of 30 cm by 40cm) 
  * 140 grams of sugar 
  * 150 grams of flour 
  * 1 cup of vanilla coffee 
  * 1 cup of baking powder 
  * zest of a lemon 
  * 1 pinch of salt 
  * 1 small cap of white vinegar (to avoid the smell of eggs) 

for lemon mousse: 
  * 1 egg. 
  * 140 g of sugar. 
  * 100 ml of lemon juice. 
  * 100 ml of water. 
  * 1 tablespoon cornflour. 
  * 200 ml of whipped cream with sugar-free whipped cream 



**Realization steps**

  1. prepare your ingredients, and start by beating the eggs, sugar and vanilla foam, to have a good foamy mixture, if you use the whisk of a kneader, it will give a nice foam, otherwise if you use an electric whisk like mine, tilt the whip as in the photo to aerate the mixture well, do it for 8 to 10 min, until the egg sugar mixture, triple volume.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/pate-de-roul%C3%A9.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41919>)
  2. add the vinegar, and continue to whip without stopping 
  3. incorporate the mixture flour and baking powder, gently spoon per spoon, without breaking the foamy mixture of eggs, 
  4. mix with a spatula while aerating the mixture. 
  5. cover the tray with a baking paper, and bray the brush a bit, 
  6. gently pour your mixture, standardize the surface with a spatula. 
  7. bake in a preheated oven at 180 degrees for 10 minutes, or depending on the capacity of your oven. 
  8. remove the biscuit from the oven and roll gently into a torchan, and let cool. 

Prepare the lemon mousse 
  1. in a saucepan, pour half of the sugar, water and lemon juice bring to a boil 
  2. in a salad bowl whip the remaining sugar and the maize 
  3. pour the mixture of lemon and hot water over this mixture and whisk vigorously, 
  4. put all in the pan over low heat while stirring, until thick, remove from heat and allow to cool. 
  5. gently add the cream of lemon to the fresh cream, using a spatula, until you have a mousse that stands, (you do not have to add all the cream   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/mousse-au-citron.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41920>)
  6. take the biscuit again, remove the towel, spread the remaining lemon cream on the biscuit   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/roul%C3%A9.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41921>)
  7. cover this cream with lemon mousse and gently roll the biscuit. 
  8. Cover with remaining lemon mousse and place in the fridge until ready to serve. 
  9. Cut out cakes of almost 2 cm wide. 



[ ![biscuit rolled with lemon mousse 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/biscuit-roul%C3%A9-a-la-mousse-de-citron-2.jpg) ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-mousse-de-citron.html/biscuit-roule-a-la-mousse-de-citron-2>)
