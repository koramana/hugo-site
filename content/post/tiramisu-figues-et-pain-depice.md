---
title: tiramisu figs and gingerbread
date: '2014-11-04'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-figues-et-pain-d-epice-044.CR2_thumb.jpg
---
[ ![tiramisu figs and gingerbread 044.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-figues-et-pain-d-epice-044.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-figues-et-pain-d-epice-044.CR2_2.jpg>)

##  tiramisu figs and gingerbread 

Hello everybody, 

Tiramisu is one of the desserts I love the most, and I can vary with all the sauces so much I like mascarpone and the taste of coffee. 

this time I combine this delicious Italian cheese, the fig I love and the gingerbread, so I can not tell you how much I like. 

then you imagine the result with all these tastes. 

a slaughter ... I assure you. 

**tiramisu figs and gingerbread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-figues-et-pain-d-epice-044.CR2_thumb.jpg)

Recipe type:  dessert  portions:  4  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 400 grs of mascarpone 
  * 4 eggs 
  * 150 grams of sugar 
  * 12 slices of gingerbread 
  * 1 big cup of black coffee 
  * Figs according to your taste. 



**Realization steps**

  1. clean the figs, wipe them, and cut them into cubes, keep some figs for the deco. 
  2. beat the sugar with the egg yolks, until the mixture whitens. 
  3. add the mascarpone and whip 
  4. turn the egg whites into firm snow. 
  5. gently stir in the previous mixture.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-040_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-040_thumb.jpg>)
  6. remove crusts of gingerbread cut in medium size. 
  7. soak the pieces of black coffee gingerbread. 
  8. put a layer of gingerbread in your verrines 
  9. cover with a layer of the mascarpone mixture 
  10. place a layer of figs in a cube. 
  11. cover again with the mascarpone preparation. 
  12. decorate with figs according to your taste. 
  13. reserve in the fridge for 4 hours before eating, 



[ ![tiramisu figs and gingerbread 033.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-figues-et-pain-d-epice-033.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-figues-et-pain-d-epice-033.CR2_2.jpg>)

[ ![tiramisu 012](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-012_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-012_2.jpg>)

and I know who will not resist ... 

[ ![tiramisu 004.CR2edited](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-004.CR2edited_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-004.CR2edited_2.jpg>)

I hope you like this recipe like me. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

[ ![tiramisu 015](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-015_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tiramisu-015_2.jpg>)
