---
title: Kefteji ...... Tunisian hmiss ... ..Pepper salad / tomato sauce.
date: '2011-10-11'
categories:
- amuse bouche, tapas, mise en bouche
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/241152181.jpg
---
& Nbsp; & Nbsp; When I saw this recipe at Wissal, I told myself I must do it. and here we are, I started cooking with my ingredients: ingredients: 3 large potatoes 1cougette 2 peppers or pepper equivalent half a finely chopped onion 3 cs chopped parsley the juice of a lemon and half salt and pepper oil for frying For tomato sauce: 6 tomatoes peeled, seeded and mixed 3 cloves garlic crushed or mix with tomato a good drizzle of olive oil 1 cc and a half seed of coriander powder & hellip; 

##  Overview of tests 

**User Rating:** Be the first one!  0 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/241152181.jpg)

and here we are, I started cooking with my ingredients: 

ingredients: 

  * 3 large potatoes 
  * 1cougette 
  * 2 peppers or pepper equivalent 
  * half of medium onion, finely chopped 
  * 3 tablespoons chopped parsley 
  * the juice of a lemon and a half 
  * salt and pepper 
  * oil for frying 



For the tomato sauce: 

  * 6 tomatoes peeled, seeded and mixed 
  * 3 crushed garlic cloves or mix with tomato 
  * a good drizzle of olive oil 
  * 1 cc and a half coriander seed powder 
  * 1/2 cs of harissa (optional) 
  * 1 tablespoon of tomato paste 
  * salt 



For the dumplings: 

  * 300 gr of minced meat 
  * 2 crushed garlic cloves 
  * 1 tablespoon of dry mint (I had not put) 
  * 1 tablespoon of ground coriander seeds 
  * salt and pepper 



![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/241152311.jpg)

start by peeling, washing, wiping and cutting the potatoes into pieces, like making fries !! fry them in an oil bath, meanwhile; 

peel, wash ... the zucchini and cut in thin slices,   
Eat chillies or peppers,   
Once the potato fries are cooked, put them in a colander to get rid of the maximum amount of oil, in the same cooking oil, fry the peppers or peppers (well wipe them if they are wet there will be the risk of oil spraying),   
reserve side fries, finish with the zucchini, which does not take a long time to fry,   
during this time, prepare the tomato sauce by mixing all the ingredients mentioned above, and let them return on a low heat, add a small glass of water if necessary; 

To prepare the meatballs, mix the meat well with the spices, shape them into a small dumpling the size of a walnut, in a saucepan heat two spoons of oil (I take that of the frying), brown the meatballs meat in it before adding a good glass of water and let them cook the time to chop the vegetables. 

in a large salad bowl, mix lemon juice, parsley, salt and pepper and onion;   
cut the potato fries into small pieces as well as the zucchini,   
peel, peel the peppers or peppers and cut into small pieces,   
mix the vegetables in the salad bowl with lemon juice, parsley ... 

pour over the tomato sauce which should not be liquid, mix, taste the salt, add also the sauce of the meat which you would have reduced;   
fry the eggs should preferably stay calf. 

For the presentation, you are in front of several choices, or you cut the eggs and you mix them with the vegetables, or you present them next to the mixture of the vegetables, the same for the meatballs or you crumble them in or you present them next, 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/241152351.jpg)

have a good meal… 

I still skip a few steps and miss some ingredients ... 

mais c’etait bon, surtout mon epoux a aimer le piquant. 
