---
title: home-made bread
date: '2014-12-20'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-mie-fait-maison-aux-grains-de-pavot-028.CR2_thumb1.jpg
---
![Homemade bread with poppy seeds 028.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-mie-fait-maison-aux-grains-de-pavot-028.CR2_thumb1.jpg)

##  home-made bread 

Hello everybody, 

The bread is not what is missing at home, because with the tuition system here in England, my children always take with them, the lunch box, or the bento, and there is always a sandwich in it, and my kids love their sandwiches with the bread, because it's still mellow. 

Generally, it's always homemade bread that I put them, unless for lack of time, I do not. 

Today, I share with you my recipe for sandwich bread that I succeed at once, here I pass you the version of homemade bread with poppy seeds, you can just get some poppy seeds if you want home-made cottage bread. 

![homemade breadcrumb.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-mie-fait-maison.CR2_thumb1.jpg)

**home-made bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-mie-fait-maison-021.CR2_thumb-300x200.jpg)

portions:  6  Prep time:  40 mins  cooking:  20 mins  total:  1 hour 

**Ingredients**

  * Ingredients: 
  * 500 g white flour 
  * 200 ml warm milk 
  * 100 ml warm water 
  * 50g of soft butter 
  * 1 tablespoon and a half of sugar 
  * 1 teaspoon of salt 
  * 1 sachet of baker's dry yeast 
  * 1 tablespoon of poppy seeds (optional) 



**Realization steps**

  1. method of preparation: 
  2. in a large salad bowl, mix the dry products 
  3. add the water and milk mixture slowly while kneading. 
  4. you will see a paste sticking a little to the hand, introduce the butter in pieces, which you spread on the worktop, and on which, you knead the dough, so that it absorbs all the butter. 
  5. place the dough in a salad bowl a little butter, cover and let rise. 
  6. take the dough and degas it by pressing in the fist. 
  7. Form a pudding and place it in a cake pan of almost 30 cm, otherwise divide your dough into two medium cake pans. 
  8. Let rise until the dough swells well. 
  9. Bake in a preheated oven at 180 degrees C, bake for about 20 minutes. or until the bread is cooked and turns golden.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-mie-maison-facile-012.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/pain-de-mie-maison-facile-012.CR2_thumb1.jpg>)


