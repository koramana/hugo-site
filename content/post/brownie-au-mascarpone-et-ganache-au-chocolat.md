---
title: Brownie with mascarpone and chocolate ganache
date: '2015-12-16'
categories:
- dessert, crumbles and bars
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-mascarpone-et-ganache-au-chocolat-2.jpg
---
[ ![mascarpone brownie and chocolate ganache 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-mascarpone-et-ganache-au-chocolat-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-mascarpone-et-ganache-au-chocolat-2.jpg>)

##  Brownie with mascarpone and chocolate ganache 

Hello everybody, 

For the children's school party today, I made some brownies, I do not tell you how many batches I made, I do not count too much, I make sure I have less than 200 pieces. In the end, I think that even 200 pieces were little, the children doubled the shares, and the parents were very happy, because despite my brownies had a huge layer of chocolate ganache, but they were not as sweet as those Trade. 

The principal of the school was more cunning, she took 5 parts before the start of the party, and she took the recipe at the same time, hihihih 

The original recipe I found on the Chanel Five TV website, contained twice the sugar I put in my recipe, and knowing that the English love to put a lot of sugar in their recipe, I put half of the amount of sugar, my ganache was made from 70% dark chocolate ... 

So if you like the sweet taste, do not hesitate to add more sugar, or used milk chocolate. 

[ ![mascarpone brownie and chocolate ganache](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-mascarpone-et-ganache-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-mascarpone-et-ganache-au-chocolat.jpg>)   


**Brownie with mascarpone and chocolate ganache**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-mascarpone-et-ganache-au-chocolat3.jpg)

portions:  16  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients** for the brownie: 

  * 220 gr of unsalted butter 
  * 85 gr of chopped chocolate (containing 30 gr of sugar per 100 gr of chocolate) 
  * 145 gr of crystallized white sugar 
  * 50 gr unsweetened cocoa 
  * 110 gr of Mascarpone at room temperature 
  * 3 large eggs 
  * 1 teaspoon of vanilla extract 
  * 80 gr of flour 
  * 1 pinch of salt 

for the chocolate ganache. 
  * 100 gr of dark chocolate 
  * 6 tablespoons of liquid cream 
  * 1 tablespoon of butter 



**Realization steps** Prepare the brownie 

  1. Preheat the oven to 180 ° C. Butter a 20 cm square pyrex mold. 
  2. place the chopped chocolate in a bowl 
  3. In a small glass bowl, melt the butter in the microwave, just to melt it and not boil, so watch it. 
  4. Pour the butter over the chocolate and let stand for 30 seconds. Stir until chocolate is completely melted and butter is well incorporated. 
  5. Add on top the sugar and the cocoa powder. mix with a wooden spoon 
  6. In another bowl, lightly beat the mascarpone to relax 
  7. introduce the eggs one by one, then the vanilla, 
  8. pour this mixture over the chocolate, and mix the mixture 
  9. Gently stir in the flour and salt. 
  10. Pour the dough into the prepared pan and spread evenly. 
  11. Place in the preheated oven and cook for 40 to 45 minutes, or until a toothpick comes out clean. 
  12. Place the mold on a rack and let cool for 10 to 15 minutes while you make the ganache. 

prepare the ganache: 
  1. Place the chopped chocolate in a bowl 
  2. In a small saucepan, bring cream and butter just below boiling point over medium heat. 
  3. Pour this hot mixture over the chocolate and let stand for 30 seconds, then mix until smooth (my tip for a super bright ganache, I use the mixer foot, the result is going to be bluffing) 
  4. Pour the ganache over the still warm brownies, and cover evenly.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-masacrpone.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-masacrpone.bmp.jpg>)
  5. Let the ganache take before cutting the pieces of brownies. It is best not to keep the brownie in the refrigerator. 



[ ![mascarpone brownie and chocolate ganache1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-mascarpone-et-ganache-au-chocolat1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/brownie-au-mascarpone-et-ganache-au-chocolat1.jpg>)
