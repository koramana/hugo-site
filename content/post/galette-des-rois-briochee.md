---
title: cake brioche
date: '2018-01-04'
categories:
- Mina el forn
- bread, traditional bread, cake
tags:
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/brioche-des-rois-couronne-des-rois-1.jpg
---
[ ![cake brioche](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/brioche-des-rois-couronne-des-rois-1.jpg) ](<https://www.amourdecuisine.fr/article-galette-des-rois-briochee.html/brioche-des-rois-couronne-des-rois-1>)

##  Galette des rois brioche 

Hello everybody, 

Here is a sublime crown of kings, or a cake of brioche kings, that I realized yesterday. This is the first time I make a brioche, based on a pasta recipe that sits all night long. 

The trick to make a brioche is to be patient ... let the dough rise the time it takes. This is the case for this beautiful cake brioche, after shaping of the crown, the dough has rested and inflated for almost 6 hours. 

To proceed, I prepared the dough in the evening, I let it rest all night in front of the radiator (my radiator is not too hot), in the morning, I degassed the dough on a floured worktop , I shaped the crown ... 

I put it in front of the radiator, for almost 6 hours again. When I came back, the dough that did not cover the half of the mold, superbly swell up to exceed the edges of the mold more than 2 cm .... 

I gently brushed my brioche crown, I garnished it, and baked for a cooking between 15 and 20 minutes depending on the oven .... 

the result, well I think I'm ready to open a bakery .... hihihihih 

For lovers of the [ galette des rois ](<https://www.amourdecuisine.fr/article-galette-des-rois.html>) a puff pastry filled with almond cream. 

[ ![brioche des rois - crown of kings - galette des kings brioche](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/brioche-des-rois-couronne-des-rois-galette-des-rois-brioch%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/article-galette-des-rois-briochee.html/brioche-des-rois-couronne-des-rois-galette-des-rois-briochee>)   


**cake brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/brioche-des-rois-galette-des-rois-brioche.jpg)

Recipe type:  brioche and pastries  portions:  12  Prep time:  45 mins  cooking:  30 mins  total:  1 hour 15 mins 

**Ingredients**

  * 500 gr of flour 
  * 1 tablespoon of dehydrated yeast, instant yeast. 
  * 1 cup of baking powder 
  * 4 eggs 
  * 5g of salt 
  * 120 gr of sugar (I do not use too much sugar, because I have garnished my bun with pearl sugar and candied cherries) 
  * 30 ml of orange blossom water 
  * 45 ml warm water 
  * 100 gr of butter 

decoration: 
  * 1 egg yolk 
  * 1 pinch of vanilla 
  * 1 cup of milk 
  * pearl sugar 
  * candied cherries 



**Realization steps** you can prepare this brioche in the bread machine, just to knead and make the first rise .. as you can do it in trouble ... otherwise proceed as follows: 

  1. In a salad bowl, mix the flour, the yeast, the sugar and the salt, 
  2. add water and orange blossom water. 
  3. break the eggs one by one and introduce them to the dough (one egg at a time and pick up) 
  4. if your eggs are big, maybe you will not use the 4 eggs, and if the eggs are small, you can go up to 5 eggs, the most important is to have a malleable dough, but not too sticky. 
  5. begin to knead the dough, introducing the butter gently, by small piece. knead until the butter is well absorbed by the dough, and add another piece, do so until you add all the butter. 
  6. put the dough back in the salad bowl and cover it with a clean cloth. 
  7. let stand at a temperature between 23 and 25 ° C, overnight. 
  8. The next day, degas the dough, spread it on a flour work plan, fold it, and spread again, then fold. Then form a crown, making a hole in the middle of the dough. 
  9. place the crown in a crown-shaped mold that is buttered and floured. 
  10. Let it rise again (the time it takes, depending on the temperature of the room, for me and because it's winter it took more than 6 hours) 
  11. decorate the crown of kings with the egg yolk mixture, milk and vanilla, then sprinkle the pearl sugar on top, and the pieces of candied cherries. 
  12. cook in a preheated oven at 180 ° C, between 25 and 30 minutes, or depending on your oven, in mine it took 20 minutes. 



[ ![brioche-of-the-kings-galette-of-the-kings-brioche 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/brioche-des-rois-galette-des-rois-briochee-1.jpg) ](<https://www.amourdecuisine.fr/article-galette-des-rois-briochee.html/brioche-des-rois-galette-des-rois-briochee-1>)

merci pour vos visites 
