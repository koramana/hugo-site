---
title: Camembert soufflé
date: '2016-05-03'
categories:
- pizzas / quiches / tartes salees et sandwichs
- Dishes and salty recipes
- rice
tags:
- inputs
- Ramadan 2016
- Easy cooking
- Fast Food
- Healthy cuisine
- accompaniment
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9s-au-camembert-1.jpg
---
[ ![blown with camembert 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9s-au-camembert-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9s-au-camembert-1.jpg>)

##  Camembert soufflé 

Hello everybody, 

The soufflé with camembert or in general, the cheese soufflés are a recipe that I often make, especially after making a lasagna and I still have bechamel. Frankly I do not hesitate to do with this béchamel soufflé very tasty, with any cheese that I have at home. I do not publish often, because you have to be quick with the blown to take a picture at the exit of the oven, while it is well inflated, it's too greedy. 

The principle of the game is super easy: Each month, we choose a godmother among the participants in the round, who will choose a new ingredient to be featured in the next round. The bloggers who are tempted by the ingredient come to participate in the game, and prepare their recipes to be published on the 3rd of next month, with the list of participants and their titles prepared by the godmother of the game, and here it is super easy and it's very friendly to visit each other afterwards, to see all the great recipes made around the star ingredient. As for this time, you imagine that you have 44 pie recipes different from each other, we are well spoiled is not it? 

[ ![puffed with camembert 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9s-au-camembert-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9s-au-camembert-4.jpg>)

You can see participating blogs in this round down, with the titles of their recipes, I will immediately see their delights and achievements, and I will also share their recipes on our facebook page dedicated to this game: 

Besides, you can find all the participating recipes in all the previous rounds of our game: 

**Camembert soufflé**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9s-au-camembert-3.jpg)

portions:  6  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 50g unsalted butter, and more for ramekins 
  * 50 gr of all-purpose flour, and even more for ramekins 
  * 300 ml whole milk 
  * 3 eggs, separated yellow from the whites 
  * 200 gr of Camembert with crust 
  * ¼ teaspoon of paprika 
  * ¼ teaspoon of turmeric 
  * nutmeg 
  * ¼ teaspoon of salt 
  * ⅛ teaspoon freshly ground pepper   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9-ingr%C3%A9dients.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9-ingr%C3%A9dients.jpg>)



**Realization steps**

  1. butter ramekins with a brush from top to bottom 
  2. sprinkle generously with flour, then tease the ramekins upside down on the top of work to remove the excess.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/fariner-les-ramequins.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/fariner-les-ramequins.jpg>)
  3. In a saucepan, melt the butter over low heat. Stir in the flour and cook, stirring constantly, until the flour is a pale golden color, about 2 minutes. 
  4. Gradually add the milk, stirring constantly, until the mixture thickens, about 3-4 minutes. 
  5. Add the cube-cut camembert and stir until the cheese is completely melted and incorporated.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/camembert-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/camembert-.jpg>)
  6. Add salt, pepper, paprika, turmeric and grated nutmeg. 
  7. Remove the pan from the heat, and introduce the egg yolks one by one   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/jaune-doeuf-au-souffl%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/jaune-doeuf-au-souffl%C3%A9.jpg>)
  8. stir after each addition, and cover with a cling film and cool. 
  9. Beat the egg whites with a pinch of salt, add a quarter of the whites to snow in the previous preparation to relax, then pour over the remaining snow whites and mix gently. 
  10. Fill two-thirds puff molds   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9.jpg>)
  11. and bake them for about 10 minutes 



[ ![puffed with camembert 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9s-au-camembert-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/souffl%C3%A9s-au-camembert-2.jpg>)

et voila la liste des participantes a cette ronde: 
