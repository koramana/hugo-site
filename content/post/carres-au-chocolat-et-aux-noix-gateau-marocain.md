---
title: 'chocolate and walnut squares: Moroccan cake'
date: '2014-07-20'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carres-aux-noix-et-chocolat-4.jpg
---
[ ![squares with nuts and chocolate 4](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carres-aux-noix-et-chocolat-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carres-aux-noix-et-chocolat-4.jpg>)

##  chocolate and walnut squares: Moroccan cake 

Hello everybody, 

Here is another recipe from Lunetoiles de l'aide el fitr passed, and that I did not have the chance to publish, but here, I will publish the recipe of these squares with chocolate and nuts: Moroccan cake, and I 'hope you'll like this recipe. 

The recipe is easy to make, and it is a delight, having it taste, it is really too good, and super fondant in the mouth .. 

You want to have more recipes from [ Algerian cakes ](<https://www.amourdecuisine.fr/article-gateau-algerien-et-gateaux-orientals-pour-laid-el-fitr-2014.html> "Algerian cake and oriental cakes for l'aid el fitr 2014") , do not forget to visit this link, you will find any style of Algerian cake, be it dry cakes, cakes without cooking, cakes with honey and almonds, and fried cakes.   


**chocolate and walnut squares: Moroccan cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carres-aux-noix-et-chocolat-5.jpg)

portions:  50  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 6 cups of mixed nuts (almonds, peanuts, pecans ... which suits your taste) 
  * ¾ cup of sesame seeds 
  * 1 cup of sugar 
  * ¼ cup melted unsalted butter 
  * 2 egg white 
  * 1 tbsp. vanilla extract 
  * 400 g + 30 g of white chocolate of good quality 
  * 400 g + 30 g of good quality dark chocolate 



**Realization steps**

  1. Preheat the oven to 175 ° C. 
  2. Brown walnuts in the oven, stirring occasionally, until golden brown (not too dark). 
  3. Check often because the nuts can brown and the color will darken quickly. 
  4. Brown the sesame seeds in the same way. 
  5. Let the nuts and sesame seeds cool completely before grinding them finely with the sugar in a food processor equipped with the metal blade. 
  6. Do this three times, mixing ⅓ of nuts and 1/3 of the sugar at a time. 
  7. Book in a large salad bowl.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carr%C3%A9s-au-chocolat-et-aux-noix1-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carr%C3%A9s-au-chocolat-et-aux-noix1.jpg>)
  8. Preheat the oven to 175 ° C. 
  9. Beat the egg whites in a salad bowl until frothy and mix the melted butter and vanilla extract. 
  10. Pour the mixture over the nuts and mix well with your hands. 
  11. Spread the nut mixture evenly over the prepared baking sheet (lined with parchment paper) and spread with a rolling pin to obtain a smooth surface.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carr%C3%A9s-au-chocolat-et-aux-noix2-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carr%C3%A9s-au-chocolat-et-aux-noix2.jpg>)
  12. Bake 10 minutes in the preheated oven. 
  13. Cool completely in the baking sheet before proceeding with chocolate coating. 
  14. Finely chop 400 g dark chocolate with a serrated knife. 
  15. Bring water to a boil in a saucepan. 
  16. Remove from heat and put a large heat-resistant bowl over the water, the bowl should not touch the water. 
  17. Add the chopped chocolate to the bowl. 
  18. Stir gently with a spoon until the chocolate is completely melted. 
  19. Try not to introduce air bubbles into the melted chocolate. 
  20. In a small ramekin, melt the 30 gr of white chocolate by putting the ramekin in very hot water or pass it for 10 to 15 seconds at a time in the microwave, stirring between each microwaving. 
  21. If you melt it in the microwave, stop cooking when some solid pieces of white chocolate are still visible. 
  22. They will melt just in contact with chocolate that has already melted, shaking a little ramekin, to make it dissolve little by little. 
  23. Pour the dark chocolate on the cooked walnuts and cooled. 
  24. Use a spatula to even the surface of the chocolate. 
  25. Put the white chocolate in a plastic food bag (a freezer bag for example) and cut a small tiny hole on a corner. 
  26. Draw lines of any patterns (decoration) that you like with white chocolate on dark chocolate. 
  27. Let cure for 2 hours. 
  28. Then refrigerate until chocolate is hard, at least 2 hours. 
  29. I think it's best to let the chocolate firm up at room temperature before putting the plate in the refrigerator. 
  30. Remove from the refrigerator and allow to come to room temperature for an hour or two. 
  31. Then use a sharp knife to loosen the baking paper on the sides of the plate. 
  32. Put a large baking sheet on it and flip it over to unmold. 
  33. Make the decoration of chocolate using the same technique as above, but it is only the white chocolate is decorated with vertical lines of dark chocolate (or patterns of decorations you want, for example make a marbling as for the mille-feuilles ) 
  34. Let the chocolate harden in the same way as for the dark chocolate layer (at room temperature first, then in the refrigerator). 
  35. Once when the chocolate is firming, cut squares. 
  36. They can be stored for up to three weeks at room temperature, or three months in the freezer. 
  37. With the recipe you will get many pieces, in fact this cake is often prepared for weddings. If you wish, you can divide the recipe in half. 



[ ![chocolate and walnut squares](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carr%C3%A9s-au-chocolat-et-aux-noix.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/carr%C3%A9s-au-chocolat-et-aux-noix.jpg>)
