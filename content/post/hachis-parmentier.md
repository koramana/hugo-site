---
title: shepherd's pie
date: '2015-12-14'
categories:
- recipe for red meat (halal)
tags:
- Minced meat
- Potato
- accompaniment
- dishes
- Easy recipe
- Algeria
- Full Dish

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier-1.jpg
---
[ ![shepherd's pie 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier-1.jpg>)

##  shepherd's pie 

Hello everybody, 

Here is a well-known dish: the shepherd's pie, very delicious, very rich, a complete dish. At home, my husband loves a lot, he likes everything that is based on mashed potatoes, hihihi ... If I'm his request, I will find myself making mince pie each week, hihihih. 

My husband loves this version, ie layer of mashed potatoes, layer of meat, and another layer of mashed potatoes ... He does not like the version one layer of meat and another of mashed potatoes , why do you know ??? 

[ ![shepherd's pie](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier_.jpg>)

**shepherd's pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier_.jpg)

portions:  6  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 kg of potatoes 
  * 400 g ground beef 
  * 1 onion 
  * 1 clove of garlic 
  * ½ bunch of parsley 
  * 1 egg 
  * 100 g of butter 
  * 300 ml of milk 
  * nutmeg 
  * salt 
  * pepper 



**Realization steps**

  1. Peel the potatoes and cook them in salted water for about 30 minutes. 
  2. Chop the onion and garlic, and sweat it in a pan. 
  3. Stir until it becomes translucent, 
  4. add the minced meat, cook well, add the chopped parsley and season to taste, then reserve. 
  5. In a bowl, combine the minced meat, chopped parsley, onions, and the whole egg. Mix everything well. 
  6. Once the potatoes are cooked, drain and purée 
  7. Heat the milk over low heat, add the butter to the mashed potato, then finally the hot milk. 
  8. Mix well to obtain a creamy mash. 
  9. Season with salt, pepper and nutmeg. 
  10. Butter a gratin dish. Spread half of the purée on the bottom of the dish, 
  11. add the minced meat mixture and spread the remaining mashed potatoes, you can use the pastry bag to spread this layer. (you can add a layer of cheese on it)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier-4.jpe) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier-4.jpe>)
  12. Bake in a hot oven at 200 ° C until the surface of the shepherd's pie has a nice golden color. 
  13. take out of the oven ... and serve immediately. 



[ ![shepherd's pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/hachis-parmentier-2.jpg>)
