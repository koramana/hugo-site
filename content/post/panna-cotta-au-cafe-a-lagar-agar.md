---
title: Panna cotta with agar agar
date: '2014-12-08'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- panna cotta, flan, and yoghurt
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/panna-cotta-au-cafe-a-lagar-agar-3.jpg
---
[ ![panna cotta with agar agar 3 coffee](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/panna-cotta-au-cafe-a-lagar-agar-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/panna-cotta-au-cafe-a-lagar-agar-3.jpg>)

##  Panna cotta with agar agar 

Hello everybody, 

I really like panna cotta, but this is the first time I make a panna cotta with agar agar. Frankly, this panna cotta is a delight, but it takes a very effective scale to measure the amount of agar agar, besides I buy the spoon balance just to do that. It must be said that with my usual balance 1 gr was not an easy quantity to measure, between 1 gr and 2 grs it was too sensitive, suddenly my panna cotta was a little thick for my taste, children have a lot love. Yes because I do not let them eaten coffee, it was their pleasure to taste this tasty dessert with the captivating scent of coffee. 

So my friends, to succeed in this delight we must measure the amount of agar agar. 

[ ![panna cotta with agar agar 3 coffee](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/panna-cotta-au-cafe-a-lagar-agar-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/panna-cotta-au-cafe-a-lagar-agar-1.jpg>)   


**Panna cotta with agar agar**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/panna-cotta-au-cafe-a-lagar-agar-2.jpg)

portions:  4-6  Prep time:  10 mins  cooking:  3 mins  total:  13 mins 

**Ingredients**

  * 350 ml of liquid cream 
  * 120 ml of milk 
  * 60 gr of brown sugar 
  * 1 gr of agar agar or 4 gr of gelatin 
  * 1 tablespoon of soluble coffee (Nescafe) 
  * 2 tablespoons orange juice 



**Realization steps**

  1. mix the agar agar with a little sugar 
  2. In a thick bottomed saucepan, pour the milk cream, sugar, orange juice and coffee. 
  3. Heat to a simmer and add the agar-agar 
  4. Boil 1 min. 
  5. Pour into verrines or ramekins and let cool to room temperature then place in a cool place. 



[ ![panna cotta with agar agar 3 coffee](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/panna-cotta-au-cafe-a-lagar-agar-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/panna-cotta-au-cafe-a-lagar-agar-4.jpg>)
