---
title: tagine of olives with chicken croquettes
date: '2015-12-07'
categories:
- Algerian cuisine
tags:
- Easy recipe
- Algeria
- dishes
- Full Dish
- ramadan 2015
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/tajine-zitoune-aux-croquettes-de-poulet-1.jpg
---
[ ![tajine zitoune with chicken nuggets 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/tajine-zitoune-aux-croquettes-de-poulet-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/tajine-zitoune-aux-croquettes-de-poulet-1.jpg>)

##  tagine of olives with chicken croquettes 

Hello everybody, 

I did not change much in Nadia's recipe, apart from adding mushrooms to my dish. 

**tagine of olives with chicken croquettes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/tajine-zitoune-aux-croquettes-de-poulet-2.jpg)

portions:  6  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * 2 chicken breast 
  * 1 onion 
  * 1 glass of rice 
  * 2 glasses of green olives 
  * Salt, black pepper, cinnamon and oil 
  * Chopped parsley 
  * sliced ​​mushrooms 
  * 1 teaspoon flour 
  * 1 beaten egg 

For breading: 
  * 2 eggs 
  * Flour 
  * breadcrumbs 
  * Oil for frying 



**Realization steps**

  1. Cook chicken breast in hot oil with grated onion, salt and spices, 
  2. add the mushrooms and sauté. 
  3. add the parsley and let it simmer 
  4. Water with warm water Cover and cook 
  5. In the meantime, cook the rice in salted water and allow to go over a little of the daily, to have seeds of rice a little pasty to facilitate the formation of croquettes 
  6. Boil the olives three times to desalt them 
  7. Cut half the olives and add the rest in the sauce 
  8. Dilute the flour in a little cold water, then pour it into the sauce for a creamy sauce, 
  9. remove the chicken from the sauce, and crumble it, add the rice and olives in small pieces, add chopped parsley too and 1 beaten egg 
  10. Form croquettes, pass them through flour, then beaten egg and breadcrumbs.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/croquettes-au-poulet-pour-tajine-zitoune.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/croquettes-au-poulet-pour-tajine-zitoune.jpg>)
  11. keep them fresh for at least 1 hour, then fry for 2 minutes in a hot oil bath 
  12. In the serving dish, pour the hot sauce over and over the croquettes. Decorate with chopped parsley 



[ ![tagine of olives with chicken croquettes](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/tajine-dolives-aux-croquettes-de-poulet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/tajine-dolives-aux-croquettes-de-poulet.jpg>)
