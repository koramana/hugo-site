---
title: tajine jelbana, tajine of peas with artichokes
date: '2012-07-23'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/jelbana-bel-qarnoun_thumb.jpg
---
#  [ ![jelbana bel qarnoun](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/jelbana-bel-qarnoun_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/jelbana-bel-qarnoun.jpg>)

##  tajine jelbana, tajine of peas with artichokes 

Hello everybody, 

when one is hungry, there is no more good than a tajine very hot, a dish of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , very tasty, especially with **artihoke hearts** very tender, which add a taste to the dish. 

and if you like peas, you can also see the recipe of: [ tajine with peas and stuffed cardoons ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee-94791294.html>) , [ velvety peas ](<https://www.amourdecuisine.fr/article-41284308.html>) . Otherwise, if you like artichokes like me, I share with you the recipe for: [ stuffed artichoke tajine ](<https://www.amourdecuisine.fr/article-tajine-d-artichauts-farcis-97187541.html>) or these delicious bouraks in the shape of a flower: [ fried flowers with artichoke cream ](<https://www.amourdecuisine.fr/article-fleurs-frites-farcies-a-la-creme-d-artichauts-102277133.html>)

we come back to the recipe of this tajine jelbana: 

**tajine jelbana, tajine of peas with artichokes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tajine-de-petits-pois-aux-artichauts_thumb.jpg)

Recipe type:  Hand  portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 2 cups of peas (frozen) 
  * artichoke hearts (frozen) 
  * chicken legs 
  * an onion 
  * 1 carrot 
  * chopped parsley 
  * a bay leaf 
  * salt, pepper, paprika, coriander / garlic (my favorite spice 
  * canned tomato 
  * 1 potato (optional, children like this) 
  * oil 



**Realization steps**

  1. method of preparation: 
  2. fry the onion in oil, 
  3. add the chicken legs, the canned tomato, the spices, 
  4. simmer a little, then add the bay leaf and the chopped parsley, the carrot cut in a slice, and cover everything with water. 
  5. to boil, add the peas, and just before the end of the cooking, add the hearts of artichokes cut in quarter or whole, and the potato cut in quarter. 
  6. cook until the vegetables are tender, and the sauce a little reduced. 
  7. serve with a delicious [ homemade bread ](<https://www.amourdecuisine.fr/article-25345314.html> "khobz eddar - homemade bread") , or a good [ matlou3 ](<https://www.amourdecuisine.fr/article-41774164.html> "galette Matlou3 - مطلوع Algerian Arabic bread")



[ ![tajine jelbana](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tajine-jelbana_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/tajine-jelbana.jpg>)

and good appetite, and for more Algerian recipes: [ cuisine algerienne avec photos ](<https://www.amourdecuisine.fr/article-cuisine-algerienne-103183969.html>)
