---
title: homemade ravioli, ravioli pasta on video
date: '2017-11-02'
categories:
- Algerian cuisine
- diverse cuisine
- Healthy cuisine
tags:
- Algeria
- dishes
- Healthy cuisine
- Vegetarian cuisine
- Easy cooking
- Full Dish
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/ravioli-fait-maison.jpg
---
[ ![homemade ravioli](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/ravioli-fait-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pate-a-ravioles-maison.jpg>)

##  homemade ravioli, ravioli pasta on video 

Hello everybody, 

I have a lot of fun making homemade pasta, they are fresh, they are too good, and we can shape them as we want. My love at first sight is for homemade ravioli or ravioli dough, because every time I go to the supermarket, I see stuffed ravioli of all kinds and colors, but too bad it's not always halal. That's why I started making homemade ravioli, putting everything I want in it, and that's the recipe I've been using for years. 

I use this recipe to make all my pasta, starting with the tagliatelle and end with the farfalle that my daughter likes to prepare with me, it is also she who pinches the little pieces of pasta to give them this beautiful shape of butterfly. 

I share with you this homemade ravioli recipe, ravioli pasta on video, in this video I prepare homemade ravioli with cepes, or homemade ravioli with wild mushrooms: 

**homemade ravioli, ravioli pasta on video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pate-a-ravioles-maison.jpg)

portions:  6  Prep time:  30 mins  total:  30 mins 

**Ingredients**

  * 3 medium eggs at room temperature 
  * 300 gr of flour 
  * 1cc of olive oil 



**Realization steps**

  1. place the flour, eggs and salt in the bowl of the mess. operate the kneader at reduced speed, and let the dough get picked up, if your eggs are a little small, and the flour does not pick up completely, just add a little water (I did not need to add water has my dough.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pate-a-ravioles-maison-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pate-a-ravioles-maison-1.jpg>)
  2. pick up the ball pasta, cover with cling film, and let rest on your worktop. 
  3. Take the dough after a rest of at least 30 minutes, and spread it out, to the roller patissier (if you want to work your bisepses), perso I preferred to work in the mill. 
  4. divide the dough in half, so as not to have very long bungs. and spread on a well floured plan. 
  5. fold it over three, spread it out again, and fold again, then start spreading it finer, squeezing the rolls of your dough machine each time. until you have a band with a thickness of almost 1 mm.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/raviolis-fait-maison-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/raviolis-fait-maison-1.jpg>)
  6. put the strip on a flouring plan, use the ravioli mold to delicately trace the impression (this will allow you to know where to put the stuffing) 
  7. place the equivalent of a teaspoon of stuffing in the middle. 
  8. with a brush, put some egg white in the inside of the print, and cover with another layer of dough. 
  9. press well to adhere both pasta, it is best not to leave too much air between the two pasta. 
  10. cut the ravioli with the help of your ravioli mold, and place them on a clean cloth.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pate-a-ravioles-ravioli-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pate-a-ravioles-ravioli-maison.jpg>)
  11. You can let the ravioli dry on the cloth for at least 1 hour, put them in a cool bag, for later use, or prepare them right away. 
  12. personally, I cook my ravioli in a salt water for at least 5 minutes, then I put them in a cold water to stop cooking, like that when I put them in the sauce, the ravioli will not get rid.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cuisson-de-raviolis.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/cuisson-de-raviolis.jpg>)


