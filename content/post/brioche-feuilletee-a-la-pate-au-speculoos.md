---
title: Puff pastry bun with speculoos
date: '2016-08-19'
categories:
- Buns and pastries
tags:
- Pastries
- Boulange
- Pastry
- Breakfast
- To taste
- Couque
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/brioche-feuillet%C3%A9e-a-la-pate-au-speculoos-2.jpg
---
[ ![Puff pastry bun with speculoos](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/brioche-feuillet%C3%A9e-a-la-pate-au-speculoos-2.jpg) ](<https://www.amourdecuisine.fr/article-brioche-feuilletee-la-pate-au-speculoos.html/brioche-feuilletee-a-la-pate-au-speculoos-2>)

##  Puff pastry bun with speculoos 

Hello everybody, 

This **Puff pastry bun with speculoos** is one of the most beautiful and delicious brioche recipes you can make. You remember [ chocolate pastry croissants ](<https://www.amourdecuisine.fr/article-croissants-feuilletes-au-chocolat.html> "Chocolate pastry croissants") that I realized recently? Well that's what was left as puff pastry when I made this puff pastry bun [ pasta with speculoos ](<https://www.amourdecuisine.fr/article-la-pate-a-speculoos-maison-hommage-a-micky-63142494.html> "pate of speculoos, homemade recipe") . 

I know this form of brioche made the buzz in the blogosphere, but here I have prepared a puff pastry and at the last moment, I did not know what to do with, I was too lazy to make a cream pastry to make small raisin breads, or even Swiss breads, maybe next time, I have to start with pastry cream, hihihihi, or just prepare it at the time of the rest of the dough in the fridge, hihihih 

This **Puff pastry with speculoos** when it is just falling, we find the crispness of the puff pastry from above, the sweetness of a super fondant crumb in the mouth, and the incomparable taste of a delicious spread that has a little caramelized during cooking ... What happiness at every bite.   


**Puff pastry bun with speculoos**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/brioche-feuillet%C3%A9e-a-la-pate-au-speuloos-1.jpg)

portions:  8  Prep time:  30 mins  cooking:  25 mins  total:  55 mins 

**Ingredients**

  * 450 gr of flour 
  * 1 teaspoon yeast dehydrated yeast 
  * 9 gr of salt if not a teaspoon 
  * 45 g of sugar 
  * 1 egg 
  * 220 ml of milk (+ or - depending on your flour, and the size of the egg) 
  * 150 g of special lamination butter at room temperature 

prank call: 
  * Pate a speculoos 

for gilding: 
  * 1 egg yolk 
  * 1 little milk 



**Realization steps**

  1. In the bowl of the robot, put the flour, the salt, and the sugar, mix and add the yeast 
  2. add the whipped egg and mix again 
  3. Introduce the milk now, until you have a nice dough that picks up well, but is not too sticky. 
  4. Knead again to obtain a smooth and homogeneous paste. 
  5. Let stand for 30 minutes covering with a clean cloth. 
  6. in the meantime, spread the butter between two sheets of baking paper, in a rectangle and place it in the fridge. 
  7. spread the dough after rest in rectangle on a floured space 
  8. place the butter in one side of the rectangle.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pate-feuillet%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40669>)
  9. fold the other side on, and weld well, 
  10. Spread the dough one more time, to be three times its original size, the best way to proceed at the beginning not to break the butter, and to give strokes gently with the help of the baking roll to crush the dough, no strong shots, ok.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pate-feuillet%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40670>)
  11. fold the dough in three like a wallet   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pate-feuillet%C3%A9e-2-003.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40674>)
  12. Put in the fridge 20 minutes. 
  13. turn the dough so that the folding is on the right (ie if you want to open the wallet, the opening is on the right) 
  14. spread the dough again in large rectangle, and fold it on three. 
  15. put in the fridge again, and repeat the operation, while respecting the position of the opening of the dough, which must always be on the right. 
  16. put the dough to rest in the fridge another 20 minutes, and here it is going to be ready for shaping. 
  17. Spread the dough into a rectangle with almost the length of your cake mold. 
  18. cover the surface of the rectangle with pate a speculoos   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/brioche-feuillet%C3%A9e-2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40804>)
  19. roll the dough to cover the dough with speculoos, then cut it in the middle, in length   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/brioche-feuillet%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40805>)
  20. weave the two pieces of dough, and put it in a mold, and let it rise well, at least 1 ½ hours, more time will be well benific   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/brioche-feuilletee.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40806>)
  21. When the brioche has risen well, gently wash with the milk and egg yolk 
  22. Bake in a preheated oven at 180 degrees for 20 to 25 minutes 



[ ![Puff pastry bun with speculoos](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/brioche-feuillet%C3%A9e-a-la-pate-au-speculoos.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-brioche-feuilletee-la-pate-au-speculoos.html/brioche-feuilletee-a-la-pate-au-speculoos-cr2>)
