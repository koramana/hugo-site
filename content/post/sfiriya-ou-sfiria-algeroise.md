---
title: Sfiriya or Algerian Sfiria
date: '2018-05-21'
categories:
- Algerian cuisine
- Cuisine by country
tags:
- Ramadan 2018
- Bread
- Anti Waste
- Healthy cuisine
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/05/Sfiriya-ou-Sfiria-alg%C3%A9roise-2-733x1024.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/Sfiriya-ou-Sfiria-alg%C3%A9roise-2-733x1024.jpg)

##  Sfiriya or Algerian Sfiria 

Hello everybody, 

Here is an anti-waste recipe that will please you during this Ramadan, a recipe of Algerian cuisine called Sfiriya or Sfiria Algiers, or chicken in white sauce with croquettes of stale bread. 

This recipe anti-waste allows us to recycle the rest of the bread but also to enjoy a super gourmet dish, and super delicious, I do not tell you ... My children prefer to taste the croquettes bread without the sauce, they like a lot, so I do it often, and sometimes I find myself running out of stale bread, hihihihi. 

You can see a video of the realization of this recipe: 

**Sfiriya or Algerian Sfiria**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/Sfiriya-ou-Sfiria-alg%C3%A9roise.jpg)

**Ingredients** For croquettes: 

  * 1 to 1 baguette and half bread stale 
  * 2 eggs 
  * 1 glass of warm milk 
  * 1 teaspoon baking powder 
  * ½ onion 
  * 100 gr grated cheese 
  * ½ teaspoon of salt 
  * ½ teaspoon of pepper 
  * 1 pinch of cinnamon 

For the sauce: 
  * 4 chicken legs 
  * 1 tablespoon smen (clarified butter) or 3 tablespoons oil 
  * 1 onion 
  * 1 teaspoon of salt 
  * ½ teaspoon of pepper 
  * 1 cinnamon stick (avoid the cinnamon powder that darkens the sauce) 
  * A handful of chickpeas dipped the day before (or in a jar) 
  * 1 liter and a half of water 



**Realization steps**

  1. Cut the stale bread into small pieces and pour over the milk. 
  2. Let the bread absorb and knead with your fingers so that there are no pieces. 
  3. Add the spices, onion, cheese, yeast and egg (if the dough does not pick up add the second egg yolk, so we do not add all at once). 
  4. Mix well with your fingers, let rest half an hour in the refrigerator to have beautiful balls that are held thereafter. 
  5. Meanwhile, prepare your sauce. Wash the chicken pieces. 
  6. Fry the onion, and the chicken pieces in the smen over low heat, until it has a nice color, which will give flavor and a little smoothness to the sauce. 
  7. Once the onion has melted, add the soaked chickpeas the day before, cinnamon, salt and black pepper and water and let simmer covered until the chicken is cooked and the sauce is reduced. 
  8. If you use precooked or canned chickpeas add the 10 minutes before the end. 
  9. Take out your preparation from the refrigerator and start forming medium-sized meatballs, larger than minced meatballs for example. 
  10. If your mixture sticks on the fingers, wet the palm of your hands slightly and form your balls or small patties. 
  11. Fry in a bath of hot oil to give them a beautiful golden color. Book. 
  12. When serving, place your meatballs all around your plate, place the chicken pieces with the chickpeas in the middle and sprinkle with a few ladles of sauce. Sprinkle with parsley and enjoy. 



![](https://www.amourdecuisine.fr/wp-content/uploads/2018/05/Sfiriya-ou-Sfiria-alg%C3%A9roise-1.jpg)
