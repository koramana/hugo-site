---
title: My brioche improvised to the taste of the orange
date: '2010-10-17'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, chaussons
- cuisine algerienne
- Tunisian cuisine

---
& Nbsp; yesterday, and although it was a busy day for me, I wanted a good brioche, and I do not know why, I went to put my ingredients to me, just trying to make a success without seeing books or blogs, and so I start to mix my ingredients, and try to have a beautiful dough, that of a nice brioche, and frankly, the result was great, my brioche a real delight, a very nice slice, and a very good taste, because there was in orange juice. without delay, I put my words to you. 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

yesterday, and although it was a busy day for me, I wanted a good brioche, and I do not know why, I went to put my ingredients to me, just trying to make a success without seeing books or blogs, and so I start to mix my ingredients, and try to have a beautiful dough, that of a nice brioche, and frankly, the result was great, my brioche a real delight, a very nice slice, and a very good taste, because there was in orange juice. 

without delay, I put you my ingredients: 

  * 500 gr of flour 
  * 2 eggs 
  * 1 C. a coffee of salt 
  * 90g of sugar 
  * 1 C. soup of milk powder 
  * 100 ml of milk 
  * 30 ml of water (next time I will try with 40 ml of water) 
  * 2 tbsp. a vanilla coffee 
  * 1 C. instant yeast 
  * 80 gr of butter 
  * 1 C. soup 
  * zest of an orange. 



for gilding 

  * 1 egg yolk 
  * 1 little vanilla 
  * a little bit of sugar 
  * a bit of milk 
  * flaked almonds. 



if you have a map, you can place the ingredients according to the recommended order for your map, but leave the butter last, and start the pate program (pizza for me) 

let it be hard not to be afraid of a long kneading because it gives a beautiful flow. 

restart the program if necessary, and add the butter by small piece. 

If you make it by hand, mix the flour, sugar, salt, milk powder, vanilla, add the beaten eggs, and knead, add the yeast and the milk a little warm, and knead gently , add the water little by little while kneading then, the orange juice, without stopping to pound, add after the zest of orange, and begin by adding the butter in small piece, while kneading until exhaustion of Butter. 

leave to rest in a warm place, for one hour, or until your dough doubles in volume, disengage it and form buns according to your taste, garnish them with the gilding and leave well raised for 1 hour. 

heat the oven to 180 degrees (preferably an oven light up and down at the same time) otherwise start by cooking from below while watching, then brown towards the end (do not overcook the buns) 

I assure you, it's a very good brioche, very delicious 

_do not forget the game of registration to the newsletter continues, each 20 th person who registered will be entitled to a beautiful book, (already 3 winners) do not forget to leave a comment, marking your registration, because I can not know who is who, thank you_
