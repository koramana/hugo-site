---
title: blueberry amandine pie
date: '2012-03-28'
categories:
- Café amour de cuisine
- riz

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/tarte-aux-myrtilles-1_thumb1.jpg
---
##  blueberry pie 

Hello everybody, a very **delicious[ pie ](<https://www.amourdecuisine.fr/categorie-12347064.html>) ** , **crunchy** with this fine **shortbread** " **so crunchy** ", Then this mellow of the **amandine cream** , irresistible is not it? 

So on top of that, imagine a nice layer of fresh blueberries **sugary tart** , covered with a thin, niellated layer of [ jam ](<https://www.amourdecuisine.fr/categorie-11700642.html>) , do you mind ????? What is great in this kind of pies, is that we can vary the decoration to infinity, you can see for example: [ strawberry shortcake ](<https://www.amourdecuisine.fr/article-tarte-sablee-aux-fraises-102110510.html>) .... so follow me, to discover this appetizing recipe, which comes straight from the kitchen Lunetoiles !!!! 

**blueberry pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/tarte-aux-myrtilles-1_thumb1.jpg)

portions:  8 

**Ingredients** for a pie of 24 cm shortbread : 

  * 120 gr of soft butter cut into pieces 
  * 70 gr of icing sugar 
  * 30 gr of almond powder 
  * 1 pinch of salt 
  * 1 egg 
  * 200 gr of flour 

Almond cream: 
  * 75 gr of soft butter cut into pieces 
  * 75 gr of almond powder 
  * 75 gr of sugar 
  * 5 gr of flour 
  * 2 eggs 

decoration: 
  * 400 gr of blueberries 
  * a little sugar or honey or agave syrup 
  * 4 c. tablespoon blueberry jam or jelly 



**Realization steps**

  1. Put the blueberries in a saucepan with a little agave syrup, bring to a boil stirring often. 
  2. Once there is no more juice, it will have reduced add the jam of blueberry or jelly and let cool. 
  3. Mix the butter by adding the icing sugar, then successively the almond powder, the salt, the egg and the flour. 
  4. Form a ball of dough, wrap it in a film of food and let rest at least 2 hours. 
  5. Preheat your oven to 180 ° 
  6. Roll out the dough, put it in your pan and cook for 20 minutes. 
  7. Let cool and reserve. 
  8. In a salad bowl, work in soft butter cream. 
  9. Add the sugar, the almond powder and the flour. Whip to get a homogeneous mixture. 
  10. Relax this preparation with the eggs that you incorporate one by one. 
  11. Pour on the bottom of cooled dough and bake for 15 minutes. 
  12. Let cool completely. 
  13. Cover with blueberries. 
  14. Place in the fridge for 1 hour. 



suivez moi sur: 
