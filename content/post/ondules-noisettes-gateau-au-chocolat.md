---
title: Wavy hazelnuts - chocolate cake
date: '2012-04-13'
categories:
- Cupcakes, macarons, et autres pâtisseries
- Chocolate cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-Ondul-s-aux-noisettes-_thumb.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-Ondul-s-aux-noisettes-_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-Ondul-s-aux-noisettes-.jpg>)

Hello everybody, 

a delicious [ cake ](<https://www.amourdecuisine.fr/categorie-10678925.html>) that Lunetoiles prepared for her niece's birthday, she loved to make a [ Barbie birthday cake ](<https://www.amourdecuisine.fr/article-princesse-barbie-gateau-d-anniversaire-86764345.html>) but his niece wanted a **Chocolate cake** , 

Luntoiles chose this hazelnut cake, a cake whose base is a very crispy and melting **shortbread** , covered with a beautiful layer of hazelnut cream, and finally decorated with a pretty **chocolate ganache** . 

look for more [ birthday cakes ](<https://www.amourdecuisine.fr/categorie-11700499.html>)

For 8 people 

Preparation time: 1 h 30 

Cooking time: 40 minutes 

Crystallization time: 3 hours 

Refrigeration time: 4 hours 

Ingredients: 

Milk chocolate ganache:    
500 g of milk chocolate 40%   
300 g whole liquid cream   
25 g of honey 

Sanded almond paste:    
180 g of butter + for the mold   
1 pinch of salt   
140 g icing sugar   
50 g of almond powder   
1 whole egg   
360 g (90 +270 g) of flour 

Vanilla custard cream:    
1/2 vanilla pod   
1 egg yolk   
30 g caster sugar   
8 g of flour   
5 g of cornstarch   
125 g whole milk 

Hazelnut cream    
100 g of butter   
100 g icing sugar   
10 g of cornstarch   
100 g of hazelnut powder   
1 whole egg 

[ ![Wavy Hazelnuts - Birthday Cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Ondul-s-noisettes-gateau-d-anniversaire_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Ondul-s-noisettes-gateau-d-anniversaire_2.jpg>)   
Prepare the milk chocolate ganache:   
Bring the cream to a boil with the honey and pour the boiling cream over the chocolate (pistoles or chocolate broken into pieces), in three times. Mix vigorously, using a spatula, by drawing small circles to create an elastic and shiny core.   
In this way a smooth and homogeneous ganache is obtained. Let cool to room temperature and let crystallize in a cool place for 3 hours, without placing in the refrigerator. 

Make the almond shortbread dough:   
In a salad bowl, mix the ointment butter, the salt, the icing sugar, the almond powder, the egg and 90 g flour.   
Once the mixture is homogeneous, add the remaining 270 g, and mix very briefly (if you use a robot, speed 1-2).   
Spread the dough about 3 mm thick between two sheets of parchment paper. Put in a frame of 27 x 30, and cook at 150/160 ° C (th; 5/6) 20 minutes. 

Prepare the pastry cream:   
Scrape the vanilla pod to extract the grains.   
In a bowl, whisk together cornstarch, flour, caster sugar and egg yolk.   
In a saucepan, boil the milk and dissolve the powders / eggs mixture with the boiling milk.   
Pour the previous mixture into the pan and cook over low heat, stirring constantly with a whisk until thickened.   
Continue cooking for a few moments without stopping, so that the cream does not attach to the bottom of the pan.   
The cream should become creamier and especially bright. Shoot on contact and reserve in the refrigerator about 1 hour. 

[ ![Wavy hazelnuts and chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Ondul-s-noisettes-et-chocolat_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Ondul-s-noisettes-et-chocolat_2.jpg>)

Prepare the hazelnut cream:   
In a container, put the butter in the ointment. While mixing, add the sifted icing sugar and starch as well as the hazelnut powder.   
Add the egg, then stir in the cold custard. 

On the shortbread already cooked and cooled, spread the cream of hazelnuts on the whole surface.   
Cook at 190 ° C (th.6 / 7) for about 20 minutes and allow to cool completely. 

Using a beveled piping bag, decorate with milk chocolate ganache creating waves and ripples effects.   
Let crystallize all in the refrigerator about 3 hours.   
At the end of the refrigerator, cut with a hot knife rectangles about 3 x 8 cm, to serve. 

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Ondul-s-aux-noisettes-et-chocolat_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Ondul-s-aux-noisettes-et-chocolat.jpg>)   


**Wavy hazelnuts - chocolate cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-d-anniversaire-Ondul-s-aux-noisettes-_thumb.jpg)

**Ingredients** Milk chocolate ganache: 

  * 500 g of milk chocolate 40% 
  * 300 g whole liquid cream 
  * 25 g of honey 

Sanded almond paste: 
  * 180 g of butter + for the mold 
  * 1 pinch of salt 
  * 140 g icing sugar 
  * 50 g of almond powder 
  * 1 whole egg 
  * 360 g (90 +270 g) of flour 

Vanilla custard cream: 
  * ½ vanilla pod 
  * 1 egg yolk 
  * 30 g caster sugar 
  * 8 g of flour 
  * 5 g of cornstarch 
  * 125 g whole milk 

Hazelnut cream 
  * 100 g of butter 
  * 100 g icing sugar 
  * 10 g of cornstarch 
  * 100 g of hazelnut powder 
  * 1 whole egg 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
