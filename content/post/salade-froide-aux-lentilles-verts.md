---
title: cold salad with green lentils
date: '2018-04-03'
categories:
- appetizer, tapas, appetizer
- Healthy cuisine
- idea, party recipe, aperitif aperitif
- ramadan recipe
- salads, salty verrines
tags:
- inputs
- Algeria
- Ramadan
- Ramadan 2018
- Healthy cuisine
- Balanced cuisine
- Tuna salad

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/03/salade-froide-aux-lentilles-verts-1.jpg
---
![cold salad with green lentils 1](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/salade-froide-aux-lentilles-verts-1.jpg)

##  cold salad with green lentils 

Hello everybody, 

This cold salad of green lentils is my favorite, I realize it very often in summer to accompany the grill, and it is always well appreciated, that's why I chose this recipe to participate with in our game of each 3 of the month, "recipe for an ingredient" 

Here is the very simple video recipe: 

{{< youtube FU5bRktKNdk >}} 

Previous rounds: 

**cold salad with green lentils**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/salade-froide-aux-lentilles-verts.jpg)

**Ingredients**

  * 3 c. tablespoon of olive oil 
  * 1 C. lemon juice in a squeeze 
  * 1 clove garlic, finely chopped 
  * ½ c. powdered cumin 
  * ½ c. ground coriander 
  * 1 C. tablespoon of mourtarde with the grains. 
  * 1 small red onion 
  * 2 tomatoes cut into cubes 
  * 400g lentils, cooked and drained 
  * 185 gr of tuna (in drained tin) 
  * 2 tbsp. chopped coriander 
  * black pepper 



**Realization steps**

  1. prepare your sauce by mixing, olive oil, lemon juice, garlic, caraway powder, salt according to taste   
(leave at the last minute because the tuna is salty, and the lentils too) 
  2. mix, the tomatoes cut in, the onion cut in, the lentils, add the crumbled tuna, and the coriander chiseled. 
  3. sprinkle with your already prepared sauce. 
  4. your salad is ready, very good, a real treat. 



![cold salad with green lentils 2](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/salade-froide-aux-lentilles-verts-2.jpg)

and here is the list of participants: 

Liste des participants : 
