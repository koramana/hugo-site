---
title: gravity cake, chocolate birthday cake M & ms
date: '2018-02-12'
categories:
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Pastry
- Icing
- Algerian cakes
- To taste
- Dessert
- Chocolate cake
- Chocolate Entremet

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/02/le-gravit%C3%A9-cake-gateau-danniversaire-1.jpg
---
![gravity cake, chocolate birthday cake M & ms](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/le-gravit%C3%A9-cake-gateau-danniversaire-1.jpg)

##  gravity cake, chocolate birthday cake M & ms 

Hello everybody, 

My Samir is already 4 years old, and for his birthday, I made him a cake with the candies he likes most, the gravity cake chocolate M & ms. No need to show you the cake just at the end of the tasting, because there was only the carcass, and a little chocolate ganache spared fingers that had licked the way, yes that will say no to a chocolate cake all covered with candy? 

The Gravity Cake is a birthday cake super easy to make, super easy, but above all too beautiful. Just have a good base of birthday cake, here I use a chocolate cake of great simplicity, decorated with a nice layer of ganache, then generously covered with a nice topping with chocolate fingers, and sweets M & Ms. 

**gravity cake, chocolate birthday cake M & ms **

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/le-gravit%C3%A9-cake-gateau-danniversaire-3.jpg)

**Ingredients** the chocolate cake: (I made 2 cakes to have 4 discs, in the end I only used 3 discs) 

  * 200 gr of flour 
  * 200 gr of butter 
  * 200 gr of dark chocolate pastry 
  * 180 gr of sugar 
  * 4 eggs 
  * 1 sachet of baking powder 

the chocolate ganache: 
  * 300 gr of dark chocolate 
  * 400 ml of liquid cream 
  * 50 gr of butter 

decoration: 
  * 2 boxes of chocolate fingers 
  * 2 bags of M  & m's 



**Realization steps** preparation of the chocolate cake: (we will prepare 2 cakes, each with 4 eggs) 

  1. melt chocolate and butter in a bain-marie, let cool 
  2. beat the egg yolks with the sugar until blanching 
  3. introduce the chocolate mixture while whisking. 
  4. add the sifted flour and baking powder mixture. 
  5. whisk the egg whites into the snow and gently add them to the chocolate mixture. 
  6. pour your machine into a nearly 18 cm baking pan lined with baking paper and bake in a preheated oven at 180 ° C for 50 to 60 minutes. 
  7. prepare the second cake in the same way and cook. Let cool before garnishing. 

prepare the chocolate ganache: 
  1. simmer the liquid cream over medium heat. 
  2. crush the chocolate and place it in a salad bowl, pour over the hot liquid cream, and let it melt. 
  3. emulsify well to homogenize the mixture, add the butter and mix well to have a beautiful ganache. 
  4. let cool a little. whip a little ganache. 

cake mounting: 
  1. cut each cake into a disc of the same size (remove the cap of the cake if during the cooking it inflated well, to have a flat cake on the surface) 
  2. fill each disc with a layer of ganache to glue the cake discs to each other (I only used 3 discs in the end, I found my cake quite high) 
  3. cover your cake generously with the ganache, and start the decoration. 
  4. stick the chocolate fingers vertically around the cake leaving an opening of almost 10 cm. 
  5. At the place not covered with fingers, place the M  & M's as you see in the photo, giving the shape of a triangle. 
  6. Stick 3 straws with a little ganache. Plant the straws in the cake at the tip of the tip formed by the M & m's. Then glue the M & m's all along the straws. Finish by placing the M & m's bag on top of slightly curved straws. 
  7. Place in the fridge until tasting. 



![Gravity Cake, Birthday Cake 2](https://www.amourdecuisine.fr/wp-content/uploads/2018/02/le-gravit%C3%A9-cake-gateau-danniversaire-2.jpg)
