---
title: ktayef, Algerian pastries
date: '2012-04-01'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ketaif31.jpg
---
& Nbsp; the recipe in Arabic is here hello everyone, a delicious Algerian pastry, it looks like the baklawa, but it is easier to realize on one side, it needs less stuffing of another, but only it is necessary to find the pasta, or qtayef, which is not found everywhere, which is a pity. with this paste, you can prepare kounafa, or fingers of ktayefs. and for more Algerian cakes, jump on the Algerian cake index & nbsp; & Nbsp; the ingredients: 2 qtayef balls 1 bowl of ground pistachios 3 cases of sugar 1/2 tsp. a cinnamon coffee 1/2 hell 

##  Overview of tests 

**User Rating:** Be the first one!  0 

[ ![http://a10.idata.over-blog.com/3/49/82/14/API/2009-12/ketaif_2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/550x412-c4.png) ](<https://www.amourdecuisine.fr/article-32532261.html>)

Hello everybody, 

a delicious [ **Algerian pastry** ](<https://www.amourdecuisine.fr/categorie-12135732.html>) , she looks like [ baklawa ](<https://www.amourdecuisine.fr/categorie-12344741.html>) but it is easier to realize on one side, it needs less stuffing of another, but only you have to find the pasta **ktaif** , or **qtayef** that we do not find everywhere, which is a pity. 

with this dough, you can prepare some [ kounafa ](<https://www.amourdecuisine.fr/article-mini-konafa-ktayefs-a-la-creme-82031678.html>) , or some [ ktayefs fingers ](<https://www.amourdecuisine.fr/article-doigts-de-kounafa-aux-raisins-ktaif-konafa-kadaif-89358522.html>) . 

and for more [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , jump on the index of [ Algerian cake ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)

[ ![http://a10.idata.over-blog.com/3/49/82/14/API/2009-12/ketaif4_4.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/550x412-c11.png) ](<https://www.amourdecuisine.fr/article-32532261.html>)

Ingredients: 

  * 2 balls of qtayef 
  * 1 bowl of ground pistachios 
  * 3 cases of sugar 
  * 1/2 c. a cinnamon coffee 
  * 1/2 glass of orange blossom water 
  * 100 gr of butter 



[ ![ketaif3](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ketaif31.jpg) ](<https://www.amourdecuisine.fr/article-32532261.html>)

for the syrup: 

  * 500 gr of sugar 
  * 1/2 liter of water 
  * 2 cases of orange blossom water 



prepare the pistachios, bake them if necessary, crush them, add the sugar and cinnamon, moisten with 1/2 glass of orange blossom water. 

generously butter a round baking dish made of metal or pyrex 

cover the bottom of the dish with a ball of qtayef scattering 

spread over the whole surface the pistachio paste. 

Cover the whole with the 2nd qtayef ball by scattering it and taking care to well enclose the pistachio paste. 

sprinkle with butter, and brown both sides in the oven for about 1/2 hour, or in your oven. 

meanwhile prepare the syrup. 

pour the sugar and water into a saucepan and cook for 20 to 30 minutes after boiling until the syrup flows in a thin stream. 

perfume the syrup with orange blossom water. 

when the cake is golden, remove it from the oven. 

[ ![ketaif6](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ketaif61.jpg) ](<https://www.amourdecuisine.fr/article-32532261.html>)

good tasting. 

[ ![ketaif7](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ketaif7_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-32532261.html>)
