---
title: Onion omelette
date: '2017-10-16'
categories:
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/omelette-aux-oignons-024.CR2_1.jpg
---
![omelette aux-onions 024.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/omelette-aux-oignons-024.CR2_1.jpg)

##  Onion omelette 

Hello everybody, 

An omelette with onions, quickly made ... well done ... This is what I realize, when I'm short of idea, or I'm overwhelmed by the household, and I forget that I did not eat anything ... . Some onions cut and roasted in a little oil, some eggs beaten on top, and tada .... It's ready…. 

so, you're up for this recipe easy, but too good? 

**Onion omelette**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/omelette-aux-oignons-016.CR2_1.jpg)

portions:  4  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 4 eggs 
  * 4 tablespoons of milk 
  * 3 medium onions 
  * 1 green onion (optional0 
  * salt 
  * thyme 



**Realization steps**

  1. clean the onions and cut them with a sharp knife, into slices. 
  2. separate the washers to have rings. 
  3. cut the green onion or slice too. 
  4. cook over low heat, with a little oil, about ten minutes, in a non-stick pan, stirring regularly with a wooden spoon. 
  5. beat the omelette eggs with the milk, salt and thyme. 
  6. when the onions are very tender, pour the egg mixture on it, and cook on low heat for about 5 minutes, 
  7. turn the omelet off the heat and cook on the other side. 
  8. serve immediately. 



![omelette aux-onions 004.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/omelette-aux-oignons-004.CR2_1.jpg)
