---
title: Homemade orange juice and carrots
date: '2017-01-13'
categories:
- boissons jus et cocktail sans alcool
- Cuisine saine
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/jus-dorange-et-carottes-1.jpg
---
[ ![orange juice and carrots 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/jus-dorange-et-carottes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/jus-dorange-et-carottes-1.jpg>)

##  Homemade orange juice and carrots 

Hello everybody, 

Although I am more used to make a fresh homemade juice bitch has my little jewelry, I never regret having bought my juicer, and that I fully enjoy savor fresh juices, no dyes, or additives, or even full of sugar, I do not know for her reason. 

This homemade orange juice and carrot, has a great place in my heart, it is the first homemade juice, I learned to do, and we enjoyed it continuously during Ramadan, especially since Ramadan time was in full orange season, and especially we lived in Dréan, a small town between Annaba and Souk-Ahras, whose region is known for the production of oranges, tomatoes and apricots ... we were even entitled to do picking ourselves, so the orange trees in the area were not lacking. 

so different from juices extracted in the centrifuge, this juice is cooked, and takes a little time to achieve, and in addition, it should be diluted with a little water.   


**Homemade orange juice and carrots**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/jus-dorange-et-carottes-3.jpg)

portions:  6  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 1 kg of carrots 
  * 1 kg of oranges 
  * 2 lemons 
  * sugar (according to your taste) 
  * 2 apricot yogurts (optional) 



**Realization steps**

  1. scrape the skin of the carrots, and cut them into slices 
  2. place them in a pot with water and boil until carrots are cooked. 
  3. Mix the carrots in the blinder bowl, or with the blender foot with some cooking water (keep the rest of the water) 
  4. When the carrot smoothie is cool, add the juice of the oranges, and lemons. 
  5. At this point, you can add the yogurt (it gives a taste and incomparable flavor to the juice), as you can do without. 
  6. Add the remaining carrot water (depending on whether you want it diluted or not) 
  7. Add the sugar according to your taste (if you think you need to dilute your juice even more, add some fresh water.) 



[ ![orange juice and carrots](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/jus-dorange-et-carottes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/jus-dorange-et-carottes.jpg>)

This juice is beautiful and it is too good. But to be more frank, I prefer the one I get to the centrifuge: 

1 kilo of carrots, 

2 oranges 

1 lemon 

and I pass to the centrifuge. This juice takes me less time to prepare, I tasted immediately (especially if all my ingredients come directly from the fridge), and more importantly, I do not add any gram of sugar to this juice. 

Even more importantly, there is a beautiful layer of moss following fruit juice extraction, which is good. 

[ ![orange juice centrifuge](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/jus-dorange-centrifugeuse.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/jus-dorange-centrifugeuse.jpg>)
