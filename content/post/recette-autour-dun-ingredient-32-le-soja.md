---
title: 'Recipe for an ingredient # 32: Soy'
date: '2017-09-16'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg)

source: Pinterest 

Hello everybody, 

We are turning more and more towards sustainable and vegetable alternatives, which is why the ingredient I chose is:  **Soy** .  **Soy in all its forms: soy milk, tofu, cream, yoghurt, soy sprouts, soy sauce ...** ![Recipe for an ingredient # 32: Soy](https://www.amourdecuisine.fr/wp-content/uploads/2017/09/soja-1.jpg)
