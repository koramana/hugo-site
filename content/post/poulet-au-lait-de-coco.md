---
title: chicken with coconut's milk
date: '2015-04-14'
categories:
- cuisine indienne
- Cuisine par pays
tags:
- India
- Easy cooking
- Fast Food
- Full Dish
- dishes
- Spice
- accompaniment

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/poulet-a-la-vanille-et-lait-de-coco_thumb1.jpg
---
![chicken with vanilla and coconut milk](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/poulet-a-la-vanille-et-lait-de-coco_thumb1.jpg)

##  chicken with coconut's milk 

Hello everybody, 

Want new flavors, want a different touch in your kitchen, want an exotic escape by the cold, towards the end of the world and the sun with a very nice recipe of Reunion, very tasty, well-scented, for a pure moment of pleasure. 

anyway, that's what I'm doing since my diet, and since my coach asks me to eat a little more chicken breast and salmon, I try to vary and change, to get out of a routine which may make me hate the diet. 

**chicken with coconut's milk**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/poulet-vanille-coco_thumb1.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 600 gr of chicken breast. 
  * 2 tomatoes. 
  * 1 lime. 
  * 1 onion. 
  * 1 clove of garlic. 
  * 300 ml of coconut milk. 
  * 1 C. grated fresh ginger. 
  * 2 tbsp. grated coconut. 
  * ½ vanilla pod. 
  * 30 g of butter 
  * 2 tbsp. curry. 
  * salt and pepper. 



**Realization steps**

  1. Peel, seed and crush the tomatoes. 
  2. Mix with chopped garlic and onion, ginger, lemon juice and curry. 
  3. Slice the chicken into strips, then marinate in the tomato mixture for about 30 minutes. 
  4. Cook in butter for 10 minutes and sprinkle with coconut milk. 
  5. Add the half vanilla pod detailed in pieces. 
  6. Add salt and pepper. 
  7. Simmer for 20 minutes over low heat and uncovered. 
  8. Sprinkle with grated coconut and serve with a good spicy rice, or pasta (tagliatelle sautéed in butter for me). 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
