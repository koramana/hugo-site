---
title: trida - chicken pasta squares "تريدة بالدجاج"
date: '2014-02-09'
categories:
- dessert, crumbles and bars
- Chocolate cake
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/trida_thumb_1.jpg
---
[ ![trida](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/trida_thumb_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/trida_4.jpg>)

##  trida - chicken pasta squares "تريدة بالدجاج" 

Hello everybody, 

this is still one of the traditional Algerian dishes, generally prepared in eastern Algeria, a kind of very fine dough cut into small squares, which are prepared well in advance, and dried like couscous.    


**trida - small squares of chicken pasta "تريدة بالدجاج"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/trida-pour-moharem-011_1.jpg)

portions:  4  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients**

  * square pasta 
  * chicken (thighs) 
  * onion 
  * 3 cases of olive oil 
  * 1 case and ½ of canned tomato 
  * 1 handle of chickpea 
  * 2 cloves garlic 
  * salt, pepper, paprika, ras el hanout 



**Realization steps**

  1. let's start by preparing the sauce, mixing the onion and garlic, put in the bottom of a couscous pot, adding the oil, the chicken, the canned tomato and the spices. let it simmer a little, and then cover well with water. and add the chickpeas. 
  2. in the meantime, we go to our trida, 
  3. we put the quantity we want to prepare in a terrine, we add a case of oil, we rub in hands. during this time the sauce should start boiling. 
  4. so we put our edges in the top of a couscoussier, and place it on the bottom of our couscoussier. 
  5. when the steam begins to escape between the edges, take the couscous and pour into a bowl and sprinkle with the sauce, and try to get the sauce in and try to separate the small squares from other.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/21543867.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/21543867.jpg>)
  6. and then put the couscoussier again on the top and redo this action 2 to 3 times until the edges become soft.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215439301.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215439301.jpg>)
  7. we remove our edges and add a little butter. 
  8. before serving, put our edges in a thick pot, and sprinkle with sauce and allow to absorb well over low heat.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215440001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/215440001.jpg>)
  9. serve well garnished with eggs, chickpeas and chicken. 



if you have tried one of my recipes, send me the picture here: 

**[ Mes recettes chez vous ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **
