---
title: bavarian strawberry
date: '2012-04-30'
categories:
- bavarois, mousses, charlottes, recette l'agar agar
- dessert, crumbles and bars
- recettes sucrees

---
& Nbsp; Ines birthday cake with almond paste Bavarian speculoos / almonds / mascarpone / strawberry & nbsp; hello everyone, a strawberry bavaroise that can be easily decorated to become a very nice birthday cake, or be simple without a dough a deco a sour or almond paste & nbsp; So here is the cut of this dessert: strawberry mascarpone, and I pass you the recipe: my cake was composed of: a biscuit speculoos / grilled almonds a foam mousse mousse mascarpone a mousse of strawberries for the base of speculoos it is well simple, according to the dimension & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 0.7  (  1  ratings)  0 

[ Ines birthday cake with almond paste ](<https://www.amourdecuisine.fr/article-la-recette-du-gateau-d-anniversaire-d-ines-59220048.html>)

[ Bavarian speculoos / almonds / mascarpone / strawberry ](<https://www.amourdecuisine.fr/article-la-recette-du-gateau-d-anniversaire-d-ines-59220048.html>)

Hello everybody, 

a **strawberry bavarian** which can easily be decorated to become a very beautiful [ **birthday cake** ](<https://www.amourdecuisine.fr/categorie-12125855.html>) , or be presented simply without a deco at the **sure** or **almond paste**

So here is the cup of this dessert: **strawberry mascarpone** , and I'll give you the recipe: 

my cake consisted of: 

a **biscuit speculoos / roasted almonds**

a foam **[ Chiffon cream ](<https://www.amourdecuisine.fr/article-genoise-creme-mousseline-103155968.html>) with mascarpone **

a **Strawberry mousse**

for the base of speculoos it is very simple, according to the size of the pastry circle, a box and a half of speculoos (or even any other biscuit) 

100 gr of grilled skinned almond, melted butter to have a paste that can be crammed without problem 

so grind the speculoos, then grind the roasted almonds, mix them, and add the melted butter very slowly until you have a paste that can stand 

pile this dough into the pastry circle, to form a uniform cookie. 

Mascarpone mousse cream mousse: 

  * 500 ml of milk 
  * 57 g of sugar 
  * 1 packet of vanilla sugar 
  * 2 egg yolks 
  * 30 g cornflour 
  * 250 gr of mascarpone 
  * 300 ml of fresh cream 
  * 8 grams of gelatin 



place the gelatin in a little water and let it swell. 

Mix the egg yolks, cornflour, flour and sugars in a saucepan and whisk well.   
Pour the hot milk over, mix at the same time, and put back on medium heat while stirring constantly with a wooden spoon until the mixture thickens (about 4-5 minutes). 

off the fire incorporate a small amount of mascarpone and whisk well 

let cool and add the remaining mascarpone while whisking 

put the cream in whipped cream with a tablespoon of sugar. 

squeeze the gelatin and melt it on a bain-marie, and add it to the pastry cream mascarpone mixture 

then add the whipped cream in small quantity, without breaking it, to have a well ventilated foam 

pour this mousse on the basis of speculoos. and put it in the fridge for it to take. 

strawberry mousse: 

300 grs fresh strawberries (or in box) 

100 grams of sugar 

300 ml of fresh cream 

8 grams of gelatin 

red dye. 

cut the strawberries into pieces, add the sugar, cook a little over low heat, if you find that you have a lot of juice, drain a little, my daughter liked the strawberry juice hihihiih 

Now put the cream in whipped cream, with a tablespoon of sugar and a little red dye, to have a beautiful pink foam. 

have the gelatin swell in a little water, wring it out and incorporate it into the strawberry compote, whisk it well, let it cool, and add the whipped cream. 

Pour this mousse over the mascarpone mousse and put back in the fridge so that everything goes well. 

now we go to the preparation of the decoration, 

we start with the flowers, I used sugar paste that I colored in shrimp a little dark and the other a little clearer 

spread the sugar dough over a surface sprinkled with sugar, until you get a thin layer. 

cut circles, according to your taste for the size of the flower, I made circles of 4 cm and others of 3 cm. 

shape a cone of almost 2 cm and a half high, and start sticking the pastry circles one by one, use a little water at the base of each circle, so that it sticks, insert the circles, to have a beautiful flower, let the flower dry a little, on a paper that you have placed in a glass to not spoil the shape of your flower, after a few minutes, return to your flower, and deform a little petals, to give the pace of a real flower. 

now prepare the butterflies, and for that you will need white chocolate and dye according to your taste. 

draw the butterflies on a white sheet, place your melted and colored chocolate in a small sachet, make a small hole at the corner of the bag and fill the butterfly, after drying the wings of the butterfly, take the melted dark chocolate and fill the body of the butterfly , place your leaf in a place that allows you to have the V shape so that the butterfly is not flat. 

let dry. it will stand alone from the sheet. 

so I wanted to make intercalated form between pink and mauve. to have the shape on the photo, to make the right shapes, use a sheet of paper 

now, remove the pastry ring very slowly and draw rectangles with the height of the cake, and the width of each petal, and stick each rectangle with on the side of the cake. 

do not do my stupidity, or I start by placing the almond paste in the pastry circle, as you see in the picture, and I pour my Bavarian mousses the next day trying to unmold the cake, the almond paste melted because of the moss, and I almost screwed up the cake 

here is the picture of my stupidity, so better use the decoration just an hour before presenting the cake 

bonne soirée. 
