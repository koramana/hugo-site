---
title: fennel soup recipe, easy velvety
date: '2013-01-18'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-au-fenouil.CR2-copie-11.jpg
---
![Soup au fenouil.CR2-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-au-fenouil.CR2-copie-11.jpg)

##  fennel soup recipe, easy velvety 

Hello everybody, 

here is a fennel soup, well scented and very unctuous! Not that, with the hazelnuts in it, and crushed over it, this velvety takes another dimension. 

a recipe very easy to make, and very very tasty. 

![Soup de fenouil.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-de-fenouil.CR2_1.jpg)

**fennel soup recipe, easy velvety**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/veloute-de-fenouil-035.CR2_1.jpg)

**Ingredients**

  * ½ diced onion 
  * 1 large fennel bulb, (white part only) diced 
  * some dill stalks 
  * 1 cube of vegetable broth diluted in water 
  * ½ cup of whole hazelnuts 
  * 1 tablespoon of olive oil 
  * 1 medium sized potato 
  * Salt and black pepper. 



**Realization steps**

  1. Preheat the oven to 180 ° C, place the hazelnuts on a baking sheet and bake for 6 to 8 minutes, checking. hazelnuts are ready when the skin cracks. Remove from oven and let cool. 
  2. heat the oil in a pot of medium size, 
  3. Add the fennel, onions and potato and fry for 4 to 5 minutes, stirring occasionally, until it becomes a little translucent. 
  4. Reduce to a minimum and cook again. 
  5. remove the skin from the chilled hazelnuts. Add the peeled hazelnuts directly to your pan, (leave some for decoration) 
  6. Add the dill, and the vegetable broth, salt and pepper to taste, increase the heat to bring to a boil, then reduce and simmer for 15 minutes. 
  7. after cooking, reduce the soup with the mixer's foot, or in the blinder bowl. 
  8. serve hot garnished with crushed hazelnuts. 



![Soup-of-fennel - kitchen vegetarienne.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-de-fenouil-cuisine-vegetarienne.CR2_1.jpg)
