---
title: The fake tiramisu with strawberry
date: '2015-12-31'
categories:
- dessert, crumbles and bars
- sweet verrines
tags:
- New Year
- Dessert
- mosses
- Holidays
- desserts
- verrines
- creams

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/faux-tiramisu-aux-fraises-1.jpg
---
[ ![The fake tiramisu with strawberry](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/faux-tiramisu-aux-fraises-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/faux-tiramisu-aux-fraises-1.jpg>)

##  The fake tiramisu with strawberry 

Hello everybody, 

Here is a very good dessert that I named a fake strawberry tiramisu, These verrines look like in tiramisu montage, but the ingredients are different, instead of putting the mascarpone I put yogurt, because I can not find the masacrpone in Algerian. 

But I'm not talking about the result, I promised you it was too good. This dessert was so succulent, unctuous .... What happiness at the end of the spoon. **The Kitchen of Tema.**

**The fake tiramisu with strawberry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/faux-tiramisu-aux-fraises.jpg)

portions:  6  Prep time:  30 mins  total:  30 mins 

**Ingredients**

  * biscuits with a spoon. 
  * 250 ml of cold liquid cream. 
  * 40 g icing sugar. 
  * 2 jars of flavored yoghurt (wooden fruit for me). 
  * strawberries cut into small pieces. Leave a few whole to decorate. 
  * syrup or fruit juice to choose (for me juice fruit cocktail). 



**Realization steps**

  1. add the cream with the icing sugar in a firm chantilly then add the yoghurt pots, mix and leave in the fridge. 
  2. cut the biscuits in half and soak them lightly with syrup or juice and line them in the bottom of the verrines. Put some strawberry pieces and so on the yoghurt cream. Continue the operation and finish with a layer of the cream. 
  3. decorate with a whole strawberry. 
  4. refrigerate at least 3 hours. 



[ ![fake tiramisu with strawberries](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/faux-tiramisu-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/faux-tiramisu-aux-fraises.jpg>)
