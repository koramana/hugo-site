---
title: eggplant steak in white sauce
date: '2015-05-28'
categories:
- recipe for red meat (halal)
tags:
- Ramadan 2015
- Ramadan
- Algeria
- dishes
- Chicken tagine
- tajine
- Chicken

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/12/steak-daubergines-en-sauce-blanche.jpg
---
##  eggplant steak in white sauce 

Hello everybody, 

A very old aubergine recipe, which will please you. A white chickpea sauce to accompany these marinated aubergine steaks, a dish too good, and very rich. 

I launched a challenge to make this dish on my group, and the winning photo was going to be highlighted on my blog: 

[ ![eggplant steak in white sauce](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/steak-daubergines-en-sauce-blanche.jpg) ](<https://www.amourdecuisine.fr/article-25849479.html/steak-daubergines-en-sauce-blanche>)

**eggplant steak in white sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/steak-daubergine-en-sauce-blanche-1.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 1 large Aubergine or 2 small 
  * 1 chick weight handle 
  * 1 small onion 
  * 1 head of garlic 
  * 1 bunch of parsley 
  * 4 beautiful chicken legs (or whatever you have on hand) 
  * salt, black pepper and cumin 
  * oil 



**Realization steps**

  1. first of all, we start by preparing our sauce of chick weight, simple is easy, take a small pot, 
  2. put the chicken in it with a little oil   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/aubergine_thumb_31.jpg) ](<https://www.amourdecuisine.fr/article-25849479.html/aubergine_thumb_31>)
  3. add the onion and 3 cloves of garlic (the garlic you can decrease according to your taste) that you have to pass to the robot, or then to the grater. 
  4. add salt and black pepper, as well as cumin. 
  5. let simmer a little, then cover the chicken with water (do not put too much water), because the sauce must be well reduced at the end of cooking (so be careful with salt) 
  6. now take the eggplants, peel them, and cut them in length, in very large slices, (not fine) 
  7. make nicks, and salt a little, and set aside. 
  8. take the rest of the garlic, go to the grater, 
  9. add the well chopped parsley 
  10. season with garlic and black pepper, 
  11. put this farce in the middle of the cuts that you have drawn on your eggplant. 
  12. put the eggplant in the flour, then fry in a hot oil 
  13. drain on paper towels 
  14. serve the eggplant, drizzled with the reduced chickpea sauce. 
  15. garnish the dish to your taste, (I'm not good at it myself, hihihihihihi) 



[ ![eggplant steak in white sauce 1](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/steak-daubergine-en-sauce-blanche-1.jpg) ](<https://www.amourdecuisine.fr/article-25849479.html/steak-daubergine-en-sauce-blanche-1>)

good tasting 

Thank you for all your comments (if your comment is not validated yet, it is that I have not yet had the time to read it and to answer you, so thank you for being patient) 

Thanks to the people who continue to subscribe to the newsletter (the newsletter is free, you can subscribe for the reception of the alert of publication of an article, and / or the reception of the newsletter) 

merci 
