---
title: 'Recipe for an ingredient # 39 Herbs'
date: '2018-04-10'
categories:
- Coffee love of cooking
tags:
- Meat
- Pesto
- herbs
- skewers
- plancha
- Chicken
- tajine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg
---
![source: Pinterest](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/recette-autour-dun-ingredient.jpg)

source: Pinterest 

##  Recipe for an ingredient # 39 Herbs 

Hello everybody, 

Here we are in round 39 of our Recipe game around an ingredient, and today I just announced the name of the new godmother, the link to his blog and the star ingredient of this new round. 

We were well taken Samar and me towards the end of the previous round and we had completely forgotten to choose a godmother before the end of the round to be announced by the previous godmother. 

![recipe-around-dun-ingrc3a9dient-39](https://www.amourdecuisine.fr/wp-content/uploads/2018/04/recette-autour-dun-ingrc3a9dient-39.jpg)

and this is what Emeline tells us: 

> _**Regarding the star ingredient of the month, I wanted to celebrate the spring and the green color that comes back in our plates with ... herbs! Coriander, chives, basil, tarragon ... the choice of flavors is vast to treat us to savory dishes but also delicious desserts.** _

**_The principle of the game:_ **

2 - Make a new recipe (recipes are not accepted) around the star ingredient ie for this month **aromatic herbs.**

4 - Publish the article on **May 3 at 8 am** . 

5 - In the article you must mention: 

\- The initiators of the game **Samar and Soulef**

\- Add the list of participants with the titles of their recipe 

If despite your registration, you have not managed to make your recipe, contact me before the 3 so that I remove you from the list and that I inform the other participants. (This remains a game and not a constraint). 

_New since edition # 12_

A Facebook page has been created so that the participants the URL of their blog and the title of their recipe, and this, to facilitate the task of the godmother and participants. 

_Voici les différents ingrédients et leurs marraines :_
