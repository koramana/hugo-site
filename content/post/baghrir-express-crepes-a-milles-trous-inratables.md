---
title: Baghrir Express, pancakes with a thousand irreversible holes
date: '2017-12-10'
categories:
- crepes, donuts, donuts, sweet waffles
- Algerian cuisine
- sweet recipes
tags:
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/baghrir-express.jpg
---
![express baghrir, crêpes with a thousand unbreakable holes](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/baghrir-express.jpg)

##  Baghrir Express, pancakes with a thousand irreversible holes 

Hello everybody, 

We always like to make baghrirs or crepes with a thousand irreparable holes, but for those who do not know it, the traditional baghrirs take a long time to make it ready, 4 hours is the minimum, to have well-drilled baghrirs , and well raised. 

Not to mention to come back each time mixing and degassing the dough, again and again ... It is sacred to follow these steps to the letter to have beautiful [ baghrir ](<https://www.amourdecuisine.fr/article-baghrir.html>) like those of my mother. 

**Baghrir Express**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/baghrir-express-2-1.jpg)

Cooked:  Algerian cuisine  Recipe type:  to taste  portions:  10  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 1 fine semolina glass (200 ml glass) 
  * 1 glass minus two fingers 
  * 1 tablespoon of sugar 
  * ½ teaspoon of salt 
  * 1 tablespoon baker's yeast 
  * 1 teaspoon of baking powder 
  * about 450 ml of warm water (+/- depending on the absorption of your meal and flour) 



**Realization steps**

  1. mix the ingredients well in a large salad bowl using a blender stand 
  2. let rise around 30 minutes 
  3. mix again. 
  4. put a Tefal frying pan over medium heat, or use a pancake pan as I do it personally.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/baghrir-express-2.jpg)
  5. cook using a ladle of dough that you pour into the pan, the dough should spread well in the pan. 
  6. cook well before removing from pan (only cook one side) 
  7. at the moment of serving, decorate according to your taste, with butter and crystallized sugar. 



![express baghrir, crêpes a thousand holes irreparable 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/baghrir-express-1.jpg)
