---
title: Algerian candles / cakes
date: '2014-01-31'
categories:
- appetizer, tapas, appetizer
- diverse cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/IMG_0082-1024x768.jpg
---
[ ![almond candles](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/IMG_0082-1024x768.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/IMG_0082.jpg>) Hello everybody, Today I share with you the recipe for this Algerian cake without cooking, known as the candles (Algerian cakes) that one of my readers Souhila, likes to share with us, using the system that I put you online: [ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.") ... it's very simple to use this system, do not worry how the recipe will ultimately be, because, I arrange it personally, before it is online. So we come back to the recipe for these almond candles? a cake without cooking, with some ingredients and at the same time not 2 hours your cake is ready to serve ...  Are you interested in the recipe? 

**the candles**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/IMG_0082-1024x768.jpg)

portions:  30  Prep time:  60 mins  total:  1 hour 

**Ingredients**

  * Ingredients: 
  * 500g ground almonds 
  * 4to5cas of orange jam for me you can put other 
  * a bag of vanilla 

for garnish 
  * a little food coloring according to your choice 
  * 200g of white chocolate 
  * some cherry stalks 



**Realization steps**

  1. In a container mix almond and vanilla 
  2. pick up with jam until you have a workable paste that is easy to work with. 
  3. form a long pudding, 
  4. cut small sticks then form small candles and let dry an hour in the fridge. 
  5. after drying, melt chocolate, put a few drops of dye and immerse the candles in the chocolate. 
  6. let drain on a rack. 
  7. after a few minutes of drying, pour a little melted chocolate on each candle to give the effect of melted candle 
  8. prick in each candle a cherry stalk that will look like a wick 
  9. good luck 



kisses 

if you have tried one of my recipes, send me the picture here: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

and if you have a recipe for you that you want to publish on my blog, you can put it online here: 

[ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
