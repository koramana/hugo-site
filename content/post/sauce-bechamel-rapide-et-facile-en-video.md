---
title: quick and easy bechamel sauce video
date: '2013-09-17'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles and bars
- panna cotta, flan, and yoghurt
- ramadan recipe
- sweet recipes
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/09/sauce-bechamel1.jpg
---
[ ![quick and easy bechamel sauce video](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/sauce-bechamel1.jpg) ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html>)

##  quick and easy bechamel sauce video 

Hello everybody, 

in many of my recipes I use bechamel sauce, and today I share with this easy and delicious recipe, which will always be present in your gratins, with your pasta, and in many delicacies, then to you my recipe for bechamel sauce quick and easy video ... 

**quick and easy bechamel sauce video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/sauce-bechamel1.jpg)

Recipe type:  basic recipe  portions:  8  Prep time:  5 mins  cooking:  15 mins  total:  20 mins 

**Ingredients**

  * 70 g of butter 
  * 70 g of flour 
  * 800 ml of milk 
  * Nutmeg 
  * Parmesan (optional) 
  * Salt pepper. 



**Realization steps**

  1. Melt the butter in a saucepan then 
  2. pour the flour. Stir quickly with the spatula and cook for 2 minutes over low heat without stopping stirring until light. 
  3. Remove from heat and gently add the milk while whisking gently. 
  4. Put everything back on the heat and continue to whip to make the mixture thicken until boiling. 
  5. Sear and pepper then add the grated nutmeg. 
  6. Remove from heat and add the grated parmesan, mix and it is ready. 



Video of preparation: 

![quick and easy bechamel sauce video](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/bechamel-facile-et-rapide1.jpg)
