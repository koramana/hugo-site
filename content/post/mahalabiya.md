---
title: Mahalabiya
date: '2017-10-06'
categories:
- panna cotta, flan, and yoghurt
- ramadan recipe
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/06/MAHALLABIYA-2.CR2_.jpg
---
[ ![MAHALLABIYA 2.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/MAHALLABIYA-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/MAHALLABIYA-2.CR2_.jpg>)

##  Mahalabiya 

Hello everybody, 

A dessert we love a lot at home, this oriental-flavored mahalabiya is just a delight. Each spoon of rose-scented mahalabiya and orange blossom water take you as far as your imagination, endless sweetness, incomparable freshness, and a delight to be tasted. 

I already have a delicious [ Mhalabiya with apricots ](<https://www.amourdecuisine.fr/article-mahalabiya-aux-abricots-mahalabiya.html> "mahalabiya with apricots, mahalabiya") on the blog, as you can also try the recipe from [ mhlabi, cream with constantinois rice ](<https://www.amourdecuisine.fr/article-mhalbi-constantinois-creme-dessert-au-riz-en-video.html> "Mhalbi constantinois: rice dessert cream video")

If not, maybe you will like [ indian rice cream ](<https://www.amourdecuisine.fr/article-creme-de-riz-indienne.html> "Indian rice cream, Phirni") ???   


**Mahalabiya**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/MAHALLABIYA-1.CR2_.jpg)

portions:  6 to 8  Prep time:  5 mins  cooking:  20 mins  total:  25 mins 

**Ingredients**

  * 500ml Of milk 
  * 4 Tbsp sugar 
  * 2 Tbsp of cornflour 
  * 2 tablespoons of rose water 
  * 1 Tbsp of orange blossom water 

For garnish: 
  * pistachio powder 
  * dry cranberrys 



**Realization steps**

  1. take 3 tablespoons of milk, and dissolve the maizena in it. 
  2. In a saucepan put milk, sugar, cornflour mixture and milk. 
  3. Mix with a wooden spoon over medium heat without stirring until the cream has thickened. 
  4. Add rose water and orange blossom water, stir again and remove from heat. Pour into ramekins and let cool completely before putting them in a cool place. 



[ ![MAHALLABIYA.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/MAHALLABIYA.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/MAHALLABIYA.CR2_.jpg>)
