---
title: apple pie smoothie
date: '2015-12-12'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-tarte-aux-pommes-1.jpg
---
![apple pie smoothie 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-tarte-aux-pommes-1.jpg)

##  apple pie smoothie 

Hello everybody, 

I continue with the saga of my breakfast smoothies, and today it was while sipping my apple pie-flavored smoothie that I started a nice, smooth day. 

Never hesitate to try to put the flavors, spices and fruits that you like the most in your smoothies ... the result is only going to be magnificent. 

For my part, and to be frank, every night before sleep, I do my little research on google, to try to make a tasty smoothie with the ingredients that I have at home. I read the ingredients, and I imagine the taste that will give, and in the morning I take my ingredients and hop in my little favorite jojo (finally the second in the list after my juicer). 

**apple pie smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-tarte-aux-pommes.jpg)

portions:  2  Prep time:  3 mins  total:  3 mins 

**Ingredients**

  * 2 large apples (preferably red) 
  * 1 banana cut in slices and frozen 
  * 1 glass of [ almond milk ](<https://www.amourdecuisine.fr/article-lait-d-amande-maison.html>) (otherwise normal milk) 
  * ½ glass of natural yoghurt 
  * 1 cup cinnamon powder 
  * 1 pinch of grated nutmeg 
  * 1 pinch of ginger 
  * 1 pinch of clove powder 
  * 1 date 



**Realization steps**

  1. place all the ingredients in the blender bowl   
If your apples are organic, it is better to peel them. 
  2. shield until you have a smooth smoothie, (you can add ice cubes to your blinder bowl for a liquefied smoothie) 
  3. Sip immediately. 



![apple pie smoothie 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/smoothie-tarte-aux-pommes-2.jpg)
