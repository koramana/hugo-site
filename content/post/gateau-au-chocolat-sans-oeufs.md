---
title: chocolate cake without eggs
date: '2016-12-14'
categories:
- dessert, crumbles and bars
- Gateaux au chocolat
- gateaux, et cakes
- recettes sucrees
tags:
- To taste
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-au-chocolat-sans-oeuf-010_thumb1.jpg
---
![chocolate cake without egg 010](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-au-chocolat-sans-oeuf-010_thumb1.jpg)

##  chocolate cake without eggs 

Hello everybody, 

here is a very good **chocolate cake without egg** , Yes,  without eggs  , very good, and very mellow, a recipe that I had prepared because I received friends, and one of them was allergic to eggs, I was disappointed the first time I invited her, she could not eat anything, thankfully I had him prepare quickly made a [ Tamina ](<https://www.amourdecuisine.fr/article-tamina-97440692.html>) if not shame, she would take her coffee ... without cake ??? !!! 

so this time I bought a really good book, and I found this recipe in it, which I really like.   


**chocolate cake without eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-au-chocolat-sans-oeuf-004_thumb1.jpg)

**Ingredients**

  * 2 jars of plain yoghurt (the jars will be used as a measure) 
  * 1 and a half jars of sugar 
  * 3 pots of flour 
  * ½ pot of oil 
  * ½ sachet of baking powder 
  * 200 g melted chocolate 

decoration: 
  * 150 gr of white chocolate 
  * 2 tablespoons fresh cream 
  * chocolate in vermicelli 



**Realization steps**

  1. melt the dark chocolate in the bain-marie 
  2. Mix yogurt and sugar, add flour and yeast 
  3. stir in oil and chocolate slowly to get a smooth dough 
  4. pour into a baking pan lined with baking paper. 
  5. Bake for 30 to 35 minutes in a 180 ° oven. 
  6. Test the cooking with the tip of a knife planted in the middle of the cake, which must come out dry. 
  7. let cool a little, then unmold. 
  8. melt the white chocolate on the bain-marie, when it is well melted, add the fresh cream, and mix. 
  9. decorate the cake well cool with this cream of white chocolate. and scatter over the chocolate in vermicelli. 


