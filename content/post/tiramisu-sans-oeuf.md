---
title: tiramisu without egg
date: '2016-03-27'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cheesecakes and tiramisus

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tiramisu-o-choco-004_thumb.jpg
---
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tiramisu-o-choco-004_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tiramisu-o-choco-004.jpg>)

##  tiramisu without egg 

Hello everybody, 

Did I tell you how much I love this Italian dessert, the delicious one? last time I was in a bookstore, I was flipping through a book of **Italian food** , and I flashed on this wonderful dessert, the book was a little expensive, but the recipe very easy with very simple ingredients, so I immediately keep in mind, I could not afford the book, can be next time, hihihiih 

in any case, with the delicious Italian cheese, mascarpone, we add dark chocolate ... .. frankly an irresistible taste, I highly recommend this wonderful recipe. 

for people who can not find mascarpone, here is the recipe of [ homemade mascarpone ](<https://www.amourdecuisine.fr/article-tiramisu-avec-du-mascarpone-fait-maison-55272756.html>) . 

so I share now this very very good recipe with you, and especially if you try it, tell me your opinion. 

**tiramisu without egg**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tiramisu-chocolat-032_thumb.jpg)

Recipe type:  dessert  portions:  4  Prep time:  30 mins  total:  30 mins 

**Ingredients**

  * 300 gr of dark chocolate 
  * 400 gr of mascarpone (trade or house) 
  * 150 grs thick cream 
  * 400 ml of sweet black coffee according to taste 
  * 36 boudoirs, or long biscuit of cats 
  * cocoa powder for decoration 



**Realization steps**

  1. whip the cream, until it becomes a whipped cream, stir in the mascarpone, it's a mix not easy to mix, so go slowly 
  2. Melt the chocolate in a bain-marie, let cool a little, before incorporating it into the mascarpone / whipped cream mixture. 
  3. for the presentation according to your taste, for my part, I put the ganache in a bag, I cut the corner.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/2010-11-30-tiramisu-chocolat_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/2010-11-30-tiramisu-chocolat_thumb.jpg>)
  4. dip the boudoirs in the sweet coffee, not too much until it melts. 
  5. place them in your presentation dish, cover them with a layer of chocolate cream / mascarpone whipped cream. 
  6. cover with another layer of boudoirs soaked in coffee, then another layer of cream, until the cream has run out. 
  7. sprinkle with some cocoa, and place in the fridge for at least 1 hour. 
  8. and enjoy. 



[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/2010-11-30-tiramisu-chocolat1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/2010-11-30-tiramisu-chocolat1_2.jpg>)

I do not tell you the taste, a spoon, after another ... and another, a supreme taste of happiness 

try it and give me your opinion 

Thank you for your feedback 

and thank you to everyone who subscribes to my newsletter 

for more tiramisu recipes you have:   
  
<table>  
<tr>  
<td>

![https://www.amourdecuisine.fr/wp-content/uploads/2013/01/123-059.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/123-059.jpg) 
</td>  
<td>



##  [ Tiramisu with homemade mascarpone ](<https://www.amourdecuisine.fr/article-tiramisu-avec-du-mascarpone-fait-maison-55272756.html> "Tiramisu with homemade mascarpone")


</td> </tr>  
<tr>  
<td>

![https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tira_thumb.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tira_thumb.jpg) 
</td>  
<td>

[ Tiramisu ](<https://www.amourdecuisine.fr/article-tiramisu-vous-ai-je-dit-a-quel-point-je-l-aime--44008438.html>) 
</td> </tr>  
<tr>  
<td>

![https://www.amourdecuisine.fr/wp-content/uploads/2013/01/25516847.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/25516847.jpg) 
</td>  
<td>

[ Tiramisu light ](<https://www.amourdecuisine.fr/article-25345349.html>) 
</td> </tr> </table>
