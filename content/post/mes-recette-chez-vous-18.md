---
title: my recipe at home 18
date: '2012-05-02'
categories:
- recettes patissieres de base

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/P32302831.jpg
---
![2011-09-04 for the moment tout1](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

thank you my dear, for the comments left, after trying one of my recipes, too bad that the vast majority, did not send photos, it does not matter. 

I continue to publish this article, my recipes at home, and it is a huge pleasure to write it every time. 

to send me the link and the photo of your recipe, if you have a blog, or only the photo if you do not have a blog, it's here: [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)

thank you 

![P3230283.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/P32302831.jpg)

![news-recipes 1427](https://www.amourdecuisine.fr//import/http://img.over-blog.com/500x375/4/11/62/97/nouvelles-recettes/)

[ frangipane cake with apricots ](<https://www.amourdecuisine.fr/article-gateau-frangipane-aux-abricots-62539467.html>) in [ the enchanted shrimp ](<http://lacrevetteenchantee.over-blog.com>)

![Egg Free Chocolate Cake](http://artdevivre.typepad.fr/.a/6a0133f5a8e69b970b016304a0ef50970d-800wi)

[ White Cheese Pie ](<https://www.amourdecuisine.fr/article-tarte-au-fromage-blanc-kaskueche-90621607.html>) at Omali (a reader) 

his opinion: 

I love to browse your extraordinary blog! 

and I copy a lot of your wonderful recipes. 

I then go to the kitchen to try them. 

There, I cooked this cheescake of your site. 

I find it very good and my daughter .... liked it a lot. 

I send you the photo well on the pie ... I do not sprinkle with icing sugar, too much sugar otherwise. 

Omali 

[ Basboussa ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html>) at Celeste (a faithful reader and a good friend of Peru) 

thanks to you my friends, and readers 

and if you too have tried one of my recipes, send me the picture on this email: 

[ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)
