---
title: Verrines for aperitifs / beets and shrimps
date: '2016-01-18'
categories:
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/recettes-de-verrines-sal-es.CR2_thumb1.jpg
---
![recipes of salty verrines.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/recettes-de-verrines-sal-es.CR2_thumb1.jpg)

##  Verrines for aperitifs / beets and shrimps 

Hello everybody, 

Here is a recipe of salty verrines for aperitifs very easy to prepare, very simple, that I had to prepare in another way, but for lack of ingredients, I went to a recipe of any simplicity, I still pass you , the ingredients needed for the recipe, and my small modifications. 

**Verrines for aperitifs / beets and shrimps**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/recettes-de-verrines-pour-aperitifs.CR2_thumb1.jpg)

portions:  8  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 2 precooked beets. 
  * 8 shrimp, shelled, precooked. 
  * the juice of an orange 
  * 30 ml juice of 1 passion fruit (I replaced both with the juice of a lemon) 
  * 2 and ½ tablespoons of olive oil 
  * 4 shallots (the white part only) 
  * chives, or dill, or parsley according to taste 
  * salt and black pepper 



**Realization steps**

  1. prepare seasoning sauce, juice + salt + black pepper 
  2. cut the beets in small dice. and season with the ¾ of the prepared sauce. 
  3. cool for 30 minutes 
  4. cut the shallots into small slices. 
  5. for the training of verrines, place 2 tablespoons of beets ... 
  6. decorate with a little shallot, then a shrimp, and add a little sauce over it. 
  7. decorate in the end with a little dill, chives or parsley. 



and for other dirty verrines, you have: 

[ ![appetizer verrines humous carrots 1](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/verrine-humous-carottes-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-verrines-houmous-tomates-concombre-carottes-95211639.html>)
