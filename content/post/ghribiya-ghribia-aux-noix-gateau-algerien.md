---
title: ghribiya ghribia with nuts, algerian cake
date: '2012-08-07'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles and bars
- Gateaux au chocolat
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghriba-aux-noix_thumb1.jpg
---
##  ghribiya ghribia with nuts, algerian cake 

Hello everybody, 

At the approach of l'aid el fitr 2012, I share with you this delicious recipe of ghribiya, a succulent [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) certainly not at all a diet cake, but a piece, will not do too much harm, but do not go to abuse and say that soulef told us, it will not hurt, because with this ghriba, we do not do not really stop in one piece, so it's fondant to wish, I made this cake late at night, so I could not do a lot of pictures for the steps, something that my readers love a lot, but I'm sure it's one of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) the easiest to do. 

this [ Algerian cake ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) is similar to the Montecaos cake, and the English short bread cake, cake with smen that is used a lot in the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>)

the ingredients for almost 60 pieces:   


**ghribiya, walnut ghribia, algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghriba-aux-noix_thumb1.jpg)

**Ingredients**

  * 370 gr of smen, ghee or clarified butter 
  * 150 ml of oil 
  * 150 gr of icing sugar 
  * a few drops of almond extract 
  * a coffee of vanilla 
  * 100g of nuts in powder 
  * flour (+ or - 1 kilo, depending on the absorption of the flour) 
  * half a walnut kernel (optional just for decoration) 



**Realization steps**

  1. in a large salad bowl, or bowl whip the smen and sugar 
  2. when the mixture smen and sugar is well creamy, introduce the oil 
  3. beat well, then add vanilla, almond extract and nuts   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghribia-montecaos_2.jpg)
  4. using a spatula add the flour, until you have a soft dough, not too sticky by hand and well manageable 
  5. form rolls 1.5 cm high and 2 cm wide 
  6. put the fork over to decorate the pudding 
  7. cut diamonds 3 cm long 
  8. place in a baking tray 
  9. cook in a preheated oven at 180 degrees, for 15 min maximum, these cakes cook quickly, so watch carefully 
  10. remove as soon as it has changed color from cream to white 
  11. let cool, before putting in an airtight box. 



![ghribia bel jouz constantinois cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/ghribia-bel-jouz-gateau-constantinois_thumb1.jpg)

thank you very much for your comments and your visits. 

if you like my blog, do not forget to put a "like" on facebook, or if you have a gmail account, to put "+1" 

bisous. 
