---
title: couscous with milk and vegetables
date: '2012-11-25'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-au-lait-aux-legumes-2_thumb1.jpg
---
![couscous with milk with vegetables 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-au-lait-aux-legumes-2_thumb1.jpg)

##  couscous with milk and vegetables 

Hello everybody, 

A super delicious **couscous with milk and vegetables** that personally, I like a lot, and I do it often and well varied. 

Today it's couscous with vegetables: zucchini, turnips, carrots, and a nice handful of chickpeas, so a couscous well perfumed and rich in taste and with this milk sauce, I do not tell you how much this couscous is good, too good!   


**couscous with milk and vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-au-lait-et-legumes-1_thumb1.jpg)

**Ingredients**

  * [ Couscous steamed ](<https://www.amourdecuisine.fr/article-comment-cuire-le-couscous-111575933.html>)
  * 1 onion 
  * 1 zucchini (or according to your taste, I put one, because it is the long zucchini that I have here) 
  * 2 zucchini 
  * 2 turnips 
  * 1 handful of chickpeas 
  * salt pepper 
  * milk (depending on the number of people, between ½ liter and 1 liter) 



**Realization steps**

  1. clean your vegetables and cut them in 2 in length, 
  2. in the bottom of the couscoussier, put the carrots, and the turnip to cook in salt water. and made cooking steamed couscous 
  3. if the chickpeas are not in a box, but just soaked the day before, add them at this stage. 
  4. after the first evaporation of the couscous, add the zucchini, and make the second evaporation of the couscous. 
  5. now that the vegetables are tender and well cooked, reduce the water if there is a lot left, just leave the equivalent of 1/2 green water. 
  6. add milk, if it lacks salt, add in, as well as black pepper. 
  7. let the milk boil with the vegetables, and remove from the heat. 
  8. here is your sauce is ready. 
  9. present the couscous well water with this sauce, it does not look like couscous with red sauce, you can float with milk sauce .... It's a delight. 



merci. 
