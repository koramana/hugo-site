---
title: special recipes ramadan 2018 boureks bricks cold entrances
date: '2017-05-14'
categories:
- Bourek, brick, samoussa, chaussons

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gozleme-crepes-turques-a-la-viande-hach%C3%A9e.jpg
---
![gozleme turkish crepes with minced meat, special recipes ramadan 2018 boureks bricks cold starters](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gozleme-crepes-turques-a-la-viande-hach%C3%A9e.jpg)

##  special recipes ramadan 2018 boureks bricks cold entrances 

Hello everybody, 

here is a nice selection of bourek recipes, some ideas for a varied ramadan menu. The month is long, and the recipes that will vary well your menu every day, are well developed on my blog. And so that all these recipes of Ramadan, are well accessible for you, I make indexes and menus. 

Know that these tables are still not complete, I update, as and when, thank you. 

It's not just the boureks, a little lower, I've also put you full of recipes that can replace the bricks and boureks 

You can see this list: 

  * [ Tunisian brick with egg ](<https://www.amourdecuisine.fr/article-brick-tunisienne-a-loeuf.html> "Permalinks to Tunisian brick with egg")

  * [ samboussek with minced meat ](<https://www.amourdecuisine.fr/article-samboussek-a-viande-hachee.html> "Permalinks to samboussek with minced meat")

  * [ chicken tacos with Algerian sauce ](<https://www.amourdecuisine.fr/article-tacos-poulet-a-sauce-algerienne.html> "Permalinks to chicken tacos with Algerian sauce")

  * [ turkish borek with spinach and feta spiral cheese ](<https://www.amourdecuisine.fr/article-borek-turque-aux-epinards-fromage-feta-spirale.html> "Permalinks to Turkish Borek with Spinach and Feta Cheese Spiral")

  * [ buns with tuna and chakchouka ](<https://www.amourdecuisine.fr/article-petits-pains-au-thon-et-chakchouka.html> "Permalinks to rolls with tuna and chakchouka")


  
<table>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/fleurs-a-la-creme-d-artichaut_thumb-272x125.jpg) [ **Fried flowers stuffed with artichoke cream** ](<https://www.amourdecuisine.fr/article-fleurs-frites-farcies-a-la-creme-d-artichauts-102277133.html>) 
</td>  
<td>

[ **![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/brick-de-poulet-au-curry-recette-de-ramadan-2013.CR2_-272x125.jpg) Chicken Brick with Curry ** ](<https://www.amourdecuisine.fr/article-brick-de-poulet-au-curry-118763982.html>) 
</td>  
<td>

**[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bricks-au-thon-009.CR2_-272x125.jpg) Tuna bricks ](<https://www.amourdecuisine.fr/article-bricks-au-thon-recette-facile-et-rapide-117943659.html>) ** 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/brick-a-la-ricotta-016.CR2-copie-1-272x125.jpg) [ **Bricks with ricotta** ](<https://www.amourdecuisine.fr/article-recette-de-ramadan-brick-a-la-ricotta-118717423.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bourak_thumb-272x125.jpg) [ **Bourak with tuna and cheese** ](<https://www.amourdecuisine.fr/article-bourak-au-thon-et-fromage-80735244.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/samossas-de-crepes-panees_thumb-272x125.jpg) [ **samosas of breaded pancakes** ](<https://www.amourdecuisine.fr/article-samossas-de-crepes-panes-114970535.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame3-272x125.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/06/briks-au-crevettes-a-la-bechame3.jpg>) [ shrimp and bechamel brigs ](<https://www.amourdecuisine.fr/article-bricks-aux-crevettes-a-la-bechamel.html> "shrimp brigs with bechamel") 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-aux-crevettes-pommes-de-terre--272x125.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2009/08/boureks-aux-crevettes-pommes-de-terre-.jpg>) [ shrimp and potato boureks ](<https://www.amourdecuisine.fr/article-boureks-aux-crevettes-et-pommes-de-terre.html> "shrimp and potato boureks") 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourak-laadjine-039.CR2_-272x125.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourak-laadjine-039.CR2_.jpg>) [ bourek laadjine ](<https://www.amourdecuisine.fr/article-bourak-laadjine-empanadas-a-la-viande-hachee.html> "Bourak laadjine / Empanadas with chopped meat") 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/samoussa-indiennes-046_thumb-272x125.jpg) [ **indian samoussa** ](<https://www.amourdecuisine.fr/article-samoussa-indienne-97307661.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bourak-aux-epinards-et-au-thon-1_thumb-272x125.jpg) [ **Boureks with spinach and tuna** ](<https://www.amourdecuisine.fr/article-boureks-aux-epinards-et-thon-107386648.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samoussa-a-la-viande-hachee_thumb-272x125.jpg) [ **Samoussa with Ground Meat** ](<https://www.amourdecuisine.fr/article-samoussa-a-la-viande-hachee-samoussas-88499027.html>) 
</td> </tr>  
<tr>  
<td>

[ **![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/briouates-a-la-viande-hachee.CR2_-272x125.jpg) bricks with chopped meat ** ](<https://www.amourdecuisine.fr/article-briouates-a-la-viande-hachee.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-de-saumon-fume.CR2_thumb-272x125.jpg) brigs with smoked salmon ](<https://www.amourdecuisine.fr/article-amuse-bouche-au-saumon-fume-et-feuilles-de-bricks-113825045.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/21715100-272x125.jpg) [ **Sambousiks with tuna (Lebanese recipe)** ](<https://www.amourdecuisine.fr/article-25345456.html>) 
</td> </tr>  
<tr>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/crepes-aux-champignons-et-dinde-2_thumb-272x125.jpg) ** [ mushroom and turkey crepes ](<https://www.amourdecuisine.fr/article-crepes-aux-champignons-dinde.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/samossas-de-crepes-panees_thumb-272x125.jpg) samosas with breaded crepes ](<https://www.amourdecuisine.fr/article-samossas-de-crepes-panees.html>) 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/Bourek-annabi-2-272x125.jpg) [ bourek annabi ](<https://www.amourdecuisine.fr/article-bourek-annabi-carres-viande-hachee-et-oeuf.html>) 
</td> </tr>  
<tr>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/bourak-au-poulet-a-la-bechamel_2-272x125.jpg) [ **Bourek chicken with béchamel** ](<https://www.amourdecuisine.fr/article-bourek-au-poulet-a-la-bechamel-107496092.html>) 
</td>  
<td>

[ **![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/menu-044_thumb-272x125.jpg) cheese bourak ** ](<https://www.amourdecuisine.fr/article-55595087.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/brick-aux-pommes-de-terre-et-thon-entree-de-ramadan.CR2_1-272x125.jpg) [ potato and tuna bricks ](<https://www.amourdecuisine.fr/article-bricks-aux-pommes-de-terre-et-thon-entree-du-ramadan-119072877.html>) ** 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/chaussons-aux-poireaux-et-au-saumon-3-272x125.jpg) slippers with leeks and salmon ](<https://www.amourdecuisine.fr/article-chaussons-aux-poireaux-et-au-saumon.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/fricass%C3%A9-tunisien-272x125.jpg) ** [ Tunisian fricassee ](<https://www.amourdecuisine.fr/article-fricasse-tunisien.html>) 
</td>  
<td>

**![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/batbouts-farcis-au-thon-recettes-de-ramadan-2014.CR2_-272x125.jpg) ** [ tuna batbouts ](<https://www.amourdecuisine.fr/article-batbouts-farcis-au-thon.html>) 
</td> </tr>  
<tr>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourek-pour-ramadan-a-la-pomme-de-terre-bourek-batata.CR2_1-272x125.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/bourek-pour-ramadan-a-la-pomme-de-terre-bourek-batata.CR2_1.jpg>) [ boureks potatoes and cheese ](<https://www.amourdecuisine.fr/article-bourek-aux-pommes-de-terre-et-fromage-entree-pour-ramadan.html>) 
</td>  
<td>

[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/malsouka-024_thumb-272x125.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/malsouka-024_thumb.jpg>) [ tajine malsouka ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks.html> "Tunisian cuisine: Tunisian Malsouka-tajine") 
</td>  
<td>

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/la-coca-1-272x125.jpg) [ Algerian Coca ](<https://www.amourdecuisine.fr/article-coca-chaussons-cuisine-algerienne.html>) 
</td> </tr> </table>

###  Cold entries for Ramadan 2018 

**special recipes ramadan 2018 boureks bricks cold entrances**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/brick-a-la-ricotta-016.CR2-copie-1.jpg)

portions:  4-6  Prep time:  20 mins  cooking:  15 mins  total:  35 mins  **Ingredients**

  * bourek 
  * briks 
  * pizza 
  * quiche 
  * salty Tart 
  * donut 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
