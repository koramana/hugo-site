---
title: cake without strawberry baking
date: '2017-04-14'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Algerian cakes
- Strawberry tart
- pies

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateaux-sans-cuisson-aux-fraises-1.jpg
---
[ ![cakes without baking with strawberries 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateaux-sans-cuisson-aux-fraises-1.jpg) ](<https://www.amourdecuisine.fr/article-gateau-sans-cuisson-aux-fraises.html/gateaux-sans-cuisson-aux-fraises-1>)

##  cake without strawberry baking 

Hello everybody, 

Again with the strawberry recipes, and this time it's a strawberry-free cake, you'll need butter cookies or whatever you want, which you'll cover with a cream cheese and a nice layer strawberries. 

Personally I am tempted by this recipe of Lunetoiles, I will immediately buy cheese, and make this dessert for this evening. and you are what you are tempted. 

**cake without strawberry baking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateaux-sans-cuisson-aux-fraises-3.jpg)

portions:  12  Prep time:  20 mins  total:  20 mins 

**Ingredients** Strawberries in syrup: 

  * 1 tray of strawberries (about 500g) 
  * 50 grams of sugar 

For the cheese filling: 
  * 250 grams of cream cheese 
  * 50 grams of sugar 
  * 1 teaspoon of puree or vanilla extract 
  * 2 bricks (500 ml) whole cream with 35% fat content (placed at least 24 hours before making the recipe) 
  * 4 tablespoons powdered sugar (40 grams) 

Decorate (optional) 
  * rinsed raisins (you can use strawberries or whole slices, depending on your preferences) 
  * cocoa 



**Realization steps** Prepare the strawberries 

  1. Wash and cut strawberries, cut into thin slices and place in a large bowl. 
  2. Sprinkle with sugar and mix gently so that the strawberries fit well into the sugar. 
  3. Set aside and stir from time to time. 

Prepare the cheese stuffing 
  1. Mix the cheese with the sugar, add the vanilla extract and mix well. 
  2. In a large bowl, whip the whole cream very cold with the powdered sugar until firm. 
  3. Stir in ⅓ cream cheese mixture and stir in remaining mixture until smooth. 

Cake assembly - 
  1. In the bottom of the mold evenly place the cookies (If the cookies do not penetrate in their entirety, you can break them to fit the shape). 
  2. Put a layer of cheese mixture.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateau-sans-cuisson-ahux-fraises.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41827>)
  3. Sprinkle with half the slices of strawberries and the remaining juice at the bottom of the bowl. 
  4. Gently push the berries into the cream that will create an even layer as possible. 
  5. Cover with another layer of cookies. 
  6. Put a layer of the cheese mixture and the remaining half strawberries and juice. 
  7. Cover again with another layer of cookies and add remaining cheese mixture. 
  8. Cover the dish with plastic wrap and send to cool for at least a whole night. 
  9. Before serving the cake it can be frozen for ½ hour - one hour to allow easy extraction of its mold. 
  10. Decorate by sprinkling with cocoa and arrange the whole strawberries or slices of strawberries and serve. 
  11. The cake is kept in the refrigerator for up to a week. 



[ ![cakes without strawberry cooking 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gateaux-sans-cuisson-aux-fraises-2.jpg) ](<https://www.amourdecuisine.fr/article-gateau-sans-cuisson-aux-fraises.html/gateaux-sans-cuisson-aux-fraises-2>)
