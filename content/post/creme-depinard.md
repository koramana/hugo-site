---
title: Cream of spinach
date: '2013-03-24'
categories:
- Buns and pastries
- Mina el forn

---
Hello everybody, 

It's not easy to make everyone eat spinach, and yet according to Popeye the sailor, well, spinach is good for the muscles, hihihihihi 

but in truth, spinach is good for the fluidity of blood, and it improves blood in certain areas of the brain, spinach is one of the best known sources of vitamin B9 or folic acid. 

now if we come back to the legend of the fact that it is a great source of iron, read this: 

Unlike a stubborn legend (illustrated in the adventures of Popeye), spinach is not the best source of dietary iron. This legend was born from the typo of an American secretary. This error, which multiplied by ten the level of food iron contained in the plant, went unnoticed for decades. Meat and pulses are much better than spinach. 

In addition, spinach iron is much better absorbed by the body when it is accompanied by a source of vitamin C, for example with lemon juice. (source: Wikipedia) 

Well, I'll admit it, this is the first time I've read this, hihihiihih, poor Popeye with all those boxes of spinach he ate to save olive, hihihiihih 

so to the recipe of this good cream, that my children did not suspect at all the taste of the spinach, and that they drank in their bowls without grumbling. 

#  Spinach soup, spinach soup 

ingredients: 

  * 5 gr of butter 
  * 150 g of onions 
  * 1 teaspoon of salt (but you have to control) 
  * 1 cup of pepper 
  * 1 teaspoon of nutmeg 
  * 1 cube of chicken stock underneath in 500 ml of water (if the broth is salty, reduce the salt, because even the spinach tends to be salty) 
  * 300 ml of fresh cream 
  * 500 g fresh spinach 


  1. Heat the butter in a saucepan over the heat and fry the onions until it becomes tender 
  2. add salt, pepper and grated nutmeg 
  3. Add the chicken broth to the mixture while stirring 
  4. Add the cream, stirring for one minute 
  5. Add spinach, cook 5 min or 10 
  6. blender the mixture and then leave on low heat until boiling for another two minutes 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter. 
