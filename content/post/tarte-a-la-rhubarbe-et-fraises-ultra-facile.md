---
title: rhubarb pie and strawberries ultra easy
date: '2013-06-07'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-a-la-rhubarbe-et-fraises-ultra-facile-copie-11.jpg
---
![rhubarb pie and strawberries ultra easy](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-a-la-rhubarbe-et-fraises-ultra-facile-copie-11.jpg)

##  rhubarb pie and strawberries ultra easy 

Hello everybody, 

This is the time to enjoy the rhubarb and strawberry season, to make delicious cakes, pies, desserts, cakes and more. 

And for an ultra easy dessert and ultra fondant, here is a beautiful, crispy rhubarb pie that hides in its heart a tart fondant of these two fruits, rhubarb and strawberry. 

This ultra-easy strawberry rhubarb pie is one of Lunetoiles' creations, which I thank very much, and which does not stop to amaze me with its beautiful cuisine ... 

![rhubarb pie and strawberries ultra easy](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-a-la-thubarbe-ultra-facile-copie-11.jpg)

**rhubarb pie and strawberries ultra easy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-a-la-rhubarbe-et-aux-fraises1.jpg)

portions:  8  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients** Marinated rhubarb: 

  * 800 g rhubarb 
  * 2 vanilla pods 
  * 60 g of sugar 

Dough : 
  * 150 g of butter 
  * 80 g of sugar 
  * 1 egg 
  * 1 pinch of salt 
  * 250 grams of flour 
  * 7 g (1/2 sachet) of baking powder 

Crumble: 
  * 150 g of soft butter 
  * 100 g of sugar 
  * 150 g flour 
  * 150 g of almond powder 
  * As well as 300 g of strawberries (for garnish + decoration) 



**Realization steps**

  1. Preparation 
  2. The day before cooking, prepare the marinated rhubarb: Clean the stems and cut into pieces add the grains of the scraped vanilla beans and the sugar. 
  3. Let stand overnight cool. 
  4. On the second day, prepare the dough by assembling all ingredients by hand or robot. 
  5. Spread and garnish a pie pan with a diameter of 28 cm. 
  6. Put the pan in the fridge while waiting to prepare the filling. 
  7. Wash, slice and cut the strawberries in half. 
  8. Drain and boil the marinated rhubarb until all the juice is evaporated and slightly thickened. 
  9. Garnish the bottom of the dough with the rhubarb, then add the strawberries on top. 
  10. Prepare the crumble: Mix all the ingredients by rubbing between the fingers. 
  11. Sprinkle the mixture on the rhubarb / strawberry filling. 
  12. Bake for 60 minutes at 170 ° C in rotary heat. 
  13. Cool completely. Put in the fridge. 
  14. You can serve with whipped cream and decorate with strawberries. 



![pie-in-the-rhubarb ultra-facile.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-a-la-rhubarbe-ultra-facile1.jpg)
