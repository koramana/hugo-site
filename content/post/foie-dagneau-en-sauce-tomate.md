---
title: lamb liver in tomato sauce
date: '2011-11-03'
categories:
- gateaux, et cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/foie-de-mouton-en-sauce-010_thumb1.jpg
---
##  lamb liver in tomato sauce 

Hello everybody 

Sometimes, it is difficult to cook lamb liver in sauce, but here is a recipe that will completely change your opinion on the liver of the sheep, a recipe of sheep liver tomato sauce super good and especially tender you go adopt it imediately. 

I still leave you another way to prepare the lamb liver, the [ kebda mchermla, or lamb liver with chermoula ](<https://www.amourdecuisine.fr/article-recette-kebda-mchermla.html>) , a delight. 

in any case with the recipe of the liver in tomato sauce, we feasted well, me and my husband, so long ago, we had not eaten. It's a very simple and easy recipe, and I'm sure everyone knows how to do it, but that's fine, I'll share the recipe with you too:   


**lamb liver in tomato sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/foie-de-mouton-en-sauce-010_thumb1.jpg)

**Ingredients**

  * 300g of lamb liver. 
  * 2 cloves garlic crushed. 
  * a few sprigs of coriander. 
  * salt, black pepper, coriander powder 
  * 2 tablespoons olive oil. 
  * 1 teaspoon of cumin. 
  * 1 can of tomato in piece 
  * 1 teaspoon of tomato paste. 



**Realization steps**

  1. clean the lamb liver and cut it in. 
  2. In a frying pan with a thick bottom, fry the liver in hot oil for 2 minutes, the principle is to skip the liver and not cook it. 
  3. remove the liver from the cooking oil. 
  4. place in this oil the tomato, add the spices, salt and pepper. and add the tomato paste, the crushed garlic and the chopped coriander. cook until you have a thin layer of oil on the surface. 
  5. return the sauteed liver, add 50 ml of water and cook for at least 15 minutes. 
  6. rectify the seasoning if necessary. 
  7. Serve in a dish and sprinkle with chopped coriander. 


