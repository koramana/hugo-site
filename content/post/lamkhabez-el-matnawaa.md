---
title: Lamkhabez el Matnawaa
date: '2010-09-26'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- Gateaux Secs algeriens, petits fours
- sweet recipes

---
M'khabez Varies, لمخبز المتنوع and here is another recipe of lunetoiles that I tasted with pleasure, following her beautiful package that she had sent me, a recipe that I will share with you saying "gouty and approved" hihihihihihi Ingredients: 500 g finely ground almonds 300 g sieved icing sugar 2 tbsp. to c. lemon peel 1 tbsp. to c. vanilla extract 2 whole eggs 0+ 1 yellow according to size Glazing: 3 egg whites 3 tbsp. to s. lemon juice 3 tbsp. to s. of orange blossom water sifted icing sugar 2 tbsp. 

##  Overview of tests 

####  please vote 

**User Rating:** 3.2  (  2  ratings)  0 

Love Varies, لمخبز المتنوع 

and here is another recipe of lunetoiles that I tasted with pleasure, following her beautiful package that she had sent me, a recipe that I will share with you saying "drip and approved" hihihihihihi 

**ingredients** :   
500 g finely ground almonds   
300 g sifted icing sugar   
2 tbsp. to c. of lemon zest   
1 C. to c. of vanilla extract   
2 whole eggs 0+ 1 yellow by size 

**Icing** :   
3 egg whites   
3 c. to s. of lemon juice   
3 c. to s. of orange blossom water   
sifted icing sugar   
2 tbsp. to s. of table oil   
blue and green dye / sweet almond flavor   
pink dye / strawberry aroma   
purple dye / blackcurrant flavor 

**Preparation** :   
Mix the almonds, the icing sugar, the zest of   
lemon, vanilla extract and wet with the eggs to get a   
manageable and firm dough.   
On a floured work plan lower the dough on a   
thickness of 3 cm and cut rounds of 4 cm in diameter using a   
cutter.   
Place on a greased and floured plate, cook for 20 to 30 minutes at 140 °.   
Cool on a baking rack. 

**Preparation of icing** : 

Prepare the icing with the given ingredients, add the oil last and mix. Proceed with coloring by adapting to each color its aroma.   
Check the icing by testing a cake. 

Ice the cakes. Let them dry completely, remove the excess frosting.   
Pass the silvery food gloss on the surface using a food sponge. Decorate the pieces by adapting to each color its decoration.   
Use perforated freezer bags with a toothpick in a corner, to make dots, festoons, branches, etc ....   
Garnish the center of each cakes with a white daisy, a bouquet of three daisies degraded in pink and white, a flower by raising the petals, small roses and green leaves made with green icing. 

thank you to lunetoiles, and thank you for all your comments, do not forget to subscribe to my newsletter if you want to be with any update articles and recipes of my blog. 

bisous 
