---
title: Macaroons with white chocolate ganache
date: '2012-12-03'
categories:
- dessert, crumbles et barres
- cakes and cakes
- recettes sucrees
tags:
- macarons
- ganache
- Chocolate
- Confectionery
- desserts
- Christmas

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/macarons-037-a_thumb1.jpg
---
[ ![macaroons with white chocolate ganache](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/macarons-037-a_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/macarons-037-a_thumb1.jpg>)

##  Macaroons with white chocolate ganache 

Hello everybody, 

I do not know if you know my story with macaroons, but I do not tell you, missed after failure, fortunately it was a failure in the form and not in the taste. 

then the little story today, you'll laugh well, every time I made macaroons, my daughter was always there, to watch me, and as the macaroons came out of the oven all plapla pla .... I put them on a plate, which I left within reach of my children, rather of my daughter. 

so today, she was there, she was following me step by step, and at the exit of the oven, she was waiting for the plate of course, but because I finally got them, I put them on a rack to cool well and I prepared the ganache, and my daughter waited wisely, she followed me here and the ... 

I did not understand at first, but the poor, she waited for her plate, she followed me, I put the ganache in the fridge, I withdraw, I whip, she always follows me, she looks at me put the ganache to garnish the macaroons, wisely always, then, take my camera, try to put all the possible landscapes to have beautiful pictures, then she finally says: "and now, can I have my cake", "I can have the cake now" , poor thing, she was waiting, and I had not even paid attention ... nasty mom that I am, huuuuuuuuuuuun 

so the recipe is not an invention is the recipe for macaroons found in all blogs .... 

so I pass you the recipe: 

**Macaroons with white chocolate ganache**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/macarons_thumb1.jpg)

portions:  30  Prep time:  40 mins  cooking:  10 mins  total:  50 mins 

**Ingredients**

  * 3 egg whites 
  * 80 g caster sugar 
  * 125 g of almond powder 
  * 170 g icing sugar 
  * dye according to taste 
  * [ white chocolate ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat-blanc.html> "white chocolate ganache")



**Realization steps**

  1. preparation of macaroons: 
  2. reduce the almond and the icing sugar, sift the mixture well and if there is a little almond that does not pass through the sieve, pass the mixture to the electric mill, until it is really fine 
  3. Beat the whites until they are sparkling 
  4. add the caster sugar in 3 times, and the food coloring 
  5. beat another 2 minutes. 
  6. Once they are mounted, add the mixture of icing sugar and almond powder 
  7. Macaroon (raise the mass and scrape the edges) until the device makes a '' ribbon '' 
  8. Make small piles of 2-3 cm in the piping bag and on parchment paper. 
  9. Let crumble for 30 minutes then put in oven at 170 ° C, 5 minutes door closed, then 6 minutes door open. 
  10. at the exit of the oven, pass a little water under the greaseproof paper to take off easily the hulls of macaroons. 



place [ the ganache ](<https://www.amourdecuisine.fr/article-ganache-au-chocolat-blanc-pour-macarons-113098300.html>) in a pastry bag and garnish the shells with macaroons 

I am super happy that finally I was able to succeed the macarons well as it should, and especially that finally the children were entitled to have macaroons well stocked 

thank you for your visits 

et merci pour vos commentaires 
