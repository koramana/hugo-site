---
title: Cake with Pierre Hermé orange
date: '2014-11-10'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-a-l-orange1.jpg
---
![Cake with Pierre Hermé orange](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-a-l-orange1.jpg)

##  Cake with Pierre Hermé orange 

Hello everybody, 

Cake with Pierre Hermé orange: Since my oven makes me head, and does not want to cook the cakes cleanly, or it is burned down, and not cooked inside, or it's cooked and rapla-pla, I'm just watching these delicious cakes on your blogs and my books, to taste that in my head ... Fortunately I have my dear Lunetoiles, who does not deprive me of these sublime achievements, and who shares them with me, before anyone else, and it gives me a lot of pleasure .... 

When it came to Pierre Hermé's orange cake, it was in my archives since January 2013, and every time Lunetoiles sent me a recipe, the poor cake was degraded even further in my archives, until to be forgotten ... 

It's never too late to do well, I went to search my archives to publish it, hoping that you will be many to realize it, especially since it's in season, we take the beautiful oranges, ok 

And do not forget, if you do, or if you made any other recipe from my blog, send me the picture on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

PS: this email is not to ask questions, or leave your opinion on a recipe, I prefer that you leave me a comment here on the blog, this is how you will live this blog and let others enjoy the answer I give. 

thank you 

![Cake with Pierre Hermé orange](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-de-pierre-herme1.jpg)

**Cake of Pierre Hermé has orange**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-a-l-orange1.jpg)

Recipe type:  cake dessert  portions:  16  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients** for 2 cakes: 

  * 400 g of sugar 
  * 375 g of flour 
  * 190 g whole cream liquid 30% mg 
  * 135 g melted butter but not hot 
  * 6 eggs 
  * 3 orange (zests taken with a thrifty) 
  * 1 sachet of baking powder 
  * 1 pinch of salt 

Syrup: 
  * juice taken from the 3 oranges 
  * 80 g of sugar 



**Realization steps**

  1. Preheat the oven to 160 ° C. 
  2. Butter and flour 2 cake molds. 
  3. Chop the zests of the oranges taken with the thrifty in a robot. Add the sugar and continue mixing until the sugar is moist and granular. 
  4. Pour the orange sugar into a bowl. 
  5. Add the eggs and beat with a whisk until it turns white. 
  6. Add cream and salt. 
  7. Whip until all are well incorporated. 
  8. Pour the flour mixed with the yeast in three times while continuing to whip. 
  9. Finally, incorporate the melted but not hot butter in two or three times. 
  10. Immediately pour this paste into the cake molds. 
  11. Bake 60 minutes. 
  12. If the cakes turn brown too quickly, cover them with aluminum foil. 
  13. Syrup: heat the juice of the oranges with the sugar and once the melted sugar and the syrup has thickened, pour on the cakes (just out of the oven) drilled holes with a toothpick 



![Cake with Pierre Hermé orange](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cake-de-Pierre-Herme-a-l-orange1.jpg)
