---
title: ramadan moubarek رمضان مبارك
date: '2018-05-16'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/ramadan-karim.jpg
---
[ ![ramadan karim](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/ramadan-karim.jpg) ](<https://www.amourdecuisine.fr/article-ramadan-moubarek-%d8%b1%d9%85%d8%b6%d8%a7%d9%86-%d9%85%d8%a8%d8%a7%d8%b1%d9%83.html/ramadan-karim-2>)

Salam alaykum, السلام عليكم 

Here we are again together to welcome the holy month of Ramadan for this year 2018. We implore Allah to accept our fasting, our prayers and that He fills us with his Clemency and Mercy during this blessed month of Ramadan . 

[ ![Date-beginning-Ramadan-country](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/date-debut-ramadan-pays.png) ](<https://www.amourdecuisine.fr/article-ramadan-moubarek-%d8%b1%d9%85%d8%b6%d8%a7%d9%86-%d9%85%d8%a8%d8%a7%d8%b1%d9%83.html/date-debut-ramadan-pays>)

May Allah accept our fast and our prayers. May he guide us to goodness and save us harm. 

Ya Allah ya Karim hear our douas, help our brothers and sisters who suffer in these wars, may you offer them peace. 

Ya Allah is 3Ali, bless us with your mercy and grant us your forgiveness and forgive our sins. 

Ya Allah ya Rezakk, show us your light, guide us to the right path, facilitate us the good and destine us Paradise. 

[ ![dou3a ramadan](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/dou3a-ramadan.jpg) ](<https://www.amourdecuisine.fr/article-ramadan-moubarek-%d8%b1%d9%85%d8%b6%d8%a7%d9%86-%d9%85%d8%a8%d8%a7%d8%b1%d9%83.html/dou3a-ramadan>)
