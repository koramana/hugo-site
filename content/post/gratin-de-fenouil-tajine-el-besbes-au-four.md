---
title: 'fennel gratin: tagine and baked besbes'
date: '2017-10-10'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tajine-el-besbes-au-four-gratin-de-fenouil.CR2_thumb.jpg
---
##  fennel gratin: tagine and baked besbes 

Hello everybody, 

it's the fennel season, and I enjoy it thoroughly, usually it's always in salad, but I left for a gratin of fennel, or tajine el besbas, a recipe very easy to make, and super delicious , especially with this tasty sweet taste of melting fennel. 

**fennel gratin / tajine and baked besbes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/tajine-el-besbes-au-four-gratin-de-fenouil.CR2_thumb.jpg)

Recipe type:  gratin dish  portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * chicken legs or any other part 
  * 3 fennels 
  * 1 onions 
  * a handful of chickpeas 
  * 3 eggs, 
  * parsley 
  * ½ glass of milk, or then fresh cream 
  * Salt, black pepper and cinnamon powder 
  * oil 
  * grated cheese. 



**Realization steps**

  1. in a pot, fry the grated onion with a little oil. 
  2. leave to brown until it becomes translucent. 
  3. add the chick pieces, and the spices, let it sweat again 
  4. add water to cover the chicken, and cook until it becomes tender 
  5. cut the fennel in four, and put in the water to remove all dirt. 
  6. in a gratin dish, place the pieces of fennel, the chicken and cover with the sauce, add the chickpeas.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gratin-de-fenouil_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gratin-de-fenouil_thumb.jpg>)
  7. cover with aluminum, and bake for about 40 minutes at 180 degrees, until the fennel becomes tender. 
  8. in a bowl, mix eggs, milk, chopped parsley, salt, black pepper and cinnamon. 
  9. pour this mixture over the fennel and the chicken, garnish with grated cheese, and put back in the oven to brown. 
  10. enjoy hot food with a good piece of homemade bread. 


