---
title: Kitchen love recipes tested and approved 47
date: '2014-12-01'
categories:
- the tests of my readers
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

Desolate if I linger every time to post this article my recipes at home, so I get a lot of message, here in comments on my blog, or in private message on facebook, sometimes on my mailbox, that I have you put on the page **[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") . **

So, I get almost every day between 10 to 15 messages from my readers, and so many times, I'm super busy, I do not put the photo immediately, on this article and I lose it with the time. So, I apologize to the people who sent me their photo essay, because I lost them, trying to empty my mailbox. 

So, I am speaking to you my dear readers, send me the photo with the link of the recipe that you tried (I speak the link from my blog) on ​​this email address: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

If you are a blogger, it will be nice to leave the link to your blog too, and if you are not a blogger, your name will be good enough. Also if you have tested and approved a recipe from my blog, and you have no photos, just leave a comment in the recipe in question. your opinion will surely help people who want to try the recipe. 

![](http://1.bp.blogspot.com/-K-wkr4aqKzc/VCHMzIK5V8I/AAAAAAAAAao/dgRU2YKTNgs/s1600/xFlan%2Bcheesecake%2B1.jpg)

[ ![nadia cheesecake flan](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-cheesecake-de-nadia-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/flan-cheesecake-de-nadia.jpg>)

[ cheesecake flan ](<https://www.amourdecuisine.fr/article-flan-cheesecake.html> "Flan cheesecake") with Nadia (a reader) 

![Gingerbread and apple spice cake](http://img.over-blog-kiwi.com/300x225-ct/0/93/38/27/20140924/ob_53b317_gateau-pommes-epices-a-pain-d-epic.JPG)

[ ![kebda mchermla](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/kebda-mchermla.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/kebda-mchermla.jpg>)

[ Kebda mchermla, lamb liver with tomato sauce ](<https://www.amourdecuisine.fr/article-recette-kebda-mchermla.html> "kebda mchermla") at Amira (a reader) 

![DSCF9119](https://cuisineretpapoter.files.wordpress.com/2014/10/dscf9119.jpg?w=700)

![Granola peanut butter & amp; pecan nuts](http://img.over-blog-kiwi.com/0/95/91/39/20141029/ob_5597af_granola-peanut-butter-et-noix-de-peca.jpg)

[ ![Arielle leek fondue](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/fondue-de-poireaux-Arielle.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/fondue-de-poireaux-Arielle.jpg>)

[ Leek fondue ](<https://www.amourdecuisine.fr/article-fondue-de-poireaux.html> "Leek fondue") with Arielle (a reader) 

![](https://scontent-a-lhr.xx.fbcdn.net/hphotos-xap1/v/t1.0-9/10425509_536603449809426_3276507406831338238_n.jpg?oh=e6acb503cd806ce0b48bc0a216c097e8&oe=54E33068)

[ ![oum raouf liver in sauce](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/oum-raouf-foie-en-sauce.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/oum-raouf-foie-en-sauce.jpg>)

[ kebda mchermla ](<https://www.amourdecuisine.fr/article-recette-kebda-mchermla.html> "kebda mchermla") at Oum Raouf 

![](http://p2.storage.canalblog.com/28/98/189678/100109662_o.jpg)

[ ![Pauline leek fondue](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Fondue-de-poireaux-Pauline.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Fondue-de-poireaux-Pauline.jpg>)

[ leek fondue ](<https://www.amourdecuisine.fr/article-fondue-de-poireaux.html> "Leek fondue") chez Pauline 
