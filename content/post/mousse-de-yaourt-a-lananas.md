---
title: pineapple mousse with pineapple
date: '2015-06-29'
categories:
- dessert, crumbles and bars
- ramadan recipe
- sweet recipes
- sweet verrines
tags:
- Ramadan 2015
- Ramadan
- Algeria
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/mousse-au-yaourt-a-lananas.jpg
---
[ ![pineapple mousse with pineapple](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/mousse-au-yaourt-a-lananas.jpg) ](<https://www.amourdecuisine.fr/article-mousse-de-yaourt-a-l-ananas.html/mousse-au-yaourt-a-lananas-1>)

##  pineapple mousse with pineapple 

Hello everybody, 

Have you ever eaten yogurt mousse? in any case not me, and even the idea never touched me to be frank. This tema Cuisine recipe has so much more to me that I do not hesitate to share it with you. 

The idea is very simple, and variations according to the fragrance of yoghurt are unlimited. Even the way of making the assembly and the ingredients will give you free choice to make various yoghurt verrines, all as delicious as the others. 

**Yogurt mousse and pineapple**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/mousse-au-yaourt-a-lananas.jpg)

portions:  6  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * cream whipped cream and firm (200 gr of whipped cream powder + 1 glass and a half of milk) 
  * 2 jars of yoghurt flavored taste to choose. 
  * about 12 boudoir cookies. 
  * 1 can of pineapple with syrup 



**Realization steps**

  1. in a bowl, mix the whipped cream with the 2 jars of yoghurt. 
  2. cut the pineapple into small pieces (leave some for the decoration) 
  3. dip the boudoirs in the pineapple syrup. 
  4. put the cookies in the bottom of the verrines 
  5. add over the small pieces of pineapple 
  6. decorate with yogurt mousse 
  7. Continue the assembly operation until you have completed the verinnes. 
  8. decorate with the pineapples left out and put in the fridge 2 hours before eating 



[ ![pineapple mousse with pineapple 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/mousse-au-yaourt-a-lananas-1.jpg) ](<https://www.amourdecuisine.fr/article-mousse-de-yaourt-a-l-ananas.html/mousse-au-yaourt-a-lananas-1>)
