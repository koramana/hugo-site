---
title: Cake with orange and coconut
date: '2014-04-02'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cake-a-l-orange_thumb.jpg
---
[ ![Cake with orange and coconut](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cake-a-l-orange_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cake-a-l-orange.jpg>)

##  Cake with orange and coconut 

Hello everybody, 

here is a recipe that I dig out of my hard drive, cake with orange, and coconut. Almost a month ago, I was invited to a friend's house, who is a loyal reader of my blog, and who recently moved to Bristol. 

So I was very happy to visit her, and since I did not want to go to her house empty-handed, I had to concoct this little [ cake ](<https://www.amourdecuisine.fr/categorie-10678925.html>) to the orange that was fluffy wish, the only problem is that I did not let it too cold, and I covered it to take it, even cutting it at home, it was still hot . 

Next time, I'll let the cake cool on a rack. That's why I can offer you some pictures that I took home. 

you can also see: 

[ orange cake without gluten ](<https://www.amourdecuisine.fr/article-gateau-sans-gluten-aux-oranges-et-amandes-91051849.html>)

and [ cake with orange ](<https://www.amourdecuisine.fr/article-gateau-a-l-orange-102623009.html>) .   


**Cake with orange and coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/cake-a-l-orange_thumb-300x208.jpg)

portions:  12  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * 6 eggs 
  * 1 and a half cups of sugar (a glass of 250 ml) 
  * 1 glass and a half sifted flour 
  * 1 packet of baking powder 
  * 1 half glass of coconut 
  * 1 glass of orange juice (if it's canned juice, reduce the amount of sugar) 
  * ½ glass of oil 



**Realization steps**

  1. Wash and grate a big orange 
  2. separate the yolks from the egg whites, and beat the egg white into the snow, with 1 pinch of salt 
  3. add half of the sugar towards the end 
  4. in another bowl, mix all remaining ingredients 
  5. incorporate this mixture any egg white paper 
  6. oil and flour a Palestinian mold and pour the mixture into it 
  7. cook at 160 degrees, check the cooking with the tip of the knife 
  8. take out the cake and sprinkle with icing sugar. 



[ ![Cake with orange and coconut](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cake-a-l-orange-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cake-a-l-orange-2_2.jpg>)

bonne dégustation 
