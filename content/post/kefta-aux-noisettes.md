---
title: Kefta with hazelnuts
date: '2011-07-26'
categories:
- soups and velvets

---
Kefta with hazelnuts كفته بالنوازات, كفته بالبندق Algerian halawiyat Finally I have the time to post one of my recipes of the cakes of the aid, and there are still some recipes, but I can not find the time, I do not know any more time, or I'm no longer organized, with these very short days, and my daughter that I take to the "play groups, Vergules" because she likes to play, and do not want to stay alone at home so I'm posting this recipe, which I find in our adorable kouky, and that I like well and it was the hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Kefta with hazelnuts 

Algerian halawiyat 

Finally I have the time to post one of my recipes of the cakes of the aid, and there are still some recipes, but I can not find the time, I do not know the time, or I'm not organized, with these very short days, and my daughter whom I take to the "play groups, Vergules" because she likes to play, and do not want to stay alone at home 

so I post you this recipe, that I find at our adorable kouky, and that I like well 

and it was the recipe with which I closed my cake preparations for 2010. 

so I copy / paste from her home. 

**_Almond paste_ **

**_2 glasses of ground almonds (1 glass = 250ml)_ **

**_1 glass of icing sugar_ **

**_1 tablespoon of vanilla extract or rose water_ **

**_Syrup: 250g of crystallized sugar + ½ liter of water_ **

**_Walnut paste (I put the hazelnut)_ **

**_1 glass of chopped walnuts (chopped hazelnuts)_ **

**_½ glass of biscuits powder_ **

**_1 tbsp softened butter (must say I put more butter, because I pick up with butter)_ **

**_Honey_ **

**_Dyes: pink, green and white_ **

**_Prepare the syrup with water and sugar, mix, bring to a boil and cook for 15 minutes over low heat until thickened. Remove and let cool_ **

**_Sift together ground almonds, icing sugar, and vanilla extract_ **

**_Wet with the syrup until you get a smooth, manageable but firm enough paste_ **

**_In a little syrup dilute the dyes_ **

**_share the almond paste in 3, a large part to leave nature and 2 small parts to color in pink and green,_ ** _**slowly pouring the colored syrup over the almond paste, kneading until the desired color is obtained,** _

**_Cover with cling film and let stand_ **

**_Prepare the stuffing of hazelnuts by mixing the chopped hazelnuts, the biscuits in powder form, the softened butter, to wet with the honey gradually to obtain a manageable and homogeneous paste (at this stage, I find the dough a little sweet, so I pick up with butter)_ **

**_Sprinkle the work surface with cornflour and spread the marzipan in a rectangle 5mm thick_ **

**_place a layer of hazelnut stuffing on top, cover again with another layer of almond paste._ **

**_and cut in squares,_ **

**_For the decor, I used the commercial roses._ **

**_Garnish with shiny food and put in pretty boxes_ **

**_Keep in a hermetically sealed box_ **

this photo is one of my photos of my ugly 2010 table in Boudouaou. 

a family help party, it's great. 

thank you for your comments, and thank you for wanting to subscribe to my newsletter, if you like to receive my new publications. 

merci pour toutes vos visites. 
