---
title: Tiramisu with brownie
date: '2014-02-14'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/tiramisu-au-brownie0001.jpg
---
[ ![tiramisu with brownie0001](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/tiramisu-au-brownie0001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/tiramisu-au-brownie0001.jpg>)

Hello everybody, 

**Tiramisu with brownie** , a delicious **dessert** forgotten in the archives of my computer. 

I really like the [ Tiramisu ](<https://www.amourdecuisine.fr/categorie-recettes-sucrees/cheesecakes-et-tiramisus>) , and I'm always looking for new recipes, and new pranks but finally it's not an inverted recipe, for this **Tiramisu** : I put in the delicious [ brownie ](<https://www.amourdecuisine.fr/article-brownies-aux-cacahuetes-97765323.html>) that I prepared. 

so if you like this dessert, you can find even more recipes from [ Tiramisu ](<https://www.amourdecuisine.fr/categorie-recettes-sucrees/cheesecakes-et-tiramisus>) .   


**Tiramisu with brownie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/tiramisu-au-brownie0003.jpg)

portions:  4  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * brownie 
  * 250 g of mascarpone 
  * 3 eggs 
  * 90 g of sugar 
  * 2 tablespoons cocoa. 



**Realization steps** prepare the brownie 

  1. Separate the egg whites from the yolks. 
  2. Whip the egg whites with a pinch of salt and set aside. 
  3. Whip the egg yolks with the sugar to have a white and sparkling mixture. 
  4. Stir in mascarpone and cocoa powder. 
  5. Gently add the egg whites to the spatula. 
  6. In verrines, proceed to assembly: line the bottom with a layer of brownie crumbled. 
  7. Cover with a layer of mascarpone cream. 
  8. cover with a new layer of brownie and finally cream. 
  9. Book fresh. 



[ ![tiramisu with brownie0002](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/tiramisu-au-brownie0002.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/02/tiramisu-au-brownie0002.jpg>)
