---
title: CHOCOLATE LEAVES
date: '2007-11-13'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/11/190576261.jpg
---
This is a very simple decoration to make, and much nicer when made from real leaves. 

![choco](https://www.amourdecuisine.fr/wp-content/uploads/2007/11/190576261.jpg)

The ideal is to use smooth and vigorous leaves like that of orange or laurel ... for my part I used that of my hedge. 

I picked and washed each leaf in hot water. and wiped gently with a paper towel.For more delicate leaves, I just spend a cotton of warm water on the leaves. 

I melt chocolate couverture or pastry chocolate of good quality, milk chocolate cover or white are also suitable. 

And I begin to realize my chocolate leaves, here are two methods to choose from: 

\- dip the back of the leaves in the chocolate (simple and fast method that is suitable for bay leaves or orange) 

\- for irregularly shaped leaves, coat them with chocolate using a brush 

paint from the heart of the leaf outwards so as not to overflow the back 

![choco1](https://www.amourdecuisine.fr/wp-content/uploads/2007/11/190576491.jpg)

I wrote well: the back of the leaves, this is where you will have a better result, because will also appear the thin veins of the leaf. Thing you will not get if you use the top side of the sheet. But I admit that ease of takeoff is with the upper surface that it is easier to take off the chocolate from the sheet. 

Finally, wait until the chocolate is well solidified before delicately taking off the chocolate leaf 

The ideal is to remove the leaf by the tail and draw gently that's why in the photo I did not brush the tail 

une fois disposées sur votre gâteaux , vous pouvez saupoudrer les feuilles de sucre glace pour plus d’effet 
