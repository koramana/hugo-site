---
title: pink lamingtons cakes
date: '2015-04-14'
categories:
- gateaux algeriens au glaçage
- gateaux algeriens- orientales- modernes- fete aid
- Gateaux Secs algeriens, petits fours
tags:
- Algeria
- Aid
- Oriental pastry
- Algerian cakes
- Cookies
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-lamington-1.jpg
---
[ ![pink lamingtons cakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-lamington-1.jpg) ](<https://www.amourdecuisine.fr/article-gateaux-lamingtons-roses.html/gateau-lamington-1>)

##  pink lamingtons cake 

Hello everybody, 

The lamingtons are small square or rectangular shaped cakes, which are at the base of a soft cake cut into small pieces of almost 4 by 4 cm, or according to taste, assembled in half with jam, cream, or chocolate ganache, then covered according to taste, or originally chocolate, then coated in a beautiful shredded coconut powder. 

These Lamingtons -so girly- from our friend Wamani, or as she called them the pink Lamingtons, are coated with a beautiful layer of white chocolate ganache colored in pink, then coconut .... A version that I like a lot, and that gives ideas for even more varieties.   


**pink lamingtons cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-lamington.jpg)

portions:  30  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 90 gr of flour 
  * 3 eggs 
  * 85 gr of sugar 
  * 30 gr of melted butter 

to coat the cakes: 
  * 100 gr of white chocolate 
  * 150 ml of liquid cream 
  * pink dye 
  * 1 tablespoon of pink water 
  * coconut to cover the cakes. 



**Realization steps**

  1. Preheat the oven th. 6 (180 ° C). 
  2. Butter a square pan 20 cm square and cover with parchment paper. 
  3. Whisk eggs and sugar with electric whisk for 10 min until doubled in volume. 
  4. Add sifted flour twice, mixing gently to keep the cake sparkling. 
  5. Stir in the melted and cooled butter. 
  6. Pour the mixture into the mold and cook for 15 to 20 minutes. 
  7. Unmold and let cool on a wire rack. 
  8. Cut the cake into 4 cm squares. 

prepare the icing: 
  1. heat the cream until boiling 
  2. pour it on the white chocolate in pieces, 
  3. leave 2 min then mix well. 
  4. Add rose water, and pink dye 
  5. dip the cake squares in this mixture with a fork, 
  6. then coat them in the coconut. 
  7. let it dry and you can put them in boxes to present them. 



[ ![pink lamingtons cakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-lamingtons-roses.jpg) ](<https://www.amourdecuisine.fr/article-gateaux-lamingtons-roses.html/gateau-lamingtons-roses>)
