---
title: Nutella hot chocolate / hot chocolate
date: '2013-11-23'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chocolat-chaud-1024x685.jpg
---
[ ![Nutella hot chocolate / hot chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chocolat-chaud-1024x685.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chocolat-chaud.jpg>)

##  Nutella hot chocolate / hot chocolate 

Hello everybody, 

It is cold, but really cold, we do not want to point the nose outside. At home, it does not snow yet, but in the morning I tell you one thing, it is better to do "skating" so it's icy out ... 

So today, when I went out to take my children to the mosque, and back home, I wanted only one thing, a hot chocolate, very hot .... 

I remembered the recipe for Lunetoiles she sent me almost a year ago, and that I never posted, I went to make a cup, I sat in front of my little radiator, and I was looking out the window, there was not much to see, because I'm on the 4th floor, and because it's so cold, nobody turns outside, especially not on Saturday morning ... 

But, I sipped my homemade hot chocolate, and I savored those relaxing tastes, and that sweet scent of Nutella melted in my hot milk .... 

Do not hesitate to make this little recipe at the first opportunity, but above all, take a step back ... enjoy this hot drink away from stress, and far from worries, let yourself be carried away by these flavors and heat that will flow gently in you, and will warm your heart.   


**easy homemade hot chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/chocolat-chaud-au-nutella-685x1024.jpg)

Recipe type:  drink  portions:  2  Prep time:  10 mins  cooking:  5 mins  total:  15 mins 

**Ingredients**

  * 500 ml of milk 
  * 2 tablespoon nutella 
  * 2 pieces of sugar   
optional 



**Realization steps**

  1. Bring the milk to a boil and melt the nutella, 
  2. add the sugar according to your tastes. 
  3. Stir with a whisk until nutella is completely melted in warm milk. 
  4. Pour into the cups and serve immediately. 
  5. Serve with whipped cream, sprinkle with a hint of cocoa and a ferrero rock stung on a wooden spike. 
  6. Treat yourself ! 



[ ![Nutella hot chocolate / hot chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/hot-chocolat-685x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/hot-chocolat.jpg>)
