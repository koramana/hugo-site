---
title: Slilates, Algerian cake 2014 in video
date: '2014-07-12'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/aid-el-fitr-2013-slilates.CR2_.jpg
---
##  [ ![help el fitr 2013 slilates.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/aid-el-fitr-2013-slilates.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/aid-el-fitr-2013-slilates.CR2_.jpg>)

##  Slilates, Algerian cake 2014 in video 

Hello everybody, 

Slilates with honey is an Algerian cake that I made almost a year ago, during the celebration of the past year, but I did not have the chance to post this recipe because I had made a video, and I really had the fleme to make this video ... 

So yesterday, I breathed deeply, and I started to arrange the video, it took me all night, ouuuuuuuuuuuuuuuuuuf. 

I'm still a little disappointed with the quality of the video, because when I left last year in Algeria, I did not take my foot off the camera, so I asked each person about the video. family that was going to hold me the camera, result, a video that dances all the time, lol ... 

I know you're giddy watching the video, but it's worth it, to see the preparation of this cake ... 

for the moment, I went to do the work, and as soon as I finish, I come back to post the method of realization ... 

For info, I found this recipe on an Algerian forum, it's a good friend who made it on this forum, thank you Batila, for your beautiful achievements.   


**Slilates, 2014 Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/slilates-gateau-algerien-aid-el-fitr-2014.jpg)

portions:  40  Prep time:  40 mins  cooking:  10 mins  total:  50 mins 

**Ingredients** dough: 

  * 3 measures of flour 
  * ½ measure of melted smen 
  * salt and vanilla 
  * water + orange blossom water 

prank call: 
  * 3 measure of crushed almonds 
  * 1 measure of crushed nuts 
  * ½ measure of sugar 
  * vanilla 
  * Orange tree Flower water 
  * egg white to stick the petals 
  * honey for the final garnish. 



**Realization steps** For the stuffing, 

  1. mix almonds, walnuts, sugar, vanilla and wet with orange blossom water. 
  2. Mix well. 

prepare the pasta: 
  1. Put the flour in a salad bowl, add vanilla and salt, mix. 
  2. Introduce the melted smen, and sand the mixture well with your hands. 
  3. Perfume with 3 to 4 tablespoon of orange blossom water. 
  4. Then pick up with water until you get a smooth dough. 
  5. Allow the dough to rest at least 1 hour, then divide into 3 balls of equal size. 
  6. Prepare the dishes in advance, grease them with a little smen. 
  7. Spread the dough thinly with a rolling pin (or baking machine), then cut rounds of almost 8 cm diameter with a cookie cutter, and darken the diced dishes. 
  8. Fill them with a tablespoon of stuffing to cover the dough well, 
  9. Once you have all your mussels garnished, brush them with egg white 
  10. cut small circles of pasta dough, using a brush brush them with a little egg white and pinch with your fingers to form a cone. 
  11. fix the cornets one by one on the stuffing 

Now prepare the decoration, 
  1. use a flower-shaped cookie cutter, dip the sharp part of the cookie cutter into dye, then cut rosettes of almost 4cm in diameter. 
  2. Fold the rose in 4, make a hole in the middle of the cake with a toothpick or wooden pick, and insert the rose, 
  3. Arrange the rosette well with a toothpick 
  4. Put the cakes in a preheated oven at 180 degrees C for 10 to 12 minutes, watch the cooking, they must not color. 
  5. After baking unmold and soak in honey diluted with orange blossom water 



{{< youtube w >}} 
