---
title: Samsa or triangles stuffed with almonds "صامصة"
date: '2016-07-27'
categories:
- gateaux algeriens- orientales- modernes- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa-laadjine-2.jpg
---
##  [ ![Samsa or triangles stuffed with almonds](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa-laadjine-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa-laadjine-2.jpg>)

##  Samsa, or triangles stuffed with almonds 

Hello everybody, 

_I love samsa (samsa laadjine) and this ramadan we enjoyed this samsa that I realized the first day of Ramadan, and that we tasted each day with a delicious mint tea._

_Anyway, I found the opportunity to put together this article of samsa, and submit the new pictures of this_ _[ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) very refined and above all very beautiful and delicious. C _ _e[ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , has different ways of preparing, the best known and the one with bricks leaves, there is also the method of preparing a dough and roll the stuffing in, but the one I like is the ravioli method, hihihihi Well yes we do not get tired, the result of pieces of the same size, and most importantly, we do not have a lot of dough in our cakes. _

![Samsa or triangles stuffed with almonds](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/samsa-028_thumb.jpg) _more_   


**Samsa, or triangles stuffed with almonds "صامصة"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa-laadjine.jpg)

portions:  40  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** dough: 

  * 3 bowls of flour   
3 measures of flour, so whatever the measure you are going to use 
  * 1 bowl of butter 
  * 1 pinch of salt 
  * water 

prank call: 
  * 2 bowls of almonds 
  * 1 bowl of crystallized sugar 
  * cinnamon 
  * Orange tree Flower water 
  * oil for frying. 



**Realization steps**

  1. prepare the dough, mixing the ingredients, to have a malleable paste, not to knead too much, not to give elasticity to the dough, because it will lose otherwise its crispness. 
  2. share the dough with medium sized balls. let it rest 
  3. prepare the stuffing that should not be too sticky or too liquid. 
  4. spread out a ball, place it on your little tool, roll it over this dough, just to have the drawing of the mold, put a spoon of stuffing in each triangle, spread another ball, place it on the first layer and pass the roll, and you have your little triangles, ready to go into the frying. 
  5. if you are afraid that your triangle will open when frying, brush the contact areas of both pasta with a little egg yolk.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa.jpg>)
  6. after frying, you can dip your Samsas in honey, and book in a large hermetic box. 
  7. and that it was boooooooooooooon 
  8. I made you many photos, well explained, and I made you a beautiful video demonstrative 



![Samsa or triangles stuffed with almonds](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa-laadjine-1.jpg)

[ _![Samsa or triangles stuffed with almonds](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa-032_thumb.jpg) _ ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/samsa-032.jpg>)

_Enjoy_

[ _![Samsa or triangles stuffed with almonds](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/samsa-033_thumb.jpg) _ ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/samsa-033.jpg>)
