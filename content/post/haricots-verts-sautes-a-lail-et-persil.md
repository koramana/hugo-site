---
title: sautéed green beans with garlic and parsley
date: '2017-05-12'
categories:
- Algerian cuisine
- diverse cuisine
- Cuisine by country
tags:
- Algeria
- Ramadan
- Full Dish
- dishes
- la France
- Easy cooking
- Vegetables

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/05/haricots-verts-saut%C3%A9s-%C3%A0-lail-et-persil-3.jpg
---
![sautéed green beans with garlic and parsley 3](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/haricots-verts-saut%C3%A9s-%C3%A0-lail-et-persil-3.jpg)

##  sautéed green beans with garlic and parsley 

Hello everybody, 

This recipe of green beans sautéed with garlic and parsley marked my childhood, because when we were little, it was only in this way that we ate green beans. My mother tried every way to introduce the green beans, but it was barely enough to take a bite. 

All this seems odd now, because frankly I eat green beans in all their states and in all the sauces, why were we so demanding when we were small ??? Poor mother, we made him see colors at the time. 

With my children on the other hand, and I touch the wood, I have no problem with vegetables, my concern and more with the meat, even when we come to roast chicken, or like here [ chicken legs roasted with mustard ](<https://www.amourdecuisine.fr/article-cuisses-de-poulet-a-la-moutarde-au-four-111087660.html>) Well, my kids prefer schnitzel and chicken breast ??? 

**sautéed green beans with garlic and parsley**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/haricots-verts-saut%C3%A9s-%C3%A0-lail-et-persil.jpg)

**Ingredients**

  * 500 gr of green beans 
  * 4 to 5 cloves of garlic 
  * ½ bunch of chopped parsley 
  * 2 tbsp. butter 
  * 1 C. tablespoon of olive oil 
  * salt and black pepper 



**Realization steps**

  1. You can cook green beans in salt water, otherwise as here I cook them in steam. 
  2. when the green beans are cooked, let them drain from their water. 
  3. Melt the butter in a heavy-bottomed pan. 
  4. add crushed garlic, then green beans and parsley, 
  5. stir it so it does not burn. If it lacks fat, add a little oil. 
  6. season to taste with a little salt and black pepper. 



![sautéed green beans with garlic and parsley 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/05/haricots-verts-saut%C3%A9s-%C3%A0-lail-et-persil-1.jpg)
