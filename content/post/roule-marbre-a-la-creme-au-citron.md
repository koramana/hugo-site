---
title: rolled marbled with lemon cream
date: '2016-12-02'
categories:
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/rouler3_thumb-300x225.jpg
---
##  rolled marbled with lemon cream 

Hello everybody, 

a delicious **biscuit** rolled, marbled **chocolate** / **vanilla** , a recipe from the TV show: **Houriat el Matbakh** , broadcast on the Arabic culinary television channel, **Fatafeat tv** . 

I really like [ houriat el matbakh ](<https://www.amourdecuisine.fr/categorie-11131465.html>) , and that's why I realize a lot **delicious recipes** . 

if you like the roules you can find on my blog: [ rolled strawberry mascarpone ](<https://www.amourdecuisine.fr/article-53001970.html>) , or [ rolled with jam ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture-a-minuit-45608781.html>) . A delicious [ cake ](<https://www.amourdecuisine.fr/categorie-10678925.html>) super good presentable, and full of flavors, I pass you the recipe without delay: 

**rolled marbled with lemon cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/rouler3_thumb-300x225.jpg)

portions:  12 

**Ingredients**

  * 5 Eggs 
  * 140 g of sugar 
  * 140 g flour 
  * 3 tablespoons cocoa 
  * 1 teaspoon of vanilla 
  * Rectangular tray wrapped with parchment paper. 

Lemon cream 
  * 100 ml of lemon juice (the equivalent of 2 lemons and a half for me) 
  * the zest of a lemon 
  * 80 g butter (small pieces) 
  * 70 g of sugar 
  * 2 eggs 
  * 1 tablespoon of starch 



**Realization steps**

  1. Heat the oven to 180 degrees Celsius. 
  2. take 40 g of flour are heavily and mix it with three tablespoons of cocoa and set aside 
  3. Beat eggs with sugar and vanilla until doubled in size. 
  4. Divide the egg mixture and the sugar into two halves using a spoon (you really need both mixtures to be equal), add in the first half the amount the 100 gr of white flour, very slowly 
  5. Add the flour + cocoa mixture to the second half of the egg mixture, still gently with a spoon 
  6. put the two blends in two socket pockets 
  7. and start cake forming 
  8. form large lines and spaced with white dough 



9\. do the same to fill the spaces with the brown dough 

10\. you can take a spoon or a knife and make mottling 

11\. immediately put in a preheated oven for 15 to 20 minutes or less depending on your oven 

12\. at the exit of the oven, remove the parchment paper, and roll the sponge cake in a clean and slightly damp cloth, before it cools 

13\. after cooling completely, re-open the roll and stuff with lemon cream 

14\. you can put this rolling in the fabric so that it stays rolling well, and put in the fridge until the cream takes well and out to decorate it to your liking. 

Preparation of the lemon cream: 

  1. In a saucepan over the heat put lemon juice and zest with sugar and butter in small pieces and bring to a boil 
  2. On the other hand, beat the eggs well and add the cornstarch, then take a small amount of the mixture (juice and lemon zest, sugar and butter) and add very gently on the egg mixture 
  3. Pour the liquid mixture after whole on your eggs 
  4. and return to low heat all and mix until you get a thick cream. 



let this cream cool a bit and then fill with it, roll it 

Bonne dégustation 
