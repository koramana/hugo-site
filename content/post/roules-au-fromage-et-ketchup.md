---
title: cheese rolls and ketchup
date: '2011-08-31'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles and bars
- Gateaux au chocolat
- cakes and cakes

---
if you like this recipe vote: 20 C here hello everyone, always for the recipes participating in the contest ramadanesque entries, that my readers without blogs send me. this time again it's a recipe from nacy, who sent me his last participation, 2 days ago, but that I did not have the opportunity to publish. so I give you his recipe: & nbsp; ingredients for the dough & nbsp; 500gr flour 100gr butter 1/2 package of baking powder a glass of milk to pick up the dough ingredients for stuffing: & nbsp; a sweaty onion 2 spoon at & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.7  (  1  ratings)  0 

if you like this recipe vote: 20 C [ right here ](<https://www.amourdecuisine.fr/article-concours-les-entrees-ramadanesques-le-vote-83357324.html>)

Hello everybody, 

always for the recipes participating in the contest the ramadanque entries, that my readers without blogs send me. 

this time again it's a recipe from nancy, which sent me his last participation, 2 days ago, but that I did not have the opportunity to publish. 

so I give you his recipe: 

ingredients for the dough 

  * 500gr of flour 
  * 100gr butter 
  * 1/2 package of baking powder 
  * a glass of milk to pick up the dough 



ingredients for stuffing: 

  * a sweaty onion 
  * 2 tablespoons dried oregano 
  * ketchup I had more I used tomato sauce 
  * a small box of minced mushrooms 
  * melted cheese a portion of 90gr 
  * a packet of grated cheese 



preparation: 

  1. mix the flour with yeast and salt then add the butter, 
  2. get the butter into the flour well 
  3. add the milk until you obtain a malleable paste 
  4. add milk if necessary set aside to let stand. 
  5. meanwhile prepare your ingredients then lower the dough into a rectangle 
  6. brush this ketchup rectangle sprinkle with dried oregano 
  7. add the sweaty onion then the pieces of melted cheese, the mushrooms 
  8. puis tout doucement rouler votre pâte farcie du côté le plus large puis envelopper le bâtonnet obtenu dans un film alimentaire bien serrée sans faire sortir la farce bien sûr 🙂 
  9. and put in the freezer. leave a moment and cut it into 2cm slices 
  10. put them on baking trays and sprinkle with grated cheese and dried oregano 
  11. bake for about 20 minutes 



thank you for your visits and comments, wait for the final recap tomorrow, and if there is a lack or a forgotten recipe, contact me 

bonne journee 
