---
title: Weight Watchers pear and apple pie
date: '2011-06-17'
categories:
- boissons jus et cocktail sans alcool
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/01/251294821.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/251294821.jpg)

Hello friends, 

since I'm in full swing, I'm putting this little recipe back to you. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251294921.jpg)

I do not tell you the disaster with my camera, I took pictures of what I was doing without  check  after  , the day I come to see the pictures, I found it all black ......... ..my god, what is it  it  , actually my son has  click  on a  button  and I did not do  Warning  , (Look at the pictures at the bottom,  despite  that I have them work a little) 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251295061.jpg)

go, without delay, I give you the recipe for this pie at the  Weight  watchers  : 

ingredients  : 

  * 2 medium apples 

  * 2  pears  averages 

  * a  pinch  of  cinnamon 

  * 1 CAS of  cornflour  (  0.5  pt  )  dissolved  in 1 case of water 

  * 2 cases of butter or margarine  lightened  (  2  pts  ) 




for the  dough  : 

  * 135  gr  of flour (  6.5  pts  ) 

  * 4 CAS of butter  lightened  (  4  pts  ) 




I add a little sugar  sweetener  , in the filling and the  dough  pie. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251295141.jpg)

ready  filling, cooking in a pan, apples  peeled  ,  deseeded  , and  cut  in  dice  , as well as  pears  peeled  ,  deseeded  , and  cut  in  dice  , add the  pinch  of  cinnamon  , and water to just cover the fruits (not too much) 

put on medium heat to cook for 7 min to 10, add  after  the  cornflour  dissolved  in water. let it cook  until  thickening  . 

remove from the heat, add the 2 cases of light butter. 

** You can, in case of no diet, to put normal sugar, normal butter, and why not prepare a sweetened shortbread pastry  **

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251295251.jpg)

prepare the dough now, mixing the flour and the butter well, then add the water little by little, to have a very malleable paste. knowing that the ingredients are for a small mold of 18 cm almost in diameter. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251295431.jpg)

spread out the dough and place it in a mold, while keeping a little to make the filling. 

put the pear and apple flan on the dough, and garnish with the rest of the dough. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251295521.jpg)

place in a preheated oven at 190 degrees, and leave golden 30 to 40 minutes 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251295651.jpg)

it was delicious anyway. 

I tell you good appetite 

to the next recipe. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251294671.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251295691.jpg) ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/251295801.jpg)
