---
title: easy birthday cake حلويات عيد ميلاد سهلة
date: '2013-03-13'
categories:
- couscous
- recipes of feculents
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-d-anniversaire-red-velvet-057.CR2_2.jpg
---
![easy birthday cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-d-anniversaire-red-velvet-057.CR2_2.jpg)

##  easy birthday cake حلويات عيد ميلاد سهلة 

Hello everybody, 

**easy birthday cake** : For my birthday this time, I wanted to make an easy birthday cake very easy even, and very simple, because I was too taken. The idea for this beautiful easy birthday cake is based on a layer cake red velvet butter cream came to me during my ride on Pinterest, a site I like a lot, a site on which we can pin everything we like on the net, in tables, and it is easier to find later when we want to review, the article, or the recipe ... .. 

So, after a small visit on pinterest I saw this birthday cake easy, but I did not have the SLR immediately to pin it ... so I kept the idea of ​​decoration, but not the recipe ... 

Not serious, a delicious [ Genoese ](<https://www.amourdecuisine.fr/article-genoise-inratable.html>) well done and inratable, a beautiful cream, and here is the recipe for my birthday cake easy, a layer cake with butter cream to fall. 

more 

![easy birthday cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-d-anniversaire-red-velvet-100.CR2_2.jpg)

**easy birthday cake حلويات عيد ميلاد سهلة**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-d-anniversaire-red-velvet-057.CR2_2.jpg)

Recipe type:  birthday cake  portions:  12  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients** for the genoise: (3 genoise for me)   
for each round biscuit 25 cm in diameter (I use a 20 cm mold to have high cakes) 

  * 2 eggs 
  * 60 gr of sugar 
  * 60 gr (flour + 1 teaspoon cocoa) 
  * 1 pinch of baking powder 
  * pink dye   
for the 3 genoise, the ingredients are multiplied by 3. 

syrup: 
  * 200 gr of sugar 
  * 300 ml of water 
  * vanilla 

decoration: 
  * [ cream with vanilla butter. ](<https://www.amourdecuisine.fr/article-la-creme-au-beurre-facile-112774625.html> "easy butter cream")



**Realization steps** for the genoise: 

  1. first prepare the syrup, mix all the ingredients, and place on medium heat, at the first boiling turn off the heat, and let cool. 
  2. beat the eggs thoroughly with the sugar until the mixture is white and very airy. 
  3. add the flour, cocoa and baking powder, 
  4. cook in a hot oven for 15 to 20 minutes (depending on the capacity of your oven) 
  5. preparez [ vanilla butter cream ](<https://www.amourdecuisine.fr/article-la-creme-au-beurre-facile-112774625.html> "easy butter cream") . 
  6. place the first layer of the biscuit, and soak it a little (not too much) with the prepared syrup. 
  7. cover the top of the biscuit with the butter cream, using a pastry bag, and then standardize well with a spatula. 
  8. place the second biscuit on top, soak it generously with the syrup, and cover once more butter cream. 
  9. then decorate as you wish. 



![easy birthday cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-d-anniversaire-red-velvet-096.CR2_2.jpg)

![easy birthday cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/gateau-d-anniversaire-red-velvet-055.CR2_3.jpg)

you can decorate a cake parreil with: 

[ ![butter cream with Swiss meringue3](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-au-beurre-a-la-meringue-suisse3_thumb3.jpg) ](<https://www.amourdecuisine.fr/article-creme-au-beurre-meringuee-113781590.html>)

[ a meringuee butter cream ](<https://www.amourdecuisine.fr/article-creme-au-beurre-meringuee-113781590.html>)

[ ![easy butter cream](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/cupcake-039_thumb2.jpg) ](<https://www.amourdecuisine.fr/article-la-creme-au-beurre-facile-112774625.html>)

[ easy butter cream ](<https://www.amourdecuisine.fr/article-la-creme-au-beurre-facile-112774625.html>)

as you can garnish the inside of [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline-108846274.html>) : 

[ ![Chiffon cream](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/creme-mousseline_thumb3.jpg) ](<https://www.amourdecuisine.fr/article-creme-mousseline-108846274.html>)

merci pour votre visite, et a la prochaine recette. 
