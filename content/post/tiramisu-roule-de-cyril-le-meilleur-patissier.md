---
title: tiramisu rolled from Cyril-the best pastry chef
date: '2015-11-23'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- cheesecakes and tiramisus
- dessert, crumbles and bars
- sweet recipes
tags:
- mascarpone
- Dessert
- desserts
- Without cooking
- Italy
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tiramisu-version-roul%C3%A9-meilleur-patissier.jpg
---
[ ![tiramisu rolled the best patissier](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tiramisu-version-roul%C3%A9-meilleur-patissier.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tiramisu-version-roul%C3%A9-meilleur-patissier.jpg>)

##  tiramisu rolled from Cyril-the best pastry chef 

Hello everybody, 

You have probably been many and many to follow the show of best pastry last week. In any case Lunetoiles did not miss it, and without hesitation, she realized the recipe of tiramsu rolled version of Cyril: 

What she tells us: 

I tried to reproduce the recipe for Cyril, but I did not make a homemade spoon biscuit, I took biscuits spoons of the trade. I then realized the mascarpone cream of **Christophe Felder** , I still reduce the amount of mascarpone. So I did make some small changes to the recipe for Cyril's rolled tiralisu. 

What I thought was very good, the mascarpone cream is very foamy, it is good, I think I will adopt it for my next tiramisu, insha ALLAH. The chocolate tube brings crispness, crispness and it goes well with the creamy side of tiramisu. 

**tiramisu rolled from Cyril, the best pastry chef**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tiramisu-version-roul%C3%A9-meilleur-patissier.jpg)

portions:  6  Prep time:  30 mins  total:  30 mins 

**Ingredients**

  * 200 g of good quality dark chocolate 
  * 3 eggs 
  * 100 g caster sugar 
  * 350 g of good quality mascarpone 
  * 18 biscuits with spoon (preferably homemade or from your pastry chef) 
  * 1 bowl of strong coffee 



**Realization steps** Chocolate tubes: 

  1. Cut out 6 rectangle of rhodoid leaves so that they are 10 cm wide and about 20 cm long so as to make 6 tubes. 
  2. Melt in a bain marie 200 gr of dark chocolate. 
  3. Once the chocolate has 55 ° C, bring it down to 28/29 ° C. 
  4. Then, put the melt again, until reaching the temperature of 32 ° C is the tempering of chocolate. 
  5. At 32 ° C spread the chocolate point on the rectangles of rhodoïd (guitar sheet), and roll them, one after the other so as to get 6 tubes that stand up, then gradually, tape them with an adhesive tape so that it fits and does not unfold. Put in the fridge. 

Prepare the mascarpone cream: 
  1. Separate whites from yellows. 
  2. Put them in two different containers. 
  3. Mount the egg whites in snow, incorporating half the sugar, as and when. Whip them until the mass is dense enough. 
  4. On the egg yolks, pour half of the sugar and whip vigorously with electric whisk until a whitish foam is obtained. 
  5. Add the mascarpone and whip to smooth the mixture. 
  6. Stir in some of the whites in the mascarpone-yellow egg mixture, using a spatula to loosen the whole. 
  7. Then add the rest of the whites and stir, trying to keep the lightness of the whole or until you obtain an airy texture and homogeneous. 
  8. Put the mascarpone cream in a pastry bag. 

Start editing: 
  1. When the chocolate tubes are set, fill the tubes as follows. 
  2. Insert in the bottom of each tube, a biscuit spoon soaked in coffee, then using the pastry bag garnish with mascarpone cream all around the biscuit. Repeat the process until you have filled all the tubes. 
  3. Sprinkle the top of the tubes with cocoa powder. 
  4. Then using the piping bag, make a small rosette of mascarpone cream on top of the tubes; and put a tip of cocoa on the rosette too. 
  5. Cool for several hours or overnight before eating 



[ ![Tiramisu rolled version](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/Tiramisu-version-roul%C3%A91.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/Tiramisu-version-roul%C3%A91.jpg>)
