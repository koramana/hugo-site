---
title: lham mhamer, roast meat tajine
date: '2016-04-26'
categories:
- Algerian cuisine
- cuisine marocaine
tags:
- Ramadan
- Potato
- Algeria
- Easy cooking
- Morocco
- dishes
- Ramadan 2016

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-1.jpg
---
[ ![lham mhamer, roast meat tajine 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-1.jpg>)

##  lham mhamer, roast meat tajine 

Hello everybody, 

Recently, I offered myself a **Moroccan tajine electric** . Although I tried to get consumer reviews on this tajine, and know how is the result of cooking, my research turned to Zero .... As soon as I received it, I did not hesitate to quickly remove the chicken drumsticks from the freezer (they were frozen) and make a **chicken tajine with chicken (** recipe to do again, and to come): tajine was just a delight, but I only made photos with my mobile phone, because my little tribe waited for dinner. Well no one could resist these captivating smells that emanated from this little tajine. 

[ ![chicken tajine with potatoes](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tajine-de-poulet-a-la-pomme-de-terre.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tajine-de-poulet-a-la-pomme-de-terre.jpg>)

**My verdict on the electric tajine** before moving on to the recipe, it's just **perfect** it is true that it is small and that it will be enough for two people, but me it suits me well, because at noon I am always with my husband only (the children are in the school). Another thing, the cooking time is long, minimum **2 hours and a half** for a **chicken-based dish** , and **3h** for a **dish with mutton** (cut into medium slices, I still have not tried with large pieces of meat. 

But the result is just melting in the mouth, rich in taste and flavors. Of course if you have the real tajine and gas at home, you will not need this tajine. But like me, I never got a tagine in the ground, or found a tajine cast iron, and having no gas at home, this tajine is just perfect. 

[ ![lham mhamer, roast meat tajine 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-2.jpg>)

Regarding now this delicious dish called **lham mhamer** , that is to say **roast meat tajine** , It was my 2nd test tagine electric, just for info, this tajine **do not consume a lot of electricity.** So not knowing how much cooking time could take, I started early at 9:30. I watched the meat from time to time, while turning it, and 3 hours later the meat was so tender, melting and beautifully scented that the children had already started eating before I did the staging to make my photos, hihihihihih .   


**lham mhamer, roast meat tajine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-3.jpg)

portions:  4  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * pieces of lamb meat according to the number of people   
leg of leg 
  * 1 chopped onion 
  * 2 cloves garlic cut lengthwise 
  * 1 piece of cinnamon stick (not too big) 
  * 2 bay leaves 
  * ¼ teaspoon ginger powder 
  * ¼ teaspoon turmeric powder 
  * salt, black pepper, and paprika 
  * 2 tablespoons olive oil 
  * hot pepper (easy) 



**Realization steps**

  1. put the electric tajine on (or place your tajine on medium heat) 
  2. pour the olive oil, add the chopped onion, garlic, bay leaves and cinnamon, until the onion becomes very soft. 
  3. add the pieces of meat and garnish them with the spices, salt and black pepper.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tajine-de-viande-roti.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/tajine-de-viande-roti.jpg>)
  4. let the meat heat slowly turning it occasionally on the other side. the meat will reject its water and fat and cook in it, which will give a unique taste to the dish   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti.jpg>)
  5. serve the hot dish, with fries and a nice salad, or [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html>) , the sauce of cooking will accompany this puree well. 



[ ![lham mhamer, roast meat tagine 6](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-6.jpg>)
