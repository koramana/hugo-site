---
title: leek and celery soup / easy and fast
date: '2014-09-06'
categories:
- soups and velvets

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-au-celeri-et-poireaux_thumb1.jpg
---
Leek and celery soup / easy and fast 

Hello everybody, 

Here is a very comforting soup for those cold days. Yes, after a beautiful summer in Algeria, here we are back in England, and already the gray covers everything, we have to put the light even the day, lol 

to forget this chill at table, I prepare this soup with leeks and celery very easy and quick to do, and above all, I was surprised by the taste, usually I do not like leeks ... but this velvety is a real delight. 

**leek and celery soup / easy and fast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/soupe-au-celeri-et-poireaux_thumb1.jpg)

portions:  2  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 2 tablespoons of butter 
  * ½ bundle of celery. 
  * 1 leek 
  * 1 medium potato 
  * 1 cube of chicken broth. 
  * 1 bay leaf. 
  * salt and black pepper 
  * 1 tablespoon lemon juice, even 2. 



**Realization steps**

  1. Heat the butter in a large pot over low heat. 
  2. Add the celery, leek, potato and bouillon cube. 
  3. let it simmer over low heat, stirring occasionally, until the vegetables begin to soften, about 10-12 minutes. (This time can vary considerably depending on the surface of the bottom of your pan). 
  4. Add water to the bay leaf and increase the temperature so that it boils a little, then reduce the heat so that the soup cooks without bubbling 
  5. let it simmer until the vegetables are very tender, about 20 minutes. 
  6. remove the bay leaf, and mix the mixture until it becomes smooth and velvety. 
  7. Season to taste with salt and lemon juice. 
  8. serve with olive oil, some celery leaves and black pepper. 



merci pour vos visites et bonne journee 
