---
title: Vanilla / Chocolate Muffins
date: '2012-06-04'
categories:
- gateaux, et cakes

---
they have just come out of the oven, and there are only two left, which I will keep for Daddy's love of cooking, before the children devour everything. 

they are soft, but above all very good 

I recommend the recipe that I find on one of my books in English, I realize all the same that I have very beautiful cookbooks, and all recipes that I make is a great success, so I share these recipes with you: 

the ingredients: for 12 muffins of average size: 

300 gr of self-raising flour, add 1 more yeast of yeast to the vote. 

1 C. coffee baking powder 

50 grams of butter, cut into small pieces 

80 grams of sugar 

150 grs of chocolate (nuggets or just like me, grated chocolate) 

2 eggs 

225 ml of milk 

1 teaspoon of vanilla extract 

prepare your ingredients 

preheat your oven to 200 degrees C, and butter your muffin pan, otherwise place your boxes in 

mix the flour and the baking powder, add the butter in pieces and sand in your hands, add the sugar and the chocolate in piece 

in another bowl, mix eggs, milk and vanilla, and afterwards pour this mixture over the flour mixture. 

mix everything gently without too much fluffing just to blend the mixture and make it homogeneous. 

pour your preparation with a spoon into the muffin pan. 

cook for 18 to 20 degrees or depending on your oven. 

remove from the oven and let cool on a wire rack. 

and enjoy 

merci pour tout vos commentaires 
