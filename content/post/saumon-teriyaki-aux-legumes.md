---
title: salmon teriyaki with vegetables
date: '2016-03-02'
categories:
- diverse cuisine
- fish and seafood recipes
- rice
tags:
- Healthy cuisine
- Easy cooking
- Fish
- broccoli
- Asia
- Soya sauce
- Full Dish

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/saumon-teriyaki-aux-legumes-1.jpg
---
[ ![salmon teriyaki with vegetables 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/saumon-teriyaki-aux-legumes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/saumon-terriyaki.jpg>)

##  salmon teriyaki with vegetables 

Hello everybody, 

At home, we really like salmon, usually I do it in foil, I first put the ingredients on baking paper, I close, then I put another aluminum foil, and the result is just divine. 

I wanted to make this recipe in the same way, but I did not have any more cooking papers, and as I did not want to make my salmon directly on aluminum foil, I chose to cook it in mini casseroles (well not so much mini than that, hihihihi) 

So with my vegetable teriyaki salmon recipe, I participate in Food Battle # 32 

[ ![Food Battle](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bataille-food.png) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/bataille-food.png>)

Agnès chose for theme: **MADNESS SEEDS**

**A little, a lot, crazy, roasted, raw, to bring some crispness ...**

**With 2 ingredients to respect:**

***** Seeds visible or hidden (poppy, chia, flax, anise, fennel, sunflower, nigella, sesame, squash ...) ***** A fruit or seasonal vegetable, raw or cooked (apple, pear, lemon, pineapple, squash, orange, butternut, carrot ...) In sweet or salty (or both), mousse, velvety, pie, cake, cake, biscuit, breadcrumbs ... I admit that I wanted to choose this recipe to participate in our round (recipes around an ingredient # 15 the fish, moreover the recipes will be online tomorrow March 3) then after I opted for another recipe . And when I saw the battle-food theme, I could not go without making the recipe. I hope you'll like her!! 

**salmon teriyaki with vegetables**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/saumon-terriyaki.jpg)

portions:  4  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients** honey teriyaki sauce: 

  * ⅓ cup of soy sauce 
  * 5 tablespoons of water 
  * ¼ cup of honey 
  * 1½ c of rice vinegar (I had not put, I used cider vinegar) 
  * 3 cloves of garlic, chopped 
  * 1 C. chopped ginger 
  * 1 C. coffee sesame oil 
  * 2 and ½ cups cornflour 

Salmon and vegetables 
  * 4 salmon fillets without skin. 
  * 4 cups of broccoli bouquets 
  * 1 and ½ cup thin sliced ​​carrots 
  * 4 green onions (green onions or cives) in slices 
  * 2 and ½ tablespoons of olive oil 
  * Salt and freshly ground black pepper 
  * Sesame seeds, to garnish 
  * cooked brown or white rice, to serve 



**Realization steps** prepare the teriyaki sauce: 

  1. In a small saucepan, mix together soy sauce, 3 tablespoons water, honey, vinegar, garlic, ginger and sesame oil. 
  2. Salt lightly as needed. Bring to a boil over medium heat. 
  3. In a small bowl, whisk together 2 spoonfuls of cornflour with water and mix well. Pour the mixture into the sauce, reduce the temperature and let it boil for 1 minute, stirring constantly. 
  4. Remove from heat and let cool slightly, about 5 to 10 minutes. 

For salmon and vegetables: 
  1. Mix broccoli, carrots and spring onions with olive oil and season with salt and pepper.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/saumon-teriyaki.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/saumon-teriyaki.jpg>)
  2. place the salmon pieces in a little cold teriyaki sauce. 
  3. Unwind broccoli and carrot mixture in mini-cocottes, 
  4. Add the piece of salmon on top. 
  5. Place the casseroles in a preheated oven at 180 ° C and cook for about 25 minutes. 
  6. Serve with brown or white rice if you wish and the remaining sauce. 



[ ![salmon teriyaki with vegetables](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/saumon-teriyaki-aux-l%C3%A9gumes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/saumon-teriyaki-aux-l%C3%A9gumes.jpg>)

La liste des participants : 
