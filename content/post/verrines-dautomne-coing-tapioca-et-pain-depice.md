---
title: Verrines d'automne Tapioca quince and gingerbread
date: '2013-11-24'
categories:
- bakery
- Indian cuisine
- Cuisine by country
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/verrines-tapioca-coings-et-pain-depice-001.CR2_-682x1024.jpg
---
[ ![Verrines d'automne Tapioca quince and gingerbread](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/verrines-tapioca-coings-et-pain-depice-001.CR2_-682x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/verrines-tapioca-coings-et-pain-depice-001.CR2_.jpg>)

##  Verrines d'automne Tapioca quince and gingerbread 

Hello everybody, 

Before posting this delicious recipe of sweet verrines of autumns with quince, tapioca and gingerbread, I tell you my little story with the quinces: 

I always complain about not finding quinces here in England, I just waited for the opportunity that my husband leaves in France in the good season, to have ... 

But to my surprise this year, my friend Sally came to see me, and took me with her a nice bag of quince, quince very yellow, very big, but especially especially, super scented. 

Oh! but what I was happy was more beautiful than a bouquet of flowers, I put them on my little kitchen counter, and every time I enter my little kitchen, I take one of these beautiful fruits to feel more ready this intoxicating smell, this sweet fragrance that emerges and which scrolls in my head a lot of ideas of recipes with these pretty quinces ... 

I stop dreaming, and I go back to you to give you the recipe of these delicious verrines that I saw on an English blog, I did not prick the recipe to the letter, because there were ingredients that I did not have on hand, or ingredients that I do not like to use in my kitchen ... 

a recipe that will take you a little time to achieve, but that will delight you, and that will awaken all your senses, of course, do not ignore this moment of pleasure when cooking these two layers, and the smells that perfume the whole house ...   


**Verrines d'automne Tapioca quince and gingerbread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/verrines-tapioca-coings-et-pain-depice-013.CR2_-1024x800.jpg)

Recipe type:  sweet verrines, dessert  portions:  6  Prep time:  60 mins  cooking:  45 mins  total:  1 hour 45 mins 

**Ingredients** the quince layer: 

  * 3 quinces, peeled, seeded and quartered 
  * 1 cinnamon stick 
  * ½ teaspoon of cardamom powder 
  * 2 cloves 
  * 50 gr of sugar 
  * ½ lemon 

the tapioca layer 
  * ⅔ a glass (200 ml) of tapioca pearls 
  * 2 glasses of water 
  * 2 glasses of whole milk 
  * 2 tablespoons of honey 
  * 1 teaspoon of cardamom 

decoration: 
  * 3 slices of [ gingerbread ](<https://www.amourdecuisine.fr/article-pain-d-epice-tres-moelleux-et-trop-bon.html> "fluffy gingerbread")



**Realization steps** layer of quinces 

  1. Place all the ingredients in a large saucepan and add enough water to cover the quince. 
  2. Bring the contents of the saucepan to a boil over high heat, then reduce the heat to a minimum, 
  3. Place a lid half on the pan and allow to reduce until the quince is very tender. 
  4. Crush the quince with the well-reduced syrup and set aside. 

layer of tapioca: 
  1. In a medium bowl, soak the tapioca pearls in water for one hour. 
  2. Drain and discard the water. 
  3. In a large saucepan over medium heat, bring the tapioca, milk, honey and cardamom to a boil and stir. 
  4. Lower the heat and simmer 20-25 minutes while stirring so that it does not stick to the bottom, and until the tapioca seems translucent. 
  5. Remove from heat and place a film of food over it to prevent film formation. 

assembly of verrines: 
  1. When serving, if not an hour before, cover the quince mixture in the bottom of 6 verrines, 
  2. When the tapioca is completely chilled, leave it over the quince layer. 
  3. decorate with roughly crumbled gingerbread. 



[ ![tapioca quince verrines and gingerbread 012.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/verrines-tapioca-coings-et-pain-depice-012.CR2_1-773x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/verrines-tapioca-coings-et-pain-depice-012.CR2_1.jpg>)
