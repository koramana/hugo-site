---
title: 'stuffed aubergines / Turkish recipe: karniyarik'
date: '2013-11-02'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/aubergines-farcies_thumb.jpg
---
[ ![stuffed aubergines / Turkish recipe: karniyarik](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/aubergines-farcies_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/aubergines-farcies.jpg>)

##  stuffed aubergines / Turkish recipe: karniyarik 

Hello everybody, 

here is a super delicious Turkish recipe, known as Karniyarik, or simply aubergines stuffed with minced meat. 

But the method of preparation of this recipe is very different, that I liked to try it, and I do not tell you what was good, the children and I, we really like it.   


**stuffed aubergines / Turkish recipe: karniyarik**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/aubergines-farcies_thumb.jpg)

Recipe type:  dish  portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 4 medium eggplants 
  * 4 peeled tomatoes 
  * 200 gr of minced meat (I did it at random, I do not remember exactly how much to stuff the 4 aubergines) 
  * ½ onion 
  * 2 cloves garlic 
  * 3 tablespoons of oil 
  * some sprig of chopped parsley 
  * 1 tablespoon of tomato paste 
  * salt, black pepper, and a pinch of coriander powder. 



**Realization steps**

  1. wash and cut the eggplant ends. 
  2. Peel the skin in strips once in two, and make a split in the middle. 
  3. sprinkle with salt, and let drain in a colander, for almost 30 min, they will disgorge a little water. 
  4. wipe with paper towel, and put some oil around. 
  5. cover with foil, in foil. 
  6. bake during. 
  7. prepare the stuffing, in a pan put some oil and cook the onion cut until it becomes translucent. 
  8. add the minced meat, cook well then add the chopped parsley, salt and pepper to taste. (pay attention to salt) 
  9. In the meantime, prepare a tomato sauce, cooking the tomato cut into cubes, in a little oil, add the crushed garlic, salt and pepper, and let cook until reduced water. 
  10. In a baking dish put the tomato sauce. then place the eggplants and stuff them with the minced meat. 
  11. bake for 15 minutes in preheated oven at 180 ° C 



[ ![stuffed aubergines / Turkish recipe: karniyarik](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/karniyarik_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/04/karniyarik_2.jpg>)
