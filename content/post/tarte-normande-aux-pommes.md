---
title: Norman apple pie
date: '2012-10-29'
categories:
- dessert, crumbles et barres

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tarte-nomade-1_thumb_11.jpg
---
##  Norman apple pie 

Hello everybody 

it is the period of pretty apples, and you have no excuses to realize this super delicious that Lunetoiles liked to share with us, a pretty ** Norman apple pie  ** . 

A recipe very easy, but above all, too good and more presentable in tasting for children, or even a dessert for his friends and guests. For my part I really like apple recipes, especially cakes, that's why you can find at home [ GENOISE MASCARPONE CARAMELIZED APPLES ](<https://www.amourdecuisine.fr/article-genoise-mascarpone-pommes-caramelisees-67729142.html>) [ With almonds and apples ](<https://www.amourdecuisine.fr/article-30256412.html> "Fluffy with almonds and apples \(2\)") , or why not a [ TATIN PIE WITH APPLES ](<https://www.amourdecuisine.fr/article-36968610.html>) , no I prefer a [ APPLE PIE WITH BUTTER ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-au-beurre-67056341.html>) ...... but I believe that you, like me, want the recipe for this sublime nomadic pie, so I will soon deliver it to you. and if you ask yourself the question [ what to do with apples ](<https://www.amourdecuisine.fr/article-que-faire-avec-des-pommes-100741309.html>) , click on the link. 

**Norman apple pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/tarte-nomade-1_thumb_11.jpg)

**Ingredients** Sanded almond paste: 

  * 180g of flour 
  * 1 pinch of salt 
  * 20 g of almond powder 
  * 50g of sugar 
  * 1 sachet vanilla sugar 
  * 80g of butter 
  * 1 egg 
  * 1 tsp. liquid vanilla 

Garnish: 
  * 3 apples 
  * 2 eggs 
  * 80 gr of sugar 
  * 2 bags of vanilla sugar 
  * 40 g of almond powder 
  * 20 cl of liquid cream 



**Realization steps** The shortbread with almond: 

  1. In a salad bowl, pour the flour, salt, almond powder, sugar, vanilla sugar packet and mix with your fingertips. Make a well and add the diced butter, sanded with your fingertips to get a big shortbread. 
  2. Add the egg and the liquid vanilla, knead until a homogeneous paste. Form a ball and reserve in the refrigerator for 2 hours. (I immediately spread it, filled in the mold and reserve the time to make the filling). 
  3. Spread the dough between two sheets of parchment paper, garnish a pie pan or pie circle, prick the bottom with a fork and reserve in the refrigerator. Preheat the oven to 190 ° C. 

The filling: 
  1. In a bowl whiten the eggs with sugar and vanilla sugar using an electric mixer, add the almond powder and the liquid cream. Mix well. 
  2. Remove the shortcrust pastry, arrange the apple slices, pack well. Add the preparation to the apples. Bake for 30 to 40 minutes. 
  3. Sprinkle with icing sugar. 


