---
title: Cracked without egg / Lemon sorbet
date: '2016-08-16'
categories:
- crème glacée, et sorbet
tags:
- Ramadan 2015
- ice cream
- Summer
- Easy recipe
- Algeria
- Without ice cream maker
- Fast Food

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/cr%C3%A9ponn%C3%A9-sans-oeuf.CR2_.jpg
---
[ ![creped without egg.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/cr%C3%A9ponn%C3%A9-sans-oeuf.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-creponne-sans-oeuf-sorbet-au-citron.html/creponne-sans-oeuf-cr2>)

##  Cracked without egg / Lemon sorbet 

Hello everybody, 

It is hot in Algeria at the end of the spring season, and on my Facebook group, I launched a small challenge that has the subject: ice cream, sorbet, and refreshing drinks ... I do not tell you the heap of recipes that girls have made for this topic. Ideas jostled each other, and delights were of all imaginations. 

Among the recipes there was that of the crepe, a lemon sherbet without egg white, I really liked the recipe, and I did not hesitate to realize it, especially since my father is at home, and he likes a lot the creped. 

In any case, this lemon sherbet is just to fall, the taste is incomparable and the most important is the taste of sugar that is just right ... I love when I make a recipe like that the first time, and that I do not find it too sweet or lacking in sugar. 

**Cracked without egg / Lemon sorbet**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/IMG_1861.jpg)

Prep time:  40 mins  total:  40 mins 

**Ingredients**

  * 1 glass of sugar (1 glass of 240 ml) 
  * 200 ml freshly squeezed lemon juice 
  * 240 ml of water 
  * 3 tablespoons whipped cream 



**Realization steps**

  1. beat all the ingredients with the mixer for at least 10 minutes 
  2. place in the freezer and remove every 30 minutes and whisk, then put back in the freezer. 
  3. you can do this at least 4 to 6 times. 
  4. and if you have an ice cream maker: leave the mixture in the freezer for at least 20 minutes. 
  5. remove the tank from the ice cream maker from the freezer, place the mixture in it and tumble for at least 10 minutes. 



[ ![creped without egg, lemon sorbet.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/cr%C3%A9ponn%C3%A9-sans-oeuf-sorbet-au-citron.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-creponne-sans-oeuf-sorbet-au-citron.html/creponne-sans-oeuf-sorbet-au-citron-cr2>)
