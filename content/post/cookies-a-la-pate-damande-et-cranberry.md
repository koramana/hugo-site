---
title: almond paste and cranberry cookies
date: '2015-11-04'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cookies-a-la-pate-damande-et-cranberry.jpg
---
[ ![almond paste and cranberry cookies](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cookies-a-la-pate-damande-et-cranberry.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cookies-a-la-pate-damande-et-cranberry-2.jpg>)

##  Cookies with almond paste and cranberry 

Hello everybody, 

Here is a little delight shared with us my friend Lunetoiles. These cookies are just falling out with pieces of [ almond paste ](<https://www.amourdecuisine.fr/article-pate-damande-maison-massepain.html>) and dry cranberries, I personally like everything that is dry cranberries, it's too good. 

[ ![almond paste and cranberry cookies](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cookies-a-la-pate-damande-et-cranberry-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cookies-a-la-pate-damande-et-cranberry-2.jpg>)

**almond paste and cranberry cookies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cookies-a-la-pate-damande-et-cranberry-1.jpg)

portions:  16  Prep time:  10 mins  cooking:  12 mins  total:  22 mins 

**Ingredients**

  * ¾ cup butter or margarine, softened = 180 g 
  * ¾ cup brown sugar = 150 g 
  * ½ cup of granulated sugar = 100 g 
  * 1 egg 
  * 1¾ cup all purpose flour (me, half whole wheat flour) = 325 gr 
  * ½ c. coffee baking soda 
  * 1 good pinch of salt 
  * 1 cup marzipan cut into pieces 
  * 1 cup dried cranberries 



**Realization steps**

  1. Preheat the oven to 175 ° C. 
  2. In large bowl, beat butter, brown sugar, granulated sugar, and egg with electric mixer on medium speed. Stir in flour, baking soda and salt. Stir in the pieces of marzipan and cranberries. 
  3. On an ungreased cookie sheet (me, baking paper), place the dough by spoonfuls using an ice cream scoop. 
  4. Bake 12 to 15 minutes or until golden brown. Cool for 2 to 3 minutes. Remove from cookie sheet and place on a wire rack. 



[ ![almond paste and cranberry cookies](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cookies-a-la-pate-damande-et-cranberry-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/cookies-a-la-pate-damande-et-cranberry-3.jpg>)
