---
title: Chocolate truffles / dates
date: '2017-03-06'
categories:
- idee, recette de fete, aperitif apero dinatoire

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/truffes-au-chocolat.jpg
---
##  Chocolate truffles / dates 

Hello everybody, 

here are super delicious and very melting chocolate truffles / dates that I made for the school party of my children, they were very happy my children to take this with them, hihihihi ... and I worked very very late at night, because I did not know until after they came back from school ... .. 

in any case. it was great to make these little delights, the only problem is to have the chocolate full fingers at night ... .. and hello the kilos .... 

I had to cover these cocoa truffles, as in the book from which I drew the recipe, but as I know that children prefer chocolate, I presented them in this way.   
Ingredients:   
for almost 70 truffles:   


**Chocolate truffles / dates**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/truffes-au-chocolat.jpg)

**Ingredients**

  * About 40 pitted dates (600 g) 
  * 100 gr of chocolate (the best will be 70% cocoa, but I put milk chocolate for children) 
  * 1 handful of hazelnuts 
  * 1 handful of almonds 
  * Zest and juice of 1 big orange 
  * 1 C. cocoa powder. 
  * 2 tbsp. cinnamon 
  * 1 teaspoon vanilla 
  * A pinch of salt 
  * black and white chocolate for decoration, otherwise cocoa powder. 



**Realization steps**

  1. Preheat oven to 175 ° C and grill without hazing hazelnuts and almonds for 10 minutes until golden brown. to remove from the oven. 
  2. rub the hazelnuts in a clean torch to remove the skin. 
  3. then crush the hazelnuts and almonds according to your taste, but not in large pieces 
  4. In a blender, mix the pitted dates and the orange juice to have a soft dough. 
  5. Add all ingredients except dark chocolate. 
  6. In a bain-marie, melt the chocolate and add to the mixture, and stir in thoroughly. 
  7. Place the day mixture in the refrigerator for a few hours or overnight (I had to put in the freezer to go faster). 
  8. Use a teaspoon, and your palms to make perfect truffle balls 
  9. put them on a rack. 
  10. Melt the chocolate for decoration in a bain-marie, and pour over the truffles, and let it dry. 
  11. for even more, I melted the white chocolate, which I put in a paper cone, and I drew these features. 


