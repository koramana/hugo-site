---
title: yassa chicken with couscous
date: '2013-04-19'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/yassa-poulet-033.CR2_1.jpg
---
![yassa chicken with couscous](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/yassa-poulet-033.CR2_1.jpg)

##  yassa chicken with couscous 

Hello everybody, 

chicken yassa, what do I like this recipe, I remember the first time I had to eat this, there are already more than 10 of that, I was still a student in Strasbourg, I had colleagues in the laboratory, Senegalese, who prepared this dish, and that we ate together at noon, when everyone goes to the restaurant ... 

it was too good, too tasty, and it must be said that in this period, I never ask the question: "I can have the recipe? ".... I must say that the kitchen was the last room where I could go ... hihihihi 

but when I made my blog, I really wanted to make this recipe ... and reading the recipes on the net, I was not conquered by the ingredients mentioned. 

and recently, I met a mother at school, who is a "Senegalese", cool ... .. is not it, besides, she does good cooking, so yesterday, I went to her house to to make a birthday cake for her son, and I asked him to make me chicken yassa. 

she presented this sauce with steamed couscous, it was a treat ... 

![Yassa-chicken-026.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/yassa-poulet-026.CR2_1.jpg)

**yassa chicken with couscous**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/yassa-poulet-031.CR2_1.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** for the marinade: 

  * 4 chicken legs 
  * 5 cloves of garlic 
  * 1 onion 
  * 1 pinch of black pepper 
  * half of a cube of chicken broth 

for the sauce: 
  * 4 big onions 
  * 1 tablespoon of natural mustard 
  * 1 tablespoon of mustard with the grains (optional, otherwise 2 tablespoons of natural mustard) 
  * 3 tablespoons of oil 
  * 1 cup of vinegar 
  * 1 green onion (optional, just for one more flavor) 
  * couscous steamed. 
  * red, green, and yellow peppers (optional, just for decoration) 



**Realization steps**

  1. start by preparing the marinade in the evening, pass garlic, onion, bouillon cube and black pepper, in a blender, otherwise crush the whole with mortar. 
  2. marinate chicken pieces in half of this marinade, and keep the rest. 
  3. put the chicken pieces in a bowl going to the fridge and cover it. 
  4. the next day, when preparing the dish, start by frying the chicken pieces in a little oil over low heat. brown on both sides until chicken is cooked through. 
  5. [ cook the couscous with steam ](<https://www.amourdecuisine.fr/article-cuisson-du-couscous-a-la-vapeur.html>) . 
  6. cut the cleaned onions into slices, separate the slices of rings to see rings. 
  7. in a large pot, cook onion until translucent. 
  8. mix in a small bowl, the mustard, the rest of the marinade, the vinegar. 
  9. pour this mixture on the onions, add a little water (3 tablespoons), 
  10. you can add sliced ​​green onions, let it simmer a little. 
  11. place the fried chicken pieces in the sauce, 
  12. let simmer for a few minutes, and remove from the heat 
  13. cut the peppers into slices, salt and fry 
  14. serve couscous garnished with this fried chicken and sauce and enjoy. 


