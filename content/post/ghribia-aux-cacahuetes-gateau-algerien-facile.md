---
title: ghribia with peanuts easy Algerian cake
date: '2014-09-29'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/ghribia-aux-cacahuetes-gateau-algerien-aid-2014.CR2_.jpg
---
[ ![ghribia peanuts cake algerian aid 2014.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/ghribia-aux-cacahuetes-gateau-algerien-aid-2014.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/gateau-aid-2014-ghribia-aux-cacahuetes.CR2_.jpg>)

##  ghribia with peanuts easy Algerian cake 

Hello everybody, 

This ghribia with peanuts is one of [ Algerian cakes ](<https://www.amourdecuisine.fr/gateaux-algeriens-orientales-modernes-fete-aid>) that I find very easy to realize at the approach of the Aid, I know that it is a cake that I will not miss, I know that I will have a good amount of this cake and that it is not going to me take a whole day to work piece by piece, and I know that it is cake that will delight any palate. 

The ghribia is a pride for me, because it is one of the first Algerian cakes I made in my life. I will never forget my mother's lesson, as I realized this delicious ghribia in the Constantine method, I mixed with my little hands the smen and the sugar, and I just waited for my mother to turn her back, to lick a little, without her noticing ... what is it delicious this cream, besides I kept this habit until today (that must be one of the causes that me pushes to prepare this cake often, lol). 

So my mother was there, to tell me, you have to mix until this cream triple volume, it's the secret to succeed the ghribia, and I always followed what she said to me, and I have always managed this delight. So do like me, do not rush to introduce the flour before having a cream beautiful unctuous, otherwise your ghribia may be as hard as a rock. 

on my blog, you will enjoy many recipes ghribia, you will have all the tastes, just follow your instincts to make the perfume you like: [ Ghribia has chickpea flour ](<https://www.amourdecuisine.fr/article-ghribia-a-la-farine-de-pois-chiche-gateau-sec-et-fondant.html> "ghribia has chickpea flour / dry and melting cake") , [ ghribia with hazelnuts ](<https://www.amourdecuisine.fr/article-ghribia-aux-noisettes-gateau-sec-algerien.html> "ghribia with hazelnuts / algerian dry cakes") , [ walnut ghribia ](<https://www.amourdecuisine.fr/article-ghribiya-ghribia-aux-noix-gateau-algerien.html> "ghribiya, walnut ghribia, algerian cake") , [ almond ghribia ](<https://www.amourdecuisine.fr/article-ghriba-aux-amandes-gateau-sec-aux-amandes-facile.html> "almond ghriba / easy almond dried cake") , [ ghribia with pistachios ](<https://www.amourdecuisine.fr/article-ghriba-a-la-pistache.html> "pistachio ghriba غريبة فستق") , [ ghribia algeroise oil ](<https://www.amourdecuisine.fr/article-ghribia-algeroise.html> "ghribia algeroise") and even more [ fondant dry cakes ](<https://www.amourdecuisine.fr/gateaux-secs-algeriens-petits-fours>)

**ghribia with peanuts easy Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/gateau-aid-2014-ghribia-aux-cacahuetes.CR2_.jpg)

portions:  40  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 1 kg of flour (almost 1 kg) 
  * 350 gr of smen 
  * 180 gr of icing sugar 
  * 200 gr roasted peanuts in powder. 
  * 150 ml of table oil 



**Realization steps**

  1. Sift the white flour 
  2. beat the smen and the sugar to have a creamy mousse 
  3. add the oil and whip again 
  4. add the peanuts in powder while whisking. 
  5. pick up the dough with flour. 
  6. form a pudding, and cut small lozenges, which you place on a floured tray in the oven 
  7. Bake in preheated oven at 180 ° C for 10-15 minutes depending on oven capacity 
  8. keep in a hermitic box. and enjoy with a good tea 



{{< youtube fe2FMVVnVZk >}} 

[ ![ghribia with peanuts cake help 2014.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/ghribia-aux-cacahuetes-gateau-aid-2014.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/ghribia-aux-cacahuetes-gateau-aid-2014.CR2_.jpg>)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
