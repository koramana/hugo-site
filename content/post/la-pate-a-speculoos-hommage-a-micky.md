---
title: the pasta with speculoos, tribute to Micky
date: '2010-10-12'
categories:
- Algerian cakes- oriental- modern- fete aid
- gâteaux sablés, ghribiya
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/10/rachid-034_thumb1.jpg
---
Hello everybody, 

![rachid 034](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/rachid-034_thumb1.jpg)   


I had a great desire to drip this speculoos dough that everyone is talking about, then I remembered that [ micky ](<http://mickymath.over-blog.com/4-categorie-10180772.html>) He had already prepared, in addition now that I found the speculoos. 

here are the ingredients: 

125gr of butter    
3/4 of a box of sweetened condensed milk    
200g of speculoos    
1 sheet of gelatin (it serves as an emulsifier)    
2 tbsp of lemon juice    
1 tablespoon of neutral oil 

I divided by two. 

soak the gelatin sheet in cold water for 10 minutes    
melt the butter in the microwave 

add the sweetened milk and crumbled biscuits, the oil and mixer 

heat the lemon juice and add the gelatin, pour it all in the dish and continue to mix until the preparation is smooth 

Thank you from the bottom of my heart to micky for all your recipes. 

she was a real treat, besides I had good use me, she served very well. 
