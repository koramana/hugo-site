---
title: chocolate cake with chocolate sauce
date: '2015-03-28'
categories:
- gateaux, et cakes
- recettes sucrees
tags:
- Yogurt cake
- Yogurt
- Cake
- Algerian cakes
- To taste
- desserts
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-danette.jpg
---
[ ![chocolate cake with chocolate sauce](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-danette.jpg) ](<https://www.amourdecuisine.fr/article-gateau-la-danette-au-chocolat.html/gateau-danette>)

##  chocolate cake with chocolate sauce 

Hello everybody, 

Everybody gets up for the danette, I'm sure you remember that slogan, well today, it's everyone, sits around a chocolate cake and more. 

This delicious cake to the danette was prepared by my friend **Wamani Merou** , who shares it with great pleasure with us on the blog. I hope that you will like to realize this recipe, and above all you will succeed without any problem. 

What's good with this cake is that you will follow the same principle of preparing a yogurt   


**chocolate cake with chocolate sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/gateau-danette.jpg)

portions:  8-12  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 3 eggs 
  * 1 Pot of chocolate danette 
  * 1 Tablespoon of bitter cocoa 
  * 50 g of melted butter 
  * 1 sachet of baking powder 
  * 1 sachet of vanilla sugar 
  * 2 jars of sugar 
  * 3 pots of flour 
  * 1 pinch of salt 



**Realization steps**

  1. preheat the oven to 160 ° 
  2. pour the contents of the pot of danette in a salad bowl.   
keep a jar because it will serve you a doser 
  3. add sugar and vanilla sugar and mix well 
  4. introduce the flour and mix after each addition of flour pot. 
  5. add the cocoa, the pinch of salt, mix well. Now add the eggs and butter. 
  6. after mixing well, add the yeast and stir for another minute. 
  7. pour the dough into a buttered mold. 
  8. bake for about 30 minutes 


