---
title: Chocolates with pti taste snickers of Celia
date: '2009-05-30'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/05/209188051.jpg
---
![S7301939](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/209188051.jpg)

![11410773_p](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/209188211.gif)

I ventured with what I had on hand, to make this chocolate as she said to Snickers' taste, and it's too good to say.   
the photos are taken with my cellphone, because my husband had to bring the camera with him. 

Ingredients: 

  * 360 g of white chocolate 

  * 140 g of peanut butter 

  * 400 g of milk chocolate. 




![86](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/2091877611.jpg)

Preparation 

Melt the two chocolates in the microwave until a smooth mixture (do not forget to shake the chocolate from time to time so that it melts well without burning). 

Add the peanut butter in the melted white chocolate. Mix. 

Line a rectangular baking dish with parchment paper and alternate spoonfuls of white chocolate and dark chocolate in the mold. 

![87](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/2091866911.jpg)

Shake gently to spread the mixture and pass a knife tip in one direction then the other several times in succession to give a marbled effect. It is important to make a maximum of mottling because the texture of 2 masses is different. 

![88](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/209186971.jpg)

Let stand at room temperature until the marbled hardens, then cut into small pieces. 

have a good meal 

![bonj1](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/209188271.gif)
