---
title: Chicken legs with spices
date: '2011-03-02'
categories:
- gateaux d'anniversaires, de fetes, celebrations
- cakes and cakes
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/poulet-aux-epices_thumb1.jpg
---
##  Chicken legs with spices 

Hello everybody, 

Here is a recipe that comes directly from Germany, one of my readers, to love to share this recipe with you, through my blog, and it makes me happy that my readers find through my link a small window to other readers it's really warming up. 

so it's a zola recipe she saw on TV and has tested for you.   


**Chicken legs with spices**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/poulet-aux-epices_thumb1.jpg)

**Ingredients** Ingredients for 4 persons: 

  * 4 chicken legs 
  * 1 teaspoon and ½ cup of Cardamom 
  * 1 teaspoon and ½ cinnamon 
  * 2 tablespoons of black pepper 
  * 1 teaspoon sweet red pepper 
  * 1 cup of hot pepper powder 
  * 1 tbsp ground oregano 
  * 1 teaspoon of lemon zest 
  * 1 teaspoon of honey 
  * 5 tablespoons olive oil 
  * Salt 
  * 2 cloves garlic 
  * 4 big potatoes 
  * 1 medium zucchini 
  * 2 tomatoes 
  * rosemary. 



**Realization steps**

  1. Preparation 
  2. clean the chicken, cut each leg in half and leave aside 
  3. in a terrine mix all the spices, Olive oil, and garlic crush to get a paste 
  4. put the chicken legs in and massage well so that the chicken is covered with the marinade 
  5. put in a soft plastic freezer bag and cool for an hour or more. 
  6. in the meantime prepare the vegetables, cut the potatoes and zucchini ½ cm slice, tomatoes too. 
  7. Oil an oven dish, put the potatoes at the bottom. 
  8. put the rosemary stems (optional) and then salt up the chicken legs (in the middle of the dish) 
  9. and in the sides put the zucchini and tomatoes, 
  10. salt and pepper again. 
  11. put in preheated oven 220 ° C for 40 to 50 minutes (depending on the oven) 
  12. after 20 minutes of cooking, cover the dish with aluminum foil and finish cooking. 



thank you my dear for this delicious dish, and this recipe to try very quickly 

Thank you for your visits and your comments 

a la prochaine recette. 
