---
title: Chicken Cornets
date: '2012-04-24'
categories:
- amuse bouche, tapas, mise en bouche
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/cornets-au-poulet-038_thumb1.jpg
---
##  Chicken Cornets 

Hello everybody, 

chicken cones, a recipe that I often prepare when I have the rest of a roast chicken, or a piece of chicken breast, which will not be enough to make a dish. 

so right now, I put a roll of puff pastry out of the freezer, and in the kitchen to make my recipe. 

I really like cornets, they are always good, and easy to achieve, the stuffing is according to your taste and medium, you will find on my blog: 

[ cheese cones ](<https://www.amourdecuisine.fr/article-les-cornets-a-la-creme-au-fromage-81580877.html>)

[ tuna cones ](<https://www.amourdecuisine.fr/article-cornets-sales-a-la-creme-au-thon-49023773.html>)

**Chicken Cornets**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/cornets-au-poulet-038_thumb1.jpg)

**Ingredients**

  * 1 roll of puff pastry 
  * remaining chicken, or golden chicken breast in a little oil. 
  * cream [ Bechamel ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html>)
  * Edam cheese, or whatever you like. 



**Realization steps**

  1. roll out the puff pastry, cut into strips of 1.3 cm wide 
  2. brush the part that is going to be outside with a little egg white 
  3. roll it around a stainless steel cone (the part brushed outwards) 
  4. cook the cornets in a preheated oven at 160 degrees until the cornets turn a beautiful golden color 
  5. take them out of the oven and allow to cool well before unmolding. 
  6. to unmold, gently hold the dough cone in your hand, and with the other hand gently rotate the stainless steel cone to separate it from the dough. 
  7. after preparing the béchamel, add in a little grated Edam 
  8. cut the chicken into small pieces 
  9. mix with custard cream. 
  10. place the mixture in a piping bag, and fill the cones. 
  11. you can decorate the top of the cone with a little grated Edam. 
  12. serve with a nice fresh salad. or present with a chorba frik. 


