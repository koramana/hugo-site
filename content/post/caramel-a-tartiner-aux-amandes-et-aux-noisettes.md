---
title: caramel spread with almonds and hazelnuts
date: '2013-11-03'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- cakes and cakes
- idea, party recipe, aperitif aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/caramel-a-tartiner-1.jpg
---
![caramel-a-spread-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/caramel-a-tartiner-1.jpg)

Hello everybody, 

here is a delicious spread, a creamy caramel sauce, the endless crunch of roasted almonds and hazelnuts .... 

it's a recipe that Lunetoiles sent me for a while now, when she had prepared delicious nutella pancakes .... and that I did not have time to post it to you. 

**caramel spread with almonds and hazelnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/caramel-a-tartiner-aux-amandes-et-noisettes.jpg)

Recipe type:  spread  portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 100 g of sugar 
  * 50 g of water 
  * 1 tablespoon of butter 
  * 60 g of heated cream 
  * 3 c. hazelnuts and crushed almonds 



**Realization steps**

  1. In a saucepan, put the water and the sugar. 
  2. Cook everything until you get an amber color,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/DSC06530.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/DSC06530.jpg>)
  3. add almonds and hazelnuts, let caramelize a little. 
  4. Out of the heat add the butter, stir and add the hot cream. 
  5. mix well and let it simmer for 2 minutes. 



![caramel-a-tartiner.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/caramel-a-tartiner.jpg)
