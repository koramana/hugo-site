---
title: Fabric softener for laundry
date: '2007-12-24'
categories:
- houriyat el matbakh- fatafeat tv
- salads, salty verrines

---
**![bonj10](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201447751.gif) **

**Baking soda can be used as an ecological and economical liquid softener.**

![th_washing1](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201447791.gif)

**Just add 1 tbsp (15 g) of baking soda to each rinse.**

![th_washing](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201447921.gif)

** **

![bye](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/201448301.gif)
