---
title: fish and chips recipe
date: '2017-02-23'
categories:
- cuisine diverse
- Cuisine par pays
- recettes aux poissons et fruits de mer
tags:
- Fish fillets
- crunchy
- Fish
- cod
- Algeria
- Easy cooking
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/02/recette-Fish-and-Chips-2.jpg
---
![Fish and Chips recipe 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/recette-Fish-and-Chips-2.jpg)

##  fish and chips recipe 

Hello everybody, 

Yes, I had to share the fish and chips recipe with you! This unmatched recipe of English cuisine, the most popular street food in England. Fish and chips were often eaten on our trips to Bath and Somerset, but since I knew that for a crisp and crispy crust, we tend to put beer. To no longer be in doubt, I started making my fish and chips at home. 

At first, I had tried a pasta recipe with water, which we had not liked at home, then replaced with milk, and it was a little thick as a crust. I had seen a recipe on TV, with lemonade or soda, but my children had a little detected the acid taste of lemonade (which I was surprised, because I found that s' was good) 

In the end, I tried the recipe with coconut cola, and to my astonishment, they liked it ... It's difficult children. But since the time I wanted to share with you this recipe, it was downright impossible, especially when the kids are at home, I can not do the pictures while they are there waiting to be served. 

This time, I made my recipe fish and chips for two, for lunch while the children were at school, which allowed me to make the pictures a little more comfortable ... well not so much to I'm comfortable, because my husband rush me, he loves that his dishes are always hot! so you imagine the scene! I could hardly do 5 or 6 more photos. 

**fish and chips recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/02/recette-fish-and-chips-1.jpg)

**Ingredients**

  * potatoes cut into medium fries 
  * pieces of white fish (cod, cod ...) 
  * salt and black pepper from the mill 

for the dough: 
  * 330 ml of soft drink to taste (here Coca Cola 0) 
  * 1 glass of flour (a 240 ml glass) 
  * 3 c. coffee baking powder 
  * 1 C. honey coffee 
  * salt and black pepper from the mill. 



**Realization steps**

  1. peel, and cut the potatoes into medium fries, and place them in the water 
  2. dry the pieces of potatoes well on a clean cloth. 
  3. fry in oil preheated to 160 ° C for 5 to 6 minutes. 
  4. drain on paper towels. 

prepare the dough: 
  1. mix flour, salt, black pepper, baking powder 
  2. add the pop and honey, mix well to have a nice smooth dough. 
  3. salt and pepper the pieces of fish. have fun. 
  4. dip them in the dough, and fry in a hot oil, until the crust has a nice golden color. 
  5. take the chips and cook them in an oil preheated to 190 ° C until they have a nice color and a crispy crust. 
  6. serve immediately with the fish pieces and mayonnaise or ketchup ... personally I like it with tartar sauce. 


