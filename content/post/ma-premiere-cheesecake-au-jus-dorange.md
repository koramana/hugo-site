---
title: my first cheesecake with orange juice
date: '2007-12-06'
categories:
- crepes, waffles, fritters

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/12/197823651.jpg
---
& Nbsp; first of all I have to thank imane a lot, for helping me to design this cake, besides I dedicate it to his son rayan on the occasion of his second birthday: & nbsp; RAYANE and here are the ingredients: for the genoise: (1 small mold) 2 eggs 60 gr caster sugar 60 gr flour 1/2 cb yeast beat the egg well with the sugar, then add the flour and baking powder, cook in a very hot oven for 10 to 15 minutes (depending on the capacity of your oven) at the oven's baking tray, well & gt; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![93i1ddb2.gif](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/th_93i1ddb21.gif)

first of all I have to thank imane a lot, for having helped me to design this cake, besides I dedicate it to his son rayan on the occasion of his second birthday: 

![happy Birthday](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/197811851.gif)

**_RAYANE_ **

and here are the ingredients: 

for the genoise:  (1 small mold) 

  * 2 eggs 
  * 60 gr of caster sugar 
  * 60 gr of flour 
  * 1/2 cac of baking powder 



beat the eggs well with the sugar, then add the flour and baking powder, cook in a hot oven, for 10 to 15 minutes (depending on the capacity of your oven) 

at the exit of the cake of the oven, well soak it with orange juice (a will, do not hesitate to put all the bottle, hehehehe) 

put aside and prepare the first layer of cream, the ingredients: 

  * 300 gr of fresh cheese 
  * 70 gr of caster sugar 
  * 1 egg 
  * 1/2 cac of vanilla 
  * 1 spoonful of vegetable gelatine boils in 3 spoonfuls of water. 



beat the cheese with half of the sugar, until it becomes very soft. beat the egg with the other half of the sugar and add the vanilla. 

mix the first mixture with the second until the total incorporation of the two, then add the gelatin, and pour this mixture on your genoise whose edges are protected by a turn of moulde demountable, and put in the refrigerator. 

Now prepare the last layer of orange juice sauce. 

It's good depending on the thickness of the layer you want, I want a little layer, so the ingredients are: 

  * 1 glass of orange juice 
  * sugar according to taste 
  * 1 cac of vegetable gelatin 



I mix the three ingredients well, and bring to the boil, to the cooling of the mixture, I pour it on the cream of cheese, I use for that a spoon so that the layers infiltrate. 

and in the fridge for a whole night. 

![Photo_0063](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/197823651.jpg)

photo taken by mobile phone (my device is in France) 

I can tell you that this cake was a great success, very very good, because the taste of orange juice in the genoise was great, and the cream in the middle, was super tender and melted in the mouth, not to mention the last layer I do not tell you, yamiiiiiiiiiiiiii 

bonne appitie. 
