---
title: mini tartlets caramel and chocolate
date: '2017-07-29'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
tags:
- Algerian cakes
- Delicacies
- To taste
- Breakfast
- Algeria
- Cakes from L Aid
- Dry Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/07/mini-tartelettes-caramel-chocolat-2.jpg
---
![mini caramel chocolate tartlets 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/07/mini-tartelettes-caramel-chocolat-2.jpg)

Hello everybody, 

These mini tartlets caramel and chocolate have made a great success on my youtube channel (register you, if you have not done so) that I told myself that I had to share with you on my blog. 

I made it to accompany the tea for my son's birthday. The photos are not at the top, because I took the photos at sunset and there is more for me to remake other photos. 

Anyway, these mini tartlets caramel and chocolate, I absolutely recommend them. They are to fall. Even if you think that between chocolate, caramel and broken pasta it may be too sweet, well no, the taste is just perfect, and sugar too. 

Although here in Algeria, I did not find chocolate rich in cocoa 75% or 65%, but with the chocolate I found, the ganache was perfect. 

**mini tartlets caramel and chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/07/mini-tartelettes-caramel-chocolat.jpg)

**Ingredients** for the pasta: 

  * 250 gr of margarine 
  * 1 glass of icing sugar (220 ml) 
  * 1 egg 
  * 1 pinch of salt 
  * 1 pinch of baking powder 
  * 1 C. vanilla coffee 
  * flour 

for garnish: 
  * a box of Nestlé caramel 
  * 200 ml of fresh cream (for the ganache) 
  * 200 gr of dark chocolate (for the ganache) 
  * walnut kernels for decoration. 

equipment: 
  * mold for mini tarts 



**Realization steps** start by preparing the dough: 

  1. using a spatula, stir in the sugar with the margarine, which is at room temperature. 
  2. when the mixture is homogeneous, add the egg and mix again. 
  3. add the baking powder, vanilla and pinch of salt. 
  4. now add the flour slowly, until a dough that picks up well and is not too sticky. 
  5. take the equivalent of a small nut of the dough, and go into the mold to form the bottom of tartlets. 
  6. prick well with the fork and cook in a preheated oven at 180 ° C until golden brown. 
  7. unmold the pie shell and allow them to cool well. 
  8. fill the pie stock with the equivalent of a teaspoon of nestled caramel. 
  9. crush the dark chocolate, place it in a large bowl. 
  10. warm up the liquid cream, pour over the chocolate, leave for 5 minutes then mix with a spatula to melt the chocolate. 
  11. fill the rest of the tart with the chocolate ganache. 
  12. place in the fridge for a few minutes and garnish with the walnut kernels. 



Note * With the prepared amount we get almost 30 tarts (depending on the size of the molds)   
* the cakes are kept cool, if you are not numerous, keep the pie base empty in a hermetic box, and 1 to 2 hours before the presentation you can garnish them. ![mini caramel chocolate tartlets 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/07/mini-tartelettes-caramel-chocolat-1.jpg)
