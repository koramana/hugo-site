---
title: easy chocolate mousse
date: '2013-11-22'
categories:
- crepes, donuts, donuts, sweet waffles

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/mousse-double-chocolat-003_thumb.jpg
---
Hello everybody, 

I hear the word chocolate, and when I eat the chocolate mousse, I feel myself flying, each spoon brings me back more than the spoon that preceded it. 

I present you my recipe for chocolate mousse, I like to make a duet, because I love this marriage in taste of white chocolate mousse and dark chocolate mousse .... 

You do not have to do both duets like that, you can prepare the verrines of the white chocolate mousse or those of the dark chocolate mousse. 

**easy chocolate mousse**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/mousse-double-chocolat-003_thumb.jpg)

Recipe type:  dessert, verrines, mousse  portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients** white chocolate mousse layer: 

  * 100 g of white chocolate 
  * 300 ml liquid whole cream 

dark chocolate mousse layer: 
  * 3 eggs + 3 egg whites 
  * 200 g dark chocolate with 70% cocoa 
  * 70 g of butter 
  * 60 g caster sugar 



**Realization steps** first layer: 

  1. Grate the white chocolate into fine chips. 
  2. assemble the chips in a bowl. and put in a bain-marie, Mix remove from the heat as soon as the chocolate is melted. 
  3. pour 100 ml liquid cream into a small saucepan and bring to a boil. Let cool, out of the fire. Add the melted chocolate cream in a thin stream while stirring briskly for 3 minutes. 
  4. Pour in the remaining 200 ml liquid cream. With an electric mixer, work until it is firm. (You can place a bowl for 10 minutes in the freezer before whipping the cream). 
  5. Pour two large spoonfuls of whipped cream into the chocolate and mix quickly with a wooden spoon to obtain a soft chocolate cream. 
  6. Finally, add the remaining whipped cream by gently lifting it. 
  7. Place the mousse in 6 verrines in the refrigerator and let it cool for about 1 hour before serving. 

second layer: 
  1. Separate the whites from the yolks of the 3 eggs 
  2. melt the chocolate in a bain-marie 
  3. add the butter in small pieces, mix 
  4. add the egg yolks and whisk to obtain a smooth mixture 
  5. in a salad bowl, whisk the 6 egg whites with sugar 
  6. incorporate them in the first preparation, gently so as not to drop the foam 
  7. pour over the white foam chocolate mousse. 
  8. garnish with dark chocolate grated from above 
  9. Reserve at least 2 hours in the refrigerator 


