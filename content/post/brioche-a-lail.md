---
title: garlic brioche
date: '2015-09-08'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/brioche-a-l-ail-013-263x300.jpg
---
##  garlic brioche 

Hello everybody, 

A very delicious recipe that I saw on a TV channel, I ran quickly to note the ingredients on the first piece of paper that I found on hand (thankfully it was not the checkbook of my husband , hihihihi) 

And here is the first opportunity I realized, the crumb is sublime, and the taste of garlic if like me you like garlic, huuuuuuuuuuuuum ... I do not tell you. 

**garlic brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/brioche-a-l-ail-013-263x300.jpg)

portions:  12  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for 12 buns: 

  * 400 g of wheat flour 
  * 20 g caster sugar 
  * half a teaspoon of salt 
  * 1 cup instant yeast 
  * 260 g of milk (I mean gram) 
  * 30 g melted butter 

the garlic filling: 
  * 60 g of soft butter 
  * salt (if the butter is not salted) 
  * 8 garlic cloves, finely chopped 
  * 2 teaspoons dried parsley (or fresh) 



**Realization steps**

  1. mix dry ingredients 
  2. wet with warm milk 
  3. knead a little 
  4. add the melted butter, and knead again 
  5. and leave double under a towel, in a sheltered place drafts 
  6. in the blinder bowl, reduce the garlic, butter and parsley cream 
  7. preheat the oven to 180 degrees C 
  8. form 12 balls with the dough 
  9. let it inflate again 
  10. make a long cut with the knife 
  11. fill it with the garlic garnish 
  12. cook 15 to 20 minutes depending on your oven 



une recette que je vous conseille vivement 
