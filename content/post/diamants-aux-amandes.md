---
title: Almond Diamonds
date: '2013-01-02'
categories:
- idea, party recipe, aperitif aperitif

---
A cake, rather small oven that I completely forgot, but I removed them from my archives for you, I know that the majority does it in a push, but not serious I post it anyway. my ingredients: 250 gr of 190 flour of half-salted butter (or normal, depending on what you have) 100 gr of powdered sugar 1 sachet of vanilla sugar (I put the almond extract) 1 yellow of egg 100 gr of crystallized sugar 80 gr of grated almonds and crusts Work the butter until you get an ointment texture, incorporate the cream. 

##  Overview of tests 

####  please vote 

**User Rating:** 4.25  (  1  ratings)  0 

A cake, rather small oven that I completely forgot, but I removed them from my archives for you, I know that the majority does it in a push, but not serious I post it anyway. 

my ingredients: 

  * 250 gr of flour 
  * 190 half-salted butter (or normal, depending on what you have) 
  * 100 gr of powdered sugar 
  * 1 sachet of vanilla sugar (I put the almond extract) 
  * 1 egg yolk 
  * 100 gr of crystallized sugar 
  * 80 gr of grated and crushed almonds 



To work the butter until obtaining an ointment texture, to incorporate the powdered sugar and the vanilla sugar.   
Stir in the flour quickly and gently. add the almonds, knead quickly. 

Wrap the dough in food film and leave 30 minutes cool (you can make two sausages), the sausage must be between 3 and 4 cm in diameter because sometimes the dough inflates when cooking and you may have large pieces   
On a work plan shape the balls of dough into two rolls of 4 cm in diameter.   
Pack in cling film and leave two hours cool.   
Preheat the oven to 180 ° (th6). 

Beat the egg yolk and use a brush to brush the rolls and roll them in the crystal sugar.   
Cut biscuits 1.5 cm thick or as desired and place on a baking sheet lined with parchment paper.   
Space them 3 cm and cook for 15 minutes in two batches. 

bonne dégustation 
