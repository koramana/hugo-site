---
title: Qmirates, small moons with peanuts »قميرات» Algerian cake
date: '2017-06-24'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
tags:
- Algerian cakes
- Algeria
- Algerian patisserie
- delicacies
- Aid
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/qmirate-012_thumb1.jpg
---
[ ![Qmirates, small moons with peanuts "قميرات" Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/qmirate-012_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-36019686.html>)

##  Qmirates, small moons with peanuts »قميرات» Algerian cake 

Hello everybody, 

When we were little, my mother always made us this little delight, which is very much like ghribiya, and I loved it thoroughly, and here is the opportunity to share it with you. The Qmirates, which means small moons, with peanuts, as melting as a shortbread, and super tasty. 

Try this recipe, you are well liked, I tell you, in addition, at the time of shaping is a breeze. 

**Qmirates, small moons with peanuts "قميرات" Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/qmirate-012_thumb-300x224.jpg)

portions:  30  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 1 measure of ghee, smen 
  * 1 measure icing sugar 
  * 1 measure of finely ground peanuts 
  * of flour 
  * egg white 
  * peanuts coarsely ground. 



**Realization steps**

  1. we start by mixing our ghee with the icing sugar until we have a nice mousse, 
  2. the peanut powder is added, and after the flour until a manageable paste is formed. 
  3. flatten the dough with a roll, until you obtain a circle of 1 cm in height, and start cutting half moons with a water glass, or a piece cutter.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/qmirate-008_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-36019686.html/qmirate-008_thumb1>)
  4. brush the top of the cakes with egg white and cover with the coarsely ground peanuts. 
  5. place in a baking tray, and cook without over-browning them, watch these cakes burn quickly from below, well guarded 



good degutation 

[ ![qmirate 014](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/qmirate-014_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-36019686.html>)

ces gâteaux se conserve très bien dans une boite hermétique. 
