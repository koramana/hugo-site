---
title: ros bratel - beans in sauce
date: '2018-01-11'
categories:
- vegetarian dishes
tags:
- tajine

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/ros-bratel-%E2%80%93-feves-en-sauce.jpg
---
![ros bratel - beans in sauce](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/ros-bratel-%E2%80%93-feves-en-sauce.jpg)

##  ros bratel - beans in sauce 

Hello everybody,  beans in sauce    
Here is a very delicious dish of the cuisine of Eastern Algeria Ros Brat (do not ask me the meaning of this word, I always learned it like that, but I never really wanted to know what that meant). A dish that comes more exactly from Constantine (my hometown), a delicious dish or stew of fresh green beans cooked in reduced sauce that goes perfectly with a good [ Matloue slab ](<https://www.amourdecuisine.fr/article-41774164.html>) (or pancake with baker's yeast, it looks like [ Moroccan batbout ](<https://www.amourdecuisine.fr/article-batbout-marocain.html> "Moroccan Batbout "بطبوط مغربي"") ) or with a good fresh bread. 

This recipe of fresh green beans cooked in a creamy tomato sauce presented with a drizzle of olive oil towards the end, and decorated with some sprigs of coriander is to eat without moderation, a vegetarian dish very greedy and tasty which you will like well, guaranteed, lol 

**ros bratel - beans in sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/ros-bratel-%E2%80%93-feves-en-sauce-1.jpg)

Recipe type:  dish, Algerian cuisine  portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 1 kg of tender beans 
  * 1 bunch of fresh coriander 
  * oil 
  * 1 onion 
  * 3 to 4 cloves of garlic 
  * 1 cup of paprika 
  * 1 case of canned tomato 
  * 1 fresh tomato   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/ros-bratel-feve-en-sauce21.jpg)
  * salt, black pepper, my spice coriander / garlic 



**Realization steps**

  1. Cut the washed and toasted beans into small pieces 
  2. put them in a stock, with oil, add crushed garlic and onion rapper, diced fresh tomato, chopped coriander, salt, pepper and paprika, dilute canned tomato in a little water and add it and mix 
  3. leave a little macerated on low heat, then cover with a little water just to cover the beans, do not submerge water. 
  4. cook on low heat until the beans are cooked and the water is completely evaporated. 
  5. serve hot, and enjoy a sublime taste of bean sauce incomparable 



![ros bratel - beans in sauce 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/ros-bratel-%E2%80%93-feves-en-sauce-2.jpg)
