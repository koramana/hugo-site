---
title: Panna cotta coconut milk / chocolate cream
date: '2011-09-15'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/09/panna-cotta1.jpg
---
here is a dessert that we will eat and eat with pleasure and great tasting. 

and every time I buy myself a box of coconut milk, I say to myself, I'm going to make a delicious or a delicious panna cotta (I do not know if it's a masculine or feminine name) I leave the care to my expert friends in the matter. 

I do not remember where I had ever seen a panna cotta with coconut milk, and the idea is still in my mind. 

so without delay I deliver you this delightful freshness to present on any occasion .... you will not be disappointed. 

ingredients: 

panna cotta with coconut milk: 

-200 ml of milk 

-200 ml of fresh cream 

-200 ml of coconut milk 

\- 40 grams of brown sugar. 

\- 4 grams of glatine 

chocolate cream: 

500 ml of milk 

1 cup soup with maizena 

1 tablespoon of brown sugar 

1 Vanilla coffee 

100 grams of dark chocolate 

preparation of panna cotta 

Melt the gelatin in cold water 

heat the mixture of liquid and sugar over medium heat 

When the mixture comes to a simmer, turn off the heat and add the drained gelatin. Mix well. 

Pour in small glasses and let cool (to have the effect tilt, just place your glasses on the side of a book, be careful that it does not fall, and do not blame me after, hihihihih)   
\- When the mixture is at room temperature, put in the refrigerator and let it take at least 3 hours. 

![panna cotta.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/panna-cotta1.jpg)

for the chocolate cream: 

Mix cornstarch, sugars and the equivalent of 2 tablespoons of milk in a bowl. Book. 

Bring the remaining milk to a boil and remove from heat. Dip the chocolate pieces in the milk and leave them for a few minutes before mixing. 

Pour into the pan the sugar-cornstarch mixture and bring to a boil while beating constantly. 

Let it boil for a minute - time to thicken the mixture - and pour over your panna cotta slowly 

it is best to remove the glasses from the fridge a little before, so that the glasses are at room temperature and does not burst with the addition of hot chocolate cream. 

leave tiedir before putting in the fridge, serve with a little Chantilly Creme 

merci mes amis pour tout vos commentaires 
