---
title: balouza with orange - balouza dessert Algerian
date: '2015-04-22'
categories:
- cuisine algerienne
- dessert, crumbles et barres
- recette de ramadan
- verrines sucrees
tags:
- Ramadan
- Orange tree Flower water
- Ramadan 2015
- desserts
- verrines
- Holidays
- Dessert
- flan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balouza-1.jpg
---
[ ![balouza has the orange](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balouza-1.jpg) ](<https://www.amourdecuisine.fr/article-balouza-a-lorange-balouza-dessert-algerien.html/balouza-1>)

##  balouza with orange - balouza dessert Algerian 

Hello everybody, 

Balouza is a delicious Algerian dessert, which is very similar to [ mhalbi ](<https://www.amourdecuisine.fr/article-mhalbi-constantinois-creme-dessert-au-riz-en-video.html> "Mhalbi constantinois: rice dessert cream video") but which is made from cornstarch instead of rice powder. We can say that the balouza and the Algerian sister of the oriental mouhalabiya, hihihiih I really like this family link. 

In eastern Algeria, and especially in Constantine, the balouza is a very famous dessert, it was even presented at weddings as a dessert perfumed with orange blossom water homemade. In any case for me I always tasted this dessert at the time during Ramadan evenings, if my mother was not the mhalbi, it was automatically the balouza, of course all this well before the small renovation The Flans appear, which for some families are the typical dessert in the month of Ramadan. 

Returning to this Balouza has orange, or even Without orange (according to your choice) that **My cooking my passion** shared with us, I'm sure that this recipe is the adopted one, and this version is just a delight.   


**balouza with orange - balouza dessert Algerian**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balouza-1.jpg)

portions:  4-6  Prep time:  10 mins  cooking:  5 mins  total:  15 mins 

**Ingredients**

  * 1 L of milk 
  * 1 glass of orange blossom water 
  * 5 tablespoons of sugar 
  * 5 tablespoons of maizena 

layer of orange cream: 
  * 1 glass of orange juice 
  * 1 tablespoon of maizena 
  * 1 box of vanilla custard flan 
  * 1 tablespoon of sugar 



**Realization steps** Start by preparing the first layer: 

  1. place all ingredients in a heavy-bottomed saucepan 
  2. stir with a wooden spatula over medium heat until the mixture has thickened 
  3. pour in verrines and let take then put in the fridge.   
You can stop at this stage, and decorate your balouza with cinnamon powder   
[ ![balouza without the orange](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balouza.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41636>)
  4. Preparation of the orange layer: 
  5. place the remaining ingredients in a heavy-bottomed saucepan. 
  6. stir over medium heat until thickened. 
  7. fill the glasses with the certainty that the first layer would have taken 
  8. Keep chilled until serving. 



[ ![balouza has the orange](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/balouza-a-lorange.jpg) ](<https://www.amourdecuisine.fr/article-balouza-a-lorange-balouza-dessert-algerien.html/balouza-a-lorange>)
