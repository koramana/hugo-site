---
title: clafouti apples
date: '2013-11-24'
categories:
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/clafouti-au-pommes-011_thumb.jpg
---
Hello everybody, 

So this clafoutis, huuuuuuuuuuuuuum a delight, the children have to love, and it made me happy, at least I will not eat it all alone. 

**clafouti apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/clafouti-au-pommes-011_thumb.jpg)

portions:  8  Prep time:  20 mins  cooking:  45 mins  total:  1 hour 5 mins 

**Ingredients**

  * 4 apples (994 gr) 
  * 80 gr of butter 
  * 12 cl whole liquid cream 
  * 12 cl of whole milk 
  * 4 eggs 
  * 80 gr of powdered sugar 
  * 80 gr of cornflour 
  * 1 teaspoon cinnamon 

For the topping: 
  * apple jelly, or jam 



**Realization steps**

  1. In a saucepan pour the milk and add the butter in pieces. 
  2. Melt over low heat and add the cream. Mix and let cool. 
  3. In a bowl whisk the whole eggs with the sugar. Add starch and cinnamon. 
  4. Mix and add the previous preparation while continuing to mix. Book. 
  5. Preheat your thermostat oven 6 (180 ° C) up and down position. 
  6. Separate apples into 4, remove skin and seeds. 
  7. Cut the quarters in thin slices ... and arrange them on a non-stick plate (or in a pie plate). 
  8. Before pouring the preparation give a boost in the mixture because the starch is deposited at the bottom. 
  9. Bake the dish for 45 minutes. 
  10. Take out the clafoutis from the oven and brush the topping on the apples, it will give them a nice shine. 



**![clafouti-to-apples-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/clafouti-au-pommes-11.jpg) **

thank you for your comments and visits 

bonne journee 
