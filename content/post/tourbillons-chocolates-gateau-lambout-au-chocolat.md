---
title: Chocolate swirls chocolate lambout cake
date: '2016-01-10'
categories:
- Algerian cakes- oriental- modern- fete aid
- Chocolate cake
- Algerian dry cakes, petits fours
tags:
- To taste
- Dry Cake
- Cookies
- Algerian cakes
- Shortbread
- delicacies
- biscuits

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/09/swirel-020-300x225.jpg
---
## 

##  Chocolate swirls chocolate lambout cake 

Hello everybody, 

A delicious, very simple cake that looks like [ lambout cake ](<https://www.amourdecuisine.fr/article-halwat-lambout-70875141.html>) , or [ mahgone cake ](<https://www.amourdecuisine.fr/article-halwat-lambout-70875141.html>) but you must have your hand with the socket pocket. 

A dry cake melting in the mouth, delicious and rich in taste of chocolate. I had to put dark chocolate, but there was only the box of my husband, he hid and believed that I was not going to find, but I found it, and I did not dare to take it. 

go I will not be too late and I give you this delicious recipe. 

the recipe in Arabic: 

**Chocolate Whirls, or Chocolate Swirls Lambout Cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/swirel-020-300x225.jpg)

**Ingredients**

  * ingredients: 
  * 125 gr of margarine 
  * 75 gr of butter 
  * 75 gr icing sugar 
  * 75 dark chocolate 
  * 15 gr of cornflour 
  * 250 gr flour 
  * ½ teaspoon of baking powder 

for the butter cream: 
  * 125 gr of butter 
  * ¼ c. a vanilla coffee 
  * 225 gr icing sugar 



**Realization steps**

  1. preheat the oven to 180 degrees C 10 minutes before the start of cooking 
  2. beat the margarine and the soft butter with an electric mixer, add the icing sugar, beat again, then incorporate the melted and cooled chocolate, whip again. 
  3. add the maïzena whisking, then the flour and baking powder, without stopping whipping, you must have a soft dough she will not be sticky because of butter but soft. 
  4. now, we arrive at the critical point, the sleeve pocket, I must say that I am novice with the sleeve pocket, so do not laugh about the result. 
  5. fill, your piping bag, and form swirls of dough, as in the picture, on a buttered tray. 
  6. cook for 10 to 15 minutes. or until the cake has a small crust. (watch the cooking) [ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/09/Whirl1_thumb.jpg) ](<https://www.amourdecuisine.fr/article-chocolate-whirls-ou-des-tourbillons-chocolates.html/whirl1_thumb>)



prepare the cream, whipping the butter with the vanilla, then add the sugar slowly. 

join two pieces of each cake to have this little delight, well melting. 

dégustez, moi personnellement j’ai beaucoup aimer. 
