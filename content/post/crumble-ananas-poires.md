---
title: Pears pineapple crumble
date: '2011-10-02'
categories:
- gateaux algeriens au miel
- Algerian cakes- oriental- modern- fete aid
- sweet recipes

---
Hello everybody, 

here is a delicious recipe, light especially, in any case, I can eat, without really feeling guilty. because for me it was a thin layer of sand pie on top to give "crunches" of crumble with pineapple and pears. 

I love light desserts, although I do not publish often, but I do almost every day, because me and my sweet little one, we like to taste it together at the exit of the school. 

among these little delights I can advise you [ CRUMBLE WITH PEARS ANIS STAR ](<https://www.amourdecuisine.fr/article-verrines-crumble-poires-anis-etoile-68357136.html>) , that one has to devour without leaving to daddy and ale, as she likes to call it, when to [ KIWIS MASCARPONE VERRINES ](<https://www.amourdecuisine.fr/article-verrines-sucrees-kiwis-mascarpone-64934521.html>) I do not know how she gave me time to take pictures, she was there on a small stool to look at me and say: "finish or not" 

but the recipe she liked most was the [ INDIAN RICE CREAM ](<https://www.amourdecuisine.fr/article-creme-de-riz-indienne-59532236.html>) and must say that me too. 

this time I had to concoct for her and me, a delicious crumble with pineapple and pears, what I had at home, and it was super delicious, so you get the recipe: 

  * 1 pineapple - peeled. hollowed out and coarsely chopped 
  * 3 medium pears - peeled. thicket. and finely diced 
  * 1 cinnamon coffee 
  * 1 tablespoon brown sugar (if pears and pineapples are sugars, no need) 



**Crumble:**

  * 100 gr butter 
  * 250 grams of flour 
  * 50 gr of coconut 
  * 75 gr of brown sugar 
  * 1 cinnamon coffee 


  1. Preheat the oven to 180 ° C. 
  2. Prepare the fruits and add the cinnamon, 
  3. transfer to a baking tin or mini baking tin. 
  4. add the brown sugar if you want it to be a little sweeter. 
  5. Mix the crumble ingredients and sprinkle the fruit evenly. 
  6. Bake for 30 minutes or until the filling is lightly browned. 
  7. To serve garnish with a little cinnamon and coconut to enhance the final flavors. 
  8. Serve with ice cream or whipped cream. 



thank you for your visits. 

have a good day 

![news](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/news_31.png)

cochez l’une des deux cases pour que votre inscription soit valide 
