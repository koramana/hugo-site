---
title: gingerbread with candied fruits
date: '2015-12-18'
categories:
- cakes and cakes
- recettes sucrees
tags:
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-depice-aux-ruits-confits.jpg
---
[ ![gingerbread with candied fruits](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-depice-aux-ruits-confits.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-depice-aux-ruits-confits.jpg>)

##  gingerbread with candied fruits 

Hello everybody, 

It's been a long time since I shared with you the recipes of my dear friend **Wamani Merou** , so today it is this delicious candied fruit breads that she shares with us, a super simple recipe. A cake topped with a nice jam and crumble with peanuts and the hard edges of the cake itself. 

This gingerbread becomes even softer and more fragrant when you keep it in an airtight container 

**gingerbread with candied fruits**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-depices-aux-fruits-confits-1.jpg)

portions:  25  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 3 eggs 
  * 2 glasses of honey 
  * 1 glass of melted butter 
  * 1 pinch of salt 
  * 1 cup of baking soda 
  * 1 cup of baking powder 
  * ½ cup of ginger 
  * ½ teaspoon cinnamon 
  * ¼ cup of nutmeg 
  * ¼ teaspoon of clove powder 
  * ¼ cup of turmeric coffee 
  * ¼ teaspoon of chili (optional) 
  * ½ candied fruit glass 
  * flour to have a cake dough 

garnish: 
  * Apricot jam 
  * a little cane sugar 
  * 1 handful of peanuts. 



**Realization steps**

  1. Work the eggs with the mixer for 4 minutes 
  2. add the honey (aassila) then the melted butter and a pinch of salt 
  3. then mix the flour bicarbonate, baking powder and spices to choose.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ingr%C3%A9dient-du-pain-depices.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ingr%C3%A9dient-du-pain-depices.jpg>)
  4. add the candied fruits, mix a little, and pour the dough into a buttered and floured mold. 
  5. place the cake in a mold butter butter at 180. 
  6. at the end of the oven, remove the edges of the cakes 
  7. top with a generous layer of apricot jam. 
  8. mix the borders, cane sugar and peanuts 
  9. leave well frozen before cutting squares of almost 4 by 4 cm. 



[ ![gingerbread finish](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-depices-finition.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/pain-depices-finition.jpg>)
