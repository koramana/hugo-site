---
title: Orange Marmalade
date: '2011-11-29'
categories:
- bavarois, mousses, charlottes, recette l'agar agar
- dessert, crumbles et barres
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/confiture-d-orange-007_thumb.jpg
---
##  Orange Marmalade 

Hello everybody, 

it is already the season of the orange and already my husband begins to make the market, this time he returned with 5 kilos of oranges. 

so I do not tell you the ideas I have to make with these oranges, and the first was the delicious jam of oranges, especially that we do not find here. 

**Orange Marmalade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/confiture-d-orange-007_thumb.jpg)

**Ingredients**

  * 1kg 500 oranges (net weight, peeled) 
  * orange zest 
  * 1 liter and a half of water 
  * 1 kg 200 of sugar 
  * 1 lemon juice 



**Realization steps**

  1. clean and brush the oranges. 
  2. wipe them off and remove the zests. 
  3. cut pieces in the direction of the width, into thin slats (1 to 2 mm thick), 
  4. leave the pips, because it contains pectin and it will help to have a jam not too runny (there was none in my oranges, so I put the seeds of apples and lemons. 
  5. Put everything (zests, fruits, pips) + lemon juice, in a pot and cover with water 
  6. Cook for 1 hour. Let stand for 12 hours if possible. 
  7. Add 800g of sugar per kg of fruit 
  8. Bake about 40 min. 
  9. check the cooking by pouring a little juice into a cold saucer: it must freeze slightly. The cooking time can be longer or shorter depending on the quality of the fruit ... 
  10. Finally put in your sterilized jars 



et voila ma réserve pour cet hiver 
