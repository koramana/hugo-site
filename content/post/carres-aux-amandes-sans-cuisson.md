---
title: Almond squares without cooking
date: '2014-02-05'
categories:
- Tunisian cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-sans-cuisson-aux-amandes.jpg
---
![cake-baking-without-the-amandes.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-sans-cuisson-aux-amandes.jpg)

##  Almond squares without cooking 

Hello everybody, 

Here is a delicious recipe, very easy to make and especially very quickly because, it is without cooking, it is a recipe of our dear Lunetoiles 

These squares with almonds without cooking are a marvel, and in addition you can replace according to your taste and your means almonds by another ingredient ... The cake will always be a delight.   


**Almond squares without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-sans-cuisson-aux-amandes.jpg)

Recipe type:  Algerian cake  portions:  10  Prep time:  15 mins  cooking:  2 mins  total:  17 mins 

**Ingredients**

  * 1 measure of finely ground roasted almonds 
  * ½ measure of sieved icing sugar 
  * 2 tbsp. softened butter 
  * 1 C. teaspoon praline extract 
  * Honey 

For icing: 
  * White chocolate 
  * black 



**Realization steps**

  1. Prepare the mixture with the ingredients mentioned above (1 part finely ground roasted almonds, ½ part sifted icing sugar, 2 tablespoons softened butter, 1 teaspoon praline extract, honey) so as to obtain a homogeneous mixture. 
  2. Spread this filling in a rectangular dish about 1 to 1.5 cm thick. 
  3. Smooth the preparation well. 

Prepare the icing: 
  1. Spread it over the preparation smoothing well with the back of a spoon, then proceed as for the millefeuille, by pouring parallel lines in the same length, melted dark chocolate, and pass a knife, in the direction perpendicular to the dashes. chocolate, in order to obtain a marbling identical to the millefeuille. 
  2. Then let dry 24 hours, or overnight. 
  3. Then cut diamond or square, depending on your preference, and put in boxes. 



**![cake-baking-without-the-amandes1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-sans-cuisson-aux-amandes1.jpg) **   

