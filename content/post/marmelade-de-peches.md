---
title: marmalade of peaches
date: '2017-09-13'
categories:
- jams and spreads
tags:
- Toasts

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-peches-1.jpg
---
[ ![marmalade of peaches](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-peches-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-peches-1.jpg>)

##  marmalade of peaches 

Hello everybody, 

A recipe with a seasonal fruit that tells you? Here is a very good recipe of peach marmalade made by my friend Lunetoiles. You will tell me: but what is the difference between a marmalade and a jam? The marmalade should not take and congeal as well as a jam, it has a rather liquid texture as a syrup. 

This marmalade of peaches easy to make and so delicious will be perfect spread on rolls, toasts or cakes. As it can be an ideal ingredient for pies or tartlets. 

[ ![marmalade of peaches](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-peches-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-peches-2.jpg>)   


**marmalade of peaches**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-peches-3-300x195.jpg)

portions:  8  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * For 1.200 kg of peaches 
  * 750 gr of sugar 
  * half a vanilla pod 



**Realization steps**

  1. Peel, open and pit peaches. Cut them into large slices. 
  2. Put them in a very low heat casserole without adding water, and let them cook for 15 minutes, stirring and foaming if necessary. 
  3. Then pass the fruits to the vegetable mill, or the electric robot to puree them. 
  4. Weigh the obtained puree and put it in a bowl of jams with the sugar (750 gr of sugar per kilo of mashed potatoes) and the half vanilla pod opened in half, scraping the inside of the vanilla pod, to recover the small black grains. 
  5. Cook for 30 minutes over low heat, mixing often. 
  6. At the end of this time, check the cooking by dropping a drop of marmalade on an inclined cold plate: it must freeze immediately. Then remove from the fire. 
  7. Immediately put in pots, scalded and dried. Shut tightly. 



[ ![marmalade of peaches](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-peches.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/marmelade-de-peches.jpg>)
