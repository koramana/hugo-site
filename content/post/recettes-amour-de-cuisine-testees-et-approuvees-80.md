---
title: kitchen love recipes tested and approved 80
date: '2015-10-20'
categories:
- the tests of my readers
tags:
- desserts
- Cakes
- Algeria
- Morocco
- dishes
- tajine
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/article-recettes-amour-de-cuisine-testees-et-approuvees-58.html/dyouls-de-kari-ben>)

Hello everybody, 

it's been a busy two weeks for me, and I forgot to post this article online. But here, as soon as I say here is I will publish the article, I find emails with recipes tested by my readers, hihihih ... 

Thank you for your loyalty, and thank you for your comments, I apologize for not validating them right away, but it is difficult to answer every day, has more than 50 or even 70 comments, messages on facebook, not to mention emails ... In any case I do it, but sometimes I linger .... 

Let's go back to your recipes that you made from my blog. You can always send the pictures on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous/email>)

[ ![coconut macaroon at lili](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/macaron-a-la-noix-de-coco-chez-lili.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/macaron-a-la-noix-de-coco-chez-lili.jpg>)

[ ![coconut macaroons at Lunetoiles](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/macarons-a-la-noix-de-coco-chez-Lunetoiles.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/macarons-a-la-noix-de-coco-chez-Lunetoiles.jpg>)

[ Coconut macarons ](<https://www.amourdecuisine.fr/article-macarons-a-la-noix-de-coco.html>) at Lunetoiles 

[ ![semolina bread nadia](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-nadia.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-nadia.jpg>)

[ semolina bread ](<https://www.amourdecuisine.fr/article-pain-de-semoule.html>) at Fleur Dz 

[ ![cakes danette at nadia](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-danette-chez-nadia.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-danette-chez-nadia.jpg>)

[ Danette cake ](<https://www.amourdecuisine.fr/article-gateau-la-danette-au-chocolat.html>) revisited in yogurt cake at Fleur Dz 

[ ![cake without filling](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-sans-petrissage.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-sans-petrissage.jpg>)

[ bread without filling ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) at Joujou Marwan Mesamours 

[ ![pita breads](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pains-pitas.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pains-pitas.jpg>)

[ pita breads ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html>) at Nesrine Sissou 

[ ![amina semolina bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-amina.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-amina.jpg>)

[ semolina bread ](<https://www.amourdecuisine.fr/article-pain-de-semoule.html>) at amina dadou 

[ ![rolled potato](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/roul%C3%A9-de-pomme-de-terre.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/roul%C3%A9-de-pomme-de-terre.jpg>)

[ rolled potatoes with minced meat ](<https://www.amourdecuisine.fr/article-roule-de-pomme-de-terre-a-la-viande-hachee.html>) at Hayat Lainser 

[ ![samia chocolate shortbread](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/sabl%C3%A9s-au-chocolat-samia.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/sabl%C3%A9s-au-chocolat-samia.jpg>)

[ Chocolate Short Cake ](<https://www.amourdecuisine.fr/article-sables-au-chocolat-gateaux-secs-2012.html>) at Samia Z 

[ ![moufida semolina bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-moufida1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-moufida1.jpg>)

[ semolina bread ](<https://www.amourdecuisine.fr/article-pain-de-semoule.html>) at Moufida Hadid 

[ ![nesrine semolina bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-semoule-nesrine.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-semoule-nesrine.jpg>)

[ semolina bread ](<https://www.amourdecuisine.fr/article-comment-faire-son-pain-maison.html>) at Nesrine Sissou 

[ ![Oumabdallah chocolate cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-au-chocolat-Oumabdallah.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gateau-au-chocolat-Oumabdallah.jpg>)

[ chocolate cake ](<https://www.amourdecuisine.fr/article-gateau-moelleux-au-chocolat-la-creme.html>) at Oumabdallah L 

[ ![bread without oiling Oumabdallah](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-sans-petrissage-Oumabdallah.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-sans-petrissage-Oumabdallah.jpg>)

[ bread without filling ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) at Oumabdallah L 

[ ![cookie nest at Mima Bellissima](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-nid-chez-Mima-Bellissima.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/biscuit-nid-chez-Mima-Bellissima.jpg>)

[ nest-shaped biscuit ](<https://www.amourdecuisine.fr/article-biscuit-gateau-algerien-le-nid.html>) , at Mima bellisima 

[ ![baba has the orange at Walid S](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/baba-a-lorange-chez-Walid-S.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/baba-a-lorange-chez-Walid-S.jpg>)

[ Baba has orange ](<https://www.amourdecuisine.fr/article-la-recette-du-gateau-le-baba-sans-rhum.html>) at Walid Sofia 

[ ![Cardamom custard at Fleur Dz](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/flan-a-la-cardamome-chez-Fleur-Dz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/flan-a-la-cardamome-chez-Fleur-Dz.jpg>)

[ reversed cream with cardamom ](<https://www.amourdecuisine.fr/article-creme-renversee-caramel-cardamome.html>) at Fleur Dz 

[ ![pumpkin soup at flower dz](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/soupe-de-potiron-chez-fleur-dz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/soupe-de-potiron-chez-fleur-dz.jpg>)

[ Pumpkin soup ](<https://www.amourdecuisine.fr/article-soupe-de-potiron.html>) at Fleur dz 

[ ![besboussa at Marie Mariam's](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/besboussa-chez-Marie-Mariam.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/besboussa-chez-Marie-Mariam.jpg>)

[ Basboussa ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html>) at Marie Mariam 

[ ![bakbouka at Aktouche Mounira](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/bakbouka-chez-Aktouche-Mounira.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/bakbouka-chez-Aktouche-Mounira.jpg>)

[ bakbouka ](<https://www.amourdecuisine.fr/article-bakbouka-tajine-de-tripes-de-mouton-cuisine-algerienne.html>) at Aktouche Mounira 

[ ![egg custard at Malika Bouaskeur](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/flan-aux-oeufs-chez-Malika-Bouaskeur.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/flan-aux-oeufs-chez-Malika-Bouaskeur.jpg>)

[ egg flan ](<https://www.amourdecuisine.fr/article-flan-aux-oeufs.html>) at Malika Bouaskeur 

[ ![chocolate pear pie at Samia Z](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-poire-chocolat-chez-Samia-Z.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/tarte-poire-chocolat-chez-Samia-Z.jpg>)

[ pie and chocolate pie ](<https://www.amourdecuisine.fr/article-tarte-poires-et-chocolat-de-stephane-glacier.html>) at Samia Z 

[ ![semolina bread samia](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-samia1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-samia1.jpg>)

semolina bread at Samia Z's 

[ ![semolina bread at Isabelle Lgd's](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-chez-Isabelle-Lgd.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pain-de-semoule-chez-Isabelle-Lgd.jpg>)

[ semolina bread ](<https://www.amourdecuisine.fr/article-comment-faire-son-pain-maison.html>) at Isabelle Lgd 

[ ![qmirate at flower Dz](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/qmirate-chez-fleur-Dz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/qmirate-chez-fleur-Dz.jpg>)

[ qmirate ](<https://www.amourdecuisine.fr/article-qmirates-petites-lunes-aux-cacahuetes.html>) at Fleur Dz 

[ ![batata kbab cjez Fawzia Ammour](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/batata-kbab-cjez-Fawzia-Ammour1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/batata-kbab-cjez-Fawzia-Ammour1.jpg>)

[ batata kbab ](<https://www.amourdecuisine.fr/article-pomme-de-terre-au-four-batata-kbab.html>) at Fawzia Ammour 

[ ![berkoukes at Fawzia ammour](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/berkoukes-chez-Fawzia-ammour.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/berkoukes-chez-Fawzia-ammour.jpg>)

[ berkoukes ](<https://www.amourdecuisine.fr/article-berkoukes-bercouces.html>) at Fawzia Ammour 

[ ![almond ghribia at Oumou Hamza](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/ghribia-aux-amandes-chez-Oumou-Hamza.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/ghribia-aux-amandes-chez-Oumou-Hamza.jpg>)

[ almond ghribia ](<https://www.amourdecuisine.fr/article-ghriba-aux-amandes-gateau-sec-aux-amandes-facile.html>) at Oumou Hamza 

[ ![Chicken malsouka at Samia Z's](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/malsouka-au-poulet-chez-Samia-Z.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/malsouka-au-poulet-chez-Samia-Z.jpg>)

[ chicken malsouka ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks.html>) at Samia Z 

[ ![croquet with jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/croquet-a-la-confiture.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/croquet-a-la-confiture.jpg>)

[ jam croquets ](<https://www.amourdecuisine.fr/article-croquet-a-la-confiture-gateaux-secs-a-la-confiture.html>) at Fleur dz 

[ ![mchewek with sesame seeds at Shayma L](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/mchewek-aux-grains-de-sesames-chez-Shayma-L.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/mchewek-aux-grains-de-sesames-chez-Shayma-L.jpg>)

[ mchewek with sesame seeds ](<https://www.amourdecuisine.fr/article-mchewek-aux-grains-de-sesame-gateau-algerien-economique.html>) at Shayma Laour 

[ ![buchty at Samia Z](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/buchty-chez-Samia-Z.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/buchty-chez-Samia-Z.jpg>)

[ Buchty ](<https://www.amourdecuisine.fr/article-buchty-brioche-facile-a-mie-filante.html>) at Samia Z 

[ ![castel at Battouta Said Redouane](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/castel-chez-Battouta-Said-Redouane.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/castel-chez-Battouta-Said-Redouane.jpg>)

[ Castel ](<https://www.amourdecuisine.fr/article-castels-gateaux-algeriens.html>) chez Battoula Said Redouane 
