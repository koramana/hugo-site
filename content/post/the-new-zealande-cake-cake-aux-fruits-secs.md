---
title: the new zealande cake, dried fruit cake
date: '2012-07-03'
categories:
- cuisine indienne
- Cuisine by country

---
hello everyone, not too far from the famous carrot cake recipe, carrot cake, here is a delicious cake, with dried fruit, which I highly recommend. a cake very delicious, and very fluffy. & Nbsp; & Nbsp; ingredients: 3 eggs 3/4 cup sugar brown sugar 1/2 cup white powdered sugar 1/4 cup oil 1 teaspoon vanilla powder 2 cups whole wheat flour (330 gr normal flour for me ) 1/2 teaspoon salt 2 teaspoon cinnamon powder 1/2 teaspoon ginger powder & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

not too far from the famous recipe of [ carrot cake ](<https://www.amourdecuisine.fr/article-38217007.html>) , [ carrot cake ](<https://www.amourdecuisine.fr/article-cake-aux-carottes-103692595.html>) Here is a delicious cake with dried fruit, which I strongly recommend. 

a cake very delicious, and very fluffy. 

ingredients: 

  * 3 eggs 
  * 3/4 cup brown sugar 
  * 1/2 cup white powdered sugar 
  * 1/4 cup of oil 
  * 1 teaspoon vanilla powder 
  * 2 cups of whole wheat flour (330 gr of normal flour for me) 
  * 1/2 teaspoon of salt 
  * 2 teaspoons cinnamon powder 
  * 1/2 teaspoon ginger powder 
  * 1 tip of nutmeg 
  * 1/2 sachet of yeast 
  * 1/4 cup of milk 
  * 3 cups grated carrots 
  * 1 cup grated coconut 
  * 1/2 cup of pecan nuts (normal nut for me) 



Preparation: 

  1. Preheat the oven to 180 ° C (th.6). 
  2. Whisk eggs and sugars, 
  3. then add the oil and vanilla flavor. 
  4. Pour together the flour, salt, yeast and spices 
  5. Then add the milk, grated carrots, coconut and whole pecans. 
  6. The dough is quite thick, with lots of carrots: it's normal. 
  7. Pour the dough into a buttered and floured baking tin. 
  8. Bake and cook for about 1 hour. 
  9. Let cool and unmold. 



une bien belle aventure, et surtout un delicieux cake 
