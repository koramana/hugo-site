---
title: maadnoussia, chicken tajine with parsley
date: '2015-03-25'
categories:
- cuisine algerienne
- diverse cuisine
- Tunisian cuisine
tags:
- omelettes
- eggs
- Ramadan
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/maadnoussia-batata.jpg
---
[ ![tajine parsley, maadnoussia batata](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/maadnoussia-batata.jpg) ](<https://www.amourdecuisine.fr/article-maadnoussia-tajine-de-poulet-au-persil.html/maadnoussia-batata>)

##  Maadnoussia, chicken tagine with parsley 

Hello everybody, 

Maadnoussia is nominating a dish from the word maadnousse, or **parsley** in French. This **Omelette** in the style of a **Tunisian tajine** is prepared by pre-cooking some important ingredients of the pan dish, such as chicken and onions, then adding the eggs out of the fire so that the dish is thickened in the oven at the end. 

What is beautiful in this kind of recipe is that we do not have to be guided literally by the recipe, we can add everything we like in our delicious omelette, and have several versions all equally good each other. 

I thank my friend **Wamani Merou** , who generously shared his recipe on my blog ... so to our aprons my friends.   


**maadnoussia batata, tajine with parsley**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/maadnoussia-batata.jpg)

portions:  4-6  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 2 pieces of chicken breast 
  * 1 chopped onion 
  * 1 bunch of chopped parsley 
  * 3 tablespoons fresh cream 
  * 3 tablespoons of milk 
  * 2 triangles of cheese (laughing cow here) 
  * 3 to 4 eggs 
  * Butter 
  * salt and black pepper 
  * ¼ teaspoon of melache 4 spices (optional) or any other spice of your choice 
  * potatoes cleaned and diced and fried in an oil bath 
  * grated cheese for decoration 



**Realization steps**

  1. over medium heat, fry the pieces of chicken breast in butter, 
  2. season with salt, black pepper, 4 spices 
  3. add the chopped onion and sauté until it becomes translucent. 
  4. add the chopped parsley and stir over low heat for 2 to 3 minutes. 
  5. Out of the fire, add the cream and the milk 
  6. add the cheese in pieces, and the eggs slightly whipped 
  7. bake an oven-baking tin, dice the fried potatoes and pour the previous mixture over 
  8. Cover with grated cheese and bake in a preheated oven at 180 degrees C for almost 15 minutes. 



![tajine parsley, maadnoussia batata](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/maadnoussia-batata-1.jpg)
