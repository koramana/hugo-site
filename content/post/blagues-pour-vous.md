---
title: jokes ............ for you
date: '2008-01-24'
categories:
- basic pastry recipes
- sweet recipes

---
<table>  
<tr>  
<td>

**A single young woman goes to a psychiatrist for the first time. She exposes her problem: "Doctor, every time I go to bed, I have a terrible anxiety. I am convinced that there is someone under my bed. I'm terrified and I can not sleep. I'm getting completely crazy. "

In her chair, the psychiatrist looks at her and explains:   
This is a case of post-trauma that is finally quite classic. The being in question is undoubtedly the inhabitant of a nightmare of your early childhood. We will begin an analysis. You will come to see me every week for the next six months for a 45-minute introspection session. It will be 100 dollars a session, I register you? " 

The young woman answers:   
I have to think a little bit about it ... "   
The patient does not give any more news to the psychiatrist. But six months later, the psychiatrist meets her by chance at the supermarket and asks for explanations. 

She tells :   
When I got home after our interview, I told my neighbor who was mowing his lawn. And he found a solution right away. " 

The incredulous psychiatrist: "Oh good? "   
The woman: "He came to my house and he sawed the feet of the bed! " 

** 
</td> </tr> </table>  
  
<table>  
<tr>  
<td>


</td>  
<td>

**A depressed type visits a psychiatrist.  
\- Doctor, it's terrible, every night, deep in my room, I see a crocodile!   
\- It's nothing ! said the psychiatrist. Take two of these pills every night and it will pass! But eight days later, the patient returns:   
\- Doctor, do something, now I see the crocodile at the foot of my bed!   
\- Swallow four pills, it's radical! Eight days after:   
\- Doctor, now the crocodile is every night in my bed now!   
\- Well, take ten pills! And he no longer hears about his patient. Two months later, out of professional curiosity, he telephones the type's home and asks for it.   
\- How? But doctor, you do not know? He died last month, eaten by a crocodile! **

**Two school children are thinking about what they will do later.  
\- What would you like to do later as a job?   
\- Me, in my future career, everyone will queue to wait for my arrival. And we will be delighted to see me appear.   
\- Awesome ! You will be someone very important. But in which branch?   
\- Bus driver ... **


</td> </tr> </table>

**A design manager, a marketing manager and the big boss of a company are on their way to a meeting.  
While crossing a park, they find an antique oil lamp.   
They rub it to dust off it and suddenly a genie appears.   
The genius says: "Normally I let 3 wishes, so you will have one Everyone"   
The design manager: "me first, me first !!!! And he expresses his wish:   
«I would like to be in the Caribbean, surrounded by the most beautiful girls in the world and an inexhaustible source of exotic cocktails»   
And pouffff, here we go   
"Now to me, to me! Exclaims the marketing manager:   
"I would like to be in the Bahamas, driving a super fast motor boat, without any worries"   
And pouffff, here we go   
In turn, tell the genius to the big boss   
The big boss: "I want to see these two morons back at work after lunch" **

Moral of the story: always let the boss talk first!   


**A photographer working for Paris-Match had to take pictures of a gigantic forest fire in the south. He calls Nice airport and asks for a pilot. He is told that a Cessna with his pilot will be ready to take him there in two hours. Two hours later, the photographer is on the tarmak and finds the Cessna and his pilot waiting for him. He gets on the plane and shouts:  
\- Let's go! Then the pilot of the Cessna starts the engine, will face the wind and take off. **

In the air, the photographer says:   
\- You see the smoke there, go fly closer by placing yourself in the North. Then you will make several passes at low altitude. 

At this moment the pilot nervously asks:   
\- But why ?   
\- Well, because I want to take pictures! I'm a photographer at Paris Match, not a fisherman! ! 

After a pause in a heavy silence the pilot responds:   
\- Come on it's a joke, huh? ! You are my instructor? ? ? ... 

**It's a soldier, a sergeant we'll say. He is practicing shooting with his Famas. He is lying on the ground and throws a few balls in one shot.**

When the first class Dutrou will control the targets, he shouts to the sergeant:   
\- NO BALL IN THE TARGET, SERGENT. 

Then the sergeant looks at his weapon ... then looks at the target ... then looks at his weapon again ... then the target again. 

Then he puts his left index finger at the end of the barrel and using his right index finger, he presses the trigger. Naturally, his finger is sprayed! At this point the sergeant screams in the direction of the soldier:   
\- HERE FROM HERE CORRECTLY. THE PROBLEM MUST BE THERE   


**It's a guy who can not stand his dog anymore!  
He does everything he can to try to sell it to his neighbor.   
\- Heps! neighbor, I sell you my dog ​​who knows how to speak !!! 200 Francs only. It tell you ?   
The neighbor :   
\- your dog is talking! you take me for a con?   
Suddenly, the dog looks up at the neighbor and says with tears running down his nose:   
\- This man is mean, he beats me all day long. I am a very affectionate doggie and it only hurts me!   
The neighbor   
\- But that's crazy! actually he speaks !! why would you sell it to me at such a low price?   
The other who wants to sell him the dog:   
\- Ben .... because I'm fed up, he does not stop lying !!!!! **

**Two ducks are at the edge of a pond.  
The first : **

" Quack quack "   
The other responds to him: 

"It's crazy, that's exactly what I was going to say! " 

**A duck enters a pharmacy:  
\- Hello sir pharmacist!   
\- Hello sir duck!   
\- Do you have grapes?   
\- No. Here is a pharmacy and not a supermarket.   
The next day, the duck returns to the pharmacy:   
\- Hello sir pharmacist!   
\- Hello sir duck!   
\- Do you have grapes?   
\- Listen, mister duck! If you come back tomorrow to ask me the same thing, I nail you both feet on the ground!   
The next day, the duck returns to the pharmacy:   
\- Hello sir pharmacist!   
\- Hello sir duck!   
\- Do you have nails ??   
\- no !   
\- Well, do you have any grapes? **

**At an auctioneer: a parrot is sold at auction.  
\- 100 francs!   
\- 200!   
\- 500!   
\- 1000!   
(Etc ...)   
After a while, a guy wins bids for an exorbitant price. He addresses the auctioneer:   
\- Say, your parrot, I hope he speaks, for this price?   
And the parrot answers:   
\- Hey, then, who raised the auction after you? **

**3 bats are in an abandoned hut. Suddenly, one of them wakes up and leaves. 30s later, she comes back with a little blood on the left tooth. The smell of blood awakens the second, the second goes away and returns 30 minutes later with blood on his chin, a drop of blood falls on the muzzle of the 3rd and it wakes up and gets go . This one comes back 7 hours later with full of blood on the figure then the 1st says:  
\- Good God, you had a good laugh, it looks like.   
The 2nd says:   
\- Where did you find all this blood?   
Then the 3rd replied:   
\- Do you see the tree there?   
The others replied that yes. Then the 3rd said to them:   
– Moi, je l’ai pas vu ! **
