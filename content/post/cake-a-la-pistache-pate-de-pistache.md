---
title: Pistachio Pistachio Cake
date: '2015-03-13'
categories:
- gateaux, et cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/cake-a-la-pate-de-pistache-040.CR2_1.jpg
---
![Pistachio Pistachio Cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/cake-a-la-pate-de-pistache-040.CR2_1.jpg)

##  Pistachio Pistachio Cake 

Hello everybody, 

Here is a very delicious cake with homemade pistachio paste, very mellow and well perfumed, but before you pass the recipe I tell you the little story of this homemade pistachio paste ... 

So, I was very excited to make the homemade pistachio paste, and to taste it, but ... Yes, but the story is that I had two bags of pistachios one pruned, and the other no, I had put the pistachio pruned, but I put in the syrup the pistachio with the brown skin, to know or I had the head .... 

So the skin gave a very very dark color to the pistachio paste ... The dough was too good, it's nice to put your finger in it and lick it, um ... 

I had a lot of projects to do with this pistachio paste, but because I had missed it, I had to change course, and just make the cake pistachio paste that has a very weird color, but a taste intense… 

A recipe to redo absolutely, with a [ pistachio paste ](<https://www.amourdecuisine.fr/article-pate-de-pistache-maison.html> "homemade pistachio paste") more successful this time. 

![pate-a-pisatches-house-017.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pate-a-pisatches-maison-017.CR2_1.jpg)

**pistachio cake "pistachio paste"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/cake-a-la-pate-de-pistache-044.CR2_1.jpg)

portions:  12  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 4 eggs 
  * 280 g of sugar 
  * 100 gr of liquid cream 
  * 80 g of butter 
  * 220 g flour 
  * 70 g of [ pistachio paste ](<https://www.amourdecuisine.fr/article-pate-de-pistache-maison.html> "homemade pistachio paste")
  * 6 g of baking powder 
  * A handful of chopped pistachios 



**Realization steps**

  1. In a bowl, beat the eggs and sugar. 
  2. When the mixture starts to whiten, add the cream warmth and the pistachio paste. 
  3. Stir in the sifted flour, then the baking powder. 
  4. Add the melted butter. The dough should be smooth and shiny. 
  5. Line the cake tin with parchment paper. 
  6. Pour the dough into the mold and sprinkle with chopped pistachios. 
  7. Bake for 5 minutes at 200 ° C, then about 35 minutes at 155 ° C (or depending on your oven) 
  8. Check the baking of the cake by pricking the heart with the blade of a knife: if the blade is clean, the cake is sufficiently cooked. Unmould and let cool. 



![Cake-a-la-pate-de-pistachio 014.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/cake-a-la-pate-de-pistache-014.CR2_1.jpg)
