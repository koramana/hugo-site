---
title: date makrout and honey Algerian cake
date: '2012-07-12'
categories:
- bavarois, mousses, charlottes, agar agar recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/makrout9_2_thumb1.jpg
---
##  date makrout and honey Algerian cake 

Hello everybody, 

Here is one of [ Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) that my husband loves the most, of course after the [ baklawa ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-aux-amandes-et-cacahuettes-61018034.html>) . the date makrout baked, but not the [ fried makrout ](<https://www.amourdecuisine.fr/article-makrout-frit-makrout-el-maqla-ou-el-makla-72046810.html>) that's for me. 

to prepare it, I am the method of my grandmother, of Constantine, who does it marvelously, besides in the weddings, one always asks my grandmother to make Makrout el koucha (in the oven),   


**date makrout and honey Algerian cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/makrout9_2_thumb1.jpg)

**Ingredients** and its ingredients are: (for a small tray of 40 x 50 cm) 

  * 1200 gr of coarse semolina (not too much fat ok) 
  * 400 gr of ghee (smen, or clarified butter) 
  * 1 little salt 
  * Orange tree Flower water 
  * Water 

for the stuffing: 
  * 500 gr of date paste 
  * ½ cac of cinnamon 
  * ½ cac of ground cloves 
  * 2 pieces of butter 
  * 2cc of jam (optional, I had put it, because I had a paste of date of poor quality, and so I was afraid that the stuffing becomes hard when cooking, I added the jam of fig, which will make the dough soft) 
  * Orange tree Flower water. 



**Realization steps**

  1. Work the date paste with the ingredients previously given until you have a soft ball. Leave aside. 
  2. In a large bowl, pour the semolina, the salt. Mix. Make a fountain and pour the melted clarified butter. Mix well so that the butter is well absorbed by the semolina. Let it rest. After, wet with orange blossom water and water, and mix without working with your fingertips and without kneading. Once you have a compact ball, leave aside, and cover well with a cling film. 
  3. take a good amount of the dough, shape a pudding. With your index finger, make a channel in the center in the direction of the length. 
  4. Roll a little roll of date paste and put it in the slot. Reassemble the edges of the dough on the dates to cover it all. Ride gently again to have a roll of a width of 2 cm in height. With an impression mold, press the top of the pudding and flatten slightly to get the pattern on the makroud. Cut the lozenge into lozenges and set aside in a baking dish.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/makrout1_thumb.jpg)
  5. Renew this operation until the dough is used up. The diamonds are arranged tightly and touch each other (to prevent the date paste from getting burned) 
  6. Preheat the oven th. 175 ° and put the tray to cook for about 30 min but watch the cooking because it must have a beautiful golden color above and below. So be sure to turn off the bottom of the oven and turn on the top at the right time. 
  7. for the presentation, You can soak them in honey. 
  8. These cakes are kept in a hermetic box for a good duration (if you resist of course) 
  9. good tasting. 



{{< youtube A9sujhy7JK4 >}} 
