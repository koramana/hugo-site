---
title: Diamond hearts, 2014 Algerian cakes
date: '2014-07-14'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-1.jpg
---
[ ![diamond heart 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-1.jpg>) [ ![diamond heart 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-2.jpg>) [ ![diamond heart 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-3.jpg>) [ ![diamond heart 4](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-4.jpg>) [ ![diamond heart 5](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-5.jpg>) [ ![diamond heart 6](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-6.jpg>) [ ![diamond heart 7](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-7.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-7.jpg>)

Hello everybody, 

Diamond hearts, all beautiful and bright, and more edible ... yes yes, it's Algerian cakes that Batila, or Oum Thaziri likes to share with us on my blog ... 

This recipe was posted the first time on the Algerian forum Djelfa, I thank Oum Thaziri, for having posted it at home today by following the form: [ Submit your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

You too can do it, and I'll be happy to put your recipes online ... 

So let's go for this delice, it's 2014 Algerian cakes the diamond hearts. 

**Diamond hearts, 2014 Algerian cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-1.jpg)

portions:  90  Prep time:  40 mins  cooking:  10 mins  total:  50 mins 

**Ingredients** for basic shortbread 

  * 250 g of butter 
  * 1 glass of iced sugar 
  * 1 glass of maizena 
  * 1 egg 
  * vanilla 
  * flour 

for Nutella stuffing: 
  * 1 box of chopped bimo biscuit (or biscuit of your choice) 
  * 1 tablespoon of butter 
  * 1 tablespoon of honey 
  * Nutella chocolate 

for decoration: 
  * almond paste 
  * jelly strawberry flavor 
  * silver beads 
  * mix all the ingredients mentioned and spread the dough with a roll and cut into hearts 



**Realization steps** prepare the shortbread: 

  1. in a large bowl, mix butter, iced sugar, egg, vanilla   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-2-225x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-2.jpg>)
  2. Then introduce maizena and flour 
  3. spread the dough with a rolling pin on a well floured space 
  4. cut shortbreads in the shape of hearts,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-3-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-3.jpg>)
  5. bake in a pre-heat oven at 180 degrees for 5 to 10 minutes   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-4-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-4.jpg>)

prepare the nutella stuffing: 
  1. mix all the ingredients mentioned 
  2. spread the dough on a floured space with a roller pastry 
  3. cut into hearts using the same mold to cut shortbreads 

preparation of the cake: 
  1. after baking shortbreads, stick the shortbread hearts and nutella hearts with the jam   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-5-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-5.jpg>)
  2. on a work surface sprinkled with maizena and butter, lower the almond paste and cut strips   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-6-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-6.jpg>)
  3. fix the strips delicately all around the shortbread cake   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-7-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/coeur-de-diamant-7.jpg>)
  4. fill in the remaining space with a little strawberry jelly 
  5. make flowers with almond paste, and garnish the cake with. 
  6. place a silver bead in the heart of the flower. 
  7. the cake and ready serve with a good tea 


