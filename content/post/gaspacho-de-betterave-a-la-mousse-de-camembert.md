---
title: beet gazpacho with camembert mousse
date: '2016-06-11'
categories:
- amuse bouche, tapas, mise en bouche
- salads, salty verrines
tags:
- Ramadan 2016
- Ramadan
- Soup
- inputs
- Algeria
- Easy recipe
- salads

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/gaspacho-de-betterave-a-la-mousse-de-camembert-3.jpg
---
![beet gazpacho with camembert mousse 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/gaspacho-de-betterave-a-la-mousse-de-camembert-3.jpg)

##  beet gazpacho with camembert mousse 

Hello everybody, 

When I was looking for a recipe for our game recipes around an ingredient: Camembert I came across this recipe, which I liked, but in the end I chose to participate with my [ puffed with camembert cheese ](<https://www.amourdecuisine.fr/article-souffle-au-camembert.html>) . Then for the next round it was the beet, so I came back to this recipe that I found on the magazine tesco (supermarket in England). and I realized it, but in the end I participated with the recipe of the [ couscous salad beaded with beets ](<https://www.amourdecuisine.fr/article-salade-de-couscous-perle-aux-betteraves.html>) . 

But as this gazpacho was too good, I really liked the sweet and sour taste of gazpacho when it mixes with the creamy salty mousse of camembert, it's just a delight, that's why I share the recipe with you. 

![beet gazpacho with camembert mousse 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/gaspacho-de-betterave-a-la-mousse-de-camembert-2.jpg)

**beet gazpacho with camembert mousse**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/gaspacho-de-betterave-a-la-mousse-de-camembert.jpg)

portions:  6  Prep time:  15 mins  total:  15 mins 

**Ingredients** From Beet gazpacho: 

  * 5 cooked beets 
  * 1 small shallot 
  * 1 green apple 
  * ½ lemon (juice) 
  * Salt pepper 
  * camembert mousse 
  * 200 of fresh cream 
  * 150 gr of camembert 
  * Salt pepper 
  * Pistachio (optional) 
  * beet chips (optional) 



**Realization steps** For gaspacho: 

  1. Peel and chop the shallot. 
  2. Peel, sift and cut the green apple. 
  3. place the beets in pieces, the shallot, the apple pieces and the lemon juice in the blinder bowl and mix until you have a smooth texture. 
  4. If you find that it is a bit thick add a little water and mix again. 
  5. Season with salt and pepper and mix again. 
  6. Spread in two-thirds verrines. 

For camembert mousse: 
  1. whip the cream into firm chantilly. 
  2. gently soften the pie with a spatula. 
  3. Add 1 tablespoon Camembert whipped cream and mix to make it more supple, add a little until mixture is well blended. 
  4. introduce this mixture to the whipped cream gently to have a homogeneous foam. 
  5. place in a pastry bag and garnish the verrines. 
  6. Decorate with some crushed pistachios (optional) and beet crisps 



![Beet gazpacho with camembert mousse 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/gaspacho-de-betterave-a-la-mousse-de-camembert-1.jpg)
