---
title: pasta with minced meatballs
date: '2017-03-23'
categories:
- cuisine algerienne
- cuisine diverse
tags:
- Tunisia
- Sheep
- dishes
- Lamb
- tajine
- Algeria
- sauces

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pates-aux-boulettes-de-viande-hachee-1.jpg
---
[ ![pasta with meatballs chopped 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pates-aux-boulettes-de-viande-hachee-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pates-aux-boulettes-de-viande-hachee-1.jpg>)

##  pasta with minced meatballs 

Hello everybody, 

At home, I never miss meatballs, I always have a nice reserve in the freezer. When I have no more meat, or I do not know what to eat, or I have a guest unexpected, immediately I have my meatballs out, to make a good dish. 

My supply of minced meatballs, I do it every time, when I buy to make [ Bolognese ](<https://www.amourdecuisine.fr/article-sauce-bolognaise-maison-halal-et-sans-vin.html>) , of [ chili con carne ](<https://www.amourdecuisine.fr/article-recette-de-chili-con-carne-facile.html>) , [ burgers ](<https://www.amourdecuisine.fr/article-hamburgers-fait-maison.html>) or any meat-based dish, I always take care of rolling 3 to 4 well-seasoned meatballs, and I keep them in a good, clean freezer bag. Every time I do that, until I have a good stock, hihihih. 

This time, I did not know what to do at noon ... and here is the little light that pops up at once ... a very quick dish to make, very delicious, and there is no better ... hihihih pasta with meatballs minced. 

**pasta with minced meatballs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pates-aux-boulettes-de-viande-hachee-2.jpg)

portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** for the minced meatballs: 

  * 300 gr of minced meat 
  * ½ chopped onion 
  * 2 cloves garlic 
  * a few sprigs of parsley 
  * cumin, salt, black pepper 
  * 2 to 3 spoons of bread crumbs 
  * 1 egg 

for the tomato sauce: 
  * 1 kg fresh tomatoes, or canned 
  * 1 tablespoon of canned tomato 
  * ½ chopped onion 
  * 1 clove of garlic 
  * some sprig of parsley 
  * salt and black pepper 
  * thyme 
  * a bay leaf 
  * 2 to 3 tablespoons of extra virgin olive oil. 
  * pasta of your choice cooked in salt water. 



**Realization steps**

  1. prepare the minced meatballs 
  2. make them come alive in a little oil to give them a nice color. 
  3. remove from the fire and book 
  4. in the same oil, fry the chopped onion. 
  5. add garlic, bay leaf and thyme 
  6. let it come back a little then add the tomatoes and the spoon of canned tomato. 
  7. season and let cook. 
  8. return the minced meatballs to the tomato sauce and cook until the sauce is reduced. 
  9. at the same time, cook the pasta in boiling salted water to which you will add a little oil. 
  10. when the pasta is cooked remove from the heat. 
  11. You can add a little butter on it (optional) 
  12. present the dish of dough well garnished with sauce with the minced meatballs. 
  13. over a little parmesan or gruyere. 



[ ![pasta with minced meatballs](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pates-aux-boulettes-de-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pates-aux-boulettes-de-viande-hach%C3%A9e.jpg>)
