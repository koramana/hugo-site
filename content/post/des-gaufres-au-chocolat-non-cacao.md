---
title: chocolate waffles ...... no, cocoa
date: '2014-02-22'
categories:
- crepes, waffles, fritters
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
- Dishes and salty recipes
- salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/chocogauffre-001_thumb.jpg
---
##  chocolate waffles, crispy waffles on the outside, mellow on the inside 

[ ![chocogauffre 001](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/chocogauffre-001_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/01/chocogauffre-001.jpg>)

Hello everybody, 

In this gray weekend, and a little cool, the kids wanted to do something in the kitchen, my kitchen is too small to be found all in a mix, so the best things and to get out my waffle iron , work together on the table without doing too much damage. 

At their request, they wanted to make chocolate waffles .. we go to the closet, and we made out all our ingredients, no chocolate, cocoa will do well: 

Our ingredients: it gives a good amount, I did only half of the ingredients.   


**chocolate waffles ...... no, cocoa**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/chocogauffre-004_thumb.jpg)

Recipe type:  dessert  portions:  8  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 250 g melted butter 
  * 55 g of pure cocoa powder 
  * 300 g of sugar 
  * 4 beaten eggs 
  * 260 g flour 
  * 2 tbsp. tablespoon of milk 
  * 2 tbsp. vanilla extract 



**Realization steps**

  1. Mix the melted butter and cocoa. 
  2. Add the sugar and the eggs, then the flour, the milk and the vanilla. 
  3. Heat the waffle iron.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/chocogauffre-003_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/01/chocogauffre-003_thumb.jpg>)
  4. When it is hot, pour the correct preparation dose into the waffle maker and cook in the same way as any other waffle. Repeat the process until there is no more dough. 
  5. In any case it was very very very good, crispy, and delicious, even my husband who criticizes the waffles a lot, he loved it. 



[ ![chocogauffre 002](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/chocogauffre-002_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/01/chocogauffre-002.jpg>)
