---
title: Flour tortillas recipe video
date: '2017-10-25'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tortillas_thumb.jpg
---
[ ![Flour tortillas recipe video](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tortillas_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tortillas.jpg>)

##  Flour tortillas recipe video 

Hello everybody, 

my husband really likes them ** sandwiches  ** , and this time, when I had prepared roast chicken, and I had leftovers of chicken breasts, that no one likes to eat, I said to myself I'm going to make sandwiches but with ** Flour tortillas  ** . 

a recipe that I really like to make (when I have time of course), and that we like it at home. usually, my husband likes them [ batbouts ](<https://www.amourdecuisine.fr/article-36233481.html>) , the [ matlou3 ](<https://www.amourdecuisine.fr/article-pain-arabe-matloue-100634655.html>) , and sometimes with [ pita bread ](<https://www.amourdecuisine.fr/article-pain-libanais-pain-pita-120308538.html>) but I much prefer the flour tortillas that I share with you today. 

**Flour tortillas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tortillas-a-la-farine_thumb.jpg)

portions:  8  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 2 glasses of flour (240 ml) and a few spoons to spread 
  * 1 and ½ teaspoon of baking powder 
  * 1 teaspoon of salt 
  * 2 teaspoons of table oil 
  * 210 ml of hot milk 



**Realization steps**

  1. In the bowl of an electric mixer, mix together flour, baking powder and salt. 
  2. Add the table oil and mix. 
  3. With the electric mixer, gently add hot milk and mix until smooth. 
  4. Transfer the dough to a lightly floured work surface and knead for a few minutes, until the dough becomes soft. 
  5. Roll the dough into a ball and put in a medium salad bowl. cover with a cling film and let stand for 20 minutes. 
  6. Divide the dough into eight pieces and roll each into a ball. 
  7. Arrange the pieces so that they do not touch each other on a plate and let them rest covered with a cling film for 10 minutes. 
  8. Use a rolling pin, spread each ball to a diameter of almost 20 cm on the lightly floured worktop. 
  9. Transfer to a dish and leave covered while spreading the rest. Repeat until exhaustion of the dough 
  10. Heat a cast iron skillet or crepe maker. 
  11. Cook each tortilla for about 40 seconds, then return to the second side until it swells. 
  12. Transfer to a dish and keep covered while you cook the rest. 
  13. Eat hot or roll in sandwiches! 



[ ![flour tortillas 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tortillas-a-la-farine-1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/tortillas-a-la-farine-1.jpg>)
