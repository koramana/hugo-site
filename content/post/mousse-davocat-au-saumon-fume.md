---
title: Avocado Mousee with smoked salmon
date: '2017-12-23'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes
tags:
- Amuse bouche
- Mouthing
- Celebration meal
- Aperitif
- Apero
- Christmas
- Holidays

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/terrine-d-avocat-au-saumon-fume-2.jpg
---
##  Avocado Mousee with smoked salmon 

Hello everybody, 

a avocado mousse, covered with a beautiful layer of smoked salmon, a recipe to redo without hesitation, to present in salad or as a starter, this recipe can only please the tasters and lovers of these two ingredients, the avocado, and smoked salmon. 

You can see my last video, with a nice selection of appetizers, tapas and appetizers, a delight: 

**Avocado Mousee with smoked salmon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/terrine-d-avocat-au-saumon-fume-2.jpg)

**Ingredients**

  * 2 average mature lawyers 
  * a few slices of smoked salmon, to cover the avocado mousse. 
  * 80 ml of fresh cream. 
  * 6 g of gelatin in foil. 
  * salt pepper 
  * lemon 



**Realization steps**

  1. cover the inside a mini cake mold with a food film. 
  2. Blend the mold with the slices of smoked salmon, letting them overflow. 
  3. Cut the avocados in half, remove the kernel and put the flesh in a blinder bowl. 
  4. Add the lemon juice and mix. Add salt, a little pepper, and cream. 
  5. Mix with an electric mixer, and at this point you can adjust the seasoning by adding even more lemon juice, salt or pepper, depending on your taste. 
  6. Put the gelatin sheets for a few minutes in cold water, then heat them in a small saucepan with very little water. 
  7. Add the melted gelatin to the mixture and beat again. Pour into the salmon-covered cake pan. 
  8. When the mold is filled with the mixture, cover with the salmon slices and fold the food film over. 
  9. Put the terrine in the refrigerator overnight, serve the next day. 



  


![smoked salmon advocado terrine](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/terrine-d-avocat-au-saumon-fume1.jpg)  
  
<table>  
<tr>  
<td>

[ ![cucumber crab and goat](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tandoori-006.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tapas-du-reveillon-concombre-au-chevre-et-crabe-113788582.html>) 
</td>  
<td>

[ ![smoked salmon tapas.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-de-saumon-fume.CR2_thumb2.jpg) ](<https://www.amourdecuisine.fr/article-amuse-bouche-au-saumon-fume-et-feuilles-de-bricks-113825045.html>) 
</td>  
<td>

[ ![olive and goat tapas 021.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-olive-et-chevre-021.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-amuse-bouche-olive-et-chevre-113801503.html>) 
</td> </tr>  
<tr>  
<td>

[ ![amuse bouche](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/fleurs-a-la-creme-d-artichaut_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-fleurs-a-la-creme-d-artichauts-tapas-et-amuse-bouche-113751782.html>) 
</td>  
<td>

[ ![homemade breadsticks](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/gressins-au-basilic_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-recette-de-gressins-tres-facile-113765720.html>) 
</td>  
<td>

[ ![crab and parmesan cheese 021](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/bouchees-au-crabe-et-parmesan-021_thumb5.jpg) ](<https://www.amourdecuisine.fr/article-bouchees-de-crabe-au-parmesan-101566392.html>) 
</td> </tr>  
<tr>  
<td>

[ ![tapas with avocado cream 023.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-a-la-creme-d-avocat-023.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tapas-creme-d-avocat-olives-et-saumon-fume-113832478.html>) 
</td>  
<td>

[ ![olives cake](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cake-sale-1-a_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-cake-aux-olives-113781840.html>) 
</td>  
<td>

[ ![tomato nests with eggs 028](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/nids-de-tomates-aux-oeufs-028_thumb.jpg) ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs-101484090.html>) 
</td> </tr>  
<tr>  
<td>

[ ![amuse mouth artichokes goats 088](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/artichauts-chevres-088_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-chevre-dore-sur-fond-d-artichaut-100098265.html>) 
</td>  
<td>

[ ![ROLLED-of-aubergine.160x12011](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/roules-d-aubergine.160x12011.jpg) ](<https://www.amourdecuisine.fr/article-roules-daubergines-au-pesto.html>) 
</td>  
<td>

[ ![smoked salmon roulade](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/roulade-au-saumon-fume_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-roulades-de-saumon-fume-au-mascarpone-et-fenouil-101257027.html>) 
</td> </tr>  
<tr>  
<td>

[ ![stuffed olives 040 a](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/olives-farcis-040-a_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-brochettes-d-olives-farcies-102948239.html>) 
</td>  
<td>

[ ![cornet with boulghour 005a](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cornet-au-boulghour-005a_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-cornets-au-taboule-110333711.html>) 
</td>  
<td>

[ ![scallop walnut 009](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/noix-de-saint-jacques-009_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-noix-de-saint-jacques-bouchee-a-la-creme-de-persil-97454414.html>) 
</td> </tr>  
<tr>  
<td>

[ ![tuna rice mouths](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/amuses-bouches-au-riz-thon_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-boulettes-de-riz-au-thon-97137268.html>) 
</td>  
<td>

[ ![TARTARE OF SMOKED SALMON](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/TARTARE-DE-SAUMON-FUMEE_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tartare-de-saumon-fumee-en-cornets-de-crepes-100294277.html>) 
</td>  
<td>

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/hasselback-potatoes-hasselbackpotatis_thumb.jpg) ](<https://www.amourdecuisine.fr/article-pommes-de-terre-a-la-suedoise-hasselbackpotatis-108541271.html>) 
</td> </tr>  
<tr>  
<td>

[ ![Pepper rolls 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/Roules-aux-poivrons-1_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-roules-aux-poivrons-97975273.html>) 
</td>  
<td>

[ ![tapas with avocado cream 025.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tapas-a-la-creme-d-avocat-025.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tapas-a-la-creme-d-avocat-tomate-cerises-farcies-113829637.html>) 
</td>  
<td>

[ ![dumplings with surimi amuse](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/boulettes-au-surimi_thumb3.jpg) ](<https://www.amourdecuisine.fr/article-amuses-bouches-boulettes-au-surimi-113306581.html>) 
</td> </tr> </table>
