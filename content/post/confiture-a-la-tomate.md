---
title: jam with tomato
date: '2015-08-15'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-tomate-007_thumb.jpg
---
[ ![jam with tomato](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-tomate-007_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-tomate-007.jpg>)

##  jam with tomato 

Hello everybody, 

My jam has tomato! As promised here is the tomato jam recipe that I try to prepare every year, especially when I go out to the market and I see this beautiful red juicy tomato. I took this recipe from my mother, and frankly I do not like when the arrival of winter I can not find a box of jam with tomato in my closet, it is a jam whose taste has much rocked my childhood. 

if you like this jam with tomato, and you want to make other jam recipes, do it all on the [ jam category ](<https://www.amourdecuisine.fr/confitures-et-pate-a-tartiner>) you will find your happiness! and enjoy the fruit season that ends very soon to make beautiful jams for the winter, no better than a piece of crisp bread topped with a thin layer of butter and a nice spoon of jam, and especially with tomato jam ... A delight. 

recipe in Arabic: 

**jam with tomato**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-tomate-008_thumb1.jpg)

portions:  10  Prep time:  20 mins  cooking:  60 mins  total:  1 hour 20 mins 

**Ingredients**

  * 800 gr of red tomatoes 
  * 400 gr of sugar 
  * 200 ml of water 
  * the juice of half a lemon 
  * 1 cinnamon stick   
(small piece only 4 cm) 



**Realization steps**

  1. Peel the tomatoes   
(passing them for a few seconds in boiling water), 
  2. Cut them into 4 and seed them. 
  3. Put the tomato pieces in the jam dish with the sugar, the water, the lemon juice and the piece of cinnamon.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tomato-s-jam_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tomato-s-jam_thumb.jpg>)
  4. cook on low heat between 1 hour and 1 hour and a half. at the end of cooking, fill the jam in jars, and turn them until cool. 



[ ![jam with tomato](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-tomate-006_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/confiture-de-tomate-006.jpg>)

dégustez et donnez moi votre avis. 
