---
title: asparagus with fish
date: '2012-05-10'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/panique-en-cuisine.bmp_thumb1.jpg
---
Hello everybody, here is a delicious recipe **asparagus with fish,** a recipe that will participate in [ "express kitchen" contest ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>) . a recipe from a faithful reader Samya Oum Sirine, and ingredients, we can know that she must be very good this recipe. **ingredients for two people:** \- a little over half a bunch of fresh asparagus (around 600g) \- two blocks of fish ( **cod** frozen here) but fresh it's always better \- two to three tablespoons  of lemon juice  \- a big onion \- two to three tablespoons of orange juice \- a little water \- salt, aromatic herbs (a little parsley and chives here) 

  1. wash and peel the stem of the asparagus with the thrifty, (cut the hard part down the stem) 
  2. put them in hot salt water around 10min 
  3. put the fish (still frozen here) to cook in another  stove with a little olive oil, and salt 
  4. cut the onion into small pieces and put in the pan with a little olive oil, once the translucent onion. 
  5. add parsley, chives, lemon juice and orange, a little water, let a little boil  but not too much to leave some sauce. 
  6. adjust to salt according to your taste. 
  7. gently remove the asparagus from the water using a drainer and put in the sauce (or directly on the plate and pour the sauce on it) 
  8. set nicely on the plate with the fish, and a little chive and a slice of fresh lemon 

and enjoy your meal ! of Oum sirine and Sayaline this recipe participates in 

###  [ contest ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>)

[ ![panic in kitchen.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/panique-en-cuisine.bmp_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>)

to win this gift: 

###  ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/Kenwood-FP-196-01.jpg)
