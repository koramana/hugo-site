---
title: Banana and peanut butter smoothies
date: '2014-05-19'
categories:
- juice and cocktail drinks without alcohol

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-a-la-banane-et-beurre-de-cacahuetes.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-a-la-banane-et-beurre-de-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-a-la-banane-et-beurre-de-cacahuetes.jpg>)

##  Banana and peanut butter smoothies 

Hello everybody, 

Another recipe beneficial to your health, and rich in vitamins and taste too, banana smoothies and peanut butter, Lunetoiles recipe. 

I have already tested the marriage in taste of these two ingredients banana and peanut butter in my recipe for [ banana ice cream and peanut butter ](<https://www.amourdecuisine.fr/article-glace-a-la-banane-sans-sorbetiere.html> "banana ice cream without sorbetiere") So I imagine the taste of these smoothies. 

Following this rhythm of lunetoiles smoothies recipes, I'm going to create a category called 365 days, 365 smoothies recipes, lol ... Anyway, what's good with smoothie recipes is that we is not obliged to follow the recipe to the letter, we can change this or that ingredient, according to our taste, or the availability of the ingredient. 

**Banana and peanut butter smoothies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-banane-et-beurre-de-cacahuetes.jpg)

portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 bananas, cut into pieces and previously frozen a few hours in advance 
  * 2 cups of milk 
  * 60 g of peanut butter butter 
  * 2 tablespoons honey, or to taste 
  * 2 cups of ice cubes 



**Realization steps**

  1. Place bananas, milk, peanut butter, honey and ice cubes in a blender; 
  2. Stir until smooth, about 2 minutes, mixing several times. 
  3. Enjoy! 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-banane-et-beurre-de-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/smoothies-banane-et-beurre-de-cacahuetes.jpg>)

I hope you'll be lucky to try this delicious banana and peanut butter smoothie. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
