---
title: rice and zucchini gratin
date: '2015-06-01'
categories:
- Chhiwate Choumicha
- Algerian cuisine
- diverse cuisine
- ramadan recipe
- rice
tags:
- Algeria
- Vegetables
- Healthy cuisine
- Easy cooking
- Full Dish
- Ramadan 2015
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gratin-de-riz-et-courgette-2.jpg
---
![rice and zucchini gratin 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gratin-de-riz-et-courgette-2.jpg)

##  Rice and zucchini gratin 

Hello everybody, 

Finally a little time to post the recipes of my readers, who have followed the link: [ suggest your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette>) , in order to share their delights with us. Today it is this gratin rice and zucchini Wamani merou that I share with you ... 

A recipe that I personally want to test because I have never tasted, and I want to know the taste .... and you? have you ever tried a rice gratin? In any case, me where there is zucchini, you do not need to invite me, it is a vegetable that I like a lot, and all the sauces. 

**rice and zucchini gratin**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gratin-de-riz-et-courgette-1.jpg)

portions:  4-5  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * some pieces of chicken 
  * 2 tablespoons fresh cream 
  * 1 tablespoon of extra virgin olive oil (if not table oil) 
  * salt, black pepper, spices (masala chicken) 
  * 2 to 3 medium sized zucchini 
  * white rice cooked in a little salt water with a little turmeric and a few grains of cardamom. 
  * 150 gr grated cheese 
  * 3 eggs 
  * 2 tablespoons fresh cream 
  * and for the decoration: 100 gr of grated cheese 



**Realization steps**

  1. cook chicken cut in small cube with fresh cream, olive oil, salt, pepper and spices (masala chicken) and set aside. 
  2. Grate zucchini and fry in 2 tbsp oil, with a little pepper a little salt (watch out for salt) and set aside. 
  3. mix the chicken and its reduced sauce with the well-drained rice and zucchini 
  4. add grated cheese, eggs and fresh cream 
  5. pour the mixture into a buttered baking tin 
  6. cook in a preheated oven at 180 degrees for 15 to 20 minutes 
  7. Once the gratin is golden, add some grated cheese on top. 



[ ![rice and zucchini gratin](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/gratin-de-riz-et-courgette.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-riz-et-courgettes.html/gratin-de-riz-et-courgette>)
