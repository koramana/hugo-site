---
title: Chicken Couscous for Achoura
date: '2009-01-08'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/19713218.jpg
---
Yesterday, it was Achoura, and I prepared a couscous on this occasion, the I give you the recipe of Algerian couscous, whatever may differ from one region to another. 

so for the ingredients: 

  * 1kg of couscous 
  * 1 kg of chicken leg (you can put any part of the chicken) 
  * 1 to 2 onions depending on the size 
  * 2 to 3 carrots (to your taste, you can put more at least) 
  * 2 to 3 turnips 
  * 2 to 3 zucchini (personally I like a lot) 
  * 1 to 2 potatoes 
  * 1 handful of chickpeas 
  * 2 to 3 cases of canned tomato 
  * salt, pepper, ras el hanout, paprika 
  * 2 peppers 
  * oil 



![S7301513](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/19713218.jpg) ![S7301514](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/19713187.jpg)

wet and salt the couscous, and let it dry, then smear your hands with the oil and roll the couscous, so that after it does not stick. 

let stand, and prepare the couscous sauce, pass the onion to the robot. put in a pot (couscousiere) oil, paprika, grated onion, chicken slices, and put on the heat, add after the turnip cut on 4, as well as the carrots, and then the potatoes, add the 2 peppers, and then add the water to cover everything, and let it boil. 

When the sauce begins to give steam, put the couscous in the top of the couscous and let evaporate. 

![S7301515](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/19713420.jpg) ![S7301510](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/19713498.jpg)

after a while you will see that the steam starts to come out of the whole surface of the couscous cook 15 to 20 min, then remove the dust, and pour the couscous in a bowl, wet with a little water and work slowly with your hands. 

![S7301516](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/19713565.jpg)

throw the zucchini cut in 4, in the sauce, and cook, put the couscous back in the top of the couscoussier, after escaping the steam. cook another fifteen minutes and remove 

pour the couscous into a terrine and add a nice piece of butter in the middle, mix well, and at the same time add the chickpeas to the sauce so that everything cooks well. 

to serve, fill your plate with couscous, garnish it well with the vegetables you have and the chickpea, then drizzle with the sauce, according to your taste. 

place the chicken in the middle, and be hot. 

Good tasting, 

et que le bon dieu accorde la paix chez nos frères et soeurs a Gaza 
