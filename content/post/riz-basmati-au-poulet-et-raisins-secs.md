---
title: Basmati rice with chicken and raisins
date: '2017-07-13'
categories:
- Algerian cuisine
- diverse cuisine
- rice
tags:
- dishes
- Algeria
- Vegetables
- inputs
- Summer
- Healthy cuisine
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/riz-bisamti-au-poulet-et-raisins-secs.jpg
---
[ ![bisamti rice with chicken and raisins](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/riz-bisamti-au-poulet-et-raisins-secs.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/riz-bisamti-au-poulet-et-raisins-secs.jpg>)

##  Basmati rice with chicken and raisins 

Hello everybody, 

Do you like rice? I'm looking for the first opportunity to do it, because rice can accompany any sauce beautifully. 

**Basmati rice with chicken and raisins**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/riz-bisamti-au-poulet-et-raisins-secs.jpg)

portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * Whole chicken legs (depending on the number of people) 
  * 2 tablespoons of oil 
  * 1 tablespoon of butter 
  * 1 chopped onion 
  * 2 glasses of basmati rice wash well (until clear water) 
  * 1 knorr cube 
  * 1/4 cup of ginger coffee 
  * ¼ of this H'ror coffee 
  * ¼ cup of saffron powder (if not saffron pistils) 
  * Diced carrots steamed (half cooked) 
  * a handful of raisins 
  * ¼ cup cinnamon powder 
  * 4 glasses of water 
  * chopped coriander 



**Realization steps**

  1. fry the chicken legs well marinated and spiced with salt, black pepper in oil and butter 
  2. when it's golden, remove and set aside, 
  3. in the same oil, put the chopped onion and leave to brown well 
  4. add the rice and brown a little then, add the knorr cube and the spices 
  5. add the carrots. raisins and cinnamon 
  6. let it come back a little, then add the water and lower the heat completely 
  7. add the chicken pieces in it and let it cook 
  8. When it's ready, put some chopped parsley on top. 


