---
title: banana-cheese cake
date: '2016-01-03'
categories:
- cakes and cakes
- sweet recipes
tags:
- White cheese
- Cheesecake

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/banane-cheese-cake-1.jpg
---
[ ![banana cheese cake 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/banane-cheese-cake-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/banane-cheese-cake-1.jpg>)

##  banana-cheese cake 

Hello everybody, 

There are two recipes that my children prefer the most, this is the [ banana cake ](<https://www.amourdecuisine.fr/article-cake-a-la-banane-ultra-moelleux.html>) and the [ cheese cake ](<https://www.amourdecuisine.fr/article-cheesecake-aux-fraises-strawberry-cheesecake.html>) , that's just stupidly, it's the recipes that are repeated the most in their menu at school with the [ carrot cake ](<https://www.amourdecuisine.fr/article-recette-du-carrot-cake.html>) . So every time I ask them what they want to taste it, it must be one of those three cakes. 

I read the recipe which was very good in my eyes, but when I came to the preparation, or we had pasta, one that is the dough of the banana cake and the other of the cheesecake, we had to put a layer of the banana cake dough, another of the cheesecake, another of the banana dough, until it's perfect, but when I did both preparation, I found myself with two different textures, one thick texture for the banana-based dough, and a liquid texture for that of the cheesecake, so I had a little doubt that I do the editing, the layer of cheesecake would float over. 

So either you had to prepare the cheesecake dough in advance and put it in a cool place so that it was a little thicker, or do it like me, put a layer of the banana cake and a layer of cheesecake , cook, and half-baking place the last layer of the banana cake dough ... It has a little work, but it looks like the baking has lost its effect during the cooking of other layers ... I do not know, but the cake was super good, I did the pictures at night, I did not really like it, I wanted to redo the pictures in the morning when the children go to school, well there was not one left crumbs, between what my husband has to eat at night, and the shares my children took with them in their lunch boxes, here .... I hope you like these pictures. 

**banana-cheese cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/banane-cheese-cake.jpg)

portions:  12  Prep time:  30 mins  cooking:  50 mins  total:  1 hour 20 mins 

**Ingredients** banana cake: (1 cup of 240 ml) 

  * 1 big egg 
  * ½ cup of brown sugar 
  * ¼ cup granulated sugar 
  * 80 ml of oil 
  * ¼ cup Greek yogurt 
  * 2 tbsp. coffee vanilla extract 
  * 1 cup crushed ripe bananas (about 2 large bananas) 
  * 1 cup all-purpose flour 
  * ½ c. coffee baking powder 
  * ½. coffee baking soda 
  * 1 pinch of salt, optional 

cheese-cake party 
  * 1 big egg 
  * 110 gr of philadelphia cheese 
  * ¼ cup granulated sugar 
  * 3 tablespoons all-purpose flour 



**Realization steps**

  1. Preheat the oven to 180 degrees C. 
  2. wrap a cake tin with baking paper, set aside. 
  3. In a large bowl, place the egg, sugar, oil, yogurt, vanilla and beat to homogenize. 
  4. Add the bananas and mix to incorporate. 
  5. Add the flour, baking powder, baking soda, salt, and mix with a spatula, do not mix too much; put aside. 
  6. place two-thirds of the dough into the prepared pan, smoothing the top lightly with a spatula and pushing it into the corners and sides; put aside. 

the cheesecake pasta: 
  1. In a large bowl, add all the ingredients and beat to combine. 
  2. Pour the filling evenly over the banana layer, smooth the top slightly with a spatula. 
  3. Garnish with the remaining banana dough 
  4. Bake for about 45 to 50 minutes   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/banane-cheese-cake-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/banane-cheese-cake-3.jpg>)
  5. Let the cake cool in the pan for about 15 minutes before unmolding on a rack and let cool completely before cutting and serving. 



[ ![banana cheese cake 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/banane-cheese-cake-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/banane-cheese-cake-2.jpg>)

Maintenant je partage avec vous la liste des participantes a cette ronde que je remercie du fond du coeur, et merci a Viane pour tout l’effort qu’elle a fait afin d’organiser cette ronde: 
