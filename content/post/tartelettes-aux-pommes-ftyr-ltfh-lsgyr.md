---
title: Apple tartlets «فطائر التفاح الصغيرة»
date: '2011-05-16'
categories:
- Algerian cuisine
- diverse cuisine
- soups and velvets

---
I always like to test the recipes of my blogger friends, and this time, I tested one of the delights of  [ Lakbira  ](<http://lakbira31.canalblog.com/archives/2008/11/27/11535606.html>) and speaking of Lakbira, we must say that we will need a lot of time, to be able to test everything, because everything seems delicious at home. 

in any case, this time I opt for these delicious apple tartlets, I recommend it 

Ingredients: 

\- 2 to 3 apples (what I had on hand) 

\- 1 roll of puff pastry 

\- 1  ** tsp. and half a tablespoon of apricot marmalade  **

** \- 3 tablespoons caster sugar  **

** \- 50 g diced butter  **

** \- 3 tbsp of apricot marmalade, sifted (for me apricot jam)   
1) Grease a large plate (I put some greaseproof paper!). Cut the apples in half. Remove the hearts, but leave the skin.Cut into thin slices as possible, keeping the shape.   
2) Lower the dough and form a rectangle of 25/36 cm. Cut in half lengthways, then crosswise to obtain six rectangles. Lay them regularly on the plate. Spread 1 tbsp. coffee apricot marmalade on each piece of dough, leaving a 2 cm edge.   
3) Divide the apple slices and arrange them on each piece of dough, leaving a border. Sprinkle with powdered sugar and sprinkle with butter cubes. Chip the edge of the pies. This will allow them to inflate and open.   
4) Cool for at least 20 minutes. Preheat oven to 210 ° C, bake pies for about 20 minutes until puffed and browned. Spoon hot marmalade sifted while still hot. Serve hot or cold.   
**
