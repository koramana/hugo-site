---
title: Provencal mussels with tomato sauce
date: '2017-08-09'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- fish and seafood recipes
tags:
- Fish
- The sea
- Healthy cuisine
- Easy cooking
- Sea food

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-023.jpg
---
[ ![Provencal mussels with tomato sauce](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-023.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-023.jpg>)

##  Provencal mussels with tomato sauce 

Hello everybody, 

Here is a recipe Number one at my husband's, if I am, tomato sauce mussels can be on the lunch table every week, fortunately for me that our local fishery, it does not bring all the time, lol ... 

This week, my husband buy me beautiful mussels, and it was worth it that I take a picture of this recipe Mussels Provencal (or tomato sauce), to share my recipe with you. 

I like to introduce this sauce with pasta, I usually use spaghetti, but to my surprise today, I did not have enough !!! but instead at the bottom of my closet, there was a bundle of twisted pasta. In any case, this did not spoil the recipe, nor its taste, and we just loved my husband and me. 

[ ![Provencal mussels with tomato sauce 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-036.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-036.jpg>)   


**Provencal mussels with tomato sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-045.jpg)

portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** For pasta: 

  * 250 gr of pasta of your choice 
  * 1 teaspoon of salt 
  * 1 cup of table oil 
  * boiling water 

Sauce 
  * 2 tablespoons extra virgin olive oil 
  * 1 small onion, chopped 
  * 3 cloves of garlic, chopped 
  * 1 celery stalks, finely chopped 
  * 1 tablespoon chopped fresh thyme 
  * crushed black pepper and salt to season 
  * 1 can of crushed tomato, otherwise 4 fresh tomatoes 
  * 1 tablespoon of canned tomato 
  * 500gr of cleaned and brushed molds 



**Realization steps**

  1. Put enough water to boil in a large saucepan 
  2. at the first boiling, add salt and oil, then pasta, stir until it does not stick. 
  3. Drain and reserve when pasta is al denté 

Prepare the sauce while cooking the pasta: 
  1. Heat the olive oil in a large skillet over medium heat. 
  2. Add the onion, garlic, celery and thyme. Season with salt and pepper. 
  3. sauté for 5 minutes then add a little water and chopped tomatoes. Bring to a boil. 
  4. Add the mussels and cover with an airtight lid or if you do not have a lid, use aluminum foil. 
  5. Cook for 4-6 minutes or until the mussels are on. If there are mussels that remain closed, discard them. 
  6. Use a skimmer and transfer cooked open mussels to a bowl and cover with foil. 
  7. Place the drained pasta in the pan with the sauce and mix well to coat the pasta and cook for a minute or two.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-058.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-058.jpg>)
  8. To serve, spread the pasta and sauce over the 4 serving plates and garnish each with the baked mussels. 
  9. Decorate with fresh herbs if desired and serve hot immediately. 



[ ![Provencal mussels with tomato sauce](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-075.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/moules-sauce-tomate-075.jpg>)

Merci pour vos commentaires 
