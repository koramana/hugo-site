---
title: makrout way maamoul
date: '2015-07-13'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
- sweet recipes
tags:
- Algeria
- Algerian patisserie
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-facon-maamoul-2.jpg
---
[ ![makrout way maamoul 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-facon-maamoul-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-facon-maamoul-2.jpg>)

##  makrout way maamoul 

Hello everybody, 

Algeria is a country very rich in culinary culture, especially cakes and pastries, and among all these delights we find Sultane el Sniwa (the kings of ugly plateaus): makroute makrout. This delight is indispensable in the Algerian table especially in religious festivals and weddings. 

We see each time a new method and a new way of preparing it but it remains the traditional makroute, the best in my hamble opinion. There are different types of makroute, makroute and Maquela (at the stove) this kind of makroute is made in several forms: rolled, square and rhombuses. On the other hand, makrout el koucha (in the oven), it is imperative to use a mold with a special imprint: Tabâa el makrout and a Nekkache, the pliers. 

Today, we will see together makrout el koucha in a new form proposed by a reader **Flower dz** , makrout way maamoul, without delay I give you his recipe. 

[ ![makrout way maamoul 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-facon-maamoul-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-facon-maamoul-1.jpg>)   


**makrout way maamoul**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-facon-maamoul.jpg)

portions:  50  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * A measure equivalent to 250ml 

For the dough 
  * Three measures of special Makrout semolina (semolina medium-fat) 
  * A measure of melted and cooled ghee (smen). 
  * Two tablespoons of flour. 
  * ⅓ teaspoon salt. 
  * 100 ml of orange blossom water. 
  * Water according to the need to pick up the dough (150 ml for me) 

For the stuffing: 
  * 1 Kg of date paste. 
  * 1 C. cinnamon powder. 
  * ½ c. ground cloves. 
  * 3 c. soup of smen. 
  * A teaspoon of grilled sesame seeds (optional). 



**Realization steps** Preparation of the date paste. 

  1. In a large salad bowl put all the ingredients together and mix well. 
  2. Knead well to have a soft, soft dough and leave well. 
  3. Form small balls and cover. 

Preparation of the dough 
  1. In a large bowl mix semolina, flour and salt. 
  2. Make a pit and add the ghee, mix well by sanding in the hands very well for 
  3. introduce the fat into the semolina and do not knead. 
  4. Let it rest for at least two hours (a whole night for me). 
  5. Water now with orange blossom water and work the dough like a couscous at 
  6. fingertips without kneading. preferably sand the dough between the two hands. 
  7. Add the water in small amounts while mixing the dough at your fingertips to pick up a nice ball. 
  8. Cover and go to shaping. 

shaping 
  1. Take a quantity of the semolina dough and shape with a ball to the desired size 
  2. In the middle of the ball, using your index finger and the major made an insertion (hole) 
  3. In this insertion place a ball of the date paste and close your semolina ball by mounting the edges on the date paste. 
  4. Keep your ball clean because you have to cover the date paste well and roll gently 
  5. the ball in your hands to have a good surface leaves and a height of about 2cm. 
  6. Keep putting the ball in a special mold imprinted like the maamoul mold, or the moon cake mold and turn the face against your worktop, press lightly to form the makrout pieces and remove your piece. 
  7. Place them in a baking tray, done until the dough is completely used up. 
  8. Preheat your oven to 200 degrees for five minutes then put your makrout and turn down 180 degrees. 
  9. keep in a hermetic box, and during the presentation, plunge makrout in honey perfumed with orange blossom water 



[ ![makrout way maamoul 3](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-facon-maamoul-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/makrout-facon-maamoul-3.jpg>)
