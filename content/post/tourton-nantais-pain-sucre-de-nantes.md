---
title: Tourton Nantes Sweet bread from Nantes
date: '2014-03-19'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tourton-nantais-1.1.jpg
---
[ ![Tourton Nantes](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tourton-nantais-1.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tourton-nantais-1.1.jpg>)

Hello everybody, 

Today I share with you a recipe from the Nantes region, it is an old recipe of the Nantes tourton. 

Tourton Nantes is actually a mixture between a brioche and a bread, a recipe without egg but still recipe very soft, because the dough is wet because of its high milk content. 

**Tourton Nantes Sweet bread from Nantes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tourton-nantais-001.1.jpg)

Cooked:  French  Recipe type:  bakery  portions:  10  Prep time:  90 mins  cooking:  30 mins  total:  2 hours 

**Ingredients**

  * 500 g of flour 
  * 130 g of sugar 
  * 50 g of butter 
  * 25 g fresh baker's yeast 
  * 250 g whole fresh milk 
  * 200 g of fermented dough 
  * 8 g of salt 



**Realization steps**

  1. Warm the milk and dissolve the yeast. 
  2. In the bowl of the kneader, put the flour, the milk containing the yeast, the sugar and the fermented dough. 
  3. Start kneading at speed 1. 
  4. When the mixture is homogeneous, add the salt and continue kneading for 15 to 20 minutes. 
  5. The dough is firm but elastic. 
  6. If necessary, do not hesitate to add 2 to 2 tablespoons of milk. 
  7. The dough is well peeled from the walls, you can add all the cold butter cut in small dice. 
  8. Knead until complete incorporation of the butter. 
  9. The dough is well kneaded if it does not tear when you take it in your hands. 
  10. Gather the dough into a ball and let stand for 1 hour at room temperature. 
  11. After this period, degas the dough with the fist, reform the ball and place cool for 8 h. This is the score! 
  12. Remove the dough, pour it on the worktop and shape into an oval. 
  13. The dough is firm enough and you will not even need to flour the worktop. 
  14. Place on a baking sheet covered with baking paper and brush with a mixture of egg yolk and milk. 
  15. Allow to regrow 1h to 1h30 depending on the temperature of the room. 
  16. It must double in size. 
  17. Make a gilding stroke and make 3 or 4 transverse incisions. 
  18. Bake about thirty minutes at 180 ° C. 
  19. Let cool on rack. 



Note For those who do not make bread on a regular basis and who wonder about the fermented dough, it is a part of the previous kneaded that is taken to sow the next batch.   
  
If you do not have one, it is always possible to prepare to make this recipe. To do this, knead together 500 g flour, 350 g water, 10 g fresh baker's yeast and 10 g of salt. Let grow 1:30. Take the amount needed for the recipe and the rest can be used as a pizza dough or serve as a next recipe 

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tourton-nantais.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/03/tourton-nantais.1.jpg>)

if you have tried one of my recipes, send me the picture here: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

and if you have a recipe for you that you want to publish on my blog, you can put it online here: 

[ propose your recipe ](<https://www.amourdecuisine.fr/proposez-votre-recette> "suggest your recipe.")

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
