---
title: sardines with baked parsley
date: '2016-07-22'
categories:
- fish and seafood recipes
tags:
- dishes
- Algeria
- Easy cooking
- Ramadan 2016
- inputs
- Ramadan
- tajine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/poisson-au-four-092_thumb111.jpg
---
##  sardines with baked parsley 

Hello everybody, 

When I go to Algeria, the first thing I buy is fresh fish, or fresh sardine, something you will never find in England. So, here is the first meal I prepared myself as soon as I settled: sardine baked generously flavored with parsley, a light dish, easy to prepare and especially the incomparable taste, I wish you were there for enjoy this delicious dish of sardines with baked parsley. 

![Fish Crust-092_thumb111](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/poisson-au-four-092_thumb111.jpg)

**sardines with baked parsley**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/poisson-au-four-092_thumb111.jpg)

**Ingredients**

  * 500 grams of fresh sardines 
  * 1 medium onion 
  * an average bunch of parsley 
  * salt pepper 
  * 3 to 4 tablespoons of table oil. 



**Realization steps**

  1. clean the sardines, remove the head and stop it, if you can not stand too, remove the tail. 
  2. chop the onion and parsley 
  3. season this mixture with salt and black pepper 
  4. in an ovenproof dish of 18 cm diameter, put the oil 
  5. put a first layer of fish 
  6. sprinkle over a little onion / parsley mixture 
  7. cover with another layer of sardines 
  8. sprinkle the parsley / onion mixture again 
  9. continue until the exhaustion of the sardine 
  10. put a lid on your dish, and place in a medium-temperature oven.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/2011-07-16-poisson-au-four_thumb.jpg)
  11. cook for 20 minutes while watching, 
  12. remove the lid, and let reduce the sauce a little 
  13. serve hot, garnished with a little parsley, and lemon juice, if you do not like too much fat 


