---
title: half filled sanded spheres
date: '2015-07-30'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
tags:
- Algerian cakes
- Algeria
- Cakes
- Cookies
- Aid
- biscuits
- Little butter

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/demi-spheres-sabl%C3%A9es-four%C3%A9es.jpg
---
[ ![half salt salted spheres](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/demi-spheres-sabl%C3%A9es-four%C3%A9es.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/demi-spheres-sabl%C3%A9es-four%C3%A9es.jpg>)

##  half filled sanded spheres 

Hello everybody, 

Today, it is with great pleasure that she shares with us the recipe for these half filled sanded spheres, then covered in chocolate, very beautiful and melting in the mouth. It is beautiful these new forms that can be given to the fondant cakes the shortbread. 

**half filled salted spheres**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/demi-spheres-sablees-four%C3%A9es-2.jpg)

portions:  40  Prep time:  30 mins  cooking:  12 mins  total:  42 mins 

**Ingredients**

  * 250 gr of butter 
  * 2 egg yolks 
  * 180 gr of icing sugar 
  * 50 gr of almond powder 
  * 1 cup of baking powder 
  * 1 packet of vanilla 
  * 1 cup of hazelnut aroma 
  * a pinch of salt 
  * enough flour for a soft dough 

prank call: 
  * according to taste: halwat halkum, Turkish halwat, almonds, or cachuetes and jam, here I put the caramel sauce plus almond powder to have a dough that picks up   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sauce-caramel.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/sauce-caramel.jpg>)



**Realization steps**

  1. beat the butter with the sugar and the vanilla until you have a nice cream. 
  2. add the egg yolks and whisk well once more 
  3. introduce yeast, vanilla, almond powder. aroma, pinch of salt and flour to have a soft dough 
  4. just spread a small ball of almost 25 gr in your hand 
  5. put a spoon of stuffing, close it place in the silicone molds   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/demi-sphere-sabl%C3%A9e-farcies.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/demi-sphere-sabl%C3%A9e-farcies.jpg>)
  6. cook in the silicone molds they must be golden! 
  7. Then cooling put in the same molds melted chocolate then ask the cakes and cool for at least 1 hour 
  8. decorate with white chocolate strokes 



[ ![half filled with sanded spheres 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/demi-spheres-sabl%C3%A9es-fourees-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/demi-spheres-sabl%C3%A9es-fourees-1.jpg>)
