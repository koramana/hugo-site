---
title: how to cook couscous
date: '2013-02-09'
categories:
- ice cream and sorbet

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Couscous-a-la-vapeur.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/Couscous-a-la-vapeur.jpg)

##  how to cook couscous 

Hello everyone, Couscous is a very delicious dish, which requires some preparation, and before making the sauce that accompanies it, we must start by baking the couscous grains. 

## 

**how to cook couscous**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/cuisson-des-grains-de-couscous-a-la-vapeur_thumb1.jpg)

**Ingredients**

  * Couscous without cooking 
  * water 
  * salt 

necessary tool: 
  * wide terrine (gasaa) 
  * couscous 



**Realization steps**

  1. the method is well explained above. 


