---
title: my recipes at home 2
date: '2010-12-03'
categories:
- gateaux, et cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/12/cimg16271.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/cimg16271.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/46799936_p1.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/50901573_p1.jpg)

[ my roll ](<https://www.amourdecuisine.fr/article-biscuit-roule-a-la-confiture-a-minuit-45608781.html>) in [ douceamande ](<http://leblogatoutfaire.canalblog.com/archives/2010/03/16/17257612.html>)

![035__9_](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/548099631.jpg)

[ maakouda with tuna ](<https://www.amourdecuisine.fr/article-maakouda-au-thon-45819035.html>) in [ Oum Elyas ](<http://lesdelicesdoumelyas.over-blog.com>)

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/DSCF27691.jpg)

[ tuna rolls ](<https://www.amourdecuisine.fr/article-les-roules-au-thon-55009430.html>) in [ souhila ](<http://cuisinedesouhila.over-blog.com>)

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/nibele-3941.jpg)

[ tuna rolls ](<https://www.amourdecuisine.fr/article-les-roules-au-thon-55009430.html>) in [ NIBELE ](<http://entrelorientetloccident.over-blog.com>)

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/nibele381.jpg)

![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/500x375/2/91/31/92/fotos4/)

[ Chocolate muffins ](<https://www.amourdecuisine.fr/article-muffins-aux-deux-chocolats-hum-50113375.html>) of [ Oum mouncif rayan ](<http://surmatable.over-blog.com/>)

![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/375x500/2/94/35/18/Pains/)

[ Brioche with orange ](<https://www.amourdecuisine.fr/article-25922246.html>) at Nacy's (a reader) 

[ Focaccia ](<https://www.amourdecuisine.fr/article-focaccia-au-romarin-et-oignon-62123009.html>) at Papillon bleu (a reader) 

[ squares amandines ](<https://www.amourdecuisine.fr/article-38772190.html>) at Papillon 

![](https://www.amourdecuisine.fr//import/http://2.bp.blogspot.com/_at4PqZ8QJS8/TLRgG8cIwHI/AAAAAAAABMo/iuBwja8UYCs/s400/)

![arayeche-houria.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/arayeche-houria1.jpg)   


![](https://www.amourdecuisine.fr//import/http://storage.canalblog.com/50/87/357372/)

[ Vanilla / chocolate muffins ](<https://www.amourdecuisine.fr/article-muffins-vanille-chocolat-61854062.html>) in [ Natt ](<http://atablecheznatt.canalblog.com/>)

![DSCN1019-1-.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/DSCN1019-1-1.jpg)

![DSCN0546-1-.jpg](http://img.over-blog.com/300x225/1/19/07/89/P-tisseries-a%0Alg-riennes/DSCN0546-1-.jpg)

![DSCN0695-1-.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/DSCN0695-1-1.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2010/12/soupe2_thumb1.jpg)

[ Focaccia ](<https://www.amourdecuisine.fr/article-focaccia-au-romarin-et-oignon-62123009.html>) in [ ratiba ](<http://lesrecettesderatiba.over-blog.com/>)

[ Mkhedettes laaroussa ](<https://www.amourdecuisine.fr/article-mkhedette-laaroussa-l-oreille-de-la-mariee-57147864.html>) at mimi (a reader) 

[ macarons ](<https://www.amourdecuisine.fr/article-29887067.html>) at Blue Butterfly (more successful than mine anyway) 

hello everyone, a long list is not it, thank you to all the people who have tried my recipes, and who like to share their experiences with you through my blog, I thank everyone for that, and if you too, with blog or without blog, you have tried one of my recipes, keep me informed on my email: 

[ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

kisses 
