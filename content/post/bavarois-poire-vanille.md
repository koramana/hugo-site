---
title: Bavarian pear vanilla
date: '2010-10-15'
categories:
- Algerian cakes with honey
- gateaux algeriens- orientales- modernes- fete aid
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2010/10/dsc043101.jpg
---
yet another recipe from our talented Lunetoiles, this recipe she took the nice blog Narjisse For this recipe, it will take: For the dough speculoos: - 200g of speculoos - 80g of melted butter for Bavarian pears: - 200g of pears in syrup - 50g of sugar - 3 sheets of halal gelatine - 20cl of whole liquid cream for vanilla bavarois: - 25cl of milk - 2 egg yolks - 75g of sugar - 1 vanilla pod - 3 leaves halal gelatin - 20cl of cream & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.38  (  6  ratings)  0 

![https://www.amourdecuisine.fr/wp-content/uploads/2010/10/dsc043101.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/dsc043101.jpg)

For this recipe, you will need:  For the speculoos dough: 

\- 200g of speculoos   
\- 80g of melted butter 

for the **Bavarian** with pears: 

\- 200g pears in syrup   
\- 50g of sugar   
\- 3 sheets of halal gelatin   
\- 20cl of whole cream 

for the **Bavarian** vanilla : 

\- 25cl of milk   
\- 2 egg yolks   
\- 75g of sugar   
\- 1 vanilla pod   
\- 3 sheets of halal gelatin   
\- 20cl of whole cream 

for the chocolate mirror: 

\- 100g of pastry chocolate   
\- 20cl of liquid cream   
\- 1 halal gelatin sheet 

Start by finely mixing the speculoos and pouring the melted butter. 

Mix well and arrange on the bottom of a pan. 

Realize the **Bavarian** with pears. Put the gelatin to soak in cold water. 

Then mix the pears with their juice and warm them in a saucepan. Add the sugar.   
Squeeze and add the gelatin. 

Assemble the cream chantilly. 

Add to pear mixture and mix well. 

Pour into the mold and place in the fridge for one hour. 

Then realize the **Bavarian** vanilla,   
for this, soak the gelatin in cold water.   
Put the milk to boil with the split vanilla pod in 2. 

In a bowl, combine the egg yolks and sugar until the mixture whitens and becomes foamy. (I used 75g of vanilla sugar to boost the vanilla taste) 

Remove the vanilla pod and pour the milk over the mixture, then put the whole thing on a low heat until the preparation laps the back of the spatula. Let cool   
Drain the gelatin leaves and incorporate them into the mixture.   
Add the whipped cream and add it. Mix.   
Pour on the mold and place in the fridge for at least an hour. 

For the chocolate mirror, first put the gelatin to soak and melt the chocolate with the cream (it is possible to use a lightened cream). 

Add the gelatin by first drying it well.   
Pour the mixture into the mold and place at a minimum 

4 hours and at best for a whole night. 

  
Before serving, unmould first pass a knife around the mold. 

Decorate according to your desire. 

![https://www.amourdecuisine.fr/wp-content/uploads/2010/10/dsc043111.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/dsc043111.jpg)

Thank you for your feedback, 

merci a toutes les personnes qui continuent a s’abonner a ma newsletter, ca me fait réellement plaisir. 
