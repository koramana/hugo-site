---
title: pumpkin soup
date: '2010-10-08'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/creme-de-citrouille-rotie-1.jpg
---
![Rotie pumpkin cream](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/creme-de-citrouille-rotie-1.jpg)

##  Pumpkin soup 

Hello everybody, 

I must say that this period, I am rather in the moud soups and velvety, a real delight, you can eat any kind of vegetables, without being disgusted 

I apologize otherwise for the quality of the photo, because frankly the photos of nights, it's not really beautiful to see or do. especially since it is a pumpkin soup, which is automatically orange, and suddenly with the light of night, the color is indigenable for me, hihihihi, but not the soup that did not last long, so that I could redo photos to the day. 

in any case, I advise you to try this soup, especially that it is the period of the pumpkins ... .. (the note my son to buy it to make the halloween lantern, I did not say no, but as soon as I got home I made the bad guy and his lantern became a soup ... hihihihihi, he wants halloween, I'm halloween, hihihihihihii ... so here we change the time, it's the laughter of the witch, hihihihi ...)   


**pumpkin soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/creme-de-citrouille-rotie-1.jpg)

portions:  4 

**Ingredients**

  * 2 tablespoons of olive oil 
  * 1 onion, cut into pieces 
  * 1 cm fresh ginger 
  * 1 minced garlic head 
  * 500 gr pumpkin cleaned and diced 
  * 1 potato 
  * 1 cup of cumin 
  * 1 cup of paprika 
  * 1 cube of Knorr (vegetable broth) 
  * 100 ml thick cream 
  * pumpkin seeds. 



**Realization steps**

  1. in a pot, heat the oil, and brown the onion, add the crushed garlic and ginger cut in small piece, let simmer for 2 min, add the pumpkin and potato, then the spices and the Knorr cube. 
  2. cover with water, and cook for 20 minutes. 
  3. let cool after cooking 20 min, and pass the mixer, add the fresh cream mixer a little. 
  4. and season, if you find that it is missing. 
  5. garnish with pumpkin seeds (if not pine nuts) before serving 



![pumpkin soup](https://www.amourdecuisine.fr/wp-content/uploads/2010/10/soupe-au-potiron_thumb1.jpg)
