---
title: 'recipes for baby: zucchini puree and rice'
date: '2014-09-05'
categories:
- recipes for babies 4 to 6 months
- recipes for children and babies
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-4-a-6-mois-puree-de-courgettes-et-riz.jpg
---
[ ![recipes for baby 4 to 6 months puree of zucchini and rice](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-4-a-6-mois-puree-de-courgettes-et-riz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-4-a-6-mois-puree-de-courgettes-et-riz.jpg>)

##  recipes for baby: zucchini puree and rice 

Hello everybody, 

I share with you today the recipes for baby, which I prepared for my little angel when he was between 6 and 4 months, there are many recipes, that I realized during my holidays in Algeria, but that I did not have time to post. 

Now, my little sweetheart will soon be 7 months, and I will post the recipes as and when. I hope these recipes will please your babies, as they liked mine. 

For a start, I share with you, this delicious mashed zucchini baby rice, I confess that it is a delight to taste it, lol. 

**recipes for baby: zucchini puree and rice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-4-a-6-mois-puree-de-courgettes-et-riz.jpg)

portions:  1  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 1 small zucchini 
  * 2 tablespoons of baby rice 
  * 90 ml of baby milk 



**Realization steps**

  1. clean the zucchini and cut it in small dice   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-courgette-et-riz-033.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-courgette-et-riz-033.CR2_.jpg>)
  2. Cook the zucchini in a little water, or if you have a "babycook" Beaba it really helps in cooking vegetables and making baby puree.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-courgette-et-riz-035.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-courgette-et-riz-035.CR2_.jpg>)
  3. When zucchini pieces are cooked, remove from heat and drain, keeping some cooking water. 
  4. reduce the zucchini purée, add some cooking water if necessary. 
  5. place the baby rice in another bowl, and add the warm milk on top, stir to get a good consistency   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-courgette-et-riz-038.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/recettes-pour-bebe-courgette-et-riz-038.CR2_.jpg>)
  6. add the zucchini on top, and stir until you have a very homogeneous mixture   
(In the photo I did not mix too much, just to give you an overview of the ingredients, but before serving the baby, I mixed well) 



,  , 
