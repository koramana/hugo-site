---
title: velvety soup with grilled eggplants
date: '2012-07-31'
categories:
- dessert, crumbles and bars
- idea, party recipe, aperitif aperitif
- sweet recipes
- verrines sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-d-aubergines-grillees_thumb.jpg
---
##  velvety soup with grilled eggplants 

Hello everybody, 

Here is a super delicious velvety soup grilled eggplant, I do not tell you the flavor, and the haunting smells that emerge from this soup. 

We loved to taste this delicious soup with good [ khobz dar without kneading ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) , must say that I was tired to clean grilled eggplant, hihihihihi   


**velvety soup with grilled eggplants**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/veloute-d-aubergines-grillees_thumb.jpg)

**Ingredients** for 4 people 

  * 4 grilled eggplants 
  * 3 tomatoes 
  * 1 onion 
  * 3 cloves of garlic 
  * 2 tablespoons of olive oil 
  * 2 medium sized potatoes 
  * some basil leaves (according to your taste) 
  * cumin, salt, black pepper. 
  * cheese (according to your taste: portioned, or feta) 



**Realization steps**

  1. cut the tomatoes into cubes and remove the grains. 
  2. cut the onions into large pieces 
  3. peel the potato and cut it into a big cube 
  4. in a pot, sauté garlic, onion, tomatoes, potato in olive oil 
  5. let sweat well, and add spices, salt and basil leaves. 
  6. add the equivalent of 3 glasses of water and let cook well. 
  7. crush the grilled eggplants. 
  8. add the eggplant to boiling, leave a few minutes and remove from heat. 
  9. mix everything with a blender stand or in the blinder bowl, add the cheese in pieces 
  10. and present well hot with chopped basil on top. 


