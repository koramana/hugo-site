---
title: dumplings - tuna dumplings
date: '2012-12-24'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/quenelles-au-thon.jpg
---
[ ![tuna dumplings](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/quenelles-au-thon.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/quenelles-au-thon.jpg>)

Hello everybody, 

today it's a cold duck, I do not tell you ...., I went out to take the children to school, and I came back like a rusty robot at home, oooooooooooooooooouf, that it's good to find in the house a little warmed up. 

after finding my breath, and my little fingers found their vitality, to be able to touch the keyboard, and here I am in the archives of my pc, to see what recipe I will post today, and I see these quenelles that my apoux to really love, and I assure you, I was surprised, because I told him, today I'm going to prepare a dish but I do not know if you're going to eat it, but do not worry, I have chorba frik in the fridge ... so much that I know my husband with French cuisine, I always try to have a recipe in reserve, in case he will not like the dish. 

and frankly I was super surprised that he had to eat his dish, and that he asked me to reserve the rest for him. 

so these quenelles were a great success for me, and you know the history of dumplings? 

The quenelle is a very old product. Already in Roman times, there are culinary preparations that can be a bit like our current quenelles. Cited in the first century in "Apicius, Culinary Art" of the famous Roman gastronome, it is tasted by King Louis XV and his courtiers during "Great Suppers" offered in the castles of royalty. Dumplings are considered a refined dish at the King's Court. 

The recipe has been refined little by little in the course of history. 

According to Félix BENOIT (La Cuisine des Traboules, 1983), the Quenelle Lyonnaise appears around 1830. At that time pikes abounded in the Saone, and the Lyonnais did not know how to accommodate this dish, it's a master pastry chef, Charles Morator, who had the idea of ​​incorporating the flesh of this fish into a choux pastry. Subsequently, the recipe was diverted by replacing pike meat with poultry meat, rabbit ...   
The nature quenelle appears during the Second World War at the time of restrictions and shortages of meat and fish. 

In Bugey, the panade was mixed with the flesh of pike caught in the lakes of Nantua and Sylans and accompanied by the famous "Nantua Sauce" made from crayfish from these same lakes. Brillat-Savarin in the 18th century appreciated the quenelle made with the Lake Bourget lavaret and promoted it in Paris. 

go for the recipe now after this little introduction: 

**for dumplings:**

  * 200 g flour 
  * 150 g of milk 
  * 150 g of water 
  * 4 eggs 
  * 70 g of butter 
  * Salt pepper 



for the tuna sauce: 

  * 2 boxes of tuna in the natural 
  * 100 g of green olives 
  * 1 onion 
  * 2 cloves of garlic 
  * 1 can of tomato in piece 
  * a little olive oil 
  * salt, black pepper, coriander 
  * thyme 
  * 1 bay leaf 



for the detailed preparation method, you can see the video: 

method of preparation: 

  1. In a saucepan, boil the milk, water and butter. 
  2. When the mixture is boiling, put the fire to a minimum and add the flour all at once, stirring with a spatula. 
  3. Stir until the dough detaches well from the walls and forms a homogeneous ball. 
  4. Remove the pan from the oven and add the eggs one by one, stirring well until they are well incorporated between each addition. 
  5. Salt and pepper the dough. 
  6. Boil a pan of salted water. 
  7. Form dumplings using two tablespoons and put them in the water as and when. Remove them with a skimmer when they rise to the surface. Be careful not to put too much in the water otherwise they will stick. 
  8. Peel the onion and fry in a little olive oil. 
  9. add the tomato then the olives, the thyme and the laurel 
  10. Drain the tuna, add it to the preparation. Season with salt and pepper and let simmer for a few minutes. 
  11. oil a baking tin, and top it with some tuna sauce 
  12. put the dumplings on it, put some cheese on it if you like 
  13. leave in the oven for 5 to 15 minutes 
  14. serve with a nice spoonful of remaining sauce 



[ ![tuna dumplings-001](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/quenelles-au-thon-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/quenelles-au-thon-001.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
