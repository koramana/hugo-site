---
title: brownie with nutella
date: '2013-12-11'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brownie-au-nutella-011.CR2_-1024x759.jpg
---
[ ![nutella brownie 011.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brownie-au-nutella-011.CR2_-1024x759.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brownie-au-nutella-011.CR2_.jpg>)

##  brownie with nutella 

Hello everybody, 

Nutella brownie melting to perfection, and super soft, and the most important is this Nutella brownie is super easy to achieve ... 

When my children asked me for a chocolate cake to taste it, something that is not in their habit! I started looking for the ingredients, but I did not have any more chocolate or eggs ... yes, there were only 2 eggs in the fridge ... 

I was too lazy to go out, buy the missing ingredients and make the cake ... There was only one solution, make a recipe with what I had on hand .... 

Thanks to my cookbooks, which I have not touched for a while, but are still there, for the most urgent emergencies, hihihihi 

First recipe, which suits me, and does not require a lot of ingredients, but had a taste all chocolate and here is so delicious Nutella brownies ... So you want the recipe? 

**brownie with nutella**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brownie-au-nutella-004.CR2_-1024x682.jpg)

Recipe type:  dessert cake  portions:  8  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * ½ glass of flour (1 glass = 250 ml) 
  * ¼ teaspoon of salt 
  * 2 eggs 
  * 1 glass of Nutella 
  * ½ glass of brown sugar (brown sugar) 
  * 1 teaspoon of vanilla extract 
  * ½ glass of melted butter, not salted, slightly cooled 



**Realization steps**

  1. preheat the oven to 170 ° C (325 ° F). 
  2. Line the bottom of a 20 cm square baking dish with paper, leaving the paper overflowing on both sides of the pan. Butter it a little. 
  3. In a bowl, mix the flour and salt. Put aside. 
  4. In another bowl, beat the eggs, hazelnuts, brown sugar and vanilla extract with an electric mixer until it becomes homogeneous. 
  5. Add the flour alternately with the melted butter. 
  6. Divide the dough into the cake pan. 
  7. Bake until a toothpick inserted in the center comes out with some crumbs attached (not completely clean), 35 to 40 minutes. 
  8. let cool in the mold for about 2 hours, then turn out and cut into squares. 



Note You can reduce the cooking time, if you want a cake even more fluffy and super fondant. 

[ ![nutella brownie 010.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brownie-au-nutella-010.CR2_-682x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/brownie-au-nutella-010.CR2_.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
