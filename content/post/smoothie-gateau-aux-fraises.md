---
title: strawberry cake smoothie
date: '2017-08-25'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-gateau-aux-fraises-1.jpg
---
![strawberry cake smoothie 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-gateau-aux-fraises-1.jpg)

##  strawberry cake smoothie 

Hello everybody, 

Yes yes, every day a smoothie, I do not promise you that I will post every day one, but in any case, I prepare myself every morning a smoothie, not just because it's good for the metabolism, but because is good, because you can have fun giving each time a new taste of the same smoothie. We add an ingredient, we remove another, a touch of vanilla here, another of cinnamon by the ... We do not need to follow the recipe to the letter. I like being the Jamie Oliver smoothies, hihihihih. 

Today it's a strawberry cake smoothie .... too good, and it will stall you until noon ... The top what. 

**strawberry cake smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-gateau-aux-fraises--300x274.jpg)

portions:  2  Prep time:  2 mins  total:  2 mins 

**Ingredients**

  * 1 glass of frozen strawberries 
  * 1 banana cut into slices 
  * ¼ cup of flaked almonds 
  * ½ glass of oatmeal 
  * 1 glass [ almond milk ](<https://www.amourdecuisine.fr/article-lait-d-amande-maison.html>) (otherwise normal milk) 
  * ¼ teaspoon of vanilla extract 
  * 1 tablespoon of honey (if needed) 



**Realization steps**

  1. place the almonds and oatmeal in the blinder bowl and mix to reduce to powder. 
  2. add the rest of the ingredients (I prefer not to put the honey to the end, after having tasted my smoothie, sometimes I do not add honey) 
  3. mix well until you have a good smoothie 
  4. Pour into glasses and decorate according to your taste. 



[ ![strawberry cake smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-de-gateau-de-fraises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/smoothie-gateau-aux-fraises-1.jpg>)
