---
title: Dress with nuts Algerian cake video
date: '2014-07-13'
categories:
- Algerian cakes with icing
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mkhebez-aux-noix-1.jpg
---
[ ![Mekhabez with nuts, Algerian cake video](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mkhebez-aux-noix-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mkhebez-aux-noix-1.jpg>)

##  Dress with nuts Algerian cake video 

Hello everybody, 

Another recipe that goes back to last year, and that I did not have time to post: [ cakes from l'aid el fitr 2013 ](<https://www.amourdecuisine.fr/article-gateau-algerien-aid-2013.html> "Algerian cake Aid 2013") , these munch with almonds and nuts, and I do not tell you the sweetness of the taste of nuts in this recipe, a delight !!! 

So I come back to this recipe for mud dressing, which is a recipe very easy to make, and very beautiful to present. For decoration, I must admit that I did not do much, but I know you will have a lot of imagination to present mkhabez very beautiful.   


**Mekhabez with nuts, Algerian cake video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mkhebez-aux-noix.jpg)

portions:  30  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients**

  * 200 gr of ground almonds 
  * 200 gr of powdered walnuts 
  * 200 gr of icing sugar 
  * the zest of 1 lemon 
  * 1 egg + 2 egg yolk 
  * vanilla 

icing: 
  * 3 egg whites 
  * 3 tablespoons of lemon juice 
  * 3 tablespoons of orange blossom water 
  * 2 tablespoons of oil 
  * icing sugar. 



**Realization steps**

  1. Mix the almonds, the nuts, the icing sugar, the zest of 
  2. lemon, vanilla extract and wet with eggs to obtain a manageable and firm dough. 
  3. On a floured work surface, lower the dough to a thickness of 1.5 cm and cut 4 cm squares on the side using a cutter. 
  4. Place On a greased and floured plate, cook for 15 to 20 minutes at 160 ° C 
  5. Cool on a baking rack. 

Preparation of the icing: 
  1. Prepare the icing with the given ingredients, 
  2. Add the color of your choice 
  3. Check the icing by testing a cake. 
  4. Ice the cakes. Let them dry completely, remove the excess frosting that would have flowed. 
  5. Pass the silvery food gloss on the surface using a food sponge. 
  6. Decorate the pieces with leaves and flowers with sugar dough. 



[ ![help el fitr 2013 mkhabez with almonds and nuts.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/aid-el-fitr-2013-mkhabez-aux-amandes-et-noix.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/aid-el-fitr-2013-mkhabez-aux-amandes-et-noix.CR2_.jpg>)

{{< youtube WTP4MtyWLkk >}} 
