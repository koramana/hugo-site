---
title: Krumchy by Christophe Michalak
date: '2017-01-04'
categories:
- Algerian dry cakes, petits fours
- recettes sucrees

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/krumchy-de-christophe-michalak.jpg
---
[ ![krumchy de christophe](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/krumchy-de-christophe-michalak.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/krumchy-de-christophe-michalak.jpg>)

##  Krumchy by Christophe Michalak 

Hello everybody, 

When Lunetoiles to share with me this recipe yesterday, I told him, it's pims? she said no, it's even better ... The krumchy are a little delicacy of Christophe Michalak, based on a crispy layer of sand, which hides a creamy layer of salted butter caramel, covered in the end by a nice layer of chocolate and a few pieces of roasted hazelnuts ... so it goes well krumchouner in your mouth, krum, krum, krumch .... because it's krumchy ... 

**Krumchy by Christophe Michalak**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/krumchy-de-christophere-michalak-1.jpg)

portions:  30  Prep time:  20 mins  cooking:  12 mins  total:  32 mins 

**Ingredients** for sandy funds: 

  * 120 g of flour 
  * 90 g of butter 
  * 70 g of sugar 
  * 2 egg yolks 
  * ½ sachet of baking powder 
  * 1 sachet of vanilla sugar 
  * 1 pinch of fleur de sel 

Salted butter caramel : 
  * 200 g of sugar 
  * 130 g whole liquid cream 
  * 50 g of half-salted butter 

Ingredients for the filling: 
  * +/- 250g of milk chocolate 
  * Crushed and roasted hazelnuts ... 



**Realization steps** Realization of shortbread: 

  1. Place the small oven cavity mold on a baking tray, and preheat the oven to 180 ° C. 
  2. Put all the shortbread ingredients in the bowl of your robot. Mix until the dough forms. 
  3. Take the dough out of the robot and spread it in the footprints until ¾ 
  4. Bake shortbreads for 8 to 10 minutes. 
  5. As soon as you leave the oven, make a hollow in the middle of each shortbread with the help of the Parisian spoon (do it right out of the oven because the shortbreads are still very soft). 
  6. When cooking shortbread, prepare the salted butter caramel: Make a caramel dry by putting the sugar to melt slowly in a pan very wide stainless steel. Above all we do not stir! It must be allowed to melt very slowly, stirring occasionally the pan so that all the caramel melts evenly and so that the sugar underneath does not burn without one noticing ... 
  7. When almost all the sugar is golden, heat the cream until it boils. This is to avoid heat shock when you pour it on the caramel ... Once the boiling cream, turn off the heat under the caramel, then pour the cream while stirring with the spatula high temperature. 
  8. Once all the cream is incorporated, add the butter to your pan and stir until it is completely melted. 
  9. [ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/les-krumchy.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/les-krumchy.jpg>)
  10. Without removing the shortbread, pour a small ½ teaspoon of caramel in the hollow of each shortbread. Melt the chocolate in the microwave. Cover the chocolate caramel, then spread the hazelnuts. Put your shortbread plate in the fridge so that the caramel and chocolate harden a little. 



[ ![the krumchy](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/krumchy-de-michalak.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/krumchy-de-michalak.jpg>)
