---
title: Mini konafa, ktayefs with ramadan dessert cream 2017
date: '2017-05-06'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries
tags:
- Algerian cakes
- Algeria
- Morocco
- Ramadan 2016
- Ramadhan
- Pastry
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/08/konafa2_thumb1.jpg
---
##  Mini konafa, ktayefs with ramadan dessert cream 2017 

Hello everybody, 

a very nice recipe, a delice, these mini konafa, which you can prepare in the blink of an eye ... so I pass you the details, and do not find any excuse to realize it as soon as possible ... 

It's just with an egg-free pastry cream covered with two layers of ktayefs soaked in melted butter, drizzled at the end with a generous stream of honey. 

This recipe is very popular at home and especially during Ramadan evenings to change some of the [ qalb elouz ](<https://www.amourdecuisine.fr/article-qalb-el-louz-kalb-elouz-gateau-coeur-damandes%d9%82%d9%84%d8%a8-%d8%a7%d9%84%d9%84%d9%88%d8%b2.html> "Qalb el louz: kalb elouz cake Heart of almonds قلب اللوز") , and some [ Basboussa ](<https://www.amourdecuisine.fr/article-la-plus-fondante-et-moelleuse-basboussa-55975294.html> "algerian patisserie, Basboussa in video بسبوسة سهلة") more   


**Mini konafa, ktayefs with ramadan dessert cream 2017**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/08/konafa2_thumb1.jpg)

Recipe type:  dessert  portions:  6  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 250 gr of katayef or finely cut angel hair (use scissors) 
  * 125 gr of melted butter 
  * 100 gr of sugar 

for the cream 
  * 400 ml of milk 
  * 35 gr of maizena 
  * 30 gr of butter 
  * vanilla 
  * 4 tablespoons of sugar 

for syrup 
  * 250 gr of honey 



**Realization steps**

  1. the preparation of konafa 
  2. put the ktayefs in a large salad bowl 
  3. sprinkle with butter and mix well, 
  4. then add the sugar and mix again. 
  5. divide the ktayefs by two 
  6. butter your mini mussels and put some of the angel hair and pack well 
  7. prepare the cream by putting all the ingredients in a saucepan and thicken over a low heat 
  8. pour the cream on the first layer of ktayefs 
  9. cover the stuffing with the other half ktayefs and tamp well with your hands 
  10. bake in preheated oven at 180 degrees until konafa is golden 
  11. at the end of the oven, drizzle with cold honey 



**merci pour vos visites et commentaires, et passez une belle soiree**
