---
title: Algerian cake, coconut dziriettes
date: '2012-06-26'
categories:
- confitures et pate a tartiner
- basic pastry recipes
- recettes pour bebe 4 a 6 mois

---
& Nbsp; Hello everyone, a very nice Algerian cake recipe, the pretty dziriettes, but this time, in new form, and different stuffing, coconut dziriettes. An Algerian cake recipe that I prepared on the occasion of the help and which was a great success. Ingredients: 250 grs of flour 75 gr of margarine 1 pinch of salt 1 ca vanilla coffee 1 ca dye coffee (blue) orange blossom water + water stuffing: 3 measures of coconut 1 measure 1/2 powdered sugar 1c & hellip; 

##  Overview of tests 

####  vote spv 

**User Rating:** 4.6  (  1  ratings)  0 

Hello everybody, 

a very nice Algerian cake recipe, the pretty dziriettes, but this time, in new form, and different stuffing, from coconut dziriettes. 

An Algerian cake recipe that I prepared on the occasion of the help and which was a great success. 

Ingredients: 

  * 250 grams of flour 
  * 75 gr of margarine 
  * 1 pinch of salt 
  * 1 cup of vanilla coffee 
  * 1 cup of dye (blue) 
  * orange blossom water + water 



the joke: 

  * 3 measures of coconut 
  * 1 measure 1/2 of powdered sugar 
  * 1 tablespoon melted butter 
  * Lemon zest 
  * honey 
  * Eggs as needed 
  * 1 teaspoon of baking powder. 



Preparation of the dough: 

  1. In a container sift the flour, add the butter in pieces, add the pinch of salt, rub between the hands. add the vanilla Mix well. 
  2. add the water and orange blossom water to which you have added the dye, and mix until you get a smooth, firm dough. 



Preparation of the stuffing: 

  1. in a container, put the coconut, the sugar, the lemon zest and the baking powder, add the melted butter. 
  2. mix everything, and add the eggs until you get a little tricky stuffing. 
  3. Oil the dough molds, lower the dough finely with the pastry roller, or with the dough machine. 
  4. Cut out circles and go through the dice molds. 



5\. Pour 1 tablespoon stuffing to cover the dough. 

6\. cut small rounds of pasta dough, using a toothpick, make holes in the stuffing, attach small circles of the dough that you bend to have a cone. 

7\. bake at 150 degrees C. 

8\. After cooking, unmold then dip in honey and decorate with food gloss. 

Good tasting. 

Thank you for your feedback 

et merci de vouloir vous abonnez a ma newsletter pour ne pas rater toutes mes recettes a venir. 
