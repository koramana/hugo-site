---
title: tuna rolls
date: '2017-06-27'
categories:
- appetizer, tapas, appetizer
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/roul%C3%A9s-au-thon.jpg
---
[ ![tuna rolls](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/roul%C3%A9s-au-thon.jpg) ](<https://www.amourdecuisine.fr/article-les-roules-au-thon.html/roules-au-thon-3>)

##  tuna rolls 

Hello everybody, 

Tuna rolls is a recipe that I can proudly present as I have guests, and I know that it will please so much, that my guests will ask for the recipe before leaving, because it's a real fondant, a incomparable taste, especially if you like tuna ... 

I also like to prepare the tuna rolls, during the holy month of Ramadan, besides it is a very beautiful entry, which accompanies well the [ chorba ](<https://www.amourdecuisine.fr/article-chorba-soupe-au-ble-vert-concasse-du-ramadan.html> "Chorba / crushed green wheat soup from Ramadan in video") , and that will easily fill the absence of [ matloue ](<https://www.amourdecuisine.fr/article-25345316.html> "Matlouh, matlou3, matlouh - home-made tajine bread") and [ boureks ](<https://www.amourdecuisine.fr/article-bricks-bourek-entrees-recette-d-accompagnement-ramadan-2013-118781731.html> "bricks, bourek, starters, ramadan recipe 2013") at the same time. 

[ ![ROLLED au thon4_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/roules-au-thon4_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-bricks-bourek-entrees-recette-d-accompagnement-ramadan-2013-118781731.html/roules-au-thon4_thumb1-2>)

_A small remark:_

Do not think that this recipe, and that if you are only four people, that it will be a big quantity for you! These tuna rolls pass in the blink of an eye, and you will quickly look for the nearest opportunity to remake them again, again and again. 

You can see the opinions of my readers who made this recipe on my articles my achievements at home [ right here ](<https://www.amourdecuisine.fr/article-mes-recettes-chez-vous-2.html>) , [ right here ](<https://www.amourdecuisine.fr/article-mes-recettes-chez-vous-12.html>) and [ right here ](<https://www.amourdecuisine.fr/article-mes-realisation-chez-vous-31.html>) .... and I'm sure a lot of people have made this recipe and we love it. 

**tuna rolls**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/roul%C3%A9-au-thon-1.jpg)

portions:  6-7  Prep time:  45 mins  cooking:  20 mins  total:  1 hour 5 mins 

**Ingredients** For the dough 

  * 3 cups of flour 
  * 3 tablespoons milk powder 
  * 1 teaspoon of salt 
  * 1 tablespoon of sugar 
  * 1 tablespoon baker's yeast 
  * 1 teaspoon of baking powder 
  * 4 to 5 tablespoons of oil 

for the farce 
  * 1 onion 
  * 2 fresh tomatoes 
  * 1 can of tuna 
  * 1 handful of pitted olives 
  * salt, black pepper. 



**Realization steps** for the pasta: 

  1. Mix all these ingredients and add enough lukewarm water until soft, soft and easy to work. 
  2. Knead a few minutes. Form the dough into a ball and let it rise under cover, protected from drafts until it has doubled in volume. 

Prepare the tuna stuffing 
  1. open the can of tuna, put its oil in a pan. 
  2. chop the onions, and sauté them in the oil, add the cut tomato, and season to taste. 
  3. let cool. 
  4. now that the dough has doubled in volume, degas it, and spread it into a large rectangle, decorate the onion stuffing, sprinkle over the tuna in pieces, and the olives cut into small slices.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/roules-au-thon2_2.jpg)
  5. roll the dough on itself, do not forget to brush the last side of the dough with a little egg so that it does not open. 
  6. cut pieces of 2 cm wide, it's a bit difficult, because the stuffing may come out, but you can arrange it   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/roules-au-thon1_2.jpg)
  7. place your rolls on an oiled plate, and let rise a little. 
  8. before placing in the oven, brush the rolls with a little egg yolk. 
  9. cook in a hot oven. 
  10. and enjoy the delight 


