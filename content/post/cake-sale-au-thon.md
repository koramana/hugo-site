---
title: Salty cake with tuna
date: '2015-05-30'
categories:
- appetizer, tapas, appetizer
- pizzas / quiches / tartes salees et sandwichs
- recette de ramadan
tags:
- inputs
- Algeria
- Morocco
- Amuse bouche
- Tunisia
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/cake-sal%C3%A9-au-thon-1.jpg
---
[ ![salty cake with tuna 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/cake-sal%C3%A9-au-thon-1.jpg) ](<https://www.amourdecuisine.fr/article-cake-sale-au-thon.html/cake-sale-au-thon-1>)

##  Salty cake with tuna 

Hello everybody, 

For a picnic, for a buffet, for a light meal with a salad, or to accompany the Ramadan soup, here is a delicious cake with tuna. A recipe super easy to make, cake version, or mini cake version. 

A recipe from my friend **Mil lakhdari** , who shared the recipe with us, on my little group of facebook ... This recipe has made a great success on our group ... It is easy to do, in addition, instead of tuna, you can leave free choice to your imagination ... Besides if you like salty cakes, why not go for a ride on these recipes: [ Salted cake feta olives ](<https://www.amourdecuisine.fr/article-cake-sale-aux-olives-et-feta.html>) , [ pepper and camembert cake ](<https://www.amourdecuisine.fr/article-cake-aux-poivrons-et-camembert.html>) , and [ salty cake with vegetables ](<https://www.amourdecuisine.fr/article-cake-sale-aux-legumes-pour-aperitifs-ou-base-pour-amuse-bouche-facile-et-rapide.html>)

**Salty cake with tuna**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/cake-sal%C3%A9-au-thon.jpg)

portions:  6  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients**

  * 150g of flour, 
  * 1 packet of baking powder, 
  * 4 eggs, 
  * 10 tablespoons of milk, 
  * 50 g grated cheese 
  * 2 tablespoons chopped parsley 
  * salt pepper, 
  * 1 half cup of baking soda 
  * tuna 



**Realization steps**

  1. In a container, mix flour, baking powder, eggs, and milk 
  2. stir, add grated cheese, chopped parsley, salt, black pepper, and baking soda. 
  3. Pour the mixture into buttered and floured molds, 
  4. put in the center of each pan 1 teaspoon of tuna, 
  5. stir gently with the spoon to have the marbled effect 
  6. cook 25 to 30 minutes at 180 degrees. 



[ ![salty cake with tuna 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/cake-sal%C3%A9-au-thon-2.jpg) ](<https://www.amourdecuisine.fr/article-cake-sale-au-thon.html/cake-sale-au-thon-2>)
