---
title: chicken fajitas, wraps or chicken tortillas
date: '2017-08-03'
categories:
- diverse cuisine
- Cuisine by country
- pizzas / quiches / pies and sandwiches
tags:
- quesadilla
- Mexico
- Vegetables
- Home made
- accompaniment
- Sandwich
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/08/les-fajitas-au-poulet-les-wraps-ou-les-tortillas-au-poulet-1.jpg
---
![chicken fajitas, wraps or chicken tortillas 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/08/les-fajitas-au-poulet-les-wraps-ou-les-tortillas-au-poulet-1.jpg)

##  chicken fajitas, wraps or chicken tortillas 

Hello everybody, 

By this heat, we can not stand hours and hours before the furnaces, that's why I'm looking every time a recipe simple and easy to do quickly but especially to enjoy with great happiness. 

So here are chicken fajitas (we do not pronounce the "J" my friends, we all put in Spanish, hihihih), or wraps or chicken tortillas, you choose the name you like. 

It is almost the equivalent of the Syrian chawarma whose video you can see: 

{{< youtube qWA24tVEO_I >}} 

But here it is the Mexican version with spice (choice of course) and avocado, huuuuuuuuuum a delight. In any case you can here see the recipe of [ flour tortillas ](<https://www.amourdecuisine.fr/article-tortillas-a-la-farine.html>) that I use for these wraps, you can always use the commercial tortillas. 

When taking photos, I pass you the photos that I made quickly with my Samsung, because these tortillas were for dinner so everyone was waiting for his turn to eat his share for which he chose his prank. hehehe 

Just a note, the real wraps start just right by putting the stuffing in the tortilla, but at home, I always have to put the tortilla in the pan just to give it some crunchiness before stuffing it, including has people who like to pass the tortilla panini machine, I personally do not like this method. So it's up to you to see how you like to taste your tortillas.   


**chicken fajitas, wraps or chicken tortillas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/08/les-fajitas-au-poulet-les-wraps-ou-les-tortillas-au-poulet-2.jpg)

portions:  8 

**Ingredients**

  * 8 flour tortillas 
  * 2 white chickens 
  * 1 red pepper 
  * 1 green pepper 
  * 1 red onion 
  * 1 lawyer 
  * green salad of your choice 
  * fresh tomato 
  * mayonnaise 

for the marinade: 
  * 1 C. strong paprika (chili) 
  * 1 C. powdered cumin 
  * 1 C. coffee powdered garlic 
  * the juice of half a lemon 
  * extra virgin olive oil 



**Realization steps** for the marinade: 

  1. In a bowl, mix chicken strips with garlic, cumin, paprika, lemon juice and 1 tablespoon oil. Cover and book cool for at least 2 hours. 
  2. fry the two peppers in a little oil, remove the skin and cut into slices. 
  3. at the time of preparation, cut the onions into strips, as well as the avocados 
  4. Now sauté the pieces of chicken in a little hot oil. 
  5. cook until sauce is well reduced. 
  6. in another pan brown the tortillas quickly 
  7. cover the surface of each tortilla with a little mayonnaise, garnish with salad, fresh tomato slices, avocado, sliced ​​onions, sliced ​​peppers, add the chicken pieces, then garnish with a mayonnaise fillet. 
  8. roll the edges of the tortilla to cover well the stuffing and roll in a paper towel or aluminum if you roll so that the sauce escapes. 
  9. enjoy quickly. 



![chicken fajitas, wraps or chicken tortillas](https://www.amourdecuisine.fr/wp-content/uploads/2017/08/les-fajitas-au-poulet-les-wraps-ou-les-tortillas-au-poulet.jpg)
