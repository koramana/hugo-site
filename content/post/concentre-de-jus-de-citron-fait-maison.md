---
title: Concentrated homemade lemon juice
date: '2015-05-23'
categories:
- boissons jus et cocktail sans alcool
tags:
- Lemonade
- Organic cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/concentr%C3%A9-de-jus-de-citron.jpg
---
[ ![concentrated lemon juice](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/concentr%C3%A9-de-jus-de-citron.jpg) ](<https://www.amourdecuisine.fr/article-concentre-de-jus-de-citron-fait-maison.html/concentre-de-jus-de-citron>)

##  Concentrated homemade lemon juice 

Hello everybody, 

With all these chemical additions in the food products we consume on a daily basis, the home made becomes a necessity. Personally now I try to do everything at home, to avoid to our children all these products which will not import anything, or rather which force will poison them. 

Today, and as long as it's always the luscious and juicy lemons season, we will enjoy this recipe of nitre friend who holds the page: A moment of pleasure, and who shows us how to make a concentrated juice of lemon at home, you can then freeze it, or keep it in the fridge if you are going to use it right away. 

**Concentrated homemade lemon juice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/concentr%C3%A9-de-jus-de-citron1.jpg)

portions:  6  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * untreated lemons 
  * sugar 



**Realization steps**

  1. wash your lemons well 
  2. take the zest   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/concentr%C3%A9-de-jus-de-citron6.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42311>)
  3. and extract their juice   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/concentr%C3%A9-de-jus-de-citron5.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42310>)
  4. for ¼ of juice put 400 gr of sugar mix everything (zest juice and sugar)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/concentr%C3%A9-de-jus-de-citron4.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42309>)
  5. cover well and leave in fridge 3 days from time to time stir a little 
  6. Then pass them through a clean muslin   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/concentr%C3%A9-de-jus-de-citron3.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42308>)
  7. You will have at the end a concentrate of lemon juice to which you can add water according to your taste   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/concentr%C3%A9-de-jus-de-citron2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42307>)
  8. You can use the remaining zest in your cakes. 



[ ![concentrated lemon juice1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/concentr%C3%A9-de-jus-de-citron1.jpg) ](<https://www.amourdecuisine.fr/article-concentre-de-jus-de-citron-fait-maison.html/concentre-de-jus-de-citron1>)
