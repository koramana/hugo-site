---
title: Shortbread ettaj shortbread chocolate crown
date: '2015-05-15'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya
- Gateaux Secs algeriens, petits fours
tags:
- Algerian cakes
- Algeria
- Cakes
- Cookies
- Aid
- biscuits
- Little butter

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-en-couronne-et-chocolat.CR2_.jpg
---
[ ![shortbread in crown and chocolate.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-en-couronne-et-chocolat.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-sables-ettaj-sables-la-couronne-au-chocolat.html/sables-en-couronne-et-chocolat-cr2>)

##  Shortbread ettaj shortbread chocolate crown 

Hello everybody, 

For an afternoon with friends, I concocted these short chocolate shortbread, shortbread crown, not the crown, but it looks a bit. Go, I'll give them to the new little princess of the royal family of England ... Yes, there are born princes and princesses, and there are who are born ... like us, hihihihi. 

Anyway, I come back to those little shortcakes that I saw a long time ago on the forum djelfa, and I realized them at the feast of aid el fitr last year, but it was a nice mess, because it was heat as not possible at the time, and the chocolate was stubborn harden. We savored well despite all these shortbreads fondant, and too good, but these shortbread cookies were ready to appear in front of the camera .... 

So to welcome my girlfriends, and as long as it is generously cold here in England ... (To know if we must envy people in Algeria who have more than 37 degrees C in May only, or it is they who will envy us for this rainy weather and these winter temperatures ???). I prepared shortbread, always with my same recipe [ shortbread with strawberry jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture-de-fraises.html> "sands with strawberry jam") So I did a 3 in 1: one paste, and 3 different presentations. 

I invite you to make this shortbread recipe, you will not regret it at all. When this form, I used a silicone mold that I bought in Algeria, the cavities are very small, because the diameter of each is only 4 cm, so it is shortbread are small and all mimic is not very good silicone molds ok. 

[ ![shortbread ettaj be chocolate.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-ettaj-be-chocolat.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-sables-ettaj-sables-la-couronne-au-chocolat.html/sables-ettaj-be-chocolat-cr2>)   


**Shortbread ettaj - shortbread chocolate crown**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-etaj-sabl%C3%A9s-la-couronne.jpg)

portions:  45  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients**

  * 500 grams of butter (at room temperature) 
  * 2 bags of vanilla sugar 
  * 250 grams of icing sugar 
  * 4 egg yolks (the whites I do the mcheweks with) 
  * 2 bags of yeast 
  * 1 kg of flour 
  * decoration: 
  * chocolate (I only had milka milk chocolate on hand) 
  * coconut 
  * jam 



**Realization steps**

  1. beat the butter with the sugar and the vanilla until you have a nice cream. 
  2. add the egg yolks and whisk well once more 
  3. introduce the yeast and the flour (I do not put all the flour of the times, to know its quality) 
  4. wrap the dough in cling film, and let stand for 30 minutes 
  5. take pellets of almost 30 gr, and fill the mold cavity, tamp with the floured fingers to escape any air ball   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/remplissage-des-moules.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42091>)
  6. bake in a preheated oven at 180 degrees for almost 20 minutes, watch the cooking well.   
for a large quantity of this shortbread, you have to have at least 3 of these molds, each mold makes 15 pieces of shortbread 
  7. let the shortbreads cool, then carefully remove it from the silicone molds. 
  8. Wash the silicone molds, and melt the chocolate in a water bath. 
  9. with a brush, generously cover each mold imprint with the chocolate 
  10. As soon as you cover the imprint place the shortbread piece in it, and go to the second imprint, So you have to put the cake in the mold on the chocolate, as long as the chocolate is still melting. 
  11. leave in a cool place until the chocolate hardens (I left overnight), and gently remove each print, do not be afraid to push with your fingers to get the cake out the other side. 
  12. with a brush, cover the shortbreads with jam on the uncovered portion of chocolate, and place the cakes in a small plate with coconut, and cover the jam. 
  13. let dry at least, 1 hour. 
  14. These shortbreads can be kept before covering them with chocolate for 15 days, but if they are covered, they can be kept for a maximum of one week, if you still want to keep the beautiful chocolate decoration. 



[ ![sandblasted ettaje, sandblasted crown.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/sabl%C3%A9s-ettaje-sabl%C3%A9s-la-couronne.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-sables-ettaj-sables-la-couronne-au-chocolat.html/sables-ettaje-sables-la-couronne-cr2>)
