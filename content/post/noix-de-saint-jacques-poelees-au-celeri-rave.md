---
title: Fried scallops with celeriac
date: '2017-12-20'
categories:
- fish and seafood recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/noix-de-saint-jacques-poelees-sur-un-lit-de-celeri-rave-038.jpg
---
![scalloped walnuts with celeri rave](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/noix-de-saint-jacques-poelees-sur-un-lit-de-celeri-rave-038.jpg)

##  Fried scallops with celeriac 

Hello everybody, 

Sautéed scallops, on a bed of celery root puree, garnished with a very delicious sauce of chives, garlic and parsley, then garnished with some toasted hazelnuts, I do not tell you the delight. 

a recipe that will take you long to realize when you read it, but in fact it is realized in less than 45 minutes ... 

![nuts-saint-Jacques-fried-and-celery-root 049.CR2_2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/noix-de-saint-jacques-poelee-et-celeri-rave-049.CR2_2.jpg)

Ingredients:   
for 4 people   


**Fried scallops with celeriac**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/noix-de-saint-jacques-poelee-entree-040.CR2_2.jpg)

portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 12 walnuts of Saint Jacques (shells of Saint Jacques) 
  * a few sprigs of fresh parsley, hulled 
  * a few sprigs of fresh tarragon, hulled 
  * 2 cloves of garlic, peeled 
  * 1 tablespoon capers, well rinsed 
  * some stalks of chives. 
  * olive oil 
  * salt and black pepper 
  * [ mashed celery root ](<https://www.amourdecuisine.fr/article-puree-de-celeri-rave-114258163.html> "Mashed recipe of celery root / delicious recipe easy")
  * grilled and crushed hazelnuts. 



**Realization steps**

  1. preparation of the herb sauce: 
  2. in a mortar, place chopped parsley, chives and tarragon, garlic, salt, black pepper, and caper. 
  3. crush well to have a dough, and introduce the olive oil slowly, to have a consistency according to your taste. 
  4. This sauce can be prepared in advance and refrigerated, and then warm it up when serving. 

There you go [ how to prepare the recipe for roe celery ](<https://www.amourdecuisine.fr/article-recette-puree-de-celeri-rave-recette-delicieuse-facile.html>) . now prepare the hazelnuts: 

  1. Heat the oven to 160 degrees C, spread the nuts on a baking sheet and grill in the oven until lightly browned, about 8 minutes. 
  2. For scallops: 
  3. sear scallops, heat a little olive oil in a large nonstick skillet over high heat. 
  4. When the oil is hot, add the scallops in batches, leaving a gap between them. 
  5. Let cook, without turning, until well browned and crisp, about 1-2 minutes. 
  6. Delicately turn them over and fry for another 1-3 minutes, depending on the size of the scallops. 
  7. you can serve the celery puree garnished with pan-fried scallops, and top with the sauce of herbs, in individual plates. 
  8. if not serve by decorating the shells of St. James. 
  9. sprinkle over crushed hazelnuts. 



[ ![nuts-saint-Jacques-fried-and-celery-root 049.CR2_2](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/noix-de-saint-jacques-poelee-et-celeri-rave-049.CR2_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/noix-de-saint-jacques-poelee-et-celeri-rave-049.CR2_2.jpg>)
