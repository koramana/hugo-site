---
title: muslin cream (ideal for strawberry)
date: '2013-05-18'
categories:
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/creme-mousseline_thumb.jpg
---
##  muslin cream (ideal for strawberry) 

Hello everybody, 

Muslin cream is a very delicious cream and very flexible, which has a pastry cream base to which you add butter. 

For my part, I do not add too much butter, so that it is not too fat, but it is a question of choice. In any case the cream muslin is part of many of my recipes, I like the sweet taste of butter, when I garnished my pies and cakes with this cream muslin. and I often use this cream especially when I'm doing [ Strawberry plant ](<https://www.amourdecuisine.fr/article-fraisier-104098836.html>)   


**muslin cream (ideal for strawberry)**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/creme-mousseline_thumb.jpg)

**Ingredients**

  * [ custard ](<https://www.amourdecuisine.fr/article-creme-patissiere.html>) made with 500 ml of milk. 
  * 250 gr of butter (you can go up to 400 gr of butter) 



**Realization steps**

  1. method of realization 
  2. remove the custard and butter a little earlier from the fridge so that both are at the same temperature 
  3. cut the butter into pieces. 
  4. whip a little custard. 
  5. and begin by introducing the butter in a small quantity, without stopping whipping, the cream will become more and more unctuous, light and airy. 
  6. put in a bowl and keep in the fridge until use. 


