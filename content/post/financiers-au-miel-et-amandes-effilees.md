---
title: with honey and flaked almonds
date: '2016-10-22'
categories:
- Cupcakes, macaroons, and other pastries
- recettes sucrees
tags:
- Cakes
- desserts
- To taste
- Easy cooking
- Pastry
- Algerian cakes
- biscuits

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/financiers-au-miel-et-amandes-effil%C3%A9es-1.jpg
---
![Financial-in-honey-and-almond-tapered-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/financiers-au-miel-et-amandes-effil%C3%A9es-1.jpg)

##  with honey and flaked almonds 

Hello everybody, 

Another recipe from Lunetoiles financiers with honey and flaked almonds that was in the dust of my email since 2013 !!! 

These financiers with honey and flaked almonds are ideal for snacking, picnics and Lunch box and on top of all this it's a super easy recipe to make. 

In any case, as I still have not put my kitchen in order after the great work on it, and especially that the children are on vacation, I will enjoy publishing all recipes Lunetoiles archived in my email for years, until 'to find my kitchen, my ustencils and the space to photograph. 

This recipe of financiers with honey and tapered almonds is a recipe from the book: Dessert happy days Guillemette Auboyer who is not the author of the blog **chocolate and so on**   


**with honey and flaked almonds**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/financiers-au-miel-et-amandes-effil%C3%A9es.jpg)

portions:  12  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 80 gr of ground almonds 
  * 120 gr of butter 
  * 180 gr of icing sugar 
  * 80 gr of flour 
  * 200 gr of egg white (about 6 egg whites) 
  * 3 tablespoons of honey 
  * 60 gr of flaked almonds 



**Realization steps**

  1. Roast the almond powder by passing it for a few minutes under the grill of the oven, watching well or by heating it dry in a stove on a low heat. 
  2. Melt the butter over low heat and let it cook until it turns a nice golden color and a good smell of hazelnut. 
  3. Preheat the oven to 220 ° C (th.7-8) 
  4. Mix the icing sugar, flour and roasted almond powder. 
  5. Add the hazelnut butter and the white ones, mix, then add the honey, mixing until the dough is smooth. 
  6. Pour the dough into well-buttered muffin pans. 
  7. Sprinkle with flaked almonds and bake for about 10 minutes, the financiers should be lightly browned and puffed. 


