---
title: chicken legs with baked vegetables «دجاج بالفرن»
date: '2010-09-27'
categories:
- cuisine algerienne
- cuisine diverse

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/pomfour-017_thumb1.jpg
---
##  chicken legs with vegetables in the oven 

Hello everybody, 

chicken legs with vegetables in the oven is the dish that I often do when I'm in a hurry. There is no easier is more good than this delicious dish that is done in the blink of an eye.   
  
Ingredients:   
\- 3 to 4 potatoes by size   
\- 5 to 6 chicken legs, or depending on the number of people   
\- 1 onion   
\- 2 cloves of garlic   
\- 1 handful of pitted green olives (optional)   
\- 1 handful of green beans (optional)   
\- 2 to 3 fresh tomatoes (in box for me)   
\- 2 tablespoons of olive oil   
\- salt, black pepper, provincial herbs, oregano, and everything you love as a spice. 

in a gratin baking pan, place the onion cut in length, and the tomatoes cut into pieces, pour over a little olive oil, place on top of the chicken legs, the potato cut in, the olives, green beans.   
in a glass of water, dissolve the salt, mix the spices, pour over the contents of the gratin dish, garnish with some of the herbs and oregano.   
cover with a sheet of aluminum foil, and place the mold in a hot oven, let cook between 1h15 min to 1:45, depending on your oven of course. 

towards the end of cooking, remove the aluminum foil, and let your dish take a nice color.   


**chicken legs with baked vegetables "دجاج بالفرن"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/pomfour-017_thumb1.jpg)

**Ingredients**

  * 3 to 4 potatoes by size 
  * 5 to 6 chicken legs, or depending on the number of people 
  * 1 onion 
  * 2 cloves of garlic 
  * 1 handful of pitted green olives (optional) 
  * 1 handful of green beans (optional) 
  * 2 to 3 fresh tomatoes (boxed for me) 
  * 2 tablespoons of olive oil 
  * salt, black pepper, provincial herbs, oregano, and everything you love as a spice. 



**Realization steps**

  1. in a gratin baking pan, place the onion cut in length, and the tomatoes cut into pieces, pour over a little olive oil, place on top of the chicken legs, the potato cut in, the olives, green beans. 
  2. in a glass of water, dissolve the salt, mix the spices, pour over the contents of the gratin dish, garnish with some of the herbs and oregano. 
  3. cover with a sheet of aluminum foil, and place the mold in a hot oven, let cook between 1h15 min to 1:45, depending on your oven of course. 
  4. towards the end of cooking, remove the aluminum foil, and let your dish take a nice color. 
  5. chicken legs with baked vegetables "دجاج بالفرن" 
  6. Author: Kitchen Love 
  7. Ingredients 
  8. has 4 potatoes by size 
  9. has 6 chicken legs, or depending on the number of people 
  10. onion 
  11. clove garlic 
  12. handful of pitted green olives (optional) 
  13. handful of green beans (optional) 
  14. has 3 fresh tomatoes (in box for me) 
  15. tablespoons of olive oil 
  16. salt, black pepper, provincial herbs, oregano, and everything you love as a spice. 



Enjoy your meal 
