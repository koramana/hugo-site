---
title: gratin pancakes with shrimps
date: '2015-07-10'
categories:
- crepes, waffles, fritters
tags:
- ramadan 2015
- Ramadan
- Ramadhan

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/c-f-002_thumb.jpg
---
[ ![gratin of crepes with shrimps](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/c-f-002_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/c-f-002_thumb.jpg>)

##  gratin pancakes with shrimps 

Hello everybody, 

Here is a super nice recipe with pancakes, a recipe from  salted crepes  , Gratin pancakes with shrimp is a recipe from [ Houriat el matbakh ](<https://www.amourdecuisine.fr/categorie-11131465.html>) , which breaks down on the chain **fatafeat TV.**

this recipe will take a little time to complete because you have to start with the realization of [ Crepes ](<https://www.amourdecuisine.fr/categorie-12348217.html>) , then you have them stuffed with a nice shrimp sauce at the [ Bechamel ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile-99197863.html>) , which will give a dish of melting and complete gratin ... 

**gratin pancakes with shrimps**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/c-f-002_thumb.jpg)

portions:  4  Prep time:  40 mins  cooking:  30 mins  total:  1 hour 10 mins 

**Ingredients** for the crepe dough: (must say that I made half of the ingredients, and I had a good amount of pancakes, already half is in the freezer) 

  * 400 g flour 
  * 3 eggs 
  * 50 g of melted butter 
  * 1 liter of milk 
  * A pinch of salt 
  * 1 tablespoon of sugar 

the joke: 
  * 300 g cooked shrimp 
  * 250 g mushrooms 
  * 2 cloves garlic 
  * 2 tbsp. chopped parsley 
  * 1 C. soup of lemon juice 
  * Black pepper to taste 
  * ½ teaspoon of salt 
  * a little nutmeg 
  * 100 g of pitted olives 
  * A sliced ​​onion 
  * Half a red pepper grilled and cut into pieces 
  * the bechamel: 
  * Bechamel sauce: 
  * 2 tbsp. butter 
  * 2 tbsp. flour 
  * Milk or 400 ml (200 ml of milk + 200 fresh cream (not for me)) 
  * Little spoon of salt 
  * a little chopped garlic 
  * a little nutmeg 
  * Black pepper to taste 
  * 200g of Emmantal cheese (for me it was a bit of Gruyère cheese) 
  * 50 g of parmesan cheese 



**Realization steps** preparation of crepes: 

  1. Mix the dry ingredients, then add the milk, eggs and melted butter and mix well with a whisk if you have one 
  2. Let the dough rest a little while, heat a frying pan and rub with a little butter, pour the amount of a small ladle and spread well cook on one side, then on the other side too. and let it cool a little   
[ ![gratin pancakes with shrimps](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepe-farcee-005_thumb-300x224.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepe-farcee-005_thumb.jpg>)

preparation of the stuffing: 
  1. In a frying pan over high heat put the butter and fry the mushrooms until it is a beautiful gilding, add the lemon juice, parsley, salt and black pepper and set aside. 
  2. on the other side put some butter and fry the onions and garlic, then add the already cooked shrimp too, then add the other ingredients and a little Bechamel sauce.   
[ ![gratin pancakes with shrimps](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepe-farcee-009_thumb-300x224.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepe-farcee-009_thumb.jpg>)

preparation of bechamel sauce: 
  1. In a saucepan over low heat, put the butter, then add the flour and mix well until it turns a nice golden color, then add the milk and mix without stopping. Add salt, black pepper, nutmeg and crushed garlic. do not stop stirring with a spoon until a mixture becomes thick. 

preparation of the dish: 
  1. Stuff each pancake with a large spoonful of stuffing and place in a well-buttered pan, cover with a little béchamel sauce and sprinkle with grated cheese then put in a hot oven to grill for 15 to 20 minutes. minutes 
  2. serve hot with a nice salad. 


