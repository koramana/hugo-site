---
title: lychee strawberry smoothie
date: '2016-01-24'
categories:
- juice and cocktail drinks without alcohol
- Healthy cuisine
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-fraises-lychee.jpg
---
[ ![lychee strawberry smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-fraises-lychee.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-fraises-lychee.jpg>)

##  lychee strawberry smoothie 

Hello everybody, 

There will always be Inchallah, smoothies recipes on my blog, because once you get started, you become an addict. Especially me who is not at all the kind to taste a fruit after eating, or when I'm hungry ... For me, homemade smoothies, healthy, blessing for health are the only solution to consume fruits a will ... and you, are you going to go there? 

This time, it is a recipe of my dear Lunetoiles which was on the archives of my email since already May 2014 .... Hihihih, I apologize for not sharing this recipe, but it's never too late to do it ... so here's a delicious and tempting smoothie with strawberries and lychees. 

![lychee strawberry smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-fraises-lychee-1.jpg)

**lychee strawberry smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-fraises-lychee-2.jpg)

portions:  4  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 1 box of canned litchis (about 500 gr, weight counting the juice) 
  * 250 g frozen strawberries 
  * 12 cl of milk 
  * 60 ml of lychee juice taken from the can 
  * 35 cl of orange juice 
  * 1 tbsp. vanilla extract (optional) 
  * 2 teaspoons of honey or agave syrup, to taste 
  * Ice cubes, optional 



**Realization steps**

  1. Place frozen strawberries and canned litchis in a blender 
  2. Add the reserved juice from the lychee preserve, and the orange juice and the milk. 
  3. Blend until smooth. 
  4. Add vanilla extract if desired, honey or agave syrup, and ice cubes (if available) and mix. 
  5. Serve immediately in large glasses, or refrigerate until ready to serve. 
  6. Enjoy! 



[ ![lychee strawberry smoothie 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-fraises-lychee-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-fraises-lychee-3.jpg>)
