---
title: Tarte tatin with quince
date: '2011-12-21'
categories:
- Café amour de cuisine

---
& Nbsp; hello everyone, Here is a beautiful cake, it's like me you like quince, well this pie will really awaken all your senses of tasting. I do not know where I saw the recipe, a tarte tatin quince, must say I never think about it, so I saw the recipe, I read it, but not keep the link, or rather I will say, I lost the link, but I was waiting for the opportunity to do it, and as today I'm doing my fast, I said to myself, I can afford a dessert to find my enrage (on & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.38  (  2  ratings)  0 

Hello everybody, 

Here is a beautiful cake, it's like me you like quince, well this pie will really awaken all your senses of tasting. 

I do not know where I saw the recipe, a tarte tatin quince, must say I never think about it, so I saw the recipe, I read it, but not keep the link, or rather I will say, I lose the link, but I was waiting for the opportunity to do it, and as today I'm doing my fast, I said to myself, I can afford a dessert to find my engrgie (we easily find pretexts to eat a delice, hihihihiih) 

Ingredients: 

  * 3 big quinces 
  * 100 g 
  * 1 liter of water 
  * 4 seeds of badienne or anise etoilee 



for caramel 

  * 180 g caster sugar 
  * 2 tablespoons water 



for the pate sande: 

  * 250g of flour 
  * 125 g caster sugar 
  * 1 egg 
  * 100 g of butter 
  * 1 pinch of salt 



30 g butter for cooking 

  1. In a bowl, mix the egg with the powdered sugar and a pinch of salt 
  2. Put the flour on the work plan, make a well in the center, 
  3. pour in the contents of the bowl and the zest of lemon, 
  4. add the melted butter (especially not liquid butter!) 
  5. mix well to have a smooth dough. 
  6. let the dough rest in the fridge 
  7. peeled and cut quince into medium-thick quarters 
  8. take the water with the 100 g sugar, 
  9. add the seeds of Anis etoilee and the quarters of quinces 
  10. cook for at least 30 min until the quince has a nice color. 
  11. prepare the caramel with the 180 grs of sugar with a little water 
  12. cook until you get the color you want 
  13. immediately immerse the pan in cold water, to stop the cooking of the sugar 
  14. pour your caramel in a mold of 20 to 22 cm 
  15. let alone 
  16. drain the quinces 
  17. Place them in the bottom of the mold. 
  18. Arrange the 30 gr of butter in parcels on top and bake for 30 min. 
  19. Remove the mold from the oven, remove the cooking juices (keep for a possible decoration) 
  20. spread the batter and cover the quince well, prick the dough in the middle 
  21. Bake again for at least 20 minutes. 
  22. Let cool for 5 min then unmold, be careful not to burn. 



in any case, she was super good this pie. 

merci pour tout. 
