---
title: stuffed Swiss chard leaves - Lebanese recipe
date: '2014-12-09'
categories:
- Algerian cuisine
- diverse cuisine
tags:
- dishes
- Lamb
- Lamb tagine
- tajine
- Stew
- East
- Having dinner

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-viande-hachee-2.jpg
---
[ ![chopped meat with chopped meat 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-viande-hachee-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-viande-hachee-2.jpg>)

##  Stuffed Swiss chard leaves - Lebanese recipe 

Hello everybody, 

A Lebanese recipe that tells you? Here is a delicious one anyway, **stuffed Swiss chard leaves** to the **minced meat** cooked on a bed of onions and lamb chops ... 

I tasted this dish last year when we met mom in the children's school. Every Monday, two of the mothers prepared a traditional dish of their country that they made us taste. One of the moms a Kurd, we prepared leaves of Swiss chard stuffed, we all liked the recipe, but the poor, and because she had a problem of English language, she could not give us the recipe… 

A few ingredients here and there, that we deciphered with difficulty, and that I tried to reconstitute today to realize this delicious dish of **Swiss chard leaves stuffed with minced meat** .. I can tell you that the dish is super good, even my husband, who at first told me, I will only eat the stuffing, finally attacked the dish, even before I finished taking photos. 

**stuffed Swiss chard leaves - Lebanese recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-viande-hachee-4.jpg)

portions:  2  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** for the sauce: 

  * 1 onion cut into strips 
  * 4 to 5 chops 
  * 3 fresh tomatoes 
  * some dill leaves 
  * 1 crushed garlic clove 
  * salt and black pepper 
  * 3 tablespoons of oil 

for the stuffing: 
  * 1 bunch of Swiss chard leaves   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-225x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-225x300.jpg>)
  * 250 gr of minced meat 
  * 3 tablespoons of rice dipped in water 
  * 1 crushed garlic clove 
  * some branch of chopped dill 
  * a few sprigs of chopped parsley 
  * salt and black pepper 
  * 1 glass of water 



**Realization steps**

  1. in a pot not too wide, pour the oil 
  2. cover with a layer of sliced ​​onion 
  3. place the chops, salt and pepper gently (pay attention to the salt)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-300x225.jpg>)
  4. cover with slice of tomato, and leave aside. 

Prepare the chard rolls: 
  1. wash the leaves well 
  2. remove the white midrib of each leaf. 
  3. place the chard leaves in a large salad bowl pour over the hot water, so that the chard become easy to handle 
  4. prepare stuffing by mixing ground meat, rice, chopped parsley, chopped dill, chopped wing, salt and black pepper (not too much salt please) 
  5. If your leaves are too big, cut them in half lengthwise, removing the white rib that may be a bit hard. 
  6. place the equivalent of one tablespoon of stuffing in the widest end of the sheet 
  7. roll covering the sides, and covering well the stuffing.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-viande-hachee-225x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-viande-hachee-225x300.jpg>)
  8. When you have filled all the chard leaves, place them in the pot delicately on the tomato layer.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-libanaise-225x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-libanaise-225x300.jpg>)
  9. Cover with a flat plate so that the leaves do not break during cooking.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/belettes-farcies-a-la-viande-hachee-246x300.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/belettes-farcies-a-la-viande-hachee-246x300.jpg>)
  10. put on low heat, for 5 minutes so that the onions all the way down just melt a little. 
  11. pour over half the cup of water (if you think during cooking that it is missing some water add a little 
  12. Cover and cook, until the sauce is reduced, it should not float in the sauce even when cooking. 
  13. as soon as it is ready, serve immediately.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-viande-hachee-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-viande-hachee-1.jpg>)



[ ![stuffed chard with chopped meat 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-viande-hachee-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/blettes-farcies-a-la-viande-hachee-3.jpg>)
