---
title: aubergine gratin recipe
date: '2017-05-04'
categories:
- Algerian cuisine
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/gratin-d-aubergines-037.CR2_1.jpg
---
![gratin-and-eggplant-037.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/gratin-d-aubergines-037.CR2_1.jpg)

##  aubergine gratin recipe 

Hello everybody. 

a aubergine gratin, very easy to make, very good and especially light, without the béchamel layer, and less oil ... 

I made the recipe for only two people, and I really regretted it, because it was so good, that my husband asked me to leave him a little for the evening .... but there was no more left. 

**Eggplant Gratin On**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/gratin-d-aubergines-026.CR2_1.jpg)

Recipe type:  dish  portions:  4  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients**

  * 3 medium eggplants 
  * 600-700 g minced meat 
  * 1 medium onion 
  * 1 can of chopped tomatoes, otherwise 4 fresh tomatoes 
  * 2 cloves garlic 
  * a little parsley 
  * salt and pepper 
  * about 150 g of cheese 



**Realization steps**

  1. wash and peel the eggplant. 
  2. cut into slices about ½ cm, sprinkle with salt and leave about 30 minutes for them to release some of its water. 
  3. place them on paper, and fry in a little hot oil on both sides. 
  4. place the eggplant on paper towels to remove excess oil. 
  5. Meanwhile, put the minced meat in a saucepan (without oil) and cook, stirring often (add a little water if necessary), until the meat is well cooked. 
  6. add the chopped onion, and a little grated garlic, salt and cook for about 5 minutes, 
  7. when cooked, add a little chopped parsley and put aside 
  8. prepare tomato sauce, cooking tomatoes, with some oil, minced garlic, salt and black pepper, be careful with salt, so that the dish is not too salty. 
  9. in an ovenproof pyrex mold, place a layer of fried aubergine slices. 
  10. do not put on the ground meat, and a little tomato sauce. 
  11. then place another layer of eggplant. and some of the remaining tomato sauce. 
  12. sprinkle some chopped parsley over and put in preheated oven. 
  13. cook between 30 and 35 minutes. 
  14. minutes before the end of cooking, garnish the dish with grated cheese 
  15. serve hot, with a good piece of homemade bread. 


