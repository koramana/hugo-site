---
title: ultra easy chocolate butter cream
date: '2016-12-30'
categories:
- confitures et pate a tartiner
- recettes patissieres de base
- sweet recipes
tags:
- Cake with stages
- Cakes
- logs
- Birthday cake
- desserts
- Rolled cake
- Rolled

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/12/creme-au-beurre-au-chocolat-3.jpg
---
![creme au chocolate butter au-3](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/creme-au-beurre-au-chocolat-3.jpg)

chocolate butter cream ultra easy, 

Hello everybody, 

Many of you were wondering about the recipe for the chocolate butter cream, and today I share with you today my recipe for an ultra-easy chocolate butter cream. 

This ultra-easy-to-use chocolate butter cream is egg-free and has no broken head, just melt the chocolate without burning it, to get a super-delicious cream that's perfect for covering your holiday cakes, birthday cakes, cupcakes , rolled biscuit, or logs. 

the recipe of this biscuit rolled with chocolate butter cream, will be on my youtube channel just now. 

**ultra easy chocolate butter cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/creme-au-beurre-au-chocolat-1.jpg)

portions:  12  Prep time:  30 mins  cooking:  2 mins  total:  32 mins 

**Ingredients**

  * 100 gr of dark chocolate 
  * 150 gr of butter at room temperature 
  * 200 gr of icing sugar 
  * 2 tbsp. cocoa mixed with the icing sugar (the amount at the top) for an even darker color, otherwise the original color that you will get without cocoa is that the   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/creme-au-beurre-au-chocolat.jpg)



**Realization steps**

  1. Melt the chocolate in a bain marie, without burning it. 
  2. cut the butter in and whip 
  3. introduce the sugar in a small amount and continue whisking. (for a darker color that's where we add the cocoa) 
  4. keep whipping for a creamy mix. 
  5. introduce the melted chocolate (which is not hot of course) whip to introduce it well, and here is your cream is ready. 
  6. cool, and leave at least 30 minutes before use 



![creme au beurre au chocolate-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/creme-au-beurre-au-chocolat-2.jpg)

{{< youtube _xrTx2REy_k >}} 
