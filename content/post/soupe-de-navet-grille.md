---
title: Grilled turnip soup
date: '2015-12-12'
categories:
- soups and velvets
tags:
- Vegetable soup
- Vegetables
- Healthy cuisine
- Easy cooking
- Algeria
- fall
- Soup

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-de-navet-grill%C3%A9-2.jpg
---
[ ![grilled turnip soup 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-de-navet-grill%C3%A9-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-de-navet-grill%C3%A9-2.jpg>)

##  Grilled turnip soup 

Hello everybody, 

My children do not eat the turnip, and I always look for a trick to make them eat this vegetable, without noticing. By searching here and there, I found a superb recipe of velouté based turnip, which is grilled in the oven ... 

The result is sublime, a velvety all good and all fragrant, and with the addition of white beans, an ingredient that my children love, the velvety is spent in the blink of an eye, which suits me of course, I always like to see their dishes empty. 

**Grilled turnip soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-de-navet-grill%C3%A9-1.jpg)

portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 4-6 large garlic cloves with skin 
  * 4 large turnips cut into pieces 
  * 1 onion cut into strips 
  * 2 tablespoons extra virgin olive oil 
  * 1 tbsp fresh or dried rosemary 
  * 1 tablespoon of salt 
  * Black pepper to taste 
  * 1 glass of white beans cooked beforehand in slightly salted water 
  * 1 cube of vegetable broth diluted in 1 liter of water 
  * 1 tablespoon lemon juice (about ½ lemon) 
  * Pumpkin seeds to serve 



**Realization steps**

  1. Preheat the oven to 180 degrees C. 
  2. On large baking sheet, combine garlic, turnip, onion, oil, rosemary, salt and pepper. 
  3. Roast for 30 to 40 minutes, until garlic and parsnips are tender (turn occasionally).   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-de-navet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/soupe-de-navet.jpg>)
  4. remove from the oven, place the mixture in an adequate stockpot, do not forget to remove the skin from the garlic 
  5. add the white beans, the vegetable broth in a small amount, and the lemon juice 
  6. mix with the blender foot until smooth, 
  7. place the pot over medium heat, stirring frequently. Season according to your taste. 
  8. Serve hot with some pumpkin seeds and a drizzle of olive oil. 


