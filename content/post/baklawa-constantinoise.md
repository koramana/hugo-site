---
title: baklawa constantinoise
date: '2017-06-22'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/baklawa-gateaux-de-laid-2014-042.CR2_.jpg
---
[ ![baklawa cake of the aid 2014 042.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/baklawa-gateaux-de-laid-2014-042.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/06/baklawa-gateaux-de-laid-2014-042.CR2_.jpg>)

#  Baklawa constantinoise, باقلاوة قسنطينة 

I share with you my first piece of Baklawa, which I just removed from the board, I should not do this before tomorrow, the baklawa likes to stay in the honey at least 2 nights to hold. 

so wait for the pictures to come very soon, anyway for the moment I share with you my recipe for baklawa, the stuffing was almonds and peanuts, but on this point, you can do the stuffing you like. 

on my blog you will find: 

**[ baklawa rolls ](<https://www.amourdecuisine.fr/article-baklawa-rolls-baklava-rolls-103320801.html>) **

**[ baklawa with almonds ](<https://www.amourdecuisine.fr/article-baklawa-aux-amandes-de-lunetoiles-56933001.html>) **

**[ baklawa has filo pate ](<https://www.amourdecuisine.fr/article-25555887.html>) **

**[ baklawa with walnuts ](<https://www.amourdecuisine.fr/article-baklawa-aux-noix-nobles-88843802.html>) **

So I pass you my ingredients:   


**baklawa constantinoise**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/baklawa-gateau-algerien1.jpg)

portions:  50  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients** for a tray 35 cm in diameter: 

  * 3 measures of flour (600 grs for me) 
  * ¼ measures of melted smen (50 grs for me) 
  * 1 pinch of salt 
  * orange blossom water plus water. 

for the stuffing: 
  * 800 grams of almonds and peanuts 
  * 400g of caster sugar 
  * ½ teaspoon cinnamon 
  * 1 drop of water of orange blossom 

decoration: 
  * almonds in pieces 
  * smen to coat between each sheet 
  * honey 



**Realization steps** preparation of the stuffing: 

  1. for this time I did not peel the almonds, but rather I prefer to grill the almonds and grind them with peanuts. 
  2. add the sugar and cinnamon and sprinkle with orange blossom water. book. 

Prepare the puff pastry. 
  1. Sift the flour in a bowl. 
  2. make a well and pour in the butter. 
  3. sprinkle with salt 
  4. amalgamate everything with your fingertips. 
  5. Add little by little water while working at the same time without kneading too much, until you get a firm dough 
  6. to share the dough in 9 balls, 5 balls of 120 grs of weight, and 4 balls of 100 grs of weight. 
  7. Roll down the dough in ways to get a very fine dough 
  8. butter a tray, arrange the first sheet and brush it with melted smen 
  9. superimpose 5 sheets without forgetting to coat them as you go 
  10. pour the stuffing on the last sheet, and spread evenly 
  11. cover with the last leaves without forgetting to butter them as for the first leaves 
  12. cut out the whole diamond cake with a knife   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2010-11-15-baklawa_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2010-11-15-baklawa_thumb.jpg>)
  13. garnish each with 1 whole almond. 
  14. Sprinkle the top with 2 or 3 tablespoons of butter (or smen) 
  15. cook in a medium oven but preheated 
  16. Dice cooking and hot sprinkle with hot honey flavored with orange blossom water. 
  17. let it soak up 24 hours at least 
  18. cut out the diamonds and place them in boxes 



dégustez 
