---
title: zucchini fingers with parmesan cheese and pine nuts
date: '2016-05-10'
categories:
- amuse bouche, tapas, mise en bouche
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/aperitifs-courgettes-au-parmesan-et-pignons-de-pin.CR2_thum1.jpg
---
Hello everybody, 

here are some delicious zucchini fingers with parmesan and pine nuts, which I presented with a dips aioli with chives .... we had a good time. 

with this crunchy that covers well this melting zucchini .... even the children did not suspect the taste of zucchini .... the best thing is that you can eat without moderation, because this recipe is prepared in the oven, and not fried ... .. so it says:    


**appetizer / zucchini fingers with parmesan and pine nuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/aperitifs-courgettes-au-parmesan-et-pignons-de-pin.CR2_thum1.jpg)

Recipe type:  appetizer, aperitifs  portions:  6  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 2 medium zucchini 
  * 3 tablespoon of pine nuts 
  * 100 gr grated parmesan cheese. 
  * 6 tablespoons of panko or bread crumbs 
  * powdered spices of your choice, (I put my spice: coriander powder with garlic that I like a lot) 
  * salt 
  * flour 
  * 2 eggs 
  * 1 tablespoon of oil 



**Realization steps**

  1. wash the zucchini and cut them on two, then each share on 6 (you can see the video) 
  2. in the mixer bowl, mix pine nuts, parmesan, spices and bread crumbs, or panko 
  3. in three small trays, put some flour in the first, the eggs beaten with oil in the other, and the breadcrumbs with pine nuts and parmesan in the last 
  4. oil a baking tray, and preheat the oven to 180 degrees C 
  5. place the zucchini fingers in the flour first, then in the eggs, and finally in the last breadcrumbs. 
  6. place in the oiled tray, and cook for 15 to 20 minutes, to have a beautiful golden color. 
  7. you can prepare in advance, and just heat in the oven for 5 minutes at the last moment .... 



# 

{{< youtube 3JwMbT238Ss >}} 
