---
title: my achievements at home 46
date: '2014-09-23'
categories:
- the tests of my readers
tags:
- Soft
- Cake
- Four quarters
- Algerian cakes
- Birthday cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

It becomes very difficult for me to post this article, my recipes at home, so I get a lot of message, here in comments on my blog, or in private message on facebook, sometimes on my mailbox, that I put you on the page **[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") . **

So, I get almost every day between 10 to 15 messages from my readers, and so many times, I'm super busy, I do not put the photo immediately, on this article and I lose it with the time. So, I apologize to the people who sent me their photo essay, because I lost them, trying to empty my mailbox. 

So, I am speaking to you my dear readers, send me the photo with the link of the recipe that you tried (I speak the link from my blog) on ​​this email address: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

If you are a blogger, it will be nice to leave the link to your blog too, and if you are not a blogger, your name will be good enough. Also if you have tested and approved a recipe from my blog, and you have no photos, just leave a comment in the recipe in question. your opinion will surely help people who want to try the recipe. 

![zelliges](http://img.over-blog-kiwi.com/0/76/31/24/20140605/ob_493ab9_zelliges-2.JPG)

![Peach frangipane pie](http://parfum-oriental.net/wp-content/uploads/2014/06/IMG-20140615-WA0000-300x225.jpg)

![DSC_0528](http://thanhlycuisine.files.wordpress.com/2014/07/dsc_0528.jpg?w=400&h=266)

# 

![Olive bread](http://lesdouceursdejelila.files.wordpress.com/2014/08/pains-aux-olives.jpg?w=400&h=266)

[ ![chocolate fingers without cooking](https://www.amourdecuisine.fr/wp-content/uploads/2014/08/les-doigts-au-chocolat-sans-cuisson-300x224.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/08/les-doigts-au-chocolat-sans-cuisson.jpg>)

[ chocolate fingers without cooking ](<https://www.amourdecuisine.fr/article-gateau-sans-cuisson-doigts-au-chocolat.html> "cake without cooking, chocolate fingers") , at Bahiya 

[ ![Bavarian](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/bavarois.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/bavarois.jpg>)

[ ![tomato sauce](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sauce-tomate-300x225.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/sauce-tomate.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
