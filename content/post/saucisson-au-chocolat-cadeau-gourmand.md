---
title: 'chocolate sausage: gourmet gift'
date: '2017-12-18'
categories:
- dessert, crumbles and bars
- Algerian cakes without cooking
- sweet recipes
tags:
- Cakes
- Holiday Recipe
- Algerian cakes
- Delicacies
- Hazelnut
- To taste
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/saucisse-mendiant-chocolat-cadeau-gourmand-3.jpg
---
![chocolate beggar sausage, gourmet gift 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/saucisse-mendiant-chocolat-cadeau-gourmand-3.jpg)

##  chocolate sausage: gourmet gift 

Hello everybody, 

chocolate sausage: gourmet gift: If you say at home **Chocolate sausage** my children will respond, "I want it, I want it", they like it a lot, and it's always present at home, and I never do the same recipe twice. I always use what I have at home, sometimes it is stuffed with raisins of all colors, sometimes it is stuffed with marshmallows, sometimes with walnuts, sometimes with biscuits, sometimes the mixture Of all, sometimes it is filled with dates, so to know what I have on hand. And the kids always tell me, so what's the surprise today ??? . 

This time, my **chocolate sausages** were **gourmet gifts** for the school party, there was a charity sale "children as needed", and I made almost 15 varieties of chocolate sausages, and that's sold as "peanuts" lol ... 

I made it for all tastes, there was even "girly" style with white chocolate colored pink, it was the first time that I made white chocolate, I was a little afraid that it does not not everyone, and in the end everyone wanted these two unique pink chocolate sausages. I was very happy for this sale for charity, because my little table was well overpopulated, I even beat the two girls next door, yet one had pretty popcakes and the other "apple of love" or apples coated with caramel ". 

But apparently, chocolate is the favorite dessert, and a nice gourmet gift, and my chocolate sausages, and my 5 logs disappeared very quickly ... 

**chocolate sausage: gourmet gift**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/saucisse-mendiant-chocolat-cadeau-gourmand-1.jpg)

portions:  12  Prep time:  10 mins  cooking:  3 mins  total:  13 mins 

**Ingredients**

  * 25 g whole roasted hazelnuts 
  * 25 g roasted unsalted pistachios 
  * 25 gr roasted cashew nuts 
  * 100 g of biscuits type "digestive" 
  * 120 g of butter 
  * 200 g of dark chocolate (60% here) 



**Realization steps**

  1. place the dried fruits (the beggars) in a bowl or a salad bowl. 
  2. break over the biscuits into small pieces. 
  3. put the butter and the chocolate in a small bowl and place in the microwave, mix each passage of 15 seconds in the microwave, until all the pieces of chocolate are melted and it makes a nice smooth cream. 
  4. Pour this beautiful sauce over the nuts and biscuits. 
  5. mix everything together and form a pudding on a sheet of aluminum foil. 
  6. Close the roll tightly and refrigerate for at least 4 hours. 
  7. then open the aluminum foil and sprinkle the sausage with icing sugar to cover it well 
  8. Then rub with your hands to give the appearance of the traditional sausage skin. 
  9. If you are going to present as a gourmet gift wrapped chocolate sausage in food film. 



![chocolate beggar sausage, gourmet gift 3](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/saucisse-mendiant-chocolat-cadeasu-gourmand-2.jpg)
