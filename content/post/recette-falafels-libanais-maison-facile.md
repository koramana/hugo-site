---
title: Lebanese falafels recipe easy home
date: '2018-04-14'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- salty recipes
tags:
- East
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/03/falafels-libanais-fait-maison-facile-2.jpg
---
![Lebanese falafels recipe easy home](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/falafels-libanais-fait-maison-facile-2.jpg)

##  Lebanese falafels recipe easy home 

Hello everybody, 

Well at home we love the Lebanese falafel homemade recipe easy that I recommend. My children are very fond of it, because with our neighbors of the Middle East (Sudan, Lebanon and Egypt) my children are always enjoying the falafel during our picnic outings together. 

So, it makes sense that they often ask me to make homemade Lebanese falafels very simple and easy, luckily for me. The most important in all history and remember to soak the chickpeas in the water the day before. 

Yes, because to succeed falafels, do not think "smart" and say no serious I'm going to make falafel with chickpeas in box, message of a person who has already done this stupidity, me! when frying a beautiful patty of falafel became the end of the seeds of an olive, I even think that I had the chance to see dumplings disappear completely in the cooking oil ... What a miracle! hihihih 

So do not do this stupidity, if you have the urge to make falafels, think to soak chickpeas .. Ok! 

You can see the version of [ falafel chickpeas and beans ](<https://www.amourdecuisine.fr/article-falafels-faits-maison-ultra-facile.html>) that you can taste with good [ Lebanese pita bread ](<https://www.amourdecuisine.fr/article-pain-pita-de-cuisine-libanaise-a-poele.html>) and still accompany a good [ chorba ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) , especially during [ Ramadan ](<https://www.amourdecuisine.fr/article-recettes-ramadan-2017-plats-pour-ramadan-2017.html>)

**Lebanese falafels made easy homemade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/falafels-libanais-fait-maison-facile-1.jpg)

**Ingredients**

  * 400 gr dry chickpeas 
  * ½ bunch of fresh parsley 
  * ½ bunch of fresh coraindre 
  * ½ onion 
  * 5 cloves of garlic 
  * 2 tbsp. powdered cumin 
  * salt pepper 
  * 1 C. coffee baking soda 
  * lemon juice 
  * 1 C. coffee lemon juice 



**Realization steps**

  1. soak the chickpeas the day before in cold water. 
  2. After overnight in the water, drain the chickpeas, and wipe them completely in a clean cloth or paper towel. 
  3. place the chickpeas in a chopper 
  4. Mix well by little strokes. 
  5. leave the chopper on and introduce all the ingredients, one by one: parsley, coriander, onion, garlic, cumin, black pepper, salt, lemon juice and baking soda. 
  6. If your falafels appear a little liquid, add a little flour, sometimes, I pass a dumpling to the frying, if it is well, so no need to add the flour 
  7. form slices of falafel almost 3 cm in diameter. 
  8. place them as you go on paper towels 
  9. heat the oil well, lower the heat and fry the falafel on each side until you have a beautiful golden color. 
  10. enjoy a sandwich with a nice salad, and a good [ homemade Lebanese pita bread ](<https://www.amourdecuisine.fr/article-pain-pita-de-cuisine-libanaise-a-poele.html>)



![Lebanese falafels made easy homemade](https://www.amourdecuisine.fr/wp-content/uploads/2017/03/falafels-libanais-fait-maison-facile.jpg)
