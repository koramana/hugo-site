---
title: apricot brownie
date: '2017-07-15'
categories:
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Algerian cakes
- Pastry
- To taste
- desserts
- Cakes
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/07/brownie-aux-abricots-1-683x1024.jpg
---
![brownie with apricots 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/07/brownie-aux-abricots-1-683x1024.jpg)

##  apricot brownie 

Hello everybody, 

This apricot brownie was braying a delight, I hardly made some pictures, and went to see the pictures taken on the computer, when I came back to do some more photos because I was not too satisfied of the result, only a few pieces remained. 

The children liked it so much that they devoured almost everything, especially since the apricots were too good. To be honest, I was not sure to find good apricots in England, I was even preparing to make the recipe with boxed apricots, but as luck would have it, I came across apricots so good, that could we eat nature, a nice little tray in one go. 

My surprise is of course very large, because despite the holidays we had a good number of participants, it made me happy, besides you can see the list at the bottom of this article. You can also visit my blogger friends for recipe ideas to fully enjoy the apricot season. 

**apricot brownie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/07/brownie-aux-abricots.jpg)

**Ingredients**

  * 200 gr of dark chocolate 
  * 180 gr of butter 
  * 4 eggs 
  * 180 gr of sugar 
  * 140 gr of flour 
  * 1 C. tablespoon natural yoghurt 
  * ½ c. coffee baking powder 
  * crushed pistachios (optional) 
  * fresh apricots or in box 
  * honey, apricot jam or apple jelly. 



**Realization steps**

  1. melt the chocolate with the butter in the microwave or bain-marie. 
  2. let cool and add sugar and eggs one by one while whisking. 
  3. then add the natural yoghurt 
  4. then introduce the sifted flour and baking powder, introduce well without mixing too much. 
  5. pour the dough into a lined pan of about 20 cm by 20 cm, for a high cake, otherwise in a larger pan for a regular cake. 
  6. decorate the top with half apricots and a few crushed pistachios. 
  7. cook in a preheated oven at 180 ° C for 25 to 30 minutes. 
  8. at the end of the oven brush the top with a little honey, apricot jam or jelly. 



![apricot brownie 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/07/brownie-aux-abricots-2.jpg)

et voila la liste des participantes et leurs recettes: 
