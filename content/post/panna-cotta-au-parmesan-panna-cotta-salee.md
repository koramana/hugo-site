---
title: Panna cotta with parmesan cheese / panna cotta salee
date: '2015-02-14'
categories:
- panna cotta, flan, and yoghurt

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/Panna-cotta-au-parmesan1.jpg
---
![Panna cotta au parmesan.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/Panna-cotta-au-parmesan1.jpg)

##  Panna cotta with parmesan cheese / panna cotta salee 

Hello everybody, 

The weather is nice today, and this panna cotta parmesan will be a nice entry to present, during a meal in the sun .... or just in a buffet. 

It's a recipe of our dear Lunetoiles, and to the people who do not stop to ask: it's who Lunetoiles, I tell you, it's a faithful reader, and a very good friend, who likes to make beautiful recipes and share them on my blog, it's his choice to share on my blog, despite that I have repeatedly urge to make a blog ... but she is happy to publish these delights at home, and me ' I am very happy with this sharing. and if you like to see all these recipes, here is the index of [ Lunetoiles recipes ](<https://www.amourdecuisine.fr/article-recettes-de-lunetoiles-sur-le-blog-pas-le-blog-de-lunetoiles-115306695.html>) .... 

![panna cotta-salted - in-parmesan.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/panna-cotta-salee-au-parmesan2.jpg)

**Panna cotta with parmesan cheese / panna cotta salee**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/panna-cotta-au-parmesan-11.jpg)

portions:  6  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** for 6 glasses of 20 cl 

  * 100 g salted crackers (or sesame breadsticks) 
  * 25 g of parmesan cheese 
  * 25 g of butter 

Panna cotta : 
  * 25 cl of milk 
  * 25 cl of liquid cream 
  * 60 g of Parmesan cheese 
  * 2 gelatin sheets 
  * pepper 

Decoration: 
  * fresh herbs, pine nuts or toasted flaked almonds 



**Realization steps** Salt crackers base: 

  1. Melt butter over low heat. 
  2. Reduce the crackers (or breadsticks) in powder, with the Parmesan cheese and add the melted and warm butter. 
  3. Spread in the bottom of the verrines and tamp well. 
  4. Put in the fridge while waiting to prepare the panna cotta. 

Panna cotta : 
  1. Soak the gelatin in a bowl of cold water. 
  2. Bring milk, cream and parmesan to a boil. 
  3. Boil 2-3 minutes, then off the heat add the drained gelatin. 
  4. Mix well, pepper, and cool. 
  5. Pour in the verrines. 
  6. Book in the fridge at least 2 hours. 
  7. Garnish with fresh herbs and golden pine nuts or golden chopped almonds. 



![panna cotta-salty au parmesan.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/panna-cotta-salee-au-parmesan11.jpg)

PS: 

If you have a facebook page, and you like my recipes, do not make a full copy of my recipe on your pages, it is an infringement of the royalties .... you like my recipe, share the recipe link ... .. do not enrich your pages with the work of others .... 

# 
