---
title: Yum o fruit breakfast slimming
date: '2016-06-15'
categories:
- Cuisine détox
- Healthy cuisine
- recipes at least 200 Kcal
tags:
- Easy cooking
- To taste
- Summer kitchen
- bananas
- Fruit salad
- salads
- Breakfast

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Miam-%C3%B4-fruits-2.jpg
---
![Yum-ô-fruit 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Miam-%C3%B4-fruits-2.jpg)

##  Yum o fruit breakfast slimming 

Hello everybody, 

Have you heard of Yum-O-Fruits? These breakfast bowls are rich in fruit, a way to start your day on the right foot! 

Personally, I had seen pictures here and there on American blogs, but without ever lingering or taking the time to read the recipe, thinking that was just a fruit salad (nice mistake, hihihihi). Thanks to Lunetoiles who shared this recipe with me, which taught me a lot about yum 0 fruits, and here is what Lunetoiles tells us: 

I often eat a bowl yum 0 fruit at breakfast, I thought it would be good to share it because it's healthy and it's very good and more. This is the trend of **healthy and raw** in addition, and the followers of the gluten-free will be able also to appreciate this recipe I think. 

We must choose between 3 or 4 fruits to choose from, we put what we want, depending on the season, according to his desires ... ect. 

![ingredient yum o fruit](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ingredient-miam-o-fruit.jpg)

**Yum o fruit breakfast slimming**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Miam-%C3%B4-fruits-1.jpg)

portions:  1  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * ½ banana crushed with a fork until smooth   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Downloads6.jpg)
  * 2 tablespoons of organic rapeseed oil or a teaspoon of linseed oil + 1 tablespoon unroasted sesame oil. We emulsify the oil in the banana, we should not see the oil. (I did not put the oil) 
  * 1 level spoon of finely ground flaxseed 
  * 1 level spoon of finely crushed sesame seeds 
  * 1 tablespoon of a mixture of 3 roughly coarsely crushed nuts (cashew, peanut, walnut, hazelnut, almond, pecan, sunflower, pumpkin seed, etc.) 
  * 2 teaspoons fresh lemon juice   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Downloads7.jpg)
  * Several fruits or pieces of different fruits: apple, pear, kiwi, papaya, strawberries, pomegranate, mango, blueberry, lychee, longan, rambutan, pineapple, strawberry, raspberry, cherries, passion fruit, dragon fruit, currant, plum , mirabelle, peach, apricot, blackberry, cranberry, soursop, cinnamon apple, etc .... depending on the season and the place.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Downloads8.jpg)



**Realization steps**

  1. All that is used must be organic or, for some fruits, as natural as possible. 
  2. We do not put dried fruits, they are cooked in the sun. 
  3. Nor do you add citrus, orange, tangerine, grapefruit, clementine, melon or watermelon. These fruits are digestion too fast, they must be consumed separately. 

When and how to consume it? 
  1. We make a beautiful plate that can be consumed at choice in the morning, noon or evening. 
  2. We add nothing, we take nothing away. 
  3. This meal is chewed, each bite must absolutely be liquefied. 



![Yum-O-fruit](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/Miam-%C3%B4-fruits.jpg)
