---
title: my shrimp rice
date: '2011-02-02'
categories:
- sweet recipes
- tartes et tartelettes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/02/riz-au-crevettes1.jpg
---
![rice au crevettes.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/riz-au-crevettes1.jpg)

![rice-shrimp-to-1.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x415/2/42/48/75/mhalbi/)

Hello everybody, 

I do not know if it's only me, but I'm just running, the flu does not leave me, besides, at Rayan's school, they made clubs just after the quantine, to teach the children of little things, like handicrafts, and I participate in the club "hooks and needles" yes, not a kitchen club, it's under study, hihihihi, so suddenly, that I put Ines in kindergarten, I run quickly to the school of rayan, to be on time, anyway, one thing is sure, what makes me happy to see these girls who smile when they make a stitch in the hook, in any case One of the girls did a 30 years old and she was very happy. 

in any case, I do not know why I'm telling you this, but because I'm very overflowing, I put you an old recipe, until I organize myself a little to find the time to write my news recipes. 

it's still my same rice recipe, but this time it has shrimp (always frozen) 

my ingredients: 

  * a nice onion 
  * 1 clove of garlic 
  * 2 bay leaves 
  * of rice beans 
  * 500 gr of shrimp 
  * 400 to 500 grs of beautiful red tomatoes (I use a canned tomato) 
  * 1 case of canned tomato 
  * oil (I cook with olive oil) 
  * salt, black pepper, paprika, tandori massala (it is a mixture of Indian condiments red and full of beautiful smells) 



first of all I start by frying shrimps in oil, then cook with this oil: 

![S7301941](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/209822591.jpg)

thaw shrimp in a pan, and fry a little. 

put the onion, and the garlic in the blender, and pour it in a pan with the olive oil, to make return the mixture a little until it has a beautiful color, add after the bay leaves, paprika, salt, black pepper, and condiments you like. 

let it simmer a little, and add the canned tomatoes. 

after, add the tablespoon of preserved tomato, because the rice likes the tomato, and let cook a little. 

![S7301943](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/209823271.jpg)

after, add the rice (so the measure of rice is according to people at the table, if you are 2 do not cook 1 kilos of rice .......... !!!!) 

![S7301945](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/209824051.jpg)

cook on low heat, and before the rice takes all the water, that is to say before the end of the cooking, we put the shrimps in the rice. So I use this method because the frozen shrimp becomes meniscule after cooking, and I prefer his taste in rice, and not in topping. 

![S7301946](https://www.amourdecuisine.fr/wp-content/uploads/2011/02/209824361.jpg)

serve hot 

![Rice shrimp au-1.jpg](https://www.amourdecuisine.fr//import/http://img.over-blog.com/555x416/2/42/48/75/mhalbi/)

and enjoy your meal 
