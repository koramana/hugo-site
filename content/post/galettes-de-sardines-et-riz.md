---
title: Sardines and rice cakes
date: '2015-08-07'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- rice
tags:
- Healthy cuisine
- Buffet
- Algeria
- Easy cooking
- inputs
- accompaniment
- Amuse bouche

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/08/galettes-de-sardines-et-riz.jpg
---
[ ![cakes of sardines and rice](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/galettes-de-sardines-et-riz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/galettes-de-sardines-et-riz.jpg>)

##  Sardines and rice cakes 

Hello everybody, 

It is the season of the delicious sardine of the Mediterranean, although it is still very expensive, but we try to get it sometimes, to make a good recipe based on sardines. 

It must be said that with my husband, if you buy the sardine, it must be fried, do not try to do anything different. Last time my friend **Samia Z** had told me about these sardines and rice cakes, I really wanted to try it, but as I told you, no luck with my husband, and here **Samia Z** specially made to share with me, and with you through my blog, thank you very much my dear for this beautiful achievement. 

**Sardines and rice cakes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/galettes-de-sardines-au-riz.jpg)

portions:  6  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 700g of small sardines clean 
  * \+ or - 350g rice cooked in salted water for 10 to 12 min (not overcooked) 
  * garlic will 
  * coriander, garlic and dried parsley 
  * salt, cumin, black pepper, ras el hanout, ginger 

for breading: 
  * 2 to 3 eggs 
  * breadcrumbs (homemade) 



**Realization steps**

  1. mix the cleaned sardines and sana the ridge with rice drained and cooled 
  2. add the spices, chopped coriander and chopped garlic and mix well while squeezing the sardines with your hand 
  3. prepare in two plates the eggs a little beaten with a fork adding salt and in the other plate breadcrumbs   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/galettes-de-sardines-et-riz-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/galettes-de-sardines-et-riz-3.jpg>)
  4. take a little of the sardine mixture, form beautiful patties neither thick nor thin 
  5. dip each pancake on both sides in the eggs, then in the bread crumbs, keeping its shape, 
  6. place in a tray as and until the stuffing 
  7. heat an oil bath and cook the patties over medium heat on both sides until golden brown 
  8. you can prepare them in advance and reserve them in the refrigerator the moment to make them leather you iron them in breadcrumbs 



[ ![rice sardine cakes](https://www.amourdecuisine.fr/wp-content/uploads/2015/08/galettes-de-sardines-au-riz.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/08/galettes-de-sardines-au-riz.jpg>)
