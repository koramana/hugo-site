---
title: marble cake
date: '2018-04-04'
categories:
- houriyat el matbakh- fatafeat tv

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-marbre-1_thumb2.jpg
---
##  marble cake 

Hello everybody, 

Here is a delicious marbled cake that I saw on the channel fatafeat TV, on the show Houriat el matbakh, I never tired of realizing it, because it is so soft and melting, a wonderful recipe that I will share with great pleasure with you. 

**marble cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/cake-marbre-1_thumb2.jpg)

Recipe type:  cake dessert  portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 2 glass of flour 
  * 1 glass of sugar 
  * 1 glass of melted butter, or oil, or half the butter / oil mixture (for me it was oil) 
  * ¾ glass of milk 
  * 1 pinch of salt 
  * 2 cac vanilla 
  * 5 large eggs 
  * 2 cases of cornstarch 
  * 3 cac of baking powder 
  * 3 cases of cocoa 
  * 2 cases of apricot jam 



**Realization steps**

  1. separate, the egg yolks from the whites, beat the egg whites well with a pinch of salt, and a little sugar, then set aside 
  2. mix the egg yolks with the remaining sugar, and vanilla, add the melted butter, or oil gently while whisking. then add the jam, and finally add the milk. 
  3. Now add the flour + baking powder mixture and mix gently. 
  4. and finally add the egg bench gently with a wooden spoon, without breaking it, try to have a well-aired mixture. 
  5. separate, this mixture has two equal parts, in the first part add the cocoa, and in the other part add the cornstarch. 
  6. now, to have the marbling, pour into a cake mold butter and flour, alternately a spoon of the cocoa mixture and then a spoon of the mix of cornstarch, you can use a large tablespoon (for each mixture) or see even a small fishy, ​​to have a well-defined marbling.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/327952073.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/327952073.jpg>)
  7. cook in a preheated oven at 200 degrees C, then after 5 min reduce the temperature to 180 degrees C, bake for 35 minutes. or on your oven.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/327952123.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/327952123.jpg>)



and you will be astonished at the taste, and the result. 

a real delight, to do and redo, I invite you .... 

bon appétit. 
