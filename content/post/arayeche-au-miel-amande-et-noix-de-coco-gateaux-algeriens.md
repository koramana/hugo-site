---
title: arayeche with honey almond and coconut cakes Algerian
date: '2011-10-31'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-miel3_thumb1.jpg
---
**arayeche with honey almond and coconut cakes Algerian**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/10/arayeche-miel3_thumb1.jpg)

**Ingredients** ingredients of the pasta 

  * 4 measures of flour (measure is any utensil you can use so that it becomes the measuring tool in all the cake) 
  * ½ measure of melted margarine 
  * Orange Blossom 
  * white and orange dye. 

prank call: 
  * almond and ground coconut 
  * sugar 
  * water + a little orange flower 



**Realization steps** the method of shaping: 

  1. Pour the flour into a terrine, add the margarine, mix well with the fingertips 
  2. divide the dough into two parts 
  3. for the one you are going to color in white add the dye, then pick up the dough with flower water, add a little water to pick up, continue to knead a few seconds. 
  4. for the other color, take the other half of the shortbread, add the dyes to the orange water until the desired color is obtained, and start to collect the dough with this water, add the water if you need, and knead a few seconds. 
  5. cover your dough and let it sit on the side. 
  6. for stuffing mix almonds, coconut pick up with water and orange blossom. 
  7. lower the dough and pass it to the machine from number 3 to number 7, cut two circles of dough, the first of the dark color with a cookie cutter 8 cm in diameter. 
  8. the second of the white color with a 9 cm diameter cookie cutter. 
  9. then superimpose them, the big one on the small 
  10. take a ball of 25 grams of the almond stuffing, form a star, place it on the circle of the dough, and pick up the sides of the dough to coat the almond stuffing, without covering it completely, press well so that the dough is well stick to the stuffing, and form well the ends of the star. 
  11. decorate with small flowers 
  12. and cook for 10 minutes in a preheated oven at 170 degrees C. 
  13. take it out of the oven, let cool a little, and emerge your cakes in a little hot honey. 
  14. now decorate the small flowers with pearls. 


