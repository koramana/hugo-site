---
title: leg of roast in the express pressure cooker
date: '2016-10-05'
categories:
- recette a la viande rouge ( halal)
tags:
- Algeria
- dishes
- Ramadan
- Ramadan 2016
- Meat
- accompany
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gigot-dagneau-roti-express_.jpg
---
![roast-lamb-roast-Express_](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gigot-dagneau-roti-express_.jpg)

##  leg of roast in the express pressure cooker 

Hello everybody, 

Here is a delicious roast leg roast in the express slow cooker that I realize when I want to enjoy with family a good leg of well cooked, very tender and I did not put in advance to put it in the oven rather! 

This recipe of leg of lamb roasted in the pressure cooker was the recipe that always saved me when I did not have an oven, and I had to present a good leg of my guests. The meat is very tender, juicy and super fragrant and the cooking is super fast, 1 leg of almost 1k and a half can cook in maximum 2 hours! The result is bluffing, even my youngest who does not like to eat meat (because it sticks to the teeth) well I was surprised he had nothing left in his dish! 

If you like anything that is roasted you can see the [ leg of lamb roasted in the oven ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-au-four-aid-el-adha-2012-111759550.html>) the [ tandoori chicken ](<https://www.amourdecuisine.fr/article-poulet-tandoori.html>) , the [ kfc chicken ](<https://www.amourdecuisine.fr/article-poulet-kfc-maison.html>) , the [ chicken with mustard ](<https://www.amourdecuisine.fr/article-cuisses-de-poulet-a-la-moutarde-au-four-111087660.html>) , the [ chicken with hot sauce ](<https://www.amourdecuisine.fr/article-ailes-de-poulet-piquantes-au-four.html>) , and even more…. 

![roast-lamb-roast-a-la-cooker-express-1](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gigot-dagneau-roti-a-la-cocotte-minute-express-1.jpg)

**leg of roast in the pressure cooker**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gigot-dagneau-roti-a-la-cocotte-minute-express_.jpg)

portions:  4  Prep time:  15 mins  cooking:  180 mins  total:  3 hours 15 mins 

**Ingredients**

  * 1 leg of lamb 
  * a head of garlic 
  * some stems of rosemary 
  * salt 
  * black pepper 
  * ½ c. paprika 
  * ½ c. coffee turmeric 
  * ½ c. coffee of salmon (since I bought this spice, we become fanatic) 
  * ½ teaspoon of cumin 
  * olive oil 



**Realization steps**

  1. take the leg of lamb, cut a little deep in it. 
  2. prick the garlic pieces in it. 
  3. in a plate, mix the spices and the salt. 
  4. and sprinkle on the leg, placing a little in the cuts. 
  5. add the rosemary and gently wipe the leg of oil   
at this stage, you can leave the piece of leg of lamb sitting and marinated, if not directly in the well-closed pressure cooker   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/gigot-roti-express.jpg)
  6. add a little oil to the bottom of the casserole, place the leg and close the casserole. 
  7. after 15 minutes open the casserole and turn the leg, you will see that it has taken a nice color and the meat has rejected its water 
  8. let it cook for 15 minutes and open the casserole to turn the meat, if there is no more water in the casserole add half a glass of water. 
  9. Continue to monitor and turn the meat and if it lacks water, add in small amounts. 
  10. at the end of cooking remove the leg. You can cook some potatoes in salt water, and sauté it in a little of the meat cooking juices, to present the leg of lamb with. 


