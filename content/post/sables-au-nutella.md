---
title: Nutella shortbread
date: '2015-08-25'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
tags:
- Algerian cakes
- Algeria
- Cakes
- Cookies
- Aid
- biscuits
- Little butter

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sables-au-nutella_thumb1.jpg
---
##  Nutella shortbread 

Hello everybody, 

here is delicious and fondant [ shortbread ](<https://www.amourdecuisine.fr/categorie-12344749.html>) , which I realized at the past aid party, but I have never posted: [ Algerian cakes 2011. ](<https://www.amourdecuisine.fr/article-aid-el-fitr-2011-les-gateaux-algeriens-82873081.html>)

and as, all the detailed photos remained on my pc in Algeria, I redid the recipe at the request of my husband, who likes the [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) , and the [ shortbread ](<https://www.amourdecuisine.fr/categorie-12344749.html>) . 

**Nutella shortbread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/sables-au-nutella_thumb1.jpg)

**Ingredients**

  * 220 g of butter, at room temperature 
  * 100 g icing sugar 
  * 2 egg yolks 
  * 300 g of flour 
  * 50 g cornflour 
  * 1 vanilla extract 
  * pinch of salt 
  * Nutella 
  * coconut 



**Realization steps**

  1. method of preparation: 
  2. Place the butter in a bowl and whip it to make it soft. 
  3. Add the sugar, at once and whip again to have a nice sweet cream. 
  4. Add egg yolks, vanilla extract, and salt, whip again. 
  5. incorporate the cornflour delicately 
  6. then pick up the dough with the flour, to have a nice smooth dough. 
  7. shape your cupcakes, according to the cookie cutter of your choice, I used a cookie cutter with a rosette stamp. 
  8. cook in preheated oven at 170 degrees for 15 to 20 minutes. 
  9. When the biscuits cool, gather each two pieces with Nutella. 
  10. also decorate the sides of the cake with nutella, 
  11. then cover with coconut. 


