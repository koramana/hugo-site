---
title: fig and almond tart
date: '2016-09-17'
categories:
- recettes sucrees
- tartes et tartelettes
tags:
- Sweet pie
- Pastry
- Custard

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/TARTES-AUX-FIGUES-ET-AMANDES.jpg
---
[ ![fig and almond tart](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/TARTES-AUX-FIGUES-ET-AMANDES.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tarte-dautomne-aux-figues-et-amandes.jpg>)

##  fig and almond tart 

Hello everybody, 

with this tart with figs and almonds we take full advantage of the season of figs that will soon leave us, it already saddens me, because the figs, I love madly, and even if I eat all season , and all the sauces, this fruit will always be missing ... 

Today, it is Lunetoiles who will share with us, one of these recipes with figs, **a fig and almond pie** . There is already on the blog the recipe of the [ fig tart with frangipane ](<https://www.amourdecuisine.fr/article-flan-aux-figues.html> "Flan with figs") . 

But the recipe this time, does not contain only cream of almonds, there is also cream patissiere ... suddenly this pie can only be an interminably fondling delight, a taste to fall. 

[ ![fig and almond pie](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tarte-dautomne-aux-figues-et-amandes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tarte-dautomne-aux-figues-et-amandes.jpg>)

If like me and like Lunetoiles, you like figs, I give you a nice selection of [ fig recipes ](<https://www.amourdecuisine.fr/recherche-google-sur-amour-de-cuisine?cx=partner-pub-8508115066064537%3A6618639480&cof=FORID%3A10&ie=UTF-8&q=figue&sa=Rechercher>) , so enjoy, as long as he still figs on the stalls. 

**fig and almond tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tarte-figues-amandes-1.jpg)

portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** Shortbread : 

  * 150 g flour 
  * 1 pinch of baking powder 
  * 50 g of sugar 
  * 75 g of soft butter 
  * 1 egg yolk 
  * 1 C. tablespoons water 

Custard: 
  * 250 g of milk 
  * ½ vanilla pod 
  * 50 g of sugar 
  * 3 egg yolks 
  * 20 g of flan powder (or Maizena) 

Almond cream: 
  * 100 g of butter 
  * 75 g icing sugar 
  * 75 g of almond powder 
  * 1 cc domed of Maïzena 
  * 30 g of egg 
  * 50g of amaretti (or other macaroons) (I did not put any) 
  * Dozens of ripe fresh figs 



**Realization steps** Prepare the dough: 

  1. put the flour, yeast and sugar in a salad bowl. 
  2. Add the soft butter in the center, and rub between the hands until you have a consistency of sand. 
  3. Then add the egg without much work. 
  4. Put the dough in a cool food film for a few hours so that it works easier. 

prepare the pastry cream: 
  1. Bring milk and vanilla to a boil. 
  2. Let infuse a few minutes. 
  3. In a bowl, whip the yolks and sugar until you have a frothy mixture. 
  4. Add the flan powder and mix. 
  5. Pour the boiling milk over the yolks, gradually and whisking. 
  6. Pour everything into the pan and let thicken over low heat while stirring briskly. 
  7. Put the cream in a bowl and film on contact. 
  8. Let cool. 

Prepare the almond cream: 
  1. Whip the butter until it is soft. 
  2. Add the icing sugar, almond powder and maizena. 
  3. Mix. 
  4. Beat 1 egg and add 30 g. 
  5. Whisk for 3 minutes. 
  6. Add the crumbled amaretti (or any other macaroons with almonds). 
  7. Mix. 

Montage: 
  1. Preheat the oven to 170 ° C. 
  2. Mix the almond cream with 175 g pastry cream. 
  3. Spread the dough on a floured work surface and garnish a pie circle or a mold about 24 cm. 
  4. Spread the cream on the dough. 
  5. Arrange figs cut in half, curved sides down. 
  6. Cook for about 40 minutes. 
  7. Let cool the pie before removing it. 
  8. Book at room temperature. 



Note If you do not have a pie circle, you can always use a pie plate. The demolding will be a little more difficult. 

[ ![fig and almond pie](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tarte-figues-amandes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/tarte-figues-amandes.jpg>)
