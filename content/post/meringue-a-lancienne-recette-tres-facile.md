---
title: Meringue with old, very easy recipe
date: '2014-08-11'
categories:
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-a-l-ancienne-013.CR2_thumb.jpg
---
[ ![Meringue with old, very easy recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-a-l-ancienne-013.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-a-l-ancienne-013.CR2_2.jpg>)

##  Meringue with old, very easy recipe 

Hello everybody, 

Old-fashioned Meringue, a very easy recipe: When I was little, I remembered those crispy and fragile meringues on the outside, which became elastic like chewing gum in the mouth. We ate it without moderation ... 

Oh the beautiful time, where we had no problem eating sweet things, under penalty of being fat ??? My turn to make my children taste this little delight these old-fashioned meringue, very easy recipe! 

As a result, the kids did not leave anything, they find that these old-fashioned meringue are just falling, they did not leave a crumb. 

[ ![Meringue with old, very easy recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-005.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-005.CR2_2.jpg>) [ ![Meringue with old, very easy recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-008.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-008.CR2_2.jpg>)

**Meringue with old, very easy recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-008.CR2_thumb.jpg)

portions:  30  Prep time:  20 mins  cooking:  90 mins  total:  1 hour 50 mins 

**Ingredients**

  * 4 egg whites 
  * 250 gr of caster sugar 
  * 1 pinch of salt 
  * dye if you want colorful meringues. 



**Realization steps**

  1. whip the whites into firm snow with the pinch of salt. 
  2. Preheat the oven to 100 ° C. 
  3. Add the sugar in small amounts while continuing to beat. and until the egg whites become shiny. 
  4. form heaps, or shape flowers in the piping bag, which you place directly on a baking sheet, covered with baking paper. 
  5. Cook at 100 ° C between 1h15 and 1h30 



[ ![Meringue with old, very easy recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-018.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-018.CR2_2.jpg>)

[ ![Meringue with old, very easy recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-025.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/meringue-025.CR2_2.jpg>)

and coming soon: the [ lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee-114713045.html> "lemon meringue pie")

[ ![lemon meringuee.CR2edited](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-au-citron-meringuee.CR2edited_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-au-citron-meringuee.CR2edited_2.jpg>)
