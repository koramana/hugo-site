---
title: Bavarian, the index
date: '2012-01-28'
categories:
- amuse bouche, tapas, mise en bouche
- cuisine diverse
- idea, party recipe, aperitif aperitif
- Mina el forn
- pain, pain traditionnel, galette
- pizzas / quiches / tartes salees et sandwichs
- Plats et recettes salees
- recettes de feculents

---
Hello everybody, 

to have easier access to my blog, I try to put each category of cakes index, with clickable photos, so if you like a recipe, click on the photo you like, and you will find yourself on the recipe you want. 

I will try to update each index as soon as possible. 

you like my blog, subscribe to my newsletter, to be aware of any new update. 

merci 
