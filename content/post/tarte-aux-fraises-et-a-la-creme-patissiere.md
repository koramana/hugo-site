---
title: Strawberry and custard pie
date: '2018-03-31'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/07/dsc042211.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/dsc042211.jpg)

##  Strawberry and custard pie 

Hello everyone, 

Do you want a delicious strawberry and custard pie? yes it's not the season, but still I can always find strawberries here even out of season. 

In any case this beautiful strawberry pie super well stocked is not mine, hihihihih .... It's my friend the pretty talented lunetoiles who realized it and she shares the recipe with you, with great pleasure, so your comments will please him, ok   


**Strawberry and custard pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/dsc0422211.jpg)

**Ingredients** The shortbread dough: 

  * 250 gr of flour 
  * 125 gr of butter 
  * 40 gr of sugar 
  * 1 sachet of vanilla sugar 
  * 50 ml of milk 
  * a pinch of salt 

Custard: 
  * 500 ml of milk 
  * 4 egg yolks 
  * 130 gr of sugar 
  * 50 gr of cornflour 
  * 1 vanilla pod 
  * Strawberries washed, hulled and cut 
  * Topping for pie 



**Realization steps** Prepare the dough: 

  1. Put 250 g flour with 125 g diced butter in a bowl, and sand the mixture between the hands. Add 40 g sugar, the vanilla sugar sachet and the pinch of salt, mix, then slowly pour the milk, kneading. Work the dough, roll it into a ball, and let it cool for 30 minutes. 
  2. Preheat the oven to 180 ° C, th.6. 
  3. Spread the dough, and garnish a pie pan. Stitch the bottom of the dough with a fork and bake to cook the dough. 
  4. When cooked, and a little brown, let cool. 

Prepare the pastry cream: 
  1. Split the vanilla pod in half, scrape the seeds and put all in the milk you will bring to the boil. 
  2. In a bowl, whisk the yolks and sugar until the mixture is white. Then add the cornflour. 
  3. Pour the boiling milk over the previous mixture, stir well. 
  4. Then put everything back in the pan. Make thicken, without mixing. 
  5. Place the shortcrust pastry on a serving dish and garnish with custard. 
  6. Let it rest cool, until the cream freezes and is cold. 
  7. Then place the strawberries on the pastry cream, squeezing them well. 
  8. Top with pie glaze. Decorate with the vanilla bean, used to make the pastry cream, having cleaned and dried it beforehand. 
  9. Place in the fridge.   
Enjoy very fresh! 


