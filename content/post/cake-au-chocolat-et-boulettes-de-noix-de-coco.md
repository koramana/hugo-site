---
title: chocolate cake and coconut dumplings
date: '2016-03-08'
categories:
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Easy cooking
- Algerian cakes
- To taste
- Cakes
- desserts
- Fast Food
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/cake-au-chocolat-et-boulettes-de-noix-de-coco.jpg
---
[ ![chocolate cake and coconut dumplings](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/cake-au-chocolat-et-boulettes-de-noix-de-coco.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/cake-au-chocolat-et-boulettes-de-noix-de-coco-1.jpg>)

##  chocolate cake and coconut dumplings 

Hello everybody, 

Its cake is super easy, 2 eggs and voila, an express recipe for you if you receive guests, take a look at it because it is different from what I am going to share with you. 

So I went to get my ingredients, I prepared the coconut dumplings, and came to realize the cake, I think I had more oil .... pffff It's always like that with me. 

I reread again and again the recipe of kiko, which mixed oil cocoa and sugar then took a portion of this sauce to water the cake with the output of the oven. So I was going to use melted butter instead of oil, the recipe was not going to work with me. So here is my cake version of this simple cake and I hope you will like this achievement. 

[ ![xVegan Nut Brownie Culinary Challenge # 14](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/xBrownie-aux-noix-vegan-D%C3%A9fi-Culinaire-14-300x241.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/xBrownie-aux-noix-vegan-D%C3%A9fi-Culinaire-14.jpg>)

**chocolate cake and coconut dumplings**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/cake-au-chocolat-et-boulettes-de-noix-de-coco-3.jpg)

portions:  12  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients** For the chocolate cake: 

  * 2 eggs 
  * A pinch of salt 
  * 1 glass of sugar 
  * 1 glass of melted butter 
  * 1 glass of milk 
  * 2 glasses of flour 
  * 3 c. cocoa 
  * 1 sachet of baking powder 
  * 1 sachet of vanilla sugar 
  * 1 C. cornflour 

For the coconut balls 
  * 150g thick cream 
  * 1 and a half glasses of grated coconut 
  * ½ glass of sugar 
  * 2 tablespoons flour 

decoration: 
  * chocolate sauce 



**Realization steps**

  1. prepare the coconut balls: 
  2. Mix all the ingredients together 
  3. Form balls the size of a walnut, and leave aside. 
  4. Prepare the cake: 
  5. whisk eggs, sugar, vanilla and salt. 
  6. Add warm melted butter and continue to whisk. 
  7. add the milk. 
  8. Mix the flour, yeast, cornflour and cocoa (pass them through the sieve so as not to have lumps of cocoa) 
  9. add this mixture gently to the liquid mixture to obtain a homogeneous paste. 
  10. Pour the dough into a large cake tin (35 cm by 12 cm).   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-noix-de-coco.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/boulettes-de-noix-de-coco.jpg>)
  11. poke the coconut dumplings in the dough (do not overdo it so that it does not sink completely to the bottom, I made the mistake of putting half of the dough place the dumplings, then put the rest of the dough, as you see the dumplings came down during cooking) You do not have to put all the balls. 
  12. cook in a preheated oven at 180 ° C for 40 to 45 minutes, or depending on the capacity of your oven. 
  13. place the remaining coconut pellets to cook for 10 to 12 minutes (they cook quickly watch!) 
  14. at the end of the oven, place the coconut dumplings on the cake and sprinkle with the chocolate sauce. 



[ ![chocolate cake and coconut dumplings 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/cake-au-chocolat-et-boulettes-de-noix-de-coco-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/cake-au-chocolat-et-boulettes-de-noix-de-coco-2.jpg>)
