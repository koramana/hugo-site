---
title: Basque cake with black cherry jam
date: '2015-01-17'
categories:
- cakes and cakes
- sweet recipes
tags:
- Galette
- Shortbread
- Algerian cakes
- Pastry
- desserts
- To taste
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/01/g%C3%A2teau-basque-a-la-confiture-de-cerises-noires.jpg
---
[ ![Basque cake with black cherry jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/g%C3%A2teau-basque-a-la-confiture-de-cerises-noires.jpg) ](<https://www.amourdecuisine.fr/article-gateau-basque-la-confiture-de-cerises-noires.html/sony-dsc-256>)

Hello everybody, 

A delight that comes from the cuisine of Lunetoiles, the Basque cake has black cherry jam from Labourd in the Basque country. This cake consists of a beautiful layer of shortbread coated with black cherry jam, which you can easily replace with cream pastry, the cake will be all the better. 

Lunetoiles tells us: 

I got 6 cupcakes 8 cm in diameter and 4 bigger ones of 11,5 cm. I preferred to make the cake as an individual part but the recipe can very well be made in a pie plate. 

[ ![Basque cake with black cherry jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/g%C3%A2teau-basque-a-la-confiture-de-cerises-noires-2.jpg) ](<https://www.amourdecuisine.fr/article-gateau-basque-la-confiture-de-cerises-noires.html/gateau-basque-a-la-confiture-de-cerises-noires-2>)   


**Basque cake with black cherry jam**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/g%C3%A2teau-basque.jpg)

**Ingredients**

  * 300 g of flour 
  * 200 g of sugar 
  * 20 g of almond powder 
  * 20 g icing sugar 
  * 1 egg 
  * 1 egg yolk 
  * 120 g of butter 
  * fine salt 
  * a packet of baking powder 
  * 2 drops of bitter almond extract 
  * 600 g of cherry jam 
  * 1 egg yolk for gilding 



**Realization steps**

  1. Blanch the sugar with the ointment butter, add the icing sugar and the almond powder, then the eggs and mix well. 
  2. Add the flour, yeast and bitter almond. 
  3. Mix without kneading, then put in a ball under food film and reserve in the refrigerator at least an hour. 
  4. Lower two-thirds of the dough and go for a high-buttered mold or small pie-like molds, allowing the dough to rise. 
  5. Garnish the bottom with cherry jam paste. 
  6. Lower the rest of the dough and cut a circle a little larger than the diameter of the mold and drop the circle on the jam, welding the edges. 
  7. Dune with egg yolk and draw streaks with a fork. 
  8. Bake at 150 ° C for about an hour until cake is golden and let cool. 



[ ![Basque cake with black cherry jam](https://www.amourdecuisine.fr/wp-content/uploads/2015/01/g%C3%A2teau-basque-a-la-confiture-de-cerises-noires-1-001.jpg) ](<https://www.amourdecuisine.fr/article-gateau-basque-la-confiture-de-cerises-noires.html/gateau-basque-a-la-confiture-de-cerises-noires-1-001>)
