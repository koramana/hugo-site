---
title: English-style fish soup
date: '2015-03-27'
categories:
- soupes et veloutes
tags:
- Soup
- Sea food
- Easy cooking
- Algeria
- Morocco
- Tunisia
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/soupe-de-poisson-nawel-1.jpg
---
[ ![fish soup](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/soupe-de-poisson-nawel-1.jpg) ](<https://www.amourdecuisine.fr/article-soupe-de-poisson-a-langlaise.html/soupe-de-poisson-nawel-1>)

##  English-style fish soup 

Hello everybody, 

A comforting and tasty soup rich in taste and well constituted. This English-style fish soup, known as chowder fish soup, is a smooth and fragrant soup. 

This recipe was prepared by the care of my friend Nawel Zellouf, and as soon as I saw it I went to do it, except that I had some ingredients that were missing, and in addition with frozen seafood. The result was just sublime, so I guess if I had all the ingredients, and fresh seafood, yum ... Make this soup and tell me your opinion ... I'm sure you will not regret it. 

**English-style fish soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/soupe-de-poisson-nawel.jpg)

portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 400 g of fish mixture (hake, monkfish, smoked salmon). 
  * 200 g fresh shelled shrimps. 
  * 200 g of mold cleaned of sand, boiled and peeled. 
  * an onion or leeks. 
  * 2 cloves garlic. 
  * 2 medium carrots cut in Brunoise. 
  * a branch of celery cut in Brunoise. 
  * 1 large potato cut into cubes. 
  * 60 g of butter. 
  * 60 g flour. 
  * 1 L of fish broth or 1 l of water + cube Knorr taste fish. 
  * a bunch of parsley. 
  * a bouquet garni: 1 bay leaf + 1 small stalk of celery + 1 stem of parsley + leeks. 
  * salt, black pepper, nutmeg. 
  * 100 ml of fresh cream liquid. 



**Realization steps**

  1. fry the onion in the butter and add the other vegetables and let it simmer. 
  2. add the flour and mix well. 
  3. add the fish broth, the bouquet garni and let it boil 
  4. after boiling, add the fish mixture (without shrimp and mussels) and cook for 30 minutes. 
  5. after cooking add shrimp and mussels and cream and cook for 7-10 minutes over medium heat. 
  6. at the end add chopped parsley and nutmeg, salt, pepper. 



[ ![fish soup 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/soupe-de-poisson-nawel.jpg) ](<https://www.amourdecuisine.fr/article-soupe-de-poisson-a-langlaise.html/soupe-de-poisson-nawel>)
