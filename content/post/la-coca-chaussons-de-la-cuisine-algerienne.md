---
title: 'coca: slippers of Algerian cuisine'
date: '2014-11-03'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- ramadan recipe
tags:
- Amuse bouche
- inputs
- Aperitif
- Easy cooking
- Algeria
- Tapas
- aperitif

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/coca-algerienne-001.jpg
---
[ ![coca slippers of Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/coca-algerienne-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/coca-algerienne-001.jpg>)

##  coca: slippers of Algerian cuisine 

Hello everybody, 

The Algerian coke, yum yum, these delicious slippers of Algerian cuisine that have accompanied our dishes and dishes, and that we always present at the reception of guests to accompany a good [ chorba Frik ](<https://www.amourdecuisine.fr/article-chorba-frik.html> "chorba Frik") . Or as for us at home, and with my mother, it was the summer entry number 1, when my mother did not know what to eat, it was directly two trays of coca, with a fresh salad next and you're done. 

I also remember, that even for our picnic outings, or beach, coca was always present and in large quantities, we ate without being tired, so it was delicious. 

![the coca](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/La-coca.jpg)

**coca: slippers of Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/la-coca-1.jpg)

portions:  6  Prep time:  30 mins  cooking:  25 mins  total:  55 mins 

**Ingredients** For the pasta: 

  * 500 g of flour 
  * 1 glass of water 
  * 1 glass of oil 
  * 1 pinch of salt 
  * an egg yolk 
  * an egg for gilding 

for the stuffing: 
  * 3 onions 
  * 3 fresh tomatoes 
  * 1 C. coffee of tomato paste (for me 1 tbsp) 
  * 1 spoon of harissa for those who like pepper (I did not put it) 
  * tuna (optional) 
  * black olives (optional) 
  * salt and black pepper 
  * olive oil 

For gilding: 
  * an egg 



**Realization steps** Prepare the dough: 

  1. put the flour in a bowl, make a fountain, add the egg yolk and the oil by sanding with the hands, add the salt and the water, until obtaining a paste. 
  2. Wrap with a cling film and let the dough rest in a cool place for 1 hour. 

Meanwhile prepare the stuffing: 
  1. remove peel from onions, rinse and cut into slices.   
_tips_ : So that it does not sting the eyes, put them directly in a bowl filled with fresh water. 
  2. Then peel the tomatoes, seed them and cut them into small pieces. 
  3. Put in a pan a little oil, put the onions, salt and pepper and cook slowly, then add the tomatoes and tomato concentrate, add if necessary a little water not too much! 
  4. Once cooked, place this stuffing in a salad bowl and allow to cool well. 

To form our cocas: 
  1. Divide the dough into several pieces. 
  2. Spread each piece of dough on a floured work surface, cut with a round cookie cutter of 12 cm diameter, and put in the center of each round stuffing and fold into slippers. 
  3. Weld each slipper with a fork, pressing on the edges 
  4. Place the slippers on the baking sheet lined with parchment paper. 
  5. Brush with an egg brush and bake in a preheated oven at 170 ° C for 20 to 25 minutes. 



Note _A little tip:_   
to soften the onions, put them in a dish going to the microwave and cook them for about 10 minutes so that they become more tender, because the onion in the pan tends to burn very quickly so be careful! 

[ ![coca, slippers with onion sauce](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/la-coca-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/la-coca-1.jpg>)
