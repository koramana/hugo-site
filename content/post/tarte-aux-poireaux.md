---
title: leek pie
date: '2013-08-23'
categories:
- Algerian cakes- oriental- modern- fete aid
- cakes and cakes
- Algerian pastries
tags:
- Dietetic cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-poireaux-2_thumb.jpg
---
[ ![leek pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-poireaux-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-poireaux-2_2.jpg>)

Hello everybody, 

a salty pie, what I like, I do not know about you, but for my part, I like this contrast in words, it makes me want to read: salty cake, salty muffins, salty cake ... I like it when these desserts all good, take the habit of a beautiful entrance. 

so when Lunetoiles gave me these mouth-watering pictures of her leek pie, I rushed to read the recipe, and as soon as I have all the ingredients under my hands, I will prepare with great pleasure this pie. 

**leek pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-poireaux-3_thumb.jpg)

Recipe type:  quiche  portions:  6  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 1 puff pastry 
  * 75 g of butter 
  * 500 gr of sliced ​​leeks 
  * 2 spoons. flour 
  * ½ liter of milk 
  * 1 spoon. thick cream 



**Realization steps**

  1. Unroll the dough, prick with a fork, turn it over and garnish with the pie pan. 
  2. Melt the butter in a pan, 
  3. fry the slices of leeks until they are melting. Add salt and pepper. 
  4. Add the flour out of the fire and the milk then the cream, 
  5. mix. Put back on the heat and make thicken. 
  6. Pour the mixture on the dough 
  7. Bake in a preheated oven at 200 ° C for 30 to 40 minutes. 



[ ![leek pie](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-poireaux_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-poireaux_2.jpg>)

[ ![leek pie 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-poireaux-1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-poireaux-1_2.jpg>)
