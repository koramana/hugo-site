---
title: Ice Cream with Chocolate and Mars
date: '2015-07-07'
categories:
- ice cream and sorbet
tags:
- Ramadan 2015
- ice cream
- Summer
- Easy recipe
- Fast Food
- Ramadan
- Ice Cream Maker

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/06/creme-glacee-chocolat_thumb1.jpg
---
##  Ice Cream with Chocolate and Mars 

Hello everybody, 

I'm sorry for my absence on my blog, on the blogs of my blogger friends, I am completely out of season, hihihihihi. Between Ramadan and my preparations for inchallah my departure in Algeria. So, I apologize to you, and I give you a recipe that my friend passed me **Ouahiba** , 4 years ago. 

A recipe that I'm sure comes at a good time, especially by this hot heat. This cream is delicious, and you can make several variations by changing the chocolate Mars, by any other sweets, or cakes. 

**Ice Cream with Chocolate and Mars**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/creme-glacee-chocolat_thumb1.jpg)

**Ingredients**

  * 4 egg yolks 
  * 150 gr of sugar 
  * 300 ml of whole milk (it gives a soft ice even when it comes out of the freezer) 
  * 200 ml of liquid cream 
  * Vanilla (in liquid pod or powder but not vanilla sugar) 
  * cocoa or melted chocolate 
  * one or two bars of chocolate Mars. 



**Realization steps** Prepare the custard: 

  1. mix egg yolks, sugar and vanilla to have a voluminous and white mixture 
  2. add on it hot boiled milk 
  3. put back on the fire without stirring until the cream laps the back of the spoon when you draw a line on the back of the spoon with the finger the line remains clean (usually when the milk has boiled it takes 3 minutes max to cook), 
  4. remove immediately from the heat and allow to cool. (if you have put the vanilla in pods it is necessary to let rest one night the mixture to well infuse and the following day you withdraw the pod and you finish the preparation) 
  5. Once cold add the cream mixes and put in the freezer until it is very cold it should not become an ice cube but a very cold liquid that makes it easier for the ice cream maker, 
  6. you put the ice cream maker on the road and pour in the cream. 
  7. Once ready, put the ice in the freezer. 
  8. frozen MARS bars, spray with the blender and added to the vanilla ice cream, 



thanks for your comments, and messages 

bonne journée 
