---
title: pancake with banana / chocolate ... raisins / chocolate
date: '2010-12-30'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya

---
the recipe is very good, so I advise you to try it. 

for my part I made two decorations: chocolate / banana, for Rayan and raisins / chocolate for Ines, it's tiring when children have different tastes. 

  * a glass of flour 
  * a glass of buttermilk 
  * a soup of sugar 
  * a coffee of baking powder 
  * half a cup of baking soda 
  * 1/4 cup of salt 
  * 1 large banana 
  * 60 grams of chocolate chips 
  * 2 tablespoons of neutral oil 
  * an egg 
  * 1 handful of raisins 



to proceed nothing is easier, mix all the products meaning together, and all the liquid together. 

mix everything together 

heat a non-stick skillet and fill your ladle with the preparation. 

then pour into the hot pan, put on a few slices of banana and chocolate chips 

turn the pancakes and cook on the other side (both sides must be golden but not burnt)   
serve warm with honey and icing sugar 

Delicious banana in the middle of all this beautiful mix. 
