---
title: The Russian cake simplified method among Algerian pastry cooks
date: '2016-04-13'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- birthday cake, party, celebrations
- cakes and cakes
tags:
- Algerian cakes
- To taste
- fondant
- desserts
- Pastry
- creams
- Butter cream

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/russes2_thumb1.jpg
---
##  The Russian cake simplified method among Algerian pastry cooks 

Hello everybody, 

For the record, I did not know that this recipe would be in 2014 one of the topics of the challenges of the best pastry chef 2014. In any case I tell you the story of this cake which is 4 years old already: 

![The Russian cake simplified method among Algerian pastry cooks](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/russes2_thumb1.jpg)

A recipe that does not deserve to be changed, or put his touch, except perhaps with peanuts. 

Although in the challenge of Mercotte, this cake has had a lot of changes, but I'm sure it will not change anything to the delight, and flavor of this Russian cake. Last week was the challenge to achieving the [ Sachertorte ](<https://www.amourdecuisine.fr/article-sachertorte-recette-gateau-autrichien.html> "Sachertorte recipe, Austrian cake") , I did not make the recipe of Mercotte, but rather the recipe of the book that I like a lot. 

**The Russian cake simplified method among Algerian pastry cooks**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/russes4_thumb1.jpg)

**Ingredients** Russian cake dough: 

  * 300 gr unhunted almonds 
  * 150 gr of flour 
  * 1 C. yeast 
  * 250 gr of butter 
  * 250 gr of icing sugar 
  * 5 egg whites 

butter cream: 
  * 2 eggs 
  * 220 gr icing sugar 
  * 250 gr of butter 



**Realization steps** preparation of the butter cream 

  1. beat the eggs and the icing sugar in a bain-marie for 15min 
  2. then let cool, then stir in the butter, mixing with the beater and leave aside 

preparation of the dough: 
  1. make the egg whites rise. 
  2. mix the almond powder that should not be too thin with the sugar yeast, flour and melted butter, 
  3. then incorporate the white in snow gently 
  4. in suites put in a dish (for biscuit roulet), going to the oven to liner of paper greaseproof and bake in 180 ° 
  5. once cooked let cool cut the cake in 2 spread the butter cream and close with the other half and spread over the butter cream 
  6. toast almonds and put on and sprinkle with icing sugar let rest and sprinkle again 
  7. and cut square or rectangle according to your tastes 



Merci a tous et a toutes pour vos visites , et commentaires. Bisous 
