---
title: mashed potatoes
date: '2014-12-25'
categories:
- Algerian cuisine
- diverse cuisine
- vegetarian dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/puree-au-poulet-a-la-sauce-indienne-003_thumb.jpg
---
[ ![puree of potato 003](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/puree-au-poulet-a-la-sauce-indienne-003_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/puree-au-poulet-a-la-sauce-indienne-003.jpg>)

##  mashed potatoes 

Hello everybody, 

To accompany your roast chickens, a [ cooked leg of lamb ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-au-four-aid-el-adha-2012-111759550.html> "Gigot D'Baked Lamb") , or one [ oriental lamb ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-a-l-orientale.html> "Leg of lamb in the east") , The mashed potato, is the ideal recipe .. 

A recipe very easy, very simple, appreciated by big and small, and since I publish the recipe for chicken in Indian sauce, we keep asking me the recipe. 

I did not publish, because in my opinion, everyone knows this recipe, but the thing we forget sometimes is that it's not just women who look at cooking blogs, looking new recipes .... 

**mashed potatoes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/puree-au-poulet-a-la-sauce-indienne-003_thumb.jpg)

Recipe type:  dish  portions:  4  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 500 gr of potatoes 
  * water to boil. 
  * salt 
  * 1 clove of garlic 
  * 1 bay leaf 
  * 1 tablespoon of butter 
  * 2 portions of kiri cheese (optional, but my kids love the taste) 
  * ½ glass of milk (more or less, everything depends on how you like your mashed potatoes) 



**Realization steps**

  1. Peel the potatoes, wash them and cut them into coarse cubes. 
  2. Put them in a pot with cold water, salt, black pepper, bay leaf, and garlic clove. 
  3. cook 20 to 25 minutes, and check with a fork that should sink easily. 
  4. remove the bay leaf, drain the potato, but do not discard the cooking water, you may need it. 
  5. you can use the vegetable mill, or the mashed press, I use the whip, not the mixer or the diver, I mean the electric whip, using both feet. 
  6. place the pieces of potato cooked without the bay leaf in a bowl, add a little butter, the piece of cheese, and the milk slowly, not at once to not liquefy your mashed potatoes. 
  7. you do not have to use all half a glass of milk. 
  8. and if your puree is a little hard, add the boiling broth slowly, until you have the desired texture. 
  9. during the presentation, serve the puree with a nice sauce of cooking chicken or beefsteak. 


