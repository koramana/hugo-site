---
title: Bavarian peaches chocolate and chocolate mirror
date: '2015-08-03'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- sweet recipes
tags:
- Pastry
- Algerian cakes
- Easy cooking
- Based
- Birthday Cakes
- To taste
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/bavarois-peche-chocolat-et-miroir-au-chocolat-2.jpg
---
[ ![Bavarian peach chocolate and chocolate mirror 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/bavarois-peche-chocolat-et-miroir-au-chocolat-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/bavarois-peche-chocolat-et-miroir-au-chocolat-2.jpg>)

##  Bavarian peaches chocolate and chocolate mirror 

Hello everybody, 

Here is the birthday cake I made for my son's 10th birthday, this time he did not ask [ Ben 10 ](<https://www.amourdecuisine.fr/article-ben-10-le-gateau-d-anniversaire-de-rayan.html>) , or [ Spiderman ](<https://www.amourdecuisine.fr/article-spiderman-le-gateau-d-anniversaire-de-rayan.html>) , or I do not know what else, in fact I was afraid he asked me the head of "Messy"! then that now he becomes a fan and a football player, I wonder who he inherited it from? and I assure you that it is not from his father !!!, hihihih 

So, this time for his birthday, he asked for something chocolate, he did not give more details, and good for me, because, it's not even 10 days that I'm in Algeria, I had do not worry too much about shopping, especially not standing for hours in front of the oven ... 

I relied on a super simple recipe, a cake that I really enjoy: [ dacquoise ](<https://www.amourdecuisine.fr/article-dacquoise-aux-noisettes.html>) , where the [ Russian patisserie Algerian ](<https://www.amourdecuisine.fr/article-le-gateau-russe-methode-simplifiee-chez-les-patissiers-algeriens.html>) . and two layers of Bavarian moss. 

So, I share with you the recipe for this delicious **Bavarian chocolate peches and chocolate mirror** , which did not last long, besides I hid this piece in the freezer to be able to make you a photo of the cut ... 

Previous rounds: 

**Bavarian peaches chocolate and chocolate mirror**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/bavarois-peche-chocolat-et-miroir-au-chocolat-3.jpg)

portions:  20  Prep time:  60 mins  cooking:  15 mins  total:  1 hour 15 mins 

**Ingredients** Dacquoise for a 35 x 45 mold 

  * 240 grs coarsely ground roasted peanuts 
  * 200 grams of icing sugar 
  * 1 teaspoon of baking powder 
  * 200 grs of melted butter 
  * 120 grams of flour 
  * 4 egg whites 

Chocolate mousse: 
  * 300 ml thick creme fraiche 
  * 3 egg yolks 
  * 300 ml of milk 
  * 120 gr of sugar 
  * 1 coffee vanilla 
  * 6 grams of gelatine leaf 
  * 120 grams of dark chocolate 

Mousse with peaches: 
  * for apricot mousse: 
  * 300 ml of fresh cream 
  * 200 g canned peaches (net weight from peaches after draining from their syrup) 
  * 60 gr of sugar (+ or - according to taste) 
  * 8 g of gelatin 
  * some drop of aroma peaches 

chocolate mirror: 
  * 200 g of sugar 
  * 70 g of water 
  * 70 g of unsweetened cocoa powder 
  * 140 g of liquid cream (35% of MG) 
  * 8g of gelatin in foil 

Decoration: 
  * peaches in syrup 
  * rolled wafers 
  * chocolate 



**Realization steps** preparation of the dacquoise: 

  1. make the egg whites rise. 
  2. mix the crushed peanuts with the sugar, the yeast, the flour and the melted butter, 
  3. then incorporate the white in snow gently 
  4. put in a baking tray lined with parchment paper and bake at 180 ° 

preparation of the chocolate mousse: 
  1. place the gelatine leaves in a little cold water and let it swell 
  2. beat egg yolks with sugar and vanilla until blanching, add hot milk while whisking, then place on low heat and stir until creamy, add chocolate and gelatin, stir and let cool. 
  3. whip the cream of fresh chantilly in a very cool place, and add it delicately to the cream mixture English cream chocolatee 
  4. place the biscuit already prepared in a pastry frame, 
  5. pour over the chocolate mousse and refrigerate for at least 2 hours, to speed up the process, put in the freezer for 1 hour 

preparation of the mousse peaches: 
  1. put the gelatin in cold water and let it swell 
  2. whip the cold cream very cold, in a bowl very cold, in whipped cream 
  3. add 30 gr of sugar, and continue to whip, put the cream in the fridge 
  4. crush the peaches roughly, add some of their syrup, 
  5. cook on low heat for a compote 
  6. add the sugar 
  7. let reduce a little, so as not to have too much liquid, and to let the water evaporate 
  8. add the dewatered gelatin and let it melt well in this mixture 
  9. put this mixture out of the fire and let cool 
  10. mix the peach compote very slowly with whipped cream, to have a homogeneous and aerated mixture 
  11. pour over the chocolate mousse and put it back in the fridge 

preparation of the chocolate mirror: 
  1. dip the gelatin in cold water. 
  2. Bring the water and sugar to the boil and leave the surroundings. 3 minutes. 
  3. In another saucepan, heat the cream. 
  4. Remove the syrup from the heat and add the cocoa powder and mix. 
  5. Pour the cream before boiling and mix again. 
  6. Add the gelatin softened and well wrung out. 
  7. let this mixture cool well, 
  8. remove the cake from the fresh, 
  9. remove the pastry frame, and gently cover the peach mousse with the chocolate mirror ... 
  10. Decorate the cake according to your taste, otherwise leave as it is, my decoration is just because it's a birthday cake 



[ ![Bavarian peach chocolate chocolate mirror](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/bavarois-peche-chocolat-miroir-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/bavarois-peche-chocolat-miroir-au-chocolat.jpg>)

and here is the list of participants in the game: 

Soulef [ Kitchen love ](<https://www.amourdecuisine.fr/>) Bavarian peach / chocolate chocolate mirror 

My articles of the day:   

