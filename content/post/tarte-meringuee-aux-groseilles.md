---
title: Meringue pie with currants
date: '2016-07-30'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-meringuee-aux-groseilles-023_thumb1.jpg
---
##  Meringue pie with currants 

Hello everybody 

here is a super delicious pie just out of the oven, the history of this pie began when I was at the supermarket, and I saw these pretty red currants, I'll be very very frank, I never buy, because the price has always been a bit expensive for a small quantity, but it is in offer, I was tempted .... 

once arrived home, I immediately jumped on the box, but ...... I was very disappointed, it's really too acidic !!!! 

Well yes I do not know the taste, I do not know if it's still acid like that, or there are some that are good, please tell me ... .. 

So, it made me feel pain and of course I will not throw the box, no, and no, I run to see my little book, and I find a recipe made with cherries, so I adopt the recipe but with a little more sugar, considering the acidity of currants.   


**Meringue pie with currants**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-meringuee-aux-groseilles-023_thumb1.jpg)

Recipe type:  pie dessert  portions:  8  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** For the shortbread dough: 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

for garnish: 
  * 500 g of currants 
  * 3 egg whites 
  * 1/4 c. cinnamon 
  * 1 bag 
  * 100 g of sugar 
  * 100 g of hazelnuts in powder 
  * 2 tbsp. flour 
  * a pinch of salt 



**Realization steps** The shortbread dough: 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a paste is removed from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Darken a pie circle 28-30 cm in diameter. 
  6. Stir the batter stock with a fork and chill for the time to prepare the filling.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2011-09-14-tarte-meringuee-aux-groseilles_thumb1-300x243.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2011-09-14-tarte-meringuee-aux-groseilles_thumb1.jpg>)

the filling 
  1. Beat the egg whites with a pinch of salt. 
  2. Gradually add the sugar, mix constantly, until they form peaks. 
  3. In another bowl combine the hazelnuts, cinnamon, sugar and vanilla flour and gently stir in the egg white. 
  4. Finally, incorporate the gooseberries. 
  5. Fill the pie shell with the filling 
  6. Bake in a preheated oven at 180 ° C for about 40 minutes 



thank you for your visits and comments 

bonne soiree 
