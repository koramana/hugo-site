---
title: Algerian cake Aid 2013
date: '2013-08-09'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/aid-el-fitr-2013-mkhabez-aux-amandes-et-noix.CR2_11.jpg
---
![aid-el-Fitr-2013-mkhabez-to-almond-and-noix.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/aid-el-fitr-2013-mkhabez-aux-amandes-et-noix.CR2_11.jpg)

##  Algerian cake Aid 2013 

Hello everybody, 

Sahha Aidkoum again, I hope you had a great day and you had the chance to visit your family and friends ... 

I share with you my cakes of help, this time, I opt for the simplest recipes, recipes that my children and my parents love ... 

I had plans of recipes to realize, but I did not have the time, can be soon incha allah ... 

on top, it's Mkhabez with walnuts and almonds, recipe and video to come .... 

![tcharek-glacee aid --- el-fitr 2013.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tcharek-glacee-aid-el-fitr-2013.CR2_1.jpg) [ Ice charek ](<https://www.amourdecuisine.fr/article-corne-de-gazelle-tcharek-glace-gateau-algerien-108301661.html>) , video to come ... 

![aid-el-Fitr-2013-tcharek-msaker.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/aid-el-fitr-2013-tcharek-msaker.CR2_1.jpg) [ tcharek msaker ](<https://www.amourdecuisine.fr/article-tcharak-msakar-103050711.html>) , video to come ... 

![mderbel, cake with jam. CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/mderbel-gateau-a-la-confiture.CR2_1.jpg) [ cake with jam ](<https://www.amourdecuisine.fr/article-25345474.html>) , bissate errih. 

![help el fitr 2013 makrout el koucha.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/aid-el-fitr-2013-makrout-el-koucha.CR2_1.jpg) [ Baked makrout and his video ](<https://www.amourdecuisine.fr/article-makrout-el-koucha-en-video-gateau-de-semoule-et-dattes-119361264.html>) ... 

![boussou the tmessou](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/boussou-la-tmessou1.jpg)

and on the Lunetoiles side you have: 

[ boussou the tmessou ](<https://www.amourdecuisine.fr/article-boussou-tmessou-losanges-fondants-aux-sesames.html> "boussou la tmessou: melting lozenges with sesame")

![praline croissants](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/croissants-au-praline1.jpg)

Croissants with Parlinee, recipe to come ... 

![Fondants with peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Fondants-aux-cacahuetes1.jpg) [ Fondant with peanuts ](<https://www.amourdecuisine.fr/article-fondants-aux-cacahuetes-gateau-se-119308095.html>) . 

![Daisy](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/marguerite1.jpg)

the daisy, recipe to come ... 

![Mkhabez-to-nuts-to-pecan-copy-1.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Mkhabez-aux-noix-de-pecan-copie-11.jpg)

Mekhabez with pecan nuts, recipe to come ... 

![petits fours with sesames-copie-1](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/petits-fours-aux-sesames-copie-11.jpg) [ Sesame Grain Bake Machines ](<https://www.amourdecuisine.fr/article-petits-fours-aux-sesames-et-confiture-119991518.html>) , 

![vanilla sands](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/sables-a-la-vanille1.jpg)

Shortbread with vanilla, recipe to come ...   


**Algerian cake Aid 2013**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/aid-el-fitr-2013-mkhabez-aux-amandes-et-noix.CR2_.jpg)

**Ingredients**

  * flour 
  * almond 
  * Butter 
  * honey 
  * candied cherries 



**Realization steps**

  1. tcharek 
  2. ghribia 
  3. mkhabez 
  4. shortbread 
  5. Griwech 


