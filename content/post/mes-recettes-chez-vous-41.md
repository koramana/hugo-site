---
title: my recipes at home 41
date: '2013-12-27'
categories:
- Algerian cuisine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg
---
[ ![2011-09-04-to-the-moment tout1_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/10/2011-09-04-pour-le-moment-tout1_thumb.jpg>)

Hello everybody, 

I come back again with an article from my recipes at home, and I thank my dear readers who have tried to contact me by email, to send me the photos of the recipes they have tried following the method on my blog . 

I put you, a system much faster so that you can send me your photos, just go on this page: 

**[ My recipes at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") **

it's very simple, I'm sure you'll like ... only, do not forget when you add the photo, to mention your name, your email if you want, the name of the recipe, the link of your blog if you are a blogger. 

[ ![my recipes at home.bmp](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/mes-recettes-chez-vous.bmp.jpg>)

we start with: 

[ muskoutchou with orange ](<https://www.amourdecuisine.fr/article-minis-mouskoutchou-a-l-orange.html> "mini mushroom with orange") at Fleur Dz (a reader) 

[ apple amandine ](<https://www.amourdecuisine.fr/article-tarte-amandine-aux-poires.html> "Pear Almond Tart") at Fleur Dz 

[ peas white sauce ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-en-sauce-blanche-cuisine-algerienne.html> "Tajine of peas in white sauce / Algerian cuisine") at Fleur Dz. 

[ Indian rice ](<https://www.amourdecuisine.fr/article-un-riz-indien-tres-delicieux.html> "a very delicious Indian rice") (as a biryani) at Fleur Dz 

[ Reverse cream with cardamom ](<https://www.amourdecuisine.fr/article-creme-renversee-caramel-cardamome.html> "Crème renversée caramel cardamome") at flower Dz 

[ Marble cake of houriyat el matbakh ](<https://www.amourdecuisine.fr/article-cake-marbre-a-la-pate-a-tartiner.html> "Marbled cake with spread") (the person did not mention his name) 

a beautiful picture of one of my readers, 

I think it's about the recipe for [ Pavlova ](<https://www.amourdecuisine.fr/article-recette-de-pavlova-au-kiwi-et-cranberry.html> "Pavlova, kiwi and cranberry recipe") and [ fruit pie with cream muslin ](<https://www.amourdecuisine.fr/article-45534727.html> "fruit tart with muslin cream / easy and fast") ... 

the reader did not mention his name or the name of the recipe. 

So please do not forget, it's easy to put the photo of your realization on this page: 

[ my achievements at home ](<https://www.amourdecuisine.fr/mes-recettes-chez-vous> "My recipes at home") , 

but I can not guess who you are, nor what is it like? 

bisous 
