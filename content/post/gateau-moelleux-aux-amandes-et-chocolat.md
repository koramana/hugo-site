---
title: fluffy cake with almonds and chocolate
date: '2016-03-19'
categories:
- Healthy cuisine
- dessert, crumbles and bars
- Chocolate cake
- cakes and cakes
- sweet recipes
tags:
- Pastry
- la France
- Algerian cakes
- Easy cooking
- Cake
- To taste
- Gluten-free cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-aux-amandes-et-chocolat-1.jpg
---
[ ![fluffy cake with almonds and chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-aux-amandes-et-chocolat-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-aux-amandes-et-chocolat-1.jpg>)

##  fluffy cake with almonds and chocolate 

Hello everybody, 

Another delicious chocolate recipe, after the [ fluffy cake with chocolate yoghurt ](<https://www.amourdecuisine.fr/article-gateau-moelleux-yaourt-chocolat.html>) Today, it is around Lunetoiles to share with you this recipe of soft cake with almonds and chocolate ... 

This chocolate cake is almost a cake without gluten, because it contains only 12 grams of flour. in any case, from the photos we get to see that the texture is sublime, and that this cake is super good, yum yum. 

You can see other recipes for chocolate cakes, such as [ chocolate cake without eggs ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-sans-oeufs-recette-delicieuse-108236853.html>) and also another sublime Lunetoiles recipe, the [ gluten-free chocolate cake ](<https://www.amourdecuisine.fr/article-gateau-au-chocolat-sans-gluten.html>) . 

[ ![fluffy cake with almonds and chocolate 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-aux-amandes-et-chocolat-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-aux-amandes-et-chocolat-2.jpg>)   


**fluffy cake with almonds and chocolate**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-aux-amandes-et-chocolat.jpg)

portions:  9  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients** For a cake of + -20 cm in diameter 

  * 100 g of chocolate 
  * 100 g of soft butter ½ salt 
  * 4 eggs beaten in omelette 
  * 80 g of sugar 
  * 80 g of almond powder 
  * 12 g of flour 
  * 6 g of baking powder 

The topping: 
  * 50 gr of chocolate, 
  * 25 gr of butter 
  * 20 gr of sugar. 



**Realization steps**

  1. Preheat your oven to 150 °. 
  2. Melt the butter and out of the heat, stir in the chocolate so that it melts gently. 
  3. Add the other ingredients one after the other in the order listed. 
  4. Mix well to obtain a nice smooth paste. 
  5. Pour the preparation into the mold of your choice buttered and floured if not silicone. 
  6. Bake for 30-40 minutes. 
  7. Wait a few moments before unmolding. 
  8. The cooking was over, the cake was well inflated and I left it in the oven door between-open so that it does not fall too quickly ... 
  9. Melt the butter (with any sugar) and from the heat, stir in the chocolate; mix well to obtain a nice topping. 
  10. Spread this chocolate sauce on the cake. 
  11. Initially, the recipe recommended a mold 32 cm in diameter; I preferred smaller for fear of not being able to unmold and also to have a little more height ....   
Enjoy your meal!! 



[ ![fluffy cake with almonds and chocolate 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-aux-amandes-et-chocolat-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/gateau-moelleux-aux-amandes-et-chocolat-3.jpg>)
