---
title: Bounty House
date: '2018-01-13'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/bounty-maison_thumb-300x224.jpg
---
##  Bounty House 

Hello everybody, 

Who does not dream of doing these **sweets** at home and add his personal touch! Well here's an opportunity with these chocolates **Bounty** **House** , these beautiful chocolate bars with coconut. I prefer to coat them with **dark chocolate** 65% cocoa or more, a very dark chocolate and very strong in taste that I like a lot. 

For my part it's a first try, especially to see the result with my molds **silicone shaped lips** I bought during sales for **Easter** .... hihihihi. these were the stocks of the **Valentine's Day** but they were a bit expensive at that time. and for those who use the **thermomix** , look at the recipe a little further down. 

Remember that to have shiny chocolate hulls, you have to respect the [ chocolate tempering curves ](<https://www.amourdecuisine.fr/article-comment-temperer-le-chocolat.html>) you can also see the [ chocolates stuffed with milk jam ](<https://www.amourdecuisine.fr/article-chocolats-fourres-a-la-confiture-de-lait.html>) . 

![would you like the recipe](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/appropriez-vous-la-recette-300x201.jpeg) ![chocolate dessert](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/dessert-chocolat-300x217.jpeg)

**Bounty House**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/bounty-maison_thumb-300x224.jpg)

portions:  12  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 125 gr of coconut 
  * 60 gr of melted butter 
  * 50 gr of icing sugar 
  * 1 C. vanilla sugar 
  * condensed and sweetened milk 
  * dark chocolate 



**Realization steps**

  1. Melt the chocolate in a bain-marie or microwave and place the chocolate with a brush at the bottom of the mold and around the edges. (best method is to fill the cavities with well-tempered chocolate, let it take almost 2 minutes and pour the mold upside down, let all the chocolate in addition flow on a sheet of baking paper) 
  2. Put in the fridge so that the chocolate can harden. 
  3. mix coconut, icing sugar and vanilla. stir in the melted butter and mix. 
  4. Add a few spoonfuls of sweetened condensed milk and collect until you get a slightly firm mixture. 
  5. Place the stuffing in the chocolate hulls and close with another layer of chocolate. 
  6. Put back in the fridge. Carefully unmold the chocolates and enjoy. 



and if you use a **thermomix** to melt and temper your chocolate (to have an impeccable shine), here is how to proceed: 

»We must temper the chocolate, have a chocolate of at least 64% cocoa or the   
**lindt** 70% dessert. 

  * Put one-third of the amount of chocolate cut into squares in the bowl, melt 50 ° V2, the time will vary depending on the amount. 
  * When the chocolate is melted, add the remaining thirds. 
  * Turn V2 until the chocolate is melted (especially do not put temperature, it must be dropped to 31 °) 
  * If your chocolate is too thick put back 3 min at 37 °. Then let V2 run again to lower the temperature. 
  * When it's ready, garnish your mussels **tupperware** , make a layer with a brush, let it take 
  * Prepare the stuffing during this time, fill the cavities with the coconut stuffing and close with another layer of chocolate. 
  * Let cool and rest at least 3 hours before unmolding.   
The ideal is to let one night rest. " 


