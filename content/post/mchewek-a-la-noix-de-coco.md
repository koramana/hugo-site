---
title: Mchewek has coconut
date: '2013-06-16'
categories:
- diverse cuisine
- Cuisine by country
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/mchewek-a-la-noix-de-coco-11.jpg
---
![Mchewek has coconut](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/mchewek-a-la-noix-de-coco-11.jpg)

##  Mchewek has coconut 

Hello everybody, 

yum yum **mchewek has coconut** a delicious Algerian dry cake from Algerian cuisine, which I share with you today, this recipe was in hibernation on my email, I do not remember when Lunetoiles sent it to me? 

wanting to clean my mailbox overload today .... you can also see the [ almond mchewek ](<https://www.amourdecuisine.fr/article-mchewek-aux-amandes-gateau-algerien-108879667.html>) , and the [ mchewek has cashew ](<https://www.amourdecuisine.fr/article-mchewek-a-la-noix-de-cajou-108371424.html>) . 

this Mchewek has the coconut is a delight I tell you, because I tried the recipe with my daughter, she loved to put her hand in the dough, and these mchewek has the coconut was just melting in the mouth . 

Thank you very much to my dear friend Lunetoiles for this nice recipe, a thank you from my daughter who had a lot of fun making your recipe ... I do not think it's going to be the last time she will do these mchewek a the coconut.   


**Mchewek has coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/mchewek-a-la-noix-de-coco1.jpg)

Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 200 gr of flour 
  * 1 sachet of baking powder 
  * 1 egg 
  * 100 gr of butter 
  * 100 gr of sugar 
  * 100 gr of coconut 

Decoration: 
  * Dreads, silver beads ... 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. With a wooden spoon, mix the butter and sugar until you get a cream. 
  3. Add the egg. 
  4. Add the coconut then the flour and the yeast and beat everything. You must get a nice dough. 
  5. Make dumplings, place a silver bead and place on a baking sheet lined with parchment paper. 
  6. Cook a dozen minutes watching. 
  7. Allow to cool completely before handling. 



![Mchewek has coconut](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/mchewek-a-la-noix-de-coco-21.jpg)
