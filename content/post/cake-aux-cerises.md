---
title: Cherry cake
date: '2009-05-29'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264041881.jpg
---
& Nbsp; it's a recipe that I found in the magazine of our super walk, and what's good is that I had all the ingredients at hand (well almost because I had to do without two ingredients, knowing that they had a great point, yes, I'm still on a diet) the recipe is very simple and it is done in 2 min, below the ingredients of the recipe (in red that's what I use, in blue the points) Ingredients: 125 gr butter (butter lighten 40% -----> 12.5 pts) 2 case & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264041881.jpg)

it's a recipe that I found in the magazine of our super walk, and what's good is that I had all the ingredients at hand (well almost because I had to do without two ingredients, knowing that they had a great contribution in point, well yes I'm still on a diet) 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264041961.jpg)

the recipe is very simple and it is done in 2 min, below the ingredients of the recipe (in red that's what I used, in blue the points) 

ingredients: 

  * 125 gr of butter (  butter lighten 40% - > 12.5 pts  ) 
  * 2 cases of brown sugar (  10 gr -------> 1 pts  ) 
  * 350 grs cherries (  270 gr frozen ----> 2.5 pts  ) 
  * 100 gr ground almonds (  I had not otherwise  10 pts  ) 
  * 125 gr sugar (  100 gr of sugar light ---> 6.5 pts  ) 
  * 2 eggs  \---------------> 4 pts 
  * 100 grams of self-raising flour  \-----> 5.5 pts 
  * 1 case of baking powder 
  * 50 grs of pine nuts (it must bring points, but I do not know how much?) 



![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264042031.jpg)

preheat the oven to 180 degrees, butter a mold less than 25 cm in diameter (mine was a bit wide and so I had a cupcake, it's you want a cake high a 20 cm mold will do well the case) 

line up your baking paper pan as shown in the picture above. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264042151.jpg)

sprinkle the surface with 2 tablespoons of brown sugar, here it is according to your taste, if you like to sweeten it, you can put the quantity you want 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264042391.jpg)

put your cherries on the whole surface of the mold (you can replace it with any other fruit) 

beat your soft butter with the beater, add the eggs, then the sugar, the almonds, the baking powder, and the pine nuts. 

beat until mixture is well blended and spread over cherries. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264042491.jpg)

if your mold is a mold with removable base like mine, put it in another mold before putting it in the oven, because your cherry will give juice, and it may run out. 

place your baking pan in the oven, and cook for about 45 minutes, depending on your oven, of course. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264042561.jpg)

at the end of the oven, let your cake cool for at least 5 minutes before unmolding. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264042641.jpg)

the cherries gave, a good juice that soaked the cake, when I reverse it. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264042821.jpg)

serve warm, or so fresh, it is delicious in both cases. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/264042871.jpg)

finally my cake, made me 32 pts in total, so by talking about 8 equitable shares, I had 4 pts for each par. 

and it was a real delight 

_**Bonne Appétit** _
