---
title: leek quiche easy recipe
date: '2017-11-23'
categories:
- appetizer, tapas, appetizer
- Algerian cuisine
- diverse cuisine
- idea, party recipe, aperitif aperitif
- pizzas / quiches / pies and sandwiches
tags:
- Salty pie
- Cheese
- Fast Food
- Pastry
- pies
- Algeria
- la France

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poirreaux-3.CR2_.jpg
---
[ ![easy leek quiche recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poirreaux-3.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poirreaux-1.CR2_.jpg>)

##  leek quiche easy recipe 

Hello everybody, 

Another leek recipe after the delicious [ leek fondue ](<https://www.amourdecuisine.fr/article-fondue-de-poireaux.html> "Leek fondue") Here we are now with a leek quiche. Another recipe that did not last long, besides I could not even take many photos, because the intoxicating and dizzying smell of this delicious leek quiche, as soon as I removed it from the oven, went to the snake dance to attract everyone to the kitchen. 

I told my children and my husband to wait because it is too hot, just as an excuse to have time to take pictures, well it did not work, I had to quickly remove the leek quiche from mold before it cools, causing the broken dough to start crumbling. 

This did not change the taste of this delicious quiche, on the contrary, it was very melting with these little crunchy crumbs of the broken dough to melt like snow in the mouth. We loved it at home, and it's not going to be the last time I'm going to do this quiche, my kids claim it as I ask them the question: what do you want at dinner ?, they say: "the thing with leeks" the thing with leeks, hahahahaha. 

So we go for this thing with leeks, I want to say the leek quiche! 

[ ![easy leek quiche recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poirreaux-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poirreaux-1.CR2_.jpg>)   


**leek quiche easy recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poirreaux-4.CR2_.jpg)

portions:  4  Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients**

  * [ pastry ](<https://www.amourdecuisine.fr/article-recette-pate-brisee-maison-facile.html> "Easy homemade broken pasta recipe")
  * 3 large leeks, or 4 ways 
  * 2 tablespoons of butter 
  * 3 medium eggs 
  * 200 ml of milk 
  * 150 ml of fresh cream 
  * salt pepper 
  * some sprigs of fresh thyme 
  * nutmeg 
  * grated cheese gruyère, emmenthal or chiddar 



**Realization steps**

  1. Prepare the broken dough, and go for a mold of almost 22 cm in diameter, place it in a cool place 
  2. Wash the leek whites very carefully (splitting them in 2) to thoroughly remove the soil. 
  3. place the butter in a pan on medium bottom, and add the sliced ​​leeks. 
  4. cook while stirring to avoid burning. add the sprigs of thyme 
  5. Season with salt and black pepper to taste. 
  6. Mix the cream, eggs and milk, then season with salt, pepper and nutmeg. 
  7. Pour the cooked leeks over the uncooked pie shell here, spread well on the surface 
  8. pour over the liquid mixture, garnish with slices of leeks   
[ ![easy recipe of leek quiche](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poireaux-facile.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poireaux-facile.jpg>)
  9. place in a preheated oven at 180 degrees C for 35 to 40 minutes, depending on the capacity of your oven. 
  10. Do not like me, let cool a little before unmolding 



[ ![easy leek quiche recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poirreaux-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/quiche-aux-poirreaux-2.CR2_.jpg>)
