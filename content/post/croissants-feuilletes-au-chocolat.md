---
title: Chocolate pastry croissants
date: '2015-02-25'
categories:
- Brioches et viennoiseries
tags:
- Pastries
- Boulange
- Pastry
- Breakfast
- To taste
- buns
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/croissants-feuillet%C3%A9e-au-chocolat-2.CR2_.jpg
---
[ ![chocolate pastry croissants](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/croissants-feuillet%C3%A9e-au-chocolat-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-croissants-feuilletes-au-chocolat.html/croissants-feuilletee-au-chocolat-2-cr2>)

##  Chocolate pastry croissants 

Hello everybody, 

The secret of the success of this puff pastry, whether or not it is raised, is to be able to keep the butter well in the dough, so that it does not escape during cooking, and the most important point is the dough before starting the foliage, if your dough is too soft, it will stick and tear. So it is very important not to wet the dough too much, even if in a recipe, you are given a quantity X of water or milk, you have to judge for yourself if you have to put all the quantity, or less . 

I hope these few points will help you make a success of your puff pastry, and to make it delicious with it. One thing is sure homemade croissants, it's a thousand times better than those of the trade, except if you have a pastry chef from the neighborhood who is very dedicated in his work, who makes croissants worthy of their names. 

[ ![chocolate pastry croissants](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Croissants-feuillet%C3%A9s-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/article-croissants-feuilletes-au-chocolat.html/croissants-feuilletes-au-chocolat>)

**Chocolate pastry croissants**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/croissants-au-chocolat-3.jpg)

portions:  12  Prep time:  60 mins  cooking:  15 mins  total:  1 hour 15 mins 

**Ingredients**

  * 450 gr of flour 
  * 1 teaspoon yeast dehydrated yeast 
  * 9 gr of salt if not a teaspoon 
  * 45 g of sugar 
  * 1 egg 
  * 220 ml of milk (+ or - depending on your flour, and the size of the egg) 
  * 150 g of special lamination butter at room temperature 

prank call: 
  * choice of nutella, or chocolate sticks 

for gilding: 
  * 1 egg yolk 
  * 1 little milk 



**Realization steps** prepare the puff pastry 

  1. In the bowl of the robot, put the flour, the salt, and the sugar, mix and add the yeast 
  2. add the whipped egg and mix again 
  3. Introduce the milk now, until you have a nice dough that picks up well, but is not too sticky. 
  4. Knead again to obtain a smooth and homogeneous paste. 
  5. Let stand for 30 minutes covering with a clean cloth. 
  6. in the meantime, spread the butter between two sheets of baking paper, in a rectangle and place it in the fridge. 
  7. spread the dough after rest in rectangle on a floured space 
  8. place the butter in one side of the rectangle.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pate-feuillet%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40669>)
  9. fold the other side on, and weld well, 
  10. Spread the dough one more time, to be three times its original size, the best way to proceed at the beginning not to break the butter, and to give strokes gently with the help of the baking roll to crush the dough, no strong shots, ok.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pate-feuillet%C3%A9e-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40670>)
  11. fold the dough in three like a wallet   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/pate-feuillet%C3%A9e-2-003.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40674>)
  12. Put in the fridge 20 minutes. 
  13. turn the dough so that the folding is on the right (ie if you want to open the wallet, the opening is on the right) 
  14. spread the dough again in large rectangle, and fold it on three. 
  15. put in the fridge again, and repeat the operation, while respecting the position of the opening of the dough, which must always be on the right. 
  16. put the dough to rest in the fridge another 20 minutes, and here it is going to be ready for shaping. 
  17. Spread the dough one more time, and cut triangles of a base of 15 cm, and of sides big enough (almost 20 cm)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/forme-de-croissant.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40675>)
  18. make a cut at the base. 
  19. place chocolate or nutella in it, and roll it from the base into a crescent shape. 
  20. place as you go in a baking sheet.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/croissant-a-cuire.jpg) ](<https://www.amourdecuisine.fr/article-croissants-feuilletes-au-chocolat.html/croissant-a-cuire>)
  21. let the croissants rise (I'm not going to say 1h or 2h, me with the brioches and pastries, it takes the time that it wants, maybe it was 4 hours for me) 
  22. preheat the oven to 180 degrees C 
  23. gently brush the surface of the croissants with the egg yolk and milk mixture. 
  24. cook for at least 15 minutes, until the croissants turn to a beautiful golden color 



[ ![chocolate pastry croissants](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/croissants-feuillet%C3%A9es-au-chocolat-1.jpg) ](<https://www.amourdecuisine.fr/article-croissants-feuilletes-au-chocolat.html/croissants-feuilletes-au-chocolat>)
