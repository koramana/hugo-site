---
title: BURNED CREAM WITH COCONUT AND GRANADA MILK
date: '2011-10-05'
categories:
- Mina el forn
- bread, traditional bread, cake

---
Hello everybody, 

a very very good burnt cream that I prepared yesterday after my return home with my daughter, we had a small outing with the school to go to the forest, the climate was not in our favor, but we have have a good time, I'll try tomorrow to post some photos. 

it was my daughter who took care of the pomegranates, degraded it, seed by seed, and in the end I only had half the quantity. 

this recipe I had noted before I started my blog, at the time I did not even know it was what a blog, and when I noted the recipe, I did not even bother to note the source, so if this person is still in the blogosphere, I make a call, hihihiih it makes us laugh to say, but returning to Cesar what belongs to Cesar ... .. 

so I could put the source. 

PS: sorry for the photos I did not like at all, because it's pictures taken in the evening, and I have a big problem of light with the photos, and especially my little camera ... so my return this afternoon at home, I'll make new pictures, because there are only two ramekins, but it will do the trick 

thank you 

then the recipe: 

Ingredients: 

  1. 1 grenade 
  2. 100 ml of coconut milk 
  3. 20 g of sugar 
  4. 2 whole eggs 
  5. 4 egg yolks 
  6. 50ml of liquid cream 
  7. 20g grated coconut (replace with brown sugar0 


  1. Get the pomegranate seeds. 
  2. Divide the seeds into the ramekins (6 for me). 
  3. Heat the coconut milk, cream and sugar. 
  4. Whip whole eggs and yolks and pour coconut milk cream over this preparation. 
  5. mix without lathering. 
  6. Pour this mixture over the pomegranate seeds. 
  7. Sprinkle with grated coconut and bake for 50 minutes at 100 °. 
  8. Let cool and keep cool for at least 2 hours. 
  9. When serving, sprinkle with sugar and caramelize with a torch. 



merci pour vos visites. 
