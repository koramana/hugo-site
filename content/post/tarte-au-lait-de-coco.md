---
title: coconut milk pie
date: '2015-03-03'
categories:
- gateaux, et cakes
- sweet recipes
- pies and tarts
tags:
- desserts
- Cakes
- Algerian cakes
- Easy cooking
- To taste
- flan
- Flan cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tarte-au-lait-de-coco-3.CR2_.jpg
---
[ ![coconut milk pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tarte-au-lait-de-coco-3.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-tarte-au-lait-de-coco.html/tarte-au-coco>)

##  coconut milk pie 

Hello everybody, 

A very beautiful coconut milk pie, that one of my friends of facebook to achieve: Nicole Gozzi, and that tempted me a lot. Especially that's good with the game: Recipes around an ingredient # 3, whose theme is: coconut milk. 

This coconut milk pie is just a delight, melting in the mouth, the texture of a soft custard, and super-scented coconut, in both states. Yes, because this pie contains coconut milk and grated coconut. 

The pleasure does not stop there, because in less than 20 minutes, and in a small gesture your pie is already baked for leather, no need to prepare a dough, just Geniale this pie with coconut milk. 

**coconut milk pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tarte-au-lait-de-coco-1.CR2_.jpg)

portions:  8-12  Prep time:  10 mins  cooking:  60 mins  total:  1 hour 10 mins 

**Ingredients**

  * ½ cup of melted butter. (my half cup measures 120 ml) 
  * ½ cup of sugar. 
  * 4 big eggs. 
  * 2 cups coconut milk, add milk to make 2 cups. 
  * 3 teaspoons of vanilla. 
  * ½ cup of flour. 
  * 1 cup of baking powder. 
  * 1 cup of grated coconut. 
  * ½ to 1 teaspoon cinnamon tea according to taste. (It's well hidden the smell of eggs) 



**Realization steps**

  1. Heat the oven to 180 degrees C. 
  2. Whisk everything. 
  3. Put in a buttered mold. 
  4. cook for 15 minutes at 180 degrees. 
  5. Lower the temperature to 160 degrees C and continue cooking for 45 minutes. 
  6. Remove from oven and let cool before eating. 



[ ![coconut milk pie](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/tarte-au-coco.jpg) ](<https://www.amourdecuisine.fr/article-tarte-au-lait-de-coco.html/tarte-au-coco>)

The participants in this tour: 

**Love of cooking** of the blog [ https://www.amourdecuisine.fr ](<https://www.amourdecuisine.fr/>) with "Coconut milk pie" 

**Isabelle Lapierre** non bloggeuse avec « Riz au lait de coco » 
