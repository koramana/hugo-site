---
title: white chocolate ganache
date: '2014-11-10'
categories:
- basic pastry recipes
- sweet recipes
tags:
- macarons
- Confectionery
- Algerian cakes
- Cakes
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/macarons_thumb1.jpg
---
[ ![macarons_thumb1](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/macarons_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/macarons_thumb1.jpg>)

##  white chocolate ganache 

Hello everybody, 

The white chocolate ganache is part of many recipes, such as entremet, personally I love to relish the white chocolate ganache to stuff macaroons, regardless of the flavor of macaroons, this white chocolate ganache, and just what you need, if you could not prepare another joke. 

As you can see in the picture, I made this white chocolate ganache to garnish my [ macarons ](<https://www.amourdecuisine.fr/article-macarons-a-la-ganache-au-chocolat-blanc.html> "Macaroons with white chocolate ganache") natures 

**white chocolate ganache**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/macarons-037-a_thumb1.jpg)

portions:  4 to 6  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 175 g white chocolate 
  * 75 g whole cream 



**Realization steps**

  1. Boil the cream 
  2. pour over the broken white chocolate into small pieces. 
  3. Leave a little and stir 
  4. put in the fridge for 10 minutes 
  5. whip again 
  6. place in a pastry bag and garnish the shells with macaroons 



preparation of the ganache: 

  1. Boil the cream 
  2. pour over the broken white chocolate into small pieces. 
  3. Leave a little and stir 
  4. put in the fridge for 10 min and whip again 
  5. place in a pastry bag and garnish the shells with macaroons 



thank you for your visits 

et merci pour vos commentaires 
