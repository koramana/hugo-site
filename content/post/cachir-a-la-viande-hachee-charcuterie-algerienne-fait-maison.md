---
title: to hide with the meat chopped, charcuterie Algerian homemade
date: '2012-12-07'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cachir-a-la-viande-amour-de-cuisine-charcuterie-036.CR2_t.jpg
---
[ ![to hide with the meat love of kitchen, charcuterie 036.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cachir-a-la-viande-amour-de-cuisine-charcuterie-036.CR2_t.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cachir-a-la-viande-amour-de-cuisine-charcuterie-036.CR2_2.jpg>)

##  to hide with the meat chopped, charcuterie Algerian homemade 

Hello everybody, 

always with the most popular Algerian charcuterie: hide with minced meat, Algerian charcuterie homemade, and I will dare to say the easiest to realize, because we also have the merguez, but here in England I do not find the casings to realize this delight. 

in any case I come back to home-made caching, yesterday it was the turn of the cachir to minced meat, huuuuuuuuuuum, it must be said that the recipe of [ Chicken ](<https://www.amourdecuisine.fr/article-cachir-fait-maison-saucisson-ou-pate-de-volaille-hallal-113036964.html>) I was so seduced that I wanted to make it, but I only had chopped meat. 

so we go for this part of nibbling ????? 

**to hide with the meat chopped, charcuterie Algerian homemade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cachir-fait-maison-a-la-viande-hachee-055.CR2_thumb.jpg)

Recipe type:  Appetizer, appetizer  portions:  10  Prep time:  15 mins  cooking:  60 mins  total:  1 hour 15 mins 

**Ingredients**

  * 5 eggs 
  * 5 tablespoons fine semolina 
  * 2 portion of kiri cow cheese 
  * 2 cloves garlic 
  * 1 knorr cube (or any other brand, I did not have one, so not put) 
  * Half a glass of oil 
  * 200 g of raw minced meat. 
  * A parsley stalk 
  * A pinch of paprika 
  * Cumin, black pepper 
  * ½ teaspoon of salt 
  * red dye. 
  * a handful of pitted green olives. 



**Realization steps**

  1. put all the ingredients (without the olives) in the blender. 
  2. puree. 
  3. then add a handful of green olives or black slices, 
  4. put in a pot to seal that closes, or fail to put in a bag of spaghetti and close with a string.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/moule-a-cachir_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/moule-a-cachir_thumb.jpg>)
  5. tap the pot against the work table to remove air bubbles. 
  6. put in the bottom of a couscoussier or a tall container, and let simmer for 1 hour, 
  7. check the cooking with the blade of a knife, it must come out dry. 
  8. put in the fridge for a few hours or overnight. 
  9. Slice and taste .... 



#  thin meat kosher (salami), homemade Algerian sausage 

Ingredients: 

  * 5 eggs 
  * 5 tablespoons semolina 
  * 2 portion cheese (laughing cow) 
  * 2 cloves garlic 
  * 1 cube of knorr (vegetable stock) 
  * 1/2 cup olive oil 
  * 200 gr frensh minced meat. 
  * some parsley 
  * A pinch of paprika 
  * Cumin, black pepper 
  * 1/2 teaspoon salt 
  * red food color. 
  * a handful of pitted green olives. 



preparation: 

[ ![mold to be squeezed](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/moule-a-cachir_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/moule-a-cachir.jpg>)

  1. put all ingredients (excluding olives) in a blender. 
  2. puree every thing very well. 
  3. then add green or black olives, sliced, 
  4. for the mixture in a special pot (cylinder pot for cachir, or in an empty spaghetti bag that you will close tightly with a string). 
  5. tap the pot against the worktable to remove air bubbles. 
  6. put in the bottom of a steamer or high container, and simmer for 1 hour, 
  7. check cooking with a knife, it should come out dry. 
  8. put in the fridge for few hours or all night. 
  9. Slice and enjoy .... 



[ ![hide with homemade chopped meat, charcuterie 049.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cachir-a-la-viande-hachee-fait-maison-charcuterie-049.C1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cachir-a-la-viande-hachee-fait-maison-charcuterie-049.CR2_.jpg>)

[ ![hide with meat 051.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cachir-a-la-viande-051.CR2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/cachir-a-la-viande-051.CR2_2.jpg>)
