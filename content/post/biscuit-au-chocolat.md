---
title: Chocolate biscuit
date: '2013-12-28'
categories:
- cheesecakes and tiramisus
- dessert, crumbles and bars
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/biscuit-algerien-052_thumb1.jpg
---
##  Chocolate biscuit 

Hello everybody, 

of the **Algerian biscuits** , **fondants** , **soft** , sweet on the palate, **perfumed** , very scented even, that's what I prepared for the drip of yesterday, a recipe for [ fluffy shortbread ](<https://www.amourdecuisine.fr/categorie-12344749.html>) wish, ultra fast, and very easy. 

at first I wanted to realize the **volcano biscuit** but since I did not have **peanuts** nor **almonds** , I thought about decorating the cake at the **coconut** , then, I told myself the coconut, will take a color when cooking .... so better that I leave the coconut fresh, and I decorate the **cake** with, after cooking with a little bit of [ jam ](<https://www.amourdecuisine.fr/categorie-11700642.html>) .... then another idea ... for what not to add **cocoa** , so we can see the decoration with the coconut ... .. 

a **recipe** improvised but a result of **chief** , **cocoa shortbread** to redo absolutely. 

**Chocolate biscuit**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/biscuit-algerien-052_thumb1.jpg)

Recipe type:  chocolate christmas sands  portions:  30  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 230 gr of flour 
  * 20 grams of bitter cocoa 
  * 125 gr of butter 
  * 70 gr of sugar 
  * 2 egg yolks 
  * 1 cup of vanilla sugar 
  * ½ packet of baking powder 

for decoration: 
  * coconut 
  * [ pear and ginger jam ](<https://www.amourdecuisine.fr/article-confiture-de-poires-et-gingembre.html> "pear and ginger jam")



**Realization steps**

  1. method of preparation: 
  2. Preheat the oven to 180 °. 
  3. In a salad bowl, mix the butter cut in small pieces with the 2 egg yolks, the sugar and the vanilla sugar. 
  4. Add the flour / cocoa mixture, and add it to a smooth dough (do not over-knead the dough). 
  5. form balls of 20 gr each 
  6. place them immediately on a baking sheet, floured or lined with baking paper 
  7. press in the middle to form a well (my daughter likes to make small prints) 
  8. cook for 5 to 10 minutes at first, if you notice that the prints disappear, take out the tray, and press again in the middle 
  9. finish cooking, knowing that the cookie cooks completely and between 15 and 20 minutes. 
  10. take out your cookies from the oven, let it cool 
  11. wrap the edges of the cake in jam, and then in the coconut 
  12. decorate the heart of the biscuit with a nice spoon of jam. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
