---
title: frangipane pie with peaches
date: '2013-01-01'
categories:
- Algerian cakes- oriental- modern- fete aid
- oriental delicacies, oriental pastries
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-amandine-aux-peches-046_thumb1.jpg
---
Hello everybody, 

a tart that will never disappoint you in front of your guests, or even just for a drop, this delight is always a great success, a 15 min work, and a bluffing result. 

and as they say, try it is adopt it, and your guests will never leave without asking the recipe. 

usually and normally, I make a lot of cakes, and the result is always a great success, but I do not know if you're like me, sometimes, when we wait, guests, and we want to do something we are used to, and we miss it ??? !!! why, or how, we'll never know, but it happens, and that's it. 

so I do not like taking the risk, and when I know that I have an invitation, well I have my recipe, which I know will not disappoint me, the result is always, a great success, a recipe easy, simple, and can be varied to the infinite, nothing that changes the fruit that is included, so know what you have on hand. 

so me yesterday, and for a drip between friends who come for the first time at home, that's what I prepared them, and I had in my closet only a peach box in pieces, I would have liked that it is with the moities of peaches, but does not prevent, the peaches remain a very delicious fruit. 

**frangipane pie with peaches**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tarte-amandine-aux-peches-046_thumb1.jpg)

Recipe type:  dessert pie  portions:  8  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** For the shortbread dough: 

  * 240 g flour 
  * 150 g of butter 
  * 90 g of sugar 
  * 30 g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

Almond cream: 
  * 120 g of almond powder 
  * 2 eggs 
  * 100 g of sugar 
  * 200 ml whole liquid cream 
  * a box of peaches in his juice   
(we do well drain, ok) 

finish: 
  * flaked almonds 
  * 2 tbsp. orange marmalade   
(That's what I had, apricot jam will do well 
  * Strawberry jam. 



**Realization steps** The shortbread dough: 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a paste is removed from the sides of the bowl and is non-sticky. 
  4. Let it rest. Spread the dough with the roll between 2 sheets of baking paper. 
  5. Darken a pie circle 28-30 cm in diameter. 
  6. Stir the batter stock with a fork and chill for the time to prepare the filling. 

following the preparation: 
  1. In the bowl of the beater this time equipped with the whisk, beat the eggs with the sugar. 
  2. When the mixture is frothy add the almond powder and the cream. 
  3. Beat again. 
  4. remove your mold with the pie shell of the refrigerator, and with the help of a brush, decorate the bottom with a little strawberry jam (this step is optional) 
  5. cover with almond cream, do not overflow 
  6. to add the peach pieces. 
  7. Sprinkle with flaked almonds. 
  8. Put in the oven for 30 minutes at 180 ° C (th.6). 
  9. Let cool. When the tart is warm, heat the jam in the microwave and brush the jam pie with a brush. 
  10. Unmould and present on a serving platter. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

et merci de vous inscrire a la newsletter, il suffit de cliquez sur cette image, et validez ensuite l’email de confirmation qui devrait arriver sur votre email adresse que vous avez mise. 
