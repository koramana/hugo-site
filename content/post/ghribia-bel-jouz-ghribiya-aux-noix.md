---
title: ghribia bel jouz, walnut ghribiya
date: '2017-04-08'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghribia_thumb.jpg
---
#  [ ![Algerian cakes ghribia](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghribia_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghribia.jpg>)

##  Beautiful ghribia play, walnut ghribiya. 

Hello everybody, 

I am in full preparation of [ Algerian cakes for the 2011 aid ](<https://www.amourdecuisine.fr/article-aid-el-fitr-2011-les-gateaux-algeriens-82873081.html>) It's a big step, and I always prefer to start with cookies, and yesterday I prepared [ these delicious ghribiya ](<https://www.amourdecuisine.fr/categorie-12344749.html>) , a succulent [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) These certainly not a diet cake, but a piece, will not hurt too much. 

but do not go to abuse and say that soulef told us, it will not hurt, because with this ghriba, we do not really stop in one piece, so it's fondant wish, I did this cake late at night, so I could not do a lot of pictures for the steps, something that my readers really like, but I'm sure it's one of [ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>) the easiest to do. 

this [ Algerian cake ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) is similar to the Montecaos cake, and the English short bread cake, cake with smen that is used a lot in the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>)   


**ghribia bel jouz, walnut ghribiya**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghribia_thumb.jpg)

portions:  60  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients**

  * 370 gr of smen, ghee or clarified butter 
  * 150 ml of oil 
  * 150 gr of icing sugar 
  * a few drops of almond extract 
  * a coffee of vanilla 
  * 100g of nuts in powder 
  * flour   
(+ or - 1 kilo, depending on the absorption of the flour) 
  * half a walnut kernel   
(optional just for the decoration) 



**Realization steps**

  1. so after these explanations, we go to the method: 
  2. in a large salad bowl, or bowl whip the smen and sugar 
  3. when the mixture smen and sugar is well creamy, introduce the oil   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghribia-montecaos_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghribia-montecaos_thumb.jpg>)
  4. beat well, then add vanilla, almond extract and nuts 
  5. using a spatula add the flour, until you have a soft dough, not too sticky by hand and well manageable 
  6. form rolls 1.5 cm high and 2 cm wide 
  7. put the fork over to decorate the pudding 
  8. cut diamonds 3 cm long 
  9. place in a baking tray 
  10. cook in a preheated oven at 180 degrees, for 15 min maximum, these cakes cook quickly, so watch carefully 
  11. remove as soon as it has changed color from cream to white 
  12. let cool, before putting in an airtight box.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/ghribia-bel-jouz-gateau-constantinois_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/ghribia-bel-jouz-gateau-constantinois_thumb.jpg>)



Note the smen or ghee, must be solid, not liquid   
the trick in this recipe for success is how to whip the smen with icing sugar, our grandmothers and mothers did it by hand, now we have the electric whisk, and it's really effective, it takes to whip until having a cream well ventilated, and which would have almost doubled of volume.   
when adding the flour, I do as for the macaroon, with a spatula or a spatula, so that it is always airy, we can do it by hand, the most important, do not pile up adding the flour, and screw up all the work done with the whip ... ok [ ![Algerian cakes ghriba with walnuts](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghriba-aux-noix_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-algeriens-ghriba-aux-noix.jpg>)

merci beaucoup pour vos commentaires et vos visites. 
