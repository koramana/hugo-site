---
title: seafood gratin
date: '2017-04-04'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- ramadan recipe
- fish and seafood recipes
tags:
- Express cuisine
- Fish
- Algeria
- inputs
- Easy cooking
- Morocco
- shrimps

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gratin-de-fruits-de-mer-4.jpg
---
[ ![seafood gratin](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gratin-de-fruits-de-mer-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gratin-de-fruits-de-mer-1.jpg>)

##  Seafood gratin 

Hello everybody, 

At home, I always have in my freezer a bag of frozen seafood, because if I am taken by the household or by the baby, and that I see that it is almost midday, it is what I prepare , a gratin with seafood, a recipe very easy to make, very express, because the time to spend the seafood to jump in a trickle of oil, I prepare the [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video") , and the time that the seafood cool a little, I blow up a little chopped onion, or as in this recipe a little shallot in a pu of butter ... 2 minutes after my dish is in the oven to brown for a maximum of 15 minutes ... a magic trick, and a perfect dish to proudly present, even for guests unannounced, it's not the recipe that will let you down, lol. 

Do you also have to be short of time, and not know what you can do to eat? what are you doing then? 

[ ![seafood gratin](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gratin-de-fruits-de-mer-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gratin-de-fruits-de-mer-1.jpg>)   


**seafood gratin**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gratin-de-fruits-de-mer-2.jpg)

portions:  2 -3  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 400 g frozen seafood 
  * 2 shallots 
  * 2 tablespoons of butter 
  * 3 tablespoons grated cheese 
  * 1 small branch of thyme 
  * salt and pepper 
  * [ béchamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video")
  * breadcrumbs 



**Realization steps**

  1. Prepare bechamel sauce first 
  2. sauté the seafood in a little oil, watch not to burn them 
  3. remove from the pan and place on a plate on absorbent paper 
  4. slice the pre-washed shallots and fry in a little butter over medium heat 
  5. butter an ovenproof baking tin. 
  6. place in the seafood, shallot and grated cheese 
  7. sprinkle over thyme 
  8. cover with a nice layer of béchamel 
  9. sprinkle over breadcrumbs and place in preheated oven, until gratin is nicely colored. 



[ ![seafood gratin](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gratin-de-fruits-de-mer-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/gratin-de-fruits-de-mer-3.jpg>)
