---
title: strawberry millefeuille with mascarpone
date: '2016-02-13'
categories:
- dessert, crumbles and bars
- sweet recipes
- pies and tarts
tags:
- Puff pastry
- Express cuisine
- Cakes
- Healthy cuisine
- To taste
- desserts

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/milles-feuilles-aux-fraises-et-mascarpone-3.jpg
---
![thousand strawberry leaves and mascarpone 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/milles-feuilles-aux-fraises-et-mascarpone-3.jpg)

##  strawberry millefeuille with mascarpone 

Hello everybody, 

I had just returned from my short stay that I hear my old neighbor knocking on my door, I thought she had a package that arrived during my absence when I saw the two bags in her hand, actually No, she came to ask me if I could make homemade cakes for Valentine's Day, that she could offer to her children, or rather 4 couples. 

The poor had bought puff pastry instead of shortbread, mascarpone, pretty strawberries, and strawberry jam, and she asked me for two different recipes, there was also chocolate and eggs. 

Here is the first realization, sorry I photographed the ugliest parts of my millefeuille with strawberries and mascarpone, because I wanted to give him cakes intact.   


**strawberry millefeuille with mascarpone**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/milles-feuilles-aux-fraises-et-mascarpone-a.jpg)

portions:  16  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients** For 16 pieces: 

  * 2 rolls of puff pastry 
  * 1 egg yolk 
  * ½ box of mascarpone 
  * 1 box of liquid cream 
  * 4 tablespoons of strawberry jam (or more according to your taste for sugar 
  * strawberries to garnish. 



**Realization steps** preparation of the pasta: 

  1. cut 6 cm slices and others 8 cm in diameter (you must have the same number, ie 16 of each) of the puff pastry 
  2. prick with a fork, and place the circles of puff pastry on a baking dish covered with baking paper. 
  3. brush the small circle with egg yolk and sprinkle a little sugar. 
  4. cook in a preheated oven at 180 degrees C between 10 and 15 minutes. (I removed the small pieces because they had taken a nice color well before large pieces.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mille-feuilles-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/mille-feuilles-aux-fraises.jpg>)

strawberry mascarpone cream: 
  1. in a salad bowl place the liquid cream, mascarpone and strawberry jam. 
  2. whip up with a nice whipped cream that holds up well. 
  3. fill a pastry bag, fill the first disc. 
  4. cut the strawberries into thin slices, garnish the mascarpone cream with then cover once more with a small layer of cream. 
  5. place over the small disk, and decorate it with a little cream. I wanted to put over a slice of strawberries, but the strawberries were too big, so I decorated with a touch of strawberry jam.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/creme-mascarpone-aux-fraises.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/creme-mascarpone-aux-fraises.jpg>)



[ ![thousand strawberry leaves and mascarpone 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/milles-feuilles-aux-fraises-et-mascarpone-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/milles-feuilles-aux-fraises-et-mascarpone-4.jpg>)
