---
title: salty crepe shrimps and mushrooms
date: '2013-01-02'
categories:
- Algerian cuisine
- ramadan recipe
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/c-f-002_thumb.jpg
---
[ ![salty crepe shrimps and mushrooms](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/c-f-002_thumb.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-crepes-aux-crevettes.html/c-f-002_thumb>)

##  salty crepe shrimps and mushrooms 

Hello everybody, 

Here is another recipe from **salty crepe shrimps and mushrooms** from [ Houriat el matbakh ](<https://www.amourdecuisine.fr/categorie-11131465.html>) , which breaks down on the chain **fatafeat TV,** of the [ Crepes ](<https://www.amourdecuisine.fr/categorie-12348217.html>) prepared with care, stuffed with a beautiful shrimp sauce at the [ Bechamel ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile-99197863.html>) , a dish melting in the mouth and complete nutritious side ... 

I thought at one point that the pancake-based dishes are heavy and not easy to disgorge, but with this gratin of salty shrimp and mushroom pancake, I promise you that you will not stop at a single bite .   


**salty crepe shrimps and mushrooms**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/crevette-600x450.jpg)

portions:  4  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** for crepe dough:   
(must say that I made half of the ingredients, and I had a nice amount of pancakes, already half is in the freezer) 

  * 400 g flour 
  * 3 eggs 
  * 50 g of melted butter 
  * 1 liter of milk 
  * A pinch of salt 
  * 1 tablespoon of sugar 

the joke: 
  * 300 g cooked shrimp 
  * 250 g mushrooms 
  * 2 cloves garlic 
  * 2 tbsp. chopped parsley 
  * 1 C. soup of lemon juice 
  * Black pepper to taste 
  * ½ teaspoon of salt 
  * a little nutmeg 
  * 100 g of pitted olives 
  * A sliced ​​onion 
  * Half a red pepper grilled and cut into pieces 
  * a quantity of bechamel sauce (recipe below) 
  * 2 tbsp. butter 

Bechamel sauce: 
  * 2 tbsp. butter 
  * 2 tbsp. flour 
  * Milk or 400 ml (200 ml of milk + 200 fresh cream (not for me)) 
  * Little spoon of salt 
  * a little chopped garlic 
  * a little nutmeg 
  * Black pepper to taste 
  * 200g of Emmantal cheese (for me it was a bit of Gruyère cheese) 
  * 50 g of parmesan cheese 



**Realization steps** Prepare the dough: 

  1. Mix the dry ingredients, then add the milk, eggs and melted butter and mix well with a whisk if you have one 
  2. Let the dough rest a little while, heat a frying pan and rub with a little butter, pour the amount of a small ladle and spread well cook on one side, then on the other side too. and let it cool a little   
![](https://www.amourdecuisine.fr/wp-content/uploads/2010/07/crepe-farcee-005_thumb.jpg)

preparation of the stuffing: 
  1. In a frying pan over high heat put the butter and fry the mushrooms until it is a beautiful gilding, add the lemon juice, parsley, salt and black pepper and set aside. 
  2. on the other side put some butter and fry the onions and garlic, then add the already cooked shrimp too, then add the other ingredients and a little Bechamel sauce.   
![gratin pancakes with shrimps](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepe-farcee-009_thumb.jpg)

prepare the béchamel sauce 
  1. In a saucepan over low heat, put the butter, then add the flour and mix well until it turns a nice golden color, then add the milk and mix without stopping. Add salt, black pepper, nutmeg and crushed garlic. do not stop stirring with a spoon until a mixture becomes thick.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/sauce-bechamel.jpg)
  2. Stuff each pancake with a large spoonful of stuffing and place in a well-buttered pan, cover with a little béchamel sauce and sprinkle with grated cheese then put in a hot oven to grill for 15 to 20 minutes. minutes 
  3. serve hot with a nice salad. 


