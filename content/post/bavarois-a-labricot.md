---
title: Bavarian apricot
date: '2012-04-28'
categories:
- salades, verrines salees

---
hello everyone, & nbsp; a delicious baravois with apricots, very unctuous, very sparkling, and very tasty, a recipe to redo with great pleasure. see here for more Bavarian ingredients: - a sponge cake with 2 eggs (here) mousse with apricots: -350 ml of cream - 200 gr of apricots (it was boxed apricot, and this is the net weight of apricot after draining of syrup) - 60 gr of sugar - 2 cases of gelatin dissolved in apricot syrup. - essence of apricots or (bitter almond + orange). Beat the cream liquid & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

a delicious baravois with apricots, very unctuous, very sparkling, and very tasty, a recipe to redo with great pleasure. see here for more [ Bavarian ](<https://www.amourdecuisine.fr/categorie-12125855.html>)

Ingredients: 

\- a sponge cake with 2 eggs ( [ right here ](<https://www.amourdecuisine.fr/article-genoise-poids-de-plume-facile-103501869.html>) ) 

apricot mousse: 

-350 ml of fresh cream 

\- 200 gr of apricots (it was apricot in box, and it is the net weight of the apricot after having drained of its syrup) 

\- 60 gr of sugar 

\- 2 cases of gelatin dissolved in apricot syrup. 

\- essence of apricots or (bitter almond + orange). 

Beat the liquid cream with the sugar until you have a whipped cream and reserve in a cool place. 

Put the gelatin in a tide bath to make it well desoldered. 

Mix the apricots to obtain a purée (you can put more than this quantity to have more taste) 

mix the melted gelatin with the apricot puree, and add it to the whipped cream 

place the sponge cake in a pastry ring, sprinkle with a syrup of apricot syrup and a little sugar (according to taste), then pour over the apricot mousse, and decorate as you like. 

j’avais utiliser tout mes abricots, alors je n’avis que des mandarines pour decorer, hihihiih 
