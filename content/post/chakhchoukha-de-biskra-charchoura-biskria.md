---
title: Biskra chakhchoukha, Charchura Biskria
date: '2012-11-06'
categories:
- cheesecakes et tiramisus

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chakhchoukha-de-biskra-trida-mfermsa-charchoura_thumb.jpg
---
[ ![biskra chakhchoukha, trida, mfermsa, charchoura](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chakhchoukha-de-biskra-trida-mfermsa-charchoura_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chakhchoukha-de-biskra-trida-mfermsa-charchoura.jpg>)

##  Biskra chakhchoukha, Charchura Biskria 

Hello everybody, 

here's one  traditional Algerian dish  , known by several nominations, depending on the region, generally known  Shakhchukha biskria  , or  chakhchoukha of Biskra  in Constantine, we know him as  trida of tajine  because there is [ trida ](<https://www.amourdecuisine.fr/article-trida-102621404.html>) shaped like small squares. and at Setif he is known as  Mfermsa  because it contained dried apricot paste,  Fremasse  (which disappeared with the time of the dish, but it is still called the Mfermsa) 

so the photos come directly from Algeria, and it's my mother who made the dish, and you made the pictures, I admit that I myself could not make this dish because I do not can not take the photos and do it, but my mother did it. 

it is a dish that takes a lot of time to realize, because the most important is to prepare the sheets of trida, rogags or msemen, and it is not an easy job. 

**Biskra chakhchoukha, Charchura Biskria**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chakhchoukha-de-biskra-trida-mfermsa-charchoura_thumb.jpg)

Recipe type:  Algerian dish  portions:  4  Prep time:  40 mins  cooking:  45 mins  total:  1 hour 25 mins 

**Ingredients** for the pasta: 

  * 500 g of fine semolina (good semolina which will give the elasticity thereafter) 
  * salt 
  * water as needed 
  * the oil to work shaping the dumplings towards the end. 

for the sauce: 
  * Lamb meat 
  * 2 cases of tomato concentrate (or one if you are not too numerous and you will do a small amount) 
  * salt 
  * paprika 
  * black pepper 
  * ras el hanout 
  * 1 onion 
  * 2 cloves garlic 
  * 1 tablespoon of butter 
  * oil 
  * 1 nice handful of chickpeas 
  * vegetables: carrots, potatoes, zucchini, or according to your taste. (my mother did not put some because my father does not like vegetables) 



**Realization steps** to prepare the msemen sheets, or the rogags is the same way to prepare the mhadjebs: 

  1. wet the semolina and the salt, with water, gently until it is easy to handle, 
  2. oil very well without tearing the dough, so we try to petrire without separating a piece of another to give elasticity to the dough, and we add the water in small quantities, we petrit, and we add the water it is heated until the dough becomes very soft, and when it is stretched it does not tear, like a shewing gum.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chakhchoukha-de-biskra-mfermsa_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chakhchoukha-de-biskra-mfermsa_thumb.jpg>)
  3. let it rest. 
  4. make meatballs the size of a tangerine (more or less depending on the size of tadjin or crepiere) 
  5. heat the pancake pan or tadjine in a slit. 
  6. put some oil in a bowl, to help spread the rogags 
  7. oil the work plan, take a ball and spread it with the palm of the hand, it is necessary to go from the center to the outside, the more the rogag sheet is fine, the more the chakhchoukh will be good 
  8. take the dough delicately and arrange it on the crepiere (this is the most difficult step of cooking, because the rogags are very delicate, you have to be professional not to break them) 
  9. work the second ball 
  10. spray the first sheet of rogags that is on the tajine with oil, and put on top of the second leaf, then turn them on the tajine, so that the second leaf thins now 
  11. oil the leaves when cooking is a very important thing. 
  12. spread a third ball, and place it on the leaves that are cooked on the tajine, and each time, turn so that the new sheet of rogags, be on the tajine to cook. 
  13. so do, with the other balls, to have a nice stack of cooked leaves, or a rechka, rechqa, as we say at home, almost 10 sheets of rogags, fold them in four and set aside. 
  14. continue cooking the remaining balls of dough. 
  15. Once the rogag sheets have cooled, put them on absorbent paper to release some of their oil.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chakhchoukha-biskria-mfermsa-charchoura-trida_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/chakhchoukha-biskria-mfermsa-charchoura-trida_thumb.jpg>)
  16. sauce preparation: 
  17. in a pot, put oil, onion and garlic, add the tomato paste, the pieces of meat, the salt and the remaining spices, let simmer a little, then add the water, 
  18. if the chickpeas are very tender, put them just at the end of the cooking, otherwise you have to put them now 
  19. if you want to put the vegetables, it's also now, like zucchini and potato. 
  20. cook well until the meat is tender. 
  21. Arrange the cut rogag leaves in a bowl and sprinkle with sauce. decorate with chickpeas and meat. Serve hot. 



[ ![Biskra chakhchoukha, Charchura Biskria trida mfermsa](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/trida-chakhchoukha-de-Biskra-charchoura-mfermsa_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/trida-chakhchoukha-de-Biskra-charchoura-mfermsa.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
