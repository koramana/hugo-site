---
title: panna cotta vanilla and mascarpone strawberry mousse
date: '2013-02-14'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/verrines-fraises_thumb.jpg
---
##  panna cotta vanilla and mascarpone strawberry mousse 

Hello everybody, 

We always like to seduce our guests or family members with a beautiful dessert such as vanilla panna cotta and mascarpone mousse strawberries, beautiful glasses that my friend Leila concocted last time. 

She liked to share with you and me through my blog, and it makes me really happy. You do not know who's Leila? it is she who made the Moorish Nutella, the [ arayeches honey with coconut ](<https://www.amourdecuisine.fr/article-arayeche-au-miel-amande-et-noix-de-coco-61897731.html>) , the [ cake with a thousand leaves ](<https://www.amourdecuisine.fr/article-gateaux-a-etage-mille-feuille-64491761.html>) and shared with us some pictures of [ her sister's engagement ](<https://www.amourdecuisine.fr/article-fete-alegrienne-en-france-64238567.html>) I say leila, but it's Lila, hihihiihi. 

So this time, it is with a nice dessert that she comes back, a marriage between the panna cotta and the mousse, then to your ingredients my dear (dear)   


**panna cotta vanilla and mascarpone strawberry mousse**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/verrines-fraises_thumb.jpg)

**Ingredients** panna cotta vanilla: 

  * 300 ml of liquid cream 
  * 160 ml of milk 
  * 1 vanilla pod 
  * 2 sheets of gelatin 

strawberry jelly: 
  * Strawberry coulis 
  * sugar 
  * 2 sheets of gelatin 

mascarpone mousse 
  * 250 gr of mascarpone 
  * 3 eggs 
  * 40 gr of sugar 
  * 2 tablespoons of grenadine (I put some strawberry coulis) 



**Realization steps**

  1. put the cream to boil, boil add gelatin previously softened in cold water and put in your verrines if you want this effect lean put your verrine in an egg box! let cool (I leave a night in the fridge) 
  2. To prepare the layer of strawberries do the same proceed when your panna cotta is taken 
  3. separate the whites from the yolks, beat the yolks with the sugar until blanching, add the mascarpone, mix the coulis then gently add the white to snow then remelange and put this preparation on the previous, then decorate 



Bonne dégustation, et a la prochaine recette. 
