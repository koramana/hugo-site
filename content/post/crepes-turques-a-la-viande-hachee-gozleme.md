---
title: Turkish crepes with minced meat, Gozleme
date: '2018-05-22'
categories:
- appetizer, tapas, appetizer
- crepes, waffles, fritters
- diverse cuisine
tags:
- Algeria
- accompaniment

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gozleme-crepes-turques-a-la-viande-hach%C3%A9e.jpg
---
[ ![Turkish crepes with minced meat, Gozleme](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gozleme-crepes-turques-a-la-viande-hach%C3%A9e.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/crepes-turques-a-la-viande-hach%C3%A9e-1-gozleme.jpg>)

##  Turkish crepes with minced meat, Gozleme 

Hello everybody, 

At home, and since I started to make Turkish pancakes, I do not stop to claim them, my children love ... But when I say my children, I must specify that everyone will choose his farce, and even the shape of his pancake, yes it's like that with children, for them, the crêpes turques is "the must" sandwich of the holidays, and I must prepare it for them at least twice a week ... 

One thing is sure the stuffed pancakes with minced meat remain their favorites, except that for one it will be without cheese. For the other it's going to be full of green olives ... and for my husband, chilli will ... 

On blogs, you'll find several pasta recipes for these pancakes, but as I'm a little too faithful to my recipes ... I prefer to stay on my mellow pizza dough recipe ... And the result for me is always super Turkish pancakes delicious ... And you what would it be your dough? 

**Turkish crepes with minced meat, Gozleme**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gozleme-crepes-turques-a-la-viande-hach%C3%A9e-2.jpg)

portions:  5  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** pasta for Turkish crepes: 

  * 3 glasses of flour 
  * 3 tablespoons of extra virgin olive oil 
  * 1 tablespoon of sugar 
  * 1 teaspoon of salt 
  * 3 tablespoons milk powder 
  * 1 tablespoon instant yeast 
  * 1 cup of baking powder. 
  * lukewarm water 

minced meat stuffing: 
  * 400 gr of minced meat 
  * ½ chopped onion 
  * 1 crushed garlic clove 
  * ¼ of a bunch of parsley 
  * 2 to 3 tablespoons of extra virgin olive oil. 
  * salt, black pepper, cumin, coriander powder 
  * Grated cheese (cheddar or cheese) 



**Realization steps**

  1. Mix all the ingredients of the dough. Try not to put the salt in front of the levire. 
  2. add lukewarm water to have a soft, smooth and easy to work dough. 
  3. Knead a few minutes. 
  4. Form the dough into a ball and let it rise away from drafts until it has doubled in volume. 
  5. form balls the size of a beautiful mandarin, I got 10. 
  6. the moment the dough rises the first time (before forming the little balls) you can start preparing the meat stuffing, fry the onion in the oil 
  7. add the garlic then the minced meat, when the meat is cooked, add the parsley and the spices 
  8. allow to reduce the sauce, until total evaporation. 
  9. For cooking you can work on a pancake pan, a griddle, or just on a stove over medium heat. 
  10. spread out each ball (giving it an oval shape, I prefer to leave the center a little thick, but spread the lathe like that when folding, we will not have a thick pasta) 
  11. place the minced meat in the center, sprinkle cheese over   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gozleme.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/gozleme.jpg>)
  12. fold the crepes like an envelope, 
  13. cook the pancakes one by one. on each side. 
  14. it is better to taste the pancakes hot ... If you have left, warm them in the oven, they will always be delicious. 



[ ![Turkish crepes with minced meat, Gozleme 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/crepes-turques-a-la-viande-hach%C3%A9e-1-gozleme.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/crepes-turques-a-la-viande-hach%C3%A9e-1-gozleme.jpg>)
