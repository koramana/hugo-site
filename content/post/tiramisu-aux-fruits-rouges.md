---
title: Tiramisu with red berries
date: '2012-05-15'
categories:
- cuisine algerienne
- cuisine marocaine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/tiramisu-aux-fruits-rouges-003.jpg
---
##  Tiramisu with red berries 

Hello everybody, 

a quick recipe  **tiramisu with red berries** , which I did this morning, for dessert. 

Because I get friends just now, so I'm really sorry for the pictures, I did not have time, to do good for this time.   


**Tiramisu with red berries** Ingredients: 250 gr of mascarpone 2 eggs 200 gr of thick cream 90 gr of sugar red fruit juice (according to your taste) a score of boudoirs. red fruits (frozen here) 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/tiramisu-aux-fruits-rouges-003.jpg)

**Ingredients**

  * 250 gr of mascarpone 
  * 2 eggs 
  * 200 gr of heavy cream 
  * 90 gr of sugar 
  * red fruit juice (according to your taste) 
  * twenty boudoirs. 
  * red fruits (frozen here) 



**Realization steps**

  1. separate the whites from the egg yolks, and whip the egg whites into snow 
  2. whip the thick cream into whipped cream. 
  3. beat the egg yolk with the sugar. 
  4. stir in the mascarpone, then gently add the whipped cream, and then the egg whites 
  5. wrap a cake mold with food film. 
  6. place a layer of red fruits 
  7. pour over a quantity of mascarpone cream 
  8. cheat the boudoirs in the fruit juice, and place on the cream 
  9. scatter a little layer of red fruit again. 
  10. cover them with a layer of cream 
  11. and cover with another layer of boudoirs deceived in juice (I was short of boudoirs for the last layer, so I use the first biscuit that falls to me at hand 
  12. leave in the fridge for at least 1 hour 
  13. turn out and serve. 



on this blog you will find: 

Tiramisu with homemade mascarpone 

####  [ Tiramisu light, special ww ](<https://www.amourdecuisine.fr/article-tiramisu-light-special-ww-64167716.html>)

####  [ Tiramisu ice cream ](<https://www.amourdecuisine.fr/article-creme-glacee-tiramisu-73695218.html>)

####  [ Chocolate Tiramisu - chocolate ganache / mascarpone ](<https://www.amourdecuisine.fr/article-tiramisu-au-chocolat-dessert-ganache-de-chocolat-mascarpone-93220347.html>)

####  [ Tiramisu with nuts and gingerbread ](<https://www.amourdecuisine.fr/article-tiramisu-aux-noix-et-au-pain-d-epice-86187387.html>)

####  [ Tiramisu with brownie ](<https://www.amourdecuisine.fr/article-tiramisu-au-brownie-104265196.html>)

bonne dégustation, et a la prochaine recette 
