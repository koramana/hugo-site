---
title: White beans in red sauce loubia
date: '2016-11-30'
categories:
- recette a la viande rouge ( halal)
tags:
- dishes
- Meat
- Algeria
- Ramadan
- Full Dish
- Healthy cuisine
- Stew

image: https://www.amourdecuisine.fr/wp-content/uploads/2008/12/haricots-blancs-a-la-sauce-rouge.jpg
---
![White beans in red sauce loubia](https://www.amourdecuisine.fr/wp-content/uploads/2008/12/haricots-blancs-a-la-sauce-rouge.jpg)

##  White beans in red sauce loubia 

Hello everybody, 

White beans in red sauce is the favorite dish at home, my husband is very difficult in the choice of his food. This dish of white beans in red sauce or Loubia bel begri is his favorite, yes he loves everything that is stews and dishes with a lot of sauce, but a nice smooth sauce. 

This dish of white beans in red sauce is just too good, a spicy sauce, well raised. Sometimes I make this recipe with chicken or lamb meat .... But with veal this is another taste .... Just perfect! 

so what are these white beans, here in England, it's the "Butter Bean" or " ![bean](https://www.amourdecuisine.fr/wp-content/uploads/2008/12/haricot_thumb1.jpg) Kidney bean ", so I do the translation word for word: it is the butter beans (hhihihihihihi it laughs that) or kidney-shaped beans (all white kidney beans are kidney-shaped, even red or speckled beans, or any other color ............ .hihihihihi). So I'm dead in translation, go the next time I buy beans I'll see their name in the box, and I'll tell you. I buy them from where? Lidl is the best of all, I bought the same Sainsbury 's (it is almost the equivalent of Auchan in France) but I prefer those Lidl. 

I thank my friend Sihem Foukroun who tested and approved my recipe of loubia, I liked these pictures so much that I replaced my old30 photos which dated from 2008 

**White beans in red sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2008/12/loubia-bel-begri-haricots-blancs-au-veau.jpg)

**Ingredients** for 3 gourmands 

  * 2 boxes of white beans otherwise beans soaked the day before 
  * 1 beautiful onion 
  * 3 cloves of garlic 
  * 1 C. tablespoons canned tomato 
  * a beautiful bouquet of coriander 
  * chicken in pieces, or meat of lamb or veal, according to taste 
  * 1 bay leaf 
  * salt, black pepper, cumin, garlic / coriander spice, paprika 
  * oil 



**Realization steps**

  1. in a pot, put the oil (2 CAS or according to your taste) put the canned tomato, chicken, coriander washed and chopped, spices and salt, the oignon and garlic passes to the robot, let the simmer a little, add after the bay leaf, and cover with water (enough for cooking the chicken, and so that it stays a little in the pot), you can also add a green pepper (my husband loves that right) 
  2. when cooking the chicken add the white beans you have rinsed (because it contains salt in the box, not to add it with its water, and to be with a nice salty sauce, hihihihihihi) 
  3. let simmer another 10 min, and put out the fire. 
  4. serve hot, with nice piece of French bread, my favorites. 



![White beans in red sauce loubia](https://www.amourdecuisine.fr/wp-content/uploads/2008/12/haricots-blancs-a-la-sauce-rouge-001.jpg)
