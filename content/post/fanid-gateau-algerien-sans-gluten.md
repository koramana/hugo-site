---
title: 'fanid: Algerian cake without gluten'
date: '2017-10-15'
categories:
- shortbread cakes, ghribiya
- oriental delicacies, oriental pastries
tags:
- Pastry
- Confectionery
- desserts
- Oriental pastries
- Algeria
- delicacies
- Almond

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-gateau-algerien-sans-gluten.CR2-001.jpg
---
[ ![fanid algerian cake without gluten](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-gateau-algerien-sans-gluten.CR2-001.jpg) ](<https://www.amourdecuisine.fr/article-fanid-gateau-algerien-sans-gluten.html/fanid-gateau-algerien-sans-gluten-cr2-001>)

##  fanid: Algerian cake without gluten 

Hello everybody, 

Finally the time to post one of the recipes of Algerian cakes that I prepared for the festival of Aid el kbir 2014. The Fanid, the Algerian cake number 1 at home, and especially for my husband, finally after makrout and the baklawa. 

To be honest, I prefer to prepare the fanid, better than the [ baklawa ](<https://www.amourdecuisine.fr/article-baklawa-constantinoise.html> "baklawa constantinoise") where the [ makrout el koucha ](<https://www.amourdecuisine.fr/article-makrout-el-koucha-en-video-gateau-de-semoule-et-dattes.html> "makrout el koucha in video / semolina cake and dates") who take a lot of preparation time. 

Remains also that the Fanid despite its ease, this Algerian cake requires preparation in advance of the almond, yes, it must begin by defatting the almond oil at least 4 days before the preparation of the cake. So I do not know if I use the right term here, but before deciding to make the fanid, we must start by reducing the fat (almond oil) by placing the almond powder in absorbent paper. (a piece of clean absorbent cloth at the time of our grandmothers) and change the paper every day, this process must be done at least 4 days before, sometimes, I completely forget the almond, and I exceed 4 days, and the result and even better, as was the case for this fanid, that I had to prepare three days before the Aid, and I found myself doing my cake the day before the Aid. 

This Algerian gluten-free cake recipe is the favorite of my husband, I often do, but I never had the chance to publish on my blog, because simply, my husband does not want decoration over! I hid these few pieces to make the decor, and make the photos, hihihih 

[ ![fanid Algerian cake without gluten 1.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-gateau-algerien-sans-gluten-1.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-fanid-gateau-algerien-sans-gluten.html/fanid-gateau-algerien-sans-gluten-1-cr2>)

**fanid: Algerian cake without gluten**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-gateau-algerien-sans-gluten.CR2_.jpg)

portions:  60  Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients**

  * 1 measure of ground almonds (650 gr) 
  * 1 measure of icing sugar (620 gr) 
  * 1 cup of vanilla sugar 
  * a few drops of almond extract 
  * egg whites as needed (4 to 5 depending on the size of the eggs) 
  * almond paste, or sugar paste for decoration 



**Realization steps** start by preparing the almonds: 

  1. measure the almonds, place them in paper towels on a plate. 
  2. every day, change the paper towel, you will see all the oil that emerges ... 
  3. repeat this for at least 4 days. 
  4. measure the sugar now, place it in the bowl to mix with the almonds, and start the robot in pulsation.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-4-300x210.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-4-300x210.jpg>)
  5. pass the mixture through the sieve. 
  6. add over vanilla and almond extract. 
  7. pick up the roof with the egg whites, go slowly to have a soft dough that picks up well, it should not be too dry, otherwise the cakes will crack after.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-5-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-5-300x200.jpg>)
  8. weigh small balls of almost 25 cm.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-6-300x196.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-6-300x196.jpg>)
  9. roll each ball into a roll of almost 20 cm long and 1 cm high   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-1.jpg>)
  10. fold the pudding in half   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-2-300x211.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-2-300x211.jpg>)
  11. and close the circle   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-3-300x200.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-3-300x200.jpg>)
  12. place as you go in a baking sheet covered with baking paper. 
  13. heat the oven to 100 degrees C, and bake the cakes for almost 35 minutes. 
  14. let cool before decorating according to your taste. 



[ ![fanid cake Algerian](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/fanid-gateau-algerien.jpg) ](<https://www.amourdecuisine.fr/article-fanid-gateau-algerien-sans-gluten.html/fanid-gateau-algerien>)
