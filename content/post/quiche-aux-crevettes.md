---
title: Shrimp quiche
date: '2017-09-10'
categories:
- appetizer, tapas, appetizer
- pizzas / quiches / pies and sandwiches
tags:
- Salty pie
- Easy cooking
- dishes
- Fast Food
- Everyday Cuisine
- inputs
- Sea food

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/quiche-aux-crevettes.CR2_.jpg
---
[ ![shrimp quiche](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/quiche-aux-crevettes.CR2_.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40268>)

##  Shrimp quiche 

Hello everybody, 

I love quiches, and at home I'm the only one who likes it. My little trick, whenever I have time, I prepare funds of savory pies, I cook, and in the freezer. The day I'm taken and no time to cook at noon, I make a quick omelette stuffed for my husband (stuffed by means of board) and for me it's going to be a salty pie or a quiche. 

This time, I enjoyed a shrimp quiche scented with cumin, ginger and a touch of curry, a delight, a recipe very easy to do, especially if like me, you prepare the dough in advance. What I like about quiches is that you can put everything you like in it, so your quiches never have the same taste. 

**Shrimp quiche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/quiche-aux-crevettes-1.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** for the pasta: 

  * 250 gr of flour 
  * 120 gr of butter in pieces 
  * 1 egg yolk 
  * ½ cup of sugar 
  * 1 teaspoon dried parsley 
  * ¼ cup of cumin 
  * ½ teaspoon of salt 
  * 50 ml of cold water (+ or - as needed) 

for the stuffing: 
  * 200 gr of frozen shrimps 
  * 4 eggs 
  * 40 cl of fresh cream 
  * 1 C. parsley 
  * ½ cup of curry 
  * 1/4 cup of ginger coffee 
  * ¼ cup of cumin 
  * salt pepper 
  * 20 gr of gruyere grated 



**Realization steps** prepare the dough: 

  1. in the bowl of the blinder, put the flour and butter in pieces, mix 
  2. add the egg yolk, sugar, salt, parsley and cumin mix again 
  3. add the water in small quantities and mix, until the dough forms a ball (go there gently with the water) 
  4. cover the ball with food film, and leave in the fridge for 30 minutes 
  5. Spread out the dough, and go for a quiche pan covered with baking paper, prick with a fork, or individual molds. 
  6. Cook the dough for 10 to 12 minutes 

for the stuffing: 
  1. fry the shrimp in a little oil, to cook well according to your taste. 
  2. In a salad bowl, mix eggs, crème fraîche, parsley, grated cheese and spices. Add salt and pepper. 
  3. cover the shrimp pie shell, pour over the egg mixture 
  4. Bake for almost 30 to 40 minutes. 



[ ![shrimp quiche](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/quiche-aux-crevettes-3.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40269>)
