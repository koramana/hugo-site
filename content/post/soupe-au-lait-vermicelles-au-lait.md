---
title: milk soup / milk vermicelli
date: '2012-12-15'
categories:
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/vermicelles-au-lait-soupe-au-lait.CR2_2.jpg
---
##  milk soup / milk vermicelli 

Hello everybody, 

Here is a recipe very very easy, very fast and very good that I prepare for me and for children, when I'm sick (like today, with these nausea due to the flu), or if I'm too lazy to cook. 

the milk soup, or the vermicelli with milk, and my irreplaceable recipe, in these sitguards, or even when I do not know what to cook, and that I have nothing at home ... .. 

and at home we really like milk vermicelli, so sometimes when I say: I do not know what I have to cook, well the children are quick to claim this dish very fast, and calle well ...   


**milk soup / milk vermicelli**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/vermicelles-au-lait-soupe-au-lait.CR2_2.jpg)

**Ingredients**

  * 200 gr of vermicelli 
  * 1 onion 
  * 1 tablespoon of butter 
  * salt and black pepper 
  * ½ liter of milk 
  * ½ liter of water 
  * 3 cheese in portions 



**Realization steps**

  1. grated onions 
  2. in a saucepan, melt the butter, and add the onion. 
  3. leave until the onion becomes translucent. 
  4. add water, salt and black pepper. 
  5. at the first boiling, add the vermicelli, and cook over medium heat. 
  6. add milk and cheese, cook 2 to 3 minutes or until pasta is very tender. 
  7. remove from the fire, and serve immediately. 



**حساء اللبن / شعرية بالحليب**

**غ 200 الشعيرية  
1 بصلة   
1 ملعقة كبيرة زبدة   
الملح والفلفل الأسود   
لتر ** **1/2** **من الحليب**

**لتر** **1/2** **من الماء**

**ثلاثة قطع جبن**

طريقة التحظير: 

**قطعي او افرمي البصل  
في مقلاة, تذوب الزبدة ويضاف البصل.   
يترك حتى يصبح البصل شفافا.   
يضاف الماء والملح والفلفل الأسود.   
عند بداية غليان الماء, اضيفي الشعرية و اطبخيها لمدة 10 دقائق على نار متوسطة الحرارة.   
أضيفي الحليب والجبن وطهي 2 إلى دقائق أو حتى تصبح الشعرية طرية.   
يرفع عن النار و تقدم على الفور. **
