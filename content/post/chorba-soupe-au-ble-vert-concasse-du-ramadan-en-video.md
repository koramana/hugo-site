---
title: Chorba / crushed green wheat soup from Ramadan in video
date: '2013-07-20'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-2-006.CR2_1.jpg
---
![chorba-frik-2-006.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-2-006.CR2_1.jpg)

##  Chorba / crushed green wheat soup from Ramadan in video 

Hello everybody, 

At the request of many of my readers (I specify readers and non-readers) "Naser", "Foued. M "," Hamid "," Aboudi ".... and still others who would prefer to remain anonymous, I realized this video of the chorba frik, a recipe typical of Eastern Algeria .... 

I hope that my tests of videos please you well, do not forget to make a love on the video. 

recipe here: 

[ Chorba Frik ](<https://www.amourdecuisine.fr/article-25345575.html>)

{{< youtube uTV1SC6P1lw >}} 

I tell you Sahha fturkoum, and have a nice day.   


**Chorba / crushed green wheat soup from Ramadan in video**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/chorba-frik-2-006.CR2_1.jpg)

portions:  4  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * chicken or meat 
  * 1 onion 
  * oil 
  * 1 and ½ cases of canned tomato 
  * 1 handful of chickpeas soaked the day before 
  * 1 bunch of coriander 
  * 1 small celery stalk 
  * 1 glass of wheat water crushed green (frik) 
  * salt, coriander powder, black pepper 



**Realization steps**

  1. cut the meat into cubes or the chicken into pieces, 
  2. place the meat in a pot, add the oil and chopped onion, chopped coriander, 
  3. add the celery stalk, salt, black pepper, and coriander powder, 
  4. let it simmer a little over low heat, the meat will drop from its water, and the onion will become very translucent 
  5. add the canned tomato diluted in water, let it return a few more minutes, and add the water, until well cover the meat 
  6. half-cooking the meat pour the green wheat crushed in the broth, 
  7. to cook and towards the end, also add the chickpeas in box, 
  8. let simmer for a few minutes, and remove the pot from the heat 
  9. at the end of cooking, sprinkle with chopped coriander and serve hot. 


