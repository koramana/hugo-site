---
title: Seffa special khotba-couscous with coconut
date: '2016-03-13'
categories:
- couscous
- Algerian cuisine
- Healthy cuisine
- recipes of feculents
tags:
- Oriental cuisine
- Algeria
- dishes
- Algerian dish
- Ramadan 2016
- Healthy cuisine
- Sweet couscous

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/seffa-special-khotba.jpg
---
![seffa special khotba](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/seffa-special-khotba.jpg)

##  Seffa special khotba-couscous with coconut 

Hello everybody, 

I then forgot the recipe I had not kept on my pc, and here is Mina redid it to share with us on my blog, this time here, I think now I have no excuse to to make this sublime Seffa which is prepared for special occasions in some parts of western Algeria, and especially during the engagement (khotba). 

**Seffa special khotba-couscous with coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Seffa-special-khotba-couscous-%C3%A0-la-noix-de-coco.jpg)

portions:  4  Prep time:  30 mins  cooking:  60 mins  total:  1 hour 30 mins 

**Ingredients**

  * 1 kg of couscous 
  * 150 g of coconut 
  * 250 g of soft butter 
  * 4 tablespoons of oil 
  * 1 teaspoon of salt 
  * a little cinnamon 
  * 100 g icing sugar 
  * 200 g slivered almonds, lightly toasted (or walnuts, or otherwise steamed grapes) 
  * a little honey green 
  * a box of pineapple 



**Realization steps**

  1. Fill the kettle with couscous and bring to a boil. 
  2. Place the couscous and the coconut in a large dish, work them to separate the grains, by wetting with ½ glass of water and 4 tablespoons of oil, 
  3. add a good pinch of salt. 
  4. place this mixture in the top of the couscoussier and let it steam. 
  5. As soon as steam escapes from the couscoussier, let the mixture cook for 20 minutes. Then put the couscous back in the dish, work with 2 glasses of cold water. 
  6. Let stand until the water is well absorbed, then cook the steam again for 20 minutes. 
  7. work again with a ½ glass of water. 
  8. cook a third time for 20 minutes,   
I cook with steam 4 to 5 times so that the couscous is very well cooked, it remains a choice and the seffa melts in the mouth 
  9. Finally, add the butter, the impalpable sugar (icing sugar). 
  10. Mix well until butter melts. 
  11. Put the seffa pyramid. Decorate with cinnamon and put at the top of the pyramid the glass of honey, cinnamon, and nuts, 
  12. decorate according to your taste   
good tasting 



[ ![Seffa special khotba-couscous with coconut 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Seffa-special-khotba-couscous-%C3%A0-la-noix-de-coco-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/Seffa-special-khotba-couscous-%C3%A0-la-noix-de-coco-1.jpg>)
