---
title: Mid-custard cake with mascarpone and strawberry
date: '2011-05-13'
categories:
- boissons jus et cocktail sans alcool
- Cuisine saine

---
& Nbsp; I bought a box of light mascarpone, to make me a delicious tiramisu, but I had forgotten in the fridge, I come to see the box this morning, and .... no .......... it will be expired tomorrow, and me who loves mascarpone, I could never throw it, so come the turn of a little imagination, and improvisation, I have a box of strawberry. and I make this cake half-flan, which was super delicious, so here are the ingredients: 3 eggs 120 grs of sugar 250 gr of mascarpone 50 gr of melted butter 100 ml of milk 150 grs of strawberry 1 sachet & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

I bought a box of light mascarpone, to make me a delicious tiramisu, but I had forgotten in the fridge, I come to see the box this morning, and .... no………. it will be expired tomorrow, and me who loves mascarpone, I could never throw it, so come the turn of a little imagination, and improvisation, I have a box of strawberry. 

and I make this cake half-flan, which was super delicious, so here are the ingredients: 

  * 3 eggs 
  * 120 grams of sugar 
  * 250 gr of mascarpone 
  * 50 gr of melted butter 
  * 100 ml of milk 
  * 150 grs of strawberry 
  * 1 packet of baker's yeast 
  * 270 grams of flour. 



in a bowl, beat the eggs and sugar well, add the mascarpone in a small amount while stirring constantly. 

then add the milk, and the melted butter. 

gently incorporate the yeast and flour, and then add the cut strawberries into pieces. 

Pour the mixture into a butter mold or silicone like me. 

cook the cake, in a preheat oven at 180 degrees. 

bonne dégustation 
