---
title: Mdefouna style burger pie
date: '2018-03-18'
categories:
- Unclassified
tags:
- inputs
- Bread
- Boulange
- Bakery
- Stuffed bread
- Ramadan
- accompaniment

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/03/tourte-burger-fa%C3%A7on-madfouna-4.jpg
---
##  ![Madfouna style 4 burger pie](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/tourte-burger-fa%C3%A7on-madfouna-4.jpg)

##  Mdefouna style burger pie 

Hello everybody, 

I love making these recipes of stuffed breads that are both a bread and a diner, hihihih just a good salad, and dinner is ready, beautiful recipes 2 in 1 like this pie burger way medfouna. 

The medfouna is actually a way to cook the meat stewed in a kind of pie, and usually in a special stone oven. 

I really wanted to make the recipe as it should, by making a raw stuffing, ie meat and vegetables without cooking, because in the original recipe, we only return the onion, then we add the pieces of meat, of course we chose the tenderest meat of the lamb, and we season. But as I know it will not be to the taste of my husband and my children, and also because of my oven, I was afraid to finish cooking the pie without the meat is cooked, I I preferred to return the pieces of meat too. 

But surely one day, I'll take the challenge, and try the recipe exactly as it should! if not for the monment, I leave you with my burger pie mdefouna way, and I wait for you to encourage me on my chain youtube! 

**Mdefouna style burger pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/tourte-burger-fa%C3%A7on-madfouna-6.jpg)

**Ingredients** for the bread: 

  * 350 gr of fine semolina 
  * 1 tablespoon of sugar 
  * 1 C. baking yeast 
  * 1 C. tablespoon milk powder 
  * 1 C. salt 
  * warm water to pick up the dough (between 200 and 250 ml, or even more depending on the quality of the dough) 

for the stuffing: 
  * 200 gr of tender meat 
  * 1 medium onion 
  * ½ bunch of chopped parsley 
  * 1 chopped carrot 
  * 1 clove garlic minced 
  * salt, black pepper, ginger, cumin, ras el hanout 
  * olive oil 
  * 2 hard boiled eggs 
  * grated cheese (optional) 



**Realization steps**

  1. start by making the dough by placing all the ingredients in the blinder bowl. 
  2. add the water gradually while kneading, until having a soft dough, and a little sticky 
  3. cover the dough, and let it rise away from drafts 

prepare the stuffing: 
  1. fry the onion until it becomes translucent. 
  2. add the meat, grated carrot, spices and chopped garlic. 
  3. when everything is cooked, add parsley and remove from heat. 

preparation of the pie, 
  1. take the dough and degas it in a space sprinkled with semolina. 
  2. divide the dough into two balls and spread each finely until you have a pancake of almost 30 cm, you have to spread it on a space well sprinkled with semolina and work on baking paper because the dough is very sticky and like that the transfer will be much easier. 
  3. fill the first galette with the stuffing, add over the quarter-cut hard-boiled eggs and grated cheese. 
  4. cover the stuffing with the second cake, stick well to enclose the stuffing. 
  5. adjust the patty well and brush the top with the egg and milk mixture. 
  6. decorate with a little sesame seeds and grains of nigella. 
  7. bake in an oven preheated to 180 ° C between 20 and 25 minutes, or until the burger is well cooked. 



![burger pie madfouna way 5](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/tourte-burger-fa%C3%A7on-madfouna-5.jpg)
