---
title: Chicken paella
date: '2017-07-03'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
tags:
- Spain
- Rice
- accompaniment
- Sea food
- dishes
- Full Dish
- shrimps

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/paella-au-poulet-1.jpg
---
[ ![chicken paella 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/paella-au-poulet-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/paella-au-poulet-1.jpg>)

##  Chicken paella 

Hello everybody, 

Here is a dish, which is not often eaten at home, or so, if I do, there must always be one less ingredient. The chicken breast, a Spanish recipe that I was lucky enough to learn from my beautiful mother who is a real Spanish ... Of course, the day she made her big paella pan, I do not tell you all the ingredients that she could put in it. My beautiful mother god had her soul had made seafood and langoustines and mussels were very fresh, I ate that day paella, as I have never eaten in my life. 

For my chicken paella, I used frozen langoustines that I bought in Iceland (a large quantity of 20 pieces not even 9lbs) especially when I know that I will only use a few pieces, and the rest will be for a next time ... to be honest, even frozen, these langoustines were too good. 

**Chicken paella**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/paella-au-poulet.jpg)

portions:  4  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients**

  * 4 chicken legs 
  * 1 onion 
  * 3 fresh tomatoes otherwise tomatoes in box 
  * 1 red pepper 
  * 1 nice handful of frozen peas 
  * 200 gr of mussels 
  * olive oil 
  * 2 glasses of paella special rice 
  * 6 to 7 langoustines 
  * 1 generous pinch of saffron pistils 
  * salt 
  * pepper 
  * ¼ bunch of coriander 
  * 4 glasses of water 



**Realization steps**

  1. Slice the onions finely. 
  2. Slice the red peppers into strips. 
  3. Crush peeled and peeled tomatoes in small cubes 
  4. In a paella dish, sweat the onions with olive oil. These must become translucent. 
  5. Add the red peppers, Sweat a few more minutes. 
  6. Take some strips of red pepper for garnish. 
  7. add the chicken pieces and leave to brown well 
  8. add the crushed tomato, and let it return on a low heat while covering, so that the chicken arrives at half cooking. 
  9. Add the rice to the pan, mix to pear the rice in the fat at the bottom of the paella dish. 
  10. add the 4 glasses of water, salt, black pepper, chopped coriander, saffron, and peas. 
  11. add the mussels and cover with a lid or aluminum foil. 
  12. when the water is almost completely absorbed, remove the langoustines 
  13. Cover with the aluminum again, and cook until the langoustines turn pink, the rice is well cooked and the water is completely absorbed 
  14. Decorate with the strips of red peppers and enjoy ... 



[ ![paella](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/paella.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/10/paella.jpg>)
