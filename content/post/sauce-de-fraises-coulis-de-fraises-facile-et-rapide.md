---
title: quick and easy strawberry / strawberry sauce
date: '2013-05-18'
categories:
- Algerian cuisine
- Cuisine by country

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pic-101_thumb1.jpg
---
##  quick and easy strawberry / strawberry sauce 

Hello everybody, 

When you buy a lot of strawberries for its low price, expect a lot to rot. And I do not like to throw food, especially not the beautiful strawberries, judge for yourself the beautiful picture above, it's so good strawberries is sweet, we can eat them all raw without even feeling acidity. 

For the part that was starting to ripen too much, I opted for a strawberry sauce that I could use in any cake filling, custard or cream after, as in the strawberry milk rice recipe   


**quick and easy strawberry / strawberry sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/pic-101_thumb1.jpg)

Prep time:  5 mins  cooking:  2 mins  total:  7 mins 

**Ingredients**

  * 2 cups of strawberries reduced in purrée 
  * 3 cups of sugar 
  * 1 packet of pectin   
(I put a teaspoon of Arrow root) (This is the starch originally extracted from the maranta root and reduced to a fine white powder that is used as a thickening agent, in the same way as the starch corn or potato.The narrow-rooted thickens soups, sauces, puddings, creams and puddings, it also serves as a binder in some pastries in which one does not put wheat flour). 
  * 2 tablespoon of lemon juice. 
  * 1/2 cup of water 



**Realization steps**

  1. pl all the ingredients in a heavy-bottomed saucepan. 
  2. Boil, and count 2 minutes at the beginning of boiling 
  3. Let stand for about 3 minutes, then pour in jars. 



![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/0061.jpg)
