---
title: homemade marshmallow recipe without eggs, marshmallow
date: '2017-12-15'
categories:
- dessert, crumbles and bars
- basic pastry recipes
- sweet recipes
tags:
- Marshmallow
- Christmas
- Gourmet gift
- delicacies
- candy
- Confectionery
- Delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/12/recette-de-chamallow-ou-guimauve-maison-sans-oeufs-1-654x1024.jpg
---
![homemade marshmallow recipe without eggs, marshmallow](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/recette-de-chamallow-ou-guimauve-maison-sans-oeufs-1-654x1024.jpg)

##  homemade marshmallow recipe without eggs, marshmallow 

Hello everybody, 

it tells you these small candies well flavored and super melting in the mouth, the recipe homemade marshmallow without eggs, also known as marshmallow or marshmallow, there is even who wrote it with "s" it's to say Shamallow. 

My kids really like marshmallows, especially since I can not easily find halal marshmallows, so I make them homemade, and what I like about the homemade marshmallow recipe is that it can be flavored and colorful as one wants, in addition, this recipe is without eggs .. Yes chamallos without eggs. 

For more tips to succeed your homemade marshmallows without eggs, I advise you to see the video, I give you more details on: 

**home-made chamallow recipe without eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/recette-de-chamallow-ou-guimauve-maison.jpg)

**Ingredients**

  * 2 tbsp. powdered gelatin 
  * 125 ml of cold water to swell the gelatin. 
  * For the syrup: 
  * 2 glasses of sugar (250 ml glass) 
  * 1 glass and a half of water (180 ml of water) 
  * ½ glass of glucose syrup (I use a cup of 125 ml for this measurement) 
  * cook the mixture until it reaches 117 ° C 

to color and flavor the marshmallows: 
  * food yellow dye 
  * aroma of pineapple 



**Realization steps**

  1. place in the bowl of your kneader, or a large salad bowl 125 ml of cold water 
  2. add the powdered gelatin and let it swell well. 
  3. in a heavy-bottomed saucepan, place the sugar, water and glucose syrup, bring to a boil and cook until the syrup reaches 116 to 117 ° C. 
  4. when you are at the right temperature, turn on the kneader and add the hot syrup on the gelatin net. 
  5. then whisk at high speed, until the mixture is completely chilled and whitened. The foam must be very thick 
  6. while whisking, add the coloring and aroma of your choice. 
  7. when the dye and aroma are well introduced, stop the robot, remove your mixture, and pour it into a mold lined with cling film, and generously covered with a mixture of icing sugar and cornstarch (for the walls, oil a little so that the sugar and glue) 
  8. sprinkle the top of the marshmallow paste with the icing sugar, and let dry between 6 and 8 o'clock, it will depend on the height of your marshmallow paste. 
  9. remove the marshmallow dough from the mold, cover generously with icing sugar to be able to cut the pieces of marshmallow. 
  10. coat the marshmallow pieces in the sugar, pat to remove excess sugar. 
  11. keep the marshmallow pieces in a tightly sealed box or in freezer packs. 



![homemade marshmallow or marshmallow recipe 2](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/recette-de-chamallow-ou-guimauve-maison-2.jpg)
