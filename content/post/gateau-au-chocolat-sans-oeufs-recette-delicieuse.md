---
title: chocolate cake without eggs, delicious recipe
date: '2013-01-17'
categories:
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-au-chocolat-sans-oeuf-010_thumb1.jpg
---
![chocolate cake without egg 010](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-au-chocolat-sans-oeuf-010_thumb1.jpg)

##  chocolate cake without eggs, delicious recipe 

Hello everybody, 

**chocolate cake without eggs, delicious recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-au-chocolat-sans-oeuf-004_thumb1.jpg)

portions:  8  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients** for the cake 

  * 2 jars of plain yoghurt (the jars will be used as a measure) 
  * 1 and a half jars of sugar 
  * 3 pots of flour 
  * ½ pot of oil 
  * ½ sachet of baking powder 
  * 200 g melted chocolate 

decoration: 
  * 150 gr of white chocolate 
  * 2 tablespoons fresh cream 
  * Chocolate in vermicelli 



**Realization steps**

  1. Melt dark chocolate in the bain-marie 
  2. Mix yogurt and sugar, add flour and yeast 
  3. Stir in the oil and chocolate slowly to obtain a smooth paste 
  4. Pour into a baking pan lined with baking paper. 
  5. Bake for 30 to 35 minutes in a 180 ° oven. 
  6. Test the cooking with the tip of a knife planted in the middle of the cake, which must come out dry. 
  7. Let cool a little, then unmold. 
  8. Melt the white chocolate on the bain-marie, when it's melted, add the cream and mix. 
  9. Decorate the cake well chilled with this cream of white chocolate. and scatter over the chocolate in vermicelli 



![chocolate cake without eggs](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateau-au-chocolat-sans-oeufs_thumb1.jpg)
