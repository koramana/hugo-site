---
title: Pineapple gaspacho
date: '2012-02-22'
categories:
- gateaux algeriens sans Cuisson
- gateaux algeriens- orientales- modernes- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/02/gaspacho-dananas-1.jpg
---
[ ![pineapple gaspacho 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/gaspacho-dananas-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/02/gaspacho-dananas-1.jpg>)

##  Pineapple gaspacho 

Hello everybody, 

A refreshing drink, Simplissime and a lot of originality at once, very easy, and within reach of everyone, is prepared in a short time, just think to have ice cubes in the freezer. 

you can also see this delicious recipe from [ pina colada ](<https://www.amourdecuisine.fr/article-pina-colada-boisson-fraiche-sans-alcool.html> "pina colada: cold drink without alcohol") if you like the marriage of both flavors: pineapple and coconut. 

**Pineapple gaspacho**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/gaspacho-dananas-2.jpg)

portions:  4  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 1 can of pineapple in its juice, 
  * ice cubes, 
  * 100 ml of water 
  * 100 ml pineapple juice (from the box), 
  * fresh mint, 
  * sugar according to your taste. 



**Realization steps**

  1. To prepare 2 hours minimum before consuming and to keep in the refrigerator. 
  2. take half of the pineapple 
  3. Put it in a blender with ice cubes, sugar, water and pineapple juice 
  4. Mix until you get a liquid consistency (no more pieces). 
  5. Pour into a jar 
  6. add the remaining half of the pineapple 
  7. Chop the fresh mint (keep for the final decoration) and add to the rest of the preparation. 
  8. Mix everything well. 
  9. Store in a refrigerator for at least 2 hours. 
  10. When serving, fill with verrines and decorate with mint. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
