---
title: shrimp donut recipe
date: '2017-12-04'
categories:
- appetizer, tapas, appetizer
- idea, party recipe, aperitif aperitif
- ramadan recipe
- fish and seafood recipes
- salty recipes
tags:
- inputs
- Aperitif
- Amuse bouche
- aperitif
- accompaniment
- Ramadan
- Cocktail dinner

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/12/beignets-de-crevettes-1-750x1024.jpg
---
![shrimp donut recipe](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/beignets-de-crevettes-1-750x1024.jpg)

##  shrimp donut recipe 

Hello everybody, 

Here is a recipe for shrimp donuts easy and fast that I prepare for lunch when I have no ideas but especially when I have precooked shrimp at home, hihihih. 

When I go to Lidl, I always buy a packet of pre-cooked shrimps, not only is the box very beautiful, with prawns arranged in a circle and a sauce ready to immerse these beautiful shrimps in and enjoy. To be honest, I never eat this sauce, but there I kept the sauce for the photo, hihihihih. 

So what I do, I always have this package in the freezer, and when I do not know what to do for children, I remove the package from the freezer, I prepare the little dough donuts and here we have a delight to enjoy with a good salad, a good mashed potatoes, or with the rest of the dinner yesterday. It must also be said that it is the recipe "save the day" when we have guests unexpectedly and we want to impress them, hihihih 

Lynda also to prepare this recipe in video for voila, so I share it with you: 

**shrimp donut recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/12/beignets-de-crevettes-2.jpg)

**Ingredients** for 24 shrimps 

  * 100 gr of flour 
  * 50 gr of cornflour 
  * 1 C. with baking powder 
  * 150 ml of water 
  * ½ c. salt 
  * black pepper 
  * chili powder (or Espelette pepper) optional when children are at home I do not put 
  * 1 C. oil soup 
  * oil for frying 



**Realization steps**

  1. defrost the shrimp in advance and let it drain 
  2. in a large salad bowl, mix flour, cornflour and baking powder 
  3. then add salt, black pepper, chili powder, or spices of your taste 
  4. add the water while mixing, until having a liquid device but which is good to well coat the shrimp. 
  5. add the oil mix again, and let stand the time that the oil is heating in a deep deep-bottomed pan. 
  6. shell the shrimp and leave only the tail. 
  7. take the shrimp from the tail and dip it into the flour apparatus, and directly into the pan 
  8. cook the shrimp fritters on both sides until you have a nice color. 
  9. drain on paper towels and enjoy warm. 



Note If you have a quantity left, for a later tasting, preheat the shrimp donuts in a hot oven, the donuts will take the crispy, as if they were freshly prepared. 
