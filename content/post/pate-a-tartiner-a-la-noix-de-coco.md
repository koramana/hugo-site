---
title: spread with coconut
date: '2013-03-08'
categories:
- appetizer, tapas, appetizer
- pizzas / quiches / pies and sandwiches
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pate-a-tartiner-a-la-noix-de-coco-1_thumb1.jpg
---
![spread with coconut 1](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pate-a-tartiner-a-la-noix-de-coco-1_thumb1.jpg)

##  spread with coconut 

Hello everybody, 

A recipe for coconut spread, very easy and very rich in taste, that I forgot in the archives of my pc (and ohhhhhhhhhhh how many recipes in the queue ....). 

This recipe Lunetoiles had passed to me when she had prepared the [ Nutella pancakes ](<https://www.amourdecuisine.fr/article-pate-a-crepe-au-nutella-crepe-au-chocolat-facile-et-rapide-115021705.html>) , and it has superbly accompany these delicious pancakes. You should know that knowing Lunetoiles I was expecting to see a recipe like that, she likes coconut a lot, and she likes to make coconut delights, like the [ muddle with coconut ](<https://www.amourdecuisine.fr/article-mkhabez-a-la-noix-de-coco.html>) , [ brownies with coconut ](<https://www.amourdecuisine.fr/article-brownies-cookies-les-brookies.html>) , [ basboussa with coconut ](<https://www.amourdecuisine.fr/article-basboussa-fourree-a-la-noix-de-coco.html>) or the [ homemade raffaello ](<https://www.amourdecuisine.fr/article-raffaello-fait-maison-recette-facile.html>) .   


**spread with coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/pate-a-tartiner-a-la-noix-de-coco_thumb.jpg)

**Ingredients**

  * 250 ml of coconut milk 
  * 100 g of sugar 
  * 50 g grated coconut 



**Realization steps**

  1. Put all the ingredients in a saucepan. 
  2. Heat and bring to a boil. 
  3. Reduce heat, and cook for 5 to 10 minutes (or until thickened), stirring often. 
  4. Potted and keep cool. 


