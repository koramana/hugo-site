---
title: Clafoutis with silky tofu
date: '2014-12-23'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/clafoutis11.jpg
---
Clafoutis with silky tofu Hello everyone, Another delicious recipe that comes to me from Lunetoiles, this recipe is based on silky tofu: Clafoutis silky tofu, which is a low odor white paste derived from the soy bean. It is produced from soy milk that is curdled and pressed to remove water. An important component of Asian diets, tofu comes in many forms (dried tofu, smoked tofu, leafy or blocky, hairy tofu, herbal tofu ...). Mainly because of its high protein content, it is & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.65  (  1  ratings)  0 

![clafoutis1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/clafoutis11.jpg)

##  Clafoutis with silky tofu 

Hello everybody, 

Another delicious recipe that comes from Lunetoiles, this recipe is based on silky tofu: **Clafoutis with silky tofu** , which is a low odor white paste derived from the soybean. 

It is produced from soy milk that is curdled and pressed to remove water. An important component of Asian diets, tofu comes in many forms (dried tofu, smoked tofu, leafy or blocky, hairy tofu, herbal tofu ...). 

Mainly because of its high protein content, it is very popular today in Western countries including **vegetarians** as a substitute for meat or fish. 

the silky tofu as its name suggests, its texture is silky, resembling that of yogurt. It is sold in can at room temperature (refrigerate after opening) or refrigerated in a plastic package. Use it for desserts (such as "smoothies"), dips or in soups. 

so we go to the recipe:   


**Clafoutis with silky tofu**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/clafoutis2.jpg)

portions:  6  Prep time:  5 mins  cooking:  30 mins  total:  35 mins 

**Ingredients**

  * 400 g of pitted cherries (I put frozen red fruits) 
  * 3 eggs 
  * 200 g silky tofu 
  * 50 g cornflour 
  * 150 ml of milk 
  * 100 gr of whole sugar 
  * 3 tbsp. tablespoon almond powder 



**Realization steps**

  1. In a bowl mix the cornflour with the sugar with a whisk. 
  2. Add the eggs and mix well. Gradually pour the milk while continuing to mix and the silky tofu. Until you get a dough without lumps. Add the almond powder. Mix well. 
  3. Arrange the cherries (or red berries) in 6 ramekins and pour over the mixture. 
  4. Put in the oven for 30 minutes at 180 ° C (th.6). 
  5. Cool and place in a cool place. Serve well fresh. 
  6. Serve with cream muslin for decoration. And a cherry on top. 



thank you to Lunetoiles for this delicious recipe, thank you for your passage 

[ apple clafoutis ](<https://www.amourdecuisine.fr/article-clafoutis-aux-pommes-69733808.html>)

[ pear clafoutis ](<https://www.amourdecuisine.fr/article-clafoutis-aux-poires-98865118.html>)

[ Cherry clafoutis ](<https://www.amourdecuisine.fr/article-clafoutis-aux-cerises-facile-et-rapide.html> "quick and easy cherry clafoutis")

bonne journee 
