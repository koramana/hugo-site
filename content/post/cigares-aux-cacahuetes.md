---
title: Peanut Cigars
date: '2015-04-14'
categories:
- gateaux algeriens au miel
- gateaux algeriens frit
tags:
- Algeria
- Aid
- Oriental pastry
- Algerian cakes
- Ramadan 2015
- Ramadan
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cigares-aux-cacahuetes-1.jpg
---
[ ![cigars with peanuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cigares-aux-cacahuetes-1.jpg) ](<https://www.amourdecuisine.fr/article-cigares-aux-cacahuetes.html/cigares-aux-cacahuetes-1>)

##  Peanut Cigars 

Hello everybody, 

A dessert omni present in the month of Ramadan, but we should not wait this month only to taste these cigars peanuts. This recipe is for one of my readers, Exotic Flower. Ella made these peanut cigars with [ homemade dyouls ](<https://www.amourdecuisine.fr/article-dyouls-ou-feuilles-de-brick-fait-maison.html> "dyouls or homemade Brick leaves") that exotic flower to make from my blog. 

The recipe is super simple, and you can substitute peanuts with any other ingredient of your choice.   


**Peanut Cigars**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cigares-aux-cacahuetes-2.jpg)

portions:  10  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * [ dyoul leaves ](<https://www.amourdecuisine.fr/article-dyouls-ou-feuilles-de-brick-fait-maison.html> "dyouls or homemade Brick leaves") as required 
  * 3 measures of peanuts 
  * 1 measure of crystallized sugar 
  * 1 pinch of cinnamon 
  * rose water 
  * oil for frying 
  * honey for decoration 



**Realization steps**

  1. prepare the peanut stuffing by mixing all the ingredients, 
  2. water gently with the rose water to have a paste that picks up.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/ingredients-cigares-aux-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41501>)
  3. Take a sheet of brick, put a stuffing pudding in it. 
  4. shape the cigars, you can stain them with egg white.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cigares-aux-cacahuetes.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41500>)
  5. fry in a hot oil bath 
  6. At the end of the frying, immerse the cigars in honey. 
  7. Enjoy with a good mint tea. 


