---
title: maamoul
date: '2012-02-29'
categories:
- Algerian cuisine
- recette de ramadan

---
Hello everyone, 

a nicely carved oriental mignardise with the help of a special mold, the maamoul mold, a recipe very close to the delicious Algerian cake [ the makrout ](<https://www.amourdecuisine.fr/article-26001222.html>) but with the touch of yeast, and the flour that is added to the semolina, this delicious cake takes on a new dimension. 

_ ingredients  _

  * _ 2 glasses of fine semolina (a glass of 250ml)  _
  * _ 1 and a half flour  _
  * _ 340 ml of melted butter  _
  * _ 1/2 teaspoon of salt  _
  * _ half a glass of icing sugar  _
  * _ a C. coffee baking powder  _
  * _ water to pick up the dough.  _



_ for the farce  _

  * _ 250 gr of the date paw  _
  * _ 2 tbsp. cinnamon powder  _
  * _ 3 c. soup of melted butter  _
  * _ 6 c. soup of flower water  _


  1. _ in a salad bowl, mix the semolina and the flour  _
  2. _ add salt, sugar and baking powder and mix well  _
  3. _ add the melted butter and sand the dough.  _
  4. _ let stand a few hours.  _
  5. _ water the mixture with the water while gently moistening  _
  6. _ pick up the mixture into a pile of dough that you will cover and let stand.  _



_ meanwhile prepare your stuffing:  _

  1. _ knead the date paste after cleaning it with any pips, with the other ingredients  _
  2. _ divide it into small meatballs and reserve  _


  1. _ now take the semolina paste again, and divide it into balls too (if possible the same number of date balls)  _
  2. _ flatten a dumpling and stuff it with a ball of dates and close the semolina pasta.  _
  3. _ continue until the dough is exhausted.  _
  4. _ now put each stuffed ball in the mold with maamoul lightly floured,  _
  5. _exert a small force to mark the decoration of the mold on your dough._
  6. _ transfer the resulting pieces to a baking tray  _
  7. _ cook in a preheated oven at 180 degrees until the maamoul is lightly browned  _
  8. _ let cool and sprinkle with icing sugar  _



_ so thank you very much for your visits and your comments, do not worry if I do not answer immediately to the comments, because I am more than overwhelmed in this period. but I try to do my best to answer your questions, and your comments.  _

_ thank you for being always faithful.  _

_ thanks to those who continue to subscribe to the newsletter (article publication notification (important if you still want to have an alert when publishing a new article), and newsletter)  _

_ thank you  _
