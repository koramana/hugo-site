---
title: stuffed cabbage with chopped meat
date: '2014-07-26'
categories:
- Algerian cuisine
- houriyat el matbakh- fatafeat tv
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/choux-farcis-au-four-houriat-el-matbakh-cuisine-algerie1.jpg
---
[ ![cabbage stuffed in oven-houriat el matbakh- Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/choux-farcis-au-four-houriat-el-matbakh-cuisine-algerie1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/choux-farcis-au-four-houriat-el-matbakh-cuisine-algerienn.jpg>)

##  stuffed cabbage with chopped meat 

Hello everybody, 

a delicious dish of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , and some [ world cuisine ](<https://www.amourdecuisine.fr/categorie-12359467.html>) , the **cabbage stuffed with minced meat in the oven** , a very tasty and very rich dish. 

before, I cooked this dish in the manner of my mother, that is to say, cook the cabbage with salt water, detach the leaves and put the stuffing, I saw this method on [ Fatafeat TV ](<https://www.amourdecuisine.fr/categorie-11131465.html>) , by [ houriyat el matbakh ](<https://www.amourdecuisine.fr/categorie-11131465.html>) , or it was frying cabbage after stuffing, an idea that I liked, and I love to try, so this step is optional, but in my opinion, she has to add a more flat, a flavor that I really like. 

**Stuffed cabbage with baked meat**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/choux-farcis-au-four-cuisine-algerienne_thumb.jpg)

Recipe type:  dish  portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * for stuffed cabbage 
  * a medium-sized cabbage (or according to family members) 
  * 350 gr of minced meat 
  * 2 cloves garlic 
  * parsley 
  * salt and black pepper 
  * nutmeg 
  * for frying: 
  * 2 eggs 
  * salt and black pepper 
  * oil bath 
  * for the sauce: 
  * some pieces of meat 
  * 1 onion 
  * 1 clove of garlic 
  * 1 handful of chickpeas (boxed for me) 
  * salt and black pepper 
  * parsley (knotted parsley stems) 
  * nutmeg, coriander powder 



**Realization steps**

  1. in a pot full of boiling salted water, put the head of cabbage to which you will remove the hard heart, during 15 to 20 minutes. 
  2. then remove the cabbage, let drain a little, then remove the leaves gently, without damaging them. 
  3. prepare your stuffing, mixing the minced meat with a little parsley, salt of black pepper and a little nutmeg (if you think that the stuffing is not enough, you can add a little precooked rice. 
  4. place a little stuffing of minced meat in each cabbage leaf, roll it, and continue until you have used the stuffing. 
  5. dip the stuffed cabbage into the beaten egg mixture, drain well to get rid of excess egg, then fry in hot oil from all sides. 
  6. drain on paper towels 
  7. In the meantime prepare the white sauce, chop the onion, and sauté in a little oil until it becomes translucent, add the meat, cover and let it sweat a little. 
  8. add salt, spices, parsley and cover with a little water 
  9. if the chickpeas are soaked only add them at this stage, if it is canned as is the case for me, add them when the meat is tender. 
  10. place the stuffed cabbage in a baking tin, sprinkle with a little sauce, garnish with the chickpeas and pieces of meat 
  11. cover with aluminum foil, and cook in a preheated oven for almost 20 minutes. 
  12. the last minutes, remove the aluminum foil, to give a little color to the dish. 
  13. at the presentation, you can sprinkle with a little remaining sauce, according to taste, and garnish with a little chopped parsley 



[ ![cabbage stuffed with minced meat, Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/choux-farcis-a-la-viande-hachee-cuisine-algerienne_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/04/choux-farcis-a-la-viande-hachee-cuisine-algerienne_2.jpg>)

Enjoy your meal. 

I invite you to see on this blog the recipes of: 

[ stuffed tomatoes ](<https://www.amourdecuisine.fr/article-tomates-farcies-nids-en-tomates-aux-oeufs-101484090.html>)

[ stuffed mushrooms ](<https://www.amourdecuisine.fr/article-champignons-farcis-aux-epinards-et-artichauts-102371354.html>)

[ stuffed olives ](<https://www.amourdecuisine.fr/article-brochettes-d-olives-farcies-102948239.html>)

[ Stuffed artichokes ](<https://www.amourdecuisine.fr/article-tajine-d-artichauts-farcis-97187541.html>)

[ stuffed cardoons ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee-94791294.html>)

and [ dolma, legumes farcis ](<https://www.amourdecuisine.fr/article-dolma-legumes-farcis-a-la-viande-hachee-au-four-66035809.html>)
