---
title: tajine with chicken and olives dumplings / ramadan dish 2013
date: '2013-11-07'
categories:
- Algerian cuisine
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tajine-de-boulettes-de-poulet-aux-olives-007.CR2_2.jpg
---
![tajine-of-meatballs-of-chicken-with-olive-007.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tajine-de-boulettes-de-poulet-aux-olives-007.CR2_2.jpg)

##  tajine with chicken and olives dumplings / ramadan dish 2013 

Hello everybody, 

tagine with chicken meatballs and olives / dish of ramadan 2013: A super delicious dish that will fill well your daily tables, or then the table of ramadan because for me it is a very delicious [ ramadan recipe ](<https://www.amourdecuisine.fr/article-menu-ramadan-plats-chorbas-et-entrees-55268353.html>) , 

this **tajine with chicken balls and olives** is a dish that I personally enjoy, and that I realize at the first opportunity, knowing that I personally prefer the meatballs to chicken, meatballs, because they are always tender, and tasty.   


**tajine with chicken and olives dumplings / ramadan dish 2013**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tajine-zitoune-bel-jaj-009.CR2-copie-11.jpg)

Recipe type:  dish, Algerian cuisine  portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * chicken pieces 
  * the breast of a chopped chicken 
  * 3 tablespoons of breadcrumbs 
  * some sprigs of chopped parsley 
  * 1 onion 
  * 1 egg yolk 
  * 300 gr of pitted olives 
  * 1 box of mushrooms 
  * salt and black pepper 
  * some saffron filaments 
  * olive oil, butter, or vegetable oil 



**Realization steps**

  1. in a large pot, put the piece of chicken, finely chopped onion with a little oil, and fry 
  2. add salt, saffron filaments and black pepper. 
  3. when the onion becomes translucent, add the water, to cover the chicken pieces well, and cook. 
  4. in another pot, boil the olives in a little water with a slice of lemon, to desalt them. 
  5. In a large salad bowl, place chopped chicken breast, chopped parsley, just a little salt, a little black pepper, bread crumbs and egg yolk. 
  6. mix everything together, and form dumplings. 
  7. change the water of olives 2 or three times if possible 
  8. now that the chicken is well cooked, add the chicken balls delicately in the sauce, let it cook a little. 
  9. then add the olives, and after the mushrooms into pieces. 
  10. cook another 10 minutes and it's ready. 
  11. serve hot after adding chopped parsley. 



![tajine aux olives-and-the-balls-of-chicken-11.CR-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tajine-aux-olives-et-aux-boulettes-de-poulet-11.CR-copie-11.jpg)
