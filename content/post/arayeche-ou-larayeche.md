---
title: Arayeche or larayeche
date: '2012-04-13'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/arayeche-gateaux-algeriens-aid-el-fitr-2014.CR2-001.jpg
---
#  [ ![arayeche algerian cakes help el fitr 2014.CR2-001](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/arayeche-gateaux-algeriens-aid-el-fitr-2014.CR2-001.jpg) ](<https://www.amourdecuisine.fr/article-arayeche-ou-larayeche.html/arayeche-gateaux-algeriens-aid-el-fitr-2014-cr2-001>)

##  Arayeche or larayeche 

Hello everybody, 

the **Arayechs** , A delicious [ Algerian cake ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) traditional that I like very much, these little sweets so beautiful, this culinary heritage of our grandmothers, a cake that we devoured with love in the **holidays** and small **receptions** , very good, well melting with a marriage of taste, between the **almond** that these Arayeches hide under a paste **half sandy** , coated with a layer of **sugar icing** all full of lemon flavor. 

you can see another model: [ arayeches with honey ](<https://www.amourdecuisine.fr/article-arayeche-au-miel-arayeche-aux-amandes-et-noix-de-coco-103366182.html>) . 

[ Algerian cakes ](<https://www.amourdecuisine.fr/categorie-10678931.html>)

[ Algerian cakes index ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)   


**Arayeche or larayeche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/arayeche-gateaux-algeriens-aid-el-fitr-2014.CR2-001.jpg)

portions:  30-35  Prep time:  30 mins  cooking:  20 mins  total:  50 mins 

**Ingredients** For the dough: 

  * 3 measures of flour 
  * 1 measure of cooled melted butter 
  * Vanilla extract 
  * 1 pinch of salt 
  * Orange tree Flower water. 

For the stuffing: 
  * 3 measures of ground almonds 
  * 1 measure of powdered sugar 
  * 1 good c to c cinnamon powder 
  * Orange tree Flower water 

For frosting; 
  * 2 egg whites (more or less depending on the amount of cakes) 
  * 2 tablespoons lemon juice 
  * 2 tablespoons of orange blossom water or rose water 
  * Icing sugar 
  * Food coloring of your choice if you want to color them 



**Realization steps**

  1. Prepare the dough with the given ingredients, pick up with orange blossom water and let stand a few minutes. 
  2. In the meantime, prepare the stuffing by mixing all the ingredients. 
  3. prepare balls of 30 gr around, and lower each ball to have a small circle, in which you will drop stuffing in the shape of a "Y" as shown in this picture.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/3rayeche_thumb1.jpg)
  4. Then fold over all 3 sides, put the cake on the palm of your hand and with the other hand, give it the shape of a star with curved angles, as if it floated in the water (a beautiful starfish ).   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/3rayeche-001_thumb1.jpg)
  5. Bake at 180 ° C and allow to cool completely before icing. 
  6. Prepare the icing with the ingredients listed above, it should not be runny or too thick, always test on a cake and grind by adding icing sugar until the desired texture is obtained.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/s1-001_thumb1.jpg)



with the same stuffing, the same dough and the same ice-cream, you can prepare [ bracelets ](<https://www.amourdecuisine.fr/article-40397484.html>) , 

there are many more [ Algerian cake ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) can be decorated with this glaze such as [ Mkhabez ](<https://www.amourdecuisine.fr/article-25345466.html>)

bonne degustation 
