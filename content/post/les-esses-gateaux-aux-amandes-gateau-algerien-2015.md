---
title: Esses, cakes with almonds, Algerian cake 2015
date: '2013-05-02'
categories:
- Chocolate cake
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateaux-aux-amandes-les-esses_thumb1.jpg
---
#  Esses, cakes with almonds, Algerian cake 2015 

Hello everybody, 

here is a delicious almond cake of Moroccan origin normally, because I saw it on a Moroccan site, a long time ago, except that I did not keep the recipe, but rather the form. 

it's too beautiful, too easy to achieve. 

I had made this beautiful and delicious cake in the month of August, and it is only now that I mail you the recipe at the request of one of my readers, I had completely forgotten.    


**Esses, almond cakes, Algerian cake 2013**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateaux-aux-amandes-les-esses_thumb1.jpg)

portions:  50  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients** for the pasta: 

  * 250 gr of flour 
  * 75 gr of margarine 
  * 1 pinch of salt 
  * 1 teaspoon of vanilla 
  * 1 teaspoon of white dye 
  * 1 cup of dye (color of your choice, here yellow) 
  * Orange blossom water + water 

the joke: 
  * 250 gr of ground almonds 
  * 125 gr of sugar 
  * 1 C. a caramel aroma coffee 
  * 1 egg 

garnish: 
  * honey 
  * shiny food 
  * silver beads 



**Realization steps**

  1. method of preparation: 
  2. Pour the flour in a bowl, add the margarine, vanilla, salt. 
  3. Mix well with your fingertips. 
  4. add the yellow dye, then pick up the dough with flower water, add a little water to pick up, continue to knead a few seconds. 
  5. cover your dough and let it rest on the side. 
  6. for stuffing, mix almonds and dry ingredients, collect with egg and set aside. 
  7. spread the dough on a floured surface lightly 
  8. cut a long rectangle 15 cm wide (the length is not important because you will cut afterwards) 
  9. shape a 13 cm stuffing pudding and flatten it regularly to have a nice rectangle, 13 cm (just a little smaller than the dough) on 1.5 cm, and not higher than 7 mm (because cooking is going inflate a little). 
  10. fold the dough over the almond pudding as you see on the picture above, and cut with a roulette. 
  11. bend the ends of the rectangle, to have a uniform "S" esse.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateaux-aux-amandes-les-Esses_2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateaux-aux-amandes-les-Esses_2.jpg>)
  12. place your cakes on a baking tray. 
  13. as always with these cakes, let them dry overnight, so that the cooking will be short, and the color of the cake does not change. if you cook the cake right away, it will take longer, and you will lose the color of the dough). 
  14. cook in a preheated oven at 160 degrees for almost 10 minutes 
  15. dip the hot esses, at the end of the oven, in lukewarm honey, perfumed with orange blossom. 
  16. present in boxes. 
  17. you can keep this cake in a hermetic box between 15 days and a month. but they must be put back in honey before presenting them. 



{{< youtube 7xI4eyscws8 >}} 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/visitecomm.png)
