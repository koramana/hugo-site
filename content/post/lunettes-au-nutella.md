---
title: Nutella glasses
date: '2012-04-21'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateaux-secs-lunettes-au-nutella_thumb1.jpg
---
##  Nutella glasses 

Hello everybody, 

Here is another recipe from [ Nutella shortbread ](<https://www.amourdecuisine.fr/article-sables-au-nutella-103621066.html>) : glasses with nutella, [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) that Lunetoiles liked to share with us. 

What I like in the recipes of [ shortbread ](<https://www.amourdecuisine.fr/categorie-12344749.html>) is that we are not limited to a single recipe or a single garnish and decoration, moreover if you click on the top category shortbread recipes, you will discover a beautiful variety of this fondant delight.   


**Nutella glasses**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateaux-secs-lunettes-au-nutella_thumb1.jpg)

**Ingredients**

  * 250 gr softened butter 
  * 2 egg yolks 
  * 100 gr of icing sugar 
  * 100 gr of cornflour 
  * 1 sachet of baking powder 
  * 1 teaspoon of vanilla extract 
  * Flour 

Fodder: 
  * Nutella 



**Realization steps**

  1. In a container, work margarine well with sugar, and vanilla extract. 
  2. Add the eggs, cornflour and yeast, 
  3. then pick up the mixture with the sifted flour until you obtain a malleable paste. 
  4. On a floured work surface, roll out the dough, 0.5 cm thick using a roller, 
  5. then cut to a short cut with shortbread. 
  6. Arrange them on a plate, 
  7. bake them in the oven, preheat them at 180 ° C for 15 to 25 minutes, 
  8. remove them and allow them to cool. 
  9. Garnish a piece not pierced with Nutella and cover with another pierced piece, then decorate by sprinkling with icing sugar. 



![cookies](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gateaux-secs-sables-fourres-au-nutella_thumb_11.jpg)

the [ gateaux algeriens ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>)
