---
title: apple crumble recipe
date: '2015-02-02'
categories:
- dessert, crumbles and bars
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/crumble-aux-pommes-020.CR2_1.jpg
---
[ ![apple crumble recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/crumble-aux-pommes-020.CR2_1.jpg) ](<https://www.amourdecuisine.fr/article-crumble-aux-pommes.html>)

##  Apple crumble 

Hello everybody, 

I really like the recipes that contain cinnamon apples, like the recipe for stuffing a delicious [ apple pie ](<https://www.amourdecuisine.fr/article-apple-pie-dautomne-au-caramel-au-beurre-sale.html> "Apple pie with salted butter caramel") [ For Apple Pie ](<https://www.amourdecuisine.fr/article-apple-pie-dautomne-au-caramel-au-beurre-sale.html> "Apple pie with salted butter caramel") . But I also like it when this cinnamon apple mix is ​​garnished with a crisp and crunchy layer of crumble. 

And despite having a home we like a lot **the crumble** , Nevertheless **apple crumble** is the one we love the most and that my children claim most often. apple crumble is delicious, especially this must-have flavor of cinnamon-flavored apples. The most important thing is that the apples keep the shape and sometimes the crunch while melting in the mouth after cooking, hum a pleasure with each spoon. 

What is this **Apple crumble** and **the most famous crumble** here in England. The funniest thing in history is that crumble was created during World War II, when poverty began to show up in homes, and there were not enough ingredients to make pies. or pies, such as flour, butter or sugar, English women have thought to cover their fruit with this thin layer of crumbs "crumble in English" and here is the birth of a delicious dessert easy to achieve, and especially with few ingredients. 

**Apple crumble**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/crumble-aux-pommes-009.CR2_1.jpg)

portions:  4  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 6 apples 
  * ½ teaspoon cinnamon 
  * the juice of 1/2 lemon 
  * a little sugar, depending on the flavor of the apples. 
  * 2 tablespoons flour 

for the crumble: 
  * 100 gr of flour 
  * 2 tablespoons brown sugar 
  * 4 tablespoons of melted butter 
  * vanilla extract 
  * a pinch of salt 



**Realization steps**

  1. Wash and peel apples, cut into medium cubes. 
  2. place in a bowl, add cinnamon, lemon juice, sugar and flour, leave aside 
  3. prepare the crumble, mixing all the ingredients with a fork. 
  4. generously butter the crumble mussels. 
  5. fill with the apple mixture 
  6. garnish the apples with crumble.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/crumble-aux-pommes-002.CR2_1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/crumble-aux-pommes-002.CR2_1.jpg>)
  7. cook in a preheated oven at 180 degrees C until the crumble turns a nice color. 
  8. enjoy with a scoop of ice cream, or a little whipped cream. 



[ ![apple crumble recipe](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/crumble-aux-pommes-006.CR2_1.jpg) ](<https://www.amourdecuisine.fr/article-crumble-aux-pommes.html>)

vous pouvez voir aussi: 
