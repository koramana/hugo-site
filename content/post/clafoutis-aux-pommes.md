---
title: Apple clafoutis
date: '2013-01-06'
categories:
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/clafouti-au-pommes-11.jpg
---
Hello everybody, 

So this clafoutis, huuuuuuuuuuuuuum a delight, the children have to love, and it made me happy, at least I will not eat it all alone. 

  
So here are the ingredients:    
(I made half of the ingredients) 

  * 4 apples (994 gr) 
  * 80 gr of butter 
  * 12 cl whole liquid cream 
  * 12 cl of whole milk 
  * 4 eggs 
  * 80 gr of powdered sugar 
  * 80 gr of cornflour 
  * 1 teaspoon cinnamon 



**For the topping:**

**apple jelly, or jam**

****![clafouti-to-apples-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/clafouti-au-pommes-11.jpg) ** **

In a saucepan pour the milk and add the butter in pieces. 

Melt over low heat and add the cream. Mix and let cool. 

In a bowl whisk the whole eggs with the sugar. Add starch and cinnamon. 

Mix and add the previous preparation while continuing to mix. 

Book. 

Preheat your thermostat oven 6 (180 ° C) up and down position. 

Separate apples into 4, remove skin and seeds. 

Cut the quarters into thin slices ... 

... and place them on a non-stick plate (or in a pie plate). 

Before pouring the preparation give a boost in the mixture because the starch is deposited at the bottom. 

Bake the dish for 45 minutes. 

Take out the clafoutis from the oven and brush the topping on the apples, it will give them a nice shine. 

thank you for your comments and visits 

bonne journee 
