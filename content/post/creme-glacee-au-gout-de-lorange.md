---
title: ice cream to the taste of the orange
date: '2011-07-19'
categories:
- les essais de mes lecteurs

---
a heat that melts, but we say "hamdoullah" because, it comes from the good god, and in england, we came back from a very small ray of sunshine, the children have a good time, and above all they take good advantage of doing the vitamin D reserve 

me by this time, I told myself a homemade ice cream, will make everyone happy. 

especially that I have a little glass of orange juice, and a half box of nestle. 

so my ingredients were simple, and especially without sorbitiere, I left mine in England. 

\- 300 ml of fresh cream whipped cream 

\- 50 gr of sugar (you can add 10 gr, if you like the sweetest taste) 

\- 150 ml of concentrated sweet milk (you can decrease the sugar, if you want to add more milk) 

\- 200 ml pure orange juice, (unsweetened) 

start by adding the fresh cream in whipped cream, add a little sugar 

place in the freezer for 5 minutes 

whip the milk and orange juice well, put again in the freezer for 5 min 

remove the two mixtures, and knead them gently, without breaking the fresh cream too much. 

return to the freezer for 15 minutes, remove and mix again, and return to the fridge, repeat the action 2 to 3 more times, until the mixture is very fresh and melting at the same time. 

garnish to your taste, and serve well fresh 

thank you for your comments, and thank you to subscribe to the newsletter if you like my blog. 
