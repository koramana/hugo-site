---
title: Lentil soup
date: '2017-06-10'
categories:
- soups and velvets
tags:
- Algeria
- Ramadan
- Ramadan 2017
- dishes
- Vegetables
- Fast Food
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/soupes-de-lentilles-1-724x1024.jpg
---
![soups of lentils 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/soupes-de-lentilles-1-724x1024.jpg)

##  Lentil soup 

Hello everybody, 

Finally a little time, to post you a recipe that I prepared during the month of Ramadan. In any case, today I have a little bit of time, and as it is an easy recipe to write, and also easy to prepare, I mail you. 

In any case, it's like me in the month of Ramadan, you only touch soup and bouraks, surely that over time, you will find that you have exhausted your iron reserves, hihihihi, we do not want especially to have anemia, and everyone knows, that lentils like spinach, are the remedies that are available to everyone, to renew their resources. 

So here are some easy ingredients, and above all a result that is too good: 

a real delight this well rich soup 

![lentil soups 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/soupes-de-lentilles-2.jpg)

**Lentil soup**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/soupes-de-lentilles-2.jpg)

**Ingredients**

  * 3 c. soup of olive oil 
  * 2 medium onions cut into pieces 
  * 3 cloves of garlic 
  * 1 carrot 
  * 1 small potato 
  * 1 C. rice soup 
  * 1 water glass of red lentils (I had green lentils) 
  * 1 cube of chicken broth (I put a cube flavor spices and olive oil that my sister-in-law had brought me from France) 
  * 1 liter and a half of water 
  * salt and black pepper 
  * 1 cup of cumin 
  * the juice of half a lemon 



**Realization steps**

  1. in a pot, sauté crushed garlic and onion in oil. 
  2. add the chopped vegetables and lentils, salt and black pepper 
  3. stir with a wooden spoon for 5 minutes over low heat. 
  4. add rice, knorr cube, and water 
  5. cook on low heat for 40 minutes. 
  6. at the end of cooking, pass the mixture to the mixer. 
  7. before serving, pour over a drizzle of lemon juice with cumin and enjoy 



![lentil soups](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/soupes-de-lentilles.jpg)

Merci pour vos visites. 
