---
title: Royal entremet with peanuts
date: '2016-07-24'
categories:
- bavarois, mousses, charlottes, recette l'agar agar
- recettes sucrees
tags:
- Pastry
- desserts
- Birthday cake
- Genoese
- Cakes
- Bavarian

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/Entremet-Royal-aux-cacahu%C3%A8tes-1.jpg
---
![Royal entremet with peanuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/Entremet-Royal-aux-cacahu%C3%A8tes-1.jpg)

##  Royal entremet with peanuts 

Hello everybody, 

If I would tell you since when this recipe is on the archives of my email, and since when I had signed the photos of **Lunetoiles** a first time, (I had to hand the new signature yesterday) you will be surprised, since July 2013 ... Yes, yes it will be 3 years in a few days since Lunetoiles share with me. 

I was like now on vacation in Algeria, and I started to sign the photos to put them online, but with the very bad connection, I have tried several times in vein, and with the time I have unlocked and forgotten the recipe completely. Then during this Ramadan, I found the recipe, I signed the photos again, and I put them on my blog, and once again so taken, I forgot to put the recipe online. 

But now with a new connection when I have the time, hihihihi I wanted to put an old recipe, and here I found this delicious Peanut Royal Entremet waiting to see the day. 

Super, everything was well done and arranged, it was only the introduction, to publish this recipe that will surely please you, and that I want to prepare very quickly, because it is a super easy recipe with ingredients to everyone's reach, except maybe for people who will not easily find lace pancakes, usually I replace with chocolate cereals and the result is just super greedy. 

![Royal entremet with peanuts 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/Entremet-Royal-aux-cacahu%C3%A8tes-3.jpg)

**Royal entremet with peanuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/Entremet-Royal-aux-cacahu%C3%A8tes-4.jpg)

**Ingredients** Dacquoise with peanuts: 

  * 3 egg whites 
  * 125 gr of caster sugar 
  * 160 gr of peanuts 

Light chocolate cream: 
  * 250g of milk 
  * 250 g of liquid cream 
  * 60g caster sugar 
  * 3 egg yolks 
  * 200 g chocolate 72% cocoa 
  * 450 g whipped cream 
  * 3 sheets of gelatin 

Crispy praline: 
  * 180 g praline to peanuts 
  * 100 g dark chocolate 64% 
  * 170 g glitter feuilletine (pancakes lace gavotte crumbled) 

Chocolate icing: 
  * 240 gr of caster sugar 
  * 65 grams of glucose syrup 
  * 100 gr of water 
  * 175 gr of liquid cream 
  * 90 gr unsweetened cocoa powder (Van Houten) 
  * 16 g of gelatin (4 leaves) 
  * 300 g of water (to swell the gelatin) 



**Realization steps** Dacquoise with peanuts: 

  1. Preheat oven to 200 ° C 
  2. Roast the peanuts for 10 minutes while watching, they should not be too brown. 
  3. Let cool, then reduce to powder 120 peanuts and keep 40 g whole. 
  4. Mount the egg whites in firm snow with the caster sugar. 
  5. Then add the peanut powder by gently mixing with a spatula. 
  6. Prepare a mold of 26 cm diameter buttered on the edges and lined with parchment paper. 
  7. Garnish the mold with the preparation. Sprinkle with remaining whole peanuts previously broken in half or chunks, and sprinkle with a little sugar.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/dacquoise.jpg)
  8. Bake for 35 to 40 minutes at 180 ° C by turning the pan halfway through cooking. 
  9. It must be crisp and both tender inside. 
  10. When the dacquoise is cooked, let it cool completely on a rack before using it. 

Light chocolate cream: 
  1. Soak the gelatin sheets in cold water. 
  2. Put the milk and liquid cream to a boil. 
  3. Mix the yolks and sugar until the mixture whitens. 
  4. Pour the boiling mixture over the mixed yolks and sugar, then pour over the chocolate and mix well and add the well-drained gelatin.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/creme-legere-au-chocolat.jpg)
  5. Wait until the mixture cools and gently fold in the cream. 

Crispy praline: 
  1. Melt the melted chocolate with the peanut praline and mix. 
  2. Add the glitter foil to the previous mixture.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/croustillant-pralin%C3%A9.jpg)

Montage: 
  1. In a 26 cm hinge pan, place the dacquoise with peanuts in the bottom. 
  2. Spread the crisp praline over the dacquoise evenly and smooth with the back of a spoon. 
  3. Pour in the light chocolate cream and put the pan in the fridge for several hours (for me all night).   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/montage.jpg)

Chocolate icing: 
  1. Soak the gelatin in cold water with crushed ice or ice cubes (this is to prevent the gelatin dissolves in water). 
  2. Pass the cocoa powder through the sieve 
  3. In a saucepan mix the water, sugar, glucose and cream and bring to a temperature of 103 ° C. 
  4. Add the cocoa powder and mix well with a whisk. 
  5. Let cool to reach a temperature of 60 ° C and add the dewatered gelatin. 
  6. Mix with a spatula, sieve if necessary, let stand and use at a temperature of about 20 to 25 ° C. 
  7. Decoration and icing of the dessert: 
  8. The next day, take out your mold from the refrigerator. 
  9. Arrange and place the dessert on a rack, place the rack on a plate to recover the excess frosting. 
  10. Pour icing on the entremet evenly to cover the edges as well.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/glacage.jpg)
  11. Renew this operation, to have a nice uniform glaze. 
  12. Decorate around the edges with roasted peanuts and caramelized whole peanuts and chill, to freeze frosting. 



![Royal entremet with peanuts](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/Entremet-Royal-aux-cacahu%C3%A8tes.jpg)
