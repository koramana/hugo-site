---
title: Chocolate cake mug
date: '2015-11-27'
categories:
- Cupcakes, macaroons, and other pastries
- Chocolate cake
- sweet recipes
- sweet verrines
tags:
- Cakes
- Algerian cakes
- To taste
- Soft
- desserts
- Easy recipe
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mug-cake-cookies-au-chocolat.jpg
---
[ ![mug cake, chocolate cookies](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mug-cake-cookies-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mug-cake-cookies-au-chocolat.jpg>)

##  Chocolate cake mug 

Hello everybody, 

When your daughter comes home from school, and claims the chocolate cookies that you promised to give her, but you forgot, because you made another cake to taste it !!! Do you send it for a walk, or do you make a mug cake with chocolate cookies? 

The mug cakes or cakes in cup have become a must at home, when sometimes one of the children just wants a little delicacy at home he is the only one to love, well mug cakes are well the solution ... You just have to find the right recipe, the right dosage of sugar ... and the good cup ... my daughter loves this pretty cup in flower all feminine, that I do not have to use for my son (besides I come from break his ... pffff I have to buy him another one before he asks me for his next cake mug, hihihihih) 

**Chocolate cake mug**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mugcake-cookie-au-chocolat.jpg)

portions:  1  Prep time:  2 mins  cooking:  1 min  total:  3 mins 

**Ingredients**

  * 1 tablespoon of butter 
  * ½ tablespoon of white sugar 
  * ½ tablespoon of brown sugar (I do not like too much sugar, the original recipe use 1 tablespoon for each sugar, in my opinion it was a lot all the same) 
  * a few drops of vanilla extract 
  * 1 egg yolk 
  * ¼ of c. tablespoon baking powder 
  * 3 tablespoons flour 
  * 2 cup chocolate chips (I did not have any, I used dark chocolate in pieces) 



**Realization steps**

  1. Melt the butter in the microwave, just to melt, not to boil 
  2. add the two sugar and the vanilla extract, mix well (we do everything in the same cup) 
  3. add the egg yolk, you have to introduce it to the mixture 
  4. then add the flour and the baking powder, mix well, then the pieces of chocolate 
  5. cook in the microwave for 40 seconds no more, and enjoy the tasting ... 



[ ![mugcake dark chocolate cookies](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mugcake-cookies-au-chocolat-noir.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/mugcake-cookies-au-chocolat-noir.jpg>)
