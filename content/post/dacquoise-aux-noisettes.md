---
title: Dacquoise with hazelnuts
date: '2014-12-28'
categories:
- cakes and cakes
- basic pastry recipes
- sweet recipes
tags:
- Dessert
- Pastry
- desserts
- Cakes
- Based
- Algerian cakes
- Confectionery

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dacquoise-aux-noisettes-1.jpg
---
[ ![dacquoise with hazelnuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dacquoise-aux-noisettes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dacquoise-aux-noisettes-1.jpg>)

##  Dacquoise with hazelnuts 

Hello everybody, 

Dacquoise is a basic biscuit for many pastry recipes and the result is a soft fondant cake, and full of crunch at the same time. You will find several recipes on my blog that are based on a dacquoise like: [ Russian ](<https://www.amourdecuisine.fr/article-le-gateau-russe-methode-simplifiee-chez-les-patissiers-algeriens.html> "The Russian cake simplified method among Algerian pastry cooks") , [ strawberry pie with almonds ](<https://www.amourdecuisine.fr/article-tarte-a-la-fraise-sur-dacquoise-aux-amandes-et-citron.html> "strawberry pie on dacquoise with almonds and lemon") , this [ dacquoise with meringue butter cream ](<https://www.amourdecuisine.fr/article-dacquoise-a-la-creme-au-beurre-meringuee-suisse.html> "dacquoise with Swiss meringue butter cream.") , the [ highball ](<https://www.amourdecuisine.fr/article-recette-trianon-recette-royal-au-chocolat.html> "trianon recipe / royal chocolate recipe") or this [ chocolate dessert ](<https://www.amourdecuisine.fr/article-entremet-au-chocolat-au-lait-de-pierre-herme.html> "Pierre Hermé milk chocolate dessert") . 

This time, it's the recipe for the hazelnut dacquoise shared with us Claudine cook, who has already shared with us her [ homemade praline paste ](<https://www.amourdecuisine.fr/article-pate-de-praline-maison.html> "homemade praline paste") , and this is what Claudine cook tells us: 

__ Hello Soulef, I come back to your blog to share this recipe for Dacquoise, and the recipe is also coming from your blog. I adopted this recipe, and I realize it as a base in many of my recipes, because it is delicious and unbreakable. So thank you Soulef for your recipes. Claudine Cook 

the recipe in Arabic: 

**Dacquoise with hazelnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dacquoise-aux-noisettes.jpg)

portions:  12  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients** for the base of a log of 25 cm by 10 cm 

  * 2 egg whites 
  * 50 g of sugar 
  * 50 g ground almonds 
  * 50 g of [ hazelnut praline ](<https://www.amourdecuisine.fr/article-pralin-aux-noisettes.html> "praline with hazelnuts")
  * 7.5 g of flour 



**Realization steps**

  1. Beat the egg whites until stiff and meringue with the caster sugar. 
  2. Mix with almond powder, hazelnut paste and flour, using a spatula.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Dacquoise-aux-noisettes-3-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Dacquoise-aux-noisettes-3-001.jpg>)
  3. Pour the preparation into a piping bag   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Dacquoise-aux-noisettes-4-002.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Dacquoise-aux-noisettes-4-002.jpg>)
  4. and make a rectangle about 1 cm thick, from the gutter mold dimension.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dacquoise-aux-noisettes-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dacquoise-aux-noisettes-4.jpg>)
  5. Bake in a gentle oven (150 °, thermostat ¾) for about 30 minutes. 
  6. Remove from the oven and let cool. 



[ ![dacquoise with hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dacquoise-aux-noisettes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/dacquoise-aux-noisettes.jpg>)
