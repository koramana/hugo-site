---
title: Cocoa cake from Kaouther
date: '2010-11-02'
categories:
- birthday cake, party, celebrations
- Algerian dry cakes, petits fours
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/cake-choc7_thumb1.jpg
---
##  Cocoa cake from Kaouther 

This cocoa cake is a delight, especially if you like the taste of cocoa in the cake, me anyway, I love ... and generally I like to make cakes and cocoa cake, as chocolate cakes ... 

here is the recipe (copy / paste)    


**Cocoa cake from Kaouther**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/cake-choc7_thumb1.jpg)

portions:  8  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 100g of butter 
  * 100g of sugar 
  * 3 tablespoons Ethiquable cocoa powder (cocoa vanhouten) 
  * 200ml of lukewarm milk 
  * 2 eggs 
  * 250g of flour with yeast incorporated (flour + a packet of baking powder) 



**Realization steps**

  1. Preheat your oven to 180 ° C. 
  2. In a bowl, mix the sugar with the softened butter. Add, the eggs one by one. Add the small flour, small, stirring well (I used the electric bater) Mix in a bowl the cocoa with the warm milk. Pour the cocoa mixture with the other device. 
  3. Stir everything until a homogeneous consistency. 
  4. Butter a cake tin, sprinkle the bottom with sugar and bake until the blade of a knife comes out dry. 



bonne Degustation. 
