---
title: 'Bread recipe: very easy'
date: '2013-12-29'
categories:
- cakes and cakes
tags:
- Aperitif
- Bouchees
- Basil

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gressins-au-basilic_thumb.jpg
---
[ ![homemade breadsticks](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gressins-au-basilic_thumb.jpg) ](<https://www.amourdecuisine.fr/categorie-10678929.html>)

Hello everybody, 

Here is a recipe **express** of **[ appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) ** , without waiting time or rest, ingredients at your fingertips, for a result bleffant, which will appeal to palates greedy. 

For other similar recipes, you can visit the [ categorie aperitifs and appetizers ](<https://www.amourdecuisine.fr/categorie-10678929.html>) (I'm still working on arranging my blog categories ...)    


**Bread recipe: very easy**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gressins-au-parmesan_thumb.jpg)

Recipe type:  homemade breadsticks, easy recipe  portions:  13  Prep time:  10 mins  cooking:  15 mins  total:  25 mins 

**Ingredients**

  * 80 g flour 
  * 25 g grated Parmesan cheese 
  * 25 g diced soft butter 
  * 30 ml of cold water 
  * 20 g finely chopped basil 
  * The zest of 1 finely grated lemon 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. In a bowl, perfume Parmesan cheese with zest of finely grated lemon by mixing these two ingredients. 
  3. add the flour, and the finely chopped basil to this mixture. 
  4. stir in the butter and knead with your fingertips. 
  5. Add the water and work by hand until you can form a ball of dough. 
  6. Divide this ball into 13 balls (12 to 15 g each). 
  7. Roll each snake meatball about 18 cm long, which you will then put on a baking sheet covered with parchment paper. 
  8. bake at 180 ° C for about 15 minutes. Breadsticks should be lightly browned. 
  9. Let cool on rack. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
