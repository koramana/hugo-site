---
title: Pear Almond Tart
date: '2014-01-03'
categories:
- jams and spreads

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/tarte-amandine-aux-poires-1_thumb.jpg
---
##  Pear Almond Tart 

Hello everybody, 

I share with you today this beautiful pie pie, it is an amandine pie, but with an extra mellow, the texture of an almond custard, or a semolina cake with almonds, so that the cream amandine contains fresh cream, yumiiiiiiiiiii, I really like this texture, must say that at midnight that I type you this recipe, I really want to enter my screen to take a last. 

this recipe is my dear Lunetoiles who shares it with us, yes, if I did it, I do not think I would have had time to take a picture of it. 

So, I stop salivating, and I pass you the recipe: 

**Pear Almond Tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/tarte-amandine-aux-poires-1_thumb.jpg)

Recipe type:  pie dessert  portions:  8  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** For the shortbread dough: 

  * 240 g flour 
  * 150 g of butter 
  * 90 of sugar 
  * 30g of almond powder 
  * 1 sachet of vanilla sugar 
  * 1 egg yolk 
  * 1 pinch of salt 

Almond cream: 
  * 12Og of almond powder 
  * 2 eggs 
  * 100g of sugar 
  * 200 ml whole liquid cream 
  * 3 pears 

Deco: 
  * Scraped almonds 
  * Icing sugar 



**Realization steps** The shortbread dough: 

  1. In the blender or robot bowl put the flour, sugar, vanilla sugar and salt. 
  2. Add the diced butter and mix with the K mixer until a sand mixture is obtained. 
  3. Add the egg yolk and almond powder and beat until a paste is removed from the sides of the bowl and is non-sticky. 
  4. Let it rest. 
  5. Spread the dough with the roll between 2 sheets of baking paper. 
  6. Smear a pie circle 24 cm in diameter. 
  7. Stir the batter stock with a fork and chill for the time to prepare the filling. 

the amnadine cream: 
  1. In the bowl of the beater this time equipped with the whisk, beat the eggs with the sugar. 
  2. When the mixture is frothy add the almond powder and the cream. Beat again. 
  3. Place half pears before peeling, emptying and slicing slices on the pie shell and then cover with almond cream. 
  4. Sprinkle with blanched almonds. 
  5. Put in the oven for 30 minutes at 180 ° C (th.6). 
  6. Let cool. 
  7. Unmould and present on a serving platter. 
  8. Sprinkle with icing sugar before serving. 



I will be frank, I have all the ingredients at home, I think I will not delay to make this delice 

and what is it for you? 

bonne journee et a la prochaine recette. 
