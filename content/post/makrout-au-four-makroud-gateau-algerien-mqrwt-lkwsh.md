---
title: Baked Makrout, Makroud Algerian Cake "مقروط الكوشة"
date: '2015-07-10'
categories:
- Algerian cakes- oriental- modern- fete aid
tags:
- Aid
- Oriental pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/aid-el-fitr-2013-makrout-el-koucha.CR2_.jpg
---
##  [ ![help el fitr 2013 makrout el koucha.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/aid-el-fitr-2013-makrout-el-koucha.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-makrout-au-four-makroud-gateau-algerien-%d9%85%d9%82%d8%b1%d9%88%d8%b7-%d8%a7%d9%84%d9%83%d9%88%d8%b4%d8%a9.html/aid-el-fitr-2013-makrout-el-koucha-cr2>)

##  Baked Makrout, Makroud Algerian Cake "مقروط الكوشة" 

Hello everybody, 

The baked makrout is a typically Algerian cake (which may look like maamoul, ingredients side), but remains one of the delights that is present much at parties and especially in the eastern region of Algeria. 

one of [ Algerian cakes ](<https://www.amourdecuisine.fr/article-index-de-gateaux-alegriens-algerois-de-fetes-et-mariages-46645282.html>) most ask by my husband after the [ baklawa ](<https://www.amourdecuisine.fr/article-baklawa-baqlawa-aux-amandes-et-cacahuettes-61018034.html>) , comes the baked makrout, but not the [ fried makrout ](<https://www.amourdecuisine.fr/article-makrout-frit-makrout-el-maqla-ou-el-makla-72046810.html>) that's for me. 

to prepare it, I am the method of my grandmother, of Constantine, who does it marvelously well, besides in the weddings, one always asks my grandmother to make them Makrout el koucha (in the oven), 

**Baked Makrout, Makroud Algerian Cake "مقروط الكوشة"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/makrout-el-koucha-au-nakache-gateaux-de-laid-2014-021.CR2_.jpg)

Recipe type:  Algerian cake  portions:  100  Prep time:  45 mins  cooking:  60 mins  total:  1 hour 45 mins 

**Ingredients** and its ingredients are: (for a small tray of 40 x 50 cm) 

  * 1200 gr of coarse semolina (not too much fat ok) 
  * 400 gr of ghee (smen, or clarified butter) 
  * 1 little salt 
  * Orange tree Flower water 
  * Water 

for the stuffing: 
  * 500 gr of date paste 
  * ½ teaspoons cinnamon 
  * ½ teaspoon of ground cloves 
  * 2 tablespoons of butter 
  * 2 cafe jam (optional, I had put it, because I had a date paste of poor quality, and so I was afraid that the stuffing becomes hard cooking, I added the fig jam, which will make the dough soft) 
  * Orange tree Flower water. 



**Realization steps**

  1. Work the date paste with the ingredients previously given until you have a soft ball. Leave aside. 
  2. In a large bowl, pour the semolina, the salt. Mix. 
  3. Make a fountain and pour the melted clarified butter. Mix well so that the butter is well absorbed by the semolina. Let it rest. 
  4. Wet with orange blossom water and water, and mix without working with your fingertips and without kneading. Once you have a compact ball, leave aside, and cover well with a cling film. 
  5. take a good amount of the dough, shape a pudding. With your index finger, make a channel in the center in the direction of the length. 
  6. Roll a little roll of date paste and put it in the slot. 
  7. Reassemble the edges of the dough on the dates to cover it all. 
  8. Ride gently again to have a roll of a width of 2 cm in height. 
  9. With an impression mold, press the top of the pudding and flatten slightly to get the pattern on the makroud. 
  10. Cut the lozenge into lozenges and set aside in a baking dish. 



[ ![makrout1](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/makrout1_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/makrout1_2.jpg>)

[ ![makrout6_4](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/makrout6_4_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/makrout6_4_2.jpg>)

  1. Renew this operation until the dough is used up. 
  2. The diamonds are arranged tightly and touch each other (to prevent the date paste from getting burned) 
  3. Preheat the oven th. 175 ° C and put the tray to cook for about 30 min, but watch the cooking because it must have a beautiful golden color above and below. So be sure to turn off the bottom of the oven and turn on the top at the right time. 
  4. for the presentation, You can soak them in honey. 
  5. These cakes are kept in a hermetic box for a good duration (if you resist of course) 
  6. good tasting. 



[ ![makrout el koucha at the nakache cake of the aid 2014 021.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/makrout-el-koucha-au-nakache-gateaux-de-laid-2014-021.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-makrout-au-four-makroud-gateau-algerien-%d9%85%d9%82%d8%b1%d9%88%d8%b7-%d8%a7%d9%84%d9%83%d9%88%d8%b4%d8%a9.html/makrout-el-koucha-au-nakache-gateaux-de-laid-2014-021-cr2-2>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
