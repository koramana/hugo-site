---
title: fruit paste with mango
date: '2017-04-03'
categories:
- jams and spreads
- dessert, crumbles and bars
- sweet recipes
tags:
- Algeria
- Confectionery
- Pastry
- Dziriette
- baklava
- Algerian cakes
- candy

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/04/p%C3%A2te-de-fruits-%C3%A0-la-mangue.jpg
---
![fruit paste with mango](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/p%C3%A2te-de-fruits-%C3%A0-la-mangue.jpg)

##  **fruit paste with mango**

Hello everybody, 

Here is a wonderful recipe for fruit paste with mango I realized to participate in our game recipes around an ingredient. Usually I often prepared my fruit paste with Vitpris that my husband took me every time he went to France. But when I do not have any more, I bought pectin, but the color was a little dark, I do not know is normal, so if you are used to using pectin, you tell me is this normal this dark color ... (you can see the video to see the color) 

So, I got a little dark color of my fruit paste, I wanted a yellowish color of my fruit paste with mango, I am a little disappointed by the visual result, but side taste, this dough of fruit with mango was a real delight, to be honest, my children like a lot, so I often make them pasta fruit (that's why I exhausted all my stock of quickspring, hihihih) 

{{< youtube 6Dz77b9xxPE >}} 

you can see here 

**fruit paste with mango**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/p%C3%A2te-de-fruits-%C3%A0-la-mangue-2.jpg)

**Ingredients**

  * 250 gr of mango pulp (1 mango) 
  * 20 g of butter 
  * 20 gr of gelling agent (pectin, Vitpris Alsa, for sale in supermarkets) 
  * 300 gr of sugar. 
  * Coating: 
  * 50 gr of sugar. 



**Realization steps**

  1. Peel the mango 
  2. Cut the flesh into cubes. 
  3. Pass it to the robot. 
  4. Get the pulp. 
  5. Weigh it. 

Before you start cooking, prepare: 
  1. gr of sugar + 20 gr of gelling agent. 
  2. gr of sugar. 
  3. g sugar. 

Cooking the fruit paste: 
  1. Put the mango pulp in the pot with a thick bottom over medium heat. 
  2. Let it heat up, turning constantly until 70 ° of boiling (beginning of the small boiling). 
  3. Add the gelling agent and the sugar. 
  4. Cook for 1 min. about. 
  5. Add the 100 gr of sugar. 
  6. Let go to the boil. 
  7. Add the butter in small pieces. 
  8. Turn well to melt. 
  9. Add the remaining sugar twice. 
  10. Cook for about 2 minutes. 
  11. The pulp thickens quickly and you will see the bottom of the pot turning with a spoon. 
  12. Pour the dough quickly into silicone molds or a square mold covered with foil. 
  13. Let take before unmolding. 
  14. Cut into the shapes you want. 
  15. Wrap in powdered sugar. 
  16. Let dry 1 to 2 days before packing. 



![fruit paste with mango 1](https://www.amourdecuisine.fr/wp-content/uploads/2017/04/p%C3%A2te-de-fruits-%C3%A0-la-mangue-1.jpg)

Avant de partir, faites un saut sur cette liste pour voir toutes les recettes ayant participé à cette ronde: 
