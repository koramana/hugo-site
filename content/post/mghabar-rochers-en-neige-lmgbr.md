---
title: Mghabar, rocks in snow لمغبر
date: '2011-06-18'
categories:
- juice and cocktail drinks without alcohol
- Cuisine saine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mghaber-007_thumb-300x224.jpg
---
##  Mghabar, rocks in snow لمغبر 

Hello everybody, 

here is a dry Algerian cake Mghabar, rocks in snow "لمغبر", that you can present for all occasions, so it is easy to make in addition super nice, and too good. 

a recipe from a book of Algerian cakes, I'm not very sure of the source, but one of my readers told me once that it's from Samira's book, knowing that I had found the recipe on a forum. 

so I will not make you wait, so here's the recipe. 

**Mghabar, rocks in snow "لمغبر"**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mghaber-007_thumb-300x224.jpg)

portions:  30  Prep time:  30 mins  cooking:  15 mins  total:  45 mins 

**Ingredients** (For my part I did half the ingredients) 

  * flour 
  * 500 gr of margarine 
  * 1 glass and a half icing sugar 
  * 2 eggs 
  * 1 pinch of baking powder 
  * Orange tree Flower water 

the joke: 
  * packet of biscuits 
  * 1 handful of ground peanuts 
  * Turkish halwa 
  * jam 



**Realization steps**

  1. In a container, put margarine ointment, icing sugar, eggs and yeast pinch. 
  2. mix everything very well to have a nice cream. 
  3. add the flour, so that you can pound the dough, then wet with a little bit of orange blossom water, to obtain a soft dough. 
  4. Formet then small balls of 3 cm in diameter, let stand, in the meantime, prepare the stuffing. 
  5. In a container, put the ground biscuits, the cachuete handle, and the Turkish halibut, pick it all up with jam, until you get a soft stuffing. 
  6. Lower the balls of dough with the palms of the hands, then put some stuffing in the middle   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/mghaber-002_thumb1.jpg)
  7. go up the edges and close. 
  8. Using a nekkache (a special pliers), pinch the entire length and all the surface of the cakes, going from the bottom to the top. 



must say that I am not too good with the nakkache, hihihihihi 

Arrange the cakes on a baking dish sprinkle with flour, once cooked, sprinkle with the icing sugar. 

you can also garnish each cake with a flower and leaves of almond paste 

moi je n’en avais pas. 
