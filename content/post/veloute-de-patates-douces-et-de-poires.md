---
title: Cream of sweet potatoes and pears
date: '2012-02-28'
categories:
- pizzas / quiches / pies and sandwiches

---
hello everyone, here is a very good velvet comforting, very sweet, unctuous and super delicious, a marriage of flavors for your eyes and your taste buds. a recipe easy and very simple with ingredients available to everyone, of course before the end of the sweet potato season. & Nbsp; Ingredients: 2 tablespoons of butter 1 chopped white onion 3 medium sweet potatoes 2 peeled pears 1 cube of chicken broth 3/4 liter of water 250 ml of crème fraîche Preparation: melt the butter and come back again 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

Hello everybody, 

here is a very good velvet comforting, very sweet, unctuous and super delicious, a marriage of flavors for your eyes and your taste buds. a recipe easy and very simple with ingredients available to everyone, of course before the end of the sweet potato season. 

**ingredients:**

**Preparation:**

  1. melt the butter and return the chopped onion in large pieces for 2 minutes, 
  2. add cubes of sweet potato and pears also cut into cubes and stir 
  3. add the bouillon cube and add the water 
  4. cook for almost 20 minutes 
  5. Mix, then add the cream, salt and pepper. 



good tasting, and the next recipe 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
