---
title: sbaa laaroussa, sbaa el aroussa
date: '2014-03-15'
categories:
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-sans-cuisson-doigts-au-chocolat_thumb.jpg
---
##  sbaa laaroussa, sbaa el aroussa 

Hello everybody, 

sbaa laaroussa, sbaa el aroussa: A very nice recipe **[ cake without cooking ](<https://www.amourdecuisine.fr/gateaux-algeriens-sans-cuisson>) ** , that I prepared often, before embarking on the adventure of the blog, and of course I did not decorate in this way, it must be said that since the blog, I learned a lot from these hundreds of books that I , and also my blogger friends. 

Not only is it easy to achieve, but it is also very good and very nice to present. Easy ingredients, and accessible to everyone, and the most important that we can adapt them according to our tastes and what we have, at home. 

Look at the link, for more [ cakes without cooking ](<https://www.amourdecuisine.fr/gateaux-algeriens-sans-cuisson>) , or [ Algerian cakes ](<https://www.amourdecuisine.fr/gateaux-algeriens-orientales-modernes-fete-aid>)

**sbaa laaroussa without cooking, Sbiaates laaroussa**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/08/gateaux-sans-cuisson-doigts-au-chocolat_thumb.jpg)

Recipe type:  Dessert  portions:  30  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * 1 box of biscuits 
  * 1 glass (220 ml) of coconut 
  * jam ( [ peach jam ](<https://www.amourdecuisine.fr/article-confiture-de-peches.html> "jam of peaches") for me) but you can one of your choice. 
  * 5 tablespoons of melted butter 
  * dark chocolate for decoration 
  * coconut for decoration 



**Realization steps**

  1. in the bowl of a blender, crush the biscuits powder 
  2. add the coconut 
  3. then stir in the butter. 
  4. pick up the dough with the jam until you have a paste that picks up. 
  5. prepare rolls or fingers 8 cm long and 2.5 cm in diameter 
  6. put in the fridge for 1 hour, so that the dough fits well 
  7. melt the chocolate in a bain-marie 
  8. place your fingers on a rack, and cover them gently with the melted chocolate, using a spoon 
  9. sprinkle the coconut on the sides, before the chocolate freezes 
  10. take the chocolate well. 
  11. melt 2 to 3 squares of chocolate, and place them in a cone of baking paper or a bag and pass over the cakes to draw scoffers. 
  12. let it freeze, and put them in boxes, in a cool place (not in the fridge otherwise you will have a point on the chocolate) 
  13. can be kept for 3 days in an airtight container in a cool place. 


