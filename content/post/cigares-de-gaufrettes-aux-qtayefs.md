---
title: wafer cigars with qtayefs
date: '2015-08-02'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid
tags:
- Algerian cakes
- Algeria
- Cakes
- Cookies
- Aid
- biscuits
- Little butter

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-de-gaufrettes-aux-qtayefs.jpg
---
[ ![wafer cigars with qtayefs](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-de-gaufrettes-aux-qtayefs.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-de-gaufrettes-aux-qtayefs.jpg>)

##  wafer cigars with qtayefs 

Hello everybody, 

Here is a great delicious cakes without cooking (the only cooking is that of grilling qtayefs so that they are crispy) 

A recipe shared with us, my friend Oum Thaziri, is made of rolled wafers stuffed with Nutella, wrapped in a very sweet layer of peanuts and Turkish halwa (tahini halva), and covered towards the end of the day. 'a beautiful layer of well roasted qtayefs, so without delay I pass you the recipe of these **wafer cigars with qtayefs.**

**wafer cigars with qtayefs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-de-gaufrettes-aux-qtayefs.jpg)

portions:  40  Prep time:  20 mins  total:  20 mins 

**Ingredients**

  * wafer cigars 
  * Nutella 
  * qtayefs 
  * grilled and crushed peanuts. 
  * halvah 
  * honey to pick up 
  * jam 
  * Orange tree Flower water 
  * chocolate for decoration 



**Realization steps** note: I did not give the exact ingredients, because it is a cake without cooking to you to taste and adjust the quantities according to your taste. 

  1. start by roasting the qtayefs in the oven, you have to watch and turn to have a homogeneous color on all qtayefs 
  2. crush the qtayefs, after cooling, you can crush them on a sieve to have a uniform size   
  
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-de-gauferettes-aux-qtayefs-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-de-gauferettes-aux-qtayefs-6.jpg>)
  3. place the Nutella in a bain-marie to be able to work it without problem 
  4. fill a pastry bag with Nutella, and fold the wafer cigars with. 
  5. now mix the peanuts with the halva, then pick up with honey to have a nice ball. 
  6. spread the ball in a thin layer of almost 5 mm, on baking paper or food film so that it does not stick. 
  7. take a cigar of wafers stuffed with Nutella, on the peanut paste 
  8. trace with a knife to have the same size in length of the cigar. 
  9. roll the cigar to cover it well, 
  10. do so until exhaustion of the cigars. 
  11. dip the rolls covered with peanut pie in a mixture of jam and orange blossom water 
  12. then immerse them directly in the crushed dishes to cover them well. 
  13. let dry and then decorate with chocolate. 



[ ![wafer cigars with qtayefs 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-de-gaufrettes-aux-qtayefs-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/cigares-de-gaufrettes-aux-qtayefs-1.jpg>)
