---
title: fingers of kounafa cacahuetes / grapes
date: '2011-11-20'
categories:
- soupes et veloutes

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/11/doigts-de-kounafa_thumb.jpg
---
[ ![konafa's fingers](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/doigts-de-kounafa_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/11/doigts-de-kounafa.jpg>)

Fingers of kounafa peanuts / grapes 

Hello my friends, 

before going out quickly I will try to publish you the recipe of the kounafa fingers, or konafa with the raisins that I made during the festival of the aid el kbir, recipe very easy, very simple, and especially a delight.   


**fingers of kounafa cacahuetes / grapes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/doigts-de-kounafa_thumb.jpg)

Recipe type:  Algerian cake  portions:  30  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 500g of ktaifs (kounafa). 
  * melted ghee (smen or clarified butter) 
  * a handful of raisins 
  * a handful of chopped peanuts 
  * 3 tablespoons of sugar 
  * from smen to piece 
  * honey + orange blossom water to water 



**Realization steps**

  1. take a strand of konafa 6 cm by 12 cm 
  2. spread it out on a work plan 
  3. sprinkle with a brush with smen (melted ghee) 
  4. put a hazelnut mixture: raisins, peanuts and sugar 
  5. roll over the ktaives while squeezing the mixture   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/doigts-de-konafa_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/11/doigts-de-konafa_thumb.jpg>)
  6. place as you go in a baking tin 
  7. clench your fingers against each other until the kataifs are exhausted 
  8. scatter pieces of smen on it 
  9. cook in a preheated oven at 170 degrees until the konafas are golden brown 
  10. take it out of the oven, sprinkle liberally with a warm mixture of honey and orange blossom 
  11. let it absorb well 
  12. present in boxes 



[ ![konafa with grapes](https://www.amourdecuisine.fr/wp-content/uploads/2011/11/konafa-au-raisin_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2011/11/konafa-au-raisin.jpg>)

knowing that it was these same konafa fingers that I had to present at Rayan's school party, I only cut each finger into 3 slices, so that it would be small delicacies, and it was sublime because children have nothing left, children love everything that is small. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
