---
title: fine pie with plums and plums
date: '2015-09-04'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-fine-aux-pommes-et-prunes-046.CR2_thumb.jpg
---
##  fine pie with plums and plums 

Hello everybody, 

for a nice thin pie you'll need a roll of puff pastry (mine is in the fridge for a week), some plums quetsches (waiting for a while), and here is a nice thin pie that is preparing less 15 minutes, if you add apple slices between these beautiful slices of plums, it will give a delicious recipe. 

**apple and plum tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/tarte-fine-aux-pommes-et-prunes-046.CR2_thumb.jpg)

Recipe type:  dessert pie  portions:  8  Prep time:  20 mins  cooking:  35 mins  total:  55 mins 

**Ingredients**

  * 1 roll of puff pastry 
  * 3 to 4 plums 
  * 3 to 4 apples 
  * 1 egg 
  * 4 tablespoons of sugar 
  * icing sugar to sprinkle 



**Realization steps**

  1. Preheat the oven to 160 degrees. 
  2. peel the apples and slice them finely. 
  3. Remove the plum stones, and slice them finely. 
  4. In a bowl, place the apple slices, add 2 tablespoons and let macerate for 5-10 minutes. 
  5. In another bowl, add 2 tablespoons of sugar to the sliced ​​plum slices and let the plums macerate for 5-10 minutes to release their juice. 
  6. on baking sheet, line with baking paper. place the roll unrolling puff pastry. 
  7. Beat the egg in a small bowl until smooth. Brush the puff pastry with and especially the edges. 
  8. place the slices of apples and plums on top, one by one as shown in the picture. 
  9. leave a little space around the puff pastry. 
  10. Bake for 20 minutes until the dough swells well. 
  11. When serving, sprinkle with sifted icing sugar. 



is not an easy and simple recipe. besides being very very good, and not too sweet. 

we love a lot at home, wishing that for a variant, you can just put plums by themselves, or apples alone. 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/visitecomm2.png)
