---
title: Qalb el louz with lben essential buttermilk recipe
date: '2015-06-20'
categories:
- Algerian cuisine
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/qalb-el-louz-au-lben1.jpg
---
##  [ ![Qalb el louz with lben essential buttermilk recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/qalb-el-louz-au-lben1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/qalb-el-louz-au-lben1.jpg>) Qalb el louz with lben essential buttermilk recipe 

Hello everybody, 

No more excuses to say I missed my qalb el louz, voila ... I share with you today "the" Qalb elouz inratable, since the beginning of Ramadan, it's almost the 5th time I make this recipe, and no mischief in any test, so, try this recipe and tell me your opinion. 

The good thing is that you do not have to wait for a whole night, this lamb buttermilk, only rest for 20 minutes while your oven is heated, and here we go ... 

I made at the request of many people who follow me on youtube, a video in French, and another in Arabic. 

I leave you with the ingredients, and that I have time I come back to write the recipe for you, otherwise you can always see the videos ... 

the recipe in Arabic: 

**Qalb el louz with lben essential buttermilk recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/qalb-el-louz-au-lben2.jpg)

portions:  8  Prep time:  15 mins  cooking:  45 mins  total:  1 hour 

**Ingredients**

  * 6 glass of coarse semolina (a glass of 180 ml) 
  * 3 glass of sugar 
  * 1 glass of lben buttermilk 
  * 1 glass of oil 
  * 100 ml of orange blossom water 

for the syrup: 
  * 6 glasses of water 
  * 3 glasses of sugar 
  * 2 tablespoons of orange blossom water 



**Realization steps** instruction on the video 

  1. Preheat your oven to 200 °. 
  2. Prepare the syrup by boiling it for about ten minutes. 
  3. Mix the semolina, the sugar and the salt, until no more differentiating the ingredients. 
  4. Add the lben, the oil and the orange blossom, you get a paste a little liquid. Let rest 15 min. 
  5. Meanwhile mix the ingredients of the stuffing. 
  6. Place half of the dough in a mold with high edges, previously oiled. (if you are going to stuff, if like me you do the qalb el praise without skipping skip this step and the one that follows) 
  7. Pour the stuffing and then the other half of the dough. Equalize with your hand or a spatula. 
  8. Then draw squares with a knife and place an almond on each square. 
  9. Bake at 180 ° for 1 hour. The cake must have a beautiful golden color, as soon as you see the edges turn brown, do not hesitate to put the cake higher in your oven. 
  10. Once the cake is cooked, sprinkle half of the syrup.Replace in your oven off but still hot, so that it absorbs the syrup.Add the rest of the syrup and let cool. 



[ ![Qalb el louz with lben essential buttermilk recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/qalb-el-louz-au-lben3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/qalb-el-louz-au-lben3.jpg>)

the video in French: 

the video in Arabic: 

{{< youtube Uyb2ByyRVDE >}}  {{< youtube 7PhfRp3q1uM >}} 
