---
title: Oven-roasted chicken
date: '2017-06-14'
categories:
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/POULET-ROTI-014.CR2_.jpg
---
##  Oven-roasted chicken 

Hello everybody, 

There's more delicious than an oven-roasted chicken, so here's a very tasty marinated chicken in a caraway sauce, then roasted in the oven, a delicious chicken recipe that everyone loved at home. 

Then it's super easy to find a simple recipe to accompany the roast chicken, such as simple fries or a delicious [ mashed potatoes ](<https://www.amourdecuisine.fr/article-puree-de-pomme-de-terre.html>) very smooth. 

you can see the recipe on video: 

**Oven-roasted chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/POULET-ROTI-014.CR2_.jpg)

portions:  6  Prep time:  20 mins  cooking:  90 mins  total:  1 hour 50 mins 

**Ingredients**

  * a chicken of almost 1.5 kilos 
  * 4 to 5 tablespoons of table oil (extra virgin olive oil for me) 
  * 1 teaspoon of cumin 
  * ½ cup of paprika 
  * ½ teaspoon of black pepper 
  * ½ teaspoon garlic powder 
  * ½ teaspoon coriander powder 
  * ½ cup of rosemary 
  * salt according to taste 



**Realization steps**

  1. clean the chicken well 
  2. mix all the spices and salt in the oil, then with a brush, cover all the chicken (inside and outside) 
  3. marinate all night, or prepare in the morning for roasting at night. 
  4. do not worry but the oil will cover the chicken well, and allows the spices to penetrate the skin 
  5. I use my oven with the roasting spit, but you can put on a rack 
  6. place over a large mold with water in it, it allows the chicken to cook without being too dry. 
  7. cook the chicken for at least 1h30. 
  8. at the end of the cooking, recover the juice in the large pan (the water would have evaporated well) add a little water to have a good juice 
  9. serve with a mashed potato (it was for me and my husband) 
  10. or with sintered potatoes (for children) 
  11. or according to your taste. 



[ ![CHICKEN ROTI 006.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/POULET-ROTI-006.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/03/POULET-ROTI-006.CR2_.jpg>)

thank you for your visits and comments 

bonne journee 
