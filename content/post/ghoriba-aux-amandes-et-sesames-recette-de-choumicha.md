---
title: Ghoriba with almonds and sesame recipe from Choumicha
date: '2014-03-14'
categories:
- Algerian cuisine
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/ghribiya2.jpg
---
![Ghoriba with almonds and sesame recipe from Choumicha](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/ghribiya2.jpg)

##  Ghoriba with almonds and sesame recipe from Choumicha 

Hello everybody, 

These Ghoriba with almonds and sesame recipe of Choumicha is a delight that shares with us my friend Lunetoiles. I love these cracked Ghoriba and well flavored with almonds and sesame seeds, and I highly recommend the recipe because it is a real delight.   


Thanks to Lunetoiles for sharing this delicious cake of choumicha recipes, knowing that we in Algeria are called these delights: ghribia ....   


**Ghoriba with almonds and sesame - Choumicha recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/ghribiya2.jpg)

Recipe type:  cookies  portions:  20  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 125 gr grilled and ground sesame 
  * 125 gr ground almonds with skin 
  * 125 gr of icing sugar 
  * 1 big egg 
  * 1 packet of dry yeast 
  * 1 pinch of salt 
  * 2 tablespoon orange blossom 



**Realization steps**

  1. Mix all dry ingredients (almond, sesame, icing sugar, baking powder, pinch salt) 
  2. then add orange blossom and egg, mix well and let stand a few minutes (30 min) 
  3. Shape balls and roll them in the icing sugar 
  4. Place them on the oven plate and put in the oven for about 10 minutes in a preheated oven at 160 ° C 



![Ghoriba with almonds and sesame recipe from Choumicha](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/ghribiya.jpg)
