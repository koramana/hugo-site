---
title: indian samoussa
date: '2014-03-08'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/samoussa-indiennes-046_thumb.jpg
---
[ ![indian samoussa 046](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/samoussa-indiennes-046_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/samoussa-indiennes-046.jpg>)

Hello everybody, 

I really like the composition of the dough, and especially the stuffing, so it was for me the ideal recipe for my menu. 

**indian samoussa**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/samoussa-indiennes-046_thumb.jpg)

portions:  10  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** Dough : 

  * 400g of flour 
  * 100g softened butter 
  * A pinch of baking powder 
  * Salt 
  * Enough plain yoghurt to pick up the dough 

Prank call : 
  * 3 medium potatoes peeled and diced 
  * 150 gr of minced meat 
  * 1 onion 
  * 2 cloves garlic 
  * 1 to 2 good handfuls of black olives pitted and sliced 
  * Some portions of "laughing cow" 
  * Salt, pepper and curry 
  * A bit of chilled fresh coriander 



**Realization steps**

  1. Prepare the stuffing by frying the onion and garlic in a tablespoon of olive oil, 
  2. add the minced meat, the olives, 
  3. cook the potato peeled and cubed in salt water 
  4. after cooking add the mashed potato 
  5. add the curry, salt, pepper, cover and simmer (add water if necessary). 
  6. Add the coriander, and the cheese, mix everything well, the stuffing should not be liquid, but mashed. 
  7. let cool. 
  8. In this time, prepare the dough, mixing all the ingredients, and picking up with the yogurt, once you get a smooth dough, divide it into 2 to 3 balls, cover them and let them rest for 15 minutes. 
  9. Drop each of the balls of dough finely on a floured work surface, cut circles with a bowl, then cut the circles obtained in the middle, to obtain half circles, pass with the help of your finger a little water around the perimeter of the half circle, put your stuffing in the middle, and close in wallet, pinch to weld the edges. 
  10. Do the same with the remaining dough and stuffing. 
  11. Fry these triangles in hot oil, until it is golden brown, 
  12. let drain on paper towels. 



[ ![indian samoussa](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/samoussa-indienne_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/samoussa-indienne.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)
