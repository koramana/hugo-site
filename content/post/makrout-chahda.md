---
title: makrout chahda
date: '2015-12-01'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
tags:
- Almond
- biscuits
- Pastry
- Algeria
- Aid
- Ramadan
- Algerian patisserie

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/makrout-echahda-2.jpg
---
[ ![makrout echahda 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/makrout-echahda-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/makrout-echahda-2.jpg>)

##  makrout chahda 

Hello everybody, 

makrout echahda, or makrout chahda, ie makrout beeswax, because the patterns formed with a special pliers, gives the appearance of beeswax has this honey Algerian sweetness, just as beautiful as delicious . 

The recipe today comes from a generous friend: Chahinez Bahria, who shares with us today, these pretty pictures and her recipe. 

If you do not understand the method of realization of this sweetness, you can see this video of Samira Tv, Makrut Chahda directed by: Fadéla Rebbahi,  مقروط الشهدة فضيلة رباحي. 

The recipe for the dough of Chahinez Bahria differs from that on the video. 

**makrout chahda**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/makrout-echahda-1.jpg)

portions:  30  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients** for the pasta: 

  * 1 measure of oil 
  * 1 measure of cooled melted margarine 
  * 1 measure of milk 
  * 1 teaspoon of vanilla extract 
  * and flour 

for the stuffing: 
  * 3 measure ground almonds 
  * ½ measure of icing sugar 
  * lemon zest 
  * 3 tablespoons of margarine 
  * Orange tree Flower water 



**Realization steps**

  1. start by making the dough, pick the ingredients well to have a malleable dough, knead well. 
  2. Divide into equal balls and let stand. 
  3. Prepare the stuffing: 
  4. mix the almond and the sugar, add the lemon zest, 
  5. add the butter and collect the stuffing with orange blossom water. 
  6. spread the dough finely in a fairly wide rectangle. 
  7. place a stuffing pudding on one of the ends 
  8. roll the dough over to cover the stuffing, make two turns. 
  9. cut diamonds of almost 4 cm. and place them on a work surface. 
  10. to make the shahda drawing or beeswax, use a clean and new tweezers. 
  11. let the cakes rest all night, as during cooking, the cakes keep their drawings. 
  12. cook in a preheated oven at 180 degrees for 15 minutes or depending on the oven capacity. 
  13. after cooking let cool, and dip the cakes in hot honey, the ideal is to pass it to honey twice. 
  14. decorate with a little almond paste decorated in the shape of a bee. 
  15. present in boxes, and good tasting 



[ ![makrout echahda](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/makrout-echahda.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/makrout-echahda.jpg>)
