---
title: Baked tuna maakouda
date: '2017-01-24'
categories:
- cuisine algerienne
- Plats et recettes salees
tags:
- Algeria
- Healthy cuisine
- Easy cooking
- Ramadan 2017
- inputs
- Salty pie
- Hot Entry

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/maakouda-au-thon-au-four.jpg
---
![Baked tuna maakouda](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/maakouda-au-thon-au-four.jpg)

##  Baked tuna maakouda 

Hello everybody, 

The baked maakouda is a delicious entry to Algerian cuisine, which I personally like a lot, especially tuna maakouda and because I do not really like fried food, which takes a lot of time, and secondly, not good for health so I prefer my baked maakouda 

This [ Entrance ](<https://www.amourdecuisine.fr/pizzas-quiches-tartes-salees-et-sandwichs>) hot, that I baked is actually a delight that can be prepared to serve with a good [ chorba ](<https://www.amourdecuisine.fr/article-chorba-frik.html>) , a good entry, with a very good [ salad ](<https://www.amourdecuisine.fr/article-salade-variee-au-boulgour-69843202.html>) fresh, or baked chicken pieces, and the choice is varied if you visit the rubric [ chicken ](<https://www.amourdecuisine.fr/recettes-a-la-viande-de-poulet-halal>)

do not hesitate to try it because it is a delight. 

{{< youtube 29x5sJ49Uvo >}} 

**tuna maakouda**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/maakouda-au-thon-aufour-2.jpg)

portions:  8  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 3 medium potatoes 
  * ½ onion 
  * ½ bunch of parsley 
  * a handful of pitted green olives 
  * 5 eggs 
  * grated cheese 
  * ½ can of tuna (or according to taste) 
  * salt, pepper, spices according to your taste 



**Realization steps**

  1. cook the potato cleaned, peeled and cut into pieces, in salted water. 
  2. at the same time fry the chopped onion into small pieces over low heat. 
  3. after baking the potato crush it, or pass it to the vegetable mill, mix with the onions, add the well-chopped parsley, the grated cheese, and the olives cut in pieces, and the tuna. mix everything well.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/maakouda-au-thon-four-1.jpg)
  4. beat the eggs well and season a little if you think your potato mixture is lacking in salt or anything else. 
  5. butter a baking tin (silicone koglouph mold for me) pour the mixture, garnish with whole olives, grate a little gruyere over it. 
  6. and cook in a preheated oven for 20 to 30 minutes or in your oven. 
  7. at the end of the oven, grate some more cheese on top of the oven. 



![baked tuna maakouda 1-2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/maakouda-au-thon-au-four-1-2.jpg)
