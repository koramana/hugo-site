---
title: Pickled Eggplant Pickled for Ramadan 2015
date: '2015-07-02'
categories:
- appetizer, tapas, appetizer
- ramadan recipe
- salads, salty verrines
tags:
- Olive oil
- Economy Cuisine
- Algeria
- Morocco
- Ramadan
- Vegetables
- Ramadan 2015

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/badinjen-mokhallal.jpg
---
[ ![badinjen mokhallal](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/badinjen-mokhallal.jpg) ](<https://www.amourdecuisine.fr/article-aubergines-marinees-au-vinaigre-pour-ramadan-2015.html/badinjen-mokhallal>)

##  Pickled Eggplant Pickled for Ramadan 2015 

Hello everybody, 

Here is a recipe of eggplant pickled vinegar that will please you personally, but eggplants are going to marinate, and I will put the new pictures as soon as it is ready. A recipe that will be ideal when it is a picnic, a barbecue, or during Ramadan. That's why you have to start preparing your pickled vegetables now, so that it's ready for this 2015 Ramadan. 

I thank my friend **Oum Ali** for this recipe, which she has published on our kitchen group on facebook ... And being a bit of eggplant, I like to eat them at all sauces, and this marinated eggplant recipe should only be delicious ... I'm going return to this article as soon as my eggplant is ready to give you the verdict. 

I go back this recipe, because my pickled eggplants are ready, and we loved to taste them: 

[ ![pickled eggplant-2](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/aubergines-marin%C3%A9s-2.jpg) ](<https://www.amourdecuisine.fr/article-aubergines-marinees-au-vinaigre-pour-ramadan-2015.html/aubergines-marines-2>)

**Pickled Eggplant Pickled for Ramadan 2015**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/aubergines-marin%C3%A9s-2.jpg)

portions:  4-5  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * Eggplant 
  * White vinegar 
  * bay leaves 
  * olive oil 

prank call: 
  * paprika 
  * powdered cumin 
  * black pepper or white pepper 
  * salt 
  * Brown sugar 
  * thyme 
  * dry red pepper (felfel driss) 
  * coarsely crushed almonds if not nuts 
  * coriander, parsley and garlic   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/aubergine-marin%C3%A9-au-vinaigre-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42495>)



**Realization steps**

  1. start by mixing all the ingredients of the stuffing. 
  2. peel the aubergines as you see in the photos and make knife slits   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/aubergine-marin%C3%A9-au-vinaigre.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42496>)
  3. Steam eggplant for 15 min no more 
  4. let cool a little, then fill the eggplant slits with the stuffing previously prepared. 
  5. place the eggplants upright in jars   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/bidejen-mkhallel.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=42498>)
  6. add the bay leaves. 
  7. fill the jars with vinegar until the eggplant is covered, then add a little oil to cover the surface 
  8. place the jars tightly closed in a place away from light. 
  9. Your eggplants will usually be ready after 7 days. 



[ ![pickled eggplant-4](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/aubergines-marin%C3%A9s-4.jpg) ](<https://www.amourdecuisine.fr/article-aubergines-marinees-au-vinaigre-pour-ramadan-2015.html/aubergines-marines-4>)

J’espere que la recette vous plaira bien, si vous l’essayez n’oublier pas de me dire votre avis. 
