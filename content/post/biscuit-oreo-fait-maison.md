---
title: Homemade oreo biscuit
date: '2016-10-13'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours
tags:
- Algerian cakes
- Aid cake
- Gateau Aid
- Oriental pastry
- Algerian patisserie
- Dry Cake
- Cookies

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/comment-faire-des-oreos-maison.jpg
---
[ ![Homemade oreo biscuit](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/comment-faire-des-oreos-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/comment-faire-des-oreos-maison.jpg>)

##  Homemade oreo biscuit 

Hello everybody, 

Who of you do not like oreo cookies ??? My kids love it, they like it with all the sauces, lol. If I say you want a smoothie, they ask me to put oreos in it. If I want to make an ice cream, they prefer it with oreos inside ... 

In any case, it's the number one biscuit at home, despite that I'm going to be frank, I prefer speculoos ... But what can we do in front of the choice of our little loulous? 

These homemade oreo cookies are an achievement of Lunetoiles, who as always love to share the recipe with you through my blog, thank you very much my dear, and have a next recipe, lol. Knowing that my email box is full of lunetoiles recipes just waiting to be published on my blog. 

[ ![how to make homemade oreo](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/oreo-fait-maison-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/oreo-fait-maison-1.jpg>)   


**Homemade oreo biscuit**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/Oreos-fait-maison.jpg)

portions:  30  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients** biscuits: 

  * 260 g flour 
  * 90 g unsweetened cocoa powder 
  * ¼ of c. coffee baking soda 
  * 1 pinch of salt 
  * 225 gr of sweet butter, unsalted, softened at room temperature 
  * 160 g of brown sugar 
  * 1 egg yolk (maybe ... if your dough is pretty sticky, the yellow will not be necessary) 

vanilla cream: 
  * 125 g of soft butter, softened at room temperature 
  * 210 g icing sugar 
  * 1 teaspoon of vanilla extract (or 2 sachets of vanilla sugar) 



**Realization steps** prepare the cookies: 

  1. In a large bowl, sift together flour, cocoa powder, baking soda and salt. 
  2. Beat to combine and set aside. 
  3. In the bowl of a stand mixer equipped with the paddle (K beater), beat the butter at medium speed, until smooth (about three to five minutes). 
  4. Scrape the edges of the bowl, add the sugar and beat for about 2 minutes afterwards. 
  5. Add the flour / cocoa mixture in 2 additions, beating on low speed for about 30 seconds after each addition. 
  6. \- then continue beating low until the dough collects on the beater K. {Note: My dough looked very dry so I added 1 egg yolk and continued to mix until what the dough got together. 
  7. Place the dough on a lightly floured surface - form a square block. 
  8. Cover tightly with plastic wrap and refrigerate for at least one hour. (The dough can be refrigerated up to 2 days.) 
  9. Place a rack in the middle of the oven. 
  10. Preheat the oven to 160 ° C. 
  11. Prepare two baking sheets of parchment paper. 
  12. Remove the cooled cookie dough - if it is too hard, let it rest at room temperature until it softens. 
  13. Flour a large work surface. {You can spread the well-floured dough between two sheets of parchment paper. } 
  14. With a rolling pin, press the top of the dough, from left to right, to start flattening, then put the dough in 90 degrees and repeat (which helps prevent cracking). 
  15. Spread the dough not too thick, and not too thin either, then pass on with a roll pattern. 
  16. Use a ribbed cookie cutter and cut as much as you can - be sure you have an even number to make sandwiches. 
  17. Gather the dough falls and repeat the operation. 
  18. Bake for 15-17 minutes, turning plates halfway through baking, until cookies are just starting to develop small cracks on the surface and give off a "chocolate" smell 
  19. Remove the mussels from the oven and place on a rack. Let the cookies sit for 10 minutes - then transfer them to the grill until completely cool. 

While the cookies are cooling, make the cream filling: 
  1. In the mixer bowl, put the butter soft, beat at high speed until creamy, about 1 minute. 
  2. Add the icing sugar and vanilla. Beat at low speed for 1 minute, then switch to high speed and beat for 1 minute, until creamy and all is well mixed. The cream is thick. 
  3. Spread the vanilla cream, using a pastry bag, on a cooled chocolate biscuit, then place another over, and press to evenly distribute the cream between the two cookies. 
  4. Repeat with the rest. 
  5. To make a double oreo, do the same operation, fill the cream with the pastry bag on the top of an oreo, and place a chocolate biscuit on top, press a little, you get a double oreo. 



Note Keep the cookies covered for 1 week.   
The cookies freeze very well, up to 2 months.   
_Notes:_   
Vanilla butter cream can be prepared in advance (up to 2 days in advance). Covered and refrigerated. Allow to come to room temperature before using. [ ![homemade oreo](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/oreo-fait-maison.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/oreo-fait-maison.jpg>)
