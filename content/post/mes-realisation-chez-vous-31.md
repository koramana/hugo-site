---
title: my realization at home 31
date: '2012-12-01'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/ob_83855a7b6b9f43609910afefab6ef6f7_pp1.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

you are from days to days more to visit my blog, to leave me comments even if it is just to say thank you .... hihihihi (sorry if I do not validate them right away, because I'm taken, but I answer as soon as possible). 

and you are more and more numerous to try my recipes, and it gives me a lot of pleasure. 

please continue to send me your test photos on my email: [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)

**just a clarification** : 

this email is not a contact email, if you have questions to ask me about a recipe, make the comment, because this email, I only consult when I have the time to write the article: my achievements at home, and sometimes I find urgent questions that are more than 1 year old .... 

sorry, I have the comments to answer your questions, and I have this email for the achievements, and I have another email to contact the author, but it's mostly for commercials, hihihihih 

I can not receive everything in the same email, it will be overflowing. 

so, and I'll say it again, if you have an urgent question about a recipe, make it a comment, it will allow me to respond faster, as it allows other visitors to enjoy the answer, and not to rest the question again and again. 

thank you. 

so for photos of your essays send me on this email: [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)

[ Khobz eddar without fluffing ](<https://www.amourdecuisine.fr/article-khobz-dar-sans-petrissage-pain-maison-105595832.html>) at lunetoiles. 

[ marble cake ](<https://www.amourdecuisine.fr/article-cake-marbre-112259514.html>) at Nassou F 

[ Tlitli constantinois ](<https://www.amourdecuisine.fr/article-tlitli-constantinois-pour-l-annee-hijir-1434-112473751.html>) at Assia K. 

![P1080974](https://www.amourdecuisine.fr//import/http://p7.storage.canalblog.com/79/01/407800/)

[ Croissants briochees ](<https://www.amourdecuisine.fr/article-35847545.html>) at Malika 

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/ob_83855a7b6b9f43609910afefab6ef6f7_pp1.jpg)

[ tuna rolls ](<https://www.amourdecuisine.fr/article-roules-au-thon-108441937.html>) at Nassima 

and she says: 

SALEM MY SISTER SOULEF HAS NOT HAS LONGTEMP THAT I AM ONE OF YOUR FAN TON BLOG IS GENIAL IT TAKES THE HALF OF MY TEMP, MY CHILDREN FIND THIS RECIPE JUST DELICIOUS ROLLING AT THE TUNA THANK YOU FOR ALL IES ON SA WILL NOT MY LAST MAIL BARAKA ALLAH FIK YAATIK SAHA ABIENTOT. 

thank you Nassima 

[ Buchty ](<https://www.amourdecuisine.fr/article-buchty-brioche-facile-a-mie-filante-112719097.html>) , revisited at Dhouha 

[ Buchty ](<https://www.amourdecuisine.fr/article-buchty-brioche-facile-a-mie-filante-112719097.html>) chocolate chips at Dhouha's place 

she says: 

I made the recipe for buchty 

the sugar is perfect   
for the initial recipe with Nutella I will add slightly when I do nature. 

[ zaaoulk ](<https://www.amourdecuisine.fr/article-zaalouk-d-aubergines-107254425.html>) at moha's place 

[ Apple pie ](<https://www.amourdecuisine.fr/article-tarte-aux-pommes-anciennes-65550114.html>) at Assia 

[ Barbie birthday cake ](<https://www.amourdecuisine.fr/article-princesse-barbie-gateau-d-anniversaire-86764345.html>) chez Lina 
