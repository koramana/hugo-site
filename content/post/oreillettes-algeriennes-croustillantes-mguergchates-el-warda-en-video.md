---
title: crispy Algerian atria-mguergchates el warda on Video
date: '2014-07-05'
categories:
- Algerian cakes with honey
- fried Algerian cakes
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/07/nguergchate-el-warda.jpg
---
[ ![crispy Algerian atria-mguergchates el warda on Video](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/nguergchate-el-warda.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/nguergchate-el-warda.jpg>)

##  crispy Algerian atria-mguergchates el warda in video 

Hello everybody, 

Fancy tea accompanied by these delicious crunchy flower-shaped Algerian earbuds, or as we call them here, mqerqchate el warda, mguergchate el warda, khechkhache el warda, a3mam in oumghar, or in French: the whirlwind of the old man. 

This delicious cake, is a legacy of my mother who does it very well, an endless crispy, deliciously coated with a beautiful drizzle of honey. 

This recipe is done in the blink of an eye, the only difficulty will be when cooking, to achieve these beautiful flowers, do not worry, I made a video, which I will put online for you very soon, so you can see how easy it is to make these little delights. 

These [ fried Algerian cakes with honey ](<https://www.amourdecuisine.fr/gateaux-algeriens-frits>) , are a delight, you can find a nice variety of these recipes on my blog.   


**crispy Algerian atria-mguergchates el warda**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/oreilletes-croustillantes.jpg)

Recipe type:  Dessert  portions:  25  Prep time:  15 mins  cooking:  15 mins  total:  30 mins 

**Ingredients**

  * 2 measures of flour 
  * ½ measure of melted butter 
  * 1 pinch of salt 
  * the zest of half a lemon 
  * 1 teaspoon of vanilla 
  * a mixture of orange blossom water and water to pick up the dough 



**Realization steps**

  1. In a large salad bowl or terrine, mix the flour, butter, lemon peel and water + orange blossom water and knead until a firm dough. 
  2. Make rolls of almost 10 cm long and 1cm and a half in size, then let stand 1 hour. 
  3. Lower the pudding to have a long strip. 
  4. prepare the strips, put them on your work surface, and cover them with a cloth for the hardened paste, and so that you will not have a crust that forms. 
  5. Heat the cooking oil, and reduce the heat. 
  6. Take a strip of dough holding it higher and vertically, and dip the end into the oil 
  7. prick this end with a fork, and start turning the fork (as you do to eat the spaghetti). 
  8. do not overtighten when turning the dough with the fork. 
  9. cook for a few seconds on one side, then turn the other side and remove from the heat. 
  10. Repeat this operation with the other strips of dough, until exhaustion. 
  11. After cooking, dip the cakes in the honey. Let cool and serve with a good mint tea. 



Note 1- If you want to keep these cakes longer, do not deceive them in honey. Preserve them in an airtight box, and dip them in honey just before serving   
2- the measure that I used here was an English bowl of almost 300 ml, and it gave me almost 25 pieces of the cake, if you want more than this quantity, change the measure, use a larger bowl. 

[ ![crispy Algerian atria-mguergchates el warda on Video](https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mqerqchate-el-warda.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/07/mqerqchate-el-warda.CR2_.jpg>)

{{< youtube dGySZjYlNEQ >}} 
