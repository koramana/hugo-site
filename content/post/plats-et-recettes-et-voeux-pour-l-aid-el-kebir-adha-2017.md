---
title: dishes and recipes and wishes for Aid el kebir adha 2017
date: '2017-08-31'
categories:
- Algerian cuisine
- Dishes and salty recipes
- rice

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg
---
[ brains of lamb in tomato sauce - chtitha mokh ](<https://www.amourdecuisine.fr/article-cervelle-d-agneau-en-sauce-tomate-89450259.html>)

##  dishes and recipes and wishes for Aid el kebir adha 2017 

Hello everybody, 

Tomorrow insha'Allah, we are going to celebrate Eid el Adha, or how we tend to call it in Algeria, Eid el kebir.  The  Eid al-Kebir or A'd al-Kabir (Arabic: العيد الكبير, literally meaning "the big celebration"), is one of the most important festivals of Islam. The Islamic name deriving from the hadiths is "feast of sacrifice" or Eid al-Adha (Arabic: عيد الأضحى), this Eid marks each year the end of hajj. It takes place on the 10th of the month of dhu al-hijja, the last of the Muslim calendar, after waqfat Arafa, or station on Mount Arafat.Good sheep festival AID LEKBIR or AID AL ADHA (big party). 

![dishes and recipes and wishes for Aid el kebir adha 2017](https://www.amourdecuisine.fr/wp-content/uploads/2016/09/voeux-aid-adha.png)

In this grandiose and wonderful occasion, I address all Muslims to wish them a happy holiday rich in peace and love, may the good God accept our sacrifice. For those who are fasting today, Siyam maqboul fi el janna. 

Here are some recipes and dishes for help el kebir, help el adha, help el kbir you will not find much, because I almost always take out the sadaqa for help, because here in England, we has no right to make the sacrifice ... Insha'Allah the next years when the el el kebir is going to be on vacation, I could celebrate it with my children in Algeria, and as well as it is necessary and insha'Allah I will enrich my blog with even more recipes for Eid el kebir. 

Otherwise, if you have made some nice recipes with mutton, I will be happy to share them here on my blog. you just have to send me the photos and the recipe on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

[ bakbouka - Algerian cuisine ](<https://www.amourdecuisine.fr/article-bakbouka-cuisine-algerienne-103194168.html>)

[ Mesrane mahchi, stuffed gut, ](<https://www.amourdecuisine.fr/article-mesrane-mahchi-boyau-farci-recettes-aid-el-kebir-2012-111231442.html>)

[ head of sheep with cumin, baked ](<https://www.amourdecuisine.fr/article-tete-de-mouton-au-cumin-au-four-64875408.html>)

[ Osbane - osbana - Stuffed belly ](<https://www.amourdecuisine.fr/article-osbane-osbana-panse-farcie-96833347.html>)

[ lamb / mutton liver in tomato sauce - kebda mchermla ](<https://www.amourdecuisine.fr/article-foie-d-agneau-de-mouton-en-sauce-tomate-kebda-mchermla-87902209.html>)

![chtitha lsane, tajine lamb tongue 6](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/chtitha-lsane-tajine-langue-dagneau-6.jpg) [ chtitha lssane or tajine with mutton tongue ](<https://www.amourdecuisine.fr/article-chtitha-lsane-tajine-de-langue-d-agneau.html>)

![fillet of lamb roasted in the oven](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/filet-dagneau-roti-au-four.jpg) [ roasted lamb fillet in the oven ](<https://www.amourdecuisine.fr/article-filet-dagneau-roti-au-four.html>)

[ roasted meat with lemon ](<https://www.amourdecuisine.fr/article-viande-rotie-au-citron-89050764.html>)

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/25340955.jpg) [ Couscous with meat ](<https://www.amourdecuisine.fr/article-25345493.html>)

[ mtewem ](<https://www.amourdecuisine.fr/article-mtewem-103198692.html>)

[ stuffed artichoke tajine ](<https://www.amourdecuisine.fr/article-tajine-d-artichauts-farcis-97187541.html>)

[ Stuffed Cabbage ](<https://www.amourdecuisine.fr/article-choux-farcis-103681282.html>)

[ mtewem white sauce ](<https://www.amourdecuisine.fr/article-mtewem-sauce-blanche-108530309.html>)

![Meatloaf 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-de-viande-2.jpg)

[ meatloaf ](<https://www.amourdecuisine.fr/article-pain-de-viande.html>)

![meatball tagine hachee spinach 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/tajine-de-boulettes-viande-hachee-epinards-1.jpg) [ Tagine with meatballs and spinach ](<https://www.amourdecuisine.fr/article-tajine-aux-boulettes-de-viande-hachee-epinards.html>)

![lham mhamer, roast meat tajine 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/04/lham-mhamer-tajine-de-viande-roti-1.jpg) [ lham mhamer ](<https://www.amourdecuisine.fr/article-lham-mhamer-tajine-de-viande-rotie.html>)

![leg of lamb in the oven.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gigot-d-agneau-au-four.CR2_thumb.jpg) [ leg of lamb roti ](<https://www.amourdecuisine.fr/article-gigot-d-agneau-au-four-aid-el-adha-2012-111759550.html>)

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/epaule-d-agneau-et-pomme-de-terre-au-four-016.CR2_thumb.jpg) [ rotie lamb shoulder ](<https://www.amourdecuisine.fr/article-epaule-d-agneau-rotie-au-four-113882735.html>)

[ Algerian rechta, ](<https://www.amourdecuisine.fr/article-rechta-algeroise-cuisine-algerienne-108257998.html>)

![Puree-de-peas-a-la-mint-and-mascarpone - foam-of-.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/Puree-de-petits-pois-a-la-menthe-et-mascarpone-mousse-de-.jpg) [ Mashed peas with mint and mascarpone / fresh goat mousse / spicy minced meat kebab ](<https://www.amourdecuisine.fr/article-puree-de-petits-pois-a-la-menthe-et-mascarpone-mousse-de-chevre-frais-brochette-de-viande-hachee-e-105369766.html>)

[ leg of roast in the pressure cooker ](<https://www.amourdecuisine.fr/article-26570801.html>)

[ tagine with spinach ](<https://www.amourdecuisine.fr/article-ta<br%20/><br%20/><br%20/><br%20/>%20jine-aux-epinards-108150322.html>)

[ Kefta, kofta bacha Hassen, Pasha "كفته حسن باشا" ](<https://www.amourdecuisine.fr/article-35713413.html>)

[ Dolma, vegetables stuffed with meat baked in the oven ](<https://www.amourdecuisine.fr/article-dolma-legumes-farcis-a-la-viande-hachee-au-four-66035809.html>)

![tajine-of-peas-and-chard-farcis.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/tajine-de-petits-pois-et-cardons-farcis1.jpg) [ tajine of peas and cardoons stuffed with minced meat ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee-94791294.html>)

[ minced meat recipe ](<https://www.amourdecuisine.fr/article-recette-viande-hachee-aux-olives-tajine-de-viande-hachee-aux-olives-111430261.html>)

[ meatballs chopped in sauce, baked ](<https://www.amourdecuisine.fr/article-boulettes-de-viande-hachee-en-sauce-au-four-110931632.html>)

![flat mullet of Algerian cuisine](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/Mhawet-plat-de-la-cuisine-algerienne.jpg) [ mhawet, dish of the Algerian cuisine ](<https://www.amourdecuisine.fr/article-mhawet-plat-de-la-cuisine-algerienne.html>)

**dishes and recipes Aid el kebir 2014**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/Couscous-bel-osbane-001_thumb1.jpg)

**Ingredients**

  * sheep's head 
  * kebda 
  * osbana 
  * sheep's feet 



**Realization steps**

  1. bouzellouf 
  2. loubia bel ker3ine 


