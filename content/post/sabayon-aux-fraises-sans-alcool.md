---
title: strawberry sabayon without alcohol
date: '2016-12-25'
categories:
- dessert, crumbles et barres
- idee, recette de fete, aperitif apero dinatoire
- recettes sucrees
- sweet verrines
tags:
- Christmas
- New Year
- Pastry
- Easy recipe
- Holidays
- desserts
- Orange

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/12/sabayon-aux-fraises-sans-alcool-2.jpg
---
![sabayon-to-strawberries-in-alcohol 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/sabayon-aux-fraises-sans-alcool-2.jpg)

##  strawberry sabayon without alcohol 

Hello everybody, 

This non-alcoholic strawberry sabayon is one of my favorite recipes from my kids, they love this smooth, creamy sauce that covers juicy and super-sweet strawberries. 

Sabayon is an Italian dessert which means "to froth". It's a creamy sauce made from sugar, marsala wine (or any other kind of alcohol, wine, liquor or champagne) and egg yolks. So I tried the non-alcoholic version a while ago, and the kids liked it so much that it became the most requested dessert at home. 

so I share with you my strawberry sabayon without alcohol, with orange juice instead of alcohol, in a verrine for a simple version. This dessert is just a treat, consume immediately! 

**strawberry sabayon without alcohol**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/sabayon-aux-fraises-sans-alcool.jpg)

portions:  4  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 100 to 150 grams of strawberries 
  * 2 tbsp. tablespoons sugar 

for sabayon: 
  * 4 egg yolks 
  * 2-3 tbsp. tablespoons sugar 
  * 3 c. orange juice   
this recipe is for 4 small verrines 



**Realization steps** start by preparing the strawberries: 

  1. cut and cut strawberries into medium pieces 
  2. add sugar, mix, cover and place in the fridge.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/sabayon-aux-fraises-sans-alcool-4.jpg)

prepare the sabayon: 
  1. place a small saucepan with a little water over medium heat 
  2. put the egg yolks and sugar in a salad bowl going to the bain-marie (on the pan). 
  3. mix well until there are no more sugar grains at the bottom of the mixture 
  4. add the orange juice whip a little and put the bowl on the bain-marie, make sure that the water is not boiling. 
  5. beat the mixture with a manual whisk, until the creamy foam forms a thick ribbon. 
  6. remove from heat and continue to whisk until the sauce has cooled down. 
  7. place the strawberries in the verrines. 
  8. cover with a generous layer of sabayon sauce. Decorate with your taste with lemon, lime or orange zest (I did not have one!) 
  9. enjoy fresh, and enjoy! 



![sabayon-to-strawberries-in-alcohol-5](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/sabayon-aux-fraises-sans-alcool-5.jpg)
