---
title: baklawa chamiya syrian
date: '2016-07-23'
categories:
- Algerian cakes with honey
- Algerian cakes- oriental- modern- fete aid
tags:
- Algeria
- delicacies
- Confectionery
- Oriental pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/07/baklawa-chamiya-syrienne-4.jpg
---
![baklawa chamiya syrian 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/baklawa-chamiya-syrienne-4.jpg)

##  baklawa chamiya syrian 

Hello everybody, 

Finally, I have some time to post the recipe of the Syrian baklawa chamiya I made for Eid 2016. This is one of the favorite recipes of my husband and I admit that it suits me to prepare this baklawa instead of our [ baklawa constantinoise ](<https://www.amourdecuisine.fr/article-baklawa-constantinoise.html>) which takes a lot of time to complete. 

The baklawa or Syrian chamiya is super easy to do, especially if you have on hand good filo dough trade, because that's what I used this time in my recipe. Next time I will share with you the recipe for filo dough, as I learned from my Sudanese friend. 

**baklawa chamiya syrian**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/baklawa-chamiya-syrienne.jpg)

portions:  25  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients**

  * 12 sheets of filo pastry 
  * 100 gr of crushed toasted almonds 
  * 50 gr of crushed hazelnuts 
  * 50 gr of crushed walnuts 
  * 50 g crushed grilled pistachios   
you can only use almonds 
  * 100 gr of sugar 
  * ½ teaspoon of cinnamon 
  * Orange tree Flower water 
  * 300 gr of butter or as needed 
  * Honey 
  * Crushed pistachios 



**Realization steps** For the stuffing: 

  1. mix dried fruits, sugar, cinnamon, and wet with orange blossom water. Let it rest. 
  2. Assembly of Baklawa: 
  3. brush each sheet of filo pastry generously with butter and put them on the others, do not hesitate to butter so that the leaves hold between them. 
  4. Cut squares of almost 6 cm on the side. 
  5. place a spoonful of stuffing on each square of dough.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/baklawa-chamiya-syrienne-5.jpg)
  6. pick up the 4 corners of each square as an envelope to form a pyramid that covers the stuffing. 
  7. Arrange the baklawa pyramids on a plate, squeezing them tightly together, and pour melted butter over them. 
  8. Bake 30 minutes at 180 ° until the chamia is golden brown. 
  9. on leaving the oven, sprinkle with honey and sprinkle with crushed pistachios. 
  10. Let cool before serving with a good tea 



![baklawa chamiya syrian 3](https://www.amourdecuisine.fr/wp-content/uploads/2016/07/baklawa-chamiya-syrienne-3.jpg)
