---
title: scandalous pie has the orange Bernard's kitchen
date: '2014-01-11'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/tarte-a-lorange-comme-dans-la-cuisine-de-bernard.jpg
---
[ ![scandalous pie has the orange Bernard's kitchen](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/tarte-a-lorange-comme-dans-la-cuisine-de-bernard.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/tarte-a-lorange-comme-dans-la-cuisine-de-bernard.jpg>)

##  scandalous pie has the orange Bernard's kitchen 

Hello everybody, 

yum yum for this scandalous pie has the orange Bernard cuisine! The weather is nice today, but you can not look outside because it's really cold. To avoid getting sick, it's the season of cold snaps, colds, nausea .... and I do not finish. 

I advise you for this season to consume citrus at will, for my part it is because up to date [ citrus salads ](<https://www.amourdecuisine.fr/article-salade-d-agrumes-salade-d-oranges-pamplemousse-mandarine.html> "citrus salad, mandarin orange grapefruit salad") very fresh, or if I do not have all the ingredients, it is the clementine at will ... I do not even find recipes, so it can be tasted in the blink of an eye. 

Today I share with you this recipe of Lunetoiles the scandalous pie with orange Bernard's cooking is a beautiful orange pie with two layers, a very light layer of almond cream, and a layer creamy and unctuous with orange. 

Anyway, if you're looking for the simple version of this pie, I'll give you my recipe for [ Orange tart ](<https://www.amourdecuisine.fr/article-tarte-a-lorange-version-individuelle-tartelette.html> "Orange tart / Individual version: tartlet") , as you can see [ the lemon meringue pie ](<https://www.amourdecuisine.fr/article-tarte-au-citron-meringuee-114713045.html> "lemon meringue pie") , the one that makes its effect where it is ... and also [ the lemon curd pie ](<https://www.amourdecuisine.fr/article-lemon-curd-creme-au-citron-fait-maison.html> "recipe of homemade lemon curd / lemon cream") .   


**scandalous pie has the orange Bernard's kitchen**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/tarte-scandaleuse-a-lorange.jpg)

Recipe type:  Dessert pie  portions:  8  Prep time:  40 mins  cooking:  40 mins  total:  1 hour 20 mins 

**Ingredients** For a pie 28 to 30 cm in diameter: Crispy sweet dough: 

  * 250g of flour 
  * 175 g of soft butter 
  * 50 g of egg 
  * 65 g icing sugar 
  * 1 pinch of salt 
  * vanilla powder 

Cream with almonds and orange: 
  * the zest of 3 untreated oranges 
  * 135 ml orange juice 
  * 3 eggs 
  * 150 g icing sugar 
  * 110 g of soft butter 
  * 130 g of almond powder 

Cream with orange: 
  * 2 eggs 
  * 2 egg yolk 
  * 130 g of sugar 
  * the zest of two untreated oranges 
  * 135 ml orange juice 
  * 130g of sweet butter 
  * 10 g cornflour 



**Realization steps** Begin by preparing the crispy sweet dough by creaming. 

  1. Place the egg, butter, icing sugar, pinch of salt and vanilla powder in the kneader bowl. 
  2. Mix with the leaf (the "k" of kitchenaid or kenwood) to obtain a cream, then add the flour at once. 
  3. Mix just to incorporate it without insisting. 
  4. Put the dough between two sheets of parchment paper, and flour it, then spread it finely. 
  5. Darken a pie circle from 28 to 30 cm by 2.5 cm. 
  6. Put the dough in the fridge and dip the bottom of the dough with a fork. 
  7. Preheat the oven to 180 ° C. 

Meanwhile, prepare the almond and orange cream. 
  1. Place the soft butter, icing sugar and almond powder in a bowl. 
  2. Add the zest of the two oranges (untreated!), The eggs, and the orange juice 
  3. Mix well. 
  4. The cream will certainly make lumps but whatever! 
  5. While cooking, everything comes back in order! 
  6. Pour on the bottom of golden dough. 
  7. And bake again in the oven at 180 ° C for 35 to 45 minutes. 
  8. It must be monitored, but it must be anyway that the evening golden cream, like a quiche ... 
  9. Decreen and let cool on a wire rack. 

Then prepare the smooth cream with orange. 
  1. Put the egg, the yolk, the sugar, the cornflour, the orange zest and the orange juice in a saucepan. 
  2. Cook slowly over low heat, stirring constantly with a whisk. 
  3. When the cream boils, remove from heat. Let cool, then when the cream is at 50 ° C, add the butter in pieces. 
  4. Mix well to incorporate all the butter. 
  5. Let cool 10 minutes, then pour over the pie. 
  6. Place the pie in the refrigerator for at least 2 hours. 



[ ![scandalous pie has the orange Bernard's kitchen](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/tarte-a-lorange.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/tarte-a-lorange.jpg>)

j’espere que cette recette vous plaira, et merci beaucoup a Lunetoiles pour ce delicieux partage, je vous dis a une prochaine recette. 
