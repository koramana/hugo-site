---
title: kefta Algerian cake without cooking
date: '2015-07-12'
categories:
- Algerian cakes without cooking
- Algerian cakes- oriental- modern- fete aid
- sweet recipes
tags:
- Algeria
- Algerian patisserie
- delicacies
- Aid
- Ramadan 2015
- Ramadan
- Aid cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kefta-gateau-algerien-1.jpg
---
[ ![Kefta Algerian cake 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kefta-gateau-algerien-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kefta-gateau-algerien-1.jpg>)

##  Kefta Algerian cake without cooking 

Hello everybody, 

This year, I have no time to make cakes for Aid, because I go to Algeria this period, so why bake cakes and take with me, I already galley to store the luggage, then not the trouble to break the head also to put away the cakes so that they arrive well intact, hihihihi 

I took the opportunity to make simple and quick cupcakes that can be enjoyed in the short evenings of Ramadan, such as [ griwechs ](<https://www.amourdecuisine.fr/article-griwech-gateau-algerien-en-video.html>) , of the [ samsa ](<https://www.amourdecuisine.fr/article-samsa-ou-triangles-farcis-damandes.html>) , [ ktayefs ](<https://www.amourdecuisine.fr/article-khobzet-ktayef-aux-amandes-et-noisettes.html>) , [ bniouen ](<https://www.amourdecuisine.fr/article-bniouen-gateau-sans-cuisson-algerien.html>) , [ shortbread with jam ](<https://www.amourdecuisine.fr/article-sables-a-la-confiture.html>) , but I made small quantities, just enough to eat in 3 or even 4 evenings. 

When to my daughter, for whom I did not have the chance or the time to make these favorite cakes as [ iced charek ](<https://www.amourdecuisine.fr/article-tcharek-glace-corne-de-gazelle-algerien-au-glacage.html>) , or [ chilled arayeches ](<https://www.amourdecuisine.fr/article-arayeche-ou-larayeche.html>) , I made him some **keftas** , she likes these cakes without almond-based cooking, last year, it is she who finished the cake [ harissat el louz ](<https://www.amourdecuisine.fr/article-harissa-aux-amandes-harissat-el-louz.html>) ... and this time, she barely gave me the chance to take a picture of this cake. 

I actually made two versions: a version at [ hazelnut praline ](<https://www.amourdecuisine.fr/article-pate-de-praline-maison.html>) , as you see in the photo, and a version at **praline flan** (the powder not the cooked custard) that I bought in Algeria, that I made in the form of [ maamoul ](<https://www.amourdecuisine.fr/article-maamoul.html>) , that is to say we did not see the prank ... But here it is, she left no room for me to take a picture, hihihih ... because she liked the salmon color that I gave to the dough almonds, as what everything that turns to feminine colors is her, hihihih ... 

In any case, even these kefta, this delicious Algerian cake ancestral did not last long, because the taste of hazelnut praline was well pronounced and we could not stop at a room, moreover it has me much astonished, because at home, generally we do not eat much this cake all covered with a thick layer of almond paste, a small room was enough. 

[ ![kefta Algerian cake](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kefta-gateau-algerien.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kefta-gateau-algerien.jpg>)   


**kefta Algerian cake without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kefta-gateau-algerien-11.jpg)

Recipe type:  Algerian cake without cooking  portions:  30-35  Prep time:  20 mins  total:  20 mins 

**Ingredients** Almond paste 

  * 2 glasses of ground almonds (1 glass = 250 ml) 
  * 1 glass of icing sugar 
  * 1 teaspoon vanilla extract or rose water 

Syrup: 
  * 250 g of crystallized sugar 
  * ½ liter of water 

the joke: 
  * 1 glass of [ hazelnut praline ](<https://www.amourdecuisine.fr/article-pralin-aux-noisettes.html>)
  * ½ glass of biscuits powder 
  * softened butter enough to pick up the dough 
  * Honey (just a little to be sure that the stuffing is not too sweet) 
  * dye: according to taste, you can not even put some. or use only one color. 



**Realization steps**

  1. Prepare the syrup with water and sugar, mix, 
  2. bring to a boil and cook for 15 minutes over low heat until thickened. 
  3. Take off heat and let cool. 
  4. Sift together ground almonds, icing sugar, and vanilla extract 
  5. Wet with the syrup until you obtain a smooth, malleable and fairly firm paste. 
  6. In a little syrup dilute the dyes 
  7. share the almond paste in 3, a large part to leave nature and 2 small parts to color in orange and green by gradually pouring the colored syrup on the almond paste and kneading until obtaining the color desired, 
  8. Cover with cling film and let stand 
  9. Prepare the stuffing of praline by mixing all the ingredients of the stuffing, taste to know if it is not too sweet. 
  10. form pudding with the three colored almond pasta, weave them, then try to make a pudding of almost 6 cm in diameter. (a little lower than the size of the mold you will use (here the mold has maamoul, or moon cake cutter) 
  11. cut slices of almost 4 cm thick 
  12. Spread the stuffing on one of the baking paper or food film, so that it does not stick to the worktop. and cut rounds of almost 6 cm in diameter. 
  13. assemble your cakes, a layer of almond paste, a layer of stuffing, and a layer of almond paste. 
  14. gently place the pieces of cake in the mold and pressed to have the drawing, and have the cakes of the same size. 
  15. Keep the cakes in a hermetically sealed box. 



[ ![Kefta Algerian cake-9](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kefta-gateau-algerien-9.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/kefta-gateau-algerien-9.jpg>)
