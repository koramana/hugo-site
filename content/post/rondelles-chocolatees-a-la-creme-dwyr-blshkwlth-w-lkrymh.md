---
title: Creamy chocolate rondelles "دوائر بالشكولاطه و الكريمه"
date: '2011-06-16'
categories:
- salades, verrines salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/06/cho-four-002_thumb.jpg
---
the recipe in video step here the recipe in Arabic here الوصفة بالعربية the ingredients: - 1 egg - 400 gr of softened butter - 200 gr of icing sugar - 1 sachet of baking powder - 12 tablespoons of cocoa (if your cocoa is bitter as mine dim inuer a little dose) - 600 grs of flour (everything depends on your flour, maybe more, maybe less) Cream: - 120 gr of softened butter - 80 gr of sugar galce - 2 sachets of vanilla - 1/2 glass a & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 2.9  (  16  ratings)  0 

[ ![cho-oven-002](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/cho-four-002_thumb.jpg) ](<https://www.amourdecuisine.fr/article-35955000.html>)

Ingredients: 

\- 1 egg 

\- 400 gr of softened butter 

\- 200 gr of icing sugar 

\- 1 sachet of baking powder 

\- 12 tablespoons of cocoa (if your cocoa is bitter like mine reduce the dose a bit) 

\- about 600 grams of flour (everything depends on your flour, maybe more, maybe less) 

[ ![S7302352](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/S7302352_thumb.jpg) ](<https://www.amourdecuisine.fr/article-35955000.html>)

Cream: 

\- 120 gr of softened butter 

\- 80 gr of icing sugar 

\- 2 bags of vanilla 

\- 1/2 glass of milk 

\- 2 teaspoons of milk powder. 

Icing: 

\- 4 cases of cocoa 

\- 3 cases of icing sugar 

\- 50 grams of butter 

\- 1 chocolate bar (100 gr to 150) 

\- a little water 

[ ![S7302376](https://www.amourdecuisine.fr/wp-content/uploads/2011/06/S7302376_thumb.jpg) ](<https://www.amourdecuisine.fr/article-35955000.html>)

Preparation: 

In a bowl, combine the butter and sugar until a creamy melange is obtained. 

Add the egg, stir well, then add the cocoa and finally the flour mixed with the yeast. 

mix well to have a firm and smooth dough. 

Using a rolling pin, lower the dough to a thickness of 4 mm, on a floured work surface. 

cut circles of 5 cm diameters with a punch piece or a tea glass. 

put them on a tray butter, and bake for 10 to 15 minutes. 

Preparation of the cream: 

Mix the softened butter with the sugar, add the vanilla and the milk little by little, then at the end the milk powder. 

after cooling the cakes, spread the cream on one side, and gather it with another piece of the cake. 

Preparation of the icing: 

in a saucepan, with a bain-marie, mix the cocoa, the icing sugar and the butter., stir until the butter is well melted, slowly add the chocolate in pieces and the water to have a rather liquid glazing but not too much flowing. 

for my part, I prepared a small white ice cream with icing sugar and water, and I put this mixture in a paper bag pocket that I shaped myself, and every time I tropm a cake in the chocolate icing, I trace two lines of white glagaceous and I do as I please. 

good tasting. 
