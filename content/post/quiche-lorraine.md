---
title: Quiche Lorraine
date: '2014-10-28'
categories:
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/quiche-lorraine-facile-et-rapide_thumb.jpg
---
[ ![quiche lorraine easy and fast](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/quiche-lorraine-facile-et-rapide_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/quiche-lorraine-facile-et-rapide_2.jpg>)

##  recipe of quiche lorraine 

Hello everybody, 

Today I share with you the recipe for quiche lorraine, but before posting this delicious and pretty quiche Lunteoiles, I want to make a small confession. 

I have the bad habit of not posting easy and simple recipes, under the pretext that everyone knows them, and above all, that each person has his own way of making these recipes. 

But in fact, with my years in blogosphere, almost 7 years soon, I receive the comments of many people asking me the method of realization of this or that recipe ... I knew with the time, that people like to leave their habits to change from the "family" recipe, or just so, that they never taste or test a recipe that you can believe it's "the recipe everyone knows". 

This is my story with the recipe for quiche lorraine, which I often realize, but with mortadella for me, and that I never post on my blog ... Here is one of my reader asks me the recipe, and by chance Lunetoiles sends me the pictures and the recipe of his quiche lorraine that I share with you today. 

and without keeping you too much, I'll give you the recipe:   


**recipe of quiche lorraine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/quiche-lorraine-facile-et-rapide-3_thumb.jpg)

portions:  8  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 1 puff pastry 
  * 150 g turkey bacon or halal chicken 
  * 3 eggs 
  * 200 ml of liquid cream 
  * 200 gr grated cheese 
  * salt and pepper 



**Realization steps**

  1. Unroll and prick the puff pastry with a fork, 
  2. go for a pie plate. 
  3. Preheat the oven to 200 ° C 
  4. Cook in a skillet, without fat, bacon for 4 to 6 minutes. 
  5. Beat the eggs with the cream, salt of the pepper, 
  6. add bacon and cheese, mix well. 
  7. Bake for 30 to 40 minutes. 



[ ![Quiche Lorraine](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/quiche-lorraine_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/quiche-lorraine_2.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
