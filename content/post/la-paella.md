---
title: the paella
date: '2009-05-26'
categories:
- appetizer, tapas, appetizer
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/05/228744221.jpg
---
you know that it's the first time in my life that I'm doing it and that I'm dropping it, yes you have to be honest do not you? 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/228744221.jpg)

my ingredients were simple: 

  * 1 can of seafood (shrimp, mussels, sepia) 

  * fresh tomato 

  * garlic 

  * onion 

  * thyme 

  * salt, paprika, black pepper, cumin 

  * Red pepper 

  * oil 

  * rice (1 small glass) 

  * water (double the amount of rice) 




![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/228744241.jpg)

in a paella pan, sauté, grated onion, crushed garlic, tomato peeled and cut into small pieces, in oil, add salt, cumin, black pepper, paprika, and thyme . 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/228744421.jpg)

add after the box of seafood (or what you have on hand) let it all go together. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/228744511.jpg)

add after the red pepper cut into strips 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/228744531.jpg)

let it cook a little 

then add the rice and finally the water. 

let everything cook well over low heat. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/228744701.jpg)

serve hot, 

a delicious dish 

I know that it lacked the taste of fresh seafood, but we do with what we have. 

bonne appétit. 
