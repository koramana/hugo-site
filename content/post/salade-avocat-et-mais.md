---
title: avocado and corn salad
date: '2015-04-20'
categories:
- amuse bouche, tapas, mise en bouche
- salades, verrines salees
tags:
- inputs
- Easy cooking
- Buffet
- Tuna
- Vinaigrette
- Ramadan
- accompaniment

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/salade-avocat-et-ma%C3%AFs.jpg
---
![salad-and-avocado-but](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/salade-avocat-et-ma%C3%AFs.jpg)

##  Avocado and corn salad 

Hello everybody, 

Here is a salad full of freshness and color prepared on the occasion of a challenge of recipes with avocado, a recipe for **Sihem F** , who presented a beautiful fresh entry between the fondant of the avocado and the crunchy corn. 

This salad of avocado and the kind of salad that I personally like a lot, from home, I often prepare the same version of the [ avocado salad with tuna ](<https://www.amourdecuisine.fr/article-salade-d-avocat-au-thon.html> "avocado salad with tuna") . Keep these two recipes in a safe corner, as they will be very useful for the coming days, be it during Ramadan, or for a light dinner during the summer, or for picnic and barbecue parties. 

**avocado salad and but**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/salade-avocat-et-ma%C3%AFs-2.jpg)

portions:  4  Prep time:  15 mins  total:  15 mins 

**Ingredients**

  * 2 mature lawyers. 
  * 2 small tuna tins in olive oil. 
  * A cucumber. 
  * 4 servings of cheese or 2 tbsp of cream cheese. 
  * Corn. 
  * a tomato. 
  * olives. 
  * lettuce and cherry tomatoes for decoration and presentation 

Vinaigrette: 
  * olive oil, 
  * salt and white pepper, 
  * Mayonnaise   
[ ![avocado salad but](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/salade-avocat-mais-3.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41620>)



**Realization steps**

  1. Open the avocado by going around the fruits with the knife, 
  2. Turn both sides to open, 
  3. Give the kernel a strong enough punch and turn the knife by pulling the kernel upwards, 
  4. With a spoon, empty the avocado by gently scraping it in a bowl. 
  5. Add tuna, cheese, corn, tomato, cucumber and olives and rectify salt and white pepper (not too much as cheese and tuna are already salted), 
  6. Stuff the avocados with this mixture 
  7. arrange them on a bed of lettuce and continue decorating with tomatoes and olives. 
  8. Sprinkle with vinaigrette just before serving. 



![salad-and-avocado-but-1](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/salade-avocat-et-ma%C3%AFs-1.jpg)
