---
title: custard
date: '2012-12-09'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/mille-feuille-maison.jpg
---
![thousand leaf house](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/mille-feuille-maison.jpg)

##  custard 

Hello everybody, 

The pastry cream was the first cream I made in my life. When I was young, my mother's role was to make the shortbread, while I prepare the pastry cream, to make beautiful fruit pies. 

and to this day, I always make the same custard recipe to garnish my cakes and pies, or especially to make [ homemade millefeuilles ](<https://www.amourdecuisine.fr/article-millefeuilles-fait-maison-mille-feuilles.html>) . 

I also use it often to garnish and stuff my birthday cakes, finally it is the base of my cream chiffon.   


**custard**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/creme-patissiere-1_thumb1.jpg)

portions:  8  Prep time:  15 mins  cooking:  10 mins  total:  25 mins 

**Ingredients**

  * 500 ml of milk 
  * 100 gr of sugar 
  * 2 bags of vanilla sugar 
  * 4 egg yolks 
  * 60 gr of cornflour 



**Realization steps**

  1. Mix the egg yolks, cornflour, sugar and vanilla in a saucepan and whisk well. 
  2. Pour the hot milk over, mix at the same time, and put back on medium heat while stirring constantly with a wooden spoon until the mixture thickens (about 4-5 minutes).   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/creme-patissiere-2_thumb1.jpg)
  3. Allow to cool, covering with a film of food, so that it forms a thick film. 



![custard](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/creme-patissiere_thumb_11.jpg)
