---
title: Banoffee Pie
date: '2013-11-13'
categories:
- appetizer, tapas, appetizer
- diverse cuisine
- Cuisine by country
- idea, party recipe, aperitif aperitif
- salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/banoffee-pie-2_thumb.jpg
---
[ ![banoffee pie 2](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/banoffee-pie-2_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/banoffee-pie-2_2.jpg>)

##  Banoffee Pie 

Hello everybody, 

here is a dessert I always like to make, a banoffee pie, which is an English pastry made with crushed biscuits, mixed with a little butter, covered with a beautiful layer of banana, swallowed with a delicious layer of caramel , all decorated with a beautiful layer of whipped cream ... 

yes I know a lot of calories, it takes three days to sharpen on the treadmill to burn all the calories of this delight ... hihihihi. 

but I will confess, that I will not deprive myself of it, I will not eat a bonfire pie every day, will I? 

in any case, I thank Lunetoiles who saved me from these few extra kilos, hihihihi, but who deprived me of this delicious dessert, hihihi, and she realized it before me ... .. 

**Banoffee Pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/banoffee-pie-1_thumb.jpg)

Recipe type:  dessert, pie, cake  portions:  8  Prep time:  60 mins  cooking:  15 mins  total:  1 hour 15 mins 

**Ingredients** Biscuit base: 

  * 335 g of tea biscuits crumbled into a robot 
  * 125 ml of melted butter 

Toffee: 
  * 125 ml of melted butter 
  * 110 g of brown sugar 
  * 2 teaspoons of vanilla extract 
  * 300 ml sweetened condensed milk 

Whipped cream : 
  * 2 cups whipping cream 
  * 3 tablespoons icing sugar 
  * the grains of a scraped vanilla bean 

forage: 
  * bananas in slices. 



**Realization steps**

  1. Mix the melted butter with the cookies in pieces. 
  2. Press bottom and sides of a slightly greased 22 cm hinge pan. 
  3. Bake in a preheated oven at 170 degrees C for 10 -15 minutes. Remove from oven and cool in the pan on a wire rack. 
  4. In a small saucepan, bring the butter and brown sugar to a boil over low heat until frothy, then add the condensed milk from the heat. 
  5. Bring to the boil again over medium-low heat and cook, stirring constantly for another 3 to 4 minutes until the mixture is lightly amber. Remove from heat and pour into pan on prepared cookie dough. 
  6. Refrigerate for 2 hours or more until complete cooling. 
  7. Add the cold cream and icing sugar to the bowl of an electric mixer and beat until soft peaks form. 
  8. Cut 2 bananas into rings and arrange the banana slices on the toffee. 
  9. Spread the whipped cream on the bananas and garnish the top of the pie with chocolate chips, if desired. 
  10. Refrigerate for about an hour before serving. 



Note in order not to let the caramel layer flow, when you spread it on the bananas do not mix too much, you risk changing its texture.   
so that the whipped cream does not leak, leave at the last moment before serving and decorate your cake with whipped cream. [ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/Banoffee-pie-gateau-au-caramel_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/02/Banoffee-pie-gateau-au-caramel.jpg>)

You can see, preparing this pie by watching this video: 

{{< youtube pGSjSLyo8wo >}} 
