---
title: Apple Tartlets by J. Genin
date: '2016-10-17'
categories:
- gateaux, et cakes
- sweet recipes
tags:
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/10/Tartelettes-aux-pommes-de-J.-Genin.jpg
---
![tart-with-apples-to-j-Genin](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/Tartelettes-aux-pommes-de-J.-Genin.jpg)

##  Apple Tartlets by J. Genin 

Hello everybody, 

Not only these J. Genin apple tartlets are super beautiful, apparently it's a breeze. 

Thank you my dear for coming back to remind me that I did not share this delight with the readers of my blog. So enjoy my dear season of beautiful apples to make these pretty tartlets, it's a breeze. 

![tart-with-apples-to-j-1-Genin](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/Tartelettes-aux-pommes-de-J.-Genin-1.jpg)

**Apple Tartlets by J. Genin**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/Tartelettes-aux-pommes-de-J.-Genin-2.jpg)

portions:  6  Prep time:  45 mins  cooking:  30 mins  total:  1 hour 15 mins 

**Ingredients** For 6 tartlets you need: 

  * apples 
  * 1 roll of thick puff pastry 
  * sugar 
  * melted butter 

For the compote: 
  * 2 apples 
  * 1 tbsp. tablespoons sugar 
  * 1 pinch of vanilla powder 
  * 1 fillet of lemon juice 

Caramel: 
  * 60 g of sugar 
  * 10 g of water 



**Realization steps**

  1. Start with the compote: 
  2. Peel the apples, cut them into small pieces, add the sugar and vanilla, and the lemon fillet, put the whole thing to cook for 5 minutes in a saucepan. 
  3. After 5 minutes of cooking, mix with a blender. 
  4. Cook the compote over low heat for drying for a few minutes.   
This is the tip of the chef J. Genin, it prevents the dough tempera. 

For tarts: 
  1. Peel the apples, empty them to the apple dunper. 
  2. Cut into extremely thin slices with the mandolin. 
  3. Reconstitute 6 apples approximately 4.5 cm high by arranging the slices in descending order, the largest at the bottom, the smaller at the top. 
  4. Preheat the oven to 240 ° C (6 ° C) in a rotating heat mode. 
  5. Unroll the dough and cut 6 rounds with a cookie cutter, of a diameter equal to that of the apples. 
  6. Place the rounds of dough on a plate lined with baking paper and garnish with applesauce up to 1 cm from the edges. 
  7. Arrange the apples on it. 
  8. Brush tartlets with melted butter and sprinkle with powdered sugar.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/tartelettes-aux-pommes.jpg)
  9. Bake for 10 minutes then return the temperature to 180 ° C (tea 6) and continue cooking for 20 minutes. 
  10. Prepare the caramel: In a small saucepan, heat the sugar (60 g) and 1 tbsp. water to obtain a very light caramel. 
  11. Out of the fire, brush quickly the top of the apples with the brush. Let cool before serving. 



![tart-with-apples-to-j-3-genin](https://www.amourdecuisine.fr/wp-content/uploads/2016/10/Tartelettes-aux-pommes-de-J.-Genin-3.jpg)
