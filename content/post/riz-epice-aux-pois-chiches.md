---
title: Spicy rice with chick peas
date: '2017-08-05'
categories:
- Algerian cuisine
- diverse cuisine
- ramadan recipe
- rice
tags:
- Vegetarian cuisine
- accompaniment
- inputs
- Lebanon
- Ramadan
- dishes
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Riz-epic%C3%A9-aux-pois-chiche-1.jpg
---
[ ![Spicy rice with chickpeas 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Riz-epic%C3%A9-aux-pois-chiche-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/12/Riz-epic%C3%A9-aux-pois-chiche-1.jpg>)

##  Spicy rice with chick peas 

Hello everybody, 

Here is a delicious dish of spicy rice chickpeas (basmati rice) that wisely waited in my mailbox, since the month of July, "Ramadan" more precisely. 

When Lunetoiles sent me this recipe for spicy rice with chick peas, I was so overwhelmed in the preparations for my vacation in Algeria, that this recipe came out of my head, until I fell yesterday on the Lunetoiles' album and I ask him what is this delicious rice recipe? And there, she answers that she had sent it to me it was a long time, lol ... How did I forget these beautiful photos, and how do I have not published this recipe with you? 

It's never too late to do it, especially since we're still in 2014, hihihihi   


**Spicy rice with chick peas**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/12/riz-epic%C3%A9-aux-pois-chiches-2.jpg)

portions:  6  Prep time:  45 mins  cooking:  45 mins  total:  1 hour 30 mins 

**Ingredients**

  * 2 to 3 tablespoons of olive oil 
  * 270 g of basmati rice 
  * 2 teaspoons of cumin seeds 
  * 1 C. coffee and half curry powder 
  * 240 g chickpeas cooked drained (or canned) 
  * 1 medium onion sliced ​​thinly 
  * ½ tablespoon flour 
  * 100 gr of raisins 
  * 2 tablespoons chopped parsley 
  * 1 C. chopped coriander 
  * black pepper from the mill 



**Realization steps**

  1. pour 1 tsp. olive oil in a saucepan and heat over high heat. 
  2. pour basmati rice and ¼ tsp. salt and stir. 
  3. gently pour 530 ml of boiling water, lower heat, cover and cook for 15 minutes. 
  4. remove the pan from the heat, lift the lid, cover with a clean cloth, replace the lid and let stand, out of the fire, about 10 min. 
  5. prepare the chickpeas: heat the remaining olive oil in a small 
  6. saucepan over high heat. pour in the cumin seeds and the curry powder, then the chickpeas and ¼ tsp. salt 
  7. stir over the heat for 1 or 2 minutes, just to heat the chickpeas, then transfer to a large bowl. 
  8. heat a deep fryer to 170 ° C 
  9. by hand, mix onion and flour to coat slightly. 
  10. take some onion and put it gently (be careful the projections!) in the oil. let it come back 2 or 3 min, until it is well browned, 
  11. transfer on paper towels to drain and salt. repeat the process several times, until all the onion is fried. 
  12. add the rice to the chickpeas, then the raisins, the herbs and the fried onion. stir, taste and adjust salt and pepper seasoning to your liking. 
  13. serve hot or at room temperature. 


