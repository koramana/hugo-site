---
title: homemade pistachio paste
date: '2015-03-13'
categories:
- jams and spreads
tags:
- Spread
- Express cuisine
- creams
- Algerian cakes
- Cake
- To taste
- Cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pate-a-pistaches-maison-1.jpg
---
[ ![homemade pistachio paste](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pate-a-pistaches-maison-1.jpg) ](<https://www.amourdecuisine.fr/article-pate-de-pistache-maison.html/pate-a-pistaches-maison-1>)

##  homemade pistachio paste 

Hello everybody, 

Here is a recipe that I completely forgot to post. Actually, I just recovered the files of photos that were on the hard drive of my old computer, which gave up rather than expected, hihihih, it must be said that the poor was sucked to the last drop, hihihih. Fortunately for me, that I was able to recover everything, I will post you soon recipes that date from 2012, do not laugh especially on the quality of photos! 

When it comes to this homemade pistachio paste, it's just falling ... I forgot when I did it, but I remember very well that it did not fire, I used a quantity to do my [ pistachio cake ](<https://www.amourdecuisine.fr/article-cake-a-la-pistache-pate-de-pistache.html> "pistachio cake "pistachio paste"") which was just a delight. and the rest of the pistachio paste, was gone on toast, or even toss with a spoon, my children loved it, of course they like everything that is sweet. 

My only mistake when I made this recipe, I realized at the last minute that I did not have enough pistachio, and wanting to add a little, without paying attention I added the pistachio sachet with the skin instead of adding the pruned one, but the stupidity was made, because I had added it directly into the syrup. So I had a pistachio paste of darker color than the original. Still, it did not lose any taste. 

But the color is never going to be like the trade, because I'm sure they add green dye, something I do not want to do. 

**homemade pistachio paste**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pate-a-pisatches-maison-010a.jpg)

portions:  10  Prep time:  20 mins  cooking:  10 mins  total:  30 mins 

**Ingredients**

  * 250g raw pistachios 
  * 125g of sugar 
  * 35g of water 
  * 60g of almond powder 
  * 1 tablespoon of oil 
  * a few drops of bitter almond extract 



**Realization steps**

  1. If you have skinless pistachios, grill them for 10 to 12 minutes at 170 ° C. 
  2. If they are not pruned, first put them in boiling water, to remove the skin by sliding the pistachio between your fingers. 
  3. let the roasted pistachios cool a little. 
  4. Heat in a saucepan with a thick bottom and over low heat, sugar and water. 
  5. When the syrup temperature reaches 121 ° C, pour directly onto the roasted pistachios. 
  6. Mix with a spoon so that all the pistachios are well coated with the syrup. 
  7. on cooling the sugar casing will become whitish and crystallized 
  8. let everything cool down and place them in the bowl of your blinder, or mixer 
  9. Add the oil and bitter almond extract. 
  10. Mix everything in a small amount of time, until you see the desired consistency. 
  11. when the dough is ready place it in an airtight box. 
  12. The dough can be kept long enough. Anyway we can see it well with the smell and the color. 



[ ![homemade pistachio paste](https://www.amourdecuisine.fr/wp-content/uploads/2015/03/pate-a-pisatches-maison-a.jpg) ](<https://www.amourdecuisine.fr/article-pate-de-pistache-maison.html/pate-a-pisatches-maison-a>)
