---
title: pasta with smoked salmon / tagliatelle
date: '2013-01-14'
categories:
- soups and velvets

---
Hello everybody, 

here is a delicious recipe from Aina Sam, a recipe for tagliatelle with smoked salmon (  pasta with smoked salmon  ). (This recipe was part of my [ contest ](<https://www.amourdecuisine.fr/article-concours-de-la-cuisine-express-cuisine-rapide-104439235.html>) ) 

Ingredients: 

  * 400g of tagliatelle 
  * 200g of smoked salmon 
  * 2 shallots 
  * a few drops of lemon (I used the little yellow bottle) 
  * 25 cl of fresh cream 
  * 10g of butter 
  * dried dill 
  * Cayenne pepper 
  * salt pepper 



method of preparation: 

  1. prepare the pasta as you usually do. 
  2. cut the shallots finely, then in a saucepan with the butter brown the shallots, 
  3. Add the lemon juice 
  4. tear / cut the salmon in small strips (I took out the salmon 30 min before the preparation is those he recommended on the package) and add it. 
  5. as soon as the salmon has taken another color, add the cream, pepper and pepper, cook 1 to 2 minutes 
  6. in a plate arrange the tagliatelle previously drained, (do not add fat) pour the sauce and sprinkle with dill 
  7. here it is ready, enjoy. 



Aina Sam's remark:   
it is one of my first recipes that I prepared, I hope you try in any case my husband asks me regularly even if it is not a fan of pasta. 

So, you also have the time to participate in the contest, the recap will be online very soon, so quickly, quickly ... 

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
