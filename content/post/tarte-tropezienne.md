---
title: Tropézienne pie
date: '2016-11-02'
categories:
- Buns and pastries
- sweet recipes
tags:
- Ramadan
- desserts
- To taste
- Breakfast

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tarte-tropezienne-1.jpg
---
Tropézienne pie    
[ ![tropezian pie 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tarte-tropezienne-1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-tropezienne.html/tarte-tropezienne-1>)

##  Tropézienne pie 

Hello everybody, 

At the request of my father today, I prepared a pie of Saint-Tropez, like the one I made during the holy month of Ramadan, and that my father loved so much, especially during the Imsak (s'hor as they say at home , or the moment when he stops eating). 

It's true that I made so many tarts tropézienne, but I never had time to take pictures, because I remember it was the last thing I prepared after a long day in the kitchen and when she was ready, it was already dark, and it was impossible to take pictures. 

This time, I was able to take the pictures of the complete pie, but I have not yet cut the pie Tropezienne, which miraculously rests wisely in the fridge .... but I do not guarantee his waking state tomorrow, hihihiih 

I hope to find a decent slice to make you at least one picture, hihihihi. Tropézienne pie 

In any case, the pie tropézienne is super easy to do, just realize the brioche that you succeed most ... prepared your cream pastry, to make a light cream muslin, not with a lot of butter, and voila. 

I had seen on the show of the best pastry chef last season that Ceryl Lyniac soaked a little brioche with a light syrup so that it was not dry in the mouth, I wanted to do that to see the difference, but I I was afraid of the result, especially since my father never complained that the brioche was dry ... But surely I will do that one day to see how it is.   


**Tropézienne pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tarte-tropezienne.jpg)

portions:  8-10  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients**

  * 250 gr of flour 
  * 90 ml of milk, 
  * 50 g of sugar, 
  * 1 tablespoon of milk powder, 
  * 1 whole egg 
  * 80 gr of butter at room temperature, 
  * 1 sachet of instant yeast, 1 tablespoon. 
  * ½ teaspoon of salt 
  * 1 to 2 tablespoons of orange blossom water. 
  * butter for the mold. 
  * egg yolk and milk for gilding 
  * flaked almonds 
  * [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline-pralinee.html> "cream chiffon praline") prepared from 500 ml of milk and 50 g of butter are added to the pastry cream 



**Realization steps**

  1. In the bowl of the mess, place the flour, sugar and salt and milk powder. 
  2. turn the kneader on at medium speed, add instant dehydrated yeast. 
  3. beat the egg slightly and add while kneading. 
  4. introduce the warm milk, then the orange blossom water, until you have a dough that picks up well. 
  5. knead to have a smooth dough, then introduce soft butter while kneading. 
  6. place the dough in a sheltered place and allow to grow well. (at least 2 hours) 
  7. degas the dough, and place it in a mold of almost 22 cm in diameter. flatten the dough to cover all the mold (the best would be to wrap the baking paper pan)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tropezienne.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41976>)
  8. during this period of rest you can prepare the muslin cream. 
  9. let the dough rise again (until it double or even triple volume)   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tropezienne-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41977>)
  10. brush the surface with egg and milk mixture, and cover generously with slivered almonds 
  11. Preheat the oven to 180 degrees C and cook the brioche for almost 15 minutes, or according to your oven.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tropezienne-2.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41978>)
  12. let the brioche cool completely. 
  13. Cut it in two gently   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/tropezienne-3.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41979>)
  14. stuff the brioche generously with the muslin cream, and let it cool until serving 



## 
