---
title: tuna and capres quiche
date: '2014-04-14'
categories:
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/quiche-au-thon-et-capres-044.CR2_.jpg
---
![tuna and capres quiche](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/quiche-au-thon-et-capres-044.CR2_.jpg)

##  tuna and capres quiche 

Hello everybody, 

This tuna and caper quiche is a recipe that I saw on a TV channel that has just been launched "Samira TV", this channel that is starting to grow, and engage chefs with a good menu rich every time. 

I come back to this quiche, one of the chefs to present, and my mother really wanted to taste it, so even though there were no ingredients, I still realize this recipe for my mother , who really appreciate it, besides that she, even the others, so it's a nice recipe that I share with you with a very clear video of the stages of preparation.   


**tuna and capres quiche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/quiche-au-thon-et-capres-044.CR2_.jpg)

Recipe type:  quiche  portions:  8  Prep time:  15 mins  cooking:  35 mins  total:  50 mins 

**Ingredients** Broken Salt dough: 

  * 250g of flour 
  * 150 g butter at room temperature 
  * 1 young egg 
  * 1 pinch of salt 
  * 1 pinch of pepper 
  * 1 pinch of coriander 
  * 1 pinch of cumin 

prank call: 
  * 1 can of tuna 
  * 200 gr grated cheese 
  * 3 to 4 eggs 
  * 100 grams of capers 
  * 150 ml of milk 
  * salt, black pepper, cumin, coriander powder (or spices of your taste). 



**Realization steps**

  1. preparation of the dough: you can do as on the video: put the flour butter, salt and spices in the tank of the robot, mix, then add the egg yolk and a small amount of water ... 
  2. if not prepare the dough by hand: In a large bowl or bowl, put the flour and butter and sand in the hands. Add the egg yolk, salt and pepper and the rest of the spices then pick up to obtain a smooth and supple paste (you can add a little water if necessary) 
  3. Cover it with a film of food and put it in the refrigerator for 30 minutes. 
  4. Take out your dough. Preheat the oven to 180 ° C, sprinkle your work surface with flour and flatten your dough with a rolling pin. Butter your mold (removable base if possible) 
  5. Wrap the dough on the baking roll and place it on the baking pan. Flank the mold with the dough and remove the excess. 
  6. Prick the pie shell with a fork and precook for 10 minutes to 15 minutes. Take out and let cool the pie shell. 
  7. Fill the pie shell: garnish the pie shell with half of the grated cheese. 
  8. sprinkle over, crumbled tuna, and capers (wash for desalted a little). 
  9. whisk the eggs, milk and spices, and pour over the filling. 
  10. decorate with the remaining grated cheese. 
  11. cook in preheated oven at 180 degrees C for about 20 to 30 minutes, or until quiche turns a nice color. 
  12. let the quiche cool on a rack before cutting it. 


