---
title: the best of "love of cooking" in 2011
date: '2011-12-31'
categories:
- les essais de mes lecteurs

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/8685213-bonne-et-heureuse-annee-20121.jpg
---
![Happy New Year 2012 Stock Photo - 8685213](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/8685213-bonne-et-heureuse-annee-20121.jpg)

Hello everybody, 

a few hours, and it's going to be the new year 2012, and to end this year, I like to share a small recapetulative recipes you love in 2011, so again: 

Happy New Year 2012 

in Algerian cakes: 

[ ![Skandraniette Algerian cakes, good and good](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/skandraniette-gateaux-algeriens-beau-et-bon.160x1201.jpg) ](<https://www.amourdecuisine.fr/article-skandraniette-gateaux-algeriens-beau-et-bon-95854677.html> "Skandraniette Algerian cakes, beautiful and good cooking at soulef") [ Skandraniette Algerian cakes, good and good ](<https://www.amourdecuisine.fr/article-skandraniette-gateaux-algeriens-beau-et-bon-95854677.html> "Skandraniette Algerian cakes, beautiful and good cooking at soulef")

hello everyone, a very nice Algerian cake recipe modern, which is the recipe of skandraniette in bloom, very pretty, and too good the recipe detailed you will find it: despite that I'm back home, but I do not do not tell ... 

[ ![Two-tone cornets with almonds and walnuts, Algerian cake](http://amour-de-cuisine.com/wp-content/uploads/2011/12/cornets-bicolores-aux-amandes-et-noix-gateau-algerien.160x120.jpg) Two-tone cornets with almonds and walnuts, Algerian cake  ](<https://www.amourdecuisine.fr/article-cornets-bicolores-aux-amandes-et-noix-90180007.html> "Two-tone cornet with almonds and walnuts, Algerian cuisine love cake at soulef")   
Hello everyone, I'm in full revision, I have to pass my exam on December 1, that's why I do not go on your blogs, and I do not answer your questions quickly, I'm really sorry. so today, quickly before going out with the ... 

[ ![Cakes without cooking with cereals](http://amour-de-cuisine.com/wp-content/uploads/2011/12/gateaux-sans-cuisson-aux-cereales.160x120.jpg) Cakes without cooking with cereals  ](<https://www.amourdecuisine.fr/article-gateaux-sans-cuisson-aux-cereals-88568129.html> "Cakes without cooking with cereals of love of cooking at soulef")   
hello everyone, an easy recipe is simple, and normally within reach for everyone, it's a cake without cooking, very melting and super good. and as I get a lot of questions and requests from this recipe, I'm mailing it today, ... 

[ ![Arayeche, pistachio larch with Algerian cake](http://amour-de-cuisine.com/wp-content/uploads/2012/06/arayeche-larayeche-aux-pistaches-gateau-algerien.160x120.jpg) Arayeche, pistachio larch with Algerian cake  ](<https://www.amourdecuisine.fr/article-arayeche-larayeche-aux-pistaches-gateau-algerien-87986224.html> "Arayeche, larayeche with pistachios, Algerian cake of love of cuisine at soulef")   
hello everyone, it's the feast of the aid el kebir that is fast approaching, and although it is the sheep festival, as it is said, some families are trying to prepare cakes for this magnificent opportunity. Algerian cakes are always ... 

[ ![Algerian cakes: bow ties](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/gateaux-algeriens-noeuds-papillons.160x1201.jpg) ](<https://www.amourdecuisine.fr/article-les-gateaux-algeriens-noeuds-papillons-84677207.html> "Algerian cakes: bow ties of kitchen love at soulef") [ Algerian cakes: bow ties ](<https://www.amourdecuisine.fr/article-les-gateaux-algeriens-noeuds-papillons-84677207.html> "Algerian cakes: bow ties of kitchen love at soulef")

hello everyone, here is another recipe that I publish you of my Algerian cakes that I prepared for the help el fitr 2011, a very nice recipe that I found on the book of Ben ghzal salma, edition: La feather, holiday cakes. so he… 

[ ![Mchekla with leaves, modern Algerian cakes](http://amour-de-cuisine.com/wp-content/uploads/2011/12/mchekla-aux-feuilles-gateaux-modernes-algeriens.160x120.jpg) Mchekla with leaves, modern Algerian cakes  ](<https://www.amourdecuisine.fr/article-mchekla-aux-feuilles-gateaux-modernes-algeriens-85426355.html> "Mchekla with leaves, modern Algerian cuisine love cakes at soulef")   
Hello everyone, here is another recipe for modern Algerian cakes that I made for 2011, so I've already posted the recipe for butterfly knots, skandraniettes, now the nutella sands, and the sands ... 

in cakes, cake, pies and desserts: 

[ ![Orange and apricot cheesecake](http://amour-de-cuisine.com/wp-content/uploads/2011/12/cheesecake-aux-oranges-et-abricots.160x120.jpg) ](<https://www.amourdecuisine.fr/article-cheesecake-aux-l-orange-et-abricots-63914634.html> "Cheesecake with oranges and apricots of kitchen love at soulef") [ Orange and apricot cheesecake ](<https://www.amourdecuisine.fr/article-cheesecake-aux-l-orange-et-abricots-63914634.html> "Cheesecake with oranges and apricots of kitchen love at soulef")

here is a very very delicious cheesecake that I found on the magazine of one of our supermarkets, the recipe was made from lemon and raisins, but I chose the taste of orange and dried apricots, because my husband does not like grapes ... 

[ ![Muffins with speculoos and dark chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/muffins-aux-speculoos-et-chocolat-noir.160x1201.jpg) ](<https://www.amourdecuisine.fr/article-muffins-aux-speculoos-et-chocolat-noir-91366942.html> "Muffins with speculoos and dark chocolate of kitchen love at soulef") [ Muffins with speculoos and dark chocolate ](<https://www.amourdecuisine.fr/article-muffins-aux-speculoos-et-chocolat-noir-91366942.html> "Muffins with speculoos and dark chocolate of kitchen love at soulef")

hello everyone, a very delicious recipe, in one of my English books, but that was with wafers, so I put in place speculons, and I changed the amount of sugar, not to say that be too sweet, and it was a delight ingredients: ... 

[ ![Barbie Princess Birthday Cake](http://amour-de-cuisine.com/wp-content/uploads/2011/12/princesse-barbie-gateau-d-anniversaire.160x120.jpg) Barbie Princess Birthday Cake  ](<https://www.amourdecuisine.fr/article-princesse-barbie-gateau-d-anniversaire-86764345.html> "Princess Barbie, kitchen love birthday cake at soulef")   
hello everyone, today, my little princess is 4 years old, yes it grew quickly children .... so she asked me for a princess this time, and I went to do haute couture this time .... what do you say, my first dress ... 

[ ![Cheesecake mascarpone / chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cheesecake-mascarpone-chocolat.160x1201.jpg) Cheesecake mascarpone / chocolate  ](<https://www.amourdecuisine.fr/article-cheesecake-mascarpone-chocolat-91867400.html> "Cheesecake mascarpone / chocolate of kitchen love at soulef")   
hello everyone, not at all this little delicacy diet, so do not blame me, it's really greedy, hihihiih for me the solution was to take him to my son's school to enjoy it with them moms around a little tea, hihihiih ... 

[ ![Cupcake has the rose](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/cupcake-a-la-rose.160x1201.jpg) Cupcake has the rose  ](<https://www.amourdecuisine.fr/article-cupcake-a-la-rose-84851829.html> "Cupcake has the kitchen love rose at soulef")   
Hello everyone, here are my first cupcakes, and especially my almost first work with the socket pocket, because I admit that I handle it with difficulty, I still can not make flowers of the same size, but it goes come. so for… 

[ ![The new zealand cake, a ride in the kitchen, and a tag](http://amour-de-cuisine.com/wp-content/uploads/2011/12/the-new-zealand-cake-un-tour-en-cuisine-et-un-tag.160x120.jpg) The new zealand cake, a ride in the kitchen, and a tag  ](<https://www.amourdecuisine.fr/article-the-new-zealand-cake-un-tour-en-cuisine-et-un-tag-84533264.html> "The new zealand cake, a tour in the kitchen, and a kitchen love tag at soulef")

hello everyone, here is a super delicious recipe that I had to pique to participate in the game: a ride in the kitchen, 7 th lap.au which, and following a draw, I had to take a recipe at: Gaelle, and petitbohnium must sting a ... 

in the loaves 

[ ![Garlic brioche](http://amour-de-cuisine.com/wp-content/uploads/2011/12/brioche-a-l-ail.160x120.jpg) Garlic brioche  ](<https://www.amourdecuisine.fr/article-brioche-a-l-ail-90935841.html> "Brioche with garlic of kitchen love at soulef")

hello everyone, a very delicious recipe that I saw on a TV channel, I ran quickly to note the ingredients on the first piece of paper that I found on hand (thankfully it was not the checkbook of my husband, hihihihi) ... 

[ ![Khobz eddar with 3 grains](http://amour-de-cuisine.com/wp-content/uploads/2011/12/khobz-eddar-aux-3-grains-10.160x120.jpg) Khobz eddar with 3 grains  ](<https://www.amourdecuisine.fr/article-khobz-eddar-aux-3-grains-80811114.html> "Khobz eddar with 3 grains of cooking love at soulef")   
Hello everyone, I hope you are well, today I post the recipe for homemade bread ... 

[ ![Focaccia with rosemary and onion](http://amour-de-cuisine.com/wp-content/uploads/2011/12/focaccia-au-romarin-et-oignon.160x120.jpg) Focaccia with rosemary and onion  ](<https://www.amourdecuisine.fr/article-focaccia-au-romarin-et-oignon-62123009.html> "Focaccia with rosemary and onion of love of cuisine at soulef")   
For the reciper in englishsalut everyone, yesterday, on the net I find a beautiful recipe here is the video, a very simple and very easy Focaccia, and especially flavored withRomarin and ... 

[ ![Naans with coriander](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/naans-a-la-coriandre.160x1201.jpg) Naans with coriander  ](<https://www.amourdecuisine.fr/article-les-naans-a-la-coriandre-66684378.html> "Naans to the coriander of kitchen love at soulef")   
Hi everyone, I really like naans, these delicious scabies   
Indian heads, very tender, and full of taste. Yesterday I had prepared a delicious Chorba, and I wanted to accompany him to ... 

in the entries 

[ ![Garantita, or karantika قرنطيطة](http://amour-de-cuisine.com/wp-content/uploads/2011/12/garantita-ou-karantika.160x120.jpg) ](<https://www.amourdecuisine.fr/article-garantita-ou-karantika-94535875.html> "Garantita, or karantika قرنطيطة of kitchen love at soulef") [ Garantita, or karantika قرنطيطة ](<https://www.amourdecuisine.fr/article-garantita-ou-karantika-94535875.html> "Garantita, or karantika قرنطيطة of kitchen love at soulef")

Hello everyone, today my brothers have asked for the guarantee, and I will be frank, I have never eat, nor ever made more my sister had made it before yesterday, so I was very tempted to try it, and I was not there, ... 

[ ![Lahmacun, Turkish pizza لحم عجين](http://amour-de-cuisine.com/wp-content/uploads/2011/12/lahmacun-pizza-turque.160x120.jpg) Lahmacun, Turkish pizza لحم عجين  ](<https://www.amourdecuisine.fr/article-lahmacun-pizza-turque-90286084.html> "Lahmacun, Turkish pizza لحم عجين of kitchen love at soulef")   
hello everyone, I remember on my last trip to Turkey this delicious pizza, large format, that I devoured me and my husband with teeth, a delight that I will never forget, and although it was spicy share report to me, ... 

[ ![Malsouka, Tunisian tagine, with leaves of bricks](https://www.amourdecuisine.fr/wp-content/uploads/2010/11/malsouka-tajine-tunisien-aux-feuilles-de-bricks.160x1201.jpg) Malsouka, Tunisian tagine, with leaves of bricks  ](<https://www.amourdecuisine.fr/article-malsouka-tajine-tunisien-aux-feuilles-de-bricks-88766924.html> "Malsouka, Tunisian tagine, with leaves of cooking love bricks at soulef")   
Hello everyone, a very delicious Tunisian tajine that I prepared last night, for my family, and that we enjoyed, the recipe is very easy, and too good, but you will need time to do it , do not do like me, I ... 

[ ![Mini quiches with carrots](http://amour-de-cuisine.com/wp-content/uploads/2011/12/mini-quiches-aux-carottes.160x120.jpg) Mini quiches with carrots  ](<https://www.amourdecuisine.fr/article-mini-quiches-aux-carottes-84166550.html> "Mini quiches with carrots of love of cuisine at soulef")   
hello everyone, how are you? I hope so, today I share with a super delicious carrot quiche recipe, I recommend this recipe because it is really a real pleasure, I swear to you that my husband who does not like the ... 

in salads 

[ ![Carrot shrimp cocktail verrines and avocado cream](http://amour-de-cuisine.com/wp-content/uploads/2011/12/verrines-cocktail-crevette-carotte-et-creme-d-avocat.160x120.jpg) ](<https://www.amourdecuisine.fr/article-verrines-cocktail-crevette-carotte-et-creme-d-avocat-95379972.html> "Carrot shrimp cocktail verrines and avocado cream of kitchen love at soulef") [ Carrot shrimp cocktail verrines and avocado cream ](<https://www.amourdecuisine.fr/article-verrines-cocktail-crevette-carotte-et-creme-d-avocat-95379972.html> "Carrot shrimp cocktail verrines and avocado cream of kitchen love at soulef")

hello everyone, my neighbor received guests today, and she asked me to prepare a salad, or something like that, I had free choice ... .. in my freezer I had shrimp frozen, avocados, a little cheese, so ... 

[ ![Verrines hummus tomatoes cucumber carrots](http://amour-de-cuisine.com/wp-content/uploads/2011/12/verrines-houmous-tomates-concombre-carottes-310.160x120.jpg) Verrines hummus tomatoes cucumber carrots  ](<https://www.amourdecuisine.fr/article-verrines-houmous-tomates-concombre-carottes-95211639.html> "Verrines hummus tomatoes cucumber carrots love cooking at soulef")   
hello everyone, here are very very good verrines, which I find while browsing the internet, but to be frank I do not remember, if it was on a site or a blog, or a forum, so please if it's your recipe, ... 

[ ![Carrot cucumber salad with yogurt](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/salade-de-concombre-carotte-au-yaourt.160x1201.jpg) Carrot cucumber salad with yogurt  ](<https://www.amourdecuisine.fr/article-salade-de-concombre-carotte-au-yaourt-86830728.html> "Carrot cucumber salad with yogurt of love of cuisine at soulef")   
hello my friends (friends) this salad, I drip it the first time at my friend Sudanese, and I'll be very frank, I'm not a big fan of cucumber, well I succumbed to this delight. it's not the famous tzatziki, ... 

[ ![Salad of radish, apple <br /> earth and its coulis](http://amour-de-cuisine.com/wp-content/uploads/2011/12/salade-de-radis-pommes-de-terre-et-son-coulis.160x120.jpg) Radish salad, potatoes and its coulis  ](<https://www.amourdecuisine.fr/article-salade-de-radis-pommes-de-terre-et-son-coulis-84922214.html> "Salad of radish, potatoes and his sauce of love at soulef")   
hello everyone, here is a very very tasty salad that I prepared for me this morning, because my children had couscous for lunch, so I wanted something light. and a salad is always welcome in these cases. a full salad ... 

in the dishes 

[ ![Tajine of peas and cardoons stuffed with minced meat](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee.160x1201.jpg) ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee-94791294.html> "Tajine of peas and cardoon stuffed with meat chopped of love of cooking at soulef") [ Tajine of peas and cardoons stuffed with minced meat ](<https://www.amourdecuisine.fr/article-tajine-de-petits-pois-et-cardons-farcis-a-la-viande-hachee-94791294.html> "Tajine of peas and cardoon stuffed with meat chopped of love of cooking at soulef")

hello everyone, I have never made recipes Cardon on my blog, just because in Bristol, I never find, then as soon as I fall on it, I take the opportunity to make good recipes, or recipes that I have not eaten since a certain ... 

[ ![Algerian rechta](http://amour-de-cuisine.com/wp-content/uploads/2011/12/rechta-algeroise.160x120.jpg) Algerian rechta  ](<https://www.amourdecuisine.fr/article-rechta-algeroise-91236467.html> "Algerian cuisine rechta at soulef")   
hello everyone, and here is my delicious homemade rechta with a very good white sauce turnip taste, I do not tell you the delight, after a day of fasting, it is a dish that is only savored. so as I told you yesterday ... 

[ ![Cauliflower in white sauce](http://amour-de-cuisine.com/wp-content/uploads/2011/12/chou-fleur-en-sauce-blanche.160x120.jpg) Cauliflower in white sauce  ](<https://www.amourdecuisine.fr/article-chou-fleur-en-sauce-blanche-90903107.html> "Cauliflower in white sauce of kitchen love at soulef")   
hello everyone as I told you, in the previous article, that's how I like to eat the cauliflower, in white sauce, and I assure you that we do not even feel that it's cauliflower in it. so to you the recipe: chunks of chickens ... 

[ ![Fall velvet with sweet potato and squash](http://amour-de-cuisine.com/wp-content/uploads/2011/12/veloute-automnal-a-la-patate-douce-et-la-courge.160x120.jpg) Fall velvet with sweet potato and squash  ](<https://www.amourdecuisine.fr/article-veloute-automnal-a-la-patate-douce-et-la-courge-88167689.html> "Autumn velvet with sweet potato and pumpkin of love at soulef")   
bonjour tout le monde, Ce mois-ci, j’ai l’immense plaisir de faire partie du jury de défi cuisine du site recettes.de dont le thème est : Les Soupes, les Crèmes et les Veloutés et je ne suis pas toute seule, je suis accompagnée de mes amies blogueuses et… 
