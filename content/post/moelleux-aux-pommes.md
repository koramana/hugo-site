---
title: Soft apple cake
date: '2017-10-01'
categories:
- cakes and cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-moelleux-aux-pommes-0451.jpg
---
#  ![Soft apple cake](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-moelleux-aux-pommes-0451.jpg)

##  Soft apple cake 

Hello everybody, 

this [ mellow apple and almond ](<https://www.amourdecuisine.fr/article-30256412.html>) is the recipe number 1 at home, since I realized the recipe since almost now 4, at the very beginning of my blog, I approved it, and I present it as a favorite cake, when I receive guests because I know it's not a cake I'll miss ... 

{{< youtube rahrzXzy0Ro >}} 

![cake-soft-to-apples-052.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-moelleux-aux-pommes-0521.jpg)

**Soft apple cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/gateau-moelleux-aux-pommes-036.CR2_.jpg)

Recipe type:  cake, dessert  portions:  8  Prep time:  15 mins  cooking:  60 mins  total:  1 hour 15 mins 

**Ingredients**

  * 200 ml of fresh cream 
  * 2 glasses of powdered sugar   
(a glass of 150 ml) 
  * 4 eggs 
  * 2 glasses of flour 
  * 1 sachet of baking powder 
  * 1 glass of ground almonds 
  * a few drops of bitter almond extract 
  * ½ glass of oil 
  * To decorate the cake: a small handful of flaked almonds 
  * a little apricot jam   
(optional) 
  * 3 big apples cut in small dice. 



**Realization steps**

  1. Preheat the oven to 180 degrees C 
  2. In the bowl of a robot, put the cream, sugar and eggs and whip. 
  3. add the almond powder and the bitter almond extract. Whip again. 
  4. Gradually add flour and yeast while whisking. 
  5. add the oil. Mix well, 
  6. Add the diced apples, stir and pour the resulting dough into a buttered and floured buttered mold, 
  7. Decorate the top with the flaked almonds. 
  8. cook about 1 hour, watch the cooking! 
  9. As soon as it is cooked, wait for it to cool before unmolding. With a brush, brush with a little apricot jam if you want! 



![cake-soft-to-apples-038.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/gateau-moelleux-aux-pommes-0381.jpg)
