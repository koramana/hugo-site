---
title: Algerian cake / tcharek maassel تشارك لعسل
date: '2013-07-28'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/07/corm-de-gazelle-au-nekkache-009_thumb_1.jpg
---
Hello everybody, 

Here is a delight of the [ Algerian pastry ](<https://www.amourdecuisine.fr/categorie-12135732.html>) , a [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) Honey, **maasal charak** , this cake can be without frosting: [ tcharek elariane ](<https://www.amourdecuisine.fr/article-tcharek-el-ariane-53899937.html>) , or covered with a nice layer of sugar: [ tcharek msakar ](<https://www.amourdecuisine.fr/article-tcharak-msakar-103050711.html>) , or a nice frosting with egg whites, [ iced charek ](<https://www.amourdecuisine.fr/article-soltan-el-meida-tcharek-glace-corne-de-gazel-avec-glacage-55370825.html>) . 

This cake looks a lot like [ kaak nakache ](<https://www.amourdecuisine.fr/article-kaak-nekkache-aux-amandes-82530128.html>) but the form differs, and the addition of honey completely changes its appearance. 

and if you like them [ Algerian cakes with honey ](<https://www.amourdecuisine.fr/categorie-12344741.html>) , visit the category.   


**Algerian cake / tcharek maassel تشارك لعسل**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/corm-de-gazelle-au-nekkache-009_thumb_1.jpg)

Recipe type:  Algerian cake  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients** for the pasta: 

  * 3 measures of flour 
  * 1 measure of cooled melted butter 
  * Vanilla extract 
  * 1 pinch of salt 
  * Orange tree Flower water. 

For the stuffing: 
  * 3 measures of ground almonds 
  * 1 measure of powdered sugar 
  * 1 good c to c cinnamon powder 
  * Orange tree Flower water 

for decoration: 
  * a pincer, nekkache 
  * honey 
  * shiny food 



**Realization steps** Prepare the dough 

  1. mix the given ingredients, collect with the orange blossom water and let rest a few minutes.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/07/corn-de-gazelle-a-nekkache-12_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/07/corn-de-gazelle-a-nekkache-12_thumb.jpg>)
  2. In the meantime, prepare the stuffing by mixing all the ingredients. 
  3. prepare balls of around 30 gr 
  4. form a hollow in the ball and fill it with almond stuffing. 
  5. enclose the ball and form a pudding, its length will be equal to your 6 fingers of both hands together. 
  6. shape a horn with this, and pinch them to your liking with the nekkache clip, place your cakes on a baking sheet. 
  7. to make leather in a preheated oven, and at the exit of the cakes of the oven, put them in hot honey, place in boxes and decorate with a little bright food 



merci pour vos commentaires, et bonne journee. 
