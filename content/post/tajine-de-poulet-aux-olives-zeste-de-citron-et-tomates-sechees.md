---
title: Chicken tajine with olives, lemon zest and sundried tomatoes
date: '2017-05-05'
categories:
- ramadan recipe
tags:
- Healthy cuisine
- Ramadan 2017

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tajine-de-poulet-aux-olives-3_thumb.jpg
---
![Chicken tajine with olives, lemon zest and sundried tomatoes](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/tajine-de-poulet-aux-olives-3_thumb.jpg)

##  Chicken tajine with olives, lemon zest and sundried tomatoes 

Hello my friends, 

a beautiful photo is not it? this delicious chicken tajine with olives, lemon zest and dried tomatoes. A recipe that comes from the sister of my friend Lunetoiles, and frankly, myself I asked him the recipe right now that I saw the photos, because I had a great desire to taste this dish. 

Chicken tajine with olives, zest of lemon and dried tomatoes very delicious, with a smooth sauce, so right now the recipe for you and me ... 

**Chicken tagine with olives, zest of lemon and dried tomatoes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tajine-de-poulet-aux-olives_thumb.jpg)

portions:  4  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 2 small farm chickens cut into pieces or in the absence of chicken thighs that you will take care of cut in half, on one side of pestle, on the other the thigh. 
  * 2 big onions 
  * 3 cloves of garlic 
  * 1 lemon 
  * 1 tablespoon sweet pepper 
  * 1 tablespoon of tagine spice or failing: 1 tbsp. coffee of cumin, 1c of curry coffee or turmeric. 
  * 300 g of green olives (which can be natural or candied according to your taste) 
  * 300 g dried tomatoes 
  * 2 tablespoons chopped coriander 
  * 2 tablespoons chopped parsley 
  * 3 tablespoons of olive oil *** 
  * 1 tablespoon of butter 
  * salt pepper 



**Realization steps**

  1. Peel and chop the onions that you cut into thin slices and small dice, put them on a plate. Peel and crush the cloves of garlic, and put them in a bowl. Rinse the lemon and using a grated board, extract the bark. Then, cut the lemon, and recover the juice. Mix this in the bowl, adding the parsley, the coriander that you have taken care of chopped, the dried tomatoes cut into slices and 1 tablespoon of olive oil. In the bowl, add 2 pinches of salt and 2 pinches of pepper. 
  2. Melt the onions over low heat in a pan with 100 gr of butter, when they start to brown, place the chickens and add the 2 tablespoons of olive oil. Brown chickens over medium heat, turning often; Pour 35 cl of water as well as the sweet pepper and spices and bring to a boil. When the water decreases, add the container of your bowl and add the olives. Lower the heat, cover and cook for about 30 minutes. Do not hesitate to stir often, prick the chicken for better cooking, add a little water if the sauce is too dry. 
  3. Serve chicken tajine with olives, zesty lemon and dried tomatoes very hot, in a tajine dish, decorated with cut lemons. 



***Si vos olives vertes sont sous vide, faites bouillir 1 litre d’eau dans une casserole avec 2 cuillères a soupe de sel ou 1 cuillère a soupe de gros sel, du curry et du persil haché, afin d’enlever l’amertume lié aux produits de conservation. 
