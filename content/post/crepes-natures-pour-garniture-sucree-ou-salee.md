---
title: plain pancakes, for sweet or savory garnish
date: '2017-01-30'
categories:
- crepes, donuts, donuts, sweet waffles

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-natures-21_thumb1.jpg
---
##  plain pancakes, for sweet or savory garnish 

Hello everybody, 

For more than a week now, I have not stopped receiving messages and comments from people looking for a crunchy pancake recipe, or easy pancakes that can be used as a base for a salty recipe or a sweet recipe. I tell you this is the recipe I make. Since Lunetoiles to share this recerre on my blog. I do it all the sauces. The result is just perfect. you can make them either for stuffing with chicken, or just enjoy with a thin layer of jam ... 

These pancakes are ideal for a sweet or salty garnish, an easy and fast recipe for plain pancakes .... 

{{< youtube hxOTDyL_Vj4 >}} 

ingredients: (15 to 20 pancakes depending on the size of your stove):   


**plain pancakes, for sweet or savory garnish**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/crepes-natures-21_thumb1.jpg)

portions:  20  Prep time:  10 mins  cooking:  10 mins  total:  20 mins 

**Ingredients**

  * 250g of flour 
  * 3 eggs 
  * ½ liter of milk 
  * 50 g melted butter + for cooking 
  * 1 pinch of salt 
  * Perfume of your choice 



**Realization steps**

  1. Mix the flour, salt. 
  2. gradually add the milk, 
  3. then the beaten eggs 
  4. then introduce 50 g of melted butter. 
  5. Pass this mixture to Chinese. 
  6. Let it rest. 
  7. cook the pancakes in a non-stick pancake pan buttered lightly. 



et voila une belle selection de recettes que vous pouvez realiser avec ces delicieuses crepes: 
