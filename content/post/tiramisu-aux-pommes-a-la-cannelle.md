---
title: apple tiramisu with cinnamon
date: '2017-01-03'
categories:
- cheesecakes et tiramisus
- dessert, crumbles et barres
- idee, recette de fete, aperitif apero dinatoire
- recettes sucrees
tags:
- Express dessert
- desserts
- Express cuisine
- Fruits
- mosses
- Easy cooking
- verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2017/01/tiramisu-aux-pommes-et-cannelle-4.jpg
---
![tiramisu-to-apples-and-cinnamon-4](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/tiramisu-aux-pommes-et-cannelle-4.jpg)

##  apple tiramisu with cinnamon 

Hello everybody, 

You can see the video of this delicious apple tiramisu with cinnamon: 

**apple tiramisu with cinnamon**

![](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/tiramisu-aux-pommes-et-cannelle-3.jpg)

portions:  4  Prep time:  30 mins  cooking:  10 mins  total:  40 mins 

**Ingredients** for the compote of apples: 

  * 3 to 4 apples 
  * 2 tbsp. brown sugar 
  * vanilla 
  * 80 ml apple juice (or as needed for cooking apples) 
  * ½ c. cinnamon powder 

to soak the boudoirs: 
  * 150 ml of apple juice 
  * 1 cinnamon stick. 

for tiramisu: 
  * 3 eggs 
  * 250 gr of mascarpone 
  * 80 gr of sugar 
  * 1 pinch of salt 
  * 1 little vanilla 



**Realization steps** Start by preparing the cinnamon-flavored apple juice 

  1. place the juice in a saucepan, add the cinnamon stick 
  2. heat a little over medium heat, turn off and let it infuse. 

Prepare the compote of apples: 
  1. in a skillet, place all the ingredients and let reduce until cooking. 

prepare the tiramisu mousse: 
  1. whip the egg yolks and sugar until you have a creamy mixture 
  2. add vanilla and mascarpone, and whip to homogenize. 
  3. in another bowl whisk the egg whites with a pinch of salt. 
  4. add the egg whites to the yellow mascarpone egg mixture, without breaking the egg whites. 

mounting: 
  1. place a little tiramisu mousse in the bottom of the verrines 
  2. add crushed puddings impregnated in apple juice with cinnamon. 
  3. add over a little apple compote 
  4. garnish with a little mascarpone mousse and sprinkle with a little cinnamon powder 
  5. cool until ready to serve. 



![tiramisu-to-apples-and-cinnamon-5](https://www.amourdecuisine.fr/wp-content/uploads/2017/01/tiramisu-aux-pommes-et-cannelle-5.jpg)

et voila la liste des participantes à cette ronde: 
