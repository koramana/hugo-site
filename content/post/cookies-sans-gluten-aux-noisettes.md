---
title: Gluten-free cookies with hazelnuts
date: '2017-09-04'
categories:
- Algerian dry cakes, petits fours
tags:
- Chocolate cake
- Soft
- biscuits

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-7.jpg
---
[ ![gluten free cookies with hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-7.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-7.jpg>)

##  Gluten-free cookies with hazelnuts 

Hello everybody, 

It is not because we are gluten intolerant that we must deprive ourselves of the delights and cakes containing flours and products rich in gluten. Now there are more and more gluten-free products, such as gluten-free flours, or for example in some recipes, replace flour, not almond powder or any other products that do not contain gluten. 

Today, I share with you Lunetoiles' gluten-free hazelnut and chocolate chip cookie recipe. Thanks to you my dear, who makes a great effort to make such beautiful pictures of these delicious recipes that you share with us so generously. 

So for this gluten-free hazelnut cookie recipe, Lunetoiles used gluten-free flour, which she bought online, too bad the link is no longer accessible. 

[ ![gluten free cookies with hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-5.jpg>)   


**Gluten-free cookies with hazelnuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-1.jpg)

portions:  10 to 12  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 200 g of chocolate chips 
  * 110 g hazelnuts cut in half 
  * 150 g of gluten-free flour (http://www.schaer.com/en/good-products/farina/mix-patisserie-mix-c) 
  * 60 g of rice flour 
  * 150 g soft butter (but not melted) 
  * 180 g of brown sugar 
  * 1 egg 
  * 1 tsp. coffee baking powder 
  * 1 tsp. coffee vanilla extract 



**Realization steps**

  1. Pour gluten-free flour, rice flour and baking powder into a large bowl and mix well. 
  2. Crush the butter with a fork in another bowl, then add the sugar. 
  3. Whip until you get a firm cream. 
  4. Add the egg and vanilla extract, then beat again. 
  5. Pour the resulting cream into the bowl containing gluten-free flour and rice flour, then stir for a long time with a wooden spoon. 
  6. Gradually, the dough will come off the bowl. 
  7. If the dough is sticky do not hesitate to add a little more flour without gluten. 
  8. Form a ball. 
  9. Add the chocolate chips and hazelnuts and mix again. 
  10. Preheat the oven to 180 ° C (item 6). 
  11. Take some dough (the equivalent of a ping-pong ball) with an ice cream scoop, form a ball, and then drop it on a 
  12. plate covered with a sheet of parchment paper and press the center to form a cookie. 
  13. Repeat the process, then bake and cook for about 20 minutes. 
  14. Let cool completely before transferring to a rack. 



[ ![gluten free cookies with hazelnuts](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-3.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/cookies-sans-gluten-aux-noisettes-3.jpg>)
