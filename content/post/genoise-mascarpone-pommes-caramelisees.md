---
title: Genoise Mascarpone Caramelized Apples
date: '2013-10-24'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/genoise-mascarpone-pomme-caramilisee-027a_thumb.jpg
---
##  Genoise Mascarpone Caramelized Apples 

Hello everybody 

For the birthday of my dear and dear Papa: long life to you Papa, "ربي يكبرنا في طاعتو", me and the children and despite the distance that separates us, we liked to make a "Birthday cake" to Jeddou (grandfather As my kids call it, and as yesterday was the day of shopping, we are late to go in, and I was tired, but my kids always insisted on making the cake. 

Quick to the kitchen, I prepared a small Genoise Mascarpone Apples Caramelized, ie a sponge cake topped with mascarpone and whole cream mousse, and they told me we want apples over ... .. caramelized then, garnished in the end with grilled flaked almonds, and here is the cake that made the children happy.   


**Genoise Mascarpone Caramelized Apples**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/genoise-mascarpone-pomme-caramilisee-027a_thumb.jpg)

Recipe type:  dessert cake  portions:  8  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients** Genoese: 

  * 3 eggs 
  * 3 tablespoons warm water 
  * 130g of sugar 
  * 120g of flour 

mascarpone mousse: 
  * 250 gr of mascarpone 
  * 300 ml whole cream 
  * 110 gr of sugar 

garnish: 
  * 3 apples 
  * 1 tablespoon of butter 
  * 3 to 4 tablespoons of sugar 
  * flaked almonds roasted 
  * 5 to 6 tablespoons of apple juice (or orange) 



**Realization steps** prepare the sponge cake: 

  1. Beat the egg yolks, warm water and foam sugar. 
  2. Add sifted flour and cornflour 
  3. beat the whites beaten in firm snow with a pinch of salt. 
  4. add to the previous mix 
  5. Pour the dough into a special mold for buttered and floured Genoese pie base. 
  6. Bake at 180 degrees for 15 to 20 minutes. 
  7. Let cool before unmolding. 

the mascarpone mousse: 
  1. whisk the mascarpone with 90 gr of sugar 
  2. put the cream in chantilly with 20 gr of sugar 
  3. mix the two masses without breaking the whipped cream 
  4. water the sponge cake with the apple juice 
  5. pour the foam over it, arrange the surface well 

the filling: 
  1. peel, and clean the apples, cut them into 
  2. place the sugar in a pan to caramelize it, without burning it 
  3. add the apples in pieces, and mix the butter as you go, until you have a nice color 
  4. pour this mixture immediately on the mascarpone mass 
  5. and arrange, it will melt a little cream, which will give it a nice effect of ice cream fondue 
  6. garnish with grilled flaked almonds 
  7. you can serve with a chocolate sauce (2 tablespoons Nutella, melted in 2 tablespoons of boiling water) 



Une recette très simple, mais très très bonne 
