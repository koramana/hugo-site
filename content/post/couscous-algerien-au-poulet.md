---
title: Algerian couscous with chicken
date: '2016-12-01'
categories:
- couscous
- Algerian cuisine
- Dishes and salty recipes
- recipes of feculents
tags:
- Vegetable

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-611_thumb1.jpg
---
[ ![Algerian couscous with chicken](https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-611_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/11/couscous-611_thumb1.jpg>)

##  Algerian couscous with chicken 

Hello everybody, 

The Algerian couscous with chicken is a traditional recipe super loved at home, a very tasty dish, rich in vegetables and presented for cooking every day, or for special occasions. 

The Algerian couscous with chicken is always presented in Algeria by the host of the house, and we never tire of it, especially that it is presented in a thousand and one way, which makes the popularity of this dish, and the special feature of being a number 1 family recipe. 

I wanted to share on this article the photo of the Algerian chicken couscous of my friend Nawel Zellouf, because my photos are from 2007, hihihihih 

**Algerian couscous with chicken**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/couscous-au-poulet.jpg)

portions:  6  Prep time:  40 mins  cooking:  45 mins  total:  1 hour 25 mins 

**Ingredients**

  * 1kg of couscous 
  * 1 kg of chicken leg (you can put any part of the chicken) 
  * 1 to 2 onions depending on the size 
  * 2 to 3 carrots (to your taste, you can put more at least) 
  * 2 to 3 turnips 
  * 2 to 3 zucchini (personally I like a lot) 
  * 1 to 2 potatoes 
  * 1 handful of chickpeas 
  * 2 to 3 cases of canned tomato 
  * salt, pepper, ras el hanout, paprika 
  * 2 peppers 
  * oil 



**Realization steps**

  1. wet and salt the couscous, and let it dry well, 
  2. smear your hand with the oil and roll the couscous, so that after it does not stick.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/19713420.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/19713420.jpg>)
  3. let stand, and prepare the couscous sauce, pass the onion to the robot. put in a pot (couscousier) the oil, the paprika, the grated onion, the slices of chicken, and put on the fire,   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/19713218.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/19713218.jpg>)
  4. then add the turnip cut in 4, and the carrots, and then the potatoes, add the 2 peppers, and then add the water to cover everything, and let it boil. 
  5. When the sauce begins to give steam, put the couscous in the top of the couscous and let evaporate.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/19713498.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/19713498.jpg>)
  6. after a while you will see that the steam starts to come out of the whole surface of the couscous cook 15 to 20 min, then remove the top of the coucoussier, and pour the couscous in a terrine, wet with a little water and work gently with your hands. 
  7. add the zucchini cut in 4, in the sauce, and cook, 
  8. put the couscous back in the top of the couscoussier, after the steam has escaped. cook another fifteen minutes and remove 
  9. pour the couscous into a terrine and add a nice piece of butter in the middle, mix well, and at the same time add the chickpeas to the sauce so that everything cooks well. 
  10. to serve, fill your plate with couscous, garnish it well with the vegetables you have and the chickpea, then drizzle with the sauce, according to your taste. 
  11. place the chicken in the middle, and serve hot. 



![S7301516](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/19713565.jpg) [ ![Algerian couscous with chicken](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/couscous-au-poulet.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/couscous-au-poulet.jpg>)

Bonne Degustation, 
