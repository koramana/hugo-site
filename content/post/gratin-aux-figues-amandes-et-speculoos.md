---
title: gratin with figs almonds and speculoos
date: '2016-08-29'
categories:
- dessert, crumbles and bars

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg
---
##  gratin with figs almonds and speculoos 

Hello everybody, The recipe is super easy to make ... and I'm sure you'll like it a lot. and do not forget if you made one of my recipes, come share your photos on my email below: ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg)

**gratin with figs almonds and speculoos**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/Petits-gratins-aux-figues-amandes-et-speculoos-1_thumb.jpg)

portions:  8  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 10 fresh figs 
  * 60 g of almond powder 
  * 1 eggs 
  * 50g of sugar 
  * 100 ml whole liquid cream 
  * 10 speculoos 



**Realization steps**

  1. Whisk the eggs with the sugar. 
  2. When the mixture is frothy add the almond powder and the cream. Beat again. 
  3. Arrange figs quartered at the bottom of each ramekin and cover with almond cream.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/gratin-aux-figues_thumb.jpg)
  4. Reduce the powdered speculoos and sprinkle on each ramekin. 
  5. Put in the oven for 20-25 min at 160 ° C while watching the cooking. or do it with your thermomix 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
