---
title: dry cakes, chocolate rings
date: '2012-06-09'
categories:
- recettes sucrees
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/cho-four-002_thumb1.jpg
---
[ ![cookies](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/cho-four-002_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35955000.html>)

Hello everybody, 

the [ cookies ](<https://www.amourdecuisine.fr/categorie-11814728.html>) are always present at my house, and despite my children   
prefer wafers, I prepare a sand each time, or dry cakes like [ croquet ](<https://www.amourdecuisine.fr/article-25647952.html> "dry cakes croquet") , and the [ lambout ](<https://www.amourdecuisine.fr/article-halwat-lambout-70875141.html> "dry cakes lambout") ... 

this time, it's a very delicious recipe that I found in a book that I bought in Algeria, almost 3 years ago ... 

he is absolutely delicious. 

Ingredients: 

  * 1 egg 
  * 400 gr of softened butter 
  * 200 gr of icing sugar 
  * 1 sachet of baking powder 
  * 12 tbsp cocoa soup (if your cocoa is bitter like mine, reduce the dose a bit) 
  * 300 grs of flour (everything depends on your flour, maybe more, maybe less) 



[ ![dry cakes with chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/S7302352_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35955000.html>)

Cream: 

  * 120 gr of softened butter 
  * 80 gr of icing sugar 
  * 2 bags of vanilla 
  * 1/2 glass of milk 
  * 2 teaspoons of milk powder. 



Icing: 

  * 4 tablespoons of cocoa 
  * 3 tablespoons of icing sugar 
  * 50 grams of butter 
  * 1 chocolate bar (100 gr to 150) 
  * 1 little water 



[ ![Algerian biscuit](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/S7302376_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-35955000.html>)

Preparation: 

In a bowl, combine the butter and sugar until a creamy melange is obtained. 

Add the egg, stir well, then add the cocoa and finally the flour mixed with the yeast. 

mix well to have a firm and smooth dough. 

Using a rolling pin, lower the dough to a thickness of 4 mm, on a floured work surface. 

cut circles of 5 cm diameters with a punch piece or a tea glass. 

put them on a tray butter, and bake for 10 to 15 minutes. 

Preparation of the cream  : 

Mix the softened butter with the sugar, add the vanilla and the milk little by little, then at the end the milk powder. 

after cooling the cakes, spread the cream on one side, and gather it with another piece of the cake. 

Preparation of icing  : 

in a saucepan, with a bain-marie, mix the cocoa, the icing sugar and the butter., stir until the butter is melted, add gently the   
chocolate in chunk and water to have a rather liquid but not too flowing glazing. 

for my part, I prepared a small white ice cream with icing sugar and water, and I put this mixture in a paper bag that I shaped me   
meme, et a chaque fois que je tropm un gateau dans le glacage au chocolat, je trace deuxc lignes de glagace blanc et je faconne a ma guise. 
