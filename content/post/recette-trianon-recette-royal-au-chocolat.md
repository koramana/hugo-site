---
title: trianon recipe / royal chocolate recipe
date: '2012-12-24'
categories:
- Algerian cuisine
- recipe for red meat (halal)
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/royal-au-chocolat.jpg
---
##  trianon recipe / royal chocolate recipe 

Hello everybody, 

I do not hide the truth, I would have liked to eat a piece, but it's a shame I have only the photos, so without delay I share with you the Lunetoiles recipe. 

**trianon recipe / royal chocolate recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/royal-au-chocolat.jpg)

**Ingredients**

  * First of all, make homemade praline the day before: 
  * Ingredients: 
  * 500 g of almonds 
  * 500 g of hazelnuts 
  * 500 g sugar (me, I use brown sugar, but you can also take a sugar semolina) 
  * 12 cl of water 



**Realization steps**

  1. In a large skillet, roast the nuts for about 10 minutes, stirring occasionally. Book. 
  2. In another pan (large enough), pour in the sugar and wet with water. Cook without touching until the mixture is liquefied and lightly colored. 
  3. Add the roasted dried fruit mix and mix quickly: the caramel whitens and recrystallizes; it is said that it "sand" or that it "mass". 
  4. Cook over medium heat and caramelize for a few more minutes: the darker the caramel, the more the praline will be strong in taste. 
  5. Once the desired color has been obtained, remove from heat and allow to cool completely. 
  6. Place the mixture in the mixer bowl. 
  7. for grains, mix quickly 
  8. a little more, and we get a powder 
  9. mix even more, and we get a paste 
  10. Store the praline grains and powder in an airtight container upside down at room temperature. 
  11. The dough itself is kept in the refrigerator. 



**Dacquois with almonds:** I **ngredients:**

4 egg whites 

100 g of sugar 

100 g ground almonds 

15 g of flour 

[ ![Dax](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/recette-trianon-recette-royal-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/recette-trianon-recette-royal-au-chocolat.jpg>)

Prepare all the ingredients. 

Beat the egg whites until stiff and meringue with the caster sugar. 

Mix with almond powder and flour, using a spatula. 

Pour the mixture into a greased circle and smooth to the desired height (about 1 cm). 

Bake in a mild oven (150 °, thermostat 3/4) for about 30 minutes. 

Remove from the oven and let cool. 

Place the sapwood bottom inside a circle. 

**Crunchy chocolate praline:** I **ngredients:**

**195 g homemade praline paste (prepared the day before)**

**30 g of Pralinoise (chocolate bar with fondant praline from Cote d'Or brand or Poulain 1848)**

105 g of feuilletine (crêpe Gavotte lace roughly crumbled) 

Put the chocolate in a small bowl and melt in the microwave 2 x 30 seconds. 

Relax the praline paste with the robot until you get a very smooth machine. 

Add the melted chocolate and continue to whisk for a few seconds. 

Gently fold the foil and mix by hand with a spoon to coat well. 

Pour on the dacquois and equalize using the back of a tablespoon. 

[ ![crunchy chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/croquant-au-chocolat.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/croquant-au-chocolat.jpg>)

**Chocolate mousse: Ingredients:**

225 gr of couverture chocolate 

1/2 liter of whole cream 

Melt the chocolate in a bain-marie. 

Let the chocolate cool down. 

Whip the whipped cream. 

Then mix the mounted cream with the chocolate to obtain a homogeneous mixture. 

Put the chocolate mousse obtained on the praline chocolate crunch. 

Smooth and froth in refrigerator. 

Decenter the entremet once the mousse "well taken". 

Sprinkle with cocoa before serving. 

Decorate at your leisure. 

thank you very much lunetoiles for this delicious recipe, happy birthday to your brother. 

et merci a vos mes lecteurs et lectrices pour tout vos messages, emails et commentaires, et merci a tous ceux qui continuent a s’abonnez a ma newsletter, gros bisous 
