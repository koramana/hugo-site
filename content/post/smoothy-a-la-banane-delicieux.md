---
title: smoothy has the delicious banana
date: '2014-05-01'
categories:
- juice and cocktail drinks without alcohol
- ramadan recipe

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Smoothy-a-la-banane.jpg
---
[ ![smoothy has the delicious banana](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Smoothy-a-la-banane.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Smoothy-a-la-banane.jpg>)

Hello everybody, 

I really like homemade fruit juices and smoothies, because you can put everything you want in it, and besides, you're sure of the contents, no preservatives, or chemicals, which can be harmful has long term health. 

Personally, the smoothies I make every day, for me and for the children, I do not publish them so far on the blog, because the children ask me them like that with the blow of head, and each one will require the ingredients that they prefer in it. 

So I'm the blinder this mixture, and a mouth wide open waiting next door, to savor this delice as it is fresh, then you imagine if I'll tell him: I'll take pictures ??? !!! 

But we do not blame you, I'll make these smoothies, all good and delicious, just for you, but I do not promise you that you will taste them, unless you make me a surprise visit. 

When it comes to today's smoothie, it's a banana smoothie that Lunetoiles is ready to share with us. 

**smoothy has the delicious banana**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/05/Smoothy-a-la-banane.jpg)

portions:  2  Prep time:  10 mins  total:  10 mins 

**Ingredients**

  * 2 bananas cut in pieces and frozen 
  * 125 ml of milk 
  * 125 g Greek yogurt with vanilla 
  * 1 tablespoon of honey 



**Realization steps**

  1. Put all the ingredients in the blender and mix several times until you get a creamy mixture 
  2. Pour immediately into glasses and decorate with fillets of honey. 
  3. Enjoy! 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
