---
title: the contest of the most beautiful Couscous of Kouky
date: '2009-10-25'
categories:
- the tests of my readers

---
click on the photo to be deriger at kouky, if you want to participate in his game of the best couscous: 

**This contest is open to anyone interested in this theme, whether or not you own a blog**

**You can participate by one or more recipes even already published on your blog previously**

**The recipe is either:**

**posted on your blog and transmitted by link with comment to this post**

**forwarded by email to:** [ **cuisinea4mains@hotmail.fr** ](<mailto:cuisinea4mains@hotmail.fr>) **if you** **do not have a blog (Pseudo + recipe + photo maximum 200 ko)**

**The contest period is 1 month from October 15th**

**The recap of your recipes will be visible on November 16th and the vote will be open from November 17th to November 30th**

**Of course there will be a small surprise gift to the recipe holding more votes!**

**To your couscous!**
