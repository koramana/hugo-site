---
title: crumble bar of peaches and apricots
date: '2017-07-11'
categories:
- dessert, crumbles and bars
- cakes and cakes
- sweet recipes
tags:
- Algerian cakes
- Pastry
- Ramadan
- Ramadan 2015
- To taste
- desserts
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/07/barre-peche-et-abricot-crumble.jpg
---
![peach and apricot crumble bar](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/barre-peche-et-abricot-crumble.jpg)

##  crumble bar of peaches and apricots 

Hello everybody, 

Here is a recipe that I draw from the archives of my blog, this Lunetoiles recipe dates from 2013 peach and apricot crumble bars, apparently Lunetoiles used peach fritters (peach dognuts as they are called here in England) a fruit that I likes a lot, I do not know what's called this fruit: peaches? 

In any case these cupcakes with fresh fruit are just a delight. and with a sandy layer from below, and a layer of crumble from above. These fruit crumble bars (peaches and apricots) are super melting in the mouth, and the hardest in the recipe, will be to cut the fruits to marinate in a little sugar so that they are very juicy, and they give this intoxicating scent to the cake when cooking 

**crumble bar of peaches and apricots**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/barre-peche-abricot-crumble.jpg)

portions:  20  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 4 peaches + 5 to 6 apricots peeled, pitted and diced (firm but ripe) 
  * 150 + 225 g of sugar 
  * 3 tablespoons + ½ tablespoon cornstarch 
  * ½ teaspoon of cinnamon 
  * ¼ teaspoon of nutmeg 
  * ¼ teaspoon of ginger 
  * 2 tablespoons orange juice 
  * 1 teaspoon of orange zest (optional) 
  * 480 g flour 
  * 1 teaspoon baking powder 
  * 1 pinch of salt 
  * 180 g butter, cold and cubed 
  * 1 big egg 
  * 60 ml of buttermilk 
  * ½ teaspoon of vanilla extract 
  * 1 tablespoon cane sugar (optional) 



**Realization steps**

  1. Preheat the oven to 170 ° C degrees. 
  2. In a small bowl, whisk together 150 g sugar, cornstarch, cinnamon, nutmeg and ginger. 
  3. In a large bowl, mix peach and apricot dice with orange juice and orange zest. 
  4. Sprinkle with sugar on the peach and apricots mixture and mix to coat, set aside. 
  5. In a large bowl, mix together the flour, baking powder and salt. 
  6. Cut the butter into the flour mixture with a fork until the mixture looks like coarse bread crumbs. 
  7. In a small bowl, whisk together the egg, buttermilk and vanilla extract. 
  8. Pour the egg mixture into the flour mixture and stir to mix well. 
  9. Add 225 g sugar and stir until you get a sandy mixture. 
  10. Spread out and press half of the crumb mixture into a layer in a greased 33x22 cm baking dish. 
  11. Mix the peach / apricot mixture one more time and pour on the basecoat and spread in an even layer (spread the juice evenly too). 
  12. Sprinkle the remaining crumb mixture over the peaches and apricots in one layer. Sprinkle evenly with cane sugar (optional). 
  13. Bake in preheated 45-50 minutes until golden brown. 
  14. Serve hot with optional vanilla ice cream or let cool and cut into bars 
  15. You can also serve them cold with sweet whipped cream. 
  16. Store the bars in an airtight container in the refrigerator. 



[ ![peach and apricot crumble bar](https://www.amourdecuisine.fr/wp-content/uploads/2015/07/barre-peche-abricot-crumble-001.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/07/barre-peche-abricot-crumble-001.jpg>)
