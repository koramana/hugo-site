---
title: french toast
date: '2018-03-27'
categories:
- Unclassified

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/03/banana-french-toast-001_thumb.jpg
---
##  french toast 

Hello everybody, 

the French baguette is one of the best breads you can eat. When it's grilled, the taste is irresistible, 

If we add to the top of that bananas coated in brown sugar, this recipe becomes just heavenly, to taste it, or for breakfast, and I promise you will succumb to this delight big or small. This recipe can become a must with your hot coffee. 

![french toast](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/banana-french-toast-001_thumb.jpg)

**french toast**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/frensh-toast-110_thumb.jpg)

**Ingredients**

  * a baguette. 
  * 4 eggs 
  * 200 ml thick cream 
  * 120 ml of milk 
  * ¼ glass of sugar 
  * 1 tablespoon cinnamon 
  * 1 teaspoon of vanilla 
  * 1 big banana 
  * 6 tablespoons brown sugar 
  * 1 tablespoon of butter 
  * powdered sugar 
  * Maple syrup. 



**Realization steps**

  1. Cut the baguette into slices, about 1.5 cm thick. Make sure three slices fit well in your molds or ramekins. butter the mussels. 
  2. In a large bowl, whisk together eggs, cream, milk, sugar, cinnamon and vanilla until smooth.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2013/03/banana-frensh-toast_thumb.jpg)
  3. place the slices of bread in this liquid, and allow to absorb well for almost 30 min, place each 3 slices in the greased molds. 
  4. slice the banana between 12 and 18 slices, depending on the number of mussels, because between each slice of bread, we will place 3 slices of banana. dip the banana slices in the remaining sauce, then coat in the brown sugar. 
  5. then place each three rings between two pieces of bread. 
  6. then whip the butter and maple syrup, to have a butter cream, which you will distribute on your device. 
  7. let it sit for an hour, or even better for one night, the result will be even better. cook in a preheat oven for 25 min at 180 degrees C. at the oven outlet gently sprinkle with powdered sugar and consume lukewarm. 


