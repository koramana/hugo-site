---
title: recipe baghrir / thousand holes
date: '2014-01-10'
categories:
- appetizer, tapas, appetizer
- Healthy cuisine
- idea, party recipe, aperitif aperitif
- ramadan recipe
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-014_thumb.jpg
---
[ ![Algerian baghrir](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-014_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-014.jpg>)

##  recipe baghrir / thousand holes 

Hello everybody, 

Here is a beautiful **baghrir / thousand holes, hihihihi,** a delight of the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , which I prepared with my bread machine. 

Generally baghrir is done by hand, a recipe with long halogen as traditionally prepared, taking all the time between small mixtures and the addition of water from time to time until having the perfect texture of baghrir. 

As I told you earlier, I prepared the baghrir in my bread machine, I added the warm water gently, I used the pizza cycle 2 times, but I had pancakes very very light and too good especially. I will not stop at this essay, hihihih 

**recipe baghrir / thousand holes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-014_thumb.jpg)

portions:  10  Prep time:  40 mins  cooking:  10 mins  total:  50 mins 

**Ingredients**

  * 3 glasses of fine semolina 
  * 1 glass of flour 
  * 1 tablespoon of oil 
  * ½ teaspoon of baking powder 
  * an instant yeast soup 
  * ½ teaspoon of salt 
  * 1 glass of milk 
  * some water 



**Realization steps**

  1. in the bowl of the bread maker, add all the ingredients in the order recommended. 
  2. start the pizza program 
  3. add a glass of warm water gradually (see less than a glass) 
  4. add the glass of warm milk gradually 
  5. the dough must be very smooth, otherwise add gently warm water 
  6. if the kneading cycle comes to an end, and the dough is not the desired consistency, restart the program. 
  7. let it sit well and double the volume 
  8. remove the bowl from the bread maker, and heat the crepe maker like me or use a crepe pan   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2011-04-25-baghrir_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/2011-04-25-baghrir_thumb.jpg>)
  9. pour the equivalent of a good ladle and spread gently to have a nice circle 
  10. cover with the top of a couscoussier, or a lid very high 
  11. When the holes are well formed on the surface of your pancakes, remove the lid, and let the pancake take a nice color. 
  12. to serve, melt butter, sprinkle the pancakes one by one, and sprinkle with sugar according to your taste 



[ ![baghrir 018](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-018_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/baghrir-018.jpg>)

merci pour vos visites, et vos commentaires, bonne journee 
