---
title: chocolate tart and strawberries
date: '2017-10-02'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-au-chocolat-et-fraises-1-copie-11.jpg
---
![tarte au chocolate-and-strawberry-1-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-au-chocolat-et-fraises-1-copie-11.jpg)

##  chocolate tart and strawberries 

Hello everybody, 

a sublime tart, full of color and sweets, a fondant chocolate ganache, which covers a beautiful strawberry jelly. 

it's a recipe for the fairy Lunetoiles, which she has shared with me for a while, but I was waiting for the strawberry season to put it online, I hope I'm not too early .... 

![tarte au chocolate-and-fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-au-chocolat-et-fraises1.jpg)

**chocolate tart and strawberries**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-choco-fraises1.jpg)

Recipe type:  pie dessert  portions:  8  Prep time:  60 mins  cooking:  45 mins  total:  1 hour 45 mins 

**Ingredients** [ The cocoa sand dough ](<https://www.amourdecuisine.fr/article-pate-sablee-au-cacao-fond-de-tarte-au-cacao.html> "cocoa sand pie / cocoa pie") . Garnish : 

  * 500 g strawberries 
  * 50 g of sugar 
  * 1 sachet (7 g) gelatin powder or 4 gelatin sheets 
  * 2 tablespoons lemon juice (to dissolve the powdered gelatin) 

The ganache: 
  * 200 ml of cream 35% 
  * 210 g dark chocolate with 64% cocoa 
  * 1 tablespoon of butter 



**Realization steps**

  1. prepare the pate sande following this recipe: cocoa pate 
  2. after cooling the dough, prepare the filling. 
  3. Swell the gelatin powder in the lemon juice. Book. (or dip the gelatin sheets in cold water) 
  4. Reserve 2 large strawberries for the decoration and mix the rest with the sugar until you obtain a homogeneous coulis. 
  5. Heat the grout to the boiling point and add the gelatin that has been previously melted for 20 seconds in the microwave (or add the sheets of dewatered gelatin). Mix well. 
  6. Pour the filling into the cocoa pie shell and refrigerate until the filling is set (about 2 hours). 

The ganache: 
  1. Break the chocolate into small pieces and put them in a small bowl. 
  2. Heat the cream to the boiling point and pour over the chocolate. 
  3. Wait a minute before mixing. Add the butter and mix until it is melted. 
  4. Pour the ¾ of the ganache over the strawberry filling and refrigerate. 
  5. With the electric mixer, whip the remaining quarter of the ganache until you get a frothy and clear texture. 
  6. Garnish a shower bag with this preparation and make grids on the pie. 
  7. Decorate with pieces of strawberries and mint leaves. 



![tarte au chocolate-and-strawberry-001.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-au-chocolat-et-fraises-0011.jpg)

{{< youtube MeO_0zWn5Xw >}} 

![choco-pie-fraises.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-choco-fraises1.jpg)

if you only like chocolate, you can see the [ chocolate pie ](<https://www.amourdecuisine.fr/article-tarte-au-chocolat-115954210.html>) : 

[ ![chocolate pie 012.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-au-chocolat-012.CR2_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-au-chocolat-115954210.html>)

and if you only like strawberries, you can see the [ Strawberry tart ](<https://www.amourdecuisine.fr/article-tarte-aux-fraises-115328651.html>) : 

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/tarte-aux-fraises-a_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-tarte-aux-fraises-115328651.html>)
