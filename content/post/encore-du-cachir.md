---
title: still to hide
date: '2007-11-13'
categories:
- Algerian cuisine
- Dishes and salty recipes

---
** INGREDIENTS:  **

5 EGGS   
6 C to S FINE SEMOLINA   
2 CUBES OF BROTH   
2 PORTIONS OF CHEESE (cow style laughing)   
1 GOUSSE OF AIL DEGERME   
1 GLASS OIL (mine is 120ml)   
1 PINCEE OF RAZ EL HANOUT   
80 GR HATCHED MEAT (I put some chicken breast)   
PEPPER CUMIN (according to his desire)   
2 C to S TOMATO CONCENTRATE   
A LITTLE PAPRIKA   
in a blender mix all ingredients (start with liquids)   
Pour this mixture into a buttered mold * (see explanation below)   
put this mold in the bottom of a couscous pot or a pot high enough and pour water up to the height of the preparation (a kind of bain-marie   
place a lid on the pot and simmer for about 1 to 1 hour 15   
let cool, unmold and slice 

* the mold   
il existe au Maroc un moule spécial mais n’en ayant pas j’ai opté pour un pot à épice en porcelaine trouvé chez CASA d’une hauteur de 14 cm ( ne pas prendre moins haut ) et de 11 cm de diamètre et comme ce moule doit etre fermé et comme le pâté monte à la cuisson ( voir photo ) j’ai recouvert celui ci de papier aluminium que j’ai fixé avec un élastique (systeme D mais très efficace et sans faille à chaque fois ) 
