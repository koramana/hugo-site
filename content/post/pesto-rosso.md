---
title: pesto Rosso
date: '2018-04-02'
categories:
- appetizer, tapas, appetizer
- dips and sauces
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/pesto-rosso-045.CR2_thumb1.jpg
---
Hello everybody, 

here is a delicious sauce, pesto rosso, which is an Italian sauce, ideal as an accompaniment. 

this sauce, made with candied tomatoes, is very very nice, I did not expect this taste frankly, I thought it was going to be a little acid, but in fact, it was too good, I had even spread on pieces of bread, and I really like the taste. 

it's going to be super delicious in tapas or aperitifs, or so on pasta, finally I'll explore recipes with this super delicious sauce. 

**pesto rosso, italian sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/pesto-rosso-045.CR2_thumb1.jpg)

Recipe type:  sauce dip  portions:  8  Prep time:  5 mins  cooking:  1 min  total:  6 mins 

**Ingredients**

  * 100 g dried tomatoes   
(Tomatoes confit with olive oil), 
  * 30 g pine nuts 
  * 3 garlic clove, 
  * 50 ml of olive oil.   
(or more according to your taste) 



**Realization steps**

  1. grill the pine nuts without burning them 
  2. cut the dried tomatoes with a scissors. 
  3. clear the garlic cloves 
  4. place everything in the bowl of the blender, and reduce to puree, making small strokes. 
  5. add the olive oil little by little, while mixing the pesto, until having the desired consistency. 
  6. keep in a small jar. 



{{< youtube Fujc8CS3l1k >}} 

if not I also pass you the recipe of the [ basil pesto ](<https://www.amourdecuisine.fr/article-pesto-au-basilic-fait-maison-101909328.html>) : 

[ ![basil pesto](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/pesto-au-basilic_thumb.jpg) ](<https://www.amourdecuisine.fr/article-pesto-au-basilic-fait-maison-101909328.html>)
