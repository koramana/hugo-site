---
title: baked chicken drumsticks
date: '2017-12-11'
categories:
- chicken meat recipes (halal)

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pilons-de-poulet-au-four-1-683x1024.jpg
---
![baked chicken drumsticks 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pilons-de-poulet-au-four-1-683x1024.jpg)

##  baked chicken drumsticks 

Hello everybody, 

When I announce to my children that for dinner we will have chicken pestles in the oven, and we come every 15 minutes to ask me the question is the dinner is ready. Because frankly these chicken drumsticks are so good, tasty, tender and especially when they are presented with mashed potatoes sprinkled with the sauce of cooking chicken drumsticks.   


To cook these chicken pestles in the oven, I put them in a cooking bag. Frankly, not only that in this cooking, we do not need to submerge the pieces of meat in the oil, because the chicken drumsticks cook in their juices, In addition this juice, is just a killer, very rich in flavors and goes well with a lot of dishes. 

Here is my little video for this recipe of oven-baked chicken drumsticks with a creamy mashed potato: 

**baked chicken drumsticks**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/10/pilon-de-poulet-au-four-030.CR2_thumb1.jpg)

Recipe type:  dish  portions:  4  Prep time:  20 mins  cooking:  40 mins  total:  1 hour 

**Ingredients**

  * chicken drumsticks (depending on the number of people) 
  * a nice onion 
  * 2 cloves garlic 
  * 2 fresh tomatoes, otherwise ½ can tomato in pieces. 
  * some branch of thyme 
  * salt, pepper and coriander powder 
  * paprika, ras el hanout or tabel. 
  * 1 C. mustard 
  * 2 tablespoons of olive oil 



**Realization steps**

  1. in a salad bowl, place the chicken drumsticks 
  2. add onions and garlic cut into strips. 
  3. add all the spices, mustard, oil and tomatoes, 
  4. mix well, cover the bowl with clingfilm, and place in the fridge for at least 2 hours. (you can do it a night in advance, the drumsticks will only be better) 
  5. after the marinade time, put the chicken drumsticks and marinade in a cooking bag, rinse the bowl with a little water that you can add in the bag. 
  6. close the baking bag well, place it on a baking tin, and arrange the chicken pestles well so they are level. 
  7. place the tray in a preheated oven at 210 ° C and cook for at least 40 minutes. If the bag inflates when cooking, prick it with a toothpick to let the cooking steam escape 
  8. after 40 minutes of cooking, remove the chicken drumsticks from the bag by pouring the contents into the baking tin, arrange well. Turn on the oven grill, and put the pan back in the oven, to give a nice golden color to the chicken drumsticks. 
  9. when the pestles have taken a nice color, remove from the oven, place the cooking sauce in the Chinese to get only the sauce. 
  10. You can present the chicken drumsticks and their sauce with a good mashed potato. 



![baked chicken drumsticks 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/10/pilons-de-poulet-au-four-2.jpg)
