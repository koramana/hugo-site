---
title: salt pie without dough with chicken and potato
date: '2015-05-11'
categories:
- amuse bouche, tapas, mise en bouche
- cuisine algerienne
- cuisine diverse
- pizzas / quiches / pies and sandwiches
- chicken meat recipes (halal)
- recipes of feculents
tags:
- inputs
- Vegetarian cuisine
- Healthy cuisine
- Easy cooking
- Economy Cuisine
- Ramadan
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/05/quiche-sans-pate-4.jpg
---
[ ![quiche without pasta 4](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/quiche-sans-pate-4.jpg) ](<https://www.amourdecuisine.fr/article-tarte-salee-sans-pate-au-poulet-et-pomme-de-terre.html/quiche-sans-pate-4>)

##  salt pie without dough with chicken and potato 

Hello everybody, 

Here is a very nice salty pie all delicious for those who do not want to make the pastry shortbread or broken, or even who have no puff pastry. This pie is very nice, because the crust is actually mashed potatoes, is not it a good idea? 

It's a recipe from my friend **Wamani** , try it and tell me your opinion, I tried the crust for mushroom tartlets yesterday at dinner, it was a treat, super fondant in the mouth, in any case for me the taste was very similar to that of the [ rolled potato with minced meat ](<https://www.amourdecuisine.fr/article-roule-de-pomme-de-terre-a-la-viande-hachee.html> "rolled potato with minced meat") .   


**salt pie without dough with chicken and potato**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/quiche-sans-pate-6-300x243.jpg)

portions:  4  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * for the crust: 
  * 2 to 3 potatoes according to the size of the mold 
  * salt and black pepper 
  * the joke: 
  * 1 onion 
  * cooked chicken breast cut into pieces 
  * [ cheese bechamel sauce ](<https://www.amourdecuisine.fr/article-bechamel-rapide-et-facile.html> "quick and easy bechamel sauce video")



**Realization steps**

  1. put the potato to cook in salt and pepper water 
  2. drain the potato well, and leave on paper towels so that no more water remains. [ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/quiche-.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=41938>)
  3. squeeze the mashed potato and cover the tart pan well 
  4. prepare the stuffing, fry the onions in a little oil, 
  5. cover the surface of the potato with the pieces of chicken, and onions 
  6. cover with bechamel sauce. 
  7. and place in a preheated oven at 180 degrees C for 15 minutes. 



[ ![quiche without pasta 5](https://www.amourdecuisine.fr/wp-content/uploads/2015/05/quiche-sans-pate-5.jpg) ](<https://www.amourdecuisine.fr/article-tarte-salee-sans-pate-au-poulet-et-pomme-de-terre.html/quiche-sans-pate-5>)
