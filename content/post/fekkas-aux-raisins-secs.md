---
title: raisins with raisins
date: '2012-11-25'
categories:
- Buns and pastries
- Mina el forn
tags:
- biscuits
- Christmas
- Cakes
- Algerian cakes
- Holidays
- To taste
- fondant
- Cookies

---
Hello everybody, 

here is a delicious [ dry cake ](<https://www.amourdecuisine.fr/categorie-11814728.html>) "The crunchy" or croquet or also Fekkas, which looks a bit like biscotti. 

  
Ingredients:   


  * 6 eggs 
  * zest of a lemon 
  * 1 cup of vanilla coffee 
  * 300 grs of crystallized sugar (I do not like them too much sugar, I put 280 gr) 
  * 500 ml of table oil 
  * 2 tablespoons of baking powder 
  * 1 nice handful of raisins, which you let swell in a little water (you can put everything you like, almonds, peanuts ...) 
  * flour (allow at least 1 kg, depending on your flour) 



for gilding: 

  * 1 egg 
  * 1 cup of milk 
  * 1 cup of coffee (or a little instant coffee) 
  * 1 pinch of sugar 
  * 1 pinch of vanilla 



method of preparation: 

  1. in a large salad bowl, beat the eggs well, add the vanilla and icing sugar, slowly add the sugar, then the oil, in a net. 
  2. continue to beat. 
  3. then add a little flour, then the baking powder, then add the raisins, and then continue to add your flour, until you get the soft and malleable dough, do not put too much flour so that you will not have not crunchy too hard. 
  4. even if to form the chopsticks, you find that the dough sticks to the hand, flour your hands for work, or your hands must be well oiled. 
  5. in a baking tray, place your chopsticks, spacing them well from each other. as in the photos above. garnish your chopsticks according to your taste, then brush with the egg mixture (you can put only one egg for the gilding, but I add the vanilla, to no longer smell the eggs and the other ingredients it's to have a nice crunchy color) 
  6. the choice of the garnish after depends on your taste: sugar, coconut, ground almonds, peanuts, vermicelli in chocolate or in color ...... the list is very long. 
  7. cook your crisp in a pre-heated oven at 180 degrees C. 
  8. dice the output of your cake from the oven, cut the cakes, lozenges to the size that you like, for me it is between 1.5 and 2 cm wide.to have more crispy, you can put your crunchy, on the side , in a tray and put back in the oven, for a small gilding from above and from below. 

allow to cool well, then put in a hermetic box, can be preserved a long duration, and even can be frozen, for use in case of unforeseen visits. 

good tasting. 




![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)

click on this picture, if you want to subscribe to the newsletter, and each time receive a new article posting alert on your email, remember, check the two boxes, otherwise the registration is not valid, then confirm your registration, we follow the link that will be on your email. 

merci. 
