---
title: chocolate and chestnut fondant
date: '2015-12-23'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- dessert, crumbles and bars
- Chocolate cake
- cakes and cakes
tags:
- Soft

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/fondant-chocolat-marrons-1.jpg
---
[ ![brown chocolate fondant](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/fondant-chocolat-marrons-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/fondant-chocolat-marrons-1.jpg>)

##  chocolate and chestnut fondant 

Hello everybody, 

Another recipe of Lunetoiles that I completely forgot in the archive of my emails. A fondant with chocolate and chestnuts, a nice recipe of the season, is not it? 

I'll try to find some chestnut puree, we'll go to the supermarket next time, but I'll be honest with you, since my return from my vacation, I'm still not cooking seriously, I'm really taken. 

The night before sleep, I do a lot of projects, I'm going to do that, and this, and go here, and come back from laba ... projects to say that voila a busy agenda. But when I wake up, everything evaporates, the time to prepare my two other children at school, to do their lunch box, already it is the galley. 

A voice that says to me of its corner: I do not want cheese in my sandwich, and another: do not put me of the [ mayonnaise ](<https://www.amourdecuisine.fr/article-mayonnaise-fait-maison-rapide-et-facile.html> "homemade mayonnaise fast and easy") , then again: can I have chips? put me a "fruit shoot" and a "YoGo" ... The other from his corner: Mom, do I get a chocolate ??? ... ah my daughter if I am, it's not going to be a "Lunch box" but rather "sweets box", she has the sweet beak it, and I do not want to get used to it, because surely she will regret it when she is going to be big, and she wants to be "size 10" English, as all girls want (I'll say everyone) now. 

So, after having their lunch box and after having prepared a good hot tea to start the day as I programmed, I hear a sweet voice at the end of the hall, which makes "gaga", "Mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm Yesiiiiiiii, sometimes: yes I'll take my bottle up to leave no drop, and sometimes: no I'm going to sulk, I'm no longer a baby, OK! 

That means he wants his cereals, or porridge, no big deal, the bottle is already ready, so I prepare his cereal with this milk, I feed him. Of course you have to make all possible sounds and gestures to make baby eat the last spoonfuls. 

I look at the time and it's already 9:30 ??? !!! Another panic, tidy the rooms, of course I do not tell you the state of the rooms, fortunately here in England is the uniform at school for children, otherwise I would have right to all the wardrobe outside, lol 

Once all this cleaning done, I still watch the time and it is already 10:30 am, I'm not a turtle at work, but rather an "OCD" as the English say, at home, everything must be perfect, and everything in its place. Just when everything is done, baby begins to do his little scenes, time to enjoy a yogurt, again 30 minutes, more times, to calm him down and put him to bed ... So already 11am. 

Fortunately again, that among the English, we will not pick up the children of the school, you remember their "Lunch box"? Well that's what they eat at lunchtime ... Of course, sometimes it's a meal in the canteen, to know if they like the menu, something that happens rarely, because they are used to the dishes of mom, lol ... 

So, I start to concoct something in the kitchen, ah we must not forget that I must also do some things for baby ??? 

Once finished, it is already half past twelve, my husband is there for his lunch. We start to eat and talk about our half-days, when baby makes his sign, like what, I'm there, and I'm hungry ... of course he always does that in the middle of meals, it does not mean that however, I lost weight, lol ... no, what I do not eat now I nibble after, and I nibble very very badly, I will not tell you, but so much that I will be hungry, I eat everything I find in the kitchen, closet or fridge .... Galley. 

In any case, After he has eaten, changed, and after me I would have again, put the kitchen in order, it is already 15:00 time to pick up the children of the school ... I will not tell you the rest, we leave that to next time, lol. 

I told you this story, just to tell you, that I can not find a moment to make a nice recipe, take it in photos, and put it on the blog. It becomes rare case, I hope to find a little my routine, and organize me even better, but in the meantime, I will try to publish the recipes that I have in the archive of my pc, or those of Lunetoiles who overflow in my mailbox, sometimes, I'm so ashamed, I think she'll think that I do not like her recipes, and that's why I did not publish it! 

Again I digress on the subject of the recipe, this delicious fondant chocolate and chestnuts Lunetoiles, and do not forget if you've tried one of my recipes, or that of lunetoiles, send me the recipe on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

[ ![brown chocolate fondant](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/fondant-chocolat-marrons-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/fondant-chocolat-marrons-2.jpg>)   


**brown chocolate fondant**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/fondant-chocolat-marrons-4.jpg)

portions:  8  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 200 g of dark chocolate with at least 52% cocoa 
  * 160 g of butter 
  * 3 eggs 
  * 500 g of [ Chestnut cream ](<https://www.amourdecuisine.fr/article-recette-de-la-creme-de-marrons-maison-113097294.html> "homemade chestnut cream recipe")



**Realization steps**

  1. Preheat the oven to 180 ° C (heat 6). Cut the chocolate into small pieces. Place it in a bowl and add the butter. Microwave for 2 minutes at maximum power (850 W). Stir to obtain a smooth and shiny chocolate. 
  2. In a bowl, beat the eggs energetically. Add the chestnut cream and stir to obtain a smooth texture. Pour over the chocolate mixture and mix. 
  3. Butter a mold 20 to 22 cm in diameter and pour the dough into it. Bake for 30 minutes. Allow the flux to cool to room temperature. 



Note This fondant can be stored for 2 or 3 days in the refrigerator. Before serving, leave it for 1 hour at room temperature. 

[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/fondant-chocolat-marrons.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/09/fondant-chocolat-marrons.jpg>)
