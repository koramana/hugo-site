---
title: apple pie without cream pastry
date: '2013-08-29'
categories:
- Chocolate cake
- birthday cake, party, celebrations
- cakes and cakes
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-pommes-ancienne17-a1.jpg
---
[ ![tart-with-apples-ancienne17-a1](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-pommes-ancienne17-a1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-pommes-ancienne17-a1.jpg>)

Hello everybody, 

yet another recipe of lunetoiles, an apple pie she kindly like to share with you, and frankly I'll wait for the first opportunity, to make this recipe, because her photos really give me a great desire to put my hand in the screen and take a piece, greedy that I am 

so I pass you the ingredients:   


**apple pie without cream pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-pommes-ancienne16a1.jpg)

Recipe type:  pie dessert  portions:  8  Prep time:  30 mins  cooking:  30 mins  total:  1 hour 

**Ingredients** For the dough: 

  * 250g of flour 
  * 125 g of butter 
  * 80 g of sugar 
  * 1 egg 
  * 1 pinch of salt 

For garnish : 
  * 3 medium sized apples 
  * the juice of a lemon 
  * Sugar and cinnamon to sprinkle the pie 
  * 5 tbsp apricot jam (or other), to give a shine to the pie 

For applesauce: 
  * 3 medium sized apples 
  * 1 tbsp honey 



**Realization steps** for the pasta 

  1. Take a salad bowl put in the softened butter, 
  2. add the sugar, the flour and the pinch of salt, 
  3. crush well with your fingers to get a nice sand pate. 
  4. Add the egg, gather everything quickly and form a ball. 
  5. Cover with a film and leave in the fridge for 30 minutes. 

For the features: 
  1. Peel, wash and then cut the apples into small pieces. 
  2. Cook them for 15 minutes in ½ liter of water (or more) with the tablespoon of honey. 
  3. Mix everything to obtain a compote. 

For garnish : 
  1. Peel the apples, cut in half, 
  2. remove the heart with the seeds before slicing 
  3. sprinkle with lemon juice to prevent them from oxidizing. 

Mounting 
  1. Take a baking sheet, put on baking paper 
  2. spread the dough, prick with a fork 
  3. pour the apple cake. 
  4. Arrange the apple slices according to your taste 
  5. Sprinkle with a mixture of sugar and cinnamon before baking 
  6. Bake at 180 - 200 ° C for 30 (or more depending on the power of your oven) 
  7. After cooking, heat in a pan about 5 tablespoon apricot jam and spread it evenly with a brush on the pie to give it a shiny appearance 



![tart-with-apples-ancienne.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/08/tarte-aux-pommes-ancienne1.jpg)
