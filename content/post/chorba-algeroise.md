---
title: Algerian Chorba
date: '2016-05-08'
categories:
- Algerian cuisine
- soups and velvets
tags:
- Ramadan 2017
- Healthy cuisine
- Easy cooking
- Algeria
- Vegetables
- Ramadan
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-5.jpg
---
[ ![algerian chorba 5](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-5.jpg>)

##  Algerian Chorba 

Hello everybody, 

Before you get the recipe for the Algerian chorba as I learned it from my mother, I tell you the story, or my story with this Algerian chorba rich in vegetables, super tasty and beautifully scented. 

When I was little I did not eat this soup, I did not even tolerate its smell. I remember when I was back from school, approaching the door I smelled its smell, and I said to myself: thin my mother made the Algerian chorba !!! Already in front of the door I began to emagine all possible scenes or all that I could tell my mother not to eat this soup. With my mother, the rule of the house, I do not cook each dish, we are not in a restaurant! hihihih, she was right about that, she was not going to follow the whims of everyone while we are a large family, at this rate she would stay all day in the kitchen to satisfy us. 

But now, the Algerian chorba is one of my favorite soups, when did I have this click and since when I love this soup, will know !!! 

In any case I make this recipe identically to realize my **chorba tongue of birds** Just replace the vermicelli with the bird languages, the result is just perfect. So are we going for the recipe?   


**Algerian Chorba**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-4.jpg)

portions:  6  Prep time:  20 mins  cooking:  50 mins  total:  1 hour 10 mins 

**Ingredients**

  * 200 gr meat 
  * 20 minced meatballs (optional, but my kids prefer minced meat to normal meat) 
  * 1 onion 
  * 2 tomatoes 
  * 1 tablespoon of tomato paste 
  * 1 medium zucchini 
  * 1 medium potato 
  * 1 average carrot 
  * 1 bunch of coriander 
  * 2 mint leaves 
  * 1 branch of Celeri (I did not put it, my children do not like) 
  * 1 handful of chickpeas in a box. 
  * 1 handful of vermicelli 
  * 1 tablespoon of smen 
  * 1 tablespoon oil 
  * salt, black pepper, paprika 
  * 1 small piece of cinnamon 



**Realization steps**

  1. In a pot, sauté the cut meat in small pieces, with grated onion, zucchini, potato and carrot diced in oil and smen. 
  2. Add the chopped coriander (leave a little for the presentation) and the paprika 
  3. add the mint and the pasta to the blinder and the little cinnamon stick. 
  4. add the mixed tomato, salt and black pepper. 
  5. Cover with water (at least 1 liter of boiling water) and cook. 
  6. When the meat is well cooked, sauté the tomato paste in a little sauce, and add it. 
  7. add the minced meatballs, and the chickpeas, let the minced meat cook 
  8. Now introduce the vermicelli in the rain and let simmer for a few minutes then remove the pot from the heat. 
  9. to taste with a little chopped coriander, and dry mint. 



[ ![algerian chorba 6](https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-6.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/05/chorba-alg%C3%A9roise-6.jpg>)
