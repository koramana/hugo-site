---
title: The Chinese - extra soft brioche
date: '2011-04-10'
categories:
- Mina el forn
- bread, traditional bread, cake

---
##  The Chinese - extra soft brioche 

in any case, immediately at the kitchen, preparing my ingredients, and hop to the map, 

so I'm going to copy / paste ok: 

\- 80ml warm milk + 20ml for yeast   
\- 2 beaten eggs   
\- 1/2 teaspoon of salt   
\- 100 gr of melted butter   
\- 55 gr of sugar   
\- 400 gr of flour T45 (I put the normal flour)   
\- 1 sachet briochin yeast diluted with 20 ml of milk (I have no yeast briochin, so 1 case of baker's yeast for me) 

put the ingredients in the map, according to the recommended order for your map. 

  * 50 gr of raisins 
  * 2 cases of orange blossom water 



custard [ right here ](<https://www.amourdecuisine.fr/article-26492437.html>)

icing 

\- 50 gr of icing sugar   
\- a few drops of lemon juice   
\- a bit of milk 

Remove the dough from the bread machine, on a floured surface, roll out the dough in a rectangle. Top the dough with the pastry cream, sprinkle with drained orange flower water. Roll in pastry, then in this one to make 6 parts that one disposes straight in a buttery mold buttered and floured for me, I did not do 6 parts, but rather 11 small parts, because I did not want to big pieces, but next time I will do 6 parts, will have to find the simple base of my mold in the photo, I only find the base with the chimney, hihihihihih I do not know how we call these molds. Let rise another hour in a warm place. 

Preheat oven Th 180 ° then bake for 30 minutes. Let cool. 

For icing: put in a bowl the icing sugar, a few drops of lemon and a little milk. Brush the Chinese with this frosting. 

very proud of the result, just like my dear OJ 

bonne dégustation. 
