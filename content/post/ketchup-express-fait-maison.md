---
title: homemade ketchup express
date: '2015-12-24'
categories:
- appetizer, tapas, appetizer
- dips and sauces
tags:
- sauces
- House burger
- Burger
- skewers
- tomatoes
- Tomato coulis
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ketchup-express-fait-maison-2.jpg
---
[ ![homemade ketchup homemade 2](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ketchup-express-fait-maison-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ketchup-express-fait-maison-2.jpg>)

##  homemade ketchup express 

Hello everybody, 

At home we are not great lovers of ketchup, suddenly I never buy it. But my oldest son he loves a lot, I do not know since when and how! but he likes. 

Every time we make burgers at home, or make fries, he asks for ketchup ... So once I went to get an express recipe if it existed, and miraculously there are ketchup homemade express. The ingredients were super simple and I had them at home, so I made a small amount just to see if my son would like it, and frankly, he adopted ... I redid the recipe several times at his request, and when I saw that this homemade ketchup can be kept easily in the fridge for over a month, I made it a nice amount, and why not share with you this homemade ketchup recipe ??? !! ! So let's go?   


**homemade ketchup express**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ketchup-express-fait-maison.jpg)

**Ingredients**

  * 450 gr of tomato paste (canned tomato) 
  * 80 ml up to 120 ml of cider vinegar 
  * ½ c. salt 
  * ½ c. coffee of dried oregano 
  * ½ c. cumin 
  * ⅛ c. black pepper 
  * 1 teaspoon mustard powder (I replace with mustard) 



**Realization steps**

  1. place all the ingredients in the bowl of a blender 
  2. mix well and place the ketchup in jars in the fridge 



Note add the cider vinegar for a more liquid ketchup, if you want an even more liquid ketchup, after 120 ml of cider vinegar, it is better to add water. [ ![homemade ketchup express a](https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ketchup-express-fait-maison-a.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/12/ketchup-express-fait-maison-a.jpg>)
