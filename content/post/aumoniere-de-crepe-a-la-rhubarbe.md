---
title: rhubarb crepe aumoniere
date: '2017-01-22'
categories:
- dessert, crumbles et barres
tags:
- grout
- To taste
- Delicacies

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/06/aumoniere-de-rhubarbe-2_thumb1.jpg
---
![with rhubarb crepes 2](https://www.amourdecuisine.fr/wp-content/uploads/2012/06/aumoniere-de-rhubarbe-2_thumb1.jpg)

##  rhubarb crepe chaplain 

Hello everybody, 

For people who like the taste of tart rhubarb, here is a very good recipe: rhubarb pancake chaplain. 

What do I like to present to my children the pancake chaplets, because there is always a surprise inside. This time, to be frank, was a dessert for a tête-à-tête with my husband, he who likes the tangy taste in his desserts, this rhubarb crepe chaplain was for him a magnificent treat.   


**rhubarb crepe aumoniere**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/02/aumoniere-a-la-rhubarbe-034_thumb1.jpg)

**Ingredients** for crepes: 

  * 2 eggs 
  * 35 g of sugar 
  * 150 g flour 
  * 450 ml of milk 
  * the zest of a lemon 
  * 50 g of butter 
  * 2 tablespoons of oil - 

for rhubarb stuffing: 
  * 1 kg of rhubarb 
  * 300 gr of powdered sugar 

for strawberry coulis: 
  * 500 gr of strawberries 
  * 250 grams of powdered sugar 
  * 1 lemon 

for the presentation: 
  * 1 scoop of vanilla ice cream 



**Realization steps**

  1. Wash your rhubarb branches quickly and cut them, without peeling them, in slices 1 to 2 cm thick. 
  2. Put the pieces in a saucepan and sprinkle them with the sugar. 
  3. Leave to drain for 10-15 minutes. 
  4. When the rhubarb has "spat" part of its liquid, cook on low heat in the juice thus obtained stirring regularly. 
  5. Let stew for about 20 minutes until the consistency suits you. 
  6. Slice the strawberries in 2 or 4 according to their size (the more they are sliced ​​thin, the more they cook quickly). 
  7. place them in a cauldron and add ½ cup of water or cover the bottom of the cauldron (if you put too much water the grout will be too liquid). 
  8. Pour sugar over the strawberries until they are completely covered or soaking the sugar. 
  9. Boil everything until the strawberries are soft enough so that you can crush them with a spatula. Everything will remain liquid. 
  10. Take a blender with an emulsifier foot and pass it through the grout, giving a few small strokes to keep some pieces of strawberries. 
  11. preheat the oven to 240 degrees C. 
  12. butter a baking dish, 
  13. garnish the heart of a pancake with the rhubarb compote. 
  14. enclose it with a toothpick, or a cooking wire, put it in the dish, 
  15. cook for 3 to 5 min. 
  16. spread the strawberry coulis in a dish, place a chaplet on top, and serve with a quenelle of ice cream 



you can see also this [ ultra easy pie with rhubarb ](<https://www.amourdecuisine.fr/article-31499515.html>) : 

![rhuba](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/rhuba_thumb1.jpg)

and to come this delightful [ tarte a la rhubarbe et fraises ](<https://www.amourdecuisine.fr/article-tarte-a-la-rhubarbe-et-fraises-ultra-facile.html>)
