---
title: ghribia with pine nuts
date: '2016-06-19'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- shortbread cakes, ghribiya
tags:
- Ramadan 2016
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-aux-pignons-de-pins-1.jpg
---
![ghribia with pine nuts 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-aux-pignons-de-pins-1.jpg)

##  ghribia with pine nuts 

Hello everybody, 

Who is what does not like these delicious Algerian cakes all melting and all sanded in the mouth, ghribia. Personally impossible to come to my house and not find a little box of ghribia. That's how I grew up, and that's the habit I learned from my mom and my grandmother. It must be said that ghribia does not require a lot of ingredients, once you have your hand and we know how to have a ghribia all melting and tasty, we will not get tired? 

Today it's a pine-nut ghribia that Lunetoiles shares with us, and here's what it tells us about this little delight: 

It was a cake that I knew at a wedding to which I was invited, it was taken by the sisters of the husband, who had brought with her tray of cakes Algiers, including this one. At the time I was already interested in oriental pastry, I really flashed on this ghribia that was pique pine nuts, and the taste was just sublime with these gables. To make the recipe, I take the traditional recipe of the ghribia that I leave nature and once all the parts ready, there is more than to stitch each piece of pine nuts and bake a sweet oven to let the cakes dry. 

**ghribia with pine nuts**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-aux-pignons-de-pins-2.jpg)

portions:  30  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients** Note: I used as measure = 1 cup of 250 ml 

  * 3 measures of flour 
  * 1 measure icing sugar 
  * 1 measure overflowing with oil 
  * pinch of salt 
  * about 150 grams of pine nuts 



**Realization steps**

  1. Sift the flour with the icing sugar and pinch of salt. 
  2. Add a little oil and rub with the palm of your hand. 
  3. Add more oil and mix well, and continue to add the oil little by little until you get a dough that stands. 
  4. You can test rolling a ball, and if the preparation does not crumble, then it's good.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-aux-pignons-de-pins-11.jpg)
  5. Make balls the size of a walnut and place in a baking tray. 
  6. Preheat the oven to 150 ° C. 
  7. Stitch each ball with the pine nuts. 
  8. Bake in the already hot oven and lower the oven to 130 ° C and bake for about 25 minutes but check the oven from time to time 
  9. At the exit of the oven, allow to cool 15 min without touching. 



![ghribia with pine nuts](https://www.amourdecuisine.fr/wp-content/uploads/2016/06/ghribia-aux-pignons-de-pins.jpg)
