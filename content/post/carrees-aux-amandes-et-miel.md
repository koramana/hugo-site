---
title: squares with almonds and honey
date: '2013-11-09'
categories:
- Mina el forn
- bread, traditional bread, cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gateaux-aux-amandes-et-miel.CR2_-1024x957.jpg
---
**[ ![cakes with almonds and honey.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gateaux-aux-amandes-et-miel.CR2_-1024x957.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gateaux-aux-amandes-et-miel.CR2_.jpg>) **

##  squares with almonds and honey 

Hello everybody, 

a delicious that we share with us Lunetoiles and that she realized for the feast of the aid el kebir (Aid el adha 2013), it is a cake very easy to realize, which gives a beautiful quantity in a single batch, not the trouble of preparing piece by piece is a cake that is spread in the baking tray, and then cut to a chosen dimension. 

For 36 square, a square mold of about 25 × 25 cm, **but for a rectangle mold, multiply by two**

**squares with almonds and honey**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/11/gateau-aux-cachuetes.CR2_-830x1024.jpg)

Recipe type:  cake  portions:  36  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * Sanded base: 
  * 115g of butter 
  * 175g of flour 
  * 130g of brown sugar 
  * Garnish : 
  * 1 large tablespoon of whole cream 
  * 1 to 2 large tablespoon of honey 
  * 75 g of sweet butter 
  * 100 g of sugar 
  * 150 g slivered almonds 



**Realization steps**

  1. In the bowl of your robot equipped with a blade, put the flour, the sugar and the butter in 
  2. mix until crumbs, and pour them into a square mold with a removable bottom of 24 cm, 
  3. pack with your fingers and bake for 15 minutes at 180 ° C. 
  4. Meanwhile, in a saucepan heat the whole cream, honey, butter and sugar. 
  5. As soon as the mixture is melted and smooth, add the flaked almonds. 
  6. Spread this mixture on the precooked shortbread and put back in the oven at 180 ° C for 15 to 20 minutes, where until the almonds are golden and caramelized 
  7. Let cool and cut into squares or bars. 
  8. Present on a plate and enjoy. 



{{< youtube l49WyHCE5Ug >}} 
