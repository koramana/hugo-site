---
title: Spinach pie
date: '2009-08-02'
categories:
- appetizer, tapas, appetizer
- Mina el forn
- bread, traditional bread, cake
- pizzas / quiches / pies and sandwiches

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/12/229139741.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/229139741.jpg)   
so since all the time that I am in front of this title and I have not yet post the recipe, will have to put me there, it is already midnight 15 min. 

oops I have to be in bed ... .. 

Ingredients: 

  * 5 leaves of Brick 
  * spinach 
  * 1 onion 
  * chicken (white or thigh according to your taste) 
  * 1 fresh tomato (or canned) 
  * grated cheese 
  * 3 to 4 eggs 
  * salt, black pepper (with spinach, we do not need much spice) 



in a frying pan, I sauté the onion in a little oil, and I add the cut spinach, in small quantity, until exhaustion. 

I add after the cut tomato, and I season with salt, according to the taste, I add after the crumbled chicken (I had cooked the chicken in salted water and peppered), I leave simmer together until the water is completely evaporated. 

in a baking dish, I place the pastry sheets so as to cover the bottom, and leave the ends to cover the top, I cover with a little butter. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/229139501.jpg) ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/229139551.jpg)

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/229139601.jpg) ![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/229139611.jpg)

I put some of the chicken spinach mixture, in the brick-covered baking tin, and I cover it with a layer of beaten and salted egg a little, I try to penetrate the eggs a little in this mixture with a fork . 

I graze on some cheese, or cheese 

I add the rest of the mixture and I cover once again beaten egg, and grated cheese 

I cover everything with the sheet of brick, and I finally cover with another sheet of brick (well oiled) to have a united surface 

I sprinkle the top with butter 

I put my pie in a preheated oven at 220 degrees 

I add the butter if necessary, before the end of the cooking, inverted the pie to give crispness to the side of the underside was to see that it is sweet. 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/229139681.jpg)

serve well hot, and as long as the bricks leaves are still crisp 

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/12/229139741.jpg)

I can tell you that the dish was really good. 

the taste of spinach appeared slightly, 

a refaire sûrement très prochainement 
