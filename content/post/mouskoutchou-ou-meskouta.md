---
title: mouskoutchou or meskouta
date: '2018-03-05'
categories:
- cakes and cakes
tags:
- Algerian cakes
- Easy recipe
- To taste
- Economic cake
- Algeria
- Cake
- Economy Cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mouskoutchou-ou-meskouta.jpg
---
![mouskoutchou or meskouta](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mouskoutchou-ou-meskouta.jpg)

##  mouskoutchou or meskouta 

Hello everybody, 

muskoutchou or meskouta: here is a sublime recipe of the sponge cake, or Savoy cake, or muslin cake more known in Algeria and Morocco, by the name: mouskoutchou, mescoutchou, meskouta, meskoutcha ... and so on. 

a cake that my husband loves a lot, and if I am, well every day you will find only the mouskoutchou cake on my blog ... .. 

and I love this cake myself, I always like to find a new recipe, and this time I came across a sublime recipe, and I thank one of the moms at my son's school for this recipe, because we chatted cooking, when she told me about her sponge cake recipe .... Of course, I note and back home, quickly to the kitchen, because what is good with this cake .... we always have the ingredients at home ... 

a sponge cake recipe that I highly recommend, and if you try it, do not forget to put me your photos and / or recipe link on this email: 

[ ![E-mail](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email-300x68.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/email.jpg>)

I publish the recipe of my recipes at your place very soon. 

on this blog you will find: 

[ mouskoutchou speckled ](<https://www.amourdecuisine.fr/article-mouskoutchou-mouskoutchou-mouchete-87564683.html>)

[ chocolate mouskoutchou ](<https://www.amourdecuisine.fr/article-38356047.html>)

[ mouscoutchou, Algerian mouskoutchou ](<https://www.amourdecuisine.fr/article-28557136.html>) more 

![fluffy muskoutchou, meskouta, meskoutcha, mescoutchou](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mouskoutchou-moelleux-meskouta-meskoutcha-mescoutchou1.jpg)

and as I told you it is a recipe to try absolutely, and you will give me some new ones. 

and here is the video at your request of this realization: 

**muskoutchou / meskouta**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mouskoutchou-ou-meskouta-2.jpg)

portions:  8  Prep time:  15 mins  cooking:  40 mins  total:  55 mins 

**Ingredients**

  * 5 eggs 
  * 1 glass of sugar (1 glass of 200 ml that I will use as a measure) 
  * 1 C. coffee vanilla extract 
  * the zest of a lemon and an orange (optional) 
  * 100 ml of milk (½ glass) 
  * 2 tbsp. tablespoon of table oil 
  * 250 gr of flour (2 glasses) 
  * 1 C. coffee baking powder 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. In a large bowl, beat the eggs at high speed for 5 minutes or until doubled in size and turn a light color. 
  3. Gradually add sugar, beating until light and fluffy 
  4. Stir in vanilla then citrus zest 
  5. add the milk while continuing to whisk 
  6. Add the flour mixed with the yeast, by incorporating it delicately with a manual whisk by raising the dough so as not to have lumps. 
  7. towards the end add the oil and mix again gently with the manual whisk. 
  8. Pour into a buttered and floured mold. 
  9. Bake for 30 to 35 minutes (do not open the oven for the first 20 minutes) 
  10. let cool completely before cutting and serving. 



![mouskoutchou or meskouta 1](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/mouskoutchou-ou-meskouta-1.jpg)
