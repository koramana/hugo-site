---
title: homemade quince paste recipe سفرجل
date: '2017-02-27'
categories:
- confitures et pate a tartiner

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/pate-de-coings.jpg
---
![Quince Paste](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/pate-de-coings.jpg)

##  Homemade quince paste recipe 

Hello everybody, 

I am very happy to share with you the homemade quince paste recipe (معجون الطباسة (تباسة, as I realize it!) My joy is actually to be able to find the quinces in their season here in Bristol at Istabul, the Turkish store where 'em buys a lot of ingredients that look like what is found in Algeria ... It's my great joy, it sells even good Merguez now ... 

After the good reservation that I made this year of the [ From Quince jam ](<https://www.amourdecuisine.fr/article-confiture-de-coings.html>) it is the turn of the quince, and this year my children have feasted with this delight that I realized many times at their request, every time I bought quince to make a recipe, they tell me Claimed the quince paste !! ?? My children are like me, I love this dough of quince, and I like to prepare it to the method of my grandmother Constantine, who always presented at the wedding, must say that my grandmother does wonders with quince, good recovery grandma, my heart is with you. 

{{< youtube wZTWsvA8MUM >}} 

**homemade quince paste recipe سفرجل**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/pate-de-coings-1.jpg)

portions:  30  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * quinces 
  * sugar (the same weight as the quince puree obtained) 
  * sugar to coat 
  * lemon to prevent staining (optional) 



**Realization steps**

  1. rub the quinces with a brush to clean them well, wash well under abundant water. 
  2. cut in quarters, remove the central part. and rub with lemon. 
  3. cut into medium pieces and steam. Test with the tip of a knife cooking pieces of quince. 
  4. Switch to the fine mesh mill, if not the blender. 
  5. Weigh this obtained puree, and put it back in a pot with its same weight of sugar 
  6. twist with a long arm wooden spoon until you get a dough that comes off a bit in clumps. 
  7. pour into a square mold lined with food film, otherwise in silicone molds to have definite shapes. 
  8. let cool, if the dough is well cooked, it will take quickly to cooling. 
  9. unmould, you can cut into cube and coat in the crystallized sugar, if you have not made your dough in the small silicone molds. 



![homemade quince paste](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/pate-de-coing-31.jpg)
