---
title: Get dressed with coconut
date: '2012-03-23'
categories:
- amuse bouche, tapas, mise en bouche
- salads, salty verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mkhabez-noix-de-coco-2a_thumb1.jpg
---
##  Get dressed with coconut 

Hello everybody, 

today I share with you this delicious recipe of [ Algerian cake ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , the [ mkhabez ](<https://www.amourdecuisine.fr/categorie-10678931.html>) , prepare by the fingers of fairy, our dear Lunetoiles, it is a very very beautiful color which she chose to garnish this delicious greediness. 

the [ mkhabez ](<https://www.amourdecuisine.fr/article-lamkhabez-el-matnawaa-57770658.html>) was always prepared with almonds, but this time it's **muddle with coconut** so without delay I give you the recipe 

To make the coconut color: take a freezer bag, put some coconut in it and a few drops of food coloring (colors of your choice), blow in the bag to inflate it with air, close the bag, hold it well and shake, until you coat all the coconut dye, in a few seconds you will see that the coconut changes color. 

Source: **Mrs. Hocine's book** Gateaux Imène n ° 3 

**![Get dressed with coconut](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mkhabez-noix-de-coco-2a_thumb1.jpg) **   


**Get dressed with coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/mkhabez-noix-de-coco-2a_thumb1.jpg)

**Ingredients**

  * 3 measures of coconut 
  * 1 measure of crystallized sugar 
  * 1 teaspoon of lemon zest 
  * 1 teaspoon of vanilla 
  * 2 to 3 eggs 

Frosting: 
  * 2 egg whites 
  * 1 teaspoon of orange blossom water 
  * ½ teaspoon of lemon juice 
  * ½ teaspoon of vanilla extract 
  * ½ teaspoon of butter 
  * Icing sugar 

Decoration: 
  * Shiny food 
  * Food coloring 



**Realization steps**

  1. In a terrine mix the coconut and sugar mixed together 
  2. add lemon zest and vanilla 
  3. pick up with the eggs to get a firm dough. 
  4. On a floured work surface, shape a roll of dough 3 cm thick and 20 cm long, 
  5. cut out squares, arrange them on a floured plate, 
  6. cook them in a preheated oven at 180 ° C for 10 to 15 minutes and allow to cool. 

Prepare the frosting: 
  1. mix the beaten egg whites, the orange blossom water, the lemon juice, the vanilla extract and the butter. 
  2. Add the sieved icing sugar until you have a thick glaze, 
  3. then color to your choice. To check the consistency of the icing, try a cake: if it flows, add icing sugar. 
  4. Dip the cakes in the frosting, 
  5. let it dry a little, then decorate half with colored coconut, 
  6. let the other part dry, then wipe with the shiny food. Present in white boxes. 


