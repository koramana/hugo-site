---
title: zucchini gratin
date: '2017-05-23'
categories:
- Algerian cuisine
- diverse cuisine
- Moroccan cuisine
- Tunisian cuisine
- houriyat el matbakh- fatafeat tv
- Dishes and salty recipes
- recipe for red meat (halal)
- ramadan recipe
tags:
- Algeria
- Vegetables
- Healthy cuisine
- Easy cooking
- Full Dish
- Ramadan 2015
- inputs

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-de-courgettes-2.CR2_.jpg
---
[ ![Zucchini gratin 2.CR2](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-de-courgettes-2.CR2_.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-courgettes.html/gratin-de-courgettes-2-cr2>)

##  zucchini gratin 

Hello everybody 

Here is a delicious **zucchini gratin** that I often prepare when I have a small amount of meat. a **gratin** prepared at bases **vegetables** steamed very tender in the mouth. I'm not going to lie to you and say that this gratin is light, or not very caloric, because personally I really like a gratin made with fresh cream and sometimes a lot of cheese, yes .... This is a good gratin in my opinion. 

In any case, this recipe is very easy to achieve, you tell me your opinion, ok? 

**zucchini gratin**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-de-courgettes-2.CR2-001.jpg)

portions:  4  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 3 zucchini 
  * 2 medium potatoes 
  * 250 gr of minced meat 
  * 1 small onion 
  * 4 cloves of garlic 
  * Parsley ax very finely 
  * 1 tablespoon of olive oil 
  * Salt pepper. 
  * 3 to 4 eggs according to size 
  * 200 ml of cream 
  * grated cheese 



**Realization steps**

  1. Peel the zucchini, cut into cubes, 
  2. peel the potatoes, cut them into cubes too, and put the two vegetables to steam with a little salt 
  3. Once the zucchini and potatoes are steamed, let it drain well. 
  4. Fry the crushed garlic and finely chopped onion with a tablespoon of oil, add the minced meat, salt, pepper, and chopped parsley, cook. 
  5. put the minced meat cooked in a salad bowl, Add on the steamed vegetables, the cream and the eggs beaten in omelette, check the seasoning, add the grated cheese, and mix well.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-de-courgette-cuisine-au-four_thumb1.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-courgettes.html/gratin-de-courgette-cuisine-au-four_thumb1>)
  6. Pour everything into a lightly buttered gratin dish, 
  7. bake in preheated oven at 180 degrees, for almost 20 minutes, or until golden brown gratin. 
  8. To know that your gratin is cooked, insert the tip of a knife in your gratin if it comes out dry, your dish is ready. 
  9. Serve hot or warm with a good salad. 



[ ![zucchini gratin](https://www.amourdecuisine.fr/wp-content/uploads/2012/04/gratin-de-courgettes.jpg) ](<https://www.amourdecuisine.fr/article-gratin-de-courgettes.html/gratin-de-courgettes-3>)
