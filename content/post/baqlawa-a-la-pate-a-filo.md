---
title: baqlawa with filo pastry
date: '2009-12-08'
categories:
- the tests of my readers

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202625761.jpg
---
& Nbsp; to succeed baklawas filo paste without overflowing the stuffing and filo leaves that hold well without cracking, here is a recipe that I found on a forum well known, that I try, and well liked. INGREDIENTS: 2 cups of ground almonds, grilled and coarsely chopped 1 cup of sugar 1/2 teaspoon of orange blossom water. Mix everything well. ASSEMBLY: Take 3-4 sheets of filo paste superimposed, coat the top side of each filo sheet with a warm mixture of melted butter + oil. Sprinkle a handful of stuffing over the entire surface of the last sheet laid. Fold the dough into pudding & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 3.33  (  4  ratings)  0 

![bonj17](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202628281.gif)

to succeed baklawas filo paste without overflowing the stuffing and filo leaves that hold well without cracking, here is a recipe that I found on a forum well known, that I try, and well liked. 

INGREDIENTS: 

  * 2 cups ground almonds, grilled and coarsely chopped 
  * 1 cup of sugar 
  * 1/2 teaspoon of orange blossom water. 



![etoile_filante](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202628421.gif)

Mix everything well. 

MOUNTING: 

Take 3-4 sheets of filo paste superimposed, coat the upper side of each filo sheet with a warm mixture of melted butter + oil. 

[ ![S7301661](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202625761.jpg) ](<https://www.amourdecuisine.fr/article-25345487.html>)

Sprinkle a handful of stuffing over the entire surface of the last sheet laid. 

[ ![S7301664](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202626061.jpg) ](<https://www.amourdecuisine.fr/article-25345487.html>)

Fold the pasta into a fairly flat pudding on each side 

[ ![S7301662](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202626611.jpg) ](<https://www.amourdecuisine.fr/article-25345487.html>)

At the join of the 2 rolls, stack them by closing them like a book. 

[ ![S7301663](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202627461.jpg) ](<https://www.amourdecuisine.fr/article-25345487.html>)

Cut the pudding diagonally every 5-6cm, 

Then start again with 4 new leaves until all the stuffing is done 

![fleur_et_coeur](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202628531.gif)

Place the lozenges on a plate covered with baking paper: 185 ° until lightly browned. 

![fleur_glitter_bleu_ciel](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202628581.gif)

Let cool. 

![noeud_etoile](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202629201.gif)

Prepare a liquid honey: put 150g of honey and 60ml of orange water to heat.Y soak the diamonds on all sides.Let cool on parchment paper. 

[ ![S7301666](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202627911.jpg) ](<https://www.amourdecuisine.fr/article-25345487.html>)

[ ![S7301669](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202628091.jpg) ](<https://www.amourdecuisine.fr/article-25345487.html>)

I hope you like, let me, please comments, so that I would have the chance to visit your blogs, because I like cooking, and I like to discover a new blog every day. 

![bye2](https://www.amourdecuisine.fr/wp-content/uploads/2009/12/202631921.jpg)
