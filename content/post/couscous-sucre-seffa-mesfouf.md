---
title: sweet couscous / seffa / mesfouf
date: '2018-05-06'
categories:
- couscous
- Algerian cuisine
- Dishes and salty recipes
- recipes of feculents
tags:
- Algeria
- dishes
- desserts
- Oriental cuisine
- Ramadan
- Ramadan 2018
- Morocco

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/02/couscous-sucr%C3%A9-seffa-mesfouf-2.jpg
---
![sweet couscous seffa mesfouf 2](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/couscous-sucr%C3%A9-seffa-mesfouf-2.jpg)

##  sweet couscous / seffa / mesfouf 

Hello everybody, 

Raisin mesfouf, or raisin raisin, a delicious dish from the [ Algerian cuisine ](<https://www.amourdecuisine.fr/categorie-12359239.html>) , which is often prepared especially during ramadan nights, with a nice cup of lben, sweet couscous 

I remember when I was young, I always said to my mother: "do not wake up at the time if there is no mesfouf", and I was serious about it, I preferred to sleep for to be frank instead of waking up to eat, but I was still sacrificing if it was to eat the mesfouf, hihihihih. 

**sweet couscous / seffa / mesfouf**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/couscous-sucr%C3%A9-seffa-mesfouf.jpg)

Recipe type:  Hand  portions:  4  Prep time:  20 mins  cooking:  30 mins  total:  50 mins 

**Ingredients**

  * 300 g of couscous close 
  * 1 pinch of salt 
  * 300 ml warm water 
  * 150 g raisins. 
  * 120 g of butter 
  * 1 tbsp sugar 
  * 1 teaspoon cinnamon 



**Realization steps**

  1. put the grapes to swell in a little warm water. 
  2. prepare the couscous with steam. 
  3. and before putting the couscous to cook for the last time, first place the drained raisins with their water, cover and cook for 8 to 10 minutes by steaming. 
  4. remove the lid, and cover the raisins with couscous 
  5. let the couscous cook for another 15 minutes 
  6. pour all into a basin (the bottom of the couscous is filled with water, shake a little so that the water is drained, before pouring the contents into the basin, 
  7. add the butter and add it to the mixture of couscous and raisins. 
  8. add the sugar according to your taste 
  9. serve hot with lben or buttermilk 



![sweet couscous seffa mesfouf 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/02/couscous-sucr%C3%A9-seffa-mesfouf-1.jpg)

Preparation 

la cuisson du couscous a la vapeur est bien expliquée sur cette vidéo: 
