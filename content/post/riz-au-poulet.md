---
title: Chicken rice
date: '2016-10-12'
categories:
- recette a la viande rouge ( halal)
- rice
tags:
- Algeria
- Morocco
- Ramadan
- dishes
- Easy cooking
- tajine
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/01/riz-au-poulet.jpg
---
![Chicken rice](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/riz-au-poulet.jpg)

##  Chicken rice 

Hello everybody, 

Chicken rice is my recipe "save the day" !!! Since we are working in the kitchen, and I use my little electric plate with a single fire, I try to make quick recipes where I will use only one pot. 

This chicken rice recipe is one of my husband's favorite recipes! Indeed he loves rice, and rice cooked in red sauce for him is the best recipe. 

In any case, I do not cook chicken rice for him, it's a very good recipe and we like it at home. This dish is very tasty and well-scented, and I really like adding a nice handful of peas in my chicken rice dish to break the acidity of the tomato. My mother has a habit of adding green pepper to the dish which gives another dimension to this recipe. 

**Chicken rice**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/riz-au-poulet-1.jpg)

portions:  4  Prep time:  15 mins  cooking:  30 mins  total:  45 mins 

**Ingredients**

  * 4 pieces of chicken (I prefer the thighs) 
  * 1 small onion 
  * 2 cloves garlic 
  * a bay leaf 
  * 1 tablespoon of tomato paste 
  * 400 gr of fresh tomatoes 
  * 3 glasses of rice (1 glass = 150 ml) 
  * 1 handful of peas 
  * 3 tablespoons of oil 
  * salt, black pepper, paprika, coriander powder 
  * water 



**Realization steps**

  1. pass the onion and garlic to the blender 
  2. pour the mixture into a deep skillet, add the olive oil 
  3. add the chicken pieces 
  4. fry the mixture a little until it has a nice color, 
  5. then add bay leaves, paprika, salt, black pepper, and condiments to taste 
  6. let it simmer a little, and add the past tomatoes to the blender 
  7. add the tablespoon of canned tomato, and let everything come back well. 
  8. When the sauce is reduced, add the water to cover the chicken and cook. 
  9. when the chicken is cooked remove the sauce and set aside. 
  10. add rice and peas, let simmer a little and cover with water   
(usually I add 2 glass of water for a glass of rice, but as there is already chicken cooking sauce, so I try to evaluate the amount of sauce and add the necessary amount of water ) 
  11. cook on low heat, covering the pan. 
  12. serve immediately and have a good appetite 



![Rice with chicken 3](https://www.amourdecuisine.fr/wp-content/uploads/2011/01/riz-au-poulet-2.jpg)
