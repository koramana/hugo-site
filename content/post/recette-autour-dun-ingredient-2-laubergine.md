---
title: 'recipe around an ingredient # 2: eggplant'
date: '2012-10-05'
categories:
- cuisine algerienne
- Plats et recettes salees

---
Hello everybody, 

The decision is made on the ingredient of this week: "eggplant" and this following a vote between a few bloggers who have a lot of ingredients, and with this round   
of "recipes around an ingredient" we will be well overcoming the next rounds. 

I explain the game for bloggers: 

\- you prepare your recipes, or you think about the recipe you want to make with "aubergine". 

\- you leave a comment on this article with the name of the recipe you will make (not the recipe link, the name) 

\- you write the recipe and program it for Sunday, 

\- Saturday I contact all the people who have left their comments with the title of their recipe here (so be sure to fill the box "email"), to give them the list of   
other girls' titles and the link to their blogs (do not worry if you change the title of the recipe, or you can not make it, and you have   
program a recipe already in your archive. the main thing is your participation in the round, so no constraint. 

\- Sunday your recipe will be online, with the list of your friends 

\- remember, if you choose an old recipe, already in your blog, you have to put it back on Sunday, and not before, ok 

It's not that you publish the article today with aubergine, because your article will be without the list that may not be ready before Saturday night. 

is everything well explained? 

merci 
