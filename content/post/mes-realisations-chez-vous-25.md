---
title: my achievements at home 25
date: '2012-09-13'
categories:
- cheesecakes et tiramisus
- dessert, crumbles et barres
- idea, party recipe, aperitif aperitif
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/09/kalb_elouz5_31.jpg
---
![](https://www.amourdecuisine.fr//import/http://img.over-blog.com/400x325/2/42/48/75/API/2011-10/11/)

Hello everybody, 

I'm back from holidays, but I'm coming back to my blog, because of my return it was already back to school, then it's the arrangement of luggage, and shopping and shopping ... .. so really not the time for the blog. 

It's not the recipes that miss my friends but rather the time, but promised, as soon as possible I put the machines in motion ... 

I am happy to always receive your achievements on my email: [ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)

This article contains some achievements that I received a while ago, and those that I received since the Aid, I will put them online this afternoon. 

Thank you for your emails, thank you for being on my blog and for making my recipes, it's a great honor for me, thank you for your trust. 

[ Strawberry tart ](<https://www.amourdecuisine.fr/article-tartelettes-fraises-mascarpone-104932089.html>) at Malika (a reader) 

[ chicken with puff pastry ](<https://www.amourdecuisine.fr/article-feuillete-tresse-au-poulet-bichamel-78545054.html>) at Samira 

[ buns with minced meat ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee-96050112.html>) at Sarah's 

[ semolina bread ](<https://www.amourdecuisine.fr/article-25345314.html>) at Sarah's 

[ buns with minced meat ](<https://www.amourdecuisine.fr/article-petits-pains-a-la-viande-hachee-96050112.html>) at Sarah's 

![kalb_elouz](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/kalb_elouz5_31.jpg)

![horn-de-g3.png](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/corne-de-g31.png)

[ Tcharek Msaker ](<https://www.amourdecuisine.fr/article-tcharek-m-saker-tcharak-massakar-61311016.html>) in [ Yes Yes ](<http://simplement-delicieux.over-blog.com/>)

![tajine-zucchini-to-oeufs.JPG](https://www.amourdecuisine.fr/wp-content/uploads/2012/09/tajine-courgettes-aux-oeufs1.jpg)

As I said, many of your achievements are waiting for me to publish them this afternoon, thank you again. 

and if you too have made my recipes, send me the picture on this email: 

[ vosessais@amourdecuisine.fr ](<mailto:vosessais@amourdecuisine.fr>)
