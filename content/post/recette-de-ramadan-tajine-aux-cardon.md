---
title: ramadan recipe / Tagine tajine
date: '2013-06-25'
categories:
- panna cotta, flan, and yoghurt
- ramadan recipe
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tajine-de-petits-pois-et-cardons-farcis11.jpg
---
hello everyone, I have never made recipes Cardon on my blog, just because in Bristol, I never find, then as soon as I fall on it, I take the opportunity to make good recipes, or recipes that I have not eaten for some time now, like this recipe, that my mother used to do often. this dish is too good, and will make a delicious main dish among your ramadan recipes so I pass you the ingredients: 1 kg of cardoons 300 g of ground meat 1 onion and a half pieces of chicken or meat & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 0.85  (  1  ratings)  0 

![tajine-of-peas-and-chard-farcis.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tajine-de-petits-pois-et-cardons-farcis11.jpg)

Hello everybody, 

I have never made recipes to Cardon on my blog, just because in Bristol, I never find, so that I fall on it, I take the opportunity to make good recipes, or recipes that I have not eaten for some time now, like this recipe, that my mother used to do often. 

this dish is too good, and will do well a delicious main dish among your [ ramadan recipe ](<https://www.amourdecuisine.fr/article-menu-ramadan-plats-chorbas-et-entrees-55268353.html>)

so I pass you the ingredients: 

  * 1 kg of cardoons 
  * 300 g minced meat 
  * 1 onion and a half 
  * pieces of chicken or meat 
  * 2 tablespoons of rice 
  * 1 egg 
  * 3 tablespoons of oil 
  * 2 handful of peas 
  * 1 bunch of parsley 
  * black pepper 
  * cinnamon 
  * salt 



![tajine de cardon.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tajine-de-cardon11.jpg)

method of realization 

  1. Clean the cardoons, and cut them into pieces 6 to 7 cm long. 
  2. Remove the wires on each side and remove the smooth skin that covers the hollow side of the card. 
  3. Rinse and put and blanch in a saucepan of salt water. 
  4. In a large pot, if possible, fry the grated onion 
  5. add the chicken pieces, black pepper, cinnamon and salt. 
  6. Cover with water and add the peas and chopped parsley. 
  7. Mix the minced meat with 1/2 grated onion, chopped parsley, egg yolk, a pinch of black pepper and salt. 
  8. Take a little stuffing and roll the cigar-shaped, surround it with two pieces of cardoon forming a small package. 
  9. Tie this bundle with a strong thread, or prick with two toothpicks 
  10. Continue the operation until the pieces of cardoon are exhausted. 
  11. Immerse them in the sauce. Cover and cook for 5 to 10 minutes 
  12. When the cards are soft, reduce the sauce and bind with an egg yolk diluted in lemon juice. 
  13. Let reduce the sauce. 



![cardoon-farci.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/cardon-farci11.jpg)
