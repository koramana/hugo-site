---
title: Meringues without easy vegan eggs
date: '2016-01-23'
categories:
- Healthy cuisine
- Cupcakes, macaroons, and other pastries
- dessert, crumbles and bars
- sweet recipes
tags:
- Vegetarian cuisine
- Chickpea
- Kitchen without egg
- Cakes
- To taste
- Pastry
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/meringue-vegan-sans-oeufs-.jpg
---
[ ![vegan meringue, without eggs](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/meringue-vegan-sans-oeufs-.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/meringue-vegan-sans-oeufs-.jpg>)

##  Meringues without easy vegan eggs 

Hello everybody, 

Since the time that Lunetoiles tells me about the **Aquafaba** **,** or the chickpea juice that vegans (people who eat nothing that is animal source) or even people who have the allergy to eggs, and that sometimes replace in their preparations the egg whites not the juice chickpea ... I neglected the subject at the very beginning, because I still have a stock of egg white in my freezer that I have not yet exploited. 

But yesterday, when I was going to make a chickpea cassoulet, I opened the box of chickpeas, and wanting to throw this water, I said to myself ... I'm going to try to make meringues with, what is it I'm going to lose ... An essay is never going to kill ... 

My friends, my surprise was more than great when I began to whip this water, which foamed more and more, and rose faster than the egg whites, a huge surprise for me ... I continued l adventure to the end .... and the result is more than blue for me. In addition to the taste, you will not even know the difference between egg meringues and chickpea juice meringues. 

To not miss anything of the adventure, I even made you a video ... 

{{< youtube M8c7rBEetAg >}} 

**Meringues without vegan eggs**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/meringue-vegan-sans-oeuf-1.jpg)

portions:  30  Prep time:  4 mins  cooking:  80 mins  total:  1 hour 24 mins 

**Ingredients**

  * 100 ml canned chickpea juice 
  * 180 gr of icing sugar 



**Realization steps**

  1. Pour the chickpea juice into a salad bowl   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/jus-de-pois-chiche.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/jus-de-pois-chiche.jpg>)
  2. whip until it becomes well foaming and firm. 
  3. Add the icing sugar in small quantities while whisking after each addition. 
  4. Place the meringue dough in a fluted piping bag and make roses, or shapes to your taste directly on a tray lined with baking paper. 
  5. Bake in a preheated oven at 110 ° C for 1 hour and then lower to 90 ° and prolong cooking 20min. (In my opinion it should leave the port a little open, because there will be plenty of steam that escapes during cooking, and maybe that's why the meringues are not too smooth. 
  6. Let cool before demoulding. 



[ ![vegan meringue without eggs](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/meringue-vegan-sans-oeufs.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/meringue-vegan-sans-oeufs.jpg>)
