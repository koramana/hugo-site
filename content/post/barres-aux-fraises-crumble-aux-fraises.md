---
title: Strawberry Bars - Strawberry Crumble
date: '2012-04-22'
categories:
- juice and cocktail drinks without alcohol
- crème glacée, et sorbet

---
Strawberry Crumbs Bars - Strawberry Bars-Strawberry Crumble & nbsp; hello everyone, this morning by opening my mailbox, I found the photos of this sublime strawberry crumble / strawberry bars, that Lunetoiles send me, I could not resist, I deprogrammed a recipe that I had to publish, because frankly, I liked the recipe, and the photos, which were really sublime. bravo lunetoiles for this work of chef. & Nbsp; for: 16 to 20 bars Ingredients for streusel: 115 gr of unsalted butter at room temperature 150 gr of brown sugar 1/4 tsp. to & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

#  Strawberry Crumbs Bars - Strawberry Bars - Strawberry Crumble 

Hello everybody, 

this morning by opening my mailbox, I found the photos of this sublime strawberry crumble / strawberry bars, that Lunetoiles sent me, I could not resist, I deprogrammed a recipe that I had publish, because frankly, I liked the recipe, and the photos, which were really sublime. bravo lunetoiles for this work of chef. 

for :  16 to 20 bars 

ingredients 

for the streusel: 

  * 115 g unsalted butter at room temperature 
  * 150 gr of brown sugar 
  * 1/4 c. salt 
  * 165 gr all-purpose flour 



for the bars: 

  * 500 g strawberries, hulled and sliced ​​1/4 thick 
  * 2 tbsp. to S. of light brown sugar 
  * 170 gr of flour 
  * 1 sachet of baking powder 
  * 1/2 c. salt 
  * 170 gr unsalted butter, at room temperature 
  * 225 gr of powdered sugar 
  * 3 big eggs 
  * 1 C. pure vanilla extract 



Preparation: 

Preheat the oven to 180 ° C. Butter a 22 cm square pan and line it with parchment paper, leaving an excess of 2 cm by 2 cm of sides exceeded. Set aside. 

for the streusel: 

Whisk together butter, brown sugar and salt. Add the flour and mix with a fork until you get large crumbs. Refrigerate until ready to serve. 

for the bars: 

In a medium bowl, combine strawberries, brown sugar, and 30 g flour. In another medium bowl, whisk remaining flour, baking powder and salt. In a large bowl, using an electric mixer, beat butter and powdered sugar until light. Beat the eggs, one at a time. With the low speed beater, beat with vanilla, then the flour mixture. Spread the dough into the prepared pan. Garnish with strawberries, then cover with prepared streusel. 

Cook until browned and a toothpick inserted in the center comes out with a little moist crumbs attached, about 50 to 55 minutes. Cool completely in the mold. Using the excess parchment paper, lift the cake from the mold. Cut into bars and sprinkle with icing sugar. Serve as is, or with a little whipped cream. 

I invite you to see on this blog: 

[ pear crumble ](<https://www.amourdecuisine.fr/article-crumble-aux-poires-98846204.html>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
