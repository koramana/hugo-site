---
title: Biscuits with raisins
date: '2015-02-25'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian dry cakes, petits fours
tags:
- Cookies
- Algeria
- Easy cooking
- Dry Cake
- Cakes
- Algerian cakes
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/02/biscuits-aux-raisins-secs.jpg
---
[ ![raisin biscuits](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/biscuits-aux-raisins-secs.jpg) ](<https://www.amourdecuisine.fr/article-biscuits-aux-raisins-secs.html/biscuits-aux-raisins-secs-1>)

##  Biscuits with raisins 

Hello everybody, 

**Biscuits with raisins**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/biscuits-aux-raisins-secs.jpg)

portions:  30  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 2 eggs 
  * 1 glass of crystallized sugar 
  * crystallized sugar for decoration 
  * 1 glass of margarine mixture and melted smen 
  * 2 to 3 tablespoons of vanilla 
  * 1 sachet of baking powder (8 gr) 
  * 100 g raisins cut into small pieces, cheat in a little orange blossom water. (you can substitute with flour chocolate chips needed.   
[ ![raisin biscuits](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/sabl%C3%A9s-aux-raisins-secs-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40658>)



**Realization steps**

  1. Work sugar and fat, 
  2. add eggs and vanilla, 
  3. mix well then add the flour and yeast. 
  4. Drain the grapes and sprinkle with 2 tablespoons flour and incoporate them last.   
[ ![raisin biscuits](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/biscuits-aux-raisins-secs-1.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40655>)
  5. Prepare balls flatten them and put in sugar,   
[ ![raisin biscuits](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/biscuits-aux-raisins-secs-61.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40659>)
  6. make drawings with a glass and arrange them in a baking dish.   
[ ![raisin biscuits](https://www.amourdecuisine.fr/wp-content/uploads/2015/02/biscuits-aux-raisins-secs-7.jpg) ](<https://www.amourdecuisine.fr/?attachment_id=40660>)
  7. Slip them in a preheated oven at 180 ° for 20 min. 


