---
title: Tortilloni with Bolognese
date: '2009-05-10'
categories:
- sweet verrines

image: https://www.amourdecuisine.fr/wp-content/uploads/2009/05/224564821.jpg
---
![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/224564821.jpg)   
I had to buy a box of Tortilloni ready-made stuffed with cheese, and I wanted to make it a simple thing, I decided on a Bolognese sauce, to garnish my tortilloni. 

and it was boooooooooooooon. 

and so for the Bolognese: 

  * minced meat, 
  * 1 big box of peeled tomatoes 
  * 2 tablespoons of olive oil, 
  * 1 onion 
  * 2 cloves garlic 
  * basil, salt and pepper 
  * 1 little olive (I like it in my Bolognese, so it's optional) 



**preparation**

heat the oil in a pan, discard the chopped onion and the crushed garlic, let it come back for a few minutes, add the minced meat, let it roast while crushing the minced meat a little so that it does not make packages. 

add the basil, the peeled tomato box, salt pepper, simmer over low heat stirring well add at the end the olives cut, let simmer a little and it is ready !! 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/224564711.jpg)

when tortillonis, wait until the last quarter of an hour, before serving, and put a salted water to a boil, throw in the tortillonis for a cooking of less than 10 min, or until they become tender 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/224564751.jpg)

drain and serve immediately garnished with Bolognese sauce 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/224564821.jpg)

I did a little more Bolognese, than it takes 

so the rest 

![](https://www.amourdecuisine.fr/wp-content/uploads/2009/05/224564911.jpg)

so the rest in a plastic box, and in the freezer for another time, when I'm in a hurry 

bonne Appétit 
