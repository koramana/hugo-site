---
title: strawberry cupcake cheesecake with lemonade
date: '2015-04-07'
categories:
- cheesecakes et tiramisus
- Cupcakes, macarons, et autres pâtisseries
- recettes sucrees
tags:
- Easy cooking
- desserts
- Pastry
- Algerian cakes
- Inratable cooking
- Fast Food
- To taste

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cheesecake-en-cupcake-aux-fraises-et-limonade.jpg
---
[ ![strawberry cupcake cheesecake with lemonade](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cheesecake-en-cupcake-aux-fraises-et-limonade.jpg) ](<https://www.amourdecuisine.fr/article-cheesecake-en-cupcake-aux-fraises-et-limonade.html/cheesecake-en-cupcake-aux-fraises-et-limonade-2>)

##  strawberry cupcake cheesecake with lemonade 

Hello everybody, 

Here is delicious cupcake-style cheesecake, A recipe of small sweets prepared by the care of my dear **Lunetoiles** . 

This cupcake cheesecake with strawberries and lemonade is a super easy recipe to make, and with this mini version, these cupcakes are ideal for birthday parties, or in a varied buffet. I even prepared this recipe when Lunetoiles shared it with me at a school party. It was easy to carry the cupcakes without the toppings at school, and the kids were happy to garnish them with me, and drop a pretty strawberry over them. Just to tell you: do not deprive yourself of preparing these cheesecakes in cupcakes, for picnic outings, leave the topping at the time of serving, it will not change anything to this delight. 

[ ![strawberry cupcake cheesecake with lemonade](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cheesecake-en-cupcake-aux-fraises-et-limonade-1.jpg) ](<https://www.amourdecuisine.fr/article-cheesecake-en-cupcake-aux-fraises-et-limonade.html/cheesecake-en-cupcake-aux-fraises-et-limonade-2>)   


**strawberry cupcake cheesecake with lemonade**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cheesecake-en-cupcake-aux-fraises-et-limonade-2.jpg)

portions:  12  Prep time:  25 mins  cooking:  25 mins  total:  50 mins 

**Ingredients** for cupcake funds: 

  * 180 g of biscuits reduced to powder (speculoos ...) 
  * 3 tablespoons salted butter, melted 
  * 1 tablespoon of sugar (optional) 

For the cheesecake 
  * 100 g of sugar 
  * 1 tablespoon lemon zest 
  * 240 g cream cheese, softened (I recommend the use of Philadelphia for this cheesecake) 
  * 2 large eggs 
  * 60 ml of buttermilk (ribot milk) 
  * 3 tablespoons thick cream 
  * 2 tablespoons lemon juice 
  * ½ teaspoon of vanilla extract 

Strawberry sauce 
  * 240 g fresh strawberries 
  * 1 tablespoon of sugar, or to taste 
  * 2 teaspoons fresh lemon juice 



**Realization steps**

  1. Preheat the oven to 170 degrees. 
  2. In a bowl whisk together the biscuit crumbs, melted butter and 1 tablespoon sugar. 
  3. Divide the mixture into 12 muffin pans filled with paper trays, adding about 1 tablespoon + ½ c to each cavity and tamp the mixture in each cavity. 
  4. Bake in preheated oven for 5 minutes, then remove from oven and let cool. 
  5. Reduce the oven temperature to 160 degrees. 
  6. In a food processor, mix together 100 ml of sugar with the lemon zest (if you do not have a food processor you just rub the sugar and lemon zest with your fingers). 
  7. Add the cream cheese to a bowl and pour the sugar mixture over the cream cheese and using an electric mixer set at low speed, mix together until smooth. 
  8. Stir in the eggs one at a time, mixing just enough to mix after each addition. 
  9. Mix the buttermilk and the thick cream. 
  10. Stir in lemon juice and vanilla extract. 
  11. Spread the mixture in the boxes, pour the mixture over the crusts. 
  12. Bake in the preheated oven for 20 to 25 minutes until the cupcakes will become puffed and bulging, but will fall when they are out of the oven). Let cool at room temperature, about 1 hour, then refrigerate for at least 2 hours. 
  13. Serve with the strawberry sauce. 

For the strawberry sauce: 
  1. Add the strawberries, 1 tablespoon of sugar and 2 teaspoons of lemon juice to a robot and puree. Refrigerate until ready to serve. 



[ ![strawberry cupcake cheesecake with lemonade](https://www.amourdecuisine.fr/wp-content/uploads/2015/04/cheesecake-en-cupcake-aux-fraises-et-limonade-3.jpg) ](<https://www.amourdecuisine.fr/article-cheesecake-en-cupcake-aux-fraises-et-limonade.html/cheesecake-en-cupcake-aux-fraises-et-limonade-4>)
