---
title: my achievements at home
date: '2010-09-21'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- Algerian dry cakes, petits fours
- sweet recipes

---
you were numerous to try my recipes, and today I have the honor to share this with all my other readers: 

[ my cakes knots ](<https://www.amourdecuisine.fr/article-36513347.html>) , at Naima, a faithful reader 

my [ skikrates with coconut ](<https://www.amourdecuisine.fr/article-25345462.html>) at Ratiba B, a faithful reader. 

my [ pepper rolls ](<https://www.amourdecuisine.fr/article-roules-aux-poivrons-55641009.html>) and thin at Sunchine, a faithful reader 

my [ Apricot cake with apricot ](<https://www.amourdecuisine.fr/article-gateau-renverse-abricot-amandes-acacia-45768674.html>) at Lunetoiles. 

and if you too, you have tried my recipes, do not forget to send me the photo of your essay to this email: 

[ vosessais@gmail.com ](<mailto:vosessais@gmail.com>)

thank you very much. 
