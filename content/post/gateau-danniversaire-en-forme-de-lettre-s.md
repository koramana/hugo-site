---
title: Birthday cake in the shape of a letter S
date: '2018-03-12'
categories:
- birthday cake, party, celebrations
- cakes and cakes
- sweet recipes
tags:
- Soft
- biscuits
- desserts
- Pastry
- Algerian cakes
- To taste
- Butter cream

image: https://www.amourdecuisine.fr/wp-content/uploads/2018/03/gateau-danniversaire-en-forme-de-lettre-S-2.jpg
---
![Birthday cake in the shape of a letter S 2](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/gateau-danniversaire-en-forme-de-lettre-S-2.jpg)

##  Birthday cake in the shape of a letter S 

Hello everybody, 

You guessed it maybe! Yes it was my birthday a few days ago, and greedy that I am I made a cake that I love, castels with Swiss meringue butter cream. I confess one thing, we feasted, but really a delight. 

At the same time, I took advantage of the anniversary cake that is in vogue right now, the birthday cake in the shape of a figure or letter, I chose that my birthday cake take the form of the letter S, the first letter of my first name! But what did I like to mark this letter everywhere I spent when I was young, I would never have imagined that one day I was going to do, while cake in the shape of the letter S, hihihihi (like we always stay young in the depths of our own, whatever the number of candles on our cake, hihihih) 

{{< youtube n1nr2bueeW8 >}} 

**Birthday cake in the shape of a letter S**

![](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/gateau-danniversaire-en-forme-de-lettre-S-3.jpg)

**Ingredients** for the castel cake with peanuts:   
(for two rectangles of 22 on 35 cm, to have two large letters S printed on a sheet A4) 

  * 450 gr unhunted almonds 
  * 225 gr of flour 
  * 1 C. half and half yeast 
  * 250 gr of butter 
  * 375 gr of icing sugar 
  * 7 egg whites 

For Swiss meringue butter cream: (to generously cover the cake) 
  * 90 gr of egg white 
  * 180 gr of sugar 
  * 270 gr of butter 
  * vanilla 

decoration: 
  * macaroons, 
  * strawberries 
  * raspberries 
  * blueberries (blueberries) 
  * mini calisson 



**Realization steps** cake preparation: 

  1. mix the crushed peanuts with the sugar, the yeast, the flour and the melted butter, 
  2. make the egg whites rise, then add them to the previous mixture delicately. 
  3. divide the dough on two, and pour into two trays lined with parchment paper 25 x 30 cm, and bake at 180 ° C 
  4. once cooked, let cool and trace the letter of your choice or the number. and keep aside (know that the falls of the cake that taste beautifully with a tea or coffee, so much that it is good) 

preparation of the cream: 
  1. mix the sugar with the egg white 
  2. place it in a bain-marie, and mix, not whip. mix until the grains of sugar are completely dissolved in the eouf white, you can check this by touching the mixture and between your fingers, you must not feel the grains of sugar anymore. 
  3. when the mixture is well viscous it is ready, remove it from the bain marie 
  4. whip the good to have a very beautiful meringue, the ideal is to work with your trouble. 
  5. Now change the squeegee of your whip, and start to beat the meringue mixture and add the butter slowly. 
  6. at some point, the mixture will attach to the whip squeegee, do not worry, you do not miss your cream, keep whipping while adding the butter, and you'll have a nice butter cream . 

cake assembly: 
  1. spread a layer of cream on your first cake. Place on the second cake. 
  2. place the rest of the butter cream in a pastry bag, and make rosettes on your cake to cover the surface. 
  3. once the cake is well covered, decorate with the fruits and cakes that you have at your disposal. 



![Birthday cake in the shape of letter S 4](https://www.amourdecuisine.fr/wp-content/uploads/2018/03/gateau-danniversaire-en-forme-de-lettre-S-4.jpg)
