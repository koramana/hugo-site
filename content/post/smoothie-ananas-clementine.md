---
title: Clementine pineapple smoothie
date: '2016-01-22'
categories:
- juice and cocktail drinks without alcohol
- cooking fat-burning, slimming recipes
- Detox kitchen
- Healthy cuisine
- recipes at least 200 Kcal
tags:
- Organic cuisine
- Fruits
- Diet
- Slimming kitchen

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-ananas-cl%C3%A9mentine-1.jpg
---
[ ![clementine pineapple smoothie 1](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-ananas-cl%C3%A9mentine-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-ananas-cl%C3%A9mentine-1.jpg>)

##  Clementine pineapple smoothie 

Hello everybody, 

It's not because it is cold as not possible that I will deprive myself of my smoothies! We all have this idea in mind that a hot beverage, a hot chocolate, or a coffee with good milk, will warm us well with this cold. Certainly it's not wrong, but also it must be said that a well-balanced smoothie vitamin C and anti-oxidant is absolutely blessed to start his day. 

Besides, I did not understand that, until I started drinking smoothies every morning, this became the little charade of the day: what fruit goes with the other, and what is it? this combination will give. The result is no exaggeration: a delight every time. This time my smoothie is based on Clementine, pineapple and melon .... A sublime combination that will give you energy for a long part of the day is duracell my friends, hihihihihi. (Do not drink it at night)   


**Clementine pineapple smoothie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/Smoothie-ananas-cl%C3%A9mentine.jpg)

portions:  4  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 4 Clementines 
  * 1 pineapple 
  * 1 half melon 
  * ½ glass of ice cubes 



**Realization steps**

  1. peel clementines, and pineapple 
  2. Cut pineapple and melon into pieces 
  3. place all the ingredients in the bowl of the blinder, and mix to have a smooth drink. 
  4. enjoy immediately. 



[ ![pineapple clementine smoothie](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-cl%C3%A9mentine-ananas.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/smoothie-cl%C3%A9mentine-ananas.jpg>)
