---
title: griweche - griwech - fried Algerian cake
date: '2012-03-17'
categories:
- gateaux algeriens- orientales- modernes- fete aid
- gâteaux sablés, ghribiya

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/gateau-algerien-griwech-057_thumb1.jpg
---
griweche Algerian cake. hello everyone, a crispy delight, well coated with honey, which comes from the Algerian cuisine, it looks like the Moroccan Chebakiya, but still it is a typical Algerian cake, that our mothers have inherited from our great grandparents. mothers. & Nbsp; we go to the ingredients: & nbsp; 500 gr of flour 150 gr of butter or margarine (butter for me) 1 egg 1/2 case of sugar (1 tablespoon) 1/2 sachet of baking powder (1/2 cac) 1/2 case of vinegar 1 case of orange blossom water 1 small pinch of honey salt for & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** 4.75  (  1  ratings)  0 

#  griweche Algerian cake. 

![Algerian cake griwech 057](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/gateau-algerien-griwech-057_thumb1.jpg)

Hello everybody, 

a crispy delight, well coated with honey, which comes from the Algerian cuisine, it looks like the Moroccan Chebakiya, but remains that it is a typical Algerian cake, that our mothers inherited from our great grandmothers. 

[ ![22225412](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/22225412_31.jpg) ](<https://www.amourdecuisine.fr/article-25345475.html>)

we go to the ingredients: 

  * 500 gr of flour 
  * 150 gr of butter or margarine (butter for me) 
  * 1 egg 
  * 1/2 case of sugar (1 tablespoon) 
  * 1/2 sachet of baking powder (1/2 cac) 
  * 1/2 case of vinegar 
  * 1 case of orange blossom water 
  * 1 small pinch of salt 
  * honey for garnish 
  * sesame seeds for garnish 



{{< youtube 7UJxTtrvc3g >}} 

Melt the butter without boiling, sift the flour in a bowl, sprinkle with sugar, salt and yeast and mix everything together. 

make a fountain, pour the butter into the center, mix well with the flour, if you have lumps, that is to say large balls of butter coated with flour, pass this mixture through the sieve, rub it well to do it pass, you will have a nice homogeneous mixture flour-butter. 

add the vinegar, the orange blossom water, and the egg a little bit beaten. incorporate all this mixture well into your flour, then wet with water, to have a soft, homogeneous paste, do not work the dough too much so that it does not become elastic. 

divide this dough into a ball and leave to rest under a film of food, or a clean cloth. 

then take balls one at a time and roll them about 2 to 3 mm thick 

cut small rectangles on the wheel, and cut five strips in these rectangles, making four cuts, or if you have a special mold, it will be even better. 

take a rectangle in your left hand, pass your finger with your right hand between the odd straps (1 strap in front, 1 strap behind, etc.) 

Grab the left corner at the top: 

[ ![22225431](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/22225431_31.jpg) ](<https://www.amourdecuisine.fr/article-25345475.html>)

and pull between the thongs, to form a kind of fish stop 

[ ![22225438](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/22225438_31.jpg) ](<https://www.amourdecuisine.fr/article-25345475.html>)

repeat the same operation until all the dough has been used up. 

Once all the cakes are made, dip them one by one into a very hot frying, but on a low heat., brown both sides, and drain well. 

[ ![22225446](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/22225446_31.jpg) ](<https://www.amourdecuisine.fr/article-25345475.html>)

dip them in melted honey, then drain, sprinkle on both sides of each cake with slightly grilled sesame seeds. 

the cake was very delicious and melting in the mouth 

[ ![22225462](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/22225462_31.jpg) ](<https://www.amourdecuisine.fr/article-25345475.html>)

this cake can be kept for 2 or 3 weeks, 

after a week if your cakes start to dry out, sprinkle them with a little honey at the time of consumption 

[ ![Griwech](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/568102383_14009601.gif) ](<https://www.amourdecuisine.fr/article-25345475.html> "GIF Montages for your Blog")
