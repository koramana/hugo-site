---
title: chocolate cake
date: '2011-12-19'
categories:
- bavarois, mousses, charlottes, agar agar recipe
- sweet recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/03/208525431.jpg
---
& Nbsp; we had a great chocolate cake this afternoon, and it was very easy to do, but also, very good, I did not have the chance to take the details of the cakes in photo, because my photo camera unloading (the batteries), and we were treated to a photo after that rachid took a good part of the cakes: the ingredients: for cakes: 150 gr of leavening flour 1 cac of baking powder 40 gr of cocoa 175 gr of margarine 175 gr of sugar crystallize 3 eggs 1 case of instant coffee garnish 75 & hellip; 

##  Overview of tests 

####  please vote 

**User Rating:** Be the first one!  0 

![gat](https://www.amourdecuisine.fr/wp-content/uploads/2012/03/208525431.jpg)

we had a great chocolate cake this afternoon, and it was very easy to do, but also, very good, I did not have the chance to take the details of the cakes in photo, because my photo camera to unload (the batteries), and we were treated to a photo after rachid took a good part of the cakes: 

Ingredients: 

for cakes: 

  * 150 gr of leavening flour 
  * 1 cup of baking powder 
  * 40 gr of cocoa 
  * 175 gr of margarine 
  * 175 gr of sugar crystallize 
  * 3 eggs 
  * 1 case of instant coffee 



garnish 

  * 75 gr of butter 
  * 250 ml of evaporated milk 
  * 50 gr of crystallized sugar 
  * 150 gr of chocolate 
  * chocolate rape or roll 



preheat the oven to 190 degrees, oil 2 small molds of 20 cm diameter each. 

cover the base with parchment paper 

mix the flour, the baking powder, the cocoa in a large container, add the margarine, the crystallized sugar, and the egg, mix well with the beater, make the coffee in 2 cases of boiling water, add to the mixture and mix again until you have a creamy dough. 

pour this pasta equitably into the two molds, arrange the surface with a spatula, because the dough is not liquid it is a little hard so it remains like a soft block in the mold. 

put in the oven for 20 minutes, leave to cool for 10 minutes before draining, and leave to cool on a wire rack. 

now prepare the cream, putting in a saucepan, the butter, the evaporating milk, the sugar and the chocolate, bring to the heat and mix until all the ingredients are melange well, remove from the heat, let cool, and then beat the mixture well until it becomes thick and creamy. 

cover the first cake with the cream, put on the second cake, and cover well the whole surface, and decorate finalemt with the chocolate. 

and good appetite, it's a real treat, so for evaporating milk it's a non-sugar milk concentrate 

bon appetit. 
