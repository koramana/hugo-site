---
title: turkey mtewem, turkey meatballs in sauce
date: '2014-05-13'
categories:
- Algerian cuisine
- Dishes and salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/boulettes-de-dinde-en-sauce-057_thumb.jpg
---
[ ![balls-of-turkey-in-sauce-057_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/boulettes-de-dinde-en-sauce-057_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/boulettes-de-dinde-en-sauce-057_thumb.jpg>)

##  turkey mtewem, turkey meatballs in sauce 

Hello everybody, 

a very good dish, very delicious with minced turkey meat, which looks very much like mtewem, a recipe I found in a cookbook "Koul Cool" by Mrs. ROUABHI Insaf. 

a dish that we love, and that I highly recommend, because the turkey meatballs are very tender, compared to the meatballs minced. 

You can easily change the turkey to make this mtewem with chicken meat, and it's going to be a real delight ... especially with these small pieces of sautéed mushrooms that add a special touch to the dish. 

You can also see on the blog, the recipe of [ mtewem white sauce ](<https://www.amourdecuisine.fr/article-mtewem-sauce-blanche.html> "mtewem white sauce") , and the recipe of [ mtewem red sauce ](<https://www.amourdecuisine.fr/article-mtewem-sauce-rouge-boulettes-de-viande-hachee-a-l-ail.html> "mtewem red sauce - minced garlic meatballs") . 

**turkey mtewem, turkey meatballs in sauce**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/boulettes-de-dinde-en-sauce-062_thumb.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients**

  * 2 turkey breast (I did not have one, I put chicken legs) 
  * 500 gr ground turkey 
  * 1 large grated onion 
  * 1 head of garlic 
  * 1 small box of mushrooms 
  * parsley 
  * 3 tablespoons of oil 
  * Salt - pepper - cumin 



**Realization steps**

  1. heat the oil and fry the piece of turkey or chicken, with half of the onion and grated garlic, and the spices for 5 min. 
  2. cover with water and cook. 
  3. Mix the chopped turkey, onion and remaining garlic, parsley and spices. 
  4. Mix it and shape dumplings. 
  5. fry the mushrooms in a little oil. 
  6. once the meat in the sauce is cooked, plunge the meatballs and mushrooms and let it cook. 
  7. let the sauce reduce, add parsley and quench. 



[ ![2012-08-03-balls-of-turkey-in-sauce_thumb](https://www.amourdecuisine.fr/wp-content/uploads/2013/05/2012-08-03-boulettes-de-dinde-en-sauce_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/05/2012-08-03-boulettes-de-dinde-en-sauce_thumb.jpg>)
