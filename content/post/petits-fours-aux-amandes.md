---
title: almond cookies
date: '2016-04-06'
categories:
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/12/petits-fours-aux-amandes.CR2_thumb-300x200.jpg
---
##  almond cookies 

Hello everybody, 

here are very good almond cookies, super fondant, and very tasty, I saw this recipe on the show "Chhiwate Choumicha" on the 2M channel. 

to make these cakes you will need a special buffer, it looks a bit like the mold to maamoul, but a little deep. These almond cookies are really a real treat. I still change a little recipe Choumicha, for example for stuffing, she prepared a kind of marzipane (almond paste flavored), and for decoration, she mix the jam with glucose (which I did not find here) so the jam froze well. 

**almond cookies**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/petits-fours-aux-amandes.CR2_thumb-300x200.jpg)

portions:  40  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients**

  * 4 egg yolks 
  * 200 gr icing sugar. 
  * 450 gr of flour 
  * 250 gr of butter 
  * 20 gr of cornflour 
  * salt 

the joke: 
  * 100 gr of ground almonds 
  * jam 
  * aromas according to jam 

decoration 
  * jam 
  * 1 cup agar agar 



**Realization steps**

  1. method of preparation: 
  2. beat the butter and icing sugar well, add the egg yolks. 
  3. then add the cornstarch, and then slowly add the flour with a little salt, until you have a dough that picks up. 
  4. in another container, pick up the almond with the jam of your choice, and its aroma, if it is an orange jam, add the orange aroma, if it is an apricot jam, add flavor apricot, and so on ...   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/12/petits-fours-aux-amandes-gateaux-marocains-de-choumicha11.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/12/petits-fours-aux-amandes-gateaux-marocains-de-choumicha11.jpg>)
  5. shape small meatballs with almond stuffing, 1.5 cm in diameter. and set apart. 
  6. Now take a small portion of the shortbread dough, and cover a dumpling of almond stuffing, to have a small dumpling almost 4 cm in diameter. 
  7. squeeze the meatballs into your mold, to have nice little ovens. 
  8. cook in a preheated oven at 160 degrees C for 10 to 20 minutes 
  9. let the petit fours cool, and decorate them with jam that you will preheat and mix with a little agar agar, or as choumicha, jam and glucose. 



bonne dégustation 
