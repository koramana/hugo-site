---
title: Samira economic cakes, kaikaates with flan
date: '2013-12-28'
categories:
- crepes, waffles, fritters

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/12/kaika%C3%A2tes-au-flan-G%C3%A2teaux-Economiques-Samira.CR2_.jpg
---
[ ![Samira economic cakes, kaikaates with flan](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/kaika%C3%A2tes-au-flan-G%C3%A2teaux-Economiques-Samira.CR2_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/kaika%C3%A2tes-au-flan-G%C3%A2teaux-Economiques-Samira.CR2_.jpg>)

##  Samira economic cakes, kaikaates with flan 

Hello everybody, 

I share with you today the recipe for these delicious economic cakes from the Samira book series. Glazed Kaikaates, or as we write here, made by Lunetoiles. 

I had the chance to taste these cakes, because Lunetoiles sent me a package after the help, besides if you notice the first two photos I took them because the almond blossom was a little deformed during transport, I had to redo them for the photos, because Lunetoiles did not make final photos of this cake. 

the cake is very simple to make, very melting in the mouth, with the crisp layer of icing, a pleasure in the mouth, I advise you to make these small delights.   


**Samira economic cakes, kaikaates with flan**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/les-couronnes-kaika%C3%A2tes-au-flan.CR2_.jpg)

Recipe type:  Algerian cake, biscuit.  portions:  50  Prep time:  20 mins  cooking:  15 mins  total:  35 mins 

**Ingredients** dough : 

  * 250 g of soft butter 
  * 3 eggs 
  * ½ glass of icing sugar (about 100 g) 
  * vanilla 
  * 2 bags of vanilla flan 
  * flour (more than 500 g) 

icing: 
  * 1 egg white 
  * vanilla 
  * ½ glass of orange blossom water 
  * 1 tablespoon lemon juice 
  * icing sugar (900 g) 

decoration: 
  * almond paste 
  * shiny food 
  * food coloring 



**Realization steps**

  1. In a bowl, mix the butter with the sugar, then add the eggs and the custard, mix, add the vanilla and gradually the flour until a firm dough. 
  2. Make a large sausage and cut into equal pieces, then these pieces form small sausages, join the ends to form a bracelet. 
  3. Put all the bracelets in a floured dish and bake in hot oven until complete gilding. Remove from the oven and let cool. 

Prepare the icing: 
  1. beat the egg white then add the water, flower vanilla and lemon juice. Stir in the icing sugar little by little until you have a thick enough mixture to cover the cakes. 
  2. Dip the bracelets in this glaze and place on a rack, let dry. We can if we want to decorate the top of shiny food. 
  3. Lower the colorful marzipan and cut flowers with the cookie cutter, where to use an impression mold and attach it to the bracelet 
  4. Present in boxes. 



[ ![Samira Economy Cakes](https://www.amourdecuisine.fr/wp-content/uploads/2013/12/G%C3%A2teaux-Economiques-Samira.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2013/12/G%C3%A2teaux-Economiques-Samira.jpg>)
