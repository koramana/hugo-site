---
title: Algerian rechta, Algerian cuisine
date: '2016-12-10'
categories:
- Algerian cuisine
- Plats et recettes salees

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/09/rechta-alg%C3%A9roise-1.jpg
---
![rechta-algeroise-1](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/rechta-alg%C3%A9roise-1.jpg)

##  Algerian rechta, Algerian cuisine 

Hello everybody, 

and here is my delicious  **[ home made rechta ](<https://www.amourdecuisine.fr/article-rechta-fait-maison-pour-l-aid-el-kbir.html> "rechta - homemade") ** with a very good white sauce turnip taste, I do not tell you the delight, after a day of fasting, it is a dish that is only savored. 

I woke up early in the morning to prepare it, and as soon as I got back from shopping, I prepared our meal, and especially the sauce for this very good dish, and I mention, that although it's pasta, dish is very light, I do not speak "light" to say "diet" but light to say that it tastes more easily than a chekhchoukhat dfar, or even chakhchoukha biskria, finally, remains that it is my opinion, despite I like both dishes. 

**Algerian rechta, Algerian cuisine**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/09/rechta-alg%C3%A9roise.jpg)

Recipe type:  Algerian cuisine  portions:  4  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients**

  * chicken legs, or pieces of meat 
  * 1 onion 
  * 3 tablespoons of oil (or mixture of smen and oil) 
  * salt, pepper, 1 stick of cinnamon 
  * precooked soaked chickpeas 
  * 200 g white turnips 
  * 2 small zucchini 
  * 1 kg of homemade rechta 



**Realization steps**

  1. In a couscous maker, prepare the sauce with chopped chicken, grated onion, salt, pepper, cinnamon, oil and smen. 
  2. Make return 2 to 3 minutes. Add 2 liters of water and the chickpeas. 
  3. As soon as the sauce begins to boil, go to steam (top of the cuckoo) the fresh rechta. 
  4. after 15 min, remove the rechta, pour it in a big salad bowl, sprinkle it with a little sauce that is being cooked 
  5. open between your fingers, and coat with a little oil 
  6. still wear the recha with evaporation. 
  7. When the chicken is cooked, remove it from the sauce. Book. 
  8. Add to the sauce turnips and zucchini cut in half lengthwise. Let it cook 
  9. Serve the drizzle drizzled with sauce, and garnished with vegetables, chick peas and chicken pieces 
  10. present with a little cinnamon powder sprinkled on top 



[ ![rechta at navis 009](https://www.amourdecuisine.fr/wp-content/uploads/2012/07/rechta-au-navis-009_thumb.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/07/rechta-au-navis-009.jpg>)

photo 2008 
