---
title: 'recipes around an ingredient # 9 the tomato'
date: '2015-08-09'
categories:
- Coffee love of cooking

---
**Virginia** chose for this round: **The tomato,** comme ingredient vedette. Alors je vous invite a venir participer a ce jeu, un jeu amical de partage ou il n’y a ni gagnant ni perdant, juste le plaisir de découvrir de nouveaux blogs, et pourquoi pas découvrir le votre, ainsi que de partager différentes recettes autour d’un seul ingrédient. 
