---
title: easy strawberry pie
date: '2018-04-15'
categories:
- cakes and cakes
- sweet recipes
- pies and tarts
tags:
- Defi Sweet or Salty Pies
- Cakes
- To taste
- Custard

image: https://www.amourdecuisine.fr/wp-content/uploads/2012/05/tarte-aux-fraises-%C3%A0-la-cr%C3%A8me-patissi%C3%A8re.jpg
---
![easy strawberry pie](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/tarte-aux-fraises-%C3%A0-la-cr%C3%A8me-patissi%C3%A8re.jpg)

##  easy strawberry pie 

Hello everybody, 

And here is a **easy strawberry pie with cream pastry** can be prepared very quickly at night for breakfast! hihihihi ... .Yes, not a joke! I wanted to have breakfast in the morning with a creamy cake, and what could I do? I open my fridge and a nice little basket of **strawberries** fresh made me an eye ... hihihi ... So no more delicious than a strawberry pie with custard cream all unctuous. 

In any case between the dough and cream pastry it did not take me 20 minutes, yet I did everything by hand, yes children slept and I did not want to wake up to the sound of the robot or electric whip. 

I left the shortbread frozen in the freezer for 10 minutes, and after cooking I put a few minutes at the edge of the kitchen window, by this cold, it quickly cooled, same thing for the cream pastry, and my pie Strawberry Shortcake was cool in less than 75 minutes (1 hour and 15 minutes) 

**easy strawberry pie**

![](https://www.amourdecuisine.fr/wp-content/uploads/2012/05/tarte-aux-fraises-%C3%A0-la-cr%C3%A8me-patissi%C3%A8re-2.jpg)

**Ingredients** For the shortbread (which can cover a mold 27 cm in diameter easily): 

  * 250 gr of flour 
  * 110 gr of hardened sugar 
  * 130 gr of butter 
  * 40 gr of ground almonds 
  * 1 egg 
  * vanilla powder 

For pastry cream: 
  * 500 ml of milk 
  * 130 gr of powdered sugar 
  * 4 egg yolks 
  * 50 gr of cornflour 
  * 1 vanilla pod 

For the decoration: 
  * fresh strawberries 
  * topping or strawberry jam 



**Realization steps** prepare the pie shell: 

  1. sift the flour, sugar and almond powder in a bowl, 
  2. add the butter in pieces at room temperature and sand the mixture between the hands. whip the egg with the vanilla, and pour into the previous mixture 
  3. pick up the dough into a ball, and let it cool for 30 minutes. 
  4. Preheat the oven to 180 ° C, th.6. 
  5. Spread the dough, and garnish a pie pan. Stitch the bottom of the dough with a fork, cover with baking paper and place on white beans 
  6. bake to cook the dough. When cooked, and a little brown, let cool. 

Prepare the pastry cream: 
  1. Split the vanilla pod in half, scrape the seeds and put all in the milk you will bring to the boil. 
  2. In a bowl, whisk the yolks and sugar until the mixture is white. Then add the cornflour. 
  3. Pour the boiling milk over the previous mixture, stir well. 
  4. Then put everything back in the pan. Make thicken, without mixing. 

mounting of the pie: 
  1. after cooling the pie shell and cream pastry, place the shortbread on a serving dish, garnish with the custard a whipping. 
  2. garnish with strawberries to your taste, and top with strawberry jam a little heated 
  3. Place in the fridge. 
  4. Enjoy very fresh! 



Bonne degustation, en tout cas le matin elle était bien délicieuse ma tarte aux fraises à la crème pâtissière. 
