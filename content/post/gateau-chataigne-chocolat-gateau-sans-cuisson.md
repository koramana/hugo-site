---
title: chestnut cake - chocolate cake without cooking
date: '2017-11-11'
categories:
- dessert, crumbles and bars
- Algerian cakes without cooking
- cakes and cakes
- sweet recipes
tags:
- desserts
- fall
- Gluten-free cooking
- fondant
- Pastry
- To taste
- Algerian cakes

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/10/g%C3%A2teau-ch%C3%A2taigne-chocolat-sans-cuisson.jpg
---
[ ![chestnut cake - chocolate](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/g%C3%A2teau-ch%C3%A2taigne-chocolat-sans-cuisson.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/g%C3%A2teau-ch%C3%A2taigne-chocolat-sans-cuisson.jpg>)

##  chestnut cake - chocolate cake without cooking 

Hello everybody, 

A cake without melting and delicious cooking that tells you? I'm really keen on it, I'm just waiting for them to start selling the chestnuts here in England, to make this Lunetoiles delight chestnut cake - chocolate cake without cooking. 

The recipe is super easy, as it is very delicious, just see and have the ingredients, after less than an hour, you can enjoy this delectable fondant to the infinite, to the taste of chestnut and chocolate. 

**chestnut cake - chocolate cake without cooking**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/g%C3%A2teau-ch%C3%A2taigne-chocolat.jpg)

portions:  8  Prep time:  20 mins  cooking:  25 mins  total:  45 mins 

**Ingredients**

  * 700 g of chestnuts 
  * 220 g of dark chocolate pastry 
  * 150 g caster sugar 
  * 2 sachets of vanilla sugar or 1 vanilla pod 
  * 150 g of soft butter 
  * a little cocoa powder 



**Realization steps**

  1. Cook the chestnuts in water for 20 to 30 minutes. 
  2. Peel them by removing the two skins. 
  3. Grind them in powder using a robot equipped with a steel blade, or pass them to the vegetable mill (puree), to obtain a purée. 
  4. Gather the powder of chestnuts, the soft butter and mix in a bowl, then add the sugars (and grains of the vanilla pod if desired). 
  5. Melt the chocolate in a bain-marie. 
  6. Pour the melted chocolate on the mixture. Mix well. 
  7. You get a pretty thick dough. 
  8. Pour the mixture into a cake mold (cake mold type, lined with cellophane) and keep in a cool place for at least 4 hours. 
  9. The more you leave it, the better it is. 
  10. Unmould carefully, and cut slices, or cubes as you wish. 
  11. Serve sprinkled with a little cocoa powder. 



[ ![chestnut cake - chocolate 1](https://www.amourdecuisine.fr/wp-content/uploads/2014/10/g%C3%A2teau-ch%C3%A2taigne-chocolat-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/10/g%C3%A2teau-ch%C3%A2taigne-chocolat-2.jpg>)
