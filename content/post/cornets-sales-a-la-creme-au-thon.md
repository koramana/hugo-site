---
title: salted cones with tuna cream
date: '2017-02-25'
categories:
- amuse bouche, tapas, mise en bouche
- Plats et recettes salees
tags:
- Easy cooking
- accompaniment
- Ramadan recipe
- inputs
- Algeria
- Cocktail dinner
- puff

image: https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cornets-017_thumb21.jpg
---
##  salted cones with tuna cream 

Hello everybody, 

I love this nice entry, the salted cones with tuna cream, just know how to prepare the cornets puff pastry, the rest is a breeze. It's a recipe that I unearth from the archives of my blog of the year 2010, surely that I must redo this delight once again for my children to redo new photos. 

A wonderful recipe, very presentable in a buffet as an aperitif, this stuffing of tuna cream and just wonderful and covered with this crisp layer of puff pastry, I do not tell you the delight.   


**salted cones with tuna cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cornets-017_thumb21.jpg)

**Ingredients**

  * Ingredients: 
  * a roll of puff pastry 
  * a small box of tuna in the oil 
  * 2 boiled eggs 
  * a small handful of pitted olives 
  * a little mayonnaise 
  * salt, black pepper, coriander ground, a little paprika for decoration 
  * chopped coriander 
  * grated cheese 
  * butter for stainless steel molds 
  * egg for gilding 
  * seed of nigella 



**Realization steps**

  1. in any case, for the farce, each one his means and his taste, me if what I had on hand, I wanted something else, it will be for the next time.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cornets2_22-300x240.jpg)
  2. cut strips of puff pastry, 
  3. roll them around a mold in the shape of cornet (cones), well buttered 
  4. brush them with egg and garnish with seeds of nigella. 
  5. cook in a preheated oven at 180 degrees for almost 15 min or less, depending on your oven. 
  6. at the outlet of the oven let cool before unmolding, turning the mold do not pull ok.   
![](https://www.amourdecuisine.fr/wp-content/uploads/2011/07/cornets-1_thumb21.jpg)
  7. for the stuffing, after cooking the eggs, crush them, add the tuna mayonnaise, the olives cut in a slice or piece, the grated cheese, and season to your taste, 
  8. fill with stuffing, decorate, and enjoy, a true crispy delight. 
  9. it's not an invented recipe, it's on many blogs, it's up to you to make new jokes, and why not share them with us 


