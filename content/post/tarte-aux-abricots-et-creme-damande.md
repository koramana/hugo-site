---
title: apricot tart and almond cream
date: '2013-06-30'
categories:
- appetizer, tapas, appetizer
- idea, party recipe, aperitif aperitif
- ramadan recipe
- fish and seafood recipes
- salty recipes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-d-abricots-a-la-creme-amandines1.jpg
---
![pie-and-apricot-a-la-creme-amandines.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-d-abricots-a-la-creme-amandines1.jpg)

##  apricot tart and almond cream 

Hello everybody, 

a seasonal pie, or even that you can make out of the apricot season, because it is with dried apricots .... does it tempt you? 

it's a recipe from my dear Lunetoiles, who was waiting to be published since March .... 

then to your aprons my dear, and prepare your ingredients .... 

**apricot tart and almond cream**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-aux-abricots-et-creme-d-amande1.jpg)

portions:  8  Prep time:  30 mins  cooking:  35 mins  total:  1 hour 5 mins 

**Ingredients** For a mold or pie circle of 23 cm For sweet pie dough: 

  * 180 g flour 
  * 70 g icing sugar 
  * 80 g of ointment butter 
  * 3 egg yolks 

For the almond cream 
  * 65 g of butter 
  * 80 g icing sugar 
  * 90 g of almond powder 
  * 1 egg 

For pastry cream 
  * ½ liter of fresh whole milk 
  * 45 g of Maïzena 
  * 125 g caster sugar 
  * 6 egg yolks 
  * Dry apricots (soaked in water for 24 hours to rehydrate) 



**Realization steps**

  1. Method of preparation 
  2. Prepare the pie dough: Put the flour and the sifted icing sugar in a bowl. Add the ointment butter and squeeze it. 
  3. When there are no more large pieces of butter, stir in the egg yolks. Keep cold for 30 minutes. Lower to 3 mm thick on a floured work surface and darken a circle 23 cm in diameter. Pass around the edge with the roller to drop the excess dough. Prick the bottom of many forklifts and slide the pie shell in the refrigerator while preparing the filling. 
  4. Prepare the custard: Put the milk to heat in a saucepan until boiling. 
  5. Beat the egg yolks with the sugar, until the mixture whitens. Then add the cornstarch and beat again. 
  6. At the first boiling of the milk, pour it on the yellow mixture of eggs / sugars / cornflour, without ever stop whipping. Beat until smooth and smooth. 
  7. Pour everything back into the saucepan and put back on the heat. Reporter has boiling without ever stop whipping. When the cream has thickened (count 1 minutes from boiling, remove from heat, place in a bowl, and let cool.) Cover the cream with cling film to prevent skin from forming. 
  8. Prepare the almond cream: Beat the butter with the icing sugar, add the almond powder, then the egg, without turning with the spatula. 
  9. Preheat the oven to 200 ° C. 
  10. Spread 200 gr of custard cream on the pie shell and put all the almond cream on top, then spread it well. 
  11. Cut the apricots in 2, arrange them on the almond cream. 
  12. Put the pie in the oven for 10 minutes at 190 ° C, then lower the oven temperature to 160 ° C and cook for 50 minutes. 
  13. With the excess of dough and custard, I made small tartlets with apricots in cream. 



![pie-frangipani-a-l-abricot.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/06/tarte-frangipane-a-l-abricot1.jpg)

Thank you for your visits and comments 

At the next recipe 

Passez une agréable journée 
