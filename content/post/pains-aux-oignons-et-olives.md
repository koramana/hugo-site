---
title: bread with onions and olives
date: '2015-09-14'
categories:
- appetizer, tapas, appetizer
- Mina el forn
- pain, pain traditionnel, galette
- pizzas / quiches / tartes salees et sandwichs
tags:
- Algeria

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-aux-oignons-et-olives.jpg
---
[ ![Bread with onions and olives](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-aux-oignons-et-olives.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/Pain-aux-oignons-et-olives.jpg>)

##  bread with onions and olives 

Hello everybody, 

During Ramadan, I watched the Samira Tv channel, when I saw on the show "bennat zmane" this recipe for bread with onions and olives ... To be honest, I did not pay too much attention to the preparation of the dough, but I really liked the final result ... 

D-day came, ie last week, I went to read the recipe on the blog of Shahira to make these loaves ... But here, reading the ingredients of the dough, I found that the recipe looks like a little to my favorite dough, my [ Pizza dough ](<https://www.amourdecuisine.fr/article-pate-a-pizza-maison-moelleuse-et-croustillante.html>) and as I do not like to get too far from my landmarks, I realized the recipe with my dough ... The only little advice that I will give you: do not do like, plan a large enough mold, to have a dough not too high, and to be able to fill easily the breads with the stuffing onions - olives.   


**bread with onions and olives**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pains-aux-oignons-et-olives-1.jpg)

portions:  5  Prep time:  40 mins  cooking:  15 mins  total:  55 mins 

**Ingredients** Dough: 

  * 3 glasses of flour (240 ml glass) 
  * 1 tablespoon of sugar   
(you can decrease the sugar a bit, it's just that the sugar allows the dough to have a more beautiful golden color) 
  * 1 teaspoon of salt 
  * 1 tablespoon instant baker's yeast 
  * 1 cup of baking powder. 
  * 3 tablespoons milk powder 
  * 2 tablespoons of oil 
  * water   
(if you do not have milk powder, do not put it, and replace the water with normal milk) 

prank call: 
  * an onion 
  * black olives according to taste 
  * salt and black pepper from the mill 
  * oil 

gilding: 
  * 1 egg yolk 
  * 1 tablespoon of milk. 



**Realization steps**

  1. place the ingredients of the dough in a large bowl. 
  2. avoid putting salt in front of the yeasts. 
  3. add the oil and sand 
  4. then pick up the dough with the water, until it is soft. 
  5. let rise away from drafts. 

Prepare the stuffing: 
  1. sauté the onions cut in julienne in a little oil, until it becomes translucent. 
  2. add the sliced ​​olives, a little salt, and black pepper. 
  3. let simmer 5 minutes, and remove from heat. 
  4. Degas the dough and cut it to 11 equal pieces. 
  5. roll them, and put them in a mold more than 30 cm in diameter, lightly oiled. Space the balls a little, because it will inflate well after. 
  6. let it rise a little, then make one in each ball with the fingertips, to give a little the shape of a donut to the ball. 
  7. fill each ball with the onion / olives stuffing. 
  8. brush the edges of the balls with the egg yolk / milk mixture. 
  9. let rise a little, and cook in a preheated oven at 180 degrees C, until a beautiful golden color. 



[ ![bread with onions and olives](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pains-aux-oignons-et-olives.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pains-aux-oignons-et-olives.jpg>)
