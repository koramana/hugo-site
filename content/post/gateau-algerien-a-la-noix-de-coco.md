---
title: Algerian cake with coconut
date: '2013-12-05'
categories:
- Algerian cakes- oriental- modern- fete aid
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-gateaux-algeriens-Aid-el-fitr-2012-boules-de-neige_thum.jpg
---
##  Algerian cake with coconut 

Hello everybody, 

Here is an Algerian cake recipe, super good, very easy to make, and above all, it is an Algerian cakes without cooking, what I like cakes without cooking. 

It's coconut snowball stuffed with a nice stuffing of biscuits mixed with nutella, which coats a grilled almond crisp. 

and as we approached the el kirir 2012, many of you had asked me for this delicious recipe, so I share it with you .... 

**Algerian cake without cooking with coconut**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/04/les-gateaux-algeriens-Aid-el-fitr-2012-boules-de-neige_thum.jpg)

Recipe type:  Algerian cake, truffle  portions:  10  Prep time:  20 mins  cooking:  5 mins  total:  25 mins 

**Ingredients**

  * grilled almonds (I put on the spit, because all this cake, is just at random) 
  * a box of biscuits. 
  * Nutella 
  * coconut 
  * sweetened condensed milk 



**Realization steps**

  1. first, toast the almonds. and let cool. 
  2. crush the biscuits, I did not powder it. 
  3. add nutella to the biscuits, until you obtain a soft and malleable paste. 
  4. coat each almond with this mixture, do not mix a lot, otherwise you may have large pellets in the end. 
  5. now mix the coconut with sweetened condensed milk, also to have a dough that picks up. 
  6. coat the Nutella balls with the coconut paste. 
  7. then roll the meatballs into powdered coconut, and your cake is ready. 



this cake can be kept 1 week, otherwise it will become too hard, but I do not think you will resist long, if you are small, do not make a large amount. 

in any case, this cake has been a great success, and personally I will do it again for the help el Kebir 2012 

Source: El Djelfa Forum. 

**حلوى جوز الهند و النوتيلا**   
المقادير:    
لوز    
علبة بسكويت »بيمو»    
شوكولا نوتيلا    
جوز الهند   
حليب معلب حسب الحاجة 

للتزيين:   
جوز الهند 

طريقة التحضيير:   
في وعاء اخلطي بيمو و شوكولا نوتيلا لتشكيل حشوة ضعي داخل هذه الاخيرة حبة لوز و شكلي كريات   
في وعاء اخر ضعي جوز الهند و ضفي الحليب تدريجيا للحصول على عجينة متماسكة اصنعي كويرات و بداخلها الحشو المشكل سابقا   
ثم رمديها في جوز الهند و ضعيها في حاويات 
