---
title: cramique, Belgian brioche
date: '2014-08-04'
categories:
- Buns and pastries
- Mina el forn

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/02/le-cramique-brioche-belge.CR2edited1.jpg
---
![on-ceramic - bun-belge.CR2edited.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/le-cramique-brioche-belge.CR2edited1.jpg)

##  cramique, Belgian brioche 

Hello everybody, 

a sublime Belgian brioche, which is the ceramic, and that I found a pleasure to realize, and the children have a lot to like at breakfast, and even at tea time, with a thin layer of Nutella, sometimes , or then butter and jam. 

for my part I like to taste this brioche super mellow, nature, because the taste of partially cooked grapes, and largely satisfactory for me, I do not need to accentuate it with other flavors.    


**cramique, Belgian brioche**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/02/le-cramique-brioche-019.CR2edited_thumb1.jpg)

portions:  12  Prep time:  45 mins  cooking:  30 mins  total:  1 hour 15 mins 

**Ingredients**

  * 550 gr of flour 
  * 180 ml of milk, (if your eggs are small, and you judge that the dough is not good mole add another 20 ml) 
  * 100 g of sugar, 
  * 3 tablespoons milk powder, 
  * 3 whole eggs 
  * 150 gr of butter at room temperature, 
  * 150 gr of raisins, 
  * 1 sachet of instant yeast, 1 tablespoon. 
  * 8g of salt 
  * butter for the mold. 
  * egg yolk and milk for gilding 



**Realization steps**

  1. place all the ingredients in the bowl of the bread maker in the order recommended for your appliance except the butter.   
I made the brioche on the bread machine, but you can do it with a dough or with your hand (you have to endure a little dough sticking to your hands, at the beginning of the kneading) 
  2. let knead until the dough collects well, and add the butter slowly in small pieces. 
  3. if your dough is a bit sticky, add a little flour. 
  4. leave the dough rested and doubled in volume, between 45 min and 60 min. 
  5. Remove the dough and degas on a very lightly floured work surface while slowly adding the amount of raisins. 
  6. make two balls, (if your cake mold is small, mine is very big, so I only make one brioche) 
  7. Butter the mussels and place each pudding in a mold. 
  8. let your dough rest all the time necessary, and depending on the temperature of the room, until the double dough see triple volume, do not hurry especially, if you want buns airy. 
  9. Bake in the preheated oven at 180 ° and turn on the heat for 30 to 35 minutes. or maybe less, depending on the capacity of your oven. 



![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
