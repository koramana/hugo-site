---
title: Galette des Rois
date: '2018-01-05'
categories:
- Buns and pastries
- Mina el forn
tags:
- Algerian cakes
- Cakes
- Pastry

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/01/galette-des-rois-1-009_thumb1.jpg
---
![galette des rois 1 009](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/galette-des-rois-1-009_thumb1.jpg)

##  Galette des Rois 

Hello everyone, 

my neighbor Margarette Landing, used to leave a lot in France, it's a woman too kind, she came yesterday to tell me, that the last time she had gone to France, she has to eat "a pastry like she called with cream of almond inside, "I told her: you mean the slab of kings, she told me I do not know, so for her I made him this cake, and I went take her away for tea time, she was so happy when she dripped at this delight, and she said to me "this is the cake, this is the cake I loved in Paris years ago" 

I was very happy, she liked it, but I have to say that I did not dare too much, played the photographer in front of the family who was around the cake when she started to cut it, hihihihi, already j I hardly got the camera out of my pocket, when she told me "Did you do any pictures for the blog" "did you make pictures for the blog?" yes everyone knows the story of my blog, hihihiihih. 

so without delay I go to the recipe: 

**Galette des Rois**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/galette-des-rois-2-0231.jpg)

portions:  12  Prep time:  20 mins  cooking:  20 mins  total:  40 mins 

**Ingredients**

  * 2 rolls of commercial puff pastry 
  * 90 g of sugar 
  * 100 g of almond powder 
  * 50 g of butter 
  * 2 whole eggs 
  * aroma of bitter almond 
  * 1 egg for gilding 



**Realization steps**

  1. Place the butter in pieces in a bowl 
  2. add sugar, ground almonds whole eggs 
  3. mix with a low speed electric whisk 
  4. When the dry elements begin to hydrate, increase the power to get a cream. 
  5. Spread the puff pastry according to your, for me my cake has become a rectangle, because our paste leaflet comes on this form, and I did not want to waste too much dough, a rectangle must be a little larger than the other, the most little will be the base 
  6. Pass the egg yolk with a brush all around the sides of the base rectangle (2 cm) 
  7. Garnish the almond cream center with the pastry bag or spatula   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2012/01/galette-des-rois-2_thumb1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2012/01/galette-des-rois-2_thumb1.jpg>)
  8. Lay the second rectangle of the puff pastry and solder with your hands to prevent the escape of the almond cream. 
  9. Chick the edges by exerting a fingertip pressure and incising the two thicknesses of the tip of a knife. 
  10. Drill with the tip of the knife in the center to make a chimney. 
  11. To brown with a beaten egg and to create the decoration in spike or in spiral with the point of the knife. 
  12. bake at 170 ° for 20 minutes. 



Let cool before eating. 

Anyway, we spent a pleasant afternoon with my neighbor, her nephews and my children, who played and asked each time a small piece, I came back home with my empty board, hihihihih 

merci pour vos commentaires, et merci pour vos visites 
