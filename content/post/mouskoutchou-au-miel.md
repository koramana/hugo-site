---
title: honey muskoutchou
date: '2017-09-28'
categories:
- Algerian cakes- oriental- modern- fete aid
- cakes and cakes
- Algerian pastries

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/01/gateau-algerien-2014-mouskoutchou-au-miel-1024x685.jpg
---
[ ![algerian cake 2014, honey muskoutchou](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/gateau-algerien-2014-mouskoutchou-au-miel-1024x685.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/gateau-algerien-2014-mouskoutchou-au-miel.jpg>)

##  honey muskoutchou 

Hello everybody, 

There is no easier than a mouskoutchou, but also it is not easy to succeed, because it is not necessary to break the foam obtained when adding the flour ... incidentally it is the fatal error that many people do, whip whites, and when introducing this mixture to the egg yolk mixture, or the most consistent mixture, it is the war with the spoon .... 

I had been watching an English TV show "the best britain bakery" the best English bakery, and the bakers had a challenge: prepare a 4-stage sponge cake, and this is the first time I see it: the great pastry chef who was watching from afar, was shocked when one of the bakers, was using a wooden spoon, to introduce the flour into the frothy egg and sugar machine. 

The chef said: never use a wooden spoon ???? it's a first for me, and to say that the pastry field is really big, and that we learn every day. 

So we come back to the recipe of this honey mouskoutchou, that Lunetoiles loved to share with us ... 

**honey muskoutchou**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/recette-de-mouskoutchou-1024x685.jpg)

Recipe type:  Dessert, Algerian cake  portions:  8  Prep time:  30 mins  cooking:  25 mins  total:  55 mins 

**Ingredients**

  * 5 eggs 
  * ½ glass of crystallized sugar 
  * 5 tbsp. honey 
  * 1 sachet of baking powder 
  * 150 gr of softened butter 
  * 150 gr of flour 

Frosting: 
  * 100 gr of chocolate 
  * 40 gr of butter 
  * 3 tablespoons of coconut milk 
  * 80 gr of honey 



**Realization steps**

  1. Prepare the ingredients and preheat the oven to 180 ° C. 
  2. Work the sugar and butter into a cream then add the egg yolks. 
  3. Add the honey while beating. 
  4. Stir in sifted flour and baking powder and whisk egg whites. 
  5. Add the egg whites to the egg / butter / honey / sugar / flour mixture and mix gently. 
  6. Pour the dough into a buttered pan. 
  7. Bake for 25 minutes, check the cooking with a wooden spike inserted into the cake, it must come out dry and clean. 
  8. Prepare the icing: Melt the chocolate in a bain-marie, add the honey, the butter and remove the heat and add the honey, mixing until a liquid is obtained. 
  9. Spread icing on the muskoutchou. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/01/mouskoutchou-au-miel-937x1024.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/01/mouskoutchou-au-miel.jpg>)
