---
title: Yennayer 2016, assegwas ameggaz
date: '2016-01-12'
categories:
- Coffee love of cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/01/yennayer.jpg
---
#  [ ![yennayer](https://www.amourdecuisine.fr/wp-content/uploads/2016/01/yennayer.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/01/yennayer.jpg>)

##  **Yennayer 2016, assegwas ameggaz**

Hello everybody, 

On the occasion of the new year Berbere (Amazigh new year), 1st Yennayer, I say to all the people who celebrated it, assegas amegaz (assegwas ameggaz), or Happy New Year .... 

Yennayer marks the New Year's Day of the agrarian calendar, used since antiquity by the Berbers.   
It corresponds to the 1st day of January of the Julian calendar.   
May this year be a year of peace and good harvest 

![assegwas-ameggaz.bmp.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/assegwas-ameggaz.bmp1.jpg)

The families who celebrate this occasion, share according to tradition, a meal based on couscous and poultry (usually turkey); the dish must be rich to symbolize the abundance of the year that starts. 

![Couscous aux cardons.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/01/couscous-aux-cardons.CR2_1.jpg)

For my part, it's going to be a delicious [ couscous with cardoons ](<https://www.amourdecuisine.fr/article-couscous-aux-cardons-couscous-bel-khorchefs.html>) , without the turkey, but has the mutton meat. 

and you can find on this index a lot of other recipe, for this beautiful occasions: 

again a happy new year, assegwas ameggaz 
