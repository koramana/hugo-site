---
title: zuchini bread
date: '2015-10-03'
categories:
- appetizer, tapas, appetizer
- pizzas / quiches / pies and sandwiches
tags:
- Easy cooking
- Vegetarian cuisine
- Healthy cuisine
- Vegetables
- inputs
- Salty cake
- Cake

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pain-de-courgettes-1.jpg
---
[ ![zucchini bread 1](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pain-de-courgettes-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pain-de-courgettes-1.jpg>)

##  Zuchini bread 

Hello everybody, 

in any case, for the recipe of this zucchini bread, I did not have enough zucchini, because as I told you, I a kilogram of zucchini passes in the blink of an eye. The zucchini is always on my table, one way, or another. Unfortunately for this time, when I wanted to make this recipe, when I had all the ingredients, I had only one zucchini ... It did the trick, but with the exact number of zucchini, I could have a bread higher ... Just for info, it's not a bread with a crumb, it's rather a kind of salty cake very fluffy. 

**Previous rounds:**

**zuchini bread**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pain-de-courgettes-2.jpg)

portions:  10  Prep time:  30 mins  cooking:  55 mins  total:  1 hour 25 mins 

**Ingredients**

  * 2 large zucchini (+/- 800 g) 
  * 1 tablespoon of olive oil 
  * ½ white onion 
  * 1 clove garlic peeled and degermed 
  * 6 g of salt 
  * 2 tablespoons chopped fresh chives 
  * 1 tbsp chopped fresh parsley 
  * 1 tablespoon of fresh thyme 
  * 4 eggs 
  * 200 ml whole cream (semi-thick) 
  * 20 g bread crumbs 
  * 10 gr of almond powder 
  * 20 gr of oats flakes 
  * sliced ​​almonds 



**Realization steps**

  1. Preheat the oven to 180 ° C 
  2. Line the bottom of a cake tin with a sheet of parchment paper. 
  3. Add a few slices of zucchini. Season with salt and pepper. (I did not have enough zucchini, so I skipped this step) 
  4. Cut the remaining zucchini into small cubes of about 2 cm section. 
  5. In a pan, heat the olive oil. Sweat the finely chopped onion for a few seconds. Add zucchini cubes and chopped garlic. Season with salt and pepper and add the thyme. 
  6. Cook for about 20 minutes until the zucchini is perfectly tender. 
  7. Pour into a colander and let drain another 20 minutes. 
  8. In a bowl, whisk the whole eggs. Add cream and bread crumbs, almonds and oat flakes. (In the original recipe, there was only 40 grams of breadcrumbs, no almonds or oatmeal) 
  9. Once the zucchini is well drained, add them to the preparation. 
  10. Finish with chives and parsley. 
  11. Pour into the mold, cover with a generous layer of chopped almonds 
  12. cook for 55 minutes, checking the coloring. 
  13. Serve immediately, warm or cold. 



[ ![zuchini bread](https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pain-de-courgettes.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/09/pain-de-courgettes.jpg>)

the list of participants in this round: 

\- **Zucchini bread** by Soulef of the blog [ Amour de cuisine ](<https://www.amourdecuisine.fr/>)
