---
title: Ballerina birthday cake
date: '2013-10-18'
categories:
- vegetarian dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2013/10/balerine-006.CR2-copie-11.jpg
---
![ballerina-006.CR2-copy-1.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/balerine-006.CR2-copie-11.jpg)

##  Ballerina birthday cake 

Hello everybody, 

I share with you today the birthday cake of my little princess ... It has been 6 years, and she asked me to prepare a birthday cake with a ballerina ... A pleasure to achieve this delicious cake, the only problem, when we have lessons, and we are taken all day, and it is only in the evening, when the children sleep that we make the cake secretly ... in any case, a recipe to come very soon and in video ... kisses 

![ballerina-020.CR2.jpg](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/balerine-020.CR2_1.jpg)

For the cake, I prepared 3 biscuits, following this recipe: 

[ Fine biscuit ](<https://www.amourdecuisine.fr/article-biscuit-fin-base-pour-gateau-danniversaire.html> "Biscuit end / base for birthday cake")

then I prepared a [ custard ](<https://www.amourdecuisine.fr/article-creme-patissiere.html> "custard") that I whipped after cooling with butter in [ Chiffon cream ](<https://www.amourdecuisine.fr/article-creme-mousseline.html> "muslin cream \(ideal for strawberry\)") to which I added [ hazelnut praline ](<https://www.amourdecuisine.fr/article-pralin-aux-noisettes.html> "praline with hazelnuts") . 

the decoration is according to taste, but here I covered the cake with marzipan, because children love more almond paste than sugar paste.   


**Ballerina birthday cake**

![](https://www.amourdecuisine.fr/wp-content/uploads/2013/10/balerine-020.CR2_1.jpg)

portions:  12  Prep time:  60 mins  cooking:  30 mins  total:  1 hour 30 mins 

**Ingredients**

  * almond paste 
  * fine biscuit 
  * Chiffon cream 
  * praline with hazelnuts 
  * light syrup. 
  * pink sugar paste 
  * white sugar paste 



**Realization steps**

  1. prepare the cake by following this method: [ fine biscuit with muslin cream ](<https://www.amourdecuisine.fr/article-gateau-fin-a-la-creme-mousseline-au-pralin-de-noisettes.html> "fine cream cake with hazelnut praline muslin")
  2. cover the cake with the cream muslin 
  3. spread the almond paste well and cover the cake 
  4. roll out the white sugar dough and cut 4 to 5 circles of 10 cm, with a round shortbread pan. 
  5. fold each circle in four to give volume, and give the look of a lace petticoat 
  6. roll out the pink dough now and cut 5 to 6 8 cm circles and place them in the folding ones too to give the look of a full volume sun skirt. 
  7. place your doll in the middle, and shape the dress. 


