---
title: milkshake with speculoos
date: '2016-03-14'
categories:
- juice and cocktail drinks without alcohol
tags:
- shakes
- To taste
- Cakes
- desserts
- Easy cooking
- ice cream
- Drinks

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/milkshake-aux-speculoos.jpg
---
[ ![milkshake with speculoos](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/milkshake-aux-speculoos.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/milkshake-aux-speculoos-1.jpg>)

##  milkshake with speculoos 

Hello everybody, 

My kids love milkshakes a lot, and to be honest, when it's time for the milkshake, they become the chefs, same thing for the smoothies. They like to choose the ingredients they want to put in the blinder. 

This time, after the oreo milkshake (I lost the pictures of the recipes in my old ordo, who left and got ready with, hihihih), they asked me for a speculoos milkshake, C'est trop caloric certainly, but it's still good for children. 

You can lighten this milkshake a bit, by replacing the ice cream with ice cubes of water, and replace the whole milk with half-creamed milk, the taste is always going to be a treat, but do not think it's okay spare to take pounds, hihihih it's not a slimming recipe.   


**milkshake with speculoos**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/milkshake-aux-speculoos-1.jpg)

portions:  3  Prep time:  5 mins  total:  5 mins 

**Ingredients**

  * 200 gr of vanilla ice cream 
  * 500 ml whole milk 
  * 250 gr of speculoos 



**Realization steps**

  1. place the speculoos in a plastic bag, and crush the finely with the roller patissier 
  2. place the ice cream, milk and speculoos in the blinder bowl and mix, until it becomes very creamy. 
  3. Savor immediately, stirring occasionally with a spoon, for perfect homogeneity. 



[ ![milkshake with speculoos 2](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/milkshake-aux-sp%C3%A9culoos-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/milkshake-aux-sp%C3%A9culoos-2.jpg>)
