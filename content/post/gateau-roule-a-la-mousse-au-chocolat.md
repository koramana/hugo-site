---
title: Cake rolled with chocolate mousse
date: '2017-11-25'
categories:
- Chocolate cake
- birthday cake, party, celebrations
- cakes and cakes
- sweet recipes
tags:
- To taste
- Easy cooking
- desserts
- Breakfast
- Algerian cakes
- Christmas
- logs

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Recette-de-G%C3%A2teau-roul%C3%A9-%C3%A0-la-mousse-au-chocolat-1.jpg
---
![Recipe of cake-rolls-a-la-mousse au chocolate-1](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Recette-de-G%C3%A2teau-roul%C3%A9-%C3%A0-la-mousse-au-chocolat-1.jpg)

##  Cake rolled with chocolate mousse 

Hello everybody, 

I really like to put my recipes with videos prepared with my own care, but it's a long-term job ... producing a 3-minute video can easily take me 3 hours or more of editing and arrangement, especially with the children around me during the shooting. So do not hesitate to visit my channel and let your "likes"! 

{{< youtube 7JNamrkUaIw >}} 

Now for this sublime cake rolled with chocolate mousse, it is an achievement of my friend Wamani Merou, she shared with us on my group youtube, and that it gives me a lot of pleasure sharing with you today on my blog. 

![Recipe of cake-rolls-a-la-mousse au chocolate-2](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Recette-de-G%C3%A2teau-roul%C3%A9-%C3%A0-la-mousse-au-chocolat-2.jpg)

**Cake recipe rolled with chocolate mousse**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Recette-de-G%C3%A2teau-roul%C3%A9-%C3%A0-la-mousse-au-chocolat-3.jpg)

**Ingredients**

  * 4 eggs, white separated from yolks 
  * ½ cup plus ⅓ cup granulated sugar (1/2 cup = 125 ml) 
  * 1 C. coffee vanilla extract 
  * ½ cup all-purpose flour 
  * ⅓ cup cocoa (75 ml) 
  * ½ c. coffee baking powder 
  * ¼ c. coffee baking soda 
  * ⅛ c. salt coffee 
  * ⅓ cup water (75 ml) 
  * ¼ cup icing sugar (50 ml) 
  * Chocolate syrup 

CHOCOLATE FOAM TRIM: 
  * ¼ cup of sugar (50 ml) 
  * 1 C. gelatin 
  * ½ cup of milk (125 ml) 
  * 1 cup dark chocolate chips (250 ml) 
  * 2 tbsp. coffee vanilla extract 
  * 250 ml cold whipping cream 



**Realization steps** Prepare the CHOCOLATE FOAM FILLER: 

  1. Mix the sugar and gelatin in a small saucepan and stir in the milk. Let stand for 2 minutes to soften the gelatin. Cook over medium heat, stirring constantly until mixture begins to boil. 
  2. Remove from fire. Stir in the chocolate chips immediately and stir until everything is melted. Stir in the vanilla and leave to cool to room temperature. 
  3. Beat the whipping cream in a small bowl until firm. Gradually add the chocolate mixture, stirring gently until the mixture is homogeneous. Cover and store in the refrigerator until ready to use. 
  4. Makes about 3 cups of filling. 
  5. Refrigerate 6 to 8 hours or overnight. 

Prepare the cake. 
  1. Preheat the oven to 190 ° C. 
  2. Line a rolled cake pan with 40 x 25 x 2.5 cm of aluminum foil, generously grease the paper. Sprinkle a thin linen or cotton cloth with ¼ cup icing sugar. 
  3. Beat the egg whites until stiff peaks form in a large bowl. Gradually add ½ cup (125 mL) granulated sugar while beating until stiff peaks form. Beat egg yolks and vanilla in medium bowl with electric mixer on medium speed for 3 minutes. Gradually add the remaining granulated sugar (1/3 cup or 85 ml) and continue mixing for 2 minutes. 
  4. Mix the flour, cocoa, baking powder, baking soda and salt; gently stir in the egg yolk mixture, alternating with the water, at low speed until a smooth dough is obtained. Gradually add the chocolate mixture to the egg whites until all is well mixed. Spread the dough evenly in the prepared pan. 
  5. Bake for 12 to 15 minutes or until the cake resumes its shape when gently pressed in the center. Immediately remove the cake from the edges of the mold and spill it over the prepared cloth. Remove the aluminum foil carefully. Immediately roll the cake into the laundry, starting at one of the narrow ends. Put on a rack and let cool completely. 
  6. Carefully unroll the cake and remove the laundry. Stir the filling gently until the mixture is of consistency to spread. Spread the filling on the cake and roll it again. Refrigerate several hours. Sift the powdered sugar on the cake just before serving. Serve the cake with a syrup coulis and garnish to taste. Cover and refrigerate the rest of the rolled cake. 



![Recipe of cake-rolls-a-la-mousse au chocolat](https://www.amourdecuisine.fr/wp-content/uploads/2016/12/Recette-de-G%C3%A2teau-roul%C3%A9-%C3%A0-la-mousse-au-chocolat.jpg)
