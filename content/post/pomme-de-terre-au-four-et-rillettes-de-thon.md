---
title: baked potato and tuna rillettes
date: '2016-11-05'
categories:
- amuse bouche, tapas, mise en bouche
- Bourek, brick, samoussa, slippers
tags:
- Algeria
- Full Dishes
- Easy cooking
- Oven cooking
- Healthy cuisine
- aperitif
- Vegetables

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-en-robe-des-champs-et-rillettes-au-thon-2.jpg
---
[ ![baked potato and tuna rillettes](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-en-robe-des-champs-et-rillettes-au-thon-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-en-robe-des-champs-et-rillettes-au-thon-1.jpg>)

##  potato in field dress rillettes of tuna 

Hello everybody, 

Today I share with you a recipe that I discovered here in England "the jacket potatoe" or **baked potato** and tuna rillettes. If you came to England and did not eat jackets potatoes, then you did not eat anything English. 

In France, this recipe is known as baked potato in field dress, or even potatoes in room dress, because the potatoes are baked with the skin. 

It took me a long time to cook, first of all you have to have the good potato, you need the mashed potato, rich in starch. Here in England, I do not break my head too much, because I go to the super walk and I easily find the potato potatoe in a clean bag of his land. 

When I go out with the kids, I always like to go to spudlike to eat my jacket potato with tuna rillette and maís, too good, and I went to ask the secret of this super-cooked and mellow potato, and I had the trick, no foil, no cooking in the water, just the time, all the time it takes.   


**baked potato and tuna rillettes**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-en-robe-des-champs-et-rillettes-au-thon-1.jpg)

portions:  4  Prep time:  5 mins  cooking:  100 mins  total:  1 hour 45 mins 

**Ingredients**

  * 4 good quality potatoes well brushed and washed well 
  * 1 tablespoon of extra virgin olive oil 
  * salt (I prefer that of the mill) 
  * 1 tablespoon of butter. 

tuna rillettes: 
  * 4 tablespoons of [ homemade mayonnaise ](<https://www.amourdecuisine.fr/article-mayonnaise-fait-maison-rapide-et-facile.html>)
  * 250 g tuna in oil (drained from its oil) 
  * 1 beautiful handful of corn 
  * salt, black pepper according to taste (with homemade mayonnaise no need to season, nor the addition of garlic in the rillette) 



**Realization steps**

  1. Start by preparing tuna rillette to let it taste well. 
  2. turn on the oven at 200 degrees C 
  3. prick the potatoes with a fork. 
  4. soak the apple of your hands with the oil and brush the potatoes well 
  5. place them in a baking tin and sprinkle generously with salt 
  6. put in the oven in the middle shelf and cook for 1 hour on one side, then turn to cook on the other side, between 20 and 40 minutes 
  7. check the cooking by pricking one of the potatoes with a curedent, the apple must be super tender inside. the skin can be a little hard but it does not matter (the English like to eat this crispy skin). 
  8. remove the potatoes from the oven, and cut them in half   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-bien-chaude.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-bien-chaude.jpg>)
  9. add a little butter while it's still hot   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-en-robe-des-champs-et-rillettes-au-thon3_.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-en-robe-des-champs-et-rillettes-au-thon3_.jpg>)
  10. try to pass the butter over the entire surface of the potato. 
  11. cover with generous layer of rillettes with tuna and taste by mixing the hot potato with the cold rillette, it is to fall! 



[ ![Potato in Field Dress and Tuna Rillettes 4](https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-en-robe-des-champs-et-rillettes-au-thon-4.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/02/pomme-de-terre-en-robe-des-champs-et-rillettes-au-thon-4.jpg>)
