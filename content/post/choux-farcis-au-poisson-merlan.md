---
title: cabbage stuffed with whiting fish
date: '2015-11-27'
categories:
- Algerian cuisine
tags:
- Full Dish
- Ramadan 2015
- Ramadan
- Ramadhan
- Healthy cuisine

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-farcis-au-poisson.jpg
---
[ ![stuffed cabbage with fish](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-farcis-au-poisson.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-farcis-au-poisson.jpg>)

##  cabbage stuffed with whiting fish 

Hello everybody, 

I admit when I saw this recipe from my friend **Hichem N** I was surprised, because I never thought, nor ever eat a recipe of cabbage stuffed with fish !!! I always eat choux à la mince, and I like this idea a lot. 

So, thank you very much Hichem N for this recipe, and especially for us to have discovered ... and you know this recipe? a comment from my friend, the recipe is a delight with any white fish, but not recommended with blue fish or sardines ... 

**cabbage stuffed with whiting fish**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-farci-au-poisson-blanc.jpg)

portions:  6  Prep time:  30 mins  cooking:  40 mins  total:  1 hour 10 mins 

**Ingredients** for fish balls: 

  * the pulpit of fish (whiting or any other white fish) without stops 
  * black pepper, salt, paprika, ginger 
  * 2 to 3 cloves of garlic 
  * chopped parsley 
  * rice 
  * Steamed cabbage leaves 

For the sauce: 
  * 1 chopped onion 
  * 2 cloves garlic 
  * 2 tomatoes grated 
  * salt, black pepper, ginger, paprika 
  * 2 potatoes cut in large cubes 



**Realization steps**

  1. place the fish pulpit in the bowl of a large blinder (chopper) 
  2. add spices, garlic and chopped parsley) 
  3. mix to have some kind of puree   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-farcis-au-merlan.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-farcis-au-merlan.jpg>)
  4. remove all in a salad bowl 
  5. add a small handful of rice 
  6. and form pellets. 
  7. Detail the cabbage leaves, without breaking them, place them to steam 
  8. after cooling, take each pellet and roll it carefully into the cabbage leaf to cover it well.   
[ ![](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-au-merlan.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-au-merlan.jpg>)
  9. Prepare the sauce: 
  10. fry the chopped onion and crushed garlic in a little oil. 
  11. add the grated tomato and spices. 
  12. let go back a little. 
  13. add the pieces of potatoes, and put over the rolls of cabbage stuffed with sardines, and reduce the sauce 



[ ![cabbage stuffed with fish 5](https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-farci-au-poisson-5.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2015/11/choux-farci-au-poisson-5.jpg>)
