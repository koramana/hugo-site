---
title: Raspberry pistachios tart
date: '2017-11-27'
categories:
- sweet recipes
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-1.1.jpg
---
[ ![Raspberry pistachio tart recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-1.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-1.1.jpg>)

##  Recipe for Raspberry Pistachio Pie 

Hello everybody, 

Here is a very beautiful and delicious pie pistachios and raspberries, yes it will not be easy to find raspberries very cool at this time of the year, but you never know ... Anyway, if you like pistachio paste, this raspberry pistachio tart is just for you. A taste that will surely delight your taste buds and savor your pupils because each bite you have this cream of pistachio all sweet and melting, which hides just behind the subtle taste of juicy raspberries and tart. I assure you, you will not stop at a single bite, and do not blame me for the pounds after. 

This recipe for pistachio and raspberry pie comes from our beloved Lunetoiles, whom I thank for passing all her help on my blog, and all the recipes she shares with us. 

[ ![Raspberry pistachios tart](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-4.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-4.1.jpg>)   


**Raspberry pistachios tart**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-2.1.jpg)

Recipe type:  Dessert  portions:  8  Prep time:  15 mins  cooking:  25 mins  total:  40 mins 

**Ingredients**

  * A shortbread dough from the shops or home-made 

Almond pistachio cream: 
  * 110 gr of soft butter 
  * 80 g caster sugar 
  * 125 g of almond powder 
  * 10 g of maizena 
  * 1 egg 
  * 2 tbsp. pistachio paste 
  * frozen raspberries (100 gr) 



**Realization steps**

  1. Preheat the oven to 180 ° C. 
  2. Go for the pie plate with the shortcrust pastry, meanwhile prepare the pistachio cream. 
  3. Beat the butter with the powdered sugar then add the egg and the cornflour. 
  4. Stir in the almond powder while continuing to whisk. 
  5. At the end, add the pistachio paste and mix. 
  6. Spread the cream over the shortcrust pastry and arrange the raspberries by pressing them lightly. 
  7. Bake at 180 ° C and cook for about 25 minutes, on leaving the oven let cool completely. and book until tasting! 



[ ![Raspberry pistachio tart recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-b.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-infiniment-pistache-b.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm3.png)
