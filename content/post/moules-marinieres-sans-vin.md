---
title: Mussels without wine
date: '2017-12-25'
categories:
- diverse cuisine
- fish and seafood recipes
tags:
- Sea food
- Easy cooking
- Healthy cuisine
- Algeria
- Ramadan 2016
- Summer kitchen
- dishes

image: https://www.amourdecuisine.fr/wp-content/uploads/2016/03/moule-%C3%A0-la-marini%C3%A8re.jpg
---
[ ![Mussels without wine](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/moule-%C3%A0-la-marini%C3%A8re.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/moule-%C3%A0-la-marini%C3%A8re.jpg>)

##  Mussels without wine 

Hello everybody, 

Mussels marinières without wine, This Monday my husband went to the fish shop (fishery) of the corner to come back with a good amount of mussels ... He likes the mussels, I prepare him every time and at his request the mussels with the provençale, with my husband, all that is tomato sauce is always: the best recipe in the world, hihihihi. 

The recipe was a delight, and I was amazed that my husband joined me to taste some pieces, the taste was even better than what I had in mind, so thank you Samar for this beautiful recipe.   


**Mussels**

![](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/moule-marini%C3%A8re-1.jpg)

portions:  2  Prep time:  10 mins  cooking:  20 mins  total:  30 mins 

**Ingredients**

  * 500 gr of mussels 
  * 1 shallot 
  * 1 branch of thyme 
  * 1 bay leaf 
  * 75 ml of wine (replace with vinegar) 
  * 25 g of butter 
  * a handful of finely chopped parsley 
  * 75 ml liquid cream 



**Realization steps**

  1. Clean the molds while rubbing them under running water.   
If there are broken or open molds, discard them. 
  2. Melt the butter in a heavy-bottomed saucepan. Add the shallot and garlic, cook on medium heat until it becomes very tender. 
  3. Add vinegar, bay leaf and thyme. 
  4. Turn down the heat and let it cook for a moment. 
  5. Add the mussels, increase the heat, cover and cook until the mussels open. Discard the mussels that remain closed. 
  6. Add the cream and cook for another minute. Add the chopped parsley. 
  7. Serve immediately. 



[ ![Mussels without wine](https://www.amourdecuisine.fr/wp-content/uploads/2016/03/moule-marini%C3%A8re-1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2016/03/moule-marini%C3%A8re-1.jpg>)
