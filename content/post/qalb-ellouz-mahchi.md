---
title: Qalb ellouz Mahchi
date: '2007-12-15'
categories:
- Algerian cakes- oriental- modern- fete aid
- shortbread cakes, ghribiya
- Algerian dry cakes, petits fours

image: https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200889661.jpg
---
& Nbsp; & Nbsp; 1 kg of semolina, 500 gr of caster sugar, 1 glass of smen, 1 tea glass of orange blossom water, 1/2 glass of water, 1 pinch of salt, Stuffing: 150 g of almonds 125 g fine caster sugar, a pinch of cinnamon. Syrup: 500 gr of sugar 1 L of water Mix 500 grs of sugar with the sieved semolina. Sprinkle with smen (dirty butter), sabler between hands to penetrate the fat. & Nbsp; Add the salt, the orange blossom water while continuing to sand well to have a homogeneous mixture. Asperger & hellip; 

##  Overview of tests 

**User Rating:** 3.52  (  2  ratings)  0 

![Bonj](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200930841.gif)

  * 1 kg of semolina, 
  * 500 gr of caster sugar, 
  * 1 glass of smen, 
  * 1 teaspoon of orange blossom water, 
  * 1/2 glass of water, 
  * 1 pinch of salt, 



**![rose2](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/20093704.png) **

**Prank call :**

  * 150 g ground almonds, 
  * 125 g fine caster sugar, 
  * 1 pinch of cinnamon. 



**![rose2](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/20093704.png) **

**Syrup:**

  * 500 gr of sugar 
  * 1 L of water 



![rose2](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/20093704.png)

Mix 500 grs of sugar with the sieved semolina. Sprinkle with smen (dirty butter), sabler between hands to penetrate the fat. 

![S7301620](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200889661.jpg)

Add the salt, the orange blossom water while continuing to sand well to have a homogeneous mixture. Spray with water and work for a moment, let stand a few minutes. Meanwhile, prepare the stuffing by mixing the almonds, sugar, cinnamon, well incorporate the ingredients to each other butter a baking tin; spread a layer of semolina on top add the stuffing and the rest of semolina spread well to have a uniform surface. 

![S7301621](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200890441.jpg)

Draw diamonds or squares. Stitch each almond on a cake. 

![S7301622](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200891001.jpg)

Put in the oven 40 minutes (Thermostat 6). Prepare in parallel the syrup with the 500 grs of sugar and the liter of water. 

Remove from the oven as soon as the cake is golden brown, sprinkle copiously with syrup, let stand and sprinkle with remaining syrup.    
![S7301623](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200891931.jpg)

![bye](https://www.amourdecuisine.fr/wp-content/uploads/2007/12/200940871.gif)
