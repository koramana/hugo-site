---
title: Easy homemade broken pasta recipe
date: '2014-12-01'
categories:
- basic pastry recipes
- sweet recipes
- pies and tarts
tags:
- Algerian cakes
- Pastry
- desserts
- Sweet pie
- pies
- Based
- Easy cooking

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-brisee.jpg
---
[ ![Easy homemade broken pasta recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-brisee.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/pate-brisee.jpg>)

##  Easy homemade pasta recipe 

Hello everybody, 

Homemade Broken Pasta Recipe: Broken pastry is a must-know dish in the kitchen, it is a basic recipe often used as a base for salty or sweet pie and often as a base for quiches. You may be telling me, but what's the difference between a [ shortbread ](<https://www.amourdecuisine.fr/article-pate-sablee-facile-et-rapide-114711793.html> "quick and easy sandblast") and a broken dough? Well the difference is the addition of cold water in the broken dough, which will make the dough a little crumbling too much. 

There are broken pasta recipes that contain egg yolk, not too important to add, but the addition of egg yolk into the dough gives it even more hold. 

Personally, I do not have a preference between the two pasta: shortbread or short pastry ??? But generally, I like the broken dough to prepare my quiches, yes I know too much butter, but it remains that the broken dough gives a more to the taste of the quiche, in addition this very soft dough which is easily cut with the fork , facilitates the tasting of quiches, and as I say, it's a personal opinion.   


**Easy homemade broken pasta recipe**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/recette-pate-brisee-1.jpg)

portions:  8  Prep time:  15 mins  cooking:  20 mins  total:  35 mins 

**Ingredients**

  * 200 g T55 flour (1 cup ½) 
  * 100 g of butter 
  * ½ teaspoon fine salt 
  * ½ glass of water (40 ml) 

For a sweet preparation: 1 tablespoon of sugar 

**Realization steps**

  1. place the flour, salt and butter cut in small pieces in a salad bowl and sand between your fingers 
  2. Add the water gradually. Mix and gather to form a soft ball 
  3. stop adding the water as soon as the dough is curled up and does not stick anymore 
  4. cover the dough with a film of flour and let it rest 20 to 30 min at least 
  5. Spread the dough on a rolling pin with a rolling pin. 
  6. Flank the mold and lightly press the dough against the walls. 
  7. Cut out the excess with a knife or by rolling the roll on the edges. 
  8. the pasta is ready for white cooking, or to garnish and cook. 



[ ![Easy homemade broken pasta recipe](https://www.amourdecuisine.fr/wp-content/uploads/2014/11/recette-pate-brisee.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/11/recette-pate-brisee.jpg>)
