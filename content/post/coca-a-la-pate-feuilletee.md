---
title: Coke has puff pastry
date: '2016-10-08'
categories:
- appetizer, tapas, appetizer
- Bourek, brick, samoussa, slippers
- Algerian cuisine
- ramadan recipe
tags:
- inputs
- Algeria
- Morocco
- Amuse bouche
- Tunisia
- Ramadan 2015
- Ramadan

image: https://www.amourdecuisine.fr/wp-content/uploads/2015/06/la-coca-a-la-pate-feuillet%C3%A9e-5.jpg
---
[ ![Coke has puff pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/la-coca-a-la-pate-feuillet%C3%A9e-5.jpg) ](<https://www.amourdecuisine.fr/article-coca-a-la-pate-feuilletee.html/la-coca-a-la-pate-feuilletee-5>)

##  Coke has puff pastry 

Hello everybody, 

Here is a recipe for coca with puff pastry, super easy to make, especially if you have puff pastry trade ... In the blink of an eye, your coke is ready to consume. 

Coke with puff pastry is a recipe that we like a lot at home, sometimes I prepare puff pastry in advance, whether to make coca in the following day, or sometimes I freeze it, and when I want to make my coke, I remove the dough from the freezer at least 12 hours before. 

This time, I went to the simplest version, with the puff pastry trade, It is just enough to prepare the stuffing and let it cool before filling the cake, and here is a nice coca puff pastry like the one that is sells in Algeria ... A coke that I ate so much at the end of the university ... it was my little consolation after a long day of work and studies ... 

**Coke has puff pastry**

![](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/la-coca-a-la-pate-feuillet%C3%A9e-5.jpg)

portions:  4-6 

**Ingredients**

  * 2 rolls of commercial puff pastry 
  * 2 medium onions 
  * 1 can of tomatoes in pieces. 
  * 1 clove of garlic 
  * 2 tablespoons of olive oil 
  * coriander powder 
  * salt and black pepper 
  * black olives 
  * 1 can of tuna (optional) 
  * 1 egg 



**Realization steps**

  1. cut the onions into strips, and fry them in a little oil 
  2. add the crushed garlic 
  3. then add the tomato and season to taste. 
  4. when the sauce is well reduced, let it cool 
  5. roll out the puff pastry, spread it on a baking tray, covered with baking paper. 
  6. prick the puff pastry with a fork 
  7. pour the stuffing gently, leaving almost 2 cm of space all around the edges 
  8. add the tuna crumbled over. 
  9. brush the clean space of the dough with the egg white. 
  10. cover the stuffing with the second layer of puff pastry pricked beforehand. 
  11. stick the ends of the two pasta well, and trace with the end of the fork to join them well. 
  12. brush the puff pastry with egg yolk, trace gently with a knife according to your choice 
  13. prick olives on the surface. 
  14. cook the coca in a preheated oven at 160 degrees, until the dough is completely cooked, and until the cake is golden brown. 



[ ![Coke has puff pastry](https://www.amourdecuisine.fr/wp-content/uploads/2015/06/la-coca-a-la-pate-feuillet%C3%A9e-6.jpg) ](<https://www.amourdecuisine.fr/article-coca-a-la-pate-feuilletee.html/la-coca-a-la-pate-feuilletee-6>)
