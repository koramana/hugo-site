---
title: bourdaloue pie of lunetoile after Lenôtre
date: '2014-04-10'
categories:
- pies and tarts

image: https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-bourdaloue.1.jpg
---
[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-bourdaloue.1.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-bourdaloue.1.jpg>)

Hello everybody, 

The frangipane pies are always a delight, and this bourdaloue pie after Lenôtre is a recipe that shares with us Lunetoiles, and as always we only appreciate these recipes. 

on my blog there is already a bourdaloue pie .... but it is different from this version because already there is a layer of cream pastry, that must add an infinite sweetness to the recipe ... 

And normally in the layer of frangipane, we can add broken macaroons, yumi yumi, a pity for Lunetoiles because she did not have that for her recipe. 

In any case, the frangipane cream is always a delight, whether with broken macaroons or without. 

the recipe is a bit long to make, but it's worth it to present a delicious dessert like this one. 

**bourdaloue pie of lunetoile after Lenôtre**

![](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-bourdaloue-1.1.jpg)

Recipe type:  dessert  portions:  8  Prep time:  30 mins  cooking:  45 mins  total:  1 hour 15 mins 

**Ingredients** Shortbread: 

  * 150 g flour 
  * 1 pinch of baking powder 
  * 50 g of sugar 
  * 75 g of soft butter 
  * 1 egg yolk 
  * 1 tablespoon of water 

Poached pears: 
  * 5 pears (I only had 3 left) 
  * 1 liter of water 
  * 150 g of sugar 
  * ½ vanilla pod 
  * 1 badiane 
  * 1 cinnamon stick 
  * lemon juice 

Custard: 
  * 250 g of milk 
  * ½ vanilla pod 
  * 50 g of sugar 
  * 3 egg yolks 
  * 20 g of flan powder (or Maizena) 

Almond cream: 
  * 100 g of butter 
  * 75 g icing sugar 
  * 75 g of almond powder 
  * 1 cc domed of Maïzena 
  * 30 g of egg 
  * 50 g of macaroons (I did not put any) 



**Realization steps** shortbread 

  1. In Thermomix, put all the ingredients in the bowl, then knead for about 30 seconds. 
  2. In the hand, put the flour, the yeast and the sugar in a salad bowl. Add the soft butter in the center, and rub in the hands until you have a consistency of sand. Then add the egg without much work. 
  3. Put the dough in a cool food film for a few hours so that it works easier. 

Poached pears: 
  1. In a saucepan, put the water, sugar and spices. 
  2. Bring to a boil. 
  3. Meanwhile, peel the pears and dig out the inside by the base to remove the pips. 
  4. Lemon them lightly. 
  5. Put the pears in the boiling syrup, and cook on medium heat for about 15 minutes. 
  6. Let cool. 

Custard: 
  1. bring milk and vanilla to a boil. Let infuse a few minutes. 
  2. In a bowl, whisk the yolks and sugar until you have a frothy mixture. 
  3. Add the flan powder. Mix. 
  4. Pour the boiling milk over the yolks, gradually and whisking. 
  5. Pour everything into the pan and let thicken over low heat while stirring briskly. 
  6. Put the cream in a bowl and film on contact. Let cool. 

Almond cream: 
  1. Whisk the butter until it is soft. 
  2. Add the icing sugar, the almond powder and the flan powder. Mix. 
  3. Beat 1 egg and add 30 g. 
  4. Whisk for 3 minutes. 
  5. Add the crumbled amaretti. Mix. 

Mounting: 
  1. Preheat the oven to 170 ° C. 
  2. Mix the almond cream with 175 g pastry cream. 
  3. Spread the dough on a floured work surface and garnish a 24 cm circle. 
  4. Spread the cream on the dough. 
  5. Arrange an entire pears in the center, then the half-pears all around. 
  6. Cook for about 40 minutes. 
  7. Leave the pie warm before decanting. 
  8. Sprinkle with icing sugar and lightly brush pears with cooking syrup using a brush. 
  9. Reserve at room temperature. 



[ ![SONY DSC](https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-bourdaloue-2.jpg) ](<https://www.amourdecuisine.fr/wp-content/uploads/2014/04/tarte-bourdaloue-2.jpg>)

![visitecomm.png](https://www.amourdecuisine.fr/wp-content/uploads/2013/09/visitecomm.png)

and thank you to sign up for the newsletter, just click on this image, and then confirm the confirmation email that should arrive on your email address you have put. 

Note: people who can not validate their registration will be deleted after 2 days, so they can try to register again, but it is really important to validate and confirm the email you receive on your box, from feedBurner. 

merci 
